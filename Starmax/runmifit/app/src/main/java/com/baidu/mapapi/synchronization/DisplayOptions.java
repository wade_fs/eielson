package com.baidu.mapapi.synchronization;

import android.view.View;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import java.util.ArrayList;
import java.util.List;

public final class DisplayOptions {

    /* renamed from: a */
    private static final String f3136a = DisplayOptions.class.getSimpleName();

    /* renamed from: A */
    private View f3137A;

    /* renamed from: B */
    private View f3138B;

    /* renamed from: C */
    private View f3139C;

    /* renamed from: D */
    private boolean f3140D;

    /* renamed from: E */
    private boolean f3141E;

    /* renamed from: F */
    private int f3142F;

    /* renamed from: G */
    private boolean f3143G;

    /* renamed from: H */
    private boolean f3144H;

    /* renamed from: I */
    private int f3145I;

    /* renamed from: J */
    private boolean f3146J;

    /* renamed from: K */
    private boolean f3147K;

    /* renamed from: L */
    private int f3148L;

    /* renamed from: M */
    private int f3149M;

    /* renamed from: N */
    private int f3150N;

    /* renamed from: O */
    private int f3151O;

    /* renamed from: P */
    private int f3152P;

    /* renamed from: Q */
    private boolean f3153Q;

    /* renamed from: R */
    private List<BitmapDescriptor> f3154R;

    /* renamed from: b */
    private BitmapDescriptor f3155b;

    /* renamed from: c */
    private boolean f3156c;

    /* renamed from: d */
    private boolean f3157d;

    /* renamed from: e */
    private int f3158e;

    /* renamed from: f */
    private BitmapDescriptor f3159f;

    /* renamed from: g */
    private boolean f3160g;

    /* renamed from: h */
    private boolean f3161h;

    /* renamed from: i */
    private int f3162i;

    /* renamed from: j */
    private BitmapDescriptor f3163j;

    /* renamed from: k */
    private boolean f3164k;

    /* renamed from: l */
    private boolean f3165l;

    /* renamed from: m */
    private int f3166m;

    /* renamed from: n */
    private BitmapDescriptor f3167n;

    /* renamed from: o */
    private boolean f3168o;

    /* renamed from: p */
    private boolean f3169p;

    /* renamed from: q */
    private int f3170q;

    /* renamed from: r */
    private boolean f3171r;

    /* renamed from: s */
    private boolean f3172s;

    /* renamed from: t */
    private int f3173t;

    /* renamed from: u */
    private BitmapDescriptor f3174u;

    /* renamed from: v */
    private BitmapDescriptor f3175v;

    /* renamed from: w */
    private BitmapDescriptor f3176w;

    /* renamed from: x */
    private BitmapDescriptor f3177x;

    /* renamed from: y */
    private BitmapDescriptor f3178y;

    /* renamed from: z */
    private int f3179z;

    public DisplayOptions() {
        this.f3158e = 6;
        this.f3162i = 7;
        this.f3164k = true;
        this.f3165l = true;
        this.f3166m = 8;
        this.f3170q = 10;
        this.f3173t = 5;
        this.f3140D = true;
        this.f3141E = true;
        this.f3142F = 6;
        this.f3143G = true;
        this.f3144H = true;
        this.f3145I = 7;
        this.f3146J = true;
        this.f3147K = true;
        this.f3148L = 8;
        this.f3149M = 50;
        this.f3150N = 50;
        this.f3151O = 50;
        this.f3152P = 50;
        this.f3154R = new ArrayList();
        this.f3155b = BitmapDescriptorFactory.fromAssetWithDpi("SDK_Default_Icon_Start.png");
        this.f3156c = true;
        this.f3157d = true;
        this.f3159f = BitmapDescriptorFactory.fromAssetWithDpi("SDK_Default_Icon_End.png");
        this.f3160g = true;
        this.f3161h = true;
        this.f3163j = BitmapDescriptorFactory.fromAssetWithDpi("SDK_Default_Icon_Car.png");
        this.f3164k = true;
        this.f3165l = true;
        this.f3167n = BitmapDescriptorFactory.fromAssetWithDpi("SDK_Default_Icon_Passenger.png");
        this.f3168o = true;
        this.f3169p = true;
        this.f3171r = true;
        this.f3172s = true;
        this.f3174u = BitmapDescriptorFactory.fromAsset("SDK_Default_Traffic_Texture_Smooth.png");
        this.f3175v = BitmapDescriptorFactory.fromAsset("SDK_Default_Traffic_Texture_Slow.png");
        this.f3176w = BitmapDescriptorFactory.fromAsset("SDK_Default_Traffic_Texture_Congestion.png");
        this.f3177x = BitmapDescriptorFactory.fromAsset("SDK_Default_Traffic_Texture_SevereCongestion.png");
        this.f3178y = BitmapDescriptorFactory.fromAsset("SDK_Default_Route_Texture_Bule_Arrow.png");
        this.f3179z = 22;
    }

    public boolean get3DCarMarkerEnable() {
        return this.f3153Q;
    }

    public List<BitmapDescriptor> get3DCarMarkerIconList() {
        return this.f3154R;
    }

    public BitmapDescriptor getCarIcon() {
        return this.f3163j;
    }

    public View getCarInfoWindowView() {
        return this.f3137A;
    }

    public int getCarPositionInfoWindowZIndex() {
        return this.f3148L;
    }

    public int getCarPositionMarkerZIndex() {
        return this.f3166m;
    }

    public BitmapDescriptor getEndPositionIcon() {
        return this.f3159f;
    }

    public View getEndPositionInfoWindowView() {
        return this.f3139C;
    }

    public int getEndPositionInfoWindowZIndex() {
        return this.f3145I;
    }

    public int getEndPositionMarkerZIndex() {
        return this.f3162i;
    }

    public int getPaddingBottom() {
        return this.f3152P;
    }

    public int getPaddingLeft() {
        return this.f3149M;
    }

    public int getPaddingRight() {
        return this.f3150N;
    }

    public int getPaddingTop() {
        return this.f3151O;
    }

    public BitmapDescriptor getPassengerIcon() {
        return this.f3167n;
    }

    public int getPassengerMarkerZIndex() {
        return this.f3170q;
    }

    public int getRouteLineWidth() {
        return this.f3179z;
    }

    public int getRouteLineZIndex() {
        return this.f3173t;
    }

    public BitmapDescriptor getStartPositionIcon() {
        return this.f3155b;
    }

    public View getStartPositionInfoWindowView() {
        return this.f3138B;
    }

    public int getStartPositionInfoWindowZIndex() {
        return this.f3142F;
    }

    public int getStartPositionMarkerZIndex() {
        return this.f3158e;
    }

    public List<BitmapDescriptor> getTrafficTextureList() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.f3178y);
        arrayList.add(this.f3174u);
        arrayList.add(this.f3175v);
        arrayList.add(this.f3176w);
        arrayList.add(this.f3177x);
        return arrayList;
    }

    public boolean isShowCarInfoWindow() {
        return this.f3146J;
    }

    public boolean isShowCarMarker() {
        return this.f3164k;
    }

    public boolean isShowCarMarkerInSpan() {
        return this.f3165l;
    }

    public boolean isShowEndPositionInfoWindow() {
        return this.f3143G;
    }

    public boolean isShowEndPositionMarker() {
        return this.f3160g;
    }

    public boolean isShowEndPositionMarkerInSpan() {
        return this.f3161h;
    }

    public boolean isShowPassengerIcon() {
        return this.f3168o;
    }

    public boolean isShowPassengerIconInSpan() {
        return this.f3169p;
    }

    public boolean isShowRoutePlan() {
        return this.f3171r;
    }

    public boolean isShowRoutePlanInSpan() {
        return this.f3172s;
    }

    public boolean isShowStartPositionInfoWindow() {
        return this.f3140D;
    }

    public boolean isShowStartPositionMarker() {
        return this.f3156c;
    }

    public boolean isShowStartPositionMarkerInSpan() {
        return this.f3157d;
    }

    public DisplayOptions set3DCarMarkerEnable(boolean z) {
        this.f3153Q = z;
        return this;
    }

    public DisplayOptions set3DCarMarkerIconList(List<BitmapDescriptor> list) {
        if (list == null || list.isEmpty()) {
            this.f3154R = null;
        } else {
            this.f3154R.addAll(list);
        }
        return this;
    }

    public DisplayOptions setCarIcon(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3163j = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: CarIcon descriptor is null");
    }

    public DisplayOptions setCarInfoWindowView(View view) {
        this.f3137A = view;
        return this;
    }

    public DisplayOptions setCarPositionInfoWindowZIndex(int i) {
        if (i <= 1) {
            i = 2;
        }
        this.f3148L = i;
        return this;
    }

    public DisplayOptions setCarPositionMarkerZIndex(int i) {
        if (i <= 1) {
            i = 2;
        }
        this.f3166m = i;
        return this;
    }

    public DisplayOptions setCongestionTrafficTexture(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3176w = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: CongestionTrafficTexture descriptor is null");
    }

    public DisplayOptions setEndPositionIcon(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3159f = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: EndPositionIcon descriptor is null");
    }

    public DisplayOptions setEndPositionInfoWindowView(View view) {
        this.f3139C = view;
        return this;
    }

    public DisplayOptions setEndPositionInfoWindowZIndex(int i) {
        if (i <= 1) {
            i = 2;
        }
        this.f3145I = i;
        return this;
    }

    public DisplayOptions setEndPositionMarkerZIndex(int i) {
        if (i <= 1) {
            i = 2;
        }
        this.f3162i = i;
        return this;
    }

    public DisplayOptions setMapViewPadding(int i, int i2, int i3, int i4) {
        if (i < 0 || i3 < 0 || i2 < 0 || i4 < 0) {
            String str = f3136a;
            C1120a.m3855b(str, "Padding param is invalid. paddingLeft = " + i + "; paddingRight = " + i3 + "; paddingTop = " + i2 + "; paddingBottom = " + i4);
        }
        if (i < 0) {
            i = 30;
        }
        this.f3149M = i;
        if (i2 < 0) {
            i2 = 30;
        }
        this.f3151O = i2;
        if (i3 < 0) {
            i3 = 30;
        }
        this.f3150N = i3;
        if (i4 < 0) {
            i4 = 50;
        }
        this.f3152P = i4;
        return this;
    }

    public DisplayOptions setPassengerIcon(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3167n = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: PassengerIcon descriptor is null");
    }

    public DisplayOptions setPassengerMarkerZIndex(int i) {
        if (i <= 1) {
            i = 2;
        }
        this.f3170q = i;
        return this;
    }

    public DisplayOptions setRouteLineWidth(int i) {
        int i2 = 5;
        if (i >= 5) {
            i2 = 40;
            if (i <= 40) {
                this.f3179z = i;
                return this;
            }
        }
        this.f3179z = i2;
        return this;
    }

    public DisplayOptions setRouteLineZIndex(int i) {
        if (i <= 1) {
            i = 2;
        }
        this.f3173t = i;
        return this;
    }

    public DisplayOptions setSevereCongestionTrafficTexture(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3177x = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: SevereCongestionTrafficTexture descriptor is null");
    }

    public DisplayOptions setSlowTrafficTexture(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3175v = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: SlowTrafficTexture descriptor is null");
    }

    public DisplayOptions setSmoothTrafficTexture(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3174u = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: SmoothTrafficTexture descriptor is null");
    }

    public DisplayOptions setStartPositionIcon(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3155b = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: StartPositionIcon descriptor is null");
    }

    public DisplayOptions setStartPositionInfoWindowView(View view) {
        this.f3138B = view;
        return this;
    }

    public DisplayOptions setStartPositionInfoWindowZIndex(int i) {
        if (i <= 1) {
            i = 2;
        }
        this.f3142F = i;
        return this;
    }

    public DisplayOptions setStartPositionMarkerZIndex(int i) {
        if (i <= 1) {
            i = 2;
        }
        this.f3158e = i;
        return this;
    }

    public DisplayOptions setUnknownTrafficTexture(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f3178y = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: UnknownTrafficTexture descriptor is null");
    }

    public DisplayOptions showCarIcon(boolean z) {
        this.f3164k = z;
        return this;
    }

    public DisplayOptions showCarIconInSpan(boolean z) {
        this.f3165l = z;
        return this;
    }

    public DisplayOptions showCarInfoWindow(boolean z) {
        this.f3146J = z;
        return this;
    }

    public DisplayOptions showCarInfoWindowInSpan(boolean z) {
        this.f3147K = z;
        return this;
    }

    public DisplayOptions showEndPositionIcon(boolean z) {
        this.f3160g = z;
        return this;
    }

    public DisplayOptions showEndPositionIconInSpan(boolean z) {
        this.f3161h = z;
        return this;
    }

    public DisplayOptions showEndPositionInfoWindow(boolean z) {
        this.f3143G = z;
        return this;
    }

    public DisplayOptions showEndPositionInfoWindowInSpan(boolean z) {
        this.f3144H = z;
        return this;
    }

    public DisplayOptions showPassengereIcon(boolean z) {
        this.f3168o = z;
        return this;
    }

    public DisplayOptions showPassengereIconInSpan(boolean z) {
        this.f3169p = z;
        return this;
    }

    public DisplayOptions showRoutePlan(boolean z) {
        this.f3171r = z;
        return this;
    }

    public DisplayOptions showRoutePlanInSpan(boolean z) {
        this.f3172s = z;
        return this;
    }

    public DisplayOptions showStartPositionIcon(boolean z) {
        this.f3156c = z;
        return this;
    }

    public DisplayOptions showStartPositionIconInSpan(boolean z) {
        this.f3157d = z;
        return this;
    }

    public DisplayOptions showStartPositionInfoWindow(boolean z) {
        this.f3140D = z;
        return this;
    }

    public DisplayOptions showStartPositionInfoWindowInSpan(boolean z) {
        this.f3141E = z;
        return this;
    }
}
