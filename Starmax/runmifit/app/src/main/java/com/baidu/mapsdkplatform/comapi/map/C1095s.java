package com.baidu.mapsdkplatform.comapi.map;

import android.os.Handler;
import android.os.Message;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.s */
class C1095s extends Handler {

    /* renamed from: a */
    final /* synthetic */ C1094r f3604a;

    C1095s(C1094r rVar) {
        this.f3604a = rVar;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (C1094r.f3600c != null) {
            this.f3604a.f3602d.mo13139a(message);
        }
    }
}
