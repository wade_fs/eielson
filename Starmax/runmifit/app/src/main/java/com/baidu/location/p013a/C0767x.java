package com.baidu.location.p013a;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import com.baidu.location.p019g.C0855k;

/* renamed from: com.baidu.location.a.x */
public class C0767x {

    /* renamed from: a */
    private static Object f1469a = new Object();

    /* renamed from: b */
    private static C0767x f1470b = null;

    /* renamed from: c */
    private HandlerThread f1471c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Handler f1472d;

    /* renamed from: e */
    private boolean f1473e = false;

    C0767x() {
    }

    /* renamed from: a */
    public static C0767x m1972a() {
        C0767x xVar;
        synchronized (f1469a) {
            if (f1470b == null) {
                f1470b = new C0767x();
            }
            xVar = f1470b;
        }
        return xVar;
    }

    /* renamed from: a */
    public void mo10509a(Location location, int i) {
        if (this.f1473e && location != null) {
            try {
                if (this.f1472d != null) {
                    Message obtainMessage = this.f1472d.obtainMessage(1);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("loc", new Location(location));
                    bundle.putInt("satnum", i);
                    obtainMessage.setData(bundle);
                    obtainMessage.sendToTarget();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: b */
    public void mo10510b() {
        if (this.f1473e) {
            try {
                if (this.f1472d != null) {
                    this.f1472d.obtainMessage(3).sendToTarget();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: c */
    public void mo10511c() {
        if (this.f1473e) {
            try {
                if (this.f1472d != null) {
                    this.f1472d.obtainMessage(2).sendToTarget();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: d */
    public void mo10512d() {
        if (this.f1473e) {
            try {
                if (this.f1472d != null) {
                    this.f1472d.obtainMessage(7).sendToTarget();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: e */
    public void mo10513e() {
        if (!this.f1473e) {
            this.f1473e = true;
            if (this.f1471c == null) {
                this.f1471c = new HandlerThread("LocUploadThreadManager");
                this.f1471c.start();
                HandlerThread handlerThread = this.f1471c;
                if (handlerThread != null) {
                    this.f1472d = new C0768y(this, handlerThread.getLooper());
                }
            }
            try {
                if (this.f1472d != null) {
                    this.f1472d.obtainMessage(5).sendToTarget();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (this.f1472d != null) {
                    this.f1472d.sendEmptyMessageDelayed(4, (long) C0855k.f1917R);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* renamed from: f */
    public void mo10514f() {
        if (this.f1473e) {
            C0731d.m1782a().mo10450b();
            try {
                if (this.f1472d != null) {
                    this.f1472d.removeCallbacksAndMessages(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.f1472d = null;
            try {
                if (this.f1471c != null) {
                    this.f1471c.quit();
                    this.f1471c.interrupt();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.f1471c = null;
            this.f1473e = false;
        }
    }
}
