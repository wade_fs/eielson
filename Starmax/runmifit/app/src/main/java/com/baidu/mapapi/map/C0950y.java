package com.baidu.mapapi.map;

import android.view.View;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;

/* renamed from: com.baidu.mapapi.map.y */
class C0950y implements View.OnClickListener {

    /* renamed from: a */
    final /* synthetic */ WearMapView f2879a;

    C0950y(WearMapView wearMapView) {
        this.f2879a = wearMapView;
    }

    public void onClick(View view) {
        C1064ab E = this.f2879a.f2811f.mo13086a().mo12989E();
        E.f3414a -= 1.0f;
        this.f2879a.f2811f.mo13086a().mo13016a(E, 300);
    }
}
