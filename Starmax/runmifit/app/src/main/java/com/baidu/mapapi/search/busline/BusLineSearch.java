package com.baidu.mapapi.search.busline;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.search.core.C0968l;
import com.baidu.platform.core.busline.C1339c;
import com.baidu.platform.core.busline.IBusLineSearch;

public class BusLineSearch extends C0968l {

    /* renamed from: a */
    IBusLineSearch f2912a = new C1339c();

    /* renamed from: b */
    private boolean f2913b = false;

    BusLineSearch() {
    }

    public static BusLineSearch newInstance() {
        BMapManager.init();
        return new BusLineSearch();
    }

    public void destroy() {
        if (!this.f2913b) {
            this.f2913b = true;
            this.f2912a.mo14043a();
            BMapManager.destroy();
        }
    }

    public boolean searchBusLine(BusLineSearchOption busLineSearchOption) {
        if (this.f2912a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (busLineSearchOption != null && busLineSearchOption.mCity != null && busLineSearchOption.mUid != null) {
            return this.f2912a.mo14045a(busLineSearchOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or city or uid can not be null");
        }
    }

    public void setOnGetBusLineSearchResultListener(OnGetBusLineSearchResultListener onGetBusLineSearchResultListener) {
        IBusLineSearch iBusLineSearch = this.f2912a;
        if (iBusLineSearch == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (onGetBusLineSearchResultListener != null) {
            iBusLineSearch.mo14044a(onGetBusLineSearchResultListener);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: listener can not be null");
        }
    }
}
