package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

public class TransitBaseInfo implements Parcelable {
    public static final Parcelable.Creator<TransitBaseInfo> CREATOR = new C0971o();

    /* renamed from: a */
    private String f2950a;

    /* renamed from: b */
    private String f2951b;

    /* renamed from: c */
    private String f2952c;

    /* renamed from: d */
    private String f2953d;

    /* renamed from: e */
    private String f2954e;

    public TransitBaseInfo() {
    }

    protected TransitBaseInfo(Parcel parcel) {
        this.f2950a = parcel.readString();
        this.f2951b = parcel.readString();
        this.f2952c = parcel.readString();
        this.f2953d = parcel.readString();
        this.f2954e = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getArriveStation() {
        return this.f2952c;
    }

    public String getArriveTime() {
        return this.f2954e;
    }

    public String getDepartureStation() {
        return this.f2951b;
    }

    public String getDepartureTime() {
        return this.f2953d;
    }

    public String getName() {
        return this.f2950a;
    }

    public void setArriveStation(String str) {
        this.f2952c = str;
    }

    public void setArriveTime(String str) {
        this.f2954e = str;
    }

    public void setDepartureStation(String str) {
        this.f2951b = str;
    }

    public void setDepartureTime(String str) {
        this.f2953d = str;
    }

    public void setName(String str) {
        this.f2950a = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2950a);
        parcel.writeString(this.f2951b);
        parcel.writeString(this.f2952c);
        parcel.writeString(this.f2953d);
        parcel.writeString(this.f2954e);
    }
}
