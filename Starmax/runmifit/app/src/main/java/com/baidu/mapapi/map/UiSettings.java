package com.baidu.mapapi.map;

import com.baidu.mapsdkplatform.comapi.map.C1078e;

public final class UiSettings {

    /* renamed from: a */
    private C1078e f2793a;

    UiSettings(C1078e eVar) {
        this.f2793a = eVar;
    }

    public boolean isCompassEnabled() {
        return this.f2793a.mo13072r();
    }

    public boolean isOverlookingGesturesEnabled() {
        return this.f2793a.mo13085z();
    }

    public boolean isRotateGesturesEnabled() {
        return this.f2793a.mo13084y();
    }

    public boolean isScrollGesturesEnabled() {
        return this.f2793a.mo13082w();
    }

    public boolean isZoomGesturesEnabled() {
        return this.f2793a.mo13083x();
    }

    public void setAllGesturesEnabled(boolean z) {
        setRotateGesturesEnabled(z);
        setScrollGesturesEnabled(z);
        setOverlookingGesturesEnabled(z);
        setZoomGesturesEnabled(z);
    }

    public void setCompassEnabled(boolean z) {
        this.f2793a.mo13058k(z);
    }

    public void setEnlargeCenterWithDoubleClickEnable(boolean z) {
        this.f2793a.mo13071r(z);
    }

    public void setOverlookingGesturesEnabled(boolean z) {
        this.f2793a.mo13076t(z);
    }

    public void setRotateGesturesEnabled(boolean z) {
        this.f2793a.mo13073s(z);
    }

    public void setScrollGesturesEnabled(boolean z) {
        this.f2793a.mo13068p(z);
    }

    public void setZoomGesturesEnabled(boolean z) {
        this.f2793a.mo13069q(z);
    }
}
