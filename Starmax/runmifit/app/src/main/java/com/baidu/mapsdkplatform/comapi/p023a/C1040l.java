package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.graphics.Point;
import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.model.LatLng;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.l */
public class C1040l extends C1031c {

    /* renamed from: a */
    private Animator f3346a = null;

    /* renamed from: b */
    private long f3347b = 0;

    /* renamed from: c */
    private Interpolator f3348c = null;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Animation.AnimationListener f3349d = null;

    /* renamed from: e */
    private int f3350e = 1;

    /* renamed from: f */
    private int f3351f = 0;

    /* renamed from: g */
    private Object[] f3352g;

    /* renamed from: com.baidu.mapsdkplatform.comapi.a.l$a */
    public class C1041a implements TypeEvaluator {
        public C1041a() {
        }

        public Object evaluate(float f, Object obj, Object obj2) {
            LatLng latLng = (LatLng) obj;
            LatLng latLng2 = (LatLng) obj2;
            double d = latLng.longitude;
            double d2 = (double) f;
            Double.isNaN(d2);
            double d3 = d + ((latLng2.longitude - latLng.longitude) * d2);
            double d4 = latLng.latitude;
            Double.isNaN(d2);
            return new LatLng(d4 + (d2 * (latLng2.latitude - latLng.latitude)), d3);
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.a.l$b */
    public class C1042b implements TypeEvaluator {
        public C1042b() {
        }

        public Object evaluate(float f, Object obj, Object obj2) {
            Point point = (Point) obj;
            Point point2 = (Point) obj2;
            return new Point((int) (((float) point.x) + (((float) (point2.x - point.x)) * f)), (int) (((float) point.y) + (f * ((float) (point2.y - point.y)))));
        }
    }

    public C1040l(Point... pointArr) {
        this.f3352g = pointArr;
    }

    public C1040l(LatLng... latLngArr) {
        this.f3352g = latLngArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ObjectAnimator mo12912a(Marker marker) {
        ObjectAnimator objectAnimator;
        if (marker.isFixed()) {
            if (this.f3352g[0] instanceof Point) {
                objectAnimator = ObjectAnimator.ofObject(marker, "fixedScreenPosition", new C1042b(), this.f3352g);
            } else {
                throw new ClassCastException("BDMapSDKException: if the marker is fixed on screen, the parameters of Transformation must be android.graphics.Point");
            }
        } else if (this.f3352g[0] instanceof LatLng) {
            objectAnimator = ObjectAnimator.ofObject(marker, "position", new C1041a(), this.f3352g);
        } else {
            throw new ClassCastException("BDMapSDKException: if the marker isn't fixed on screen, the parameters of Transformation must be Latlng");
        }
        if (objectAnimator != null) {
            objectAnimator.setRepeatCount(this.f3351f);
            objectAnimator.setRepeatMode(mo12913c());
            objectAnimator.setDuration(this.f3347b);
            Interpolator interpolator = this.f3348c;
            if (interpolator != null) {
                objectAnimator.setInterpolator(interpolator);
            }
        }
        return objectAnimator;
    }

    /* renamed from: a */
    public void mo12874a() {
        Animator animator = this.f3346a;
        if (animator != null) {
            animator.start();
        }
    }

    /* renamed from: a */
    public void mo12875a(int i) {
        this.f3350e = i;
    }

    /* renamed from: a */
    public void mo12876a(long j) {
        if (j < 0) {
            j = 0;
        }
        this.f3347b = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12877a(Animator animator) {
        if (animator != null) {
            animator.addListener(new C1043m(this));
        }
    }

    /* renamed from: a */
    public void mo12878a(Interpolator interpolator) {
        this.f3348c = interpolator;
    }

    /* renamed from: a */
    public void mo12879a(Animation.AnimationListener animationListener) {
        this.f3349d = animationListener;
    }

    /* renamed from: a */
    public void mo12880a(Marker marker, Animation animation) {
        this.f3346a = mo12912a(marker);
        mo12877a(this.f3346a);
    }

    /* renamed from: b */
    public void mo12881b() {
        Animator animator = this.f3346a;
        if (animator != null) {
            animator.cancel();
            this.f3346a = null;
        }
    }

    /* renamed from: b */
    public void mo12882b(int i) {
        if (i > 0 || i == -1) {
            this.f3351f = i;
        }
    }

    /* renamed from: c */
    public int mo12913c() {
        return this.f3350e;
    }

    /* renamed from: c */
    public void mo12884c(int i) {
    }
}
