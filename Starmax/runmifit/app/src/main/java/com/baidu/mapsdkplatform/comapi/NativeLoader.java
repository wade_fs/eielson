package com.baidu.mapsdkplatform.comapi;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public class NativeLoader {

    /* renamed from: a */
    private static final String f3283a = NativeLoader.class.getSimpleName();

    /* renamed from: b */
    private static Context f3284b;

    /* renamed from: c */
    private static final Set<String> f3285c = new HashSet();

    /* renamed from: d */
    private static final Set<String> f3286d = new HashSet();

    /* renamed from: e */
    private static NativeLoader f3287e;

    /* renamed from: f */
    private static C1027a f3288f = C1027a.ARMEABI;

    /* renamed from: g */
    private static boolean f3289g = false;

    /* renamed from: h */
    private static String f3290h = null;

    /* renamed from: com.baidu.mapsdkplatform.comapi.NativeLoader$a */
    private enum C1027a {
        ARMEABI("armeabi"),
        ARMV7("armeabi-v7a"),
        ARM64("arm64-v8a"),
        X86("x86"),
        X86_64("x86_64");
        

        /* renamed from: f */
        private String f3297f;

        private C1027a(String str) {
            this.f3297f = str;
        }

        /* renamed from: a */
        public String mo12865a() {
            return this.f3297f;
        }
    }

    private NativeLoader() {
    }

    /* renamed from: a */
    private String m3325a() {
        return 8 <= Build.VERSION.SDK_INT ? f3284b.getPackageCodePath() : "";
    }

    /* renamed from: a */
    private String m3326a(C1027a aVar) {
        return "lib/" + aVar.mo12865a() + "/";
    }

    /* renamed from: a */
    private void m3327a(InputStream inputStream, FileOutputStream fileOutputStream) throws IOException {
        byte[] bArr = new byte[4096];
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(f3283a, "Close InputStream error", e);
                }
                try {
                    fileOutputStream.close();
                } catch (IOException e2) {
                    Log.e(f3283a, "Close OutputStream error", e2);
                }
            }
        }
        fileOutputStream.flush();
    }

    /* renamed from: a */
    private void m3328a(Throwable th) {
        Log.e(f3283a, "loadException", th);
        for (String str : f3286d) {
            String str2 = f3283a;
            Log.e(str2, str + " Failed to load.");
        }
    }

    /* renamed from: a */
    static void m3329a(boolean z, String str) {
        f3289g = z;
        f3290h = str;
    }

    /* renamed from: a */
    private boolean m3330a(String str) {
        try {
            synchronized (f3285c) {
                if (f3285c.contains(str)) {
                    return true;
                }
                System.loadLibrary(str);
                synchronized (f3285c) {
                    f3285c.add(str);
                }
                return true;
            }
        } catch (Throwable unused) {
            return m3334b(str);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x008e A[SYNTHETIC, Splitter:B:39:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009b A[SYNTHETIC, Splitter:B:45:0x009b] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m3331a(java.lang.String r8, com.baidu.mapsdkplatform.comapi.NativeLoader.C1027a r9) {
        /*
            r7 = this;
            java.lang.String r0 = "Release file failed"
            java.io.File r1 = new java.io.File
            java.lang.String r2 = r7.m3333b()
            r1.<init>(r2, r8)
            boolean r2 = r1.exists()
            r3 = 1
            if (r2 == 0) goto L_0x001d
            long r1 = r1.length()
            r4 = 0
            int r6 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x001d
            return r3
        L_0x001d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r9 = r7.m3326a(r9)
            r1.append(r9)
            r1.append(r8)
            java.lang.String r9 = r1.toString()
            r1 = 0
            boolean r2 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3289g
            if (r2 != 0) goto L_0x003a
            java.lang.String r2 = r7.m3325a()
            goto L_0x003c
        L_0x003a:
            java.lang.String r2 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3290h
        L_0x003c:
            r4 = 0
            if (r2 == 0) goto L_0x00a6
            boolean r5 = r2.isEmpty()
            if (r5 == 0) goto L_0x0046
            goto L_0x00a6
        L_0x0046:
            java.util.zip.ZipFile r5 = new java.util.zip.ZipFile     // Catch:{ Exception -> 0x0084 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0084 }
            java.util.zip.ZipEntry r9 = r5.getEntry(r9)     // Catch:{ Exception -> 0x007e, all -> 0x007c }
            if (r9 != 0) goto L_0x005c
            r5.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x005b
        L_0x0055:
            r8 = move-exception
            java.lang.String r9 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3283a
            android.util.Log.e(r9, r0, r8)
        L_0x005b:
            return r4
        L_0x005c:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x007e, all -> 0x007c }
            java.lang.String r2 = r7.m3333b()     // Catch:{ Exception -> 0x007e, all -> 0x007c }
            r1.<init>(r2, r8)     // Catch:{ Exception -> 0x007e, all -> 0x007c }
            java.io.InputStream r8 = r5.getInputStream(r9)     // Catch:{ Exception -> 0x007e, all -> 0x007c }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x007e, all -> 0x007c }
            r9.<init>(r1)     // Catch:{ Exception -> 0x007e, all -> 0x007c }
            r7.m3327a(r8, r9)     // Catch:{ Exception -> 0x007e, all -> 0x007c }
            r5.close()     // Catch:{ IOException -> 0x0075 }
            goto L_0x007b
        L_0x0075:
            r8 = move-exception
            java.lang.String r9 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3283a
            android.util.Log.e(r9, r0, r8)
        L_0x007b:
            return r3
        L_0x007c:
            r8 = move-exception
            goto L_0x0099
        L_0x007e:
            r8 = move-exception
            r1 = r5
            goto L_0x0085
        L_0x0081:
            r8 = move-exception
            r5 = r1
            goto L_0x0099
        L_0x0084:
            r8 = move-exception
        L_0x0085:
            java.lang.String r9 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3283a     // Catch:{ all -> 0x0081 }
            java.lang.String r2 = "Copy library file error"
            android.util.Log.e(r9, r2, r8)     // Catch:{ all -> 0x0081 }
            if (r1 == 0) goto L_0x0098
            r1.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x0098
        L_0x0092:
            r8 = move-exception
            java.lang.String r9 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3283a
            android.util.Log.e(r9, r0, r8)
        L_0x0098:
            return r4
        L_0x0099:
            if (r5 == 0) goto L_0x00a5
            r5.close()     // Catch:{ IOException -> 0x009f }
            goto L_0x00a5
        L_0x009f:
            r9 = move-exception
            java.lang.String r1 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3283a
            android.util.Log.e(r1, r0, r9)
        L_0x00a5:
            throw r8
        L_0x00a6:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.NativeLoader.m3331a(java.lang.String, com.baidu.mapsdkplatform.comapi.NativeLoader$a):boolean");
    }

    /* renamed from: a */
    private boolean m3332a(String str, String str2) {
        return !m3331a(str2, C1027a.ARMV7) ? m3335b(str, str2) : m3340f(str2, str);
    }

    /* renamed from: b */
    private String m3333b() {
        File file = new File(f3284b.getFilesDir(), "libs");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0020, code lost:
        if (r1 == 2) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        if (r1 == 3) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        if (r1 == 4) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        if (r1 == 5) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002b, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002d, code lost:
        r0 = m3338d(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0032, code lost:
        r0 = m3339e(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0037, code lost:
        r0 = m3335b(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003c, code lost:
        r0 = m3332a(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0041, code lost:
        r0 = m3337c(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0045, code lost:
        r2 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3285c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0047, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        com.baidu.mapsdkplatform.comapi.NativeLoader.f3285c.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004d, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        r1 = com.baidu.mapsdkplatform.comapi.C1054d.f3373a[com.baidu.mapsdkplatform.comapi.NativeLoader.f3288f.ordinal()];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        if (r1 == 1) goto L_0x0041;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m3334b(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.String r0 = java.lang.System.mapLibraryName(r5)
            java.util.Set<java.lang.String> r1 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3285c
            monitor-enter(r1)
            java.util.Set<java.lang.String> r2 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3285c     // Catch:{ all -> 0x0052 }
            boolean r2 = r2.contains(r5)     // Catch:{ all -> 0x0052 }
            r3 = 1
            if (r2 == 0) goto L_0x0012
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
            return r3
        L_0x0012:
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
            int[] r1 = com.baidu.mapsdkplatform.comapi.C1054d.f3373a
            com.baidu.mapsdkplatform.comapi.NativeLoader$a r2 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3288f
            int r2 = r2.ordinal()
            r1 = r1[r2]
            if (r1 == r3) goto L_0x0041
            r2 = 2
            if (r1 == r2) goto L_0x003c
            r2 = 3
            if (r1 == r2) goto L_0x0037
            r2 = 4
            if (r1 == r2) goto L_0x0032
            r2 = 5
            if (r1 == r2) goto L_0x002d
            r0 = 0
            goto L_0x0045
        L_0x002d:
            boolean r0 = r4.m3338d(r5, r0)
            goto L_0x0045
        L_0x0032:
            boolean r0 = r4.m3339e(r5, r0)
            goto L_0x0045
        L_0x0037:
            boolean r0 = r4.m3335b(r5, r0)
            goto L_0x0045
        L_0x003c:
            boolean r0 = r4.m3332a(r5, r0)
            goto L_0x0045
        L_0x0041:
            boolean r0 = r4.m3337c(r5, r0)
        L_0x0045:
            java.util.Set<java.lang.String> r2 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3285c
            monitor-enter(r2)
            java.util.Set<java.lang.String> r1 = com.baidu.mapsdkplatform.comapi.NativeLoader.f3285c     // Catch:{ all -> 0x004f }
            r1.add(r5)     // Catch:{ all -> 0x004f }
            monitor-exit(r2)     // Catch:{ all -> 0x004f }
            return r0
        L_0x004f:
            r5 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x004f }
            throw r5
        L_0x0052:
            r5 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0052 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.NativeLoader.m3334b(java.lang.String):boolean");
    }

    /* renamed from: b */
    private boolean m3335b(String str, String str2) {
        if (m3331a(str2, C1027a.ARMEABI)) {
            return m3340f(str2, str);
        }
        String str3 = f3283a;
        Log.e(str3, "found lib" + str + ".so error");
        return false;
    }

    /* renamed from: c */
    private static C1027a m3336c() {
        String str = Build.VERSION.SDK_INT < 21 ? Build.CPU_ABI : Build.SUPPORTED_ABIS[0];
        if (str == null) {
            return C1027a.ARMEABI;
        }
        if (str.contains("arm") && str.contains("v7")) {
            f3288f = C1027a.ARMV7;
        }
        if (str.contains("arm") && str.contains("64")) {
            f3288f = C1027a.ARM64;
        }
        if (str.contains("x86")) {
            f3288f = str.contains("64") ? C1027a.X86_64 : C1027a.X86;
        }
        return f3288f;
    }

    /* renamed from: c */
    private boolean m3337c(String str, String str2) {
        return !m3331a(str2, C1027a.ARM64) ? m3332a(str, str2) : m3340f(str2, str);
    }

    /* renamed from: d */
    private boolean m3338d(String str, String str2) {
        return !m3331a(str2, C1027a.X86) ? m3332a(str, str2) : m3340f(str2, str);
    }

    /* renamed from: e */
    private boolean m3339e(String str, String str2) {
        return !m3331a(str2, C1027a.X86_64) ? m3338d(str, str2) : m3340f(str2, str);
    }

    /* renamed from: f */
    private boolean m3340f(String str, String str2) {
        try {
            System.load(new File(m3333b(), str).getAbsolutePath());
            synchronized (f3285c) {
                f3285c.add(str2);
            }
            return true;
        } catch (Throwable th) {
            synchronized (f3286d) {
                f3286d.add(str2);
                m3328a(th);
                return false;
            }
        }
    }

    public static synchronized NativeLoader getInstance() {
        NativeLoader nativeLoader;
        synchronized (NativeLoader.class) {
            if (f3287e == null) {
                f3287e = new NativeLoader();
                f3288f = m3336c();
            }
            nativeLoader = f3287e;
        }
        return nativeLoader;
    }

    public static void setContext(Context context) {
        f3284b = context;
    }

    public synchronized boolean loadLibrary(String str) {
        if (!f3289g) {
            return m3330a(str);
        } else if (f3290h == null || f3290h.isEmpty()) {
            Log.e(f3283a, "Given custom so file path is null, please check!");
            return false;
        } else {
            return m3334b(str);
        }
    }
}
