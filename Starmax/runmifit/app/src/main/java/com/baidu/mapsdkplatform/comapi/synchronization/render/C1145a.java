package com.baidu.mapsdkplatform.comapi.synchronization.render;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.a */
final class C1145a implements Parcelable.Creator<LinkPointPolyLineInfo> {
    C1145a() {
    }

    /* renamed from: a */
    public LinkPointPolyLineInfo createFromParcel(Parcel parcel) {
        return new LinkPointPolyLineInfo(parcel);
    }

    /* renamed from: a */
    public LinkPointPolyLineInfo[] newArray(int i) {
        return new LinkPointPolyLineInfo[i];
    }
}
