package com.baidu.mapapi.synchronization.histroytrace;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceData;

/* renamed from: com.baidu.mapapi.synchronization.histroytrace.b */
final class C1010b implements Parcelable.Creator<HistoryTraceData.HistoryTracePoint> {
    C1010b() {
    }

    /* renamed from: a */
    public HistoryTraceData.HistoryTracePoint createFromParcel(Parcel parcel) {
        return new HistoryTraceData.HistoryTracePoint(parcel);
    }

    /* renamed from: a */
    public HistoryTraceData.HistoryTracePoint[] newArray(int i) {
        return new HistoryTraceData.HistoryTracePoint[i];
    }
}
