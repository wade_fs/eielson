package com.baidu.location.p017e;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import com.baidu.location.C0839f;
import com.baidu.location.p013a.C0723a;
import com.baidu.mobstat.Config;
import java.util.List;

/* renamed from: com.baidu.location.e.i */
public class C0835i {

    /* renamed from: a */
    public static long f1794a;

    /* renamed from: b */
    private static C0835i f1795b;

    /* renamed from: c */
    private WifiManager f1796c = null;

    /* renamed from: d */
    private C0837a f1797d = null;

    /* renamed from: e */
    private C0834h f1798e = null;

    /* renamed from: f */
    private long f1799f = 0;

    /* renamed from: g */
    private long f1800g = 0;

    /* renamed from: h */
    private boolean f1801h = false;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public Handler f1802i = new Handler();
    /* access modifiers changed from: private */

    /* renamed from: j */
    public boolean f1803j = false;

    /* renamed from: k */
    private long f1804k = 0;

    /* renamed from: l */
    private long f1805l = 0;

    /* renamed from: com.baidu.location.e.i$a */
    private class C0837a extends BroadcastReceiver {

        /* renamed from: b */
        private long f1807b;

        /* renamed from: c */
        private boolean f1808c;

        private C0837a() {
            this.f1807b = 0;
            this.f1808c = false;
        }

        public void onReceive(Context context, Intent intent) {
            if (context != null) {
                String action = intent.getAction();
                if (action.equals("android.net.wifi.SCAN_RESULTS")) {
                    C0835i.f1794a = System.currentTimeMillis() / 1000;
                    C0835i.this.f1802i.post(new C0838j(this, intent.getBooleanExtra("resultsUpdated", true)));
                } else if (action.equals("android.net.wifi.STATE_CHANGE") && ((NetworkInfo) intent.getParcelableExtra("networkInfo")).getState().equals(NetworkInfo.State.CONNECTED) && System.currentTimeMillis() - this.f1807b >= 5000) {
                    this.f1807b = System.currentTimeMillis();
                    if (!this.f1808c) {
                        this.f1808c = true;
                    }
                }
            }
        }
    }

    private C0835i() {
    }

    /* renamed from: a */
    public static synchronized C0835i m2376a() {
        C0835i iVar;
        synchronized (C0835i.class) {
            if (f1795b == null) {
                f1795b = new C0835i();
            }
            iVar = f1795b;
        }
        return iVar;
    }

    /* renamed from: a */
    private String m2377a(long j) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(String.valueOf((int) (j & 255)));
        stringBuffer.append('.');
        stringBuffer.append(String.valueOf((int) ((j >> 8) & 255)));
        stringBuffer.append('.');
        stringBuffer.append(String.valueOf((int) ((j >> 16) & 255)));
        stringBuffer.append('.');
        stringBuffer.append(String.valueOf((int) ((j >> 24) & 255)));
        return stringBuffer.toString();
    }

    /* renamed from: a */
    public static boolean m2378a(C0834h hVar, C0834h hVar2) {
        boolean a = m2379a(hVar, hVar2, 0.7f);
        long currentTimeMillis = System.currentTimeMillis() - C0723a.f1203c;
        if (currentTimeMillis <= 0 || currentTimeMillis >= 30000 || !a || hVar2.mo10680h() - hVar.mo10680h() <= 30) {
            return a;
        }
        return false;
    }

    /* renamed from: a */
    public static boolean m2379a(C0834h hVar, C0834h hVar2, float f) {
        if (!(hVar == null || hVar2 == null)) {
            List<ScanResult> list = hVar.f1789a;
            List<ScanResult> list2 = hVar2.f1789a;
            if (list == list2) {
                return true;
            }
            if (!(list == null || list2 == null)) {
                int size = list.size();
                int size2 = list2.size();
                float f2 = (float) (size + size2);
                if (size == 0 && size2 == 0) {
                    return true;
                }
                if (!(size == 0 || size2 == 0)) {
                    int i = 0;
                    for (int i2 = 0; i2 < size; i2++) {
                        String str = list.get(i2) != null ? list.get(i2).BSSID : null;
                        if (str != null) {
                            int i3 = 0;
                            while (true) {
                                if (i3 >= size2) {
                                    break;
                                }
                                String str2 = list2.get(i3) != null ? list2.get(i3).BSSID : null;
                                if (str2 != null && str.equals(str2)) {
                                    i++;
                                    break;
                                }
                                i3++;
                            }
                        }
                    }
                    if (((float) (i * 2)) >= f2 * f) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* renamed from: j */
    public static boolean m2384j() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) C0839f.getServiceContext().getSystemService("connectivity")).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: t */
    public void m2385t() {
        WifiManager wifiManager = this.f1796c;
        if (wifiManager != null) {
            try {
                List<ScanResult> scanResults = wifiManager.getScanResults();
                if (scanResults != null) {
                    C0834h hVar = new C0834h(scanResults, System.currentTimeMillis());
                    C0834h hVar2 = this.f1798e;
                    if (hVar2 == null || !hVar.mo10668a(hVar2)) {
                        this.f1798e = hVar;
                    }
                }
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: b */
    public void mo10686b() {
        this.f1804k = 0;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:12|13|14|15|16|17|18|19) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0038 */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo10687c() {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r0 = r5.f1801h     // Catch:{ all -> 0x003c }
            r1 = 1
            if (r0 != r1) goto L_0x0008
            monitor-exit(r5)
            return
        L_0x0008:
            boolean r0 = com.baidu.location.C0839f.isServing     // Catch:{ all -> 0x003c }
            if (r0 != 0) goto L_0x000e
            monitor-exit(r5)
            return
        L_0x000e:
            android.content.Context r0 = com.baidu.location.C0839f.getServiceContext()     // Catch:{ all -> 0x003c }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ all -> 0x003c }
            java.lang.String r2 = "wifi"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ all -> 0x003c }
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0     // Catch:{ all -> 0x003c }
            r5.f1796c = r0     // Catch:{ all -> 0x003c }
            com.baidu.location.e.i$a r0 = new com.baidu.location.e.i$a     // Catch:{ all -> 0x003c }
            r2 = 0
            r0.<init>()     // Catch:{ all -> 0x003c }
            r5.f1797d = r0     // Catch:{ all -> 0x003c }
            android.content.Context r0 = com.baidu.location.C0839f.getServiceContext()     // Catch:{ Exception -> 0x0038 }
            com.baidu.location.e.i$a r2 = r5.f1797d     // Catch:{ Exception -> 0x0038 }
            android.content.IntentFilter r3 = new android.content.IntentFilter     // Catch:{ Exception -> 0x0038 }
            java.lang.String r4 = "android.net.wifi.SCAN_RESULTS"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0038 }
            r0.registerReceiver(r2, r3)     // Catch:{ Exception -> 0x0038 }
        L_0x0038:
            r5.f1801h = r1     // Catch:{ all -> 0x003c }
            monitor-exit(r5)
            return
        L_0x003c:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0835i.mo10687c():void");
    }

    /* renamed from: d */
    public List<WifiConfiguration> mo10688d() {
        try {
            if (this.f1796c != null) {
                return this.f1796c.getConfiguredNetworks();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: e */
    public synchronized void mo10689e() {
        if (this.f1801h) {
            try {
                C0839f.getServiceContext().unregisterReceiver(this.f1797d);
                f1794a = 0;
            } catch (Exception unused) {
            }
            this.f1797d = null;
            this.f1796c = null;
            this.f1801h = false;
        }
    }

    /* renamed from: f */
    public boolean mo10690f() {
        long currentTimeMillis = System.currentTimeMillis();
        long j = this.f1800g;
        if (currentTimeMillis - j > 0 && currentTimeMillis - j <= 5000) {
            return false;
        }
        this.f1800g = currentTimeMillis;
        mo10686b();
        return mo10691g();
    }

    /* renamed from: g */
    public boolean mo10691g() {
        if (this.f1796c == null) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        long j = this.f1799f;
        if (currentTimeMillis - j > 0) {
            long j2 = this.f1804k;
            if (currentTimeMillis - j <= j2 + 5000 || currentTimeMillis - (f1794a * 1000) <= j2 + 5000) {
                return false;
            }
            if (m2384j() && currentTimeMillis - this.f1799f <= this.f1804k + 10000) {
                return false;
            }
        }
        return mo10693i();
    }

    /* renamed from: h */
    public String mo10692h() {
        WifiManager wifiManager = this.f1796c;
        if (wifiManager == null) {
            return "";
        }
        try {
            return (wifiManager.isWifiEnabled() || (Build.VERSION.SDK_INT > 17 && this.f1796c.isScanAlwaysAvailable())) ? "&wifio=1" : "";
        } catch (Exception | NoSuchMethodError unused) {
            return "";
        }
    }

    /* renamed from: i */
    public boolean mo10693i() {
        long currentTimeMillis = System.currentTimeMillis() - this.f1805l;
        if (currentTimeMillis >= 0 && currentTimeMillis <= 2000) {
            return false;
        }
        this.f1805l = System.currentTimeMillis();
        try {
            if (!this.f1796c.isWifiEnabled()) {
                if (Build.VERSION.SDK_INT <= 17 || !this.f1796c.isScanAlwaysAvailable()) {
                    return false;
                }
            }
            this.f1796c.startScan();
            this.f1799f = System.currentTimeMillis();
            return true;
        } catch (Exception | NoSuchMethodError unused) {
            return false;
        }
    }

    /* renamed from: k */
    public boolean mo10694k() {
        try {
            return (this.f1796c.isWifiEnabled() || (Build.VERSION.SDK_INT > 17 && this.f1796c.isScanAlwaysAvailable())) && !m2384j() && new C0834h(this.f1796c.getScanResults(), 0).mo10677e();
        } catch (Exception | NoSuchMethodError unused) {
            return false;
        }
    }

    /* renamed from: l */
    public WifiInfo mo10695l() {
        WifiManager wifiManager = this.f1796c;
        if (wifiManager == null) {
            return null;
        }
        try {
            WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (!(connectionInfo == null || connectionInfo.getBSSID() == null)) {
                if (connectionInfo.getRssi() > -100) {
                    String bssid = connectionInfo.getBSSID();
                    if (bssid != null) {
                        String replace = bssid.replace(Config.TRACE_TODAY_VISIT_SPLIT, "");
                        if ("000000000000".equals(replace) || "".equals(replace)) {
                            return null;
                        }
                    }
                    return connectionInfo;
                }
            }
        } catch (Error | Exception unused) {
        }
        return null;
    }

    /* renamed from: m */
    public String mo10696m() {
        StringBuffer stringBuffer = new StringBuffer();
        WifiInfo l = m2376a().mo10695l();
        if (!(l == null || l.getBSSID() == null)) {
            String replace = l.getBSSID().replace(Config.TRACE_TODAY_VISIT_SPLIT, "");
            int rssi = l.getRssi();
            String n = m2376a().mo10697n();
            if (rssi < 0) {
                rssi = -rssi;
            }
            if (replace != null && rssi < 100) {
                stringBuffer.append("&wf=");
                stringBuffer.append(replace);
                stringBuffer.append(";");
                stringBuffer.append("" + rssi + ";");
                String ssid = l.getSSID();
                if (ssid != null && (ssid.contains("&") || ssid.contains(";"))) {
                    ssid = ssid.replace("&", "_");
                }
                stringBuffer.append(ssid);
                stringBuffer.append("&wf_n=1");
                if (n != null) {
                    stringBuffer.append("&wf_gw=");
                    stringBuffer.append(n);
                }
                return stringBuffer.toString();
            }
        }
        return null;
    }

    /* renamed from: n */
    public String mo10697n() {
        DhcpInfo dhcpInfo;
        WifiManager wifiManager = this.f1796c;
        if (wifiManager == null || (dhcpInfo = wifiManager.getDhcpInfo()) == null) {
            return null;
        }
        return m2377a((long) dhcpInfo.gateway);
    }

    /* renamed from: o */
    public C0834h mo10698o() {
        C0834h hVar = this.f1798e;
        return (hVar == null || !hVar.mo10683k()) ? mo10700q() : this.f1798e;
    }

    /* renamed from: p */
    public C0834h mo10699p() {
        C0834h hVar = this.f1798e;
        return (hVar == null || !hVar.mo10684l()) ? mo10700q() : this.f1798e;
    }

    /* renamed from: q */
    public C0834h mo10700q() {
        WifiManager wifiManager = this.f1796c;
        if (wifiManager != null) {
            try {
                return new C0834h(wifiManager.getScanResults(), this.f1799f);
            } catch (Exception unused) {
            }
        }
        return new C0834h(null, 0);
    }

    /* renamed from: r */
    public boolean mo10701r() {
        try {
            return this.f1796c.isWifiEnabled() || (Build.VERSION.SDK_INT > 17 && this.f1796c.isScanAlwaysAvailable());
        } catch (Exception | NoSuchMethodError unused) {
            return false;
        }
    }

    /* renamed from: s */
    public String mo10702s() {
        try {
            WifiInfo connectionInfo = this.f1796c.getConnectionInfo();
            if (connectionInfo != null) {
                return connectionInfo.getMacAddress();
            }
            return null;
        } catch (Error | Exception unused) {
            return null;
        }
    }
}
