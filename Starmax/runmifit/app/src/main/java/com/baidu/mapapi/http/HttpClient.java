package com.baidu.mapapi.http;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.baidu.mapapi.JNIInitializer;
import com.baidu.mapapi.common.Logger;
import com.baidu.mapsdkplatform.comapi.util.C1175i;
import com.baidu.mapsdkplatform.comapi.util.PermissionCheck;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpClient {
    public static boolean isHttpsEnable = true;

    /* renamed from: a */
    HttpURLConnection f2353a;

    /* renamed from: b */
    private String f2354b = null;

    /* renamed from: c */
    private String f2355c = null;

    /* renamed from: d */
    private int f2356d;

    /* renamed from: e */
    private int f2357e;

    /* renamed from: f */
    private String f2358f;

    /* renamed from: g */
    private ProtoResultCallback f2359g;

    public enum HttpStateError {
        NO_ERROR,
        NETWORK_ERROR,
        INNER_ERROR,
        REQUEST_ERROR,
        SERVER_ERROR
    }

    public static abstract class ProtoResultCallback {
        public abstract void onFailed(HttpStateError httpStateError);

        public abstract void onSuccess(String str);
    }

    public HttpClient(String str, ProtoResultCallback protoResultCallback) {
        this.f2358f = str;
        this.f2359g = protoResultCallback;
    }

    /* renamed from: a */
    private HttpURLConnection m2814a() {
        HttpURLConnection httpURLConnection;
        try {
            URL url = new URL(this.f2354b);
            if (isHttpsEnable) {
                httpURLConnection = (HttpsURLConnection) url.openConnection();
                ((HttpsURLConnection) httpURLConnection).setHostnameVerifier(new C0918b(this));
            } else {
                httpURLConnection = (HttpURLConnection) url.openConnection();
            }
            httpURLConnection.setRequestMethod(this.f2358f);
            httpURLConnection.setDoOutput(false);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(this.f2356d);
            httpURLConnection.setReadTimeout(this.f2357e);
            return httpURLConnection;
        } catch (Exception e) {
            Log.e("HttpClient", "url connect failed");
            if (Logger.debugEnable()) {
                e.printStackTrace();
                return null;
            }
            Logger.logW("HttpClient", e.getMessage());
            return null;
        }
    }

    /* renamed from: a */
    private void m2815a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
                if (!jSONObject.has("status_sp")) {
                    return;
                }
            }
            int i = jSONObject.has(NotificationCompat.CATEGORY_STATUS) ? jSONObject.getInt(NotificationCompat.CATEGORY_STATUS) : jSONObject.getInt("status_sp");
            if (i == 105 || i == 106) {
                int permissionCheck = PermissionCheck.permissionCheck();
                if (permissionCheck != 0) {
                    Log.e("HttpClient", "permissionCheck result is: " + permissionCheck);
                }
            }
        } catch (JSONException e) {
            Log.e("HttpClient", "Parse json happened exception");
            e.printStackTrace();
        }
    }

    public static String getAuthToken() {
        return C1175i.f3914d;
    }

    public static String getPhoneInfo() {
        return C1175i.m4270c();
    }

    /* access modifiers changed from: protected */
    public boolean checkNetwork() {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) JNIInitializer.getCachedContext().getSystemService("connectivity");
            return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isAvailable()) ? false : true;
        } catch (Exception e) {
            if (Logger.debugEnable()) {
                e.printStackTrace();
            } else {
                Logger.logW("HttpClient", e.getMessage());
            }
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:101:? A[Catch:{ Exception -> 0x0158 }, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0118 A[Catch:{ all -> 0x0143 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x011c A[Catch:{ all -> 0x0143 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x013d A[Catch:{ Exception -> 0x0158 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0152 A[Catch:{ Exception -> 0x0158 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void request(java.lang.String r7) {
        /*
            r6 = this;
            r6.f2354b = r7
            boolean r7 = r6.checkNetwork()
            if (r7 != 0) goto L_0x0010
            com.baidu.mapapi.http.HttpClient$ProtoResultCallback r7 = r6.f2359g
            com.baidu.mapapi.http.HttpClient$HttpStateError r0 = com.baidu.mapapi.http.HttpClient.HttpStateError.NETWORK_ERROR
            r7.onFailed(r0)
            return
        L_0x0010:
            java.net.HttpURLConnection r7 = r6.m2814a()
            r6.f2353a = r7
            java.net.HttpURLConnection r7 = r6.f2353a
            java.lang.String r0 = "HttpClient"
            if (r7 != 0) goto L_0x0029
            java.lang.String r7 = "url connection failed"
            android.util.Log.e(r0, r7)
            com.baidu.mapapi.http.HttpClient$ProtoResultCallback r7 = r6.f2359g
            com.baidu.mapapi.http.HttpClient$HttpStateError r0 = com.baidu.mapapi.http.HttpClient.HttpStateError.INNER_ERROR
            r7.onFailed(r0)
            return
        L_0x0029:
            java.lang.String r7 = r6.f2354b
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x0039
            com.baidu.mapapi.http.HttpClient$ProtoResultCallback r7 = r6.f2359g
            com.baidu.mapapi.http.HttpClient$HttpStateError r0 = com.baidu.mapapi.http.HttpClient.HttpStateError.REQUEST_ERROR
            r7.onFailed(r0)
            return
        L_0x0039:
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            r7.connect()     // Catch:{ Exception -> 0x0158 }
            r7 = 0
            java.net.HttpURLConnection r1 = r6.f2353a     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            int r1 = r1.getResponseCode()     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            r2 = 200(0xc8, float:2.8E-43)
            if (r2 != r1) goto L_0x00a9
            java.net.HttpURLConnection r1 = r6.f2353a     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00a3, all -> 0x009c }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00a3, all -> 0x009c }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r1, r4)     // Catch:{ Exception -> 0x00a3, all -> 0x009c }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00a3, all -> 0x009c }
            java.lang.StringBuffer r7 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
            r7.<init>()     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
        L_0x0060:
            int r3 = r2.read()     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
            r4 = -1
            if (r3 == r4) goto L_0x006c
            char r3 = (char) r3     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
            r7.append(r3)     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
            goto L_0x0060
        L_0x006c:
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
            r6.f2355c = r7     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
            java.lang.String r7 = r6.f2355c     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
            r6.m2815a(r7)     // Catch:{ Exception -> 0x0096, all -> 0x0090 }
            if (r1 == 0) goto L_0x007f
            r2.close()     // Catch:{ Exception -> 0x0158 }
            r1.close()     // Catch:{ Exception -> 0x0158 }
        L_0x007f:
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            if (r7 == 0) goto L_0x0088
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            r7.disconnect()     // Catch:{ Exception -> 0x0158 }
        L_0x0088:
            com.baidu.mapapi.http.HttpClient$ProtoResultCallback r7 = r6.f2359g
            java.lang.String r0 = r6.f2355c
            r7.onSuccess(r0)
            return
        L_0x0090:
            r7 = move-exception
            r5 = r1
            r1 = r7
            r7 = r5
            goto L_0x0144
        L_0x0096:
            r7 = move-exception
            r5 = r1
            r1 = r7
            r7 = r5
            goto L_0x0112
        L_0x009c:
            r2 = move-exception
            r5 = r2
            r2 = r7
            r7 = r1
            r1 = r5
            goto L_0x0144
        L_0x00a3:
            r2 = move-exception
            r5 = r2
            r2 = r7
            r7 = r1
            r1 = r5
            goto L_0x0112
        L_0x00a9:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            r2.<init>()     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.lang.String r3 = "responseCode is: "
            r2.append(r3)     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            r2.append(r1)     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            android.util.Log.e(r0, r2)     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            r2 = 500(0x1f4, float:7.0E-43)
            if (r1 < r2) goto L_0x00c4
            com.baidu.mapapi.http.HttpClient$HttpStateError r2 = com.baidu.mapapi.http.HttpClient.HttpStateError.SERVER_ERROR     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            goto L_0x00cd
        L_0x00c4:
            r2 = 400(0x190, float:5.6E-43)
            if (r1 < r2) goto L_0x00cb
            com.baidu.mapapi.http.HttpClient$HttpStateError r2 = com.baidu.mapapi.http.HttpClient.HttpStateError.REQUEST_ERROR     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            goto L_0x00cd
        L_0x00cb:
            com.baidu.mapapi.http.HttpClient$HttpStateError r2 = com.baidu.mapapi.http.HttpClient.HttpStateError.INNER_ERROR     // Catch:{ Exception -> 0x0110, all -> 0x010d }
        L_0x00cd:
            boolean r3 = com.baidu.mapapi.common.Logger.debugEnable()     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            if (r3 == 0) goto L_0x00e1
            java.net.HttpURLConnection r1 = r6.f2353a     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.io.InputStream r1 = r1.getErrorStream()     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.lang.String r3 = r1.toString()     // Catch:{ Exception -> 0x00a3, all -> 0x009c }
            com.baidu.mapapi.common.Logger.logW(r0, r3)     // Catch:{ Exception -> 0x00a3, all -> 0x009c }
            goto L_0x00fe
        L_0x00e1:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            r3.<init>()     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.lang.String r4 = "Get response from server failed, http response code="
            r3.append(r4)     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            r3.append(r1)     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.lang.String r1 = ", error="
            r3.append(r1)     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            r3.append(r2)     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            java.lang.String r1 = r3.toString()     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            com.baidu.mapapi.common.Logger.logW(r0, r1)     // Catch:{ Exception -> 0x0110, all -> 0x010d }
            r1 = r7
        L_0x00fe:
            com.baidu.mapapi.http.HttpClient$ProtoResultCallback r3 = r6.f2359g     // Catch:{ Exception -> 0x00a3, all -> 0x009c }
            r3.onFailed(r2)     // Catch:{ Exception -> 0x00a3, all -> 0x009c }
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            if (r7 == 0) goto L_0x010c
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            r7.disconnect()     // Catch:{ Exception -> 0x0158 }
        L_0x010c:
            return
        L_0x010d:
            r1 = move-exception
            r2 = r7
            goto L_0x0144
        L_0x0110:
            r1 = move-exception
            r2 = r7
        L_0x0112:
            boolean r3 = com.baidu.mapapi.common.Logger.debugEnable()     // Catch:{ all -> 0x0143 }
            if (r3 == 0) goto L_0x011c
            r1.printStackTrace()     // Catch:{ all -> 0x0143 }
            goto L_0x0123
        L_0x011c:
            java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x0143 }
            com.baidu.mapapi.common.Logger.logW(r0, r3)     // Catch:{ all -> 0x0143 }
        L_0x0123:
            java.lang.String r3 = "Catch exception. INNER_ERROR"
            android.util.Log.e(r0, r3, r1)     // Catch:{ all -> 0x0143 }
            com.baidu.mapapi.http.HttpClient$ProtoResultCallback r1 = r6.f2359g     // Catch:{ all -> 0x0143 }
            com.baidu.mapapi.http.HttpClient$HttpStateError r3 = com.baidu.mapapi.http.HttpClient.HttpStateError.INNER_ERROR     // Catch:{ all -> 0x0143 }
            r1.onFailed(r3)     // Catch:{ all -> 0x0143 }
            if (r7 == 0) goto L_0x0139
            if (r2 == 0) goto L_0x0139
            r2.close()     // Catch:{ Exception -> 0x0158 }
            r7.close()     // Catch:{ Exception -> 0x0158 }
        L_0x0139:
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            if (r7 == 0) goto L_0x0142
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            r7.disconnect()     // Catch:{ Exception -> 0x0158 }
        L_0x0142:
            return
        L_0x0143:
            r1 = move-exception
        L_0x0144:
            if (r7 == 0) goto L_0x014e
            if (r2 == 0) goto L_0x014e
            r2.close()     // Catch:{ Exception -> 0x0158 }
            r7.close()     // Catch:{ Exception -> 0x0158 }
        L_0x014e:
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            if (r7 == 0) goto L_0x0157
            java.net.HttpURLConnection r7 = r6.f2353a     // Catch:{ Exception -> 0x0158 }
            r7.disconnect()     // Catch:{ Exception -> 0x0158 }
        L_0x0157:
            throw r1     // Catch:{ Exception -> 0x0158 }
        L_0x0158:
            r7 = move-exception
            boolean r1 = com.baidu.mapapi.common.Logger.debugEnable()
            if (r1 == 0) goto L_0x0163
            r7.printStackTrace()
            goto L_0x016a
        L_0x0163:
            java.lang.String r1 = r7.getMessage()
            com.baidu.mapapi.common.Logger.logW(r0, r1)
        L_0x016a:
            java.lang.String r1 = "Catch connection exception, INNER_ERROR"
            android.util.Log.e(r0, r1, r7)
            com.baidu.mapapi.http.HttpClient$ProtoResultCallback r7 = r6.f2359g
            com.baidu.mapapi.http.HttpClient$HttpStateError r0 = com.baidu.mapapi.http.HttpClient.HttpStateError.INNER_ERROR
            r7.onFailed(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.http.HttpClient.request(java.lang.String):void");
    }

    public void setMaxTimeOut(int i) {
        this.f2356d = i;
    }

    public void setReadTimeOut(int i) {
        this.f2357e = i;
    }
}
