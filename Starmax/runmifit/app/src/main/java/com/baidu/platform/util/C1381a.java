package com.baidu.platform.util;

import com.baidu.mapsdkplatform.comjni.util.AppMD5;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: com.baidu.platform.util.a */
public class C1381a implements ParamBuilder<C1381a> {

    /* renamed from: a */
    protected Map<String, String> f4472a;

    /* renamed from: a */
    public C1381a mo14094a(String str, String str2) {
        if (this.f4472a == null) {
            this.f4472a = new LinkedHashMap();
        }
        this.f4472a.put(str, str2);
        return this;
    }

    /* renamed from: a */
    public String mo14095a() {
        StringBuilder sb;
        Map<String, String> map = this.f4472a;
        if (map == null || map.isEmpty()) {
            return null;
        }
        String str = new String();
        int i = 0;
        for (String str2 : this.f4472a.keySet()) {
            String encodeUrlParamsValue = AppMD5.encodeUrlParamsValue(this.f4472a.get(str2));
            if (i != 0) {
                sb = new StringBuilder();
                sb.append(str);
                str = "&";
            }
            sb.append(str);
            sb.append(str2);
            sb.append("=");
            sb.append(encodeUrlParamsValue);
            str = sb.toString();
            i++;
        }
        return str;
    }
}
