package com.baidu.mapsdkplatform.comapi.util;

import android.util.Log;
import com.baidu.mapapi.http.HttpClient;

/* renamed from: com.baidu.mapsdkplatform.comapi.util.c */
class C1168c extends HttpClient.ProtoResultCallback {

    /* renamed from: a */
    final /* synthetic */ CustomMapStyleLoader f3891a;

    C1168c(CustomMapStyleLoader customMapStyleLoader) {
        this.f3891a = customMapStyleLoader;
    }

    public void onFailed(HttpClient.HttpStateError httpStateError) {
        String a = CustomMapStyleLoader.f3857a;
        Log.e(a, "sendRequest onFailed error = " + httpStateError);
    }

    public void onSuccess(String str) {
        this.f3891a.m4219b(str);
    }
}
