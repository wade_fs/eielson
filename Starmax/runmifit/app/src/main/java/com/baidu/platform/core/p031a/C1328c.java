package com.baidu.platform.core.p031a;

import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.google.android.gms.dynamite.ProviderConstants;
import com.tencent.connect.common.Constants;

/* renamed from: com.baidu.platform.core.a.c */
public class C1328c extends C1320e {
    public C1328c(String str) {
        m4938a(str);
    }

    /* renamed from: a */
    private void m4938a(String str) {
        this.f4435a.mo14094a("qt", "ext");
        this.f4435a.mo14094a("num", Constants.DEFAULT_UIN);
        this.f4435a.mo14094a("l", Constants.VIA_REPORT_TYPE_SHARE_TO_QQ);
        this.f4435a.mo14094a("ie", "utf-8");
        this.f4435a.mo14094a("oue", "1");
        this.f4435a.mo14094a("res", ProviderConstants.API_PATH);
        this.f4435a.mo14094a("fromproduct", "android_map_sdk");
        this.f4435a.mo14094a(Config.CUSTOM_USER_ID, str);
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14090o();
    }
}
