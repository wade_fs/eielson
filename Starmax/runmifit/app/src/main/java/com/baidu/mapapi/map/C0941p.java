package com.baidu.mapapi.map;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* renamed from: com.baidu.mapapi.map.p */
class C0941p extends AnimatorListenerAdapter {

    /* renamed from: a */
    final /* synthetic */ SwipeDismissTouchListener f2863a;

    C0941p(SwipeDismissTouchListener swipeDismissTouchListener) {
        this.f2863a = swipeDismissTouchListener;
    }

    public void onAnimationEnd(Animator animator) {
        this.f2863a.m2955a();
    }
}
