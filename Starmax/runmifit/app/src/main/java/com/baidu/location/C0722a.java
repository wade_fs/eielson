package com.baidu.location;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.location.a */
final class C0722a implements Parcelable.Creator<BDLocation> {
    C0722a() {
    }

    public BDLocation createFromParcel(Parcel parcel) {
        return new BDLocation(parcel, null);
    }

    public BDLocation[] newArray(int i) {
        return new BDLocation[i];
    }
}
