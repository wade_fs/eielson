package com.baidu.platform.core.p035e;

import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.share.OnGetShareUrlResultListener;
import com.baidu.mapapi.search.share.ShareUrlResult;
import com.baidu.platform.base.C1319d;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.core.e.f */
public class C1370f extends C1319d {
    /* renamed from: a */
    public SearchResult mo14018a(String str) {
        SearchResult.ERRORNO errorno;
        ShareUrlResult shareUrlResult = new ShareUrlResult();
        if (str != null && !str.equals("")) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has("SDK_InnerError")) {
                    JSONObject optJSONObject = jSONObject.optJSONObject("SDK_InnerError");
                    if (optJSONObject.has("PermissionCheckError")) {
                        shareUrlResult.error = SearchResult.ERRORNO.PERMISSION_UNFINISHED;
                        return shareUrlResult;
                    } else if (optJSONObject.has("httpStateError")) {
                        String optString = optJSONObject.optString("httpStateError");
                        shareUrlResult.error = optString.equals("NETWORK_ERROR") ? SearchResult.ERRORNO.NETWORK_ERROR : optString.equals("REQUEST_ERROR") ? SearchResult.ERRORNO.REQUEST_ERROR : SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR;
                        return shareUrlResult;
                    }
                }
                if (str != null) {
                    try {
                        JSONObject jSONObject2 = new JSONObject(str);
                        if (!jSONObject2.optString("state").equals("success")) {
                            errorno = SearchResult.ERRORNO.RESULT_NOT_FOUND;
                        } else {
                            shareUrlResult.setUrl(jSONObject2.optString("url"));
                            shareUrlResult.setType(mo14019a().ordinal());
                            errorno = SearchResult.ERRORNO.NO_ERROR;
                        }
                        shareUrlResult.error = errorno;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return shareUrlResult;
                }
                shareUrlResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
                return shareUrlResult;
            } catch (Exception unused) {
            }
        }
        shareUrlResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
        return shareUrlResult;
    }

    /* renamed from: a */
    public void mo14020a(SearchResult searchResult, Object obj) {
        if (obj != null && (obj instanceof OnGetShareUrlResultListener)) {
            OnGetShareUrlResultListener onGetShareUrlResultListener = (OnGetShareUrlResultListener) obj;
            int i = C1371g.f4468a[mo14019a().ordinal()];
            if (i == 1) {
                onGetShareUrlResultListener.onGetPoiDetailShareUrlResult((ShareUrlResult) searchResult);
            } else if (i == 2) {
                onGetShareUrlResultListener.onGetLocationShareUrlResult((ShareUrlResult) searchResult);
            }
        }
    }
}
