package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.baidu.mapapi.synchronization.DisplayOptions;
import com.baidu.mapapi.synchronization.RoleOptions;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.d */
public class C1127d {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final String f3723a = C1127d.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static int f3724b = 0;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static C1135g f3725c;

    /* renamed from: d */
    private static Thread f3726d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static volatile boolean f3727e = true;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static volatile long f3728g = 5000;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public static long f3729h = 5000;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static volatile boolean f3730i = false;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static int f3731j = 1000;

    /* renamed from: f */
    private C1130c f3732f;

    /* renamed from: k */
    private boolean f3733k;

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.d$a */
    private static class C1128a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1127d f3734a = new C1127d();
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.d$b */
    private static class C1129b implements Runnable {

        /* renamed from: a */
        private String f3735a;

        C1129b(String str) {
            this.f3735a = str;
        }

        public void run() {
            while (!C1127d.f3727e) {
                int i = 0;
                if (C1127d.f3725c != null) {
                    C1127d.f3725c.mo13290a(C1127d.f3724b, C1127d.f3730i);
                    boolean unused = C1127d.f3730i = false;
                }
                try {
                    Thread.sleep(C1127d.f3728g);
                } catch (InterruptedException unused2) {
                    Thread.currentThread().interrupt();
                }
                if (C1127d.f3725c != null) {
                    i = C1127d.f3725c.mo13300d();
                }
                if (i >= 3) {
                    long j = (long) ((i / 3) + 1);
                    long j2 = 60000;
                    if (C1127d.f3729h * j < 60000) {
                        j2 = C1127d.f3729h * j;
                    }
                    long unused3 = C1127d.f3728g = j2;
                } else {
                    long unused4 = C1127d.f3728g = C1127d.f3729h;
                }
            }
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.d$c */
    private static class C1130c extends Handler {
        C1130c() {
        }

        /* renamed from: a */
        private void m3924a(int i) {
            if (C1127d.f3731j != i) {
                boolean unused = C1127d.f3730i = true;
                int unused2 = C1127d.f3731j = i;
                return;
            }
            boolean unused3 = C1127d.f3730i = false;
        }

        public void handleMessage(Message message) {
            String k = C1127d.f3723a;
            C1120a.m3851a(k, "The order state is: " + message.what);
            m3924a(message.what);
            int i = message.what;
            if (i != 0) {
                if (i == 1 || i == 2 || i == 3 || i == 4) {
                    C1127d.m3910q();
                    return;
                } else if (i != 5) {
                    C1120a.m3855b(C1127d.f3723a, "The order state is undefined");
                    return;
                }
            }
            C1127d.m3909p();
        }
    }

    private C1127d() {
        this.f3733k = true;
    }

    /* renamed from: a */
    static C1127d m3895a() {
        return C1128a.f3734a;
    }

    /* renamed from: o */
    private void m3908o() {
        f3727e = true;
        Thread thread = f3726d;
        if (thread != null) {
            thread.interrupt();
            f3726d = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: p */
    public static synchronized void m3909p() {
        synchronized (C1127d.class) {
            f3727e = true;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0042, code lost:
        return;
     */
    /* renamed from: q */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void m3910q() {
        /*
            java.lang.Class<com.baidu.mapsdkplatform.comapi.synchronization.data.d> r0 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.class
            monitor-enter(r0)
            java.lang.Thread r1 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.f3726d     // Catch:{ all -> 0x0043 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r0)
            return
        L_0x0009:
            r1 = 0
            com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.f3727e = r1     // Catch:{ all -> 0x0043 }
            java.lang.Thread$State r1 = java.lang.Thread.State.NEW     // Catch:{ all -> 0x0043 }
            java.lang.Thread r2 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.f3726d     // Catch:{ all -> 0x0043 }
            java.lang.Thread$State r2 = r2.getState()     // Catch:{ all -> 0x0043 }
            if (r1 != r2) goto L_0x001b
            java.lang.Thread r1 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.f3726d     // Catch:{ all -> 0x0043 }
            r1.start()     // Catch:{ all -> 0x0043 }
        L_0x001b:
            java.lang.Thread$State r1 = java.lang.Thread.State.TERMINATED     // Catch:{ all -> 0x0043 }
            java.lang.Thread r2 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.f3726d     // Catch:{ all -> 0x0043 }
            java.lang.Thread$State r2 = r2.getState()     // Catch:{ all -> 0x0043 }
            if (r1 != r2) goto L_0x0041
            r1 = 0
            com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.f3726d = r1     // Catch:{ all -> 0x0043 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ all -> 0x0043 }
            com.baidu.mapsdkplatform.comapi.synchronization.data.d$b r2 = new com.baidu.mapsdkplatform.comapi.synchronization.data.d$b     // Catch:{ all -> 0x0043 }
            java.lang.Thread r3 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0043 }
            java.lang.String r3 = r3.getName()     // Catch:{ all -> 0x0043 }
            r2.<init>(r3)     // Catch:{ all -> 0x0043 }
            r1.<init>(r2)     // Catch:{ all -> 0x0043 }
            com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.f3726d = r1     // Catch:{ all -> 0x0043 }
            java.lang.Thread r1 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.f3726d     // Catch:{ all -> 0x0043 }
            r1.start()     // Catch:{ all -> 0x0043 }
        L_0x0041:
            monitor-exit(r0)
            return
        L_0x0043:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.data.C1127d.m3910q():void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo13260a(int i) {
        String str = f3723a;
        C1120a.m3856c(str, "The order state = " + i);
        f3724b = i;
        if (this.f3732f == null) {
            C1120a.m3855b(f3723a, "SyncDataRequestHandler is null");
            return;
        }
        Message obtainMessage = this.f3732f.obtainMessage();
        obtainMessage.what = i;
        this.f3732f.sendMessage(obtainMessage);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13261a(View view) {
        C1135g gVar = f3725c;
        if (gVar != null) {
            gVar.mo13291a(view);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13262a(DisplayOptions displayOptions) {
        C1135g gVar = f3725c;
        if (gVar != null) {
            gVar.mo13292a(displayOptions);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13263a(RoleOptions roleOptions) {
        C1135g gVar = f3725c;
        if (gVar != null) {
            gVar.mo13293a(roleOptions);
        }
    }

    /* renamed from: a */
    public void mo13264a(RoleOptions roleOptions, DisplayOptions displayOptions) {
        f3725c = C1135g.m3945a();
        C1135g gVar = f3725c;
        if (gVar != null) {
            gVar.mo13296b();
            f3725c.mo13293a(roleOptions);
            f3725c.mo13292a(displayOptions);
        }
        f3726d = new Thread(new C1129b(Thread.currentThread().getName()));
        this.f3732f = new C1130c();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13265a(C1141k kVar) {
        C1135g gVar = f3725c;
        if (gVar != null) {
            gVar.mo13295a(kVar);
        }
    }

    /* renamed from: b */
    public void mo13266b() {
        if (this.f3733k) {
            this.f3733k = false;
        } else {
            m3910q();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13267b(int i) {
        long j = (long) (i * 1000);
        f3729h = j;
        f3728g = j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13268b(View view) {
        C1135g gVar = f3725c;
        if (gVar != null) {
            gVar.mo13297b(view);
        }
    }

    /* renamed from: c */
    public void mo13269c() {
        m3909p();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo13270c(View view) {
        C1135g gVar = f3725c;
        if (gVar != null) {
            gVar.mo13299c(view);
        }
    }

    /* renamed from: d */
    public void mo13271d() {
        m3908o();
        this.f3732f.removeCallbacksAndMessages(null);
        f3724b = 0;
        f3729h = 5000;
        f3730i = false;
        f3731j = 1000;
        this.f3733k = true;
        C1135g gVar = f3725c;
        if (gVar != null) {
            gVar.mo13304h();
        }
    }
}
