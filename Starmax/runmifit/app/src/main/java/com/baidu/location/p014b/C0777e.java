package com.baidu.location.p014b;

import android.content.SharedPreferences;
import com.baidu.location.C0839f;
import com.baidu.location.Jni;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p018f.C0840a;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0845c;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0854j;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.json.JSONObject;

/* renamed from: com.baidu.location.b.e */
public class C0777e {

    /* renamed from: i */
    private static C0777e f1510i;

    /* renamed from: k */
    private static final String f1511k = (C0854j.f1898a + "/conlts.dat");

    /* renamed from: l */
    private static int f1512l = -1;

    /* renamed from: m */
    private static int f1513m = -1;

    /* renamed from: n */
    private static int f1514n = 0;

    /* renamed from: a */
    public boolean f1515a = true;

    /* renamed from: b */
    public boolean f1516b = true;

    /* renamed from: c */
    public boolean f1517c = false;

    /* renamed from: d */
    public boolean f1518d = true;

    /* renamed from: e */
    public boolean f1519e = true;

    /* renamed from: f */
    public boolean f1520f = true;

    /* renamed from: g */
    public boolean f1521g = true;

    /* renamed from: h */
    public boolean f1522h = false;

    /* renamed from: j */
    private C0778a f1523j = null;

    /* renamed from: com.baidu.location.b.e$a */
    class C0778a extends C0849e {

        /* renamed from: a */
        String f1524a = null;

        /* renamed from: b */
        boolean f1525b = false;

        /* renamed from: c */
        boolean f1526c = false;

        public C0778a() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            String str;
            Map map;
            this.f1882h = C0855k.m2466e();
            this.f1883i = 2;
            String encode = Jni.encode(this.f1524a);
            this.f1524a = null;
            if (this.f1525b) {
                map = this.f1885k;
                str = "grid";
            } else {
                map = this.f1885k;
                str = "conf";
            }
            map.put("qt", str);
            this.f1885k.put("req", encode);
        }

        /* renamed from: a */
        public void mo10538a(String str, boolean z) {
            if (!this.f1526c) {
                this.f1526c = true;
                this.f1524a = str;
                this.f1525b = z;
                ExecutorService c = C0762v.m1943a().mo10505c();
                if (z) {
                    mo10732a(c, true, "loc.map.baidu.com");
                } else if (c != null) {
                    mo10731a(c, C0855k.f1962f);
                } else {
                    mo10734c(C0855k.f1962f);
                }
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:11:0x0021  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo10446a(boolean r2) {
            /*
                r1 = this;
                if (r2 == 0) goto L_0x0017
                java.lang.String r2 = r1.f1884j
                if (r2 == 0) goto L_0x0017
                boolean r2 = r1.f1525b
                if (r2 == 0) goto L_0x0012
                com.baidu.location.b.e r2 = com.baidu.location.p014b.C0777e.this
                byte[] r0 = r1.f1887m
                r2.m2019a(r0)
                goto L_0x001d
            L_0x0012:
                com.baidu.location.b.e r2 = com.baidu.location.p014b.C0777e.this
                java.lang.String r0 = r1.f1884j
                goto L_0x001a
            L_0x0017:
                com.baidu.location.b.e r2 = com.baidu.location.p014b.C0777e.this
                r0 = 0
            L_0x001a:
                r2.m2022b(r0)
            L_0x001d:
                java.util.Map r2 = r1.f1885k
                if (r2 == 0) goto L_0x0026
                java.util.Map r2 = r1.f1885k
                r2.clear()
            L_0x0026:
                r2 = 0
                r1.f1526c = r2
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p014b.C0777e.C0778a.mo10446a(boolean):void");
        }
    }

    private C0777e() {
    }

    /* renamed from: a */
    public static C0777e m2013a() {
        if (f1510i == null) {
            f1510i = new C0777e();
        }
        return f1510i;
    }

    /* renamed from: a */
    private void m2014a(int i) {
        boolean z = true;
        this.f1515a = (i & 1) == 1;
        this.f1516b = (i & 2) == 2;
        this.f1517c = (i & 4) == 4;
        this.f1518d = (i & 8) == 8;
        this.f1520f = (i & 65536) == 65536;
        if ((i & 131072) != 131072) {
            z = false;
        }
        this.f1521g = z;
        if ((i & 16) == 16) {
            this.f1519e = false;
        }
    }

    /* renamed from: a */
    private void m2018a(JSONObject jSONObject) {
        if (jSONObject != null) {
            boolean z = true;
            int i = 14400000;
            int i2 = 10;
            try {
                if (jSONObject.has("ipen") && jSONObject.getInt("ipen") == 0) {
                    z = false;
                }
                if (jSONObject.has("ipvt")) {
                    i = jSONObject.getInt("ipvt");
                }
                if (jSONObject.has("ipvn")) {
                    i2 = jSONObject.getInt("ipvn");
                }
                SharedPreferences.Editor edit = C0839f.getServiceContext().getSharedPreferences("MapCoreServicePre", 0).edit();
                edit.putBoolean("ipLocInfoUpload", z);
                edit.putInt("ipValidTime", i);
                edit.putInt("ipLocInfoUploadTimesPerDay", i2);
                edit.commit();
            } catch (Exception unused) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2019a(byte[] bArr) {
        byte[] bArr2 = bArr;
        int i = 0;
        if (bArr2 != null) {
            try {
                if (bArr2.length < 640) {
                    C0855k.f1980x = false;
                    C0855k.f1977u = C0855k.f1975s + 0.025d;
                    C0855k.f1976t = C0855k.f1974r - 0.025d;
                } else {
                    C0855k.f1980x = true;
                    C0855k.f1976t = Double.longBitsToDouble(((((long) bArr2[7]) & 255) << 56) | ((((long) bArr2[6]) & 255) << 48) | ((((long) bArr2[5]) & 255) << 40) | ((((long) bArr2[4]) & 255) << 32) | ((((long) bArr2[3]) & 255) << 24) | ((((long) bArr2[2]) & 255) << 16) | ((((long) bArr2[1]) & 255) << 8) | (((long) bArr2[0]) & 255));
                    C0855k.f1977u = Double.longBitsToDouble(((((long) bArr2[15]) & 255) << 56) | ((((long) bArr2[14]) & 255) << 48) | ((((long) bArr2[13]) & 255) << 40) | ((((long) bArr2[12]) & 255) << 32) | ((((long) bArr2[11]) & 255) << 24) | ((((long) bArr2[10]) & 255) << 16) | ((((long) bArr2[9]) & 255) << 8) | (255 & ((long) bArr2[8])));
                    C0855k.f1979w = new byte[625];
                    while (i < 625) {
                        C0855k.f1979w[i] = bArr2[i + 16];
                        i++;
                    }
                }
                i = 1;
            } catch (Exception unused) {
                return;
            }
        }
        if (i != 0) {
            m2025g();
        }
    }

    /* renamed from: a */
    private boolean m2020a(String str) {
        if (str == null) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("ipconf")) {
                try {
                    m2018a(jSONObject.getJSONObject("ipconf"));
                } catch (Exception unused) {
                }
            }
            int parseInt = Integer.parseInt(jSONObject.getString("ver"));
            if (parseInt <= C0855k.f1981y) {
                return false;
            }
            C0855k.f1981y = parseInt;
            if (jSONObject.has("gps")) {
                String[] split = jSONObject.getString("gps").split("\\|");
                if (split.length > 10) {
                    if (split[0] != null && !split[0].equals("")) {
                        C0855k.f1982z = Float.parseFloat(split[0]);
                    }
                    if (split[1] != null && !split[1].equals("")) {
                        C0855k.f1900A = Float.parseFloat(split[1]);
                    }
                    if (split[2] != null && !split[2].equals("")) {
                        C0855k.f1901B = Float.parseFloat(split[2]);
                    }
                    if (split[3] != null && !split[3].equals("")) {
                        C0855k.f1902C = Float.parseFloat(split[3]);
                    }
                    if (split[4] != null && !split[4].equals("")) {
                        C0855k.f1903D = Integer.parseInt(split[4]);
                    }
                    if (split[5] != null && !split[5].equals("")) {
                        C0855k.f1904E = Integer.parseInt(split[5]);
                    }
                    if (split[6] != null && !split[6].equals("")) {
                        C0855k.f1905F = Integer.parseInt(split[6]);
                    }
                    if (split[7] != null && !split[7].equals("")) {
                        C0855k.f1906G = Integer.parseInt(split[7]);
                    }
                    if (split[8] != null && !split[8].equals("")) {
                        C0855k.f1907H = Integer.parseInt(split[8]);
                    }
                    if (split[9] != null && !split[9].equals("")) {
                        C0855k.f1908I = Integer.parseInt(split[9]);
                    }
                    if (split[10] != null && !split[10].equals("")) {
                        C0855k.f1909J = Integer.parseInt(split[10]);
                    }
                }
            }
            if (jSONObject.has("up")) {
                String[] split2 = jSONObject.getString("up").split("\\|");
                if (split2.length > 3) {
                    if (split2[0] != null && !split2[0].equals("")) {
                        C0855k.f1910K = Float.parseFloat(split2[0]);
                    }
                    if (split2[1] != null && !split2[1].equals("")) {
                        C0855k.f1911L = Float.parseFloat(split2[1]);
                    }
                    if (split2[2] != null && !split2[2].equals("")) {
                        C0855k.f1912M = Float.parseFloat(split2[2]);
                    }
                    if (split2[3] != null && !split2[3].equals("")) {
                        C0855k.f1913N = Float.parseFloat(split2[3]);
                    }
                }
            }
            if (jSONObject.has("wf")) {
                String[] split3 = jSONObject.getString("wf").split("\\|");
                if (split3.length > 3) {
                    if (split3[0] != null && !split3[0].equals("")) {
                        C0855k.f1914O = Integer.parseInt(split3[0]);
                    }
                    if (split3[1] != null && !split3[1].equals("")) {
                        C0855k.f1916Q = Float.parseFloat(split3[1]);
                    }
                    if (split3[2] != null && !split3[2].equals("")) {
                        C0855k.f1917R = Integer.parseInt(split3[2]);
                    }
                    if (split3[3] != null && !split3[3].equals("")) {
                        C0855k.f1918S = Float.parseFloat(split3[3]);
                    }
                }
            }
            if (jSONObject.has("ab")) {
                String[] split4 = jSONObject.getString("ab").split("\\|");
                if (split4.length > 3) {
                    if (split4[0] != null && !split4[0].equals("")) {
                        C0855k.f1919T = Float.parseFloat(split4[0]);
                    }
                    if (split4[1] != null && !split4[1].equals("")) {
                        C0855k.f1920U = Float.parseFloat(split4[1]);
                    }
                    if (split4[2] != null && !split4[2].equals("")) {
                        C0855k.f1921V = Integer.parseInt(split4[2]);
                    }
                    if (split4[3] != null && !split4[3].equals("")) {
                        C0855k.f1922W = Integer.parseInt(split4[3]);
                    }
                }
            }
            if (jSONObject.has("zxd")) {
                String[] split5 = jSONObject.getString("zxd").split("\\|");
                if (split5.length > 4) {
                    if (split5[0] != null && !split5[0].equals("")) {
                        C0855k.f1950as = Float.parseFloat(split5[0]);
                    }
                    if (split5[1] != null && !split5[1].equals("")) {
                        C0855k.f1951at = Float.parseFloat(split5[1]);
                    }
                    if (split5[2] != null && !split5[2].equals("")) {
                        C0855k.f1952au = Integer.parseInt(split5[2]);
                    }
                    if (split5[3] != null && !split5[3].equals("")) {
                        C0855k.f1953av = Integer.parseInt(split5[3]);
                    }
                    if (split5[4] != null && !split5[4].equals("")) {
                        C0855k.f1954aw = Integer.parseInt(split5[4]);
                    }
                }
            }
            if (jSONObject.has("gpc")) {
                String[] split6 = jSONObject.getString("gpc").split("\\|");
                if (split6.length > 5) {
                    if (split6[0] != null && !split6[0].equals("")) {
                        if (Integer.parseInt(split6[0]) > 0) {
                            C0855k.f1933ab = true;
                        } else {
                            C0855k.f1933ab = false;
                        }
                    }
                    if (split6[1] != null && !split6[1].equals("")) {
                        if (Integer.parseInt(split6[1]) > 0) {
                            C0855k.f1934ac = true;
                        } else {
                            C0855k.f1934ac = false;
                        }
                    }
                    if (split6[2] != null && !split6[2].equals("")) {
                        C0855k.f1935ad = Integer.parseInt(split6[2]);
                    }
                    if (split6[3] != null && !split6[3].equals("")) {
                        C0855k.f1937af = Integer.parseInt(split6[3]);
                    }
                    if (split6[4] != null && !split6[4].equals("")) {
                        int parseInt2 = Integer.parseInt(split6[4]);
                        if (parseInt2 > 0) {
                            C0855k.f1943al = (long) parseInt2;
                            C0855k.f1939ah = C0855k.f1943al * 1000 * 60;
                            C0855k.f1944am = C0855k.f1939ah >> 2;
                        } else {
                            C0855k.f1972p = false;
                        }
                    }
                    if (split6[5] != null && !split6[5].equals("")) {
                        C0855k.f1946ao = Integer.parseInt(split6[5]);
                    }
                }
            }
            if (jSONObject.has("shak")) {
                String[] split7 = jSONObject.getString("shak").split("\\|");
                if (split7.length > 2) {
                    if (split7[0] != null && !split7[0].equals("")) {
                        C0855k.f1947ap = Integer.parseInt(split7[0]);
                    }
                    if (split7[1] != null && !split7[1].equals("")) {
                        C0855k.f1948aq = Integer.parseInt(split7[1]);
                    }
                    if (split7[2] != null && !split7[2].equals("")) {
                        C0855k.f1949ar = Float.parseFloat(split7[2]);
                    }
                }
            }
            if (jSONObject.has("dmx")) {
                C0855k.f1945an = jSONObject.getInt("dmx");
            }
            return true;
        } catch (Exception unused2) {
            return false;
        }
    }

    /* renamed from: b */
    private void m2021b(int i) {
        File file = new File(f1511k);
        if (!file.exists()) {
            m2027i();
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(4);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            randomAccessFile.seek((long) ((readInt * f1514n) + 128));
            byte[] bytes = (C0844b.f1848e + 0).getBytes();
            randomAccessFile.writeInt(bytes.length);
            randomAccessFile.write(bytes, 0, bytes.length);
            randomAccessFile.writeInt(i);
            if (readInt2 == f1514n) {
                randomAccessFile.seek(8);
                randomAccessFile.writeInt(readInt2 + 1);
            }
            randomAccessFile.close();
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(10:2|3|(1:5)|6|7|(1:9)|10|11|(1:13)(2:14|(1:16)(1:17))|(2:19|21)(1:24)) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0025 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0010 */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002c A[Catch:{ Exception -> 0x0041 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034 A[Catch:{ Exception -> 0x0041 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e A[Catch:{ Exception -> 0x0041 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001b A[Catch:{ Exception -> 0x0025 }] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m2022b(java.lang.String r4) {
        /*
            r3 = this;
            java.lang.String r0 = "ctr"
            r1 = -1
            com.baidu.location.p014b.C0777e.f1513m = r1
            if (r4 == 0) goto L_0x0041
            boolean r2 = r3.m2020a(r4)     // Catch:{ Exception -> 0x0010 }
            if (r2 == 0) goto L_0x0010
            r3.m2024f()     // Catch:{ Exception -> 0x0010 }
        L_0x0010:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x0025 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0025 }
            boolean r4 = r2.has(r0)     // Catch:{ Exception -> 0x0025 }
            if (r4 == 0) goto L_0x0025
            java.lang.String r4 = r2.getString(r0)     // Catch:{ Exception -> 0x0025 }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x0025 }
            com.baidu.location.p014b.C0777e.f1513m = r4     // Catch:{ Exception -> 0x0025 }
        L_0x0025:
            r3.m2028j()     // Catch:{ Exception -> 0x0041 }
            int r4 = com.baidu.location.p014b.C0777e.f1513m     // Catch:{ Exception -> 0x0041 }
            if (r4 == r1) goto L_0x0034
            int r4 = com.baidu.location.p014b.C0777e.f1513m     // Catch:{ Exception -> 0x0041 }
            int r0 = com.baidu.location.p014b.C0777e.f1513m     // Catch:{ Exception -> 0x0041 }
            r3.m2021b(r0)     // Catch:{ Exception -> 0x0041 }
            goto L_0x003c
        L_0x0034:
            int r4 = com.baidu.location.p014b.C0777e.f1512l     // Catch:{ Exception -> 0x0041 }
            if (r4 == r1) goto L_0x003b
            int r4 = com.baidu.location.p014b.C0777e.f1512l     // Catch:{ Exception -> 0x0041 }
            goto L_0x003c
        L_0x003b:
            r4 = -1
        L_0x003c:
            if (r4 == r1) goto L_0x0041
            r3.m2014a(r4)     // Catch:{ Exception -> 0x0041 }
        L_0x0041:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p014b.C0777e.m2022b(java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.b.e.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.baidu.location.g.e.a(android.content.Context, android.net.NetworkInfo):int
      com.baidu.location.g.e.a(java.util.concurrent.ExecutorService, java.lang.String):void
      com.baidu.location.b.e.a.a(java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m2023e() {
        String str = "&ver=" + C0855k.f1981y + "&usr=" + C0844b.m2417a().mo10717b() + "&app=" + C0844b.f1848e + "&prod=" + C0844b.f1849f;
        if (this.f1523j == null) {
            this.f1523j = new C0778a();
        }
        if (!C0855k.m2460b()) {
            this.f1523j.mo10538a(str, false);
        }
    }

    /* renamed from: f */
    private void m2024f() {
        String str = C0854j.f1898a + "/config.dat";
        byte[] bytes = String.format(Locale.CHINA, "{\"ver\":\"%d\",\"gps\":\"%.1f|%.1f|%.1f|%.1f|%d|%d|%d|%d|%d|%d|%d\",\"up\":\"%.1f|%.1f|%.1f|%.1f\",\"wf\":\"%d|%.1f|%d|%.1f\",\"ab\":\"%.2f|%.2f|%d|%d\",\"gpc\":\"%d|%d|%d|%d|%d|%d\",\"zxd\":\"%.1f|%.1f|%d|%d|%d\",\"shak\":\"%d|%d|%.1f\",\"dmx\":%d}", Integer.valueOf(C0855k.f1981y), Float.valueOf(C0855k.f1982z), Float.valueOf(C0855k.f1900A), Float.valueOf(C0855k.f1901B), Float.valueOf(C0855k.f1902C), Integer.valueOf(C0855k.f1903D), Integer.valueOf(C0855k.f1904E), Integer.valueOf(C0855k.f1905F), Integer.valueOf(C0855k.f1906G), Integer.valueOf(C0855k.f1907H), Integer.valueOf(C0855k.f1908I), Integer.valueOf(C0855k.f1909J), Float.valueOf(C0855k.f1910K), Float.valueOf(C0855k.f1911L), Float.valueOf(C0855k.f1912M), Float.valueOf(C0855k.f1913N), Integer.valueOf(C0855k.f1914O), Float.valueOf(C0855k.f1916Q), Integer.valueOf(C0855k.f1917R), Float.valueOf(C0855k.f1918S), Float.valueOf(C0855k.f1919T), Float.valueOf(C0855k.f1920U), Integer.valueOf(C0855k.f1921V), Integer.valueOf(C0855k.f1922W), Integer.valueOf(C0855k.f1933ab ? 1 : 0), Integer.valueOf(C0855k.f1934ac ? 1 : 0), Integer.valueOf(C0855k.f1935ad), Integer.valueOf(C0855k.f1937af), Long.valueOf(C0855k.f1943al), Integer.valueOf(C0855k.f1946ao), Float.valueOf(C0855k.f1950as), Float.valueOf(C0855k.f1951at), Integer.valueOf(C0855k.f1952au), Integer.valueOf(C0855k.f1953av), Integer.valueOf(C0855k.f1954aw), Integer.valueOf(C0855k.f1947ap), Integer.valueOf(C0855k.f1948aq), Float.valueOf(C0855k.f1949ar), Integer.valueOf(C0855k.f1945an)).getBytes();
        try {
            File file = new File(str);
            if (!file.exists()) {
                File file2 = new File(C0854j.f1898a);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (file.createNewFile()) {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(0);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.close();
                } else {
                    return;
                }
            }
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
            randomAccessFile2.seek(0);
            randomAccessFile2.writeBoolean(true);
            randomAccessFile2.seek(2);
            randomAccessFile2.writeInt(bytes.length);
            randomAccessFile2.write(bytes);
            randomAccessFile2.close();
        } catch (Exception unused) {
        }
    }

    /* renamed from: g */
    private void m2025g() {
        try {
            File file = new File(C0854j.f1898a + "/config.dat");
            if (!file.exists()) {
                File file2 = new File(C0854j.f1898a);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (file.createNewFile()) {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(0);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.close();
                } else {
                    return;
                }
            }
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
            randomAccessFile2.seek(1);
            randomAccessFile2.writeBoolean(true);
            randomAccessFile2.seek(1024);
            randomAccessFile2.writeDouble(C0855k.f1976t);
            randomAccessFile2.writeDouble(C0855k.f1977u);
            randomAccessFile2.writeBoolean(C0855k.f1980x);
            if (C0855k.f1980x && C0855k.f1979w != null) {
                randomAccessFile2.write(C0855k.f1979w);
            }
            randomAccessFile2.close();
        } catch (Exception unused) {
        }
    }

    /* renamed from: h */
    private void m2026h() {
        try {
            File file = new File(C0854j.f1898a + "/config.dat");
            if (file.exists()) {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                if (randomAccessFile.readBoolean()) {
                    randomAccessFile.seek(2);
                    int readInt = randomAccessFile.readInt();
                    byte[] bArr = new byte[readInt];
                    randomAccessFile.read(bArr, 0, readInt);
                    m2020a(new String(bArr));
                }
                randomAccessFile.seek(1);
                if (randomAccessFile.readBoolean()) {
                    randomAccessFile.seek(1024);
                    C0855k.f1976t = randomAccessFile.readDouble();
                    C0855k.f1977u = randomAccessFile.readDouble();
                    C0855k.f1980x = randomAccessFile.readBoolean();
                    if (C0855k.f1980x) {
                        C0855k.f1979w = new byte[625];
                        randomAccessFile.read(C0855k.f1979w, 0, 625);
                    }
                }
                randomAccessFile.close();
            }
        } catch (Exception unused) {
        }
        if (C0855k.f1972p) {
            boolean z = C0839f.isServing;
        }
    }

    /* renamed from: i */
    private void m2027i() {
        try {
            File file = new File(f1511k);
            if (!file.exists()) {
                File file2 = new File(C0854j.f1898a);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (!file.createNewFile()) {
                    file = null;
                }
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(0);
                randomAccessFile.writeInt(0);
                randomAccessFile.writeInt(128);
                randomAccessFile.writeInt(0);
                randomAccessFile.close();
            }
        } catch (Exception unused) {
        }
    }

    /* renamed from: j */
    private void m2028j() {
        try {
            File file = new File(f1511k);
            if (file.exists()) {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(4);
                int readInt = randomAccessFile.readInt();
                if (readInt > 3000) {
                    randomAccessFile.close();
                    f1514n = 0;
                    m2027i();
                    return;
                }
                int readInt2 = randomAccessFile.readInt();
                randomAccessFile.seek(128);
                byte[] bArr = new byte[readInt];
                int i = 0;
                while (true) {
                    if (i >= readInt2) {
                        break;
                    }
                    randomAccessFile.seek((long) ((readInt * i) + 128));
                    int readInt3 = randomAccessFile.readInt();
                    if (readInt3 > 0 && readInt3 < readInt) {
                        randomAccessFile.read(bArr, 0, readInt3);
                        int i2 = readInt3 - 1;
                        if (bArr[i2] == 0) {
                            String str = new String(bArr, 0, i2);
                            C0844b.m2417a();
                            if (str.equals(C0844b.f1848e)) {
                                f1512l = randomAccessFile.readInt();
                                f1514n = i;
                                break;
                            }
                        } else {
                            continue;
                        }
                    }
                    i++;
                }
                if (i == readInt2) {
                    f1514n = readInt2;
                }
                randomAccessFile.close();
            }
        } catch (Exception unused) {
        }
    }

    /* renamed from: b */
    public void mo10535b() {
        m2026h();
    }

    /* renamed from: c */
    public void mo10536c() {
    }

    /* renamed from: d */
    public void mo10537d() {
        if (System.currentTimeMillis() - C0845c.m2426a().mo10725d() > Config.MAX_LOG_DATA_EXSIT_TIME) {
            C0845c.m2426a().mo10724c(System.currentTimeMillis());
            C0840a.m2405a().postDelayed(new C0779f(this), 1000);
        }
    }
}
