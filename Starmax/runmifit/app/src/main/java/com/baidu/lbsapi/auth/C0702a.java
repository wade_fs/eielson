package com.baidu.lbsapi.auth;

import android.util.Log;

/* renamed from: com.baidu.lbsapi.auth.a */
class C0702a {

    /* renamed from: a */
    public static boolean f1077a = false;

    /* renamed from: b */
    private static String f1078b = "BaiduApiAuth";

    /* renamed from: a */
    public static String m1615a() {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[2];
        return stackTraceElement.getFileName() + "[" + stackTraceElement.getLineNumber() + "]";
    }

    /* renamed from: a */
    public static void m1616a(String str) {
        if (f1077a && Thread.currentThread().getStackTrace().length != 0) {
            String str2 = f1078b;
            Log.d(str2, m1615a() + ";" + str);
        }
    }

    /* renamed from: b */
    public static void m1617b(String str) {
        if (Thread.currentThread().getStackTrace().length != 0) {
            Log.i(f1078b, str);
        }
    }

    /* renamed from: c */
    public static void m1618c(String str) {
        if (f1077a && Thread.currentThread().getStackTrace().length != 0) {
            String str2 = f1078b;
            Log.e(str2, m1615a() + ";" + str);
        }
    }
}
