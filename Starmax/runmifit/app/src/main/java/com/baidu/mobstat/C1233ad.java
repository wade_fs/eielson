package com.baidu.mobstat;

import android.content.Context;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.ad */
class C1233ad implements C1288h {

    /* renamed from: a */
    private C1237ag f4242a = C1237ag.f4266a;

    /* renamed from: b */
    private Object f4243b;

    /* renamed from: c */
    private Class<?> f4244c;

    public C1233ad(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("proxy is null.");
        } else if ("com.baidu.bottom.remote.BPStretegyController2".equals(obj.getClass().getName())) {
            this.f4243b = obj;
            this.f4244c = obj.getClass();
        } else {
            throw new IllegalArgumentException("class isn't com.baidu.bottom.remote.BPStretegyController2");
        }
    }

    /* renamed from: a */
    public void mo13884a(Context context, JSONObject jSONObject) {
        try {
            m4514a(new Object[]{context, jSONObject}, "startDataAnynalyze", new Class[]{Context.class, JSONObject.class});
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            this.f4242a.mo13909a(context, jSONObject);
        }
    }

    /* renamed from: a */
    public void mo13883a(Context context, String str) {
        try {
            m4514a(new Object[]{context, str}, "saveRemoteConfig2", new Class[]{Context.class, String.class});
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            this.f4242a.mo13908a(context, str);
        }
    }

    /* renamed from: b */
    public void mo13886b(Context context, String str) {
        try {
            m4514a(new Object[]{context, str}, "saveRemoteSign", new Class[]{Context.class, String.class});
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            this.f4242a.mo13911b(context, str);
        }
    }

    /* renamed from: a */
    public void mo13882a(Context context, long j) {
        try {
            m4514a(new Object[]{context, Long.valueOf(j)}, "setLastUpdateTime", new Class[]{Context.class, Long.TYPE});
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            this.f4242a.mo13907a(context, j);
        }
    }

    /* renamed from: a */
    public boolean mo13885a(Context context) {
        try {
            return ((Boolean) m4514a(new Object[]{context}, "needUpdate", new Class[]{Context.class})).booleanValue();
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            return this.f4242a.mo13910a(context);
        }
    }

    /* renamed from: b */
    public boolean mo13887b(Context context) {
        try {
            return ((Boolean) m4514a(new Object[]{context}, "canStartService", new Class[]{Context.class})).booleanValue();
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            return this.f4242a.mo13912b(context);
        }
    }

    /* renamed from: a */
    private <T> T m4514a(Object[] objArr, String str, Class<?>[] clsArr) throws Exception {
        return this.f4244c.getMethod(str, clsArr).invoke(this.f4243b, objArr);
    }
}
