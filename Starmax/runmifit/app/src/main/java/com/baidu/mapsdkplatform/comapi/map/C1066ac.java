package com.baidu.mapsdkplatform.comapi.map;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.TextureView;
import com.baidu.mapapi.common.EnvironmentUtilities;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1088m;
import com.baidu.mobstat.Config;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.ac */
public class C1066ac extends TextureView implements GestureDetector.OnDoubleTapListener, GestureDetector.OnGestureListener, TextureView.SurfaceTextureListener, C1088m.C1089a {

    /* renamed from: a */
    public static int f3442a;

    /* renamed from: b */
    public static int f3443b;

    /* renamed from: c */
    public static int f3444c;

    /* renamed from: d */
    private GestureDetector f3445d;

    /* renamed from: e */
    private Handler f3446e;

    /* renamed from: f */
    private boolean f3447f = false;

    /* renamed from: g */
    private SurfaceTexture f3448g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public C1088m f3449h = null;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public C1078e f3450i;

    public C1066ac(Context context, C1102z zVar, String str, int i) {
        super(context);
        m3506a(context, zVar, str, i);
    }

    /* renamed from: a */
    private void m3506a(Context context, C1102z zVar, String str, int i) {
        setSurfaceTextureListener(this);
        if (context != null) {
            this.f3445d = new GestureDetector(context, this);
            EnvironmentUtilities.initAppDirectory(context);
            if (this.f3450i == null) {
                this.f3450i = new C1078e(context, str, i);
            }
            this.f3450i.mo13008a(context.hashCode());
            this.f3450i.mo13006a();
            this.f3450i.mo13020a(zVar);
            m3508e();
            this.f3450i.mo13012a(this.f3446e);
            this.f3450i.mo13048f();
            return;
        }
        throw new RuntimeException("BDMapSDKException: when you create an mapview, the context can not be null");
    }

    /* renamed from: e */
    private void m3508e() {
        this.f3446e = new C1067ad(this);
    }

    /* renamed from: a */
    public int mo12951a() {
        C1078e eVar = this.f3450i;
        if (eVar == null) {
            return 0;
        }
        if (f3444c <= 1) {
            MapRenderer.nativeResize(eVar.f3534j, f3442a, f3443b);
            f3444c++;
        }
        return MapRenderer.nativeRender(this.f3450i.f3534j);
    }

    /* renamed from: a */
    public void mo12952a(int i) {
        synchronized (this.f3450i) {
            if (this.f3450i.f3532h != null) {
                for (C1087l lVar : this.f3450i.f3532h) {
                    if (lVar != null) {
                        lVar.mo11558f();
                    }
                }
            }
            if (this.f3450i != null) {
                this.f3450i.mo13034b(this.f3446e);
                this.f3450i.mo13032b(i);
                this.f3450i = null;
            }
            this.f3446e.removeCallbacksAndMessages(null);
            if (this.f3449h != null) {
                this.f3449h.mo13117c();
                this.f3449h = null;
            }
            if (this.f3448g != null) {
                if (Build.VERSION.SDK_INT >= 19) {
                    this.f3448g.release();
                }
                this.f3448g = null;
            }
        }
    }

    /* renamed from: a */
    public void mo12953a(String str, Rect rect) {
        C1088m mVar;
        C1078e eVar = this.f3450i;
        if (eVar != null && eVar.f3533i != null) {
            if (rect != null) {
                int i = rect.left;
                int i2 = f3443b < rect.bottom ? 0 : f3443b - rect.bottom;
                int width = rect.width();
                int height = rect.height();
                if (i >= 0 && i2 >= 0 && width > 0 && height > 0) {
                    if (width > f3442a) {
                        width = Math.abs(rect.width()) - (rect.right - f3442a);
                    }
                    if (height > f3443b) {
                        height = Math.abs(rect.height()) - (rect.bottom - f3443b);
                    }
                    if (i > SysOSUtil.getScreenSizeX() || i2 > SysOSUtil.getScreenSizeY()) {
                        this.f3450i.f3533i.mo13476a(str, (Bundle) null);
                        C1088m mVar2 = this.f3449h;
                        if (mVar2 != null) {
                            mVar2.mo13115a();
                            return;
                        }
                        return;
                    }
                    f3442a = width;
                    f3443b = height;
                    Bundle bundle = new Bundle();
                    bundle.putInt(Config.EVENT_HEAT_X, i);
                    bundle.putInt("y", i2);
                    bundle.putInt("width", width);
                    bundle.putInt("height", height);
                    this.f3450i.f3533i.mo13476a(str, bundle);
                    mVar = this.f3449h;
                    if (mVar == null) {
                        return;
                    }
                } else {
                    return;
                }
            } else {
                this.f3450i.f3533i.mo13476a(str, (Bundle) null);
                mVar = this.f3449h;
                if (mVar == null) {
                    return;
                }
            }
            mVar.mo13115a();
        }
    }

    /* renamed from: b */
    public C1078e mo12954b() {
        return this.f3450i;
    }

    /* renamed from: c */
    public void mo12955c() {
        C1078e eVar = this.f3450i;
        if (eVar != null && eVar.f3533i != null) {
            if (this.f3450i.f3532h != null) {
                for (C1087l lVar : this.f3450i.f3532h) {
                    if (lVar != null) {
                        lVar.mo11554d();
                    }
                }
            }
            this.f3450i.f3533i.mo13512g();
            this.f3450i.f3533i.mo13500d();
            this.f3450i.f3533i.mo13521n();
            C1088m mVar = this.f3449h;
            if (mVar != null) {
                mVar.mo13115a();
            }
            if (this.f3450i.mo13036b()) {
                this.f3447f = true;
            }
        }
    }

    /* renamed from: d */
    public void mo12956d() {
        C1078e eVar = this.f3450i;
        if (eVar != null && eVar.f3533i != null) {
            this.f3447f = false;
            this.f3450i.f3533i.mo13497c();
            synchronized (this.f3450i) {
                this.f3450i.f3533i.mo13497c();
                if (this.f3449h != null) {
                    this.f3449h.mo13116b();
                }
            }
        }
    }

    public boolean onDoubleTap(MotionEvent motionEvent) {
        C1078e eVar = this.f3450i;
        if (eVar == null || eVar.f3533i == null || !this.f3450i.f3535k) {
            return true;
        }
        GeoPoint b = this.f3450i.mo13030b((int) motionEvent.getX(), (int) motionEvent.getY());
        if (b != null) {
            if (this.f3450i.f3532h != null) {
                for (C1087l lVar : this.f3450i.f3532h) {
                    if (lVar != null) {
                        lVar.mo11548b(b);
                    }
                }
            }
            if (this.f3450i.f3530f) {
                C1064ab E = this.f3450i.mo12989E();
                E.f3414a += 1.0f;
                if (!this.f3450i.f3531g) {
                    E.f3417d = b.getLongitudeE6();
                    E.f3418e = b.getLatitudeE6();
                }
                BaiduMap.mapStatusReason |= 1;
                this.f3450i.mo13016a(E, 300);
                C1078e eVar2 = this.f3450i;
                C1078e.f3479m = System.currentTimeMillis();
                return true;
            }
        }
        return false;
    }

    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        C1078e eVar = this.f3450i;
        if (eVar == null || eVar.f3533i == null || !this.f3450i.f3535k) {
            return true;
        }
        if (!this.f3450i.f3529e) {
            return false;
        }
        float sqrt = (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
        if (sqrt <= 500.0f) {
            return false;
        }
        BaiduMap.mapStatusReason |= 1;
        this.f3450i.mo12985A();
        this.f3450i.mo13003a(34, (int) (sqrt * 0.6f), ((int) motionEvent2.getX()) | (((int) motionEvent2.getY()) << 16));
        this.f3450i.mo12997M();
        return true;
    }

    public void onLongPress(MotionEvent motionEvent) {
        C1078e eVar = this.f3450i;
        if (eVar != null && eVar.f3533i != null && this.f3450i.f3535k) {
            String a = this.f3450i.f3533i.mo13471a(-1, (int) motionEvent.getX(), (int) motionEvent.getY(), this.f3450i.f3536l);
            if (this.f3450i.f3532h != null) {
                if (a == null || a.equals("")) {
                    for (C1087l lVar : this.f3450i.f3532h) {
                        GeoPoint b = this.f3450i.mo13030b((int) motionEvent.getX(), (int) motionEvent.getY());
                        if (lVar != null) {
                            lVar.mo11552c(b);
                        }
                    }
                    return;
                }
                for (C1087l lVar2 : this.f3450i.f3532h) {
                    if (lVar2.mo11550b(a)) {
                        this.f3450i.f3539p = true;
                    } else {
                        lVar2.mo11552c(this.f3450i.mo13030b((int) motionEvent.getX(), (int) motionEvent.getY()));
                    }
                }
            }
        }
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0069  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onSingleTapConfirmed(android.view.MotionEvent r7) {
        /*
            r6 = this;
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3450i
            r1 = 1
            if (r0 == 0) goto L_0x00a5
            com.baidu.mapsdkplatform.comjni.map.basemap.a r0 = r0.f3533i
            if (r0 == 0) goto L_0x00a5
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3450i
            boolean r0 = r0.f3535k
            if (r0 != 0) goto L_0x0011
            goto L_0x00a5
        L_0x0011:
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r0 = r0.f3532h
            if (r0 != 0) goto L_0x0018
            return r1
        L_0x0018:
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3450i
            com.baidu.mapsdkplatform.comjni.map.basemap.a r0 = r0.f3533i
            r2 = -1
            float r3 = r7.getX()
            int r3 = (int) r3
            float r4 = r7.getY()
            int r4 = (int) r4
            com.baidu.mapsdkplatform.comapi.map.e r5 = r6.f3450i
            int r5 = r5.f3536l
            java.lang.String r0 = r0.mo13471a(r2, r3, r4, r5)
            r2 = 0
            if (r0 == 0) goto L_0x007b
            java.lang.String r3 = ""
            boolean r3 = r0.equals(r3)
            if (r3 != 0) goto L_0x007b
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0056 }
            r3.<init>(r0)     // Catch:{ JSONException -> 0x0056 }
            java.lang.String r0 = "px"
            float r2 = r7.getX()     // Catch:{ JSONException -> 0x0054 }
            int r2 = (int) r2     // Catch:{ JSONException -> 0x0054 }
            r3.put(r0, r2)     // Catch:{ JSONException -> 0x0054 }
            java.lang.String r0 = "py"
            float r7 = r7.getY()     // Catch:{ JSONException -> 0x0054 }
            int r7 = (int) r7     // Catch:{ JSONException -> 0x0054 }
            r3.put(r0, r7)     // Catch:{ JSONException -> 0x0054 }
            goto L_0x005b
        L_0x0054:
            r7 = move-exception
            goto L_0x0058
        L_0x0056:
            r7 = move-exception
            r3 = r2
        L_0x0058:
            r7.printStackTrace()
        L_0x005b:
            com.baidu.mapsdkplatform.comapi.map.e r7 = r6.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r7 = r7.f3532h
            java.util.Iterator r7 = r7.iterator()
        L_0x0063:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x00a5
            java.lang.Object r0 = r7.next()
            com.baidu.mapsdkplatform.comapi.map.l r0 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r0
            if (r3 == 0) goto L_0x0063
            if (r0 == 0) goto L_0x0063
            java.lang.String r2 = r3.toString()
            r0.mo11543a(r2)
            goto L_0x0063
        L_0x007b:
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r0 = r0.f3532h
            java.util.Iterator r0 = r0.iterator()
        L_0x0083:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00a5
            java.lang.Object r2 = r0.next()
            com.baidu.mapsdkplatform.comapi.map.l r2 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r2
            if (r2 == 0) goto L_0x0083
            com.baidu.mapsdkplatform.comapi.map.e r3 = r6.f3450i
            float r4 = r7.getX()
            int r4 = (int) r4
            float r5 = r7.getY()
            int r5 = (int) r5
            com.baidu.mapapi.model.inner.GeoPoint r3 = r3.mo13030b(r4, r5)
            r2.mo11541a(r3)
            goto L_0x0083
        L_0x00a5:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.map.C1066ac.onSingleTapConfirmed(android.view.MotionEvent):boolean");
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        if (this.f3450i != null) {
            SurfaceTexture surfaceTexture2 = this.f3448g;
            if (surfaceTexture2 == null) {
                this.f3448g = surfaceTexture;
                this.f3449h = new C1088m(this.f3448g, this, new AtomicBoolean(true), this);
                this.f3449h.start();
                f3442a = i;
                f3443b = i2;
                C1064ab E = this.f3450i.mo12989E();
                if (E != null) {
                    if (E.f3419f == 0 || E.f3419f == -1 || E.f3419f == (E.f3423j.left - E.f3423j.right) / 2) {
                        E.f3419f = -1;
                    }
                    if (E.f3420g == 0 || E.f3420g == -1 || E.f3420g == (E.f3423j.bottom - E.f3423j.top) / 2) {
                        E.f3420g = -1;
                    }
                    E.f3423j.left = 0;
                    E.f3423j.top = 0;
                    E.f3423j.bottom = i2;
                    E.f3423j.right = i;
                    this.f3450i.mo13015a(E);
                    this.f3450i.mo13009a(f3442a, f3443b);
                    return;
                }
                return;
            }
            setSurfaceTexture(surfaceTexture2);
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        C1078e eVar = this.f3450i;
        if (eVar != null) {
            f3442a = i;
            f3443b = i2;
            f3444c = 1;
            C1064ab E = eVar.mo12989E();
            if (E.f3419f == 0 || E.f3419f == -1 || E.f3419f == (E.f3423j.left - E.f3423j.right) / 2) {
                E.f3419f = -1;
            }
            if (E.f3420g == 0 || E.f3420g == -1 || E.f3420g == (E.f3423j.bottom - E.f3423j.top) / 2) {
                E.f3420g = -1;
            }
            E.f3423j.left = 0;
            E.f3423j.top = 0;
            E.f3423j.bottom = i2;
            E.f3423j.right = i;
            this.f3450i.mo13015a(E);
            this.f3450i.mo13009a(f3442a, f3443b);
            MapRenderer.nativeResize(this.f3450i.f3534j, i, i2);
        }
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        C1088m mVar;
        if (this.f3447f && (mVar = this.f3449h) != null) {
            mVar.mo13115a();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        C1078e eVar = this.f3450i;
        if (eVar == null || eVar.f3533i == null) {
            return true;
        }
        super.onTouchEvent(motionEvent);
        if (this.f3450i.f3532h != null) {
            for (C1087l lVar : this.f3450i.f3532h) {
                if (lVar != null) {
                    lVar.mo11540a(motionEvent);
                }
            }
        }
        if (this.f3445d.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f3450i.mo13028a(motionEvent);
    }
}
