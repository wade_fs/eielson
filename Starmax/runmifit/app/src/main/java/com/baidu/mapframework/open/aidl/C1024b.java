package com.baidu.mapframework.open.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

/* renamed from: com.baidu.mapframework.open.aidl.b */
public interface C1024b extends IInterface {

    /* renamed from: com.baidu.mapframework.open.aidl.b$a */
    public static abstract class C1025a extends Binder implements C1024b {

        /* renamed from: com.baidu.mapframework.open.aidl.b$a$a */
        private static class C1026a implements C1024b {

            /* renamed from: a */
            private IBinder f3282a;

            C1026a(IBinder iBinder) {
                this.f3282a = iBinder;
            }

            /* renamed from: a */
            public void mo12827a(IBinder iBinder) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.baidu.mapframework.open.aidl.IOpenClientCallback");
                    obtain.writeStrongBinder(iBinder);
                    this.f3282a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f3282a;
            }
        }

        public C1025a() {
            attachInterface(this, "com.baidu.mapframework.open.aidl.IOpenClientCallback");
        }

        /* renamed from: b */
        public static C1024b m3323b(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.baidu.mapframework.open.aidl.IOpenClientCallback");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof C1024b)) ? new C1026a(iBinder) : (C1024b) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("com.baidu.mapframework.open.aidl.IOpenClientCallback");
                mo12827a(parcel.readStrongBinder());
                parcel2.writeNoException();
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("com.baidu.mapframework.open.aidl.IOpenClientCallback");
                return true;
            }
        }
    }

    /* renamed from: a */
    void mo12827a(IBinder iBinder) throws RemoteException;
}
