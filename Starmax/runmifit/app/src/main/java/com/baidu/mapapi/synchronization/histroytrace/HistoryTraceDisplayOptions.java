package com.baidu.mapapi.synchronization.histroytrace;

import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;

public class HistoryTraceDisplayOptions {

    /* renamed from: a */
    private BitmapDescriptor f3213a = BitmapDescriptorFactory.fromAssetWithDpi("SDK_Default_Icon_Start.png");

    /* renamed from: b */
    private BitmapDescriptor f3214b = BitmapDescriptorFactory.fromAssetWithDpi("SDK_Default_Icon_End.png");

    /* renamed from: c */
    private BitmapDescriptor f3215c = BitmapDescriptorFactory.fromAssetWithDpi("SDK_Default_Icon_Car.png");

    /* renamed from: d */
    private BitmapDescriptor f3216d = BitmapDescriptorFactory.fromAsset("SDK_Default_Route_Texture_Gray_Arrow.png");

    /* renamed from: e */
    private int f3217e = 22;

    /* renamed from: f */
    private boolean f3218f = true;

    /* renamed from: g */
    private boolean f3219g = true;

    /* renamed from: h */
    private boolean f3220h = false;

    /* renamed from: i */
    private boolean f3221i = true;

    /* renamed from: j */
    private boolean f3222j = true;

    /* renamed from: k */
    private int f3223k = 30;

    /* renamed from: l */
    private int f3224l = 30;

    /* renamed from: m */
    private int f3225m = 30;

    /* renamed from: n */
    private int f3226n = 30;

    public BitmapDescriptor getCarIcon() {
        return this.f3215c;
    }

    public BitmapDescriptor getEndPositionIcon() {
        return this.f3214b;
    }

    public int getPaddingBottom() {
        return this.f3226n;
    }

    public int getPaddingLeft() {
        return this.f3223k;
    }

    public int getPaddingRight() {
        return this.f3224l;
    }

    public int getPaddingTop() {
        return this.f3225m;
    }

    public BitmapDescriptor getRouteLineTexture() {
        return this.f3216d;
    }

    public int getRouteLineWidth() {
        return this.f3217e;
    }

    public BitmapDescriptor getStartPositionIcon() {
        return this.f3213a;
    }

    public boolean isRouteLineRenderBySubSection() {
        return this.f3222j;
    }

    public boolean isShowCarIcon() {
        return this.f3220h;
    }

    public boolean isShowEndPositionIcon() {
        return this.f3219g;
    }

    public boolean isShowRoutePlan() {
        return this.f3221i;
    }

    public boolean isShowStartPositionIcon() {
        return this.f3218f;
    }

    public HistoryTraceDisplayOptions setCarIcon(BitmapDescriptor bitmapDescriptor) {
        this.f3215c = bitmapDescriptor;
        return this;
    }

    public HistoryTraceDisplayOptions setEndPositionIcon(BitmapDescriptor bitmapDescriptor) {
        this.f3214b = bitmapDescriptor;
        return this;
    }

    public HistoryTraceDisplayOptions setPaddingBottom(int i) {
        this.f3226n = i;
        return this;
    }

    public HistoryTraceDisplayOptions setPaddingLeft(int i) {
        this.f3223k = i;
        return this;
    }

    public HistoryTraceDisplayOptions setPaddingRight(int i) {
        this.f3224l = i;
        return this;
    }

    public HistoryTraceDisplayOptions setPaddingTop(int i) {
        this.f3225m = i;
        return this;
    }

    public void setRouteLineRenderBySubSection(boolean z) {
        this.f3222j = z;
    }

    public HistoryTraceDisplayOptions setRouteLineTexture(BitmapDescriptor bitmapDescriptor) {
        this.f3216d = bitmapDescriptor;
        return this;
    }

    public HistoryTraceDisplayOptions setRouteLineWidth(int i) {
        int i2 = 5;
        if (i >= 5) {
            i2 = 40;
            if (i <= 40) {
                this.f3217e = i;
                return this;
            }
        }
        this.f3217e = i2;
        return this;
    }

    public HistoryTraceDisplayOptions setShowCarIcon(boolean z) {
        this.f3220h = z;
        return this;
    }

    public HistoryTraceDisplayOptions setShowEndPositionIcon(boolean z) {
        this.f3219g = z;
        return this;
    }

    public HistoryTraceDisplayOptions setShowRoutePlan(boolean z) {
        this.f3221i = z;
        return this;
    }

    public HistoryTraceDisplayOptions setShowStartPositionIcon(boolean z) {
        this.f3218f = z;
        return this;
    }

    public HistoryTraceDisplayOptions setStartPositionIcon(BitmapDescriptor bitmapDescriptor) {
        this.f3213a = bitmapDescriptor;
        return this;
    }
}
