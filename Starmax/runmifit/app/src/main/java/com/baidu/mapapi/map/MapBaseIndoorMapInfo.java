package com.baidu.mapapi.map;

import java.util.ArrayList;

public final class MapBaseIndoorMapInfo {

    /* renamed from: d */
    private static final String f2526d = MapBaseIndoorMapInfo.class.getSimpleName();

    /* renamed from: a */
    String f2527a;

    /* renamed from: b */
    String f2528b;

    /* renamed from: c */
    ArrayList<String> f2529c;

    public enum SwitchFloorError {
        SWITCH_OK,
        FLOOR_INFO_ERROR,
        FLOOR_OVERLFLOW,
        FOCUSED_ID_ERROR,
        SWITCH_ERROR
    }

    public MapBaseIndoorMapInfo() {
    }

    public MapBaseIndoorMapInfo(MapBaseIndoorMapInfo mapBaseIndoorMapInfo) {
        this.f2527a = mapBaseIndoorMapInfo.f2527a;
        this.f2528b = mapBaseIndoorMapInfo.f2528b;
        this.f2529c = mapBaseIndoorMapInfo.f2529c;
    }

    public MapBaseIndoorMapInfo(String str, String str2, ArrayList<String> arrayList) {
        this.f2527a = str;
        this.f2528b = str2;
        this.f2529c = arrayList;
    }

    public String getCurFloor() {
        return this.f2528b;
    }

    public ArrayList<String> getFloors() {
        return this.f2529c;
    }

    public String getID() {
        return this.f2527a;
    }
}
