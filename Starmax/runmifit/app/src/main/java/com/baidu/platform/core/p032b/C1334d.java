package com.baidu.platform.core.p032b;

import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeOption;

/* renamed from: com.baidu.platform.core.b.d */
public interface C1334d {
    /* renamed from: a */
    void mo14038a();

    /* renamed from: a */
    void mo14039a(OnGetGeoCoderResultListener onGetGeoCoderResultListener);

    /* renamed from: a */
    boolean mo14040a(GeoCodeOption geoCodeOption);

    /* renamed from: a */
    boolean mo14041a(ReverseGeoCodeOption reverseGeoCodeOption);
}
