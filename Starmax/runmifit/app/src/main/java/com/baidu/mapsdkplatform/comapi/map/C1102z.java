package com.baidu.mapsdkplatform.comapi.map;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.z */
public final class C1102z {

    /* renamed from: a */
    C1064ab f3625a = new C1064ab();

    /* renamed from: b */
    boolean f3626b = true;

    /* renamed from: c */
    int f3627c = 1;

    /* renamed from: d */
    boolean f3628d = true;

    /* renamed from: e */
    boolean f3629e = true;

    /* renamed from: f */
    boolean f3630f = true;

    /* renamed from: g */
    boolean f3631g = true;

    /* renamed from: a */
    public C1102z mo13142a(int i) {
        this.f3627c = i;
        return this;
    }

    /* renamed from: a */
    public C1102z mo13143a(C1064ab abVar) {
        this.f3625a = abVar;
        return this;
    }

    /* renamed from: a */
    public C1102z mo13144a(boolean z) {
        this.f3626b = z;
        return this;
    }

    /* renamed from: b */
    public C1102z mo13145b(boolean z) {
        this.f3628d = z;
        return this;
    }

    /* renamed from: c */
    public C1102z mo13146c(boolean z) {
        this.f3629e = z;
        return this;
    }

    /* renamed from: d */
    public C1102z mo13147d(boolean z) {
        this.f3630f = z;
        return this;
    }

    /* renamed from: e */
    public C1102z mo13148e(boolean z) {
        this.f3631g = z;
        return this;
    }
}
