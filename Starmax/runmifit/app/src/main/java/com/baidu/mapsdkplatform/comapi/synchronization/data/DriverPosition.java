package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;

public final class DriverPosition implements Parcelable {
    public static final Parcelable.Creator<DriverPosition> CREATOR = new C1124a();

    /* renamed from: a */
    private String f3700a;

    /* renamed from: b */
    private LatLng f3701b;

    /* renamed from: c */
    private double f3702c;

    /* renamed from: d */
    private double f3703d;

    /* renamed from: e */
    private int f3704e;

    public DriverPosition() {
        this.f3700a = null;
        this.f3701b = null;
        this.f3702c = 0.0d;
        this.f3703d = 0.0d;
        this.f3704e = 0;
    }

    protected DriverPosition(Parcel parcel) {
        this.f3700a = parcel.readString();
        this.f3701b = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        this.f3702c = parcel.readDouble();
        this.f3703d = parcel.readDouble();
        this.f3704e = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public double getAngle() {
        return this.f3702c;
    }

    public int getOrderStateInPosition() {
        return this.f3704e;
    }

    public LatLng getPoint() {
        return this.f3701b;
    }

    public double getSpeed() {
        return this.f3703d;
    }

    public String getTimeStamp() {
        return this.f3700a;
    }

    public void setAngle(double d) {
        double d2 = 0.0d;
        if (d >= 0.0d) {
            d2 = 360.0d;
            if (d < 360.0d) {
                this.f3702c = d;
                return;
            }
        }
        this.f3702c = d2;
    }

    public void setOrderStateInPosition(int i) {
        this.f3704e = i;
    }

    public void setPoint(LatLng latLng) {
        this.f3701b = latLng;
    }

    public void setSpeed(double d) {
        this.f3703d = d;
    }

    public void setTimeStamp(String str) {
        this.f3700a = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3700a);
        parcel.writeParcelable(this.f3701b, i);
        parcel.writeDouble(this.f3702c);
        parcel.writeDouble(this.f3703d);
        parcel.writeInt(this.f3704e);
    }
}
