package com.baidu.platform.core.p036f;

import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;

/* renamed from: com.baidu.platform.core.f.a */
public interface C1373a {
    /* renamed from: a */
    void mo14073a();

    /* renamed from: a */
    void mo14074a(OnGetSuggestionResultListener onGetSuggestionResultListener);

    /* renamed from: a */
    boolean mo14075a(SuggestionSearchOption suggestionSearchOption);
}
