package com.baidu.mapapi.utils.poi;

import com.baidu.mapapi.http.HttpClient;
import com.baidu.platform.comapi.pano.PanoStateError;

/* renamed from: com.baidu.mapapi.utils.poi.b */
/* synthetic */ class C1018b {

    /* renamed from: a */
    static final /* synthetic */ int[] f3270a = new int[PanoStateError.values().length];

    /* renamed from: b */
    static final /* synthetic */ int[] f3271b = new int[HttpClient.HttpStateError.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(15:0|(2:1|2)|3|5|6|7|9|10|11|12|13|14|15|16|18) */
    /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|5|6|7|9|10|11|12|13|14|15|16|18) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0047 */
    static {
        /*
            com.baidu.mapapi.http.HttpClient$HttpStateError[] r0 = com.baidu.mapapi.http.HttpClient.HttpStateError.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.baidu.mapapi.utils.poi.C1018b.f3271b = r0
            r0 = 1
            int[] r1 = com.baidu.mapapi.utils.poi.C1018b.f3271b     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.baidu.mapapi.http.HttpClient$HttpStateError r2 = com.baidu.mapapi.http.HttpClient.HttpStateError.NETWORK_ERROR     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.baidu.mapapi.utils.poi.C1018b.f3271b     // Catch:{ NoSuchFieldError -> 0x001f }
            com.baidu.mapapi.http.HttpClient$HttpStateError r3 = com.baidu.mapapi.http.HttpClient.HttpStateError.INNER_ERROR     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            com.baidu.platform.comapi.pano.PanoStateError[] r2 = com.baidu.platform.comapi.pano.PanoStateError.values()
            int r2 = r2.length
            int[] r2 = new int[r2]
            com.baidu.mapapi.utils.poi.C1018b.f3270a = r2
            int[] r2 = com.baidu.mapapi.utils.poi.C1018b.f3270a     // Catch:{ NoSuchFieldError -> 0x0032 }
            com.baidu.platform.comapi.pano.PanoStateError r3 = com.baidu.platform.comapi.pano.PanoStateError.PANO_UID_ERROR     // Catch:{ NoSuchFieldError -> 0x0032 }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
            r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
        L_0x0032:
            int[] r0 = com.baidu.mapapi.utils.poi.C1018b.f3270a     // Catch:{ NoSuchFieldError -> 0x003c }
            com.baidu.platform.comapi.pano.PanoStateError r2 = com.baidu.platform.comapi.pano.PanoStateError.PANO_NOT_FOUND     // Catch:{ NoSuchFieldError -> 0x003c }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
            r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
        L_0x003c:
            int[] r0 = com.baidu.mapapi.utils.poi.C1018b.f3270a     // Catch:{ NoSuchFieldError -> 0x0047 }
            com.baidu.platform.comapi.pano.PanoStateError r1 = com.baidu.platform.comapi.pano.PanoStateError.PANO_NO_TOKEN     // Catch:{ NoSuchFieldError -> 0x0047 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0047 }
        L_0x0047:
            int[] r0 = com.baidu.mapapi.utils.poi.C1018b.f3270a     // Catch:{ NoSuchFieldError -> 0x0052 }
            com.baidu.platform.comapi.pano.PanoStateError r1 = com.baidu.platform.comapi.pano.PanoStateError.PANO_NO_ERROR     // Catch:{ NoSuchFieldError -> 0x0052 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
            r2 = 4
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0052 }
        L_0x0052:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.utils.poi.C1018b.<clinit>():void");
    }
}
