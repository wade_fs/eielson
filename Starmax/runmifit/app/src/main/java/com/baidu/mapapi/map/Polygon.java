package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;
import java.util.List;

public final class Polygon extends Overlay {

    /* renamed from: a */
    Stroke f2666a;

    /* renamed from: b */
    int f2667b;

    /* renamed from: c */
    List<LatLng> f2668c;

    Polygon() {
        this.type = C1082h.polygon;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        super.mo10900a(bundle);
        GeoPoint ll2mc = CoordUtil.ll2mc(this.f2668c.get(0));
        bundle.putDouble("location_x", ll2mc.getLongitudeE6());
        bundle.putDouble("location_y", ll2mc.getLatitudeE6());
        Overlay.m2940a(this.f2668c, bundle);
        Overlay.m2939a(this.f2667b, bundle);
        if (this.f2666a == null) {
            bundle.putInt("has_stroke", 0);
        } else {
            bundle.putInt("has_stroke", 1);
            bundle.putBundle("stroke", this.f2666a.mo11396a(new Bundle()));
        }
        return bundle;
    }

    public int getFillColor() {
        return this.f2667b;
    }

    public List<LatLng> getPoints() {
        return this.f2668c;
    }

    public Stroke getStroke() {
        return this.f2666a;
    }

    public void setFillColor(int i) {
        this.f2667b = i;
        this.listener.mo11330b(super);
    }

    public void setPoints(List<LatLng> list) {
        if (list == null) {
            throw new IllegalArgumentException("BDMapSDKException: points list can not be null");
        } else if (list.size() <= 2) {
            throw new IllegalArgumentException("BDMapSDKException: points count can not less than three");
        } else if (!list.contains(null)) {
            int i = 0;
            while (i < list.size()) {
                int i2 = i + 1;
                int i3 = i2;
                while (i3 < list.size()) {
                    if (list.get(i) != list.get(i3)) {
                        i3++;
                    } else {
                        throw new IllegalArgumentException("BDMapSDKException: points list can not has same points");
                    }
                }
                i = i2;
            }
            this.f2668c = list;
            this.listener.mo11330b(super);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: points list can not contains null");
        }
    }

    public void setStroke(Stroke stroke) {
        this.f2666a = stroke;
        this.listener.mo11330b(super);
    }
}
