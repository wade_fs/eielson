package com.baidu.android.bbalbs.common.p012a;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: com.baidu.android.bbalbs.common.a.d */
public final class C0694d {
    /* renamed from: a */
    public static byte[] m1544a(byte[] bArr) {
        try {
            return MessageDigest.getInstance("SHA-1").digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
