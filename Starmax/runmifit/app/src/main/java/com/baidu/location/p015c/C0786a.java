package com.baidu.location.p015c;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.support.v4.app.NotificationCompat;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.BDNotifyListener;
import com.baidu.location.Jni;
import com.baidu.location.LocationClient;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: com.baidu.location.c.a */
public class C0786a {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public ArrayList<BDNotifyListener> f1548a = null;

    /* renamed from: b */
    private float f1549b = Float.MAX_VALUE;

    /* renamed from: c */
    private BDLocation f1550c = null;

    /* renamed from: d */
    private long f1551d = 0;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public LocationClient f1552e = null;

    /* renamed from: f */
    private Context f1553f = null;

    /* renamed from: g */
    private int f1554g = 0;

    /* renamed from: h */
    private long f1555h = 0;

    /* renamed from: i */
    private boolean f1556i = false;

    /* renamed from: j */
    private PendingIntent f1557j = null;

    /* renamed from: k */
    private AlarmManager f1558k = null;

    /* renamed from: l */
    private C0787a f1559l = null;

    /* renamed from: m */
    private C0788b f1560m = new C0788b();

    /* renamed from: n */
    private boolean f1561n = false;

    /* renamed from: com.baidu.location.c.a$a */
    public class C0787a extends BroadcastReceiver {
        public C0787a() {
        }

        public void onReceive(Context context, Intent intent) {
            if (C0786a.this.f1548a != null && !C0786a.this.f1548a.isEmpty()) {
                C0786a.this.f1552e.requestNotifyLocation();
            }
        }
    }

    /* renamed from: com.baidu.location.c.a$b */
    public class C0788b implements BDLocationListener {
        public C0788b() {
        }

        public void onReceiveLocation(BDLocation bDLocation) {
            if (C0786a.this.f1548a != null && C0786a.this.f1548a.size() > 0) {
                C0786a.this.m2054a(bDLocation);
            }
        }
    }

    public C0786a(Context context, LocationClient locationClient) {
        this.f1553f = context;
        this.f1552e = locationClient;
        this.f1552e.registerNotifyLocationListener(this.f1560m);
        this.f1558k = (AlarmManager) this.f1553f.getSystemService(NotificationCompat.CATEGORY_ALARM);
        this.f1559l = new C0787a();
        this.f1561n = false;
    }

    /* renamed from: a */
    private void m2053a(long j) {
        try {
            if (this.f1557j != null) {
                this.f1558k.cancel(this.f1557j);
            }
            this.f1557j = PendingIntent.getBroadcast(this.f1553f, 0, new Intent("android.com.baidu.location.TIMER.NOTIFY"), 134217728);
            if (this.f1557j != null) {
                this.f1558k.set(0, System.currentTimeMillis() + j, this.f1557j);
            }
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2054a(BDLocation bDLocation) {
        BDLocation bDLocation2 = bDLocation;
        if (bDLocation.getLocType() != 61 && bDLocation.getLocType() != 161 && bDLocation.getLocType() != 65) {
            m2053a(120000);
        } else if (System.currentTimeMillis() - this.f1551d >= 5000 && this.f1548a != null) {
            this.f1550c = bDLocation2;
            this.f1551d = System.currentTimeMillis();
            float[] fArr = new float[1];
            Iterator<BDNotifyListener> it = this.f1548a.iterator();
            float f = Float.MAX_VALUE;
            while (it.hasNext()) {
                BDNotifyListener next = it.next();
                BDNotifyListener bDNotifyListener = next;
                Location.distanceBetween(bDLocation.getLatitude(), bDLocation.getLongitude(), next.mLatitudeC, next.mLongitudeC, fArr);
                float radius = (fArr[0] - bDNotifyListener.mRadius) - bDLocation.getRadius();
                if (radius > 0.0f) {
                    if (radius < f) {
                        f = radius;
                    }
                } else if (bDNotifyListener.Notified < 3) {
                    bDNotifyListener.Notified++;
                    bDNotifyListener.onNotify(bDLocation2, fArr[0]);
                    if (bDNotifyListener.Notified < 3) {
                        this.f1556i = true;
                    }
                }
            }
            if (f < this.f1549b) {
                this.f1549b = f;
            }
            this.f1554g = 0;
            m2058c();
        }
    }

    /* renamed from: b */
    private boolean m2057b() {
        ArrayList<BDNotifyListener> arrayList = this.f1548a;
        boolean z = false;
        if (arrayList != null && !arrayList.isEmpty()) {
            Iterator<BDNotifyListener> it = this.f1548a.iterator();
            while (it.hasNext()) {
                if (it.next().Notified < 3) {
                    z = true;
                }
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0045, code lost:
        if (((long) r0) > ((r7.f1555h + ((long) r1)) - java.lang.System.currentTimeMillis())) goto L_0x0049;
     */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2058c() {
        /*
            r7 = this;
            boolean r0 = r7.m2057b()
            if (r0 != 0) goto L_0x0007
            return
        L_0x0007:
            float r0 = r7.f1549b
            r1 = 1167867904(0x459c4000, float:5000.0)
            r2 = 10000(0x2710, float:1.4013E-41)
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0016
            r0 = 600000(0x927c0, float:8.40779E-40)
            goto L_0x002c
        L_0x0016:
            r1 = 1148846080(0x447a0000, float:1000.0)
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0020
            r0 = 120000(0x1d4c0, float:1.68156E-40)
            goto L_0x002c
        L_0x0020:
            r1 = 1140457472(0x43fa0000, float:500.0)
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x002a
            r0 = 60000(0xea60, float:8.4078E-41)
            goto L_0x002c
        L_0x002a:
            r0 = 10000(0x2710, float:1.4013E-41)
        L_0x002c:
            boolean r1 = r7.f1556i
            r3 = 0
            if (r1 == 0) goto L_0x0035
            r7.f1556i = r3
            r0 = 10000(0x2710, float:1.4013E-41)
        L_0x0035:
            int r1 = r7.f1554g
            if (r1 == 0) goto L_0x0048
            long r4 = r7.f1555h
            long r1 = (long) r1
            long r4 = r4 + r1
            long r1 = java.lang.System.currentTimeMillis()
            long r4 = r4 - r1
            long r1 = (long) r0
            int r6 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x0048
            goto L_0x0049
        L_0x0048:
            r3 = 1
        L_0x0049:
            if (r3 == 0) goto L_0x0059
            r7.f1554g = r0
            long r0 = java.lang.System.currentTimeMillis()
            r7.f1555h = r0
            int r0 = r7.f1554g
            long r0 = (long) r0
            r7.m2053a(r0)
        L_0x0059:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p015c.C0786a.m2058c():void");
    }

    /* renamed from: a */
    public int mo10547a(BDNotifyListener bDNotifyListener) {
        if (this.f1548a == null) {
            this.f1548a = new ArrayList<>();
        }
        this.f1548a.add(bDNotifyListener);
        bDNotifyListener.isAdded = true;
        bDNotifyListener.mNotifyCache = this;
        if (!this.f1561n) {
            this.f1553f.registerReceiver(this.f1559l, new IntentFilter("android.com.baidu.location.TIMER.NOTIFY"), "android.permission.ACCESS_FINE_LOCATION", null);
            this.f1561n = true;
        }
        if (bDNotifyListener.mCoorType == null) {
            return 1;
        }
        if (!bDNotifyListener.mCoorType.equals(CoordinateType.GCJ02)) {
            double[] coorEncrypt = Jni.coorEncrypt(bDNotifyListener.mLongitude, bDNotifyListener.mLatitude, bDNotifyListener.mCoorType + "2gcj");
            bDNotifyListener.mLongitudeC = coorEncrypt[0];
            bDNotifyListener.mLatitudeC = coorEncrypt[1];
        }
        if (this.f1550c == null || System.currentTimeMillis() - this.f1551d > 30000) {
            this.f1552e.requestNotifyLocation();
        } else {
            float[] fArr = new float[1];
            Location.distanceBetween(this.f1550c.getLatitude(), this.f1550c.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
            float radius = (fArr[0] - bDNotifyListener.mRadius) - this.f1550c.getRadius();
            if (radius > 0.0f) {
                if (radius < this.f1549b) {
                    this.f1549b = radius;
                }
            } else if (bDNotifyListener.Notified < 3) {
                bDNotifyListener.Notified++;
                bDNotifyListener.onNotify(this.f1550c, fArr[0]);
                if (bDNotifyListener.Notified < 3) {
                    this.f1556i = true;
                }
            }
        }
        m2058c();
        return 1;
    }

    /* renamed from: a */
    public void mo10548a() {
        PendingIntent pendingIntent = this.f1557j;
        if (pendingIntent != null) {
            this.f1558k.cancel(pendingIntent);
        }
        this.f1550c = null;
        this.f1551d = 0;
        if (this.f1561n) {
            this.f1553f.unregisterReceiver(this.f1559l);
        }
        this.f1561n = false;
    }

    /* renamed from: b */
    public void mo10549b(BDNotifyListener bDNotifyListener) {
        if (bDNotifyListener.mCoorType != null) {
            if (!bDNotifyListener.mCoorType.equals(CoordinateType.GCJ02)) {
                double[] coorEncrypt = Jni.coorEncrypt(bDNotifyListener.mLongitude, bDNotifyListener.mLatitude, bDNotifyListener.mCoorType + "2gcj");
                bDNotifyListener.mLongitudeC = coorEncrypt[0];
                bDNotifyListener.mLatitudeC = coorEncrypt[1];
            }
            if (this.f1550c == null || System.currentTimeMillis() - this.f1551d > 300000) {
                this.f1552e.requestNotifyLocation();
            } else {
                float[] fArr = new float[1];
                Location.distanceBetween(this.f1550c.getLatitude(), this.f1550c.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                float radius = (fArr[0] - bDNotifyListener.mRadius) - this.f1550c.getRadius();
                if (radius > 0.0f) {
                    if (radius < this.f1549b) {
                        this.f1549b = radius;
                    }
                } else if (bDNotifyListener.Notified < 3) {
                    bDNotifyListener.Notified++;
                    bDNotifyListener.onNotify(this.f1550c, fArr[0]);
                    if (bDNotifyListener.Notified < 3) {
                        this.f1556i = true;
                    }
                }
            }
            m2058c();
        }
    }

    /* renamed from: c */
    public int mo10550c(BDNotifyListener bDNotifyListener) {
        PendingIntent pendingIntent;
        ArrayList<BDNotifyListener> arrayList = this.f1548a;
        if (arrayList == null) {
            return 0;
        }
        if (arrayList.contains(bDNotifyListener)) {
            this.f1548a.remove(bDNotifyListener);
        }
        if (this.f1548a.size() != 0 || (pendingIntent = this.f1557j) == null) {
            return 1;
        }
        this.f1558k.cancel(pendingIntent);
        return 1;
    }
}
