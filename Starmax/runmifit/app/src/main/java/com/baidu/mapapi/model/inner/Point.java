package com.baidu.mapapi.model.inner;

import java.io.Serializable;

public class Point implements Serializable {

    /* renamed from: x */
    public int f2890x;

    /* renamed from: y */
    public int f2891y;

    public Point() {
    }

    public Point(int i, int i2) {
        this.f2890x = i;
        this.f2891y = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Point point = (Point) obj;
        return this.f2890x == point.f2890x && this.f2891y == point.f2891y;
    }

    public int getmPtx() {
        return this.f2890x;
    }

    public int getmPty() {
        return this.f2891y;
    }

    public int hashCode() {
        return ((this.f2890x + 31) * 31) + this.f2891y;
    }

    public void setmPtx(int i) {
        this.f2890x = i;
    }

    public void setmPty(int i) {
        this.f2891y = i;
    }

    public String toString() {
        return "Point [x=" + this.f2890x + ", y=" + this.f2891y + "]";
    }
}
