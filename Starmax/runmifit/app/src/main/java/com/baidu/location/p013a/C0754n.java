package com.baidu.location.p013a;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.baidu.location.C0839f;
import com.baidu.location.indoor.C0865g;
import com.baidu.location.indoor.mapversion.IndoorJni;

/* renamed from: com.baidu.location.a.n */
public class C0754n implements SensorEventListener {

    /* renamed from: d */
    private static C0754n f1407d;

    /* renamed from: a */
    private float[] f1408a;

    /* renamed from: b */
    private float[] f1409b;

    /* renamed from: c */
    private SensorManager f1410c;

    /* renamed from: e */
    private float f1411e;

    /* renamed from: f */
    private boolean f1412f = false;

    /* renamed from: g */
    private boolean f1413g = false;

    /* renamed from: h */
    private boolean f1414h = false;

    private C0754n() {
    }

    /* renamed from: a */
    public static synchronized C0754n m1911a() {
        C0754n nVar;
        synchronized (C0754n.class) {
            if (f1407d == null) {
                f1407d = new C0754n();
            }
            nVar = f1407d;
        }
        return nVar;
    }

    /* renamed from: a */
    public void mo10491a(boolean z) {
        this.f1412f = z;
    }

    /* renamed from: b */
    public synchronized void mo10492b() {
        if (!this.f1414h) {
            if (this.f1412f) {
                if (this.f1410c == null) {
                    this.f1410c = (SensorManager) C0839f.getServiceContext().getSystemService("sensor");
                }
                if (this.f1410c != null) {
                    Sensor defaultSensor = this.f1410c.getDefaultSensor(11);
                    if (defaultSensor != null && this.f1412f) {
                        this.f1410c.registerListener(this, defaultSensor, 3);
                    }
                    Sensor defaultSensor2 = this.f1410c.getDefaultSensor(2);
                    if (defaultSensor2 != null && this.f1412f) {
                        this.f1410c.registerListener(this, defaultSensor2, 3);
                    }
                }
                this.f1414h = true;
            }
        }
    }

    /* renamed from: b */
    public void mo10493b(boolean z) {
        this.f1413g = z;
    }

    /* renamed from: c */
    public synchronized void mo10494c() {
        if (this.f1414h) {
            if (this.f1410c != null) {
                this.f1410c.unregisterListener(this);
                this.f1410c = null;
            }
            this.f1414h = false;
        }
    }

    /* renamed from: d */
    public boolean mo10495d() {
        return this.f1412f;
    }

    /* renamed from: e */
    public float mo10496e() {
        return this.f1411e;
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        int type = sensorEvent.sensor.getType();
        if (type == 2) {
            this.f1409b = (float[]) sensorEvent.values.clone();
            float[] fArr = this.f1409b;
            double sqrt = Math.sqrt((double) ((fArr[0] * fArr[0]) + (fArr[1] * fArr[1]) + (fArr[2] * fArr[2])));
            if (this.f1409b != null) {
                try {
                    if (C0865g.m2519a().mo10768e()) {
                        IndoorJni.setPfGeomag(sqrt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (type == 11) {
            this.f1408a = (float[]) sensorEvent.values.clone();
            float[] fArr2 = this.f1408a;
            if (fArr2 != null) {
                float[] fArr3 = new float[9];
                try {
                    SensorManager.getRotationMatrixFromVector(fArr3, fArr2);
                    float[] fArr4 = new float[3];
                    SensorManager.getOrientation(fArr3, fArr4);
                    this.f1411e = (float) Math.toDegrees((double) fArr4[0]);
                    this.f1411e = (float) Math.floor((double) (this.f1411e >= 0.0f ? this.f1411e : this.f1411e + 360.0f));
                } catch (Exception unused) {
                    this.f1411e = 0.0f;
                }
            }
        }
    }
}
