package com.baidu.location.indoor;

import com.baidu.location.BDLocation;
import com.baidu.location.indoor.C0865g;
import com.baidu.location.indoor.C0881m;
import com.baidu.location.indoor.mapversion.p021b.C0895a;
import java.util.Date;

/* renamed from: com.baidu.location.indoor.i */
class C0877i implements C0881m.C0882a {

    /* renamed from: a */
    final /* synthetic */ C0865g f2150a;

    C0877i(C0865g gVar) {
        this.f2150a = gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, double):double
     arg types: [com.baidu.location.indoor.g, int]
     candidates:
      com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, int):int
      com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, long):long
      com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, com.baidu.location.e.h):java.lang.String
      com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, java.lang.String):java.lang.String
      com.baidu.location.indoor.g.a(com.baidu.location.BDLocation, int):void
      com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, android.os.Message):void
      com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, com.baidu.location.BDLocation):void
      com.baidu.location.indoor.g.a(java.lang.String, java.lang.String):void
      com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, boolean):boolean
      com.baidu.location.indoor.g.a(double, double):boolean
      com.baidu.location.indoor.g.a(android.location.Location, java.util.ArrayList<java.util.ArrayList<java.lang.Float>>):boolean
      com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, double):double */
    /* renamed from: a */
    public synchronized void mo10780a(double d, double d2, double d3, long j) {
        if (this.f2150a.f2064n) {
            double unused = this.f2150a.f2028J = 0.4d;
            this.f2150a.f2050ae.m2598a(d, d2, d3, j);
            double[] a = C0895a.m2737a(this.f2150a.f2073w, d, d2, d3);
            if (a != null) {
                if (a[0] != -1.0d) {
                    if (a[0] == 0.0d) {
                        double unused2 = this.f2150a.f2027I = a[2];
                        double unused3 = this.f2150a.f2026H = a[1];
                        if (this.f2150a.f2031M.size() > 50) {
                            this.f2150a.f2031M.clear();
                        }
                        this.f2150a.f2031M.add(new C0865g.C0874h(this.f2150a.f2060j.mo10787d(), d, d3, d2));
                        C0865g.m2549g(this.f2150a);
                        try {
                            BDLocation bDLocation = new BDLocation();
                            bDLocation.setLocType(BDLocation.TypeNetWorkLocation);
                            bDLocation.setLatitude(a[2]);
                            bDLocation.setLongitude(a[1]);
                            bDLocation.setDirection((float) d3);
                            bDLocation.setTime(this.f2150a.f2053b.format(new Date()));
                            bDLocation.setFloor(this.f2150a.f2073w);
                            bDLocation.setBuildingID(this.f2150a.f2074x);
                            bDLocation.setBuildingName(this.f2150a.f2076z);
                            bDLocation.setParkAvailable(this.f2150a.f2021C);
                            bDLocation.setIndoorLocMode(true);
                            if (this.f2150a.f2038T) {
                                bDLocation.setRadius(8.0f);
                            } else {
                                bDLocation.setRadius(15.0f);
                            }
                            bDLocation.setFusionLocInfo("res", a);
                            bDLocation.setRadius((float) a[5]);
                            bDLocation.setDirection((float) a[6]);
                            bDLocation.setSpeed((float) a[8]);
                            bDLocation.setNetworkLocationType("dr");
                            BDLocation bDLocation2 = new BDLocation(bDLocation);
                            bDLocation2.setNetworkLocationType("dr2");
                            if (this.f2150a.f2039U == null || !this.f2150a.f2039U.mo10841c()) {
                                this.f2150a.m2526a(bDLocation2, 21);
                            } else {
                                this.f2150a.f2039U.mo10839a(bDLocation2);
                            }
                            if (!this.f2150a.f2050ae.m2603a(bDLocation, a[5], "dr")) {
                                this.f2150a.mo10767d();
                            }
                        } catch (Exception unused4) {
                        }
                    }
                }
            }
        }
    }
}
