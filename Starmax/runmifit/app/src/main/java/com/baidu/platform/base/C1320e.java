package com.baidu.platform.base;

import android.util.Log;
import com.baidu.mapapi.http.HttpClient;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.Point;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapsdkplatform.comapi.util.PermissionCheck;
import com.baidu.mapsdkplatform.comjni.util.AppMD5;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.domain.C1380d;
import com.baidu.platform.util.C1381a;
import org.apache.commons.math3.geometry.VectorFormat;

/* renamed from: com.baidu.platform.base.e */
public abstract class C1320e {

    /* renamed from: a */
    protected C1381a f4435a = new C1381a();

    /* renamed from: b */
    private boolean f4436b = true;

    /* renamed from: c */
    private boolean f4437c = true;

    /* renamed from: a */
    public String mo14023a() {
        String a = mo14025a(C1380d.m5193a());
        String authToken = HttpClient.getAuthToken();
        if (authToken == null) {
            Log.e("SearchRequest", "toUrlString get authtoken failed");
            int permissionCheck = PermissionCheck.permissionCheck();
            if (permissionCheck != 0) {
                Log.e("SearchRequest", "try permissionCheck result is: " + permissionCheck);
                return null;
            }
            authToken = HttpClient.getAuthToken();
        }
        if (this.f4436b) {
            this.f4435a.mo14094a("token", authToken);
        }
        String str = this.f4435a.mo14095a() + HttpClient.getPhoneInfo();
        if (this.f4437c) {
            str = str + "&sign=" + AppMD5.getSignMD5String(str);
        }
        return a + "?" + str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final String mo14024a(PlanNode planNode) {
        StringBuilder sb;
        if (planNode == null) {
            return null;
        }
        String str = new String(VectorFormat.DEFAULT_PREFIX);
        LatLng location = planNode.getLocation();
        if (location != null) {
            String str2 = str + "\"type\":1,";
            Point ll2point = CoordUtil.ll2point(location);
            sb = new StringBuilder();
            sb.append(str2);
            sb.append("\"xy\":\"");
            sb.append(ll2point.f2890x);
            sb.append(",");
            sb.append(ll2point.f2891y);
        } else if (planNode.getName() == null) {
            return str;
        } else {
            sb = new StringBuilder();
            sb.append(str + "\"type\":2,");
            sb.append("\"keyword\":\"");
            sb.append(planNode.getName());
        }
        sb.append("\"}");
        return sb.toString();
    }

    /* renamed from: a */
    public abstract String mo14025a(C1379c cVar);

    /* renamed from: a */
    public void mo14026a(boolean z) {
        this.f4437c = z;
    }

    /* renamed from: b */
    public void mo14027b(boolean z) {
        this.f4436b = z;
    }
}
