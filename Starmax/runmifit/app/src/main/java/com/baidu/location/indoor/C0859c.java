package com.baidu.location.indoor;

import java.util.ArrayList;

/* renamed from: com.baidu.location.indoor.c */
public class C0859c<T> extends ArrayList<T> {

    /* renamed from: a */
    private int f1997a = 0;

    public C0859c(int i) {
        this.f1997a = i;
    }

    public boolean add(T t) {
        synchronized (this) {
            if (size() == this.f1997a) {
                remove(0);
            }
            add(size(), t);
        }
        return true;
    }

    public void clear() {
        synchronized (this) {
            if (size() > 3) {
                int size = size() / 2;
                while (true) {
                    int i = size - 1;
                    if (size > 0) {
                        remove(0);
                        size = i;
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
