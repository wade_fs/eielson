package com.baidu.location.indoor.mapversion.p020a;

import com.baidu.mobstat.Config;
import org.json.JSONObject;

/* renamed from: com.baidu.location.indoor.mapversion.a.c */
class C0891c {

    /* renamed from: a */
    private String f2234a;

    /* renamed from: b */
    private String f2235b;

    /* renamed from: c */
    private String f2236c;

    /* renamed from: d */
    private String f2237d;

    /* renamed from: e */
    private String f2238e;

    /* renamed from: f */
    private String f2239f;

    /* renamed from: g */
    private String f2240g;

    /* renamed from: h */
    private double f2241h;

    /* renamed from: i */
    private double f2242i;

    public C0891c() {
    }

    public C0891c(JSONObject jSONObject) {
        this.f2234a = jSONObject.optString("bldg");
        this.f2235b = jSONObject.optString("guid");
        this.f2236c = jSONObject.optString("building_bid");
        this.f2237d = jSONObject.optString("poi_guid");
        this.f2238e = jSONObject.optString("poi_bid");
        this.f2239f = jSONObject.optString(Config.FEED_LIST_NAME);
        this.f2240g = jSONObject.optString("floor");
        this.f2241h = jSONObject.optDouble(Config.EVENT_HEAT_X);
        this.f2242i = jSONObject.optDouble("y");
    }

    /* renamed from: a */
    public static String m2711a(String str) {
        return str.toLowerCase().replaceAll("[^a-zA-Z0-9]+", "");
    }

    /* renamed from: a */
    public String mo10803a() {
        return this.f2234a;
    }

    /* renamed from: b */
    public String mo10804b() {
        return this.f2236c;
    }

    /* renamed from: c */
    public String mo10805c() {
        return this.f2239f;
    }

    /* renamed from: d */
    public String mo10806d() {
        return this.f2240g;
    }

    /* renamed from: e */
    public double mo10807e() {
        return this.f2241h;
    }

    /* renamed from: f */
    public double mo10808f() {
        return this.f2242i;
    }
}
