package com.baidu.mobstat;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.baidu.mobstat.C1262ay;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.download.MimeType;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.GZIPOutputStream;
import org.json.JSONObject;

public class LogSender {

    /* renamed from: a */
    private static LogSender f4175a = new LogSender();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public boolean f4176b = false;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public int f4177c = 0;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public int f4178d = 1;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public SendStrategyEnum f4179e = SendStrategyEnum.APP_START;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public Timer f4180f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public Handler f4181g;

    public static LogSender instance() {
        return f4175a;
    }

    private LogSender() {
        HandlerThread handlerThread = new HandlerThread("LogSenderThread");
        handlerThread.start();
        this.f4181g = new Handler(handlerThread.getLooper());
    }

    public void setLogSenderDelayed(int i) {
        if (i >= 0 && i <= 30) {
            this.f4177c = i;
        }
    }

    public void setSendLogStrategy(Context context, SendStrategyEnum sendStrategyEnum, int i, boolean z) {
        if (!sendStrategyEnum.equals(SendStrategyEnum.SET_TIME_INTERVAL)) {
            this.f4179e = sendStrategyEnum;
            BasicStoreTools.getInstance().setSendStrategy(context, this.f4179e.ordinal());
            if (sendStrategyEnum.equals(SendStrategyEnum.ONCE_A_DAY)) {
                BasicStoreTools.getInstance().setSendStrategyTime(context, 24);
            }
        } else if (i > 0 && i <= 24) {
            this.f4178d = i;
            this.f4179e = SendStrategyEnum.SET_TIME_INTERVAL;
            BasicStoreTools.getInstance().setSendStrategy(context, this.f4179e.ordinal());
            BasicStoreTools.getInstance().setSendStrategyTime(context, this.f4178d);
        }
        this.f4176b = z;
        BasicStoreTools.getInstance().setOnlyWifi(context, this.f4176b);
    }

    public void onSend(final Context context) {
        if (context != null) {
            context = context.getApplicationContext();
        }
        if (context != null) {
            this.f4181g.post(new Runnable() {
                /* class com.baidu.mobstat.LogSender.C12151 */

                public void run() {
                    if (LogSender.this.f4180f != null) {
                        LogSender.this.f4180f.cancel();
                        Timer unused = LogSender.this.f4180f = (Timer) null;
                    }
                    SendStrategyEnum unused2 = LogSender.this.f4179e = SendStrategyEnum.values()[BasicStoreTools.getInstance().getSendStrategy(context)];
                    int unused3 = LogSender.this.f4178d = BasicStoreTools.getInstance().getSendStrategyTime(context);
                    boolean unused4 = LogSender.this.f4176b = BasicStoreTools.getInstance().getOnlyWifiChannel(context);
                    if (LogSender.this.f4179e.equals(SendStrategyEnum.SET_TIME_INTERVAL)) {
                        LogSender.this.setSendingLogTimer(context);
                    } else if (LogSender.this.f4179e.equals(SendStrategyEnum.ONCE_A_DAY)) {
                        LogSender.this.setSendingLogTimer(context);
                    }
                    LogSender.this.f4181g.postDelayed(new Runnable() {
                        /* class com.baidu.mobstat.LogSender.C12151.C12161 */

                        public void run() {
                            LogSender.this.m4449a(context);
                        }
                    }, (long) (LogSender.this.f4177c * 1000));
                }
            });
        }
    }

    public void setSendingLogTimer(Context context) {
        final Context applicationContext = context.getApplicationContext();
        long j = (long) (this.f4178d * 3600000);
        this.f4180f = new Timer();
        this.f4180f.schedule(new TimerTask() {
            /* class com.baidu.mobstat.LogSender.C12172 */

            public void run() {
                LogSender.this.m4449a(applicationContext);
            }
        }, j, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    public void saveLogData(Context context, String str, boolean z) {
        String str2 = z ? Config.PREFIX_SEND_DATA_FULL : Config.PREFIX_SEND_DATA;
        C1267ba.m4681a(context, str2 + System.currentTimeMillis(), str, false);
        if (z) {
            m4450a(context, (long) Config.FULL_TRACE_LOG_LIMIT, Config.PREFIX_SEND_DATA_FULL);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        if (r2 == null) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0021, code lost:
        if (r2 != null) goto L_0x002e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003d A[LOOP:1: B:22:0x003b->B:23:0x003d, LOOP_END] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m4450a(android.content.Context r8, long r9, java.lang.String r11) {
        /*
            r7 = this;
            java.util.ArrayList r11 = r7.m4445a(r8, r11)
            int r0 = r11.size()
            int r0 = r0 + -1
            r1 = 0
            r2 = 0
            r3 = r2
            r2 = r1
        L_0x000f:
            if (r0 < 0) goto L_0x003a
            java.lang.Object r5 = r11.get(r0)     // Catch:{ Exception -> 0x002b, all -> 0x0024 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x002b, all -> 0x0024 }
            java.io.FileInputStream r2 = r8.openFileInput(r5)     // Catch:{ Exception -> 0x002b, all -> 0x0024 }
            int r5 = r2.available()     // Catch:{ Exception -> 0x002b, all -> 0x0024 }
            long r5 = (long) r5
            long r3 = r3 + r5
            if (r2 == 0) goto L_0x0032
            goto L_0x002e
        L_0x0024:
            r8 = move-exception
            if (r2 == 0) goto L_0x002a
            r2.close()     // Catch:{ Exception -> 0x002a }
        L_0x002a:
            throw r8
        L_0x002b:
            if (r2 == 0) goto L_0x0032
        L_0x002e:
            r2.close()     // Catch:{ Exception -> 0x0031 }
        L_0x0031:
            r2 = r1
        L_0x0032:
            int r5 = (r3 > r9 ? 1 : (r3 == r9 ? 0 : -1))
            if (r5 <= 0) goto L_0x0037
            goto L_0x003a
        L_0x0037:
            int r0 = r0 + -1
            goto L_0x000f
        L_0x003a:
            r9 = 0
        L_0x003b:
            if (r9 > r0) goto L_0x0049
            java.lang.Object r10 = r11.get(r9)
            java.lang.String r10 = (java.lang.String) r10
            com.baidu.mobstat.C1267ba.m4685b(r8, r10)
            int r9 = r9 + 1
            goto L_0x003b
        L_0x0049:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.LogSender.m4450a(android.content.Context, long, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public ArrayList<String> m4445a(Context context, final String str) {
        File filesDir;
        ArrayList<String> arrayList = new ArrayList<>();
        if (!(context == null || (filesDir = context.getFilesDir()) == null || !filesDir.exists())) {
            C12183 r1 = new FilenameFilter() {
                /* class com.baidu.mobstat.LogSender.C12183 */

                public boolean accept(File file, String str) {
                    return str.startsWith(str);
                }
            };
            String[] strArr = null;
            try {
                strArr = filesDir.list(r1);
            } catch (Exception unused) {
            }
            if (!(strArr == null || strArr.length == 0)) {
                try {
                    Arrays.sort(strArr, new Comparator<String>() {
                        /* class com.baidu.mobstat.LogSender.C12194 */

                        /* renamed from: a */
                        public int compare(String str, String str2) {
                            return str.compareTo(str2);
                        }
                    });
                } catch (Exception unused2) {
                }
                for (String str2 : strArr) {
                    arrayList.add(str2);
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m4449a(final Context context) {
        if (!this.f4176b || C1276bh.m4743p(context)) {
            this.f4181g.post(new Runnable() {
                /* class com.baidu.mobstat.LogSender.C12205 */

                public void run() {
                    try {
                        ArrayList arrayList = new ArrayList();
                        arrayList.addAll(LogSender.this.m4445a(context, Config.PREFIX_SEND_DATA));
                        arrayList.addAll(LogSender.this.m4445a(context, Config.PREFIX_SEND_DATA_FULL));
                        Iterator it = arrayList.iterator();
                        while (true) {
                            int i = 0;
                            while (it.hasNext()) {
                                String str = (String) it.next();
                                String a = C1267ba.m4679a(context, str);
                                if (TextUtils.isEmpty(a)) {
                                    C1267ba.m4685b(context, str);
                                } else {
                                    if (LogSender.this.m4453a(context, a, str.contains(Config.PREFIX_SEND_DATA_FULL))) {
                                        C1267ba.m4685b(context, str);
                                    } else {
                                        LogSender.m4458b(context, str, a);
                                        i++;
                                        if (i >= 5) {
                                            return;
                                        }
                                    }
                                }
                            }
                            return;
                        }
                    } catch (Exception unused) {
                    }
                }
            });
        }
    }

    public void sendLogData(Context context, final String str, boolean z) {
        if (context != null && !TextUtils.isEmpty(str)) {
            final Context applicationContext = context.getApplicationContext();
            if (z) {
                m4457b(applicationContext, str);
            } else {
                this.f4181g.post(new Runnable() {
                    /* class com.baidu.mobstat.LogSender.C12216 */

                    public void run() {
                        LogSender.this.m4457b(applicationContext, str);
                    }
                });
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m4457b(Context context, String str) {
        String str2 = Config.PREFIX_SEND_DATA + System.currentTimeMillis();
        C1267ba.m4681a(context, str2, str, false);
        if (m4462c(context, str)) {
            C1267ba.m4685b(context, str2);
        } else {
            m4458b(context, str2, str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m4458b(Context context, String str, String str2) {
        JSONObject jSONObject;
        try {
            jSONObject = new JSONObject(str2);
        } catch (Exception unused) {
            jSONObject = null;
        }
        if (jSONObject != null) {
            try {
                JSONObject jSONObject2 = (JSONObject) jSONObject.get(Config.TRACE_PART);
                jSONObject2.put(Config.TRACE_FAILED_CNT, jSONObject2.getLong(Config.TRACE_FAILED_CNT) + 1);
            } catch (Exception unused2) {
            }
            C1267ba.m4681a(context, str, jSONObject.toString(), false);
        }
    }

    public void sendEmptyLogData(Context context, final String str) {
        final Context applicationContext = context.getApplicationContext();
        this.f4181g.post(new Runnable() {
            /* class com.baidu.mobstat.LogSender.C12227 */

            public void run() {
                String constructLogWithEmptyBody = DataCore.instance().constructLogWithEmptyBody(applicationContext, str);
                if (!TextUtils.isEmpty(constructLogWithEmptyBody)) {
                    boolean unused = LogSender.this.m4462c(applicationContext, constructLogWithEmptyBody);
                }
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.LogSender.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.baidu.mobstat.LogSender.a(com.baidu.mobstat.LogSender, android.content.Context, java.lang.String):java.util.ArrayList
      com.baidu.mobstat.LogSender.a(android.content.Context, long, java.lang.String):void
      com.baidu.mobstat.LogSender.a(android.content.Context, java.lang.String, java.lang.String):void
      com.baidu.mobstat.LogSender.a(android.content.Context, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    /* renamed from: c */
    public boolean m4462c(Context context, String str) {
        return m4453a(context, str, false);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m4453a(Context context, String str, boolean z) {
        if (!z) {
            C1256at.m4629c().mo13941a("Start send log \n" + str);
        }
        boolean z2 = false;
        if (!this.f4176b || C1276bh.m4743p(context)) {
            String str2 = Config.LOG_SEND_URL;
            if (z) {
                str2 = Config.LOG_FULL_SEND_URL;
            }
            try {
                m4461c(context, str2, str);
                z2 = true;
            } catch (Exception e) {
                C1256at.m4629c().mo13947c(e);
            }
            if (!z) {
                String str3 = z2 ? "success" : "failed";
                C1256at.m4629c().mo13941a("Send log " + str3);
            }
            return z2;
        }
        C1256at.m4629c().mo13941a("[WARNING] wifi not available, log will be cached, next time will try to resend");
        return false;
    }

    /* renamed from: c */
    private String m4461c(Context context, String str, String str2) throws Exception {
        if (!str.startsWith("https://")) {
            return m4466e(context, str, str2);
        }
        return m4465d(context, str, str2);
    }

    /* renamed from: d */
    private String m4465d(Context context, String str, String str2) throws IOException {
        HttpURLConnection d = C1267ba.m4688d(context, str);
        d.setDoOutput(true);
        d.setInstanceFollowRedirects(false);
        d.setUseCaches(false);
        d.setRequestProperty(HttpHeaders.CONTENT_TYPE, MimeType.GZIP);
        d.connect();
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(d.getOutputStream())));
            bufferedWriter.write(str2);
            bufferedWriter.flush();
            bufferedWriter.close();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(d.getInputStream()));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            int contentLength = d.getContentLength();
            if (d.getResponseCode() == 200 && contentLength == 0) {
                return sb.toString();
            }
            throw new IOException("http code = " + d.getResponseCode() + "; contentResponse = " + ((Object) sb));
        } finally {
            d.disconnect();
        }
    }

    /* renamed from: e */
    private String m4466e(Context context, String str, String str2) throws Exception {
        HttpURLConnection d = C1267ba.m4688d(context, str);
        d.setDoOutput(true);
        d.setInstanceFollowRedirects(false);
        d.setUseCaches(false);
        d.setRequestProperty(HttpHeaders.CONTENT_TYPE, MimeType.GZIP);
        byte[] a = C1262ay.C1263a.m4666a();
        byte[] b = C1262ay.C1263a.m4669b();
        d.setRequestProperty("key", C1275bg.m4706a(a));
        d.setRequestProperty("iv", C1275bg.m4706a(b));
        byte[] a2 = C1262ay.C1263a.m4667a(a, b, str2.getBytes("utf-8"));
        d.connect();
        try {
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(d.getOutputStream());
            gZIPOutputStream.write(a2);
            gZIPOutputStream.flush();
            gZIPOutputStream.close();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(d.getInputStream()));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            int contentLength = d.getContentLength();
            if (d.getResponseCode() == 200 && contentLength == 0) {
                return sb.toString();
            }
            throw new IOException("http code = " + d.getResponseCode() + "; contentResponse = " + ((Object) sb));
        } finally {
            d.disconnect();
        }
    }
}
