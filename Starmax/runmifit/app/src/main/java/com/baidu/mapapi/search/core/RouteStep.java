package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;
import java.util.ArrayList;
import java.util.List;

public class RouteStep implements Parcelable {
    public static final Parcelable.Creator<RouteStep> CREATOR = new C0967k();

    /* renamed from: a */
    int f2939a;

    /* renamed from: b */
    int f2940b;

    /* renamed from: c */
    String f2941c;
    protected List<LatLng> mWayPoints;

    protected RouteStep() {
    }

    protected RouteStep(Parcel parcel) {
        this.f2939a = parcel.readInt();
        this.f2940b = parcel.readInt();
        this.f2941c = parcel.readString();
        this.mWayPoints = new ArrayList();
        parcel.readList(this.mWayPoints, LatLng.class.getClassLoader());
        if (this.mWayPoints.size() == 0) {
            this.mWayPoints = null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public int getDistance() {
        return this.f2939a;
    }

    public int getDuration() {
        return this.f2940b;
    }

    public String getName() {
        return this.f2941c;
    }

    public List<LatLng> getWayPoints() {
        return this.mWayPoints;
    }

    public void setDistance(int i) {
        this.f2939a = i;
    }

    public void setDuration(int i) {
        this.f2940b = i;
    }

    public void setName(String str) {
        this.f2941c = str;
    }

    public void setWayPoints(List<LatLng> list) {
        this.mWayPoints = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f2939a);
        parcel.writeInt(this.f2940b);
        parcel.writeString(this.f2941c);
        parcel.writeList(this.mWayPoints);
    }
}
