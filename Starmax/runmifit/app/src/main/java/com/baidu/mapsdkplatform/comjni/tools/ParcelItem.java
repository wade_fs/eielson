package com.baidu.mapsdkplatform.comjni.tools;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class ParcelItem implements Parcelable {
    public static final Parcelable.Creator<ParcelItem> CREATOR = new C1183b();

    /* renamed from: a */
    private Bundle f3951a;

    public int describeContents() {
        return 0;
    }

    public Bundle getBundle() {
        return this.f3951a;
    }

    public void setBundle(Bundle bundle) {
        this.f3951a = bundle;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f3951a);
    }
}
