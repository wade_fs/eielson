package com.baidu.mapapi.model;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.model.a */
final class C0952a implements Parcelable.Creator<LatLng> {
    C0952a() {
    }

    /* renamed from: a */
    public LatLng createFromParcel(Parcel parcel) {
        return new LatLng(parcel);
    }

    /* renamed from: a */
    public LatLng[] newArray(int i) {
        return new LatLng[i];
    }
}
