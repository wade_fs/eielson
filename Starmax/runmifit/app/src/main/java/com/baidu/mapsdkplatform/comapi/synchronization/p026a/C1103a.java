package com.baidu.mapsdkplatform.comapi.synchronization.p026a;

import android.content.Context;
import android.view.View;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.synchronization.DisplayOptions;
import com.baidu.mapapi.synchronization.RoleOptions;
import com.baidu.mapapi.synchronization.SyncCoordinateConverter;
import com.baidu.mapapi.synchronization.SynchronizationConstants;
import com.baidu.mapapi.synchronization.SynchronizationDisplayListener;
import com.baidu.mapsdkplatform.comapi.synchronization.data.C1141k;
import com.baidu.mapsdkplatform.comapi.synchronization.data.C1142l;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import com.baidu.mapsdkplatform.comapi.synchronization.render.C1153d;
import com.baidu.mapsdkplatform.comapi.synchronization.render.C1154e;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.a.a */
public class C1103a implements C1141k, C1153d {

    /* renamed from: a */
    private static final String f3632a = C1103a.class.getSimpleName();

    /* renamed from: b */
    private C1142l f3633b = null;

    /* renamed from: c */
    private C1154e f3634c = null;

    /* renamed from: d */
    private SynchronizationDisplayListener f3635d;

    public C1103a(Context context, BaiduMap baiduMap, RoleOptions roleOptions, DisplayOptions displayOptions) {
        if (context == null) {
            throw new IllegalArgumentException("BDMapSDKException: Context invalid, please check!");
        } else if (baiduMap == null || !(baiduMap instanceof BaiduMap)) {
            throw new IllegalArgumentException("BDMapSDKException: BaiduMap is null or invalid, please check!");
        } else if (m3740b(roleOptions)) {
            this.f3633b = new C1142l(roleOptions, displayOptions);
            this.f3633b.mo13317a(this);
            this.f3634c = new C1154e(context, baiduMap);
            this.f3634c.mo13360a(this);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: RoleOptions is invalid, please check!");
        }
    }

    /* renamed from: a */
    private boolean m3738a(LatLng latLng, RoleOptions roleOptions) {
        double d;
        LatLng latLng2 = latLng;
        if (latLng2 == null) {
            return false;
        }
        double d2 = -180.0d;
        double d3 = 180.0d;
        double d4 = -90.0d;
        double d5 = 90.0d;
        double d6 = 0.0d;
        if (SyncCoordinateConverter.CoordType.COMMON == roleOptions.getCoordType()) {
            SyncCoordinateConverter syncCoordinateConverter = new SyncCoordinateConverter();
            LatLng convert = syncCoordinateConverter.from(roleOptions.getCoordType()).coord(new LatLng(-90.0d, -180.0d)).convert();
            double d7 = convert.longitude;
            double d8 = convert.latitude;
            LatLng convert2 = syncCoordinateConverter.from(roleOptions.getCoordType()).coord(new LatLng(90.0d, 180.0d)).convert();
            double d9 = convert2.longitude;
            double d10 = convert2.latitude;
            LatLng convert3 = syncCoordinateConverter.from(roleOptions.getCoordType()).coord(new LatLng(0.0d, 0.0d)).convert();
            d6 = convert3.longitude;
            d = convert3.latitude;
            double d11 = d10;
            d3 = d9;
            d5 = d11;
            double d12 = d8;
            d2 = d7;
            d4 = d12;
        } else {
            d = 0.0d;
        }
        return !(Double.valueOf(d6).compareTo(Double.valueOf(latLng2.longitude)) == 0 && Double.valueOf(d).compareTo(Double.valueOf(latLng2.latitude)) == 0) && latLng2.longitude >= d2 && latLng2.longitude <= d3 && latLng2.latitude >= d4 && latLng2.latitude <= d5;
    }

    /* renamed from: a */
    private boolean m3739a(SyncCoordinateConverter.CoordType coordType) {
        return SyncCoordinateConverter.CoordType.BD09LL == coordType || SyncCoordinateConverter.CoordType.COMMON == coordType;
    }

    /* renamed from: b */
    private boolean m3740b(RoleOptions roleOptions) {
        if (roleOptions != null && roleOptions.getOrderId() != null && !roleOptions.getOrderId().equals("") && roleOptions.getRoleType() == 0 && roleOptions.getDriverId() != null && !roleOptions.getDriverId().equals("") && roleOptions.getUserId() != null && !roleOptions.getUserId().equals("") && m3739a(roleOptions.getCoordType()) && m3738a(roleOptions.getStartPosition(), roleOptions)) {
            return true;
        }
        if (roleOptions == null) {
            C1120a.m3855b(f3632a, "The roleOptions is null");
            return false;
        }
        String str = f3632a;
        C1120a.m3855b(str, "The roleOptions content is: OrderId = " + roleOptions.getOrderId() + "; DriverId = " + roleOptions.getDriverId() + "; UserId = " + roleOptions.getUserId() + "; StartPosition = " + roleOptions.getStartPosition() + "; EndPosition = " + roleOptions.getEndPosition() + "; DriverPosition = " + roleOptions.getDriverPosition() + "; CoordType = " + roleOptions.getCoordType());
        return false;
    }

    /* renamed from: e */
    private boolean m3741e(int i) {
        return i >= 0 && i <= 5;
    }

    /* renamed from: a */
    public void mo13149a() {
        C1120a.m3851a(f3632a, "onResume");
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13312a();
        }
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13357a();
        }
    }

    /* renamed from: a */
    public void mo13150a(float f, long j) {
        SynchronizationDisplayListener synchronizationDisplayListener = this.f3635d;
        if (synchronizationDisplayListener != null) {
            synchronizationDisplayListener.onRoutePlanInfoFreshFinished(f, j);
        }
    }

    /* renamed from: a */
    public void mo13151a(int i) {
        String str = f3632a;
        C1120a.m3856c(str, "The order state = " + i);
        if (!m3741e(i)) {
            SynchronizationDisplayListener synchronizationDisplayListener = this.f3635d;
            if (synchronizationDisplayListener != null) {
                synchronizationDisplayListener.onSynchronizationProcessResult(1002, SynchronizationConstants.LBS_STATUS_MESSAGE_ORDER_STATE_INVALID);
            }
            i = 0;
        }
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13358a(i);
        }
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13313a(i);
        }
    }

    /* renamed from: a */
    public void mo13152a(int i, int i2, int i3, int i4) {
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13359a(i, i2, i3, i4);
        }
    }

    /* renamed from: a */
    public void mo13153a(int i, String str) {
        SynchronizationDisplayListener synchronizationDisplayListener = this.f3635d;
        if (synchronizationDisplayListener != null) {
            synchronizationDisplayListener.onSynchronizationProcessResult(i, str);
        }
    }

    /* renamed from: a */
    public void mo13154a(View view) {
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13314a(view);
        }
    }

    /* renamed from: a */
    public void mo13155a(DisplayOptions displayOptions) {
        C1142l lVar = this.f3633b;
        if (lVar != null && displayOptions != null) {
            lVar.mo13315a(displayOptions);
        }
    }

    /* renamed from: a */
    public void mo13156a(RoleOptions roleOptions) {
        if (roleOptions == null || !m3740b(roleOptions)) {
            SynchronizationDisplayListener synchronizationDisplayListener = this.f3635d;
            if (synchronizationDisplayListener != null) {
                synchronizationDisplayListener.onSynchronizationProcessResult(1003, SynchronizationConstants.LBS_STATUS_MESSAGE_ORDER_PARAM_INVALID);
                return;
            }
            return;
        }
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13316a(roleOptions);
        }
    }

    /* renamed from: a */
    public void mo13157a(SynchronizationDisplayListener synchronizationDisplayListener) {
        if (synchronizationDisplayListener != null) {
            this.f3635d = synchronizationDisplayListener;
        } else {
            C1120a.m3855b(f3632a, "SynchronizationDisplayListener is null, must be applied.");
            throw new IllegalArgumentException("BDMapSDKException: synchronizationDisplayListener is null");
        }
    }

    /* renamed from: a */
    public void mo13158a(boolean z) {
        C1115c.f3681b = z;
    }

    /* renamed from: b */
    public void mo13159b() {
        C1120a.m3851a(f3632a, "onPause");
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13318b();
        }
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13361b();
        }
    }

    /* renamed from: b */
    public void mo13160b(int i) {
        if (i < 2) {
            i = 2;
        }
        if (i > 30) {
            i = 30;
        }
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13319b(i);
        }
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13362b(i);
        }
    }

    /* renamed from: b */
    public void mo13161b(int i, String str) {
        SynchronizationDisplayListener synchronizationDisplayListener = this.f3635d;
        if (synchronizationDisplayListener != null) {
            synchronizationDisplayListener.onSynchronizationProcessResult(i, str);
        }
    }

    /* renamed from: b */
    public void mo13162b(View view) {
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13320b(view);
        }
    }

    /* renamed from: b */
    public void mo13163b(SynchronizationDisplayListener synchronizationDisplayListener) {
        if (this.f3635d != null) {
            this.f3635d = null;
        }
    }

    /* renamed from: c */
    public void mo13164c() {
        C1120a.m3851a(f3632a, "release");
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13321c();
        }
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13368f();
        }
        if (this.f3635d != null) {
            this.f3635d = null;
        }
    }

    /* renamed from: c */
    public void mo13165c(int i) {
        if (i < 10) {
            i = 10;
        }
        if (i > 30) {
            i = 30;
        }
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13364c(i);
        }
    }

    /* renamed from: c */
    public void mo13166c(int i, String str) {
        SynchronizationDisplayListener synchronizationDisplayListener = this.f3635d;
        if (synchronizationDisplayListener != null) {
            synchronizationDisplayListener.onSynchronizationProcessResult(i, str);
        }
    }

    /* renamed from: c */
    public void mo13167c(View view) {
        C1142l lVar = this.f3633b;
        if (lVar != null) {
            lVar.mo13322c(view);
        }
    }

    /* renamed from: d */
    public Marker mo13168d() {
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            return eVar.mo13363c();
        }
        C1120a.m3855b(f3632a, "Data manager instance is null");
        return null;
    }

    /* renamed from: d */
    public void mo13169d(int i) {
        if (i < 5) {
            i = 5;
        }
        if (i > 30) {
            i = 30;
        }
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13366d(i);
        }
    }

    /* renamed from: e */
    public Marker mo13170e() {
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            return eVar.mo13365d();
        }
        C1120a.m3855b(f3632a, "Data manager instance is null");
        return null;
    }

    /* renamed from: f */
    public Marker mo13171f() {
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            return eVar.mo13367e();
        }
        C1120a.m3855b(f3632a, "Data manager instance is null");
        return null;
    }

    /* renamed from: g */
    public void mo13172g() {
        C1154e eVar = this.f3634c;
        if (eVar != null) {
            eVar.mo13369g();
        }
    }

    /* renamed from: h */
    public boolean mo13173h() {
        return C1115c.f3681b;
    }
}
