package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.b */
class C1030b implements Animator.AnimatorListener {

    /* renamed from: a */
    final /* synthetic */ C1029a f3313a;

    C1030b(C1029a aVar) {
        this.f3313a = aVar;
    }

    public void onAnimationCancel(Animator animator) {
        if (this.f3313a.f3309d != null) {
            this.f3313a.f3309d.onAnimationCancel();
        }
    }

    public void onAnimationEnd(Animator animator) {
        if (this.f3313a.f3309d != null) {
            this.f3313a.f3309d.onAnimationEnd();
        }
    }

    public void onAnimationRepeat(Animator animator) {
        if (this.f3313a.f3309d != null) {
            this.f3313a.f3309d.onAnimationRepeat();
        }
    }

    public void onAnimationStart(Animator animator) {
        if (this.f3313a.f3309d != null) {
            this.f3313a.f3309d.onAnimationStart();
        }
    }
}
