package com.baidu.platform.core.p033c;

import android.text.TextUtils;
import com.baidu.mapapi.search.poi.PoiBoundSearchOption;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiNearbySearchOption;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;
import com.google.android.gms.actions.SearchIntents;
import com.tamic.novate.download.MimeType;
import com.tencent.connect.common.Constants;

/* renamed from: com.baidu.platform.core.c.i */
public class C1348i extends C1320e {
    C1348i(PoiBoundSearchOption poiBoundSearchOption) {
        m5019a(poiBoundSearchOption);
    }

    C1348i(PoiCitySearchOption poiCitySearchOption) {
        m5020a(poiCitySearchOption);
    }

    C1348i(PoiNearbySearchOption poiNearbySearchOption) {
        m5021a(poiNearbySearchOption);
    }

    /* renamed from: a */
    private void m5019a(PoiBoundSearchOption poiBoundSearchOption) {
        this.f4435a.mo14094a(SearchIntents.EXTRA_QUERY, poiBoundSearchOption.mKeyword);
        this.f4435a.mo14094a("tag", poiBoundSearchOption.mTag);
        C1381a aVar = this.f4435a;
        aVar.mo14094a("bounds", poiBoundSearchOption.mBound.southwest.latitude + "," + poiBoundSearchOption.mBound.southwest.longitude + "," + poiBoundSearchOption.mBound.northeast.latitude + "," + poiBoundSearchOption.mBound.northeast.longitude);
        this.f4435a.mo14094a("output", MimeType.JSON);
        C1381a aVar2 = this.f4435a;
        StringBuilder sb = new StringBuilder();
        sb.append(poiBoundSearchOption.mScope);
        sb.append("");
        aVar2.mo14094a(Constants.PARAM_SCOPE, sb.toString());
        C1381a aVar3 = this.f4435a;
        aVar3.mo14094a("page_num", poiBoundSearchOption.mPageNum + "");
        C1381a aVar4 = this.f4435a;
        aVar4.mo14094a("page_size", poiBoundSearchOption.mPageCapacity + "");
        if (poiBoundSearchOption.mScope == 2 && poiBoundSearchOption.mPoiFilter != null && !TextUtils.isEmpty(poiBoundSearchOption.mPoiFilter.toString())) {
            this.f4435a.mo14094a("filter", poiBoundSearchOption.mPoiFilter.toString());
        }
    }

    /* renamed from: a */
    private void m5020a(PoiCitySearchOption poiCitySearchOption) {
        String str;
        C1381a aVar;
        this.f4435a.mo14094a(SearchIntents.EXTRA_QUERY, poiCitySearchOption.mKeyword);
        this.f4435a.mo14094a("region", poiCitySearchOption.mCity);
        this.f4435a.mo14094a("output", MimeType.JSON);
        C1381a aVar2 = this.f4435a;
        aVar2.mo14094a("page_num", poiCitySearchOption.mPageNum + "");
        C1381a aVar3 = this.f4435a;
        aVar3.mo14094a("page_size", poiCitySearchOption.mPageCapacity + "");
        C1381a aVar4 = this.f4435a;
        aVar4.mo14094a(Constants.PARAM_SCOPE, poiCitySearchOption.mScope + "");
        this.f4435a.mo14094a("tag", poiCitySearchOption.mTag);
        if (poiCitySearchOption.mIsCityLimit) {
            aVar = this.f4435a;
            str = "true";
        } else {
            aVar = this.f4435a;
            str = "false";
        }
        aVar.mo14094a("city_limit", str);
        if (poiCitySearchOption.mScope == 2 && poiCitySearchOption.mPoiFilter != null && !TextUtils.isEmpty(poiCitySearchOption.mPoiFilter.toString())) {
            this.f4435a.mo14094a("filter", poiCitySearchOption.mPoiFilter.toString());
        }
    }

    /* renamed from: a */
    private void m5021a(PoiNearbySearchOption poiNearbySearchOption) {
        String str;
        C1381a aVar;
        this.f4435a.mo14094a(SearchIntents.EXTRA_QUERY, poiNearbySearchOption.mKeyword);
        C1381a aVar2 = this.f4435a;
        aVar2.mo14094a("location", poiNearbySearchOption.mLocation.latitude + "," + poiNearbySearchOption.mLocation.longitude);
        C1381a aVar3 = this.f4435a;
        aVar3.mo14094a("radius", poiNearbySearchOption.mRadius + "");
        this.f4435a.mo14094a("output", MimeType.JSON);
        C1381a aVar4 = this.f4435a;
        aVar4.mo14094a("page_num", poiNearbySearchOption.mPageNum + "");
        C1381a aVar5 = this.f4435a;
        aVar5.mo14094a("page_size", poiNearbySearchOption.mPageCapacity + "");
        C1381a aVar6 = this.f4435a;
        aVar6.mo14094a(Constants.PARAM_SCOPE, poiNearbySearchOption.mScope + "");
        this.f4435a.mo14094a("tag", poiNearbySearchOption.mTag);
        if (poiNearbySearchOption.mRadiusLimit) {
            aVar = this.f4435a;
            str = "true";
        } else {
            aVar = this.f4435a;
            str = "false";
        }
        aVar.mo14094a("radius_limit", str);
        if (poiNearbySearchOption.mScope == 2 && poiNearbySearchOption.mPoiFilter != null && !TextUtils.isEmpty(poiNearbySearchOption.mPoiFilter.toString())) {
            this.f4435a.mo14094a("filter", poiNearbySearchOption.mPoiFilter.toString());
        }
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14076a();
    }
}
