package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.RouteNode;
import com.baidu.mapapi.search.core.RouteStep;
import com.baidu.mapapi.search.core.TaxiInfo;
import com.baidu.mapapi.search.core.VehicleInfo;
import java.util.List;

public final class TransitRouteLine extends RouteLine<TransitStep> implements Parcelable {
    public static final Parcelable.Creator<TransitRouteLine> CREATOR = new C0999o();

    /* renamed from: b */
    private TaxiInfo f3105b;

    public static class TransitStep extends RouteStep implements Parcelable {
        public static final Parcelable.Creator<TransitStep> CREATOR = new C1000p();

        /* renamed from: d */
        private VehicleInfo f3106d;

        /* renamed from: e */
        private RouteNode f3107e;

        /* renamed from: f */
        private RouteNode f3108f;

        /* renamed from: g */
        private TransitRouteStepType f3109g;

        /* renamed from: h */
        private String f3110h;

        /* renamed from: i */
        private String f3111i;

        public enum TransitRouteStepType {
            BUSLINE,
            SUBWAY,
            WAKLING
        }

        public TransitStep() {
        }

        protected TransitStep(Parcel parcel) {
            super(parcel);
            this.f3106d = (VehicleInfo) parcel.readParcelable(VehicleInfo.class.getClassLoader());
            this.f3107e = (RouteNode) parcel.readParcelable(RouteNode.class.getClassLoader());
            this.f3108f = (RouteNode) parcel.readParcelable(RouteNode.class.getClassLoader());
            int readInt = parcel.readInt();
            this.f3109g = readInt == -1 ? null : TransitRouteStepType.values()[readInt];
            this.f3110h = parcel.readString();
            this.f3111i = parcel.readString();
        }

        public int describeContents() {
            return 0;
        }

        public RouteNode getEntrance() {
            return this.f3107e;
        }

        public RouteNode getExit() {
            return this.f3108f;
        }

        public String getInstructions() {
            return this.f3110h;
        }

        public TransitRouteStepType getStepType() {
            return this.f3109g;
        }

        public VehicleInfo getVehicleInfo() {
            return this.f3106d;
        }

        public List<LatLng> getWayPoints() {
            if (this.mWayPoints == null) {
                this.mWayPoints = CoordUtil.decodeLocationList(this.f3111i);
            }
            return this.mWayPoints;
        }

        public void setEntrace(RouteNode routeNode) {
            this.f3107e = routeNode;
        }

        public void setExit(RouteNode routeNode) {
            this.f3108f = routeNode;
        }

        public void setInstructions(String str) {
            this.f3110h = str;
        }

        public void setPathString(String str) {
            this.f3111i = str;
        }

        public void setStepType(TransitRouteStepType transitRouteStepType) {
            this.f3109g = transitRouteStepType;
        }

        public void setVehicleInfo(VehicleInfo vehicleInfo) {
            this.f3106d = vehicleInfo;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeParcelable(this.f3106d, 1);
            parcel.writeParcelable(this.f3107e, 1);
            parcel.writeParcelable(this.f3108f, 1);
            TransitRouteStepType transitRouteStepType = this.f3109g;
            parcel.writeInt(transitRouteStepType == null ? -1 : transitRouteStepType.ordinal());
            parcel.writeString(this.f3110h);
            parcel.writeString(this.f3111i);
        }
    }

    public TransitRouteLine() {
    }

    protected TransitRouteLine(Parcel parcel) {
        super(parcel);
        this.f3105b = (TaxiInfo) parcel.readParcelable(TaxiInfo.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    @Deprecated
    public TaxiInfo getTaxitInfo() {
        return this.f3105b;
    }

    public void setTaxitInfo(TaxiInfo taxiInfo) {
        this.f3105b = taxiInfo;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.setType(RouteLine.TYPE.TRANSITSTEP);
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.f3105b, 1);
    }
}
