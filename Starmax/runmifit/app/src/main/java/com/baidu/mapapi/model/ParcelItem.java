package com.baidu.mapapi.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class ParcelItem implements Parcelable {
    public static final Parcelable.Creator<ParcelItem> CREATOR = new C0954c();

    /* renamed from: a */
    private Bundle f2887a;

    public int describeContents() {
        return 0;
    }

    public Bundle getBundle() {
        return this.f2887a;
    }

    public void setBundle(Bundle bundle) {
        this.f2887a = bundle;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.f2887a);
    }
}
