package com.baidu.location.p019g;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import com.baidu.location.C0839f;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/* renamed from: com.baidu.location.g.e */
public abstract class C0849e {

    /* renamed from: a */
    private static String f1878a = "10.0.0.172";

    /* renamed from: b */
    private static int f1879b = 80;

    /* renamed from: g */
    public static int f1880g = C0843a.f1832g;

    /* renamed from: p */
    protected static int f1881p = 0;

    /* renamed from: h */
    public String f1882h = null;

    /* renamed from: i */
    public int f1883i = 1;

    /* renamed from: j */
    public String f1884j = null;

    /* renamed from: k */
    public Map<String, Object> f1885k = null;

    /* renamed from: l */
    public String f1886l = null;

    /* renamed from: m */
    public byte[] f1887m = null;

    /* renamed from: n */
    public byte[] f1888n = null;

    /* renamed from: o */
    public String f1889o = null;

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00ac, code lost:
        if (r4.equals(r5.trim()) != false) goto L_0x004e;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int m2439a(android.content.Context r4, android.net.NetworkInfo r5) {
        /*
            java.lang.String r4 = "10.0.0.200"
            java.lang.String r0 = "10.0.0.172"
            if (r5 == 0) goto L_0x008b
            java.lang.String r1 = r5.getExtraInfo()
            if (r1 == 0) goto L_0x008b
            java.lang.String r5 = r5.getExtraInfo()
            java.lang.String r5 = r5.toLowerCase()
            if (r5 == 0) goto L_0x008b
            java.lang.String r1 = "cmwap"
            boolean r1 = r5.startsWith(r1)
            java.lang.String r2 = "null"
            java.lang.String r3 = ""
            if (r1 != 0) goto L_0x0076
            java.lang.String r1 = "uniwap"
            boolean r1 = r5.startsWith(r1)
            if (r1 != 0) goto L_0x0076
            java.lang.String r1 = "3gwap"
            boolean r1 = r5.startsWith(r1)
            if (r1 == 0) goto L_0x0033
            goto L_0x0076
        L_0x0033:
            java.lang.String r1 = "ctwap"
            boolean r1 = r5.startsWith(r1)
            if (r1 == 0) goto L_0x0053
            java.lang.String r5 = android.net.Proxy.getDefaultHost()
            if (r5 == 0) goto L_0x004e
            boolean r0 = r5.equals(r3)
            if (r0 != 0) goto L_0x004e
            boolean r0 = r5.equals(r2)
            if (r0 != 0) goto L_0x004e
            r4 = r5
        L_0x004e:
            com.baidu.location.p019g.C0849e.f1878a = r4
        L_0x0050:
            int r4 = com.baidu.location.p019g.C0843a.f1829d
            return r4
        L_0x0053:
            java.lang.String r1 = "cmnet"
            boolean r1 = r5.startsWith(r1)
            if (r1 != 0) goto L_0x0073
            java.lang.String r1 = "uninet"
            boolean r1 = r5.startsWith(r1)
            if (r1 != 0) goto L_0x0073
            java.lang.String r1 = "ctnet"
            boolean r1 = r5.startsWith(r1)
            if (r1 != 0) goto L_0x0073
            java.lang.String r1 = "3gnet"
            boolean r5 = r5.startsWith(r1)
            if (r5 == 0) goto L_0x008b
        L_0x0073:
            int r4 = com.baidu.location.p019g.C0843a.f1830e
            return r4
        L_0x0076:
            java.lang.String r4 = android.net.Proxy.getDefaultHost()
            if (r4 == 0) goto L_0x0089
            boolean r5 = r4.equals(r3)
            if (r5 != 0) goto L_0x0089
            boolean r5 = r4.equals(r2)
            if (r5 != 0) goto L_0x0089
            goto L_0x004e
        L_0x0089:
            r4 = r0
            goto L_0x004e
        L_0x008b:
            java.lang.String r5 = android.net.Proxy.getDefaultHost()
            if (r5 == 0) goto L_0x00af
            int r1 = r5.length()
            if (r1 <= 0) goto L_0x00af
            java.lang.String r1 = r5.trim()
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00a4
            com.baidu.location.p019g.C0849e.f1878a = r0
            goto L_0x0050
        L_0x00a4:
            java.lang.String r5 = r5.trim()
            boolean r5 = r4.equals(r5)
            if (r5 == 0) goto L_0x00af
            goto L_0x004e
        L_0x00af:
            int r4 = com.baidu.location.p019g.C0843a.f1830e
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p019g.C0849e.m2439a(android.content.Context, android.net.NetworkInfo):int");
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void mo10508b() {
        f1880g = mo10499c();
    }

    /* renamed from: c */
    private int mo10499c() {
        Context serviceContext = C0839f.getServiceContext();
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) serviceContext.getSystemService("connectivity");
            if (connectivityManager == null) {
                return C0843a.f1832g;
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.isAvailable()) {
                    if (activeNetworkInfo.getType() != 1) {
                        return m2439a(serviceContext, activeNetworkInfo);
                    }
                    String defaultHost = Proxy.getDefaultHost();
                    return (defaultHost == null || defaultHost.length() <= 0) ? C0843a.f1831f : C0843a.f1833h;
                }
            }
            return C0843a.f1832g;
        } catch (Exception unused) {
            return C0843a.f1832g;
        }
    }

    /* renamed from: a */
    public abstract void mo10444a();

    /* renamed from: a */
    public void mo10730a(ExecutorService executorService) {
        try {
            executorService.execute(new C0850f(this));
        } catch (Throwable unused) {
            mo10446a(false);
        }
    }

    /* renamed from: a */
    public void mo10731a(ExecutorService executorService, String str) {
        try {
            executorService.execute(new C0853i(this, str));
        } catch (Throwable unused) {
            mo10446a(false);
        }
    }

    /* renamed from: a */
    public void mo10732a(ExecutorService executorService, boolean z, String str) {
        try {
            executorService.execute(new C0851g(this, str, z));
        } catch (Throwable unused) {
            mo10446a(false);
        }
    }

    /* renamed from: a */
    public abstract void mo10446a(boolean z);

    /* renamed from: b */
    public void mo10733b(ExecutorService executorService) {
        mo10732a(executorService, false, "loc.map.baidu.com");
    }

    /* renamed from: c */
    public void mo10734c(String str) {
        try {
            new C0852h(this, str).start();
        } catch (Throwable unused) {
            mo10446a(false);
        }
    }
}
