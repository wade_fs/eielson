package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

public class CoachInfo extends TransitBaseInfo {
    public static final Parcelable.Creator<CoachInfo> CREATOR = new C0959c();

    /* renamed from: a */
    private double f2916a;

    /* renamed from: b */
    private String f2917b;

    /* renamed from: c */
    private String f2918c;

    /* renamed from: d */
    private String f2919d;

    public CoachInfo() {
    }

    protected CoachInfo(Parcel parcel) {
        super(parcel);
        this.f2916a = parcel.readDouble();
        this.f2917b = parcel.readString();
        this.f2918c = parcel.readString();
        this.f2919d = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getBooking() {
        return this.f2917b;
    }

    public double getPrice() {
        return this.f2916a;
    }

    public String getProviderName() {
        return this.f2918c;
    }

    public String getProviderUrl() {
        return this.f2919d;
    }

    public void setBooking(String str) {
        this.f2917b = str;
    }

    public void setPrice(double d) {
        this.f2916a = d;
    }

    public void setProviderName(String str) {
        this.f2918c = str;
    }

    public void setProviderUrl(String str) {
        this.f2919d = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeDouble(this.f2916a);
        parcel.writeString(this.f2917b);
        parcel.writeString(this.f2918c);
        parcel.writeString(this.f2919d);
    }
}
