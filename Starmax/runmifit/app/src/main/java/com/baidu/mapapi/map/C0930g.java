package com.baidu.mapapi.map;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.map.g */
final class C0930g implements Parcelable.Creator<BaiduMapOptions> {
    C0930g() {
    }

    /* renamed from: a */
    public BaiduMapOptions createFromParcel(Parcel parcel) {
        return new BaiduMapOptions(parcel);
    }

    /* renamed from: a */
    public BaiduMapOptions[] newArray(int i) {
        return new BaiduMapOptions[i];
    }
}
