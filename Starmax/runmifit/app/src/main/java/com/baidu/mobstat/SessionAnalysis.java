package com.baidu.mobstat;

import android.content.Context;
import android.text.TextUtils;
import com.baidu.mobstat.Session;
import com.runmifit.android.app.Constants;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class SessionAnalysis {

    /* renamed from: a */
    private boolean f4220a = false;

    /* renamed from: b */
    private Map<String, C1224a> f4221b = new HashMap();

    /* renamed from: c */
    private C1224a f4222c = new C1224a();

    /* renamed from: d */
    private C1224a f4223d = new C1224a();

    /* renamed from: e */
    private boolean f4224e = false;

    /* renamed from: f */
    private long f4225f = 0;

    /* renamed from: g */
    private Session f4226g = new Session();

    /* renamed from: h */
    private int f4227h = 0;

    /* renamed from: i */
    private int f4228i = 0;

    /* renamed from: j */
    private long f4229j = 0;
    public Callback mCallback;

    public interface Callback {
        void onCallback(JSONObject jSONObject);
    }

    public SessionAnalysis() {
    }

    public SessionAnalysis(Callback callback) {
        this.mCallback = callback;
    }

    public void setSessionTimeOut(int i) {
        if (i < 1) {
            i = 30;
            C1256at.m4629c().mo13943b("[WARNING] SessionTimeout should be between 1 and 600. Default value[30] is used");
        } else if (i > 600) {
            C1256at.m4629c().mo13943b("[WARNING] SessionTimeout should be between 1 and 600. Default value[600] is used");
            i = Constants.DEFAULT_WEIGHT_KG;
        }
        this.f4227h = i * 1000;
    }

    public int getSessionTimeOut() {
        if (this.f4227h <= 0) {
            this.f4227h = 30000;
        }
        return this.f4227h;
    }

    public long getSessionStartTime() {
        return this.f4226g.getStartTime();
    }

    public JSONObject getPageSessionHead() {
        return this.f4226g.getPageSessionHead();
    }

    public int getSessionSize() {
        return this.f4228i;
    }

    public void doSendLogCheck(Context context, long j) {
        long j2 = this.f4225f;
        if (j2 > 0 && j - j2 > ((long) getSessionTimeOut())) {
            m4482a(context, -1, false, false);
        }
    }

    public void autoTrackSessionStartTime(Context context, long j) {
        if (context != null) {
            this.f4226g.setTrackStartTime(j);
            this.f4229j = j;
        }
    }

    public void autoTrackSessionEndTime(Context context, long j) {
        if (context != null) {
            this.f4226g.setTrackEndTime(j);
            m4480a(context);
        }
    }

    public void onSessionStart(Context context, long j, boolean z) {
        if (!this.f4220a) {
            DataCore.instance().init(context);
            m4482a(context, j, z, true);
            this.f4220a = true;
        }
    }

    public void onPageStart(Context context, String str, int i, long j) {
        C1224a a;
        onSessionStart(context, j, false);
        if (!TextUtils.isEmpty(str) && (a = m4479a(str)) != null) {
            if (a.f4232c) {
                C1256at c = C1256at.m4629c();
                c.mo13946c("[WARNING] 遗漏StatService.onPageEnd(), 请检查邻近页面埋点: " + str);
            }
            if (!this.f4224e) {
                m4481a(context, this.f4225f, j, i, 3);
                this.f4224e = true;
            }
            a.f4232c = true;
            a.f4231b = j;
        }
    }

    public void onPageEnd(Context context, String str, String str2, String str3, long j, ExtraInfo extraInfo, boolean z) {
        C1224a a;
        String str4 = str;
        this.f4224e = false;
        if (TextUtils.isEmpty(str) || (a = m4479a(str4)) == null) {
            return;
        }
        if (!a.f4232c) {
            C1256at c = C1256at.m4629c();
            c.mo13946c("[WARNING] 遗漏StatService.onPageStart(), 请检查邻近页面埋点: " + str4);
            return;
        }
        m4483a(context, a.f4230a, str, a.f4231b, j, str2, "", str3, false, extraInfo, z);
        m4484b(str4);
        this.f4225f = j;
    }

    public void onPageStartAct(Context context, String str, long j, boolean z) {
        onSessionStart(context, j, false);
        if (!TextUtils.isEmpty(str)) {
            C1224a aVar = z ? this.f4223d : this.f4222c;
            if (aVar.f4232c && !z) {
                C1256at c = C1256at.m4629c();
                c.mo13946c("[WARNING] 遗漏StatService.onPause(Activity), 请检查邻近页面埋点: " + str);
            }
            if (!this.f4224e) {
                m4481a(context, this.f4225f, j, 1, 1);
                this.f4224e = true;
            }
            aVar.f4232c = true;
            aVar.f4230a = str;
            aVar.f4231b = j;
        }
    }

    public void onPageEndAct(Context context, String str, String str2, String str3, long j, boolean z, ExtraInfo extraInfo) {
        this.f4224e = false;
        C1224a aVar = z ? this.f4223d : this.f4222c;
        if (aVar.f4232c) {
            m4483a(context, aVar.f4230a, str, aVar.f4231b, j, str2, str3, str, z, extraInfo, false);
            aVar.f4232c = false;
            this.f4225f = j;
        } else if (!z) {
            C1256at c = C1256at.m4629c();
            c.mo13946c("[WARNING] 遗漏StatService.onResume(Activity), 请检查邻近页面埋点: " + str);
        }
    }

    public void onPageStartFrag(Context context, String str, long j) {
        onSessionStart(context, j, false);
        if (!TextUtils.isEmpty(str)) {
            C1224a a = m4479a(str);
            if (a.f4232c) {
                C1256at c = C1256at.m4629c();
                c.mo13946c("[WARNING] 遗漏StatService.onPause(Fragment), 请检查邻近页面埋点: " + str);
            }
            m4481a(context, this.f4225f, j, 2, 2);
            a.f4232c = true;
            a.f4230a = str;
            a.f4231b = j;
        }
    }

    public void onPageEndFrag(Context context, String str, String str2, String str3, long j) {
        C1224a a;
        String str4 = str;
        if (TextUtils.isEmpty(str) || (a = m4479a(str4)) == null) {
            return;
        }
        if (!a.f4232c) {
            C1256at c = C1256at.m4629c();
            c.mo13946c("[WARNING] 遗漏StatService.onResume(Fragment), 请检查邻近页面埋点: " + str4);
            return;
        }
        m4483a(context, a.f4230a, str, a.f4231b, j, str2, str3, null, false, null, false);
        m4484b(str4);
        this.f4225f = j;
    }

    /* renamed from: a */
    private void m4481a(Context context, long j, long j2, int i, int i2) {
        if (j2 - j > ((long) getSessionTimeOut())) {
            if (j > 0) {
                if (2 == i2) {
                    this.f4226g.setEndTime(j);
                }
                m4482a(context, j2, false, false);
            }
            this.f4226g.setTrackStartTime(this.f4229j);
            this.f4226g.setInvokeType(i);
        }
    }

    /* renamed from: a */
    private void m4483a(Context context, String str, String str2, long j, long j2, String str3, String str4, String str5, boolean z, ExtraInfo extraInfo, boolean z2) {
        long j3 = j2;
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && str.equals(str2)) {
            this.f4226g.addPageView(new Session.C1223a(str3, str4, str5, j3 - j, j, z, extraInfo, z2));
            this.f4226g.setEndTime(j3);
            m4480a(context);
        }
    }

    /* renamed from: a */
    private C1224a m4479a(String str) {
        if (!this.f4221b.containsKey(str)) {
            this.f4221b.put(str, new C1224a(str));
        }
        return this.f4221b.get(str);
    }

    /* renamed from: a */
    private void m4482a(Context context, long j, boolean z, boolean z2) {
        long j2;
        if (this.f4226g.hasEnd()) {
            DataCore.instance().putSession(this.f4226g);
            DataCore.instance().flush(context);
            C1248ao.m4591a(this.f4226g.getPageSessionHead());
            this.f4226g.setEndTime(0);
        }
        boolean z3 = j > 0;
        if (z3) {
            j2 = j;
        } else {
            j2 = this.f4226g.getStartTime();
        }
        if (z3) {
            this.f4226g.reset();
            this.f4226g.setStartTime(j);
        }
        DataCore.instance().saveLogData(context, z3, z, j2, z2);
        Callback callback = this.mCallback;
        if (callback != null) {
            callback.onCallback(DataCore.instance().getLogData());
        }
        LogSender.instance().onSend(context);
        clearLastSessionCache(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    /* renamed from: a */
    private void m4480a(Context context) {
        if (this.f4226g.hasStart()) {
            String jSONObject = this.f4226g.constructJSONObject().toString();
            this.f4228i = jSONObject.getBytes().length;
            String s = C1276bh.m4746s(context);
            C1267ba.m4681a(context, s + Config.LAST_SESSION_FILE_NAME, jSONObject, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    public void clearLastSessionCache(Context context) {
        if (context != null) {
            String jSONObject = new JSONObject().toString();
            String s = C1276bh.m4746s(context);
            C1267ba.m4681a(context, s + Config.LAST_SESSION_FILE_NAME, jSONObject, false);
        }
    }

    /* renamed from: b */
    private void m4484b(String str) {
        if (!TextUtils.isEmpty(str) && this.f4221b.containsKey(str)) {
            this.f4221b.remove(str);
        }
    }

    public boolean isSessionStart() {
        return this.f4226g.getStartTime() > 0;
    }

    /* renamed from: com.baidu.mobstat.SessionAnalysis$a */
    static class C1224a {

        /* renamed from: a */
        String f4230a;

        /* renamed from: b */
        long f4231b;

        /* renamed from: c */
        boolean f4232c = false;

        public C1224a() {
        }

        public C1224a(String str) {
            this.f4230a = str;
        }
    }
}
