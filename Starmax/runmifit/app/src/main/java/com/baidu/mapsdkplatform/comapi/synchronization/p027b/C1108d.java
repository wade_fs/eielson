package com.baidu.mapsdkplatform.comapi.synchronization.p027b;

import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceQueryOptions;
import com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1105b;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1118e;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.b.d */
class C1108d extends C1118e {

    /* renamed from: a */
    final /* synthetic */ int f3658a;

    /* renamed from: b */
    final /* synthetic */ HistoryTraceQueryOptions f3659b;

    /* renamed from: c */
    final /* synthetic */ String f3660c;

    /* renamed from: d */
    final /* synthetic */ C1105b.C1106a f3661d;

    C1108d(C1105b.C1106a aVar, int i, HistoryTraceQueryOptions historyTraceQueryOptions, String str) {
        this.f3661d = aVar;
        this.f3658a = i;
        this.f3659b = historyTraceQueryOptions;
        this.f3660c = str;
    }

    /* renamed from: a */
    public void mo13186a(C1115c.C1116a aVar) {
        if ((C1115c.C1116a.SERVER_ERROR == aVar || C1115c.C1116a.NETWORK_ERROR == aVar || C1115c.C1116a.INNER_ERROR == aVar || C1115c.C1116a.REQUEST_ERROR == aVar) && C1105b.f3651g <= 2) {
            this.f3661d.m3801a(this.f3660c, this.f3658a, this.f3659b);
            int unused = C1105b.f3651g = C1105b.f3651g + 1;
        }
        if (2 < C1105b.f3651g) {
            this.f3661d.m3800a(aVar, this.f3658a);
        }
    }

    /* renamed from: a */
    public void mo13187a(String str) {
        String c = C1105b.f3647a;
        C1120a.m3855b(c, "Request success, the result = " + str);
        this.f3661d.m3795a(3, str, this.f3658a, this.f3659b);
        int unused = C1105b.f3651g = 0;
    }
}
