package com.baidu.platform.core.p034d;

import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.google.android.gms.dynamite.ProviderConstants;
import com.tamic.novate.download.MimeType;
import com.tencent.connect.common.Constants;
import java.util.List;

/* renamed from: com.baidu.platform.core.d.d */
public class C1352d extends C1320e {
    C1352d(DrivingRoutePlanOption drivingRoutePlanOption) {
        m5043a(drivingRoutePlanOption);
    }

    /* renamed from: a */
    private void m5043a(DrivingRoutePlanOption drivingRoutePlanOption) {
        this.f4435a.mo14094a("qt", "cars");
        this.f4435a.mo14094a("sy", drivingRoutePlanOption.mPolicy.getInt() + "");
        this.f4435a.mo14094a("ie", "utf-8");
        this.f4435a.mo14094a("lrn", "20");
        this.f4435a.mo14094a(ProviderConstants.API_COLNAME_FEATURE_VERSION, Constants.VIA_SHARE_TYPE_INFO);
        this.f4435a.mo14094a("extinfo", "32");
        this.f4435a.mo14094a("mrs", "1");
        this.f4435a.mo14094a("rp_format", MimeType.JSON);
        this.f4435a.mo14094a("rp_filter", "mobile");
        this.f4435a.mo14094a("route_traffic", drivingRoutePlanOption.mtrafficPolicy.getInt() + "");
        this.f4435a.mo14094a("sn", mo14024a(drivingRoutePlanOption.mFrom));
        this.f4435a.mo14094a("en", mo14024a(drivingRoutePlanOption.mTo));
        if (drivingRoutePlanOption.mCityName != null) {
            this.f4435a.mo14094a("c", drivingRoutePlanOption.mCityName);
        }
        if (drivingRoutePlanOption.mFrom != null) {
            this.f4435a.mo14094a(Config.STAT_SDK_CHANNEL, drivingRoutePlanOption.mFrom.getCity());
        }
        if (drivingRoutePlanOption.mTo != null) {
            this.f4435a.mo14094a("ec", drivingRoutePlanOption.mTo.getCity());
        }
        List<PlanNode> list = drivingRoutePlanOption.mWayPoints;
        String str = new String();
        String str2 = new String();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                PlanNode planNode = list.get(i);
                if (planNode != null) {
                    str = str + mo14024a(planNode);
                    str2 = str2 + planNode.getCity();
                    if (i != list.size() - 1) {
                        str2 = str2 + "|";
                        str = str + "|";
                    }
                }
            }
            this.f4435a.mo14094a("wp", str);
            this.f4435a.mo14094a("wpc", str2);
        }
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14084i();
    }
}
