package com.baidu.mapapi.utils;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.common.AppTools;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapapi.navi.NaviParaOption;
import com.baidu.mapapi.utils.poi.DispathcPoiData;
import com.baidu.mapapi.utils.poi.PoiParaOption;
import com.baidu.mapapi.utils.route.RouteParaOption;
import com.baidu.mapframework.open.aidl.C1021a;
import com.baidu.mapframework.open.aidl.IComOpenClient;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.mobstat.Config;
import com.baidu.platform.comapi.p030a.C1321a;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.android.gms.common.util.CrashUtils;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapapi.utils.b */
public class C1012b {

    /* renamed from: a */
    public static int f3237a = -1;

    /* renamed from: b */
    static ServiceConnection f3238b = new C1014d();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static final String f3239c = C1012b.class.getName();
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static C1021a f3240d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static IComOpenClient f3241e;

    /* renamed from: f */
    private static int f3242f;

    /* renamed from: g */
    private static String f3243g = null;

    /* renamed from: h */
    private static String f3244h = null;

    /* renamed from: i */
    private static String f3245i = null;

    /* renamed from: j */
    private static List<DispathcPoiData> f3246j = new ArrayList();

    /* renamed from: k */
    private static LatLng f3247k = null;

    /* renamed from: l */
    private static LatLng f3248l = null;

    /* renamed from: m */
    private static String f3249m = null;

    /* renamed from: n */
    private static String f3250n = null;

    /* renamed from: o */
    private static RouteParaOption.EBusStrategyType f3251o;

    /* renamed from: p */
    private static String f3252p = null;

    /* renamed from: q */
    private static String f3253q = null;

    /* renamed from: r */
    private static LatLng f3254r = null;

    /* renamed from: s */
    private static int f3255s = 0;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public static boolean f3256t = false;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public static boolean f3257u = false;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public static Thread f3258v;

    /* renamed from: a */
    public static String m3268a() {
        return AppTools.getBaiduMapToken();
    }

    /* renamed from: a */
    public static void m3269a(int i, Context context) {
        switch (i) {
            case 0:
            case 1:
            case 2:
                m3288c(context, i);
                return;
            case 3:
                m3287c(context);
                return;
            case 4:
                m3290d(context);
                return;
            case 5:
                m3292e(context);
                return;
            case 6:
            default:
                return;
            case 7:
                m3293f(context);
                return;
            case 8:
                m3295g(context);
                return;
            case 9:
                m3297h(context);
                return;
        }
    }

    /* renamed from: a */
    public static void m3270a(Context context) {
        if (f3257u) {
            context.unbindService(f3238b);
            f3257u = false;
        }
    }

    /* renamed from: a */
    private static void m3271a(List<DispathcPoiData> list, Context context) {
        f3243g = context.getPackageName();
        f3244h = m3280b(context);
        f3245i = "";
        List<DispathcPoiData> list2 = f3246j;
        if (list2 != null) {
            list2.clear();
        }
        for (DispathcPoiData dispathcPoiData : list) {
            f3246j.add(dispathcPoiData);
        }
    }

    /* renamed from: a */
    public static boolean m3272a(int i) {
        switch (i) {
            case 0:
            case 1:
            case 2:
                return m3296g();
            case 3:
                return m3298h();
            case 4:
                return m3303m();
            case 5:
                return m3300j();
            case 6:
                return m3299i();
            case 7:
                return m3301k();
            case 8:
                return m3302l();
            default:
                return false;
        }
    }

    /* renamed from: a */
    public static boolean m3273a(Context context, int i) {
        try {
            if (!C1321a.m4916a(context)) {
                Log.d(f3239c, "package sign verify failed");
                return false;
            }
            f3256t = false;
            switch (i) {
                case 0:
                    f3237a = 0;
                    break;
                case 1:
                    f3237a = 1;
                    break;
                case 2:
                    f3237a = 2;
                    break;
                case 3:
                    f3237a = 3;
                    break;
                case 4:
                    f3237a = 4;
                    break;
                case 5:
                    f3237a = 5;
                    break;
                case 6:
                    f3237a = 6;
                    break;
                case 7:
                    f3237a = 7;
                    break;
                case 8:
                    f3237a = 8;
                    break;
                case 9:
                    f3237a = 9;
                    break;
            }
            if (i == 9) {
                f3257u = false;
            }
            if (f3240d == null || !f3257u) {
                m3281b(context, i);
                return true;
            } else if (f3241e != null) {
                f3256t = true;
                return m3272a(i);
            } else {
                f3240d.mo12858a(new C1013c(i));
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public static boolean m3274a(NaviParaOption naviParaOption, Context context, int i) {
        m3282b(naviParaOption, context, i);
        return m3273a(context, i);
    }

    /* renamed from: a */
    public static boolean m3275a(PoiParaOption poiParaOption, Context context, int i) {
        m3283b(poiParaOption, context, i);
        return m3273a(context, i);
    }

    /* renamed from: a */
    public static boolean m3276a(RouteParaOption routeParaOption, Context context, int i) {
        m3284b(routeParaOption, context, i);
        return m3273a(context, i);
    }

    /* renamed from: a */
    public static boolean m3277a(List<DispathcPoiData> list, Context context, int i) {
        m3271a(list, context);
        return m3273a(context, i);
    }

    /* renamed from: b */
    public static String m3280b(Context context) {
        PackageManager packageManager;
        ApplicationInfo applicationInfo = null;
        try {
            packageManager = context.getPackageManager();
            try {
                applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException unused) {
            }
        } catch (PackageManager.NameNotFoundException unused2) {
            packageManager = null;
        }
        return (String) packageManager.getApplicationLabel(applicationInfo);
    }

    /* renamed from: b */
    private static void m3281b(Context context, int i) {
        Intent intent = new Intent();
        String a = m3268a();
        if (a != null) {
            intent.putExtra("api_token", a);
            intent.setAction("com.baidu.map.action.OPEN_SERVICE");
            intent.setPackage("com.baidu.BaiduMap");
            if (i != 9) {
                f3257u = context.bindService(intent, f3238b, 1);
            }
            if (f3257u) {
                f3258v = new Thread(new C1016f(context, i));
                f3258v.setDaemon(true);
                f3258v.start();
                return;
            }
            Log.e("baidumapsdk", "bind service failed，call openapi");
            m3269a(i, context);
        }
    }

    /* renamed from: b */
    private static void m3282b(NaviParaOption naviParaOption, Context context, int i) {
        f3243g = context.getPackageName();
        f3249m = null;
        f3247k = null;
        f3250n = null;
        f3248l = null;
        if (naviParaOption.getStartPoint() != null) {
            f3247k = naviParaOption.getStartPoint();
        }
        if (naviParaOption.getEndPoint() != null) {
            f3248l = naviParaOption.getEndPoint();
        }
        if (naviParaOption.getStartName() != null) {
            f3249m = naviParaOption.getStartName();
        }
        if (naviParaOption.getEndName() != null) {
            f3250n = naviParaOption.getEndName();
        }
    }

    /* renamed from: b */
    private static void m3283b(PoiParaOption poiParaOption, Context context, int i) {
        f3252p = null;
        f3253q = null;
        f3254r = null;
        f3255s = 0;
        f3243g = context.getPackageName();
        if (poiParaOption.getUid() != null) {
            f3252p = poiParaOption.getUid();
        }
        if (poiParaOption.getKey() != null) {
            f3253q = poiParaOption.getKey();
        }
        if (poiParaOption.getCenter() != null) {
            f3254r = poiParaOption.getCenter();
        }
        if (poiParaOption.getRadius() != 0) {
            f3255s = poiParaOption.getRadius();
        }
    }

    /* renamed from: b */
    private static void m3284b(RouteParaOption routeParaOption, Context context, int i) {
        int i2;
        f3249m = null;
        f3247k = null;
        f3250n = null;
        f3248l = null;
        f3243g = context.getPackageName();
        if (routeParaOption.getStartPoint() != null) {
            f3247k = routeParaOption.getStartPoint();
        }
        if (routeParaOption.getEndPoint() != null) {
            f3248l = routeParaOption.getEndPoint();
        }
        if (routeParaOption.getStartName() != null) {
            f3249m = routeParaOption.getStartName();
        }
        if (routeParaOption.getEndName() != null) {
            f3250n = routeParaOption.getEndName();
        }
        if (routeParaOption.getBusStrategyType() != null) {
            f3251o = routeParaOption.getBusStrategyType();
        }
        if (i != 0) {
            i2 = 1;
            if (i != 1) {
                i2 = 2;
                if (i != 2) {
                    return;
                }
            }
        } else {
            i2 = 0;
        }
        f3242f = i2;
    }

    /* renamed from: c */
    private static void m3287c(Context context) {
        Thread thread = f3258v;
        if (thread != null) {
            thread.interrupt();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("baidumap://map/place/detail?");
        sb.append("uid=");
        sb.append(f3252p);
        sb.append("&show_type=");
        sb.append("detail_page");
        sb.append("&src=");
        sb.append("sdk_[" + f3243g + "]");
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
        intent.setFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
        context.startActivity(intent);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0073, code lost:
        if (r2 != null) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (r2 != null) goto L_0x00a8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c6  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m3288c(android.content.Context r8, int r9) {
        /*
            java.lang.Thread r0 = com.baidu.mapapi.utils.C1012b.f3258v
            if (r0 == 0) goto L_0x0007
            r0.interrupt()
        L_0x0007:
            java.lang.String r0 = "driving"
            java.lang.String r1 = "transit"
            java.lang.String r2 = "walking"
            java.lang.String[] r0 = new java.lang.String[]{r0, r1, r2}
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "baidumap://map/direction?"
            r1.append(r2)
            java.lang.String r2 = "origin="
            r1.append(r2)
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3247k
            if (r2 == 0) goto L_0x0034
            com.baidu.mapapi.CoordType r2 = com.baidu.mapapi.SDKInitializer.getCoordType()
            com.baidu.mapapi.CoordType r3 = com.baidu.mapapi.CoordType.GCJ02
            if (r2 != r3) goto L_0x0034
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3247k
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapsdkplatform.comapi.util.CoordTrans.gcjToBaidu(r2)
            com.baidu.mapapi.utils.C1012b.f3247k = r2
        L_0x0034:
            java.lang.String r2 = com.baidu.mapapi.utils.C1012b.f3249m
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            java.lang.String r3 = "|latlng:"
            java.lang.String r4 = "name:"
            java.lang.String r5 = ","
            if (r2 != 0) goto L_0x0063
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3247k
            if (r2 == 0) goto L_0x0063
            r1.append(r4)
            java.lang.String r2 = com.baidu.mapapi.utils.C1012b.f3249m
            r1.append(r2)
            r1.append(r3)
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3247k
        L_0x0053:
            double r6 = r2.latitude
            r1.append(r6)
            r1.append(r5)
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3247k
            double r6 = r2.longitude
            r1.append(r6)
            goto L_0x0076
        L_0x0063:
            java.lang.String r2 = com.baidu.mapapi.utils.C1012b.f3249m
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x0071
            java.lang.String r2 = com.baidu.mapapi.utils.C1012b.f3249m
            r1.append(r2)
            goto L_0x0076
        L_0x0071:
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3247k
            if (r2 == 0) goto L_0x0076
            goto L_0x0053
        L_0x0076:
            java.lang.String r2 = "&destination="
            r1.append(r2)
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3248l
            if (r2 == 0) goto L_0x008f
            com.baidu.mapapi.CoordType r2 = com.baidu.mapapi.SDKInitializer.getCoordType()
            com.baidu.mapapi.CoordType r6 = com.baidu.mapapi.CoordType.GCJ02
            if (r2 != r6) goto L_0x008f
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3248l
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapsdkplatform.comapi.util.CoordTrans.gcjToBaidu(r2)
            com.baidu.mapapi.utils.C1012b.f3248l = r2
        L_0x008f:
            java.lang.String r2 = com.baidu.mapapi.utils.C1012b.f3250n
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x00b8
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3248l
            if (r2 == 0) goto L_0x00b8
            r1.append(r4)
            java.lang.String r2 = com.baidu.mapapi.utils.C1012b.f3250n
            r1.append(r2)
            r1.append(r3)
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3248l
        L_0x00a8:
            double r2 = r2.latitude
            r1.append(r2)
            r1.append(r5)
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3248l
            double r2 = r2.longitude
            r1.append(r2)
            goto L_0x00cb
        L_0x00b8:
            java.lang.String r2 = com.baidu.mapapi.utils.C1012b.f3250n
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x00c6
            java.lang.String r2 = com.baidu.mapapi.utils.C1012b.f3250n
            r1.append(r2)
            goto L_0x00cb
        L_0x00c6:
            com.baidu.mapapi.model.LatLng r2 = com.baidu.mapapi.utils.C1012b.f3248l
            if (r2 == 0) goto L_0x00cb
            goto L_0x00a8
        L_0x00cb:
            java.lang.String r2 = "&mode="
            r1.append(r2)
            r9 = r0[r9]
            r1.append(r9)
            java.lang.String r9 = "&target="
            r1.append(r9)
            java.lang.String r9 = "1"
            r1.append(r9)
            java.lang.String r9 = "&src="
            r1.append(r9)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r0 = "sdk_["
            r9.append(r0)
            java.lang.String r0 = com.baidu.mapapi.utils.C1012b.f3243g
            r9.append(r0)
            java.lang.String r0 = "]"
            r9.append(r0)
            java.lang.String r9 = r9.toString()
            r1.append(r9)
            java.lang.String r9 = r1.toString()
            android.net.Uri r9 = android.net.Uri.parse(r9)
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.VIEW"
            r0.<init>(r1, r9)
            r9 = 268435456(0x10000000, float:2.5243549E-29)
            r0.setFlags(r9)
            r8.startActivity(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.utils.C1012b.m3288c(android.content.Context, int):void");
    }

    /* renamed from: d */
    private static void m3290d(Context context) {
        Thread thread = f3258v;
        if (thread != null) {
            thread.interrupt();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("baidumap://map/nearbysearch?");
        if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
            f3254r = CoordTrans.gcjToBaidu(f3254r);
        }
        sb.append("center=");
        sb.append(f3254r.latitude);
        sb.append(",");
        sb.append(f3254r.longitude);
        sb.append("&query=");
        sb.append(f3253q);
        sb.append("&radius=");
        sb.append(f3255s);
        sb.append("&src=");
        sb.append("sdk_[" + f3243g + "]");
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
        intent.setFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
        context.startActivity(intent);
    }

    /* renamed from: e */
    private static void m3292e(Context context) {
        Thread thread = f3258v;
        if (thread != null) {
            thread.interrupt();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("baidumap://map/navi?");
        if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
            f3247k = CoordTrans.gcjToBaidu(f3247k);
            f3248l = CoordTrans.gcjToBaidu(f3248l);
        }
        sb.append("origin=");
        sb.append(f3247k.latitude);
        sb.append(",");
        sb.append(f3247k.longitude);
        sb.append("&location=");
        sb.append(f3248l.latitude);
        sb.append(",");
        sb.append(f3248l.longitude);
        sb.append("&src=");
        sb.append("sdk_[" + f3243g + "]");
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
        intent.setFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
        context.startActivity(intent);
    }

    /* renamed from: f */
    private static void m3293f(Context context) {
        Thread thread = f3258v;
        if (thread != null) {
            thread.interrupt();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("baidumap://map/walknavi?");
        if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
            f3247k = CoordTrans.gcjToBaidu(f3247k);
            f3248l = CoordTrans.gcjToBaidu(f3248l);
        }
        sb.append("origin=");
        sb.append(f3247k.latitude);
        sb.append(",");
        sb.append(f3247k.longitude);
        sb.append("&destination=");
        sb.append(f3248l.latitude);
        sb.append(",");
        sb.append(f3248l.longitude);
        sb.append("&src=");
        sb.append("sdk_[" + f3243g + "]");
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
        intent.setFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
        context.startActivity(intent);
    }

    /* renamed from: g */
    private static void m3295g(Context context) {
        Thread thread = f3258v;
        if (thread != null) {
            thread.interrupt();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("baidumap://map/bikenavi?");
        if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
            f3247k = CoordTrans.gcjToBaidu(f3247k);
            f3248l = CoordTrans.gcjToBaidu(f3248l);
        }
        sb.append("origin=");
        sb.append(f3247k.latitude);
        sb.append(",");
        sb.append(f3247k.longitude);
        sb.append("&destination=");
        sb.append(f3248l.latitude);
        sb.append(",");
        sb.append(f3248l.longitude);
        sb.append("&src=");
        sb.append("sdk_[" + f3243g + "]");
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
        intent.setFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
        context.startActivity(intent);
    }

    /* renamed from: g */
    private static boolean m3296g() {
        try {
            Log.d(f3239c, "callDispatchTakeOutRoute");
            String a = f3241e.mo12854a("map.android.baidu.mainmap");
            if (a != null) {
                Bundle bundle = new Bundle();
                bundle.putString("target", "route_search_page");
                Bundle bundle2 = new Bundle();
                bundle2.putInt("route_type", f3242f);
                bundle2.putInt("bus_strategy", f3251o.ordinal());
                bundle2.putInt("cross_city_bus_strategy", 5);
                if (f3247k != null) {
                    bundle2.putInt("start_type", 1);
                    bundle2.putInt("start_longitude", (int) CoordUtil.ll2mc(f3247k).getLongitudeE6());
                    bundle2.putInt("start_latitude", (int) CoordUtil.ll2mc(f3247k).getLatitudeE6());
                } else {
                    bundle2.putInt("start_type", 2);
                    bundle2.putInt("start_longitude", 0);
                    bundle2.putInt("start_latitude", 0);
                }
                if (f3249m != null) {
                    bundle2.putString("start_keyword", f3249m);
                } else {
                    bundle2.putString("start_keyword", "地图上的点");
                }
                bundle2.putString("start_uid", "");
                if (f3248l != null) {
                    bundle2.putInt("end_type", 1);
                    bundle2.putInt("end_longitude", (int) CoordUtil.ll2mc(f3248l).getLongitudeE6());
                    bundle2.putInt("end_latitude", (int) CoordUtil.ll2mc(f3248l).getLatitudeE6());
                } else {
                    bundle2.putInt("end_type", 2);
                    bundle2.putInt("end_longitude", 0);
                    bundle2.putInt("end_latitude", 0);
                }
                if (f3250n != null) {
                    bundle2.putString("end_keyword", f3250n);
                } else {
                    bundle2.putString("end_keyword", "地图上的点");
                }
                bundle2.putString("end_uid", "");
                bundle.putBundle("base_params", bundle2);
                Bundle bundle3 = new Bundle();
                bundle3.putString("launch_from", "sdk_[" + f3243g + "]");
                bundle.putBundle("ext_params", bundle3);
                return f3241e.mo12855a("map.android.baidu.mainmap", a, bundle);
            }
            Log.d(f3239c, "callDispatchTakeOut com not found");
            return false;
        } catch (RemoteException e) {
            Log.d(f3239c, "callDispatchTakeOut exception", e);
        }
    }

    /* renamed from: h */
    private static void m3297h(Context context) {
        Thread thread = f3258v;
        if (thread != null) {
            thread.interrupt();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("baidumap://map/walknavi?");
        if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
            f3247k = CoordTrans.gcjToBaidu(f3247k);
            f3248l = CoordTrans.gcjToBaidu(f3248l);
        }
        sb.append("origin=");
        sb.append(f3247k.latitude);
        sb.append(",");
        sb.append(f3247k.longitude);
        sb.append("&destination=");
        sb.append(f3248l.latitude);
        sb.append(",");
        sb.append(f3248l.longitude);
        sb.append("&mode=");
        sb.append("walking_ar");
        sb.append("&src=");
        sb.append("sdk_[" + f3243g + "]");
        Log.e("test", sb.toString());
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
        intent.setFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
        context.startActivity(intent);
    }

    /* renamed from: h */
    private static boolean m3298h() {
        try {
            Log.d(f3239c, "callDispatchTakeOutPoiDetials");
            String a = f3241e.mo12854a("map.android.baidu.mainmap");
            if (a != null) {
                Bundle bundle = new Bundle();
                bundle.putString("target", "request_poi_detail_page");
                Bundle bundle2 = new Bundle();
                bundle2.putString(Config.CUSTOM_USER_ID, f3252p != null ? f3252p : "");
                bundle.putBundle("base_params", bundle2);
                Bundle bundle3 = new Bundle();
                bundle3.putString("launch_from", "sdk_[" + f3243g + "]");
                bundle.putBundle("ext_params", bundle3);
                return f3241e.mo12855a("map.android.baidu.mainmap", a, bundle);
            }
            Log.d(f3239c, "callDispatchTakeOut com not found");
            return false;
        } catch (RemoteException e) {
            Log.d(f3239c, "callDispatchTakeOut exception", e);
            return false;
        }
    }

    /* renamed from: i */
    private static boolean m3299i() {
        List<DispathcPoiData> list = f3246j;
        if (list != null && list.size() > 0) {
            try {
                Log.d(f3239c, "callDispatchPoiToBaiduMap");
                String a = f3241e.mo12854a("map.android.baidu.mainmap");
                if (a != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("target", "favorite_page");
                    Bundle bundle2 = new Bundle();
                    JSONArray jSONArray = new JSONArray();
                    int i = 0;
                    for (int i2 = 0; i2 < f3246j.size(); i2++) {
                        if (f3246j.get(i2).name != null && !f3246j.get(i2).name.equals("")) {
                            if (f3246j.get(i2).f3264pt != null) {
                                JSONObject jSONObject = new JSONObject();
                                try {
                                    jSONObject.put(Config.FEED_LIST_NAME, f3246j.get(i2).name);
                                    GeoPoint ll2mc = CoordUtil.ll2mc(f3246j.get(i2).f3264pt);
                                    jSONObject.put("ptx", ll2mc.getLongitudeE6());
                                    jSONObject.put("pty", ll2mc.getLatitudeE6());
                                    jSONObject.put("addr", f3246j.get(i2).addr);
                                    jSONObject.put(Config.CUSTOM_USER_ID, f3246j.get(i2).uid);
                                    i++;
                                    jSONArray.put(jSONObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    if (i == 0) {
                        return false;
                    }
                    bundle2.putString("data", jSONArray.toString());
                    bundle2.putString("from", f3244h);
                    bundle2.putString("pkg", f3243g);
                    bundle2.putString("cls", f3245i);
                    bundle2.putInt(Config.TRACE_VISIT_RECENT_COUNT, i);
                    bundle.putBundle("base_params", bundle2);
                    Bundle bundle3 = new Bundle();
                    bundle3.putString("launch_from", "sdk_[" + f3243g + "]");
                    bundle.putBundle("ext_params", bundle3);
                    return f3241e.mo12855a("map.android.baidu.mainmap", a, bundle);
                }
                Log.d(f3239c, "callDispatchPoiToBaiduMap com not found");
            } catch (RemoteException e2) {
                Log.d(f3239c, "callDispatchPoiToBaiduMap exception", e2);
            }
        }
        return false;
    }

    /* renamed from: j */
    private static boolean m3300j() {
        try {
            Log.d(f3239c, "callDispatchTakeOutRouteNavi");
            String a = f3241e.mo12854a("map.android.baidu.mainmap");
            if (a != null) {
                Bundle bundle = new Bundle();
                bundle.putString("target", "navigation_page");
                Bundle bundle2 = new Bundle();
                bundle2.putString("coord_type", "bd09ll");
                bundle2.putString(SocialConstants.PARAM_TYPE, "DIS");
                StringBuffer stringBuffer = new StringBuffer();
                if (f3249m != null) {
                    stringBuffer.append("name:" + f3249m + "|");
                }
                if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                    f3247k = CoordTrans.gcjToBaidu(f3247k);
                }
                stringBuffer.append(String.format("latlng:%f,%f", Double.valueOf(f3247k.latitude), Double.valueOf(f3247k.longitude)));
                StringBuffer stringBuffer2 = new StringBuffer();
                if (f3250n != null) {
                    stringBuffer2.append("name:" + f3250n + "|");
                }
                if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                    f3248l = CoordTrans.gcjToBaidu(f3248l);
                }
                stringBuffer2.append(String.format("latlng:%f,%f", Double.valueOf(f3248l.latitude), Double.valueOf(f3248l.longitude)));
                bundle2.putString("origin", stringBuffer.toString());
                bundle2.putString("destination", stringBuffer2.toString());
                bundle.putBundle("base_params", bundle2);
                Bundle bundle3 = new Bundle();
                bundle3.putString("launch_from", "sdk_[" + f3243g + "]");
                bundle.putBundle("ext_params", bundle3);
                return f3241e.mo12855a("map.android.baidu.mainmap", a, bundle);
            }
            Log.d(f3239c, "callDispatchTakeOut com not found");
            return false;
        } catch (RemoteException e) {
            Log.d(f3239c, "callDispatchTakeOut exception", e);
        }
    }

    /* renamed from: k */
    private static boolean m3301k() {
        try {
            Log.d(f3239c, "callDispatchTakeOutRouteNavi");
            String a = f3241e.mo12854a("map.android.baidu.mainmap");
            if (a != null) {
                Bundle bundle = new Bundle();
                bundle.putString("target", "walknavi_page");
                Bundle bundle2 = new Bundle();
                bundle2.putString("coord_type", "bd09ll");
                StringBuffer stringBuffer = new StringBuffer();
                if (f3249m != null) {
                    stringBuffer.append("name:" + f3249m + "|");
                }
                if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                    f3247k = CoordTrans.gcjToBaidu(f3247k);
                }
                stringBuffer.append(String.format("latlng:%f,%f", Double.valueOf(f3247k.latitude), Double.valueOf(f3247k.longitude)));
                StringBuffer stringBuffer2 = new StringBuffer();
                if (f3250n != null) {
                    stringBuffer2.append("name:" + f3250n + "|");
                }
                if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                    f3248l = CoordTrans.gcjToBaidu(f3248l);
                }
                stringBuffer2.append(String.format("latlng:%f,%f", Double.valueOf(f3248l.latitude), Double.valueOf(f3248l.longitude)));
                bundle2.putString("origin", stringBuffer.toString());
                bundle2.putString("destination", stringBuffer2.toString());
                bundle.putBundle("base_params", bundle2);
                Bundle bundle3 = new Bundle();
                bundle3.putString("launch_from", "sdk_[" + f3243g + "]");
                bundle.putBundle("ext_params", bundle3);
                return f3241e.mo12855a("map.android.baidu.mainmap", a, bundle);
            }
            Log.d(f3239c, "callDispatchTakeOut com not found");
            return false;
        } catch (Exception e) {
            Log.d(f3239c, "callDispatchTakeOut exception", e);
        }
    }

    /* renamed from: l */
    private static boolean m3302l() {
        try {
            Log.d(f3239c, "callDispatchTakeOutRouteRidingNavi");
            String a = f3241e.mo12854a("map.android.baidu.mainmap");
            if (a != null) {
                Bundle bundle = new Bundle();
                bundle.putString("target", "bikenavi_page");
                Bundle bundle2 = new Bundle();
                bundle2.putString("coord_type", "bd09ll");
                StringBuffer stringBuffer = new StringBuffer();
                if (f3249m != null) {
                    stringBuffer.append("name:" + f3249m + "|");
                }
                if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                    f3247k = CoordTrans.gcjToBaidu(f3247k);
                }
                stringBuffer.append(String.format("latlng:%f,%f", Double.valueOf(f3247k.latitude), Double.valueOf(f3247k.longitude)));
                StringBuffer stringBuffer2 = new StringBuffer();
                if (f3250n != null) {
                    stringBuffer2.append("name:" + f3250n + "|");
                }
                if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                    f3248l = CoordTrans.gcjToBaidu(f3248l);
                }
                stringBuffer2.append(String.format("latlng:%f,%f", Double.valueOf(f3248l.latitude), Double.valueOf(f3248l.longitude)));
                bundle2.putString("origin", stringBuffer.toString());
                bundle2.putString("destination", stringBuffer2.toString());
                bundle.putBundle("base_params", bundle2);
                Bundle bundle3 = new Bundle();
                bundle3.putString("launch_from", "sdk_[" + f3243g + "]");
                bundle.putBundle("ext_params", bundle3);
                return f3241e.mo12855a("map.android.baidu.mainmap", a, bundle);
            }
            Log.d(f3239c, "callDispatchTakeOut com not found");
            return false;
        } catch (RemoteException e) {
            Log.d(f3239c, "callDispatchTakeOut exception", e);
        }
    }

    /* renamed from: m */
    private static boolean m3303m() {
        try {
            Log.d(f3239c, "callDispatchTakeOutPoiNearbySearch");
            String a = f3241e.mo12854a("map.android.baidu.mainmap");
            if (a != null) {
                Bundle bundle = new Bundle();
                bundle.putString("target", "poi_search_page");
                Bundle bundle2 = new Bundle();
                if (f3253q != null) {
                    bundle2.putString("search_key", f3253q);
                } else {
                    bundle2.putString("search_key", "");
                }
                if (f3254r != null) {
                    bundle2.putInt("center_pt_x", (int) CoordUtil.ll2mc(f3254r).getLongitudeE6());
                    bundle2.putInt("center_pt_y", (int) CoordUtil.ll2mc(f3254r).getLatitudeE6());
                } else {
                    bundle2.putString("search_key", "");
                }
                if (f3255s != 0) {
                    bundle2.putInt("search_radius", f3255s);
                } else {
                    bundle2.putInt("search_radius", 1000);
                }
                bundle2.putBoolean("is_direct_search", true);
                bundle2.putBoolean("is_direct_area_search", true);
                bundle.putBundle("base_params", bundle2);
                Bundle bundle3 = new Bundle();
                bundle3.putString("launch_from", "sdk_[" + f3243g + "]");
                bundle.putBundle("ext_params", bundle3);
                return f3241e.mo12855a("map.android.baidu.mainmap", a, bundle);
            }
            Log.d(f3239c, "callDispatchTakeOut com not found");
            return false;
        } catch (RemoteException e) {
            Log.d(f3239c, "callDispatchTakeOut exception", e);
            return false;
        }
    }
}
