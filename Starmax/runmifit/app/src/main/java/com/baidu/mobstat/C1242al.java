package com.baidu.mobstat;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

/* renamed from: com.baidu.mobstat.al */
class C1242al extends C1265az {

    /* renamed from: a */
    private static final String f4281a = "baidu_mtj_sdk_record";

    /* renamed from: b */
    private static C1242al f4282b = new C1242al();

    private C1242al() {
    }

    /* renamed from: a */
    public static C1242al m4572a() {
        return f4282b;
    }

    public SharedPreferences getSharedPreferences(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            return context.getSharedPreferences(f4281a, 4);
        }
        return context.getSharedPreferences(f4281a, 0);
    }

    /* renamed from: a */
    public void mo13917a(Context context, long j) {
        putLong(context, "session_first_visit_time", j);
    }

    /* renamed from: a */
    public Long mo13916a(Context context) {
        return Long.valueOf(getLong(context, "session_first_visit_time", 0));
    }

    /* renamed from: b */
    public void mo13920b(Context context, long j) {
        putLong(context, "session_last_visit_time", j);
    }

    /* renamed from: b */
    public Long mo13919b(Context context) {
        return Long.valueOf(getLong(context, "session_last_visit_time", 0));
    }

    /* renamed from: c */
    public void mo13923c(Context context, long j) {
        putLong(context, "session_visit_interval", j);
    }

    /* renamed from: c */
    public Long mo13922c(Context context) {
        return Long.valueOf(getLong(context, "session_visit_interval", 0));
    }

    /* renamed from: a */
    public void mo13918a(Context context, String str) {
        putString(context, "session_today_visit_count", str);
    }

    /* renamed from: d */
    public String mo13924d(Context context) {
        return getString(context, "session_today_visit_count", "");
    }

    /* renamed from: b */
    public void mo13921b(Context context, String str) {
        putString(context, "session_recent_visit", str);
    }

    /* renamed from: e */
    public String mo13925e(Context context) {
        return getString(context, "session_recent_visit", "");
    }
}
