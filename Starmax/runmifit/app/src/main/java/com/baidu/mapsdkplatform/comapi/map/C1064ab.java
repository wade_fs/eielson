package com.baidu.mapsdkplatform.comapi.map;

import android.os.Bundle;
import com.baidu.mapapi.map.WinRound;
import com.baidu.mapapi.model.inner.Point;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.ab */
public class C1064ab {

    /* renamed from: t */
    private static final String f3413t = C1064ab.class.getSimpleName();

    /* renamed from: a */
    public float f3414a = 12.0f;

    /* renamed from: b */
    public int f3415b = 0;

    /* renamed from: c */
    public int f3416c = 0;

    /* renamed from: d */
    public double f3417d = 1.2958162E7d;

    /* renamed from: e */
    public double f3418e = 4825907.0d;

    /* renamed from: f */
    public int f3419f = -1;

    /* renamed from: g */
    public int f3420g = -1;

    /* renamed from: h */
    public long f3421h = 0;

    /* renamed from: i */
    public long f3422i = 0;

    /* renamed from: j */
    public WinRound f3423j = new WinRound();

    /* renamed from: k */
    public C1065a f3424k = new C1065a();

    /* renamed from: l */
    public boolean f3425l = false;

    /* renamed from: m */
    public double f3426m;

    /* renamed from: n */
    public double f3427n;

    /* renamed from: o */
    public int f3428o;

    /* renamed from: p */
    public String f3429p;

    /* renamed from: q */
    public float f3430q;

    /* renamed from: r */
    public boolean f3431r;

    /* renamed from: s */
    public int f3432s;

    /* renamed from: com.baidu.mapsdkplatform.comapi.map.ab$a */
    public class C1065a {

        /* renamed from: a */
        public long f3433a = 0;

        /* renamed from: b */
        public long f3434b = 0;

        /* renamed from: c */
        public long f3435c = 0;

        /* renamed from: d */
        public long f3436d = 0;

        /* renamed from: e */
        public Point f3437e = new Point(0, 0);

        /* renamed from: f */
        public Point f3438f = new Point(0, 0);

        /* renamed from: g */
        public Point f3439g = new Point(0, 0);

        /* renamed from: h */
        public Point f3440h = new Point(0, 0);

        public C1065a() {
        }
    }

    /* renamed from: a */
    public Bundle mo12949a(C1078e eVar) {
        int i;
        if (this.f3414a < eVar.f3527b) {
            this.f3414a = eVar.f3527b;
        }
        if (this.f3414a > eVar.f3505a) {
            if (this.f3414a == 1096.0f || C1078e.f3478d == 26.0f) {
                this.f3414a = 26.0f;
                C1078e.f3478d = 26.0f;
            } else {
                this.f3414a = eVar.f3505a;
            }
        }
        while (true) {
            i = this.f3415b;
            if (i >= 0) {
                break;
            }
            this.f3415b = i + 360;
        }
        this.f3415b = i % 360;
        if (this.f3416c > 0) {
            this.f3416c = 0;
        }
        if (this.f3416c < -45) {
            this.f3416c = -45;
        }
        Bundle bundle = new Bundle();
        bundle.putDouble("level", (double) this.f3414a);
        bundle.putDouble("rotation", (double) this.f3415b);
        bundle.putDouble("overlooking", (double) this.f3416c);
        bundle.putDouble("centerptx", this.f3417d);
        bundle.putDouble("centerpty", this.f3418e);
        bundle.putInt("left", this.f3423j.left);
        bundle.putInt("right", this.f3423j.right);
        bundle.putInt("top", this.f3423j.top);
        bundle.putInt("bottom", this.f3423j.bottom);
        int i2 = this.f3419f;
        if (i2 >= 0 && this.f3420g >= 0 && i2 <= this.f3423j.right && this.f3420g <= this.f3423j.bottom && this.f3423j.right > 0 && this.f3423j.bottom > 0) {
            int i3 = this.f3419f - ((this.f3423j.right - this.f3423j.left) / 2);
            int i4 = this.f3420g - ((this.f3423j.bottom - this.f3423j.top) / 2);
            this.f3421h = (long) i3;
            this.f3422i = (long) (-i4);
            bundle.putLong("xoffset", this.f3421h);
            bundle.putLong("yoffset", this.f3422i);
        }
        bundle.putInt("lbx", this.f3424k.f3437e.f2890x);
        bundle.putInt("lby", this.f3424k.f3437e.f2891y);
        bundle.putInt("ltx", this.f3424k.f3438f.f2890x);
        bundle.putInt("lty", this.f3424k.f3438f.f2891y);
        bundle.putInt("rtx", this.f3424k.f3439g.f2890x);
        bundle.putInt("rty", this.f3424k.f3439g.f2891y);
        bundle.putInt("rbx", this.f3424k.f3440h.f2890x);
        bundle.putInt("rby", this.f3424k.f3440h.f2891y);
        bundle.putInt("bfpp", this.f3425l ? 1 : 0);
        bundle.putInt("animation", 1);
        bundle.putInt("animatime", this.f3428o);
        bundle.putString("panoid", this.f3429p);
        bundle.putInt("autolink", 0);
        bundle.putFloat("siangle", this.f3430q);
        bundle.putInt("isbirdeye", this.f3431r ? 1 : 0);
        bundle.putInt("ssext", this.f3432s);
        return bundle;
    }

    /* renamed from: a */
    public void mo12950a(Bundle bundle) {
        this.f3414a = (float) bundle.getDouble("level");
        this.f3415b = (int) bundle.getDouble("rotation");
        this.f3416c = (int) bundle.getDouble("overlooking");
        this.f3417d = bundle.getDouble("centerptx");
        this.f3418e = bundle.getDouble("centerpty");
        this.f3423j.left = bundle.getInt("left");
        this.f3423j.right = bundle.getInt("right");
        this.f3423j.top = bundle.getInt("top");
        this.f3423j.bottom = bundle.getInt("bottom");
        this.f3421h = bundle.getLong("xoffset");
        this.f3422i = bundle.getLong("yoffset");
        if (!(this.f3423j.right == 0 || this.f3423j.bottom == 0)) {
            int i = (int) this.f3421h;
            int i2 = (int) (-this.f3422i);
            this.f3419f = i + ((this.f3423j.right - this.f3423j.left) / 2);
            this.f3420g = i2 + ((this.f3423j.bottom - this.f3423j.top) / 2);
        }
        this.f3424k.f3433a = bundle.getLong("gleft");
        this.f3424k.f3434b = bundle.getLong("gright");
        this.f3424k.f3435c = bundle.getLong("gtop");
        this.f3424k.f3436d = bundle.getLong("gbottom");
        if (this.f3424k.f3433a <= -20037508) {
            this.f3424k.f3433a = -20037508;
        }
        if (this.f3424k.f3434b >= 20037508) {
            this.f3424k.f3434b = 20037508;
        }
        if (this.f3424k.f3435c >= 20037508) {
            this.f3424k.f3435c = 20037508;
        }
        if (this.f3424k.f3436d <= -20037508) {
            this.f3424k.f3436d = -20037508;
        }
        this.f3424k.f3437e.f2890x = bundle.getInt("lbx");
        this.f3424k.f3437e.f2891y = bundle.getInt("lby");
        this.f3424k.f3438f.f2890x = bundle.getInt("ltx");
        this.f3424k.f3438f.f2891y = bundle.getInt("lty");
        this.f3424k.f3439g.f2890x = bundle.getInt("rtx");
        this.f3424k.f3439g.f2891y = bundle.getInt("rty");
        this.f3424k.f3440h.f2890x = bundle.getInt("rbx");
        this.f3424k.f3440h.f2891y = bundle.getInt("rby");
        boolean z = false;
        this.f3425l = bundle.getInt("bfpp") == 1;
        this.f3426m = bundle.getDouble("adapterzoomunit");
        this.f3427n = bundle.getDouble("zoomunit");
        this.f3429p = bundle.getString("panoid");
        this.f3430q = bundle.getFloat("siangle");
        if (bundle.getInt("isbirdeye") != 0) {
            z = true;
        }
        this.f3431r = z;
        this.f3432s = bundle.getInt("ssext");
    }
}
