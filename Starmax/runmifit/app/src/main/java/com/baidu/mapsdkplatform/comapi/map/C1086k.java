package com.baidu.mapsdkplatform.comapi.map;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.k */
class C1086k extends Handler {

    /* renamed from: a */
    final /* synthetic */ C1084j f3582a;

    C1086k(C1084j jVar) {
        this.f3582a = jVar;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.f3582a.f3573g != null && ((Long) message.obj).longValue() == this.f3582a.f3573g.f3534j) {
            boolean z = false;
            if (message.what == 4000) {
                if (this.f3582a.f3573g.f3532h != null) {
                    for (C1087l lVar : this.f3582a.f3573g.f3532h) {
                        Bitmap bitmap = null;
                        if (message.arg2 == 1) {
                            int[] iArr = new int[(this.f3582a.f3570d * this.f3582a.f3571e)];
                            int[] iArr2 = new int[(this.f3582a.f3570d * this.f3582a.f3571e)];
                            if (this.f3582a.f3573g.f3533i != null) {
                                int[] a = this.f3582a.f3573g.f3533i.mo13487a(iArr, this.f3582a.f3570d, this.f3582a.f3571e);
                                for (int i = 0; i < this.f3582a.f3571e; i++) {
                                    for (int i2 = 0; i2 < this.f3582a.f3570d; i2++) {
                                        int i3 = a[(this.f3582a.f3570d * i) + i2];
                                        iArr2[(((this.f3582a.f3571e - i) - 1) * this.f3582a.f3570d) + i2] = (i3 & -16711936) | ((i3 << 16) & 16711680) | ((i3 >> 16) & 255);
                                    }
                                }
                                bitmap = Bitmap.createBitmap(iArr2, this.f3582a.f3570d, this.f3582a.f3571e, Bitmap.Config.RGB_565);
                            } else {
                                return;
                            }
                        }
                        if (lVar != null) {
                            lVar.mo11539a(bitmap);
                        }
                    }
                }
            } else if (message.what == 39) {
                if (this.f3582a.f3573g != null && this.f3582a.f3573g.f3532h != null) {
                    if (message.arg1 == 100) {
                        this.f3582a.f3573g.mo12986B();
                    } else if (message.arg1 == 200) {
                        this.f3582a.f3573g.mo12996L();
                    } else if (message.arg1 == 1) {
                        this.f3582a.requestRender();
                    } else if (message.arg1 == 0) {
                        this.f3582a.requestRender();
                        if (!this.f3582a.f3573g.mo13036b() && this.f3582a.getRenderMode() != 0) {
                            this.f3582a.setRenderMode(0);
                        }
                    } else if (message.arg1 == 2) {
                        if (this.f3582a.f3573g.f3532h != null) {
                            for (C1087l lVar2 : this.f3582a.f3573g.f3532h) {
                                if (lVar2 != null) {
                                    lVar2.mo11551c();
                                }
                            }
                        } else {
                            return;
                        }
                    }
                    if (!this.f3582a.f3573g.f3535k && this.f3582a.f3571e > 0 && this.f3582a.f3570d > 0 && this.f3582a.f3573g.mo13030b(0, 0) != null) {
                        this.f3582a.f3573g.f3535k = true;
                        for (C1087l lVar3 : this.f3582a.f3573g.f3532h) {
                            if (lVar3 != null) {
                                lVar3.mo11547b();
                            }
                        }
                    }
                    for (C1087l lVar4 : this.f3582a.f3573g.f3532h) {
                        if (lVar4 != null) {
                            lVar4.mo11538a();
                        }
                    }
                }
            } else if (message.what == 41) {
                if (this.f3582a.f3573g != null && this.f3582a.f3573g.f3532h != null) {
                    if (this.f3582a.f3573g.f3537n || this.f3582a.f3573g.f3538o) {
                        for (C1087l lVar5 : this.f3582a.f3573g.f3532h) {
                            if (lVar5 != null) {
                                lVar5.mo11549b(this.f3582a.f3573g.mo12989E());
                            }
                        }
                    }
                }
            } else if (message.what == 999) {
                if (this.f3582a.f3573g.f3532h != null) {
                    for (C1087l lVar6 : this.f3582a.f3573g.f3532h) {
                        if (lVar6 != null) {
                            lVar6.mo11556e();
                        }
                    }
                }
            } else if (message.what == 50) {
                if (this.f3582a.f3573g.f3532h != null) {
                    for (C1087l lVar7 : this.f3582a.f3573g.f3532h) {
                        if (lVar7 != null) {
                            if (message.arg1 != 0) {
                                if (message.arg1 == 1) {
                                    if (this.f3582a.f3573g.mo12989E().f3414a >= 18.0f) {
                                        lVar7.mo11545a(true);
                                    }
                                }
                            }
                            lVar7.mo11545a(false);
                        }
                    }
                }
            } else if (message.what == 65289) {
                int i4 = message.arg2;
                if (message.arg1 == 300) {
                    if (message.arg2 == 1003) {
                        i4 = 0;
                        z = true;
                    } else if (message.arg2 >= 1004) {
                        int i5 = message.arg2;
                    }
                    for (C1087l lVar8 : this.f3582a.f3573g.f3532h) {
                        if (lVar8 != null) {
                            lVar8.mo11546a(z, i4);
                        }
                    }
                }
            }
        }
    }
}
