package com.baidu.mapsdkplatform.comapi.synchronization.p028c;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.baidu.mapapi.JNIInitializer;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import com.baidu.mapsdkplatform.comapi.util.PermissionCheck;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.c.c */
public class C1115c {

    /* renamed from: b */
    public static boolean f3681b = true;

    /* renamed from: c */
    private static final String f3682c = C1115c.class.getSimpleName();

    /* renamed from: a */
    HttpURLConnection f3683a;

    /* renamed from: d */
    private String f3684d = null;

    /* renamed from: e */
    private String f3685e = null;

    /* renamed from: f */
    private int f3686f;

    /* renamed from: g */
    private int f3687g;

    /* renamed from: h */
    private String f3688h;

    /* renamed from: i */
    private C1118e f3689i;

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.c.c$a */
    public enum C1116a {
        SUCCESS,
        NETWORK_ERROR,
        INNER_ERROR,
        REQUEST_ERROR,
        SERVER_ERROR
    }

    public C1115c(String str, C1118e eVar) {
        this.f3688h = str;
        this.f3689i = eVar;
    }

    /* renamed from: a */
    private void m3832a(InputStream inputStream, BufferedReader bufferedReader, HttpURLConnection httpURLConnection) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                C1120a.m3852a(f3682c, "IOException happened when release res", e);
            }
        }
        if (bufferedReader != null) {
            bufferedReader.close();
        }
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
    }

    /* renamed from: a */
    private void m3833a(HttpURLConnection httpURLConnection) {
        try {
            httpURLConnection.connect();
            int responseCode = httpURLConnection.getResponseCode();
            String str = f3682c;
            C1120a.m3855b(str, "responseCode is: " + responseCode);
            if (200 != responseCode) {
                m3834a(httpURLConnection, responseCode);
            } else {
                m3837b(httpURLConnection);
            }
        } catch (IOException unused) {
            httpURLConnection.disconnect();
            C1120a.m3855b(f3682c, "Catch connection exception, INNER_ERROR");
            this.f3689i.mo13186a(C1116a.INNER_ERROR);
        }
    }

    /* renamed from: a */
    private void m3834a(HttpURLConnection httpURLConnection, int i) {
        C1116a aVar = C1116a.SUCCESS;
        C1116a aVar2 = i >= 500 ? C1116a.SERVER_ERROR : i >= 400 ? C1116a.REQUEST_ERROR : C1116a.INNER_ERROR;
        InputStream errorStream = httpURLConnection.getErrorStream();
        C1120a.m3851a(f3682c, errorStream.toString());
        String str = f3682c;
        C1120a.m3851a(str, "Response error, response code = " + i + ", error = " + aVar2);
        if (errorStream != null) {
            try {
                errorStream.close();
            } catch (IOException e) {
                C1120a.m3852a(f3682c, "IOException caught", e);
            }
        }
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.f3689i.mo13186a(aVar2);
    }

    /* renamed from: a */
    private boolean m3835a() {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) JNIInitializer.getCachedContext().getSystemService("connectivity");
            return (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isAvailable()) ? false : true;
        } catch (Exception e) {
            C1120a.m3852a(f3682c, "Exception happened when check network", e);
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: b */
    private HttpURLConnection m3836b() {
        HttpURLConnection httpURLConnection;
        try {
            URL url = new URL(this.f3684d);
            if (f3681b) {
                httpURLConnection = (HttpsURLConnection) url.openConnection();
                ((HttpsURLConnection) httpURLConnection).setHostnameVerifier(new C1117d(this));
            } else {
                httpURLConnection = (HttpURLConnection) url.openConnection();
            }
            httpURLConnection.setRequestMethod(this.f3688h);
            httpURLConnection.setDoOutput(false);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(this.f3686f);
            httpURLConnection.setReadTimeout(this.f3687g);
            return httpURLConnection;
        } catch (Exception e) {
            C1120a.m3852a(f3682c, "url connect failed", e);
            return null;
        }
    }

    /* renamed from: b */
    private void m3837b(HttpURLConnection httpURLConnection) {
        BufferedReader bufferedReader;
        InputStream inputStream;
        IOException e;
        try {
            inputStream = httpURLConnection.getInputStream();
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            } catch (IOException e2) {
                IOException iOException = e2;
                bufferedReader = null;
                e = iOException;
                try {
                    C1120a.m3852a(f3682c, "Catch exception. INNER_ERROR", e);
                    this.f3689i.mo13186a(C1116a.INNER_ERROR);
                    m3832a(inputStream, bufferedReader, httpURLConnection);
                } catch (Throwable th) {
                    th = th;
                    m3832a(inputStream, bufferedReader, httpURLConnection);
                    throw th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                bufferedReader = null;
                th = th3;
                m3832a(inputStream, bufferedReader, httpURLConnection);
                throw th;
            }
            try {
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    int read = bufferedReader.read();
                    if (read != -1) {
                        stringBuffer.append((char) read);
                    } else {
                        this.f3685e = stringBuffer.toString();
                        m3839c(this.f3685e);
                        m3832a(inputStream, bufferedReader, httpURLConnection);
                        this.f3689i.mo13187a(this.f3685e);
                        return;
                    }
                }
            } catch (IOException e3) {
                e = e3;
                C1120a.m3852a(f3682c, "Catch exception. INNER_ERROR", e);
                this.f3689i.mo13186a(C1116a.INNER_ERROR);
                m3832a(inputStream, bufferedReader, httpURLConnection);
            }
        } catch (IOException e4) {
            bufferedReader = null;
            e = e4;
            inputStream = null;
            C1120a.m3852a(f3682c, "Catch exception. INNER_ERROR", e);
            this.f3689i.mo13186a(C1116a.INNER_ERROR);
            m3832a(inputStream, bufferedReader, httpURLConnection);
        } catch (Throwable th4) {
            bufferedReader = null;
            th = th4;
            inputStream = null;
            m3832a(inputStream, bufferedReader, httpURLConnection);
            throw th;
        }
    }

    /* renamed from: b */
    private boolean m3838b(String str) {
        if (!TextUtils.isEmpty(str) && this.f3689i != null) {
            return true;
        }
        String str2 = f3682c;
        C1120a.m3855b(str2, "RequestUrl or ResultCallback is null. RequestUrl = " + str + "; ResultCallback is: " + this.f3689i);
        this.f3689i.mo13186a(C1116a.REQUEST_ERROR);
        return false;
    }

    /* renamed from: c */
    private void m3839c(String str) {
        if (m3840d(str)) {
            C1120a.m3855b(f3682c, "Permission check failed, try again");
            int permissionCheck = PermissionCheck.permissionCheck();
            if (permissionCheck != 0) {
                String str2 = f3682c;
                C1120a.m3855b(str2, "The authorized result is: " + permissionCheck);
            }
        }
    }

    /* renamed from: d */
    private boolean m3840d(String str) {
        return m3841e(str) || m3842f(str);
    }

    /* renamed from: e */
    private boolean m3841e(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
                if (!jSONObject.has("status_sp")) {
                    return false;
                }
            }
            int i = jSONObject.has(NotificationCompat.CATEGORY_STATUS) ? jSONObject.getInt(NotificationCompat.CATEGORY_STATUS) : jSONObject.getInt("status_sp");
            if (106 != i && 105 != i) {
                return false;
            }
            C1120a.m3851a(f3682c, "Permission check failed due token");
            return true;
        } catch (JSONException e) {
            C1120a.m3852a(f3682c, "Parse json happened exception", e);
            return false;
        }
    }

    /* renamed from: f */
    private boolean m3842f(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has("SDK_InnerError") || !jSONObject.optJSONObject("SDK_InnerError").has("PermissionCheckError")) {
                return false;
            }
            C1120a.m3855b(f3682c, "Permission check error due other");
            return true;
        } catch (JSONException e) {
            C1120a.m3852a(f3682c, "Parse json happened exception", e);
            return false;
        }
    }

    /* renamed from: a */
    public void mo13196a(int i) {
        this.f3687g = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13197a(String str) {
        C1118e eVar;
        C1116a aVar;
        if (m3838b(str)) {
            this.f3684d = str;
            String str2 = f3682c;
            C1120a.m3855b(str2, "mRequestUrl is: " + this.f3684d);
            if (!m3835a()) {
                eVar = this.f3689i;
                aVar = C1116a.NETWORK_ERROR;
            } else {
                this.f3683a = m3836b();
                HttpURLConnection httpURLConnection = this.f3683a;
                if (httpURLConnection == null) {
                    C1120a.m3855b(f3682c, "url connection failed");
                    eVar = this.f3689i;
                    aVar = C1116a.INNER_ERROR;
                } else {
                    m3833a(httpURLConnection);
                    return;
                }
            }
            eVar.mo13186a(aVar);
        }
    }

    /* renamed from: b */
    public void mo13198b(int i) {
        this.f3686f = i;
    }
}
