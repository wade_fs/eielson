package com.baidu.platform.comapi.pano;

/* renamed from: com.baidu.platform.comapi.pano.c */
public class C1325c {

    /* renamed from: a */
    String f4447a;

    /* renamed from: b */
    PanoStateError f4448b;

    /* renamed from: c */
    int f4449c;

    public C1325c() {
    }

    public C1325c(PanoStateError panoStateError) {
        this.f4448b = panoStateError;
    }

    /* renamed from: a */
    public PanoStateError mo14029a() {
        return this.f4448b;
    }

    /* renamed from: a */
    public void mo14030a(int i) {
        this.f4449c = i;
    }

    /* renamed from: a */
    public void mo14031a(String str) {
        this.f4447a = str;
    }

    /* renamed from: b */
    public String mo14032b() {
        return this.f4447a;
    }

    /* renamed from: c */
    public int mo14033c() {
        return this.f4449c;
    }
}
