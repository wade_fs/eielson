package com.baidu.location.p013a;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.baidu.location.p014b.C0775d;
import com.baidu.location.p014b.C0781h;
import com.baidu.location.p016d.C0804h;
import com.baidu.location.p017e.C0821a;
import com.baidu.location.p017e.C0834h;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0855k;

/* renamed from: com.baidu.location.a.y */
class C0768y extends Handler {

    /* renamed from: a */
    final /* synthetic */ C0767x f1474a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C0768y(C0767x xVar, Looper looper) {
        super(looper);
        this.f1474a = xVar;
    }

    public void handleMessage(Message message) {
        C0821a aVar;
        String str;
        Location location;
        C0834h hVar;
        switch (message.what) {
            case 1:
                Bundle data = message.getData();
                Location location2 = (Location) data.getParcelable("loc");
                data.getInt("satnum");
                if (location2 != null) {
                    C0731d.m1782a().mo10449a(location2);
                    return;
                }
                return;
            case 2:
                aVar = C0760t.m1940c();
                hVar = C0835i.m2376a().mo10698o();
                location = C0760t.m1941d();
                str = C0760t.m1934a();
                C0765w.m1952a(aVar, hVar, location, str);
                return;
            case 3:
                aVar = C0760t.m1940c();
                hVar = null;
                location = C0760t.m1941d();
                str = C0723a.m1729a().mo10432d();
                C0765w.m1952a(aVar, hVar, location, str);
                return;
            case 4:
                boolean j = C0835i.m2384j();
                if (C0855k.m2460b()) {
                    j = false;
                }
                if (j) {
                    j = C0736h.m1819a().mo10457d() != 1;
                }
                if (j) {
                    if (C0775d.m2005a().mo10532e()) {
                        C0804h.m2193a().mo10602m();
                        C0804h.m2193a().mo10598i();
                    }
                    C0781h.m2040a().mo10543c();
                    if (C0775d.m2005a().mo10532e()) {
                        C0765w.m1949a().mo10507c();
                    }
                }
                try {
                    if (this.f1474a.f1472d != null) {
                        this.f1474a.f1472d.sendEmptyMessageDelayed(4, (long) C0855k.f1917R);
                        return;
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case 5:
                C0781h.m2040a().mo10542b();
                return;
            case 6:
            default:
                return;
            case 7:
                C0765w.m1949a().mo10507c();
                C0781h.m2040a().mo10543c();
                return;
            case 8:
            case 9:
                message.getData();
                return;
        }
    }
}
