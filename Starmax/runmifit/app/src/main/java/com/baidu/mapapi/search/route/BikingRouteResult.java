package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.SearchResult;
import java.util.ArrayList;
import java.util.List;

public class BikingRouteResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<BikingRouteResult> CREATOR = new C0987c();

    /* renamed from: a */
    private List<BikingRouteLine> f3027a;

    /* renamed from: b */
    private SuggestAddrInfo f3028b;

    public BikingRouteResult() {
    }

    protected BikingRouteResult(Parcel parcel) {
        this.f3027a = new ArrayList();
        parcel.readList(this.f3027a, BikingRouteLine.class.getClassLoader());
        this.f3028b = (SuggestAddrInfo) parcel.readParcelable(SuggestAddrInfo.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public List<BikingRouteLine> getRouteLines() {
        return this.f3027a;
    }

    public SuggestAddrInfo getSuggestAddrInfo() {
        return this.f3028b;
    }

    public void setRouteLines(List<BikingRouteLine> list) {
        this.f3027a = list;
    }

    public void setSuggestAddrInfo(SuggestAddrInfo suggestAddrInfo) {
        this.f3028b = suggestAddrInfo;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.f3027a);
        parcel.writeParcelable(this.f3028b, 1);
    }
}
