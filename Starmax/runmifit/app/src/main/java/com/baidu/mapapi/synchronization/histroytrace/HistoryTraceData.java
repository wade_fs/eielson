package com.baidu.mapapi.synchronization.histroytrace;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.synchronization.SyncCoordinateConverter;
import java.util.List;

public class HistoryTraceData implements Parcelable {
    public static final Parcelable.Creator<HistoryTraceData> CREATOR = new C1009a();

    /* renamed from: a */
    private int f3201a;

    /* renamed from: b */
    private double f3202b;

    /* renamed from: c */
    private double f3203c;

    /* renamed from: d */
    private int f3204d;

    /* renamed from: e */
    private LatLng f3205e;

    /* renamed from: f */
    private LatLng f3206f;

    /* renamed from: g */
    private SyncCoordinateConverter.CoordType f3207g = SyncCoordinateConverter.CoordType.BD09LL;

    /* renamed from: h */
    private List<HistoryTracePoint> f3208h;

    /* renamed from: i */
    private int f3209i;

    public static class HistoryTracePoint implements Parcelable {
        public static final Parcelable.Creator<HistoryTracePoint> CREATOR = new C1010b();

        /* renamed from: a */
        private LatLng f3210a;

        /* renamed from: b */
        private long f3211b;

        /* renamed from: c */
        private String f3212c;

        public HistoryTracePoint() {
        }

        protected HistoryTracePoint(Parcel parcel) {
            this.f3210a = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
            this.f3211b = parcel.readLong();
            this.f3212c = parcel.readString();
        }

        public int describeContents() {
            return 0;
        }

        public String getCreateTime() {
            return this.f3212c;
        }

        public long getLocationTime() {
            return this.f3211b;
        }

        public LatLng getPoint() {
            return this.f3210a;
        }

        public void setCreateTime(String str) {
            this.f3212c = str;
        }

        public void setLocationTime(long j) {
            this.f3211b = j;
        }

        public void setPoint(LatLng latLng) {
            this.f3210a = latLng;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.f3210a, i);
            parcel.writeLong(this.f3211b);
            parcel.writeString(this.f3212c);
        }
    }

    public HistoryTraceData() {
    }

    protected HistoryTraceData(Parcel parcel) {
        this.f3201a = parcel.readInt();
        this.f3202b = parcel.readDouble();
        this.f3203c = parcel.readDouble();
        this.f3204d = parcel.readInt();
        this.f3205e = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        this.f3206f = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        this.f3208h = parcel.createTypedArrayList(HistoryTracePoint.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public SyncCoordinateConverter.CoordType getCoordType() {
        return this.f3207g;
    }

    public int getCurrentOrderState() {
        return this.f3204d;
    }

    public int getCurrentPageIndex() {
        return this.f3209i;
    }

    public double getDistance() {
        return this.f3202b;
    }

    public LatLng getOrderEndPosition() {
        return this.f3206f;
    }

    public LatLng getOrderStartPosition() {
        return this.f3205e;
    }

    public List<HistoryTracePoint> getPointsList() {
        return this.f3208h;
    }

    public double getTollDiatance() {
        return this.f3203c;
    }

    public int getTotalPoints() {
        return this.f3201a;
    }

    public void setCoordType(SyncCoordinateConverter.CoordType coordType) {
        this.f3207g = coordType;
    }

    public void setCurrentOrderState(int i) {
        this.f3204d = i;
    }

    public void setCurrentPageIndex(int i) {
        this.f3209i = i;
    }

    public void setDistance(double d) {
        this.f3202b = d;
    }

    public void setOrderEndPosition(LatLng latLng) {
        this.f3206f = latLng;
    }

    public void setOrderStartPosition(LatLng latLng) {
        this.f3205e = latLng;
    }

    public void setPointsList(List<HistoryTracePoint> list) {
        this.f3208h = list;
    }

    public void setTollDiatance(double d) {
        this.f3203c = d;
    }

    public void setTotalPoints(int i) {
        this.f3201a = i;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("HistoryTraceData: \n");
        stringBuffer.append("TotalPoints = ");
        stringBuffer.append(this.f3201a);
        stringBuffer.append("; Distance = ");
        stringBuffer.append(this.f3202b);
        stringBuffer.append("; TollDistance = ");
        stringBuffer.append(this.f3203c);
        stringBuffer.append("; CurrentOrderState = ");
        stringBuffer.append(this.f3204d);
        stringBuffer.append("; OrderStartPosition = ");
        stringBuffer.append(this.f3205e);
        stringBuffer.append("; OrderEndPosition = ");
        stringBuffer.append(this.f3206f);
        List<HistoryTracePoint> list = this.f3208h;
        if (list != null && !list.isEmpty()) {
            stringBuffer.append("\n#History Trace Points Info BEGIN# \n");
            for (int i = 0; i < this.f3208h.size(); i++) {
                HistoryTracePoint historyTracePoint = this.f3208h.get(i);
                if (historyTracePoint != null) {
                    stringBuffer.append("The ");
                    stringBuffer.append(i);
                    stringBuffer.append(" Point Info: ");
                    stringBuffer.append("Point = ");
                    stringBuffer.append(historyTracePoint.getPoint().toString());
                    stringBuffer.append("; LocationTime = ");
                    stringBuffer.append(historyTracePoint.getLocationTime());
                    stringBuffer.append("; CreateTime = ");
                    stringBuffer.append(historyTracePoint.getCreateTime());
                    stringBuffer.append("\n");
                }
            }
        }
        return stringBuffer.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f3201a);
        parcel.writeDouble(this.f3202b);
        parcel.writeDouble(this.f3203c);
        parcel.writeInt(this.f3204d);
        parcel.writeParcelable(this.f3205e, i);
        parcel.writeParcelable(this.f3206f, i);
        parcel.writeTypedList(this.f3208h);
    }
}
