package com.baidu.platform.core.busline;

import com.baidu.mapapi.search.busline.BusLineSearchOption;
import com.baidu.mapapi.search.busline.OnGetBusLineSearchResultListener;

public interface IBusLineSearch {
    /* renamed from: a */
    void mo14043a();

    /* renamed from: a */
    void mo14044a(OnGetBusLineSearchResultListener onGetBusLineSearchResultListener);

    /* renamed from: a */
    boolean mo14045a(BusLineSearchOption busLineSearchOption);
}
