package com.baidu.lbsapi.auth;

import android.os.Handler;
import android.os.Looper;

/* renamed from: com.baidu.lbsapi.auth.m */
class C0717m extends Thread {

    /* renamed from: a */
    Handler f1103a = null;

    /* renamed from: b */
    private Object f1104b = new Object();

    /* renamed from: c */
    private boolean f1105c = false;

    C0717m() {
    }

    C0717m(String str) {
        super(str);
    }

    /* renamed from: a */
    public void mo10216a() {
        if (C0702a.f1077a) {
            C0702a.m1616a("Looper thread quit()");
        }
        this.f1103a.getLooper().quit();
    }

    /* renamed from: b */
    public void mo10217b() {
        synchronized (this.f1104b) {
            try {
                if (!this.f1105c) {
                    this.f1104b.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: c */
    public void mo10218c() {
        synchronized (this.f1104b) {
            this.f1105c = true;
            this.f1104b.notifyAll();
        }
    }

    public void run() {
        Looper.prepare();
        this.f1103a = new Handler();
        if (C0702a.f1077a) {
            C0702a.m1616a("new Handler() finish!!");
        }
        Looper.loop();
        if (C0702a.f1077a) {
            C0702a.m1616a("LooperThread run() thread id:" + String.valueOf(Thread.currentThread().getId()));
        }
    }
}
