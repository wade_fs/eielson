package com.baidu.mapapi.common;

import android.content.Context;
import com.baidu.mapsdkplatform.comapi.util.C1174h;
import java.io.File;

public class EnvironmentUtilities {

    /* renamed from: a */
    static String f2334a;

    /* renamed from: b */
    static String f2335b;

    /* renamed from: c */
    static String f2336c;

    /* renamed from: d */
    static int f2337d;

    /* renamed from: e */
    static int f2338e;

    /* renamed from: f */
    static int f2339f;

    /* renamed from: g */
    private static C1174h f2340g;

    public static String getAppCachePath() {
        return f2335b;
    }

    public static String getAppSDCardPath() {
        String str = f2334a + "/BaiduMapSDKNew";
        if (str.length() != 0) {
            File file = new File(str);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        return str;
    }

    public static String getAppSecondCachePath() {
        return f2336c;
    }

    public static int getDomTmpStgMax() {
        return f2338e;
    }

    public static int getItsTmpStgMax() {
        return f2339f;
    }

    public static int getMapTmpStgMax() {
        return f2337d;
    }

    public static String getSDCardPath() {
        return f2334a;
    }

    public static void initAppDirectory(Context context) {
        String str;
        if (f2340g == null) {
            f2340g = C1174h.m4256a();
            f2340g.mo13401a(context);
        }
        String str2 = f2334a;
        if (str2 == null || str2.length() <= 0) {
            f2334a = f2340g.mo13403b().mo13396a();
            str = f2340g.mo13403b().mo13398c();
        } else {
            str = f2334a + File.separator + "BaiduMapSDKNew" + File.separator + "cache";
        }
        f2335b = str;
        f2336c = f2340g.mo13403b().mo13399d();
        f2337d = 20971520;
        f2338e = 52428800;
        f2339f = 5242880;
    }

    public static void setSDCardPath(String str) {
        f2334a = str;
    }
}
