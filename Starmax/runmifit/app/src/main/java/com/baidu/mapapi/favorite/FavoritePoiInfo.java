package com.baidu.mapapi.favorite;

import com.baidu.mapapi.model.LatLng;

public class FavoritePoiInfo {

    /* renamed from: a */
    String f2343a;

    /* renamed from: b */
    String f2344b;

    /* renamed from: c */
    LatLng f2345c;

    /* renamed from: d */
    String f2346d;

    /* renamed from: e */
    String f2347e;

    /* renamed from: f */
    String f2348f;

    /* renamed from: g */
    long f2349g;

    public FavoritePoiInfo addr(String str) {
        this.f2346d = str;
        return this;
    }

    public FavoritePoiInfo cityName(String str) {
        this.f2347e = str;
        return this;
    }

    public String getAddr() {
        return this.f2346d;
    }

    public String getCityName() {
        return this.f2347e;
    }

    public String getID() {
        return this.f2343a;
    }

    public String getPoiName() {
        return this.f2344b;
    }

    public LatLng getPt() {
        return this.f2345c;
    }

    public long getTimeStamp() {
        return this.f2349g;
    }

    public String getUid() {
        return this.f2348f;
    }

    public FavoritePoiInfo poiName(String str) {
        this.f2344b = str;
        return this;
    }

    /* renamed from: pt */
    public FavoritePoiInfo mo10887pt(LatLng latLng) {
        this.f2345c = latLng;
        return this;
    }

    public FavoritePoiInfo uid(String str) {
        this.f2348f = str;
        return this;
    }
}
