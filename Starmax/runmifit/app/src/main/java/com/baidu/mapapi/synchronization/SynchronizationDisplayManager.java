package com.baidu.mapapi.synchronization;

import android.content.Context;
import android.view.View;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapsdkplatform.comapi.synchronization.p026a.C1103a;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;

public class SynchronizationDisplayManager {

    /* renamed from: a */
    private static final String f3198a = SynchronizationDisplayManager.class.getSimpleName();

    /* renamed from: b */
    private C1103a f3199b;

    public SynchronizationDisplayManager(Context context, BaiduMap baiduMap, RoleOptions roleOptions, DisplayOptions displayOptions) {
        this.f3199b = new C1103a(context, baiduMap, roleOptions, displayOptions);
    }

    public void adjustVisibleSpanByUser() {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13172g();
        }
    }

    public void adjustVisibleSpanByUser(int i, int i2, int i3, int i4) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13152a(i, i2, i3, i4);
        }
    }

    public Marker getCarMarker() {
        C1103a aVar = this.f3199b;
        if (aVar != null) {
            return aVar.mo13171f();
        }
        C1120a.m3855b(f3198a, "The implement instance is null");
        return null;
    }

    public Marker getEndPositionMarker() {
        C1103a aVar = this.f3199b;
        if (aVar != null) {
            return aVar.mo13170e();
        }
        C1120a.m3855b(f3198a, "The implement instance is null");
        return null;
    }

    public Marker getStartPositionMarker() {
        C1103a aVar = this.f3199b;
        if (aVar != null) {
            return aVar.mo13168d();
        }
        C1120a.m3855b(f3198a, "The implement instance is null");
        return null;
    }

    public boolean isHttpsEnable() {
        C1103a aVar = this.f3199b;
        if (aVar != null) {
            return aVar.mo13173h();
        }
        C1120a.m3855b(f3198a, "The implement instance is null");
        return true;
    }

    public void onPause() {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13159b();
        }
    }

    public void onResume() {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13149a();
        }
    }

    public void registerSynchronizationDisplayListener(SynchronizationDisplayListener synchronizationDisplayListener) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13157a(synchronizationDisplayListener);
        }
    }

    public void release() {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13164c();
        }
    }

    public void setDriverPositionFreshFrequency(int i) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13160b(i);
        }
    }

    public void setHttpsEnable(boolean z) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13158a(z);
        }
    }

    public void setOperatedMapFrozenInterval(int i) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13165c(i);
        }
    }

    public void setUnOperatedMapFrozenInterval(int i) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13169d(i);
        }
    }

    public void unRegisterSynchronizationDisplayListener(SynchronizationDisplayListener synchronizationDisplayListener) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13163b(synchronizationDisplayListener);
        }
    }

    public void updateCarPositionInfoWindowView(View view) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13167c(view);
        }
    }

    public void updateDisplayOptions(DisplayOptions displayOptions) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13155a(displayOptions);
        }
    }

    public void updateEndPositionInfoWindowView(View view) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13162b(view);
        }
    }

    public void updateOrderState(int i) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13151a(i);
        }
    }

    public void updateRoleOptions(RoleOptions roleOptions) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13156a(roleOptions);
        }
    }

    public void updateStartPositionInfoWindowView(View view) {
        C1103a aVar = this.f3199b;
        if (aVar == null) {
            C1120a.m3855b(f3198a, "The implement instance is null");
        } else {
            aVar.mo13154a(view);
        }
    }
}
