package com.baidu.location.p013a;

import android.content.Context;
import android.util.Log;
import com.baidu.lbsapi.auth.LBSAuthManager;
import com.baidu.lbsapi.auth.LBSAuthManagerListener;
import com.baidu.location.p019g.C0843a;
import org.json.JSONObject;

/* renamed from: com.baidu.location.a.j */
public class C0741j implements LBSAuthManagerListener {

    /* renamed from: a */
    private static Object f1340a = new Object();

    /* renamed from: b */
    private static C0741j f1341b = null;

    /* renamed from: c */
    private int f1342c = 0;

    /* renamed from: d */
    private Context f1343d = null;

    /* renamed from: e */
    private long f1344e = 0;

    /* renamed from: f */
    private String f1345f = null;

    /* renamed from: a */
    public static C0741j m1844a() {
        C0741j jVar;
        synchronized (f1340a) {
            if (f1341b == null) {
                f1341b = new C0741j();
            }
            jVar = f1341b;
        }
        return jVar;
    }

    /* renamed from: b */
    public static String m1845b(Context context) {
        try {
            return LBSAuthManager.getInstance(context).getPublicKey(context);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: c */
    public static String m1846c(Context context) {
        try {
            return LBSAuthManager.getInstance(context).getMCode();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public void mo10466a(Context context) {
        this.f1343d = context;
        LBSAuthManager.getInstance(this.f1343d).authenticate(false, "lbs_locsdk", null, this);
        this.f1344e = System.currentTimeMillis();
    }

    /* renamed from: b */
    public boolean mo10467b() {
        int i = this.f1342c;
        boolean z = i == 0 || i == 602 || i == 601 || i == -10 || i == -11;
        if (this.f1343d != null) {
            long currentTimeMillis = System.currentTimeMillis() - this.f1344e;
            if (!z ? currentTimeMillis < 0 || currentTimeMillis > 10000 : currentTimeMillis > 86400000) {
                LBSAuthManager.getInstance(this.f1343d).authenticate(false, "lbs_locsdk", null, this);
                this.f1344e = System.currentTimeMillis();
            }
        }
        return z;
    }

    public void onAuthResult(int i, String str) {
        this.f1342c = i;
        if (this.f1342c == 0) {
            Log.i(C0843a.f1826a, "LocationAuthManager Authentication AUTHENTICATE_SUCC");
        } else {
            String str2 = C0843a.f1826a;
            Log.i(str2, "LocationAuthManager Authentication Error errorcode = " + i + " , msg = " + str);
        }
        if (str != null) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.getString("token") != null) {
                    this.f1345f = jSONObject.getString("token");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
