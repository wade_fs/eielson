package com.baidu.location.indoor;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import com.baidu.android.bbalbs.common.p012a.C0692b;
import com.baidu.android.bbalbs.common.p012a.C0693c;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0849e;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

/* renamed from: com.baidu.location.indoor.a */
public class C0856a extends C0849e {

    /* renamed from: a */
    private static HashMap<String, Long> f1983a = new HashMap<>();

    /* renamed from: v */
    private static Object f1984v = new Object();

    /* renamed from: w */
    private static C0856a f1985w = null;

    /* renamed from: b */
    private String f1986b = "http://loc.map.baidu.com/indoorlocbuildinginfo.php";

    /* renamed from: c */
    private final SimpleDateFormat f1987c = new SimpleDateFormat("yyyyMM");

    /* renamed from: d */
    private Context f1988d;

    /* renamed from: e */
    private boolean f1989e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public String f1990f;

    /* renamed from: q */
    private HashSet<String> f1991q;

    /* renamed from: r */
    private C0857a f1992r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public String f1993s = null;

    /* renamed from: t */
    private Handler f1994t;

    /* renamed from: u */
    private Runnable f1995u;

    /* renamed from: com.baidu.location.indoor.a$a */
    public interface C0857a {
        /* renamed from: a */
        void mo10742a(boolean z);
    }

    public C0856a(Context context) {
        this.f1988d = context;
        this.f1991q = new HashSet<>();
        this.f1989e = false;
        this.f1885k = new HashMap();
        this.f1994t = new Handler();
        this.f1995u = new C0858b(this);
    }

    /* renamed from: a */
    private String m2476a(Date date) {
        File file = new File(this.f1988d.getCacheDir(), C0693c.m1543a((this.f1990f + this.f1987c.format(date)).getBytes(), false));
        if (!file.isFile()) {
            return null;
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String str = "";
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                str = str + readLine + "\n";
            }
            bufferedReader.close();
            if (!str.equals("")) {
                return new String(C0692b.m1540a(str.getBytes()));
            }
        } catch (Exception unused) {
        }
        return null;
    }

    /* renamed from: d */
    private Date m2477d() {
        Calendar instance = Calendar.getInstance();
        instance.add(2, -1);
        return instance.getTime();
    }

    /* renamed from: d */
    private void m2478d(String str) {
        for (String str2 : str.split(",")) {
            this.f1991q.add(str2.toLowerCase());
        }
    }

    /* renamed from: e */
    private void m2479e() {
        try {
            Date d = m2477d();
            File file = new File(this.f1988d.getCacheDir(), C0693c.m1543a((this.f1990f + this.f1987c.format(d)).getBytes(), false));
            if (file.isFile()) {
                file.delete();
            }
        } catch (Exception unused) {
        }
    }

    /* renamed from: e */
    private void m2480e(String str) {
        try {
            FileWriter fileWriter = new FileWriter(new File(this.f1988d.getCacheDir(), C0693c.m1543a((this.f1990f + this.f1987c.format(new Date())).getBytes(), false)));
            fileWriter.write(C0692b.m1539a(str.getBytes(), "UTF-8"));
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException unused) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* renamed from: f */
    private void m2481f(String str) {
        try {
            FileWriter fileWriter = new FileWriter(new File(this.f1988d.getCacheDir(), "buildings"), true);
            fileWriter.write(str + "\n");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public void mo10444a() {
        this.f1882h = this.f1986b;
        this.f1885k.clear();
        this.f1885k.put("bid", "none");
        this.f1885k.put("bldg", this.f1990f);
        this.f1885k.put("mb", Build.MODEL);
        this.f1885k.put("msdk", "2.0");
        this.f1885k.put("cuid", C0844b.m2417a().f1856c);
        this.f1885k.put("anchors", "v1");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10446a(boolean r6) {
        /*
            r5 = this;
            java.lang.String r0 = "anchorinfo"
            r1 = 1
            r2 = 0
            if (r6 == 0) goto L_0x0046
            java.lang.String r6 = r5.f1884j
            if (r6 == 0) goto L_0x0046
            java.lang.String r6 = r5.f1884j     // Catch:{ Exception -> 0x0046 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0046 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0046 }
            byte[] r6 = r6.getBytes()     // Catch:{ Exception -> 0x0046 }
            byte[] r6 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r6)     // Catch:{ Exception -> 0x0046 }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0046 }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x0046 }
            r6.<init>(r3)     // Catch:{ Exception -> 0x0046 }
            boolean r3 = r6.has(r0)     // Catch:{ Exception -> 0x0046 }
            if (r3 == 0) goto L_0x0046
            java.lang.String r6 = r6.optString(r0)     // Catch:{ Exception -> 0x0046 }
            if (r6 == 0) goto L_0x0046
            java.lang.String r0 = ""
            boolean r0 = r6.equals(r0)     // Catch:{ Exception -> 0x0046 }
            if (r0 != 0) goto L_0x0046
            java.util.HashSet<java.lang.String> r0 = r5.f1991q     // Catch:{ Exception -> 0x0046 }
            r0.clear()     // Catch:{ Exception -> 0x0046 }
            r5.m2478d(r6)     // Catch:{ Exception -> 0x0046 }
            r5.m2480e(r6)     // Catch:{ Exception -> 0x0046 }
            r5.m2479e()     // Catch:{ Exception -> 0x0044 }
        L_0x0044:
            r6 = 1
            goto L_0x0047
        L_0x0046:
            r6 = 0
        L_0x0047:
            if (r6 != 0) goto L_0x005c
            java.lang.String r0 = r5.f1993s
            if (r0 != 0) goto L_0x005c
            java.lang.String r0 = r5.f1990f
            r5.f1993s = r0
            android.os.Handler r0 = r5.f1994t
            java.lang.Runnable r1 = r5.f1995u
            r3 = 60000(0xea60, double:2.9644E-319)
            r0.postDelayed(r1, r3)
            goto L_0x007d
        L_0x005c:
            r0 = 0
            if (r6 == 0) goto L_0x0062
            r5.f1993s = r0
            goto L_0x007d
        L_0x0062:
            java.lang.String r3 = r5.f1993s
            r5.m2481f(r3)
            r5.f1993s = r0
            java.util.Date r0 = r5.m2477d()
            java.lang.String r0 = r5.m2476a(r0)
            if (r0 == 0) goto L_0x007d
            r5.m2478d(r0)
            com.baidu.location.indoor.a$a r0 = r5.f1992r
            if (r0 == 0) goto L_0x007d
            r0.mo10742a(r1)
        L_0x007d:
            r5.f1989e = r2
            com.baidu.location.indoor.a$a r0 = r5.f1992r
            if (r0 == 0) goto L_0x0086
            r0.mo10742a(r6)
        L_0x0086:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.indoor.C0856a.mo10446a(boolean):void");
    }

    /* renamed from: a */
    public boolean mo10739a(String str) {
        String str2 = this.f1990f;
        return str2 != null && str2.equalsIgnoreCase(str) && !this.f1991q.isEmpty();
    }

    /* renamed from: a */
    public boolean mo10740a(String str, C0857a aVar) {
        if (!this.f1989e) {
            this.f1992r = aVar;
            this.f1989e = true;
            this.f1990f = str;
            try {
                String a = m2476a(new Date());
                if (a == null) {
                    long currentTimeMillis = System.currentTimeMillis();
                    if (f1983a.get(str) == null || currentTimeMillis - f1983a.get(str).longValue() > 86400000) {
                        f1983a.put(str, Long.valueOf(currentTimeMillis));
                        mo10733b(C0762v.m1943a().mo10505c());
                    }
                } else {
                    m2478d(a);
                    if (this.f1992r != null) {
                        this.f1992r.mo10742a(true);
                    }
                    this.f1989e = false;
                }
            } catch (Exception unused) {
                this.f1989e = false;
            }
        }
        return false;
    }

    /* renamed from: b */
    public boolean mo10508b() {
        HashSet<String> hashSet = this.f1991q;
        return hashSet != null && !hashSet.isEmpty();
    }

    /* renamed from: b */
    public boolean mo10741b(String str) {
        HashSet<String> hashSet;
        return this.f1990f != null && (hashSet = this.f1991q) != null && !hashSet.isEmpty() && this.f1991q.contains(str);
    }

    /* renamed from: c */
    public void mo10499c() {
        this.f1990f = null;
        this.f1991q.clear();
    }
}
