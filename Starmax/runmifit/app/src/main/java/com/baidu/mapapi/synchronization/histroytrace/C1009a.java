package com.baidu.mapapi.synchronization.histroytrace;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.synchronization.histroytrace.a */
final class C1009a implements Parcelable.Creator<HistoryTraceData> {
    C1009a() {
    }

    /* renamed from: a */
    public HistoryTraceData createFromParcel(Parcel parcel) {
        return new HistoryTraceData(parcel);
    }

    /* renamed from: a */
    public HistoryTraceData[] newArray(int i) {
        return new HistoryTraceData[i];
    }
}
