package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

public class TaxiInfo implements Parcelable {
    public static final Parcelable.Creator<TaxiInfo> CREATOR = new C0969m();

    /* renamed from: a */
    private float f2942a;

    /* renamed from: b */
    private String f2943b;

    /* renamed from: c */
    private int f2944c;

    /* renamed from: d */
    private int f2945d;

    /* renamed from: e */
    private float f2946e;

    /* renamed from: f */
    private float f2947f;

    public TaxiInfo() {
    }

    TaxiInfo(Parcel parcel) {
        this.f2942a = parcel.readFloat();
        this.f2943b = parcel.readString();
        this.f2944c = parcel.readInt();
        this.f2945d = parcel.readInt();
        this.f2946e = parcel.readFloat();
        this.f2947f = parcel.readFloat();
    }

    public int describeContents() {
        return 0;
    }

    public String getDesc() {
        return this.f2943b;
    }

    public int getDistance() {
        return this.f2944c;
    }

    public int getDuration() {
        return this.f2945d;
    }

    public float getPerKMPrice() {
        return this.f2946e;
    }

    public float getStartPrice() {
        return this.f2947f;
    }

    public float getTotalPrice() {
        return this.f2942a;
    }

    public void setDesc(String str) {
        this.f2943b = str;
    }

    public void setDistance(int i) {
        this.f2944c = i;
    }

    public void setDuration(int i) {
        this.f2945d = i;
    }

    public void setPerKMPrice(float f) {
        this.f2946e = f;
    }

    public void setStartPrice(float f) {
        this.f2947f = f;
    }

    public void setTotalPrice(float f) {
        this.f2942a = f;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.f2942a);
        parcel.writeString(this.f2943b);
        parcel.writeInt(this.f2944c);
        parcel.writeInt(this.f2945d);
        parcel.writeFloat(this.f2946e);
        parcel.writeFloat(this.f2947f);
    }
}
