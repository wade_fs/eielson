package com.baidu.mobstat;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import com.tencent.connect.common.Constants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.o */
public class C1297o {
    /* renamed from: a */
    public static JSONObject m4826a(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("s", Build.VERSION.SDK_INT);
            jSONObject.put("sv", Build.VERSION.RELEASE);
            jSONObject.put(Config.CUID_SEC, C1276bh.m4713a(2, context));
            jSONObject.put(Config.DEVICE_WIDTH, C1276bh.m4722c(context));
            jSONObject.put("h", C1276bh.m4726d(context));
            jSONObject.put("ly", C1239ai.f4272c);
            jSONObject.put("pv", Constants.VIA_REPORT_TYPE_START_WAP);
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                jSONObject.put(Config.PACKAGE_NAME, C1276bh.m4734h(2, context));
                jSONObject.put(Config.APP_VERSION_CODE, packageInfo.versionCode);
                jSONObject.put("n", packageInfo.versionName);
            } catch (Exception e) {
                C1255as.m4626c().mo13942a(e);
            }
            jSONObject.put("mc", C1276bh.m4719b(2, context));
            jSONObject.put(Config.DEVICE_BLUETOOTH_MAC, C1276bh.m4731f(2, context));
            jSONObject.put(Config.MODEL, Build.MODEL);
            jSONObject.put(Config.DEVICE_NAME, C1276bh.m4715a(context, 2));
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(Config.TRACE_FAILED_CNT, 0);
            jSONObject2.put("send_index", 0);
            String b = C1276bh.m4718b();
            if (b == null) {
                b = "";
            }
            jSONObject2.put(Config.ROM, b);
            jSONObject.put(Config.TRACE_PART, jSONObject2);
        } catch (JSONException e2) {
            C1255as.m4626c().mo13944b(e2);
        }
        return jSONObject;
    }

    /* renamed from: a */
    public static JSONObject m4827a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        try {
            JSONArray jSONArray = (JSONArray) jSONObject.get("payload");
            JSONObject jSONObject2 = (jSONArray == null || jSONArray.length() <= 0) ? null : (JSONObject) jSONArray.get(0);
            if (jSONObject2 != null) {
                return jSONObject2.getJSONObject(Config.HEADER_PART);
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: b */
    public static void m4828b(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(Config.TRACE_PART);
            jSONObject2.put(Config.TRACE_FAILED_CNT, jSONObject2.getLong(Config.TRACE_FAILED_CNT) + 1);
        } catch (Exception unused) {
        }
    }

    /* renamed from: c */
    public static void m4829c(JSONObject jSONObject) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(Config.TRACE_PART);
            jSONObject2.put("send_index", jSONObject2.getLong("send_index") + 1);
        } catch (Exception unused) {
        }
    }
}
