package com.baidu.location.indoor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import com.baidu.location.indoor.mapversion.C0883a;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

/* renamed from: com.baidu.location.indoor.m */
public class C0881m {

    /* renamed from: A */
    private int f2158A;

    /* renamed from: B */
    private long f2159B;

    /* renamed from: C */
    private int f2160C;

    /* renamed from: D */
    private int f2161D;

    /* renamed from: E */
    private double f2162E;

    /* renamed from: F */
    private double f2163F;

    /* renamed from: G */
    private double f2164G;

    /* renamed from: H */
    private double f2165H;

    /* renamed from: I */
    private double f2166I;

    /* renamed from: J */
    private double f2167J;

    /* renamed from: K */
    private double f2168K;

    /* renamed from: L */
    private int f2169L;

    /* renamed from: M */
    private float f2170M;
    /* access modifiers changed from: private */

    /* renamed from: N */
    public int f2171N;
    /* access modifiers changed from: private */

    /* renamed from: O */
    public int f2172O;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public double[] f2173P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public boolean f2174Q;

    /* renamed from: R */
    private double f2175R;

    /* renamed from: S */
    private String f2176S;

    /* renamed from: a */
    Timer f2177a;

    /* renamed from: b */
    public SensorEventListener f2178b;

    /* renamed from: c */
    private C0882a f2179c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public SensorManager f2180d;

    /* renamed from: e */
    private boolean f2181e;

    /* renamed from: f */
    private int f2182f;

    /* renamed from: g */
    private Sensor f2183g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public Sensor f2184h;

    /* renamed from: i */
    private Sensor f2185i;

    /* renamed from: j */
    private final long f2186j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public boolean f2187k;

    /* renamed from: l */
    private boolean f2188l;

    /* renamed from: m */
    private boolean f2189m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public volatile int f2190n;

    /* renamed from: o */
    private int f2191o;

    /* renamed from: p */
    private float[] f2192p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public float[] f2193q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public double[] f2194r;

    /* renamed from: s */
    private int f2195s;

    /* renamed from: t */
    private double[] f2196t;

    /* renamed from: u */
    private int f2197u;

    /* renamed from: v */
    private int f2198v;

    /* renamed from: w */
    private int f2199w;

    /* renamed from: x */
    private double[] f2200x;

    /* renamed from: y */
    private int f2201y;

    /* renamed from: z */
    private double f2202z;

    /* renamed from: com.baidu.location.indoor.m$a */
    public interface C0882a {
        /* renamed from: a */
        void mo10780a(double d, double d2, double d3, long j);
    }

    private C0881m(Context context, int i) {
        this.f2186j = 30;
        this.f2187k = true;
        this.f2188l = false;
        this.f2189m = false;
        this.f2190n = 1;
        this.f2191o = 1;
        this.f2192p = new float[3];
        this.f2193q = new float[]{0.0f, 0.0f, 0.0f};
        this.f2194r = new double[]{0.0d, 0.0d, 0.0d};
        this.f2195s = 31;
        this.f2196t = new double[this.f2195s];
        this.f2197u = 0;
        this.f2200x = new double[6];
        this.f2201y = 0;
        this.f2159B = 0;
        this.f2160C = 0;
        this.f2161D = 0;
        this.f2162E = 0.0d;
        this.f2163F = 0.0d;
        this.f2164G = 100.0d;
        this.f2165H = 0.5d;
        this.f2166I = this.f2165H;
        this.f2167J = 0.85d;
        this.f2168K = 0.42d;
        this.f2169L = -1;
        this.f2170M = 0.0f;
        this.f2171N = 20;
        this.f2172O = 0;
        this.f2173P = new double[this.f2171N];
        this.f2174Q = false;
        this.f2175R = -1.0d;
        this.f2176S = null;
        this.f2178b = new C0906n(this);
        this.f2202z = 1.6d;
        this.f2158A = 440;
        try {
            this.f2180d = (SensorManager) context.getSystemService("sensor");
            this.f2182f = i;
            this.f2183g = this.f2180d.getDefaultSensor(1);
            this.f2184h = this.f2180d.getDefaultSensor(3);
            if (C0883a.m2674b()) {
                this.f2185i = this.f2180d.getDefaultSensor(4);
            }
            m2652j();
        } catch (Exception unused) {
        }
    }

    public C0881m(Context context, C0882a aVar) {
        this(context, 1);
        this.f2179c = aVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public double m2630a(double d, double d2, double d3) {
        double d4 = d2 - d;
        if (d4 < -180.0d) {
            d4 += 360.0d;
        } else if (d4 > 180.0d) {
            d4 -= 360.0d;
        }
        return d + (d3 * d4);
    }

    /* renamed from: a */
    private double m2632a(double[] dArr) {
        int length = dArr.length;
        double d = 0.0d;
        double d2 = 0.0d;
        for (double d3 : dArr) {
            d2 += d3;
        }
        double d4 = (double) length;
        Double.isNaN(d4);
        double d5 = d2 / d4;
        for (int i = 0; i < length; i++) {
            d += (dArr[i] - d5) * (dArr[i] - d5);
        }
        double d6 = (double) (length - 1);
        Double.isNaN(d6);
        return d / d6;
    }

    /* renamed from: a */
    private void m2634a(double d) {
        double[] dArr = this.f2200x;
        int i = this.f2201y;
        dArr[i % 6] = d;
        this.f2201y = i + 1;
        this.f2201y %= 6;
    }

    /* renamed from: a */
    private synchronized void m2635a(int i) {
        this.f2191o = i | this.f2191o;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public float[] m2638a(float f, float f2, float f3) {
        float[] fArr = this.f2192p;
        fArr[0] = (fArr[0] * 0.8f) + (f * 0.19999999f);
        fArr[1] = (fArr[1] * 0.8f) + (f2 * 0.19999999f);
        fArr[2] = (fArr[2] * 0.8f) + (0.19999999f * f3);
        return new float[]{f - fArr[0], f2 - fArr[1], f3 - fArr[2]};
    }

    /* renamed from: b */
    static /* synthetic */ int m2641b(C0881m mVar) {
        int i = mVar.f2198v + 1;
        mVar.f2198v = i;
        return i;
    }

    /* renamed from: b */
    private boolean m2643b(double d) {
        for (int i = 1; i <= 5; i++) {
            double[] dArr = this.f2200x;
            int i2 = this.f2201y;
            if (dArr[((((i2 - 1) - i) + 6) + 6) % 6] - dArr[((i2 - 1) + 6) % 6] > d) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: f */
    static /* synthetic */ int m2647f(C0881m mVar) {
        int i = mVar.f2172O;
        mVar.f2172O = i + 1;
        return i;
    }

    /* renamed from: h */
    static /* synthetic */ int m2649h(C0881m mVar) {
        int i = mVar.f2199w + 1;
        mVar.f2199w = i;
        return i;
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public boolean m2650i() {
        for (int i = 0; i < this.f2171N; i++) {
            if (this.f2173P[i] > 1.0E-7d) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: j */
    private void m2652j() {
        try {
            List<Sensor> sensorList = this.f2180d.getSensorList(-1);
            HashMap hashMap = new HashMap();
            hashMap.put(1, 0);
            hashMap.put(10, 1);
            hashMap.put(9, 2);
            hashMap.put(4, 3);
            hashMap.put(2, 4);
            hashMap.put(11, 5);
            hashMap.put(6, 6);
            if (Build.VERSION.SDK_INT >= 18) {
                hashMap.put(14, 7);
                hashMap.put(16, 8);
            }
            int size = hashMap.size();
            char[] cArr = new char[size];
            for (int i = 0; i < size; i++) {
                cArr[i] = '0';
            }
            for (Sensor sensor : sensorList) {
                int type = sensor.getType();
                if (hashMap.get(Integer.valueOf(type)) != null) {
                    int intValue = ((Integer) hashMap.get(Integer.valueOf(type))).intValue();
                    if (intValue < size) {
                        cArr[intValue] = '1';
                    }
                }
            }
            this.f2176S = new String(cArr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: k */
    private void m2655k() {
        this.f2187k = false;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f5, code lost:
        if (r5 < r7) goto L_0x00ee;
     */
    /* renamed from: l */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m2657l() {
        /*
            r19 = this;
            r0 = r19
            int r1 = r0.f2198v
            r2 = 20
            if (r1 >= r2) goto L_0x0009
            return
        L_0x0009:
            boolean r1 = r0.f2181e
            if (r1 != 0) goto L_0x000e
            return
        L_0x000e:
            long r9 = java.lang.System.currentTimeMillis()
            r1 = 3
            float[] r2 = new float[r1]
            float[] r3 = r0.f2193q
            r4 = 0
            java.lang.System.arraycopy(r3, r4, r2, r4, r1)
            double[] r3 = new double[r1]
            double[] r5 = r0.f2194r
            java.lang.System.arraycopy(r5, r4, r3, r4, r1)
            r1 = r2[r4]
            r5 = r2[r4]
            float r1 = r1 * r5
            r5 = 1
            r6 = r2[r5]
            r7 = r2[r5]
            float r6 = r6 * r7
            float r1 = r1 + r6
            r6 = 2
            r7 = r2[r6]
            r2 = r2[r6]
            float r7 = r7 * r2
            float r1 = r1 + r7
            double r1 = (double) r1
            double r1 = java.lang.Math.sqrt(r1)
            double[] r6 = r0.f2196t
            int r7 = r0.f2197u
            r6[r7] = r1
            r0.m2634a(r1)
            int r6 = r0.f2161D
            int r6 = r6 + r5
            r0.f2161D = r6
            double r6 = r0.f2163F
            int r8 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r8 <= 0) goto L_0x0054
            r0.f2163F = r1
            goto L_0x005c
        L_0x0054:
            double r6 = r0.f2164G
            int r8 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r8 >= 0) goto L_0x005c
            r0.f2164G = r1
        L_0x005c:
            int r6 = r0.f2197u
            int r6 = r6 + r5
            r0.f2197u = r6
            int r6 = r0.f2197u
            int r7 = r0.f2195s
            if (r6 != r7) goto L_0x0087
            r0.f2197u = r4
            double[] r6 = r0.f2196t
            double r6 = r0.m2632a(r6)
            int r8 = r0.f2190n
            if (r8 != 0) goto L_0x0082
            r11 = 4599075939470750515(0x3fd3333333333333, double:0.3)
            int r8 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r8 >= 0) goto L_0x0082
            r0.m2635a(r4)
            r0.f2190n = r4
            goto L_0x0087
        L_0x0082:
            r0.m2635a(r5)
            r0.f2190n = r5
        L_0x0087:
            long r6 = r0.f2159B
            long r6 = r9 - r6
            int r8 = r0.f2158A
            long r11 = (long) r8
            int r8 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r8 <= 0) goto L_0x012f
            double r6 = r0.f2202z
            boolean r6 = r0.m2643b(r6)
            if (r6 == 0) goto L_0x012f
            int r6 = r0.f2160C
            int r6 = r6 + r5
            r0.f2160C = r6
            r0.f2159B = r9
            r6 = r3[r4]
            r11 = r3[r4]
            boolean r8 = r0.f2187k
            r13 = 0
            if (r8 == 0) goto L_0x00c9
            boolean r8 = r0.f2188l
            if (r8 == 0) goto L_0x00c9
            boolean r8 = com.baidu.location.indoor.mapversion.C0883a.m2674b()
            if (r8 == 0) goto L_0x00c9
            float[] r6 = com.baidu.location.indoor.mapversion.C0883a.m2675c()
            r6 = r6[r4]
            double r6 = (double) r6
            boolean r8 = java.lang.Double.isNaN(r6)
            if (r8 != 0) goto L_0x00c6
            int r8 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r8 >= 0) goto L_0x00c8
        L_0x00c6:
            r6 = r3[r4]
        L_0x00c8:
            r4 = 1
        L_0x00c9:
            int r3 = r0.f2161D
            r8 = 40
            if (r3 >= r8) goto L_0x00f8
            if (r3 <= 0) goto L_0x00f8
            double r13 = r0.f2163F
            r17 = r6
            double r5 = r0.f2164G
            double r13 = r13 - r5
            double r5 = java.lang.Math.sqrt(r13)
            double r5 = java.lang.Math.sqrt(r5)
            double r7 = r0.f2168K
            double r5 = r5 * r7
            r0.f2166I = r5
            double r5 = r0.f2166I
            double r7 = r0.f2167J
            int r13 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r13 <= 0) goto L_0x00f1
        L_0x00ee:
            r0.f2166I = r7
            goto L_0x00fe
        L_0x00f1:
            double r7 = r0.f2165H
            int r13 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r13 >= 0) goto L_0x00fe
            goto L_0x00ee
        L_0x00f8:
            r17 = r6
            double r5 = r0.f2165H
            r0.f2166I = r5
        L_0x00fe:
            float r5 = r0.f2170M
            double r5 = (double) r5
            java.lang.Double.isNaN(r5)
            double r6 = r17 + r5
            r13 = 4645040803167600640(0x4076800000000000, double:360.0)
            int r5 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r5 <= 0) goto L_0x0110
            double r6 = r6 - r13
        L_0x0110:
            r15 = 0
            int r5 = (r6 > r15 ? 1 : (r6 == r15 ? 0 : -1))
            if (r5 >= 0) goto L_0x0117
            double r6 = r6 + r13
        L_0x0117:
            r7 = r6
            r3 = 1
            r0.f2161D = r3
            r0.f2163F = r1
            r0.f2164G = r1
            r0.f2175R = r7
            boolean r1 = r0.f2174Q
            if (r1 != 0) goto L_0x0127
            if (r4 == 0) goto L_0x012f
        L_0x0127:
            com.baidu.location.indoor.m$a r2 = r0.f2179c
            double r3 = r0.f2166I
            r5 = r11
            r2.mo10780a(r3, r5, r7, r9)
        L_0x012f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.indoor.C0881m.m2657l():void");
    }

    /* renamed from: a */
    public void mo10783a() {
        if (!this.f2181e) {
            Sensor sensor = this.f2183g;
            if (sensor != null) {
                try {
                    this.f2180d.registerListener(this.f2178b, sensor, this.f2182f);
                } catch (Exception unused) {
                    this.f2187k = false;
                }
                this.f2177a = new Timer("UpdateData", false);
                this.f2177a.schedule(new C0907o(this), 500, 30);
                this.f2181e = true;
            }
            Sensor sensor2 = this.f2184h;
            if (sensor2 != null) {
                try {
                    this.f2180d.registerListener(this.f2178b, sensor2, this.f2182f);
                } catch (Exception unused2) {
                    this.f2187k = false;
                }
            }
        }
    }

    /* renamed from: a */
    public void mo10784a(boolean z) {
        this.f2188l = z;
        if (z && !this.f2189m) {
            m2655k();
            this.f2189m = true;
        }
    }

    /* renamed from: b */
    public void mo10785b() {
        if (this.f2181e) {
            this.f2181e = false;
            try {
                this.f2180d.unregisterListener(this.f2178b);
            } catch (Exception unused) {
            }
            this.f2177a.cancel();
            this.f2177a.purge();
            this.f2177a = null;
            this.f2189m = false;
            if (C0883a.m2674b()) {
                C0883a.m2672a();
            }
        }
    }

    /* renamed from: c */
    public synchronized int mo10786c() {
        if (this.f2198v < 20) {
            return 1;
        }
        return this.f2191o;
    }

    /* renamed from: d */
    public synchronized int mo10787d() {
        if (this.f2198v < 20) {
            return -1;
        }
        return this.f2160C;
    }

    /* renamed from: e */
    public double mo10788e() {
        return this.f2175R;
    }

    /* renamed from: f */
    public synchronized void mo10789f() {
        this.f2191o = 0;
    }

    /* renamed from: g */
    public boolean mo10790g() {
        return this.f2188l;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String mo10791h() {
        return this.f2176S;
    }
}
