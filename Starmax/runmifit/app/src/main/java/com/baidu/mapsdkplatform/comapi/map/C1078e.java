package com.baidu.mapsdkplatform.comapi.map;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import com.baidu.mapapi.UIMsg;
import com.baidu.mapapi.common.EnvironmentUtilities;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapBaseIndoorMapInfo;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.model.ParcelItem;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1084j;
import com.baidu.mapsdkplatform.comjni.map.basemap.BaseMapCallback;
import com.baidu.mapsdkplatform.comjni.map.basemap.C1177a;
import com.baidu.mapsdkplatform.comjni.map.basemap.C1178b;
import com.baidu.mapsdkplatform.comjni.map.basemap.JNIBaseMap;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.tencent.open.SocialConstants;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.e */
public class C1078e implements C1178b {

    /* renamed from: N */
    private static int f3475N;

    /* renamed from: O */
    private static int f3476O;

    /* renamed from: ar */
    private static List<JNIBaseMap> f3477ar;

    /* renamed from: d */
    public static float f3478d = 1096.0f;

    /* renamed from: m */
    static long f3479m = 0;

    /* renamed from: r */
    private static final String f3480r = C1084j.class.getSimpleName();

    /* renamed from: A */
    private boolean f3481A = false;

    /* renamed from: B */
    private C1073aj f3482B;

    /* renamed from: C */
    private C1072ai f3483C;

    /* renamed from: D */
    private Context f3484D;

    /* renamed from: E */
    private List<C1077d> f3485E;

    /* renamed from: F */
    private C1100x f3486F;

    /* renamed from: G */
    private C1081g f3487G;

    /* renamed from: H */
    private C1068ae f3488H;

    /* renamed from: I */
    private C1071ah f3489I;

    /* renamed from: J */
    private C1090n f3490J;

    /* renamed from: K */
    private C1062a f3491K;

    /* renamed from: L */
    private C1091o f3492L;

    /* renamed from: M */
    private C1069af f3493M;

    /* renamed from: P */
    private int f3494P;

    /* renamed from: Q */
    private int f3495Q;

    /* renamed from: R */
    private int f3496R;

    /* renamed from: S */
    private C1084j.C1085a f3497S = new C1084j.C1085a();

    /* renamed from: T */
    private VelocityTracker f3498T;

    /* renamed from: U */
    private long f3499U;

    /* renamed from: V */
    private long f3500V;

    /* renamed from: W */
    private long f3501W;

    /* renamed from: X */
    private long f3502X;

    /* renamed from: Y */
    private int f3503Y;

    /* renamed from: Z */
    private float f3504Z;

    /* renamed from: a */
    public float f3505a = 21.0f;

    /* renamed from: aa */
    private float f3506aa;

    /* renamed from: ab */
    private boolean f3507ab;

    /* renamed from: ac */
    private long f3508ac;

    /* renamed from: ad */
    private long f3509ad;

    /* renamed from: ae */
    private boolean f3510ae = false;

    /* renamed from: af */
    private boolean f3511af = false;

    /* renamed from: ag */
    private float f3512ag;

    /* renamed from: ah */
    private float f3513ah;

    /* renamed from: ai */
    private float f3514ai;

    /* renamed from: aj */
    private float f3515aj;

    /* renamed from: ak */
    private long f3516ak = 0;

    /* renamed from: al */
    private long f3517al = 0;

    /* renamed from: am */
    private C1080f f3518am;

    /* renamed from: an */
    private String f3519an;

    /* renamed from: ao */
    private int f3520ao;

    /* renamed from: ap */
    private C1075b f3521ap;

    /* renamed from: aq */
    private C1076c f3522aq;

    /* renamed from: as */
    private boolean f3523as = false;

    /* renamed from: at */
    private Queue<C1079a> f3524at = new LinkedList();

    /* renamed from: au */
    private boolean f3525au = false;

    /* renamed from: av */
    private boolean f3526av = false;

    /* renamed from: b */
    public float f3527b = 4.0f;

    /* renamed from: c */
    public float f3528c = 21.0f;

    /* renamed from: e */
    boolean f3529e = true;

    /* renamed from: f */
    boolean f3530f = true;

    /* renamed from: g */
    boolean f3531g = false;

    /* renamed from: h */
    List<C1087l> f3532h;

    /* renamed from: i */
    C1177a f3533i;

    /* renamed from: j */
    long f3534j;

    /* renamed from: k */
    boolean f3535k;

    /* renamed from: l */
    int f3536l;

    /* renamed from: n */
    boolean f3537n;

    /* renamed from: o */
    boolean f3538o;

    /* renamed from: p */
    boolean f3539p;

    /* renamed from: q */
    public MapStatusUpdate f3540q = null;

    /* renamed from: s */
    private boolean f3541s;

    /* renamed from: t */
    private boolean f3542t;

    /* renamed from: u */
    private boolean f3543u = true;

    /* renamed from: v */
    private boolean f3544v = false;

    /* renamed from: w */
    private boolean f3545w = false;

    /* renamed from: x */
    private boolean f3546x = false;

    /* renamed from: y */
    private boolean f3547y = true;

    /* renamed from: z */
    private boolean f3548z = true;

    /* renamed from: com.baidu.mapsdkplatform.comapi.map.e$a */
    public static class C1079a {

        /* renamed from: a */
        public long f3549a;

        /* renamed from: b */
        public int f3550b;

        /* renamed from: c */
        public int f3551c;

        /* renamed from: d */
        public int f3552d;

        /* renamed from: e */
        public Bundle f3553e;

        public C1079a(long j, int i, int i2, int i3) {
            this.f3549a = j;
            this.f3550b = i;
            this.f3551c = i2;
            this.f3552d = i3;
        }

        public C1079a(Bundle bundle) {
            this.f3553e = bundle;
        }
    }

    public C1078e(Context context, String str, int i) {
        this.f3484D = context;
        this.f3532h = new ArrayList();
        this.f3519an = str;
        this.f3520ao = i;
    }

    /* renamed from: R */
    private void m3533R() {
        if (this.f3545w || this.f3542t || this.f3541s || this.f3546x) {
            if (this.f3505a > 20.0f) {
                this.f3505a = 20.0f;
            }
            if (mo12989E().f3414a > 20.0f) {
                C1064ab E = mo12989E();
                E.f3414a = 20.0f;
                mo13015a(E);
                return;
            }
            return;
        }
        this.f3505a = this.f3528c;
    }

    /* renamed from: S */
    private void m3534S() {
        if (!this.f3537n) {
            this.f3537n = true;
            this.f3538o = false;
            List<C1087l> list = this.f3532h;
            if (list != null) {
                for (C1087l lVar : list) {
                    if (lVar != null) {
                        lVar.mo11542a(mo12989E());
                    }
                }
            }
        }
    }

    /* renamed from: T */
    private boolean m3535T() {
        if (this.f3533i == null || !this.f3535k) {
            return true;
        }
        this.f3511af = false;
        if (!this.f3529e) {
            return false;
        }
        float f = (float) (this.f3517al - this.f3516ak);
        float abs = (Math.abs(this.f3514ai - this.f3512ag) * 1000.0f) / f;
        float abs2 = (Math.abs(this.f3515aj - this.f3513ah) * 1000.0f) / f;
        float sqrt = (float) Math.sqrt((double) ((abs * abs) + (abs2 * abs2)));
        if (sqrt <= 500.0f) {
            return false;
        }
        mo12985A();
        mo13003a(34, (int) (sqrt * 0.6f), (((int) this.f3515aj) << 16) | ((int) this.f3514ai));
        mo12997M();
        return true;
    }

    /* renamed from: a */
    private Activity m3536a(Context context) {
        if (context == null) {
            return null;
        }
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return m3536a(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    /* renamed from: a */
    private void m3537a(C1077d dVar) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            dVar.f3471a = aVar.mo13469a(dVar.f3473c, dVar.f3474d, dVar.f3472b);
            this.f3485E.add(dVar);
        }
    }

    /* renamed from: a */
    private void m3538a(String str, String str2, long j) {
        try {
            Class<?> cls = Class.forName(str);
            Object newInstance = cls.newInstance();
            cls.getMethod(str2, Long.TYPE).invoke(newInstance, Long.valueOf(j));
        } catch (Exception unused) {
        }
    }

    /* renamed from: b */
    private void m3539b(MotionEvent motionEvent) {
        if (!this.f3497S.f3578e) {
            this.f3509ad = motionEvent.getDownTime();
            long j = this.f3509ad;
            if (j - this.f3508ac < 400) {
                j = (Math.abs(motionEvent.getX() - this.f3504Z) >= 120.0f || Math.abs(motionEvent.getY() - this.f3506aa) >= 120.0f) ? this.f3509ad : 0;
            }
            this.f3508ac = j;
            this.f3504Z = motionEvent.getX();
            this.f3506aa = motionEvent.getY();
            mo13003a(4, 0, (((int) motionEvent.getY()) << 16) | ((int) motionEvent.getX()));
            this.f3507ab = true;
        }
    }

    /* renamed from: b */
    private void m3540b(String str, Bundle bundle) {
        if (this.f3533i != null) {
            this.f3487G.mo12976a(str);
            this.f3487G.mo12975a(bundle);
            this.f3533i.mo13489b(this.f3487G.f3471a);
        }
    }

    /* renamed from: c */
    private boolean m3541c(MotionEvent motionEvent) {
        if (this.f3497S.f3578e || System.currentTimeMillis() - f3479m < 300) {
            return true;
        }
        if (this.f3539p) {
            List<C1087l> list = this.f3532h;
            if (list != null) {
                for (C1087l lVar : list) {
                    GeoPoint b = mo13030b((int) motionEvent.getX(), (int) motionEvent.getY());
                    if (lVar != null) {
                        lVar.mo11555d(b);
                    }
                }
            }
            return true;
        }
        float abs = Math.abs(motionEvent.getX() - this.f3504Z);
        float abs2 = Math.abs(motionEvent.getY() - this.f3506aa);
        int i = (((double) SysOSUtil.getDensity()) > 1.5d ? 1 : (((double) SysOSUtil.getDensity()) == 1.5d ? 0 : -1));
        double density = (double) SysOSUtil.getDensity();
        if (i > 0) {
            Double.isNaN(density);
            density *= 1.5d;
        }
        float f = (float) density;
        if (this.f3507ab && abs / f <= 3.0f && abs2 / f <= 3.0f) {
            return true;
        }
        this.f3507ab = false;
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (x < 0) {
            x = 0;
        }
        if (y < 0) {
            y = 0;
        }
        if (this.f3529e) {
            BaiduMap.mapStatusReason = 1 | BaiduMap.mapStatusReason;
            m3534S();
            mo13003a(3, 0, (y << 16) | x);
        }
        return false;
    }

    /* renamed from: d */
    private boolean m3542d(MotionEvent motionEvent) {
        if (this.f3539p) {
            List<C1087l> list = this.f3532h;
            if (list != null) {
                for (C1087l lVar : list) {
                    GeoPoint b = mo13030b((int) motionEvent.getX(), (int) motionEvent.getY());
                    if (lVar != null) {
                        lVar.mo11557e(b);
                    }
                }
            }
            this.f3539p = false;
            return true;
        }
        boolean z = !this.f3497S.f3578e && motionEvent.getEventTime() - this.f3509ad < 400 && Math.abs(motionEvent.getX() - this.f3504Z) < 10.0f && Math.abs(motionEvent.getY() - this.f3506aa) < 10.0f;
        mo12997M();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (z) {
            return false;
        }
        if (x < 0) {
            x = 0;
        }
        if (y < 0) {
            y = 0;
        }
        mo13003a(5, 0, (y << 16) | x);
        return true;
    }

    /* renamed from: e */
    private boolean m3543e(float f, float f2) {
        if (this.f3533i == null || !this.f3535k) {
            return true;
        }
        this.f3510ae = false;
        GeoPoint b = mo13030b((int) f, (int) f2);
        if (b != null) {
            List<C1087l> list = this.f3532h;
            if (list != null) {
                for (C1087l lVar : list) {
                    if (lVar != null) {
                        lVar.mo11548b(b);
                    }
                }
            }
            if (this.f3530f) {
                C1064ab E = mo12989E();
                E.f3414a += 1.0f;
                E.f3417d = b.getLongitudeE6();
                E.f3418e = b.getLatitudeE6();
                mo13016a(E, 300);
                f3479m = System.currentTimeMillis();
                return true;
            }
        }
        return false;
    }

    /* renamed from: e */
    private boolean m3544e(Bundle bundle) {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return false;
        }
        return aVar.mo13509e(bundle);
    }

    /* renamed from: f */
    private boolean m3545f(Bundle bundle) {
        C1177a aVar;
        if (bundle == null || (aVar = this.f3533i) == null) {
            return false;
        }
        boolean d = aVar.mo13504d(bundle);
        if (d) {
            mo13049f(d);
            this.f3533i.mo13489b(this.f3482B.f3471a);
        }
        return d;
    }

    /* renamed from: g */
    private void m3546g(Bundle bundle) {
        int i;
        int i2;
        if (bundle.get("param") == null ? (i = bundle.getInt(SocialConstants.PARAM_TYPE)) != C1082h.ground.ordinal() && i < C1082h.arc.ordinal() : (i2 = (bundle = (Bundle) bundle.get("param")).getInt(SocialConstants.PARAM_TYPE)) != C1082h.ground.ordinal() && i2 < C1082h.arc.ordinal()) {
            int ordinal = C1082h.popup.ordinal();
        }
        bundle.putLong("layer_addr", this.f3489I.f3471a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapsdkplatform.comjni.map.basemap.a.b(long, boolean):void
     arg types: [int, boolean]
     candidates:
      com.baidu.mapsdkplatform.comjni.map.basemap.a.b(int, int):java.lang.String
      com.baidu.mapsdkplatform.comjni.map.basemap.a.b(int, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.b(long, boolean):void */
    /* renamed from: m */
    static void m3547m(boolean z) {
        f3477ar = C1177a.m4291b();
        List<JNIBaseMap> list = f3477ar;
        if (list == null || list.size() == 0) {
            C1177a.m4292b(0L, z);
            return;
        }
        C1177a.m4292b(f3477ar.get(0).f3940a, z);
        for (JNIBaseMap jNIBaseMap : f3477ar) {
            if (jNIBaseMap != null) {
                jNIBaseMap.ClearLayer(jNIBaseMap.f3940a, -1);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: A */
    public void mo12985A() {
        if (!this.f3537n && !this.f3538o) {
            this.f3538o = true;
            List<C1087l> list = this.f3532h;
            if (list != null) {
                for (C1087l lVar : list) {
                    if (lVar != null) {
                        lVar.mo11542a(mo12989E());
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: B */
    public void mo12986B() {
        this.f3538o = false;
        this.f3537n = false;
        List<C1087l> list = this.f3532h;
        if (list != null) {
            for (C1087l lVar : list) {
                if (lVar != null) {
                    lVar.mo11553c(mo12989E());
                }
            }
        }
    }

    /* renamed from: C */
    public boolean mo12987C() {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            return aVar.mo13482a(this.f3488H.f3471a);
        }
        return false;
    }

    /* renamed from: D */
    public boolean mo12988D() {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            return aVar.mo13482a(this.f3522aq.f3471a);
        }
        return false;
    }

    /* renamed from: E */
    public C1064ab mo12989E() {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return null;
        }
        Bundle h = aVar.mo13514h();
        C1064ab abVar = new C1064ab();
        abVar.mo12950a(h);
        return abVar;
    }

    /* renamed from: F */
    public LatLngBounds mo12990F() {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return null;
        }
        Bundle i = aVar.mo13516i();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        int i2 = i.getInt("maxCoorx");
        int i3 = i.getInt("minCoorx");
        builder.include(CoordUtil.mc2ll(new GeoPoint((double) i.getInt("minCoory"), (double) i2))).include(CoordUtil.mc2ll(new GeoPoint((double) i.getInt("maxCoory"), (double) i3)));
        return builder.build();
    }

    /* renamed from: G */
    public MapStatusUpdate mo12991G() {
        return this.f3540q;
    }

    /* renamed from: H */
    public int mo12992H() {
        return this.f3494P;
    }

    /* renamed from: I */
    public int mo12993I() {
        return this.f3495Q;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: J */
    public C1064ab mo12994J() {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return null;
        }
        Bundle j = aVar.mo13517j();
        C1064ab abVar = new C1064ab();
        abVar.mo12950a(j);
        return abVar;
    }

    /* renamed from: K */
    public double mo12995K() {
        return mo12989E().f3426m;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: L */
    public void mo12996L() {
        List<C1087l> list;
        this.f3537n = false;
        if (!this.f3538o && (list = this.f3532h) != null) {
            for (C1087l lVar : list) {
                if (lVar != null) {
                    lVar.mo11553c(mo12989E());
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: M */
    public void mo12997M() {
        this.f3496R = 0;
        C1084j.C1085a aVar = this.f3497S;
        aVar.f3578e = false;
        aVar.f3581h = 0.0d;
    }

    /* renamed from: N */
    public float[] mo12998N() {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return null;
        }
        return aVar.mo13528u();
    }

    /* renamed from: O */
    public float[] mo12999O() {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return null;
        }
        return aVar.mo13529v();
    }

    /* renamed from: P */
    public Queue<C1079a> mo13000P() {
        return this.f3524at;
    }

    /* renamed from: Q */
    public void mo13001Q() {
        if (!this.f3524at.isEmpty()) {
            C1079a poll = this.f3524at.poll();
            if (poll.f3553e == null) {
                C1177a.m4290a(poll.f3549a, poll.f3550b, poll.f3551c, poll.f3552d);
            } else if (this.f3533i != null) {
                mo12985A();
                this.f3533i.mo13475a(poll.f3553e);
            }
        }
    }

    /* renamed from: a */
    public float mo13002a(int i, int i2, int i3, int i4, int i5, int i6) {
        if (!this.f3535k) {
            return 12.0f;
        }
        if (this.f3533i == null) {
            return 0.0f;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("left", i);
        bundle.putInt("right", i3);
        bundle.putInt("bottom", i4);
        bundle.putInt("top", i2);
        bundle.putInt("hasHW", 1);
        bundle.putInt("width", i5);
        bundle.putInt("height", i6);
        return this.f3533i.mo13495c(bundle);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo13003a(int i, int i2, int i3) {
        if (!this.f3523as) {
            return C1177a.m4290a(this.f3534j, i, i2, i3);
        }
        this.f3524at.add(new C1079a(this.f3534j, i, i2, i3));
        return 0;
    }

    /* renamed from: a */
    public int mo13004a(Bundle bundle, long j, int i, Bundle bundle2) {
        if (j == this.f3487G.f3471a) {
            bundle.putString("jsondata", this.f3487G.mo12974a());
            bundle.putBundle("param", this.f3487G.mo12977b());
            return this.f3487G.f3456g;
        } else if (j == this.f3486F.f3471a) {
            bundle.putString("jsondata", this.f3486F.mo12974a());
            bundle.putBundle("param", this.f3486F.mo12977b());
            return this.f3486F.f3456g;
        } else if (j == this.f3490J.f3471a) {
            bundle.putBundle("param", this.f3492L.mo11559a(bundle2.getInt(Config.EVENT_HEAT_X), bundle2.getInt("y"), bundle2.getInt("zoom")));
            return this.f3490J.f3456g;
        } else if (j != this.f3482B.f3471a) {
            return 0;
        } else {
            bundle.putBundle("param", this.f3483C.mo11560a(bundle2.getInt(Config.EVENT_HEAT_X), bundle2.getInt("y"), bundle2.getInt("zoom"), this.f3484D));
            return this.f3482B.f3456g;
        }
    }

    /* renamed from: a */
    public Point mo13005a(GeoPoint geoPoint) {
        return this.f3493M.mo12972a(geoPoint);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13006a() {
        this.f3485E = new ArrayList();
        this.f3518am = new C1080f();
        m3537a(this.f3518am);
        this.f3521ap = new C1075b();
        m3537a(this.f3521ap);
        this.f3490J = new C1090n();
        m3537a(this.f3490J);
        this.f3491K = new C1062a();
        m3537a(this.f3491K);
        m3537a(new C1092p());
        this.f3488H = new C1068ae();
        m3537a(this.f3488H);
        this.f3522aq = new C1076c();
        m3537a(this.f3522aq);
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13508e(false);
        }
        this.f3489I = new C1071ah();
        m3537a(this.f3489I);
        this.f3487G = new C1081g();
        m3537a(this.f3487G);
        this.f3486F = new C1100x();
        m3537a(this.f3486F);
    }

    /* renamed from: a */
    public void mo13007a(float f, float f2) {
        this.f3505a = f;
        this.f3528c = f;
        this.f3527b = f2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13008a(int i) {
        this.f3533i = new C1177a();
        this.f3533i.mo13479a(i);
        this.f3534j = this.f3533i.mo13468a();
        m3538a("com.baidu.platform.comapi.wnplatform.walkmap.WNaviBaiduMap", "setId", this.f3534j);
        this.f3536l = SysOSUtil.getDensityDpi() < 180 ? 18 : SysOSUtil.getDensityDpi() < 240 ? 25 : SysOSUtil.getDensityDpi() < 320 ? 37 : 50;
        String moduleFileName = SysOSUtil.getModuleFileName();
        String appSDCardPath = EnvironmentUtilities.getAppSDCardPath();
        String appCachePath = EnvironmentUtilities.getAppCachePath();
        String appSecondCachePath = EnvironmentUtilities.getAppSecondCachePath();
        int mapTmpStgMax = EnvironmentUtilities.getMapTmpStgMax();
        int domTmpStgMax = EnvironmentUtilities.getDomTmpStgMax();
        int itsTmpStgMax = EnvironmentUtilities.getItsTmpStgMax();
        String str = SysOSUtil.getDensityDpi() >= 180 ? "/h/" : "/l/";
        String str2 = moduleFileName + "/cfg";
        String str3 = appSDCardPath + "/vmp";
        String str4 = str2 + "/a/";
        String str5 = str2 + "/a/";
        String str6 = str2 + "/idrres/";
        String str7 = str3 + str;
        String str8 = str3 + str;
        String str9 = appCachePath + "/tmp/";
        String str10 = appSecondCachePath + "/tmp/";
        Activity a = m3536a(this.f3484D);
        if (a != null) {
            Display defaultDisplay = a.getWindowManager().getDefaultDisplay();
            this.f3533i.mo13485a(str4, str7, str9, str10, str8, str5, this.f3519an, this.f3520ao, str6, defaultDisplay.getWidth(), defaultDisplay.getHeight(), SysOSUtil.getDensityDpi(), mapTmpStgMax, domTmpStgMax, itsTmpStgMax, 0);
            return;
        }
        throw new RuntimeException("BDMapSDKException: Please give the right context.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13009a(int i, int i2) {
        this.f3494P = i;
        this.f3495Q = i2;
    }

    /* renamed from: a */
    public void mo13010a(long j, long j2, long j3, long j4, boolean z) {
        this.f3533i.mo13473a(j, j2, j3, j4, z);
    }

    /* renamed from: a */
    public void mo13011a(Bitmap bitmap) {
        Bundle bundle;
        if (this.f3533i != null) {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject.put(SocialConstants.PARAM_TYPE, 0);
                jSONObject2.put(Config.EVENT_HEAT_X, f3475N);
                jSONObject2.put("y", f3476O);
                jSONObject2.put("hidetime", 1000);
                jSONArray.put(jSONObject2);
                jSONObject.put("data", jSONArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (bitmap == null) {
                bundle = null;
            } else {
                Bundle bundle2 = new Bundle();
                ArrayList arrayList = new ArrayList();
                ParcelItem parcelItem = new ParcelItem();
                Bundle bundle3 = new Bundle();
                ByteBuffer allocate = ByteBuffer.allocate(bitmap.getWidth() * bitmap.getHeight() * 4);
                bitmap.copyPixelsToBuffer(allocate);
                bundle3.putByteArray("imgdata", allocate.array());
                bundle3.putInt("imgindex", bitmap.hashCode());
                bundle3.putInt("imgH", bitmap.getHeight());
                bundle3.putInt("imgW", bitmap.getWidth());
                bundle3.putInt("hasIcon", 1);
                parcelItem.setBundle(bundle3);
                arrayList.add(parcelItem);
                if (arrayList.size() > 0) {
                    ParcelItem[] parcelItemArr = new ParcelItem[arrayList.size()];
                    for (int i = 0; i < arrayList.size(); i++) {
                        parcelItemArr[i] = (ParcelItem) arrayList.get(i);
                    }
                    bundle2.putParcelableArray("icondata", parcelItemArr);
                }
                bundle = bundle2;
            }
            m3540b(jSONObject.toString(), bundle);
            this.f3533i.mo13489b(this.f3487G.f3471a);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13012a(Handler handler) {
        MessageCenter.registMessage(UIMsg.m_AppUI.MSG_APP_SAVESCREEN, handler);
        MessageCenter.registMessage(39, handler);
        MessageCenter.registMessage(41, handler);
        MessageCenter.registMessage(49, handler);
        MessageCenter.registMessage(65289, handler);
        MessageCenter.registMessage(50, handler);
        MessageCenter.registMessage(999, handler);
        BaseMapCallback.addLayerDataInterface(this.f3534j, this);
    }

    /* renamed from: a */
    public void mo13013a(MapStatusUpdate mapStatusUpdate) {
        this.f3540q = mapStatusUpdate;
    }

    /* renamed from: a */
    public void mo13014a(LatLngBounds latLngBounds) {
        if (latLngBounds != null && this.f3533i != null) {
            LatLng latLng = latLngBounds.northeast;
            LatLng latLng2 = latLngBounds.southwest;
            GeoPoint ll2mc = CoordUtil.ll2mc(latLng);
            GeoPoint ll2mc2 = CoordUtil.ll2mc(latLng2);
            int longitudeE6 = (int) ll2mc2.getLongitudeE6();
            int latitudeE6 = (int) ll2mc.getLatitudeE6();
            Bundle bundle = new Bundle();
            bundle.putInt("maxCoorx", (int) ll2mc.getLongitudeE6());
            bundle.putInt("minCoory", (int) ll2mc2.getLatitudeE6());
            bundle.putInt("minCoorx", longitudeE6);
            bundle.putInt("maxCoory", latitudeE6);
            this.f3533i.mo13490b(bundle);
        }
    }

    /* renamed from: a */
    public void mo13015a(C1064ab abVar) {
        if (this.f3533i != null && abVar != null) {
            Bundle a = abVar.mo12949a(this);
            a.putInt("animation", 0);
            a.putInt("animatime", 0);
            this.f3533i.mo13475a(a);
        }
    }

    /* renamed from: a */
    public void mo13016a(C1064ab abVar, int i) {
        if (this.f3533i != null && abVar != null) {
            Bundle a = abVar.mo12949a(this);
            a.putInt("animation", 1);
            a.putInt("animatime", i);
            if (this.f3523as) {
                this.f3524at.add(new C1079a(a));
                return;
            }
            mo12985A();
            this.f3533i.mo13475a(a);
        }
    }

    /* renamed from: a */
    public void mo13017a(C1072ai aiVar) {
        this.f3483C = aiVar;
    }

    /* renamed from: a */
    public void mo13018a(C1087l lVar) {
        List<C1087l> list;
        if (lVar != null && (list = this.f3532h) != null) {
            list.add(lVar);
        }
    }

    /* renamed from: a */
    public void mo13019a(C1091o oVar) {
        this.f3492L = oVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, android.os.Bundle):void
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, long):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, java.lang.String):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(boolean, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13020a(C1102z zVar) {
        new C1064ab();
        if (zVar == null) {
            zVar = new C1102z();
        }
        C1064ab abVar = zVar.f3625a;
        this.f3547y = zVar.f3630f;
        this.f3548z = zVar.f3628d;
        this.f3529e = zVar.f3629e;
        this.f3530f = zVar.f3631g;
        this.f3533i.mo13475a(abVar.mo12949a(this));
        this.f3533i.mo13496c(C1101y.DEFAULT.ordinal());
        this.f3543u = zVar.f3626b;
        if (!zVar.f3626b) {
            this.f3533i.mo13474a(this.f3487G.f3471a, false);
        } else {
            f3475N = (int) (SysOSUtil.getDensity() * 40.0f);
            f3476O = (int) (SysOSUtil.getDensity() * 40.0f);
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put(Config.EVENT_HEAT_X, f3475N);
                jSONObject2.put("y", f3476O);
                jSONObject2.put("hidetime", 1000);
                jSONArray.put(jSONObject2);
                jSONObject.put("data", jSONArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.f3487G.mo12976a(jSONObject.toString());
            this.f3533i.mo13474a(this.f3487G.f3471a, true);
        }
        int i = zVar.f3627c;
        if (i == 2) {
            mo13023a(true);
        }
        if (i == 3) {
            this.f3533i.mo13474a(this.f3518am.f3471a, false);
            this.f3533i.mo13474a(this.f3522aq.f3471a, false);
            this.f3533i.mo13474a(this.f3488H.f3471a, false);
            this.f3533i.mo13508e(false);
        }
    }

    /* renamed from: a */
    public void mo13021a(String str, Bundle bundle) {
        if (this.f3533i != null) {
            this.f3486F.mo12976a(str);
            this.f3486F.mo12975a(bundle);
            this.f3533i.mo13489b(this.f3486F.f3471a);
        }
    }

    /* renamed from: a */
    public void mo13022a(List<Bundle> list) {
        if (this.f3533i != null && list != null) {
            int size = list.size();
            Bundle[] bundleArr = new Bundle[list.size()];
            for (int i = 0; i < size; i++) {
                m3546g(list.get(i));
                bundleArr[i] = list.get(i);
            }
            this.f3533i.mo13478a(bundleArr);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, android.os.Bundle):void
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, long):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, java.lang.String):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(boolean, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void */
    /* renamed from: a */
    public void mo13023a(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            if (!aVar.mo13482a(this.f3518am.f3471a)) {
                this.f3533i.mo13474a(this.f3518am.f3471a, true);
            }
            this.f3542t = z;
            m3533R();
            this.f3533i.mo13477a(this.f3542t);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:81:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0248  */
    /* JADX WARNING: Removed duplicated region for block: B:88:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo13024a(float r23, float r24, float r25, float r26) {
        /*
            r22 = this;
            r0 = r22
            r1 = r23
            r2 = r25
            int r3 = r0.f3495Q
            float r4 = (float) r3
            float r4 = r4 - r24
            float r3 = (float) r3
            float r3 = r3 - r26
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            boolean r5 = r5.f3578e
            r9 = 1
            if (r5 != r9) goto L_0x022d
            int r5 = r0.f3496R
            r13 = 4640537203540230144(0x4066800000000000, double:180.0)
            r15 = 4666723172467343360(0x40c3880000000000, double:10000.0)
            r17 = 4611686018427387904(0x4000000000000000, double:2.0)
            r19 = 0
            if (r5 != 0) goto L_0x00bc
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3576c
            float r5 = r5 - r4
            int r5 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r5 <= 0) goto L_0x0039
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3577d
            float r5 = r5 - r3
            int r5 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r5 > 0) goto L_0x004b
        L_0x0039:
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3576c
            float r5 = r5 - r4
            int r5 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r5 >= 0) goto L_0x00b3
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3577d
            float r5 = r5 - r3
            int r5 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r5 >= 0) goto L_0x00b3
        L_0x004b:
            float r5 = r3 - r4
            double r8 = (double) r5
            float r10 = r2 - r1
            double r6 = (double) r10
            double r6 = java.lang.Math.atan2(r8, r6)
            com.baidu.mapsdkplatform.comapi.map.j$a r8 = r0.f3497S
            float r8 = r8.f3577d
            com.baidu.mapsdkplatform.comapi.map.j$a r9 = r0.f3497S
            float r9 = r9.f3576c
            float r8 = r8 - r9
            double r8 = (double) r8
            com.baidu.mapsdkplatform.comapi.map.j$a r11 = r0.f3497S
            float r11 = r11.f3575b
            com.baidu.mapsdkplatform.comapi.map.j$a r12 = r0.f3497S
            float r12 = r12.f3574a
            float r11 = r11 - r12
            double r11 = (double) r11
            double r8 = java.lang.Math.atan2(r8, r11)
            double r6 = r6 - r8
            float r10 = r10 * r10
            float r5 = r5 * r5
            float r10 = r10 + r5
            double r8 = (double) r10
            double r8 = java.lang.Math.sqrt(r8)
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            double r10 = r5.f3581h
            double r8 = r8 / r10
            double r10 = java.lang.Math.log(r8)
            double r20 = java.lang.Math.log(r17)
            double r10 = r10 / r20
            double r10 = r10 * r15
            int r5 = (int) r10
            double r6 = r6 * r13
            r10 = 4614256673094690983(0x400921ff2e48e8a7, double:3.1416)
            double r6 = r6 / r10
            int r6 = (int) r6
            r10 = 0
            int r7 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r7 <= 0) goto L_0x00a1
            r7 = 3000(0xbb8, float:4.204E-42)
            if (r5 > r7) goto L_0x00a9
            r7 = -3000(0xfffffffffffff448, float:NaN)
            if (r5 < r7) goto L_0x00a9
        L_0x00a1:
            int r5 = java.lang.Math.abs(r6)
            r6 = 10
            if (r5 < r6) goto L_0x00ae
        L_0x00a9:
            r5 = 2
            r0.f3496R = r5
            r6 = 1
            goto L_0x00b7
        L_0x00ae:
            r5 = 2
            r6 = 1
            r0.f3496R = r6
            goto L_0x00b7
        L_0x00b3:
            r5 = 2
            r6 = 1
            r0.f3496R = r5
        L_0x00b7:
            int r5 = r0.f3496R
            if (r5 != 0) goto L_0x00bd
            return r6
        L_0x00bc:
            r6 = 1
        L_0x00bd:
            int r5 = r0.f3496R
            r7 = 0
            if (r5 != r6) goto L_0x00fb
            boolean r5 = r0.f3547y
            if (r5 == 0) goto L_0x00fb
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3576c
            float r5 = r5 - r4
            int r5 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r5 <= 0) goto L_0x00e3
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3577d
            float r5 = r5 - r3
            int r5 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r5 <= 0) goto L_0x00e3
            r22.m3534S()
            r5 = 83
        L_0x00dd:
            r6 = 1
            r0.mo13003a(r6, r5, r7)
            goto L_0x022d
        L_0x00e3:
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3576c
            float r5 = r5 - r4
            int r5 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r5 >= 0) goto L_0x022d
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3577d
            float r5 = r5 - r3
            int r5 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r5 >= 0) goto L_0x022d
            r22.m3534S()
            r5 = 87
            goto L_0x00dd
        L_0x00fb:
            int r5 = r0.f3496R
            r6 = 4
            r8 = 3
            r9 = 2
            if (r5 == r9) goto L_0x0106
            if (r5 == r6) goto L_0x0106
            if (r5 != r8) goto L_0x022d
        L_0x0106:
            float r5 = r3 - r4
            double r9 = (double) r5
            float r11 = r2 - r1
            double r6 = (double) r11
            double r6 = java.lang.Math.atan2(r9, r6)
            com.baidu.mapsdkplatform.comapi.map.j$a r9 = r0.f3497S
            float r9 = r9.f3577d
            com.baidu.mapsdkplatform.comapi.map.j$a r10 = r0.f3497S
            float r10 = r10.f3576c
            float r9 = r9 - r10
            double r9 = (double) r9
            com.baidu.mapsdkplatform.comapi.map.j$a r12 = r0.f3497S
            float r12 = r12.f3575b
            com.baidu.mapsdkplatform.comapi.map.j$a r8 = r0.f3497S
            float r8 = r8.f3574a
            float r12 = r12 - r8
            double r13 = (double) r12
            double r8 = java.lang.Math.atan2(r9, r13)
            double r6 = r6 - r8
            float r11 = r11 * r11
            float r5 = r5 * r5
            float r11 = r11 + r5
            double r8 = (double) r11
            double r8 = java.lang.Math.sqrt(r8)
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            double r10 = r5.f3581h
            double r8 = r8 / r10
            double r10 = java.lang.Math.log(r8)
            double r12 = java.lang.Math.log(r17)
            double r10 = r10 / r12
            double r10 = r10 * r15
            int r5 = (int) r10
            com.baidu.mapsdkplatform.comapi.map.j$a r10 = r0.f3497S
            float r10 = r10.f3580g
            com.baidu.mapsdkplatform.comapi.map.j$a r11 = r0.f3497S
            float r11 = r11.f3576c
            float r10 = r10 - r11
            double r10 = (double) r10
            com.baidu.mapsdkplatform.comapi.map.j$a r12 = r0.f3497S
            float r12 = r12.f3579f
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3574a
            float r12 = r12 - r13
            double r12 = (double) r12
            double r10 = java.lang.Math.atan2(r10, r12)
            com.baidu.mapsdkplatform.comapi.map.j$a r12 = r0.f3497S
            float r12 = r12.f3579f
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3574a
            float r12 = r12 - r13
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3579f
            com.baidu.mapsdkplatform.comapi.map.j$a r14 = r0.f3497S
            float r14 = r14.f3574a
            float r13 = r13 - r14
            float r12 = r12 * r13
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3580g
            com.baidu.mapsdkplatform.comapi.map.j$a r14 = r0.f3497S
            float r14 = r14.f3576c
            float r13 = r13 - r14
            com.baidu.mapsdkplatform.comapi.map.j$a r14 = r0.f3497S
            float r14 = r14.f3580g
            com.baidu.mapsdkplatform.comapi.map.j$a r15 = r0.f3497S
            float r15 = r15.f3576c
            float r14 = r14 - r15
            float r13 = r13 * r14
            float r12 = r12 + r13
            double r12 = (double) r12
            double r12 = java.lang.Math.sqrt(r12)
            double r10 = r10 + r6
            double r14 = java.lang.Math.cos(r10)
            double r14 = r14 * r12
            double r14 = r14 * r8
            r16 = r3
            double r2 = (double) r1
            java.lang.Double.isNaN(r2)
            double r14 = r14 + r2
            float r2 = (float) r14
            double r10 = java.lang.Math.sin(r10)
            double r12 = r12 * r10
            double r12 = r12 * r8
            double r10 = (double) r4
            java.lang.Double.isNaN(r10)
            double r12 = r12 + r10
            float r3 = (float) r12
            r10 = 4640537203540230144(0x4066800000000000, double:180.0)
            double r6 = r6 * r10
            r10 = 4614256673094690983(0x400921ff2e48e8a7, double:3.1416)
            double r6 = r6 / r10
            int r6 = (int) r6
            r7 = 8193(0x2001, float:1.1481E-41)
            r10 = 0
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 <= 0) goto L_0x0200
            int r10 = r0.f3496R
            r11 = 3
            if (r11 == r10) goto L_0x01d1
            int r10 = java.lang.Math.abs(r5)
            r12 = 2000(0x7d0, float:2.803E-42)
            if (r10 <= r12) goto L_0x0200
            int r10 = r0.f3496R
            r12 = 2
            if (r12 != r10) goto L_0x0200
        L_0x01d1:
            r0.f3496R = r11
            com.baidu.mapsdkplatform.comapi.map.ab r6 = r22.mo12989E()
            float r6 = r6.f3414a
            boolean r10 = r0.f3530f
            if (r10 == 0) goto L_0x0226
            r10 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 <= 0) goto L_0x01f0
            float r8 = r0.f3505a
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 < 0) goto L_0x01eb
            r8 = 0
            return r8
        L_0x01eb:
            r22.m3534S()
            r9 = 3
            goto L_0x01fc
        L_0x01f0:
            r8 = 0
            r9 = 3
            float r10 = r0.f3527b
            int r6 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r6 > 0) goto L_0x01f9
            return r8
        L_0x01f9:
            r22.m3534S()
        L_0x01fc:
            r0.mo13003a(r7, r9, r5)
            goto L_0x0226
        L_0x0200:
            if (r6 == 0) goto L_0x0226
            int r5 = r0.f3496R
            r8 = 4
            if (r8 == r5) goto L_0x0214
            int r5 = java.lang.Math.abs(r6)
            r9 = 10
            if (r5 <= r9) goto L_0x0226
            int r5 = r0.f3496R
            r9 = 2
            if (r9 != r5) goto L_0x0226
        L_0x0214:
            r0.f3496R = r8
            boolean r5 = r0.f3548z
            if (r5 == 0) goto L_0x0226
            int r5 = com.baidu.mapapi.map.BaiduMap.mapStatusReason
            r8 = 1
            r5 = r5 | r8
            com.baidu.mapapi.map.BaiduMap.mapStatusReason = r5
            r22.m3534S()
            r0.mo13003a(r7, r8, r6)
        L_0x0226:
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            r5.f3579f = r2
            r5.f3580g = r3
            goto L_0x022f
        L_0x022d:
            r16 = r3
        L_0x022f:
            int r2 = r0.f3496R
            r3 = 2
            if (r3 == r2) goto L_0x0242
            com.baidu.mapsdkplatform.comapi.map.j$a r2 = r0.f3497S
            r2.f3576c = r4
            r3 = r16
            r2.f3577d = r3
            r2.f3574a = r1
            r1 = r25
            r2.f3575b = r1
        L_0x0242:
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            boolean r1 = r1.f3578e
            if (r1 != 0) goto L_0x0294
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            int r2 = r0.f3494P
            r3 = 2
            int r2 = r2 / r3
            float r2 = (float) r2
            r1.f3579f = r2
            int r2 = r0.f3495Q
            int r2 = r2 / r3
            float r2 = (float) r2
            r1.f3580g = r2
            r2 = 1
            r1.f3578e = r2
            double r1 = r1.f3581h
            r3 = 0
            int r5 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r5 != 0) goto L_0x0294
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3575b
            com.baidu.mapsdkplatform.comapi.map.j$a r2 = r0.f3497S
            float r2 = r2.f3574a
            float r1 = r1 - r2
            com.baidu.mapsdkplatform.comapi.map.j$a r2 = r0.f3497S
            float r2 = r2.f3575b
            com.baidu.mapsdkplatform.comapi.map.j$a r3 = r0.f3497S
            float r3 = r3.f3574a
            float r2 = r2 - r3
            float r1 = r1 * r2
            com.baidu.mapsdkplatform.comapi.map.j$a r2 = r0.f3497S
            float r2 = r2.f3577d
            com.baidu.mapsdkplatform.comapi.map.j$a r3 = r0.f3497S
            float r3 = r3.f3576c
            float r2 = r2 - r3
            com.baidu.mapsdkplatform.comapi.map.j$a r3 = r0.f3497S
            float r3 = r3.f3577d
            com.baidu.mapsdkplatform.comapi.map.j$a r4 = r0.f3497S
            float r4 = r4.f3576c
            float r3 = r3 - r4
            float r2 = r2 * r3
            float r1 = r1 + r2
            double r1 = (double) r1
            double r1 = java.lang.Math.sqrt(r1)
            com.baidu.mapsdkplatform.comapi.map.j$a r3 = r0.f3497S
            r3.f3581h = r1
        L_0x0294:
            r1 = 1
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.map.C1078e.mo13024a(float, float, float, float):boolean");
    }

    /* renamed from: a */
    public boolean mo13025a(long j) {
        for (C1077d dVar : this.f3485E) {
            if (dVar.f3471a == j) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: a */
    public boolean mo13026a(Point point) {
        if (point == null || this.f3533i == null || point.x < 0 || point.y < 0) {
            return false;
        }
        f3475N = point.x;
        f3476O = point.y;
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put(Config.EVENT_HEAT_X, f3475N);
            jSONObject2.put("y", f3476O);
            jSONObject2.put("hidetime", 1000);
            jSONArray.put(jSONObject2);
            jSONObject.put("data", jSONArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.f3487G.mo12976a(jSONObject.toString());
        this.f3533i.mo13489b(this.f3487G.f3471a);
        return true;
    }

    /* renamed from: a */
    public boolean mo13027a(Bundle bundle) {
        if (this.f3533i == null) {
            return false;
        }
        this.f3482B = new C1073aj();
        long a = this.f3533i.mo13469a(this.f3482B.f3473c, this.f3482B.f3474d, this.f3482B.f3472b);
        if (a != 0) {
            C1073aj ajVar = this.f3482B;
            ajVar.f3471a = a;
            this.f3485E.add(ajVar);
            bundle.putLong("sdktileaddr", a);
            return m3544e(bundle) && m3545f(bundle);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0363  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0375  */
    /* JADX WARNING: Removed duplicated region for block: B:151:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0124  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo13028a(android.view.MotionEvent r23) {
        /*
            r22 = this;
            r0 = r22
            r1 = r23
            int r2 = r23.getPointerCount()
            r3 = 0
            r4 = 2
            r5 = 1
            if (r2 != r4) goto L_0x002e
            float r6 = r1.getX(r3)
            int r6 = (int) r6
            float r7 = r1.getY(r3)
            int r7 = (int) r7
            boolean r6 = r0.mo13041c(r6, r7)
            if (r6 == 0) goto L_0x002d
            float r6 = r1.getX(r5)
            int r6 = (int) r6
            float r7 = r1.getY(r5)
            int r7 = (int) r7
            boolean r6 = r0.mo13041c(r6, r7)
            if (r6 != 0) goto L_0x002e
        L_0x002d:
            r2 = 1
        L_0x002e:
            if (r2 != r4) goto L_0x03c3
            int r2 = r0.f3495Q
            float r2 = (float) r2
            float r6 = r1.getY(r3)
            float r2 = r2 - r6
            int r6 = r0.f3495Q
            float r6 = (float) r6
            float r7 = r1.getY(r5)
            float r6 = r6 - r7
            float r7 = r1.getX(r3)
            float r8 = r1.getX(r5)
            int r9 = r23.getAction()
            r10 = 5
            if (r9 == r10) goto L_0x0073
            r10 = 6
            if (r9 == r10) goto L_0x0069
            r10 = 261(0x105, float:3.66E-43)
            if (r9 == r10) goto L_0x0062
            r10 = 262(0x106, float:3.67E-43)
            if (r9 == r10) goto L_0x005b
            goto L_0x007e
        L_0x005b:
            long r9 = r23.getEventTime()
            r0.f3501W = r9
            goto L_0x006f
        L_0x0062:
            long r9 = r23.getEventTime()
            r0.f3499U = r9
            goto L_0x0079
        L_0x0069:
            long r9 = r23.getEventTime()
            r0.f3502X = r9
        L_0x006f:
            int r9 = r0.f3503Y
            int r9 = r9 + r5
            goto L_0x007c
        L_0x0073:
            long r9 = r23.getEventTime()
            r0.f3500V = r9
        L_0x0079:
            int r9 = r0.f3503Y
            int r9 = r9 - r5
        L_0x007c:
            r0.f3503Y = r9
        L_0x007e:
            android.view.VelocityTracker r9 = r0.f3498T
            if (r9 != 0) goto L_0x0088
            android.view.VelocityTracker r9 = android.view.VelocityTracker.obtain()
            r0.f3498T = r9
        L_0x0088:
            android.view.VelocityTracker r9 = r0.f3498T
            r9.addMovement(r1)
            int r1 = android.view.ViewConfiguration.getMinimumFlingVelocity()
            int r9 = android.view.ViewConfiguration.getMaximumFlingVelocity()
            android.view.VelocityTracker r10 = r0.f3498T
            r11 = 1000(0x3e8, float:1.401E-42)
            float r9 = (float) r9
            r10.computeCurrentVelocity(r11, r9)
            android.view.VelocityTracker r9 = r0.f3498T
            float r9 = r9.getXVelocity(r5)
            android.view.VelocityTracker r10 = r0.f3498T
            float r10 = r10.getYVelocity(r5)
            android.view.VelocityTracker r11 = r0.f3498T
            float r11 = r11.getXVelocity(r4)
            android.view.VelocityTracker r12 = r0.f3498T
            float r12 = r12.getYVelocity(r4)
            float r9 = java.lang.Math.abs(r9)
            float r1 = (float) r1
            int r9 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r9 > 0) goto L_0x011e
            float r9 = java.lang.Math.abs(r10)
            int r9 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r9 > 0) goto L_0x011e
            float r9 = java.lang.Math.abs(r11)
            int r9 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r9 > 0) goto L_0x011e
            float r9 = java.lang.Math.abs(r12)
            int r1 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x00d7
            goto L_0x011e
        L_0x00d7:
            int r1 = r0.f3496R
            if (r1 != 0) goto L_0x035b
            int r1 = r0.f3503Y
            if (r1 != 0) goto L_0x035b
            long r9 = r0.f3501W
            long r11 = r0.f3502X
            int r1 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r1 <= 0) goto L_0x00e8
            goto L_0x00e9
        L_0x00e8:
            r9 = r11
        L_0x00e9:
            r0.f3501W = r9
            long r9 = r0.f3499U
            long r11 = r0.f3500V
            int r1 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r1 >= 0) goto L_0x00f4
            r9 = r11
        L_0x00f4:
            r0.f3499U = r9
            long r9 = r0.f3501W
            long r11 = r0.f3499U
            long r9 = r9 - r11
            r11 = 200(0xc8, double:9.9E-322)
            int r1 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r1 >= 0) goto L_0x035b
            boolean r1 = r0.f3530f
            if (r1 == 0) goto L_0x035b
            com.baidu.mapsdkplatform.comapi.map.ab r1 = r22.mo12989E()
            if (r1 == 0) goto L_0x035b
            float r3 = r1.f3414a
            r9 = 1065353216(0x3f800000, float:1.0)
            float r3 = r3 - r9
            r1.f3414a = r3
            int r3 = com.baidu.mapapi.map.BaiduMap.mapStatusReason
            r3 = r3 | r5
            com.baidu.mapapi.map.BaiduMap.mapStatusReason = r3
            r3 = 300(0x12c, float:4.2E-43)
            r0.mo13016a(r1, r3)
            goto L_0x035b
        L_0x011e:
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            boolean r1 = r1.f3578e
            if (r1 == 0) goto L_0x035b
            int r1 = r0.f3496R
            r15 = 4640537203540230144(0x4066800000000000, double:180.0)
            r17 = 4666723172467343360(0x40c3880000000000, double:10000.0)
            r19 = 4611686018427387904(0x4000000000000000, double:2.0)
            r12 = 0
            if (r1 != 0) goto L_0x01cd
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3576c
            float r1 = r1 - r2
            int r1 = (r1 > r12 ? 1 : (r1 == r12 ? 0 : -1))
            if (r1 <= 0) goto L_0x0147
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3577d
            float r1 = r1 - r6
            int r1 = (r1 > r12 ? 1 : (r1 == r12 ? 0 : -1))
            if (r1 > 0) goto L_0x0159
        L_0x0147:
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3576c
            float r1 = r1 - r2
            int r1 = (r1 > r12 ? 1 : (r1 == r12 ? 0 : -1))
            if (r1 >= 0) goto L_0x01c2
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3577d
            float r1 = r1 - r6
            int r1 = (r1 > r12 ? 1 : (r1 == r12 ? 0 : -1))
            if (r1 >= 0) goto L_0x01c2
        L_0x0159:
            float r1 = r6 - r2
            r21 = r6
            double r5 = (double) r1
            float r3 = r8 - r7
            double r12 = (double) r3
            double r5 = java.lang.Math.atan2(r5, r12)
            com.baidu.mapsdkplatform.comapi.map.j$a r12 = r0.f3497S
            float r12 = r12.f3577d
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3576c
            float r12 = r12 - r13
            double r12 = (double) r12
            com.baidu.mapsdkplatform.comapi.map.j$a r14 = r0.f3497S
            float r14 = r14.f3575b
            com.baidu.mapsdkplatform.comapi.map.j$a r4 = r0.f3497S
            float r4 = r4.f3574a
            float r14 = r14 - r4
            double r9 = (double) r14
            double r9 = java.lang.Math.atan2(r12, r9)
            double r5 = r5 - r9
            float r3 = r3 * r3
            float r1 = r1 * r1
            float r3 = r3 + r1
            double r9 = (double) r3
            double r9 = java.lang.Math.sqrt(r9)
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            double r11 = r1.f3581h
            double r9 = r9 / r11
            double r11 = java.lang.Math.log(r9)
            double r13 = java.lang.Math.log(r19)
            double r11 = r11 / r13
            double r11 = r11 * r17
            int r1 = (int) r11
            double r5 = r5 * r15
            r11 = 4614256673094690983(0x400921ff2e48e8a7, double:3.1416)
            double r5 = r5 / r11
            int r3 = (int) r5
            r5 = 0
            int r11 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r11 <= 0) goto L_0x01b0
            r5 = 3000(0xbb8, float:4.204E-42)
            if (r1 > r5) goto L_0x01b8
            r5 = -3000(0xfffffffffffff448, float:NaN)
            if (r1 < r5) goto L_0x01b8
        L_0x01b0:
            int r1 = java.lang.Math.abs(r3)
            r3 = 10
            if (r1 < r3) goto L_0x01bd
        L_0x01b8:
            r1 = 2
            r0.f3496R = r1
            r3 = 1
            goto L_0x01c8
        L_0x01bd:
            r1 = 2
            r3 = 1
            r0.f3496R = r3
            goto L_0x01c8
        L_0x01c2:
            r21 = r6
            r1 = 2
            r3 = 1
            r0.f3496R = r1
        L_0x01c8:
            int r1 = r0.f3496R
            if (r1 != 0) goto L_0x01d0
            return r3
        L_0x01cd:
            r21 = r6
            r3 = 1
        L_0x01d0:
            int r1 = r0.f3496R
            if (r1 != r3) goto L_0x021c
            boolean r1 = r0.f3547y
            if (r1 == 0) goto L_0x021c
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3576c
            float r1 = r1 - r2
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x01fc
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3577d
            float r1 = r1 - r21
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x01fc
            int r1 = com.baidu.mapapi.map.BaiduMap.mapStatusReason
            r3 = 1
            r1 = r1 | r3
            com.baidu.mapapi.map.BaiduMap.mapStatusReason = r1
            r22.m3534S()
            r1 = 83
        L_0x01f7:
            r4 = 0
            r0.mo13003a(r3, r1, r4)
            goto L_0x0228
        L_0x01fc:
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3576c
            float r1 = r1 - r2
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0228
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            float r1 = r1.f3577d
            float r1 = r1 - r21
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0228
            int r1 = com.baidu.mapapi.map.BaiduMap.mapStatusReason
            r3 = 1
            r1 = r1 | r3
            com.baidu.mapapi.map.BaiduMap.mapStatusReason = r1
            r22.m3534S()
            r1 = 87
            goto L_0x01f7
        L_0x021c:
            int r1 = r0.f3496R
            r3 = 4
            r5 = 3
            r6 = 2
            if (r1 == r6) goto L_0x022b
            if (r1 == r3) goto L_0x022b
            if (r1 != r5) goto L_0x0228
            goto L_0x022b
        L_0x0228:
            r5 = r0
            goto L_0x035e
        L_0x022b:
            float r6 = r21 - r2
            double r9 = (double) r6
            float r1 = r8 - r7
            double r11 = (double) r1
            double r9 = java.lang.Math.atan2(r9, r11)
            com.baidu.mapsdkplatform.comapi.map.j$a r11 = r0.f3497S
            float r11 = r11.f3577d
            com.baidu.mapsdkplatform.comapi.map.j$a r12 = r0.f3497S
            float r12 = r12.f3576c
            float r11 = r11 - r12
            double r11 = (double) r11
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3575b
            com.baidu.mapsdkplatform.comapi.map.j$a r14 = r0.f3497S
            float r14 = r14.f3574a
            float r13 = r13 - r14
            double r13 = (double) r13
            double r11 = java.lang.Math.atan2(r11, r13)
            double r9 = r9 - r11
            float r1 = r1 * r1
            float r6 = r6 * r6
            float r1 = r1 + r6
            double r11 = (double) r1
            double r11 = java.lang.Math.sqrt(r11)
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r0.f3497S
            double r13 = r1.f3581h
            double r11 = r11 / r13
            double r13 = java.lang.Math.log(r11)
            double r19 = java.lang.Math.log(r19)
            double r13 = r13 / r19
            double r13 = r13 * r17
            int r1 = (int) r13
            com.baidu.mapsdkplatform.comapi.map.j$a r6 = r0.f3497S
            float r6 = r6.f3580g
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3576c
            float r6 = r6 - r13
            double r13 = (double) r6
            com.baidu.mapsdkplatform.comapi.map.j$a r6 = r0.f3497S
            float r6 = r6.f3579f
            com.baidu.mapsdkplatform.comapi.map.j$a r4 = r0.f3497S
            float r4 = r4.f3574a
            float r6 = r6 - r4
            double r3 = (double) r6
            double r3 = java.lang.Math.atan2(r13, r3)
            com.baidu.mapsdkplatform.comapi.map.j$a r6 = r0.f3497S
            float r6 = r6.f3579f
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3574a
            float r6 = r6 - r13
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3579f
            com.baidu.mapsdkplatform.comapi.map.j$a r14 = r0.f3497S
            float r14 = r14.f3574a
            float r13 = r13 - r14
            float r6 = r6 * r13
            com.baidu.mapsdkplatform.comapi.map.j$a r13 = r0.f3497S
            float r13 = r13.f3580g
            com.baidu.mapsdkplatform.comapi.map.j$a r14 = r0.f3497S
            float r14 = r14.f3576c
            float r13 = r13 - r14
            com.baidu.mapsdkplatform.comapi.map.j$a r14 = r0.f3497S
            float r14 = r14.f3580g
            com.baidu.mapsdkplatform.comapi.map.j$a r5 = r0.f3497S
            float r5 = r5.f3576c
            float r14 = r14 - r5
            float r13 = r13 * r14
            float r6 = r6 + r13
            double r5 = (double) r6
            double r5 = java.lang.Math.sqrt(r5)
            double r3 = r3 + r9
            double r13 = java.lang.Math.cos(r3)
            double r13 = r13 * r5
            double r13 = r13 * r11
            r19 = r1
            double r0 = (double) r7
            java.lang.Double.isNaN(r0)
            double r13 = r13 + r0
            float r0 = (float) r13
            double r3 = java.lang.Math.sin(r3)
            double r5 = r5 * r3
            double r5 = r5 * r11
            double r3 = (double) r2
            java.lang.Double.isNaN(r3)
            double r5 = r5 + r3
            float r1 = (float) r5
            double r9 = r9 * r15
            r3 = 4614256673094690983(0x400921ff2e48e8a7, double:3.1416)
            double r9 = r9 / r3
            int r3 = (int) r9
            r4 = 8193(0x2001, float:1.1481E-41)
            r5 = 0
            int r9 = (r11 > r5 ? 1 : (r11 == r5 ? 0 : -1))
            r5 = r22
            if (r9 <= 0) goto L_0x0333
            int r6 = r5.f3496R
            r9 = 3
            if (r9 == r6) goto L_0x02f4
            int r6 = java.lang.Math.abs(r19)
            r10 = 2000(0x7d0, float:2.803E-42)
            if (r6 <= r10) goto L_0x0333
            int r6 = r5.f3496R
            r10 = 2
            if (r10 != r6) goto L_0x0333
        L_0x02f4:
            r5.f3496R = r9
            com.baidu.mapsdkplatform.comapi.map.ab r3 = r22.mo12989E()
            float r3 = r3.f3414a
            boolean r6 = r5.f3530f
            if (r6 == 0) goto L_0x0354
            r9 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r6 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r6 <= 0) goto L_0x031b
            float r6 = r5.f3505a
            int r3 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r3 < 0) goto L_0x030e
            r6 = 0
            return r6
        L_0x030e:
            int r3 = com.baidu.mapapi.map.BaiduMap.mapStatusReason
            r9 = 1
            r3 = r3 | r9
            com.baidu.mapapi.map.BaiduMap.mapStatusReason = r3
            r22.m3534S()
            r10 = r19
            r11 = 3
            goto L_0x032f
        L_0x031b:
            r10 = r19
            r6 = 0
            r9 = 1
            r11 = 3
            float r12 = r5.f3527b
            int r3 = (r3 > r12 ? 1 : (r3 == r12 ? 0 : -1))
            if (r3 > 0) goto L_0x0327
            return r6
        L_0x0327:
            int r3 = com.baidu.mapapi.map.BaiduMap.mapStatusReason
            r3 = r3 | r9
            com.baidu.mapapi.map.BaiduMap.mapStatusReason = r3
            r22.m3534S()
        L_0x032f:
            r5.mo13003a(r4, r11, r10)
            goto L_0x0354
        L_0x0333:
            if (r3 == 0) goto L_0x0354
            int r6 = r5.f3496R
            r9 = 4
            if (r9 == r6) goto L_0x0347
            int r6 = java.lang.Math.abs(r3)
            r10 = 10
            if (r6 <= r10) goto L_0x0354
            int r6 = r5.f3496R
            r10 = 2
            if (r10 != r6) goto L_0x0354
        L_0x0347:
            r5.f3496R = r9
            boolean r6 = r5.f3548z
            if (r6 == 0) goto L_0x0354
            r22.m3534S()
            r6 = 1
            r5.mo13003a(r4, r6, r3)
        L_0x0354:
            com.baidu.mapsdkplatform.comapi.map.j$a r3 = r5.f3497S
            r3.f3579f = r0
            r3.f3580g = r1
            goto L_0x035e
        L_0x035b:
            r5 = r0
            r21 = r6
        L_0x035e:
            int r0 = r5.f3496R
            r1 = 2
            if (r1 == r0) goto L_0x036f
            com.baidu.mapsdkplatform.comapi.map.j$a r0 = r5.f3497S
            r0.f3576c = r2
            r6 = r21
            r0.f3577d = r6
            r0.f3574a = r7
            r0.f3575b = r8
        L_0x036f:
            com.baidu.mapsdkplatform.comapi.map.j$a r0 = r5.f3497S
            boolean r0 = r0.f3578e
            if (r0 != 0) goto L_0x03c1
            com.baidu.mapsdkplatform.comapi.map.j$a r0 = r5.f3497S
            int r1 = r5.f3494P
            r2 = 2
            int r1 = r1 / r2
            float r1 = (float) r1
            r0.f3579f = r1
            int r1 = r5.f3495Q
            int r1 = r1 / r2
            float r1 = (float) r1
            r0.f3580g = r1
            r1 = 1
            r0.f3578e = r1
            double r0 = r0.f3581h
            r2 = 0
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 != 0) goto L_0x03c1
            com.baidu.mapsdkplatform.comapi.map.j$a r0 = r5.f3497S
            float r0 = r0.f3575b
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r5.f3497S
            float r1 = r1.f3574a
            float r0 = r0 - r1
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r5.f3497S
            float r1 = r1.f3575b
            com.baidu.mapsdkplatform.comapi.map.j$a r2 = r5.f3497S
            float r2 = r2.f3574a
            float r1 = r1 - r2
            float r0 = r0 * r1
            com.baidu.mapsdkplatform.comapi.map.j$a r1 = r5.f3497S
            float r1 = r1.f3577d
            com.baidu.mapsdkplatform.comapi.map.j$a r2 = r5.f3497S
            float r2 = r2.f3576c
            float r1 = r1 - r2
            com.baidu.mapsdkplatform.comapi.map.j$a r2 = r5.f3497S
            float r2 = r2.f3577d
            com.baidu.mapsdkplatform.comapi.map.j$a r3 = r5.f3497S
            float r3 = r3.f3576c
            float r2 = r2 - r3
            float r1 = r1 * r2
            float r0 = r0 + r1
            double r0 = (double) r0
            double r0 = java.lang.Math.sqrt(r0)
            com.baidu.mapsdkplatform.comapi.map.j$a r2 = r5.f3497S
            r2.f3581h = r0
        L_0x03c1:
            r0 = 1
            return r0
        L_0x03c3:
            r5 = r0
            r0 = 1
            int r2 = r23.getAction()
            if (r2 == 0) goto L_0x03db
            if (r2 == r0) goto L_0x03d6
            r3 = 2
            if (r2 == r3) goto L_0x03d2
            r2 = 0
            return r2
        L_0x03d2:
            r22.m3541c(r23)
            goto L_0x03de
        L_0x03d6:
            boolean r0 = r22.m3542d(r23)
            return r0
        L_0x03db:
            r22.m3539b(r23)
        L_0x03de:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.map.C1078e.mo13028a(android.view.MotionEvent):boolean");
    }

    /* renamed from: a */
    public boolean mo13029a(String str, String str2) {
        return this.f3533i.mo13484a(str, str2);
    }

    /* renamed from: b */
    public GeoPoint mo13030b(int i, int i2) {
        return this.f3493M.mo12973a(i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13031b(float f, float f2) {
        if (!this.f3497S.f3578e) {
            this.f3509ad = System.currentTimeMillis();
            long j = this.f3509ad;
            if (j - this.f3508ac < 400) {
                if (Math.abs(f - this.f3504Z) >= 120.0f || Math.abs(f2 - this.f3506aa) >= 120.0f) {
                    j = this.f3509ad;
                } else {
                    this.f3508ac = 0;
                    this.f3510ae = true;
                    this.f3504Z = f;
                    this.f3506aa = f2;
                    mo13003a(4, 0, ((int) f) | (((int) f2) << 16));
                    this.f3507ab = true;
                }
            }
            this.f3508ac = j;
            this.f3504Z = f;
            this.f3506aa = f2;
            mo13003a(4, 0, ((int) f) | (((int) f2) << 16));
            this.f3507ab = true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13032b(int i) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13492b(i);
            this.f3533i = null;
        }
    }

    /* renamed from: b */
    public void mo13033b(Bundle bundle) {
        if (this.f3533i != null) {
            m3546g(bundle);
            this.f3533i.mo13511f(bundle);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13034b(Handler handler) {
        MessageCenter.unregistMessage(UIMsg.m_AppUI.MSG_APP_SAVESCREEN, handler);
        MessageCenter.unregistMessage(41, handler);
        MessageCenter.unregistMessage(49, handler);
        MessageCenter.unregistMessage(39, handler);
        MessageCenter.unregistMessage(65289, handler);
        MessageCenter.unregistMessage(50, handler);
        MessageCenter.unregistMessage(999, handler);
        BaseMapCallback.removeLayerDataInterface(this.f3534j);
    }

    /* renamed from: b */
    public void mo13035b(boolean z) {
        this.f3481A = z;
    }

    /* renamed from: b */
    public boolean mo13036b() {
        return this.f3481A;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, android.os.Bundle):void
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, long):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, java.lang.String):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(boolean, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void */
    /* renamed from: c */
    public void mo13037c() {
        if (this.f3533i != null) {
            for (C1077d dVar : this.f3485E) {
                this.f3533i.mo13474a(dVar.f3471a, false);
            }
        }
    }

    /* renamed from: c */
    public void mo13038c(Bundle bundle) {
        if (this.f3533i != null) {
            m3546g(bundle);
            this.f3533i.mo13513g(bundle);
        }
    }

    /* renamed from: c */
    public void mo13039c(boolean z) {
        boolean z2;
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            if (z) {
                if (!this.f3525au) {
                    aVar.mo13483a(this.f3521ap.f3471a, this.f3489I.f3471a);
                    z2 = true;
                } else {
                    return;
                }
            } else if (this.f3525au) {
                aVar.mo13483a(this.f3489I.f3471a, this.f3521ap.f3471a);
                z2 = false;
            } else {
                return;
            }
            this.f3525au = z2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public boolean mo13040c(float f, float f2) {
        if (this.f3497S.f3578e || System.currentTimeMillis() - f3479m < 300) {
            return true;
        }
        if (this.f3539p) {
            List<C1087l> list = this.f3532h;
            if (list != null) {
                for (C1087l lVar : list) {
                    GeoPoint b = mo13030b((int) f, (int) f2);
                    if (lVar != null) {
                        lVar.mo11555d(b);
                    }
                }
            }
            return true;
        }
        float abs = Math.abs(f - this.f3504Z);
        float abs2 = Math.abs(f2 - this.f3506aa);
        int i = (((double) SysOSUtil.getDensity()) > 1.5d ? 1 : (((double) SysOSUtil.getDensity()) == 1.5d ? 0 : -1));
        double density = (double) SysOSUtil.getDensity();
        if (i > 0) {
            Double.isNaN(density);
            density *= 1.5d;
        }
        float f3 = (float) density;
        if (this.f3507ab && abs / f3 <= 3.0f && abs2 / f3 <= 3.0f) {
            return true;
        }
        this.f3507ab = false;
        int i2 = (int) f;
        int i3 = (int) f2;
        if (i2 < 0) {
            i2 = 0;
        }
        if (i3 < 0) {
            i3 = 0;
        }
        if (this.f3529e) {
            this.f3512ag = this.f3514ai;
            this.f3513ah = this.f3515aj;
            this.f3514ai = f;
            this.f3515aj = f2;
            this.f3516ak = this.f3517al;
            this.f3517al = System.currentTimeMillis();
            this.f3511af = true;
            m3534S();
            mo13003a(3, 0, (i3 << 16) | i2);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public boolean mo13041c(int i, int i2) {
        return i >= 0 && i <= this.f3494P + 0 && i2 >= 0 && i2 <= this.f3495Q + 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, android.os.Bundle):void
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, long):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, java.lang.String):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(boolean, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void */
    /* renamed from: d */
    public void mo13042d() {
        if (this.f3533i != null) {
            for (C1077d dVar : this.f3485E) {
                if ((dVar instanceof C1100x) || (dVar instanceof C1062a) || (dVar instanceof C1090n)) {
                    this.f3533i.mo13474a(dVar.f3471a, false);
                } else {
                    this.f3533i.mo13474a(dVar.f3471a, true);
                }
            }
            this.f3533i.mo13498c(false);
        }
    }

    /* renamed from: d */
    public void mo13043d(Bundle bundle) {
        if (this.f3533i != null) {
            m3546g(bundle);
            this.f3533i.mo13515h(bundle);
        }
    }

    /* renamed from: d */
    public void mo13044d(boolean z) {
        boolean z2;
        if (z) {
            if (!this.f3526av) {
                this.f3533i.mo13483a(this.f3489I.f3471a, this.f3486F.f3471a);
                z2 = true;
            } else {
                return;
            }
        } else if (this.f3526av) {
            this.f3533i.mo13483a(this.f3486F.f3471a, this.f3489I.f3471a);
            z2 = false;
        } else {
            return;
        }
        this.f3526av = z2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public boolean mo13045d(float f, float f2) {
        if (this.f3539p) {
            List<C1087l> list = this.f3532h;
            if (list != null) {
                for (C1087l lVar : list) {
                    GeoPoint b = mo13030b((int) f, (int) f2);
                    if (lVar != null) {
                        lVar.mo11557e(b);
                    }
                }
            }
            this.f3539p = false;
            return true;
        }
        if (!this.f3497S.f3578e) {
            if (this.f3510ae) {
                return m3543e(f, f2);
            }
            if (this.f3511af) {
                return m3535T();
            }
            if (System.currentTimeMillis() - this.f3509ad < 400 && Math.abs(f - this.f3504Z) < 10.0f && Math.abs(f2 - this.f3506aa) < 10.0f) {
                mo12997M();
                return true;
            }
        }
        mo12997M();
        int i = (int) f;
        int i2 = (int) f2;
        if (i < 0) {
            i = 0;
        }
        if (i2 < 0) {
            i2 = 0;
        }
        mo13003a(5, 0, i | (i2 << 16));
        return true;
    }

    /* renamed from: e */
    public void mo13046e(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13474a(this.f3487G.f3471a, z);
        }
    }

    /* renamed from: e */
    public boolean mo13047e() {
        C1177a aVar;
        C1073aj ajVar = this.f3482B;
        if (ajVar == null || (aVar = this.f3533i) == null) {
            return false;
        }
        return aVar.mo13499c(ajVar.f3471a);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public void mo13048f() {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            this.f3493M = new C1069af(aVar);
        }
    }

    /* renamed from: f */
    public void mo13049f(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13474a(this.f3482B.f3471a, z);
        }
    }

    /* renamed from: g */
    public void mo13050g(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13474a(this.f3518am.f3471a, z);
        }
    }

    /* renamed from: g */
    public boolean mo13051g() {
        return this.f3541s;
    }

    /* renamed from: h */
    public String mo13052h() {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return null;
        }
        return aVar.mo13506e(this.f3487G.f3471a);
    }

    /* renamed from: h */
    public void mo13053h(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            this.f3546x = z;
            aVar.mo13491b(this.f3546x);
        }
    }

    /* renamed from: i */
    public void mo13054i(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            this.f3541s = z;
            aVar.mo13498c(this.f3541s);
        }
    }

    /* renamed from: i */
    public boolean mo13055i() {
        return this.f3546x;
    }

    /* renamed from: j */
    public void mo13056j(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13502d(z);
        }
    }

    /* renamed from: j */
    public boolean mo13057j() {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return false;
        }
        return aVar.mo13518k();
    }

    /* renamed from: k */
    public void mo13058k(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            this.f3543u = z;
            aVar.mo13474a(this.f3487G.f3471a, z);
        }
    }

    /* renamed from: k */
    public boolean mo13059k() {
        return this.f3542t;
    }

    /* renamed from: l */
    public void mo13060l(boolean z) {
        float f = z ? 22.0f : 21.0f;
        this.f3505a = f;
        this.f3528c = f;
        this.f3533i.mo13508e(z);
        this.f3533i.mo13501d(this.f3521ap.f3471a);
        this.f3533i.mo13501d(this.f3522aq.f3471a);
    }

    /* renamed from: l */
    public boolean mo13061l() {
        return this.f3533i.mo13482a(this.f3518am.f3471a);
    }

    /* renamed from: m */
    public boolean mo13062m() {
        C1177a aVar = this.f3533i;
        if (aVar == null) {
            return false;
        }
        return aVar.mo13522o();
    }

    /* renamed from: n */
    public void mo13063n() {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13501d(this.f3489I.f3471a);
        }
    }

    /* renamed from: n */
    public void mo13064n(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            this.f3544v = z;
            aVar.mo13474a(this.f3486F.f3471a, z);
        }
    }

    /* renamed from: o */
    public void mo13065o() {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13523p();
            this.f3533i.mo13489b(this.f3490J.f3471a);
        }
    }

    /* renamed from: o */
    public void mo13066o(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            this.f3545w = z;
            aVar.mo13474a(this.f3490J.f3471a, z);
        }
    }

    /* renamed from: p */
    public MapBaseIndoorMapInfo mo13067p() {
        return this.f3533i.mo13524q();
    }

    /* renamed from: p */
    public void mo13068p(boolean z) {
        this.f3529e = z;
    }

    /* renamed from: q */
    public void mo13069q(boolean z) {
        this.f3530f = z;
    }

    /* renamed from: q */
    public boolean mo13070q() {
        return this.f3533i.mo13525r();
    }

    /* renamed from: r */
    public void mo13071r(boolean z) {
        this.f3531g = z;
    }

    /* renamed from: r */
    public boolean mo13072r() {
        return this.f3543u;
    }

    /* renamed from: s */
    public void mo13073s(boolean z) {
        this.f3548z = z;
    }

    /* renamed from: s */
    public boolean mo13074s() {
        return this.f3544v;
    }

    /* renamed from: t */
    public void mo13075t() {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13489b(this.f3490J.f3471a);
        }
    }

    /* renamed from: t */
    public void mo13076t(boolean z) {
        this.f3547y = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: u */
    public void mo13077u() {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13507e();
        }
    }

    /* renamed from: u */
    public void mo13078u(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13474a(this.f3488H.f3471a, z);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: v */
    public void mo13079v() {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13510f();
        }
    }

    /* renamed from: v */
    public void mo13080v(boolean z) {
        C1177a aVar = this.f3533i;
        if (aVar != null) {
            aVar.mo13474a(this.f3522aq.f3471a, z);
        }
    }

    /* renamed from: w */
    public void mo13081w(boolean z) {
        this.f3523as = z;
    }

    /* renamed from: w */
    public boolean mo13082w() {
        return this.f3529e;
    }

    /* renamed from: x */
    public boolean mo13083x() {
        return this.f3530f;
    }

    /* renamed from: y */
    public boolean mo13084y() {
        return this.f3548z;
    }

    /* renamed from: z */
    public boolean mo13085z() {
        return this.f3547y;
    }
}
