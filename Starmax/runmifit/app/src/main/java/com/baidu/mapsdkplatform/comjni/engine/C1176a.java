package com.baidu.mapsdkplatform.comjni.engine;

import android.os.Handler;
import android.os.Message;
import android.util.SparseArray;
import java.util.List;

/* renamed from: com.baidu.mapsdkplatform.comjni.engine.a */
public class C1176a {

    /* renamed from: a */
    private static final String f3937a = C1176a.class.getSimpleName();

    /* renamed from: b */
    private static SparseArray<List<Handler>> f3938b = new SparseArray<>();

    /* renamed from: a */
    public static void m4287a(int i, int i2, int i3, long j) {
        synchronized (f3938b) {
            List<Handler> list = f3938b.get(i);
            if (list != null && !list.isEmpty()) {
                for (Handler handler : list) {
                    Message.obtain(handler, i, i2, i3, Long.valueOf(j)).sendToTarget();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m4288a(int r2, android.os.Handler r3) {
        /*
            android.util.SparseArray<java.util.List<android.os.Handler>> r0 = com.baidu.mapsdkplatform.comjni.engine.C1176a.f3938b
            monitor-enter(r0)
            if (r3 != 0) goto L_0x0007
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x0007:
            android.util.SparseArray<java.util.List<android.os.Handler>> r1 = com.baidu.mapsdkplatform.comjni.engine.C1176a.f3938b     // Catch:{ all -> 0x002a }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x002a }
            java.util.List r1 = (java.util.List) r1     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x001b
            boolean r2 = r1.contains(r3)     // Catch:{ all -> 0x002a }
            if (r2 != 0) goto L_0x0028
            r1.add(r3)     // Catch:{ all -> 0x002a }
            goto L_0x0028
        L_0x001b:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x002a }
            r1.<init>()     // Catch:{ all -> 0x002a }
            r1.add(r3)     // Catch:{ all -> 0x002a }
            android.util.SparseArray<java.util.List<android.os.Handler>> r3 = com.baidu.mapsdkplatform.comjni.engine.C1176a.f3938b     // Catch:{ all -> 0x002a }
            r3.put(r2, r1)     // Catch:{ all -> 0x002a }
        L_0x0028:
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x002a:
            r2 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comjni.engine.C1176a.m4288a(int, android.os.Handler):void");
    }

    /* renamed from: b */
    public static void m4289b(int i, Handler handler) {
        synchronized (f3938b) {
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
                List list = f3938b.get(i);
                if (list != null) {
                    list.remove(handler);
                }
            }
        }
    }
}
