package com.baidu.location.p013a;

import android.os.Bundle;

/* renamed from: com.baidu.location.a.b */
public class C0726b {

    /* renamed from: a */
    private static Object f1225a = new Object();

    /* renamed from: b */
    private static C0726b f1226b = null;

    /* renamed from: c */
    private int f1227c = -1;

    /* renamed from: a */
    public static C0726b m1759a() {
        C0726b bVar;
        synchronized (f1225a) {
            if (f1226b == null) {
                f1226b = new C0726b();
            }
            bVar = f1226b;
        }
        return bVar;
    }

    /* renamed from: a */
    public void mo10440a(int i, int i2, String str) {
        if (i2 != this.f1227c) {
            this.f1227c = i2;
            Bundle bundle = new Bundle();
            bundle.putInt("loctype", i);
            bundle.putInt("diagtype", i2);
            bundle.putByteArray("diagmessage", str.getBytes());
            C0723a.m1729a().mo10420a(bundle, 303);
        }
    }

    /* renamed from: b */
    public void mo10441b() {
        this.f1227c = -1;
    }
}
