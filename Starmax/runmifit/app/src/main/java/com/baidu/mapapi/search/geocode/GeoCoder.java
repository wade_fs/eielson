package com.baidu.mapapi.search.geocode;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.search.core.C0968l;
import com.baidu.platform.core.p032b.C1331a;
import com.baidu.platform.core.p032b.C1334d;

public class GeoCoder extends C0968l {

    /* renamed from: a */
    private C1334d f2971a = new C1331a();

    /* renamed from: b */
    private boolean f2972b;

    private GeoCoder() {
    }

    public static GeoCoder newInstance() {
        BMapManager.init();
        return new GeoCoder();
    }

    public void destroy() {
        if (!this.f2972b) {
            this.f2972b = true;
            this.f2971a.mo14038a();
            BMapManager.destroy();
        }
    }

    public boolean geocode(GeoCodeOption geoCodeOption) {
        if (this.f2971a == null) {
            throw new IllegalStateException("BDMapSDKException: GeoCoder is null, please call newInstance() first.");
        } else if (geoCodeOption != null && geoCodeOption.mAddress != null && geoCodeOption.mCity != null) {
            return this.f2971a.mo14040a(geoCodeOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or address or city can not be null");
        }
    }

    public boolean reverseGeoCode(ReverseGeoCodeOption reverseGeoCodeOption) {
        if (this.f2971a == null) {
            throw new IllegalStateException("BDMapSDKException: GeoCoder is null, please call newInstance() first.");
        } else if (reverseGeoCodeOption != null && reverseGeoCodeOption.getLocation() != null) {
            return this.f2971a.mo14041a(reverseGeoCodeOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or mLocation can not be null");
        }
    }

    public void setOnGetGeoCodeResultListener(OnGetGeoCoderResultListener onGetGeoCoderResultListener) {
        C1334d dVar = this.f2971a;
        if (dVar == null) {
            throw new IllegalStateException("BDMapSDKException: GeoCoder is null, please call newInstance() first.");
        } else if (onGetGeoCoderResultListener != null) {
            dVar.mo14039a(onGetGeoCoderResultListener);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: listener can not be null");
        }
    }
}
