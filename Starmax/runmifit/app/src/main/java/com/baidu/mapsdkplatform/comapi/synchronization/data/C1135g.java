package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.synchronization.DisplayOptions;
import com.baidu.mapapi.synchronization.RoleOptions;
import com.baidu.mapapi.synchronization.SynchronizationConstants;
import com.baidu.mapsdkplatform.comapi.synchronization.data.RouteLineInfo;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1112a;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.g */
public final class C1135g {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final String f3754a = C1135g.class.getSimpleName();

    /* renamed from: b */
    private RoleOptions f3755b;

    /* renamed from: c */
    private DisplayOptions f3756c;

    /* renamed from: d */
    private BlockingQueue<SyncResponseResult> f3757d;

    /* renamed from: e */
    private C1139i f3758e;

    /* renamed from: f */
    private String f3759f;

    /* renamed from: g */
    private String f3760g;

    /* renamed from: h */
    private C1112a f3761h;

    /* renamed from: i */
    private HandlerThread f3762i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public C1137b f3763j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public volatile int f3764k;

    /* renamed from: l */
    private C1141k f3765l;

    /* renamed from: m */
    private boolean f3766m;

    /* renamed from: n */
    private float f3767n;

    /* renamed from: o */
    private long f3768o;

    /* renamed from: p */
    private int f3769p;

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.g$a */
    private static class C1136a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1135g f3770a = new C1135g(null);
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.g$b */
    private class C1137b extends Handler {
        C1137b(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 100000:
                    SyncResponseResult a = C1135g.this.m3954b((String) message.obj);
                    if (a == null) {
                        C1120a.m3855b(C1135g.f3754a, "parser response data is null");
                        return;
                    }
                    C1135g.this.m3946a(a);
                    C1135g.this.m3965j();
                    C1135g.this.m3955b(a);
                    return;
                case 100001:
                    C1135g.this.m3966k();
                    return;
                default:
                    C1120a.m3855b(C1135g.f3754a, "Undefined message type");
                    return;
            }
        }
    }

    private C1135g() {
        this.f3757d = new LinkedBlockingQueue();
        this.f3759f = String.valueOf(0);
        this.f3760g = String.valueOf(0);
        this.f3761h = new C1112a();
        this.f3764k = 0;
        this.f3766m = false;
        this.f3767n = 0.0f;
        this.f3768o = 0;
        this.f3769p = 0;
    }

    /* synthetic */ C1135g(C1138h hVar) {
        this();
    }

    /* renamed from: a */
    private C1132f m3943a(int i) {
        if (this.f3755b == null) {
            C1120a.m3855b(f3754a, "RoleOptions is null");
            return null;
        }
        C1132f fVar = new C1132f();
        fVar.mo13276a(this.f3755b.getOrderId());
        fVar.mo13278b(this.f3755b.getDriverId());
        fVar.mo13280c(this.f3755b.getUserId());
        fVar.mo13275a(i);
        if (this.f3766m) {
            this.f3759f = String.valueOf(0);
            this.f3760g = String.valueOf(0);
        }
        fVar.mo13282d(this.f3759f);
        fVar.mo13284e(this.f3760g);
        return fVar;
    }

    /* renamed from: a */
    public static C1135g m3945a() {
        return C1136a.f3770a;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3946a(SyncResponseResult syncResponseResult) {
        if (this.f3757d == null) {
            this.f3757d = new LinkedBlockingQueue();
        }
        if (this.f3766m) {
            this.f3766m = false;
            this.f3757d.clear();
        }
        try {
            this.f3757d.put(syncResponseResult);
        } catch (InterruptedException e) {
            C1120a.m3852a(f3754a, "InterruptedException happened when put item into queue", e);
            Thread.currentThread().interrupt();
        }
    }

    /* renamed from: a */
    private void m3948a(String str) {
        C1112a aVar = this.f3761h;
        if (aVar == null) {
            C1120a.m3855b(f3754a, "HttpClient cannot be null");
        } else {
            aVar.mo13193a(str, new C1138h(this));
        }
    }

    /* renamed from: a */
    private void m3949a(String str, SyncResponseResult syncResponseResult) {
        LatLng latLng;
        String[] split = str.split(";");
        if (split.length == 0) {
            C1120a.m3855b(f3754a, "There's no section route data");
            return;
        }
        String[] split2 = split[0].split(",");
        if (2 != split2.length) {
            C1120a.m3855b(f3754a, "Section start position latlng invalid: " + split[0]);
            return;
        }
        LatLng latLng2 = null;
        try {
            latLng = new LatLng(Double.valueOf(split2[1]).doubleValue(), Double.valueOf(split2[0]).doubleValue());
        } catch (NumberFormatException e) {
            C1120a.m3852a(f3754a, "Get startPosition failed", e);
            latLng = null;
        }
        for (int i = 1; i < split.length; i++) {
            RouteLineInfo.RouteSectionInfo routeSectionInfo = new RouteLineInfo.RouteSectionInfo();
            routeSectionInfo.mo13222a(latLng);
            String[] split3 = split[i].split(",");
            if (2 != split3.length) {
                C1120a.m3855b(f3754a, "Section position latlng invalid: " + split[i]);
            } else {
                try {
                    latLng2 = new LatLng(Double.valueOf(split3[1]).doubleValue(), Double.valueOf(split3[0]).doubleValue());
                } catch (NumberFormatException e2) {
                    C1120a.m3852a(f3754a, "Get endPosition failed", e2);
                }
                routeSectionInfo.mo13224b(latLng2);
                syncResponseResult.mo13227a().mo13215a(routeSectionInfo);
                latLng = latLng2;
            }
        }
    }

    /* renamed from: a */
    private void m3950a(JSONArray jSONArray, SyncResponseResult syncResponseResult) {
        double d;
        JSONObject optJSONObject = jSONArray.optJSONObject(jSONArray.length() - 1);
        if (optJSONObject == null) {
            C1120a.m3855b(f3754a, "Invalid driver position data");
            return;
        }
        syncResponseResult.mo13236c().setTimeStamp(optJSONObject.optString("t"));
        String optString = optJSONObject.optString("p");
        if (optString == null) {
            C1120a.m3855b(f3754a, "No position info data");
            return;
        }
        String[] split = optString.split(";");
        if (split.length == 0) {
            C1120a.m3855b(f3754a, "Position info array is empty");
            return;
        }
        int i = 0;
        String[] split2 = split[0].split(",");
        if (2 != split2.length) {
            C1120a.m3855b(f3754a, "Position latlng invalid");
            return;
        }
        LatLng latLng = null;
        try {
            latLng = new LatLng(Double.valueOf(split2[1]).doubleValue(), Double.valueOf(split2[0]).doubleValue());
        } catch (NumberFormatException e) {
            C1120a.m3852a(f3754a, "Get driver position failed", e);
        }
        syncResponseResult.mo13236c().setPoint(latLng);
        double d2 = 0.0d;
        try {
            d = Double.valueOf(split[1]).doubleValue();
        } catch (NumberFormatException e2) {
            C1120a.m3852a(f3754a, "Get angle failed", e2);
            d = 0.0d;
        }
        syncResponseResult.mo13236c().setAngle(d);
        try {
            d2 = Double.valueOf(split[2]).doubleValue();
        } catch (NumberFormatException e3) {
            C1120a.m3852a(f3754a, "Get speed failed", e3);
        }
        syncResponseResult.mo13236c().setSpeed(d2);
        try {
            i = Integer.valueOf(split[3]).intValue();
        } catch (NumberFormatException e4) {
            C1120a.m3852a(f3754a, "Get orderStateInPosition failed", e4);
        }
        syncResponseResult.mo13236c().setOrderStateInPosition(i);
    }

    /* renamed from: a */
    private void m3951a(JSONObject jSONObject, SyncResponseResult syncResponseResult) {
        JSONObject optJSONObject = jSONObject.optJSONObject("route");
        if (optJSONObject != null) {
            C1120a.m3851a(f3754a, "parser route data");
            m3958b(optJSONObject, syncResponseResult);
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("traffic");
        if (optJSONObject2 != null) {
            C1120a.m3851a(f3754a, "parser traffic data");
            m3961c(optJSONObject2, syncResponseResult);
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("positions");
        if (optJSONArray == null || optJSONArray.length() == 0) {
            this.f3765l.mo13166c(2004, SynchronizationConstants.LBS_STATUS_MESSAGE_QUERY_TRACK_DRIVER_POSITION_FAILED);
        } else {
            C1120a.m3851a(f3754a, "parser driver position data");
            m3950a(optJSONArray, syncResponseResult);
        }
        String optString = jSONObject.optString("run");
        if (!TextUtils.isEmpty(optString)) {
            C1120a.m3851a(f3754a, "parser travelled distance and elapsed time data");
            m3960c(optString, syncResponseResult);
        }
        String optString2 = jSONObject.optString("remain");
        if (!TextUtils.isEmpty(optString2)) {
            C1120a.m3851a(f3754a, "parser remain distance and estimated time data");
            m3963d(optString2, syncResponseResult);
        }
        syncResponseResult.mo13229a(jSONObject.optInt("s"));
        syncResponseResult.mo13231a(jSONObject.optString("mtime"));
        syncResponseResult.mo13235b(jSONObject.optString("ext"));
    }

    /* renamed from: a */
    private boolean m3952a(JSONObject jSONObject) {
        if (jSONObject == null || !jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
            C1141k kVar = this.f3765l;
            if (kVar != null) {
                kVar.mo13166c(SynchronizationConstants.LBS_ERROR_QUERY_TRACK_ROUTE_FAILED, SynchronizationConstants.LBS_STATUS_MESSAGE_QUERY_TRACK_ROUTE_FAILED);
            }
            return false;
        }
        int optInt = jSONObject.optInt(NotificationCompat.CATEGORY_STATUS);
        String optString = jSONObject.optString("message");
        if (optInt == 0) {
            C1141k kVar2 = this.f3765l;
            if (kVar2 == null) {
                return true;
            }
            kVar2.mo13161b(optInt, optString);
            return true;
        }
        C1141k kVar3 = this.f3765l;
        if (kVar3 != null) {
            kVar3.mo13166c(optInt, optString);
        }
        return false;
    }

    /* renamed from: b */
    static /* synthetic */ int m3953b(C1135g gVar) {
        int i = gVar.f3764k;
        gVar.f3764k = i + 1;
        return i;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public SyncResponseResult m3954b(String str) {
        if (TextUtils.isEmpty(str)) {
            C1120a.m3855b(f3754a, "Response result is null");
            return null;
        }
        SyncResponseResult syncResponseResult = new SyncResponseResult();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!m3952a(jSONObject)) {
                C1120a.m3855b(f3754a, "Response result is invalid");
                return null;
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("data");
            if (optJSONObject != null) {
                if (optJSONObject.length() > 0) {
                    m3951a(optJSONObject, syncResponseResult);
                    return syncResponseResult;
                }
            }
            C1120a.m3855b(f3754a, "No route and traffic and driver data");
            return null;
        } catch (JSONException unused) {
            C1120a.m3855b(f3754a, "JSONException happened when parser");
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m3955b(SyncResponseResult syncResponseResult) {
        float d = syncResponseResult.mo13237d();
        long e = syncResponseResult.mo13239e();
        int i = this.f3769p;
        if (1 == i || 2 == i || 4 == i) {
            if (this.f3769p != 1 && this.f3766m) {
                this.f3767n = 0.0f;
                this.f3768o = 0;
                e = 0;
                d = 0.0f;
            }
            if (0.0f != d) {
                this.f3767n = d;
            }
            if (0 != e) {
                this.f3768o = e;
            }
        } else {
            this.f3767n = 0.0f;
            this.f3768o = 0;
        }
        C1141k kVar = this.f3765l;
        if (kVar != null) {
            kVar.mo13150a(this.f3767n, this.f3768o);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0073 A[LOOP:1: B:24:0x006f->B:26:0x0073, LOOP_END] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3957b(java.lang.String r10, com.baidu.mapsdkplatform.comapi.synchronization.data.SyncResponseResult r11) {
        /*
            r9 = this;
            java.lang.String r0 = ";"
            java.lang.String[] r10 = r10.split(r0)
            int r0 = r10.length
            if (r0 != 0) goto L_0x0011
            java.lang.String r10 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1135g.f3754a
            java.lang.String r11 = "There's no section traffic data"
            com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a.m3855b(r10, r11)
            return
        L_0x0011:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            int r1 = r10.length
            r2 = 0
            r3 = 0
        L_0x0019:
            if (r3 >= r1) goto L_0x0087
            r4 = r10[r3]
            java.lang.String r5 = ","
            java.lang.String[] r5 = r4.split(r5)
            r6 = 3
            int r7 = r5.length
            if (r6 == r7) goto L_0x003e
            java.lang.String r5 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1135g.f3754a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "section traffic data is invalid: "
            r6.append(r7)
            r6.append(r4)
            java.lang.String r4 = r6.toString()
            com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a.m3855b(r5, r4)
            goto L_0x0084
        L_0x003e:
            r4 = r5[r2]     // Catch:{ NumberFormatException -> 0x0063 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ NumberFormatException -> 0x0063 }
            int r4 = r4.intValue()     // Catch:{ NumberFormatException -> 0x0063 }
            r6 = 1
            r6 = r5[r6]     // Catch:{ NumberFormatException -> 0x0061 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ NumberFormatException -> 0x0061 }
            int r6 = r6.intValue()     // Catch:{ NumberFormatException -> 0x0061 }
            r7 = 2
            r5 = r5[r7]     // Catch:{ NumberFormatException -> 0x005f }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ NumberFormatException -> 0x005f }
            int r5 = r5.intValue()     // Catch:{ NumberFormatException -> 0x005f }
            goto L_0x006e
        L_0x005f:
            r5 = move-exception
            goto L_0x0066
        L_0x0061:
            r5 = move-exception
            goto L_0x0065
        L_0x0063:
            r5 = move-exception
            r4 = 0
        L_0x0065:
            r6 = 0
        L_0x0066:
            java.lang.String r7 = com.baidu.mapsdkplatform.comapi.synchronization.data.C1135g.f3754a
            java.lang.String r8 = "Get traffic status info failed"
            com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a.m3852a(r7, r8, r5)
            r5 = 0
        L_0x006e:
            r7 = r4
        L_0x006f:
            int r8 = r4 + r6
            if (r7 >= r8) goto L_0x007d
            java.lang.Integer r8 = java.lang.Integer.valueOf(r5)
            r0.add(r8)
            int r7 = r7 + 1
            goto L_0x006f
        L_0x007d:
            com.baidu.mapsdkplatform.comapi.synchronization.data.TrafficInfo r4 = r11.mo13232b()
            r4.mo13243a(r0)
        L_0x0084:
            int r3 = r3 + 1
            goto L_0x0019
        L_0x0087:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.data.C1135g.m3957b(java.lang.String, com.baidu.mapsdkplatform.comapi.synchronization.data.SyncResponseResult):void");
    }

    /* renamed from: b */
    private void m3958b(JSONObject jSONObject, SyncResponseResult syncResponseResult) {
        syncResponseResult.mo13227a().mo13217a(jSONObject.optInt("c") != 0);
        this.f3759f = jSONObject.optString("f");
        syncResponseResult.mo13227a().mo13216a(this.f3759f);
        String optString = jSONObject.optString("d");
        if (!TextUtils.isEmpty(optString)) {
            m3949a(optString, syncResponseResult);
        }
    }

    /* renamed from: c */
    private void m3960c(String str, SyncResponseResult syncResponseResult) {
        String[] split = str.split(";");
        if (2 != split.length) {
            C1120a.m3855b(f3754a, "The travelled data is null or invalid");
            return;
        }
        float f = 0.0f;
        try {
            f = Float.valueOf(split[0]).floatValue();
        } catch (NumberFormatException e) {
            C1120a.m3852a(f3754a, "Get travelledDistance failed", e);
        }
        syncResponseResult.mo13228a(f);
        long j = 0;
        try {
            j = Long.valueOf(split[1]).longValue();
        } catch (NumberFormatException e2) {
            C1120a.m3852a(f3754a, "Get elapsedTime failed", e2);
        }
        syncResponseResult.mo13230a(j);
    }

    /* renamed from: c */
    private void m3961c(JSONObject jSONObject, SyncResponseResult syncResponseResult) {
        syncResponseResult.mo13232b().mo13244a(jSONObject.optInt("c") != 0);
        this.f3760g = jSONObject.optString("f");
        syncResponseResult.mo13232b().mo13242a(this.f3760g);
        String optString = jSONObject.optString("d");
        if (!TextUtils.isEmpty(optString)) {
            m3957b(optString, syncResponseResult);
        }
    }

    /* renamed from: d */
    private void m3963d(String str, SyncResponseResult syncResponseResult) {
        String[] split = str.split(";");
        if (2 != split.length) {
            C1120a.m3855b(f3754a, "The remain data is null or invalid: ");
            return;
        }
        float f = 0.0f;
        try {
            f = Float.valueOf(split[0]).floatValue();
        } catch (NumberFormatException e) {
            C1120a.m3852a(f3754a, "Get remainDistance failed", e);
        }
        syncResponseResult.mo13233b(f);
        long j = 0;
        try {
            j = Long.valueOf(split[1]).longValue();
        } catch (NumberFormatException e2) {
            C1120a.m3852a(f3754a, "Get estimatedTime failed", e2);
        }
        syncResponseResult.mo13234b(j);
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m3965j() {
        C1139i iVar = this.f3758e;
        if (iVar != null) {
            iVar.mo13306a();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m3966k() {
        C1139i iVar = this.f3758e;
        if (iVar != null) {
            iVar.mo13307b();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13290a(int i, boolean z) {
        String str;
        String str2;
        this.f3766m = z;
        this.f3769p = i;
        C1132f a = m3943a(i);
        if (a == null) {
            str = f3754a;
            str2 = "Data request option is null";
        } else {
            String a2 = new C1143m(a).mo13323a();
            if (a2 == null) {
                str = f3754a;
                str2 = "send url string is null";
            } else {
                m3948a(a2);
                return;
            }
        }
        C1120a.m3855b(str, str2);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo13291a(android.view.View r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.baidu.mapapi.synchronization.DisplayOptions r0 = r2.f3756c     // Catch:{ all -> 0x001b }
            if (r0 == 0) goto L_0x000c
            com.baidu.mapapi.synchronization.DisplayOptions r0 = r2.f3756c     // Catch:{ all -> 0x001b }
            r0.setStartPositionInfoWindowView(r3)     // Catch:{ all -> 0x001b }
            monitor-exit(r2)
            return
        L_0x000c:
            com.baidu.mapsdkplatform.comapi.synchronization.data.k r3 = r2.f3765l     // Catch:{ all -> 0x001b }
            if (r3 == 0) goto L_0x0019
            com.baidu.mapsdkplatform.comapi.synchronization.data.k r3 = r2.f3765l     // Catch:{ all -> 0x001b }
            r0 = 1004(0x3ec, float:1.407E-42)
            java.lang.String r1 = "DisplayOptions instance init null."
            r3.mo13166c(r0, r1)     // Catch:{ all -> 0x001b }
        L_0x0019:
            monitor-exit(r2)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.data.C1135g.mo13291a(android.view.View):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo13292a(DisplayOptions displayOptions) {
        this.f3756c = displayOptions;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo13293a(RoleOptions roleOptions) {
        this.f3755b = roleOptions;
    }

    /* renamed from: a */
    public void mo13294a(C1139i iVar) {
        this.f3758e = iVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13295a(C1141k kVar) {
        this.f3765l = kVar;
    }

    /* renamed from: b */
    public void mo13296b() {
        this.f3762i = new HandlerThread("SyncDataStorage");
        this.f3762i.start();
        this.f3763j = new C1137b(this.f3762i.getLooper());
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo13297b(android.view.View r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.baidu.mapapi.synchronization.DisplayOptions r0 = r2.f3756c     // Catch:{ all -> 0x001b }
            if (r0 == 0) goto L_0x000c
            com.baidu.mapapi.synchronization.DisplayOptions r0 = r2.f3756c     // Catch:{ all -> 0x001b }
            r0.setEndPositionInfoWindowView(r3)     // Catch:{ all -> 0x001b }
            monitor-exit(r2)
            return
        L_0x000c:
            com.baidu.mapsdkplatform.comapi.synchronization.data.k r3 = r2.f3765l     // Catch:{ all -> 0x001b }
            if (r3 == 0) goto L_0x0019
            com.baidu.mapsdkplatform.comapi.synchronization.data.k r3 = r2.f3765l     // Catch:{ all -> 0x001b }
            r0 = 1004(0x3ec, float:1.407E-42)
            java.lang.String r1 = "DisplayOptions instance init null."
            r3.mo13166c(r0, r1)     // Catch:{ all -> 0x001b }
        L_0x0019:
            monitor-exit(r2)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.data.C1135g.mo13297b(android.view.View):void");
    }

    /* renamed from: c */
    public void mo13298c() {
        if (this.f3758e != null) {
            this.f3758e = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return;
     */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo13299c(android.view.View r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.baidu.mapapi.synchronization.DisplayOptions r0 = r2.f3756c     // Catch:{ all -> 0x001b }
            if (r0 == 0) goto L_0x000c
            com.baidu.mapapi.synchronization.DisplayOptions r0 = r2.f3756c     // Catch:{ all -> 0x001b }
            r0.setCarInfoWindowView(r3)     // Catch:{ all -> 0x001b }
            monitor-exit(r2)
            return
        L_0x000c:
            com.baidu.mapsdkplatform.comapi.synchronization.data.k r3 = r2.f3765l     // Catch:{ all -> 0x001b }
            if (r3 == 0) goto L_0x0019
            com.baidu.mapsdkplatform.comapi.synchronization.data.k r3 = r2.f3765l     // Catch:{ all -> 0x001b }
            r0 = 1004(0x3ec, float:1.407E-42)
            java.lang.String r1 = "DisplayOptions instance init null."
            r3.mo13166c(r0, r1)     // Catch:{ all -> 0x001b }
        L_0x0019:
            monitor-exit(r2)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.data.C1135g.mo13299c(android.view.View):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public int mo13300d() {
        return this.f3764k;
    }

    /* renamed from: e */
    public RoleOptions mo13301e() {
        return this.f3755b;
    }

    /* renamed from: f */
    public DisplayOptions mo13302f() {
        return this.f3756c;
    }

    /* renamed from: g */
    public BlockingQueue<SyncResponseResult> mo13303g() {
        return this.f3757d;
    }

    /* renamed from: h */
    public void mo13304h() {
        this.f3766m = false;
        this.f3767n = 0.0f;
        this.f3768o = 0;
        this.f3763j.removeCallbacksAndMessages(null);
        this.f3762i.quit();
    }
}
