package com.baidu.mapapi.map;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.MapViewLayoutParams;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapsdkplatform.comapi.commonutils.C1053a;
import com.baidu.mapsdkplatform.comapi.map.C1074ak;
import com.baidu.mapsdkplatform.comapi.map.C1083i;
import com.baidu.mapsdkplatform.comapi.map.C1084j;
import com.baidu.mapsdkplatform.comapi.map.C1087l;
import com.baidu.mapsdkplatform.comapi.util.CustomMapStyleLoader;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

public class WearMapView extends ViewGroup implements View.OnApplyWindowInsetsListener {
    public static final int BT_INVIEW = 1;

    /* renamed from: b */
    private static final String f2794b = MapView.class.getSimpleName();

    /* renamed from: c */
    private static String f2795c;

    /* renamed from: d */
    private static int f2796d = 0;

    /* renamed from: e */
    private static int f2797e = 0;

    /* renamed from: s */
    private static int f2798s = 0;

    /* renamed from: t */
    private static int f2799t = 0;

    /* renamed from: u */
    private static int f2800u = 10;
    /* access modifiers changed from: private */

    /* renamed from: x */
    public static final SparseArray<Integer> f2801x = new SparseArray<>();
    /* access modifiers changed from: private */

    /* renamed from: A */
    public float f2802A;

    /* renamed from: B */
    private C1087l f2803B;

    /* renamed from: C */
    private int f2804C;

    /* renamed from: D */
    private int f2805D;

    /* renamed from: E */
    private int f2806E;

    /* renamed from: F */
    private int f2807F;

    /* renamed from: G */
    private int f2808G;

    /* renamed from: H */
    private int f2809H;

    /* renamed from: a */
    ScreenShape f2810a = ScreenShape.ROUND;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public C1084j f2811f;

    /* renamed from: g */
    private BaiduMap f2812g;

    /* renamed from: h */
    private ImageView f2813h;

    /* renamed from: i */
    private Bitmap f2814i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public C1074ak f2815j;

    /* renamed from: k */
    private boolean f2816k = true;

    /* renamed from: l */
    private Point f2817l;

    /* renamed from: m */
    private Point f2818m;
    public AnimationTask mTask;
    public Timer mTimer;
    public C0922a mTimerHandler;

    /* renamed from: n */
    private RelativeLayout f2819n;

    /* renamed from: o */
    private SwipeDismissView f2820o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public TextView f2821p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public TextView f2822q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public ImageView f2823r;

    /* renamed from: v */
    private boolean f2824v = true;

    /* renamed from: w */
    private Context f2825w;

    /* renamed from: y */
    private boolean f2826y = true;

    /* renamed from: z */
    private boolean f2827z = true;

    public class AnimationTask extends TimerTask {
        public AnimationTask() {
        }

        public void run() {
            Message message = new Message();
            message.what = 1;
            WearMapView.this.mTimerHandler.sendMessage(message);
        }
    }

    public interface OnDismissCallback {
        void onDismiss();

        void onNotify();
    }

    public enum ScreenShape {
        ROUND,
        RECTANGLE,
        UNDETECTED
    }

    /* renamed from: com.baidu.mapapi.map.WearMapView$a */
    private class C0922a extends Handler {

        /* renamed from: b */
        private final WeakReference<Context> f2831b;

        public C0922a(Context context) {
            this.f2831b = new WeakReference<>(context);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.mapapi.map.WearMapView.a(com.baidu.mapapi.map.WearMapView, boolean):void
         arg types: [com.baidu.mapapi.map.WearMapView, int]
         candidates:
          com.baidu.mapapi.map.WearMapView.a(com.baidu.mapapi.map.WearMapView, float):float
          com.baidu.mapapi.map.WearMapView.a(int, int):int
          com.baidu.mapapi.map.WearMapView.a(android.content.Context, com.baidu.mapapi.map.BaiduMapOptions):void
          com.baidu.mapapi.map.WearMapView.a(android.view.View, boolean):void
          com.baidu.mapapi.map.WearMapView.a(com.baidu.mapapi.map.WearMapView, boolean):void */
        public void handleMessage(Message message) {
            if (this.f2831b.get() != null) {
                super.handleMessage(message);
                if (message.what == 1 && WearMapView.this.f2815j != null) {
                    WearMapView.this.m3004a(true);
                }
            }
        }
    }

    static {
        f2801x.append(3, 2000000);
        f2801x.append(4, 1000000);
        f2801x.append(5, 500000);
        f2801x.append(6, 200000);
        f2801x.append(7, 100000);
        f2801x.append(8, 50000);
        f2801x.append(9, 25000);
        f2801x.append(10, 20000);
        f2801x.append(11, 10000);
        f2801x.append(12, 5000);
        f2801x.append(13, 2000);
        f2801x.append(14, 1000);
        f2801x.append(15, 500);
        f2801x.append(16, 200);
        f2801x.append(17, 100);
        f2801x.append(18, 50);
        f2801x.append(19, 20);
        f2801x.append(20, 10);
        f2801x.append(21, 5);
        f2801x.append(22, 2);
    }

    public WearMapView(Context context) {
        super(context);
        m2999a(context, (BaiduMapOptions) null);
    }

    public WearMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m2999a(context, (BaiduMapOptions) null);
    }

    public WearMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m2999a(context, (BaiduMapOptions) null);
    }

    public WearMapView(Context context, BaiduMapOptions baiduMapOptions) {
        super(context);
        m2999a(context, baiduMapOptions);
    }

    /* renamed from: a */
    private int m2994a(int i, int i2) {
        return i - ((int) Math.sqrt(Math.pow((double) i, 2.0d) - Math.pow((double) i2, 2.0d)));
    }

    /* renamed from: a */
    private void m2997a(int i) {
        C1084j jVar = this.f2811f;
        if (jVar != null) {
            if (i == 0) {
                jVar.onPause();
                m3006b();
            } else if (i == 1) {
                jVar.onResume();
                m3009c();
            }
        }
    }

    /* renamed from: a */
    private static void m2998a(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point point = new Point();
        defaultDisplay.getSize(point);
        f2798s = point.x;
        f2799t = point.y;
    }

    /* renamed from: a */
    private void m2999a(Context context, BaiduMapOptions baiduMapOptions) {
        AnimationTask animationTask;
        m2998a(context);
        setOnApplyWindowInsetsListener(this);
        this.f2825w = context;
        this.mTimerHandler = new C0922a(context);
        this.mTimer = new Timer();
        if (!(this.mTimer == null || (animationTask = this.mTask) == null)) {
            animationTask.cancel();
        }
        this.mTask = new AnimationTask();
        this.mTimer.schedule(this.mTask, 5000);
        C1083i.m3649a();
        BMapManager.init();
        m3000a(context, baiduMapOptions, f2796d == 0 ? f2795c : CustomMapStyleLoader.getCustomStyleFilePath());
        this.f2812g = new BaiduMap(this.f2811f);
        this.f2811f.mo13086a().mo13076t(false);
        this.f2811f.mo13086a().mo13073s(false);
        m3010c(context);
        m3013d(context);
        m3007b(context);
        if (baiduMapOptions != null && !baiduMapOptions.f2432h) {
            this.f2815j.setVisibility(View.INVISIBLE);
        }
        m3016e(context);
        if (baiduMapOptions != null && !baiduMapOptions.f2433i) {
            this.f2819n.setVisibility(View.INVISIBLE);
        }
        if (!(baiduMapOptions == null || baiduMapOptions.f2436l == null)) {
            this.f2818m = baiduMapOptions.f2436l;
        }
        if (baiduMapOptions != null && baiduMapOptions.f2435k != null) {
            this.f2817l = baiduMapOptions.f2435k;
        }
    }

    /* renamed from: a */
    private void m3000a(Context context, BaiduMapOptions baiduMapOptions, String str) {
        if (baiduMapOptions == null) {
            this.f2811f = new C1084j(context, null, str, f2797e);
        } else {
            this.f2811f = new C1084j(context, baiduMapOptions.mo11031a(), str, f2797e);
        }
        addView(this.f2811f);
        this.f2803B = new C0949x(this);
        this.f2811f.mo13086a().mo13018a(this.f2803B);
    }

    /* renamed from: a */
    private void m3001a(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-2, -2);
        }
        int i = layoutParams.width;
        int makeMeasureSpec = i > 0 ? View.MeasureSpec.makeMeasureSpec(i, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0);
        int i2 = layoutParams.height;
        view.measure(makeMeasureSpec, i2 > 0 ? View.MeasureSpec.makeMeasureSpec(i2, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* renamed from: a */
    private void m3002a(View view, boolean z) {
        AnimatorSet animatorSet;
        if (z) {
            animatorSet = new AnimatorSet();
            animatorSet.playTogether(ObjectAnimator.ofFloat(view, "TranslationY", 0.0f, -50.0f), ObjectAnimator.ofFloat(view, "alpha", 1.0f, 0.0f));
            animatorSet.addListener(new C0924aa(this, view));
        } else {
            view.setVisibility(View.VISIBLE);
            animatorSet = new AnimatorSet();
            animatorSet.playTogether(ObjectAnimator.ofFloat(view, "TranslationY", -50.0f, 0.0f), ObjectAnimator.ofFloat(view, "alpha", 0.0f, 1.0f));
        }
        animatorSet.setDuration(1200L);
        animatorSet.start();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3004a(boolean z) {
        if (this.f2816k) {
            m3002a(this.f2815j, z);
        }
    }

    /* renamed from: b */
    private void m3006b() {
        if (this.f2811f != null && !this.f2824v) {
            m3012d();
            this.f2824v = true;
        }
    }

    /* renamed from: b */
    private void m3007b(Context context) {
        this.f2820o = new SwipeDismissView(context, this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams((int) ((context.getResources().getDisplayMetrics().density * 34.0f) + 0.5f), f2799t);
        this.f2820o.setBackgroundColor(Color.argb(0, 0, 0, 0));
        this.f2820o.setLayoutParams(layoutParams);
        addView(this.f2820o);
    }

    /* renamed from: c */
    private void m3009c() {
        if (this.f2811f != null && this.f2824v) {
            m3015e();
            this.f2824v = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m3010c(android.content.Context r10) {
        /*
            r9 = this;
            int r0 = com.baidu.mapapi.common.SysOSUtil.getDensityDpi()
            r1 = 180(0xb4, float:2.52E-43)
            if (r0 >= r1) goto L_0x000b
            java.lang.String r1 = "logo_l.png"
            goto L_0x000d
        L_0x000b:
            java.lang.String r1 = "logo_h.png"
        L_0x000d:
            android.graphics.Bitmap r2 = com.baidu.mapsdkplatform.comapi.commonutils.C1053a.m3464a(r1, r10)
            r1 = 480(0x1e0, float:6.73E-43)
            if (r0 <= r1) goto L_0x0031
            android.graphics.Matrix r7 = new android.graphics.Matrix
            r7.<init>()
            r0 = 1073741824(0x40000000, float:2.0)
        L_0x001c:
            r7.postScale(r0, r0)
            r3 = 0
            r4 = 0
            int r5 = r2.getWidth()
            int r6 = r2.getHeight()
            r8 = 1
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r2, r3, r4, r5, r6, r7, r8)
            r9.f2814i = r0
            goto L_0x0041
        L_0x0031:
            r3 = 320(0x140, float:4.48E-43)
            if (r0 <= r3) goto L_0x003f
            if (r0 > r1) goto L_0x003f
            android.graphics.Matrix r7 = new android.graphics.Matrix
            r7.<init>()
            r0 = 1069547520(0x3fc00000, float:1.5)
            goto L_0x001c
        L_0x003f:
            r9.f2814i = r2
        L_0x0041:
            android.graphics.Bitmap r0 = r9.f2814i
            if (r0 == 0) goto L_0x0058
            android.widget.ImageView r0 = new android.widget.ImageView
            r0.<init>(r10)
            r9.f2813h = r0
            android.widget.ImageView r10 = r9.f2813h
            android.graphics.Bitmap r0 = r9.f2814i
            r10.setImageBitmap(r0)
            android.widget.ImageView r10 = r9.f2813h
            r9.addView(r10)
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.map.WearMapView.m3010c(android.content.Context):void");
    }

    /* renamed from: d */
    private void m3012d() {
        C1084j jVar = this.f2811f;
        if (jVar != null) {
            jVar.mo13091b();
        }
    }

    /* renamed from: d */
    private void m3013d(Context context) {
        this.f2815j = new C1074ak(context, true);
        if (this.f2815j.mo12980a()) {
            this.f2815j.mo12982b(new C0950y(this));
            this.f2815j.mo12978a(new C0951z(this));
            addView(this.f2815j);
        }
    }

    /* renamed from: e */
    private void m3015e() {
        C1084j jVar = this.f2811f;
        if (jVar != null) {
            jVar.mo13094c();
        }
    }

    /* renamed from: e */
    private void m3016e(Context context) {
        this.f2819n = new RelativeLayout(context);
        this.f2819n.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.f2821p = new TextView(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        this.f2821p.setTextColor(Color.parseColor("#FFFFFF"));
        this.f2821p.setTextSize(2, 11.0f);
        TextView textView = this.f2821p;
        textView.setTypeface(textView.getTypeface(), 1);
        this.f2821p.setLayoutParams(layoutParams);
        this.f2821p.setId(Integer.MAX_VALUE);
        this.f2819n.addView(this.f2821p);
        this.f2822q = new TextView(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.width = -2;
        layoutParams2.height = -2;
        layoutParams2.addRule(14);
        this.f2822q.setTextColor(Color.parseColor("#000000"));
        this.f2822q.setTextSize(2, 11.0f);
        this.f2822q.setLayoutParams(layoutParams2);
        this.f2819n.addView(this.f2822q);
        this.f2823r = new ImageView(context);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.width = -2;
        layoutParams3.height = -2;
        layoutParams3.addRule(14);
        layoutParams3.addRule(3, this.f2821p.getId());
        this.f2823r.setLayoutParams(layoutParams3);
        Bitmap a = C1053a.m3464a("icon_scale.9.png", context);
        byte[] ninePatchChunk = a.getNinePatchChunk();
        NinePatch.isNinePatchChunk(ninePatchChunk);
        this.f2823r.setBackgroundDrawable(new NinePatchDrawable(a, ninePatchChunk, new Rect(), null));
        this.f2819n.addView(this.f2823r);
        addView(this.f2819n);
    }

    public static void setCustomMapStylePath(String str) {
        if (str == null || str.length() == 0) {
            throw new RuntimeException("BDMapSDKException: customMapStylePath String is illegal");
        } else if (new File(str).exists()) {
            f2795c = str;
        } else {
            throw new RuntimeException("BDMapSDKException: please check whether the customMapStylePath file exits");
        }
    }

    @Deprecated
    public static void setIconCustom(int i) {
        f2797e = i;
    }

    public static void setLoadCustomMapStyleFileMode(int i) {
        f2796d = i;
    }

    @Deprecated
    public static void setMapCustomEnable(boolean z) {
        C1083i.m3651a(z);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof MapViewLayoutParams) {
            super.addView(view, layoutParams);
        }
    }

    public final BaiduMap getMap() {
        BaiduMap baiduMap = this.f2812g;
        baiduMap.f2402c = this;
        return baiduMap;
    }

    public final int getMapLevel() {
        return f2801x.get((int) this.f2811f.mo13086a().mo12989E().f3414a).intValue();
    }

    public int getScaleControlViewHeight() {
        return this.f2808G;
    }

    public int getScaleControlViewWidth() {
        return this.f2809H;
    }

    public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        this.f2810a = windowInsets.isRound() ? ScreenShape.ROUND : ScreenShape.RECTANGLE;
        return windowInsets;
    }

    public void onCreate(Context context, Bundle bundle) {
        if (bundle != null) {
            MapStatus mapStatus = (MapStatus) bundle.getParcelable("mapstatus");
            if (this.f2817l != null) {
                this.f2817l = (Point) bundle.getParcelable("scalePosition");
            }
            if (this.f2818m != null) {
                this.f2818m = (Point) bundle.getParcelable("zoomPosition");
            }
            this.f2826y = bundle.getBoolean("mZoomControlEnabled");
            this.f2827z = bundle.getBoolean("mScaleControlEnabled");
            setPadding(bundle.getInt("paddingLeft"), bundle.getInt("paddingTop"), bundle.getInt("paddingRight"), bundle.getInt("paddingBottom"));
            m2999a(context, new BaiduMapOptions().mapStatus(mapStatus));
        }
    }

    public final void onDestroy() {
        Context context = this.f2825w;
        if (context != null) {
            this.f2811f.mo13092b(context.hashCode());
        }
        Bitmap bitmap = this.f2814i;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f2814i.recycle();
            this.f2814i = null;
        }
        this.f2815j.mo12981b();
        BMapManager.destroy();
        C1083i.m3652b();
        AnimationTask animationTask = this.mTask;
        if (animationTask != null) {
            animationTask.cancel();
        }
        this.f2825w = null;
    }

    public final void onDismiss() {
        removeAllViews();
    }

    public final void onEnterAmbient(Bundle bundle) {
        m2997a(0);
    }

    public void onExitAmbient() {
        m2997a(1);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        AnimationTask animationTask;
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action == 1) {
                this.mTimer = new Timer();
                if (!(this.mTimer == null || (animationTask = this.mTask) == null)) {
                    animationTask.cancel();
                }
                this.mTask = new AnimationTask();
                this.mTimer.schedule(this.mTask, 5000);
            }
        } else if (this.f2815j.getVisibility() == 0) {
            Timer timer = this.mTimer;
            if (timer != null) {
                if (this.mTask != null) {
                    timer.cancel();
                    this.mTask.cancel();
                }
                this.mTimer = null;
                this.mTask = null;
            }
        } else if (this.f2815j.getVisibility() == 4) {
            if (this.mTimer != null) {
                AnimationTask animationTask2 = this.mTask;
                if (animationTask2 != null) {
                    animationTask2.cancel();
                }
                this.mTimer.cancel();
                this.mTask = null;
                this.mTimer = null;
            }
            m3004a(false);
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        float f;
        int i5;
        int i6;
        int i7;
        int i8;
        int childCount = getChildCount();
        m3001a(this.f2813h);
        float f2 = 1.0f;
        if (((getWidth() - this.f2804C) - this.f2805D) - this.f2813h.getMeasuredWidth() <= 0 || ((getHeight() - this.f2806E) - this.f2807F) - this.f2813h.getMeasuredHeight() <= 0) {
            this.f2804C = 0;
            this.f2805D = 0;
            this.f2807F = 0;
            this.f2806E = 0;
            f = 1.0f;
        } else {
            f2 = ((float) ((getWidth() - this.f2804C) - this.f2805D)) / ((float) getWidth());
            f = ((float) ((getHeight() - this.f2806E) - this.f2807F)) / ((float) getHeight());
        }
        for (int i9 = 0; i9 < childCount; i9++) {
            View childAt = getChildAt(i9);
            C1084j jVar = this.f2811f;
            if (childAt == jVar) {
                jVar.layout(0, 0, getWidth(), getHeight());
            } else if (childAt == this.f2813h) {
                int i10 = (int) (((float) this.f2807F) + (12.0f * f));
                if (this.f2810a == ScreenShape.ROUND) {
                    m3001a(this.f2815j);
                    int i11 = f2798s / 2;
                    i7 = m2994a(i11, this.f2815j.getMeasuredWidth() / 2);
                    i8 = f2800u + ((f2798s / 2) - m2994a(i11, i11 - i7));
                } else {
                    i8 = 0;
                    i7 = 0;
                }
                int i12 = (f2799t - i7) - i10;
                int i13 = f2798s - i8;
                this.f2813h.layout(i13 - this.f2813h.getMeasuredWidth(), i12 - this.f2813h.getMeasuredHeight(), i13, i12);
            } else {
                C1074ak akVar = this.f2815j;
                if (childAt == akVar) {
                    if (akVar.mo12980a()) {
                        m3001a(this.f2815j);
                        Point point = this.f2818m;
                        if (point == null) {
                            int a = (int) ((12.0f * f) + ((float) this.f2806E) + ((float) (this.f2810a == ScreenShape.ROUND ? m2994a(f2799t / 2, this.f2815j.getMeasuredWidth() / 2) : 0)));
                            int measuredWidth = (f2798s - this.f2815j.getMeasuredWidth()) / 2;
                            this.f2815j.layout(measuredWidth, a, this.f2815j.getMeasuredWidth() + measuredWidth, this.f2815j.getMeasuredHeight() + a);
                        } else {
                            this.f2815j.layout(point.x, this.f2818m.y, this.f2818m.x + this.f2815j.getMeasuredWidth(), this.f2818m.y + this.f2815j.getMeasuredHeight());
                        }
                    }
                } else if (childAt == this.f2819n) {
                    if (this.f2810a == ScreenShape.ROUND) {
                        m3001a(this.f2815j);
                        int i14 = f2798s / 2;
                        i5 = m2994a(i14, this.f2815j.getMeasuredWidth() / 2);
                        i6 = f2800u + ((f2798s / 2) - m2994a(i14, i14 - i5));
                    } else {
                        i6 = 0;
                        i5 = 0;
                    }
                    m3001a(this.f2819n);
                    Point point2 = this.f2817l;
                    if (point2 == null) {
                        int i15 = (int) (((float) this.f2807F) + (12.0f * f));
                        this.f2809H = this.f2819n.getMeasuredWidth();
                        this.f2808G = this.f2819n.getMeasuredHeight();
                        int i16 = (int) (((float) this.f2804C) + (5.0f * f2) + ((float) i6));
                        int i17 = (f2799t - i15) - i5;
                        this.f2819n.layout(i16, i17 - this.f2819n.getMeasuredHeight(), this.f2809H + i16, i17);
                    } else {
                        this.f2819n.layout(point2.x, this.f2817l.y, this.f2817l.x + this.f2819n.getMeasuredWidth(), this.f2817l.y + this.f2819n.getMeasuredHeight());
                    }
                } else {
                    SwipeDismissView swipeDismissView = this.f2820o;
                    if (childAt == swipeDismissView) {
                        m3001a(swipeDismissView);
                        this.f2820o.layout(0, 0, this.f2820o.getMeasuredWidth(), f2799t);
                    } else {
                        ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
                        if (layoutParams instanceof MapViewLayoutParams) {
                            MapViewLayoutParams mapViewLayoutParams = (MapViewLayoutParams) layoutParams;
                            Point a2 = mapViewLayoutParams.f2596c == MapViewLayoutParams.ELayoutMode.absoluteMode ? mapViewLayoutParams.f2595b : this.f2811f.mo13086a().mo13005a(CoordUtil.ll2mc(mapViewLayoutParams.f2594a));
                            m3001a(childAt);
                            int measuredWidth2 = childAt.getMeasuredWidth();
                            int measuredHeight = childAt.getMeasuredHeight();
                            float f3 = mapViewLayoutParams.f2597d;
                            float f4 = mapViewLayoutParams.f2598e;
                            int i18 = (int) (((float) a2.x) - (f3 * ((float) measuredWidth2)));
                            int i19 = ((int) (((float) a2.y) - (f4 * ((float) measuredHeight)))) + mapViewLayoutParams.f2599f;
                            childAt.layout(i18, i19, measuredWidth2 + i18, measuredHeight + i19);
                        }
                    }
                }
            }
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        BaiduMap baiduMap;
        if (bundle != null && (baiduMap = this.f2812g) != null) {
            bundle.putParcelable("mapstatus", baiduMap.getMapStatus());
            Point point = this.f2817l;
            if (point != null) {
                bundle.putParcelable("scalePosition", point);
            }
            Point point2 = this.f2818m;
            if (point2 != null) {
                bundle.putParcelable("zoomPosition", point2);
            }
            bundle.putBoolean("mZoomControlEnabled", this.f2826y);
            bundle.putBoolean("mScaleControlEnabled", this.f2827z);
            bundle.putInt("paddingLeft", this.f2804C);
            bundle.putInt("paddingTop", this.f2806E);
            bundle.putInt("paddingRight", this.f2805D);
            bundle.putInt("paddingBottom", this.f2807F);
        }
    }

    public void removeView(View view) {
        if (view != this.f2813h) {
            super.removeView(view);
        }
    }

    public void setOnDismissCallbackListener(OnDismissCallback onDismissCallback) {
        SwipeDismissView swipeDismissView = this.f2820o;
        if (swipeDismissView != null) {
            swipeDismissView.setCallback(onDismissCallback);
        }
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.f2804C = i;
        this.f2806E = i2;
        this.f2805D = i3;
        this.f2807F = i4;
    }

    public void setScaleControlPosition(Point point) {
        if (point != null && point.x >= 0 && point.y >= 0 && point.x <= getWidth() && point.y <= getHeight()) {
            this.f2817l = point;
            requestLayout();
        }
    }

    public void setShape(ScreenShape screenShape) {
        this.f2810a = screenShape;
    }

    public void setViewAnimitionEnable(boolean z) {
        this.f2816k = z;
    }

    public void setZoomControlsPosition(Point point) {
        if (point != null && point.x >= 0 && point.y >= 0 && point.x <= getWidth() && point.y <= getHeight()) {
            this.f2818m = point;
            requestLayout();
        }
    }

    public void showScaleControl(boolean z) {
        this.f2819n.setVisibility(z ? 0 : 8);
        this.f2827z = z;
    }

    public void showZoomControls(boolean z) {
        if (this.f2815j.mo12980a()) {
            this.f2815j.setVisibility(z ? 0 : 8);
            this.f2826y = z;
        }
    }
}
