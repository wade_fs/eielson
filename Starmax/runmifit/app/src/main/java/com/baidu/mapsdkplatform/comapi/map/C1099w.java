package com.baidu.mapsdkplatform.comapi.map;

import android.os.Message;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.w */
class C1099w {

    /* renamed from: a */
    private static final String f3618a = C1099w.class.getSimpleName();

    /* renamed from: b */
    private C1098v f3619b;

    C1099w() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13139a(Message message) {
        if (message.what == 65289) {
            int i = message.arg1;
            if (!(i == 12 || i == 101 || i == 102)) {
                switch (i) {
                    case -1:
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        break;
                    default:
                        return;
                }
            }
            C1098v vVar = this.f3619b;
            if (vVar != null) {
                vVar.mo11594a(message.arg1, message.arg2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13140a(C1098v vVar) {
        this.f3619b = vVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13141b(C1098v vVar) {
        this.f3619b = null;
    }
}
