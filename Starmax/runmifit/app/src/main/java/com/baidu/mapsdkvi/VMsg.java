package com.baidu.mapsdkvi;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

public class VMsg {

    /* renamed from: a */
    private static final String f3954a = VMsg.class.getSimpleName();

    /* renamed from: b */
    private static Handler f3955b;

    /* renamed from: c */
    private static HandlerThread f3956c;

    /* renamed from: d */
    private static VMsg f3957d = new VMsg();

    /* renamed from: com.baidu.mapsdkvi.VMsg$a */
    static class C1185a extends Handler {
        public C1185a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            VMsg.OnUserCommand1(message.what, message.arg1, message.arg2, message.obj == null ? 0 : ((Long) message.obj).longValue());
        }
    }

    public static native void InitClass(Object obj);

    /* access modifiers changed from: private */
    public static native void OnUserCommand1(int i, int i2, int i3, long j);

    public static void destroy() {
        f3956c.quit();
        f3956c = null;
        f3955b.removeCallbacksAndMessages(null);
        f3955b = null;
    }

    public static VMsg getInstance() {
        return f3957d;
    }

    public static void init() {
        f3956c = new HandlerThread("VIMsgThread");
        f3956c.start();
        f3955b = new C1185a(f3956c.getLooper());
    }

    private static void postMessage(int i, int i2, int i3, long j) {
        Handler handler = f3955b;
        if (handler != null) {
            Message.obtain(handler, i, i2, i3, j == 0 ? null : Long.valueOf(j)).sendToTarget();
        }
    }
}
