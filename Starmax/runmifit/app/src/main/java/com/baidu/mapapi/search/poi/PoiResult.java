package com.baidu.mapapi.search.poi;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.CityInfo;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import java.util.List;

public class PoiResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<PoiResult> CREATOR = new C0984f();

    /* renamed from: a */
    private int f3007a = 0;

    /* renamed from: b */
    private int f3008b = 0;

    /* renamed from: c */
    private int f3009c = 0;

    /* renamed from: d */
    private int f3010d = 0;

    /* renamed from: e */
    private List<PoiInfo> f3011e;

    /* renamed from: f */
    private boolean f3012f = false;

    /* renamed from: g */
    private List<PoiAddrInfo> f3013g;

    /* renamed from: h */
    private List<CityInfo> f3014h;

    public PoiResult() {
    }

    protected PoiResult(Parcel parcel) {
        super(parcel);
        boolean z = false;
        this.f3007a = parcel.readInt();
        this.f3008b = parcel.readInt();
        this.f3009c = parcel.readInt();
        this.f3010d = parcel.readInt();
        this.f3011e = parcel.createTypedArrayList(PoiInfo.CREATOR);
        this.f3012f = parcel.readByte() != 0 ? true : z;
        this.f3014h = parcel.createTypedArrayList(CityInfo.CREATOR);
    }

    public PoiResult(SearchResult.ERRORNO errorno) {
        super(errorno);
    }

    public int describeContents() {
        return 0;
    }

    public List<PoiAddrInfo> getAllAddr() {
        return this.f3013g;
    }

    public List<PoiInfo> getAllPoi() {
        return this.f3011e;
    }

    public int getCurrentPageCapacity() {
        return this.f3009c;
    }

    public int getCurrentPageNum() {
        return this.f3007a;
    }

    public List<CityInfo> getSuggestCityList() {
        return this.f3014h;
    }

    public int getTotalPageNum() {
        return this.f3008b;
    }

    public int getTotalPoiNum() {
        return this.f3010d;
    }

    public boolean isHasAddrInfo() {
        return this.f3012f;
    }

    public void setAddrInfo(List<PoiAddrInfo> list) {
        this.f3013g = list;
    }

    public void setCurrentPageCapacity(int i) {
        this.f3009c = i;
    }

    public void setCurrentPageNum(int i) {
        this.f3007a = i;
    }

    public void setHasAddrInfo(boolean z) {
        this.f3012f = z;
    }

    public void setPoiInfo(List<PoiInfo> list) {
        this.f3011e = list;
    }

    public void setSuggestCityList(List<CityInfo> list) {
        this.f3014h = list;
    }

    public void setTotalPageNum(int i) {
        this.f3008b = i;
    }

    public void setTotalPoiNum(int i) {
        this.f3010d = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.f3007a);
        parcel.writeInt(this.f3008b);
        parcel.writeInt(this.f3009c);
        parcel.writeInt(this.f3010d);
        parcel.writeTypedList(this.f3011e);
        parcel.writeByte(this.f3012f ? (byte) 1 : 0);
        parcel.writeTypedList(this.f3014h);
    }
}
