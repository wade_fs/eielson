package com.baidu.mobstat;

import android.content.Context;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.h */
public interface C1288h {
    /* renamed from: a */
    void mo13882a(Context context, long j);

    /* renamed from: a */
    void mo13883a(Context context, String str);

    /* renamed from: a */
    void mo13884a(Context context, JSONObject jSONObject);

    /* renamed from: a */
    boolean mo13885a(Context context);

    /* renamed from: b */
    void mo13886b(Context context, String str);

    /* renamed from: b */
    boolean mo13887b(Context context);
}
