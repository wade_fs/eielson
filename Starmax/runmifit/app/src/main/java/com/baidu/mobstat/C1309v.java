package com.baidu.mobstat;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;

/* renamed from: com.baidu.mobstat.v */
class C1309v extends C1299q {
    public C1309v() {
        super("app_apk3", "Create table if not exists app_apk3(_id Integer primary key AUTOINCREMENT,time VARCHAR(50),content TEXT);");
    }

    /* renamed from: a */
    public ArrayList<C1298p> mo13983a(int i, int i2) {
        Cursor a = mo13981a("time", i, i2);
        ArrayList<C1298p> a2 = m4866a(a);
        if (a != null) {
            a.close();
        }
        return a2;
    }

    /* renamed from: a */
    public long mo13980a(String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("time", str);
        contentValues.put("content", str2);
        return mo13979a(contentValues);
    }

    /* renamed from: b */
    public boolean mo13987b(long j) {
        return mo13985a(j);
    }

    /* renamed from: a */
    private ArrayList<C1298p> m4866a(Cursor cursor) {
        ArrayList<C1298p> arrayList = new ArrayList<>();
        if (cursor == null || cursor.getCount() == 0) {
            return arrayList;
        }
        int columnIndex = cursor.getColumnIndex("_id");
        int columnIndex2 = cursor.getColumnIndex("time");
        int columnIndex3 = cursor.getColumnIndex("content");
        while (cursor.moveToNext()) {
            arrayList.add(new C1298p(cursor.getLong(columnIndex), cursor.getString(columnIndex2), cursor.getString(columnIndex3)));
        }
        return arrayList;
    }
}
