package com.baidu.mapapi.model;

import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapapi.model.inner.Point;
import com.baidu.mapsdkplatform.comapi.util.C1166b;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.mapsdkplatform.comjni.tools.C1182a;
import java.util.List;

public class CoordUtil {
    public static LatLng Coordinate_encryptEx(float f, float f2, String str) {
        return C1166b.m4236a(f, f2, str);
    }

    public static LatLng decodeLocation(String str) {
        CoordType coordType = SDKInitializer.getCoordType();
        CoordType coordType2 = CoordType.GCJ02;
        LatLng a = C1166b.m4238a(str);
        return coordType == coordType2 ? CoordTrans.baiduToGcj(a) : a;
    }

    public static List<LatLng> decodeLocationList(String str) {
        return C1166b.m4243c(str);
    }

    public static List<List<LatLng>> decodeLocationList2D(String str) {
        return C1166b.m4244d(str);
    }

    public static LatLng decodeNodeLocation(String str) {
        CoordType coordType = SDKInitializer.getCoordType();
        CoordType coordType2 = CoordType.GCJ02;
        LatLng b = C1166b.m4241b(str);
        return coordType == coordType2 ? CoordTrans.baiduToGcj(b) : b;
    }

    public static double getDistance(Point point, Point point2) {
        return C1182a.m4373a(point, point2);
    }

    public static int getMCDistanceByOneLatLngAndRadius(LatLng latLng, int i) {
        return SDKInitializer.getCoordType() == CoordType.GCJ02 ? C1166b.m4235a(CoordTrans.gcjToBaidu(latLng), i) : C1166b.m4235a(latLng, i);
    }

    public static GeoPoint ll2mc(LatLng latLng) {
        return SDKInitializer.getCoordType() == CoordType.GCJ02 ? C1166b.m4239a(CoordTrans.gcjToBaidu(latLng)) : C1166b.m4239a(latLng);
    }

    public static Point ll2point(LatLng latLng) {
        return SDKInitializer.getCoordType() == CoordType.GCJ02 ? C1166b.m4242b(CoordTrans.gcjToBaidu(latLng)) : C1166b.m4242b(latLng);
    }

    public static LatLng mc2ll(GeoPoint geoPoint) {
        CoordType coordType = SDKInitializer.getCoordType();
        CoordType coordType2 = CoordType.GCJ02;
        LatLng a = C1166b.m4237a(geoPoint);
        return coordType == coordType2 ? CoordTrans.baiduToGcj(a) : a;
    }
}
