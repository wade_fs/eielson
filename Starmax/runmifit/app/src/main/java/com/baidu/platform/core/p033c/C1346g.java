package com.baidu.platform.core.p033c;

import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.PoiChildrenInfo;
import com.baidu.mapapi.search.core.PoiDetailInfo;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1319d;
import com.baidu.platform.base.SearchType;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.core.c.g */
public class C1346g extends C1319d {

    /* renamed from: b */
    private static final String f4460b = C1346g.class.getSimpleName();

    /* renamed from: c */
    private int f4461c;

    /* renamed from: d */
    private int f4462d;

    C1346g(int i, int i2) {
        this.f4461c = i;
        this.f4462d = i2;
    }

    /* renamed from: a */
    private LatLng m5012a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        double optDouble = jSONObject.optDouble("lat");
        double optDouble2 = jSONObject.optDouble("lng");
        return SDKInitializer.getCoordType() == CoordType.GCJ02 ? CoordTrans.baiduToGcj(new LatLng(optDouble, optDouble2)) : new LatLng(optDouble, optDouble2);
    }

    /* renamed from: a */
    private boolean m5013a(String str, PoiResult poiResult) {
        if (str != null && !str.equals("") && !str.isEmpty()) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt(NotificationCompat.CATEGORY_STATUS);
                if (optInt == 0) {
                    return m5014a(jSONObject, poiResult);
                }
                poiResult.error = optInt != 1 ? optInt != 2 ? SearchResult.ERRORNO.RESULT_NOT_FOUND : SearchResult.ERRORNO.SEARCH_OPTION_ERROR : SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR;
                return false;
            } catch (JSONException e) {
                Log.e(f4460b, "Parse poi search failed", e);
                poiResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
            }
        }
        return false;
    }

    /* renamed from: a */
    private boolean m5014a(JSONObject jSONObject, PoiResult poiResult) {
        if (!(jSONObject == null || jSONObject.length() == 0)) {
            poiResult.error = SearchResult.ERRORNO.NO_ERROR;
            JSONArray optJSONArray = jSONObject.optJSONArray("results");
            if (optJSONArray == null || optJSONArray.length() <= 0) {
                poiResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
            } else {
                int optInt = jSONObject.optInt(Config.EXCEPTION_MEMORY_TOTAL);
                poiResult.setTotalPoiNum(optInt);
                int length = optJSONArray.length();
                poiResult.setCurrentPageCapacity(length);
                poiResult.setCurrentPageNum(this.f4461c);
                if (length != 0) {
                    int i = this.f4462d;
                    poiResult.setTotalPageNum((optInt / i) + (optInt % i > 0 ? 1 : 0));
                }
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                    JSONObject jSONObject2 = (JSONObject) optJSONArray.opt(i2);
                    if (!(jSONObject2 == null || jSONObject2.length() == 0)) {
                        PoiInfo poiInfo = new PoiInfo();
                        poiInfo.setName(jSONObject2.optString(Config.FEED_LIST_NAME));
                        poiInfo.setAddress(jSONObject2.optString("address"));
                        poiInfo.setProvince(jSONObject2.optString("province"));
                        poiInfo.setCity(jSONObject2.optString("city"));
                        poiInfo.setArea(jSONObject2.optString("area"));
                        poiInfo.setStreetId(jSONObject2.optString("street_id"));
                        poiInfo.setUid(jSONObject2.optString(Config.CUSTOM_USER_ID));
                        poiInfo.setPhoneNum(jSONObject2.optString("telephone"));
                        poiInfo.setDetail(jSONObject2.optInt("detail"));
                        poiInfo.setLocation(m5012a(jSONObject2.optJSONObject("location")));
                        String optString = jSONObject2.optString("detail_info");
                        if (!(optString == null || optString.length() == 0)) {
                            poiInfo.setPoiDetailInfo(m5015b(optString));
                        }
                        arrayList.add(poiInfo);
                    }
                }
                poiResult.setPoiInfo(arrayList);
                return true;
            }
        }
        return false;
    }

    /* renamed from: b */
    private PoiDetailInfo m5015b(String str) {
        PoiDetailInfo poiDetailInfo = new PoiDetailInfo();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.length() == 0) {
                return null;
            }
            poiDetailInfo.setDistance(jSONObject.optInt("distance", 0));
            poiDetailInfo.setTag(jSONObject.optString("tag"));
            poiDetailInfo.setDetailUrl(jSONObject.optString("detail_url"));
            poiDetailInfo.setType(jSONObject.optString(SocialConstants.PARAM_TYPE));
            poiDetailInfo.setPrice(jSONObject.optDouble("price", 0.0d));
            poiDetailInfo.setOverallRating(jSONObject.optDouble("overall_rating", 0.0d));
            poiDetailInfo.setTasteRating(jSONObject.optDouble("taste_rating", 0.0d));
            poiDetailInfo.setServiceRating(jSONObject.optDouble("service_rating", 0.0d));
            poiDetailInfo.setEnvironmentRating(jSONObject.optDouble("environment_rating", 0.0d));
            poiDetailInfo.setFacilityRating(jSONObject.optDouble("facility_rating", 0.0d));
            poiDetailInfo.setHygieneRating(jSONObject.optDouble("hygiene_rating", 0.0d));
            poiDetailInfo.setTechnologyRating(jSONObject.optDouble("technology_rating", 0.0d));
            poiDetailInfo.setImageNum(jSONObject.optInt("image_num"));
            poiDetailInfo.setGrouponNum(jSONObject.optInt("groupon_num"));
            poiDetailInfo.setCommentNum(jSONObject.optInt("comment_num"));
            poiDetailInfo.setDiscountNum(jSONObject.optInt("discount_num"));
            poiDetailInfo.setFavoriteNum(jSONObject.optInt("favorite_num"));
            poiDetailInfo.setCheckinNum(jSONObject.optInt("checkin_num"));
            poiDetailInfo.setShopHours(jSONObject.optString("shop_hours"));
            poiDetailInfo.naviLocation = m5012a(jSONObject.optJSONObject("navi_location"));
            SearchType a = mo14019a();
            if (SearchType.POI_IN_CITY_SEARCH == a || SearchType.POI_NEAR_BY_SEARCH == a) {
                poiDetailInfo.setPoiChildrenInfoList(m5016b(jSONObject));
            }
            return poiDetailInfo;
        } catch (JSONException e) {
            Log.e(f4460b, "Parse poi search detail info failed", e);
            return null;
        }
    }

    /* renamed from: b */
    private List<PoiChildrenInfo> m5016b(JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("children");
        if (optJSONArray == null || optJSONArray.length() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (!(optJSONObject == null || optJSONObject.length() == 0)) {
                PoiChildrenInfo poiChildrenInfo = new PoiChildrenInfo();
                poiChildrenInfo.setUid(optJSONObject.optString(Config.CUSTOM_USER_ID));
                poiChildrenInfo.setName(optJSONObject.optString(Config.FEED_LIST_NAME));
                poiChildrenInfo.setShowName(optJSONObject.optString("show_name"));
                poiChildrenInfo.setTag(optJSONObject.optString("tag"));
                poiChildrenInfo.setLocation(m5012a(optJSONObject.optJSONObject("location")));
                poiChildrenInfo.setAddress(optJSONObject.optString("address"));
                arrayList.add(poiChildrenInfo);
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    public SearchResult mo14018a(String str) {
        SearchResult.ERRORNO errorno;
        PoiResult poiResult = new PoiResult();
        if (str != null && !str.equals("") && !str.isEmpty()) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has("SDK_InnerError")) {
                    JSONObject optJSONObject = jSONObject.optJSONObject("SDK_InnerError");
                    if (optJSONObject.has("PermissionCheckError")) {
                        errorno = SearchResult.ERRORNO.PERMISSION_UNFINISHED;
                        poiResult.error = errorno;
                        return poiResult;
                    } else if (optJSONObject.has("httpStateError")) {
                        String optString = optJSONObject.optString("httpStateError");
                        char c = 65535;
                        int hashCode = optString.hashCode();
                        if (hashCode != -879828873) {
                            if (hashCode == 1470557208 && optString.equals("REQUEST_ERROR")) {
                                c = 1;
                            }
                        } else if (optString.equals("NETWORK_ERROR")) {
                            c = 0;
                        }
                        poiResult.error = c != 0 ? c != 1 ? SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR : SearchResult.ERRORNO.REQUEST_ERROR : SearchResult.ERRORNO.NETWORK_ERROR;
                        return poiResult;
                    }
                }
                if (mo14022a(str, poiResult, false)) {
                    return poiResult;
                }
                poiResult.error = m5013a(str, poiResult) ? SearchResult.ERRORNO.NO_ERROR : SearchResult.ERRORNO.RESULT_NOT_FOUND;
                return poiResult;
            } catch (JSONException e) {
                Log.e(f4460b, "Parse poi search error", e);
            }
        }
        errorno = SearchResult.ERRORNO.RESULT_NOT_FOUND;
        poiResult.error = errorno;
        return poiResult;
    }

    /* renamed from: a */
    public void mo14020a(SearchResult searchResult, Object obj) {
        if (obj != null && (obj instanceof OnGetPoiSearchResultListener)) {
            int i = C1347h.f4463a[mo14019a().ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                ((OnGetPoiSearchResultListener) obj).onGetPoiResult((PoiResult) searchResult);
            }
        }
    }
}
