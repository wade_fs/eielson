package com.baidu.platform.core.p032b;

import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeOption;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;
import com.tamic.novate.download.MimeType;

/* renamed from: com.baidu.platform.core.b.f */
public class C1336f extends C1320e {
    public C1336f(ReverseGeoCodeOption reverseGeoCodeOption) {
        m4971a(reverseGeoCodeOption);
    }

    /* renamed from: a */
    private void m4971a(ReverseGeoCodeOption reverseGeoCodeOption) {
        if (reverseGeoCodeOption.getLocation() != null) {
            LatLng latLng = new LatLng(reverseGeoCodeOption.getLocation().latitude, reverseGeoCodeOption.getLocation().longitude);
            if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                latLng = CoordTrans.gcjToBaidu(latLng);
            }
            C1381a aVar = this.f4435a;
            aVar.mo14094a("location", latLng.latitude + "," + latLng.longitude);
        }
        this.f4435a.mo14094a("coordtype", "bd09ll");
        this.f4435a.mo14094a("page_index", String.valueOf(reverseGeoCodeOption.getPageNum()));
        this.f4435a.mo14094a("page_size", String.valueOf(reverseGeoCodeOption.getPageSize()));
        this.f4435a.mo14094a("pois", "1");
        this.f4435a.mo14094a("output", MimeType.JSON);
        this.f4435a.mo14094a("from", "android_map_sdk");
        this.f4435a.mo14094a("latest_admin", String.valueOf(reverseGeoCodeOption.getLatestAdmin()));
        this.f4435a.mo14094a("radius", String.valueOf(reverseGeoCodeOption.getRadius()));
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14080e();
    }
}
