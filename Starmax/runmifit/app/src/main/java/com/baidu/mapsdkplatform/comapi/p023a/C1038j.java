package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.map.Marker;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.j */
public class C1038j extends C1031c {

    /* renamed from: a */
    private Animator f3337a = null;

    /* renamed from: b */
    private long f3338b = 0;

    /* renamed from: c */
    private Interpolator f3339c = null;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Animation.AnimationListener f3340d = null;

    /* renamed from: e */
    private int f3341e = 1;

    /* renamed from: f */
    private int f3342f = 0;

    /* renamed from: g */
    private float[] f3343g;

    /* renamed from: h */
    private int f3344h = 1;

    public C1038j(int i, float... fArr) {
        this.f3343g = fArr;
        this.f3344h = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ObjectAnimator mo12906a(Marker marker) {
        int i = this.f3344h;
        ObjectAnimator ofFloat = i == 1 ? ObjectAnimator.ofFloat(marker, "scaleX", this.f3343g) : i == 2 ? ObjectAnimator.ofFloat(marker, "scaleY", this.f3343g) : null;
        if (ofFloat != null) {
            ofFloat.setRepeatCount(this.f3342f);
            ofFloat.setRepeatMode(mo12907c());
            ofFloat.setDuration(this.f3338b);
            Interpolator interpolator = this.f3339c;
            if (interpolator != null) {
                ofFloat.setInterpolator(interpolator);
            }
        }
        return ofFloat;
    }

    /* renamed from: a */
    public void mo12874a() {
        Animator animator = this.f3337a;
        if (animator != null) {
            animator.start();
        }
    }

    /* renamed from: a */
    public void mo12875a(int i) {
        this.f3341e = i;
    }

    /* renamed from: a */
    public void mo12876a(long j) {
        if (j < 0) {
            j = 0;
        }
        this.f3338b = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12877a(Animator animator) {
        if (animator != null) {
            animator.addListener(new C1039k(this));
        }
    }

    /* renamed from: a */
    public void mo12878a(Interpolator interpolator) {
        this.f3339c = interpolator;
    }

    /* renamed from: a */
    public void mo12879a(Animation.AnimationListener animationListener) {
        this.f3340d = animationListener;
    }

    /* renamed from: a */
    public void mo12880a(Marker marker, Animation animation) {
        this.f3337a = mo12906a(marker);
        mo12877a(this.f3337a);
    }

    /* renamed from: b */
    public void mo12881b() {
        Animator animator = this.f3337a;
        if (animator != null) {
            animator.cancel();
            this.f3337a = null;
        }
    }

    /* renamed from: b */
    public void mo12882b(int i) {
        if (i > 0 || i == -1) {
            this.f3342f = i;
        }
    }

    /* renamed from: c */
    public int mo12907c() {
        return this.f3341e;
    }

    /* renamed from: c */
    public void mo12884c(int i) {
    }
}
