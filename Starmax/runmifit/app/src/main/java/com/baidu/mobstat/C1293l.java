package com.baidu.mobstat;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.baidu.mobstat.C1262ay;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.l */
class C1293l {

    /* renamed from: a */
    static final C1293l f4365a = new C1293l();

    C1293l() {
    }

    /* renamed from: a */
    public synchronized void mo13973a(Context context, boolean z) {
        m4812b(context, z);
    }

    /* renamed from: b */
    private void m4812b(Context context, boolean z) {
        String str;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager != null) {
            List<PackageInfo> arrayList = new ArrayList<>(1);
            try {
                arrayList = packageManager.getInstalledPackages(0);
            } catch (Exception e) {
                C1255as.m4626c().mo13944b(e);
            }
            JSONArray jSONArray = new JSONArray();
            for (PackageInfo packageInfo : arrayList) {
                ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                if (applicationInfo != null) {
                    boolean z2 = (applicationInfo.flags & 1) != 0;
                    String charSequence = applicationInfo.loadLabel(packageManager).toString();
                    String str2 = applicationInfo.sourceDir;
                    if (z == z2) {
                        m4811a(z, charSequence, str2, packageInfo, jSONArray);
                    }
                }
            }
            if (jSONArray.length() != 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(System.currentTimeMillis() + "|");
                sb.append(z ? 1 : 0);
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("app_list", jSONArray);
                    jSONObject.put("meta-data", sb.toString());
                    str = C1262ay.C1263a.m4665a(jSONObject.toString().getBytes());
                } catch (Exception unused) {
                    str = "";
                }
                if (!TextUtils.isEmpty(str)) {
                    C1300r.APP_LIST.mo13990a(System.currentTimeMillis(), str);
                }
            }
        }
    }

    /* renamed from: a */
    private void m4811a(boolean z, String str, String str2, PackageInfo packageInfo, JSONArray jSONArray) {
        long j;
        if (!z || !packageInfo.packageName.startsWith("com.android.")) {
            long j2 = 0;
            try {
                j = packageInfo.firstInstallTime;
            } catch (Throwable th) {
                C1255as.m4626c().mo13944b(th);
                j = 0;
            }
            try {
                j2 = packageInfo.lastUpdateTime;
            } catch (Throwable th2) {
                C1255as.m4626c().mo13944b(th2);
            }
            long a = m4810a(str2);
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("n", packageInfo.packageName);
                jSONObject.put(Config.APP_VERSION_CODE, str);
                jSONObject.put("v", String.valueOf(packageInfo.versionName));
                jSONObject.put("f", j);
                jSONObject.put("l", j2);
                jSONObject.put(Config.MODEL, a);
                jSONArray.put(jSONObject);
            } catch (JSONException e) {
                C1255as.m4626c().mo13944b(e);
            }
        }
    }

    /* renamed from: a */
    private long m4810a(String str) {
        if (str != null) {
            File file = new File(str);
            if (file.exists()) {
                return file.lastModified();
            }
        }
        return 0;
    }
}
