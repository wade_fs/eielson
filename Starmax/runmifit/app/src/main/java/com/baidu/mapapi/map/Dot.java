package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;

public final class Dot extends Overlay {

    /* renamed from: a */
    LatLng f2453a;

    /* renamed from: b */
    int f2454b;

    /* renamed from: c */
    int f2455c;

    Dot() {
        this.type = C1082h.dot;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        super.mo10900a(bundle);
        GeoPoint ll2mc = CoordUtil.ll2mc(this.f2453a);
        bundle.putDouble("location_x", ll2mc.getLongitudeE6());
        bundle.putDouble("location_y", ll2mc.getLatitudeE6());
        bundle.putInt("radius", this.f2455c);
        Overlay.m2939a(this.f2454b, bundle);
        return bundle;
    }

    public LatLng getCenter() {
        return this.f2453a;
    }

    public int getColor() {
        return this.f2454b;
    }

    public int getRadius() {
        return this.f2455c;
    }

    public void setCenter(LatLng latLng) {
        if (latLng != null) {
            this.f2453a = latLng;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: dot center can not be null");
    }

    public void setColor(int i) {
        this.f2454b = i;
        this.listener.mo11330b(super);
    }

    public void setRadius(int i) {
        if (i > 0) {
            this.f2455c = i;
            this.listener.mo11330b(super);
        }
    }
}
