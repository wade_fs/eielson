package com.baidu.mapsdkplatform.comapi.synchronization.p029d;

import android.util.Log;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.d.a */
public final class C1120a {

    /* renamed from: a */
    private static boolean f3697a = true;

    /* renamed from: a */
    private static String m3850a() {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[2];
        return stackTraceElement.getFileName() + ": Line " + stackTraceElement.getLineNumber();
    }

    /* renamed from: a */
    public static void m3851a(String str, String str2) {
        if (f3697a) {
            Log.d(str, str2);
        }
    }

    /* renamed from: a */
    public static void m3852a(String str, String str2, Throwable th) {
        if (f3697a) {
            Log.e(str, str2, th);
        }
    }

    /* renamed from: a */
    public static void m3853a(boolean z) {
        f3697a = z;
    }

    /* renamed from: b */
    private static String m3854b() {
        return new Throwable().getStackTrace()[2].getMethodName();
    }

    /* renamed from: b */
    public static void m3855b(String str, String str2) {
        if (f3697a) {
            Log.e(str, str2);
        }
    }

    /* renamed from: c */
    public static void m3856c(String str, String str2) {
        Log.d(str + "-" + m3854b() + "-" + m3850a(), str2);
    }
}
