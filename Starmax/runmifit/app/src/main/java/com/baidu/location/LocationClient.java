package com.baidu.location;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import com.baidu.location.p013a.C0727c;
import com.baidu.location.p013a.C0741j;
import com.baidu.location.p013a.C0742k;
import com.baidu.location.p015c.C0786a;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import com.tencent.mid.api.MidEntity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public final class LocationClient implements C0727c.C0728a {
    public static final int CONNECT_HOT_SPOT_FALSE = 0;
    public static final int CONNECT_HOT_SPOT_TRUE = 1;
    public static final int CONNECT_HOT_SPOT_UNKNOWN = -1;
    public static final int LOC_DIAGNOSTIC_TYPE_BETTER_OPEN_GPS = 1;
    public static final int LOC_DIAGNOSTIC_TYPE_BETTER_OPEN_WIFI = 2;
    public static final int LOC_DIAGNOSTIC_TYPE_FAIL_UNKNOWN = 9;
    public static final int LOC_DIAGNOSTIC_TYPE_NEED_CHECK_LOC_PERMISSION = 4;
    public static final int LOC_DIAGNOSTIC_TYPE_NEED_CHECK_NET = 3;
    public static final int LOC_DIAGNOSTIC_TYPE_NEED_CLOSE_FLYMODE = 7;
    public static final int LOC_DIAGNOSTIC_TYPE_NEED_INSERT_SIMCARD_OR_OPEN_WIFI = 6;
    public static final int LOC_DIAGNOSTIC_TYPE_NEED_OPEN_PHONE_LOC_SWITCH = 5;
    public static final int LOC_DIAGNOSTIC_TYPE_SERVER_FAIL = 8;

    /* renamed from: A */
    private Boolean f1164A = false;

    /* renamed from: B */
    private Boolean f1165B = false;
    /* access modifiers changed from: private */

    /* renamed from: C */
    public Boolean f1166C = true;

    /* renamed from: D */
    private boolean f1167D;
    /* access modifiers changed from: private */

    /* renamed from: E */
    public C0727c f1168E = null;
    /* access modifiers changed from: private */

    /* renamed from: F */
    public boolean f1169F = false;
    /* access modifiers changed from: private */

    /* renamed from: G */
    public boolean f1170G = false;

    /* renamed from: H */
    private boolean f1171H = false;

    /* renamed from: I */
    private ServiceConnection f1172I = new C0769b(this);

    /* renamed from: a */
    private long f1173a = 0;

    /* renamed from: b */
    private String f1174b = null;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public LocationClientOption f1175c = new LocationClientOption();
    /* access modifiers changed from: private */

    /* renamed from: d */
    public LocationClientOption f1176d = new LocationClientOption();
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f1177e = false;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public Context f1178f = null;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public Messenger f1179g = null;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public C0719a f1180h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public final Messenger f1181i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public ArrayList<BDLocationListener> f1182j = null;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public ArrayList<BDAbstractLocationListener> f1183k = null;

    /* renamed from: l */
    private BDLocation f1184l = null;

    /* renamed from: m */
    private boolean f1185m = false;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public boolean f1186n = false;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public boolean f1187o = false;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public C0720b f1188p = null;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public boolean f1189q = false;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public final Object f1190r = new Object();

    /* renamed from: s */
    private long f1191s = 0;

    /* renamed from: t */
    private long f1192t = 0;

    /* renamed from: u */
    private C0786a f1193u = null;

    /* renamed from: v */
    private BDLocationListener f1194v = null;

    /* renamed from: w */
    private String f1195w = null;

    /* renamed from: x */
    private String f1196x;

    /* renamed from: y */
    private boolean f1197y = false;
    /* access modifiers changed from: private */

    /* renamed from: z */
    public boolean f1198z = true;

    /* renamed from: com.baidu.location.LocationClient$a */
    private static class C0719a extends Handler {

        /* renamed from: a */
        private final WeakReference<LocationClient> f1199a;

        C0719a(Looper looper, LocationClient locationClient) {
            super(looper);
            this.f1199a = new WeakReference<>(locationClient);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.LocationClient.d(com.baidu.location.LocationClient, boolean):boolean
         arg types: [com.baidu.location.LocationClient, int]
         candidates:
          com.baidu.location.LocationClient.d(com.baidu.location.LocationClient, android.os.Message):void
          com.baidu.location.LocationClient.d(com.baidu.location.LocationClient, boolean):boolean */
        public void handleMessage(Message message) {
            LocationClient locationClient = this.f1199a.get();
            if (locationClient != null) {
                int i = message.what;
                int i2 = 21;
                boolean z = true;
                if (i == 21) {
                    Bundle data = message.getData();
                    data.setClassLoader(BDLocation.class.getClassLoader());
                    BDLocation bDLocation = (BDLocation) data.getParcelable("locStr");
                    if (locationClient.f1170G || !locationClient.f1169F || bDLocation.getLocType() != 66) {
                        if (!locationClient.f1170G && locationClient.f1169F) {
                            boolean unused = locationClient.f1170G = true;
                            return;
                        } else if (!locationClient.f1170G) {
                            boolean unused2 = locationClient.f1170G = true;
                        }
                    } else {
                        return;
                    }
                } else if (i == 303) {
                    Bundle data2 = message.getData();
                    int i3 = data2.getInt("loctype");
                    int i4 = data2.getInt("diagtype");
                    byte[] byteArray = data2.getByteArray("diagmessage");
                    if (i3 > 0 && i4 > 0 && byteArray != null && locationClient.f1183k != null) {
                        Iterator it = locationClient.f1183k.iterator();
                        while (it.hasNext()) {
                            ((BDAbstractLocationListener) it.next()).onLocDiagnosticMessage(i3, i4, new String(byteArray, "UTF-8"));
                        }
                        return;
                    }
                    return;
                } else if (i == 406) {
                    Bundle data3 = message.getData();
                    byte[] byteArray2 = data3.getByteArray(MidEntity.TAG_MAC);
                    String str = null;
                    if (byteArray2 != null) {
                        str = new String(byteArray2, "UTF-8");
                    }
                    int i5 = data3.getInt("hotspot", -1);
                    if (locationClient.f1183k != null) {
                        Iterator it2 = locationClient.f1183k.iterator();
                        while (it2.hasNext()) {
                            ((BDAbstractLocationListener) it2.next()).onConnectHotSpotMessage(str, i5);
                        }
                        return;
                    }
                    return;
                } else if (i == 701) {
                    locationClient.m1671a((BDLocation) message.obj);
                    return;
                } else if (i == 1300) {
                    locationClient.m1702f(message);
                    return;
                } else if (i != 1400) {
                    i2 = 26;
                    if (i != 26) {
                        if (i != 27) {
                            if (i != 54) {
                                z = false;
                                if (i != 55) {
                                    if (i == 703) {
                                        Bundle data4 = message.getData();
                                        int i6 = data4.getInt(Config.FEED_LIST_ITEM_CUSTOM_ID, 0);
                                        if (i6 > 0) {
                                            locationClient.m1668a(i6, (Notification) data4.getParcelable("notification"));
                                            return;
                                        }
                                        return;
                                    } else if (i != 704) {
                                        switch (i) {
                                            case 1:
                                                locationClient.m1679b();
                                                return;
                                            case 2:
                                                locationClient.m1686c();
                                                return;
                                            case 3:
                                                locationClient.m1687c(message);
                                                return;
                                            case 4:
                                                locationClient.m1701f();
                                                return;
                                            case 5:
                                                locationClient.m1697e(message);
                                                return;
                                            case 6:
                                                locationClient.m1708h(message);
                                                return;
                                            case 7:
                                                return;
                                            case 8:
                                                locationClient.m1692d(message);
                                                return;
                                            case 9:
                                                locationClient.m1669a(message);
                                                return;
                                            case 10:
                                                locationClient.m1681b(message);
                                                return;
                                            case 11:
                                                locationClient.m1696e();
                                                return;
                                            case 12:
                                                locationClient.m1667a();
                                                return;
                                            default:
                                                super.handleMessage(message);
                                                return;
                                        }
                                    } else {
                                        try {
                                            locationClient.m1676a(message.getData().getBoolean("removenotify"));
                                            return;
                                        } catch (Exception unused3) {
                                            return;
                                        }
                                    }
                                } else if (!locationClient.f1175c.location_change_notify) {
                                    return;
                                }
                            } else if (!locationClient.f1175c.location_change_notify) {
                                return;
                            }
                            boolean unused4 = locationClient.f1189q = z;
                            return;
                        }
                        locationClient.m1711i(message);
                        return;
                    }
                } else {
                    locationClient.m1705g(message);
                    return;
                }
                locationClient.m1670a(message, i2);
            }
        }
    }

    /* renamed from: com.baidu.location.LocationClient$b */
    private class C0720b implements Runnable {
        private C0720b() {
        }

        /* synthetic */ C0720b(LocationClient locationClient, C0769b bVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.LocationClient.b(com.baidu.location.LocationClient, boolean):boolean
         arg types: [com.baidu.location.LocationClient, int]
         candidates:
          com.baidu.location.LocationClient.b(com.baidu.location.LocationClient, android.os.Message):void
          com.baidu.location.LocationClient.b(com.baidu.location.LocationClient, boolean):boolean */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x008f, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0091, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r5 = this;
                com.baidu.location.LocationClient r0 = com.baidu.location.LocationClient.this
                java.lang.Object r0 = r0.f1190r
                monitor-enter(r0)
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                r2 = 0
                boolean unused = r1.f1187o = r2     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                android.os.Messenger r1 = r1.f1179g     // Catch:{ all -> 0x0092 }
                if (r1 == 0) goto L_0x0090
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                android.os.Messenger r1 = r1.f1181i     // Catch:{ all -> 0x0092 }
                if (r1 != 0) goto L_0x001e
                goto L_0x0090
            L_0x001e:
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                java.util.ArrayList r1 = r1.f1182j     // Catch:{ all -> 0x0092 }
                r2 = 1
                if (r1 == 0) goto L_0x0033
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                java.util.ArrayList r1 = r1.f1182j     // Catch:{ all -> 0x0092 }
                int r1 = r1.size()     // Catch:{ all -> 0x0092 }
                if (r1 >= r2) goto L_0x0048
            L_0x0033:
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                java.util.ArrayList r1 = r1.f1183k     // Catch:{ all -> 0x0092 }
                if (r1 == 0) goto L_0x008e
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                java.util.ArrayList r1 = r1.f1183k     // Catch:{ all -> 0x0092 }
                int r1 = r1.size()     // Catch:{ all -> 0x0092 }
                if (r1 >= r2) goto L_0x0048
                goto L_0x008e
            L_0x0048:
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                boolean r1 = r1.f1186n     // Catch:{ all -> 0x0092 }
                if (r1 == 0) goto L_0x007e
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient$b r1 = r1.f1188p     // Catch:{ all -> 0x0092 }
                if (r1 != 0) goto L_0x0064
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient$b r2 = new com.baidu.location.LocationClient$b     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient r3 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                r2.<init>()     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient.C0720b unused = r1.f1188p = r2     // Catch:{ all -> 0x0092 }
            L_0x0064:
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient$a r1 = r1.f1180h     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient r2 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient$b r2 = r2.f1188p     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient r3 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClientOption r3 = r3.f1175c     // Catch:{ all -> 0x0092 }
                int r3 = r3.scanSpan     // Catch:{ all -> 0x0092 }
                long r3 = (long) r3     // Catch:{ all -> 0x0092 }
                r1.postDelayed(r2, r3)     // Catch:{ all -> 0x0092 }
                monitor-exit(r0)     // Catch:{ all -> 0x0092 }
                return
            L_0x007e:
                com.baidu.location.LocationClient r1 = com.baidu.location.LocationClient.this     // Catch:{ all -> 0x0092 }
                com.baidu.location.LocationClient$a r1 = r1.f1180h     // Catch:{ all -> 0x0092 }
                r2 = 4
                android.os.Message r1 = r1.obtainMessage(r2)     // Catch:{ all -> 0x0092 }
                r1.sendToTarget()     // Catch:{ all -> 0x0092 }
                monitor-exit(r0)     // Catch:{ all -> 0x0092 }
                return
            L_0x008e:
                monitor-exit(r0)     // Catch:{ all -> 0x0092 }
                return
            L_0x0090:
                monitor-exit(r0)     // Catch:{ all -> 0x0092 }
                return
            L_0x0092:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0092 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.LocationClient.C0720b.run():void");
        }
    }

    public LocationClient(Context context) {
        this.f1178f = context;
        this.f1175c = new LocationClientOption();
        this.f1180h = new C0719a(Looper.getMainLooper(), this);
        this.f1181i = new Messenger(this.f1180h);
    }

    public LocationClient(Context context, LocationClientOption locationClientOption) {
        this.f1178f = context;
        this.f1175c = locationClientOption;
        this.f1176d = new LocationClientOption(locationClientOption);
        this.f1180h = new C0719a(Looper.getMainLooper(), this);
        this.f1181i = new Messenger(this.f1180h);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1667a() {
        Message obtain = Message.obtain((Handler) null, 28);
        try {
            obtain.replyTo = this.f1181i;
            this.f1179g.send(obtain);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1668a(int i, Notification notification) {
        try {
            Intent intent = new Intent(this.f1178f, C0839f.class);
            intent.putExtra("notification", notification);
            intent.putExtra(Config.FEED_LIST_ITEM_CUSTOM_ID, i);
            intent.putExtra("command", 1);
            if (Build.VERSION.SDK_INT >= 26) {
                this.f1178f.startForegroundService(intent);
            } else {
                this.f1178f.startService(intent);
            }
            this.f1171H = true;
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1669a(Message message) {
        if (message != null && message.obj != null) {
            BDNotifyListener bDNotifyListener = (BDNotifyListener) message.obj;
            if (this.f1193u == null) {
                this.f1193u = new C0786a(this.f1178f, this);
            }
            this.f1193u.mo10547a(bDNotifyListener);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1670a(Message message, int i) {
        if (this.f1177e) {
            try {
                Bundle data = message.getData();
                data.setClassLoader(BDLocation.class.getClassLoader());
                this.f1184l = (BDLocation) data.getParcelable("locStr");
                if (this.f1184l.getLocType() == 61) {
                    this.f1191s = System.currentTimeMillis();
                }
                m1680b(i);
            } catch (Exception unused) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1671a(BDLocation bDLocation) {
        if (!this.f1198z) {
            this.f1184l = bDLocation;
            if (!this.f1170G && bDLocation.getLocType() == 161) {
                this.f1169F = true;
            }
            ArrayList<BDLocationListener> arrayList = this.f1182j;
            if (arrayList != null) {
                Iterator<BDLocationListener> it = arrayList.iterator();
                while (it.hasNext()) {
                    it.next().onReceiveLocation(bDLocation);
                }
            }
            ArrayList<BDAbstractLocationListener> arrayList2 = this.f1183k;
            if (arrayList2 != null) {
                Iterator<BDAbstractLocationListener> it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    it2.next().onReceiveLocation(bDLocation);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1676a(boolean z) {
        try {
            Intent intent = new Intent(this.f1178f, C0839f.class);
            intent.putExtra("removenotify", z);
            intent.putExtra("command", 2);
            this.f1178f.startService(intent);
            this.f1171H = true;
        } catch (Exception unused) {
        }
    }

    /* renamed from: a */
    private boolean m1677a(int i) {
        if (this.f1179g != null && this.f1177e) {
            try {
                this.f1179g.send(Message.obtain((Handler) null, i));
                return true;
            } catch (Exception unused) {
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m1679b() {
        if (!this.f1177e) {
            if (this.f1166C.booleanValue()) {
                try {
                    new C0785c(this).start();
                } catch (Throwable unused) {
                }
                this.f1166C = false;
            }
            this.f1174b = this.f1178f.getPackageName();
            this.f1195w = this.f1174b + "_bdls_v2.9";
            Intent intent = new Intent(this.f1178f, C0839f.class);
            try {
                intent.putExtra("debug_dev", this.f1167D);
            } catch (Exception unused2) {
            }
            if (this.f1175c == null) {
                this.f1175c = new LocationClientOption();
            }
            intent.putExtra("cache_exception", this.f1175c.isIgnoreCacheException);
            intent.putExtra("kill_process", this.f1175c.isIgnoreKillProcess);
            try {
                this.f1178f.bindService(intent, this.f1172I, 1);
            } catch (Exception e) {
                e.printStackTrace();
                this.f1177e = false;
            }
        }
    }

    /* renamed from: b */
    private void m1680b(int i) {
        if (this.f1184l.getCoorType() == null) {
            this.f1184l.setCoorType(this.f1175c.coorType);
        }
        if (this.f1185m || ((this.f1175c.location_change_notify && this.f1184l.getLocType() == 61) || this.f1184l.getLocType() == 66 || this.f1184l.getLocType() == 67 || this.f1197y || this.f1184l.getLocType() == 161)) {
            ArrayList<BDLocationListener> arrayList = this.f1182j;
            if (arrayList != null) {
                Iterator<BDLocationListener> it = arrayList.iterator();
                while (it.hasNext()) {
                    it.next().onReceiveLocation(this.f1184l);
                }
            }
            ArrayList<BDAbstractLocationListener> arrayList2 = this.f1183k;
            if (arrayList2 != null) {
                Iterator<BDAbstractLocationListener> it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    it2.next().onReceiveLocation(this.f1184l);
                }
            }
            if (this.f1184l.getLocType() != 66 && this.f1184l.getLocType() != 67) {
                this.f1185m = false;
                this.f1192t = System.currentTimeMillis();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m1681b(Message message) {
        if (message != null && message.obj != null) {
            BDNotifyListener bDNotifyListener = (BDNotifyListener) message.obj;
            C0786a aVar = this.f1193u;
            if (aVar != null) {
                aVar.mo10550c(bDNotifyListener);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:13|14|15|16) */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0038 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x004e */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0053  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m1686c() {
        /*
            r5 = this;
            boolean r0 = r5.f1177e
            if (r0 == 0) goto L_0x0065
            android.os.Messenger r0 = r5.f1179g
            if (r0 != 0) goto L_0x0009
            goto L_0x0065
        L_0x0009:
            r0 = 12
            r1 = 0
            android.os.Message r0 = android.os.Message.obtain(r1, r0)
            android.os.Messenger r2 = r5.f1181i
            r0.replyTo = r2
            android.os.Messenger r2 = r5.f1179g     // Catch:{ Exception -> 0x001a }
            r2.send(r0)     // Catch:{ Exception -> 0x001a }
            goto L_0x001e
        L_0x001a:
            r0 = move-exception
            r0.printStackTrace()
        L_0x001e:
            r0 = 0
            android.content.Context r2 = r5.f1178f     // Catch:{ Exception -> 0x003a }
            android.content.ServiceConnection r3 = r5.f1172I     // Catch:{ Exception -> 0x003a }
            r2.unbindService(r3)     // Catch:{ Exception -> 0x003a }
            boolean r2 = r5.f1171H     // Catch:{ Exception -> 0x003a }
            if (r2 == 0) goto L_0x003a
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Exception -> 0x0038 }
            android.content.Context r3 = r5.f1178f     // Catch:{ Exception -> 0x0038 }
            java.lang.Class<com.baidu.location.f> r4 = com.baidu.location.C0839f.class
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0038 }
            android.content.Context r3 = r5.f1178f     // Catch:{ Exception -> 0x0038 }
            r3.stopService(r2)     // Catch:{ Exception -> 0x0038 }
        L_0x0038:
            r5.f1171H = r0     // Catch:{ Exception -> 0x003a }
        L_0x003a:
            java.lang.Object r2 = r5.f1190r
            monitor-enter(r2)
            boolean r3 = r5.f1187o     // Catch:{ Exception -> 0x004e }
            r4 = 1
            if (r3 != r4) goto L_0x004e
            com.baidu.location.LocationClient$a r3 = r5.f1180h     // Catch:{ Exception -> 0x004e }
            com.baidu.location.LocationClient$b r4 = r5.f1188p     // Catch:{ Exception -> 0x004e }
            r3.removeCallbacks(r4)     // Catch:{ Exception -> 0x004e }
            r5.f1187o = r0     // Catch:{ Exception -> 0x004e }
            goto L_0x004e
        L_0x004c:
            r0 = move-exception
            goto L_0x0063
        L_0x004e:
            monitor-exit(r2)     // Catch:{ all -> 0x004c }
            com.baidu.location.c.a r2 = r5.f1193u
            if (r2 == 0) goto L_0x0056
            r2.mo10548a()
        L_0x0056:
            r5.f1179g = r1
            r5.f1186n = r0
            r5.f1197y = r0
            r5.f1177e = r0
            r5.f1169F = r0
            r5.f1170G = r0
            return
        L_0x0063:
            monitor-exit(r2)     // Catch:{ all -> 0x004c }
            throw r0
        L_0x0065:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.LocationClient.m1686c():void");
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m1687c(Message message) {
        this.f1186n = false;
        if (message != null && message.obj != null) {
            LocationClientOption locationClientOption = (LocationClientOption) message.obj;
            if (!this.f1175c.optionEquals(locationClientOption)) {
                if (this.f1175c.scanSpan != locationClientOption.scanSpan) {
                    try {
                        synchronized (this.f1190r) {
                            if (this.f1187o) {
                                this.f1180h.removeCallbacks(this.f1188p);
                                this.f1187o = false;
                            }
                            if (locationClientOption.scanSpan >= 1000 && !this.f1187o) {
                                if (this.f1188p == null) {
                                    this.f1188p = new C0720b(this, null);
                                }
                                this.f1180h.postDelayed(this.f1188p, (long) locationClientOption.scanSpan);
                                this.f1187o = true;
                            }
                        }
                    } catch (Exception unused) {
                    }
                }
                this.f1175c = new LocationClientOption(locationClientOption);
                if (this.f1179g != null) {
                    try {
                        Message obtain = Message.obtain((Handler) null, 15);
                        obtain.replyTo = this.f1181i;
                        obtain.setData(m1690d());
                        this.f1179g.send(obtain);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public Bundle m1690d() {
        if (this.f1175c == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("packName", this.f1174b);
        bundle.putString("prodName", this.f1175c.prodName);
        bundle.putString("coorType", this.f1175c.coorType);
        bundle.putString("addrType", this.f1175c.addrType);
        bundle.putBoolean("openGPS", this.f1175c.openGps);
        bundle.putBoolean("location_change_notify", this.f1175c.location_change_notify);
        bundle.putInt("scanSpan", this.f1175c.scanSpan);
        bundle.putBoolean("enableSimulateGps", this.f1175c.enableSimulateGps);
        bundle.putInt("timeOut", this.f1175c.timeOut);
        bundle.putInt("priority", this.f1175c.priority);
        bundle.putBoolean("map", this.f1164A.booleanValue());
        bundle.putBoolean("import", this.f1165B.booleanValue());
        bundle.putBoolean("needDirect", this.f1175c.mIsNeedDeviceDirect);
        bundle.putBoolean("isneedaptag", this.f1175c.isNeedAptag);
        bundle.putBoolean("isneedpoiregion", this.f1175c.isNeedPoiRegion);
        bundle.putBoolean("isneedregular", this.f1175c.isNeedRegular);
        bundle.putBoolean("isneedaptagd", this.f1175c.isNeedAptagd);
        bundle.putBoolean("isneedaltitude", this.f1175c.isNeedAltitude);
        bundle.putBoolean("isneednewrgc", this.f1175c.isNeedNewVersionRgc);
        bundle.putInt("autoNotifyMaxInterval", this.f1175c.mo10374a());
        bundle.putInt("autoNotifyMinTimeInterval", this.f1175c.getAutoNotifyMinTimeInterval());
        bundle.putInt("autoNotifyMinDistance", this.f1175c.getAutoNotifyMinDistance());
        bundle.putFloat("autoNotifyLocSensitivity", this.f1175c.mo10375b());
        bundle.putInt("wifitimeout", this.f1175c.wifiCacheTimeOut);
        return bundle;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m1692d(Message message) {
        if (message != null && message.obj != null) {
            this.f1194v = (BDLocationListener) message.obj;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m1696e() {
        if (this.f1179g != null) {
            Message obtain = Message.obtain((Handler) null, 22);
            try {
                obtain.replyTo = this.f1181i;
                this.f1179g.send(obtain);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m1697e(Message message) {
        if (message != null && message.obj != null) {
            BDLocationListener bDLocationListener = (BDLocationListener) message.obj;
            if (this.f1182j == null) {
                this.f1182j = new ArrayList<>();
            }
            if (!this.f1182j.contains(bDLocationListener)) {
                this.f1182j.add(bDLocationListener);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m1701f() {
        LocationClientOption locationClientOption;
        if (this.f1179g != null) {
            if ((System.currentTimeMillis() - this.f1191s > 3000 || (((locationClientOption = this.f1175c) != null && !locationClientOption.location_change_notify) || this.f1186n)) && (!this.f1197y || System.currentTimeMillis() - this.f1192t > 20000 || this.f1186n)) {
                Message obtain = Message.obtain((Handler) null, 22);
                if (this.f1186n) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isWaitingLocTag", this.f1186n);
                    this.f1186n = false;
                    obtain.setData(bundle);
                }
                try {
                    obtain.replyTo = this.f1181i;
                    this.f1179g.send(obtain);
                    this.f1173a = System.currentTimeMillis();
                    this.f1185m = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            synchronized (this.f1190r) {
                if (this.f1175c != null && this.f1175c.scanSpan >= 1000 && !this.f1187o) {
                    if (this.f1188p == null) {
                        this.f1188p = new C0720b(this, null);
                    }
                    this.f1180h.postDelayed(this.f1188p, (long) this.f1175c.scanSpan);
                    this.f1187o = true;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m1702f(Message message) {
        if (message != null && message.obj != null) {
            BDAbstractLocationListener bDAbstractLocationListener = (BDAbstractLocationListener) message.obj;
            if (this.f1183k == null) {
                this.f1183k = new ArrayList<>();
            }
            if (!this.f1183k.contains(bDAbstractLocationListener)) {
                this.f1183k.add(bDAbstractLocationListener);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m1705g(Message message) {
        if (message != null && message.obj != null) {
            BDAbstractLocationListener bDAbstractLocationListener = (BDAbstractLocationListener) message.obj;
            ArrayList<BDAbstractLocationListener> arrayList = this.f1183k;
            if (arrayList != null && arrayList.contains(bDAbstractLocationListener)) {
                this.f1183k.remove(bDAbstractLocationListener);
            }
        }
    }

    public static BDLocation getBDLocationInCoorType(BDLocation bDLocation, String str) {
        BDLocation bDLocation2 = new BDLocation(bDLocation);
        double[] coorEncrypt = Jni.coorEncrypt(bDLocation.getLongitude(), bDLocation.getLatitude(), str);
        bDLocation2.setLatitude(coorEncrypt[1]);
        bDLocation2.setLongitude(coorEncrypt[0]);
        return bDLocation2;
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public void m1708h(Message message) {
        if (message != null && message.obj != null) {
            BDLocationListener bDLocationListener = (BDLocationListener) message.obj;
            ArrayList<BDLocationListener> arrayList = this.f1182j;
            if (arrayList != null && arrayList.contains(bDLocationListener)) {
                this.f1182j.remove(bDLocationListener);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m1711i(Message message) {
        try {
            Bundle data = message.getData();
            data.setClassLoader(BDLocation.class.getClassLoader());
            BDLocation bDLocation = (BDLocation) data.getParcelable("locStr");
            if (this.f1194v == null) {
                return;
            }
            if (this.f1175c == null || !this.f1175c.isDisableCache() || bDLocation.getLocType() != 65) {
                this.f1194v.onReceiveLocation(bDLocation);
            }
        } catch (Exception unused) {
        }
    }

    public void disableAssistantLocation() {
        C0742k.m1853a().mo10469b();
    }

    public void disableLocInForeground(boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("removenotify", z);
        Message obtainMessage = this.f1180h.obtainMessage(704);
        obtainMessage.setData(bundle);
        obtainMessage.sendToTarget();
    }

    public void enableAssistantLocation(WebView webView) {
        C0742k.m1853a().mo10468a(this.f1178f, webView, this);
    }

    public void enableLocInForeground(int i, Notification notification) {
        if (i <= 0 || notification == null) {
            Log.e("baidu_location_Client", "can not startLocInForeground if the param is unlegal");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(Config.FEED_LIST_ITEM_CUSTOM_ID, i);
        bundle.putParcelable("notification", notification);
        Message obtainMessage = this.f1180h.obtainMessage(703);
        obtainMessage.setData(bundle);
        obtainMessage.sendToTarget();
    }

    public String getAccessKey() {
        try {
            this.f1196x = C0741j.m1845b(this.f1178f);
            if (!TextUtils.isEmpty(this.f1196x)) {
                return String.format("KEY=%s", this.f1196x);
            }
            throw new IllegalStateException("please setting key from Manifest.xml");
        } catch (Exception unused) {
            return null;
        }
    }

    public BDLocation getLastKnownLocation() {
        return this.f1184l;
    }

    public LocationClientOption getLocOption() {
        return this.f1175c;
    }

    public String getVersion() {
        return "7.8.2";
    }

    public boolean isStarted() {
        return this.f1177e;
    }

    public void onReceiveLocation(BDLocation bDLocation) {
        if ((!this.f1170G || this.f1169F) && bDLocation != null) {
            Message obtainMessage = this.f1180h.obtainMessage(701);
            obtainMessage.obj = bDLocation;
            obtainMessage.sendToTarget();
        }
    }

    public void registerLocationListener(BDAbstractLocationListener bDAbstractLocationListener) {
        if (bDAbstractLocationListener != null) {
            Message obtainMessage = this.f1180h.obtainMessage(1300);
            obtainMessage.obj = bDAbstractLocationListener;
            obtainMessage.sendToTarget();
            return;
        }
        throw new IllegalStateException("please set a non-null listener");
    }

    public void registerLocationListener(BDLocationListener bDLocationListener) {
        if (bDLocationListener != null) {
            Message obtainMessage = this.f1180h.obtainMessage(5);
            obtainMessage.obj = bDLocationListener;
            obtainMessage.sendToTarget();
            return;
        }
        throw new IllegalStateException("please set a non-null listener");
    }

    public void registerNotify(BDNotifyListener bDNotifyListener) {
        Message obtainMessage = this.f1180h.obtainMessage(9);
        obtainMessage.obj = bDNotifyListener;
        obtainMessage.sendToTarget();
    }

    public void registerNotifyLocationListener(BDLocationListener bDLocationListener) {
        Message obtainMessage = this.f1180h.obtainMessage(8);
        obtainMessage.obj = bDLocationListener;
        obtainMessage.sendToTarget();
    }

    public void removeNotifyEvent(BDNotifyListener bDNotifyListener) {
        Message obtainMessage = this.f1180h.obtainMessage(10);
        obtainMessage.obj = bDNotifyListener;
        obtainMessage.sendToTarget();
    }

    public boolean requestHotSpotState() {
        if (this.f1179g != null && this.f1177e) {
            try {
                this.f1179g.send(Message.obtain((Handler) null, 406));
                return true;
            } catch (Exception unused) {
            }
        }
        return false;
    }

    public int requestLocation() {
        ArrayList<BDAbstractLocationListener> arrayList;
        if (this.f1179g == null || this.f1181i == null) {
            return 1;
        }
        ArrayList<BDLocationListener> arrayList2 = this.f1182j;
        if ((arrayList2 == null || arrayList2.size() < 1) && ((arrayList = this.f1183k) == null || arrayList.size() < 1)) {
            return 2;
        }
        if (System.currentTimeMillis() - this.f1173a < 1000) {
            return 6;
        }
        this.f1186n = true;
        Message obtainMessage = this.f1180h.obtainMessage(4);
        obtainMessage.arg1 = 0;
        obtainMessage.sendToTarget();
        return 0;
    }

    public void requestNotifyLocation() {
        this.f1180h.obtainMessage(11).sendToTarget();
    }

    public int requestOfflineLocation() {
        ArrayList<BDAbstractLocationListener> arrayList;
        if (this.f1179g == null || this.f1181i == null) {
            return 1;
        }
        ArrayList<BDLocationListener> arrayList2 = this.f1182j;
        if ((arrayList2 == null || arrayList2.size() < 1) && ((arrayList = this.f1183k) == null || arrayList.size() < 1)) {
            return 2;
        }
        this.f1180h.obtainMessage(12).sendToTarget();
        return 0;
    }

    public void restart() {
        stop();
        this.f1198z = false;
        this.f1180h.sendEmptyMessageDelayed(1, 1000);
    }

    public void setLocOption(LocationClientOption locationClientOption) {
        if (locationClientOption == null) {
            locationClientOption = new LocationClientOption();
        }
        if (locationClientOption.mo10374a() > 0) {
            locationClientOption.setScanSpan(0);
            locationClientOption.setLocationNotify(true);
        }
        this.f1176d = new LocationClientOption(locationClientOption);
        Message obtainMessage = this.f1180h.obtainMessage(3);
        obtainMessage.obj = locationClientOption;
        obtainMessage.sendToTarget();
    }

    public void start() {
        this.f1198z = false;
        if (!C0855k.m2460b()) {
            this.f1180h.obtainMessage(1).sendToTarget();
        }
    }

    public boolean startIndoorMode() {
        boolean a = m1677a(110);
        if (a) {
            this.f1197y = true;
        }
        return a;
    }

    public void stop() {
        this.f1198z = true;
        this.f1180h.obtainMessage(2).sendToTarget();
        this.f1168E = null;
    }

    public boolean stopIndoorMode() {
        boolean a = m1677a(111);
        if (a) {
            this.f1197y = false;
        }
        return a;
    }

    public void unRegisterLocationListener(BDAbstractLocationListener bDAbstractLocationListener) {
        if (bDAbstractLocationListener != null) {
            Message obtainMessage = this.f1180h.obtainMessage(1400);
            obtainMessage.obj = bDAbstractLocationListener;
            obtainMessage.sendToTarget();
            return;
        }
        throw new IllegalStateException("please set a non-null listener");
    }

    public void unRegisterLocationListener(BDLocationListener bDLocationListener) {
        if (bDLocationListener != null) {
            Message obtainMessage = this.f1180h.obtainMessage(6);
            obtainMessage.obj = bDLocationListener;
            obtainMessage.sendToTarget();
            return;
        }
        throw new IllegalStateException("please set a non-null listener");
    }

    public boolean updateLocation(Location location) {
        if (this.f1179g == null || this.f1181i == null || location == null) {
            return false;
        }
        try {
            Message obtain = Message.obtain((Handler) null, 57);
            obtain.obj = location;
            this.f1179g.send(obtain);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }
}
