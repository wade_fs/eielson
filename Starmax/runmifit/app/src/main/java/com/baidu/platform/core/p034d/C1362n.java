package com.baidu.platform.core.p034d;

import com.baidu.mapapi.search.route.TransitRoutePlanOption;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;
import com.google.android.gms.dynamite.ProviderConstants;
import com.tamic.novate.download.MimeType;

/* renamed from: com.baidu.platform.core.d.n */
public class C1362n extends C1320e {
    public C1362n(TransitRoutePlanOption transitRoutePlanOption) {
        m5094a(transitRoutePlanOption);
    }

    /* renamed from: a */
    private void m5094a(TransitRoutePlanOption transitRoutePlanOption) {
        this.f4435a.mo14094a("qt", "bus");
        C1381a aVar = this.f4435a;
        aVar.mo14094a("sy", transitRoutePlanOption.mPolicy.getInt() + "");
        this.f4435a.mo14094a("ie", "utf-8");
        this.f4435a.mo14094a("lrn", "20");
        this.f4435a.mo14094a(ProviderConstants.API_COLNAME_FEATURE_VERSION, "3");
        this.f4435a.mo14094a("rp_format", MimeType.JSON);
        this.f4435a.mo14094a("rp_filter", "mobile");
        this.f4435a.mo14094a("ic_info", "2");
        this.f4435a.mo14094a("exptype", "depall");
        this.f4435a.mo14094a("sn", mo14024a(transitRoutePlanOption.mFrom));
        this.f4435a.mo14094a("en", mo14024a(transitRoutePlanOption.mTo));
        if (transitRoutePlanOption.mCityName != null) {
            this.f4435a.mo14094a("c", transitRoutePlanOption.mCityName);
        }
        if (TransitRoutePlanOption.TransitPolicy.EBUS_NO_SUBWAY == transitRoutePlanOption.mPolicy) {
            this.f4435a.mo14094a("f", "[0,2,4,7,5,8,9,10,11]");
        }
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14083h();
    }
}
