package com.baidu.platform.core.p032b;

import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.tamic.novate.download.MimeType;

/* renamed from: com.baidu.platform.core.b.c */
public class C1333c extends C1320e {
    public C1333c(GeoCodeOption geoCodeOption) {
        m4955a(geoCodeOption);
    }

    /* renamed from: a */
    private void m4955a(GeoCodeOption geoCodeOption) {
        this.f4435a.mo14094a("city", geoCodeOption.mCity);
        this.f4435a.mo14094a("address", geoCodeOption.mAddress);
        this.f4435a.mo14094a("output", MimeType.JSON);
        this.f4435a.mo14094a("ret_coordtype", "bd09ll");
        this.f4435a.mo14094a("from", "android_map_sdk");
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14081f();
    }
}
