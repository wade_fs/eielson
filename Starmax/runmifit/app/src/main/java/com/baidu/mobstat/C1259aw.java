package com.baidu.mobstat;

import android.app.Activity;
import android.graphics.Rect;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebView;
import android.widget.GridView;
import android.widget.ListView;
import com.baidu.mobstat.C1245an;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.aw */
public class C1259aw {
    /* renamed from: a */
    public static View m4639a(Activity activity) {
        Window window;
        if (activity == null || (window = activity.getWindow()) == null) {
            return null;
        }
        return window.getDecorView();
    }

    /* renamed from: a */
    public static String m4640a(View view) {
        String str;
        if (view instanceof ListView) {
            str = ListView.class.getSimpleName();
        } else {
            str = view instanceof WebView ? WebView.class.getSimpleName() : "";
        }
        if (TextUtils.isEmpty(str)) {
            String a = m4643a(view.getClass());
            if (!"android.widget".equals(a) && !"android.view".equals(a)) {
                Class<?> cls = null;
                try {
                    cls = Class.forName("android.support.v7.widget.RecyclerView");
                } catch (Exception unused) {
                }
                if (cls != null && cls.isAssignableFrom(view.getClass())) {
                    str = "RecyclerView";
                }
            }
        }
        if (TextUtils.isEmpty(str)) {
            str = m4654c(view.getClass());
        }
        return TextUtils.isEmpty(str) ? "Object" : str;
    }

    /* renamed from: c */
    private static String m4654c(Class<?> cls) {
        if (cls == null) {
            return "";
        }
        String a = m4643a(cls);
        if ("android.widget".equals(a) || "android.view".equals(a)) {
            return m4657d(cls);
        }
        return m4654c(cls.getSuperclass());
    }

    /* renamed from: a */
    public static String m4643a(Class<?> cls) {
        String str;
        if (cls == null) {
            return "";
        }
        Package packageR = cls.getPackage();
        if (packageR != null) {
            str = packageR.getName();
        } else {
            str = "";
        }
        if (str == null) {
            return "";
        }
        return str;
    }

    /* renamed from: a */
    public static String m4641a(View view, View view2) {
        if (view == null) {
            return String.valueOf(0);
        }
        if (view == view2) {
            return String.valueOf(0);
        }
        ViewParent parent = view.getParent();
        if (parent == null || !(parent instanceof ViewGroup)) {
            return String.valueOf(0);
        }
        Class<?> cls = view.getClass();
        if (cls == null) {
            return String.valueOf(0);
        }
        String b = m4650b(cls);
        if (TextUtils.isEmpty(b)) {
            return String.valueOf(0);
        }
        ViewGroup viewGroup = (ViewGroup) parent;
        int i = 0;
        for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if (childAt != null) {
                if (childAt == view) {
                    break;
                } else if (childAt.getClass() != null && b.equals(m4650b(childAt.getClass()))) {
                    i++;
                }
            }
        }
        return String.valueOf(i);
    }

    /* renamed from: a */
    public static String m4642a(View view, String str) {
        ViewParent parent;
        if (TextUtils.isEmpty(str) || view == null || (parent = view.getParent()) == null || !(parent instanceof View)) {
            return "";
        }
        View view2 = (View) parent;
        if (ListView.class.getSimpleName().equals(str)) {
            try {
                if (!(view2 instanceof ListView) || view.getParent() == null) {
                    return "";
                }
                return String.valueOf(((ListView) view2).getPositionForView(view));
            } catch (Throwable unused) {
                return "";
            }
        } else if (GridView.class.getSimpleName().equals(str)) {
            if (!(view2 instanceof GridView) || view.getParent() == null) {
                return "";
            }
            return String.valueOf(((GridView) view2).getPositionForView(view));
        } else if ("RecyclerView".equals(str)) {
            return String.valueOf(((RecyclerView) view2).getChildLayoutPosition(view));
        } else {
            return "";
        }
    }

    /* renamed from: b */
    public static String m4649b(View view) {
        ViewParent parent;
        String str;
        if (view == null || (parent = view.getParent()) == null || !(parent instanceof ViewGroup)) {
            return "";
        }
        String a = m4643a(parent.getClass());
        if ("android.widget".equals(a) || "android.view".equals(a)) {
            return "";
        }
        ViewGroup viewGroup = (ViewGroup) parent;
        Class<?> cls = null;
        try {
            cls = Class.forName("android.support.v4.view.ViewPager");
        } catch (ClassNotFoundException unused) {
        }
        if (cls == null || !cls.isAssignableFrom(viewGroup.getClass())) {
            return "";
        }
        try {
            ViewPager viewPager = (ViewPager) viewGroup;
            ArrayList arrayList = new ArrayList();
            int childCount = viewPager.getChildCount();
            int i = 0;
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewPager.getChildAt(i2);
                arrayList.add(childAt);
                if (m4653c(childAt) != null) {
                    i++;
                }
            }
            if (arrayList.size() < 2 || i < 2) {
                str = String.valueOf(viewPager.getCurrentItem());
            } else {
                try {
                    Collections.sort(arrayList, new Comparator<View>() {
                        /* class com.baidu.mobstat.C1259aw.C12601 */

                        /* renamed from: a */
                        public int compare(View view, View view2) {
                            return view.getLeft() - view2.getLeft();
                        }
                    });
                } catch (Exception unused2) {
                }
                int left = view.getLeft() / Math.abs(((View) arrayList.get(1)).getLeft() - ((View) arrayList.get(0)).getLeft());
                int count = viewPager.getAdapter().getCount();
                if (count != 0) {
                    left %= count;
                }
                str = String.valueOf(left);
            }
            return str;
        } catch (Throwable unused3) {
            return "";
        }
    }

    /* renamed from: c */
    public static Rect m4653c(View view) {
        if (view.getVisibility() != 0) {
            return null;
        }
        Rect rect = new Rect();
        if (m4648a(view, rect) && rect.right > rect.left && rect.bottom > rect.top) {
            return rect;
        }
        return null;
    }

    /* renamed from: a */
    private static boolean m4648a(View view, Rect rect) {
        if (view == null || rect == null) {
            return false;
        }
        try {
            return view.getGlobalVisibleRect(rect);
        } catch (Exception unused) {
            return false;
        }
    }

    /* renamed from: d */
    public static String m4656d(View view) {
        int lastIndexOf;
        int i;
        String str = null;
        try {
            if (view.getId() != 0) {
                str = view.getResources().getResourceName(view.getId());
            }
        } catch (Exception unused) {
        }
        if (!TextUtils.isEmpty(str) && str.contains(":id/") && (lastIndexOf = str.lastIndexOf(":id/")) != -1 && (i = lastIndexOf + 4) < str.length()) {
            str = str.substring(i);
        }
        return str == null ? "" : str;
    }

    /* renamed from: a */
    public static JSONArray m4647a(Activity activity, View view) {
        JSONArray jSONArray = new JSONArray();
        if (activity == null || view == null) {
            return jSONArray;
        }
        View view2 = null;
        try {
            view2 = m4639a(activity);
        } catch (Exception unused) {
        }
        if (view2 == null) {
            return jSONArray;
        }
        while (true) {
            if (view == null) {
                break;
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("p", m4660f(view));
                String b = m4649b(view);
                if (TextUtils.isEmpty(b)) {
                    String str = "";
                    ViewParent parent = view.getParent();
                    if (parent != null && (parent instanceof View)) {
                        str = m4640a((View) parent);
                    }
                    b = m4642a(view, str);
                    if (TextUtils.isEmpty(b)) {
                        b = m4641a(view, view2);
                    }
                }
                jSONObject.put("i", b);
                jSONObject.put("t", m4640a(view));
                jSONArray.put(jSONObject);
                ViewParent parent2 = view.getParent();
                if (parent2 == null) {
                    break;
                } else if (view == view2) {
                    break;
                } else if (!(parent2 instanceof View)) {
                    break;
                } else if (m4661g(view)) {
                    break;
                } else if (jSONArray.length() > 1000) {
                    break;
                } else {
                    view = (View) parent2;
                }
            } catch (Exception unused2) {
                jSONArray = new JSONArray();
            }
        }
        JSONArray jSONArray2 = new JSONArray();
        try {
            for (int length = jSONArray.length() - 1; length >= 0; length--) {
                jSONArray2.put(jSONArray.get(length));
            }
        } catch (Exception unused3) {
        }
        return jSONArray2;
    }

    /* renamed from: e */
    public static Map<String, String> m4659e(View view) {
        Map<String, String> map;
        Object tag = view.getTag(-96000);
        if (tag != null && (tag instanceof Map)) {
            try {
                map = (Map) tag;
            } catch (Exception unused) {
                map = null;
            }
            if (map == null || map.size() == 0) {
                return null;
            }
            return map;
        }
        return null;
    }

    /* renamed from: a */
    public static String m4646a(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                String string = jSONObject.getString("p");
                String string2 = jSONObject.getString("i");
                sb.append("/" + string + "[" + string2 + "]");
                i++;
            } catch (Exception unused) {
                return "";
            }
        }
        return sb.toString();
    }

    /* renamed from: b */
    public static String m4652b(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                String string = jSONObject.getString("p");
                String string2 = jSONObject.getString("i");
                sb.append("/" + string + "[" + string2 + "]");
                String optString = jSONObject.optString("d");
                if (!TextUtils.isEmpty(optString)) {
                    sb.append("#" + optString);
                }
                i++;
            } catch (Exception unused) {
                return "";
            }
        }
        return sb.toString();
    }

    /* renamed from: c */
    public static String m4655c(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                String b = m4651b(jSONObject.getString("p"));
                String string = jSONObject.getString("i");
                sb.append("/" + b + "[" + string + "]");
                i++;
            } catch (Exception unused) {
                return "";
            }
        }
        return sb.toString();
    }

    /* renamed from: d */
    public static String m4658d(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                String b = m4651b(jSONObject.getString("p"));
                String string = jSONObject.getString("i");
                sb.append("/" + b + "[" + string + "]");
                String optString = jSONObject.optString("d");
                if (!TextUtils.isEmpty(optString)) {
                    sb.append("#" + optString);
                }
                i++;
            } catch (Exception unused) {
                return "";
            }
        }
        return sb.toString();
    }

    /* renamed from: b */
    private static String m4651b(String str) {
        String a = C1253aq.m4611a().mo13939a(str);
        if (TextUtils.isEmpty(a)) {
            a = C1245an.m4584a().mo13926a(str, C1245an.C1247a.f4293a);
        }
        return a == null ? "" : a;
    }

    /* renamed from: a */
    public static String m4645a(String str) {
        String a = C1245an.m4584a().mo13926a(str, C1245an.C1247a.f4294b);
        return a == null ? "" : a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.aw.a(java.lang.Class<?>, boolean):java.lang.String
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.baidu.mobstat.aw.a(android.view.View, android.view.View):java.lang.String
      com.baidu.mobstat.aw.a(android.view.View, java.lang.String):java.lang.String
      com.baidu.mobstat.aw.a(android.app.Activity, android.view.View):org.json.JSONArray
      com.baidu.mobstat.aw.a(android.view.View, android.graphics.Rect):boolean
      com.baidu.mobstat.aw.a(java.lang.Class<?>, boolean):java.lang.String */
    /* renamed from: b */
    public static String m4650b(Class<?> cls) {
        String str;
        if (cls == null) {
            return "";
        }
        String a = m4644a(cls, false);
        if (TextUtils.isEmpty(a) || !cls.isAnonymousClass()) {
            str = a;
        } else {
            str = a + "$";
        }
        if (str == null) {
            return "";
        }
        return str;
    }

    /* renamed from: f */
    public static String m4660f(View view) {
        Class<?> cls;
        String str;
        if (view == null || (cls = view.getClass()) == null) {
            return "";
        }
        String d = m4657d(cls);
        if (TextUtils.isEmpty(d) || !cls.isAnonymousClass()) {
            str = d;
        } else {
            str = d + "$";
        }
        if (str == null) {
            return "";
        }
        return str;
    }

    /* renamed from: a */
    private static String m4644a(Class<?> cls, boolean z) {
        if (!cls.isAnonymousClass()) {
            return z ? cls.getSimpleName() : cls.getName();
        }
        Class<? super Object> superclass = cls.getSuperclass();
        if (superclass != null) {
            return z ? superclass.getSimpleName() : superclass.getName();
        }
        return "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.aw.a(java.lang.Class<?>, boolean):java.lang.String
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.baidu.mobstat.aw.a(android.view.View, android.view.View):java.lang.String
      com.baidu.mobstat.aw.a(android.view.View, java.lang.String):java.lang.String
      com.baidu.mobstat.aw.a(android.app.Activity, android.view.View):org.json.JSONArray
      com.baidu.mobstat.aw.a(android.view.View, android.graphics.Rect):boolean
      com.baidu.mobstat.aw.a(java.lang.Class<?>, boolean):java.lang.String */
    /* renamed from: d */
    private static String m4657d(Class<?> cls) {
        return m4644a(cls, true);
    }

    /* renamed from: g */
    private static boolean m4661g(View view) {
        if (view != null && "com.android.internal.policy".equals(m4643a(view.getClass())) && "DecorView".equals(m4660f(view))) {
            return true;
        }
        return false;
    }
}
