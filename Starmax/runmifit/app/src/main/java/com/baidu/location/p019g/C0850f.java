package com.baidu.location.p019g;

/* renamed from: com.baidu.location.g.f */
class C0850f implements Runnable {

    /* renamed from: a */
    final /* synthetic */ C0849e f1890a;

    C0850f(C0849e eVar) {
        this.f1890a = eVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00e3 A[SYNTHETIC, Splitter:B:61:0x00e3] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00ed A[SYNTHETIC, Splitter:B:66:0x00ed] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00f9 A[LOOP:0: B:1:0x0018->B:72:0x00f9, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0107 A[SYNTHETIC, Splitter:B:78:0x0107] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0111 A[SYNTHETIC, Splitter:B:83:0x0111] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x011a A[EDGE_INSN: B:92:0x011a->B:88:0x011a ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            com.baidu.location.g.e r0 = r12.f1890a
            java.lang.String r1 = com.baidu.location.p019g.C0855k.m2466e()
            r0.f1882h = r1
            com.baidu.location.g.e r0 = r12.f1890a
            r0.mo10508b()
            com.baidu.location.g.e r0 = r12.f1890a
            r0.mo10444a()
            com.baidu.location.g.e r0 = r12.f1890a
            int r0 = r0.f1883i
            r1 = 0
            r2 = r1
        L_0x0018:
            r3 = 1
            r4 = 0
            if (r0 <= 0) goto L_0x011a
            java.net.URL r5 = new java.net.URL     // Catch:{ Exception -> 0x00d3, all -> 0x00cf }
            com.baidu.location.g.e r6 = r12.f1890a     // Catch:{ Exception -> 0x00d3, all -> 0x00cf }
            java.lang.String r6 = r6.f1882h     // Catch:{ Exception -> 0x00d3, all -> 0x00cf }
            r5.<init>(r6)     // Catch:{ Exception -> 0x00d3, all -> 0x00cf }
            java.net.URLConnection r5 = r5.openConnection()     // Catch:{ Exception -> 0x00d3, all -> 0x00cf }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ Exception -> 0x00d3, all -> 0x00cf }
            java.lang.String r2 = "GET"
            r5.setRequestMethod(r2)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r5.setDoInput(r3)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r5.setDoOutput(r3)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r5.setUseCaches(r4)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            int r2 = com.baidu.location.p019g.C0843a.f1827b     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r5.setConnectTimeout(r2)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            int r2 = com.baidu.location.p019g.C0843a.f1827b     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r5.setReadTimeout(r2)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r6 = "application/x-www-form-urlencoded; charset=utf-8"
            r5.setRequestProperty(r2, r6)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            java.lang.String r2 = "Accept-Charset"
            java.lang.String r6 = "UTF-8"
            r5.setRequestProperty(r2, r6)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            java.lang.String r2 = com.baidu.location.p019g.C0855k.f1955ax     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            if (r2 == 0) goto L_0x005c
            java.lang.String r2 = "bd-loc-android"
            java.lang.String r6 = com.baidu.location.p019g.C0855k.f1955ax     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r5.setRequestProperty(r2, r6)     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
        L_0x005c:
            int r2 = r5.getResponseCode()     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r6 = 200(0xc8, float:2.8E-43)
            if (r2 != r6) goto L_0x00a7
            java.io.InputStream r2 = r5.getInputStream()     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00a2, all -> 0x009d }
            r6.<init>()     // Catch:{ Exception -> 0x00a2, all -> 0x009d }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
        L_0x0071:
            int r8 = r2.read(r7)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r9 = -1
            if (r8 == r9) goto L_0x007c
            r6.write(r7, r4, r8)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            goto L_0x0071
        L_0x007c:
            r2.close()     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r6.close()     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            com.baidu.location.g.e r7 = r12.f1890a     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.lang.String r8 = new java.lang.String     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            byte[] r9 = r6.toByteArray()     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.lang.String r10 = "utf-8"
            r8.<init>(r9, r10)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r7.f1884j = r8     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            com.baidu.location.g.e r7 = r12.f1890a     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r7.mo10446a(r3)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r5.disconnect()     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r7 = 1
            goto L_0x00ad
        L_0x009b:
            r0 = move-exception
            goto L_0x009f
        L_0x009d:
            r0 = move-exception
            r6 = r1
        L_0x009f:
            r1 = r2
            goto L_0x0100
        L_0x00a2:
            r6 = r1
        L_0x00a3:
            r11 = r5
            r5 = r2
            r2 = r11
            goto L_0x00d5
        L_0x00a7:
            r5.disconnect()     // Catch:{ Exception -> 0x00cb, all -> 0x00c8 }
            r2 = r1
            r6 = r2
            r7 = 0
        L_0x00ad:
            if (r5 == 0) goto L_0x00b2
            r5.disconnect()
        L_0x00b2:
            if (r2 == 0) goto L_0x00bc
            r2.close()     // Catch:{ Exception -> 0x00b8 }
            goto L_0x00bc
        L_0x00b8:
            r2 = move-exception
            r2.printStackTrace()
        L_0x00bc:
            if (r6 == 0) goto L_0x00c6
            r6.close()     // Catch:{ Exception -> 0x00c2 }
            goto L_0x00c6
        L_0x00c2:
            r2 = move-exception
            r2.printStackTrace()
        L_0x00c6:
            r2 = r5
            goto L_0x00f6
        L_0x00c8:
            r0 = move-exception
            r6 = r1
            goto L_0x0100
        L_0x00cb:
            r6 = r1
            r2 = r5
            r5 = r6
            goto L_0x00d5
        L_0x00cf:
            r0 = move-exception
            r6 = r1
        L_0x00d1:
            r5 = r2
            goto L_0x0100
        L_0x00d3:
            r5 = r1
            r6 = r5
        L_0x00d5:
            java.lang.String r7 = com.baidu.location.p019g.C0843a.f1826a     // Catch:{ all -> 0x00fd }
            java.lang.String r8 = "NetworkCommunicationException!"
            android.util.Log.d(r7, r8)     // Catch:{ all -> 0x00fd }
            if (r2 == 0) goto L_0x00e1
            r2.disconnect()
        L_0x00e1:
            if (r5 == 0) goto L_0x00eb
            r5.close()     // Catch:{ Exception -> 0x00e7 }
            goto L_0x00eb
        L_0x00e7:
            r5 = move-exception
            r5.printStackTrace()
        L_0x00eb:
            if (r6 == 0) goto L_0x00f5
            r6.close()     // Catch:{ Exception -> 0x00f1 }
            goto L_0x00f5
        L_0x00f1:
            r5 = move-exception
            r5.printStackTrace()
        L_0x00f5:
            r7 = 0
        L_0x00f6:
            if (r7 == 0) goto L_0x00f9
            goto L_0x011a
        L_0x00f9:
            int r0 = r0 + -1
            goto L_0x0018
        L_0x00fd:
            r0 = move-exception
            r1 = r5
            goto L_0x00d1
        L_0x0100:
            if (r5 == 0) goto L_0x0105
            r5.disconnect()
        L_0x0105:
            if (r1 == 0) goto L_0x010f
            r1.close()     // Catch:{ Exception -> 0x010b }
            goto L_0x010f
        L_0x010b:
            r1 = move-exception
            r1.printStackTrace()
        L_0x010f:
            if (r6 == 0) goto L_0x0119
            r6.close()     // Catch:{ Exception -> 0x0115 }
            goto L_0x0119
        L_0x0115:
            r1 = move-exception
            r1.printStackTrace()
        L_0x0119:
            throw r0
        L_0x011a:
            if (r0 > 0) goto L_0x0129
            int r0 = com.baidu.location.p019g.C0849e.f1881p
            int r0 = r0 + r3
            com.baidu.location.p019g.C0849e.f1881p = r0
            com.baidu.location.g.e r0 = r12.f1890a
            r0.f1884j = r1
            r0.mo10446a(r4)
            goto L_0x012b
        L_0x0129:
            com.baidu.location.p019g.C0849e.f1881p = r4
        L_0x012b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p019g.C0850f.run():void");
    }
}
