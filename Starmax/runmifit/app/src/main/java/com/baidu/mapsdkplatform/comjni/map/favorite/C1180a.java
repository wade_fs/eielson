package com.baidu.mapsdkplatform.comjni.map.favorite;

import android.os.Bundle;

/* renamed from: com.baidu.mapsdkplatform.comjni.map.favorite.a */
public class C1180a {

    /* renamed from: a */
    private long f3948a;

    /* renamed from: b */
    private JNIFavorite f3949b;

    /* renamed from: com.baidu.mapsdkplatform.comjni.map.favorite.a$a */
    public static class C1181a {

        /* renamed from: a */
        public static boolean f3950a = false;

        /* access modifiers changed from: private */
        /* renamed from: b */
        public static void m4372b() {
            f3950a = true;
        }
    }

    public C1180a() {
        this.f3948a = 0;
        this.f3949b = null;
        this.f3949b = new JNIFavorite();
    }

    /* renamed from: a */
    public int mo13546a(Bundle bundle) {
        try {
            return this.f3949b.GetAll(this.f3948a, bundle);
        } catch (Throwable unused) {
            return 0;
        }
    }

    /* renamed from: a */
    public long mo13547a() {
        this.f3948a = this.f3949b.Create();
        return this.f3948a;
    }

    /* renamed from: a */
    public boolean mo13548a(int i) {
        return this.f3949b.SetType(this.f3948a, i);
    }

    /* renamed from: a */
    public boolean mo13549a(String str) {
        return this.f3949b.Remove(this.f3948a, str);
    }

    /* renamed from: a */
    public boolean mo13550a(String str, String str2) {
        C1181a.m4372b();
        return this.f3949b.Add(this.f3948a, str, str2);
    }

    /* renamed from: a */
    public boolean mo13551a(String str, String str2, String str3, int i, int i2, int i3) {
        return this.f3949b.Load(this.f3948a, str, str2, str3, i, i2, i3);
    }

    /* renamed from: b */
    public int mo13552b() {
        return this.f3949b.Release(this.f3948a);
    }

    /* renamed from: b */
    public String mo13553b(String str) {
        try {
            return this.f3949b.GetValue(this.f3948a, str);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: b */
    public boolean mo13554b(String str, String str2) {
        C1181a.m4372b();
        return this.f3949b.Update(this.f3948a, str, str2);
    }

    /* renamed from: c */
    public boolean mo13555c() {
        return this.f3949b.Clear(this.f3948a);
    }

    /* renamed from: c */
    public boolean mo13556c(String str) {
        try {
            return this.f3949b.IsExist(this.f3948a, str);
        } catch (Throwable unused) {
            return false;
        }
    }

    /* renamed from: d */
    public boolean mo13557d() {
        return this.f3949b.SaveCache(this.f3948a);
    }
}
