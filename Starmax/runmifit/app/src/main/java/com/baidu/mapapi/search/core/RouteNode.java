package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;

public class RouteNode implements Parcelable {
    public static final Parcelable.Creator<RouteNode> CREATOR = new C0966j();

    /* renamed from: a */
    private String f2936a;

    /* renamed from: b */
    private LatLng f2937b;

    /* renamed from: c */
    private String f2938c;

    public RouteNode() {
    }

    protected RouteNode(Parcel parcel) {
        this.f2936a = parcel.readString();
        this.f2937b = (LatLng) parcel.readValue(LatLng.class.getClassLoader());
        this.f2938c = parcel.readString();
    }

    public static RouteNode location(LatLng latLng) {
        RouteNode routeNode = new RouteNode();
        routeNode.setLocation(latLng);
        return routeNode;
    }

    public static RouteNode titleAndLocation(String str, LatLng latLng) {
        RouteNode routeNode = new RouteNode();
        routeNode.setTitle(str);
        routeNode.setLocation(latLng);
        return routeNode;
    }

    public int describeContents() {
        return 0;
    }

    public LatLng getLocation() {
        return this.f2937b;
    }

    public String getTitle() {
        return this.f2936a;
    }

    public String getUid() {
        return this.f2938c;
    }

    public void setLocation(LatLng latLng) {
        this.f2937b = latLng;
    }

    public void setTitle(String str) {
        this.f2936a = str;
    }

    public void setUid(String str) {
        this.f2938c = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2936a);
        parcel.writeValue(this.f2937b);
        parcel.writeString(this.f2938c);
    }
}
