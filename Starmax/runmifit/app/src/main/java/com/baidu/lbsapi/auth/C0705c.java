package com.baidu.lbsapi.auth;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.lbsapi.auth.c */
class C0705c {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public Context f1079a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public HashMap<String, String> f1080b = null;

    /* renamed from: c */
    private C0706a<String> f1081c = null;

    /* renamed from: com.baidu.lbsapi.auth.c$a */
    interface C0706a<Result> {
        /* renamed from: a */
        void mo10204a(Result result);
    }

    protected C0705c(Context context) {
        this.f1079a = context;
    }

    /* renamed from: a */
    private HashMap<String, String> m1631a(HashMap<String, String> hashMap) {
        HashMap<String, String> hashMap2 = new HashMap<>();
        for (String str : hashMap.keySet()) {
            String str2 = str.toString();
            hashMap2.put(str2, hashMap.get(str2));
        }
        return hashMap2;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1633a(String str) {
        JSONObject jSONObject;
        if (str == null) {
            str = "";
        }
        try {
            jSONObject = new JSONObject(str);
            if (!jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
                jSONObject.put(NotificationCompat.CATEGORY_STATUS, -1);
            }
        } catch (JSONException unused) {
            jSONObject = new JSONObject();
            try {
                jSONObject.put(NotificationCompat.CATEGORY_STATUS, -1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        C0706a<String> aVar = this.f1081c;
        if (aVar != null) {
            aVar.mo10204a(jSONObject.toString());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo10203a(HashMap<String, String> hashMap, C0706a<String> aVar) {
        this.f1080b = m1631a(hashMap);
        this.f1081c = aVar;
        new Thread(new C0707d(this)).start();
    }
}
