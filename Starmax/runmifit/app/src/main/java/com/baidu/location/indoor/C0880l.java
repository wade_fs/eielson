package com.baidu.location.indoor;

import android.location.Location;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.baidu.location.indoor.l */
public class C0880l {

    /* renamed from: a */
    private List<Location> f2155a;

    /* renamed from: b */
    private String f2156b;

    /* renamed from: c */
    private Location f2157c = null;

    C0880l(String str, Location[] locationArr) {
        if (locationArr != null && locationArr.length > 0) {
            m2628a(locationArr);
            this.f2156b = str;
        }
    }

    /* renamed from: a */
    private void m2628a(Location[] locationArr) {
        if (locationArr != null && locationArr.length > 0) {
            if (this.f2155a == null) {
                this.f2155a = new ArrayList();
            }
            double d = 0.0d;
            double d2 = 0.0d;
            for (int i = 0; i < locationArr.length; i++) {
                d += locationArr[i].getLatitude();
                d2 += locationArr[i].getLongitude();
                this.f2155a.add(locationArr[i]);
            }
            if (this.f2157c == null) {
                this.f2157c = new Location("gps");
                Location location = this.f2157c;
                double length = (double) locationArr.length;
                Double.isNaN(length);
                location.setLatitude(d / length);
                Location location2 = this.f2157c;
                double length2 = (double) locationArr.length;
                Double.isNaN(length2);
                location2.setLongitude(d2 / length2);
            }
        }
    }

    /* renamed from: a */
    public String mo10782a() {
        return this.f2156b;
    }
}
