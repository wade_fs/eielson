package com.baidu.mobstat;

import android.content.ContentValues;
import android.database.Cursor;
import java.io.Closeable;
import java.io.File;
import java.util.ArrayList;

/* renamed from: com.baidu.mobstat.q */
abstract class C1299q implements Closeable {

    /* renamed from: a */
    private C1307t f4385a;

    /* renamed from: a */
    public abstract long mo13980a(String str, String str2);

    /* renamed from: a */
    public abstract ArrayList<C1298p> mo13983a(int i, int i2);

    /* renamed from: b */
    public abstract boolean mo13987b(long j);

    public C1299q(String str, String str2) {
        C1306s sVar = new C1306s();
        this.f4385a = new C1307t(sVar, str);
        File databasePath = sVar.getDatabasePath(".confd");
        if (databasePath != null && databasePath.canWrite()) {
            m4832a(str2);
        }
    }

    /* renamed from: a */
    private void m4832a(String str) {
        this.f4385a.mo14003a(str);
    }

    /* renamed from: a */
    public synchronized boolean mo13984a() {
        try {
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            return false;
        }
        return this.f4385a.mo14004a();
    }

    public synchronized void close() {
        try {
            this.f4385a.close();
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
        }
        return;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public int mo13986b() {
        return this.f4385a.mo14005b();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Cursor mo13981a(String str, int i, int i2) {
        return this.f4385a.mo14002a(null, null, null, null, null, str + " desc", i2 + ", " + i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Cursor mo13982a(String str, String str2, String str3, int i) {
        String str4 = str + "=? ";
        String[] strArr = {str2};
        return this.f4385a.mo14002a(null, str4, strArr, null, null, str3 + " desc", i + "");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public long mo13979a(ContentValues contentValues) {
        return this.f4385a.mo14001a((String) null, contentValues);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo13985a(long j) {
        if (this.f4385a.mo14000a("_id=? ", new String[]{j + ""}) > 0) {
            return true;
        }
        return false;
    }
}
