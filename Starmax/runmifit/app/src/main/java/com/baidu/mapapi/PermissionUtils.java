package com.baidu.mapapi;

import com.baidu.mapsdkplatform.comapi.util.C1169d;

public class PermissionUtils {

    /* renamed from: com.baidu.mapapi.PermissionUtils$a */
    private static class C0913a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final PermissionUtils f2322a = new PermissionUtils();
    }

    private PermissionUtils() {
    }

    public static PermissionUtils getInstance() {
        return C0913a.f2322a;
    }

    public boolean isIndoorNaviAuthorized() {
        return C1169d.m4246a().mo13395b();
    }
}
