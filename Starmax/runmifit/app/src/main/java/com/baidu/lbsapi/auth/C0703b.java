package com.baidu.lbsapi.auth;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Base64;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.util.AndroidUtilsLight;
import com.google.common.base.Ascii;
import com.tencent.mid.api.MidEntity;
import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Locale;

/* renamed from: com.baidu.lbsapi.auth.b */
class C0703b {

    /* renamed from: com.baidu.lbsapi.auth.b$a */
    static class C0704a {
        /* renamed from: a */
        public static String m1629a(byte[] bArr) {
            char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
            StringBuilder sb = new StringBuilder(bArr.length * 2);
            for (int i = 0; i < bArr.length; i++) {
                sb.append(cArr[(bArr[i] & 240) >> 4]);
                sb.append(cArr[bArr[i] & Ascii.f4512SI]);
            }
            return sb.toString();
        }
    }

    /* renamed from: a */
    static String m1619a() {
        return Locale.getDefault().getLanguage();
    }

    /* renamed from: a */
    protected static String m1620a(Context context) {
        String packageName = context.getPackageName();
        String a = m1621a(context, packageName);
        return a + ";" + packageName;
    }

    /* renamed from: a */
    private static String m1621a(Context context, String str) {
        String str2;
        try {
            str2 = m1622a((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(context.getPackageManager().getPackageInfo(str, 64).signatures[0].toByteArray())));
        } catch (PackageManager.NameNotFoundException | CertificateException unused) {
            str2 = "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str2.length(); i++) {
            stringBuffer.append(str2.charAt(i));
            if (i > 0 && i % 2 == 1 && i < str2.length() - 1) {
                stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
            }
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    static String m1622a(X509Certificate x509Certificate) {
        try {
            return C0704a.m1629a(m1623a(x509Certificate.getEncoded()));
        } catch (CertificateEncodingException unused) {
            return null;
        }
    }

    /* renamed from: a */
    static byte[] m1623a(byte[] bArr) {
        try {
            return MessageDigest.getInstance(AndroidUtilsLight.DIGEST_ALGORITHM_SHA1).digest(bArr);
        } catch (NoSuchAlgorithmException unused) {
            return null;
        }
    }

    /* renamed from: b */
    protected static String[] m1624b(Context context) {
        String packageName = context.getPackageName();
        String[] b = m1625b(context, packageName);
        if (b == null || b.length <= 0) {
            return null;
        }
        String[] strArr = new String[b.length];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = b[i] + ";" + packageName;
            if (C0702a.f1077a) {
                C0702a.m1616a("mcode" + strArr[i]);
            }
        }
        return strArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0049  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String[] m1625b(android.content.Context r6, java.lang.String r7) {
        /*
            r0 = 0
            r1 = 0
            android.content.pm.PackageManager r6 = r6.getPackageManager()     // Catch:{ NameNotFoundException | CertificateException -> 0x003c }
            r2 = 64
            android.content.pm.PackageInfo r6 = r6.getPackageInfo(r7, r2)     // Catch:{ NameNotFoundException | CertificateException -> 0x003c }
            android.content.pm.Signature[] r6 = r6.signatures     // Catch:{ NameNotFoundException | CertificateException -> 0x003c }
            if (r6 == 0) goto L_0x003c
            int r7 = r6.length     // Catch:{ NameNotFoundException | CertificateException -> 0x003c }
            if (r7 <= 0) goto L_0x003c
            int r7 = r6.length     // Catch:{ NameNotFoundException | CertificateException -> 0x003c }
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ NameNotFoundException | CertificateException -> 0x003c }
            r2 = 0
        L_0x0017:
            int r3 = r6.length     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            if (r2 >= r3) goto L_0x003d
            java.lang.String r3 = "X.509"
            java.security.cert.CertificateFactory r3 = java.security.cert.CertificateFactory.getInstance(r3)     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            r5 = r6[r2]     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            byte[] r5 = r5.toByteArray()     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            r4.<init>(r5)     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            java.security.cert.Certificate r3 = r3.generateCertificate(r4)     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            java.security.cert.X509Certificate r3 = (java.security.cert.X509Certificate) r3     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            java.lang.String r3 = m1622a(r3)     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            r7[r2] = r3     // Catch:{ NameNotFoundException | CertificateException -> 0x003a }
            int r2 = r2 + 1
            goto L_0x0017
        L_0x003a:
            goto L_0x003d
        L_0x003c:
            r7 = r0
        L_0x003d:
            if (r7 == 0) goto L_0x0081
            int r6 = r7.length
            if (r6 <= 0) goto L_0x0081
            int r6 = r7.length
            java.lang.String[] r0 = new java.lang.String[r6]
            r6 = 0
        L_0x0046:
            int r2 = r7.length
            if (r6 >= r2) goto L_0x0081
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            r3 = 0
        L_0x004f:
            r4 = r7[r6]
            int r4 = r4.length()
            if (r3 >= r4) goto L_0x0078
            r4 = r7[r6]
            char r4 = r4.charAt(r3)
            r2.append(r4)
            if (r3 <= 0) goto L_0x0075
            int r4 = r3 % 2
            r5 = 1
            if (r4 != r5) goto L_0x0075
            r4 = r7[r6]
            int r4 = r4.length()
            int r4 = r4 - r5
            if (r3 >= r4) goto L_0x0075
            java.lang.String r4 = ":"
            r2.append(r4)
        L_0x0075:
            int r3 = r3 + 1
            goto L_0x004f
        L_0x0078:
            java.lang.String r2 = r2.toString()
            r0[r6] = r2
            int r6 = r6 + 1
            goto L_0x0046
        L_0x0081:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.lbsapi.auth.C0703b.m1625b(android.content.Context, java.lang.String):java.lang.String[]");
    }

    /* renamed from: c */
    static String m1626c(Context context) {
        String string = context.getSharedPreferences(MidEntity.TAG_MAC, 0).getString("macaddr", null);
        if (string == null) {
            String d = m1628d(context);
            if (d != null) {
                string = Base64.encodeToString(d.getBytes(), 0);
                if (!TextUtils.isEmpty(string)) {
                    context.getSharedPreferences(MidEntity.TAG_MAC, 0).edit().putString("macaddr", string).commit();
                }
            } else {
                string = "";
            }
        }
        if (C0702a.f1077a) {
            C0702a.m1616a("getMacID mac_adress: " + string);
        }
        return string;
    }

    /* renamed from: c */
    private static boolean m1627c(Context context, String str) {
        boolean z = context.checkCallingOrSelfPermission(str) != -1;
        if (C0702a.f1077a) {
            C0702a.m1616a("hasPermission " + z + " | " + str);
        }
        return z;
    }

    /* renamed from: d */
    static String m1628d(Context context) {
        String str;
        String str2 = null;
        try {
            if (m1627c(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
                str2 = connectionInfo.getMacAddress();
                if (!TextUtils.isEmpty(str2)) {
                    Base64.encode(str2.getBytes(), 0);
                }
                if (C0702a.f1077a) {
                    str = String.format("ssid=%s mac=%s", connectionInfo.getSSID(), connectionInfo.getMacAddress());
                }
                return str2;
            }
            if (C0702a.f1077a) {
                str = "You need the android.Manifest.permission.ACCESS_WIFI_STATE permission. Open AndroidManifest.xml and just before the final </manifest> tag add:android.permission.ACCESS_WIFI_STATE";
            }
            return str2;
            C0702a.m1616a(str);
        } catch (Exception e) {
            if (C0702a.f1077a) {
                C0702a.m1616a(e.toString());
            }
        }
        return str2;
    }
}
