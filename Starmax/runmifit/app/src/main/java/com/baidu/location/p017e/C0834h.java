package com.baidu.location.p017e;

import android.net.wifi.ScanResult;
import android.text.TextUtils;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/* renamed from: com.baidu.location.e.h */
public class C0834h {

    /* renamed from: a */
    public List<ScanResult> f1789a = null;

    /* renamed from: b */
    private long f1790b = 0;

    /* renamed from: c */
    private long f1791c = 0;

    /* renamed from: d */
    private boolean f1792d = false;

    /* renamed from: e */
    private boolean f1793e;

    public C0834h(List<ScanResult> list, long j) {
        this.f1790b = j;
        this.f1789a = list;
        this.f1791c = System.currentTimeMillis();
        try {
            m2353o();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    private boolean m2350a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return Pattern.compile("wpa|wep", 2).matcher(str).find();
    }

    /* renamed from: b */
    private String m2351b(String str) {
        return str != null ? (str.contains("&") || str.contains(";")) ? str.replace("&", "_").replace(";", "_") : str : str;
    }

    /* renamed from: n */
    private int m2352n() {
        List<ScanResult> list = this.f1789a;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    /* renamed from: o */
    private void m2353o() {
        if (m2352n() >= 1) {
            int size = this.f1789a.size() - 1;
            boolean z = true;
            while (size >= 1 && z) {
                boolean z2 = false;
                for (int i = 0; i < size; i++) {
                    if (this.f1789a.get(i) != null) {
                        int i2 = i + 1;
                        if (this.f1789a.get(i2) != null && this.f1789a.get(i).level < this.f1789a.get(i2).level) {
                            List<ScanResult> list = this.f1789a;
                            list.set(i2, list.get(i));
                            this.f1789a.set(i, this.f1789a.get(i2));
                            z2 = true;
                        }
                    }
                }
                size--;
                z = z2;
            }
        }
    }

    /* renamed from: a */
    public int mo10664a() {
        List<ScanResult> list = this.f1789a;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    /* renamed from: a */
    public String mo10665a(int i) {
        return mo10666a(i, false, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0328, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005c, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x020d A[Catch:{ Error -> 0x0328, Exception -> 0x005c }] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0232 A[Catch:{ Error -> 0x0328, Exception -> 0x005c }] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0326 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:160:? A[ExcHandler: Error (unused java.lang.Error), SYNTHETIC, Splitter:B:34:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053 A[SYNTHETIC, Splitter:B:17:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:34:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00d1 A[Catch:{ Error -> 0x0328, Exception -> 0x005c }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00da A[Catch:{ Error -> 0x0328, Exception -> 0x005c }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00fe A[Catch:{ Error -> 0x0328, Exception -> 0x005c }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0125 A[Catch:{ Error -> 0x0328, Exception -> 0x005c }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String mo10666a(int r27, boolean r28, boolean r29) {
        /*
            r26 = this;
            r0 = r26
            int r1 = r26.mo10664a()
            r2 = 0
            r3 = 1
            if (r1 >= r3) goto L_0x000b
            return r2
        L_0x000b:
            java.util.Random r1 = new java.util.Random     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            r1.<init>()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            r5 = 512(0x200, float:7.175E-43)
            r4.<init>(r5)     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            r5.<init>()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            com.baidu.location.e.i r6 = com.baidu.location.p017e.C0835i.m2376a()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            android.net.wifi.WifiInfo r6 = r6.mo10695l()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            java.lang.String r7 = ":"
            java.lang.String r9 = ""
            if (r6 == 0) goto L_0x0048
            java.lang.String r10 = r6.getBSSID()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            if (r10 == 0) goto L_0x0048
            java.lang.String r10 = r6.getBSSID()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            java.lang.String r10 = r10.replace(r7, r9)     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            int r6 = r6.getRssi()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            com.baidu.location.e.i r11 = com.baidu.location.p017e.C0835i.m2376a()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            java.lang.String r11 = r11.mo10697n()     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            if (r6 >= 0) goto L_0x004b
            int r6 = -r6
            goto L_0x004b
        L_0x0048:
            r10 = r2
            r11 = r10
            r6 = -1
        L_0x004b:
            int r12 = android.os.Build.VERSION.SDK_INT     // Catch:{ Error -> 0x032c, Exception -> 0x032a }
            r13 = 17
            r14 = 0
            if (r12 < r13) goto L_0x006b
            long r12 = android.os.SystemClock.elapsedRealtimeNanos()     // Catch:{ Error -> 0x005f, Exception -> 0x005c }
            r16 = 1000(0x3e8, double:4.94E-321)
            long r12 = r12 / r16
            goto L_0x0060
        L_0x005c:
            r1 = 0
            goto L_0x032b
        L_0x005f:
            r12 = r14
        L_0x0060:
            int r16 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r16 <= 0) goto L_0x0068
            r16 = r12
            r12 = 1
            goto L_0x006e
        L_0x0068:
            r16 = r12
            goto L_0x006d
        L_0x006b:
            r16 = r14
        L_0x006d:
            r12 = 0
        L_0x006e:
            if (r12 == 0) goto L_0x0077
            if (r12 == 0) goto L_0x0076
            if (r28 == 0) goto L_0x0076
            r12 = 1
            goto L_0x0077
        L_0x0076:
            r12 = 0
        L_0x0077:
            java.util.List<android.net.wifi.ScanResult> r13 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r13 = r13.size()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r14 = r27
            if (r13 <= r14) goto L_0x0082
            r13 = r14
        L_0x0082:
            r2 = 0
            r8 = 0
            r14 = 0
            r15 = 1
            r18 = 0
            r20 = 0
            r21 = 0
        L_0x008c:
            java.lang.String r3 = "|"
            if (r14 >= r13) goto L_0x022e
            r27 = r13
            java.util.List<android.net.wifi.ScanResult> r13 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Object r13 = r13.get(r14)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r13 == 0) goto L_0x0218
            java.util.List<android.net.wifi.ScanResult> r13 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Object r13 = r13.get(r14)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            android.net.wifi.ScanResult r13 = (android.net.wifi.ScanResult) r13     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r13 = r13.level     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r13 != 0) goto L_0x00a8
            goto L_0x0218
        L_0x00a8:
            if (r12 == 0) goto L_0x00d2
            java.util.List<android.net.wifi.ScanResult> r13 = r0.f1789a     // Catch:{ Exception -> 0x00c0, Error -> 0x0328 }
            java.lang.Object r13 = r13.get(r14)     // Catch:{ Exception -> 0x00c0, Error -> 0x0328 }
            android.net.wifi.ScanResult r13 = (android.net.wifi.ScanResult) r13     // Catch:{ Exception -> 0x00c0, Error -> 0x0328 }
            r28 = r12
            long r12 = r13.timestamp     // Catch:{ Exception -> 0x00c2, Error -> 0x0328 }
            long r12 = r16 - r12
            r22 = 1000000(0xf4240, double:4.940656E-318)
            long r12 = r12 / r22
            r22 = r11
            goto L_0x00c6
        L_0x00c0:
            r28 = r12
        L_0x00c2:
            r22 = r11
            r12 = 0
        L_0x00c6:
            java.lang.Long r11 = java.lang.Long.valueOf(r12)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r5.add(r11)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r11 = (r12 > r18 ? 1 : (r12 == r18 ? 0 : -1))
            if (r11 <= 0) goto L_0x00d6
            goto L_0x00d8
        L_0x00d2:
            r22 = r11
            r28 = r12
        L_0x00d6:
            r12 = r18
        L_0x00d8:
            if (r15 == 0) goto L_0x00fe
            java.lang.String r3 = "&wf="
            r4.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r29 == 0) goto L_0x00fc
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r2.<init>()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r3 = "&wf_ch="
            r2.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.util.List<android.net.wifi.ScanResult> r3 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Object r3 = r3.get(r14)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            android.net.wifi.ScanResult r3 = (android.net.wifi.ScanResult) r3     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r3 = r3.frequency     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r3 = r0.mo10669b(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r2.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
        L_0x00fc:
            r15 = 0
            goto L_0x0119
        L_0x00fe:
            r4.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r29 == 0) goto L_0x0119
            if (r2 == 0) goto L_0x0119
            r2.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.util.List<android.net.wifi.ScanResult> r3 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Object r3 = r3.get(r14)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            android.net.wifi.ScanResult r3 = (android.net.wifi.ScanResult) r3     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r3 = r3.frequency     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r3 = r0.mo10669b(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r2.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
        L_0x0119:
            java.util.List<android.net.wifi.ScanResult> r3 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Object r3 = r3.get(r14)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            android.net.wifi.ScanResult r3 = (android.net.wifi.ScanResult) r3     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r3 = r3.BSSID     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r3 == 0) goto L_0x020d
            java.lang.String r3 = r3.replace(r7, r9)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.util.List<android.net.wifi.ScanResult> r11 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Object r11 = r11.get(r14)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            android.net.wifi.ScanResult r11 = (android.net.wifi.ScanResult) r11     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r11 = r11.level     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r11 >= 0) goto L_0x0139
            int r11 = -r11
        L_0x0139:
            r18 = r2
            java.util.Locale r2 = java.util.Locale.CHINA     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r23 = r7
            java.lang.String r7 = ";%d;"
            r24 = r12
            r12 = 1
            java.lang.Object[] r13 = new java.lang.Object[r12]     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r12 = 0
            r13[r12] = r11     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r2 = java.lang.String.format(r2, r7, r13)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r2)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            int r2 = r20 + 1
            if (r10 == 0) goto L_0x0171
            boolean r3 = r10.equals(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r3 == 0) goto L_0x0171
            java.util.List<android.net.wifi.ScanResult> r3 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Object r3 = r3.get(r14)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            android.net.wifi.ScanResult r3 = (android.net.wifi.ScanResult) r3     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r3 = r3.capabilities     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            boolean r3 = r0.m2350a(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r0.f1793e = r3     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r8 = r2
            r3 = 1
            goto L_0x0172
        L_0x0171:
            r3 = 0
        L_0x0172:
            if (r3 != 0) goto L_0x01f5
            r3 = 30
            r7 = 2
            r11 = r21
            if (r11 != 0) goto L_0x01b3
            r12 = 10
            int r12 = r1.nextInt(r12)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            if (r12 != r7) goto L_0x01ee
            java.util.List<android.net.wifi.ScanResult> r7 = r0.f1789a     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.Object r7 = r7.get(r14)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            android.net.wifi.ScanResult r7 = (android.net.wifi.ScanResult) r7     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.String r7 = r7.SSID     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            if (r7 == 0) goto L_0x01ee
            java.util.List<android.net.wifi.ScanResult> r7 = r0.f1789a     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.Object r7 = r7.get(r14)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            android.net.wifi.ScanResult r7 = (android.net.wifi.ScanResult) r7     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.String r7 = r7.SSID     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            int r7 = r7.length()     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            if (r7 >= r3) goto L_0x01ee
            java.util.List<android.net.wifi.ScanResult> r3 = r0.f1789a     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.Object r3 = r3.get(r14)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            android.net.wifi.ScanResult r3 = (android.net.wifi.ScanResult) r3     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.String r3 = r3.SSID     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.String r3 = r0.m2351b(r3)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            r4.append(r3)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            r21 = 1
            goto L_0x01f0
        L_0x01b3:
            r12 = 1
            if (r11 != r12) goto L_0x01ee
            r13 = 20
            int r13 = r1.nextInt(r13)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            if (r13 != r12) goto L_0x01ee
            java.util.List<android.net.wifi.ScanResult> r12 = r0.f1789a     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.Object r12 = r12.get(r14)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            android.net.wifi.ScanResult r12 = (android.net.wifi.ScanResult) r12     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.String r12 = r12.SSID     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            if (r12 == 0) goto L_0x01ee
            java.util.List<android.net.wifi.ScanResult> r12 = r0.f1789a     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.Object r12 = r12.get(r14)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            android.net.wifi.ScanResult r12 = (android.net.wifi.ScanResult) r12     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.String r12 = r12.SSID     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            int r12 = r12.length()     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            if (r12 >= r3) goto L_0x01ee
            java.util.List<android.net.wifi.ScanResult> r3 = r0.f1789a     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.Object r3 = r3.get(r14)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            android.net.wifi.ScanResult r3 = (android.net.wifi.ScanResult) r3     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.String r3 = r3.SSID     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            java.lang.String r3 = r0.m2351b(r3)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            r4.append(r3)     // Catch:{ Exception -> 0x0208, Error -> 0x0328 }
            r21 = 2
            goto L_0x01f0
        L_0x01ee:
            r21 = r11
        L_0x01f0:
            r20 = r2
        L_0x01f2:
            r2 = r18
            goto L_0x0215
        L_0x01f5:
            r11 = r21
            java.util.List<android.net.wifi.ScanResult> r3 = r0.f1789a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Object r3 = r3.get(r14)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            android.net.wifi.ScanResult r3 = (android.net.wifi.ScanResult) r3     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r3 = r3.SSID     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r3 = r0.m2351b(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
        L_0x0208:
            r20 = r2
            r21 = r11
            goto L_0x01f2
        L_0x020d:
            r18 = r2
            r23 = r7
            r24 = r12
            r11 = r21
        L_0x0215:
            r18 = r24
            goto L_0x0222
        L_0x0218:
            r23 = r7
            r22 = r11
            r28 = r12
            r11 = r21
            r21 = r11
        L_0x0222:
            int r14 = r14 + 1
            r13 = r27
            r12 = r28
            r11 = r22
            r7 = r23
            goto L_0x008c
        L_0x022e:
            r22 = r11
            if (r15 != 0) goto L_0x0326
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r1.<init>()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r7 = "&wf_n="
            r1.append(r7)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r1.append(r8)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r1 = r1.toString()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r10 == 0) goto L_0x025f
            r1 = -1
            if (r6 == r1) goto L_0x025f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r1.<init>()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r7 = "&wf_rs="
            r1.append(r7)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r1.append(r6)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r1 = r1.toString()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
        L_0x025f:
            r6 = 10
            int r1 = (r18 > r6 ? 1 : (r18 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x02da
            int r1 = r5.size()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r1 <= 0) goto L_0x02da
            r1 = 0
            java.lang.Object r6 = r5.get(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Long r6 = (java.lang.Long) r6     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            long r6 = r6.longValue()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r10 = 0
            int r1 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r1 <= 0) goto L_0x02da
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r6 = 128(0x80, float:1.794E-43)
            r1.<init>(r6)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r6 = "&wf_ut="
            r1.append(r6)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r6 = 0
            java.lang.Object r7 = r5.get(r6)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r10 = 1
        L_0x0294:
            boolean r11 = r5.hasNext()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r11 == 0) goto L_0x02d2
            java.lang.Object r11 = r5.next()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.Long r11 = (java.lang.Long) r11     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r10 == 0) goto L_0x02ad
            long r10 = r11.longValue()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r1.append(r10)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r10 = 0
            r13 = 0
            goto L_0x02ce
        L_0x02ad:
            long r11 = r11.longValue()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            long r13 = r7.longValue()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            long r11 = r11 - r13
            r13 = 0
            int r15 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r15 == 0) goto L_0x02ce
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r15.<init>()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r15.append(r9)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r15.append(r11)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r11 = r15.toString()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r1.append(r11)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
        L_0x02ce:
            r1.append(r3)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            goto L_0x0294
        L_0x02d2:
            java.lang.String r1 = r1.toString()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            goto L_0x02db
        L_0x02da:
            r6 = 0
        L_0x02db:
            java.lang.String r1 = "&wf_st="
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            long r9 = r0.f1790b     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r9)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r1 = "&wf_et="
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            long r9 = r0.f1791c     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r9)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r1 = "&wf_vt="
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            long r9 = com.baidu.location.p017e.C0835i.f1794a     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r9)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r8 <= 0) goto L_0x030c
            r12 = 1
            r0.f1792d = r12     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            java.lang.String r1 = "&wf_en="
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            boolean r1 = r0.f1793e     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            if (r1 == 0) goto L_0x0308
            goto L_0x0309
        L_0x0308:
            r12 = 0
        L_0x0309:
            r4.append(r12)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
        L_0x030c:
            if (r22 == 0) goto L_0x0318
            java.lang.String r1 = "&wf_gw="
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r11 = r22
            r4.append(r11)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
        L_0x0318:
            if (r2 == 0) goto L_0x0321
            java.lang.String r1 = r2.toString()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            r4.append(r1)     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
        L_0x0321:
            java.lang.String r1 = r4.toString()     // Catch:{ Error -> 0x0328, Exception -> 0x005c }
            return r1
        L_0x0326:
            r1 = 0
            return r1
        L_0x0328:
            r1 = 0
            goto L_0x032d
        L_0x032a:
            r1 = r2
        L_0x032b:
            return r1
        L_0x032c:
            r1 = r2
        L_0x032d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0834h.mo10666a(int, boolean, boolean):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003b  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo10667a(long r20) {
        /*
            r19 = this;
            r0 = r19
            int r1 = android.os.Build.VERSION.SDK_INT
            r3 = 1000(0x3e8, double:4.94E-321)
            r5 = 0
            r6 = 0
            r8 = 17
            if (r1 < r8) goto L_0x001a
            long r8 = android.os.SystemClock.elapsedRealtimeNanos()     // Catch:{ Error | Exception -> 0x0013 }
            long r8 = r8 / r3
            goto L_0x0014
        L_0x0013:
            r8 = r6
        L_0x0014:
            int r1 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x001b
            r1 = 1
            goto L_0x001c
        L_0x001a:
            r8 = r6
        L_0x001b:
            r1 = 0
        L_0x001c:
            if (r1 != 0) goto L_0x001f
            return r5
        L_0x001f:
            java.util.List<android.net.wifi.ScanResult> r10 = r0.f1789a
            if (r10 == 0) goto L_0x0086
            int r10 = r10.size()
            if (r10 != 0) goto L_0x002a
            goto L_0x0086
        L_0x002a:
            java.util.List<android.net.wifi.ScanResult> r10 = r0.f1789a
            int r10 = r10.size()
            r11 = 16
            if (r10 <= r11) goto L_0x0036
            r10 = 16
        L_0x0036:
            r12 = r6
            r14 = r12
            r11 = 0
        L_0x0039:
            if (r11 >= r10) goto L_0x0071
            java.util.List<android.net.wifi.ScanResult> r2 = r0.f1789a
            java.lang.Object r2 = r2.get(r11)
            if (r2 == 0) goto L_0x006c
            java.util.List<android.net.wifi.ScanResult> r2 = r0.f1789a
            java.lang.Object r2 = r2.get(r11)
            android.net.wifi.ScanResult r2 = (android.net.wifi.ScanResult) r2
            int r2 = r2.level
            if (r2 != 0) goto L_0x0050
            goto L_0x006c
        L_0x0050:
            if (r1 == 0) goto L_0x006c
            java.util.List<android.net.wifi.ScanResult> r2 = r0.f1789a     // Catch:{ Error | Exception -> 0x0064 }
            java.lang.Object r2 = r2.get(r11)     // Catch:{ Error | Exception -> 0x0064 }
            android.net.wifi.ScanResult r2 = (android.net.wifi.ScanResult) r2     // Catch:{ Error | Exception -> 0x0064 }
            long r6 = r2.timestamp     // Catch:{ Error | Exception -> 0x0064 }
            long r6 = r8 - r6
            r17 = 1000000(0xf4240, double:4.940656E-318)
            long r6 = r6 / r17
            goto L_0x0066
        L_0x0064:
            r6 = 0
        L_0x0066:
            long r12 = r12 + r6
            int r2 = (r6 > r14 ? 1 : (r6 == r14 ? 0 : -1))
            if (r2 <= 0) goto L_0x006c
            r14 = r6
        L_0x006c:
            int r11 = r11 + 1
            r6 = 0
            goto L_0x0039
        L_0x0071:
            long r1 = (long) r10
            long r12 = r12 / r1
            long r14 = r14 * r3
            int r1 = (r14 > r20 ? 1 : (r14 == r20 ? 0 : -1))
            if (r1 > 0) goto L_0x0083
            long r12 = r12 * r3
            int r1 = (r12 > r20 ? 1 : (r12 == r20 ? 0 : -1))
            if (r1 <= 0) goto L_0x0080
            goto L_0x0083
        L_0x0080:
            r16 = 0
            goto L_0x0085
        L_0x0083:
            r16 = 1
        L_0x0085:
            return r16
        L_0x0086:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0834h.mo10667a(long):boolean");
    }

    /* renamed from: a */
    public boolean mo10668a(C0834h hVar) {
        List<ScanResult> list = this.f1789a;
        if (list == null || hVar == null || hVar.f1789a == null) {
            return false;
        }
        int size = (list.size() < hVar.f1789a.size() ? this.f1789a : hVar.f1789a).size();
        for (int i = 0; i < size; i++) {
            if (this.f1789a.get(i) != null && !this.f1789a.get(i).BSSID.equals(hVar.f1789a.get(i).BSSID)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: b */
    public int mo10669b(int i) {
        if (i <= 2400 || i >= 2500) {
            return (i <= 4900 || i >= 5900) ? 0 : 5;
        }
        return 2;
    }

    /* renamed from: b */
    public String mo10670b() {
        try {
            return mo10666a(C0855k.f1914O, true, true);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: b */
    public boolean mo10671b(C0834h hVar) {
        List<ScanResult> list = this.f1789a;
        if (list == null || hVar == null || hVar.f1789a == null) {
            return false;
        }
        int size = (list.size() < hVar.f1789a.size() ? this.f1789a : hVar.f1789a).size();
        for (int i = 0; i < size; i++) {
            if (this.f1789a.get(i) != null) {
                String str = this.f1789a.get(i).BSSID;
                int i2 = this.f1789a.get(i).level;
                String str2 = hVar.f1789a.get(i).BSSID;
                int i3 = hVar.f1789a.get(i).level;
                if (!str.equals(str2) || i2 != i3) {
                    return false;
                }
            }
        }
        return true;
    }

    /* renamed from: c */
    public String mo10672c() {
        try {
            return mo10666a(C0855k.f1914O, true, false);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: c */
    public String mo10673c(int i) {
        if (mo10664a() < 1) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(512);
        int size = this.f1789a.size();
        if (size <= i) {
            i = size;
        }
        boolean z = true;
        for (int i2 = 0; i2 < i; i2++) {
            if (!(this.f1789a.get(i2) == null || this.f1789a.get(i2).level == 0 || this.f1789a.get(i2).BSSID == null)) {
                if (z) {
                    z = false;
                } else {
                    stringBuffer.append("|");
                }
                stringBuffer.append(this.f1789a.get(i2).BSSID.replace(Config.TRACE_TODAY_VISIT_SPLIT, ""));
                int i3 = this.f1789a.get(i2).level;
                if (i3 < 0) {
                    i3 = -i3;
                }
                stringBuffer.append(String.format(Locale.CHINA, ";%d;", Integer.valueOf(i3)));
            }
        }
        if (!z) {
            return stringBuffer.toString();
        }
        return null;
    }

    /* renamed from: c */
    public boolean mo10674c(C0834h hVar) {
        return C0835i.m2378a(hVar, this);
    }

    /* renamed from: d */
    public String mo10675d() {
        try {
            return mo10665a(15);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: d */
    public String mo10676d(int i) {
        if (i == 0) {
            return null;
        }
        int i2 = 1;
        if (mo10664a() < 1) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(256);
        int size = this.f1789a.size();
        if (size > C0855k.f1914O) {
            size = C0855k.f1914O;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            if (this.f1789a.get(i4) != null) {
                if (!((i2 & i) == 0 || this.f1789a.get(i4).BSSID == null)) {
                    stringBuffer.append(i3 == 0 ? "&ssid=" : "|");
                    stringBuffer.append(this.f1789a.get(i4).BSSID.replace(Config.TRACE_TODAY_VISIT_SPLIT, ""));
                    stringBuffer.append(";");
                    stringBuffer.append(m2351b(this.f1789a.get(i4).SSID));
                    i3++;
                }
                i2 <<= 1;
            }
        }
        return stringBuffer.toString();
    }

    /* renamed from: e */
    public boolean mo10677e() {
        return mo10667a((long) C0855k.f1938ag);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002c  */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long mo10678f() {
        /*
            r13 = this;
            java.util.List<android.net.wifi.ScanResult> r0 = r13.f1789a
            r1 = 0
            if (r0 == 0) goto L_0x0075
            int r0 = r0.size()
            if (r0 != 0) goto L_0x000e
            goto L_0x0075
        L_0x000e:
            r3 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r0 = android.os.Build.VERSION.SDK_INT
            r5 = 17
            r6 = 0
            if (r0 < r5) goto L_0x0027
            long r7 = android.os.SystemClock.elapsedRealtimeNanos()     // Catch:{ Error | Exception -> 0x0020 }
            r9 = 1000(0x3e8, double:4.94E-321)
            long r7 = r7 / r9
            goto L_0x0021
        L_0x0020:
            r7 = r1
        L_0x0021:
            int r0 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0028
            r0 = 1
            goto L_0x0029
        L_0x0027:
            r7 = r1
        L_0x0028:
            r0 = 0
        L_0x0029:
            if (r0 != 0) goto L_0x002c
            return r1
        L_0x002c:
            java.util.List<android.net.wifi.ScanResult> r5 = r13.f1789a
            int r5 = r5.size()
            r9 = 16
            if (r5 <= r9) goto L_0x0038
            r5 = 16
        L_0x0038:
            if (r6 >= r5) goto L_0x006b
            java.util.List<android.net.wifi.ScanResult> r9 = r13.f1789a
            java.lang.Object r9 = r9.get(r6)
            if (r9 == 0) goto L_0x0068
            java.util.List<android.net.wifi.ScanResult> r9 = r13.f1789a
            java.lang.Object r9 = r9.get(r6)
            android.net.wifi.ScanResult r9 = (android.net.wifi.ScanResult) r9
            int r9 = r9.level
            if (r9 != 0) goto L_0x004f
            goto L_0x0068
        L_0x004f:
            if (r0 == 0) goto L_0x0068
            java.util.List<android.net.wifi.ScanResult> r9 = r13.f1789a     // Catch:{ Error | Exception -> 0x0062 }
            java.lang.Object r9 = r9.get(r6)     // Catch:{ Error | Exception -> 0x0062 }
            android.net.wifi.ScanResult r9 = (android.net.wifi.ScanResult) r9     // Catch:{ Error | Exception -> 0x0062 }
            long r9 = r9.timestamp     // Catch:{ Error | Exception -> 0x0062 }
            long r9 = r7 - r9
            r11 = 1000000(0xf4240, double:4.940656E-318)
            long r9 = r9 / r11
            goto L_0x0063
        L_0x0062:
            r9 = r1
        L_0x0063:
            int r11 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r11 >= 0) goto L_0x0068
            r3 = r9
        L_0x0068:
            int r6 = r6 + 1
            goto L_0x0038
        L_0x006b:
            if (r0 == 0) goto L_0x006e
            goto L_0x006f
        L_0x006e:
            r3 = r1
        L_0x006f:
            int r0 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0074
            goto L_0x0075
        L_0x0074:
            r1 = r3
        L_0x0075:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0834h.mo10678f():long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0027 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0028  */
    /* renamed from: g */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long mo10679g() {
        /*
            r13 = this;
            java.util.List<android.net.wifi.ScanResult> r0 = r13.f1789a
            r1 = 0
            if (r0 == 0) goto L_0x0069
            int r0 = r0.size()
            if (r0 != 0) goto L_0x000d
            goto L_0x0069
        L_0x000d:
            int r0 = android.os.Build.VERSION.SDK_INT
            r3 = 17
            r4 = 0
            if (r0 < r3) goto L_0x0023
            long r5 = android.os.SystemClock.elapsedRealtimeNanos()     // Catch:{ Error | Exception -> 0x001c }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r7
            goto L_0x001d
        L_0x001c:
            r5 = r1
        L_0x001d:
            int r0 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x0024
            r0 = 1
            goto L_0x0025
        L_0x0023:
            r5 = r1
        L_0x0024:
            r0 = 0
        L_0x0025:
            if (r0 != 0) goto L_0x0028
            return r1
        L_0x0028:
            java.util.List<android.net.wifi.ScanResult> r3 = r13.f1789a
            int r3 = r3.size()
            r7 = 16
            if (r3 <= r7) goto L_0x0034
            r3 = 16
        L_0x0034:
            r7 = r1
        L_0x0035:
            if (r4 >= r3) goto L_0x0068
            java.util.List<android.net.wifi.ScanResult> r9 = r13.f1789a
            java.lang.Object r9 = r9.get(r4)
            if (r9 == 0) goto L_0x0065
            java.util.List<android.net.wifi.ScanResult> r9 = r13.f1789a
            java.lang.Object r9 = r9.get(r4)
            android.net.wifi.ScanResult r9 = (android.net.wifi.ScanResult) r9
            int r9 = r9.level
            if (r9 != 0) goto L_0x004c
            goto L_0x0065
        L_0x004c:
            if (r0 == 0) goto L_0x0065
            java.util.List<android.net.wifi.ScanResult> r9 = r13.f1789a     // Catch:{ Error | Exception -> 0x005f }
            java.lang.Object r9 = r9.get(r4)     // Catch:{ Error | Exception -> 0x005f }
            android.net.wifi.ScanResult r9 = (android.net.wifi.ScanResult) r9     // Catch:{ Error | Exception -> 0x005f }
            long r9 = r9.timestamp     // Catch:{ Error | Exception -> 0x005f }
            long r9 = r5 - r9
            r11 = 1000000(0xf4240, double:4.940656E-318)
            long r9 = r9 / r11
            goto L_0x0060
        L_0x005f:
            r9 = r1
        L_0x0060:
            int r11 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r11 <= 0) goto L_0x0065
            r7 = r9
        L_0x0065:
            int r4 = r4 + 1
            goto L_0x0035
        L_0x0068:
            return r7
        L_0x0069:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0834h.mo10679g():long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002b  */
    /* renamed from: h */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long mo10680h() {
        /*
            r18 = this;
            r0 = r18
            java.util.List<android.net.wifi.ScanResult> r1 = r0.f1789a
            r2 = 0
            if (r1 == 0) goto L_0x007e
            int r1 = r1.size()
            if (r1 != 0) goto L_0x0010
            goto L_0x007e
        L_0x0010:
            int r1 = android.os.Build.VERSION.SDK_INT
            r4 = 17
            r5 = 0
            if (r1 < r4) goto L_0x0026
            long r6 = android.os.SystemClock.elapsedRealtimeNanos()     // Catch:{ Error | Exception -> 0x001f }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 / r8
            goto L_0x0020
        L_0x001f:
            r6 = r2
        L_0x0020:
            int r1 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x0027
            r1 = 1
            goto L_0x0028
        L_0x0026:
            r6 = r2
        L_0x0027:
            r1 = 0
        L_0x0028:
            if (r1 != 0) goto L_0x002b
            return r2
        L_0x002b:
            java.util.List<android.net.wifi.ScanResult> r4 = r0.f1789a
            int r4 = r4.size()
            r8 = 16
            if (r4 <= r8) goto L_0x0037
            r4 = 16
        L_0x0037:
            r8 = r2
            r10 = r8
            r12 = r10
        L_0x003a:
            r14 = 1
            if (r5 >= r4) goto L_0x0075
            java.util.List<android.net.wifi.ScanResult> r2 = r0.f1789a
            java.lang.Object r2 = r2.get(r5)
            if (r2 == 0) goto L_0x0070
            java.util.List<android.net.wifi.ScanResult> r2 = r0.f1789a
            java.lang.Object r2 = r2.get(r5)
            android.net.wifi.ScanResult r2 = (android.net.wifi.ScanResult) r2
            int r2 = r2.level
            if (r2 != 0) goto L_0x0053
            goto L_0x0070
        L_0x0053:
            if (r1 == 0) goto L_0x0070
            java.util.List<android.net.wifi.ScanResult> r2 = r0.f1789a     // Catch:{ Error | Exception -> 0x0067 }
            java.lang.Object r2 = r2.get(r5)     // Catch:{ Error | Exception -> 0x0067 }
            android.net.wifi.ScanResult r2 = (android.net.wifi.ScanResult) r2     // Catch:{ Error | Exception -> 0x0067 }
            long r2 = r2.timestamp     // Catch:{ Error | Exception -> 0x0067 }
            long r2 = r6 - r2
            r16 = 1000000(0xf4240, double:4.940656E-318)
            long r2 = r2 / r16
            goto L_0x0069
        L_0x0067:
            r2 = 0
        L_0x0069:
            long r12 = r12 + r2
            long r8 = r8 + r14
            int r14 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r14 <= 0) goto L_0x0070
            r10 = r2
        L_0x0070:
            int r5 = r5 + 1
            r2 = 0
            goto L_0x003a
        L_0x0075:
            int r1 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r1 <= 0) goto L_0x007d
            long r12 = r12 - r10
            long r8 = r8 - r14
            long r10 = r12 / r8
        L_0x007d:
            return r10
        L_0x007e:
            r1 = r2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0834h.mo10680h():long");
    }

    /* renamed from: i */
    public int mo10681i() {
        int i;
        for (int i2 = 0; i2 < mo10664a(); i2++) {
            if (this.f1789a.get(i2) != null && (i = -this.f1789a.get(i2).level) > 0) {
                return i;
            }
        }
        return 0;
    }

    /* renamed from: j */
    public boolean mo10682j() {
        return this.f1792d;
    }

    /* renamed from: k */
    public boolean mo10683k() {
        return System.currentTimeMillis() - this.f1791c > 0 && System.currentTimeMillis() - this.f1791c < 5000;
    }

    /* renamed from: l */
    public boolean mo10684l() {
        return System.currentTimeMillis() - this.f1791c > 0 && System.currentTimeMillis() - this.f1791c < 5000;
    }

    /* renamed from: m */
    public boolean mo10685m() {
        return System.currentTimeMillis() - this.f1791c > 0 && System.currentTimeMillis() - this.f1790b < 5000;
    }
}
