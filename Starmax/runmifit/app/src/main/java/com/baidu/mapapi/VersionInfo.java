package com.baidu.mapapi;

public class VersionInfo {
    public static final String KIT_NAME = "BaiduMapSDK_base_v5_4_1";
    public static final String VERSION_DESC = "baidumapapi_base";
    public static final String VERSION_INFO = "5_4_1";

    public static String getApiVersion() {
        return VERSION_INFO;
    }

    public static String getKitName() {
        return KIT_NAME;
    }

    public static String getVersionDesc() {
        return VERSION_DESC;
    }
}
