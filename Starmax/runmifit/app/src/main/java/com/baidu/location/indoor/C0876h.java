package com.baidu.location.indoor;

import com.baidu.location.BDLocation;
import com.baidu.location.indoor.C0908p;
import com.baidu.location.p017e.C0826e;

/* renamed from: com.baidu.location.indoor.h */
class C0876h implements C0908p.C0909a {

    /* renamed from: a */
    final /* synthetic */ C0865g f2149a;

    C0876h(C0865g gVar) {
        this.f2149a = gVar;
    }

    /* renamed from: a */
    public void mo10779a(BDLocation bDLocation) {
        String g;
        if (this.f2149a.mo10769f()) {
            if (this.f2149a.f2050ae != null && System.currentTimeMillis() - this.f2149a.f2050ae.f2103c > 20000 && System.currentTimeMillis() - this.f2149a.f2050ae.f2105e < 10000) {
                bDLocation.setLocType(61);
                bDLocation.setFloor(null);
                bDLocation.setBuildingID(null);
                bDLocation.setBuildingName(null);
            }
            BDLocation bDLocation2 = new BDLocation(bDLocation);
            if (C0826e.m2306a().mo10647j() && (g = C0826e.m2306a().mo10644g()) != null) {
                BDLocation bDLocation3 = new BDLocation(g);
                bDLocation2.setLocType(61);
                bDLocation2.setSatelliteNumber(bDLocation3.getSatelliteNumber());
                bDLocation2.setSpeed(bDLocation3.getSpeed());
                bDLocation2.setAltitude(bDLocation3.getAltitude());
                bDLocation2.setDirection(bDLocation3.getDirection());
            }
            this.f2149a.m2526a(bDLocation2, 29);
            this.f2149a.f2051af.mo10775a(bDLocation);
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f2149a.f2050ae != null && currentTimeMillis - this.f2149a.f2050ae.f2103c > 30000 && currentTimeMillis - this.f2149a.f2050ae.f2105e > 30000) {
            this.f2149a.mo10767d();
        }
    }
}
