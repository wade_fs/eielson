package com.baidu.location.p019g;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import com.baidu.android.bbalbs.common.p012a.C0692b;
import com.baidu.location.BDLocation;
import com.baidu.location.C0839f;
import com.baidu.location.Jni;
import com.baidu.location.p014b.C0775d;
import com.baidu.location.p017e.C0821a;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0826e;
import com.baidu.location.p017e.C0834h;
import com.google.common.primitives.UnsignedBytes;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Locale;

/* renamed from: com.baidu.location.g.k */
public class C0855k {

    /* renamed from: A */
    public static float f1900A = 2.2f;

    /* renamed from: B */
    public static float f1901B = 2.3f;

    /* renamed from: C */
    public static float f1902C = 3.8f;

    /* renamed from: D */
    public static int f1903D = 3;

    /* renamed from: E */
    public static int f1904E = 10;

    /* renamed from: F */
    public static int f1905F = 2;

    /* renamed from: G */
    public static int f1906G = 7;

    /* renamed from: H */
    public static int f1907H = 20;

    /* renamed from: I */
    public static int f1908I = 70;

    /* renamed from: J */
    public static int f1909J = 120;

    /* renamed from: K */
    public static float f1910K = 2.0f;

    /* renamed from: L */
    public static float f1911L = 10.0f;

    /* renamed from: M */
    public static float f1912M = 50.0f;

    /* renamed from: N */
    public static float f1913N = 200.0f;

    /* renamed from: O */
    public static int f1914O = 16;

    /* renamed from: P */
    public static int f1915P = 32;

    /* renamed from: Q */
    public static float f1916Q = 0.9f;

    /* renamed from: R */
    public static int f1917R = 10000;

    /* renamed from: S */
    public static float f1918S = 0.5f;

    /* renamed from: T */
    public static float f1919T = 0.0f;

    /* renamed from: U */
    public static float f1920U = 0.1f;

    /* renamed from: V */
    public static int f1921V = 30;

    /* renamed from: W */
    public static int f1922W = 100;

    /* renamed from: X */
    public static int f1923X = 0;

    /* renamed from: Y */
    public static int f1924Y = 0;

    /* renamed from: Z */
    public static int f1925Z = 0;

    /* renamed from: a */
    public static boolean f1926a = false;

    /* renamed from: aA */
    private static String f1927aA = "http://loc.map.baidu.com/oqur.php";

    /* renamed from: aB */
    private static String f1928aB = "http://loc.map.baidu.com/tcu.php";

    /* renamed from: aC */
    private static String f1929aC = "http://loc.map.baidu.com/rtbu.php";

    /* renamed from: aD */
    private static String f1930aD = "http://loc.map.baidu.com/iofd.php";

    /* renamed from: aE */
    private static String f1931aE = "http://loc.map.baidu.com/wloc";

    /* renamed from: aa */
    public static int f1932aa = 420000;

    /* renamed from: ab */
    public static boolean f1933ab = true;

    /* renamed from: ac */
    public static boolean f1934ac = true;

    /* renamed from: ad */
    public static int f1935ad = 20;

    /* renamed from: ae */
    public static int f1936ae = 300;

    /* renamed from: af */
    public static int f1937af = 1000;

    /* renamed from: ag */
    public static int f1938ag = Integer.MAX_VALUE;

    /* renamed from: ah */
    public static long f1939ah = 900000;

    /* renamed from: ai */
    public static long f1940ai = 420000;

    /* renamed from: aj */
    public static long f1941aj = 180000;

    /* renamed from: ak */
    public static long f1942ak = 0;

    /* renamed from: al */
    public static long f1943al = 15;

    /* renamed from: am */
    public static long f1944am = 300000;

    /* renamed from: an */
    public static int f1945an = 1000;

    /* renamed from: ao */
    public static int f1946ao = 0;

    /* renamed from: ap */
    public static int f1947ap = 30000;

    /* renamed from: aq */
    public static int f1948aq = 30000;

    /* renamed from: ar */
    public static float f1949ar = 10.0f;

    /* renamed from: as */
    public static float f1950as = 6.0f;

    /* renamed from: at */
    public static float f1951at = 10.0f;

    /* renamed from: au */
    public static int f1952au = 60;

    /* renamed from: av */
    public static int f1953av = 70;

    /* renamed from: aw */
    public static int f1954aw = 6;

    /* renamed from: ax */
    public static String f1955ax = null;

    /* renamed from: ay */
    private static String f1956ay = "http://loc.map.baidu.com/sdk.php";

    /* renamed from: az */
    private static String f1957az = "http://loc.map.baidu.com/user_err.php";

    /* renamed from: b */
    public static boolean f1958b = false;

    /* renamed from: c */
    public static boolean f1959c = false;

    /* renamed from: d */
    public static int f1960d = 0;

    /* renamed from: e */
    public static String f1961e = "http://loc.map.baidu.com/sdk_ep.php";

    /* renamed from: f */
    public static String f1962f = "https://loc.map.baidu.com/sdk.php";

    /* renamed from: g */
    public static String f1963g = "no";

    /* renamed from: h */
    public static boolean f1964h = false;

    /* renamed from: i */
    public static boolean f1965i = false;

    /* renamed from: j */
    public static boolean f1966j = false;

    /* renamed from: k */
    public static boolean f1967k = false;

    /* renamed from: l */
    public static boolean f1968l = false;

    /* renamed from: m */
    public static boolean f1969m = false;

    /* renamed from: n */
    public static String f1970n = "gcj02";

    /* renamed from: o */
    public static String f1971o = "";

    /* renamed from: p */
    public static boolean f1972p = true;

    /* renamed from: q */
    public static int f1973q = 3;

    /* renamed from: r */
    public static double f1974r = 0.0d;

    /* renamed from: s */
    public static double f1975s = 0.0d;

    /* renamed from: t */
    public static double f1976t = 0.0d;

    /* renamed from: u */
    public static double f1977u = 0.0d;

    /* renamed from: v */
    public static int f1978v = 0;

    /* renamed from: w */
    public static byte[] f1979w = null;

    /* renamed from: x */
    public static boolean f1980x = false;

    /* renamed from: y */
    public static int f1981y = 0;

    /* renamed from: z */
    public static float f1982z = 1.1f;

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0016 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0017 A[RETURN] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int m2450a(android.content.Context r4, java.lang.String r5) {
        /*
            r0 = 0
            r1 = 1
            int r2 = android.os.Process.myPid()     // Catch:{ Exception -> 0x0013 }
            int r3 = android.os.Process.myUid()     // Catch:{ Exception -> 0x0013 }
            int r4 = r4.checkPermission(r5, r2, r3)     // Catch:{ Exception -> 0x0013 }
            if (r4 != 0) goto L_0x0011
            goto L_0x0013
        L_0x0011:
            r4 = 0
            goto L_0x0014
        L_0x0013:
            r4 = 1
        L_0x0014:
            if (r4 != 0) goto L_0x0017
            return r0
        L_0x0017:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p019g.C0855k.m2450a(android.content.Context, java.lang.String):int");
    }

    /* renamed from: a */
    public static int m2451a(String str, String str2, String str3) {
        int indexOf;
        int length;
        int indexOf2;
        String substring;
        if (!(str == null || str.equals("") || (indexOf = str.indexOf(str2)) == -1 || (indexOf2 = str.indexOf(str3, (length = indexOf + str2.length()))) == -1 || (substring = str.substring(length, indexOf2)) == null || substring.equals(""))) {
            try {
                return Integer.parseInt(substring);
            } catch (NumberFormatException unused) {
            }
        }
        return Integer.MIN_VALUE;
    }

    /* renamed from: a */
    public static String m2452a() {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(5);
        int i2 = instance.get(1);
        int i3 = instance.get(11);
        int i4 = instance.get(12);
        int i5 = instance.get(13);
        return String.format(Locale.CHINA, "%d-%02d-%02d %02d:%02d:%02d", Integer.valueOf(i2), Integer.valueOf(instance.get(2) + 1), Integer.valueOf(i), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5));
    }

    /* renamed from: a */
    public static String m2453a(C0821a aVar, C0834h hVar, Location location, String str, int i) {
        return m2454a(aVar, hVar, location, str, i, false);
    }

    /* renamed from: a */
    public static String m2454a(C0821a aVar, C0834h hVar, Location location, String str, int i, boolean z) {
        String a;
        String b;
        StringBuffer stringBuffer = new StringBuffer(1024);
        if (!(aVar == null || (b = C0822b.m2279a().mo10626b(aVar)) == null)) {
            stringBuffer.append(b);
        }
        if (hVar != null) {
            String b2 = i == 0 ? z ? hVar.mo10670b() : hVar.mo10672c() : hVar.mo10675d();
            if (b2 != null) {
                stringBuffer.append(b2);
            }
        }
        if (location != null) {
            String b3 = (f1960d == 0 || i == 0) ? C0826e.m2319b(location) : C0826e.m2327c(location);
            if (b3 != null) {
                stringBuffer.append(b3);
            }
        }
        boolean z2 = false;
        if (i == 0) {
            z2 = true;
        }
        String a2 = C0844b.m2417a().mo10713a(z2);
        if (a2 != null) {
            stringBuffer.append(a2);
        }
        if (str != null) {
            stringBuffer.append(str);
        }
        String d = C0775d.m2005a().mo10531d();
        if (!TextUtils.isEmpty(d)) {
            stringBuffer.append("&bc=");
            stringBuffer.append(d);
        }
        if (!(aVar == null || (a = C0822b.m2279a().mo10625a(aVar)) == null || a.length() + stringBuffer.length() >= 750)) {
            stringBuffer.append(a);
        }
        String stringBuffer2 = stringBuffer.toString();
        if (!(location == null || hVar == null)) {
            try {
                float speed = location.getSpeed();
                int i2 = f1960d;
                int i3 = hVar.mo10681i();
                int a3 = hVar.mo10664a();
                boolean j = hVar.mo10682j();
                if (speed < f1950as && ((i2 == 1 || i2 == 0) && (i3 < f1952au || j))) {
                    f1973q = 1;
                    return stringBuffer2;
                } else if (speed < f1951at && ((i2 == 1 || i2 == 0 || i2 == 3) && (i3 < f1953av || a3 > f1954aw))) {
                    f1973q = 2;
                    return stringBuffer2;
                }
            } catch (Exception unused) {
                f1973q = 3;
            }
        }
        f1973q = 3;
        return stringBuffer2;
    }

    /* renamed from: a */
    public static String m2455a(File file, String str) {
        if (!file.isFile()) {
            return null;
        }
        byte[] bArr = new byte[1024];
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            FileInputStream fileInputStream = new FileInputStream(file);
            while (true) {
                int read = fileInputStream.read(bArr, 0, 1024);
                if (read != -1) {
                    instance.update(bArr, 0, read);
                } else {
                    fileInputStream.close();
                    return new BigInteger(1, instance.digest()).toString(16);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public static String m2456a(String str) {
        return Jni.en1(f1971o + ";" + str);
    }

    /* renamed from: a */
    public static boolean m2457a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            NetworkInfo[] networkInfoArr = null;
            try {
                networkInfoArr = connectivityManager.getAllNetworkInfo();
            } catch (Exception unused) {
            }
            if (networkInfoArr != null) {
                for (NetworkInfo networkInfo : networkInfoArr) {
                    if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* renamed from: a */
    public static boolean m2458a(BDLocation bDLocation) {
        int locType = bDLocation.getLocType();
        return (locType > 100 && locType < 200) || locType == 62;
    }

    /* renamed from: b */
    public static int m2459b(Context context) {
        try {
            return Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0);
        } catch (Exception unused) {
            return 2;
        }
    }

    /* renamed from: b */
    public static boolean m2460b() {
        return false;
    }

    /* renamed from: b */
    public static boolean m2461b(String str, String str2, String str3) {
        try {
            PublicKey generatePublic = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(C0692b.m1540a(str3.getBytes())));
            Signature instance = Signature.getInstance("SHA1WithRSA");
            instance.initVerify(generatePublic);
            instance.update(str.getBytes());
            return instance.verify(C0692b.m1540a(str2.getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: c */
    public static int m2462c(Context context) {
        if (Build.VERSION.SDK_INT < 19) {
            return -2;
        }
        try {
            return Settings.Secure.getInt(context.getContentResolver(), "location_mode", -1);
        } catch (Exception unused) {
            return -1;
        }
    }

    /* renamed from: c */
    public static String m2463c() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress() && (nextElement instanceof Inet4Address)) {
                            byte[] address = nextElement.getAddress();
                            String str = "";
                            for (byte b : address) {
                                String hexString = Integer.toHexString(b & UnsignedBytes.MAX_VALUE);
                                if (hexString.length() == 1) {
                                    hexString = '0' + hexString;
                                }
                                str = str + hexString;
                            }
                            return str;
                        }
                    }
                }
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: d */
    public static String m2464d() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress() && (nextElement instanceof Inet6Address) && nextElement.getHostAddress() != null && !nextElement.getHostAddress().startsWith("fe80:")) {
                            return nextElement.getHostAddress();
                        }
                    }
                }
            }
            return "";
        } catch (SocketException unused) {
            return "";
        }
    }

    /* renamed from: d */
    public static String m2465d(Context context) {
        int a = m2450a(context, "android.permission.ACCESS_COARSE_LOCATION");
        int a2 = m2450a(context, "android.permission.ACCESS_FINE_LOCATION");
        int a3 = m2450a(context, "android.permission.READ_PHONE_STATE");
        return "&per=" + a + "|" + a2 + "|" + a3;
    }

    /* renamed from: e */
    public static String m2466e() {
        return f1956ay;
    }

    /* renamed from: e */
    public static String m2467e(Context context) {
        int i = -1;
        if (context != null) {
            try {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                    i = activeNetworkInfo.getType();
                }
            } catch (Exception unused) {
            }
        }
        return "&netc=" + i;
    }

    /* renamed from: f */
    public static String m2468f() {
        return f1928aB;
    }

    /* renamed from: g */
    public static String m2469g() {
        return "https://daup.map.baidu.com/cltr/rcvr";
    }

    /* renamed from: h */
    public static String m2470h() {
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                String path = Environment.getExternalStorageDirectory().getPath();
                File file = new File(path + "/baidu/tempdata");
                if (!file.exists()) {
                    file.mkdirs();
                }
                return path;
            }
        } catch (Exception unused) {
        }
        return null;
    }

    /* renamed from: i */
    public static String m2471i() {
        String h = m2470h();
        if (h == null) {
            return null;
        }
        return h + "/baidu/tempdata";
    }

    /* renamed from: j */
    public static String m2472j() {
        try {
            File file = new File(C0839f.getServiceContext().getFilesDir() + File.separator + "lldt");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file.getAbsolutePath();
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: k */
    public static String m2473k() {
        try {
            File file = new File(C0839f.getServiceContext().getFilesDir() + File.separator + "/baidu/tempdata");
            if (!file.exists()) {
                file.mkdirs();
            }
            return C0839f.getServiceContext().getFilesDir().getPath();
        } catch (Exception unused) {
            return null;
        }
    }
}
