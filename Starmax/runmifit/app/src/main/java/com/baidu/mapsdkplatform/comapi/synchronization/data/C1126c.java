package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapsdkplatform.comapi.synchronization.data.RouteLineInfo;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.c */
final class C1126c implements Parcelable.Creator<RouteLineInfo.RouteSectionInfo> {
    C1126c() {
    }

    /* renamed from: a */
    public RouteLineInfo.RouteSectionInfo createFromParcel(Parcel parcel) {
        return new RouteLineInfo.RouteSectionInfo(parcel);
    }

    /* renamed from: a */
    public RouteLineInfo.RouteSectionInfo[] newArray(int i) {
        return new RouteLineInfo.RouteSectionInfo[i];
    }
}
