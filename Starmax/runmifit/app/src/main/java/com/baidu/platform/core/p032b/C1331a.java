package com.baidu.platform.core.p032b;

import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeOption;
import com.baidu.platform.base.C1316a;
import com.baidu.platform.base.SearchType;

/* renamed from: com.baidu.platform.core.b.a */
public class C1331a extends C1316a implements C1334d {

    /* renamed from: b */
    OnGetGeoCoderResultListener f4453b = null;

    /* renamed from: a */
    public void mo14038a() {
        this.f4422a.lock();
        this.f4453b = null;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public void mo14039a(OnGetGeoCoderResultListener onGetGeoCoderResultListener) {
        this.f4422a.lock();
        this.f4453b = onGetGeoCoderResultListener;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public boolean mo14040a(GeoCodeOption geoCodeOption) {
        C1332b bVar = new C1332b();
        C1333c cVar = new C1333c(geoCodeOption);
        bVar.mo14021a(SearchType.GEO_CODER);
        if (geoCodeOption != null) {
            bVar.mo14042b(geoCodeOption.getAddress());
        }
        return mo14016a(cVar, this.f4453b, bVar);
    }

    /* renamed from: a */
    public boolean mo14041a(ReverseGeoCodeOption reverseGeoCodeOption) {
        C1335e eVar = new C1335e();
        C1336f fVar = new C1336f(reverseGeoCodeOption);
        eVar.mo14021a(SearchType.REVERSE_GEO_CODER);
        return mo14016a(fVar, this.f4453b, eVar);
    }
}
