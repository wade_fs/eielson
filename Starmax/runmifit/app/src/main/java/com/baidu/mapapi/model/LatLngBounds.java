package com.baidu.mapapi.model;

import android.os.Parcel;
import android.os.Parcelable;

public final class LatLngBounds implements Parcelable {
    public static final Parcelable.Creator<LatLngBounds> CREATOR = new C0953b();
    public final LatLng northeast;
    public final LatLng southwest;

    public static final class Builder {

        /* renamed from: a */
        private double f2882a;

        /* renamed from: b */
        private double f2883b;

        /* renamed from: c */
        private double f2884c;

        /* renamed from: d */
        private double f2885d;

        /* renamed from: e */
        private boolean f2886e = true;

        public LatLngBounds build() {
            return new LatLngBounds(new LatLng(this.f2883b, this.f2885d), new LatLng(this.f2882a, this.f2884c));
        }

        public Builder include(LatLng latLng) {
            if (latLng == null) {
                return this;
            }
            if (this.f2886e) {
                this.f2886e = false;
                double d = latLng.latitude;
                this.f2882a = d;
                this.f2883b = d;
                double d2 = latLng.longitude;
                this.f2884c = d2;
                this.f2885d = d2;
            }
            double d3 = latLng.latitude;
            double d4 = latLng.longitude;
            if (d3 < this.f2882a) {
                this.f2882a = d3;
            }
            if (d3 > this.f2883b) {
                this.f2883b = d3;
            }
            if (d4 < this.f2884c) {
                this.f2884c = d4;
            }
            if (d4 > this.f2885d) {
                this.f2885d = d4;
            }
            return this;
        }
    }

    protected LatLngBounds(Parcel parcel) {
        this.northeast = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        this.southwest = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
    }

    LatLngBounds(LatLng latLng, LatLng latLng2) {
        this.northeast = latLng;
        this.southwest = latLng2;
    }

    public boolean contains(LatLng latLng) {
        if (latLng == null) {
            return false;
        }
        double d = this.southwest.latitude;
        double d2 = this.northeast.latitude;
        double d3 = this.southwest.longitude;
        double d4 = this.northeast.longitude;
        double d5 = latLng.latitude;
        double d6 = latLng.longitude;
        return d5 >= d && d5 <= d2 && d6 >= d3 && d6 <= d4;
    }

    public int describeContents() {
        return 0;
    }

    public LatLng getCenter() {
        return new LatLng(((this.northeast.latitude - this.southwest.latitude) / 2.0d) + this.southwest.latitude, ((this.northeast.longitude - this.southwest.longitude) / 2.0d) + this.southwest.longitude);
    }

    public String toString() {
        return "southwest: " + this.southwest.latitude + ", " + this.southwest.longitude + "\n" + "northeast: " + this.northeast.latitude + ", " + this.northeast.longitude;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.northeast, i);
        parcel.writeParcelable(this.southwest, i);
    }
}
