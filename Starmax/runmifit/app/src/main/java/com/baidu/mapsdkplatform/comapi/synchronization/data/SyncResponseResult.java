package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;

public final class SyncResponseResult implements Parcelable {
    public static final Parcelable.Creator<SyncResponseResult> CREATOR = new C1140j();

    /* renamed from: a */
    private RouteLineInfo f3710a;

    /* renamed from: b */
    private TrafficInfo f3711b;

    /* renamed from: c */
    private DriverPosition f3712c;

    /* renamed from: d */
    private float f3713d;

    /* renamed from: e */
    private long f3714e;

    /* renamed from: f */
    private float f3715f;

    /* renamed from: g */
    private long f3716g;

    /* renamed from: h */
    private int f3717h;

    /* renamed from: i */
    private String f3718i;

    /* renamed from: j */
    private String f3719j;

    public SyncResponseResult() {
        this.f3710a = new RouteLineInfo();
        this.f3711b = new TrafficInfo();
        this.f3712c = new DriverPosition();
        this.f3713d = 0.0f;
        this.f3714e = 0;
        this.f3715f = 0.0f;
        this.f3716g = 0;
        this.f3717h = 0;
        this.f3718i = null;
        this.f3719j = null;
    }

    protected SyncResponseResult(Parcel parcel) {
        this.f3710a = (RouteLineInfo) parcel.readParcelable(RouteLineInfo.class.getClassLoader());
        this.f3711b = (TrafficInfo) parcel.readParcelable(TrafficInfo.class.getClassLoader());
        this.f3712c = (DriverPosition) parcel.readParcelable(DriverPosition.class.getClassLoader());
        this.f3713d = (float) parcel.readLong();
        this.f3714e = parcel.readLong();
        this.f3715f = (float) parcel.readLong();
        this.f3716g = parcel.readLong();
        this.f3717h = parcel.readInt();
        this.f3718i = parcel.readString();
        this.f3719j = parcel.readString();
    }

    /* renamed from: a */
    public RouteLineInfo mo13227a() {
        return this.f3710a;
    }

    /* renamed from: a */
    public void mo13228a(float f) {
        this.f3713d = f;
    }

    /* renamed from: a */
    public void mo13229a(int i) {
        this.f3717h = i;
    }

    /* renamed from: a */
    public void mo13230a(long j) {
        this.f3714e = j;
    }

    /* renamed from: a */
    public void mo13231a(String str) {
        this.f3718i = str;
    }

    /* renamed from: b */
    public TrafficInfo mo13232b() {
        return this.f3711b;
    }

    /* renamed from: b */
    public void mo13233b(float f) {
        this.f3715f = f;
    }

    /* renamed from: b */
    public void mo13234b(long j) {
        this.f3716g = j;
    }

    /* renamed from: b */
    public void mo13235b(String str) {
        this.f3719j = str;
    }

    /* renamed from: c */
    public DriverPosition mo13236c() {
        return this.f3712c;
    }

    /* renamed from: d */
    public float mo13237d() {
        return this.f3715f;
    }

    public int describeContents() {
        return 0;
    }

    /* renamed from: e */
    public long mo13239e() {
        return this.f3716g;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f3710a, 1);
        parcel.writeParcelable(this.f3711b, 1);
        parcel.writeParcelable(this.f3712c, 1);
        parcel.writeFloat(this.f3713d);
        parcel.writeLong(this.f3714e);
        parcel.writeFloat(this.f3715f);
        parcel.writeLong(this.f3716g);
        parcel.writeInt(this.f3717h);
        parcel.writeString(this.f3718i);
        parcel.writeString(this.f3719j);
    }
}
