package com.baidu.mobstat;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.system.Os;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class CarUUID {

    /* renamed from: a */
    private static final Pattern f4111a = Pattern.compile("(\\w{32})");

    public static String optUUID(Context context) {
        String b = m4407b(context);
        if (b != null) {
            return b;
        }
        String c = m4409c(context);
        if (c != null) {
            m4404a(context, c);
            return c;
        }
        String d = m4411d(context);
        if (d != null) {
            m4404a(context, d);
            m4408b(context, d);
            return d;
        }
        String a = m4402a(context);
        if (a == null) {
            return "";
        }
        m4404a(context, a);
        m4408b(context, a);
        return a;
    }

    /* renamed from: a */
    private static String m4402a(Context context) {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /* renamed from: b */
    private static String m4407b(Context context) {
        return m4403a(context.getFileStreamPath("libdueros_uuid.so"));
    }

    /* renamed from: c */
    private static String m4409c(Context context) {
        if (!m4410c(context, "android.permission.READ_EXTERNAL_STORAGE")) {
            return null;
        }
        return m4403a(new File(new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig"), ".dueros_uuid"));
    }

    /* renamed from: d */
    private static String m4411d(Context context) {
        String a;
        List<ApplicationInfo> installedApplications = context.getPackageManager().getInstalledApplications(0);
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        for (ApplicationInfo applicationInfo2 : installedApplications) {
            if (!applicationInfo.packageName.equals(applicationInfo2.packageName) && (a = m4403a(new File(new File(applicationInfo2.dataDir, "files"), "libdueros_uuid.so"))) != null) {
                return a;
            }
        }
        return null;
    }

    /* renamed from: a */
    private static boolean m4404a(Context context, String str) {
        boolean z = false;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = context.openFileOutput("libdueros_uuid.so", Build.VERSION.SDK_INT >= 21 ? 0 : 1);
            if (m4406a(fileOutputStream, str)) {
                if (Build.VERSION.SDK_INT >= 21) {
                    ApplicationInfo applicationInfo = context.getApplicationInfo();
                    File fileStreamPath = context.getFileStreamPath("libdueros_uuid.so");
                    if (m4405a(new File(applicationInfo.dataDir), 457) && m4405a(fileStreamPath, 484)) {
                        z = true;
                    }
                    C1274bf.m4704a(fileOutputStream);
                    return z;
                }
                C1274bf.m4704a(fileOutputStream);
                return true;
            }
        } catch (Exception unused) {
        } catch (Throwable th) {
            C1274bf.m4704a(null);
            throw th;
        }
        C1274bf.m4704a(fileOutputStream);
        return false;
    }

    /* renamed from: b */
    private static boolean m4408b(Context context, String str) {
        if (!m4410c(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            return false;
        }
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fileOutputStream2 = new FileOutputStream(new File(new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig"), ".dueros_uuid"));
            try {
                boolean a = m4406a(fileOutputStream2, str);
                C1274bf.m4704a(fileOutputStream2);
                return a;
            } catch (Exception unused) {
                fileOutputStream = fileOutputStream2;
                C1274bf.m4704a(fileOutputStream);
                return false;
            } catch (Throwable th) {
                th = th;
                fileOutputStream = fileOutputStream2;
                C1274bf.m4704a(fileOutputStream);
                throw th;
            }
        } catch (Exception unused2) {
            C1274bf.m4704a(fileOutputStream);
            return false;
        } catch (Throwable th2) {
            th = th2;
            C1274bf.m4704a(fileOutputStream);
            throw th;
        }
    }

    /* renamed from: a */
    private static String m4403a(File file) {
        FileInputStream fileInputStream;
        String str = null;
        if (file != null && file.exists()) {
            try {
                fileInputStream = new FileInputStream(file);
            } catch (Exception unused) {
                fileInputStream = null;
                C1274bf.m4704a(fileInputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                fileInputStream = null;
                C1274bf.m4704a(fileInputStream);
                throw th;
            }
            try {
                byte[] bArr = new byte[1024];
                String str2 = new String(bArr, 0, fileInputStream.read(bArr));
                if (f4111a.matcher(str2).matches()) {
                    str = str2;
                }
                C1274bf.m4704a(fileInputStream);
                return str;
            } catch (Exception unused2) {
                C1274bf.m4704a(fileInputStream);
                return null;
            } catch (Throwable th2) {
                th = th2;
                C1274bf.m4704a(fileInputStream);
                throw th;
            }
        }
        return null;
    }

    /* renamed from: a */
    private static boolean m4406a(FileOutputStream fileOutputStream, String str) {
        try {
            fileOutputStream.write(str.getBytes());
            fileOutputStream.flush();
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    /* renamed from: a */
    private static boolean m4405a(File file, int i) {
        if (Build.VERSION.SDK_INT < 21) {
            return true;
        }
        try {
            Os.chmod(file.getAbsolutePath(), i);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    /* renamed from: c */
    private static boolean m4410c(Context context, String str) {
        return context.checkPermission(str, Process.myPid(), Process.myUid()) == 0;
    }
}
