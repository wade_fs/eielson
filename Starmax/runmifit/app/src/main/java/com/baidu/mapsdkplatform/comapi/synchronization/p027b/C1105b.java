package com.baidu.mapsdkplatform.comapi.synchronization.p027b;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceConstant;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceData;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceDisplayOptions;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceQueryOptions;
import com.baidu.mapapi.synchronization.histroytrace.OnHistoryTraceListener;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1112a;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.b.b */
public class C1105b {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final String f3647a = C1105b.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static C1112a f3648b;

    /* renamed from: c */
    private static OnHistoryTraceListener f3649c;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static C1104a f3650e;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static int f3651g = 0;

    /* renamed from: d */
    private HandlerThread f3652d;

    /* renamed from: f */
    private C1106a f3653f;

    /* renamed from: h */
    private HandlerThread f3654h;

    /* renamed from: i */
    private C1109e f3655i;

    /* renamed from: j */
    private volatile boolean f3656j = false;

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.b.b$a */
    public static class C1106a extends Handler {
        C1106a() {
        }

        /* renamed from: a */
        private String m3794a(HistoryTraceQueryOptions historyTraceQueryOptions) {
            String a = new C1110f(historyTraceQueryOptions).mo13192a();
            if (!TextUtils.isEmpty(a)) {
                return a;
            }
            C1120a.m3855b(C1105b.f3647a, "Build request url failed");
            return null;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m3795a(int i, String str, int i2, HistoryTraceQueryOptions historyTraceQueryOptions) {
            if (C1105b.f3650e == null) {
                C1120a.m3855b(C1105b.f3647a, "Data parser handler is null");
                return;
            }
            Message obtainMessage = C1105b.f3650e.obtainMessage();
            obtainMessage.what = i;
            obtainMessage.arg1 = i2;
            obtainMessage.obj = str;
            C1105b.f3650e.sendMessage(obtainMessage);
            C1105b.f3650e.mo13175a(historyTraceQueryOptions);
        }

        /* renamed from: a */
        private void m3796a(HistoryTraceQueryOptions historyTraceQueryOptions, int i) {
            String a = m3794a(historyTraceQueryOptions);
            if (TextUtils.isEmpty(a)) {
                C1105b.m3782b(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_URL_NULL, "QueryOptions is null, please check.", i);
            } else {
                m3801a(a, i, historyTraceQueryOptions);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m3800a(C1115c.C1116a aVar, int i) {
            int i2;
            String str;
            int i3 = C1107c.f3657a[aVar.ordinal()];
            if (i3 == 1) {
                i2 = 0;
                str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_SUCCESS;
            } else if (i3 == 2 || i3 == 3) {
                i2 = HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_SERVER_INNER_ERROR;
                str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_SERVER_INNER_ERROR;
            } else if (i3 == 4) {
                i2 = HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_NETWORK_ERROR;
                str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_NETWORK_ERROR;
            } else if (i3 != 5) {
                i2 = HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_UNDEFINE_ERROR;
                str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_UNDEFINE_ERROR;
            } else {
                i2 = HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_REQUEST_PARAMETER_ERROR;
                str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_REQUEST_PARAMETER_ERROR;
            }
            C1105b.m3782b(i2, str, i);
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m3801a(String str, int i, HistoryTraceQueryOptions historyTraceQueryOptions) {
            if (TextUtils.isEmpty(str)) {
                C1120a.m3855b(C1105b.f3647a, "Request url is null");
                return;
            }
            if (C1105b.f3648b == null) {
                C1112a unused = C1105b.f3648b = new C1112a();
            }
            C1105b.f3648b.mo13193a(str, new C1108d(this, i, historyTraceQueryOptions, str));
        }

        public void handleMessage(Message message) {
            int i = message.what;
            String c = C1105b.f3647a;
            C1120a.m3856c(c, "The query type is: " + i);
            HistoryTraceQueryOptions historyTraceQueryOptions = (HistoryTraceQueryOptions) message.obj;
            if (i == 1) {
                m3796a(historyTraceQueryOptions, i);
            }
        }
    }

    public C1105b() {
        f3648b = new C1112a();
        this.f3653f = new C1106a();
        this.f3652d = new HandlerThread("HistoryTraceDataParser");
        this.f3652d.start();
        f3650e = new C1104a(this.f3652d.getLooper());
        f3650e.mo13177a(this.f3653f);
        this.f3654h = new HandlerThread("HistoryTraceRender");
        this.f3654h.start();
        this.f3655i = new C1109e(this.f3654h.getLooper());
    }

    /* renamed from: a */
    private void m3780a(HistoryTraceData historyTraceData, int i) {
        if (this.f3655i == null) {
            this.f3655i = new C1109e(this.f3654h.getLooper());
        }
        Message obtainMessage = this.f3655i.obtainMessage();
        obtainMessage.what = i;
        if (historyTraceData != null) {
            obtainMessage.obj = historyTraceData;
        }
        this.f3655i.sendMessage(obtainMessage);
    }

    /* renamed from: a */
    private boolean m3781a(HistoryTraceQueryOptions historyTraceQueryOptions, int i) {
        int i2;
        String str;
        if (historyTraceQueryOptions == null) {
            C1120a.m3855b(f3647a, "QueryOptions is null, please check!");
            i2 = 10001;
            str = "QueryOptions is null, please check.";
        } else if (TextUtils.isEmpty(historyTraceQueryOptions.getOrderId())) {
            C1120a.m3855b(f3647a, "Query orderId is null, please check");
            i2 = HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ORDER_ID_NULL;
            str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_QUERY_ORDER_ID_NULL;
        } else if (historyTraceQueryOptions.getRoleType() != 0) {
            C1120a.m3855b(f3647a, "Current role type not the passenger");
            i2 = HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ROLE_TYPE_ERROR;
            str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_QUERY_ROLE_TYPE_ERROR;
        } else if (TextUtils.isEmpty(historyTraceQueryOptions.getUserId())) {
            C1120a.m3855b(f3647a, "Order's user id is null");
            i2 = HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_QUERY_USER_ID_NULL;
        } else if (!TextUtils.isEmpty(historyTraceQueryOptions.getDriverId())) {
            return true;
        } else {
            C1120a.m3855b(f3647a, "Driver id is null");
            i2 = HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_DRIVER_ID_NULL;
            str = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_QUERY_DRIVER_ID_NULL;
        }
        m3782b(i2, str, i);
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m3782b(int i, String str, int i2) {
        OnHistoryTraceListener onHistoryTraceListener = f3649c;
        if (onHistoryTraceListener == null) {
            C1120a.m3855b(f3647a, "OnHistoryTraceListener is null");
        } else if (i2 == 1) {
            onHistoryTraceListener.onQueryHistroyTraceData(i, str, null);
        } else if (i2 == 2) {
            onHistoryTraceListener.onRenderHistroyTrace(i, str);
        }
    }

    /* renamed from: b */
    private void m3783b(HistoryTraceQueryOptions historyTraceQueryOptions, int i) {
        if (this.f3653f == null) {
            this.f3653f = new C1106a();
        }
        Message obtainMessage = this.f3653f.obtainMessage();
        obtainMessage.what = i;
        obtainMessage.obj = historyTraceQueryOptions;
        this.f3653f.sendMessage(obtainMessage);
    }

    /* renamed from: a */
    public void mo13179a() {
        if (f3648b != null) {
            f3648b = null;
        }
        if (f3649c != null) {
            f3649c = null;
        }
        C1106a aVar = this.f3653f;
        if (aVar != null) {
            aVar.removeCallbacksAndMessages(null);
            this.f3653f = null;
        }
        C1104a aVar2 = f3650e;
        if (aVar2 != null) {
            aVar2.removeCallbacksAndMessages(null);
            f3650e.mo13174a();
            f3650e = null;
        }
        HandlerThread handlerThread = this.f3652d;
        if (handlerThread != null) {
            handlerThread.quit();
            this.f3652d = null;
        }
        C1109e eVar = this.f3655i;
        if (eVar != null) {
            eVar.removeCallbacksAndMessages(null);
            this.f3655i.mo13188a();
            this.f3655i = null;
        }
        HandlerThread handlerThread2 = this.f3654h;
        if (handlerThread2 != null) {
            handlerThread2.quit();
            this.f3654h = null;
        }
        this.f3656j = false;
    }

    /* renamed from: a */
    public void mo13180a(BaiduMap baiduMap, HistoryTraceData historyTraceData, HistoryTraceDisplayOptions historyTraceDisplayOptions, int i) {
        if (baiduMap == null) {
            C1120a.m3855b(f3647a, "BaiduMap instance is null");
            OnHistoryTraceListener onHistoryTraceListener = f3649c;
            if (onHistoryTraceListener != null) {
                onHistoryTraceListener.onRenderHistroyTrace(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_BAIDUMAP_NULL, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_BAIDUMAP_NULL);
            }
        } else if (5 != i) {
            OnHistoryTraceListener onHistoryTraceListener2 = f3649c;
            if (onHistoryTraceListener2 != null) {
                onHistoryTraceListener2.onRenderHistroyTrace(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_CURRENT_ORDER_STATE_NOT_COMPLETE, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_CURRENT_ORDER_STATE_NOT_COMPLETE);
            }
        } else {
            this.f3655i.mo13189a(historyTraceDisplayOptions, baiduMap, i);
            m3780a(historyTraceData, 4);
        }
    }

    /* renamed from: a */
    public void mo13181a(HistoryTraceQueryOptions historyTraceQueryOptions) {
        if (!m3781a(historyTraceQueryOptions, 1)) {
            C1120a.m3855b(f3647a, "QueryOptions error, please check!");
        } else {
            m3783b(historyTraceQueryOptions, 1);
        }
    }

    /* renamed from: a */
    public void mo13182a(OnHistoryTraceListener onHistoryTraceListener) {
        f3649c = onHistoryTraceListener;
        f3650e.mo13176a(onHistoryTraceListener);
        this.f3655i.mo13190a(onHistoryTraceListener);
    }

    /* renamed from: a */
    public void mo13183a(boolean z) {
        C1115c.f3681b = z;
    }

    /* renamed from: b */
    public boolean mo13184b() {
        return C1115c.f3681b;
    }
}
