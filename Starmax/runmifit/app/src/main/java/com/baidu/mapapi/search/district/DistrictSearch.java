package com.baidu.mapapi.search.district;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.search.core.C0968l;
import com.baidu.platform.core.p031a.C1329d;
import com.baidu.platform.core.p031a.C1330e;

public class DistrictSearch extends C0968l {

    /* renamed from: a */
    private C1330e f2964a;

    /* renamed from: b */
    private boolean f2965b;

    DistrictSearch() {
        this.f2964a = null;
        this.f2965b = false;
        this.f2964a = new C1329d();
    }

    public static DistrictSearch newInstance() {
        BMapManager.init();
        return new DistrictSearch();
    }

    public void destroy() {
        if (!this.f2965b) {
            this.f2965b = true;
            this.f2964a.mo14035a();
            BMapManager.destroy();
        }
    }

    public boolean searchDistrict(DistrictSearchOption districtSearchOption) {
        if (this.f2964a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (districtSearchOption != null && districtSearchOption.mCityName != null && !districtSearchOption.mCityName.equals("")) {
            return this.f2964a.mo14037a(districtSearchOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or city name can not be null or empty.");
        }
    }

    public void setOnDistrictSearchListener(OnGetDistricSearchResultListener onGetDistricSearchResultListener) {
        C1330e eVar = this.f2964a;
        if (eVar == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (onGetDistricSearchResultListener != null) {
            eVar.mo14036a(onGetDistricSearchResultListener);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: listener can not be null");
        }
    }
}
