package com.baidu.mapapi.map.offline;

import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapsdkplatform.comapi.map.C1093q;
import com.baidu.mapsdkplatform.comapi.map.C1096t;
import java.util.ArrayList;
import java.util.Iterator;

public class OfflineMapUtil {
    public static MKOLSearchRecord getSearchRecordFromLocalCityInfo(C1093q qVar) {
        if (qVar == null) {
            return null;
        }
        MKOLSearchRecord mKOLSearchRecord = new MKOLSearchRecord();
        mKOLSearchRecord.cityID = qVar.f3594a;
        mKOLSearchRecord.cityName = qVar.f3595b;
        mKOLSearchRecord.cityType = qVar.f3597d;
        long j = 0;
        if (qVar.mo13119a() != null) {
            ArrayList<MKOLSearchRecord> arrayList = new ArrayList<>();
            Iterator<C1093q> it = qVar.mo13119a().iterator();
            while (it.hasNext()) {
                C1093q next = it.next();
                arrayList.add(getSearchRecordFromLocalCityInfo(next));
                j += (long) next.f3596c;
                mKOLSearchRecord.childCities = arrayList;
            }
        }
        if (mKOLSearchRecord.cityType != 1) {
            j = (long) qVar.f3596c;
        }
        mKOLSearchRecord.dataSize = j;
        return mKOLSearchRecord;
    }

    public static MKOLUpdateElement getUpdatElementFromLocalMapElement(C1096t tVar) {
        if (tVar == null) {
            return null;
        }
        MKOLUpdateElement mKOLUpdateElement = new MKOLUpdateElement();
        mKOLUpdateElement.cityID = tVar.f3605a;
        mKOLUpdateElement.cityName = tVar.f3606b;
        if (tVar.f3611g != null) {
            mKOLUpdateElement.geoPt = CoordUtil.mc2ll(tVar.f3611g);
        }
        mKOLUpdateElement.level = tVar.f3609e;
        mKOLUpdateElement.ratio = tVar.f3613i;
        mKOLUpdateElement.serversize = tVar.f3612h;
        mKOLUpdateElement.size = tVar.f3613i == 100 ? tVar.f3612h : (tVar.f3612h / 100) * tVar.f3613i;
        mKOLUpdateElement.status = tVar.f3616l;
        mKOLUpdateElement.update = tVar.f3614j;
        return mKOLUpdateElement;
    }
}
