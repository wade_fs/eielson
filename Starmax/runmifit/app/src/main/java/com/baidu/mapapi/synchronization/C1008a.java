package com.baidu.mapapi.synchronization;

import com.baidu.mapapi.synchronization.SyncCoordinateConverter;

/* renamed from: com.baidu.mapapi.synchronization.a */
/* synthetic */ class C1008a {

    /* renamed from: a */
    static final /* synthetic */ int[] f3200a = new int[SyncCoordinateConverter.CoordType.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    static {
        /*
            com.baidu.mapapi.synchronization.SyncCoordinateConverter$CoordType[] r0 = com.baidu.mapapi.synchronization.SyncCoordinateConverter.CoordType.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.baidu.mapapi.synchronization.C1008a.f3200a = r0
            int[] r0 = com.baidu.mapapi.synchronization.C1008a.f3200a     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.baidu.mapapi.synchronization.SyncCoordinateConverter$CoordType r1 = com.baidu.mapapi.synchronization.SyncCoordinateConverter.CoordType.COMMON     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            int[] r0 = com.baidu.mapapi.synchronization.C1008a.f3200a     // Catch:{ NoSuchFieldError -> 0x001f }
            com.baidu.mapapi.synchronization.SyncCoordinateConverter$CoordType r1 = com.baidu.mapapi.synchronization.SyncCoordinateConverter.CoordType.BD09LL     // Catch:{ NoSuchFieldError -> 0x001f }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.synchronization.C1008a.<clinit>():void");
    }
}
