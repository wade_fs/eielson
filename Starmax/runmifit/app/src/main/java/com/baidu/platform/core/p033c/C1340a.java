package com.baidu.platform.core.p033c;

import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiBoundSearchOption;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
import com.baidu.mapapi.search.poi.PoiIndoorOption;
import com.baidu.mapapi.search.poi.PoiNearbySearchOption;

/* renamed from: com.baidu.platform.core.c.a */
public interface C1340a {
    /* renamed from: a */
    void mo14047a();

    /* renamed from: a */
    void mo14048a(OnGetPoiSearchResultListener onGetPoiSearchResultListener);

    /* renamed from: a */
    boolean mo14049a(PoiBoundSearchOption poiBoundSearchOption);

    /* renamed from: a */
    boolean mo14050a(PoiCitySearchOption poiCitySearchOption);

    /* renamed from: a */
    boolean mo14051a(PoiDetailSearchOption poiDetailSearchOption);

    /* renamed from: a */
    boolean mo14052a(PoiIndoorOption poiIndoorOption);

    /* renamed from: a */
    boolean mo14053a(PoiNearbySearchOption poiNearbySearchOption);
}
