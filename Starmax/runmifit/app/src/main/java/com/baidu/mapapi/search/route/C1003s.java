package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.route.WalkingRouteLine;

/* renamed from: com.baidu.mapapi.search.route.s */
final class C1003s implements Parcelable.Creator<WalkingRouteLine.WalkingStep> {
    C1003s() {
    }

    /* renamed from: a */
    public WalkingRouteLine.WalkingStep createFromParcel(Parcel parcel) {
        return new WalkingRouteLine.WalkingStep(parcel);
    }

    /* renamed from: a */
    public WalkingRouteLine.WalkingStep[] newArray(int i) {
        return new WalkingRouteLine.WalkingStep[i];
    }
}
