package com.baidu.location.p019g;

import android.content.Context;
import android.os.Build;
import com.baidu.android.bbalbs.common.util.CommonParam;
import com.baidu.location.C0839f;
import com.baidu.mobstat.Config;
import com.tencent.connect.common.Constants;

/* renamed from: com.baidu.location.g.b */
public class C0844b {

    /* renamed from: e */
    public static String f1848e;

    /* renamed from: f */
    public static String f1849f;

    /* renamed from: g */
    public static String f1850g;

    /* renamed from: h */
    public static String f1851h;

    /* renamed from: i */
    public static int f1852i;

    /* renamed from: j */
    private static C0844b f1853j;

    /* renamed from: a */
    public String f1854a = null;

    /* renamed from: b */
    public String f1855b = null;

    /* renamed from: c */
    public String f1856c = null;

    /* renamed from: d */
    public String f1857d = null;

    /* renamed from: k */
    private boolean f1858k = false;

    private C0844b() {
        if (C0839f.getServiceContext() != null) {
            mo10715a(C0839f.getServiceContext());
        }
    }

    /* renamed from: a */
    public static C0844b m2417a() {
        if (f1853j == null) {
            f1853j = new C0844b();
        }
        return f1853j;
    }

    /* renamed from: a */
    public String mo10713a(boolean z) {
        return mo10714a(z, (String) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0118  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String mo10714a(boolean r4, java.lang.String r5) {
        /*
            r3 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r1 = 256(0x100, float:3.59E-43)
            r0.<init>(r1)
            java.lang.String r1 = "&sdk="
            r0.append(r1)
            r1 = 1090141553(0x40fa3d71, float:7.82)
            r0.append(r1)
            if (r4 == 0) goto L_0x0066
            java.lang.String r1 = com.baidu.location.p019g.C0855k.f1963g
            java.lang.String r2 = "all"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0023
            java.lang.String r1 = "&addr=allj"
            r0.append(r1)
        L_0x0023:
            boolean r1 = com.baidu.location.p019g.C0855k.f1965i
            if (r1 != 0) goto L_0x0028
            goto L_0x002d
        L_0x0028:
            java.lang.String r1 = "&adtp=n2"
            r0.append(r1)
        L_0x002d:
            boolean r1 = com.baidu.location.p019g.C0855k.f1964h
            if (r1 != 0) goto L_0x003d
            boolean r1 = com.baidu.location.p019g.C0855k.f1967k
            if (r1 != 0) goto L_0x003d
            boolean r1 = com.baidu.location.p019g.C0855k.f1968l
            if (r1 != 0) goto L_0x003d
            boolean r1 = com.baidu.location.p019g.C0855k.f1966j
            if (r1 == 0) goto L_0x0066
        L_0x003d:
            java.lang.String r1 = "&sema="
            r0.append(r1)
            boolean r1 = com.baidu.location.p019g.C0855k.f1964h
            if (r1 == 0) goto L_0x004b
            java.lang.String r1 = "aptag|"
            r0.append(r1)
        L_0x004b:
            boolean r1 = com.baidu.location.p019g.C0855k.f1966j
            if (r1 == 0) goto L_0x0054
            java.lang.String r1 = "aptagd|"
            r0.append(r1)
        L_0x0054:
            boolean r1 = com.baidu.location.p019g.C0855k.f1967k
            if (r1 == 0) goto L_0x005d
            java.lang.String r1 = "poiregion|"
            r0.append(r1)
        L_0x005d:
            boolean r1 = com.baidu.location.p019g.C0855k.f1968l
            if (r1 == 0) goto L_0x0066
            java.lang.String r1 = "regular"
            r0.append(r1)
        L_0x0066:
            if (r4 == 0) goto L_0x007e
            if (r5 != 0) goto L_0x006d
            java.lang.String r5 = "&coor=gcj02"
            goto L_0x0072
        L_0x006d:
            java.lang.String r1 = "&coor="
            r0.append(r1)
        L_0x0072:
            r0.append(r5)
            java.lang.String r5 = com.baidu.location.p017e.C0826e.m2336k()
            if (r5 == 0) goto L_0x007e
            r0.append(r5)
        L_0x007e:
            java.lang.String r5 = r3.f1856c
            if (r5 != 0) goto L_0x008d
            java.lang.String r5 = "&im="
        L_0x0084:
            r0.append(r5)
            java.lang.String r5 = r3.f1854a
            r0.append(r5)
            goto L_0x00bd
        L_0x008d:
            java.lang.String r5 = "&cu="
            r0.append(r5)
            java.lang.String r5 = r3.f1856c
            r0.append(r5)
            java.lang.String r5 = r3.f1854a
            if (r5 == 0) goto L_0x00bd
            java.lang.String r1 = "NULL"
            boolean r5 = r5.equals(r1)
            if (r5 != 0) goto L_0x00bd
            java.lang.String r5 = r3.f1856c
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = r3.f1854a
            r1.<init>(r2)
            java.lang.StringBuffer r1 = r1.reverse()
            java.lang.String r1 = r1.toString()
            boolean r5 = r5.contains(r1)
            if (r5 != 0) goto L_0x00bd
            java.lang.String r5 = "&Aim="
            goto L_0x0084
        L_0x00bd:
            java.lang.String r5 = r3.f1855b
            if (r5 == 0) goto L_0x00cb
            java.lang.String r5 = "&snd="
            r0.append(r5)
            java.lang.String r5 = r3.f1855b
            r0.append(r5)
        L_0x00cb:
            java.lang.String r5 = r3.f1857d
            if (r5 == 0) goto L_0x00d9
            java.lang.String r5 = "&Aid="
            r0.append(r5)
            java.lang.String r5 = r3.f1857d
            r0.append(r5)
        L_0x00d9:
            java.lang.String r5 = "&fw="
            r0.append(r5)
            float r5 = com.baidu.location.C0839f.getFrameVersion()
            r0.append(r5)
            java.lang.String r5 = "&lt=1"
            r0.append(r5)
            java.lang.String r5 = "&mb="
            r0.append(r5)
            java.lang.String r5 = android.os.Build.MODEL
            r0.append(r5)
            java.lang.String r5 = com.baidu.location.p019g.C0855k.m2463c()
            if (r5 == 0) goto L_0x0102
            java.lang.String r1 = "&laip="
            r0.append(r1)
            r0.append(r5)
        L_0x0102:
            java.lang.String r5 = "&resid="
            r0.append(r5)
            java.lang.String r5 = "12"
            r0.append(r5)
            java.lang.String r5 = "&os=A"
            r0.append(r5)
            int r5 = android.os.Build.VERSION.SDK_INT
            r0.append(r5)
            if (r4 == 0) goto L_0x0130
            java.lang.String r4 = "&sv="
            r0.append(r4)
            java.lang.String r4 = android.os.Build.VERSION.RELEASE
            if (r4 == 0) goto L_0x012d
            int r5 = r4.length()
            r1 = 6
            if (r5 <= r1) goto L_0x012d
            r5 = 0
            java.lang.String r4 = r4.substring(r5, r1)
        L_0x012d:
            r0.append(r4)
        L_0x0130:
            java.lang.String r4 = r0.toString()
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p019g.C0844b.mo10714a(boolean, java.lang.String):java.lang.String");
    }

    /* renamed from: a */
    public void mo10715a(Context context) {
        if (context != null && !this.f1858k) {
            try {
                this.f1856c = CommonParam.m1545a(context);
            } catch (Exception unused) {
                this.f1856c = null;
            }
            try {
                f1848e = context.getPackageName();
            } catch (Exception unused2) {
                f1848e = null;
            }
            C0855k.f1971o = "" + this.f1856c;
            this.f1858k = true;
        }
    }

    /* renamed from: a */
    public void mo10716a(String str, String str2) {
        f1849f = str;
        f1848e = str2;
    }

    /* renamed from: b */
    public String mo10717b() {
        String str;
        StringBuilder sb;
        if (this.f1856c != null) {
            sb = new StringBuilder();
            sb.append("v7.82|");
            str = this.f1856c;
        } else {
            sb = new StringBuilder();
            sb.append("v7.82|");
            str = this.f1854a;
        }
        sb.append(str);
        sb.append("|");
        sb.append(Build.MODEL);
        return sb.toString();
    }

    /* renamed from: c */
    public String mo10718c() {
        String str;
        StringBuffer stringBuffer = new StringBuffer(200);
        if (this.f1856c != null) {
            stringBuffer.append("&cu=");
            str = this.f1856c;
        } else {
            stringBuffer.append("&im=");
            str = this.f1854a;
        }
        stringBuffer.append(str);
        try {
            stringBuffer.append("&mb=");
            stringBuffer.append(Build.MODEL);
        } catch (Exception unused) {
        }
        stringBuffer.append("&pack=");
        try {
            stringBuffer.append(f1848e);
        } catch (Exception unused2) {
        }
        stringBuffer.append("&sdk=");
        stringBuffer.append(7.82f);
        return stringBuffer.toString();
    }

    /* renamed from: d */
    public String mo10719d() {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        if (this.f1856c == null) {
            stringBuffer.append("&im=");
            str = this.f1854a;
        } else {
            stringBuffer.append("&cu=");
            str = this.f1856c;
        }
        stringBuffer.append(str);
        stringBuffer.append("&sdk=");
        stringBuffer.append(7.82f);
        stringBuffer.append("&mb=");
        stringBuffer.append(Build.MODEL);
        stringBuffer.append("&stp=1");
        stringBuffer.append("&os=A");
        stringBuffer.append(Build.VERSION.SDK);
        stringBuffer.append("&prod=");
        stringBuffer.append(f1849f + Config.TRACE_TODAY_VISIT_SPLIT + f1848e);
        stringBuffer.append(C0855k.m2467e(C0839f.getServiceContext()));
        stringBuffer.append("&resid=");
        stringBuffer.append(Constants.VIA_REPORT_TYPE_SET_AVATAR);
        return stringBuffer.toString();
    }
}
