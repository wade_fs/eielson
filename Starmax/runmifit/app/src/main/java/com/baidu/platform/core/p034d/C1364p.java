package com.baidu.platform.core.p034d;

import com.baidu.mapapi.search.route.WalkingRoutePlanOption;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.google.android.gms.dynamite.ProviderConstants;
import com.tamic.novate.download.MimeType;

/* renamed from: com.baidu.platform.core.d.p */
public class C1364p extends C1320e {
    public C1364p(WalkingRoutePlanOption walkingRoutePlanOption) {
        m5102a(walkingRoutePlanOption);
    }

    /* renamed from: a */
    private void m5102a(WalkingRoutePlanOption walkingRoutePlanOption) {
        this.f4435a.mo14094a("qt", "walk2");
        this.f4435a.mo14094a("sn", mo14024a(walkingRoutePlanOption.mFrom));
        this.f4435a.mo14094a("en", mo14024a(walkingRoutePlanOption.mTo));
        if (walkingRoutePlanOption.mFrom != null) {
            this.f4435a.mo14094a(Config.STAT_SDK_CHANNEL, walkingRoutePlanOption.mFrom.getCity());
        }
        if (walkingRoutePlanOption.mTo != null) {
            this.f4435a.mo14094a("ec", walkingRoutePlanOption.mTo.getCity());
        }
        this.f4435a.mo14094a("ie", "utf-8");
        this.f4435a.mo14094a("lrn", "20");
        this.f4435a.mo14094a(ProviderConstants.API_COLNAME_FEATURE_VERSION, "3");
        this.f4435a.mo14094a("rp_format", MimeType.JSON);
        this.f4435a.mo14094a("rp_filter", "mobile");
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14086k();
    }
}
