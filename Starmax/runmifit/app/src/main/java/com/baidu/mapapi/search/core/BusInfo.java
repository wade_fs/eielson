package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

public class BusInfo extends TransitBaseInfo {
    public static final Parcelable.Creator<BusInfo> CREATOR = new C0957a();

    /* renamed from: a */
    private int f2914a;

    /* renamed from: b */
    private int f2915b;

    public BusInfo() {
    }

    protected BusInfo(Parcel parcel) {
        super(parcel);
        this.f2914a = parcel.readInt();
        this.f2915b = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public int getStopNum() {
        return this.f2915b;
    }

    public int getType() {
        return this.f2914a;
    }

    public void setStopNum(int i) {
        this.f2915b = i;
    }

    public void setType(int i) {
        this.f2914a = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.f2914a);
        parcel.writeInt(this.f2915b);
    }
}
