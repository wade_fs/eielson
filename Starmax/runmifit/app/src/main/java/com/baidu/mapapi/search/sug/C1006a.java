package com.baidu.mapapi.search.sug;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.sug.a */
final class C1006a implements Parcelable.Creator<SuggestionResult> {
    C1006a() {
    }

    /* renamed from: a */
    public SuggestionResult createFromParcel(Parcel parcel) {
        return new SuggestionResult(parcel);
    }

    /* renamed from: a */
    public SuggestionResult[] newArray(int i) {
        return new SuggestionResult[i];
    }
}
