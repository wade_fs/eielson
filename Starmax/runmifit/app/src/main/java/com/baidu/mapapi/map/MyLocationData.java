package com.baidu.mapapi.map;

public class MyLocationData {
    public final float accuracy;
    public final float direction;
    public final double latitude;
    public final double longitude;
    public final int satellitesNum;
    public final float speed;

    public static class Builder {

        /* renamed from: a */
        private double f2656a;

        /* renamed from: b */
        private double f2657b;

        /* renamed from: c */
        private float f2658c;

        /* renamed from: d */
        private float f2659d;

        /* renamed from: e */
        private float f2660e;

        /* renamed from: f */
        private int f2661f;

        public Builder accuracy(float f) {
            this.f2660e = f;
            return this;
        }

        public MyLocationData build() {
            return new MyLocationData(this.f2656a, this.f2657b, this.f2658c, this.f2659d, this.f2660e, this.f2661f);
        }

        public Builder direction(float f) {
            this.f2659d = f;
            return this;
        }

        public Builder latitude(double d) {
            this.f2656a = d;
            return this;
        }

        public Builder longitude(double d) {
            this.f2657b = d;
            return this;
        }

        public Builder satellitesNum(int i) {
            this.f2661f = i;
            return this;
        }

        public Builder speed(float f) {
            this.f2658c = f;
            return this;
        }
    }

    MyLocationData(double d, double d2, float f, float f2, float f3, int i) {
        this.latitude = d;
        this.longitude = d2;
        this.speed = f;
        this.direction = f2;
        this.accuracy = f3;
        this.satellitesNum = i;
    }
}
