package com.baidu.mapapi.http;

import com.baidu.mapapi.http.AsyncHttpClient;
import com.baidu.mapapi.http.HttpClient;

/* renamed from: com.baidu.mapapi.http.a */
class C0917a extends AsyncHttpClient.C0916a {

    /* renamed from: a */
    final /* synthetic */ HttpClient.ProtoResultCallback f2360a;

    /* renamed from: b */
    final /* synthetic */ String f2361b;

    /* renamed from: c */
    final /* synthetic */ AsyncHttpClient f2362c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C0917a(AsyncHttpClient asyncHttpClient, HttpClient.ProtoResultCallback protoResultCallback, String str) {
        super(null);
        this.f2362c = asyncHttpClient;
        this.f2360a = protoResultCallback;
        this.f2361b = str;
    }

    /* renamed from: a */
    public void mo10891a() {
        HttpClient httpClient = new HttpClient("GET", this.f2360a);
        httpClient.setMaxTimeOut(this.f2362c.f2350a);
        httpClient.setReadTimeOut(this.f2362c.f2351b);
        httpClient.request(this.f2361b);
    }
}
