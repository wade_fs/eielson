package com.baidu.platform.core.p033c;

import com.baidu.mapapi.search.poi.PoiIndoorOption;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;

/* renamed from: com.baidu.platform.core.c.c */
public class C1342c extends C1320e {
    public C1342c(PoiIndoorOption poiIndoorOption) {
        m4994a(poiIndoorOption);
    }

    /* renamed from: a */
    private void m4994a(PoiIndoorOption poiIndoorOption) {
        this.f4435a.mo14094a("qt", "indoor_s");
        this.f4435a.mo14094a(Config.EVENT_HEAT_X, "0");
        this.f4435a.mo14094a("y", "0");
        this.f4435a.mo14094a("from", "android_map_sdk");
        String str = poiIndoorOption.bid;
        if (str != null && !str.equals("")) {
            this.f4435a.mo14094a("bid", str);
        }
        String str2 = poiIndoorOption.f3005wd;
        if (str2 != null && !str2.equals("")) {
            this.f4435a.mo14094a("wd", str2);
        }
        String str3 = poiIndoorOption.floor;
        if (str3 != null && !str3.equals("")) {
            this.f4435a.mo14094a("floor", str3);
        }
        C1381a aVar = this.f4435a;
        aVar.mo14094a("current", poiIndoorOption.currentPage + "");
        C1381a aVar2 = this.f4435a;
        aVar2.mo14094a("pageSize", poiIndoorOption.pageSize + "");
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14078c();
    }
}
