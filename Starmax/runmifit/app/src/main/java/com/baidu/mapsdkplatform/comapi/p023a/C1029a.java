package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.map.Marker;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.a */
public class C1029a extends C1031c {

    /* renamed from: a */
    private Animator f3306a = null;

    /* renamed from: b */
    private long f3307b = 0;

    /* renamed from: c */
    private Interpolator f3308c = null;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Animation.AnimationListener f3309d = null;

    /* renamed from: e */
    private int f3310e = 1;

    /* renamed from: f */
    private int f3311f = 0;

    /* renamed from: g */
    private float[] f3312g;

    public C1029a(float... fArr) {
        this.f3312g = fArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ObjectAnimator mo12873a(Marker marker) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(marker, "alpha", this.f3312g);
        if (ofFloat != null) {
            ofFloat.setRepeatCount(this.f3311f);
            ofFloat.setRepeatMode(mo12883c());
            ofFloat.setDuration(this.f3307b);
            Interpolator interpolator = this.f3308c;
            if (interpolator != null) {
                ofFloat.setInterpolator(interpolator);
            }
        }
        return ofFloat;
    }

    /* renamed from: a */
    public void mo12874a() {
        Animator animator = this.f3306a;
        if (animator != null) {
            animator.start();
        }
    }

    /* renamed from: a */
    public void mo12875a(int i) {
        this.f3310e = i;
    }

    /* renamed from: a */
    public void mo12876a(long j) {
        if (j < 0) {
            j = 0;
        }
        this.f3307b = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12877a(Animator animator) {
        if (animator != null) {
            animator.addListener(new C1030b(this));
        }
    }

    /* renamed from: a */
    public void mo12878a(Interpolator interpolator) {
        this.f3308c = interpolator;
    }

    /* renamed from: a */
    public void mo12879a(Animation.AnimationListener animationListener) {
        this.f3309d = animationListener;
    }

    /* renamed from: a */
    public void mo12880a(Marker marker, Animation animation) {
        this.f3306a = mo12873a(marker);
        mo12877a(this.f3306a);
    }

    /* renamed from: b */
    public void mo12881b() {
        Animator animator = this.f3306a;
        if (animator != null) {
            animator.cancel();
            this.f3306a = null;
        }
    }

    /* renamed from: b */
    public void mo12882b(int i) {
        if (i > 0 || i == -1) {
            this.f3311f = i;
        }
    }

    /* renamed from: c */
    public int mo12883c() {
        return this.f3310e;
    }

    /* renamed from: c */
    public void mo12884c(int i) {
    }
}
