package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class TrafficInfo implements Parcelable {
    public static final Parcelable.Creator<TrafficInfo> CREATOR = new C1144n();

    /* renamed from: a */
    private boolean f3720a;

    /* renamed from: b */
    private String f3721b;

    /* renamed from: c */
    private ArrayList<Integer> f3722c;

    public TrafficInfo() {
        this.f3720a = false;
        this.f3721b = null;
        this.f3720a = false;
        this.f3721b = null;
        this.f3722c = new ArrayList<>();
    }

    protected TrafficInfo(Parcel parcel) {
        boolean z = false;
        this.f3720a = false;
        this.f3721b = null;
        this.f3720a = parcel.readByte() != 0 ? true : z;
        this.f3721b = parcel.readString();
    }

    /* renamed from: a */
    public String mo13241a() {
        return this.f3721b;
    }

    /* renamed from: a */
    public void mo13242a(String str) {
        this.f3721b = str;
    }

    /* renamed from: a */
    public void mo13243a(ArrayList<Integer> arrayList) {
        this.f3722c = arrayList;
    }

    /* renamed from: a */
    public void mo13244a(boolean z) {
        this.f3720a = z;
    }

    /* renamed from: b */
    public ArrayList<Integer> mo13245b() {
        return this.f3722c;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.f3720a ? (byte) 1 : 0);
        parcel.writeString(this.f3721b);
    }
}
