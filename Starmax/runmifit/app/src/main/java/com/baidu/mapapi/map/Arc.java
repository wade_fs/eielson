package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;
import java.util.ArrayList;

public final class Arc extends Overlay {

    /* renamed from: f */
    private static final String f2364f = Arc.class.getSimpleName();

    /* renamed from: a */
    int f2365a;

    /* renamed from: b */
    int f2366b;

    /* renamed from: c */
    LatLng f2367c;

    /* renamed from: d */
    LatLng f2368d;

    /* renamed from: e */
    LatLng f2369e;

    Arc() {
        this.type = C1082h.arc;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        super.mo10900a(bundle);
        ArrayList arrayList = new ArrayList();
        arrayList.clear();
        arrayList.add(this.f2367c);
        arrayList.add(this.f2368d);
        arrayList.add(this.f2369e);
        GeoPoint ll2mc = CoordUtil.ll2mc((LatLng) arrayList.get(0));
        bundle.putDouble("location_x", ll2mc.getLongitudeE6());
        bundle.putDouble("location_y", ll2mc.getLatitudeE6());
        bundle.putInt("width", this.f2366b);
        Overlay.m2940a(arrayList, bundle);
        Overlay.m2939a(this.f2365a, bundle);
        return bundle;
    }

    public int getColor() {
        return this.f2365a;
    }

    public LatLng getEndPoint() {
        return this.f2369e;
    }

    public LatLng getMiddlePoint() {
        return this.f2368d;
    }

    public LatLng getStartPoint() {
        return this.f2367c;
    }

    public int getWidth() {
        return this.f2366b;
    }

    public void setColor(int i) {
        this.f2365a = i;
        this.listener.mo11330b(super);
    }

    public void setPoints(LatLng latLng, LatLng latLng2, LatLng latLng3) {
        if (latLng == null || latLng2 == null || latLng3 == null) {
            throw new IllegalArgumentException("BDMapSDKException:start and middle and end points can not be null");
        } else if (latLng == latLng2 || latLng == latLng3 || latLng2 == latLng3) {
            throw new IllegalArgumentException("BDMapSDKException: start and middle and end points can not be same");
        } else {
            this.f2367c = latLng;
            this.f2368d = latLng2;
            this.f2369e = latLng3;
            this.listener.mo11330b(super);
        }
    }

    public void setWidth(int i) {
        if (i > 0) {
            this.f2366b = i;
            this.listener.mo11330b(super);
        }
    }
}
