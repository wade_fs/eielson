package com.baidu.mapsdkplatform.comapi.synchronization.render;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;

public final class LinkPointPolyLineInfo implements Parcelable {
    public static final Parcelable.Creator<LinkPointPolyLineInfo> CREATOR = new C1145a();

    /* renamed from: a */
    private long f3780a;

    /* renamed from: b */
    private LatLng f3781b;

    /* renamed from: c */
    private LatLng f3782c;

    /* renamed from: d */
    private int f3783d;

    public LinkPointPolyLineInfo() {
        this.f3780a = 0;
        this.f3783d = 0;
        this.f3780a = 0;
        this.f3781b = null;
        this.f3782c = null;
        this.f3783d = 0;
    }

    protected LinkPointPolyLineInfo(Parcel parcel) {
        this.f3780a = 0;
        this.f3783d = 0;
        this.f3780a = parcel.readLong();
        this.f3781b = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        this.f3782c = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        this.f3783d = parcel.readInt();
    }

    /* renamed from: a */
    public LatLng mo13328a() {
        return this.f3781b;
    }

    /* renamed from: a */
    public void mo13329a(int i) {
        this.f3783d = i;
    }

    /* renamed from: a */
    public void mo13330a(LatLng latLng) {
        this.f3781b = latLng;
    }

    /* renamed from: b */
    public LatLng mo13331b() {
        return this.f3782c;
    }

    /* renamed from: b */
    public void mo13332b(LatLng latLng) {
        this.f3782c = latLng;
    }

    /* renamed from: c */
    public int mo13333c() {
        return this.f3783d;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f3780a);
        parcel.writeParcelable(this.f3781b, i);
        parcel.writeParcelable(this.f3782c, i);
        parcel.writeInt(this.f3783d);
    }
}
