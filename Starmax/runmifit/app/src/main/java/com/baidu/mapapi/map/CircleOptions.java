package com.baidu.mapapi.map;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import com.baidu.mapapi.model.LatLng;

public final class CircleOptions extends OverlayOptions {

    /* renamed from: d */
    private static final String f2445d = CircleOptions.class.getSimpleName();

    /* renamed from: a */
    int f2446a;

    /* renamed from: b */
    boolean f2447b = true;

    /* renamed from: c */
    Bundle f2448c;

    /* renamed from: e */
    private LatLng f2449e;

    /* renamed from: f */
    private int f2450f = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: g */
    private int f2451g;

    /* renamed from: h */
    private Stroke f2452h;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Overlay mo10909a() {
        Circle circle = new Circle();
        circle.f2662A = this.f2447b;
        circle.f2665z = this.f2446a;
        circle.f2663B = this.f2448c;
        circle.f2442b = this.f2450f;
        circle.f2441a = this.f2449e;
        circle.f2443c = this.f2451g;
        circle.f2444d = this.f2452h;
        return circle;
    }

    public CircleOptions center(LatLng latLng) {
        if (latLng != null) {
            this.f2449e = latLng;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: circle center can not be null");
    }

    public CircleOptions extraInfo(Bundle bundle) {
        this.f2448c = bundle;
        return this;
    }

    public CircleOptions fillColor(int i) {
        this.f2450f = i;
        return this;
    }

    public LatLng getCenter() {
        return this.f2449e;
    }

    public Bundle getExtraInfo() {
        return this.f2448c;
    }

    public int getFillColor() {
        return this.f2450f;
    }

    public int getRadius() {
        return this.f2451g;
    }

    public Stroke getStroke() {
        return this.f2452h;
    }

    public int getZIndex() {
        return this.f2446a;
    }

    public boolean isVisible() {
        return this.f2447b;
    }

    public CircleOptions radius(int i) {
        this.f2451g = i;
        return this;
    }

    public CircleOptions stroke(Stroke stroke) {
        this.f2452h = stroke;
        return this;
    }

    public CircleOptions visible(boolean z) {
        this.f2447b = z;
        return this;
    }

    public CircleOptions zIndex(int i) {
        this.f2446a = i;
        return this;
    }
}
