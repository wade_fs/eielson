package com.baidu.android.bbalbs.common.util;

import com.baidu.android.bbalbs.common.util.C0696b;

/* renamed from: com.baidu.android.bbalbs.common.util.d */
class C0701d implements Runnable {

    /* renamed from: a */
    final /* synthetic */ C0696b.C0698b f1066a;

    /* renamed from: b */
    final /* synthetic */ C0696b f1067b;

    C0701d(C0696b bVar, C0696b.C0698b bVar2) {
        this.f1067b = bVar;
        this.f1066a = bVar2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0098, code lost:
        if (r4 == null) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d3, code lost:
        if (r5 == null) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0059, code lost:
        if (r2 == null) goto L_0x002f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r8 = this;
            com.baidu.android.bbalbs.common.util.b$b r0 = new com.baidu.android.bbalbs.common.util.b$b
            r1 = 0
            r0.<init>(r1)
            java.lang.String r1 = "0"
            r0.f1063b = r1
            com.baidu.android.bbalbs.common.util.b$b r2 = r8.f1066a
            java.lang.String r2 = r2.f1062a
            r0.f1062a = r2
            java.io.File r2 = new java.io.File
            com.baidu.android.bbalbs.common.util.b r3 = r8.f1067b
            android.content.Context r3 = r3.f1055b
            java.io.File r3 = r3.getFilesDir()
            java.lang.String r4 = "libcuid.so"
            r2.<init>(r3, r4)
            java.lang.String r3 = r0.mo10192b()
            java.lang.String r3 = com.baidu.android.bbalbs.common.util.C0696b.m1585h(r3)
            boolean r4 = r2.exists()
            if (r4 != 0) goto L_0x0035
        L_0x002f:
            com.baidu.android.bbalbs.common.util.b r2 = r8.f1067b
            boolean unused = r2.m1579d(r3)
            goto L_0x005c
        L_0x0035:
            java.lang.String r2 = com.baidu.android.bbalbs.common.util.C0696b.m1566b(r2)
            java.lang.String r2 = com.baidu.android.bbalbs.common.util.C0696b.m1586i(r2)
            com.baidu.android.bbalbs.common.util.b$b r2 = com.baidu.android.bbalbs.common.util.C0696b.C0698b.m1589a(r2)
            if (r2 == 0) goto L_0x0059
            boolean r4 = r2.mo10191a()
            if (r4 != 0) goto L_0x0059
            r2.f1063b = r1
            com.baidu.android.bbalbs.common.util.b r4 = r8.f1067b
            java.lang.String r2 = r2.mo10192b()
            java.lang.String r2 = com.baidu.android.bbalbs.common.util.C0696b.m1585h(r2)
            boolean unused = r4.m1579d(r2)
            goto L_0x005c
        L_0x0059:
            if (r2 != 0) goto L_0x005c
            goto L_0x002f
        L_0x005c:
            com.baidu.android.bbalbs.common.util.b r2 = r8.f1067b
            boolean r2 = r2.m1575c()
            if (r2 == 0) goto L_0x009b
            com.baidu.android.bbalbs.common.util.b r4 = r8.f1067b
            java.lang.String r5 = "com.baidu.deviceid.v2"
            java.lang.String r4 = r4.m1582e(r5)
            boolean r6 = android.text.TextUtils.isEmpty(r4)
            if (r6 == 0) goto L_0x0078
        L_0x0072:
            com.baidu.android.bbalbs.common.util.b r4 = r8.f1067b
            boolean unused = r4.m1570b(r5, r3)
            goto L_0x009b
        L_0x0078:
            java.lang.String r4 = com.baidu.android.bbalbs.common.util.C0696b.m1586i(r4)
            com.baidu.android.bbalbs.common.util.b$b r4 = com.baidu.android.bbalbs.common.util.C0696b.C0698b.m1589a(r4)
            if (r4 == 0) goto L_0x0098
            boolean r6 = r4.mo10191a()
            if (r6 != 0) goto L_0x0098
            r4.f1063b = r1
            com.baidu.android.bbalbs.common.util.b r6 = r8.f1067b
            java.lang.String r4 = r4.mo10192b()
            java.lang.String r4 = com.baidu.android.bbalbs.common.util.C0696b.m1585h(r4)
            boolean unused = r6.m1570b(r5, r4)
            goto L_0x009b
        L_0x0098:
            if (r4 != 0) goto L_0x009b
            goto L_0x0072
        L_0x009b:
            com.baidu.android.bbalbs.common.util.b r4 = r8.f1067b
            java.lang.String r5 = "android.permission.WRITE_EXTERNAL_STORAGE"
            boolean r4 = r4.m1583f(r5)
            if (r4 == 0) goto L_0x00d6
            java.io.File r5 = new java.io.File
            java.io.File r6 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r7 = "backups/.SystemConfig/.cuid2"
            r5.<init>(r6, r7)
            boolean r5 = r5.exists()
            if (r5 != 0) goto L_0x00ba
        L_0x00b6:
            com.baidu.android.bbalbs.common.util.C0696b.m1587j(r3)
            goto L_0x00d6
        L_0x00ba:
            com.baidu.android.bbalbs.common.util.b r5 = r8.f1067b
            com.baidu.android.bbalbs.common.util.b$b r5 = r5.m1580e()
            if (r5 == 0) goto L_0x00d3
            boolean r6 = r5.mo10191a()
            if (r6 != 0) goto L_0x00d3
            r5.f1063b = r1
            java.lang.String r3 = r5.mo10192b()
            java.lang.String r3 = com.baidu.android.bbalbs.common.util.C0696b.m1585h(r3)
            goto L_0x00b6
        L_0x00d3:
            if (r5 != 0) goto L_0x00d6
            goto L_0x00b6
        L_0x00d6:
            if (r2 == 0) goto L_0x00f4
            com.baidu.android.bbalbs.common.util.b r2 = r8.f1067b
            java.lang.String r3 = "com.baidu.deviceid"
            java.lang.String r2 = r2.m1582e(r3)
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 == 0) goto L_0x00ed
            com.baidu.android.bbalbs.common.util.b r2 = r8.f1067b
            java.lang.String r5 = r0.f1062a
            boolean unused = r2.m1570b(r3, r5)
        L_0x00ed:
            com.baidu.android.bbalbs.common.util.b r2 = r8.f1067b
            java.lang.String r3 = "bd_setting_i"
            boolean unused = r2.m1570b(r3, r1)
        L_0x00f4:
            if (r4 == 0) goto L_0x012b
            java.io.File r2 = new java.io.File
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r4 = "backups/.SystemConfig/.cuid"
            r2.<init>(r3, r4)
            boolean r2 = r2.exists()
            if (r2 != 0) goto L_0x010f
        L_0x0107:
            java.lang.String r1 = r0.f1063b
            java.lang.String r0 = r0.f1062a
        L_0x010b:
            com.baidu.android.bbalbs.common.util.C0696b.m1574c(r1, r0)
            goto L_0x012b
        L_0x010f:
            com.baidu.android.bbalbs.common.util.b r2 = r8.f1067b
            java.lang.String r3 = ""
            java.lang.String r2 = r2.m1588k(r3)
            com.baidu.android.bbalbs.common.util.b r3 = r8.f1067b
            com.baidu.android.bbalbs.common.util.b$b r2 = r3.m1584g(r2)
            if (r2 == 0) goto L_0x0128
            boolean r3 = r2.mo10191a()
            if (r3 != 0) goto L_0x0128
            java.lang.String r0 = r2.f1062a
            goto L_0x010b
        L_0x0128:
            if (r2 != 0) goto L_0x012b
            goto L_0x0107
        L_0x012b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.bbalbs.common.util.C0701d.run():void");
    }
}
