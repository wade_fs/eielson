package com.baidu.mapapi.map;

import android.graphics.Point;

/* renamed from: com.baidu.mapapi.map.h */
class C0931h {

    /* renamed from: a */
    public final double f2842a;

    /* renamed from: b */
    public final double f2843b;

    /* renamed from: c */
    public final double f2844c;

    /* renamed from: d */
    public final double f2845d;

    /* renamed from: e */
    public final double f2846e;

    /* renamed from: f */
    public final double f2847f;

    public C0931h(double d, double d2, double d3, double d4) {
        this.f2842a = d;
        this.f2843b = d3;
        this.f2844c = d2;
        this.f2845d = d4;
        this.f2846e = (d + d2) / 2.0d;
        this.f2847f = (d3 + d4) / 2.0d;
    }

    /* renamed from: a */
    public boolean mo11565a(double d, double d2) {
        return this.f2842a <= d && d <= this.f2844c && this.f2843b <= d2 && d2 <= this.f2845d;
    }

    /* renamed from: a */
    public boolean mo11566a(double d, double d2, double d3, double d4) {
        return d < this.f2844c && this.f2842a < d2 && d3 < this.f2845d && this.f2843b < d4;
    }

    /* renamed from: a */
    public boolean mo11567a(Point point) {
        return mo11565a((double) point.x, (double) point.y);
    }

    /* renamed from: a */
    public boolean mo11568a(C0931h hVar) {
        return mo11566a(hVar.f2842a, hVar.f2844c, hVar.f2843b, hVar.f2845d);
    }

    /* renamed from: b */
    public boolean mo11569b(C0931h hVar) {
        return hVar.f2842a >= this.f2842a && hVar.f2844c <= this.f2844c && hVar.f2843b >= this.f2843b && hVar.f2845d <= this.f2845d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("minX: " + this.f2842a);
        sb.append(" minY: " + this.f2843b);
        sb.append(" maxX: " + this.f2844c);
        sb.append(" maxY: " + this.f2845d);
        sb.append(" midX: " + this.f2846e);
        sb.append(" midY: " + this.f2847f);
        return sb.toString();
    }
}
