package com.baidu.mapapi.map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.baidu.mapapi.map.WearMapView;

public class SwipeDismissView extends RelativeLayout {

    /* renamed from: a */
    WearMapView.OnDismissCallback f2718a = null;

    public SwipeDismissView(Context context, AttributeSet attributeSet, int i, View view) {
        super(context, attributeSet, i);
        mo11403a(context, view);
    }

    public SwipeDismissView(Context context, AttributeSet attributeSet, View view) {
        super(context, attributeSet);
        mo11403a(context, view);
    }

    public SwipeDismissView(Context context, View view) {
        super(context);
        mo11403a(context, view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo11403a(Context context, View view) {
        setOnTouchListener(new SwipeDismissTouchListener(view, new Object(), new C0944s(this)));
    }

    public void setCallback(WearMapView.OnDismissCallback onDismissCallback) {
        this.f2718a = onDismissCallback;
    }
}
