package com.baidu.location.p014b;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.baidu.location.C0839f;

/* renamed from: com.baidu.location.b.d */
public class C0775d {

    /* renamed from: d */
    private static C0775d f1504d;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public boolean f1505a = false;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public String f1506b = null;

    /* renamed from: c */
    private C0776a f1507c = null;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public int f1508e = -1;

    /* renamed from: com.baidu.location.b.d$a */
    public class C0776a extends BroadcastReceiver {
        public C0776a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.d.a(com.baidu.location.b.d, boolean):boolean
         arg types: [com.baidu.location.b.d, int]
         candidates:
          com.baidu.location.b.d.a(com.baidu.location.b.d, int):int
          com.baidu.location.b.d.a(com.baidu.location.b.d, java.lang.String):java.lang.String
          com.baidu.location.b.d.a(com.baidu.location.b.d, boolean):boolean */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x005d A[Catch:{ Exception -> 0x0075 }] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x006d A[Catch:{ Exception -> 0x0075 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onReceive(android.content.Context r6, android.content.Intent r7) {
            /*
                r5 = this;
                java.lang.String r6 = r7.getAction()
                r0 = 0
                java.lang.String r1 = "android.intent.action.BATTERY_CHANGED"
                boolean r6 = r6.equals(r1)     // Catch:{ Exception -> 0x0075 }
                if (r6 == 0) goto L_0x007a
                com.baidu.location.b.d r6 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                r1 = 0
                boolean unused = r6.f1505a = r1     // Catch:{ Exception -> 0x0075 }
                java.lang.String r6 = "status"
                int r6 = r7.getIntExtra(r6, r1)     // Catch:{ Exception -> 0x0075 }
                java.lang.String r2 = "plugged"
                int r1 = r7.getIntExtra(r2, r1)     // Catch:{ Exception -> 0x0075 }
                java.lang.String r2 = "level"
                r3 = -1
                int r2 = r7.getIntExtra(r2, r3)     // Catch:{ Exception -> 0x0075 }
                java.lang.String r4 = "scale"
                int r7 = r7.getIntExtra(r4, r3)     // Catch:{ Exception -> 0x0075 }
                if (r2 <= 0) goto L_0x0039
                if (r7 <= 0) goto L_0x0039
                com.baidu.location.b.d r3 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                int r2 = r2 * 100
                int r2 = r2 / r7
                int unused = r3.f1508e = r2     // Catch:{ Exception -> 0x0075 }
                goto L_0x003e
            L_0x0039:
                com.baidu.location.b.d r7 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                int unused = r7.f1508e = r3     // Catch:{ Exception -> 0x0075 }
            L_0x003e:
                r7 = 2
                if (r6 == r7) goto L_0x0055
                r2 = 3
                if (r6 == r2) goto L_0x004d
                r2 = 4
                if (r6 == r2) goto L_0x004d
                com.baidu.location.b.d r6 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                java.lang.String unused = r6.f1506b = r0     // Catch:{ Exception -> 0x0075 }
                goto L_0x005a
            L_0x004d:
                com.baidu.location.b.d r6 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                java.lang.String r2 = "3"
            L_0x0051:
                java.lang.String unused = r6.f1506b = r2     // Catch:{ Exception -> 0x0075 }
                goto L_0x005a
            L_0x0055:
                com.baidu.location.b.d r6 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                java.lang.String r2 = "4"
                goto L_0x0051
            L_0x005a:
                r6 = 1
                if (r1 == r6) goto L_0x006d
                if (r1 == r7) goto L_0x0060
                goto L_0x007a
            L_0x0060:
                com.baidu.location.b.d r7 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                java.lang.String r1 = "5"
                java.lang.String unused = r7.f1506b = r1     // Catch:{ Exception -> 0x0075 }
            L_0x0067:
                com.baidu.location.b.d r7 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                boolean unused = r7.f1505a = r6     // Catch:{ Exception -> 0x0075 }
                goto L_0x007a
            L_0x006d:
                com.baidu.location.b.d r7 = com.baidu.location.p014b.C0775d.this     // Catch:{ Exception -> 0x0075 }
                java.lang.String r1 = "6"
                java.lang.String unused = r7.f1506b = r1     // Catch:{ Exception -> 0x0075 }
                goto L_0x0067
            L_0x0075:
                com.baidu.location.b.d r6 = com.baidu.location.p014b.C0775d.this
                java.lang.String unused = r6.f1506b = r0
            L_0x007a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p014b.C0775d.C0776a.onReceive(android.content.Context, android.content.Intent):void");
        }
    }

    private C0775d() {
    }

    /* renamed from: a */
    public static synchronized C0775d m2005a() {
        C0775d dVar;
        synchronized (C0775d.class) {
            if (f1504d == null) {
                f1504d = new C0775d();
            }
            dVar = f1504d;
        }
        return dVar;
    }

    /* renamed from: b */
    public void mo10529b() {
        this.f1507c = new C0776a();
        try {
            C0839f.getServiceContext().registerReceiver(this.f1507c, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        } catch (Exception unused) {
        }
    }

    /* renamed from: c */
    public void mo10530c() {
        if (this.f1507c != null) {
            try {
                C0839f.getServiceContext().unregisterReceiver(this.f1507c);
            } catch (Exception unused) {
            }
        }
        this.f1507c = null;
    }

    /* renamed from: d */
    public String mo10531d() {
        return this.f1506b;
    }

    /* renamed from: e */
    public boolean mo10532e() {
        return this.f1505a;
    }

    /* renamed from: f */
    public int mo10533f() {
        return this.f1508e;
    }
}
