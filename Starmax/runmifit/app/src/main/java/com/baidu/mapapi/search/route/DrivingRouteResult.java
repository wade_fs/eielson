package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.core.TaxiInfo;
import java.util.ArrayList;
import java.util.List;

public final class DrivingRouteResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<DrivingRouteResult> CREATOR = new C0990f();

    /* renamed from: a */
    private List<DrivingRouteLine> f3047a;

    /* renamed from: b */
    private List<TaxiInfo> f3048b;

    /* renamed from: c */
    private TaxiInfo f3049c;

    /* renamed from: d */
    private SuggestAddrInfo f3050d;

    public DrivingRouteResult() {
    }

    protected DrivingRouteResult(Parcel parcel) {
        this.f3047a = new ArrayList();
        parcel.readTypedList(this.f3047a, DrivingRouteLine.CREATOR);
        this.f3048b = new ArrayList();
        parcel.readTypedList(this.f3048b, TaxiInfo.CREATOR);
        this.f3050d = (SuggestAddrInfo) parcel.readParcelable(SuggestAddrInfo.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public List<DrivingRouteLine> getRouteLines() {
        return this.f3047a;
    }

    public SuggestAddrInfo getSuggestAddrInfo() {
        return this.f3050d;
    }

    @Deprecated
    public TaxiInfo getTaxiInfo() {
        return this.f3049c;
    }

    public List<TaxiInfo> getTaxiInfos() {
        return this.f3048b;
    }

    public void setRouteLines(List<DrivingRouteLine> list) {
        this.f3047a = list;
    }

    public void setSuggestAddrInfo(SuggestAddrInfo suggestAddrInfo) {
        this.f3050d = suggestAddrInfo;
    }

    public void setTaxiInfos(List<TaxiInfo> list) {
        this.f3048b = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.f3047a);
        parcel.writeTypedList(this.f3048b);
        parcel.writeParcelable(this.f3050d, 1);
    }
}
