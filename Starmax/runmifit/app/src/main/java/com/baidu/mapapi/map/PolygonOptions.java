package com.baidu.mapapi.map;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import com.baidu.mapapi.model.LatLng;
import java.util.List;

public final class PolygonOptions extends OverlayOptions {

    /* renamed from: a */
    int f2669a;

    /* renamed from: b */
    boolean f2670b = true;

    /* renamed from: c */
    Bundle f2671c;

    /* renamed from: d */
    private Stroke f2672d;

    /* renamed from: e */
    private int f2673e = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: f */
    private List<LatLng> f2674f;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Overlay mo10909a() {
        Polygon polygon = new Polygon();
        polygon.f2662A = this.f2670b;
        polygon.f2665z = this.f2669a;
        polygon.f2663B = this.f2671c;
        List<LatLng> list = this.f2674f;
        if (list == null || list.size() < 2) {
            throw new IllegalStateException("BDMapSDKException: when you add polyline, you must at least supply 2 points");
        }
        polygon.f2668c = this.f2674f;
        polygon.f2667b = this.f2673e;
        polygon.f2666a = this.f2672d;
        return polygon;
    }

    public PolygonOptions extraInfo(Bundle bundle) {
        this.f2671c = bundle;
        return this;
    }

    public PolygonOptions fillColor(int i) {
        this.f2673e = i;
        return this;
    }

    public Bundle getExtraInfo() {
        return this.f2671c;
    }

    public int getFillColor() {
        return this.f2673e;
    }

    public List<LatLng> getPoints() {
        return this.f2674f;
    }

    public Stroke getStroke() {
        return this.f2672d;
    }

    public int getZIndex() {
        return this.f2669a;
    }

    public boolean isVisible() {
        return this.f2670b;
    }

    public PolygonOptions points(List<LatLng> list) {
        if (list == null) {
            throw new IllegalArgumentException("BDMapSDKException: points list can not be null");
        } else if (list.size() <= 2) {
            throw new IllegalArgumentException("BDMapSDKException: points count can not less than three");
        } else if (!list.contains(null)) {
            int i = 0;
            while (i < list.size()) {
                int i2 = i + 1;
                int i3 = i2;
                while (i3 < list.size()) {
                    if (list.get(i) != list.get(i3)) {
                        i3++;
                    } else {
                        throw new IllegalArgumentException("BDMapSDKException: points list can not has same points");
                    }
                }
                i = i2;
            }
            this.f2674f = list;
            return this;
        } else {
            throw new IllegalArgumentException("BDMapSDKException: points list can not contains null");
        }
    }

    public PolygonOptions stroke(Stroke stroke) {
        this.f2672d = stroke;
        return this;
    }

    public PolygonOptions visible(boolean z) {
        this.f2670b = z;
        return this;
    }

    public PolygonOptions zIndex(int i) {
        this.f2669a = i;
        return this;
    }
}
