package com.baidu.mapapi.synchronization;

import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;

public class SyncCoordinateConverter {

    /* renamed from: a */
    private LatLng f3195a;

    /* renamed from: b */
    private CoordType f3196b;

    public enum CoordType {
        COMMON,
        BD09LL
    }

    /* renamed from: a */
    private static LatLng m3253a(LatLng latLng) {
        return m3254a(latLng, CoordinateType.GCJ02);
    }

    /* renamed from: a */
    private static LatLng m3254a(LatLng latLng, String str) {
        if (latLng == null) {
            return null;
        }
        return CoordUtil.Coordinate_encryptEx((float) latLng.longitude, (float) latLng.latitude, str);
    }

    /* renamed from: b */
    private static LatLng m3255b(LatLng latLng) {
        if (latLng == null) {
            return null;
        }
        return CoordTrans.baiduToGcj(latLng);
    }

    public LatLng convert() {
        if (this.f3195a == null) {
            return null;
        }
        if (this.f3196b == null) {
            this.f3196b = CoordType.BD09LL;
        }
        int i = C1008a.f3200a[this.f3196b.ordinal()];
        if (i == 1) {
            return m3253a(this.f3195a);
        }
        if (i != 2) {
            return null;
        }
        return m3255b(this.f3195a);
    }

    public SyncCoordinateConverter coord(LatLng latLng) {
        this.f3195a = latLng;
        return this;
    }

    public SyncCoordinateConverter from(CoordType coordType) {
        this.f3196b = coordType;
        return this;
    }
}
