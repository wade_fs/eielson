package com.baidu.mapapi.map;

import android.animation.ValueAnimator;
import android.view.ViewGroup;

/* renamed from: com.baidu.mapapi.map.r */
class C0943r implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a */
    final /* synthetic */ ViewGroup.LayoutParams f2867a;

    /* renamed from: b */
    final /* synthetic */ SwipeDismissTouchListener f2868b;

    C0943r(SwipeDismissTouchListener swipeDismissTouchListener, ViewGroup.LayoutParams layoutParams) {
        this.f2868b = swipeDismissTouchListener;
        this.f2867a = layoutParams;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f2867a.height = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        this.f2868b.f2706e.setLayoutParams(this.f2867a);
    }
}
