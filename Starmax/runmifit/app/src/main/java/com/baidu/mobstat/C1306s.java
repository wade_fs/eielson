package com.baidu.mobstat;

import android.content.ContextWrapper;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

/* renamed from: com.baidu.mobstat.s */
class C1306s extends ContextWrapper {
    public C1306s() {
        super(null);
    }

    public File getDatabasePath(String str) {
        if (!"mounted".equals(C1267ba.m4678a())) {
            return null;
        }
        String str2 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "backups/system";
        File file = new File(str2);
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(str2 + File.separator + str);
        if (!file2.exists()) {
            try {
                file2.createNewFile();
            } catch (IOException e) {
                C1255as.m4626c().mo13944b(e);
            }
        }
        if (file2.exists()) {
            return file2;
        }
        return null;
    }

    public SQLiteDatabase openOrCreateDatabase(String str, int i, SQLiteDatabase.CursorFactory cursorFactory) {
        File databasePath = getDatabasePath(str);
        if (databasePath != null && databasePath.canWrite()) {
            return SQLiteDatabase.openOrCreateDatabase(databasePath, (SQLiteDatabase.CursorFactory) null);
        }
        throw new RuntimeException("db path is null or path can not be write");
    }

    public SQLiteDatabase openOrCreateDatabase(String str, int i, SQLiteDatabase.CursorFactory cursorFactory, DatabaseErrorHandler databaseErrorHandler) {
        File databasePath = getDatabasePath(str);
        if (databasePath != null && databasePath.canWrite()) {
            return SQLiteDatabase.openOrCreateDatabase(databasePath, (SQLiteDatabase.CursorFactory) null);
        }
        throw new RuntimeException("db path is null or path can not be write");
    }
}
