package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.RouteNode;
import com.baidu.mapapi.search.core.RouteStep;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import java.util.ArrayList;
import java.util.List;

public class BikingRouteLine extends RouteLine<BikingStep> implements Parcelable {
    public static final Parcelable.Creator<BikingRouteLine> CREATOR = new C0985a();

    public static class BikingStep extends RouteStep implements Parcelable {
        public static final Parcelable.Creator<BikingStep> CREATOR = new C0986b();

        /* renamed from: d */
        private int f3019d;

        /* renamed from: e */
        private RouteNode f3020e;

        /* renamed from: f */
        private RouteNode f3021f;

        /* renamed from: g */
        private String f3022g;

        /* renamed from: h */
        private String f3023h;

        /* renamed from: i */
        private String f3024i;

        /* renamed from: j */
        private String f3025j;

        /* renamed from: k */
        private String f3026k;

        public BikingStep() {
        }

        protected BikingStep(Parcel parcel) {
            super(parcel);
            this.f3019d = parcel.readInt();
            this.f3020e = (RouteNode) parcel.readParcelable(RouteNode.class.getClassLoader());
            this.f3021f = (RouteNode) parcel.readParcelable(RouteNode.class.getClassLoader());
            this.f3022g = parcel.readString();
            this.f3023h = parcel.readString();
            this.f3024i = parcel.readString();
            this.f3025j = parcel.readString();
            this.f3026k = parcel.readString();
        }

        /* renamed from: a */
        private List<LatLng> m3194a(String str) {
            if (!(str == null || str.length() == 0)) {
                ArrayList arrayList = new ArrayList();
                String[] split = str.split(";");
                if (!(split == null || split.length == 0)) {
                    for (String str2 : split) {
                        String[] split2 = str2.split(",");
                        if (split2 != null && split2.length >= 2) {
                            LatLng latLng = new LatLng(Double.valueOf(split2[1]).doubleValue(), Double.valueOf(split2[0]).doubleValue());
                            if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                                latLng = CoordTrans.baiduToGcj(latLng);
                            }
                            arrayList.add(latLng);
                        }
                    }
                    return arrayList;
                }
            }
            return null;
        }

        public int describeContents() {
            return 0;
        }

        public int getDirection() {
            return this.f3019d;
        }

        public RouteNode getEntrance() {
            return this.f3020e;
        }

        public String getEntranceInstructions() {
            return this.f3023h;
        }

        public RouteNode getExit() {
            return this.f3021f;
        }

        public String getExitInstructions() {
            return this.f3024i;
        }

        public String getInstructions() {
            return this.f3025j;
        }

        public String getTurnType() {
            return this.f3026k;
        }

        public List<LatLng> getWayPoints() {
            if (this.mWayPoints == null) {
                this.mWayPoints = m3194a(this.f3022g);
            }
            return this.mWayPoints;
        }

        public void setDirection(int i) {
            this.f3019d = i;
        }

        public void setEntrance(RouteNode routeNode) {
            this.f3020e = routeNode;
        }

        public void setEntranceInstructions(String str) {
            this.f3023h = str;
        }

        public void setExit(RouteNode routeNode) {
            this.f3021f = routeNode;
        }

        public void setExitInstructions(String str) {
            this.f3024i = str;
        }

        public void setInstructions(String str) {
            this.f3025j = str;
        }

        public void setPathString(String str) {
            this.f3022g = str;
        }

        public void setTurnType(String str) {
            this.f3026k = str;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, 1);
            parcel.writeInt(this.f3019d);
            parcel.writeParcelable(this.f3020e, 1);
            parcel.writeParcelable(this.f3021f, 1);
            parcel.writeString(this.f3022g);
            parcel.writeString(this.f3023h);
            parcel.writeString(this.f3024i);
            parcel.writeString(this.f3025j);
            parcel.writeString(this.f3026k);
        }
    }

    public BikingRouteLine() {
    }

    protected BikingRouteLine(Parcel parcel) {
        super(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public List<BikingStep> getAllStep() {
        return super.getAllStep();
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.setType(RouteLine.TYPE.BIKINGSTEP);
        super.writeToParcel(parcel, 1);
    }
}
