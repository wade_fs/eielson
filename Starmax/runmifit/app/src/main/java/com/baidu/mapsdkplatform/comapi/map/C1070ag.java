package com.baidu.mapsdkplatform.comapi.map;

import android.os.Bundle;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.ag */
public class C1070ag extends C1077d {

    /* renamed from: h */
    private static final String f3453h = C1070ag.class.getSimpleName();

    /* renamed from: e */
    Bundle f3454e;

    /* renamed from: f */
    String f3455f;

    /* renamed from: g */
    int f3456g;

    /* renamed from: a */
    public String mo12974a() {
        return this.f3455f;
    }

    /* renamed from: a */
    public void mo12975a(Bundle bundle) {
        this.f3454e = bundle;
    }

    /* renamed from: a */
    public void mo12976a(String str) {
        this.f3455f = str;
    }

    /* renamed from: b */
    public Bundle mo12977b() {
        return this.f3454e;
    }
}
