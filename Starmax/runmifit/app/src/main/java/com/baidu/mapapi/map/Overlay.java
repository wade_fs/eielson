package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;
import com.baidu.mobstat.Config;
import com.tencent.open.SocialConstants;
import java.util.List;

public abstract class Overlay {

    /* renamed from: A */
    boolean f2662A;

    /* renamed from: B */
    Bundle f2663B;
    protected C0921a listener;
    public C1082h type;

    /* renamed from: y */
    String f2664y = (System.currentTimeMillis() + "_" + hashCode());

    /* renamed from: z */
    int f2665z;

    /* renamed from: com.baidu.mapapi.map.Overlay$a */
    interface C0921a {
        /* renamed from: a */
        void mo11329a(Overlay overlay);

        /* renamed from: b */
        void mo11330b(Overlay overlay);
    }

    protected Overlay() {
    }

    /* renamed from: a */
    static void m2939a(int i, Bundle bundle) {
        Bundle bundle2 = new Bundle();
        bundle2.putFloat("red", ((float) ((i >> 16) & 255)) / 255.0f);
        bundle2.putFloat("green", ((float) ((i >> 8) & 255)) / 255.0f);
        bundle2.putFloat("blue", ((float) (i & 255)) / 255.0f);
        bundle2.putFloat("alpha", ((float) (i >>> 24)) / 255.0f);
        bundle.putBundle("color", bundle2);
    }

    /* renamed from: a */
    static void m2940a(List<LatLng> list, Bundle bundle) {
        int size = list.size();
        double[] dArr = new double[size];
        double[] dArr2 = new double[size];
        for (int i = 0; i < size; i++) {
            GeoPoint ll2mc = CoordUtil.ll2mc(list.get(i));
            dArr[i] = ll2mc.getLongitudeE6();
            dArr2[i] = ll2mc.getLatitudeE6();
        }
        bundle.putDoubleArray("x_array", dArr);
        bundle.putDoubleArray("y_array", dArr2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo11321a() {
        Bundle bundle = new Bundle();
        bundle.putString(Config.FEED_LIST_ITEM_CUSTOM_ID, this.f2664y);
        bundle.putInt(SocialConstants.PARAM_TYPE, this.type.ordinal());
        return bundle;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        bundle.putString(Config.FEED_LIST_ITEM_CUSTOM_ID, this.f2664y);
        bundle.putInt(SocialConstants.PARAM_TYPE, this.type.ordinal());
        bundle.putInt("visibility", this.f2662A ? 1 : 0);
        bundle.putInt("z_index", this.f2665z);
        return bundle;
    }

    public Bundle getExtraInfo() {
        return this.f2663B;
    }

    public int getZIndex() {
        return this.f2665z;
    }

    public boolean isVisible() {
        return this.f2662A;
    }

    public void remove() {
        this.listener.mo11329a(this);
    }

    public void setExtraInfo(Bundle bundle) {
        this.f2663B = bundle;
    }

    public void setVisible(boolean z) {
        this.f2662A = z;
        this.listener.mo11330b(this);
    }

    public void setZIndex(int i) {
        this.f2665z = i;
        this.listener.mo11330b(this);
    }
}
