package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.n */
final class C1144n implements Parcelable.Creator<TrafficInfo> {
    C1144n() {
    }

    /* renamed from: a */
    public TrafficInfo createFromParcel(Parcel parcel) {
        return new TrafficInfo(parcel);
    }

    /* renamed from: a */
    public TrafficInfo[] newArray(int i) {
        return new TrafficInfo[i];
    }
}
