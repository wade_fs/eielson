package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.map.Marker;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.h */
public class C1036h extends C1031c {

    /* renamed from: a */
    private Animator f3329a = null;

    /* renamed from: b */
    private long f3330b = 0;

    /* renamed from: c */
    private Interpolator f3331c = null;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Animation.AnimationListener f3332d = null;

    /* renamed from: e */
    private int f3333e = 1;

    /* renamed from: f */
    private int f3334f = 0;

    /* renamed from: g */
    private float[] f3335g;

    public C1036h(float... fArr) {
        this.f3335g = fArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ObjectAnimator mo12900a(Marker marker) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(marker, "scale", this.f3335g);
        if (ofFloat != null) {
            ofFloat.setRepeatCount(this.f3334f);
            ofFloat.setRepeatMode(mo12901c());
            ofFloat.setDuration(this.f3330b);
            Interpolator interpolator = this.f3331c;
            if (interpolator != null) {
                ofFloat.setInterpolator(interpolator);
            }
        }
        return ofFloat;
    }

    /* renamed from: a */
    public void mo12874a() {
        Animator animator = this.f3329a;
        if (animator != null) {
            animator.start();
        }
    }

    /* renamed from: a */
    public void mo12875a(int i) {
        this.f3333e = i;
    }

    /* renamed from: a */
    public void mo12876a(long j) {
        if (j < 0) {
            j = 0;
        }
        this.f3330b = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12877a(Animator animator) {
        if (animator != null) {
            animator.addListener(new C1037i(this));
        }
    }

    /* renamed from: a */
    public void mo12878a(Interpolator interpolator) {
        this.f3331c = interpolator;
    }

    /* renamed from: a */
    public void mo12879a(Animation.AnimationListener animationListener) {
        this.f3332d = animationListener;
    }

    /* renamed from: a */
    public void mo12880a(Marker marker, Animation animation) {
        this.f3329a = mo12900a(marker);
        mo12877a(this.f3329a);
    }

    /* renamed from: b */
    public void mo12881b() {
        Animator animator = this.f3329a;
        if (animator != null) {
            animator.cancel();
            this.f3329a = null;
        }
    }

    /* renamed from: b */
    public void mo12882b(int i) {
        if (i > 0 || i == -1) {
            this.f3334f = i;
        }
    }

    /* renamed from: c */
    public int mo12901c() {
        return this.f3333e;
    }

    /* renamed from: c */
    public void mo12884c(int i) {
    }
}
