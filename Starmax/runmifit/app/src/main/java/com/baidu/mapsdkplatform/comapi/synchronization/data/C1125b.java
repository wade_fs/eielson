package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.b */
final class C1125b implements Parcelable.Creator<RouteLineInfo> {
    C1125b() {
    }

    /* renamed from: a */
    public RouteLineInfo createFromParcel(Parcel parcel) {
        return new RouteLineInfo(parcel);
    }

    /* renamed from: a */
    public RouteLineInfo[] newArray(int i) {
        return new RouteLineInfo[i];
    }
}
