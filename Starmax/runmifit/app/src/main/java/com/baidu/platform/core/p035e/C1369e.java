package com.baidu.platform.core.p035e;

import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.inner.Point;
import com.baidu.mapapi.search.share.RouteShareURLOption;
import com.baidu.mapsdkplatform.comjni.util.AppMD5;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;

/* renamed from: com.baidu.platform.core.e.e */
public class C1369e extends C1320e {
    public C1369e(RouteShareURLOption routeShareURLOption) {
        m5116a(routeShareURLOption);
    }

    /* renamed from: a */
    private int m5115a(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    /* renamed from: a */
    private void m5116a(RouteShareURLOption routeShareURLOption) {
        String str;
        String str2;
        C1381a aVar = new C1381a();
        Point ll2point = CoordUtil.ll2point(routeShareURLOption.mFrom.getLocation());
        Point ll2point2 = CoordUtil.ll2point(routeShareURLOption.mTo.getLocation());
        String str3 = "2$$$$$$";
        if (ll2point != null) {
            str = "1$$$$" + ll2point.f2890x + "," + ll2point.f2891y + "$$";
        } else {
            str = str3;
        }
        String name = routeShareURLOption.mFrom.getName();
        String str4 = "";
        if (name == null || name.equals(str4)) {
            name = "起点";
        }
        String str5 = str + name + "$$0$$$$";
        if (ll2point2 != null) {
            str3 = "1$$$$" + ll2point2.f2890x + "," + ll2point2.f2891y + "$$";
        }
        String name2 = routeShareURLOption.mTo.getName();
        if (name2 == null || name2.equals(str4)) {
            name2 = "终点";
        }
        String str6 = str3 + name2 + "$$0$$$$";
        int ordinal = routeShareURLOption.mMode.ordinal();
        if (ordinal == 0) {
            aVar.mo14094a(Config.STAT_SDK_CHANNEL, m5115a(routeShareURLOption.mFrom.getCity()) + str4);
            aVar.mo14094a("ec", m5115a(routeShareURLOption.mTo.getCity()) + str4);
            str4 = "&sharecallbackflag=carRoute";
            str2 = "nav";
        } else if (ordinal == 1) {
            aVar.mo14094a(Config.STAT_SDK_CHANNEL, m5115a(routeShareURLOption.mFrom.getCity()) + str4);
            aVar.mo14094a("ec", m5115a(routeShareURLOption.mTo.getCity()) + str4);
            str4 = "&sharecallbackflag=footRoute";
            str2 = "walk";
        } else if (ordinal == 2) {
            aVar.mo14094a(Config.STAT_SDK_CHANNEL, m5115a(routeShareURLOption.mFrom.getCity()) + str4);
            aVar.mo14094a("ec", m5115a(routeShareURLOption.mTo.getCity()) + str4);
            str4 = "&sharecallbackflag=cycleRoute";
            str2 = "cycle";
        } else if (ordinal != 3) {
            str2 = str4;
        } else {
            String str7 = "&i=" + routeShareURLOption.mPn + ",1,1&sharecallbackflag=busRoute";
            aVar.mo14094a("c", routeShareURLOption.mCityCode + str4);
            str2 = "bt";
            str4 = str7;
        }
        aVar.mo14094a("sn", str5);
        aVar.mo14094a("en", str6);
        String str8 = "&" + aVar.mo14095a() + ("&start=" + name + "&end=" + name2);
        this.f4435a.mo14094a("url", "http://map.baidu.com/?newmap=1&s=" + str2 + (AppMD5.encodeUrlParamsValue(str8) + str4));
        this.f4435a.mo14094a("from", "android_map_sdk");
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14093r();
    }
}
