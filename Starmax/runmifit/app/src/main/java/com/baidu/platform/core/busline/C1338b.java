package com.baidu.platform.core.busline;

import com.baidu.mapapi.search.busline.BusLineSearchOption;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;

/* renamed from: com.baidu.platform.core.busline.b */
public class C1338b extends C1320e {
    public C1338b(BusLineSearchOption busLineSearchOption) {
        m4979a(busLineSearchOption);
    }

    /* renamed from: a */
    private void m4979a(BusLineSearchOption busLineSearchOption) {
        this.f4435a.mo14094a("qt", "bsl");
        this.f4435a.mo14094a("rt_info", "1");
        this.f4435a.mo14094a("ie", "utf-8");
        this.f4435a.mo14094a("oue", "0");
        this.f4435a.mo14094a("c", busLineSearchOption.mCity);
        this.f4435a.mo14094a(Config.CUSTOM_USER_ID, busLineSearchOption.mUid);
        C1381a aVar = this.f4435a;
        aVar.mo14094a("t", System.currentTimeMillis() + "");
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14088m();
    }
}
