package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.k */
class C1039k implements Animator.AnimatorListener {

    /* renamed from: a */
    final /* synthetic */ C1038j f3345a;

    C1039k(C1038j jVar) {
        this.f3345a = jVar;
    }

    public void onAnimationCancel(Animator animator) {
        if (this.f3345a.f3340d != null) {
            this.f3345a.f3340d.onAnimationCancel();
        }
    }

    public void onAnimationEnd(Animator animator) {
        if (this.f3345a.f3340d != null) {
            this.f3345a.f3340d.onAnimationEnd();
        }
    }

    public void onAnimationRepeat(Animator animator) {
        if (this.f3345a.f3340d != null) {
            this.f3345a.f3340d.onAnimationRepeat();
        }
    }

    public void onAnimationStart(Animator animator) {
        if (this.f3345a.f3340d != null) {
            this.f3345a.f3340d.onAnimationStart();
        }
    }
}
