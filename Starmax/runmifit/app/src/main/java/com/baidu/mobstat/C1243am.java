package com.baidu.mobstat;

import android.app.Activity;
import android.os.Bundle;
import com.baidu.mobstat.ActivityLifeObserver;
import org.json.JSONArray;

/* renamed from: com.baidu.mobstat.am */
public class C1243am {

    /* renamed from: a */
    private static volatile boolean f4283a = true;

    /* renamed from: b */
    private static volatile boolean f4284b = false;

    /* renamed from: com.baidu.mobstat.am$a */
    public static class C1244a implements ActivityLifeObserver.IActivityLifeCallback {
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    /* renamed from: a */
    public static JSONArray m4583a() {
        return new JSONArray();
    }
}
