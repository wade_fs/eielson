package com.baidu.platform.base;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.baidu.mapapi.http.AsyncHttpClient;
import com.baidu.mapapi.http.HttpClient;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.district.DistrictResult;
import com.baidu.mapsdkplatform.comapi.util.PermissionCheck;
import com.baidu.platform.core.p031a.C1327b;
import com.baidu.platform.core.p031a.C1328c;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.base.a */
public abstract class C1316a {

    /* renamed from: a */
    protected final Lock f4422a = new ReentrantLock();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public AsyncHttpClient f4423b = new AsyncHttpClient();

    /* renamed from: c */
    private Handler f4424c = new Handler(Looper.getMainLooper());

    /* renamed from: d */
    private boolean f4425d = true;

    /* renamed from: e */
    private DistrictResult f4426e = null;

    /* renamed from: a */
    private void m4894a(AsyncHttpClient asyncHttpClient, HttpClient.ProtoResultCallback protoResultCallback, SearchResult searchResult) {
        asyncHttpClient.get(new C1328c(((DistrictResult) searchResult).getCityName()).mo14023a(), protoResultCallback);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m4895a(HttpClient.HttpStateError httpStateError, C1319d dVar, Object obj) {
        m4896a(dVar.mo14018a("{SDK_InnerError:{httpStateError:" + httpStateError + "}}"), obj, dVar);
    }

    /* renamed from: a */
    private void m4896a(SearchResult searchResult, Object obj, C1319d dVar) {
        this.f4424c.post(new C1318c(this, dVar, searchResult, obj));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m4900a(String str) {
        if (!m4903b(str)) {
            Log.e("BaseSearch", "Permission check unfinished, try again");
            int permissionCheck = PermissionCheck.permissionCheck();
            if (permissionCheck != 0) {
                Log.e("BaseSearch", "The authorized result is: " + permissionCheck);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m4901a(String str, C1319d dVar, Object obj, AsyncHttpClient asyncHttpClient, HttpClient.ProtoResultCallback protoResultCallback) {
        SearchResult a = dVar.mo14018a(str);
        a.status = m4904c(str);
        if (m4902a(dVar, a)) {
            m4894a(asyncHttpClient, protoResultCallback, a);
        } else if (dVar instanceof C1327b) {
            DistrictResult districtResult = this.f4426e;
            if (districtResult != null) {
                DistrictResult districtResult2 = (DistrictResult) a;
                districtResult2.setCityCode(districtResult.getCityCode());
                districtResult2.setCenterPt(this.f4426e.getCenterPt());
            }
            m4896a(a, obj, dVar);
            this.f4425d = true;
            this.f4426e = null;
            ((C1327b) dVar).mo14034a(false);
        } else {
            m4896a(a, obj, dVar);
        }
    }

    /* renamed from: a */
    private boolean m4902a(C1319d dVar, SearchResult searchResult) {
        if (!(dVar instanceof C1327b)) {
            return false;
        }
        DistrictResult districtResult = (DistrictResult) searchResult;
        if (SearchResult.ERRORNO.RESULT_NOT_FOUND != districtResult.error || districtResult.getCityName() == null || !this.f4425d) {
            return false;
        }
        this.f4425d = false;
        this.f4426e = districtResult;
        ((C1327b) dVar).mo14034a(true);
        return true;
    }

    /* renamed from: b */
    private boolean m4903b(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has("SDK_InnerError") || !jSONObject.optJSONObject("SDK_InnerError").has("PermissionCheckError")) {
                return true;
            }
            Log.e("BaseSearch", "Permission check unfinished");
            return false;
        } catch (JSONException unused) {
            Log.e("BaseSearch", "Create JSONObject failed");
            return false;
        }
    }

    /* renamed from: c */
    private int m4904c(String str) {
        JSONObject optJSONObject;
        if (str != null && !str.equals("")) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
                    return jSONObject.getInt(NotificationCompat.CATEGORY_STATUS);
                }
                if (jSONObject.has("status_sp")) {
                    return jSONObject.getInt("status_sp");
                }
                if (!jSONObject.has("result") || (optJSONObject = jSONObject.optJSONObject("result")) == null) {
                    return 10204;
                }
                return optJSONObject.optInt("error");
            } catch (JSONException unused) {
                Log.e("BaseSearch", "Create JSONObject failed when get response result status");
            }
        }
        return 10204;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo14016a(C1320e eVar, Object obj, C1319d dVar) {
        if (dVar == null) {
            Log.e(C1316a.class.getSimpleName(), "The SearchParser is null, must be applied.");
            return false;
        }
        String a = eVar.mo14023a();
        if (a == null) {
            Log.e("BaseSearch", "The sendurl is: " + a);
            m4896a(dVar.mo14018a("{SDK_InnerError:{PermissionCheckError:Error}}"), obj, dVar);
            return false;
        }
        this.f4423b.get(a, new C1317b(this, dVar, obj));
        return true;
    }
}
