package com.baidu.mapapi.common;

import com.baidu.mapsdkplatform.comapi.util.C1175i;

public class SysOSUtil {
    public static float getDensity() {
        return C1175i.f3913c;
    }

    public static int getDensityDpi() {
        return C1175i.m4281l();
    }

    public static String getDeviceID() {
        return C1175i.m4285p();
    }

    public static String getModuleFileName() {
        return C1175i.m4284o();
    }

    public static String getPhoneType() {
        return C1175i.m4276g();
    }

    public static int getScreenSizeX() {
        return C1175i.m4277h();
    }

    public static int getScreenSizeY() {
        return C1175i.m4279j();
    }
}
