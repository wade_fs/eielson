package com.baidu.mapapi.map;

import android.view.View;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;

/* renamed from: com.baidu.mapapi.map.v */
class C0947v implements View.OnClickListener {

    /* renamed from: a */
    final /* synthetic */ TextureMapView f2872a;

    C0947v(TextureMapView textureMapView) {
        this.f2872a = textureMapView;
    }

    public void onClick(View view) {
        float f = this.f2872a.f2752b.mo12954b().f3505a;
        C1064ab E = this.f2872a.f2752b.mo12954b().mo12989E();
        E.f3414a += 1.0f;
        if (E.f3414a <= f) {
            f = E.f3414a;
        }
        E.f3414a = f;
        BaiduMap.mapStatusReason |= 16;
        this.f2872a.f2752b.mo12954b().mo13016a(E, 300);
    }
}
