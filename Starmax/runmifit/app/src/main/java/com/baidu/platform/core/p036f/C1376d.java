package com.baidu.platform.core.p036f;

import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;
import com.tamic.novate.download.MimeType;

/* renamed from: com.baidu.platform.core.f.d */
public class C1376d extends C1320e {
    public C1376d(SuggestionSearchOption suggestionSearchOption) {
        m5137a(suggestionSearchOption);
    }

    /* renamed from: a */
    private void m5137a(SuggestionSearchOption suggestionSearchOption) {
        C1381a aVar;
        String str;
        this.f4435a.mo14094a("q", suggestionSearchOption.mKeyword);
        this.f4435a.mo14094a("region", suggestionSearchOption.mCity);
        if (suggestionSearchOption.mLocation != null) {
            LatLng latLng = new LatLng(suggestionSearchOption.mLocation.latitude, suggestionSearchOption.mLocation.longitude);
            if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                latLng = CoordTrans.gcjToBaidu(latLng);
            }
            C1381a aVar2 = this.f4435a;
            aVar2.mo14094a("location", latLng.latitude + "," + latLng.longitude);
        }
        if (suggestionSearchOption.mCityLimit.booleanValue()) {
            aVar = this.f4435a;
            str = "true";
        } else {
            aVar = this.f4435a;
            str = "false";
        }
        aVar.mo14094a("city_limit", str);
        this.f4435a.mo14094a("from", "android_map_sdk");
        this.f4435a.mo14094a("output", MimeType.JSON);
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14079d();
    }
}
