package com.baidu.mobstat;

import android.content.Context;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.ag */
public class C1237ag {

    /* renamed from: a */
    public static final C1237ag f4266a = new C1237ag();

    /* renamed from: a */
    public void mo13909a(Context context, JSONObject jSONObject) {
        C1255as.m4626c().mo13941a("startDataAnynalyzed start");
        m4555a(jSONObject);
        C1236af a = C1236af.m4534a(context);
        boolean a2 = a.mo13892a();
        C1255as c = C1255as.m4626c();
        c.mo13941a("is data collect closed:" + a2);
        if (!a2) {
            if (!C1300r.AP_LIST.mo13995b(10000)) {
                m4558c(context);
            }
            if (!C1300r.APP_LIST.mo13995b(10000)) {
                m4559d(context);
            }
            if (!C1300r.APP_TRACE.mo13995b(10000)) {
                m4560e(context);
            }
            if (C1239ai.f4274e && !C1300r.APP_APK.mo13995b(10000)) {
                m4561f(context);
            }
            boolean p = C1276bh.m4743p(context);
            if (p && a.mo13906l()) {
                C1255as.m4626c().mo13941a("sendLog");
                m4562g(context);
            } else if (!p) {
                C1255as.m4626c().mo13941a("isWifiAvailable = false, will not sendLog");
            } else {
                C1255as.m4626c().mo13941a("can not sendLog due to time stratergy");
            }
        }
        C1255as.m4626c().mo13941a("startDataAnynalyzed finished");
    }

    /* renamed from: a */
    private void m4555a(JSONObject jSONObject) {
        C1240aj ajVar = new C1240aj(jSONObject);
        C1239ai.f4271b = ajVar.f4275a;
        C1239ai.f4272c = ajVar.f4276b;
        C1239ai.f4273d = ajVar.f4277c;
    }

    /* renamed from: c */
    private void m4558c(Context context) {
        C1255as.m4626c().mo13941a("collectAPWithStretegy 1");
        C1236af a = C1236af.m4534a(context);
        long a2 = a.mo13889a(C1296n.AP_LIST);
        long currentTimeMillis = System.currentTimeMillis();
        long e = a.mo13899e();
        C1255as c = C1255as.m4626c();
        c.mo13941a("now time: " + currentTimeMillis + ": last time: " + a2 + "; time interval: " + e);
        if (a2 == 0 || currentTimeMillis - a2 > e) {
            C1255as.m4626c().mo13941a("collectAPWithStretegy 2");
            C1290j.m4800a(context);
        }
    }

    /* renamed from: d */
    private void m4559d(Context context) {
        C1255as.m4626c().mo13941a("collectAPPListWithStretegy 1");
        long currentTimeMillis = System.currentTimeMillis();
        C1236af a = C1236af.m4534a(context);
        long a2 = a.mo13889a(C1296n.APP_USER_LIST);
        long f = a.mo13900f();
        C1255as c = C1255as.m4626c();
        c.mo13941a("now time: " + currentTimeMillis + ": last time: " + a2 + "; userInterval : " + f);
        if (a2 == 0 || currentTimeMillis - a2 > f || !a.mo13893a(a2)) {
            C1255as.m4626c().mo13941a("collectUserAPPListWithStretegy 2");
            C1290j.m4801a(context, false);
        }
        long a3 = a.mo13889a(C1296n.APP_SYS_LIST);
        long g = a.mo13901g();
        C1255as c2 = C1255as.m4626c();
        c2.mo13941a("now time: " + currentTimeMillis + ": last time: " + a3 + "; sysInterval : " + g);
        if (a3 == 0 || currentTimeMillis - a3 > g) {
            C1255as.m4626c().mo13941a("collectSysAPPListWithStretegy 2");
            C1290j.m4801a(context, true);
        }
    }

    /* renamed from: e */
    private void m4560e(Context context) {
        C1255as.m4626c().mo13941a("collectAPPTraceWithStretegy 1");
        long currentTimeMillis = System.currentTimeMillis();
        C1236af a = C1236af.m4534a(context);
        long a2 = a.mo13889a(C1296n.APP_TRACE_HIS);
        long i = a.mo13903i();
        C1255as c = C1255as.m4626c();
        c.mo13941a("now time: " + currentTimeMillis + ": last time: " + a2 + "; time interval: " + i);
        if (a2 == 0 || currentTimeMillis - a2 > i) {
            C1255as.m4626c().mo13941a("collectAPPTraceWithStretegy 2");
            C1290j.m4803b(context, false);
        }
    }

    /* renamed from: f */
    private void m4561f(Context context) {
        C1255as.m4626c().mo13941a("collectAPKWithStretegy 1");
        long currentTimeMillis = System.currentTimeMillis();
        C1236af a = C1236af.m4534a(context);
        long a2 = a.mo13889a(C1296n.APP_APK);
        long h = a.mo13902h();
        C1255as c = C1255as.m4626c();
        c.mo13941a("now time: " + currentTimeMillis + ": last time: " + a2 + "; interval : " + h);
        if (a2 == 0 || currentTimeMillis - a2 > h) {
            C1255as.m4626c().mo13941a("collectAPKWithStretegy 2");
            C1290j.m4802b(context);
        }
    }

    /* renamed from: a */
    public void mo13908a(Context context, String str) {
        C1236af.m4534a(context).mo13891a(str);
    }

    /* renamed from: b */
    public void mo13911b(Context context, String str) {
        C1236af.m4534a(context).mo13894b(str);
    }

    /* renamed from: a */
    public void mo13907a(Context context, long j) {
        C1236af.m4534a(context).mo13890a(C1296n.LAST_UPDATE, j);
    }

    /* renamed from: g */
    private void m4562g(Context context) {
        C1236af.m4534a(context).mo13890a(C1296n.LAST_SEND, System.currentTimeMillis());
        JSONObject a = C1297o.m4826a(context);
        C1255as c = C1255as.m4626c();
        c.mo13941a("header: " + a);
        int i = 0;
        while (m4556a()) {
            int i2 = i + 1;
            if (i > 0) {
                C1297o.m4829c(a);
            }
            m4557b(context, a);
            i = i2;
        }
    }

    /* renamed from: a */
    private boolean m4556a() {
        if (C1300r.AP_LIST.mo13994b() && C1300r.APP_LIST.mo13994b() && C1300r.APP_TRACE.mo13994b() && C1300r.APP_CHANGE.mo13994b() && C1300r.APP_APK.mo13994b()) {
            return false;
        }
        return true;
    }

    /* renamed from: b */
    private void m4557b(Context context, JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        int i = 0;
        try {
            jSONObject2.put(Config.HEADER_PART, jSONObject);
            i = 0 + jSONObject.toString().length();
        } catch (JSONException e) {
            C1255as.m4626c().mo13942a(e);
        }
        C1255as.m4626c().mo13941a("APP_MEM");
        if (!C1236af.m4534a(context).mo13895b()) {
            String v = C1276bh.m4749v(context);
            JSONArray jSONArray = new JSONArray();
            C1255as.m4626c().mo13941a(v);
            jSONArray.put(v);
            if (jSONArray.length() > 0) {
                try {
                    jSONObject2.put("app_mem3", jSONArray);
                    i += jSONArray.toString().length();
                } catch (JSONException e2) {
                    C1255as.m4626c().mo13942a(e2);
                }
            }
        }
        C1255as.m4626c().mo13941a("APP_APK");
        List<String> a = C1300r.APP_APK.mo13993a(20480);
        JSONArray jSONArray2 = new JSONArray();
        for (String str : a) {
            C1255as.m4626c().mo13941a(str);
            jSONArray2.put(str);
        }
        if (jSONArray2.length() > 0) {
            try {
                jSONObject2.put("app_apk3", jSONArray2);
                i += jSONArray2.toString().length();
            } catch (JSONException e3) {
                C1255as.m4626c().mo13942a(e3);
            }
        }
        C1255as.m4626c().mo13941a("APP_CHANGE");
        List<String> a2 = C1300r.APP_CHANGE.mo13993a(10240);
        JSONArray jSONArray3 = new JSONArray();
        for (String str2 : a2) {
            C1255as.m4626c().mo13941a(str2);
            jSONArray3.put(str2);
        }
        if (jSONArray3.length() > 0) {
            try {
                jSONObject2.put("app_change3", jSONArray3);
                i += jSONArray3.toString().length();
            } catch (JSONException e4) {
                C1255as.m4626c().mo13942a(e4);
            }
        }
        C1255as.m4626c().mo13941a("APP_TRACE");
        List<String> a3 = C1300r.APP_TRACE.mo13993a(15360);
        JSONArray jSONArray4 = new JSONArray();
        for (String str3 : a3) {
            C1255as.m4626c().mo13941a(str3);
            jSONArray4.put(str3);
        }
        if (jSONArray4.length() > 0) {
            try {
                jSONObject2.put("app_trace3", jSONArray4);
                i += jSONArray4.toString().length();
            } catch (JSONException e5) {
                C1255as.m4626c().mo13942a(e5);
            }
        }
        C1255as.m4626c().mo13941a("APP_LIST");
        List<String> a4 = C1300r.APP_LIST.mo13993a(46080);
        JSONArray jSONArray5 = new JSONArray();
        for (String str4 : a4) {
            C1255as.m4626c().mo13941a(str4);
            jSONArray5.put(str4);
        }
        if (jSONArray5.length() > 0) {
            try {
                jSONObject2.put("app_list3", jSONArray5);
                i += jSONArray5.toString().length();
            } catch (JSONException e6) {
                C1255as.m4626c().mo13942a(e6);
            }
        }
        C1255as.m4626c().mo13941a("AP_LIST");
        List<String> a5 = C1300r.AP_LIST.mo13993a(Config.MAX_CACHE_JSON_CAPACITY - i);
        JSONArray jSONArray6 = new JSONArray();
        for (String str5 : a5) {
            C1255as.m4626c().mo13941a(str5);
            jSONArray6.put(str5);
        }
        if (jSONArray6.length() > 0) {
            try {
                jSONObject2.put("ap_list3", jSONArray6);
                i += jSONArray6.toString().length();
            } catch (JSONException e7) {
                C1255as.m4626c().mo13942a(e7);
            }
        }
        C1255as c = C1255as.m4626c();
        c.mo13941a("log in bytes is almost :" + i);
        JSONArray jSONArray7 = new JSONArray();
        jSONArray7.put(jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        try {
            jSONObject3.put("payload", jSONArray7);
            C1313z.m4882a().mo14012a(context, jSONObject3.toString());
        } catch (Exception e8) {
            C1255as.m4626c().mo13942a(e8);
        }
    }

    /* renamed from: a */
    public boolean mo13910a(Context context) {
        C1236af a = C1236af.m4534a(context);
        long a2 = a.mo13889a(C1296n.LAST_UPDATE);
        long c = a.mo13896c();
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - a2 > c) {
            C1255as c2 = C1255as.m4626c();
            c2.mo13941a("need to update, checkWithLastUpdateTime lastUpdateTime =" + a2 + "nowTime=" + currentTimeMillis + ";timeInteveral=" + c);
            return true;
        }
        C1255as c3 = C1255as.m4626c();
        c3.mo13941a("no need to update, checkWithLastUpdateTime lastUpdateTime =" + a2 + "nowTime=" + currentTimeMillis + ";timeInteveral=" + c);
        return false;
    }

    /* renamed from: b */
    public boolean mo13912b(Context context) {
        return !C1236af.m4534a(context).mo13892a() || mo13910a(context);
    }
}
