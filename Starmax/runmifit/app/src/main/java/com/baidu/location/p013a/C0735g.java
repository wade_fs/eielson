package com.baidu.location.p013a;

import com.baidu.location.p019g.C0854j;
import com.baidu.location.p019g.C0855k;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/* renamed from: com.baidu.location.a.g */
public class C0735g {

    /* renamed from: a */
    private static C0735g f1296a = null;

    /* renamed from: b */
    private static String f1297b = "Temp_in.dat";

    /* renamed from: c */
    private static File f1298c = new File(C0854j.f1898a, f1297b);

    /* renamed from: d */
    private static StringBuffer f1299d = null;

    /* renamed from: e */
    private static boolean f1300e = true;

    /* renamed from: f */
    private static int f1301f = 0;

    /* renamed from: g */
    private static int f1302g = 0;

    /* renamed from: h */
    private static long f1303h = 0;

    /* renamed from: i */
    private static long f1304i = 0;

    /* renamed from: j */
    private static long f1305j = 0;

    /* renamed from: k */
    private static double f1306k = 0.0d;

    /* renamed from: l */
    private static double f1307l = 0.0d;

    /* renamed from: m */
    private static int f1308m = 0;

    /* renamed from: n */
    private static int f1309n = 0;

    /* renamed from: o */
    private static int f1310o = 0;

    /* renamed from: a */
    public static String m1814a() {
        File file = f1298c;
        if (file == null || !file.exists()) {
            return null;
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(f1298c, "rw");
            randomAccessFile.seek(0);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            int readInt3 = randomAccessFile.readInt();
            if (!m1815a(readInt, readInt2, readInt3)) {
                randomAccessFile.close();
                m1817c();
                return null;
            }
            if (readInt2 != 0) {
                if (readInt2 != readInt3) {
                    long j = ((long) (((readInt2 - 1) * 1024) + 12)) + 0;
                    randomAccessFile.seek(j);
                    int readInt4 = randomAccessFile.readInt();
                    byte[] bArr = new byte[readInt4];
                    randomAccessFile.seek(j + 4);
                    for (int i = 0; i < readInt4; i++) {
                        bArr[i] = randomAccessFile.readByte();
                    }
                    String str = new String(bArr);
                    int i2 = readInt < C0855k.f1937af ? readInt2 + 1 : readInt2 == C0855k.f1937af ? 1 : 1 + readInt2;
                    randomAccessFile.seek(4);
                    randomAccessFile.writeInt(i2);
                    randomAccessFile.close();
                    return str;
                }
            }
            randomAccessFile.close();
            return null;
        } catch (IOException unused) {
        }
    }

    /* renamed from: a */
    private static boolean m1815a(int i, int i2, int i3) {
        int i4;
        return i >= 0 && i <= C0855k.f1937af && i2 >= 0 && i2 <= (i4 = i + 1) && i3 >= 1 && i3 <= i4 && i3 <= C0855k.f1937af;
    }

    /* renamed from: b */
    private static void m1816b() {
        f1300e = true;
        f1299d = null;
        f1301f = 0;
        f1302g = 0;
        f1303h = 0;
        f1304i = 0;
        f1305j = 0;
        f1306k = 0.0d;
        f1307l = 0.0d;
        f1308m = 0;
        f1309n = 0;
        f1310o = 0;
    }

    /* renamed from: c */
    private static boolean m1817c() {
        if (f1298c.exists()) {
            f1298c.delete();
        }
        if (!f1298c.getParentFile().exists()) {
            f1298c.getParentFile().mkdirs();
        }
        try {
            f1298c.createNewFile();
            RandomAccessFile randomAccessFile = new RandomAccessFile(f1298c, "rw");
            randomAccessFile.seek(0);
            randomAccessFile.writeInt(0);
            randomAccessFile.writeInt(0);
            randomAccessFile.writeInt(1);
            randomAccessFile.close();
            m1816b();
            return f1298c.exists();
        } catch (IOException unused) {
            return false;
        }
    }
}
