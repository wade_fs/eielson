package com.baidu.location.p018f;

import android.util.Log;
import java.lang.ref.WeakReference;

/* renamed from: com.baidu.location.f.b */
class C0842b implements Runnable {

    /* renamed from: a */
    final /* synthetic */ WeakReference f1824a;

    /* renamed from: b */
    final /* synthetic */ C0840a f1825b;

    C0842b(C0840a aVar, WeakReference weakReference) {
        this.f1825b = aVar;
        this.f1824a = weakReference;
    }

    public void run() {
        C0840a aVar = (C0840a) this.f1824a.get();
        if (aVar != null && aVar.f1822h == 3) {
            Log.d("baidu_location_service", "baidu location service force stopped ...");
            aVar.m2416d();
        }
    }
}
