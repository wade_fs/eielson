package com.baidu.mapsdkplatform.comapi.synchronization.p027b;

import android.support.v4.app.NotificationCompat;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceQueryOptions;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1122c;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1123d;
import com.baidu.mapsdkplatform.comapi.util.PermissionCheck;
import com.baidu.mapsdkplatform.comapi.util.SyncSysInfo;
import com.baidu.mapsdkplatform.comjni.util.AppMD5;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.b.f */
public class C1110f {

    /* renamed from: a */
    private static final String f3671a = C1110f.class.getSimpleName();

    /* renamed from: c */
    private static boolean f3672c = true;

    /* renamed from: d */
    private static int f3673d = 1;

    /* renamed from: b */
    private C1123d f3674b = new C1123d();

    public C1110f(HistoryTraceQueryOptions historyTraceQueryOptions) {
        m3820a(historyTraceQueryOptions);
    }

    /* renamed from: a */
    public static void m3819a(int i) {
        f3673d = i;
    }

    /* renamed from: a */
    private void m3820a(HistoryTraceQueryOptions historyTraceQueryOptions) {
        this.f3674b.mo13200a("order_id", m3821b(historyTraceQueryOptions));
        this.f3674b.mo13200a("original_order_id", historyTraceQueryOptions.getOrderId().toLowerCase());
        this.f3674b.mo13200a("company", historyTraceQueryOptions.getUserId());
        this.f3674b.mo13200a("order_attr", historyTraceQueryOptions.getDriverId());
        this.f3674b.mo13200a("track_status", String.valueOf(historyTraceQueryOptions.getQueryOrderState()));
        this.f3674b.mo13200a(NotificationCompat.CATEGORY_STATUS, String.valueOf(historyTraceQueryOptions.getCurrentOrderState()));
        if (CoordType.BD09LL != SDKInitializer.getCoordType() && CoordType.GCJ02 == SDKInitializer.getCoordType()) {
            this.f3674b.mo13200a("coord_type", CoordinateType.GCJ02);
        } else {
            this.f3674b.mo13200a("coord_type", "bd09ll");
        }
        this.f3674b.mo13200a("page_index", String.valueOf(f3673d));
        f3673d = 1;
        this.f3674b.mo13200a("page_size", "5000");
        this.f3674b.mo13200a("is_processed", "1");
        m3822b();
    }

    /* renamed from: b */
    private String m3821b(HistoryTraceQueryOptions historyTraceQueryOptions) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(historyTraceQueryOptions.getUserId().toLowerCase());
        stringBuffer.append("-");
        stringBuffer.append(historyTraceQueryOptions.getOrderId().toLowerCase());
        stringBuffer.append("-");
        stringBuffer.append("9sc87244121ip32590fq234mn6641tx7".toLowerCase());
        String a = C1122c.m3858a(stringBuffer.toString());
        String str = f3671a;
        C1120a.m3851a(str, "The orderId = " + stringBuffer.toString() + "; result = " + a);
        return a;
    }

    /* renamed from: b */
    private void m3822b() {
        String authToken = SyncSysInfo.getAuthToken();
        if (authToken == null) {
            C1120a.m3855b(f3671a, "Token is null, permission check again");
            int permissionCheck = PermissionCheck.permissionCheck();
            if (permissionCheck != 0) {
                String str = f3671a;
                C1120a.m3855b(str, "Permission check result is: " + permissionCheck);
                return;
            }
            authToken = SyncSysInfo.getAuthToken();
        }
        this.f3674b.mo13200a("token", authToken);
    }

    /* renamed from: c */
    private String m3823c() {
        return f3672c ? C1111g.m3825a() : C1111g.m3826b();
    }

    /* renamed from: a */
    public String mo13192a() {
        StringBuffer stringBuffer = new StringBuffer(this.f3674b.mo13201a());
        stringBuffer.append(SyncSysInfo.getPhoneInfo());
        String signMD5String = AppMD5.getSignMD5String(stringBuffer.toString());
        stringBuffer.append("&sign=");
        stringBuffer.append(signMD5String);
        StringBuffer stringBuffer2 = new StringBuffer(m3823c());
        stringBuffer2.append("?");
        stringBuffer2.append(stringBuffer);
        return stringBuffer2.toString();
    }
}
