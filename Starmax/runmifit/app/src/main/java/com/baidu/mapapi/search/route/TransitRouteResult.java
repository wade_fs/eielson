package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.core.TaxiInfo;
import java.util.ArrayList;
import java.util.List;

public final class TransitRouteResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<TransitRouteResult> CREATOR = new C1001q();

    /* renamed from: a */
    private TaxiInfo f3114a;

    /* renamed from: b */
    private List<TransitRouteLine> f3115b;

    /* renamed from: c */
    private SuggestAddrInfo f3116c;

    public TransitRouteResult() {
    }

    protected TransitRouteResult(Parcel parcel) {
        this.f3114a = (TaxiInfo) parcel.readParcelable(TaxiInfo.class.getClassLoader());
        this.f3115b = new ArrayList();
        parcel.readList(this.f3115b, TransitRouteLine.class.getClassLoader());
        this.f3116c = (SuggestAddrInfo) parcel.readParcelable(SuggestAddrInfo.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public List<TransitRouteLine> getRouteLines() {
        return this.f3115b;
    }

    public SuggestAddrInfo getSuggestAddrInfo() {
        return this.f3116c;
    }

    public TaxiInfo getTaxiInfo() {
        return this.f3114a;
    }

    public void setRoutelines(List<TransitRouteLine> list) {
        this.f3115b = list;
    }

    public void setSuggestAddrInfo(SuggestAddrInfo suggestAddrInfo) {
        this.f3116c = suggestAddrInfo;
    }

    public void setTaxiInfo(TaxiInfo taxiInfo) {
        this.f3114a = taxiInfo;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f3114a, 1);
        parcel.writeList(this.f3115b);
        parcel.writeParcelable(this.f3116c, 1);
    }
}
