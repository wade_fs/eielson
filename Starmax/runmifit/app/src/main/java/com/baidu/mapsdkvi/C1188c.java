package com.baidu.mapsdkvi;

import android.net.NetworkInfo;

/* renamed from: com.baidu.mapsdkvi.c */
public class C1188c {

    /* renamed from: a */
    public String f3959a;

    /* renamed from: b */
    public int f3960b;

    /* renamed from: c */
    public int f3961c;

    public C1188c(NetworkInfo networkInfo) {
        this.f3959a = networkInfo.getTypeName();
        this.f3960b = networkInfo.getType();
        int i = C1189d.f3962a[networkInfo.getState().ordinal()];
        if (i == 1) {
            this.f3961c = 2;
        } else if (i != 2) {
            this.f3961c = 0;
        } else {
            this.f3961c = 1;
        }
    }
}
