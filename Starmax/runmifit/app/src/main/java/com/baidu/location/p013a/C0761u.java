package com.baidu.location.p013a;

import android.os.HandlerThread;

/* renamed from: com.baidu.location.a.u */
public class C0761u {

    /* renamed from: a */
    private static HandlerThread f1430a;

    /* renamed from: a */
    public static synchronized HandlerThread m1942a() {
        HandlerThread handlerThread;
        synchronized (C0761u.class) {
            if (f1430a == null) {
                try {
                    f1430a = new HandlerThread("ServiceStartArguments", 10);
                    f1430a.start();
                } catch (Throwable th) {
                    th.printStackTrace();
                    f1430a = null;
                }
            }
            handlerThread = f1430a;
        }
        return handlerThread;
    }
}
