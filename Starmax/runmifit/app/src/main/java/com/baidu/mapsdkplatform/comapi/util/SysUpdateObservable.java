package com.baidu.mapsdkplatform.comapi.util;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public class SysUpdateObservable {

    /* renamed from: a */
    private static volatile SysUpdateObservable f3883a;

    /* renamed from: b */
    private List<SysUpdateObserver> f3884b;

    private SysUpdateObservable() {
        this.f3884b = null;
        this.f3884b = new ArrayList();
    }

    public static SysUpdateObservable getInstance() {
        if (f3883a == null) {
            synchronized (SysUpdateObservable.class) {
                if (f3883a == null) {
                    f3883a = new SysUpdateObservable();
                }
            }
        }
        return f3883a;
    }

    public void addObserver(SysUpdateObserver sysUpdateObserver) {
        this.f3884b.add(sysUpdateObserver);
    }

    public void init() {
        for (SysUpdateObserver sysUpdateObserver : this.f3884b) {
            if (sysUpdateObserver != null) {
                sysUpdateObserver.init();
            }
        }
    }

    public void updateNetworkInfo(Context context) {
        for (SysUpdateObserver sysUpdateObserver : this.f3884b) {
            if (sysUpdateObserver != null) {
                sysUpdateObserver.updateNetworkInfo(context);
            }
        }
    }

    public void updateNetworkProxy(Context context) {
        for (SysUpdateObserver sysUpdateObserver : this.f3884b) {
            if (sysUpdateObserver != null) {
                sysUpdateObserver.updateNetworkProxy(context);
            }
        }
    }

    public void updatePhoneInfo() {
        for (SysUpdateObserver sysUpdateObserver : this.f3884b) {
            if (sysUpdateObserver != null) {
                sysUpdateObserver.updatePhoneInfo();
            }
        }
    }
}
