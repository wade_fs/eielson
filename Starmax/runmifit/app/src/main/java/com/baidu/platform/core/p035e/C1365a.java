package com.baidu.platform.core.p035e;

import com.baidu.mapapi.search.share.LocationShareURLOption;
import com.baidu.mapapi.search.share.OnGetShareUrlResultListener;
import com.baidu.mapapi.search.share.PoiDetailShareURLOption;
import com.baidu.mapapi.search.share.RouteShareURLOption;

/* renamed from: com.baidu.platform.core.e.a */
public interface C1365a {
    /* renamed from: a */
    void mo14068a();

    /* renamed from: a */
    void mo14069a(OnGetShareUrlResultListener onGetShareUrlResultListener);

    /* renamed from: a */
    boolean mo14070a(LocationShareURLOption locationShareURLOption);

    /* renamed from: a */
    boolean mo14071a(PoiDetailShareURLOption poiDetailShareURLOption);

    /* renamed from: a */
    boolean mo14072a(RouteShareURLOption routeShareURLOption);
}
