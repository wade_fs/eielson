package com.baidu.mapsdkplatform.comapi.map;

import android.graphics.Bitmap;
import android.view.MotionEvent;
import com.baidu.mapapi.model.inner.GeoPoint;
import javax.microedition.khronos.opengles.GL10;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.l */
public interface C1087l {
    /* renamed from: a */
    void mo11538a();

    /* renamed from: a */
    void mo11539a(Bitmap bitmap);

    /* renamed from: a */
    void mo11540a(MotionEvent motionEvent);

    /* renamed from: a */
    void mo11541a(GeoPoint geoPoint);

    /* renamed from: a */
    void mo11542a(C1064ab abVar);

    /* renamed from: a */
    void mo11543a(String str);

    /* renamed from: a */
    void mo11544a(GL10 gl10, C1064ab abVar);

    /* renamed from: a */
    void mo11545a(boolean z);

    /* renamed from: a */
    void mo11546a(boolean z, int i);

    /* renamed from: b */
    void mo11547b();

    /* renamed from: b */
    void mo11548b(GeoPoint geoPoint);

    /* renamed from: b */
    void mo11549b(C1064ab abVar);

    /* renamed from: b */
    boolean mo11550b(String str);

    /* renamed from: c */
    void mo11551c();

    /* renamed from: c */
    void mo11552c(GeoPoint geoPoint);

    /* renamed from: c */
    void mo11553c(C1064ab abVar);

    /* renamed from: d */
    void mo11554d();

    /* renamed from: d */
    void mo11555d(GeoPoint geoPoint);

    /* renamed from: e */
    void mo11556e();

    /* renamed from: e */
    void mo11557e(GeoPoint geoPoint);

    /* renamed from: f */
    void mo11558f();
}
