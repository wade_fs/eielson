package com.baidu.platform.base;

import com.baidu.mapapi.http.HttpClient;

/* renamed from: com.baidu.platform.base.b */
class C1317b extends HttpClient.ProtoResultCallback {

    /* renamed from: a */
    final /* synthetic */ C1319d f4427a;

    /* renamed from: b */
    final /* synthetic */ Object f4428b;

    /* renamed from: c */
    final /* synthetic */ C1316a f4429c;

    C1317b(C1316a aVar, C1319d dVar, Object obj) {
        this.f4429c = aVar;
        this.f4427a = dVar;
        this.f4428b = obj;
    }

    public void onFailed(HttpClient.HttpStateError httpStateError) {
        this.f4429c.m4895a(httpStateError, this.f4427a, this.f4428b);
    }

    public void onSuccess(String str) {
        this.f4429c.m4900a(str);
        C1316a aVar = this.f4429c;
        aVar.m4901a(str, this.f4427a, this.f4428b, aVar.f4423b, super);
    }
}
