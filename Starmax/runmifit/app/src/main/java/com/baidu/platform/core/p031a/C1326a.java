package com.baidu.platform.core.p031a;

import com.baidu.mapapi.search.district.DistrictSearchOption;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;
import com.tamic.novate.download.MimeType;
import com.tencent.connect.common.Constants;

/* renamed from: com.baidu.platform.core.a.a */
public class C1326a extends C1320e {
    C1326a(DistrictSearchOption districtSearchOption) {
        m4931a(districtSearchOption);
    }

    /* renamed from: a */
    private void m4931a(DistrictSearchOption districtSearchOption) {
        String str;
        C1381a aVar;
        if (districtSearchOption != null) {
            this.f4435a.mo14094a("qt", "con");
            this.f4435a.mo14094a("rp_format", MimeType.JSON);
            this.f4435a.mo14094a("rp_filter", "mobile");
            this.f4435a.mo14094a("area_res", "true");
            this.f4435a.mo14094a("addr_identify", "1");
            this.f4435a.mo14094a("ie", "utf-8");
            this.f4435a.mo14094a(Config.PACKAGE_NAME, "0");
            this.f4435a.mo14094a(Config.EVENT_VIEW_RES_NAME, Constants.VIA_REPORT_TYPE_SHARE_TO_QQ);
            this.f4435a.mo14094a("c", districtSearchOption.mCityName);
            if (districtSearchOption.mDistrictName == null || districtSearchOption.mDistrictName.equals("")) {
                aVar = this.f4435a;
                str = districtSearchOption.mCityName;
            } else {
                aVar = this.f4435a;
                str = districtSearchOption.mDistrictName;
            }
            aVar.mo14094a("wd", str);
        }
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14089n();
    }
}
