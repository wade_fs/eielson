package com.baidu.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import com.google.android.gms.common.internal.ImagesContract;
import com.google.common.primitives.UnsignedBytes;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class BitmapDescriptor {

    /* renamed from: a */
    Bitmap f2437a;

    /* renamed from: b */
    private Bundle f2438b;

    BitmapDescriptor(Bitmap bitmap) {
        if (bitmap != null) {
            this.f2437a = m2863a(bitmap, bitmap.getWidth(), bitmap.getHeight());
        }
    }

    /* renamed from: a */
    private Bitmap m2863a(Bitmap bitmap, int i, int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return createBitmap;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public byte[] mo11046a() {
        ByteBuffer allocate = ByteBuffer.allocate(this.f2437a.getWidth() * this.f2437a.getHeight() * 4);
        this.f2437a.copyPixelsToBuffer(allocate);
        return allocate.array();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Bundle mo11047b() {
        if (this.f2437a != null) {
            if (this.f2438b == null) {
                Bundle bundle = new Bundle();
                bundle.putInt("image_width", this.f2437a.getWidth());
                bundle.putInt("image_height", this.f2437a.getHeight());
                byte[] a = mo11046a();
                bundle.putByteArray(ImagesContract.IMAGE_DATA, a);
                MessageDigest messageDigest = null;
                try {
                    messageDigest = MessageDigest.getInstance("MD5");
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                if (messageDigest != null) {
                    messageDigest.update(a, 0, a.length);
                    byte[] digest = messageDigest.digest();
                    StringBuilder sb = new StringBuilder("");
                    for (byte b : digest) {
                        sb.append(Integer.toString((b & UnsignedBytes.MAX_VALUE) + 256, 16).substring(1));
                    }
                    bundle.putString("image_hashcode", sb.toString());
                }
                this.f2438b = bundle;
            }
            return this.f2438b;
        }
        throw new IllegalStateException("BDMapSDKException: the bitmap has been recycled! you can not use it again");
    }

    public Bitmap getBitmap() {
        return this.f2437a;
    }

    public void recycle() {
        Bitmap bitmap = this.f2437a;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f2437a.recycle();
            this.f2437a = null;
        }
    }
}
