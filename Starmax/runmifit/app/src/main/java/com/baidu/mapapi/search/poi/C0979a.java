package com.baidu.mapapi.search.poi;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.poi.a */
final class C0979a implements Parcelable.Creator<PoiDetailResult> {
    C0979a() {
    }

    /* renamed from: a */
    public PoiDetailResult createFromParcel(Parcel parcel) {
        return new PoiDetailResult(parcel);
    }

    /* renamed from: a */
    public PoiDetailResult[] newArray(int i) {
        return new PoiDetailResult[i];
    }
}
