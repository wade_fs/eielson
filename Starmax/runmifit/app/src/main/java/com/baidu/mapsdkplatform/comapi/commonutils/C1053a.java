package com.baidu.mapsdkplatform.comapi.commonutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import java.io.InputStream;

/* renamed from: com.baidu.mapsdkplatform.comapi.commonutils.a */
public class C1053a {

    /* renamed from: a */
    private static final boolean f3372a = (Build.VERSION.SDK_INT >= 8);

    /* renamed from: a */
    public static Bitmap m3464a(String str, Context context) {
        try {
            InputStream open = context.getAssets().open(str);
            if (open != null) {
                return BitmapFactory.decodeStream(open);
            }
            return null;
        } catch (Exception unused) {
            return BitmapFactory.decodeFile(m3467b("assets/" + str, str, context));
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:11|12|13|14|15|17) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|(3:1|2|(1:4)(1:18))|5|6|7|8|9|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x001e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0016 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m3465a(java.io.InputStream r3, java.io.FileOutputStream r4) throws java.io.IOException {
        /*
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r0 = new byte[r0]
        L_0x0004:
            int r1 = r3.read(r0)     // Catch:{ all -> 0x001a }
            r2 = -1
            if (r1 == r2) goto L_0x0010
            r2 = 0
            r4.write(r0, r2, r1)     // Catch:{ all -> 0x001a }
            goto L_0x0004
        L_0x0010:
            r4.flush()     // Catch:{ all -> 0x001a }
            r3.close()     // Catch:{ IOException -> 0x0016 }
        L_0x0016:
            r4.close()     // Catch:{ IOException -> 0x0019 }
        L_0x0019:
            return
        L_0x001a:
            r0 = move-exception
            r3.close()     // Catch:{ IOException -> 0x001e }
        L_0x001e:
            r4.close()     // Catch:{ IOException -> 0x0021 }
        L_0x0021:
            goto L_0x0023
        L_0x0022:
            throw r0
        L_0x0023:
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.commonutils.C1053a.m3465a(java.io.InputStream, java.io.FileOutputStream):void");
    }

    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r0v2 */
    /* JADX WARN: Type inference failed for: r0v3, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r0v4, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0081 A[SYNTHETIC, Splitter:B:33:0x0081] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0086 A[Catch:{ IOException -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008f A[SYNTHETIC, Splitter:B:41:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0097 A[Catch:{ IOException -> 0x0093 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m3466a(java.lang.String r6, java.lang.String r7, android.content.Context r8) {
        /*
            r0 = 0
            android.content.res.AssetManager r1 = r8.getAssets()     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            java.io.InputStream r1 = r1.open(r6)     // Catch:{ Exception -> 0x006a, all -> 0x0067 }
            if (r1 == 0) goto L_0x0055
            int r2 = r1.available()     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            r1.read(r2)     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            r4.<init>()     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            java.io.File r5 = r8.getFilesDir()     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            r4.append(r5)     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            java.lang.String r5 = "/"
            r4.append(r5)     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            r4.append(r7)     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            boolean r4 = r3.exists()     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            if (r4 == 0) goto L_0x003e
            r3.delete()     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
        L_0x003e:
            r3.createNewFile()     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0052, all -> 0x0050 }
            r4.write(r2)     // Catch:{ Exception -> 0x0053, all -> 0x004e }
            r4.close()     // Catch:{ Exception -> 0x0053, all -> 0x004e }
            r0 = r4
            goto L_0x0055
        L_0x004e:
            r6 = move-exception
            goto L_0x008c
        L_0x0050:
            r6 = move-exception
            goto L_0x008d
        L_0x0052:
            r4 = r0
        L_0x0053:
            r0 = r1
            goto L_0x006b
        L_0x0055:
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x005d
        L_0x005b:
            r6 = move-exception
            goto L_0x0063
        L_0x005d:
            if (r0 == 0) goto L_0x0089
            r0.close()     // Catch:{ IOException -> 0x005b }
            goto L_0x0089
        L_0x0063:
            r6.printStackTrace()
            goto L_0x0089
        L_0x0067:
            r6 = move-exception
            r1 = r0
            goto L_0x008d
        L_0x006a:
            r4 = r0
        L_0x006b:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008a }
            r1.<init>()     // Catch:{ all -> 0x008a }
            java.lang.String r2 = "assets/"
            r1.append(r2)     // Catch:{ all -> 0x008a }
            r1.append(r6)     // Catch:{ all -> 0x008a }
            java.lang.String r6 = r1.toString()     // Catch:{ all -> 0x008a }
            m3467b(r6, r7, r8)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0084
            r0.close()     // Catch:{ IOException -> 0x005b }
        L_0x0084:
            if (r4 == 0) goto L_0x0089
            r4.close()     // Catch:{ IOException -> 0x005b }
        L_0x0089:
            return
        L_0x008a:
            r6 = move-exception
            r1 = r0
        L_0x008c:
            r0 = r4
        L_0x008d:
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x0095
        L_0x0093:
            r7 = move-exception
            goto L_0x009b
        L_0x0095:
            if (r0 == 0) goto L_0x009e
            r0.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x009e
        L_0x009b:
            r7.printStackTrace()
        L_0x009e:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.commonutils.C1053a.m3466a(java.lang.String, java.lang.String, android.content.Context):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ab A[SYNTHETIC, Splitter:B:32:0x00ab] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b5 A[SYNTHETIC, Splitter:B:38:0x00b5] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m3467b(java.lang.String r8, java.lang.String r9, android.content.Context r10) {
        /*
            java.lang.String r0 = "/"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.io.File r2 = r10.getFilesDir()
            java.lang.String r2 = r2.getAbsolutePath()
            r1.<init>(r2)
            boolean r2 = com.baidu.mapsdkplatform.comapi.commonutils.C1053a.f3372a
            if (r2 == 0) goto L_0x0018
            java.lang.String r2 = r10.getPackageCodePath()
            goto L_0x001a
        L_0x0018:
            java.lang.String r2 = ""
        L_0x001a:
            r3 = 0
            java.util.zip.ZipFile r4 = new java.util.zip.ZipFile     // Catch:{ Exception -> 0x009d }
            r4.<init>(r2)     // Catch:{ Exception -> 0x009d }
            int r2 = r9.lastIndexOf(r0)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            if (r2 <= 0) goto L_0x005e
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.io.File r10 = r10.getFilesDir()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.lang.String r10 = r10.getAbsolutePath()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r5.<init>(r10)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r10 = 0
            java.lang.String r10 = r9.substring(r10, r2)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            int r2 = r2 + 1
            int r6 = r9.length()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.lang.String r9 = r9.substring(r2, r6)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r6.<init>()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.lang.String r7 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r6.append(r7)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r6.append(r0)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r6.append(r10)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.lang.String r10 = r6.toString()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r2.<init>(r10, r9)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            goto L_0x0072
        L_0x005e:
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.io.File r10 = r10.getFilesDir()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.lang.String r2 = "assets"
            r5.<init>(r10, r2)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.lang.String r10 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r2.<init>(r10, r9)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
        L_0x0072:
            r5.mkdirs()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.util.zip.ZipEntry r9 = r4.getEntry(r8)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            if (r9 != 0) goto L_0x007f
            r4.close()     // Catch:{ IOException -> 0x007e }
        L_0x007e:
            return r3
        L_0x007f:
            java.io.InputStream r9 = r4.getInputStream(r9)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r10.<init>(r2)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            m3465a(r9, r10)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r1.append(r0)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r1.append(r8)     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            r4.close()     // Catch:{ IOException -> 0x00ae }
            goto L_0x00ae
        L_0x0095:
            r8 = move-exception
            goto L_0x00b3
        L_0x0097:
            r8 = move-exception
            r3 = r4
            goto L_0x009e
        L_0x009a:
            r8 = move-exception
            r4 = r3
            goto L_0x00b3
        L_0x009d:
            r8 = move-exception
        L_0x009e:
            java.lang.Class<com.baidu.mapsdkplatform.comapi.commonutils.a> r9 = com.baidu.mapsdkplatform.comapi.commonutils.C1053a.class
            java.lang.String r9 = r9.getSimpleName()     // Catch:{ all -> 0x009a }
            java.lang.String r10 = "copyAssetsError"
            android.util.Log.e(r9, r10, r8)     // Catch:{ all -> 0x009a }
            if (r3 == 0) goto L_0x00ae
            r3.close()     // Catch:{ IOException -> 0x00ae }
        L_0x00ae:
            java.lang.String r8 = r1.toString()
            return r8
        L_0x00b3:
            if (r4 == 0) goto L_0x00b8
            r4.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x00b8:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.commonutils.C1053a.m3467b(java.lang.String, java.lang.String, android.content.Context):java.lang.String");
    }
}
