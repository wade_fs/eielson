package com.baidu.location;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

/* renamed from: com.baidu.location.b */
class C0769b implements ServiceConnection {

    /* renamed from: a */
    final /* synthetic */ LocationClient f1475a;

    C0769b(LocationClient locationClient) {
        this.f1475a = locationClient;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, boolean):boolean
     arg types: [com.baidu.location.LocationClient, int]
     candidates:
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, android.os.Messenger):android.os.Messenger
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, com.baidu.location.LocationClient$b):com.baidu.location.LocationClient$b
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, com.baidu.location.a.c):com.baidu.location.a.c
      com.baidu.location.LocationClient.a(int, android.app.Notification):void
      com.baidu.location.LocationClient.a(android.os.Message, int):void
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, android.os.Message):void
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, com.baidu.location.BDLocation):void
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, boolean):boolean */
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Messenger unused = this.f1475a.f1179g = new Messenger(iBinder);
        if (this.f1475a.f1179g != null) {
            boolean unused2 = this.f1475a.f1177e = true;
            Log.d("baidu_location_client", "baidu location connected ...");
            if (this.f1475a.f1198z) {
                this.f1475a.f1180h.obtainMessage(2).sendToTarget();
                return;
            }
            try {
                Message obtain = Message.obtain((Handler) null, 11);
                obtain.replyTo = this.f1475a.f1181i;
                obtain.setData(this.f1475a.m1690d());
                this.f1475a.f1179g.send(obtain);
                boolean unused3 = this.f1475a.f1177e = true;
                if (this.f1475a.f1175c != null) {
                    this.f1475a.f1166C.booleanValue();
                    this.f1475a.f1180h.obtainMessage(4).sendToTarget();
                }
            } catch (Exception unused4) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, boolean):boolean
     arg types: [com.baidu.location.LocationClient, int]
     candidates:
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, android.os.Messenger):android.os.Messenger
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, com.baidu.location.LocationClient$b):com.baidu.location.LocationClient$b
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, com.baidu.location.a.c):com.baidu.location.a.c
      com.baidu.location.LocationClient.a(int, android.app.Notification):void
      com.baidu.location.LocationClient.a(android.os.Message, int):void
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, android.os.Message):void
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, com.baidu.location.BDLocation):void
      com.baidu.location.LocationClient.a(com.baidu.location.LocationClient, boolean):boolean */
    public void onServiceDisconnected(ComponentName componentName) {
        Messenger unused = this.f1475a.f1179g = (Messenger) null;
        boolean unused2 = this.f1475a.f1177e = false;
    }
}
