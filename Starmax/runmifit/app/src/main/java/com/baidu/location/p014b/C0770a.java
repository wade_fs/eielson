package com.baidu.location.p014b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.baidu.android.bbalbs.common.p012a.C0692b;
import com.baidu.location.BDLocation;
import com.baidu.location.Jni;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0855k;
import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: com.baidu.location.b.a */
public class C0770a {

    /* renamed from: b */
    private static Object f1476b = new Object();

    /* renamed from: c */
    private static C0770a f1477c = null;

    /* renamed from: d */
    private static final String f1478d = (C0855k.m2472j() + "/gal.db");
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static Lock f1479f = new ReentrantLock();

    /* renamed from: a */
    C0771a f1480a = null;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public SQLiteDatabase f1481e = null;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f1482g = false;

    /* renamed from: h */
    private Map<String, Integer> f1483h = new HashMap();

    /* renamed from: i */
    private String f1484i = null;

    /* renamed from: j */
    private int f1485j = -1;

    /* renamed from: k */
    private String f1486k = null;

    /* renamed from: l */
    private double f1487l = Double.MAX_VALUE;

    /* renamed from: m */
    private double f1488m = Double.MAX_VALUE;

    /* renamed from: com.baidu.location.b.a$a */
    class C0771a extends C0849e {

        /* renamed from: a */
        int f1489a;

        /* renamed from: b */
        int f1490b;

        /* renamed from: c */
        int f1491c;

        /* renamed from: d */
        int f1492d;

        /* renamed from: e */
        double f1493e;

        C0771a() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            String str;
            this.f1882h = "http://loc.map.baidu.com/gpsz";
            String format = String.format(Locale.CHINESE, "&is_vdr=1&x=%d&y=%d%s", Integer.valueOf(this.f1489a), Integer.valueOf(this.f1490b), C0844b.m2417a().mo10718c());
            String encode = Jni.encode(format);
            if (encode.contains("err!")) {
                try {
                    str = C0692b.m1539a(format.toString().getBytes(), "UTF-8");
                } catch (Exception unused) {
                    str = "err2!";
                }
                this.f1885k.put("gpszb", str);
                return;
            }
            this.f1885k.put("gpsz", encode);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.a.a(com.baidu.location.b.a, boolean):boolean
         arg types: [com.baidu.location.b.a, int]
         candidates:
          com.baidu.location.b.a.a(double, double):double[]
          com.baidu.location.b.a.a(com.baidu.location.b.a, boolean):boolean */
        /* renamed from: a */
        public void mo10522a(double d, double d2, double d3) {
            if (!C0770a.this.f1482g) {
                double[] coorEncrypt = Jni.coorEncrypt(d, d2, "gcj2wgs");
                this.f1489a = (int) Math.floor(coorEncrypt[0] * 100.0d);
                this.f1490b = (int) Math.floor(coorEncrypt[1] * 100.0d);
                this.f1491c = (int) Math.floor(d * 100.0d);
                this.f1492d = (int) Math.floor(d2 * 100.0d);
                this.f1493e = d3;
                boolean unused = C0770a.this.f1482g = true;
                if (!C0855k.m2460b()) {
                    mo10733b(C0762v.m1943a().mo10505c());
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.a.a(com.baidu.location.b.a, boolean):boolean
         arg types: [com.baidu.location.b.a, int]
         candidates:
          com.baidu.location.b.a.a(double, double):double[]
          com.baidu.location.b.a.a(com.baidu.location.b.a, boolean):boolean */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0121 A[Catch:{ Exception -> 0x0132 }] */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0132 A[SYNTHETIC] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo10446a(boolean r26) {
            /*
                r25 = this;
                r0 = r25
                java.lang.String r1 = "locStateData"
                java.lang.String r2 = ":"
                java.lang.String r3 = "info"
                java.lang.String r4 = "height"
                if (r26 == 0) goto L_0x021c
                java.lang.String r6 = r0.f1884j
                if (r6 == 0) goto L_0x021c
                java.util.concurrent.locks.Lock r6 = com.baidu.location.p014b.C0770a.f1479f
                r6.lock()
                org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x0215 }
                java.lang.String r7 = r0.f1884j     // Catch:{ Exception -> 0x0215 }
                r6.<init>(r7)     // Catch:{ Exception -> 0x0215 }
                boolean r7 = r6.has(r4)     // Catch:{ Exception -> 0x0215 }
                java.lang.String r8 = "%d,%d"
                java.lang.String r9 = "E"
                java.lang.String r11 = ","
                r12 = 2
                if (r7 == 0) goto L_0x0150
                java.lang.String r4 = r6.getString(r4)     // Catch:{ Exception -> 0x0215 }
                boolean r7 = r4.contains(r11)     // Catch:{ Exception -> 0x0215 }
                if (r7 == 0) goto L_0x0150
                java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x0215 }
                java.lang.String[] r4 = r4.split(r11)     // Catch:{ Exception -> 0x0215 }
                int r7 = r4.length     // Catch:{ Exception -> 0x0215 }
                double r14 = (double) r7     // Catch:{ Exception -> 0x0215 }
                double r14 = java.lang.Math.sqrt(r14)     // Catch:{ Exception -> 0x0215 }
                int r14 = (int) r14     // Catch:{ Exception -> 0x0215 }
                int r15 = r14 * r14
                if (r15 != r7) goto L_0x0150
                int r7 = r0.f1491c     // Catch:{ Exception -> 0x0215 }
                int r15 = r14 + -1
                int r16 = r15 / 2
                int r7 = r7 - r16
                int r10 = r0.f1492d     // Catch:{ Exception -> 0x0215 }
                int r15 = r15 / r12
                int r10 = r10 - r15
                r15 = 0
            L_0x0055:
                if (r15 >= r14) goto L_0x0150
                r13 = 0
            L_0x0058:
                if (r13 >= r14) goto L_0x0140
                android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ Exception -> 0x0215 }
                r5.<init>()     // Catch:{ Exception -> 0x0215 }
                int r17 = r15 * r14
                int r17 = r17 + r13
                r12 = r4[r17]     // Catch:{ Exception -> 0x0215 }
                boolean r12 = r12.contains(r9)     // Catch:{ Exception -> 0x0215 }
                r18 = r14
                java.lang.String r14 = "sigma"
                r19 = r1
                java.lang.String r1 = "aldata"
                r20 = 4666723172467343360(0x40c3880000000000, double:10000.0)
                if (r12 == 0) goto L_0x008b
                java.lang.Double r12 = java.lang.Double.valueOf(r20)     // Catch:{ Exception -> 0x0215 }
                r5.put(r1, r12)     // Catch:{ Exception -> 0x0215 }
                java.lang.Double r1 = java.lang.Double.valueOf(r20)     // Catch:{ Exception -> 0x0215 }
            L_0x0083:
                r5.put(r14, r1)     // Catch:{ Exception -> 0x0215 }
                r17 = r2
                r22 = r4
                goto L_0x00ce
            L_0x008b:
                r12 = r4[r17]     // Catch:{ Exception -> 0x0215 }
                boolean r12 = r12.contains(r2)     // Catch:{ Exception -> 0x0215 }
                if (r12 != 0) goto L_0x00a1
                r12 = r4[r17]     // Catch:{ Exception -> 0x0215 }
                java.lang.Double r12 = java.lang.Double.valueOf(r12)     // Catch:{ Exception -> 0x0215 }
                r5.put(r1, r12)     // Catch:{ Exception -> 0x0215 }
                java.lang.Double r1 = java.lang.Double.valueOf(r20)     // Catch:{ Exception -> 0x0215 }
                goto L_0x0083
            L_0x00a1:
                r12 = r4[r17]     // Catch:{ Exception -> 0x0215 }
                java.lang.String[] r12 = r12.split(r2)     // Catch:{ Exception -> 0x0215 }
                r17 = r2
                int r2 = r12.length     // Catch:{ Exception -> 0x0215 }
                r22 = r4
                r4 = 2
                if (r2 != r4) goto L_0x00c0
                r2 = 0
                r4 = r12[r2]     // Catch:{ Exception -> 0x0215 }
                java.lang.Double r2 = java.lang.Double.valueOf(r4)     // Catch:{ Exception -> 0x0215 }
                r5.put(r1, r2)     // Catch:{ Exception -> 0x0215 }
                r1 = 1
                r2 = r12[r1]     // Catch:{ Exception -> 0x0215 }
                r5.put(r14, r2)     // Catch:{ Exception -> 0x0215 }
                goto L_0x00ce
            L_0x00c0:
                java.lang.Double r2 = java.lang.Double.valueOf(r20)     // Catch:{ Exception -> 0x0215 }
                r5.put(r1, r2)     // Catch:{ Exception -> 0x0215 }
                java.lang.Double r1 = java.lang.Double.valueOf(r20)     // Catch:{ Exception -> 0x0215 }
                r5.put(r14, r1)     // Catch:{ Exception -> 0x0215 }
            L_0x00ce:
                java.lang.String r1 = "tt"
                long r20 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0215 }
                r23 = 1000(0x3e8, double:4.94E-321)
                r2 = r11
                long r11 = r20 / r23
                int r4 = (int) r11     // Catch:{ Exception -> 0x0215 }
                java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0215 }
                r5.put(r1, r4)     // Catch:{ Exception -> 0x0215 }
                int r1 = r7 + r13
                int r4 = r10 + r15
                java.util.Locale r11 = java.util.Locale.CHINESE     // Catch:{ Exception -> 0x0215 }
                r12 = 2
                java.lang.Object[] r14 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x0215 }
                java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0215 }
                r12 = 0
                r14[r12] = r1     // Catch:{ Exception -> 0x0215 }
                java.lang.Integer r1 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0215 }
                r4 = 1
                r14[r4] = r1     // Catch:{ Exception -> 0x0215 }
                java.lang.String r1 = java.lang.String.format(r11, r8, r14)     // Catch:{ Exception -> 0x0215 }
                com.baidu.location.b.a r4 = com.baidu.location.p014b.C0770a.this     // Catch:{ Exception -> 0x0132 }
                android.database.sqlite.SQLiteDatabase r4 = r4.f1481e     // Catch:{ Exception -> 0x0132 }
                java.lang.String r11 = "galdata_new"
                java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0132 }
                r12.<init>()     // Catch:{ Exception -> 0x0132 }
                java.lang.String r14 = "id = \""
                r12.append(r14)     // Catch:{ Exception -> 0x0132 }
                r12.append(r1)     // Catch:{ Exception -> 0x0132 }
                java.lang.String r14 = "\""
                r12.append(r14)     // Catch:{ Exception -> 0x0132 }
                java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x0132 }
                r14 = 0
                int r4 = r4.update(r11, r5, r12, r14)     // Catch:{ Exception -> 0x0132 }
                if (r4 > 0) goto L_0x0132
                java.lang.String r4 = "id"
                r5.put(r4, r1)     // Catch:{ Exception -> 0x0132 }
                com.baidu.location.b.a r1 = com.baidu.location.p014b.C0770a.this     // Catch:{ Exception -> 0x0132 }
                android.database.sqlite.SQLiteDatabase r1 = r1.f1481e     // Catch:{ Exception -> 0x0132 }
                java.lang.String r4 = "galdata_new"
                r11 = 0
                r1.insert(r4, r11, r5)     // Catch:{ Exception -> 0x0132 }
            L_0x0132:
                int r13 = r13 + 1
                r11 = r2
                r2 = r17
                r14 = r18
                r1 = r19
                r4 = r22
                r12 = 2
                goto L_0x0058
            L_0x0140:
                r19 = r1
                r17 = r2
                r22 = r4
                r2 = r11
                r18 = r14
                int r15 = r15 + 1
                r2 = r17
                r12 = 2
                goto L_0x0055
            L_0x0150:
                r19 = r1
                r2 = r11
                boolean r1 = r6.has(r3)     // Catch:{ Exception -> 0x0215 }
                if (r1 == 0) goto L_0x0215
                java.lang.String r1 = r6.getString(r3)     // Catch:{ Exception -> 0x0215 }
                boolean r3 = r1.contains(r2)     // Catch:{ Exception -> 0x0215 }
                if (r3 == 0) goto L_0x0215
                java.lang.String r1 = r1.trim()     // Catch:{ Exception -> 0x0215 }
                java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0215 }
                int r2 = r1.length     // Catch:{ Exception -> 0x0215 }
                double r3 = (double) r2     // Catch:{ Exception -> 0x0215 }
                double r3 = java.lang.Math.sqrt(r3)     // Catch:{ Exception -> 0x0215 }
                int r3 = (int) r3     // Catch:{ Exception -> 0x0215 }
                int r4 = r3 * r3
                if (r4 != r2) goto L_0x0215
                int r2 = r0.f1491c     // Catch:{ Exception -> 0x0215 }
                int r4 = r3 + -1
                int r5 = r4 / 2
                int r2 = r2 - r5
                int r5 = r0.f1492d     // Catch:{ Exception -> 0x0215 }
                r6 = 2
                int r4 = r4 / r6
                int r5 = r5 - r4
                r4 = 0
            L_0x0183:
                if (r4 >= r3) goto L_0x0215
                r6 = 0
            L_0x0186:
                if (r6 >= r3) goto L_0x020d
                android.content.ContentValues r7 = new android.content.ContentValues     // Catch:{ Exception -> 0x0215 }
                r7.<init>()     // Catch:{ Exception -> 0x0215 }
                int r10 = r4 * r3
                int r10 = r10 + r6
                r11 = r1[r10]     // Catch:{ Exception -> 0x0215 }
                boolean r11 = r11.contains(r9)     // Catch:{ Exception -> 0x0215 }
                java.lang.String r12 = "state"
                if (r11 == 0) goto L_0x01a3
                r10 = -1
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0215 }
            L_0x019f:
                r7.put(r12, r10)     // Catch:{ Exception -> 0x0215 }
                goto L_0x01b2
            L_0x01a3:
                r10 = r1[r10]     // Catch:{ Exception -> 0x0215 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0215 }
                int r10 = r10.intValue()     // Catch:{ Exception -> 0x0215 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0215 }
                goto L_0x019f
            L_0x01b2:
                int r10 = r2 + r6
                int r11 = r5 + r4
                java.util.Locale r12 = java.util.Locale.CHINESE     // Catch:{ Exception -> 0x0215 }
                r13 = 2
                java.lang.Object[] r14 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x0215 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0215 }
                r15 = 0
                r14[r15] = r10     // Catch:{ Exception -> 0x0215 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x0215 }
                r11 = 1
                r14[r11] = r10     // Catch:{ Exception -> 0x0215 }
                java.lang.String r10 = java.lang.String.format(r12, r8, r14)     // Catch:{ Exception -> 0x0215 }
                com.baidu.location.b.a r12 = com.baidu.location.p014b.C0770a.this     // Catch:{ Exception -> 0x0204 }
                android.database.sqlite.SQLiteDatabase r12 = r12.f1481e     // Catch:{ Exception -> 0x0204 }
                java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0204 }
                r14.<init>()     // Catch:{ Exception -> 0x0204 }
                java.lang.String r15 = "id = \""
                r14.append(r15)     // Catch:{ Exception -> 0x0204 }
                r14.append(r10)     // Catch:{ Exception -> 0x0204 }
                java.lang.String r15 = "\""
                r14.append(r15)     // Catch:{ Exception -> 0x0204 }
                java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x0204 }
                r15 = r19
                r11 = 0
                int r12 = r12.update(r15, r7, r14, r11)     // Catch:{ Exception -> 0x0207 }
                if (r12 > 0) goto L_0x0202
                java.lang.String r11 = "id"
                r7.put(r11, r10)     // Catch:{ Exception -> 0x0202 }
                com.baidu.location.b.a r10 = com.baidu.location.p014b.C0770a.this     // Catch:{ Exception -> 0x0202 }
                android.database.sqlite.SQLiteDatabase r10 = r10.f1481e     // Catch:{ Exception -> 0x0202 }
                r11 = 0
                r10.insert(r15, r11, r7)     // Catch:{ Exception -> 0x0207 }
                goto L_0x0207
            L_0x0202:
                r11 = 0
                goto L_0x0207
            L_0x0204:
                r15 = r19
                goto L_0x0202
            L_0x0207:
                int r6 = r6 + 1
                r19 = r15
                goto L_0x0186
            L_0x020d:
                r15 = r19
                r11 = 0
                r13 = 2
                int r4 = r4 + 1
                goto L_0x0183
            L_0x0215:
                java.util.concurrent.locks.Lock r1 = com.baidu.location.p014b.C0770a.f1479f
                r1.unlock()
            L_0x021c:
                java.util.Map r1 = r0.f1885k
                if (r1 == 0) goto L_0x0225
                java.util.Map r1 = r0.f1885k
                r1.clear()
            L_0x0225:
                com.baidu.location.b.a r1 = com.baidu.location.p014b.C0770a.this
                r2 = 0
                boolean unused = r1.f1482g = r2
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p014b.C0770a.C0771a.mo10446a(boolean):void");
        }
    }

    /* renamed from: a */
    public static C0770a m1980a() {
        C0770a aVar;
        synchronized (f1476b) {
            if (f1477c == null) {
                f1477c = new C0770a();
            }
            aVar = f1477c;
        }
        return aVar;
    }

    /* renamed from: a */
    private void m1981a(double d, double d2, double d3) {
        if (this.f1480a == null) {
            this.f1480a = new C0771a();
        }
        this.f1480a.mo10522a(d, d2, d3);
    }

    /* renamed from: a */
    public int mo10518a(BDLocation bDLocation) {
        double d;
        float f;
        if (bDLocation != null) {
            f = bDLocation.getRadius();
            d = bDLocation.getAltitude();
        } else {
            d = 0.0d;
            f = 0.0f;
        }
        if (this.f1481e == null || f <= 0.0f || d <= 0.0d || bDLocation == null) {
            return 0;
        }
        double d2 = mo10519a(bDLocation.getLongitude(), bDLocation.getLatitude())[0];
        if (d2 == Double.MAX_VALUE) {
            return 0;
        }
        double gpsSwiftRadius = Jni.getGpsSwiftRadius(f, d, d2);
        if (gpsSwiftRadius > 50.0d) {
            return 3;
        }
        return gpsSwiftRadius > 20.0d ? 2 : 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00da, code lost:
        r18 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00dd, code lost:
        r18 = r7;
        r3 = Double.MAX_VALUE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e0, code lost:
        r12 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e2, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e3, code lost:
        r18 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00e6, code lost:
        r18 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0104, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        r18.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        r18.close();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:36:0x00c3, B:55:0x00eb] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00e2 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:15:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0100 A[SYNTHETIC, Splitter:B:61:0x0100] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0104 A[ExcHandler: all (th java.lang.Throwable), PHI: r18 10  PHI: (r18v5 android.database.Cursor) = (r18v7 android.database.Cursor), (r18v17 android.database.Cursor) binds: [B:55:0x00eb, B:36:0x00c3] A[DONT_GENERATE, DONT_INLINE], Splitter:B:36:0x00c3] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x010b A[SYNTHETIC, Splitter:B:67:0x010b] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0114 A[SYNTHETIC, Splitter:B:76:0x0114] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0137  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public double[] mo10519a(double r20, double r22) {
        /*
            r19 = this;
            r8 = r19
            r0 = 2
            double[] r9 = new double[r0]
            android.database.sqlite.SQLiteDatabase r1 = r8.f1481e
            r10 = 0
            r11 = 1
            r12 = 9218868437227405311(0x7fefffffffffffff, double:1.7976931348623157E308)
            r14 = 4666723172467343360(0x40c3880000000000, double:10000.0)
            if (r1 == 0) goto L_0x011e
            r1 = 4591870180066957722(0x3fb999999999999a, double:0.1)
            int r3 = (r20 > r1 ? 1 : (r20 == r1 ? 0 : -1))
            if (r3 <= 0) goto L_0x011e
            int r3 = (r22 > r1 ? 1 : (r22 == r1 ? 0 : -1))
            if (r3 <= 0) goto L_0x011e
            java.util.Locale r1 = java.util.Locale.CHINESE
            java.lang.Object[] r2 = new java.lang.Object[r0]
            r3 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r5 = r20 * r3
            double r5 = java.lang.Math.floor(r5)
            int r5 = (int) r5
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r2[r10] = r5
            double r3 = r3 * r22
            double r3 = java.lang.Math.floor(r3)
            int r3 = (int) r3
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r11] = r3
            java.lang.String r3 = "%d,%d"
            java.lang.String r6 = java.lang.String.format(r1, r3, r2)
            java.lang.String r1 = r8.f1486k
            if (r1 == 0) goto L_0x0059
            boolean r1 = r1.equals(r6)
            if (r1 == 0) goto L_0x0059
            double r12 = r8.f1487l
            double r0 = r8.f1488m
            r2 = r14
            goto L_0x0124
        L_0x0059:
            java.util.concurrent.locks.Lock r1 = com.baidu.location.p014b.C0770a.f1479f
            r1.lock()
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r8.f1481e     // Catch:{ Exception -> 0x010f, all -> 0x0106 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010f, all -> 0x0106 }
            r3.<init>()     // Catch:{ Exception -> 0x010f, all -> 0x0106 }
            java.lang.String r4 = "select * from galdata_new where id = \""
            r3.append(r4)     // Catch:{ Exception -> 0x010f, all -> 0x0106 }
            r3.append(r6)     // Catch:{ Exception -> 0x010f, all -> 0x0106 }
            java.lang.String r4 = "\";"
            r3.append(r4)     // Catch:{ Exception -> 0x010f, all -> 0x0106 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x010f, all -> 0x0106 }
            android.database.Cursor r7 = r2.rawQuery(r3, r1)     // Catch:{ Exception -> 0x010f, all -> 0x0106 }
            if (r7 == 0) goto L_0x00e9
            boolean r1 = r7.moveToFirst()     // Catch:{ Exception -> 0x00e6, all -> 0x00e2 }
            if (r1 == 0) goto L_0x00e9
            double r1 = r7.getDouble(r11)     // Catch:{ Exception -> 0x00e6, all -> 0x00e2 }
            double r3 = r7.getDouble(r0)     // Catch:{ Exception -> 0x00dd, all -> 0x00e2 }
            r0 = 3
            int r0 = r7.getInt(r0)     // Catch:{ Exception -> 0x00da, all -> 0x00e2 }
            int r5 = (r1 > r14 ? 1 : (r1 == r14 ? 0 : -1))
            if (r5 != 0) goto L_0x0095
            r1 = r12
        L_0x0095:
            r16 = 0
            int r5 = (r3 > r16 ? 1 : (r3 == r16 ? 0 : -1))
            if (r5 > 0) goto L_0x009c
            goto L_0x009d
        L_0x009c:
            r12 = r3
        L_0x009d:
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00d4, all -> 0x00e2 }
            r16 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r16
            long r10 = (long) r0     // Catch:{ Exception -> 0x00d4, all -> 0x00e2 }
            long r3 = r3 - r10
            boolean r0 = r8.f1482g     // Catch:{ Exception -> 0x00d4, all -> 0x00e2 }
            if (r0 != 0) goto L_0x00c7
            r10 = 604800(0x93a80, double:2.98811E-318)
            int r0 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c7
            r10 = 4620490555892170752(0x401f47ae20000000, double:7.820000171661377)
            r4 = r1
            r1 = r19
            r2 = r20
            r14 = r4
            r4 = r22
            r0 = r6
            r18 = r7
            r6 = r10
            r1.m1981a(r2, r4, r6)     // Catch:{ Exception -> 0x00d7, all -> 0x0104 }
            goto L_0x00cb
        L_0x00c7:
            r14 = r1
            r0 = r6
            r18 = r7
        L_0x00cb:
            r8.f1486k = r0     // Catch:{ Exception -> 0x00d7, all -> 0x0104 }
            r8.f1487l = r14     // Catch:{ Exception -> 0x00d7, all -> 0x0104 }
            r8.f1488m = r12     // Catch:{ Exception -> 0x00d7, all -> 0x0104 }
            r0 = r12
            r12 = r14
            goto L_0x00fe
        L_0x00d4:
            r14 = r1
            r18 = r7
        L_0x00d7:
            r3 = r12
            r12 = r14
            goto L_0x0112
        L_0x00da:
            r18 = r7
            goto L_0x00e0
        L_0x00dd:
            r18 = r7
            r3 = r12
        L_0x00e0:
            r12 = r1
            goto L_0x0112
        L_0x00e2:
            r0 = move-exception
            r18 = r7
            goto L_0x0109
        L_0x00e6:
            r18 = r7
            goto L_0x0111
        L_0x00e9:
            r18 = r7
            boolean r0 = r8.f1482g     // Catch:{ Exception -> 0x0111, all -> 0x0104 }
            if (r0 != 0) goto L_0x00fd
            r6 = 4620490555892170752(0x401f47ae20000000, double:7.820000171661377)
            r1 = r19
            r2 = r20
            r4 = r22
            r1.m1981a(r2, r4, r6)     // Catch:{ Exception -> 0x0111, all -> 0x0104 }
        L_0x00fd:
            r0 = r12
        L_0x00fe:
            if (r18 == 0) goto L_0x0118
            r18.close()     // Catch:{ Exception -> 0x0118 }
            goto L_0x0118
        L_0x0104:
            r0 = move-exception
            goto L_0x0109
        L_0x0106:
            r0 = move-exception
            r18 = r1
        L_0x0109:
            if (r18 == 0) goto L_0x010e
            r18.close()     // Catch:{ Exception -> 0x010e }
        L_0x010e:
            throw r0
        L_0x010f:
            r18 = r1
        L_0x0111:
            r3 = r12
        L_0x0112:
            if (r18 == 0) goto L_0x0117
            r18.close()     // Catch:{ Exception -> 0x0117 }
        L_0x0117:
            r0 = r3
        L_0x0118:
            java.util.concurrent.locks.Lock r2 = com.baidu.location.p014b.C0770a.f1479f
            r2.unlock()
            goto L_0x011f
        L_0x011e:
            r0 = r12
        L_0x011f:
            r2 = 4666723172467343360(0x40c3880000000000, double:10000.0)
        L_0x0124:
            int r4 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x012c
            r4 = 0
            r9[r4] = r2
            goto L_0x012f
        L_0x012c:
            r4 = 0
            r9[r4] = r12
        L_0x012f:
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x0137
            r4 = 1
            r9[r4] = r2
            goto L_0x013a
        L_0x0137:
            r4 = 1
            r9[r4] = r0
        L_0x013a:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p014b.C0770a.mo10519a(double, double):double[]");
    }

    /* renamed from: b */
    public void mo10520b() {
        try {
            File file = new File(f1478d);
            if (!file.exists()) {
                file.createNewFile();
            }
            if (file.exists()) {
                this.f1481e = SQLiteDatabase.openOrCreateDatabase(file, (SQLiteDatabase.CursorFactory) null);
                Cursor rawQuery = this.f1481e.rawQuery("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='galdata'", null);
                if (rawQuery.moveToFirst()) {
                    if (rawQuery.getInt(0) == 0) {
                        this.f1481e.execSQL("CREATE TABLE IF NOT EXISTS galdata_new(id CHAR(40) PRIMARY KEY,aldata DOUBLE, sigma DOUBLE,tt INT);");
                    } else {
                        this.f1481e.execSQL("DROP TABLE galdata");
                        this.f1481e.execSQL("CREATE TABLE galdata_new(id CHAR(40) PRIMARY KEY,aldata DOUBLE, sigma DOUBLE,tt INT);");
                    }
                    this.f1481e.execSQL("CREATE TABLE IF NOT EXISTS locStateData(id CHAR(40) PRIMARY KEY,state INT);");
                }
                this.f1481e.setVersion(1);
                rawQuery.close();
            }
        } catch (Exception unused) {
            this.f1481e = null;
        }
    }

    /* renamed from: c */
    public void mo10521c() {
        SQLiteDatabase sQLiteDatabase = this.f1481e;
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.close();
            } catch (Exception unused) {
            } catch (Throwable th) {
                this.f1481e = null;
                throw th;
            }
            this.f1481e = null;
        }
    }
}
