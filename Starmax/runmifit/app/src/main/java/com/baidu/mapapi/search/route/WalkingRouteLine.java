package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.RouteNode;
import com.baidu.mapapi.search.core.RouteStep;
import java.util.List;

public class WalkingRouteLine extends RouteLine<WalkingStep> implements Parcelable {
    public static final Parcelable.Creator<WalkingRouteLine> CREATOR = new C1002r();

    public static class WalkingStep extends RouteStep implements Parcelable {
        public static final Parcelable.Creator<WalkingStep> CREATOR = new C1003s();

        /* renamed from: d */
        private int f3117d;

        /* renamed from: e */
        private RouteNode f3118e;

        /* renamed from: f */
        private RouteNode f3119f;

        /* renamed from: g */
        private String f3120g;

        /* renamed from: h */
        private String f3121h;

        /* renamed from: i */
        private String f3122i;

        /* renamed from: j */
        private String f3123j;

        public WalkingStep() {
        }

        protected WalkingStep(Parcel parcel) {
            super(parcel);
            this.f3117d = parcel.readInt();
            this.f3118e = (RouteNode) parcel.readParcelable(RouteNode.class.getClassLoader());
            this.f3119f = (RouteNode) parcel.readParcelable(RouteNode.class.getClassLoader());
            this.f3120g = parcel.readString();
            this.f3121h = parcel.readString();
            this.f3122i = parcel.readString();
            this.f3123j = parcel.readString();
        }

        public int describeContents() {
            return 0;
        }

        public int getDirection() {
            return this.f3117d;
        }

        public RouteNode getEntrance() {
            return this.f3118e;
        }

        public String getEntranceInstructions() {
            return this.f3121h;
        }

        public RouteNode getExit() {
            return this.f3119f;
        }

        public String getExitInstructions() {
            return this.f3122i;
        }

        public String getInstructions() {
            return this.f3123j;
        }

        public List<LatLng> getWayPoints() {
            if (this.mWayPoints == null) {
                this.mWayPoints = CoordUtil.decodeLocationList(this.f3120g);
            }
            return this.mWayPoints;
        }

        public void setDirection(int i) {
            this.f3117d = i;
        }

        public void setEntrance(RouteNode routeNode) {
            this.f3118e = routeNode;
        }

        public void setEntranceInstructions(String str) {
            this.f3121h = str;
        }

        public void setExit(RouteNode routeNode) {
            this.f3119f = routeNode;
        }

        public void setExitInstructions(String str) {
            this.f3122i = str;
        }

        public void setInstructions(String str) {
            this.f3123j = str;
        }

        public void setPathString(String str) {
            this.f3120g = str;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, 1);
            parcel.writeInt(this.f3117d);
            parcel.writeParcelable(this.f3118e, 1);
            parcel.writeParcelable(this.f3119f, 1);
            parcel.writeString(this.f3120g);
            parcel.writeString(this.f3121h);
            parcel.writeString(this.f3122i);
            parcel.writeString(this.f3123j);
        }
    }

    public WalkingRouteLine() {
    }

    protected WalkingRouteLine(Parcel parcel) {
        super(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public List<WalkingStep> getAllStep() {
        return super.getAllStep();
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.setType(RouteLine.TYPE.WALKSTEP);
        super.writeToParcel(parcel, 1);
    }
}
