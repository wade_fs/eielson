package com.baidu.mobstat;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.webkit.WebView;
import android.webkit.WebViewFragment;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;
import java.util.HashMap;
import java.util.Locale;

/* renamed from: com.baidu.mobstat.aq */
public class C1253aq {

    /* renamed from: b */
    private static final C1253aq f4322b = new C1253aq();

    /* renamed from: a */
    private HashMap<String, String> f4323a = new HashMap<>();

    /* renamed from: a */
    public static C1253aq m4611a() {
        return f4322b;
    }

    private C1253aq() {
        m4614b();
    }

    /* renamed from: a */
    private void m4613a(Throwable th) {
        if (C1258av.m4636c().mo13945b()) {
            C1258av.m4636c().mo13943b(th.toString());
        }
    }

    /* renamed from: b */
    private void m4614b() {
        if (Build.VERSION.SDK_INT >= 14 && this.f4323a.size() == 0) {
            try {
                m4612a(AutoCompleteTextView.class.getSimpleName(), "A0");
            } catch (Throwable th) {
                m4613a(th);
            }
            try {
                m4612a(ActionBar.class.getSimpleName(), "A1");
            } catch (Throwable th2) {
                m4613a(th2);
            }
            try {
                m4612a(AlertDialog.class.getSimpleName(), "A2");
            } catch (Throwable th3) {
                m4613a(th3);
            }
            try {
                m4612a(Button.class.getSimpleName(), "B0");
            } catch (Throwable th4) {
                m4613a(th4);
            }
            try {
                m4612a(CheckBox.class.getSimpleName(), "C0");
            } catch (Throwable th5) {
                m4613a(th5);
            }
            try {
                m4612a(CheckedTextView.class.getSimpleName(), "C1");
            } catch (Throwable th6) {
                m4613a(th6);
            }
            try {
                m4612a(Class.forName("com.android.internal.policy.DecorView").getSimpleName(), "D0");
            } catch (Throwable th7) {
                m4613a(th7);
            }
            try {
                m4612a(DrawerLayout.class.getSimpleName(), "D1");
            } catch (Throwable th8) {
                m4613a(th8);
            }
            try {
                m4612a(EditText.class.getSimpleName(), "E0");
            } catch (Throwable th9) {
                m4613a(th9);
            }
            try {
                m4612a(ExpandableListView.class.getSimpleName(), "E1");
            } catch (Throwable th10) {
                m4613a(th10);
            }
            try {
                m4612a(FrameLayout.class.getSimpleName(), "F0");
            } catch (Throwable th11) {
                m4613a(th11);
            }
            try {
                m4612a(Fragment.class.getSimpleName(), "F1");
            } catch (Throwable th12) {
                m4613a(th12);
            }
            try {
                m4612a(Gallery.class.getSimpleName(), "G0");
            } catch (Throwable th13) {
                m4613a(th13);
            }
            try {
                m4612a(GridView.class.getSimpleName(), "G1");
            } catch (Throwable th14) {
                m4613a(th14);
            }
            try {
                m4612a(HorizontalScrollView.class.getSimpleName(), "H0");
            } catch (Throwable th15) {
                m4613a(th15);
            }
            try {
                m4612a(ImageButton.class.getSimpleName(), "I0");
            } catch (Throwable th16) {
                m4613a(th16);
            }
            try {
                m4612a(ImageSwitcher.class.getSimpleName(), "I1");
            } catch (Throwable th17) {
                m4613a(th17);
            }
            try {
                m4612a(ImageView.class.getSimpleName(), "I2");
            } catch (Throwable th18) {
                m4613a(th18);
            }
            try {
                m4612a(LinearLayout.class.getSimpleName(), "L0");
            } catch (Throwable th19) {
                m4613a(th19);
            }
            try {
                m4612a(ListView.class.getSimpleName(), "L1");
            } catch (Throwable th20) {
                m4613a(th20);
            }
            try {
                m4612a(ListFragment.class.getSimpleName(), "L2");
            } catch (Throwable th21) {
                m4613a(th21);
            }
            try {
                m4612a(MultiAutoCompleteTextView.class.getSimpleName(), "M0");
            } catch (Throwable th22) {
                m4613a(th22);
            }
            try {
                m4612a(NestedScrollView.class.getSimpleName(), "N0");
            } catch (Throwable th23) {
                m4613a(th23);
            }
            try {
                m4612a(ProgressBar.class.getSimpleName(), "P0");
            } catch (Throwable th24) {
                m4613a(th24);
            }
            try {
                m4612a(RadioButton.class.getSimpleName(), "R0");
            } catch (Throwable th25) {
                m4613a(th25);
            }
            try {
                m4612a(RadioGroup.class.getSimpleName(), "R1");
            } catch (Throwable th26) {
                m4613a(th26);
            }
            try {
                m4612a(RatingBar.class.getSimpleName(), "R2");
            } catch (Throwable th27) {
                m4613a(th27);
            }
            try {
                m4612a(RelativeLayout.class.getSimpleName(), "R3");
            } catch (Throwable th28) {
                m4613a(th28);
            }
            try {
                m4612a(RecyclerView.class.getSimpleName(), "R4");
            } catch (Throwable th29) {
                m4613a(th29);
            }
            try {
                m4612a(ScrollView.class.getSimpleName(), "S0");
            } catch (Throwable th30) {
                m4613a(th30);
            }
            try {
                m4612a(SearchView.class.getSimpleName(), "S1");
            } catch (Throwable th31) {
                m4613a(th31);
            }
            try {
                m4612a(SeekBar.class.getSimpleName(), "S2");
            } catch (Throwable th32) {
                m4613a(th32);
            }
            try {
                m4612a(Spinner.class.getSimpleName(), "S3");
            } catch (Throwable th33) {
                m4613a(th33);
            }
            try {
                m4612a(Switch.class.getSimpleName(), "S4");
            } catch (Throwable th34) {
                m4613a(th34);
            }
            try {
                m4612a(SurfaceView.class.getSimpleName(), "S5");
            } catch (Throwable th35) {
                m4613a(th35);
            }
            try {
                m4612a(SwipeRefreshLayout.class.getSimpleName(), "S6");
            } catch (Throwable th36) {
                m4613a(th36);
            }
            try {
                m4612a(TabHost.class.getSimpleName(), "T0");
            } catch (Throwable th37) {
                m4613a(th37);
            }
            try {
                m4612a(TableLayout.class.getSimpleName(), "T1");
            } catch (Throwable th38) {
                m4613a(th38);
            }
            try {
                m4612a(TableRow.class.getSimpleName(), "T2");
            } catch (Throwable th39) {
                m4613a(th39);
            }
            try {
                m4612a(TabWidget.class.getSimpleName(), "T3");
            } catch (Throwable th40) {
                m4613a(th40);
            }
            try {
                m4612a(TextSwitcher.class.getSimpleName(), "T4");
            } catch (Throwable th41) {
                m4613a(th41);
            }
            try {
                m4612a(TextView.class.getSimpleName(), "T5");
            } catch (Throwable th42) {
                m4613a(th42);
            }
            try {
                m4612a(Toast.class.getSimpleName(), "T6");
            } catch (Throwable th43) {
                m4613a(th43);
            }
            try {
                m4612a(ToggleButton.class.getSimpleName(), "T7");
            } catch (Throwable th44) {
                m4613a(th44);
            }
            try {
                m4612a(TextureView.class.getSimpleName(), "T8");
            } catch (Throwable th45) {
                m4613a(th45);
            }
            try {
                m4612a(Toolbar.class.getSimpleName(), "T9");
            } catch (Throwable th46) {
                m4613a(th46);
            }
            try {
                m4612a(View.class.getSimpleName(), "V0");
            } catch (Throwable th47) {
                m4613a(th47);
            }
            try {
                m4612a(ViewGroup.class.getSimpleName(), "V1");
            } catch (Throwable th48) {
                m4613a(th48);
            }
            try {
                m4612a(ViewStub.class.getSimpleName(), "V2");
            } catch (Throwable th49) {
                m4613a(th49);
            }
            try {
                m4612a(VideoView.class.getSimpleName(), "V3");
            } catch (Throwable th50) {
                m4613a(th50);
            }
            try {
                m4612a(ViewSwitcher.class.getSimpleName(), "V4");
            } catch (Throwable th51) {
                m4613a(th51);
            }
            try {
                m4612a(ViewFlipper.class.getSimpleName(), "V5");
            } catch (Throwable th52) {
                m4613a(th52);
            }
            try {
                m4612a(ViewPager.class.getSimpleName(), "V6");
            } catch (Throwable th53) {
                m4613a(th53);
            }
            try {
                m4612a(WebView.class.getSimpleName(), "W0");
            } catch (Throwable th54) {
                m4613a(th54);
            }
            try {
                m4612a(WebViewFragment.class.getSimpleName(), "W1");
            } catch (Throwable th55) {
                m4613a(th55);
            }
        }
    }

    /* renamed from: a */
    private void m4612a(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !this.f4323a.containsKey(str)) {
            this.f4323a.put(str, str2.toUpperCase(Locale.ENGLISH));
        }
    }

    /* renamed from: a */
    public String mo13939a(String str) {
        return this.f4323a.get(str);
    }
}
