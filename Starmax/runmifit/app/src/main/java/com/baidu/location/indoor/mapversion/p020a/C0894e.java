package com.baidu.location.indoor.mapversion.p020a;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/* renamed from: com.baidu.location.indoor.mapversion.a.e */
class C0894e implements Runnable {

    /* renamed from: a */
    final /* synthetic */ C0892d f2249a;

    C0894e(C0892d dVar) {
        this.f2249a = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.indoor.mapversion.a.d.a(com.baidu.location.indoor.mapversion.a.d, boolean):boolean
     arg types: [com.baidu.location.indoor.mapversion.a.d, int]
     candidates:
      com.baidu.location.indoor.mapversion.a.d.a(com.baidu.location.indoor.mapversion.a.d, java.lang.String):java.lang.String
      com.baidu.location.indoor.mapversion.a.d.a(java.lang.String, java.lang.String):com.baidu.location.indoor.mapversion.a.c
      com.baidu.location.indoor.mapversion.a.d.a(com.baidu.location.indoor.mapversion.a.d, boolean):boolean */
    public void run() {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://loc.map.baidu.com/check_indoor_data_update").openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setReadTimeout(60000);
            httpURLConnection.setConnectTimeout(60000);
            StringBuilder sb = new StringBuilder();
            sb.append("&data_type=ps");
            sb.append("&bid=");
            sb.append(this.f2249a.f2247e);
            if (this.f2249a.f2248f != null) {
                sb.append("&md5=");
                sb.append(this.f2249a.f2248f);
            }
            httpURLConnection.getOutputStream().write(sb.toString().getBytes());
            if (httpURLConnection.getResponseCode() == 200) {
                String headerField = httpURLConnection.getHeaderField("Hash");
                if (headerField != null && (this.f2249a.f2248f == null || !headerField.equalsIgnoreCase(this.f2249a.f2248f))) {
                    String unused = this.f2249a.f2248f = headerField;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    FileWriter fileWriter = new FileWriter(new File(this.f2249a.f2245c, this.f2249a.f2247e));
                    fileWriter.write(headerField + "\n");
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        fileWriter.write(readLine);
                        fileWriter.write("\n");
                    }
                    fileWriter.flush();
                    fileWriter.close();
                }
                boolean unused2 = this.f2249a.m2726d();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            boolean unused3 = this.f2249a.f2246d = false;
            throw th;
        }
        boolean unused4 = this.f2249a.f2246d = false;
    }
}
