package com.baidu.location;

import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mobstat.Config;

public class Jni {

    /* renamed from: a */
    private static int f1154a = 0;

    /* renamed from: b */
    private static int f1155b = 1;

    /* renamed from: c */
    private static int f1156c = 2;

    /* renamed from: d */
    private static int f1157d = 11;

    /* renamed from: e */
    private static int f1158e = 12;

    /* renamed from: f */
    private static int f1159f = 13;

    /* renamed from: g */
    private static int f1160g = 14;

    /* renamed from: h */
    private static int f1161h = 15;

    /* renamed from: i */
    private static int f1162i = 1024;

    /* renamed from: j */
    private static boolean f1163j = true;

    static {
        try {
            System.loadLibrary("locSDK7b");
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    private static native String m1659a(byte[] bArr, int i);

    /* renamed from: b */
    private static native String m1660b(double d, double d2, int i, int i2);

    /* renamed from: c */
    private static native String m1661c(byte[] bArr, int i);

    public static double[] coorEncrypt(double d, double d2, String str) {
        double[] dArr = {0.0d, 0.0d};
        if (f1163j) {
            return dArr;
        }
        int i = -1;
        if (str.equals(BDLocation.BDLOCATION_GCJ02_TO_BD09)) {
            i = f1154a;
        } else if (str.equals("bd09ll")) {
            i = f1155b;
        } else if (str.equals(CoordinateType.GCJ02)) {
            i = f1156c;
        } else if (str.equals(BDLocation.BDLOCATION_WGS84_TO_GCJ02)) {
            i = f1157d;
        } else if (str.equals(BDLocation.BDLOCATION_BD09_TO_GCJ02)) {
            i = f1158e;
        } else if (str.equals(BDLocation.BDLOCATION_BD09LL_TO_GCJ02)) {
            i = f1159f;
        } else if (str.equals("wgs842mc")) {
            i = f1161h;
        }
        try {
            String[] split = m1660b(d, d2, str.equals("gcj2wgs") ? 16 : i, 132456).split(Config.TRACE_TODAY_VISIT_SPLIT);
            dArr[0] = Double.parseDouble(split[0]);
            dArr[1] = Double.parseDouble(split[1]);
        } catch (UnsatisfiedLinkError unused) {
        }
        return dArr;
    }

    /* renamed from: ee */
    private static native String m1662ee(String str, int i);

    public static String en1(String str) {
        if (f1163j) {
            return "err!";
        }
        if (str == null) {
            return "null";
        }
        byte[] bytes = str.getBytes();
        byte[] bArr = new byte[f1162i];
        int length = bytes.length;
        if (length > 740) {
            length = 740;
        }
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (bytes[i2] != 0) {
                bArr[i] = bytes[i2];
                i++;
            }
        }
        try {
            return m1659a(bArr, 132456);
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
            return "err!";
        }
    }

    public static String encode(String str) {
        if (f1163j) {
            return "err!";
        }
        return en1(str) + "|tp=3";
    }

    public static String encode2(String str) {
        if (f1163j) {
            return "err!";
        }
        if (str == null) {
            return "null";
        }
        try {
            return m1661c(str.getBytes(), 132456);
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
            return "err!";
        }
    }

    public static Long encode3(String str) {
        String str2;
        if (f1163j) {
            return null;
        }
        try {
            str2 = new String(str.getBytes(), "UTF-8");
        } catch (Exception unused) {
            str2 = "";
        }
        try {
            return Long.valueOf(murmur(str2));
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
            return null;
        }
    }

    private static native String encodeNotLimit(String str, int i);

    public static String encodeOfflineLocationUpdateRequest(String str) {
        String str2;
        String str3 = "err!";
        if (f1163j) {
            return str3;
        }
        try {
            str2 = new String(str.getBytes(), "UTF-8");
        } catch (Exception unused) {
            str2 = "";
        }
        try {
            str3 = encodeNotLimit(str2, 132456);
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
        }
        return str3 + "|tp=3";
    }

    public static String encodeTp4(String str) {
        String str2;
        String str3 = "err!";
        if (f1163j) {
            return str3;
        }
        try {
            str2 = new String(str.getBytes(), "UTF-8");
        } catch (Exception unused) {
            str2 = "";
        }
        try {
            str3 = m1662ee(str2, 132456);
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
        }
        return str3 + "|tp=4";
    }

    public static double getGpsSwiftRadius(float f, double d, double d2) {
        if (f1163j) {
            return 0.0d;
        }
        try {
            return gsr(f, d, d2);
        } catch (UnsatisfiedLinkError unused) {
            return 0.0d;
        }
    }

    private static native double gsr(float f, double d, double d2);

    private static native long murmur(String str);
}
