package com.baidu.mapsdkplatform.comapi.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapsdkplatform.comapi.commonutils.C1053a;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.ak */
public class C1074ak extends LinearLayout implements View.OnTouchListener {

    /* renamed from: a */
    private ImageView f3457a;

    /* renamed from: b */
    private ImageView f3458b;

    /* renamed from: c */
    private Context f3459c;

    /* renamed from: d */
    private Bitmap f3460d;

    /* renamed from: e */
    private Bitmap f3461e;

    /* renamed from: f */
    private Bitmap f3462f;

    /* renamed from: g */
    private Bitmap f3463g;

    /* renamed from: h */
    private Bitmap f3464h;

    /* renamed from: i */
    private Bitmap f3465i;

    /* renamed from: j */
    private Bitmap f3466j;

    /* renamed from: k */
    private Bitmap f3467k;

    /* renamed from: l */
    private int f3468l;

    /* renamed from: m */
    private boolean f3469m = false;

    /* renamed from: n */
    private boolean f3470n = false;

    @Deprecated
    public C1074ak(Context context) {
        super(context);
        this.f3459c = context;
        m3525c();
        if (this.f3460d != null && this.f3461e != null && this.f3462f != null && this.f3463g != null) {
            this.f3457a = new ImageView(this.f3459c);
            this.f3458b = new ImageView(this.f3459c);
            this.f3457a.setImageBitmap(this.f3460d);
            this.f3458b.setImageBitmap(this.f3462f);
            this.f3468l = m3522a(this.f3462f.getHeight() / 6);
            m3524a(this.f3457a, "main_topbtn_up.9.png");
            m3524a(this.f3458b, "main_bottombtn_up.9.png");
            this.f3457a.setId(0);
            this.f3458b.setId(1);
            this.f3457a.setClickable(true);
            this.f3458b.setClickable(true);
            this.f3457a.setOnTouchListener(this);
            this.f3458b.setOnTouchListener(this);
            setOrientation(1);
            setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
            addView(this.f3457a);
            addView(this.f3458b);
            this.f3470n = true;
        }
    }

    public C1074ak(Context context, boolean z) {
        super(context);
        this.f3459c = context;
        this.f3469m = z;
        this.f3457a = new ImageView(this.f3459c);
        this.f3458b = new ImageView(this.f3459c);
        if (z) {
            m3526d();
            if (this.f3464h != null && this.f3465i != null && this.f3466j != null && this.f3467k != null) {
                this.f3457a.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                this.f3458b.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                this.f3457a.setImageBitmap(this.f3464h);
                this.f3458b.setImageBitmap(this.f3466j);
                setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                setOrientation(0);
            } else {
                return;
            }
        } else {
            m3525c();
            Bitmap bitmap = this.f3460d;
            if (bitmap != null && this.f3461e != null && this.f3462f != null && this.f3463g != null) {
                this.f3457a.setImageBitmap(bitmap);
                this.f3458b.setImageBitmap(this.f3462f);
                this.f3468l = m3522a(this.f3462f.getHeight() / 6);
                m3524a(this.f3457a, "main_topbtn_up.9.png");
                m3524a(this.f3458b, "main_bottombtn_up.9.png");
                setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                setOrientation(1);
            } else {
                return;
            }
        }
        this.f3457a.setId(0);
        this.f3458b.setId(1);
        this.f3457a.setClickable(true);
        this.f3458b.setClickable(true);
        this.f3457a.setOnTouchListener(this);
        this.f3458b.setOnTouchListener(this);
        addView(this.f3457a);
        addView(this.f3458b);
        this.f3470n = true;
    }

    /* renamed from: a */
    private int m3522a(int i) {
        return (int) ((this.f3459c.getResources().getDisplayMetrics().density * ((float) i)) + 0.5f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* renamed from: a */
    private Bitmap m3523a(String str) {
        Matrix matrix = new Matrix();
        int densityDpi = SysOSUtil.getDensityDpi();
        float f = densityDpi > 480 ? 1.8f : (densityDpi <= 320 || densityDpi > 480) ? 1.2f : 1.5f;
        matrix.postScale(f, f);
        Bitmap a = C1053a.m3464a(str, this.f3459c);
        return Bitmap.createBitmap(a, 0, 0, a.getWidth(), a.getHeight(), matrix, true);
    }

    /* renamed from: a */
    private void m3524a(View view, String str) {
        Bitmap a = C1053a.m3464a(str, this.f3459c);
        byte[] ninePatchChunk = a.getNinePatchChunk();
        NinePatch.isNinePatchChunk(ninePatchChunk);
        view.setBackgroundDrawable(new NinePatchDrawable(a, ninePatchChunk, new Rect(), null));
        int i = this.f3468l;
        view.setPadding(i, i, i, i);
    }

    /* renamed from: c */
    private void m3525c() {
        this.f3460d = m3523a("main_icon_zoomin.png");
        this.f3461e = m3523a("main_icon_zoomin_dis.png");
        this.f3462f = m3523a("main_icon_zoomout.png");
        this.f3463g = m3523a("main_icon_zoomout_dis.png");
    }

    /* renamed from: d */
    private void m3526d() {
        this.f3464h = m3523a("wear_zoom_in.png");
        this.f3465i = m3523a("wear_zoom_in_pressed.png");
        this.f3466j = m3523a("wear_zoon_out.png");
        this.f3467k = m3523a("wear_zoom_out_pressed.png");
    }

    /* renamed from: a */
    public void mo12978a(View.OnClickListener onClickListener) {
        this.f3457a.setOnClickListener(onClickListener);
    }

    /* renamed from: a */
    public void mo12979a(boolean z) {
        ImageView imageView;
        Bitmap bitmap;
        this.f3457a.setEnabled(z);
        if (!z) {
            imageView = this.f3457a;
            bitmap = this.f3461e;
        } else {
            imageView = this.f3457a;
            bitmap = this.f3460d;
        }
        imageView.setImageBitmap(bitmap);
    }

    /* renamed from: a */
    public boolean mo12980a() {
        return this.f3470n;
    }

    /* renamed from: b */
    public void mo12981b() {
        Bitmap bitmap = this.f3460d;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f3460d.recycle();
            this.f3460d = null;
        }
        Bitmap bitmap2 = this.f3461e;
        if (bitmap2 != null && !bitmap2.isRecycled()) {
            this.f3461e.recycle();
            this.f3461e = null;
        }
        Bitmap bitmap3 = this.f3462f;
        if (bitmap3 != null && !bitmap3.isRecycled()) {
            this.f3462f.recycle();
            this.f3462f = null;
        }
        Bitmap bitmap4 = this.f3463g;
        if (bitmap4 != null && !bitmap4.isRecycled()) {
            this.f3463g.recycle();
            this.f3463g = null;
        }
        Bitmap bitmap5 = this.f3464h;
        if (bitmap5 != null && !bitmap5.isRecycled()) {
            this.f3464h.recycle();
            this.f3464h = null;
        }
        Bitmap bitmap6 = this.f3465i;
        if (bitmap6 != null && !bitmap6.isRecycled()) {
            this.f3465i.recycle();
            this.f3465i = null;
        }
        Bitmap bitmap7 = this.f3466j;
        if (bitmap7 != null && !bitmap7.isRecycled()) {
            this.f3466j.recycle();
            this.f3466j = null;
        }
        Bitmap bitmap8 = this.f3467k;
        if (bitmap8 != null && !bitmap8.isRecycled()) {
            this.f3467k.recycle();
            this.f3467k = null;
        }
    }

    /* renamed from: b */
    public void mo12982b(View.OnClickListener onClickListener) {
        this.f3458b.setOnClickListener(onClickListener);
    }

    /* renamed from: b */
    public void mo12983b(boolean z) {
        ImageView imageView;
        Bitmap bitmap;
        this.f3458b.setEnabled(z);
        if (!z) {
            imageView = this.f3458b;
            bitmap = this.f3463g;
        } else {
            imageView = this.f3458b;
            bitmap = this.f3462f;
        }
        imageView.setImageBitmap(bitmap);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        Bitmap bitmap;
        ImageView imageView;
        String str;
        ImageView imageView2;
        if (!(view instanceof ImageView)) {
            return false;
        }
        int id = ((ImageView) view).getId();
        if (id != 0) {
            if (id != 1) {
                return false;
            }
            if (motionEvent.getAction() == 0) {
                if (!this.f3469m) {
                    imageView2 = this.f3458b;
                    str = "main_bottombtn_down.9.png";
                } else {
                    imageView = this.f3458b;
                    bitmap = this.f3467k;
                    imageView.setImageBitmap(bitmap);
                    return false;
                }
            } else if (motionEvent.getAction() != 1) {
                return false;
            } else {
                if (!this.f3469m) {
                    imageView2 = this.f3458b;
                    str = "main_bottombtn_up.9.png";
                } else {
                    imageView = this.f3458b;
                    bitmap = this.f3466j;
                    imageView.setImageBitmap(bitmap);
                    return false;
                }
            }
        } else if (motionEvent.getAction() == 0) {
            if (!this.f3469m) {
                imageView2 = this.f3457a;
                str = "main_topbtn_down.9.png";
            } else {
                imageView = this.f3457a;
                bitmap = this.f3465i;
                imageView.setImageBitmap(bitmap);
                return false;
            }
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            if (!this.f3469m) {
                imageView2 = this.f3457a;
                str = "main_topbtn_up.9.png";
            } else {
                imageView = this.f3457a;
                bitmap = this.f3464h;
                imageView.setImageBitmap(bitmap);
                return false;
            }
        }
        m3524a(imageView2, str);
        return false;
    }
}
