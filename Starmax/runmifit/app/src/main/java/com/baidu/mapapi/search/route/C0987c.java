package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.route.c */
final class C0987c implements Parcelable.Creator<BikingRouteResult> {
    C0987c() {
    }

    /* renamed from: a */
    public BikingRouteResult createFromParcel(Parcel parcel) {
        return new BikingRouteResult(parcel);
    }

    /* renamed from: a */
    public BikingRouteResult[] newArray(int i) {
        return new BikingRouteResult[i];
    }
}
