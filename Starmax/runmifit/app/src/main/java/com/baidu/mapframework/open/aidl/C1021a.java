package com.baidu.mapframework.open.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.baidu.mapframework.open.aidl.C1024b;

/* renamed from: com.baidu.mapframework.open.aidl.a */
public interface C1021a extends IInterface {

    /* renamed from: com.baidu.mapframework.open.aidl.a$a */
    public static abstract class C1022a extends Binder implements C1021a {

        /* renamed from: com.baidu.mapframework.open.aidl.a$a$a */
        private static class C1023a implements C1021a {

            /* renamed from: a */
            private IBinder f3281a;

            C1023a(IBinder iBinder) {
                this.f3281a = iBinder;
            }

            /* renamed from: a */
            public void mo12858a(C1024b bVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.baidu.mapframework.open.aidl.IMapOpenService");
                    obtain.writeStrongBinder(bVar != null ? bVar.asBinder() : null);
                    this.f3281a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f3281a;
            }
        }

        /* renamed from: a */
        public static C1021a m3320a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.baidu.mapframework.open.aidl.IMapOpenService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof C1021a)) ? new C1023a(iBinder) : (C1021a) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("com.baidu.mapframework.open.aidl.IMapOpenService");
                mo12858a(C1024b.C1025a.m3323b(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("com.baidu.mapframework.open.aidl.IMapOpenService");
                return true;
            }
        }
    }

    /* renamed from: a */
    void mo12858a(C1024b bVar) throws RemoteException;
}
