package com.baidu.mobstat;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import java.util.LinkedHashSet;
import java.util.Set;

public class ActivityLifeObserver {

    /* renamed from: b */
    private static final ActivityLifeObserver f3963b = new ActivityLifeObserver();

    /* renamed from: a */
    private boolean f3964a;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public Set<IActivityLifeCallback> f3965c = new LinkedHashSet();

    public interface IActivityLifeCallback {
        void onActivityCreated(Activity activity, Bundle bundle);

        void onActivityDestroyed(Activity activity);

        void onActivityPaused(Activity activity);

        void onActivityResumed(Activity activity);

        void onActivitySaveInstanceState(Activity activity, Bundle bundle);

        void onActivityStarted(Activity activity);

        void onActivityStopped(Activity activity);
    }

    public static ActivityLifeObserver instance() {
        return f3963b;
    }

    public void addObserver(IActivityLifeCallback iActivityLifeCallback) {
        synchronized (this.f3965c) {
            this.f3965c.add(iActivityLifeCallback);
        }
    }

    public void clearObservers() {
        synchronized (this.f3965c) {
            this.f3965c.clear();
        }
    }

    public void removeObserver(IActivityLifeCallback iActivityLifeCallback) {
        synchronized (this.f3965c) {
            this.f3965c.remove(iActivityLifeCallback);
        }
    }

    public void registerActivityLifeCallback(Context context) {
        if (!this.f3964a && Build.VERSION.SDK_INT >= 14) {
            doRegister(context);
            this.f3964a = true;
        }
    }

    public void doRegister(Context context) {
        try {
            ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                /* class com.baidu.mobstat.ActivityLifeObserver.C11901 */

                public void onActivityResumed(Activity activity) {
                    synchronized (ActivityLifeObserver.this.f3965c) {
                        for (IActivityLifeCallback iActivityLifeCallback : ActivityLifeObserver.this.f3965c) {
                            iActivityLifeCallback.onActivityResumed(activity);
                        }
                    }
                }

                public void onActivityPaused(Activity activity) {
                    synchronized (ActivityLifeObserver.this.f3965c) {
                        for (IActivityLifeCallback iActivityLifeCallback : ActivityLifeObserver.this.f3965c) {
                            iActivityLifeCallback.onActivityPaused(activity);
                        }
                    }
                }

                public void onActivityCreated(Activity activity, Bundle bundle) {
                    synchronized (ActivityLifeObserver.this.f3965c) {
                        for (IActivityLifeCallback iActivityLifeCallback : ActivityLifeObserver.this.f3965c) {
                            iActivityLifeCallback.onActivityCreated(activity, bundle);
                        }
                    }
                }

                public void onActivityStarted(Activity activity) {
                    synchronized (ActivityLifeObserver.this.f3965c) {
                        for (IActivityLifeCallback iActivityLifeCallback : ActivityLifeObserver.this.f3965c) {
                            iActivityLifeCallback.onActivityStarted(activity);
                        }
                    }
                }

                public void onActivityStopped(Activity activity) {
                    synchronized (ActivityLifeObserver.this.f3965c) {
                        for (IActivityLifeCallback iActivityLifeCallback : ActivityLifeObserver.this.f3965c) {
                            iActivityLifeCallback.onActivityStopped(activity);
                        }
                    }
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                    synchronized (ActivityLifeObserver.this.f3965c) {
                        for (IActivityLifeCallback iActivityLifeCallback : ActivityLifeObserver.this.f3965c) {
                            iActivityLifeCallback.onActivitySaveInstanceState(activity, bundle);
                        }
                    }
                }

                public void onActivityDestroyed(Activity activity) {
                    synchronized (ActivityLifeObserver.this.f3965c) {
                        for (IActivityLifeCallback iActivityLifeCallback : ActivityLifeObserver.this.f3965c) {
                            iActivityLifeCallback.onActivityDestroyed(activity);
                        }
                    }
                }
            });
        } catch (Exception unused) {
            C1256at.m4629c().mo13941a("registerActivityLifecycleCallbacks encounter exception");
        }
    }
}
