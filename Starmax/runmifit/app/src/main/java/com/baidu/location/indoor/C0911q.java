package com.baidu.location.indoor;

import com.baidu.location.BDLocation;
import com.baidu.location.indoor.C0908p;

/* renamed from: com.baidu.location.indoor.q */
class C0911q implements Runnable {

    /* renamed from: a */
    final /* synthetic */ C0908p f2318a;

    C0911q(C0908p pVar) {
        this.f2318a = pVar;
    }

    public void run() {
        C0908p pVar = this.f2318a;
        C0908p.C0910b a = pVar.m2785a(pVar.f2304e);
        if (!(a == null || this.f2318a.f2300a == null)) {
            C0908p pVar2 = this.f2318a;
            C0908p.C0910b unused = pVar2.f2304e = pVar2.f2304e.mo10844b(a);
            long currentTimeMillis = System.currentTimeMillis();
            if (!a.mo10845b(2.0E-6d) && currentTimeMillis - this.f2318a.f2310k > this.f2318a.f2301b) {
                BDLocation bDLocation = new BDLocation(this.f2318a.f2302c);
                bDLocation.setLatitude(this.f2318a.f2304e.f2315a);
                bDLocation.setLongitude(this.f2318a.f2304e.f2316b);
                this.f2318a.f2300a.mo10779a(bDLocation);
                long unused2 = this.f2318a.f2310k = currentTimeMillis;
            }
        }
        this.f2318a.f2312m.postDelayed(this.f2318a.f2314o, 450);
    }
}
