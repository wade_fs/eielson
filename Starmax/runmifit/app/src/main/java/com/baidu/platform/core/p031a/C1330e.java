package com.baidu.platform.core.p031a;

import com.baidu.mapapi.search.district.DistrictSearchOption;
import com.baidu.mapapi.search.district.OnGetDistricSearchResultListener;

/* renamed from: com.baidu.platform.core.a.e */
public interface C1330e {
    /* renamed from: a */
    void mo14035a();

    /* renamed from: a */
    void mo14036a(OnGetDistricSearchResultListener onGetDistricSearchResultListener);

    /* renamed from: a */
    boolean mo14037a(DistrictSearchOption districtSearchOption);
}
