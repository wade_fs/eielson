package com.baidu.mapapi.map;

import android.graphics.drawable.Drawable;
import com.baidu.mapsdkplatform.comapi.map.C1082h;

public class ItemizedOverlay extends Overlay {

    /* renamed from: a */
    MapView f2524a;

    public ItemizedOverlay(Drawable drawable, MapView mapView) {
        this.type = C1082h.marker;
        this.f2524a = mapView;
    }

    public void addItem(OverlayOptions overlayOptions) {
        if (overlayOptions != null && overlayOptions != null) {
            this.f2524a.getMap().addOverlay(overlayOptions);
        }
    }

    public void reAddAll() {
    }

    public void removeAll() {
        this.f2524a.getMap().clear();
    }
}
