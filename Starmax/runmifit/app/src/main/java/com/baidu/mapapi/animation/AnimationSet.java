package com.baidu.mapapi.animation;

import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapsdkplatform.comapi.p023a.C1032d;

public class AnimationSet extends Animation {
    public AnimationSet() {
        this.bdAnimation = new C1032d();
    }

    public void addAnimation(Animation animation) {
        if (animation != null) {
            ((C1032d) this.bdAnimation).mo12889a(super);
        }
    }

    public void cancel() {
        this.bdAnimation.mo12881b();
    }

    public void setAnimationListener(Animation.AnimationListener animationListener) {
        this.bdAnimation.mo12879a(animationListener);
    }

    public void setAnimatorSetMode(int i) {
        this.bdAnimation.mo12884c(i);
    }

    public void setDuration(long j) {
        this.bdAnimation.mo12876a(j);
    }

    public void setInterpolator(Interpolator interpolator) {
        this.bdAnimation.mo12878a(interpolator);
    }
}
