package com.baidu.location.p016d;

import com.baidu.location.BDLocation;
import java.util.concurrent.Callable;

/* renamed from: com.baidu.location.d.i */
class C0808i implements Callable<BDLocation> {

    /* renamed from: a */
    final /* synthetic */ String[] f1676a;

    /* renamed from: b */
    final /* synthetic */ C0804h f1677b;

    C0808i(C0804h hVar, String[] strArr) {
        this.f1677b = hVar;
        this.f1676a = strArr;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:54:? */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v6, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r2v9 */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004e, code lost:
        if (r0 != null) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005e, code lost:
        if (r0 != null) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x008a, code lost:
        if (r0 != null) goto L_0x008c;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0059 A[SYNTHETIC, Splitter:B:22:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0085 A[SYNTHETIC, Splitter:B:44:0x0085] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.baidu.location.BDLocation call() {
        /*
            r11 = this;
            com.baidu.location.BDLocation r0 = new com.baidu.location.BDLocation
            r0.<init>()
            java.lang.String[] r1 = r11.f1676a
            int r1 = r1.length
            if (r1 <= 0) goto L_0x009f
            com.baidu.location.d.h r0 = r11.f1677b
            com.baidu.location.d.f r0 = r0.f1663j
            java.lang.String[] r0 = r0.mo10585o()
            r1 = 0
            r2 = 0
            r4 = r2
            r3 = 0
        L_0x0018:
            int r5 = r0.length
            if (r3 >= r5) goto L_0x0031
            android.content.Context r4 = com.baidu.location.p016d.C0804h.f1656c     // Catch:{ Exception -> 0x002a }
            android.content.pm.PackageManager r4 = r4.getPackageManager()     // Catch:{ Exception -> 0x002a }
            r5 = r0[r3]     // Catch:{ Exception -> 0x002a }
            android.content.pm.ProviderInfo r4 = r4.resolveContentProvider(r5, r1)     // Catch:{ Exception -> 0x002a }
            goto L_0x002b
        L_0x002a:
            r4 = r2
        L_0x002b:
            if (r4 == 0) goto L_0x002e
            goto L_0x0031
        L_0x002e:
            int r3 = r3 + 1
            goto L_0x0018
        L_0x0031:
            if (r4 == 0) goto L_0x0061
            android.content.Context r0 = com.baidu.location.p016d.C0804h.f1656c     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            android.content.ContentResolver r5 = r0.getContentResolver()     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            java.lang.String r0 = r4.authority     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            android.net.Uri r6 = com.baidu.location.p016d.C0804h.m2197c(r0)     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            java.lang.String[] r7 = r11.f1676a     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r0 = r5.query(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            com.baidu.location.BDLocation r2 = com.baidu.location.p016d.C0809j.m2219a(r0)     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
            if (r0 == 0) goto L_0x008f
        L_0x0050:
            goto L_0x008c
        L_0x0051:
            r1 = move-exception
            r2 = r0
            goto L_0x0057
        L_0x0054:
            goto L_0x005e
        L_0x0056:
            r1 = move-exception
        L_0x0057:
            if (r2 == 0) goto L_0x005c
            r2.close()     // Catch:{ Exception -> 0x005c }
        L_0x005c:
            throw r1
        L_0x005d:
            r0 = r2
        L_0x005e:
            if (r0 == 0) goto L_0x008f
            goto L_0x0050
        L_0x0061:
            com.baidu.location.d.j$a r0 = new com.baidu.location.d.j$a
            java.lang.String[] r1 = r11.f1676a
            r0.<init>(r1)
            com.baidu.location.d.h r1 = r11.f1677b     // Catch:{ Exception -> 0x0089, all -> 0x0082 }
            com.baidu.location.d.c r1 = r1.f1661h     // Catch:{ Exception -> 0x0089, all -> 0x0082 }
            android.database.Cursor r0 = r1.mo10564a(r0)     // Catch:{ Exception -> 0x0089, all -> 0x0082 }
            com.baidu.location.BDLocation r1 = com.baidu.location.p016d.C0809j.m2219a(r0)     // Catch:{ Exception -> 0x0080, all -> 0x007d }
            if (r0 == 0) goto L_0x007b
            r0.close()     // Catch:{ Exception -> 0x007b }
        L_0x007b:
            r0 = r1
            goto L_0x0090
        L_0x007d:
            r1 = move-exception
            r2 = r0
            goto L_0x0083
        L_0x0080:
            goto L_0x008a
        L_0x0082:
            r1 = move-exception
        L_0x0083:
            if (r2 == 0) goto L_0x0088
            r2.close()     // Catch:{ Exception -> 0x0088 }
        L_0x0088:
            throw r1
        L_0x0089:
            r0 = r2
        L_0x008a:
            if (r0 == 0) goto L_0x008f
        L_0x008c:
            r0.close()     // Catch:{ Exception -> 0x008f }
        L_0x008f:
            r0 = r2
        L_0x0090:
            if (r0 == 0) goto L_0x009f
            int r1 = r0.getLocType()
            r2 = 67
            if (r1 == r2) goto L_0x009f
            r1 = 66
            r0.setLocType(r1)
        L_0x009f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0808i.call():com.baidu.location.BDLocation");
    }
}
