package com.baidu.mapsdkplatform.comapi.map;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MapRenderer implements GLSurfaceView.Renderer {

    /* renamed from: d */
    private static final String f3402d = MapRenderer.class.getSimpleName();

    /* renamed from: a */
    public int f3403a;

    /* renamed from: b */
    public int f3404b;

    /* renamed from: c */
    public int f3405c;

    /* renamed from: e */
    private long f3406e;

    /* renamed from: f */
    private C1061a f3407f;

    /* renamed from: g */
    private final C1084j f3408g;

    /* renamed from: com.baidu.mapsdkplatform.comapi.map.MapRenderer$a */
    public interface C1061a {
        /* renamed from: f */
        void mo12948f();
    }

    public MapRenderer(C1084j jVar, C1061a aVar) {
        this.f3407f = aVar;
        this.f3408g = jVar;
    }

    /* renamed from: a */
    private void m3499a(GL10 gl10) {
        GLES20.glClear(16640);
        GLES20.glClearColor(0.85f, 0.8f, 0.8f, 0.0f);
    }

    /* renamed from: a */
    private boolean m3500a() {
        return this.f3406e != 0;
    }

    public static native void nativeInit(long j);

    public static native int nativeRender(long j);

    public static native void nativeResize(long j, int i, int i2);

    /* renamed from: a */
    public void mo12944a(long j) {
        this.f3406e = j;
    }

    public void onDrawFrame(GL10 gl10) {
        if (!m3500a()) {
            m3499a(gl10);
            return;
        }
        if (this.f3405c <= 1) {
            nativeResize(this.f3406e, this.f3403a, this.f3404b);
            this.f3405c++;
        }
        this.f3407f.mo12948f();
        int nativeRender = nativeRender(this.f3406e);
        if (this.f3408g.mo13086a() != null) {
            if (this.f3408g.mo13086a().f3532h != null) {
                for (C1087l lVar : this.f3408g.mo13086a().f3532h) {
                    if (this.f3408g.mo13086a() != null) {
                        C1064ab J = this.f3408g.mo13086a().mo12994J();
                        if (lVar != null) {
                            lVar.mo11544a(gl10, J);
                        }
                    } else {
                        return;
                    }
                }
            }
            C1084j jVar = this.f3408g;
            if (nativeRender == 1) {
                jVar.requestRender();
            } else if (!jVar.mo13086a().mo13036b()) {
                if (jVar.getRenderMode() != 0) {
                    jVar.setRenderMode(0);
                }
            } else if (jVar.getRenderMode() != 1) {
                jVar.setRenderMode(1);
            }
        }
    }

    public void onSurfaceChanged(GL10 gl10, int i, int i2) {
        long j = this.f3406e;
        if (j != 0) {
            nativeResize(j, i, i2);
        }
    }

    public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        nativeInit(this.f3406e);
        if (m3500a()) {
            this.f3407f.mo12948f();
        }
    }
}
