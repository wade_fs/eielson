package com.baidu.location.p017e;

import android.location.OnNmeaMessageListener;

/* renamed from: com.baidu.location.e.f */
class C0832f implements OnNmeaMessageListener {

    /* renamed from: a */
    final /* synthetic */ C0826e f1787a;

    C0832f(C0826e eVar) {
        this.f1787a = eVar;
    }

    public void onNmeaMessage(String str, long j) {
        if (this.f1787a.m2323b(str)) {
            this.f1787a.mo10637a(str);
        }
    }
}
