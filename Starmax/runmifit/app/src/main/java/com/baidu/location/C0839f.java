package com.baidu.location;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;
import com.baidu.location.p018f.C0840a;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.RandomAccessFile;

/* renamed from: com.baidu.location.f */
public class C0839f extends Service {
    public static boolean isServing = false;
    public static boolean isStartedServing = false;

    /* renamed from: mC */
    public static Context f1811mC = null;
    public static String replaceFileName = "repll.jar";

    /* renamed from: a */
    LLSInterface f1812a = null;

    /* renamed from: b */
    LLSInterface f1813b = null;

    /* renamed from: c */
    LLSInterface f1814c = null;

    /* renamed from: a */
    private boolean m2403a(File file) {
        int readInt;
        boolean z = false;
        try {
            File file2 = new File(C0855k.m2472j() + "/grtcfrsa.dat");
            if (file2.exists()) {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file2, "rw");
                randomAccessFile.seek(200);
                if (randomAccessFile.readBoolean() && randomAccessFile.readBoolean() && (readInt = randomAccessFile.readInt()) != 0) {
                    byte[] bArr = new byte[readInt];
                    randomAccessFile.read(bArr, 0, readInt);
                    String str = new String(bArr);
                    String a = C0855k.m2455a(file, "SHA-256");
                    if (a != null && C0855k.m2461b(a, str, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCiP7BS5IjEOzrKGR9/Ww9oSDhdX1ir26VOsYjT1T6tk2XumRpkHRwZbrucDcNnvSB4QsqiEJnvTSRi7YMbh2H9sLMkcvHlMV5jAErNvnuskWfcvf7T2mq7EUZI/Hf4oVZhHV0hQJRFVdTcjWI6q2uaaKM3VMh+roDesiE7CR2biQIDAQAB")) {
                        z = true;
                    }
                }
                randomAccessFile.close();
            }
        } catch (Exception unused) {
        }
        return z;
    }

    public static float getFrameVersion() {
        return 7.82f;
    }

    public static String getJarFileName() {
        return "app.jar";
    }

    public static Context getServiceContext() {
        return f1811mC;
    }

    public IBinder onBind(Intent intent) {
        LLSInterface lLSInterface = this.f1814c;
        if (lLSInterface != null) {
            return lLSInterface.onBind(intent);
        }
        return null;
    }

    public void onCreate() {
        if (isServing) {
            Log.d("baidu_location_service", "baidu location service can not start again ...20190306..." + Process.myPid());
            return;
        }
        f1811mC = getApplicationContext();
        System.currentTimeMillis();
        this.f1813b = new C0840a();
        try {
            File file = new File(C0855k.m2472j() + File.separator + replaceFileName);
            File file2 = new File(C0855k.m2472j() + File.separator + "app.jar");
            if (file.exists()) {
                if (file2.exists()) {
                    file2.delete();
                }
                file.renameTo(file2);
            }
            if (file2.exists()) {
                if (m2403a(new File(C0855k.m2472j() + File.separator + "app.jar"))) {
                    this.f1812a = (LLSInterface) new DexClassLoader(C0855k.m2472j() + File.separator + "app.jar", C0855k.m2472j(), null, getClassLoader()).loadClass("com.baidu.serverLoc.LocationService").newInstance();
                }
            }
        } catch (Exception unused) {
            this.f1812a = null;
        }
        LLSInterface lLSInterface = this.f1812a;
        if (lLSInterface == null || lLSInterface.getVersion() < this.f1813b.getVersion()) {
            this.f1814c = this.f1813b;
            this.f1812a = null;
        } else {
            this.f1814c = this.f1812a;
            this.f1813b = null;
        }
        isServing = true;
        this.f1814c.onCreate(this);
    }

    public void onDestroy() {
        isServing = false;
        LLSInterface lLSInterface = this.f1814c;
        if (lLSInterface != null) {
            lLSInterface.onDestroy();
        }
        if (isStartedServing) {
            stopForeground(true);
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null) {
            try {
                int intExtra = intent.getIntExtra("command", 0);
                if (intExtra == 1) {
                    startForeground(intent.getIntExtra(Config.FEED_LIST_ITEM_CUSTOM_ID, 0), (Notification) intent.getParcelableExtra("notification"));
                    isStartedServing = true;
                } else if (intExtra == 2) {
                    stopForeground(intent.getBooleanExtra("removenotify", true));
                    isStartedServing = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this.f1814c.onStartCommand(intent, i, i2);
    }

    public void onTaskRemoved(Intent intent) {
        LLSInterface lLSInterface = this.f1814c;
        if (lLSInterface != null) {
            lLSInterface.onTaskRemoved(intent);
        }
    }

    public boolean onUnbind(Intent intent) {
        return false;
    }
}
