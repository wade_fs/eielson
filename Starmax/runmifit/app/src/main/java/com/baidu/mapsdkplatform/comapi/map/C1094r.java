package com.baidu.mapsdkplatform.comapi.map;

import android.content.Context;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.common.EnvironmentUtilities;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comjni.map.basemap.C1177a;
import com.baidu.mobstat.Config;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.r */
public class C1094r {

    /* renamed from: a */
    private static final String f3599a = C1094r.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static C1094r f3600c;

    /* renamed from: b */
    private C1177a f3601b;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C1099w f3602d;

    /* renamed from: e */
    private Handler f3603e;

    private C1094r() {
    }

    /* renamed from: a */
    public static C1094r m3705a() {
        if (f3600c == null) {
            f3600c = new C1094r();
            f3600c.m3708g();
        }
        return f3600c;
    }

    /* renamed from: g */
    private void m3708g() {
        m3709h();
        this.f3602d = new C1099w();
        this.f3603e = new C1095s(this);
        MessageCenter.registMessage(65289, this.f3603e);
    }

    /* renamed from: h */
    private void m3709h() {
        Context context = BMapManager.getContext();
        EnvironmentUtilities.initAppDirectory(context);
        this.f3601b = new C1177a();
        this.f3601b.mo13479a(context.hashCode());
        String moduleFileName = SysOSUtil.getModuleFileName();
        String appSDCardPath = EnvironmentUtilities.getAppSDCardPath();
        String appCachePath = EnvironmentUtilities.getAppCachePath();
        String appSecondCachePath = EnvironmentUtilities.getAppSecondCachePath();
        int mapTmpStgMax = EnvironmentUtilities.getMapTmpStgMax();
        int domTmpStgMax = EnvironmentUtilities.getDomTmpStgMax();
        int itsTmpStgMax = EnvironmentUtilities.getItsTmpStgMax();
        String str = SysOSUtil.getDensityDpi() >= 180 ? "/h/" : "/l/";
        String str2 = moduleFileName + "/cfg";
        String str3 = appSDCardPath + "/vmp";
        String str4 = str3 + str;
        String str5 = str3 + str;
        String str6 = appCachePath + "/tmp/";
        this.f3601b.mo13485a(str2 + "/a/", str4, str6, appSecondCachePath + "/tmp/", str5, str2 + "/a/", null, 0, str2 + "/idrres/", SysOSUtil.getScreenSizeX(), SysOSUtil.getScreenSizeY(), SysOSUtil.getDensityDpi(), mapTmpStgMax, domTmpStgMax, itsTmpStgMax, 0);
        this.f3601b.mo13500d();
    }

    /* renamed from: a */
    public ArrayList<C1093q> mo13121a(String str) {
        JSONArray optJSONArray;
        String str2 = str;
        if (!str2.equals("")) {
            C1177a aVar = this.f3601b;
            if (aVar != null) {
                String a = aVar.mo13472a(str2);
                if (a == null || a.equals("")) {
                    return null;
                }
                ArrayList<C1093q> arrayList = new ArrayList<>();
                try {
                    JSONObject jSONObject = new JSONObject(a);
                    if (jSONObject.length() == 0 || (optJSONArray = jSONObject.optJSONArray("dataset")) == null) {
                        return null;
                    }
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        C1093q qVar = new C1093q();
                        JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                        int optInt = jSONObject2.optInt(Config.FEED_LIST_ITEM_CUSTOM_ID);
                        if (optInt <= 2000 || optInt == 2912 || optInt == 2911 || optInt == 9000) {
                            qVar.f3594a = optInt;
                            qVar.f3595b = jSONObject2.optString(Config.FEED_LIST_NAME);
                            qVar.f3596c = jSONObject2.optInt("mapsize");
                            qVar.f3597d = jSONObject2.optInt("cty");
                            if (jSONObject2.has("child")) {
                                JSONArray optJSONArray2 = jSONObject2.optJSONArray("child");
                                ArrayList arrayList2 = new ArrayList();
                                for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                                    C1093q qVar2 = new C1093q();
                                    JSONObject optJSONObject = optJSONArray2.optJSONObject(i2);
                                    qVar2.f3594a = optJSONObject.optInt(Config.FEED_LIST_ITEM_CUSTOM_ID);
                                    qVar2.f3595b = optJSONObject.optString(Config.FEED_LIST_NAME);
                                    qVar2.f3596c = optJSONObject.optInt("mapsize");
                                    qVar2.f3597d = optJSONObject.optInt("cty");
                                    arrayList2.add(qVar2);
                                }
                                qVar.mo13120a(arrayList2);
                            }
                            arrayList.add(qVar);
                        }
                    }
                    return arrayList;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo13122a(C1098v vVar) {
        C1099w wVar = this.f3602d;
        if (wVar != null) {
            wVar.mo13140a(vVar);
        }
    }

    /* renamed from: a */
    public boolean mo13123a(int i) {
        if (this.f3601b == null || i < 0) {
            return false;
        }
        if (i <= 2000 || i == 2912 || i == 2911 || i == 9000) {
            return this.f3601b.mo13503d(i);
        }
        return false;
    }

    /* renamed from: a */
    public boolean mo13124a(boolean z, boolean z2) {
        C1177a aVar = this.f3601b;
        if (aVar == null) {
            return false;
        }
        return aVar.mo13486a(z, z2);
    }

    /* renamed from: b */
    public void mo13125b() {
        MessageCenter.unregistMessage(65289, this.f3603e);
        this.f3601b.mo13492b(BMapManager.getContext().hashCode());
        f3600c = null;
    }

    /* renamed from: b */
    public void mo13126b(C1098v vVar) {
        C1099w wVar = this.f3602d;
        if (wVar != null) {
            wVar.mo13141b(vVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, boolean, int):boolean
     arg types: [int, int, int]
     candidates:
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, int, java.lang.String):long
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int[], int, int):int[]
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, boolean, int):boolean */
    /* renamed from: b */
    public boolean mo13127b(int i) {
        if (this.f3601b == null || i < 0) {
            return false;
        }
        if (i <= 2000 || i == 2912 || i == 2911 || i == 9000) {
            return this.f3601b.mo13481a(i, false, 0);
        }
        return false;
    }

    /* renamed from: c */
    public ArrayList<C1093q> mo13128c() {
        C1177a aVar = this.f3601b;
        if (aVar == null) {
            return null;
        }
        String m = aVar.mo13520m();
        ArrayList<C1093q> arrayList = new ArrayList<>();
        try {
            JSONArray optJSONArray = new JSONObject(m).optJSONArray("dataset");
            for (int i = 0; i < optJSONArray.length(); i++) {
                C1093q qVar = new C1093q();
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                qVar.f3594a = optJSONObject.optInt(Config.FEED_LIST_ITEM_CUSTOM_ID);
                qVar.f3595b = optJSONObject.optString(Config.FEED_LIST_NAME);
                qVar.f3596c = optJSONObject.optInt("mapsize");
                qVar.f3597d = optJSONObject.optInt("cty");
                if (optJSONObject.has("child")) {
                    JSONArray optJSONArray2 = optJSONObject.optJSONArray("child");
                    ArrayList arrayList2 = new ArrayList();
                    for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                        C1093q qVar2 = new C1093q();
                        JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                        qVar2.f3594a = optJSONObject2.optInt(Config.FEED_LIST_ITEM_CUSTOM_ID);
                        qVar2.f3595b = optJSONObject2.optString(Config.FEED_LIST_NAME);
                        qVar2.f3596c = optJSONObject2.optInt("mapsize");
                        qVar2.f3597d = optJSONObject2.optInt("cty");
                        arrayList2.add(qVar2);
                    }
                    qVar.mo13120a(arrayList2);
                }
                arrayList.add(qVar);
            }
            return arrayList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: c */
    public boolean mo13129c(int i) {
        C1177a aVar = this.f3601b;
        if (aVar == null || i < 0) {
            return false;
        }
        return aVar.mo13494b(i, false, 0);
    }

    /* renamed from: d */
    public ArrayList<C1093q> mo13130d() {
        C1177a aVar = this.f3601b;
        if (aVar == null) {
            return null;
        }
        String a = aVar.mo13472a("");
        ArrayList<C1093q> arrayList = new ArrayList<>();
        try {
            JSONArray optJSONArray = new JSONObject(a).optJSONArray("dataset");
            for (int i = 0; i < optJSONArray.length(); i++) {
                C1093q qVar = new C1093q();
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                int optInt = optJSONObject.optInt(Config.FEED_LIST_ITEM_CUSTOM_ID);
                if (optInt <= 2000 || optInt == 2912 || optInt == 2911 || optInt == 9000) {
                    qVar.f3594a = optInt;
                    qVar.f3595b = optJSONObject.optString(Config.FEED_LIST_NAME);
                    qVar.f3596c = optJSONObject.optInt("mapsize");
                    qVar.f3597d = optJSONObject.optInt("cty");
                    if (optJSONObject.has("child")) {
                        JSONArray optJSONArray2 = optJSONObject.optJSONArray("child");
                        ArrayList arrayList2 = new ArrayList();
                        int i2 = 0;
                        while (i2 < optJSONArray2.length()) {
                            C1093q qVar2 = new C1093q();
                            JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                            try {
                                qVar2.f3594a = optJSONObject2.optInt(Config.FEED_LIST_ITEM_CUSTOM_ID);
                                qVar2.f3595b = optJSONObject2.optString(Config.FEED_LIST_NAME);
                                qVar2.f3596c = optJSONObject2.optInt("mapsize");
                                qVar2.f3597d = optJSONObject2.optInt("cty");
                                arrayList2.add(qVar2);
                                i2++;
                            } catch (JSONException unused) {
                                return null;
                            } catch (Exception unused2) {
                                return null;
                            }
                        }
                        qVar.mo13120a(arrayList2);
                    }
                    arrayList.add(qVar);
                }
            }
            return arrayList;
        } catch (JSONException unused3) {
            return null;
        } catch (Exception unused4) {
            return null;
        }
    }

    /* renamed from: d */
    public boolean mo13131d(int i) {
        C1177a aVar = this.f3601b;
        if (aVar == null) {
            return false;
        }
        return aVar.mo13494b(0, true, i);
    }

    /* renamed from: e */
    public ArrayList<C1097u> mo13132e() {
        String l;
        C1177a aVar = this.f3601b;
        if (!(aVar == null || (l = aVar.mo13519l()) == null || l.equals(""))) {
            ArrayList<C1097u> arrayList = new ArrayList<>();
            try {
                JSONObject jSONObject = new JSONObject(l);
                if (jSONObject.length() == 0) {
                    return null;
                }
                JSONArray optJSONArray = jSONObject.optJSONArray("dataset");
                for (int i = 0; i < optJSONArray.length(); i++) {
                    C1097u uVar = new C1097u();
                    C1096t tVar = new C1096t();
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                    tVar.f3605a = optJSONObject.optInt(Config.FEED_LIST_ITEM_CUSTOM_ID);
                    tVar.f3606b = optJSONObject.optString(Config.FEED_LIST_NAME);
                    tVar.f3607c = optJSONObject.optString("pinyin");
                    tVar.f3612h = optJSONObject.optInt("mapoldsize");
                    tVar.f3613i = optJSONObject.optInt("ratio");
                    tVar.f3616l = optJSONObject.optInt(NotificationCompat.CATEGORY_STATUS);
                    tVar.f3611g = new GeoPoint((double) optJSONObject.optInt("y"), (double) optJSONObject.optInt(Config.EVENT_HEAT_X));
                    boolean z = true;
                    if (optJSONObject.optInt("up") != 1) {
                        z = false;
                    }
                    tVar.f3614j = z;
                    tVar.f3609e = optJSONObject.optInt("lev");
                    if (tVar.f3614j) {
                        tVar.f3615k = optJSONObject.optInt("mapsize");
                    } else {
                        tVar.f3615k = 0;
                    }
                    uVar.mo13138a(tVar);
                    arrayList.add(uVar);
                }
                return arrayList;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapsdkplatform.comjni.map.basemap.a.b(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.baidu.mapsdkplatform.comjni.map.basemap.a.b(long, boolean):void
      com.baidu.mapsdkplatform.comjni.map.basemap.a.b(int, int):java.lang.String
      com.baidu.mapsdkplatform.comjni.map.basemap.a.b(int, boolean):boolean */
    /* renamed from: e */
    public boolean mo13133e(int i) {
        C1177a aVar = this.f3601b;
        if (aVar == null || i < 0) {
            return false;
        }
        return aVar.mo13493b(i, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, boolean):void
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, android.os.Bundle):void
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(long, long):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(java.lang.String, java.lang.String):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(boolean, boolean):boolean
      com.baidu.mapsdkplatform.comjni.map.basemap.a.a(int, boolean):boolean */
    /* renamed from: f */
    public boolean mo13134f(int i) {
        if (this.f3601b == null || i < 0) {
            return false;
        }
        if (i <= 2000 || i == 2912 || i == 2911 || i == 9000) {
            return this.f3601b.mo13480a(i, false);
        }
        return false;
    }

    /* renamed from: g */
    public C1097u mo13135g(int i) {
        String e;
        C1177a aVar = this.f3601b;
        if (aVar != null && i >= 0 && (e = aVar.mo13505e(i)) != null && !e.equals("")) {
            C1097u uVar = new C1097u();
            C1096t tVar = new C1096t();
            try {
                JSONObject jSONObject = new JSONObject(e);
                if (jSONObject.length() == 0) {
                    return null;
                }
                int optInt = jSONObject.optInt(Config.FEED_LIST_ITEM_CUSTOM_ID);
                if (optInt > 2000 && optInt != 2912 && optInt != 2911 && optInt != 9000) {
                    return null;
                }
                tVar.f3605a = optInt;
                tVar.f3606b = jSONObject.optString(Config.FEED_LIST_NAME);
                tVar.f3607c = jSONObject.optString("pinyin");
                tVar.f3608d = jSONObject.optString("headchar");
                tVar.f3612h = jSONObject.optInt("mapoldsize");
                tVar.f3613i = jSONObject.optInt("ratio");
                tVar.f3616l = jSONObject.optInt(NotificationCompat.CATEGORY_STATUS);
                tVar.f3611g = new GeoPoint((double) jSONObject.optInt("y"), (double) jSONObject.optInt(Config.EVENT_HEAT_X));
                boolean z = true;
                if (jSONObject.optInt("up") != 1) {
                    z = false;
                }
                tVar.f3614j = z;
                tVar.f3609e = jSONObject.optInt("lev");
                if (tVar.f3614j) {
                    tVar.f3615k = jSONObject.optInt("mapsize");
                } else {
                    tVar.f3615k = 0;
                }
                tVar.f3610f = jSONObject.optInt("ver");
                uVar.mo13138a(tVar);
                return uVar;
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }
}
