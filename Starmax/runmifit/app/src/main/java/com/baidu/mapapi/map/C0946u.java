package com.baidu.mapapi.map;

import android.view.View;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;

/* renamed from: com.baidu.mapapi.map.u */
class C0946u implements View.OnClickListener {

    /* renamed from: a */
    final /* synthetic */ TextureMapView f2871a;

    C0946u(TextureMapView textureMapView) {
        this.f2871a = textureMapView;
    }

    public void onClick(View view) {
        float f = this.f2871a.f2752b.mo12954b().f3527b;
        C1064ab E = this.f2871a.f2752b.mo12954b().mo12989E();
        E.f3414a -= 1.0f;
        if (E.f3414a >= f) {
            f = E.f3414a;
        }
        E.f3414a = f;
        BaiduMap.mapStatusReason |= 16;
        this.f2871a.f2752b.mo12954b().mo13016a(E, 300);
    }
}
