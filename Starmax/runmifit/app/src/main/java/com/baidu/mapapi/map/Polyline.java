package com.baidu.mapapi.map;

import android.os.Bundle;
import android.util.Log;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;
import com.baidu.mobstat.Config;
import java.util.List;
import p041io.reactivex.annotations.SchedulerSupport;

public final class Polyline extends Overlay {

    /* renamed from: a */
    int f2675a;

    /* renamed from: b */
    List<LatLng> f2676b;

    /* renamed from: c */
    int[] f2677c;

    /* renamed from: d */
    int[] f2678d;

    /* renamed from: e */
    int f2679e = 5;

    /* renamed from: f */
    boolean f2680f = false;

    /* renamed from: g */
    boolean f2681g = false;

    /* renamed from: h */
    boolean f2682h = true;

    /* renamed from: i */
    BitmapDescriptor f2683i;

    /* renamed from: j */
    List<BitmapDescriptor> f2684j;

    Polyline() {
        this.type = C1082h.polyline;
    }

    /* renamed from: a */
    private Bundle m2948a(boolean z) {
        return (z ? BitmapDescriptorFactory.fromAsset("lineDashTexture.png") : this.f2683i).mo11047b();
    }

    /* renamed from: a */
    private static void m2949a(int[] iArr, Bundle bundle) {
        if (iArr != null && iArr.length > 0) {
            bundle.putIntArray("traffic_array", iArr);
        }
    }

    /* renamed from: b */
    private Bundle m2950b(boolean z) {
        if (z) {
            Bundle bundle = new Bundle();
            bundle.putInt(Config.EXCEPTION_MEMORY_TOTAL, 1);
            bundle.putBundle("texture_0", BitmapDescriptorFactory.fromAsset("lineDashTexture.png").mo11047b());
            return bundle;
        }
        Bundle bundle2 = new Bundle();
        int i = 0;
        for (int i2 = 0; i2 < this.f2684j.size(); i2++) {
            if (this.f2684j.get(i2) != null) {
                bundle2.putBundle("texture_" + String.valueOf(i), this.f2684j.get(i2).mo11047b());
                i++;
            }
        }
        bundle2.putInt(Config.EXCEPTION_MEMORY_TOTAL, i);
        return bundle2;
    }

    /* renamed from: b */
    private static void m2951b(int[] iArr, Bundle bundle) {
        if (iArr != null && iArr.length > 0) {
            bundle.putIntArray("color_array", iArr);
            bundle.putInt(Config.EXCEPTION_MEMORY_TOTAL, 1);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        super.mo10900a(bundle);
        GeoPoint ll2mc = CoordUtil.ll2mc(this.f2676b.get(0));
        bundle.putDouble("location_x", ll2mc.getLongitudeE6());
        bundle.putDouble("location_y", ll2mc.getLatitudeE6());
        bundle.putInt("width", this.f2679e);
        Overlay.m2940a(this.f2676b, bundle);
        Overlay.m2939a(this.f2675a, bundle);
        m2949a(this.f2677c, bundle);
        m2951b(this.f2678d, bundle);
        int[] iArr = this.f2677c;
        int i = 1;
        if (iArr != null && iArr.length > 0 && iArr.length > this.f2676b.size() - 1) {
            Log.e("baidumapsdk", "the size of textureIndexs is larger than the size of points");
        }
        bundle.putInt("dotline", this.f2680f ? 1 : 0);
        bundle.putInt("focus", this.f2681g ? 1 : 0);
        try {
            if (this.f2683i != null) {
                bundle.putInt(SchedulerSupport.CUSTOM, 1);
                bundle.putBundle("image_info", m2948a(false));
            } else {
                if (this.f2680f) {
                    bundle.putBundle("image_info", m2948a(true));
                }
                bundle.putInt(SchedulerSupport.CUSTOM, 0);
            }
            if (this.f2684j != null) {
                bundle.putInt("customlist", 1);
                bundle.putBundle("image_info_list", m2950b(false));
            } else {
                if (this.f2680f && ((this.f2677c != null && this.f2677c.length > 0) || (this.f2678d != null && this.f2678d.length > 0))) {
                    bundle.putBundle("image_info_list", m2950b(true));
                }
                bundle.putInt("customlist", 0);
            }
            if (!this.f2682h) {
                i = 0;
            }
            bundle.putInt("keep", i);
        } catch (Exception unused) {
            Log.e("baidumapsdk", "load texture resource failed!");
            bundle.putInt("dotline", 0);
        }
        return bundle;
    }

    public int getColor() {
        return this.f2675a;
    }

    public int[] getColorList() {
        return this.f2678d;
    }

    public List<LatLng> getPoints() {
        return this.f2676b;
    }

    public BitmapDescriptor getTexture() {
        return this.f2683i;
    }

    public int getWidth() {
        return this.f2679e;
    }

    public boolean isDottedLine() {
        return this.f2680f;
    }

    public boolean isFocus() {
        return this.f2681g;
    }

    public boolean isIsKeepScale() {
        return this.f2682h;
    }

    public void setColor(int i) {
        this.f2675a = i;
        this.listener.mo11330b(super);
    }

    public void setColorList(int[] iArr) {
        if (iArr == null || iArr.length == 0) {
            throw new IllegalArgumentException("BDMapSDKException: colorList can not empty");
        }
        this.f2678d = iArr;
    }

    public void setDottedLine(boolean z) {
        this.f2680f = z;
        this.listener.mo11330b(super);
    }

    public void setFocus(boolean z) {
        this.f2681g = z;
        this.listener.mo11330b(super);
    }

    public void setIndexs(int[] iArr) {
        if (iArr == null || iArr.length == 0) {
            throw new IllegalArgumentException("BDMapSDKException: indexList can not empty");
        }
        this.f2677c = iArr;
    }

    public void setIsKeepScale(boolean z) {
        this.f2682h = z;
    }

    public void setPoints(List<LatLng> list) {
        if (list == null) {
            throw new IllegalArgumentException("BDMapSDKException: points list can not be null");
        } else if (list.size() < 2) {
            throw new IllegalArgumentException("BDMapSDKException: points count can not less than 2 or more than 10000");
        } else if (!list.contains(null)) {
            this.f2676b = list;
            this.listener.mo11330b(super);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: points list can not contains null");
        }
    }

    public void setTexture(BitmapDescriptor bitmapDescriptor) {
        this.f2683i = bitmapDescriptor;
        this.listener.mo11330b(super);
    }

    public void setTextureList(List<BitmapDescriptor> list) {
        if (list == null || list.isEmpty()) {
            throw new IllegalArgumentException("BDMapSDKException: textureList can not empty");
        }
        this.f2684j = list;
    }

    public void setWidth(int i) {
        if (i > 0) {
            this.f2679e = i;
            this.listener.mo11330b(super);
        }
    }
}
