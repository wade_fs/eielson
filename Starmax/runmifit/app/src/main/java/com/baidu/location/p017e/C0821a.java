package com.baidu.location.p017e;

import java.util.Locale;

/* renamed from: com.baidu.location.e.a */
public class C0821a {

    /* renamed from: a */
    public int f1718a;

    /* renamed from: b */
    public int f1719b;

    /* renamed from: c */
    public int f1720c;

    /* renamed from: d */
    public int f1721d;

    /* renamed from: e */
    public int f1722e;

    /* renamed from: f */
    public int f1723f;

    /* renamed from: g */
    public long f1724g;

    /* renamed from: h */
    public int f1725h;

    /* renamed from: i */
    public char f1726i;

    /* renamed from: j */
    public String f1727j;

    /* renamed from: k */
    private boolean f1728k;

    public C0821a() {
        this.f1718a = -1;
        this.f1719b = -1;
        this.f1720c = -1;
        this.f1721d = -1;
        this.f1722e = Integer.MAX_VALUE;
        this.f1723f = Integer.MAX_VALUE;
        this.f1724g = 0;
        this.f1725h = -1;
        this.f1726i = '0';
        this.f1727j = null;
        this.f1728k = false;
        this.f1724g = System.currentTimeMillis();
    }

    public C0821a(int i, int i2, int i3, int i4, int i5, char c) {
        this.f1718a = -1;
        this.f1719b = -1;
        this.f1720c = -1;
        this.f1721d = -1;
        this.f1722e = Integer.MAX_VALUE;
        this.f1723f = Integer.MAX_VALUE;
        this.f1724g = 0;
        this.f1725h = -1;
        this.f1726i = '0';
        this.f1727j = null;
        this.f1728k = false;
        this.f1718a = i;
        this.f1719b = i2;
        this.f1720c = i3;
        this.f1721d = i4;
        this.f1725h = i5;
        this.f1726i = c;
        this.f1724g = System.currentTimeMillis();
    }

    public C0821a(C0821a aVar) {
        this(aVar.f1718a, aVar.f1719b, aVar.f1720c, aVar.f1721d, aVar.f1725h, aVar.f1726i);
        this.f1724g = aVar.f1724g;
    }

    /* renamed from: a */
    public boolean mo10615a() {
        long currentTimeMillis = System.currentTimeMillis();
        long j = this.f1724g;
        return currentTimeMillis - j > 0 && currentTimeMillis - j < 3000;
    }

    /* renamed from: a */
    public boolean mo10616a(C0821a aVar) {
        return this.f1718a == aVar.f1718a && this.f1719b == aVar.f1719b && this.f1721d == aVar.f1721d && this.f1720c == aVar.f1720c;
    }

    /* renamed from: b */
    public boolean mo10617b() {
        return this.f1718a > -1 && this.f1719b > 0;
    }

    /* renamed from: c */
    public boolean mo10618c() {
        return this.f1718a == -1 && this.f1719b == -1 && this.f1721d == -1 && this.f1720c == -1;
    }

    /* renamed from: d */
    public boolean mo10619d() {
        return this.f1718a > -1 && this.f1719b > -1 && this.f1721d == -1 && this.f1720c == -1;
    }

    /* renamed from: e */
    public boolean mo10620e() {
        return this.f1718a > -1 && this.f1719b > -1 && this.f1721d > -1 && this.f1720c > -1;
    }

    /* renamed from: f */
    public void mo10621f() {
        this.f1728k = true;
    }

    /* renamed from: g */
    public String mo10622g() {
        StringBuffer stringBuffer = new StringBuffer(128);
        stringBuffer.append(this.f1719b + 23);
        stringBuffer.append("H");
        stringBuffer.append(this.f1718a + 45);
        stringBuffer.append("K");
        stringBuffer.append(this.f1721d + 54);
        stringBuffer.append("Q");
        stringBuffer.append(this.f1720c + 203);
        return stringBuffer.toString();
    }

    /* renamed from: h */
    public String mo10623h() {
        StringBuffer stringBuffer = new StringBuffer(128);
        stringBuffer.append("&nw=");
        stringBuffer.append(this.f1726i);
        stringBuffer.append(String.format(Locale.CHINA, "&cl=%d|%d|%d|%d&cl_s=%d", Integer.valueOf(this.f1720c), Integer.valueOf(this.f1721d), Integer.valueOf(this.f1718a), Integer.valueOf(this.f1719b), Integer.valueOf(this.f1725h)));
        if (this.f1728k) {
            stringBuffer.append("&newcl=1");
        }
        return stringBuffer.toString();
    }

    /* renamed from: i */
    public String mo10624i() {
        StringBuffer stringBuffer = new StringBuffer(128);
        stringBuffer.append("&nw2=");
        stringBuffer.append(this.f1726i);
        stringBuffer.append(String.format(Locale.CHINA, "&cl2=%d|%d|%d|%d&cl_s2=%d", Integer.valueOf(this.f1720c), Integer.valueOf(this.f1721d), Integer.valueOf(this.f1718a), Integer.valueOf(this.f1719b), Integer.valueOf(this.f1725h)));
        return stringBuffer.toString();
    }
}
