package com.baidu.mapsdkplatform.comapi.util;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import com.baidu.mapapi.NetworkUtil;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.http.AsyncHttpClient;
import com.baidu.mapapi.http.HttpClient;
import com.baidu.mapsdkplatform.comjni.util.AppMD5;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.tamic.novate.download.MimeType;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomMapStyleLoader {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final String f3857a = CustomMapStyleLoader.class.getSimpleName();

    /* renamed from: c */
    private static String f3858c;

    /* renamed from: b */
    private String f3859b;

    /* renamed from: d */
    private Context f3860d;

    /* renamed from: e */
    private String f3861e;

    /* renamed from: f */
    private boolean f3862f;

    /* renamed from: g */
    private AsyncHttpClient f3863g;

    /* renamed from: com.baidu.mapsdkplatform.comapi.util.CustomMapStyleLoader$a */
    public class C1159a implements Runnable {
        public C1159a() {
        }

        public void run() {
            CustomMapStyleLoader.this.m4216b();
            String b = CustomMapStyleLoader.this.m4221d();
            if (TextUtils.isEmpty(b)) {
                Log.e(CustomMapStyleLoader.f3857a, "build request url failed");
            } else {
                CustomMapStyleLoader.this.m4212a(b);
            }
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.util.CustomMapStyleLoader$b */
    private static class C1160b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final CustomMapStyleLoader f3865a = new CustomMapStyleLoader(null);
    }

    private CustomMapStyleLoader() {
        this.f3862f = true;
        this.f3863g = new AsyncHttpClient();
    }

    /* synthetic */ CustomMapStyleLoader(C1168c cVar) {
        this();
    }

    /* renamed from: a */
    private String m4209a(Map<String, String> map) {
        if (map.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String str : map.keySet()) {
            String encodeUrlParamsValue = AppMD5.encodeUrlParamsValue(map.get(str));
            if (i != 0) {
                sb.append("&");
            }
            sb.append(str);
            sb.append("=");
            sb.append(encodeUrlParamsValue);
            i++;
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m4212a(String str) {
        this.f3863g.get(str, new C1168c(this));
    }

    /* renamed from: a */
    private void m4213a(JSONObject jSONObject) {
        File file = new File(f3858c);
        if (file.exists()) {
            file.delete();
        }
        try {
            if (file.createNewFile()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            Log.e(f3857a, "create custom file failed", e);
        }
        String optString = jSONObject.optString(MimeType.JSON);
        String optString2 = jSONObject.optString("md5", "null");
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put(MimeType.JSON, optString);
            jSONObject2.put("md5", optString2);
        } catch (JSONException e2) {
            Log.e(f3857a, "build style data failed", e2);
        }
        String jSONObject3 = jSONObject2.toString();
        try {
            FileOutputStream openFileOutput = this.f3860d.openFileOutput("server_custom_style_file.json", 0);
            openFileOutput.write(jSONObject3.getBytes());
            openFileOutput.flush();
            openFileOutput.close();
        } catch (IOException e3) {
            Log.e(f3857a, "write style data into file failed", e3);
        }
    }

    /* renamed from: a */
    private boolean m4214a(int i, String str) {
        if (103 != i || !m4220c()) {
            m4217b(i, str);
            return i == 0;
        }
        m4217b(i, str);
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m4216b() {
        if (!m4220c()) {
            this.f3859b = "null";
            return;
        }
        try {
            FileInputStream openFileInput = this.f3860d.openFileInput("server_custom_style_file.json");
            JsonReader jsonReader = new JsonReader(new InputStreamReader(openFileInput));
            try {
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    if (jsonReader.nextName().equals("md5")) {
                        this.f3859b = jsonReader.nextString();
                    } else {
                        jsonReader.skipValue();
                    }
                }
                jsonReader.endObject();
                try {
                    jsonReader.close();
                    openFileInput.close();
                } catch (IOException e) {
                    Log.e(f3857a, "Close custom style failed", e);
                }
            } catch (IOException e2) {
                this.f3859b = "null";
                Log.e(f3857a, "Read custom style failed", e2);
                jsonReader.close();
                openFileInput.close();
            } catch (Throwable th) {
                try {
                    jsonReader.close();
                    openFileInput.close();
                } catch (IOException e3) {
                    Log.e(f3857a, "Close custom style failed", e3);
                }
                throw th;
            }
        } catch (FileNotFoundException e4) {
            this.f3859b = "null";
            Log.e(f3857a, "Open custom style failed", e4);
        }
    }

    /* renamed from: b */
    private void m4217b(int i, String str) {
        Intent intent;
        if (i != 0) {
            intent = new Intent(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_LOAD_CUSTOM_STYLE_ERROR);
        }
        intent.putExtra(SDKInitializer.SDK_BROADTCAST_INTENT_EXTRA_INFO_KEY_ERROR_CODE, i);
        intent.putExtra(SDKInitializer.SDK_BROADTCAST_INTENT_EXTRA_INFO_KEY_ERROR_MESSAGE, str);
        this.f3860d.sendBroadcast(intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m4219b(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (m4214a(jSONObject.optInt(NotificationCompat.CATEGORY_STATUS), jSONObject.optString("message"))) {
                    JSONObject optJSONObject = jSONObject.optJSONObject("data");
                    if (optJSONObject == null || optJSONObject.length() == 0) {
                        Log.e(f3857a, "custom style data is null");
                    } else {
                        m4213a(optJSONObject);
                    }
                }
            } catch (JSONException e) {
                Log.e(f3857a, "parse response result failed", e);
            }
        }
    }

    /* renamed from: c */
    private boolean m4220c() {
        return new File(f3858c).exists();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public String m4221d() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("style_id", this.f3861e);
        linkedHashMap.put(SocialConstants.PARAM_TYPE, this.f3862f ? "publish" : "edit");
        linkedHashMap.put("md5", this.f3859b);
        linkedHashMap.put("token", C1175i.f3914d);
        String str = m4209a(linkedHashMap) + C1175i.m4270c();
        return m4222e() + "?" + (str + "&sign=" + AppMD5.getSignMD5String(str));
    }

    /* renamed from: e */
    private String m4222e() {
        return HttpClient.isHttpsEnable ? "https://api.map.baidu.com/sdkproxy/v2/lbs_androidsdk/custom/v2/getjsonstyle" : "http://api.map.baidu.com/sdkproxy/v2/lbs_androidsdk/custom/v2/getjsonstyle";
    }

    public static String getCustomStyleFilePath() {
        return f3858c;
    }

    public static CustomMapStyleLoader getInstance() {
        return C1160b.f3865a;
    }

    public void initCustomStyleFilePath(Context context) {
        this.f3860d = context;
        f3858c = this.f3860d.getFilesDir().getAbsolutePath();
        f3858c += "/" + "server_custom_style_file.json";
    }

    public void loadCustomMapStyleFile(String str, boolean z) {
        if (NetworkUtil.isNetworkAvailable(this.f3860d) && !TextUtils.isEmpty(str)) {
            this.f3861e = str;
            this.f3862f = z;
            new Thread(new C1159a(), "Load custom style").start();
        }
    }
}
