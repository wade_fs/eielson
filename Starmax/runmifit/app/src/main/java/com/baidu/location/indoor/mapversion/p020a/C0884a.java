package com.baidu.location.indoor.mapversion.p020a;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import com.baidu.location.Jni;
import com.baidu.location.indoor.mapversion.IndoorJni;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0849e;
import com.baidu.mobstat.Config;
import com.google.common.net.HttpHeaders;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.json.JSONObject;

/* renamed from: com.baidu.location.indoor.mapversion.a.a */
public class C0884a {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public C0887c f2205a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C0892d f2206b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public ExecutorService f2207c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public File f2208d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f2209e;

    /* renamed from: com.baidu.location.indoor.mapversion.a.a$a */
    private class C0885a implements Runnable {

        /* renamed from: a */
        public C0888d f2210a;

        /* renamed from: b */
        public String f2211b;

        private C0885a() {
            this.f2211b = null;
        }

        /* synthetic */ C0885a(C0884a aVar, C0890b bVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.indoor.mapversion.a.a.a(com.baidu.location.indoor.mapversion.a.a, boolean):boolean
         arg types: [com.baidu.location.indoor.mapversion.a.a, int]
         candidates:
          com.baidu.location.indoor.mapversion.a.a.a(com.baidu.location.indoor.mapversion.a.a, java.lang.String):java.lang.String
          com.baidu.location.indoor.mapversion.a.a.a(java.lang.String, java.lang.String):boolean
          com.baidu.location.indoor.mapversion.a.a.a(com.baidu.location.indoor.mapversion.a.a, boolean):boolean */
        public void run() {
            if (!(this.f2211b == null || this.f2210a == null)) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("timestamp", System.currentTimeMillis());
                    jSONObject.put("manufacturer", Build.MANUFACTURER);
                    jSONObject.put("product", Build.PRODUCT);
                    jSONObject.put("brand", Build.BRAND);
                    jSONObject.put("model", Build.MODEL);
                    jSONObject.put("gravity", String.format(Locale.CHINESE, "%.5f,%.5f,%.5f", Float.valueOf(this.f2210a.f2220e[0]), Float.valueOf(this.f2210a.f2220e[1]), Float.valueOf(this.f2210a.f2220e[2])));
                    jSONObject.put("fov", this.f2210a.f2219d);
                    jSONObject.put("bid", this.f2210a.f2222g);
                    String str = "";
                    if (C0844b.m2417a().f1856c != null) {
                        str = C0844b.m2417a().f1856c;
                    }
                    jSONObject.put("cu", str);
                    IndoorJni.m2671a(this.f2210a.f2218c, C0884a.this.f2208d);
                    File file = new File(C0884a.this.f2208d, "compress.jpg");
                    String a = C0884a.this.m2686c(file.getAbsolutePath());
                    if (a != null) {
                        jSONObject.put("image", a);
                        file.delete();
                        C0884a.this.m2684b(this.f2210a.f2218c);
                        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.f2211b).openConnection();
                        httpURLConnection.setDoInput(true);
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.setConnectTimeout(10000);
                        httpURLConnection.setReadTimeout(10000);
                        httpURLConnection.setRequestMethod("POST");
                        httpURLConnection.addRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
                        httpURLConnection.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
                        httpURLConnection.getOutputStream().write(jSONObject.toString().getBytes());
                        httpURLConnection.disconnect();
                        boolean unused = C0884a.this.f2209e = false;
                        return;
                    }
                } catch (Error | Exception unused2) {
                } catch (Throwable th) {
                    boolean unused3 = C0884a.this.f2209e = false;
                    throw th;
                }
            }
            boolean unused4 = C0884a.this.f2209e = false;
        }
    }

    /* renamed from: com.baidu.location.indoor.mapversion.a.a$b */
    private class C0886b extends C0849e {

        /* renamed from: a */
        public C0888d f2213a;

        /* renamed from: c */
        private boolean f2215c;

        C0886b() {
            this.f2215c = false;
            this.f2215c = false;
        }

        /* renamed from: b */
        private void m2690b() {
            if (C0884a.this.f2205a != null) {
                C0889e eVar = new C0889e();
                if (C0884a.this.f2206b != null) {
                    eVar.f2231i = C0884a.this.f2206b.mo10812b();
                }
                eVar.f2223a = "param not enough";
                C0884a.this.f2205a.mo10794a(false, eVar);
            }
        }

        /* renamed from: c */
        private String m2691c() {
            String a;
            float[] d = this.f2213a.f2220e;
            if (d == null || this.f2213a.f2218c == null) {
                return null;
            }
            C0888d dVar = this.f2213a;
            Bitmap unused = dVar.f2217b = BitmapFactory.decodeFile(dVar.f2218c);
            if (this.f2213a.f2217b == null || (a = IndoorJni.m2670a(C0884a.this.f2208d, this.f2213a.f2217b, this.f2213a.f2219d, d)) == null) {
                return null;
            }
            try {
                if (!this.f2213a.f2217b.isRecycled()) {
                    this.f2213a.f2217b.recycle();
                }
            } catch (Error e) {
                e.printStackTrace();
            }
            String a2 = C0884a.this.m2686c(a);
            if (a2 == null) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(C0844b.m2417a().mo10713a(true));
            sb.append("&bid=");
            sb.append(this.f2213a.f2222g);
            sb.append("&data_type=vps");
            sb.append("&coor=gcj02");
            if (this.f2213a.f2221f != null && !this.f2213a.f2221f.equalsIgnoreCase("")) {
                sb.append("&code=");
                sb.append(this.f2213a.f2221f);
            }
            sb.append("&img=");
            sb.append(a2);
            return sb.toString();
        }

        /* renamed from: a */
        public void mo10444a() {
            String c = m2691c();
            if (c == null) {
                m2690b();
                this.f2215c = true;
                return;
            }
            String encodeTp4 = Jni.encodeTp4(c);
            this.f1885k = new HashMap();
            Map map = this.f1885k;
            map.put("vps", encodeTp4 + "|tp=4");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.indoor.mapversion.a.a.a(com.baidu.location.indoor.mapversion.a.a, boolean):boolean
         arg types: [com.baidu.location.indoor.mapversion.a.a, int]
         candidates:
          com.baidu.location.indoor.mapversion.a.a.a(com.baidu.location.indoor.mapversion.a.a, java.lang.String):java.lang.String
          com.baidu.location.indoor.mapversion.a.a.a(java.lang.String, java.lang.String):boolean
          com.baidu.location.indoor.mapversion.a.a.a(com.baidu.location.indoor.mapversion.a.a, boolean):boolean */
        /* renamed from: a */
        public void mo10446a(boolean z) {
            C0889e eVar;
            if (!this.f2215c) {
                if (!z || this.f1884j == null) {
                    eVar = null;
                } else {
                    eVar = new C0889e();
                    if (C0884a.this.f2206b != null) {
                        eVar.f2231i = C0884a.this.f2206b.mo10812b();
                    }
                    try {
                        z = eVar.mo10801a(new JSONObject(this.f1884j));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (C0884a.this.f2205a != null) {
                    C0884a.this.f2205a.mo10794a(z, eVar);
                }
                if (eVar == null) {
                    return;
                }
                if (eVar.f2232j <= 0 || C0884a.this.f2209e || z) {
                    C0884a.this.m2684b(this.f2213a.f2218c);
                } else if (!TextUtils.isEmpty(eVar.f2233k)) {
                    C0885a aVar = new C0885a(C0884a.this, null);
                    aVar.f2211b = eVar.f2233k;
                    aVar.f2210a = this.f2213a;
                    boolean unused = C0884a.this.f2209e = true;
                    C0884a.this.f2207c.execute(aVar);
                }
            }
        }
    }

    /* renamed from: com.baidu.location.indoor.mapversion.a.a$c */
    public interface C0887c {
        /* renamed from: a */
        void mo10794a(boolean z, C0889e eVar);
    }

    /* renamed from: com.baidu.location.indoor.mapversion.a.a$d */
    public static class C0888d {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public boolean f2216a = false;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public Bitmap f2217b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public String f2218c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public double f2219d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public float[] f2220e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public String f2221f;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public String f2222g;

        /* renamed from: a */
        public C0888d mo10795a(double d) {
            this.f2219d = d;
            return this;
        }

        /* renamed from: a */
        public C0888d mo10796a(String str) {
            this.f2218c = str;
            return this;
        }

        /* renamed from: a */
        public C0888d mo10797a(boolean z) {
            this.f2216a = z;
            return this;
        }

        /* renamed from: a */
        public C0888d mo10798a(float[] fArr) {
            this.f2220e = (float[]) fArr.clone();
            return this;
        }

        /* renamed from: b */
        public C0888d mo10799b(String str) {
            this.f2222g = str;
            return this;
        }

        /* renamed from: c */
        public C0888d mo10800c(String str) {
            this.f2221f = str;
            return this;
        }
    }

    /* renamed from: com.baidu.location.indoor.mapversion.a.a$e */
    public static class C0889e {

        /* renamed from: a */
        public String f2223a;

        /* renamed from: b */
        public String f2224b;

        /* renamed from: c */
        public String f2225c;

        /* renamed from: d */
        public String f2226d;

        /* renamed from: e */
        public double f2227e;

        /* renamed from: f */
        public double f2228f;

        /* renamed from: g */
        public double f2229g;

        /* renamed from: h */
        public String f2230h;

        /* renamed from: i */
        public String f2231i;

        /* renamed from: j */
        public int f2232j = 0;

        /* renamed from: k */
        public String f2233k;

        /* renamed from: a */
        public boolean mo10801a(JSONObject jSONObject) {
            this.f2224b = jSONObject.optString("bldg");
            this.f2225c = jSONObject.optString("floor");
            this.f2226d = jSONObject.optString("bldgid");
            String optString = jSONObject.optString("indoor");
            JSONObject optJSONObject = jSONObject.optJSONObject("extra");
            if (optJSONObject != null) {
                this.f2232j = optJSONObject.optInt("rpfg");
                this.f2233k = optJSONObject.optString("rpurl");
                String str = this.f2233k;
                if (str != null) {
                    this.f2233k = new String(Base64.decode(str, 0));
                }
            }
            if (optString == null || optString.equalsIgnoreCase("0")) {
                this.f2223a = "定位失败";
                return false;
            }
            String optString2 = jSONObject.optString("radius");
            if (optString2 == null) {
                this.f2223a = "定位失败";
                return false;
            }
            this.f2229g = Double.valueOf(optString2).doubleValue();
            JSONObject optJSONObject2 = jSONObject.optJSONObject(Config.EVENT_HEAT_POINT);
            if (optJSONObject2 == null) {
                this.f2223a = "定位失败";
                return false;
            }
            String optString3 = optJSONObject2.optString(Config.EVENT_HEAT_X);
            String optString4 = optJSONObject2.optString("y");
            if (!optString3.equals("-1.000000") || !optString4.equals("-1.000000")) {
                this.f2223a = "";
                this.f2228f = Double.valueOf(optString3).doubleValue();
                this.f2227e = Double.valueOf(optString4).doubleValue();
                if (optJSONObject == null) {
                    return true;
                }
                this.f2230h = optJSONObject.optString("code");
                return true;
            }
            this.f2223a = "定位失败";
            return false;
        }
    }

    /* renamed from: a */
    private void m2678a(String str) {
        C0889e eVar = new C0889e();
        eVar.f2223a = str;
        C0892d dVar = this.f2206b;
        if (dVar != null) {
            eVar.f2231i = dVar.mo10812b();
        }
        C0887c cVar = this.f2205a;
        if (cVar != null) {
            cVar.mo10794a(false, eVar);
        }
    }

    /* renamed from: a */
    private boolean m2680a(String str, String str2) {
        C0892d dVar = this.f2206b;
        if (dVar == null || !dVar.mo10811a()) {
            return false;
        }
        C0891c cVar = null;
        C0892d dVar2 = this.f2206b;
        if (dVar2 != null) {
            cVar = dVar2.mo10809a(str, str2);
        }
        if (cVar == null) {
            return false;
        }
        C0889e eVar = new C0889e();
        eVar.f2224b = cVar.mo10803a();
        eVar.f2226d = cVar.mo10804b();
        eVar.f2225c = cVar.mo10806d();
        eVar.f2228f = cVar.mo10807e();
        eVar.f2227e = cVar.mo10808f();
        eVar.f2230h = str2;
        C0892d dVar3 = this.f2206b;
        if (dVar3 != null) {
            eVar.f2231i = dVar3.mo10812b();
        }
        C0887c cVar2 = this.f2205a;
        if (cVar2 != null) {
            cVar2.mo10794a(true, eVar);
        }
        return true;
    }

    /* renamed from: b */
    private void m2682b(C0888d dVar) {
        C0886b bVar = new C0886b();
        bVar.f2213a = dVar;
        bVar.mo10734c("https://loc.map.baidu.com/ios_indoorloc");
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m2684b(String str) {
        try {
            File file = new File(str);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public String m2686c(String str) {
        if (str == null) {
            return null;
        }
        try {
            File file = new File(str);
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] bArr = new byte[((int) file.length())];
            fileInputStream.read(bArr);
            fileInputStream.close();
            return Base64.encodeToString(bArr, 2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public void mo10792a(C0888d dVar) {
        if (dVar.f2222g == null) {
            m2678a("no bid");
            return;
        }
        this.f2206b.mo10810a(dVar.f2222g);
        if (dVar.f2221f == null || dVar.f2216a || !m2680a(dVar.f2222g, dVar.f2221f)) {
            m2682b(dVar);
        }
    }
}
