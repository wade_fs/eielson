package com.baidu.location.indoor.mapversion;

import android.os.Build;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: com.baidu.location.indoor.mapversion.a */
public class C0883a {

    /* renamed from: a */
    private static Lock f2204a = new ReentrantLock();

    /* renamed from: a */
    public static void m2672a() {
        f2204a.lock();
        try {
            IndoorJni.stopPdr();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            f2204a.unlock();
            throw th;
        }
        f2204a.unlock();
    }

    /* renamed from: a */
    public static synchronized void m2673a(int i, float[] fArr, long j) {
        Lock lock;
        synchronized (C0883a.class) {
            f2204a.lock();
            try {
                if (m2674b() && fArr != null && fArr.length >= 3) {
                    IndoorJni.phs(i, fArr[0], fArr[1], fArr[2], j);
                }
                lock = f2204a;
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                    lock = f2204a;
                } catch (Throwable th) {
                    f2204a.unlock();
                    throw th;
                }
            }
            lock.unlock();
        }
        return;
    }

    /* renamed from: b */
    public static boolean m2674b() {
        if (Build.VERSION.SDK_INT < 19) {
            return false;
        }
        return IndoorJni.f2203a;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: c */
    public static float[] m2675c() {
        f2204a.lock();
        try {
            float[] pgo = IndoorJni.pgo();
            f2204a.unlock();
            return pgo;
        } catch (Exception e) {
            e.printStackTrace();
            f2204a.unlock();
            return null;
        } catch (Throwable th) {
            f2204a.unlock();
            throw th;
        }
    }
}
