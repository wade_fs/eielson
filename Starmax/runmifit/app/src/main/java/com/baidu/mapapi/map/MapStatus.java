package com.baidu.mapapi.map;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;

public final class MapStatus implements Parcelable {
    public static final Parcelable.Creator<MapStatus> CREATOR = new C0934k();

    /* renamed from: a */
    C1064ab f2538a;

    /* renamed from: b */
    private double f2539b;
    public final LatLngBounds bound;

    /* renamed from: c */
    private double f2540c;
    public final float overlook;
    public final float rotate;
    public final LatLng target;
    public final Point targetScreen;
    public WinRound winRound;
    public final float zoom;

    public static final class Builder {

        /* renamed from: a */
        private float f2541a = -2.14748365E9f;

        /* renamed from: b */
        private LatLng f2542b = null;

        /* renamed from: c */
        private float f2543c = -2.14748365E9f;

        /* renamed from: d */
        private float f2544d = -2.14748365E9f;

        /* renamed from: e */
        private Point f2545e = null;

        /* renamed from: f */
        private LatLngBounds f2546f = null;

        /* renamed from: g */
        private double f2547g = 0.0d;

        /* renamed from: h */
        private double f2548h = 0.0d;

        /* renamed from: i */
        private final float f2549i = 15.0f;

        public Builder() {
        }

        public Builder(MapStatus mapStatus) {
            this.f2541a = mapStatus.rotate;
            this.f2542b = mapStatus.target;
            this.f2543c = mapStatus.overlook;
            this.f2544d = mapStatus.zoom;
            this.f2545e = mapStatus.targetScreen;
            this.f2547g = mapStatus.mo11178a();
            this.f2548h = mapStatus.mo11179b();
        }

        /* renamed from: a */
        private float m2912a(float f) {
            if (15.0f == f) {
                return 15.5f;
            }
            return f;
        }

        public MapStatus build() {
            return new MapStatus(this.f2541a, this.f2542b, this.f2543c, this.f2544d, this.f2545e, this.f2546f);
        }

        public Builder overlook(float f) {
            this.f2543c = f;
            return this;
        }

        public Builder rotate(float f) {
            this.f2541a = f;
            return this;
        }

        public Builder target(LatLng latLng) {
            this.f2542b = latLng;
            return this;
        }

        public Builder targetScreen(Point point) {
            this.f2545e = point;
            return this;
        }

        public Builder zoom(float f) {
            this.f2544d = m2912a(f);
            return this;
        }
    }

    MapStatus(float f, LatLng latLng, float f2, float f3, Point point, double d, double d2, LatLngBounds latLngBounds) {
        this.rotate = f;
        this.target = latLng;
        this.overlook = f2;
        this.zoom = f3;
        this.targetScreen = point;
        this.f2539b = d;
        this.f2540c = d2;
        this.bound = latLngBounds;
    }

    MapStatus(float f, LatLng latLng, float f2, float f3, Point point, LatLngBounds latLngBounds) {
        this.rotate = f;
        this.target = latLng;
        this.overlook = f2;
        this.zoom = f3;
        this.targetScreen = point;
        LatLng latLng2 = this.target;
        if (latLng2 != null) {
            this.f2539b = CoordUtil.ll2mc(latLng2).getLongitudeE6();
            this.f2540c = CoordUtil.ll2mc(this.target).getLatitudeE6();
        }
        this.bound = latLngBounds;
    }

    MapStatus(float f, LatLng latLng, float f2, float f3, Point point, C1064ab abVar, double d, double d2, LatLngBounds latLngBounds, WinRound winRound2) {
        this.rotate = f;
        this.target = latLng;
        this.overlook = f2;
        this.zoom = f3;
        this.targetScreen = point;
        this.f2538a = abVar;
        this.f2539b = d;
        this.f2540c = d2;
        this.bound = latLngBounds;
        this.winRound = winRound2;
    }

    protected MapStatus(Parcel parcel) {
        this.rotate = parcel.readFloat();
        this.target = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        this.overlook = parcel.readFloat();
        this.zoom = parcel.readFloat();
        this.targetScreen = (Point) parcel.readParcelable(Point.class.getClassLoader());
        this.bound = (LatLngBounds) parcel.readParcelable(LatLngBounds.class.getClassLoader());
        this.f2539b = parcel.readDouble();
        this.f2540c = parcel.readDouble();
    }

    /* renamed from: a */
    static MapStatus m2907a(C1064ab abVar) {
        C1064ab abVar2 = abVar;
        if (abVar2 == null) {
            return null;
        }
        float f = (float) abVar2.f3415b;
        double d = abVar2.f3418e;
        double d2 = abVar2.f3417d;
        LatLng mc2ll = CoordUtil.mc2ll(new GeoPoint(d, d2));
        float f2 = (float) abVar2.f3416c;
        float f3 = abVar2.f3414a;
        Point point = new Point(abVar2.f3419f, abVar2.f3420g);
        LatLng mc2ll2 = CoordUtil.mc2ll(new GeoPoint((double) abVar2.f3424k.f3437e.f2891y, (double) abVar2.f3424k.f3437e.f2890x));
        LatLng mc2ll3 = CoordUtil.mc2ll(new GeoPoint((double) abVar2.f3424k.f3438f.f2891y, (double) abVar2.f3424k.f3438f.f2890x));
        double d3 = d;
        LatLng mc2ll4 = CoordUtil.mc2ll(new GeoPoint((double) abVar2.f3424k.f3440h.f2891y, (double) abVar2.f3424k.f3440h.f2890x));
        LatLng mc2ll5 = CoordUtil.mc2ll(new GeoPoint((double) abVar2.f3424k.f3439g.f2891y, (double) abVar2.f3424k.f3439g.f2890x));
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(mc2ll2);
        builder.include(mc2ll3);
        builder.include(mc2ll4);
        builder.include(mc2ll5);
        WinRound winRound2 = abVar2.f3423j;
        return new MapStatus(f, mc2ll, f2, f3, point, abVar, d2, d3, builder.build(), winRound2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public double mo11178a() {
        return this.f2539b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public double mo11179b() {
        return this.f2540c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public C1064ab mo11180b(C1064ab abVar) {
        if (abVar == null) {
            return null;
        }
        float f = this.rotate;
        if (f != -2.14748365E9f) {
            abVar.f3415b = (int) f;
        }
        float f2 = this.zoom;
        if (f2 != -2.14748365E9f) {
            abVar.f3414a = f2;
        }
        float f3 = this.overlook;
        if (f3 != -2.14748365E9f) {
            abVar.f3416c = (int) f3;
        }
        if (this.target != null) {
            abVar.f3417d = this.f2539b;
            abVar.f3418e = this.f2540c;
        }
        Point point = this.targetScreen;
        if (point != null) {
            abVar.f3419f = point.x;
            abVar.f3420g = this.targetScreen.y;
        }
        return abVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public C1064ab mo11181c() {
        return mo11180b(new C1064ab());
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.target != null) {
            sb.append("target lat: " + this.target.latitude + "\n");
            sb.append("target lng: " + this.target.longitude + "\n");
        }
        if (this.targetScreen != null) {
            sb.append("target screen x: " + this.targetScreen.x + "\n");
            sb.append("target screen y: " + this.targetScreen.y + "\n");
        }
        sb.append("zoom: " + this.zoom + "\n");
        sb.append("rotate: " + this.rotate + "\n");
        sb.append("overlook: " + this.overlook + "\n");
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.rotate);
        parcel.writeParcelable(this.target, i);
        parcel.writeFloat(this.overlook);
        parcel.writeFloat(this.zoom);
        parcel.writeParcelable(this.targetScreen, i);
        parcel.writeParcelable(this.bound, i);
        parcel.writeDouble(this.f2539b);
        parcel.writeDouble(this.f2540c);
    }
}
