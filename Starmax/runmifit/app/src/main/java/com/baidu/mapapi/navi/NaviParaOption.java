package com.baidu.mapapi.navi;

import com.baidu.mapapi.model.LatLng;

public class NaviParaOption {

    /* renamed from: a */
    LatLng f2897a;

    /* renamed from: b */
    String f2898b;

    /* renamed from: c */
    LatLng f2899c;

    /* renamed from: d */
    String f2900d;

    public NaviParaOption endName(String str) {
        this.f2900d = str;
        return this;
    }

    public NaviParaOption endPoint(LatLng latLng) {
        this.f2899c = latLng;
        return this;
    }

    public String getEndName() {
        return this.f2900d;
    }

    public LatLng getEndPoint() {
        return this.f2899c;
    }

    public String getStartName() {
        return this.f2898b;
    }

    public LatLng getStartPoint() {
        return this.f2897a;
    }

    public NaviParaOption startName(String str) {
        this.f2898b = str;
        return this;
    }

    public NaviParaOption startPoint(LatLng latLng) {
        this.f2897a = latLng;
        return this;
    }
}
