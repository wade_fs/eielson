package com.baidu.mapapi.map;

import android.view.View;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;

/* renamed from: com.baidu.mapapi.map.z */
class C0951z implements View.OnClickListener {

    /* renamed from: a */
    final /* synthetic */ WearMapView f2880a;

    C0951z(WearMapView wearMapView) {
        this.f2880a = wearMapView;
    }

    public void onClick(View view) {
        C1064ab E = this.f2880a.f2811f.mo13086a().mo12989E();
        E.f3414a += 1.0f;
        this.f2880a.f2811f.mo13086a().mo13016a(E, 300);
    }
}
