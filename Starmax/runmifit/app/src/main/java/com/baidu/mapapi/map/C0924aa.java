package com.baidu.mapapi.map;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/* renamed from: com.baidu.mapapi.map.aa */
class C0924aa extends AnimatorListenerAdapter {

    /* renamed from: a */
    final /* synthetic */ View f2834a;

    /* renamed from: b */
    final /* synthetic */ WearMapView f2835b;

    C0924aa(WearMapView wearMapView, View view) {
        this.f2835b = wearMapView;
        this.f2834a = view;
    }

    public void onAnimationEnd(Animator animator) {
        this.f2834a.setVisibility(View.INVISIBLE);
        super.onAnimationEnd(animator);
    }
}
