package com.baidu.location.p013a;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import com.baidu.location.Jni;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import com.tencent.mid.api.MidEntity;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import org.json.JSONObject;

/* renamed from: com.baidu.location.a.h */
public class C0736h {

    /* renamed from: c */
    private static Object f1311c = new Object();

    /* renamed from: d */
    private static C0736h f1312d = null;

    /* renamed from: e */
    private static final String f1313e = (C0855k.m2472j() + "/hst.db");

    /* renamed from: a */
    C0737a f1314a = null;

    /* renamed from: b */
    C0737a f1315b = null;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public SQLiteDatabase f1316f = null;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f1317g = false;

    /* renamed from: h */
    private String f1318h = null;

    /* renamed from: i */
    private int f1319i = -2;

    /* renamed from: com.baidu.location.a.h$a */
    class C0737a extends C0849e {

        /* renamed from: b */
        private String f1321b = null;

        /* renamed from: c */
        private String f1322c = null;

        /* renamed from: d */
        private boolean f1323d = true;

        /* renamed from: e */
        private boolean f1324e = false;

        C0737a() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1883i = 1;
            this.f1882h = C0855k.m2466e();
            String encodeTp4 = Jni.encodeTp4(this.f1322c);
            this.f1322c = null;
            this.f1885k.put("bloc", encodeTp4);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.h.a(com.baidu.location.a.h, boolean):boolean
         arg types: [com.baidu.location.a.h, int]
         candidates:
          com.baidu.location.a.h.a(com.baidu.location.a.h, android.os.Bundle):void
          com.baidu.location.a.h.a(com.baidu.location.a.h, boolean):boolean */
        /* renamed from: a */
        public void mo10459a(String str, String str2) {
            if (!C0736h.this.f1317g) {
                boolean unused = C0736h.this.f1317g = true;
                this.f1321b = str;
                this.f1322c = str2;
                ExecutorService c = C0762v.m1943a().mo10505c();
                if (c != null) {
                    mo10731a(c, C0855k.f1962f);
                } else {
                    mo10734c(C0855k.f1962f);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.h.a(com.baidu.location.a.h, boolean):boolean
         arg types: [com.baidu.location.a.h, int]
         candidates:
          com.baidu.location.a.h.a(com.baidu.location.a.h, android.os.Bundle):void
          com.baidu.location.a.h.a(com.baidu.location.a.h, boolean):boolean */
        /* JADX WARNING: Can't wrap try/catch for region: R(6:16|17|18|(1:20)|21|22) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x00a5 */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo10446a(boolean r11) {
            /*
                r10 = this;
                java.lang.String r0 = "hstdata"
                java.lang.String r1 = "mac"
                java.lang.String r2 = "content"
                java.lang.String r3 = "imo"
                if (r11 == 0) goto L_0x00c0
                java.lang.String r11 = r10.f1884j
                if (r11 == 0) goto L_0x00c0
                java.lang.String r11 = r10.f1884j     // Catch:{ Exception -> 0x00be }
                boolean r4 = r10.f1323d     // Catch:{ Exception -> 0x00be }
                if (r4 == 0) goto L_0x00c9
                org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x00be }
                r4.<init>(r11)     // Catch:{ Exception -> 0x00be }
                boolean r11 = r4.has(r2)     // Catch:{ Exception -> 0x00be }
                r5 = 0
                if (r11 == 0) goto L_0x0025
                org.json.JSONObject r11 = r4.getJSONObject(r2)     // Catch:{ Exception -> 0x00be }
                goto L_0x0026
            L_0x0025:
                r11 = r5
            L_0x0026:
                if (r11 == 0) goto L_0x00c9
                boolean r2 = r11.has(r3)     // Catch:{ Exception -> 0x00be }
                if (r2 == 0) goto L_0x00c9
                org.json.JSONObject r2 = r11.getJSONObject(r3)     // Catch:{ Exception -> 0x00be }
                java.lang.String r2 = r2.getString(r1)     // Catch:{ Exception -> 0x00be }
                java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ Exception -> 0x00be }
                org.json.JSONObject r11 = r11.getJSONObject(r3)     // Catch:{ Exception -> 0x00be }
                java.lang.String r3 = "mv"
                int r11 = r11.getInt(r3)     // Catch:{ Exception -> 0x00be }
                java.lang.String r3 = r10.f1321b     // Catch:{ Exception -> 0x00be }
                java.lang.Long r3 = com.baidu.location.Jni.encode3(r3)     // Catch:{ Exception -> 0x00be }
                long r3 = r3.longValue()     // Catch:{ Exception -> 0x00be }
                long r6 = r2.longValue()     // Catch:{ Exception -> 0x00be }
                int r8 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
                if (r8 != 0) goto L_0x00c9
                android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Exception -> 0x00be }
                r3.<init>()     // Catch:{ Exception -> 0x00be }
                java.lang.String r4 = "tt"
                long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00be }
                r8 = 1000(0x3e8, double:4.94E-321)
                long r6 = r6 / r8
                int r7 = (int) r6     // Catch:{ Exception -> 0x00be }
                java.lang.Integer r6 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x00be }
                r3.put(r4, r6)     // Catch:{ Exception -> 0x00be }
                java.lang.String r4 = "hst"
                java.lang.Integer r6 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x00be }
                r3.put(r4, r6)     // Catch:{ Exception -> 0x00be }
                com.baidu.location.a.h r4 = com.baidu.location.p013a.C0736h.this     // Catch:{ Exception -> 0x00a5 }
                android.database.sqlite.SQLiteDatabase r4 = r4.f1316f     // Catch:{ Exception -> 0x00a5 }
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a5 }
                r6.<init>()     // Catch:{ Exception -> 0x00a5 }
                java.lang.String r7 = "id = \""
                r6.append(r7)     // Catch:{ Exception -> 0x00a5 }
                r6.append(r2)     // Catch:{ Exception -> 0x00a5 }
                java.lang.String r7 = "\""
                r6.append(r7)     // Catch:{ Exception -> 0x00a5 }
                java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00a5 }
                int r4 = r4.update(r0, r3, r6, r5)     // Catch:{ Exception -> 0x00a5 }
                if (r4 > 0) goto L_0x00a5
                java.lang.String r4 = "id"
                r3.put(r4, r2)     // Catch:{ Exception -> 0x00a5 }
                com.baidu.location.a.h r2 = com.baidu.location.p013a.C0736h.this     // Catch:{ Exception -> 0x00a5 }
                android.database.sqlite.SQLiteDatabase r2 = r2.f1316f     // Catch:{ Exception -> 0x00a5 }
                r2.insert(r0, r5, r3)     // Catch:{ Exception -> 0x00a5 }
            L_0x00a5:
                android.os.Bundle r0 = new android.os.Bundle     // Catch:{ Exception -> 0x00be }
                r0.<init>()     // Catch:{ Exception -> 0x00be }
                java.lang.String r2 = r10.f1321b     // Catch:{ Exception -> 0x00be }
                byte[] r2 = r2.getBytes()     // Catch:{ Exception -> 0x00be }
                r0.putByteArray(r1, r2)     // Catch:{ Exception -> 0x00be }
                java.lang.String r1 = "hotspot"
                r0.putInt(r1, r11)     // Catch:{ Exception -> 0x00be }
                com.baidu.location.a.h r11 = com.baidu.location.p013a.C0736h.this     // Catch:{ Exception -> 0x00be }
                r11.m1821a(r0)     // Catch:{ Exception -> 0x00be }
                goto L_0x00c9
            L_0x00be:
                goto L_0x00c9
            L_0x00c0:
                boolean r11 = r10.f1323d
                if (r11 == 0) goto L_0x00c9
                com.baidu.location.a.h r11 = com.baidu.location.p013a.C0736h.this
                r11.m1826f()
            L_0x00c9:
                java.util.Map r11 = r10.f1885k
                if (r11 == 0) goto L_0x00d2
                java.util.Map r11 = r10.f1885k
                r11.clear()
            L_0x00d2:
                com.baidu.location.a.h r11 = com.baidu.location.p013a.C0736h.this
                r0 = 0
                boolean unused = r11.f1317g = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0736h.C0737a.mo10446a(boolean):void");
        }
    }

    /* renamed from: a */
    public static C0736h m1819a() {
        C0736h hVar;
        synchronized (f1311c) {
            if (f1312d == null) {
                f1312d = new C0736h();
            }
            hVar = f1312d;
        }
        return hVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m1820a(boolean r5) {
        /*
            r4 = this;
            com.baidu.location.e.b r0 = com.baidu.location.p017e.C0822b.m2279a()
            com.baidu.location.e.a r0 = r0.mo10631f()
            com.baidu.location.e.i r1 = com.baidu.location.p017e.C0835i.m2376a()
            com.baidu.location.e.h r1 = r1.mo10699p()
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r3 = 1024(0x400, float:1.435E-42)
            r2.<init>(r3)
            if (r0 == 0) goto L_0x0026
            boolean r3 = r0.mo10617b()
            if (r3 == 0) goto L_0x0026
            java.lang.String r0 = r0.mo10623h()
            r2.append(r0)
        L_0x0026:
            if (r1 == 0) goto L_0x0036
            int r0 = r1.mo10664a()
            r3 = 1
            if (r0 <= r3) goto L_0x0036
            r0 = 15
            java.lang.String r0 = r1.mo10665a(r0)
            goto L_0x0048
        L_0x0036:
            com.baidu.location.e.i r0 = com.baidu.location.p017e.C0835i.m2376a()
            java.lang.String r0 = r0.mo10696m()
            if (r0 == 0) goto L_0x004b
            com.baidu.location.e.i r0 = com.baidu.location.p017e.C0835i.m2376a()
            java.lang.String r0 = r0.mo10696m()
        L_0x0048:
            r2.append(r0)
        L_0x004b:
            if (r5 == 0) goto L_0x0052
            java.lang.String r5 = "&imo=1"
            r2.append(r5)
        L_0x0052:
            com.baidu.location.g.b r5 = com.baidu.location.p019g.C0844b.m2417a()
            r0 = 0
            java.lang.String r5 = r5.mo10713a(r0)
            r2.append(r5)
            com.baidu.location.a.a r5 = com.baidu.location.p013a.C0723a.m1729a()
            java.lang.String r5 = r5.mo10432d()
            r2.append(r5)
            java.lang.String r5 = r2.toString()
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0736h.m1820a(boolean):java.lang.String");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1821a(Bundle bundle) {
        C0723a.m1729a().mo10420a(bundle, 406);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m1826f() {
        Bundle bundle = new Bundle();
        bundle.putInt("hotspot", -1);
        m1821a(bundle);
    }

    /* renamed from: a */
    public void mo10454a(String str) {
        if (!this.f1317g) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                JSONObject jSONObject2 = jSONObject.has("content") ? jSONObject.getJSONObject("content") : null;
                if (jSONObject2 != null && jSONObject2.has("imo")) {
                    Long valueOf = Long.valueOf(jSONObject2.getJSONObject("imo").getString(MidEntity.TAG_MAC));
                    int i = jSONObject2.getJSONObject("imo").getInt("mv");
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("tt", Integer.valueOf((int) (System.currentTimeMillis() / 1000)));
                    contentValues.put("hst", Integer.valueOf(i));
                    SQLiteDatabase sQLiteDatabase = this.f1316f;
                    if (sQLiteDatabase.update("hstdata", contentValues, "id = \"" + valueOf + "\"", null) <= 0) {
                        contentValues.put(Config.FEED_LIST_ITEM_CUSTOM_ID, valueOf);
                        this.f1316f.insert("hstdata", null, contentValues);
                    }
                }
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: b */
    public void mo10455b() {
        try {
            File file = new File(f1313e);
            if (!file.exists()) {
                file.createNewFile();
            }
            if (file.exists()) {
                this.f1316f = SQLiteDatabase.openOrCreateDatabase(file, (SQLiteDatabase.CursorFactory) null);
                this.f1316f.execSQL("CREATE TABLE IF NOT EXISTS hstdata(id Long PRIMARY KEY,hst INT,tt INT);");
                this.f1316f.setVersion(1);
            }
        } catch (Exception unused) {
            this.f1316f = null;
        }
    }

    /* renamed from: c */
    public void mo10456c() {
        SQLiteDatabase sQLiteDatabase = this.f1316f;
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.close();
            } catch (Exception unused) {
            } catch (Throwable th) {
                this.f1316f = null;
                throw th;
            }
            this.f1316f = null;
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:31:0x0076 */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.lang.String[], android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r3v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r3v4, types: [android.database.Cursor] */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:34|(2:36|37)|38|39) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0076, code lost:
        if (r3 != 0) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0084, code lost:
        if (r3 == 0) goto L_0x0087;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x0082 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x0087 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int mo10457d() {
        /*
            r8 = this;
            monitor-enter(r8)
            r0 = -3
            boolean r1 = r8.f1317g     // Catch:{ all -> 0x008b }
            if (r1 == 0) goto L_0x0008
            monitor-exit(r8)
            return r0
        L_0x0008:
            boolean r1 = com.baidu.location.p017e.C0835i.m2384j()     // Catch:{ Exception -> 0x0087 }
            if (r1 == 0) goto L_0x0087
            android.database.sqlite.SQLiteDatabase r1 = r8.f1316f     // Catch:{ Exception -> 0x0087 }
            if (r1 == 0) goto L_0x0087
            com.baidu.location.e.i r1 = com.baidu.location.p017e.C0835i.m2376a()     // Catch:{ Exception -> 0x0087 }
            android.net.wifi.WifiInfo r1 = r1.mo10695l()     // Catch:{ Exception -> 0x0087 }
            if (r1 == 0) goto L_0x0087
            java.lang.String r2 = r1.getBSSID()     // Catch:{ Exception -> 0x0087 }
            if (r2 == 0) goto L_0x0087
            java.lang.String r1 = r1.getBSSID()     // Catch:{ Exception -> 0x0087 }
            java.lang.String r2 = ":"
            java.lang.String r3 = ""
            java.lang.String r1 = r1.replace(r2, r3)     // Catch:{ Exception -> 0x0087 }
            java.lang.Long r2 = com.baidu.location.Jni.encode3(r1)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r3 = r8.f1318h     // Catch:{ Exception -> 0x0087 }
            r4 = -2
            if (r3 == 0) goto L_0x0046
            java.lang.String r3 = r8.f1318h     // Catch:{ Exception -> 0x0087 }
            boolean r3 = r1.equals(r3)     // Catch:{ Exception -> 0x0087 }
            if (r3 == 0) goto L_0x0046
            int r3 = r8.f1319i     // Catch:{ Exception -> 0x0087 }
            if (r3 <= r4) goto L_0x0046
            int r0 = r8.f1319i     // Catch:{ Exception -> 0x0087 }
            goto L_0x0087
        L_0x0046:
            r3 = 0
            android.database.sqlite.SQLiteDatabase r5 = r8.f1316f     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            r6.<init>()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            java.lang.String r7 = "select * from hstdata where id = \""
            r6.append(r7)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            r6.append(r2)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            java.lang.String r2 = "\";"
            r6.append(r2)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            java.lang.String r2 = r6.toString()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            android.database.Cursor r3 = r5.rawQuery(r2, r3)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            if (r3 == 0) goto L_0x0075
            boolean r2 = r3.moveToFirst()     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            if (r2 == 0) goto L_0x0075
            r2 = 1
            int r0 = r3.getInt(r2)     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            r8.f1318h = r1     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            r8.f1319i = r0     // Catch:{ Exception -> 0x0083, all -> 0x007c }
            goto L_0x0076
        L_0x0075:
            r0 = -2
        L_0x0076:
            if (r3 == 0) goto L_0x0087
        L_0x0078:
            r3.close()     // Catch:{ Exception -> 0x0087 }
            goto L_0x0087
        L_0x007c:
            r1 = move-exception
            if (r3 == 0) goto L_0x0082
            r3.close()     // Catch:{ Exception -> 0x0082 }
        L_0x0082:
            throw r1     // Catch:{ Exception -> 0x0087 }
        L_0x0083:
            if (r3 == 0) goto L_0x0087
            goto L_0x0078
        L_0x0087:
            r8.f1319i = r0     // Catch:{ all -> 0x008b }
            monitor-exit(r8)
            return r0
        L_0x008b:
            r0 = move-exception
            monitor-exit(r8)
            goto L_0x008f
        L_0x008e:
            throw r0
        L_0x008f:
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0736h.mo10457d():int");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:24:0x0088 */
    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.String[], android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r3v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r3v2, types: [android.database.Cursor] */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:28|(2:30|31)|32|33) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0088, code lost:
        if (r3 == 0) goto L_0x009b;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x0096 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10458e() {
        /*
            r10 = this;
            boolean r0 = r10.f1317g
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            boolean r0 = com.baidu.location.p017e.C0835i.m2384j()     // Catch:{ Exception -> 0x00b9 }
            if (r0 == 0) goto L_0x00b6
            android.database.sqlite.SQLiteDatabase r0 = r10.f1316f     // Catch:{ Exception -> 0x00b9 }
            if (r0 == 0) goto L_0x00b6
            com.baidu.location.e.i r0 = com.baidu.location.p017e.C0835i.m2376a()     // Catch:{ Exception -> 0x00b9 }
            android.net.wifi.WifiInfo r0 = r0.mo10695l()     // Catch:{ Exception -> 0x00b9 }
            if (r0 == 0) goto L_0x00b6
            java.lang.String r1 = r0.getBSSID()     // Catch:{ Exception -> 0x00b9 }
            if (r1 == 0) goto L_0x00b6
            java.lang.String r0 = r0.getBSSID()     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r1 = ":"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x00b9 }
            java.lang.Long r1 = com.baidu.location.Jni.encode3(r0)     // Catch:{ Exception -> 0x00b9 }
            r2 = 0
            r3 = 0
            r4 = 1
            android.database.sqlite.SQLiteDatabase r5 = r10.f1316f     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            r6.<init>()     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            java.lang.String r7 = "select * from hstdata where id = \""
            r6.append(r7)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            r6.append(r1)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            java.lang.String r1 = "\";"
            r6.append(r1)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            java.lang.String r1 = r6.toString()     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            android.database.Cursor r3 = r5.rawQuery(r1, r3)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            if (r3 == 0) goto L_0x0087
            boolean r1 = r3.moveToFirst()     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            if (r1 == 0) goto L_0x0087
            int r1 = r3.getInt(r4)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            r5 = 2
            int r5 = r3.getInt(r5)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 / r8
            long r8 = (long) r5     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            long r6 = r6 - r8
            r8 = 259200(0x3f480, double:1.28062E-318)
            int r5 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r5 <= 0) goto L_0x0070
            goto L_0x0087
        L_0x0070:
            android.os.Bundle r5 = new android.os.Bundle     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            r5.<init>()     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            java.lang.String r6 = "mac"
            byte[] r7 = r0.getBytes()     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            r5.putByteArray(r6, r7)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            java.lang.String r6 = "hotspot"
            r5.putInt(r6, r1)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            r10.m1821a(r5)     // Catch:{ Exception -> 0x0097, all -> 0x0090 }
            goto L_0x0088
        L_0x0087:
            r2 = 1
        L_0x0088:
            if (r3 == 0) goto L_0x009b
        L_0x008a:
            r3.close()     // Catch:{ Exception -> 0x008e }
            goto L_0x009b
        L_0x008e:
            goto L_0x009b
        L_0x0090:
            r0 = move-exception
            if (r3 == 0) goto L_0x0096
            r3.close()     // Catch:{ Exception -> 0x0096 }
        L_0x0096:
            throw r0     // Catch:{ Exception -> 0x00b9 }
        L_0x0097:
            if (r3 == 0) goto L_0x009b
            goto L_0x008a
        L_0x009b:
            if (r2 == 0) goto L_0x00b9
            com.baidu.location.a.h$a r1 = r10.f1314a     // Catch:{ Exception -> 0x00b9 }
            if (r1 != 0) goto L_0x00a8
            com.baidu.location.a.h$a r1 = new com.baidu.location.a.h$a     // Catch:{ Exception -> 0x00b9 }
            r1.<init>()     // Catch:{ Exception -> 0x00b9 }
            r10.f1314a = r1     // Catch:{ Exception -> 0x00b9 }
        L_0x00a8:
            com.baidu.location.a.h$a r1 = r10.f1314a     // Catch:{ Exception -> 0x00b9 }
            if (r1 == 0) goto L_0x00b9
            com.baidu.location.a.h$a r1 = r10.f1314a     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r2 = r10.m1820a(r4)     // Catch:{ Exception -> 0x00b9 }
            r1.mo10459a(r0, r2)     // Catch:{ Exception -> 0x00b9 }
            goto L_0x00b9
        L_0x00b6:
            r10.m1826f()     // Catch:{ Exception -> 0x00b9 }
        L_0x00b9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0736h.mo10458e():void");
    }
}
