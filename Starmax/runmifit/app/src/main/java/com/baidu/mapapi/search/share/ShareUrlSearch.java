package com.baidu.mapapi.search.share;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.search.core.C0968l;
import com.baidu.mapapi.search.share.RouteShareURLOption;
import com.baidu.platform.core.p035e.C1365a;
import com.baidu.platform.core.p035e.C1372h;

public class ShareUrlSearch extends C0968l {

    /* renamed from: a */
    C1365a f3131a = new C1372h();

    /* renamed from: b */
    private boolean f3132b = false;

    ShareUrlSearch() {
    }

    /* renamed from: a */
    private boolean m3245a(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    public static ShareUrlSearch newInstance() {
        BMapManager.init();
        return new ShareUrlSearch();
    }

    public void destroy() {
        if (!this.f3132b) {
            this.f3132b = true;
            this.f3131a.mo14068a();
            BMapManager.destroy();
        }
    }

    public boolean requestLocationShareUrl(LocationShareURLOption locationShareURLOption) {
        if (this.f3131a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher has been destroyed");
        } else if (locationShareURLOption != null && locationShareURLOption.mLocation != null && locationShareURLOption.mName != null && locationShareURLOption.mSnippet != null) {
            return this.f3131a.mo14070a(locationShareURLOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or name or snippet  can not be null");
        }
    }

    public boolean requestPoiDetailShareUrl(PoiDetailShareURLOption poiDetailShareURLOption) {
        if (this.f3131a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher has been destroyed");
        } else if (poiDetailShareURLOption != null && poiDetailShareURLOption.mUid != null) {
            return this.f3131a.mo14071a(poiDetailShareURLOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or uid can not be null");
        }
    }

    public boolean requestRouteShareUrl(RouteShareURLOption routeShareURLOption) {
        if (this.f3131a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher has been destroyed");
        } else if (routeShareURLOption == null) {
            throw new IllegalArgumentException("BDMapSDKException: option is null");
        } else if (routeShareURLOption.getmMode().ordinal() < 0) {
            return false;
        } else {
            if (routeShareURLOption.mFrom == null || routeShareURLOption.mTo == null) {
                throw new IllegalArgumentException("BDMapSDKException: start or end point can not be null");
            }
            if (routeShareURLOption.mMode == RouteShareURLOption.RouteShareMode.BUS_ROUTE_SHARE_MODE) {
                if ((routeShareURLOption.mFrom.getLocation() == null || routeShareURLOption.mTo.getLocation() == null) && routeShareURLOption.mCityCode < 0) {
                    throw new IllegalArgumentException("BDMapSDKException: city code can not be null if don't set start or end point");
                }
            } else if (routeShareURLOption.mFrom.getLocation() == null && !m3245a(routeShareURLOption.mFrom.getCity())) {
                throw new IllegalArgumentException("BDMapSDKException: start cityCode must be set if not set start location");
            } else if (routeShareURLOption.mTo.getLocation() == null && !m3245a(routeShareURLOption.mTo.getCity())) {
                throw new IllegalArgumentException("BDMapSDKException: end cityCode must be set if not set end location");
            }
            return this.f3131a.mo14072a(routeShareURLOption);
        }
    }

    public void setOnGetShareUrlResultListener(OnGetShareUrlResultListener onGetShareUrlResultListener) {
        C1365a aVar = this.f3131a;
        if (aVar == null) {
            throw new IllegalStateException("BDMapSDKException: searcher has been destroyed");
        } else if (onGetShareUrlResultListener != null) {
            aVar.mo14069a(onGetShareUrlResultListener);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: listener can not be null");
        }
    }
}
