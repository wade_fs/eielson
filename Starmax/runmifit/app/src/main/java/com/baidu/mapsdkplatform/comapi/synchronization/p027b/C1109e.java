package com.baidu.mapsdkplatform.comapi.synchronization.p027b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceConstant;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceData;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceDisplayOptions;
import com.baidu.mapapi.synchronization.histroytrace.OnHistoryTraceListener;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.b.e */
class C1109e extends Handler {

    /* renamed from: a */
    private static final String f3662a = C1109e.class.getSimpleName();

    /* renamed from: b */
    private static OnHistoryTraceListener f3663b;

    /* renamed from: c */
    private HistoryTraceDisplayOptions f3664c;

    /* renamed from: d */
    private BaiduMap f3665d;

    /* renamed from: e */
    private int f3666e;

    /* renamed from: f */
    private Marker f3667f;

    /* renamed from: g */
    private Marker f3668g;

    /* renamed from: h */
    private Marker f3669h;

    /* renamed from: i */
    private List<Polyline> f3670i = new CopyOnWriteArrayList();

    C1109e(Looper looper) {
        super(looper);
    }

    /* renamed from: a */
    private void m3804a(int i, String str) {
        OnHistoryTraceListener onHistoryTraceListener = f3663b;
        if (onHistoryTraceListener == null) {
            C1120a.m3855b(f3662a, "OnHistoryTraceListener is null");
        } else {
            onHistoryTraceListener.onRenderHistroyTrace(i, str);
        }
    }

    /* renamed from: a */
    private void m3805a(BitmapDescriptor bitmapDescriptor, int i, List<HistoryTraceData.HistoryTracePoint> list) {
        List<List<LatLng>> b = m3810b(list);
        if (b == null || b.isEmpty()) {
            C1120a.m3855b(f3662a, "Calculate sub section points error");
            return;
        }
        m3811b();
        for (int i2 = 0; i2 < b.size(); i2++) {
            List list2 = b.get(i2);
            if (list2 != null && !list2.isEmpty()) {
                if (list2.size() < 2) {
                    String str = f3662a;
                    C1120a.m3855b(str, "Error points list, index = " + i2);
                } else {
                    m3806a((Polyline) this.f3665d.addOverlay(new PolylineOptions().width(i).points(list2).dottedLine(true).customTexture(bitmapDescriptor).zIndex(4)));
                }
            }
        }
    }

    /* renamed from: a */
    private void m3806a(Polyline polyline) {
        this.f3670i.add(polyline);
    }

    /* renamed from: a */
    private void m3807a(LatLng latLng) {
        if (!this.f3664c.isShowStartPositionIcon()) {
            C1120a.m3855b(f3662a, "User set not render start point marker");
        } else if (latLng == null) {
            C1120a.m3855b(f3662a, "Start point is null");
            m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_START_POINT_NULL, "History trace end point is null, can't render start point marker");
        } else {
            BitmapDescriptor startPositionIcon = this.f3664c.getStartPositionIcon();
            if (startPositionIcon == null) {
                C1120a.m3855b(f3662a, "There is no startPositionIcon");
                m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_START_POINT_ICON_NULL, "History trace end point icon is null, can't render start point marker");
                return;
            }
            this.f3667f = (Marker) this.f3665d.addOverlay(new MarkerOptions().position(latLng).icon(startPositionIcon).zIndex(5));
        }
    }

    /* renamed from: a */
    private void m3808a(HistoryTraceData historyTraceData) {
        if (5 != this.f3666e) {
            C1120a.m3855b(f3662a, "Current order state not the complete state, render forbidden");
            m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_CURRENT_ORDER_STATE_NOT_COMPLETE, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_CURRENT_ORDER_STATE_NOT_COMPLETE);
            return;
        }
        BaiduMap baiduMap = this.f3665d;
        if (baiduMap == null) {
            m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_BAIDUMAP_NULL, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_BAIDUMAP_NULL);
            return;
        }
        baiduMap.clear();
        if (this.f3664c == null) {
            m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_DISPLAY_OPTIONS_NULL, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_DISPLAY_OPTIONS_NULL);
            this.f3664c = new HistoryTraceDisplayOptions();
        }
        m3807a(historyTraceData.getOrderStartPosition());
        m3813b(historyTraceData.getOrderEndPosition());
        List<HistoryTraceData.HistoryTracePoint> pointsList = historyTraceData.getPointsList();
        if (pointsList != null && !pointsList.isEmpty()) {
            m3815c(pointsList.get(0).getPoint());
        }
        m3809a(pointsList);
        m3814c();
    }

    /* renamed from: a */
    private void m3809a(List<HistoryTraceData.HistoryTracePoint> list) {
        if (!this.f3664c.isShowRoutePlan()) {
            C1120a.m3855b(f3662a, "User set not render route polyline");
        } else if (list == null || list.isEmpty()) {
            C1120a.m3855b(f3662a, "There is no points data");
            m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_POINTS_NULL, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_POINTS_NULL);
        } else if (list.size() < 2) {
            C1120a.m3855b(f3662a, "History trace points less than 2, can't render polyline");
            m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_POINTS_LESS, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_POINTS_LESS);
        } else {
            BitmapDescriptor routeLineTexture = this.f3664c.getRouteLineTexture();
            if (routeLineTexture == null) {
                C1120a.m3855b(f3662a, "Route polyline texture is null");
                m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_ROUTE_TEXTURE_NULL, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_ROUTE_TEXTURE_NULL);
                return;
            }
            int routeLineWidth = this.f3664c.getRouteLineWidth();
            if (!this.f3664c.isRouteLineRenderBySubSection()) {
                m3805a(routeLineTexture, routeLineWidth, list);
            } else {
                m3812b(routeLineTexture, routeLineWidth, list);
            }
        }
    }

    /* renamed from: b */
    private List<List<LatLng>> m3810b(List<HistoryTraceData.HistoryTracePoint> list) {
        String str;
        String str2;
        HistoryTraceData.HistoryTracePoint historyTracePoint;
        if (list == null || list.isEmpty()) {
            str = f3662a;
            str2 = "History trace point list is null";
        } else if (list.size() < 2) {
            str = f3662a;
            str2 = "History trace point list size is less than 2, can't render polyline";
        } else {
            CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size() - 1; i++) {
                HistoryTraceData.HistoryTracePoint historyTracePoint2 = list.get(i);
                if (!(historyTracePoint2 == null || (historyTracePoint = list.get(i + 1)) == null)) {
                    arrayList.add(historyTracePoint2.getPoint());
                    if (historyTracePoint.getLocationTime() - historyTracePoint2.getLocationTime() > 300) {
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.addAll(arrayList);
                        copyOnWriteArrayList.add(arrayList2);
                        arrayList.clear();
                    }
                }
            }
            if (!arrayList.isEmpty()) {
                arrayList.add(list.get(list.size() - 1).getPoint());
                copyOnWriteArrayList.add(arrayList);
            }
            return copyOnWriteArrayList;
        }
        C1120a.m3855b(str, str2);
        return null;
    }

    /* renamed from: b */
    private void m3811b() {
        List<Polyline> list = this.f3670i;
        if (list != null && !list.isEmpty()) {
            this.f3670i.clear();
        }
        if (this.f3670i == null) {
            this.f3670i = new CopyOnWriteArrayList();
        }
    }

    /* renamed from: b */
    private void m3812b(BitmapDescriptor bitmapDescriptor, int i, List<HistoryTraceData.HistoryTracePoint> list) {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            if (list.get(i2) != null) {
                arrayList.add(list.get(i2).getPoint());
            }
        }
        PolylineOptions zIndex = new PolylineOptions().width(i).points(arrayList).dottedLine(true).customTexture(bitmapDescriptor).zIndex(4);
        m3811b();
        m3806a((Polyline) this.f3665d.addOverlay(zIndex));
    }

    /* renamed from: b */
    private void m3813b(LatLng latLng) {
        if (!this.f3664c.isShowEndPositionIcon()) {
            C1120a.m3855b(f3662a, "User set not render end point marker");
        } else if (latLng == null) {
            C1120a.m3855b(f3662a, "End point is null");
            m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_END_POINT_NULL, "History trace end point is null, can't render start point marker");
        } else {
            BitmapDescriptor endPositionIcon = this.f3664c.getEndPositionIcon();
            if (endPositionIcon == null) {
                C1120a.m3855b(f3662a, "There is no endPositionIcon");
                m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_END_POINT_ICON_NULL, "History trace end point icon is null, can't render start point marker");
                return;
            }
            this.f3668g = (Marker) this.f3665d.addOverlay(new MarkerOptions().position(latLng).icon(endPositionIcon).zIndex(5));
        }
    }

    /* renamed from: c */
    private void m3814c() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        Marker marker = this.f3667f;
        if (marker != null) {
            builder.include(marker.getPosition());
        }
        Marker marker2 = this.f3668g;
        if (marker2 != null) {
            builder.include(marker2.getPosition());
        }
        Marker marker3 = this.f3669h;
        if (marker3 != null) {
            builder.include(marker3.getPosition());
        }
        List<Polyline> list = this.f3670i;
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < this.f3670i.size(); i++) {
                Polyline polyline = this.f3670i.get(i);
                if (!(polyline == null || polyline.getPoints() == null || polyline.getPoints().isEmpty())) {
                    for (int i2 = 0; i2 < polyline.getPoints().size(); i2++) {
                        builder.include(polyline.getPoints().get(i2));
                    }
                }
            }
        }
        LatLngBounds build = builder.build();
        if (build == null) {
            C1120a.m3855b(f3662a, "Visibility span is null");
            return;
        }
        this.f3665d.animateMapStatus(MapStatusUpdateFactory.newLatLngBounds(build, this.f3664c.getPaddingLeft(), this.f3664c.getPaddingTop(), this.f3664c.getPaddingRight(), this.f3664c.getPaddingBottom()));
    }

    /* renamed from: c */
    private void m3815c(LatLng latLng) {
        if (!this.f3664c.isShowCarIcon()) {
            C1120a.m3855b(f3662a, "User set not render car marker");
        } else if (latLng == null) {
            C1120a.m3855b(f3662a, "Car point is null");
            m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_CAR_POINT_NULL, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_CAR_POINT_NULL);
        } else {
            BitmapDescriptor carIcon = this.f3664c.getCarIcon();
            if (carIcon == null) {
                C1120a.m3855b(f3662a, "Car icon is null");
                m3804a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_CAR_POINT_ICON_NULL, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_CAR_POINT_ICON_NULL);
                return;
            }
            this.f3669h = (Marker) this.f3665d.addOverlay(new MarkerOptions().position(latLng).icon(carIcon).flat(true).rotate(0.0f).zIndex(5).anchor(0.5f, 0.5f));
        }
    }

    /* renamed from: a */
    public void mo13188a() {
        f3663b = null;
        Marker marker = this.f3667f;
        if (marker != null) {
            marker.remove();
            this.f3667f = null;
        }
        Marker marker2 = this.f3668g;
        if (marker2 != null) {
            marker2.remove();
            this.f3668g = null;
        }
        Marker marker3 = this.f3669h;
        if (marker3 != null) {
            marker3.remove();
            this.f3669h = null;
        }
        List<Polyline> list = this.f3670i;
        if (list != null && !list.isEmpty()) {
            this.f3670i.clear();
            this.f3670i = null;
        }
        HistoryTraceDisplayOptions historyTraceDisplayOptions = this.f3664c;
        if (historyTraceDisplayOptions != null) {
            historyTraceDisplayOptions.getCarIcon().recycle();
            this.f3664c.getStartPositionIcon().recycle();
            this.f3664c.getEndPositionIcon().recycle();
            this.f3664c.getRouteLineTexture().recycle();
            this.f3664c = null;
        }
        BaiduMap baiduMap = this.f3665d;
        if (baiduMap != null) {
            baiduMap.clear();
        }
        removeCallbacksAndMessages(null);
    }

    /* renamed from: a */
    public void mo13189a(HistoryTraceDisplayOptions historyTraceDisplayOptions, BaiduMap baiduMap, int i) {
        this.f3664c = historyTraceDisplayOptions;
        this.f3665d = baiduMap;
        this.f3666e = i;
    }

    /* renamed from: a */
    public void mo13190a(OnHistoryTraceListener onHistoryTraceListener) {
        f3663b = onHistoryTraceListener;
    }

    public void handleMessage(Message message) {
        String str = f3662a;
        C1120a.m3855b(str, "Render message type = " + message.what);
        if (message.what != 4) {
            C1120a.m3855b(f3662a, "Undefine Render message");
        } else {
            m3808a((HistoryTraceData) message.obj);
        }
    }
}
