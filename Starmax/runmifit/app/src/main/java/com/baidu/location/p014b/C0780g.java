package com.baidu.location.p014b;

import android.os.Environment;
import android.text.TextUtils;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0843a;
import com.baidu.location.p019g.C0855k;
import com.google.common.net.HttpHeaders;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.lang.Thread;
import java.net.HttpURLConnection;
import java.net.URL;

/* renamed from: com.baidu.location.b.g */
public class C0780g implements Thread.UncaughtExceptionHandler {

    /* renamed from: a */
    private static C0780g f1529a;

    /* renamed from: b */
    private int f1530b = 0;

    private C0780g() {
    }

    /* renamed from: a */
    public static C0780g m2035a() {
        if (f1529a == null) {
            f1529a = new C0780g();
        }
        return f1529a;
    }

    /* renamed from: a */
    private String m2036a(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        printWriter.close();
        return stringWriter.toString();
    }

    /* renamed from: a */
    private void m2037a(File file, String str, String str2) {
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(280);
            randomAccessFile.writeInt(12346);
            randomAccessFile.seek(300);
            randomAccessFile.writeLong(System.currentTimeMillis());
            byte[] bytes = str.getBytes();
            randomAccessFile.writeInt(bytes.length);
            randomAccessFile.write(bytes, 0, bytes.length);
            randomAccessFile.seek(600);
            byte[] bytes2 = str2.getBytes();
            randomAccessFile.writeInt(bytes2.length);
            randomAccessFile.write(bytes2, 0, bytes2.length);
            if (!m2038a(str, str2)) {
                randomAccessFile.seek(280);
                randomAccessFile.writeInt(1326);
            }
            randomAccessFile.close();
        } catch (Exception unused) {
        }
    }

    /* renamed from: a */
    private boolean m2038a(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || !C0835i.m2384j()) {
            return false;
        }
        try {
            URL url = new URL(C0855k.f1961e);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("e0");
            stringBuffer.append("=");
            stringBuffer.append(str);
            stringBuffer.append("&");
            stringBuffer.append("e1");
            stringBuffer.append("=");
            stringBuffer.append(str2);
            stringBuffer.append("&");
            if (stringBuffer.length() > 0) {
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            }
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setConnectTimeout(C0843a.f1827b);
            httpURLConnection.setReadTimeout(C0843a.f1827b);
            httpURLConnection.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded; charset=utf-8");
            httpURLConnection.setRequestProperty(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(stringBuffer.toString().getBytes());
            outputStream.flush();
            outputStream.close();
            return httpURLConnection.getResponseCode() == 200;
        } catch (Exception unused) {
        }
    }

    /* renamed from: b */
    public void mo10540b() {
        String str;
        try {
            File file = new File((Environment.getExternalStorageDirectory().getPath() + "/traces") + "/error_fs2.dat");
            if (file.exists()) {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(280);
                if (1326 == randomAccessFile.readInt()) {
                    randomAccessFile.seek(308);
                    int readInt = randomAccessFile.readInt();
                    String str2 = null;
                    if (readInt <= 0 || readInt >= 2048) {
                        str = null;
                    } else {
                        byte[] bArr = new byte[readInt];
                        randomAccessFile.read(bArr, 0, readInt);
                        str = new String(bArr, 0, readInt);
                    }
                    randomAccessFile.seek(600);
                    int readInt2 = randomAccessFile.readInt();
                    if (readInt2 > 0 && readInt2 < 2048) {
                        byte[] bArr2 = new byte[readInt2];
                        randomAccessFile.read(bArr2, 0, readInt2);
                        str2 = new String(bArr2, 0, readInt2);
                    }
                    if (m2038a(str, str2)) {
                        randomAccessFile.seek(280);
                        randomAccessFile.writeInt(12346);
                    }
                }
                randomAccessFile.close();
            }
        } catch (Exception unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00aa A[Catch:{ Exception -> 0x00b6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ba A[SYNTHETIC, Splitter:B:35:0x00ba] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void uncaughtException(java.lang.Thread r9, java.lang.Throwable r10) {
        /*
            r8 = this;
            int r9 = r8.f1530b
            r0 = 1
            int r9 = r9 + r0
            r8.f1530b = r9
            int r9 = r8.f1530b
            r1 = 2
            if (r9 <= r1) goto L_0x0013
        L_0x000b:
            int r9 = android.os.Process.myPid()
            android.os.Process.killProcess(r9)
            return
        L_0x0013:
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = com.baidu.location.p018f.C0840a.m2408b()
            long r1 = r1 - r3
            r3 = 10000(0x2710, double:4.9407E-320)
            int r9 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r9 >= 0) goto L_0x0077
            r9 = 1090141553(0x40fa3d71, float:7.82)
            float r1 = com.baidu.location.C0839f.getFrameVersion()
            int r9 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r9 <= 0) goto L_0x0077
            com.baidu.location.g.c r9 = com.baidu.location.p019g.C0845c.m2426a()
            long r1 = r9.mo10723c()
            long r3 = java.lang.System.currentTimeMillis()
            long r3 = r3 - r1
            r1 = 40000(0x9c40, double:1.97626E-319)
            int r9 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r9 >= 0) goto L_0x006c
            java.io.File r9 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = com.baidu.location.p019g.C0855k.m2472j()
            r1.append(r2)
            java.lang.String r2 = java.io.File.separator
            r1.append(r2)
            java.lang.String r2 = com.baidu.location.C0839f.getJarFileName()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.<init>(r1)
            boolean r1 = r9.exists()
            if (r1 == 0) goto L_0x0077
            r9.delete()
            goto L_0x0077
        L_0x006c:
            com.baidu.location.g.c r9 = com.baidu.location.p019g.C0845c.m2426a()
            long r1 = java.lang.System.currentTimeMillis()
            r9.mo10722b(r1)
        L_0x0077:
            r9 = 0
            r1 = 0
            java.lang.String r10 = r8.m2036a(r10)     // Catch:{ Exception -> 0x00b5 }
            if (r10 == 0) goto L_0x0088
            java.lang.String r2 = "com.baidu.location"
            boolean r2 = r10.contains(r2)     // Catch:{ Exception -> 0x00b6 }
            if (r2 == 0) goto L_0x0088
            goto L_0x0089
        L_0x0088:
            r0 = 0
        L_0x0089:
            com.baidu.location.g.b r2 = com.baidu.location.p019g.C0844b.m2417a()     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r2 = r2.mo10713a(r9)     // Catch:{ Exception -> 0x00b6 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b6 }
            r3.<init>()     // Catch:{ Exception -> 0x00b6 }
            r3.append(r2)     // Catch:{ Exception -> 0x00b6 }
            com.baidu.location.a.a r2 = com.baidu.location.p013a.C0723a.m1729a()     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r2 = r2.mo10432d()     // Catch:{ Exception -> 0x00b6 }
            r3.append(r2)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x00b6 }
            if (r2 == 0) goto L_0x00af
            java.lang.String r9 = com.baidu.location.Jni.encode(r2)     // Catch:{ Exception -> 0x00b6 }
            goto L_0x00b0
        L_0x00af:
            r9 = r1
        L_0x00b0:
            r7 = r10
            r10 = r9
            r9 = r0
            r0 = r7
            goto L_0x00b8
        L_0x00b5:
            r10 = r1
        L_0x00b6:
            r0 = r10
            r10 = r1
        L_0x00b8:
            if (r9 == 0) goto L_0x000b
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x000b }
            r9.<init>()     // Catch:{ Exception -> 0x000b }
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x000b }
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x000b }
            r9.append(r2)     // Catch:{ Exception -> 0x000b }
            java.lang.String r2 = "/traces"
            r9.append(r2)     // Catch:{ Exception -> 0x000b }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x000b }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x000b }
            r2.<init>()     // Catch:{ Exception -> 0x000b }
            r2.append(r9)     // Catch:{ Exception -> 0x000b }
            java.lang.String r3 = "/error_fs2.dat"
            r2.append(r3)     // Catch:{ Exception -> 0x000b }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x000b }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x000b }
            r3.<init>(r2)     // Catch:{ Exception -> 0x000b }
            boolean r2 = r3.exists()     // Catch:{ Exception -> 0x000b }
            if (r2 != 0) goto L_0x010a
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x000b }
            r2.<init>(r9)     // Catch:{ Exception -> 0x000b }
            boolean r9 = r2.exists()     // Catch:{ Exception -> 0x000b }
            if (r9 != 0) goto L_0x00fd
            r2.mkdirs()     // Catch:{ Exception -> 0x000b }
        L_0x00fd:
            boolean r9 = r3.createNewFile()     // Catch:{ Exception -> 0x000b }
            if (r9 != 0) goto L_0x0104
            goto L_0x0105
        L_0x0104:
            r1 = r3
        L_0x0105:
            r8.m2037a(r1, r10, r0)     // Catch:{ Exception -> 0x000b }
            goto L_0x000b
        L_0x010a:
            java.io.RandomAccessFile r9 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x000b }
            java.lang.String r1 = "rw"
            r9.<init>(r3, r1)     // Catch:{ Exception -> 0x000b }
            r1 = 300(0x12c, double:1.48E-321)
            r9.seek(r1)     // Catch:{ Exception -> 0x000b }
            long r1 = r9.readLong()     // Catch:{ Exception -> 0x000b }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x000b }
            long r4 = r4 - r1
            r1 = 86400000(0x5265c00, double:4.2687272E-316)
            int r6 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r6 <= 0) goto L_0x0129
            r8.m2037a(r3, r10, r0)     // Catch:{ Exception -> 0x000b }
        L_0x0129:
            r9.close()     // Catch:{ Exception -> 0x000b }
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p014b.C0780g.uncaughtException(java.lang.Thread, java.lang.Throwable):void");
    }
}
