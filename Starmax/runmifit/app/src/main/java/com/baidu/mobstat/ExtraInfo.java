package com.baidu.mobstat;

import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class ExtraInfo {

    /* renamed from: a */
    String f4135a = "";

    /* renamed from: b */
    String f4136b = "";

    /* renamed from: c */
    String f4137c = "";

    /* renamed from: d */
    String f4138d = "";

    /* renamed from: e */
    String f4139e = "";

    /* renamed from: f */
    String f4140f = "";

    /* renamed from: g */
    String f4141g = "";

    /* renamed from: h */
    String f4142h = "";

    /* renamed from: i */
    String f4143i = "";

    /* renamed from: j */
    String f4144j = "";

    /* renamed from: a */
    private static boolean m4441a(String str, int i) {
        int i2;
        if (str == null) {
            return false;
        }
        try {
            i2 = str.getBytes().length;
        } catch (Exception unused) {
            i2 = 0;
        }
        return i2 > i;
    }

    /* renamed from: a */
    private static String m4440a(String str) {
        if (TextUtils.isEmpty(str)) {
            str = "";
        }
        if (m4441a(str, 1024)) {
            return "";
        }
        return str;
    }

    public String getV1() {
        return this.f4135a;
    }

    public void setV1(String str) {
        this.f4135a = m4440a(str);
    }

    public String getV2() {
        return this.f4136b;
    }

    public void setV2(String str) {
        this.f4136b = m4440a(str);
    }

    public String getV3() {
        return this.f4137c;
    }

    public void setV3(String str) {
        this.f4137c = m4440a(str);
    }

    public String getV4() {
        return this.f4138d;
    }

    public void setV4(String str) {
        this.f4138d = m4440a(str);
    }

    public String getV5() {
        return this.f4139e;
    }

    public void setV5(String str) {
        this.f4139e = m4440a(str);
    }

    public String getV6() {
        return this.f4140f;
    }

    public void setV6(String str) {
        this.f4140f = m4440a(str);
    }

    public String getV7() {
        return this.f4141g;
    }

    public void setV7(String str) {
        this.f4141g = m4440a(str);
    }

    public String getV8() {
        return this.f4142h;
    }

    public void setV8(String str) {
        this.f4142h = m4440a(str);
    }

    public String getV9() {
        return this.f4143i;
    }

    public void setV9(String str) {
        this.f4143i = m4440a(str);
    }

    public String getV10() {
        return this.f4144j;
    }

    public void setV10(String str) {
        this.f4144j = m4440a(str);
    }

    public JSONObject dumpToJson() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(this.f4135a)) {
                jSONObject.put("v1", this.f4135a);
            }
            if (!TextUtils.isEmpty(this.f4136b)) {
                jSONObject.put("v2", this.f4136b);
            }
            if (!TextUtils.isEmpty(this.f4137c)) {
                jSONObject.put("v3", this.f4137c);
            }
            if (!TextUtils.isEmpty(this.f4138d)) {
                jSONObject.put("v4", this.f4138d);
            }
            if (!TextUtils.isEmpty(this.f4139e)) {
                jSONObject.put("v5", this.f4139e);
            }
            if (!TextUtils.isEmpty(this.f4140f)) {
                jSONObject.put("v6", this.f4140f);
            }
            if (!TextUtils.isEmpty(this.f4141g)) {
                jSONObject.put("v7", this.f4141g);
            }
            if (!TextUtils.isEmpty(this.f4142h)) {
                jSONObject.put("v8", this.f4142h);
            }
            if (!TextUtils.isEmpty(this.f4143i)) {
                jSONObject.put("v9", this.f4143i);
            }
            if (!TextUtils.isEmpty(this.f4144j)) {
                jSONObject.put("v10", this.f4144j);
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
