package com.baidu.location.p013a;

import android.location.Location;
import com.baidu.location.p017e.C0826e;
import com.baidu.location.p017e.C0834h;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0854j;
import com.baidu.location.p019g.C0855k;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: com.baidu.location.a.w */
public class C0765w {

    /* renamed from: A */
    private static C0765w f1434A = null;

    /* renamed from: C */
    private static long f1435C = 0;

    /* renamed from: b */
    private static ArrayList<String> f1436b = new ArrayList<>();

    /* renamed from: c */
    private static ArrayList<String> f1437c = new ArrayList<>();

    /* renamed from: d */
    private static ArrayList<String> f1438d = new ArrayList<>();

    /* renamed from: e */
    private static String f1439e = (C0854j.f1898a + "/yo.dat");

    /* renamed from: f */
    private static String f1440f = (C0854j.f1898a + "/yoh.dat");

    /* renamed from: g */
    private static String f1441g = (C0854j.f1898a + "/yom.dat");

    /* renamed from: h */
    private static String f1442h = (C0854j.f1898a + "/yol.dat");

    /* renamed from: i */
    private static String f1443i = (C0854j.f1898a + "/yor.dat");

    /* renamed from: j */
    private static File f1444j = null;

    /* renamed from: k */
    private static int f1445k = 8;

    /* renamed from: l */
    private static int f1446l = 8;

    /* renamed from: m */
    private static int f1447m = 16;

    /* renamed from: n */
    private static int f1448n = 1024;

    /* renamed from: o */
    private static double f1449o = 0.0d;

    /* renamed from: p */
    private static double f1450p = 0.1d;

    /* renamed from: q */
    private static double f1451q = 30.0d;

    /* renamed from: r */
    private static double f1452r = 100.0d;

    /* renamed from: s */
    private static int f1453s = 0;

    /* renamed from: t */
    private static int f1454t = 64;

    /* renamed from: u */
    private static int f1455u = 128;

    /* renamed from: v */
    private static Location f1456v = null;

    /* renamed from: w */
    private static Location f1457w = null;

    /* renamed from: x */
    private static Location f1458x = null;

    /* renamed from: y */
    private static C0834h f1459y = null;

    /* renamed from: B */
    private int f1460B;

    /* renamed from: a */
    long f1461a;

    /* renamed from: z */
    private C0766a f1462z;

    /* renamed from: com.baidu.location.a.w$a */
    private class C0766a extends C0849e {

        /* renamed from: a */
        boolean f1463a = false;

        /* renamed from: b */
        int f1464b = 0;

        /* renamed from: c */
        int f1465c = 0;

        /* renamed from: e */
        private ArrayList<String> f1467e = new ArrayList<>();

        /* renamed from: f */
        private boolean f1468f = true;

        public C0766a() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            String str;
            StringBuilder sb;
            Map map;
            this.f1882h = C0855k.m2466e();
            if (this.f1464b != 1) {
                this.f1882h = C0855k.m2469g();
            }
            this.f1883i = 2;
            if (this.f1467e != null) {
                for (int i = 0; i < this.f1467e.size(); i++) {
                    if (this.f1464b == 1) {
                        map = this.f1885k;
                        sb = new StringBuilder();
                        str = "cldc[";
                    } else {
                        map = this.f1885k;
                        sb = new StringBuilder();
                        str = "cltr[";
                    }
                    sb.append(str);
                    sb.append(i);
                    sb.append("]");
                    map.put(sb.toString(), this.f1467e.get(i));
                }
                this.f1885k.put("trtm", String.format(Locale.CHINA, "%d", Long.valueOf(System.currentTimeMillis())));
                if (this.f1464b != 1) {
                    this.f1885k.put("qt", "cltrg");
                }
            }
        }

        /* renamed from: a */
        public void mo10446a(boolean z) {
            if (z && this.f1884j != null) {
                ArrayList<String> arrayList = this.f1467e;
                if (arrayList != null) {
                    arrayList.clear();
                }
                try {
                    JSONObject jSONObject = new JSONObject(this.f1884j);
                    if (jSONObject.has("ison") && jSONObject.getInt("ison") == 0) {
                        this.f1468f = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (this.f1885k != null) {
                this.f1885k.clear();
            }
            this.f1463a = false;
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(5:73|74|(1:76)|77|78) */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x00b2, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x00c2, code lost:
            if (r7.f1467e != null) goto L_0x00c4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x00c4, code lost:
            r7.f1467e.clear();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x00ca, code lost:
            return;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x00c0 */
        /* renamed from: b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public synchronized void mo10508b() {
            /*
                r7 = this;
                monitor-enter(r7)
                boolean r0 = r7.f1463a     // Catch:{ all -> 0x00cb }
                if (r0 == 0) goto L_0x0007
                monitor-exit(r7)
                return
            L_0x0007:
                int r0 = com.baidu.location.p013a.C0765w.C0766a.f1881p     // Catch:{ all -> 0x00cb }
                r1 = 4
                r2 = 1
                if (r0 <= r1) goto L_0x001a
                int r0 = r7.f1465c     // Catch:{ all -> 0x00cb }
                int r1 = com.baidu.location.p013a.C0765w.C0766a.f1881p     // Catch:{ all -> 0x00cb }
                if (r0 >= r1) goto L_0x001a
                int r0 = r7.f1465c     // Catch:{ all -> 0x00cb }
                int r0 = r0 + r2
                r7.f1465c = r0     // Catch:{ all -> 0x00cb }
                monitor-exit(r7)
                return
            L_0x001a:
                r0 = 0
                r7.f1465c = r0     // Catch:{ all -> 0x00cb }
                r7.f1463a = r2     // Catch:{ all -> 0x00cb }
                r7.f1464b = r0     // Catch:{ all -> 0x00cb }
                java.util.ArrayList<java.lang.String> r1 = r7.f1467e     // Catch:{ Exception -> 0x00c0 }
                if (r1 == 0) goto L_0x002d
                java.util.ArrayList<java.lang.String> r1 = r7.f1467e     // Catch:{ Exception -> 0x00c0 }
                int r1 = r1.size()     // Catch:{ Exception -> 0x00c0 }
                if (r1 >= r2) goto L_0x0076
            L_0x002d:
                java.util.ArrayList<java.lang.String> r1 = r7.f1467e     // Catch:{ Exception -> 0x00c0 }
                if (r1 != 0) goto L_0x0038
                java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x00c0 }
                r1.<init>()     // Catch:{ Exception -> 0x00c0 }
                r7.f1467e = r1     // Catch:{ Exception -> 0x00c0 }
            L_0x0038:
                r7.f1464b = r0     // Catch:{ Exception -> 0x00c0 }
                r1 = 0
            L_0x003b:
                int r3 = r7.f1464b     // Catch:{ Exception -> 0x00c0 }
                r4 = 2
                r5 = 0
                if (r3 >= r4) goto L_0x0046
                java.lang.String r3 = com.baidu.location.p013a.C0765w.m1959b()     // Catch:{ Exception -> 0x00c0 }
                goto L_0x0047
            L_0x0046:
                r3 = r5
            L_0x0047:
                if (r3 != 0) goto L_0x005a
                int r6 = r7.f1464b     // Catch:{ Exception -> 0x00c0 }
                if (r6 == r2) goto L_0x005a
                boolean r6 = r7.f1468f     // Catch:{ Exception -> 0x00c0 }
                if (r6 == 0) goto L_0x005a
                r7.f1464b = r4     // Catch:{ Exception -> 0x00c0 }
                java.lang.String r5 = com.baidu.location.p013a.C0735g.m1814a()     // Catch:{ Exception -> 0x0058 }
                goto L_0x005d
            L_0x0058:
                goto L_0x005d
            L_0x005a:
                r7.f1464b = r2     // Catch:{ Exception -> 0x00c0 }
                r5 = r3
            L_0x005d:
                if (r5 != 0) goto L_0x0060
                goto L_0x0076
            L_0x0060:
                java.lang.String r3 = "err!"
                boolean r3 = r5.contains(r3)     // Catch:{ Exception -> 0x00c0 }
                if (r3 != 0) goto L_0x003b
                java.util.ArrayList<java.lang.String> r3 = r7.f1467e     // Catch:{ Exception -> 0x00c0 }
                r3.add(r5)     // Catch:{ Exception -> 0x00c0 }
                int r3 = r5.length()     // Catch:{ Exception -> 0x00c0 }
                int r1 = r1 + r3
                int r3 = com.baidu.location.p019g.C0843a.f1834i     // Catch:{ Exception -> 0x00c0 }
                if (r1 < r3) goto L_0x003b
            L_0x0076:
                java.util.ArrayList<java.lang.String> r1 = r7.f1467e     // Catch:{ Exception -> 0x00c0 }
                if (r1 == 0) goto L_0x00b3
                java.util.ArrayList<java.lang.String> r1 = r7.f1467e     // Catch:{ Exception -> 0x00c0 }
                int r1 = r1.size()     // Catch:{ Exception -> 0x00c0 }
                if (r1 >= r2) goto L_0x0083
                goto L_0x00b3
            L_0x0083:
                int r0 = r7.f1464b     // Catch:{ all -> 0x00cb }
                if (r0 == r2) goto L_0x00a1
                com.baidu.location.a.v r0 = com.baidu.location.p013a.C0762v.m1943a()     // Catch:{ all -> 0x00cb }
                java.util.concurrent.ExecutorService r0 = r0.mo10505c()     // Catch:{ all -> 0x00cb }
                if (r0 == 0) goto L_0x0099
                java.lang.String r1 = com.baidu.location.p019g.C0855k.m2469g()     // Catch:{ all -> 0x00cb }
            L_0x0095:
                r7.mo10731a(r0, r1)     // Catch:{ all -> 0x00cb }
                goto L_0x00b1
            L_0x0099:
                java.lang.String r0 = com.baidu.location.p019g.C0855k.m2469g()     // Catch:{ all -> 0x00cb }
            L_0x009d:
                r7.mo10734c(r0)     // Catch:{ all -> 0x00cb }
                goto L_0x00b1
            L_0x00a1:
                com.baidu.location.a.v r0 = com.baidu.location.p013a.C0762v.m1943a()     // Catch:{ all -> 0x00cb }
                java.util.concurrent.ExecutorService r0 = r0.mo10505c()     // Catch:{ all -> 0x00cb }
                if (r0 == 0) goto L_0x00ae
                java.lang.String r1 = com.baidu.location.p019g.C0855k.f1962f     // Catch:{ all -> 0x00cb }
                goto L_0x0095
            L_0x00ae:
                java.lang.String r0 = com.baidu.location.p019g.C0855k.f1962f     // Catch:{ all -> 0x00cb }
                goto L_0x009d
            L_0x00b1:
                monitor-exit(r7)
                return
            L_0x00b3:
                java.util.ArrayList<java.lang.String> r1 = r7.f1467e     // Catch:{ Exception -> 0x00c0 }
                if (r1 == 0) goto L_0x00bc
                java.util.ArrayList<java.lang.String> r1 = r7.f1467e     // Catch:{ Exception -> 0x00c0 }
                r1.clear()     // Catch:{ Exception -> 0x00c0 }
            L_0x00bc:
                r7.f1463a = r0     // Catch:{ Exception -> 0x00c0 }
                monitor-exit(r7)
                return
            L_0x00c0:
                java.util.ArrayList<java.lang.String> r0 = r7.f1467e     // Catch:{ all -> 0x00cb }
                if (r0 == 0) goto L_0x00c9
                java.util.ArrayList<java.lang.String> r0 = r7.f1467e     // Catch:{ all -> 0x00cb }
                r0.clear()     // Catch:{ all -> 0x00cb }
            L_0x00c9:
                monitor-exit(r7)
                return
            L_0x00cb:
                r0 = move-exception
                monitor-exit(r7)
                goto L_0x00cf
            L_0x00ce:
                throw r0
            L_0x00cf:
                goto L_0x00ce
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0765w.C0766a.mo10508b():void");
        }
    }

    private C0765w() {
        this.f1462z = null;
        this.f1460B = 0;
        this.f1461a = 0;
        this.f1462z = new C0766a();
        this.f1460B = 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00d2, code lost:
        return -1;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized int m1948a(java.util.List<java.lang.String> r17, int r18) {
        /*
            r0 = r17
            r1 = r18
            java.lang.Class<com.baidu.location.a.w> r2 = com.baidu.location.p013a.C0765w.class
            monitor-enter(r2)
            if (r0 == 0) goto L_0x00d1
            r3 = 256(0x100, float:3.59E-43)
            if (r1 > r3) goto L_0x00d1
            if (r1 >= 0) goto L_0x0011
            goto L_0x00d1
        L_0x0011:
            java.io.File r3 = com.baidu.location.p013a.C0765w.f1444j     // Catch:{ Exception -> 0x00c8 }
            if (r3 != 0) goto L_0x002c
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x00c8 }
            java.lang.String r4 = com.baidu.location.p013a.C0765w.f1439e     // Catch:{ Exception -> 0x00c8 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x00c8 }
            com.baidu.location.p013a.C0765w.f1444j = r3     // Catch:{ Exception -> 0x00c8 }
            java.io.File r3 = com.baidu.location.p013a.C0765w.f1444j     // Catch:{ Exception -> 0x00c8 }
            boolean r3 = r3.exists()     // Catch:{ Exception -> 0x00c8 }
            if (r3 != 0) goto L_0x002c
            r0 = 0
            com.baidu.location.p013a.C0765w.f1444j = r0     // Catch:{ Exception -> 0x00c8 }
            r0 = -2
            monitor-exit(r2)
            return r0
        L_0x002c:
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x00c8 }
            java.io.File r4 = com.baidu.location.p013a.C0765w.f1444j     // Catch:{ Exception -> 0x00c8 }
            java.lang.String r5 = "rw"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x00c8 }
            long r4 = r3.length()     // Catch:{ Exception -> 0x00c8 }
            r6 = 1
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 >= 0) goto L_0x0045
            r3.close()     // Catch:{ Exception -> 0x00c8 }
            r0 = -3
            monitor-exit(r2)
            return r0
        L_0x0045:
            long r4 = (long) r1
            r3.seek(r4)     // Catch:{ Exception -> 0x00c8 }
            int r1 = r3.readInt()     // Catch:{ Exception -> 0x00c8 }
            int r12 = r3.readInt()     // Catch:{ Exception -> 0x00c8 }
            int r13 = r3.readInt()     // Catch:{ Exception -> 0x00c8 }
            int r14 = r3.readInt()     // Catch:{ Exception -> 0x00c8 }
            long r10 = r3.readLong()     // Catch:{ Exception -> 0x00c8 }
            r6 = r1
            r7 = r12
            r8 = r13
            r9 = r14
            r15 = r10
            boolean r6 = m1954a(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x00c8 }
            if (r6 == 0) goto L_0x00c0
            r6 = 1
            if (r12 >= r6) goto L_0x006c
            goto L_0x00c0
        L_0x006c:
            int r7 = com.baidu.location.p013a.C0765w.f1448n     // Catch:{ Exception -> 0x00c8 }
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x00c8 }
            int r8 = com.baidu.location.p013a.C0765w.f1445k     // Catch:{ Exception -> 0x00c8 }
        L_0x0072:
            if (r8 <= 0) goto L_0x00a5
            if (r12 <= 0) goto L_0x00a5
            int r9 = r1 + r12
            int r9 = r9 - r6
            int r9 = r9 % r13
            int r9 = r9 * r14
            long r9 = (long) r9     // Catch:{ Exception -> 0x00c8 }
            r11 = r7
            r6 = r15
            long r9 = r9 + r6
            r3.seek(r9)     // Catch:{ Exception -> 0x00c8 }
            int r9 = r3.readInt()     // Catch:{ Exception -> 0x00c8 }
            if (r9 <= 0) goto L_0x009d
            if (r9 >= r14) goto L_0x009d
            r10 = 0
            r3.read(r11, r10, r9)     // Catch:{ Exception -> 0x00c8 }
            int r9 = r9 + -1
            byte r15 = r11[r9]     // Catch:{ Exception -> 0x00c8 }
            if (r15 != 0) goto L_0x009d
            java.lang.String r15 = new java.lang.String     // Catch:{ Exception -> 0x00c8 }
            r15.<init>(r11, r10, r9)     // Catch:{ Exception -> 0x00c8 }
            r0.add(r15)     // Catch:{ Exception -> 0x00c8 }
        L_0x009d:
            int r8 = r8 + -1
            int r12 = r12 + -1
            r15 = r6
            r7 = r11
            r6 = 1
            goto L_0x0072
        L_0x00a5:
            r6 = r15
            r3.seek(r4)     // Catch:{ Exception -> 0x00c8 }
            r3.writeInt(r1)     // Catch:{ Exception -> 0x00c8 }
            r3.writeInt(r12)     // Catch:{ Exception -> 0x00c8 }
            r3.writeInt(r13)     // Catch:{ Exception -> 0x00c8 }
            r3.writeInt(r14)     // Catch:{ Exception -> 0x00c8 }
            r3.writeLong(r6)     // Catch:{ Exception -> 0x00c8 }
            r3.close()     // Catch:{ Exception -> 0x00c8 }
            int r0 = com.baidu.location.p013a.C0765w.f1445k     // Catch:{ Exception -> 0x00c8 }
            int r0 = r0 - r8
            monitor-exit(r2)
            return r0
        L_0x00c0:
            r3.close()     // Catch:{ Exception -> 0x00c8 }
            r0 = -4
            monitor-exit(r2)
            return r0
        L_0x00c6:
            r0 = move-exception
            goto L_0x00cf
        L_0x00c8:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00c6 }
            r0 = -5
            monitor-exit(r2)
            return r0
        L_0x00cf:
            monitor-exit(r2)
            throw r0
        L_0x00d1:
            monitor-exit(r2)
            r0 = -1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0765w.m1948a(java.util.List, int):int");
    }

    /* renamed from: a */
    public static synchronized C0765w m1949a() {
        C0765w wVar;
        synchronized (C0765w.class) {
            if (f1434A == null) {
                f1434A = new C0765w();
            }
            wVar = f1434A;
        }
        return wVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:20|21|(6:23|24|25|26|27|28)|30|31) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x003f */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m1950a(int r4) {
        /*
            r0 = 1
            r1 = 0
            if (r4 != r0) goto L_0x0009
            java.lang.String r4 = com.baidu.location.p013a.C0765w.f1440f
            java.util.ArrayList<java.lang.String> r2 = com.baidu.location.p013a.C0765w.f1436b
            goto L_0x001f
        L_0x0009:
            r2 = 2
            if (r4 != r2) goto L_0x0011
            java.lang.String r4 = com.baidu.location.p013a.C0765w.f1441g
            java.util.ArrayList<java.lang.String> r2 = com.baidu.location.p013a.C0765w.f1437c
            goto L_0x001f
        L_0x0011:
            r2 = 3
            if (r4 != r2) goto L_0x0019
            java.lang.String r4 = com.baidu.location.p013a.C0765w.f1442h
        L_0x0016:
            java.util.ArrayList<java.lang.String> r2 = com.baidu.location.p013a.C0765w.f1438d
            goto L_0x001f
        L_0x0019:
            r2 = 4
            if (r4 != r2) goto L_0x0044
            java.lang.String r4 = com.baidu.location.p013a.C0765w.f1443i
            goto L_0x0016
        L_0x001f:
            if (r2 != 0) goto L_0x0022
            return r1
        L_0x0022:
            int r3 = r2.size()
            if (r3 >= r0) goto L_0x002b
            m1958a(r4, r2)
        L_0x002b:
            java.lang.Class<com.baidu.location.a.w> r4 = com.baidu.location.p013a.C0765w.class
            monitor-enter(r4)
            int r3 = r2.size()     // Catch:{ all -> 0x0041 }
            if (r3 <= 0) goto L_0x003f
            int r3 = r3 - r0
            java.lang.Object r0 = r2.get(r3)     // Catch:{ Exception -> 0x003f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x003f }
            r2.remove(r3)     // Catch:{ Exception -> 0x003e }
        L_0x003e:
            r1 = r0
        L_0x003f:
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            return r1
        L_0x0041:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0041 }
            throw r0
        L_0x0044:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0765w.m1950a(int):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.a.w.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.location.a.w.a(java.util.List<java.lang.String>, int):int
      com.baidu.location.a.w.a(android.location.Location, com.baidu.location.e.h):boolean
      com.baidu.location.a.w.a(android.location.Location, boolean):boolean
      com.baidu.location.a.w.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.baidu.location.a.w.a(int, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001c, code lost:
        if (r15 != false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r15 != false) goto L_0x0009;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0060 A[Catch:{ Exception -> 0x00e5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00df A[ADDED_TO_REGION, Catch:{ Exception -> 0x00e5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00cc A[EDGE_INSN: B:42:0x00cc->B:36:0x00cc ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m1951a(int r14, boolean r15) {
        /*
            r0 = 4
            r1 = 1
            if (r14 != r1) goto L_0x000c
            java.lang.String r2 = com.baidu.location.p013a.C0765w.f1440f
            if (r15 == 0) goto L_0x0009
            return
        L_0x0009:
            java.util.ArrayList<java.lang.String> r3 = com.baidu.location.p013a.C0765w.f1436b
            goto L_0x0029
        L_0x000c:
            r2 = 2
            if (r14 != r2) goto L_0x0017
            java.lang.String r2 = com.baidu.location.p013a.C0765w.f1441g
            if (r15 == 0) goto L_0x0014
            goto L_0x0009
        L_0x0014:
            java.util.ArrayList<java.lang.String> r3 = com.baidu.location.p013a.C0765w.f1437c
            goto L_0x0029
        L_0x0017:
            r2 = 3
            if (r14 != r2) goto L_0x0022
            java.lang.String r2 = com.baidu.location.p013a.C0765w.f1442h
            if (r15 == 0) goto L_0x001f
            goto L_0x0014
        L_0x001f:
            java.util.ArrayList<java.lang.String> r3 = com.baidu.location.p013a.C0765w.f1438d
            goto L_0x0029
        L_0x0022:
            if (r14 != r0) goto L_0x00e5
            java.lang.String r2 = com.baidu.location.p013a.C0765w.f1443i
            if (r15 == 0) goto L_0x00e5
            goto L_0x001f
        L_0x0029:
            java.io.File r4 = new java.io.File
            r4.<init>(r2)
            boolean r5 = r4.exists()
            if (r5 != 0) goto L_0x0037
            m1963d(r2)
        L_0x0037:
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x00e5 }
            java.lang.String r5 = "rw"
            r2.<init>(r4, r5)     // Catch:{ Exception -> 0x00e5 }
            r4 = 4
            r2.seek(r4)     // Catch:{ Exception -> 0x00e5 }
            int r4 = r2.readInt()     // Catch:{ Exception -> 0x00e5 }
            int r5 = r2.readInt()     // Catch:{ Exception -> 0x00e5 }
            int r6 = r2.readInt()     // Catch:{ Exception -> 0x00e5 }
            int r7 = r2.readInt()     // Catch:{ Exception -> 0x00e5 }
            int r8 = r2.readInt()     // Catch:{ Exception -> 0x00e5 }
            int r9 = r3.size()     // Catch:{ Exception -> 0x00e5 }
        L_0x005b:
            int r10 = com.baidu.location.p013a.C0765w.f1446l     // Catch:{ Exception -> 0x00e5 }
            r11 = 0
            if (r9 <= r10) goto L_0x00cc
            if (r15 == 0) goto L_0x0064
            int r8 = r8 + 1
        L_0x0064:
            if (r6 >= r4) goto L_0x0095
            int r10 = r5 * r6
            int r10 = r10 + 128
            long r12 = (long) r10     // Catch:{ Exception -> 0x00e5 }
            r2.seek(r12)     // Catch:{ Exception -> 0x00e5 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5 }
            r10.<init>()     // Catch:{ Exception -> 0x00e5 }
            java.lang.Object r12 = r3.get(r11)     // Catch:{ Exception -> 0x00e5 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ Exception -> 0x00e5 }
            r10.append(r12)     // Catch:{ Exception -> 0x00e5 }
            r10.append(r11)     // Catch:{ Exception -> 0x00e5 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00e5 }
            byte[] r10 = r10.getBytes()     // Catch:{ Exception -> 0x00e5 }
            int r12 = r10.length     // Catch:{ Exception -> 0x00e5 }
            r2.writeInt(r12)     // Catch:{ Exception -> 0x00e5 }
            int r12 = r10.length     // Catch:{ Exception -> 0x00e5 }
            r2.write(r10, r11, r12)     // Catch:{ Exception -> 0x00e5 }
            r3.remove(r11)     // Catch:{ Exception -> 0x00e5 }
            int r6 = r6 + 1
            goto L_0x00c8
        L_0x0095:
            if (r15 == 0) goto L_0x00cb
            int r10 = r7 * r5
            int r10 = r10 + 128
            long r12 = (long) r10     // Catch:{ Exception -> 0x00e5 }
            r2.seek(r12)     // Catch:{ Exception -> 0x00e5 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5 }
            r10.<init>()     // Catch:{ Exception -> 0x00e5 }
            java.lang.Object r12 = r3.get(r11)     // Catch:{ Exception -> 0x00e5 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ Exception -> 0x00e5 }
            r10.append(r12)     // Catch:{ Exception -> 0x00e5 }
            r10.append(r11)     // Catch:{ Exception -> 0x00e5 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00e5 }
            byte[] r10 = r10.getBytes()     // Catch:{ Exception -> 0x00e5 }
            int r12 = r10.length     // Catch:{ Exception -> 0x00e5 }
            r2.writeInt(r12)     // Catch:{ Exception -> 0x00e5 }
            int r12 = r10.length     // Catch:{ Exception -> 0x00e5 }
            r2.write(r10, r11, r12)     // Catch:{ Exception -> 0x00e5 }
            r3.remove(r11)     // Catch:{ Exception -> 0x00e5 }
            int r7 = r7 + 1
            if (r7 <= r6) goto L_0x00c8
            r7 = 0
        L_0x00c8:
            int r9 = r9 + -1
            goto L_0x005b
        L_0x00cb:
            r11 = 1
        L_0x00cc:
            r3 = 12
            r2.seek(r3)     // Catch:{ Exception -> 0x00e5 }
            r2.writeInt(r6)     // Catch:{ Exception -> 0x00e5 }
            r2.writeInt(r7)     // Catch:{ Exception -> 0x00e5 }
            r2.writeInt(r8)     // Catch:{ Exception -> 0x00e5 }
            r2.close()     // Catch:{ Exception -> 0x00e5 }
            if (r11 == 0) goto L_0x00e5
            if (r14 >= r0) goto L_0x00e5
            int r14 = r14 + r1
            m1951a(r14, r1)     // Catch:{ Exception -> 0x00e5 }
        L_0x00e5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0765w.m1951a(int, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.a.w.a(android.location.Location, boolean):boolean
     arg types: [android.location.Location, int]
     candidates:
      com.baidu.location.a.w.a(java.util.List<java.lang.String>, int):int
      com.baidu.location.a.w.a(int, boolean):void
      com.baidu.location.a.w.a(android.location.Location, com.baidu.location.e.h):boolean
      com.baidu.location.a.w.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.baidu.location.a.w.a(android.location.Location, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:102:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:108:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01d8 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01eb  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m1952a(com.baidu.location.p017e.C0821a r10, com.baidu.location.p017e.C0834h r11, android.location.Location r12, java.lang.String r13) {
        /*
            com.baidu.location.b.e r0 = com.baidu.location.p014b.C0777e.m2013a()
            boolean r0 = r0.f1515a
            if (r0 != 0) goto L_0x0009
            return
        L_0x0009:
            int r0 = com.baidu.location.p019g.C0855k.f1978v
            r1 = 3
            r2 = 0
            if (r0 != r1) goto L_0x001c
            boolean r0 = m1956a(r12, r11)
            if (r0 != 0) goto L_0x001c
            boolean r0 = m1957a(r12, r2)
            if (r0 != 0) goto L_0x001c
            return
        L_0x001c:
            if (r10 == 0) goto L_0x01ed
            boolean r0 = r10.mo10618c()
            if (r0 == 0) goto L_0x0026
            goto L_0x01ed
        L_0x0026:
            android.content.Context r0 = com.baidu.location.C0839f.getServiceContext()
            boolean r0 = com.baidu.location.p019g.C0855k.m2457a(r0)
            com.baidu.location.d.h r3 = com.baidu.location.p016d.C0804h.m2193a()
            r6 = 0
            if (r0 == 0) goto L_0x0038
            com.baidu.location.d.h$b r7 = com.baidu.location.p016d.C0804h.C0806b.IS_MIX_MODE
            goto L_0x003a
        L_0x0038:
            com.baidu.location.d.h$b r7 = com.baidu.location.p016d.C0804h.C0806b.IS_NOT_MIX_MODE
        L_0x003a:
            com.baidu.location.d.h$a r8 = com.baidu.location.p016d.C0804h.C0805a.NO_NEED_TO_LOG
            r4 = r10
            r5 = r11
            com.baidu.location.BDLocation r0 = r3.mo10590a(r4, r5, r6, r7, r8)
            java.lang.String r3 = "1"
            r4 = 2
            r5 = 0
            r6 = 1
            if (r0 == 0) goto L_0x00bf
            int r7 = r0.getLocType()
            r8 = 67
            if (r7 != r8) goto L_0x0053
            goto L_0x00bf
        L_0x0053:
            if (r0 == 0) goto L_0x0060
            java.lang.String r7 = r0.getNetworkLocationType()
            if (r7 == 0) goto L_0x0060
            java.lang.String r7 = r0.getNetworkLocationType()
            goto L_0x0061
        L_0x0060:
            r7 = r5
        L_0x0061:
            if (r7 == 0) goto L_0x006d
            java.lang.String r8 = "cl"
            boolean r8 = r7.equals(r8)
            if (r8 == 0) goto L_0x006d
            r7 = 1
            goto L_0x007a
        L_0x006d:
            if (r7 == 0) goto L_0x0079
            java.lang.String r8 = "wf"
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x0079
            r7 = 2
            goto L_0x007a
        L_0x0079:
            r7 = 0
        L_0x007a:
            if (r0 == 0) goto L_0x00da
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r13)
            java.util.Locale r13 = java.util.Locale.CHINA
            r9 = 5
            java.lang.Object[] r9 = new java.lang.Object[r9]
            r9[r2] = r3
            java.lang.Integer r2 = java.lang.Integer.valueOf(r7)
            r9[r6] = r2
            double r2 = r0.getLongitude()
            java.lang.Double r2 = java.lang.Double.valueOf(r2)
            r9[r4] = r2
            double r2 = r0.getLatitude()
            java.lang.Double r2 = java.lang.Double.valueOf(r2)
            r9[r1] = r2
            r2 = 4
            float r0 = r0.getRadius()
            int r0 = (int) r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r9[r2] = r0
            java.lang.String r0 = "&ofl=%s|%d|%f|%f|%d"
            java.lang.String r13 = java.lang.String.format(r13, r0, r9)
            r8.append(r13)
            java.lang.String r13 = r8.toString()
            goto L_0x00da
        L_0x00bf:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r13)
            java.util.Locale r13 = java.util.Locale.CHINA
            java.lang.Object[] r7 = new java.lang.Object[r6]
            r7[r2] = r3
            java.lang.String r2 = "&ofl=%s|0"
            java.lang.String r13 = java.lang.String.format(r13, r2, r7)
            r0.append(r13)
            java.lang.String r13 = r0.toString()
        L_0x00da:
            if (r10 == 0) goto L_0x00ff
            boolean r0 = r10.mo10615a()
            if (r0 == 0) goto L_0x00ff
            boolean r0 = m1956a(r12, r11)
            if (r0 != 0) goto L_0x00e9
            r11 = r5
        L_0x00e9:
            java.lang.String r10 = com.baidu.location.p019g.C0855k.m2453a(r10, r11, r12, r13, r6)
            if (r10 == 0) goto L_0x00fe
            java.lang.String r10 = com.baidu.location.Jni.encode(r10)
            m1953a(r10)
            com.baidu.location.p013a.C0765w.f1457w = r12
            com.baidu.location.p013a.C0765w.f1456v = r12
            if (r11 == 0) goto L_0x00fe
            com.baidu.location.p013a.C0765w.f1459y = r11
        L_0x00fe:
            return
        L_0x00ff:
            java.lang.String r0 = "&cfr=3"
            java.lang.String r2 = "&cfr=2"
            java.lang.String r3 = "&cfr=1"
            if (r11 == 0) goto L_0x017c
            boolean r6 = r11.mo10685m()
            if (r6 == 0) goto L_0x017c
            boolean r6 = m1956a(r12, r11)
            if (r6 == 0) goto L_0x017c
            boolean r1 = m1955a(r12)
            if (r1 != 0) goto L_0x0133
            com.baidu.location.e.b r1 = com.baidu.location.p017e.C0822b.m2279a()
            boolean r1 = r1.mo10629d()
            if (r1 != 0) goto L_0x0133
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
        L_0x012b:
            r0.append(r13)
            java.lang.String r13 = r0.toString()
            goto L_0x0166
        L_0x0133:
            boolean r1 = m1955a(r12)
            if (r1 != 0) goto L_0x0153
            com.baidu.location.e.b r1 = com.baidu.location.p017e.C0822b.m2279a()
            boolean r1 = r1.mo10629d()
            if (r1 == 0) goto L_0x0153
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r13)
            java.lang.String r13 = r1.toString()
            goto L_0x0166
        L_0x0153:
            com.baidu.location.e.b r0 = com.baidu.location.p017e.C0822b.m2279a()
            boolean r0 = r0.mo10629d()
            if (r0 == 0) goto L_0x0166
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r2)
            goto L_0x012b
        L_0x0166:
            java.lang.String r10 = com.baidu.location.p019g.C0855k.m2453a(r10, r11, r12, r13, r4)
            if (r10 == 0) goto L_0x017b
            java.lang.String r10 = com.baidu.location.Jni.encode(r10)
            m1960b(r10)
            com.baidu.location.p013a.C0765w.f1458x = r12
            com.baidu.location.p013a.C0765w.f1456v = r12
            if (r11 == 0) goto L_0x017b
            com.baidu.location.p013a.C0765w.f1459y = r11
        L_0x017b:
            return
        L_0x017c:
            boolean r4 = m1955a(r12)
            if (r4 != 0) goto L_0x019c
            com.baidu.location.e.b r4 = com.baidu.location.p017e.C0822b.m2279a()
            boolean r4 = r4.mo10629d()
            if (r4 != 0) goto L_0x019c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
        L_0x0194:
            r0.append(r13)
            java.lang.String r13 = r0.toString()
            goto L_0x01cf
        L_0x019c:
            boolean r3 = m1955a(r12)
            if (r3 != 0) goto L_0x01bc
            com.baidu.location.e.b r3 = com.baidu.location.p017e.C0822b.m2279a()
            boolean r3 = r3.mo10629d()
            if (r3 == 0) goto L_0x01bc
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r0)
            r2.append(r13)
            java.lang.String r13 = r2.toString()
            goto L_0x01cf
        L_0x01bc:
            com.baidu.location.e.b r0 = com.baidu.location.p017e.C0822b.m2279a()
            boolean r0 = r0.mo10629d()
            if (r0 == 0) goto L_0x01cf
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r2)
            goto L_0x0194
        L_0x01cf:
            boolean r0 = m1956a(r12, r11)
            if (r0 != 0) goto L_0x01d6
            r11 = r5
        L_0x01d6:
            if (r10 != 0) goto L_0x01da
            if (r11 == 0) goto L_0x01ed
        L_0x01da:
            java.lang.String r10 = com.baidu.location.p019g.C0855k.m2453a(r10, r11, r12, r13, r1)
            if (r10 == 0) goto L_0x01ed
            java.lang.String r10 = com.baidu.location.Jni.encode(r10)
            m1961c(r10)
            com.baidu.location.p013a.C0765w.f1456v = r12
            if (r11 == 0) goto L_0x01ed
            com.baidu.location.p013a.C0765w.f1459y = r11
        L_0x01ed:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0765w.m1952a(com.baidu.location.e.a, com.baidu.location.e.h, android.location.Location, java.lang.String):void");
    }

    /* renamed from: a */
    private static void m1953a(String str) {
        m1965e(str);
    }

    /* renamed from: a */
    private static boolean m1954a(int i, int i2, int i3, int i4, long j) {
        return i >= 0 && i < i3 && i2 >= 0 && i2 <= i3 && i3 >= 0 && i3 <= 1024 && i4 >= 128 && i4 <= 1024;
    }

    /* renamed from: a */
    private static boolean m1955a(Location location) {
        if (location == null) {
            return false;
        }
        Location location2 = f1457w;
        if (location2 == null || f1456v == null) {
            f1457w = location;
            return true;
        }
        double distanceTo = (double) location.distanceTo(location2);
        double d = (double) C0855k.f1919T;
        Double.isNaN(d);
        Double.isNaN(distanceTo);
        Double.isNaN(distanceTo);
        double d2 = (double) C0855k.f1920U;
        Double.isNaN(d2);
        Double.isNaN(distanceTo);
        double d3 = (double) C0855k.f1921V;
        Double.isNaN(d3);
        return ((double) location.distanceTo(f1456v)) > (((d * distanceTo) * distanceTo) + (d2 * distanceTo)) + d3;
    }

    /* renamed from: a */
    private static boolean m1956a(Location location, C0834h hVar) {
        boolean z = false;
        if (!(location == null || hVar == null || hVar.f1789a == null || hVar.f1789a.isEmpty())) {
            if (hVar.mo10671b(f1459y)) {
                return false;
            }
            z = true;
            if (f1458x == null) {
                f1458x = location;
            }
        }
        return z;
    }

    /* renamed from: a */
    public static boolean m1957a(Location location, boolean z) {
        return C0826e.m2312a(f1456v, location, z);
    }

    /* renamed from: a */
    private static boolean m1958a(String str, List<String> list) {
        File file = new File(str);
        if (!file.exists()) {
            return false;
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(8);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            int readInt3 = randomAccessFile.readInt();
            byte[] bArr = new byte[f1448n];
            int i = f1446l + 1;
            boolean z = false;
            while (i > 0 && readInt2 > 0) {
                if (readInt2 < readInt3) {
                    readInt3 = 0;
                }
                try {
                    randomAccessFile.seek((long) (((readInt2 - 1) * readInt) + 128));
                    int readInt4 = randomAccessFile.readInt();
                    if (readInt4 > 0 && readInt4 < readInt) {
                        randomAccessFile.read(bArr, 0, readInt4);
                        int i2 = readInt4 - 1;
                        if (bArr[i2] == 0) {
                            list.add(0, new String(bArr, 0, i2));
                            z = true;
                        }
                    }
                    i--;
                    readInt2--;
                } catch (Exception unused) {
                    return z;
                }
            }
            randomAccessFile.seek(12);
            randomAccessFile.writeInt(readInt2);
            randomAccessFile.writeInt(readInt3);
            randomAccessFile.close();
            return z;
        } catch (Exception unused2) {
            return false;
        }
    }

    /* renamed from: b */
    public static String m1959b() {
        return m1966f();
    }

    /* renamed from: b */
    private static void m1960b(String str) {
        m1965e(str);
    }

    /* renamed from: c */
    private static void m1961c(String str) {
        m1965e(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.a.w.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.location.a.w.a(java.util.List<java.lang.String>, int):int
      com.baidu.location.a.w.a(android.location.Location, com.baidu.location.e.h):boolean
      com.baidu.location.a.w.a(android.location.Location, boolean):boolean
      com.baidu.location.a.w.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.baidu.location.a.w.a(int, boolean):void */
    /* renamed from: d */
    public static void m1962d() {
        f1446l = 0;
        m1951a(1, false);
        m1951a(2, false);
        m1951a(3, false);
        f1446l = 8;
    }

    /* renamed from: d */
    private static void m1963d(String str) {
        try {
            File file = new File(str);
            if (!file.exists()) {
                File file2 = new File(C0854j.f1898a);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (!file.createNewFile()) {
                    file = null;
                }
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(0);
                randomAccessFile.writeInt(32);
                randomAccessFile.writeInt(2048);
                randomAccessFile.writeInt(1040);
                randomAccessFile.writeInt(0);
                randomAccessFile.writeInt(0);
                randomAccessFile.writeInt(0);
                randomAccessFile.close();
            }
        } catch (Exception unused) {
        }
    }

    /* renamed from: e */
    public static String m1964e() {
        File file = new File(f1441g);
        String str = null;
        if (file.exists()) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(20);
                int readInt = randomAccessFile.readInt();
                if (readInt > 128) {
                    String str2 = "&p1=" + readInt;
                    try {
                        randomAccessFile.seek(20);
                        randomAccessFile.writeInt(0);
                        randomAccessFile.close();
                        return str2;
                    } catch (Exception unused) {
                        str = str2;
                    }
                } else {
                    randomAccessFile.close();
                }
            } catch (Exception unused2) {
            }
        }
        File file2 = new File(f1442h);
        if (file2.exists()) {
            try {
                RandomAccessFile randomAccessFile2 = new RandomAccessFile(file2, "rw");
                randomAccessFile2.seek(20);
                int readInt2 = randomAccessFile2.readInt();
                if (readInt2 > 256) {
                    String str3 = "&p2=" + readInt2;
                    try {
                        randomAccessFile2.seek(20);
                        randomAccessFile2.writeInt(0);
                        randomAccessFile2.close();
                        return str3;
                    } catch (Exception unused3) {
                        str = str3;
                    }
                } else {
                    randomAccessFile2.close();
                }
            } catch (Exception unused4) {
            }
        }
        File file3 = new File(f1443i);
        if (!file3.exists()) {
            return str;
        }
        try {
            RandomAccessFile randomAccessFile3 = new RandomAccessFile(file3, "rw");
            randomAccessFile3.seek(20);
            int readInt3 = randomAccessFile3.readInt();
            if (readInt3 > 512) {
                String str4 = "&p3=" + readInt3;
                try {
                    randomAccessFile3.seek(20);
                    randomAccessFile3.writeInt(0);
                    randomAccessFile3.close();
                    return str4;
                } catch (Exception unused5) {
                    return str4;
                }
            } else {
                randomAccessFile3.close();
                return str;
            }
        } catch (Exception unused6) {
            return str;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.a.w.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.location.a.w.a(java.util.List<java.lang.String>, int):int
      com.baidu.location.a.w.a(android.location.Location, com.baidu.location.e.h):boolean
      com.baidu.location.a.w.a(android.location.Location, boolean):boolean
      com.baidu.location.a.w.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.baidu.location.a.w.a(int, boolean):void */
    /* renamed from: e */
    private static synchronized void m1965e(String str) {
        ArrayList<String> arrayList;
        synchronized (C0765w.class) {
            if (!str.contains("err!")) {
                int i = C0855k.f1973q;
                if (i == 1) {
                    arrayList = f1436b;
                } else if (i == 2) {
                    arrayList = f1437c;
                } else if (i == 3) {
                    arrayList = f1438d;
                } else {
                    return;
                }
                if (arrayList != null) {
                    if (arrayList.size() <= f1447m) {
                        arrayList.add(str);
                    }
                    if (arrayList.size() >= f1447m) {
                        m1951a(i, false);
                    }
                    while (arrayList.size() > f1447m) {
                        arrayList.remove(0);
                    }
                }
            }
        }
    }

    /* renamed from: f */
    private static String m1966f() {
        String str = null;
        for (int i = 1; i < 5; i++) {
            str = m1950a(i);
            if (str != null) {
                return str;
            }
        }
        m1948a(f1438d, f1454t);
        if (f1438d.size() > 0) {
            str = f1438d.get(0);
            f1438d.remove(0);
        }
        if (str != null) {
            return str;
        }
        m1948a(f1438d, f1453s);
        if (f1438d.size() > 0) {
            str = f1438d.get(0);
            f1438d.remove(0);
        }
        if (str != null) {
            return str;
        }
        m1948a(f1438d, f1455u);
        if (f1438d.size() <= 0) {
            return str;
        }
        String str2 = f1438d.get(0);
        f1438d.remove(0);
        return str2;
    }

    /* renamed from: c */
    public void mo10507c() {
        if (C0835i.m2384j() && !C0855k.m2460b()) {
            this.f1462z.mo10508b();
        }
    }
}
