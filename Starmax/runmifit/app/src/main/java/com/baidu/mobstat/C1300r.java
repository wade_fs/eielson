package com.baidu.mobstat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.baidu.mobstat.r */
public enum C1300r {
    AP_LIST(0) {
        /* renamed from: a */
        public C1299q mo13991a() {
            return new C1308u();
        }
    },
    APP_LIST(1) {
        /* renamed from: a */
        public C1299q mo13991a() {
            return new C1311x();
        }
    },
    APP_TRACE(2) {
        /* renamed from: a */
        public C1299q mo13991a() {
            return new C1312y();
        }
    },
    APP_CHANGE(3) {
        /* renamed from: a */
        public C1299q mo13991a() {
            return new C1310w();
        }
    },
    APP_APK(4) {
        /* renamed from: a */
        public C1299q mo13991a() {
            return new C1309v();
        }
    };
    

    /* renamed from: f */
    private int f4392f;

    /* renamed from: a */
    public abstract C1299q mo13991a();

    private C1300r(int i) {
        this.f4392f = i;
    }

    public String toString() {
        return String.valueOf(this.f4392f);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001c, code lost:
        if (r1 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002c, code lost:
        if (r1 == null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0030, code lost:
        return r0;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.baidu.mobstat.C1298p> mo13992a(int r4, int r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0037 }
            r0.<init>()     // Catch:{ all -> 0x0037 }
            r1 = 0
            com.baidu.mobstat.q r1 = r3.mo13991a()     // Catch:{ Exception -> 0x0024 }
            boolean r2 = r1.mo13984a()     // Catch:{ Exception -> 0x0024 }
            if (r2 != 0) goto L_0x0018
            if (r1 == 0) goto L_0x0016
            r1.close()     // Catch:{ all -> 0x0037 }
        L_0x0016:
            monitor-exit(r3)
            return r0
        L_0x0018:
            java.util.ArrayList r0 = r1.mo13983a(r4, r5)     // Catch:{ Exception -> 0x0024 }
            if (r1 == 0) goto L_0x002f
        L_0x001e:
            r1.close()     // Catch:{ all -> 0x0037 }
            goto L_0x002f
        L_0x0022:
            r4 = move-exception
            goto L_0x0031
        L_0x0024:
            r4 = move-exception
            com.baidu.mobstat.as r5 = com.baidu.mobstat.C1255as.m4626c()     // Catch:{ all -> 0x0022 }
            r5.mo13944b(r4)     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x002f
            goto L_0x001e
        L_0x002f:
            monitor-exit(r3)
            return r0
        L_0x0031:
            if (r1 == 0) goto L_0x0036
            r1.close()     // Catch:{ all -> 0x0037 }
        L_0x0036:
            throw r4     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r4 = move-exception
            monitor-exit(r3)
            goto L_0x003b
        L_0x003a:
            throw r4
        L_0x003b:
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1300r.mo13992a(int, int):java.util.ArrayList");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        if (r2 != null) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002d, code lost:
        if (r2 == null) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0031, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        return -1;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long mo13990a(long r5, java.lang.String r7) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = -1
            r2 = 0
            com.baidu.mobstat.q r2 = r4.mo13991a()     // Catch:{ Exception -> 0x0025 }
            boolean r3 = r2.mo13984a()     // Catch:{ Exception -> 0x0025 }
            if (r3 != 0) goto L_0x0015
            if (r2 == 0) goto L_0x0013
            r2.close()     // Catch:{ all -> 0x0038 }
        L_0x0013:
            monitor-exit(r4)
            return r0
        L_0x0015:
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0025 }
            long r0 = r2.mo13980a(r5, r7)     // Catch:{ Exception -> 0x0025 }
            if (r2 == 0) goto L_0x0030
        L_0x001f:
            r2.close()     // Catch:{ all -> 0x0038 }
            goto L_0x0030
        L_0x0023:
            r5 = move-exception
            goto L_0x0032
        L_0x0025:
            r5 = move-exception
            com.baidu.mobstat.as r6 = com.baidu.mobstat.C1255as.m4626c()     // Catch:{ all -> 0x0023 }
            r6.mo13944b(r5)     // Catch:{ all -> 0x0023 }
            if (r2 == 0) goto L_0x0030
            goto L_0x001f
        L_0x0030:
            monitor-exit(r4)
            return r0
        L_0x0032:
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ all -> 0x0038 }
        L_0x0037:
            throw r5     // Catch:{ all -> 0x0038 }
        L_0x0038:
            r5 = move-exception
            monitor-exit(r4)
            goto L_0x003c
        L_0x003b:
            throw r5
        L_0x003c:
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1300r.mo13990a(long, java.lang.String):long");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001c, code lost:
        return 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003a, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0042, code lost:
        if (r1 != null) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0053, code lost:
        if (r1 != null) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0057, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0062, code lost:
        return 0;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int mo13989a(java.util.ArrayList<java.lang.Long> r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            if (r7 == 0) goto L_0x0061
            int r1 = r7.size()     // Catch:{ all -> 0x005e }
            if (r1 != 0) goto L_0x000b
            goto L_0x0061
        L_0x000b:
            r1 = 0
            com.baidu.mobstat.q r1 = r6.mo13991a()     // Catch:{ Exception -> 0x004a }
            boolean r2 = r1.mo13984a()     // Catch:{ Exception -> 0x004a }
            if (r2 != 0) goto L_0x001d
            if (r1 == 0) goto L_0x001b
            r1.close()     // Catch:{ all -> 0x005e }
        L_0x001b:
            monitor-exit(r6)
            return r0
        L_0x001d:
            int r2 = r7.size()     // Catch:{ Exception -> 0x004a }
            r3 = 0
        L_0x0022:
            if (r0 >= r2) goto L_0x0042
            java.lang.Object r4 = r7.get(r0)     // Catch:{ Exception -> 0x0040 }
            java.lang.Long r4 = (java.lang.Long) r4     // Catch:{ Exception -> 0x0040 }
            long r4 = r4.longValue()     // Catch:{ Exception -> 0x0040 }
            boolean r4 = r1.mo13987b(r4)     // Catch:{ Exception -> 0x0040 }
            if (r4 != 0) goto L_0x003b
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ all -> 0x005e }
        L_0x0039:
            monitor-exit(r6)
            return r3
        L_0x003b:
            int r3 = r3 + 1
            int r0 = r0 + 1
            goto L_0x0022
        L_0x0040:
            r7 = move-exception
            goto L_0x004c
        L_0x0042:
            if (r1 == 0) goto L_0x0056
        L_0x0044:
            r1.close()     // Catch:{ all -> 0x005e }
            goto L_0x0056
        L_0x0048:
            r7 = move-exception
            goto L_0x0058
        L_0x004a:
            r7 = move-exception
            r3 = 0
        L_0x004c:
            com.baidu.mobstat.as r0 = com.baidu.mobstat.C1255as.m4626c()     // Catch:{ all -> 0x0048 }
            r0.mo13944b(r7)     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x0056
            goto L_0x0044
        L_0x0056:
            monitor-exit(r6)
            return r3
        L_0x0058:
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ all -> 0x005e }
        L_0x005d:
            throw r7     // Catch:{ all -> 0x005e }
        L_0x005e:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        L_0x0061:
            monitor-exit(r6)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1300r.mo13989a(java.util.ArrayList):int");
    }

    /* renamed from: a */
    public synchronized List<String> mo13993a(int i) {
        List<String> arrayList;
        arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        m4842a(arrayList, arrayList2, arrayList3, i, 500);
        if (arrayList3.size() != 0 && arrayList.size() == 0 && arrayList2.size() == 0) {
            C1298p pVar = (C1298p) arrayList3.get(0);
            long a = pVar.mo13977a();
            String b = pVar.mo13978b();
            arrayList2.add(Long.valueOf(a));
            arrayList.add(b);
        }
        int a2 = mo13989a(arrayList2);
        if (a2 != arrayList.size()) {
            arrayList = arrayList.subList(0, a2);
        }
        return arrayList;
    }

    /* renamed from: a */
    private int m4842a(List<String> list, ArrayList<Long> arrayList, ArrayList<C1298p> arrayList2, int i, int i2) {
        int c = m4843c();
        int i3 = i2;
        int i4 = 0;
        int i5 = 0;
        while (c > 0) {
            if (c < i3) {
                i3 = c;
            }
            ArrayList<C1298p> a = mo13992a(i3, i5);
            if (i5 == 0 && a.size() != 0) {
                arrayList2.add(a.get(0));
            }
            Iterator<C1298p> it = a.iterator();
            while (it.hasNext()) {
                C1298p next = it.next();
                long a2 = next.mo13977a();
                String b = next.mo13978b();
                int length = b.length() + i4;
                if (length > i) {
                    break;
                }
                arrayList.add(Long.valueOf(a2));
                list.add(b);
                i4 = length;
            }
            c -= i3;
            i5 += i3;
        }
        return i4;
    }

    /* renamed from: b */
    public synchronized boolean mo13994b() {
        return m4843c() == 0;
    }

    /* renamed from: b */
    public synchronized boolean mo13995b(int i) {
        return m4843c() >= i;
    }

    /* renamed from: c */
    private int m4843c() {
        C1299q qVar = null;
        try {
            qVar = mo13991a();
            if (qVar.mo13984a()) {
                int b = qVar.mo13986b();
                if (qVar != null) {
                    qVar.close();
                }
                return b;
            }
            if (qVar == null) {
                return 0;
            }
            qVar.close();
            return 0;
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            if (qVar == null) {
                return 0;
            }
        } catch (Throwable th) {
            if (qVar != null) {
                qVar.close();
            }
            throw th;
        }
    }
}
