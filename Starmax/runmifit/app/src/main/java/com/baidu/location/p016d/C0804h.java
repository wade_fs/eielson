package com.baidu.location.p016d;

import android.content.Context;
import android.content.pm.ProviderInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import com.baidu.location.BDLocation;
import com.baidu.location.C0839f;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p017e.C0821a;
import com.baidu.location.p017e.C0834h;
import com.baidu.location.p019g.C0843a;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0855k;
import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: com.baidu.location.d.h */
public final class C0804h {

    /* renamed from: a */
    public static final String f1654a = C0843a.f1826a;

    /* renamed from: b */
    static final String f1655b = "http://ofloc.map.baidu.com/offline_loc";
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static Context f1656c;

    /* renamed from: d */
    private static volatile C0804h f1657d;

    /* renamed from: e */
    private static Object f1658e = new Object();

    /* renamed from: f */
    private final File f1659f;

    /* renamed from: g */
    private final C0811k f1660g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final C0794c f1661h;

    /* renamed from: i */
    private final C0813l f1662i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public final C0800f f1663j;

    /* renamed from: com.baidu.location.d.h$a */
    public enum C0805a {
        NEED_TO_LOG,
        NO_NEED_TO_LOG
    }

    /* renamed from: com.baidu.location.d.h$b */
    public enum C0806b {
        IS_MIX_MODE,
        IS_NOT_MIX_MODE
    }

    /* renamed from: com.baidu.location.d.h$c */
    private enum C0807c {
        NETWORK_UNKNOWN,
        NETWORK_WIFI,
        NETWORK_2G,
        NETWORK_3G,
        NETWORK_4G
    }

    private C0804h() {
        File file;
        try {
            file = new File(f1656c.getFilesDir(), "ofld");
            try {
                if (!file.exists()) {
                    file.mkdir();
                }
            } catch (Exception unused) {
            }
        } catch (Exception unused2) {
            file = null;
        }
        this.f1659f = file;
        this.f1661h = new C0794c(this);
        this.f1660g = new C0811k(this.f1661h.mo10565a());
        this.f1663j = new C0800f(this, this.f1661h.mo10565a());
        this.f1662i = new C0813l(this, this.f1661h.mo10565a(), this.f1663j.mo10584n());
    }

    /* renamed from: a */
    private BDLocation m2191a(String[] strArr) {
        new BDLocation();
        FutureTask futureTask = (FutureTask) C0762v.m1943a().mo10505c().submit(new C0808i(this, strArr));
        try {
            return (BDLocation) futureTask.get(2000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException unused) {
            futureTask.cancel(true);
            return null;
        }
    }

    /* renamed from: a */
    public static C0804h m2193a() {
        C0804h hVar;
        synchronized (f1658e) {
            if (f1657d == null) {
                if (f1656c == null) {
                    m2194a(C0839f.getServiceContext());
                }
                f1657d = new C0804h();
            }
            f1657d.m2199q();
            hVar = f1657d;
        }
        return hVar;
    }

    /* renamed from: a */
    public static void m2194a(Context context) {
        if (f1656c == null) {
            f1656c = context;
            C0844b.m2417a().mo10715a(f1656c);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static final Uri m2197c(String str) {
        return Uri.parse(String.format("content://%s/", str));
    }

    /* renamed from: q */
    private void m2199q() {
        this.f1663j.mo10577g();
    }

    /* renamed from: r */
    private boolean m2200r() {
        String packageName = f1656c.getPackageName();
        String[] o = this.f1663j.mo10585o();
        ProviderInfo providerInfo = null;
        for (int i = 0; i < o.length; i++) {
            try {
                providerInfo = f1656c.getPackageManager().resolveContentProvider(o[i], 0);
            } catch (Exception unused) {
                providerInfo = null;
            }
            if (providerInfo != null) {
                break;
            }
        }
        return providerInfo == null || packageName.equals(providerInfo.packageName);
    }

    /* renamed from: a */
    public long mo10589a(String str) {
        return this.f1663j.mo10570a(str);
    }

    /* renamed from: a */
    public BDLocation mo10590a(C0821a aVar, C0834h hVar, BDLocation bDLocation, C0806b bVar, C0805a aVar2) {
        int i;
        String str;
        if (bVar == C0806b.IS_MIX_MODE) {
            i = this.f1663j.mo10569a();
            str = C0844b.m2417a().mo10719d() + "&mixMode=1";
        } else {
            str = C0844b.m2417a().mo10719d();
            i = 0;
        }
        String[] a = C0809j.m2222a(aVar, hVar, bDLocation, str, (aVar2 == C0805a.NEED_TO_LOG).booleanValue(), i);
        BDLocation bDLocation2 = null;
        if (a.length > 0 && (bDLocation2 = m2191a(a)) != null) {
            bDLocation2.getLocType();
        }
        return bDLocation2;
    }

    /* renamed from: b */
    public Context mo10591b() {
        return f1656c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public File mo10592c() {
        return this.f1659f;
    }

    /* renamed from: d */
    public boolean mo10593d() {
        return this.f1663j.mo10578h();
    }

    /* renamed from: e */
    public boolean mo10594e() {
        return this.f1663j.mo10579i();
    }

    /* renamed from: f */
    public boolean mo10595f() {
        return this.f1663j.mo10580j();
    }

    /* renamed from: g */
    public boolean mo10596g() {
        return this.f1663j.mo10581k();
    }

    /* renamed from: h */
    public boolean mo10597h() {
        return this.f1663j.mo10583m();
    }

    /* renamed from: i */
    public void mo10598i() {
        if (!C0855k.m2460b()) {
            this.f1660g.mo10607a();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j */
    public C0811k mo10599j() {
        return this.f1660g;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k */
    public C0813l mo10600k() {
        return this.f1662i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public C0800f mo10601l() {
        return this.f1663j;
    }

    /* renamed from: m */
    public void mo10602m() {
        if (m2200r() && !C0855k.m2460b()) {
            this.f1661h.mo10566b();
        }
    }

    /* renamed from: n */
    public void mo10603n() {
    }

    /* renamed from: o */
    public double mo10604o() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) f1656c.getSystemService("connectivity")).getActiveNetworkInfo();
        C0807c cVar = C0807c.NETWORK_UNKNOWN;
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            if (activeNetworkInfo.getType() == 1) {
                cVar = C0807c.NETWORK_WIFI;
            }
            if (activeNetworkInfo.getType() == 0) {
                int subtype = activeNetworkInfo.getSubtype();
                if (subtype == 1 || subtype == 2 || subtype == 4 || subtype == 7 || subtype == 11) {
                    cVar = C0807c.NETWORK_2G;
                } else if (subtype == 3 || subtype == 5 || subtype == 6 || subtype == 8 || subtype == 9 || subtype == 10 || subtype == 12 || subtype == 14 || subtype == 15) {
                    cVar = C0807c.NETWORK_3G;
                } else if (subtype == 13) {
                    cVar = C0807c.NETWORK_4G;
                }
            }
        }
        if (cVar == C0807c.NETWORK_UNKNOWN) {
            return this.f1663j.mo10572b();
        }
        if (cVar == C0807c.NETWORK_WIFI) {
            return this.f1663j.mo10573c();
        }
        if (cVar == C0807c.NETWORK_2G) {
            return this.f1663j.mo10574d();
        }
        if (cVar == C0807c.NETWORK_3G) {
            return this.f1663j.mo10575e();
        }
        if (cVar == C0807c.NETWORK_4G) {
            return this.f1663j.mo10576f();
        }
        return 0.0d;
    }
}
