package com.baidu.mobstat;

import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;

/* renamed from: com.baidu.mobstat.ar */
public abstract class C1254ar {

    /* renamed from: a */
    public static int f4324a = 2;

    /* renamed from: a */
    public abstract String mo13940a();

    /* renamed from: b */
    public abstract boolean mo13945b();

    /* renamed from: a */
    public void mo13941a(String str) {
        m4616a(3, str);
    }

    /* renamed from: a */
    public void mo13942a(Throwable th) {
        m4616a(3, m4617d(th));
    }

    /* renamed from: b */
    public void mo13943b(String str) {
        m4616a(5, str);
    }

    /* renamed from: b */
    public void mo13944b(Throwable th) {
        m4616a(5, m4617d(th));
    }

    /* renamed from: c */
    public void mo13946c(String str) {
        m4616a(6, str);
    }

    /* renamed from: c */
    public void mo13947c(Throwable th) {
        m4616a(6, m4617d(th));
    }

    /* renamed from: d */
    private String m4617d(Throwable th) {
        if (th == null) {
            return "";
        }
        for (Throwable th2 = th; th2 != null; th2 = th2.getCause()) {
            if (th2 instanceof UnknownHostException) {
                return "";
            }
        }
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    /* renamed from: a */
    private void m4616a(int i, String str) {
        if (mo13945b() && i >= f4324a) {
            Log.println(i, mo13940a(), str);
        }
    }
}
