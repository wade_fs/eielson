package com.baidu.mapapi.map;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import com.baidu.mapapi.model.LatLng;

public final class ArcOptions extends OverlayOptions {

    /* renamed from: d */
    private static final String f2370d = ArcOptions.class.getSimpleName();

    /* renamed from: a */
    int f2371a;

    /* renamed from: b */
    boolean f2372b = true;

    /* renamed from: c */
    Bundle f2373c;

    /* renamed from: e */
    private int f2374e = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: f */
    private int f2375f = 5;

    /* renamed from: g */
    private LatLng f2376g;

    /* renamed from: h */
    private LatLng f2377h;

    /* renamed from: i */
    private LatLng f2378i;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Overlay mo10909a() {
        Arc arc = new Arc();
        arc.f2662A = this.f2372b;
        arc.f2665z = this.f2371a;
        arc.f2663B = this.f2373c;
        arc.f2365a = this.f2374e;
        arc.f2366b = this.f2375f;
        arc.f2367c = this.f2376g;
        arc.f2368d = this.f2377h;
        arc.f2369e = this.f2378i;
        return arc;
    }

    public ArcOptions color(int i) {
        this.f2374e = i;
        return this;
    }

    public ArcOptions extraInfo(Bundle bundle) {
        this.f2373c = bundle;
        return this;
    }

    public int getColor() {
        return this.f2374e;
    }

    public LatLng getEndPoint() {
        return this.f2378i;
    }

    public Bundle getExtraInfo() {
        return this.f2373c;
    }

    public LatLng getMiddlePoint() {
        return this.f2377h;
    }

    public LatLng getStartPoint() {
        return this.f2376g;
    }

    public int getWidth() {
        return this.f2375f;
    }

    public int getZIndex() {
        return this.f2371a;
    }

    public boolean isVisible() {
        return this.f2372b;
    }

    public ArcOptions points(LatLng latLng, LatLng latLng2, LatLng latLng3) {
        if (latLng == null || latLng2 == null || latLng3 == null) {
            throw new IllegalArgumentException("BDMapSDKException: start and middle and end points can not be null");
        } else if (latLng == latLng2 || latLng == latLng3 || latLng2 == latLng3) {
            throw new IllegalArgumentException("BDMapSDKException: start and middle and end points can not be same");
        } else {
            this.f2376g = latLng;
            this.f2377h = latLng2;
            this.f2378i = latLng3;
            return this;
        }
    }

    public ArcOptions visible(boolean z) {
        this.f2372b = z;
        return this;
    }

    public ArcOptions width(int i) {
        if (i > 0) {
            this.f2375f = i;
        }
        return this;
    }

    public ArcOptions zIndex(int i) {
        this.f2371a = i;
        return this;
    }
}
