package com.baidu.mapsdkplatform.comapi.util;

import android.content.Context;
import android.os.Environment;
import java.io.File;

/* renamed from: com.baidu.mapsdkplatform.comapi.util.g */
public final class C1173g {

    /* renamed from: a */
    private final boolean f3895a;

    /* renamed from: b */
    private final String f3896b;

    /* renamed from: c */
    private final String f3897c;

    /* renamed from: d */
    private final String f3898d;

    /* renamed from: e */
    private final String f3899e;

    /* renamed from: f */
    private final String f3900f;

    C1173g(Context context) {
        this.f3895a = false;
        this.f3896b = Environment.getExternalStorageDirectory().getAbsolutePath();
        this.f3897c = this.f3896b + File.separator + "BaiduMapSDKNew";
        this.f3898d = context.getCacheDir().getAbsolutePath();
        this.f3899e = "";
        this.f3900f = "";
    }

    C1173g(String str, boolean z, String str2, Context context) {
        this.f3895a = z;
        this.f3896b = str;
        this.f3897c = this.f3896b + File.separator + "BaiduMapSDKNew";
        this.f3898d = this.f3897c + File.separator + "cache";
        this.f3899e = context.getCacheDir().getAbsolutePath();
        this.f3900f = str2;
    }

    /* renamed from: a */
    public String mo13396a() {
        return this.f3896b;
    }

    /* renamed from: b */
    public String mo13397b() {
        return this.f3896b + File.separator + "BaiduMapSDKNew";
    }

    /* renamed from: c */
    public String mo13398c() {
        return this.f3898d;
    }

    /* renamed from: d */
    public String mo13399d() {
        return this.f3899e;
    }

    public boolean equals(Object obj) {
        if (obj == null || !C1173g.class.isInstance(obj)) {
            return false;
        }
        return this.f3896b.equals(((C1173g) obj).f3896b);
    }
}
