package com.baidu.location.p013a;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.baidu.location.BDLocation;
import com.baidu.location.Jni;
import com.baidu.location.LocationClientOption;
import com.baidu.location.p016d.C0790a;
import com.baidu.location.p017e.C0821a;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0855k;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mobstat.Config;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/* renamed from: com.baidu.location.a.c */
public class C0727c {

    /* renamed from: i */
    private static Class<?> f1228i;

    /* renamed from: a */
    String f1229a = null;

    /* renamed from: b */
    String f1230b = null;

    /* renamed from: c */
    C0729b f1231c = new C0729b();

    /* renamed from: d */
    private Context f1232d = null;

    /* renamed from: e */
    private TelephonyManager f1233e = null;

    /* renamed from: f */
    private C0821a f1234f = new C0821a();
    /* access modifiers changed from: private */

    /* renamed from: g */
    public WifiManager f1235g = null;

    /* renamed from: h */
    private C0730c f1236h = null;

    /* renamed from: j */
    private String f1237j = null;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public LocationClientOption f1238k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public C0728a f1239l;

    /* renamed from: m */
    private String f1240m = null;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public String f1241n = null;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public String f1242o = null;

    /* renamed from: p */
    private boolean f1243p = false;

    /* renamed from: q */
    private long f1244q = 0;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public boolean f1245r = false;

    /* renamed from: com.baidu.location.a.c$a */
    public interface C0728a {
        void onReceiveLocation(BDLocation bDLocation);
    }

    /* renamed from: com.baidu.location.a.c$b */
    class C0729b extends C0849e {

        /* renamed from: a */
        String f1246a = null;

        C0729b() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1882h = C0855k.m2466e();
            if (!(C0727c.this.f1241n == null || C0727c.this.f1242o == null)) {
                this.f1246a += String.format(Locale.CHINA, "&ki=%s&sn=%s", C0727c.this.f1241n, C0727c.this.f1242o);
            }
            String encodeTp4 = Jni.encodeTp4(this.f1246a);
            this.f1246a = null;
            this.f1885k.put("bloc", encodeTp4);
            this.f1885k.put("trtm", String.format(Locale.CHINA, "%d", Long.valueOf(System.currentTimeMillis())));
        }

        /* renamed from: a */
        public void mo10445a(String str) {
            this.f1246a = str;
            mo10734c(C0855k.f1962f);
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(8:4|5|6|7|8|9|10|(1:12)) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0010 */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo10446a(boolean r4) {
            /*
                r3 = this;
                java.lang.String r0 = ";"
                if (r4 == 0) goto L_0x006d
                java.lang.String r4 = r3.f1884j
                if (r4 == 0) goto L_0x006d
                java.lang.String r4 = r3.f1884j     // Catch:{ Exception -> 0x006c }
                com.baidu.location.BDLocation r1 = new com.baidu.location.BDLocation     // Catch:{ Exception -> 0x0010 }
                r1.<init>(r4)     // Catch:{ Exception -> 0x0010 }
                goto L_0x001a
            L_0x0010:
                com.baidu.location.BDLocation r1 = new com.baidu.location.BDLocation     // Catch:{ Exception -> 0x006c }
                r1.<init>()     // Catch:{ Exception -> 0x006c }
                r4 = 63
                r1.setLocType(r4)     // Catch:{ Exception -> 0x006c }
            L_0x001a:
                int r4 = r1.getLocType()     // Catch:{ Exception -> 0x006c }
                r2 = 161(0xa1, float:2.26E-43)
                if (r4 != r2) goto L_0x006d
                com.baidu.location.a.c r4 = com.baidu.location.p013a.C0727c.this     // Catch:{ Exception -> 0x006c }
                com.baidu.location.LocationClientOption r4 = r4.f1238k     // Catch:{ Exception -> 0x006c }
                java.lang.String r4 = r4.coorType     // Catch:{ Exception -> 0x006c }
                r1.setCoorType(r4)     // Catch:{ Exception -> 0x006c }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006c }
                r4.<init>()     // Catch:{ Exception -> 0x006c }
                com.baidu.location.a.c r2 = com.baidu.location.p013a.C0727c.this     // Catch:{ Exception -> 0x006c }
                java.lang.String r2 = r2.f1229a     // Catch:{ Exception -> 0x006c }
                r4.append(r2)     // Catch:{ Exception -> 0x006c }
                r4.append(r0)     // Catch:{ Exception -> 0x006c }
                com.baidu.location.a.c r2 = com.baidu.location.p013a.C0727c.this     // Catch:{ Exception -> 0x006c }
                java.lang.String r2 = r2.f1230b     // Catch:{ Exception -> 0x006c }
                r4.append(r2)     // Catch:{ Exception -> 0x006c }
                r4.append(r0)     // Catch:{ Exception -> 0x006c }
                java.lang.String r0 = r1.getTime()     // Catch:{ Exception -> 0x006c }
                r4.append(r0)     // Catch:{ Exception -> 0x006c }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x006c }
                java.lang.String r4 = com.baidu.location.Jni.en1(r4)     // Catch:{ Exception -> 0x006c }
                r1.setLocationID(r4)     // Catch:{ Exception -> 0x006c }
                r4 = 0
                r1.setRoadLocString(r4, r4)     // Catch:{ Exception -> 0x006c }
                com.baidu.location.a.c r4 = com.baidu.location.p013a.C0727c.this     // Catch:{ Exception -> 0x006c }
                r0 = 1
                boolean unused = r4.f1245r = r0     // Catch:{ Exception -> 0x006c }
                com.baidu.location.a.c r4 = com.baidu.location.p013a.C0727c.this     // Catch:{ Exception -> 0x006c }
                com.baidu.location.a.c$a r4 = r4.f1239l     // Catch:{ Exception -> 0x006c }
                r4.onReceiveLocation(r1)     // Catch:{ Exception -> 0x006c }
                goto L_0x006d
            L_0x006c:
            L_0x006d:
                java.util.Map r4 = r3.f1885k
                if (r4 == 0) goto L_0x0076
                java.util.Map r4 = r3.f1885k
                r4.clear()
            L_0x0076:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0727c.C0729b.mo10446a(boolean):void");
        }
    }

    /* renamed from: com.baidu.location.a.c$c */
    protected class C0730c {

        /* renamed from: a */
        public List<ScanResult> f1248a = null;

        /* renamed from: c */
        private long f1250c = 0;

        public C0730c(List<ScanResult> list) {
            this.f1248a = list;
            this.f1250c = System.currentTimeMillis();
            try {
                m1779c();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* renamed from: b */
        private String m1778b() {
            WifiInfo connectionInfo;
            if (C0727c.this.f1235g == null || (connectionInfo = C0727c.this.f1235g.getConnectionInfo()) == null) {
                return null;
            }
            try {
                String bssid = connectionInfo.getBSSID();
                String replace = bssid != null ? bssid.replace(Config.TRACE_TODAY_VISIT_SPLIT, "") : null;
                if (replace == null || replace.length() == 12) {
                    return new String(replace);
                }
                return null;
            } catch (Exception unused) {
                return null;
            }
        }

        /* renamed from: c */
        private void m1779c() {
            if (mo10447a() >= 1) {
                int size = this.f1248a.size() - 1;
                boolean z = true;
                while (size >= 1 && z) {
                    boolean z2 = false;
                    for (int i = 0; i < size; i++) {
                        if (this.f1248a.get(i) != null) {
                            int i2 = i + 1;
                            if (this.f1248a.get(i2) != null && this.f1248a.get(i).level < this.f1248a.get(i2).level) {
                                List<ScanResult> list = this.f1248a;
                                list.set(i2, list.get(i));
                                this.f1248a.set(i, this.f1248a.get(i2));
                                z2 = true;
                            }
                        }
                    }
                    size--;
                    z = z2;
                }
            }
        }

        /* renamed from: a */
        public int mo10447a() {
            List<ScanResult> list = this.f1248a;
            if (list == null) {
                return 0;
            }
            return list.size();
        }

        /* JADX WARNING: Removed duplicated region for block: B:18:0x004d  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x00d5  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x00df  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x00ef  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x00f9 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x00fb  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x00e9 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:79:0x00eb A[EDGE_INSN: B:79:0x00eb->B:53:0x00eb ?: BREAK  , SYNTHETIC] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String mo10448a(int r25) {
            /*
                r24 = this;
                r0 = r24
                int r1 = r24.mo10447a()
                r2 = 0
                r3 = 2
                if (r1 >= r3) goto L_0x000b
                return r2
            L_0x000b:
                java.util.ArrayList r1 = new java.util.ArrayList
                r1.<init>()
                int r3 = android.os.Build.VERSION.SDK_INT
                r4 = 19
                r5 = 1
                r6 = 0
                if (r3 < r4) goto L_0x002b
                long r3 = android.os.SystemClock.elapsedRealtimeNanos()     // Catch:{ Error -> 0x0021 }
                r9 = 1000(0x3e8, double:4.94E-321)
                long r3 = r3 / r9
                goto L_0x0022
            L_0x0021:
                r3 = r6
            L_0x0022:
                int r9 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
                if (r9 <= 0) goto L_0x0029
                r9 = r3
                r3 = 1
                goto L_0x002d
            L_0x0029:
                r9 = r3
                goto L_0x002c
            L_0x002b:
                r9 = r6
            L_0x002c:
                r3 = 0
            L_0x002d:
                java.lang.StringBuffer r4 = new java.lang.StringBuffer
                r11 = 512(0x200, float:7.175E-43)
                r4.<init>(r11)
                java.util.List<android.net.wifi.ScanResult> r11 = r0.f1248a
                int r11 = r11.size()
                java.lang.String r12 = r24.m1778b()
                r17 = r6
                r13 = 0
                r14 = 0
                r15 = 1
                r16 = 0
                r19 = 0
            L_0x0047:
                java.lang.String r6 = ""
                java.lang.String r7 = "|"
                if (r13 >= r11) goto L_0x00e9
                java.util.List<android.net.wifi.ScanResult> r2 = r0.f1248a
                java.lang.Object r2 = r2.get(r13)
                android.net.wifi.ScanResult r2 = (android.net.wifi.ScanResult) r2
                int r2 = r2.level
                if (r2 != 0) goto L_0x005f
                r5 = r25
                r21 = r11
                goto L_0x00e1
            L_0x005f:
                int r14 = r14 + 1
                if (r15 == 0) goto L_0x006a
                java.lang.String r2 = "&wf="
                r4.append(r2)
                r15 = 0
                goto L_0x006d
            L_0x006a:
                r4.append(r7)
            L_0x006d:
                java.util.List<android.net.wifi.ScanResult> r2 = r0.f1248a
                java.lang.Object r2 = r2.get(r13)
                android.net.wifi.ScanResult r2 = (android.net.wifi.ScanResult) r2
                java.lang.String r2 = r2.BSSID
                java.lang.String r8 = ":"
                java.lang.String r2 = r2.replace(r8, r6)
                r4.append(r2)
                if (r12 == 0) goto L_0x008a
                boolean r2 = r2.equals(r12)
                if (r2 == 0) goto L_0x008a
                r19 = r14
            L_0x008a:
                java.util.List<android.net.wifi.ScanResult> r2 = r0.f1248a
                java.lang.Object r2 = r2.get(r13)
                android.net.wifi.ScanResult r2 = (android.net.wifi.ScanResult) r2
                int r2 = r2.level
                if (r2 >= 0) goto L_0x0097
                int r2 = -r2
            L_0x0097:
                java.util.Locale r8 = java.util.Locale.CHINA
                r21 = r11
                java.lang.Object[] r11 = new java.lang.Object[r5]
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                r20 = 0
                r11[r20] = r2
                java.lang.String r2 = ";%d;"
                java.lang.String r2 = java.lang.String.format(r8, r2, r11)
                r4.append(r2)
                int r2 = r16 + 1
                if (r3 == 0) goto L_0x00d8
                java.util.List<android.net.wifi.ScanResult> r8 = r0.f1248a     // Catch:{ all -> 0x00c6 }
                java.lang.Object r8 = r8.get(r13)     // Catch:{ all -> 0x00c6 }
                android.net.wifi.ScanResult r8 = (android.net.wifi.ScanResult) r8     // Catch:{ all -> 0x00c6 }
                r16 = r6
                long r5 = r8.timestamp     // Catch:{ all -> 0x00c8 }
                long r5 = r9 - r5
                r22 = 1000000(0xf4240, double:4.940656E-318)
                long r5 = r5 / r22
                goto L_0x00ca
            L_0x00c6:
                r16 = r6
            L_0x00c8:
                r5 = 0
            L_0x00ca:
                java.lang.Long r8 = java.lang.Long.valueOf(r5)
                r1.add(r8)
                int r8 = (r5 > r17 ? 1 : (r5 == r17 ? 0 : -1))
                if (r8 <= 0) goto L_0x00da
                r17 = r5
                goto L_0x00da
            L_0x00d8:
                r16 = r6
            L_0x00da:
                r5 = r25
                if (r2 <= r5) goto L_0x00df
                goto L_0x00eb
            L_0x00df:
                r16 = r2
            L_0x00e1:
                int r13 = r13 + 1
                r11 = r21
                r2 = 0
                r5 = 1
                goto L_0x0047
            L_0x00e9:
                r16 = r6
            L_0x00eb:
                r2 = r19
                if (r2 <= 0) goto L_0x00f7
                java.lang.String r3 = "&wf_n="
                r4.append(r3)
                r4.append(r2)
            L_0x00f7:
                if (r15 == 0) goto L_0x00fb
                r2 = 0
                return r2
            L_0x00fb:
                r2 = 10
                int r5 = (r17 > r2 ? 1 : (r17 == r2 ? 0 : -1))
                if (r5 <= 0) goto L_0x0179
                int r2 = r1.size()
                if (r2 <= 0) goto L_0x0179
                r2 = 0
                java.lang.Object r3 = r1.get(r2)
                java.lang.Long r3 = (java.lang.Long) r3
                long r5 = r3.longValue()
                r8 = 0
                int r3 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
                if (r3 <= 0) goto L_0x0179
                java.lang.StringBuffer r3 = new java.lang.StringBuffer
                r5 = 128(0x80, float:1.794E-43)
                r3.<init>(r5)
                java.lang.String r5 = "&wf_ut="
                r3.append(r5)
                java.lang.Object r5 = r1.get(r2)
                java.lang.Long r5 = (java.lang.Long) r5
                java.util.Iterator r1 = r1.iterator()
                r11 = 1
            L_0x012f:
                boolean r6 = r1.hasNext()
                if (r6 == 0) goto L_0x0172
                java.lang.Object r6 = r1.next()
                java.lang.Long r6 = (java.lang.Long) r6
                long r8 = r6.longValue()
                if (r11 == 0) goto L_0x014a
                r3.append(r8)
                r10 = r16
                r11 = 0
                r12 = 0
                goto L_0x016c
            L_0x014a:
                long r12 = r5.longValue()
                long r8 = r8 - r12
                r12 = 0
                int r6 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
                if (r6 == 0) goto L_0x016a
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                r10 = r16
                r6.append(r10)
                r6.append(r8)
                java.lang.String r6 = r6.toString()
                r3.append(r6)
                goto L_0x016c
            L_0x016a:
                r10 = r16
            L_0x016c:
                r3.append(r7)
                r16 = r10
                goto L_0x012f
            L_0x0172:
                java.lang.String r1 = r3.toString()
                r4.append(r1)
            L_0x0179:
                java.lang.String r1 = r4.toString()
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0727c.C0730c.mo10448a(int):java.lang.String");
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0075 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0177  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0190  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C0727c(android.content.Context r5, com.baidu.location.LocationClientOption r6, com.baidu.location.p013a.C0727c.C0728a r7) {
        /*
            r4 = this;
            r4.<init>()
            r0 = 0
            r4.f1232d = r0
            r4.f1233e = r0
            com.baidu.location.e.a r1 = new com.baidu.location.e.a
            r1.<init>()
            r4.f1234f = r1
            r4.f1235g = r0
            r4.f1236h = r0
            r4.f1237j = r0
            r4.f1240m = r0
            r4.f1241n = r0
            r4.f1242o = r0
            r4.f1229a = r0
            r4.f1230b = r0
            r1 = 0
            r4.f1243p = r1
            com.baidu.location.a.c$b r2 = new com.baidu.location.a.c$b
            r2.<init>()
            r4.f1231c = r2
            r2 = 0
            r4.f1244q = r2
            r4.f1245r = r1
            android.content.Context r5 = r5.getApplicationContext()
            r4.f1232d = r5
            android.content.Context r5 = r4.f1232d     // Catch:{ Exception -> 0x003e }
            java.lang.String r5 = r5.getPackageName()     // Catch:{ Exception -> 0x003e }
            com.baidu.location.p019g.C0855k.f1955ax = r5     // Catch:{ Exception -> 0x003e }
            goto L_0x003f
        L_0x003e:
        L_0x003f:
            r5 = 1
            r4.f1243p = r5
            boolean r1 = r4.f1243p
            if (r1 == 0) goto L_0x01de
            com.baidu.location.LocationClientOption r1 = new com.baidu.location.LocationClientOption
            r1.<init>(r6)
            r4.f1238k = r1
            r4.f1239l = r7
            android.content.Context r7 = r4.f1232d
            java.lang.String r7 = r7.getPackageName()
            r4.f1229a = r7
            r4.f1230b = r0
            android.content.Context r7 = r4.f1232d     // Catch:{ Exception -> 0x0075 }
            java.lang.String r1 = "phone"
            java.lang.Object r7 = r7.getSystemService(r1)     // Catch:{ Exception -> 0x0075 }
            android.telephony.TelephonyManager r7 = (android.telephony.TelephonyManager) r7     // Catch:{ Exception -> 0x0075 }
            r4.f1233e = r7     // Catch:{ Exception -> 0x0075 }
            android.content.Context r7 = r4.f1232d     // Catch:{ Exception -> 0x0075 }
            android.content.Context r7 = r7.getApplicationContext()     // Catch:{ Exception -> 0x0075 }
            java.lang.String r1 = "wifi"
            java.lang.Object r7 = r7.getSystemService(r1)     // Catch:{ Exception -> 0x0075 }
            android.net.wifi.WifiManager r7 = (android.net.wifi.WifiManager) r7     // Catch:{ Exception -> 0x0075 }
            r4.f1235g = r7     // Catch:{ Exception -> 0x0075 }
        L_0x0075:
            android.content.Context r7 = r4.f1232d     // Catch:{ all -> 0x007e }
            java.lang.String r7 = com.baidu.android.bbalbs.common.util.CommonParam.m1545a(r7)     // Catch:{ all -> 0x007e }
            r4.f1230b = r7     // Catch:{ all -> 0x007e }
            goto L_0x0084
        L_0x007e:
            r4.f1230b = r0
            r4.f1233e = r0
            r4.f1235g = r0
        L_0x0084:
            java.lang.String r7 = r4.f1230b
            java.lang.String r1 = "&coor="
            java.lang.String r2 = ":"
            java.lang.String r3 = "&prod="
            if (r7 == 0) goto L_0x00c2
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r0 = ""
            r7.append(r0)
            java.lang.String r0 = r4.f1230b
            r7.append(r0)
            java.lang.String r7 = r7.toString()
            com.baidu.location.p019g.C0855k.f1971o = r7
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r3)
            com.baidu.location.LocationClientOption r0 = r4.f1238k
            java.lang.String r0 = r0.prodName
            r7.append(r0)
            r7.append(r2)
            java.lang.String r0 = r4.f1229a
            r7.append(r0)
            java.lang.String r0 = "|&cu="
            r7.append(r0)
            java.lang.String r0 = r4.f1230b
            goto L_0x00de
        L_0x00c2:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r3)
            com.baidu.location.LocationClientOption r3 = r4.f1238k
            java.lang.String r3 = r3.prodName
            r7.append(r3)
            r7.append(r2)
            java.lang.String r2 = r4.f1229a
            r7.append(r2)
            java.lang.String r2 = "|&im="
            r7.append(r2)
        L_0x00de:
            r7.append(r0)
            r7.append(r1)
            java.lang.String r0 = r6.getCoorType()
            r7.append(r0)
            java.lang.String r7 = r7.toString()
            r4.f1237j = r7
            java.lang.StringBuffer r7 = new java.lang.StringBuffer
            r0 = 256(0x100, float:3.59E-43)
            r7.<init>(r0)
            java.lang.String r0 = "&fw="
            r7.append(r0)
            java.lang.String r0 = "7.82"
            r7.append(r0)
            java.lang.String r1 = "&sdk="
            r7.append(r1)
            r7.append(r0)
            java.lang.String r0 = "&lt=1"
            r7.append(r0)
            java.lang.String r0 = "&mb="
            r7.append(r0)
            java.lang.String r0 = android.os.Build.MODEL
            r7.append(r0)
            java.lang.String r0 = "&resid="
            r7.append(r0)
            java.lang.String r0 = "12"
            r7.append(r0)
            r6.getAddrType()
            java.lang.String r0 = r6.getAddrType()
            if (r0 == 0) goto L_0x0156
            java.lang.String r0 = r6.getAddrType()
            java.lang.String r1 = "all"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0156
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r4.f1237j
            r0.append(r1)
            java.lang.String r1 = "&addr=allj"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r4.f1237j = r0
            boolean r0 = r6.isNeedNewVersionRgc
            if (r0 == 0) goto L_0x0156
            java.lang.String r0 = "&adtp=n2"
            r7.append(r0)
        L_0x0156:
            boolean r0 = r6.isNeedAptag
            if (r0 == r5) goto L_0x015e
            boolean r0 = r6.isNeedAptagd
            if (r0 != r5) goto L_0x01b5
        L_0x015e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r4.f1237j
            r0.append(r1)
            java.lang.String r1 = "&sema="
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r4.f1237j = r0
            boolean r0 = r6.isNeedAptag
            if (r0 != r5) goto L_0x018c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r4.f1237j
            r0.append(r1)
            java.lang.String r1 = "aptag|"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r4.f1237j = r0
        L_0x018c:
            boolean r6 = r6.isNeedAptagd
            if (r6 != r5) goto L_0x01a5
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = r4.f1237j
            r5.append(r6)
            java.lang.String r6 = "aptagd|"
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.f1237j = r5
        L_0x01a5:
            android.content.Context r5 = r4.f1232d
            java.lang.String r5 = com.baidu.location.p013a.C0741j.m1845b(r5)
            r4.f1241n = r5
            android.content.Context r5 = r4.f1232d
            java.lang.String r5 = com.baidu.location.p013a.C0741j.m1846c(r5)
            r4.f1242o = r5
        L_0x01b5:
            java.lang.String r5 = "&first=1"
            r7.append(r5)
            java.lang.String r5 = "&os=A"
            r7.append(r5)
            java.lang.String r5 = android.os.Build.VERSION.SDK
            r7.append(r5)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = r4.f1237j
            r5.append(r6)
            java.lang.String r6 = r7.toString()
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.f1237j = r5
            r4.mo10442a()
        L_0x01de:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0727c.<init>(android.content.Context, com.baidu.location.LocationClientOption, com.baidu.location.a.c$a):void");
    }

    /* renamed from: a */
    private int m1762a(int i) {
        if (i == Integer.MAX_VALUE) {
            return -1;
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d4, code lost:
        if (r2 <= 0) goto L_0x0117;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x015f */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0121 A[Catch:{ Exception -> 0x015f }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.baidu.location.p017e.C0821a m1763a(android.telephony.CellInfo r10) {
        /*
            r9 = this;
            int r0 = android.os.Build.VERSION.SDK_INT
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r0 = r0.intValue()
            r1 = 17
            if (r0 >= r1) goto L_0x0010
            r10 = 0
            return r10
        L_0x0010:
            com.baidu.location.e.a r1 = new com.baidu.location.e.a
            r1.<init>()
            boolean r2 = r10 instanceof android.telephony.CellInfoGsm
            r3 = 0
            r4 = 1
            r5 = 103(0x67, float:1.44E-43)
            if (r2 == 0) goto L_0x005a
            r2 = r10
            android.telephony.CellInfoGsm r2 = (android.telephony.CellInfoGsm) r2
            android.telephony.CellIdentityGsm r3 = r2.getCellIdentity()
            int r6 = r3.getMcc()
            int r6 = r9.m1762a(r6)
            r1.f1720c = r6
            int r6 = r3.getMnc()
            int r6 = r9.m1762a(r6)
            r1.f1721d = r6
            int r6 = r3.getLac()
            int r6 = r9.m1762a(r6)
            r1.f1718a = r6
            int r3 = r3.getCid()
            int r3 = r9.m1762a(r3)
            r1.f1719b = r3
            r1.f1726i = r5
            android.telephony.CellSignalStrengthGsm r2 = r2.getCellSignalStrength()
            int r2 = r2.getAsuLevel()
        L_0x0056:
            r1.f1725h = r2
            goto L_0x0117
        L_0x005a:
            boolean r2 = r10 instanceof android.telephony.CellInfoCdma
            if (r2 == 0) goto L_0x00d7
            r2 = r10
            android.telephony.CellInfoCdma r2 = (android.telephony.CellInfoCdma) r2
            android.telephony.CellIdentityCdma r6 = r2.getCellIdentity()
            int r7 = r6.getLatitude()
            r1.f1722e = r7
            int r7 = r6.getLongitude()
            r1.f1723f = r7
            int r7 = r6.getSystemId()
            int r7 = r9.m1762a(r7)
            r1.f1721d = r7
            int r7 = r6.getNetworkId()
            int r7 = r9.m1762a(r7)
            r1.f1718a = r7
            int r6 = r6.getBasestationId()
            int r6 = r9.m1762a(r6)
            r1.f1719b = r6
            r6 = 99
            r1.f1726i = r6
            android.telephony.CellSignalStrengthCdma r2 = r2.getCellSignalStrength()
            int r2 = r2.getCdmaDbm()
            r1.f1725h = r2
            com.baidu.location.e.a r2 = r9.f1234f
            if (r2 == 0) goto L_0x00ac
            int r2 = r2.f1720c
            if (r2 <= 0) goto L_0x00ac
            com.baidu.location.e.a r2 = r9.f1234f
            int r2 = r2.f1720c
        L_0x00a9:
            r1.f1720c = r2
            goto L_0x0117
        L_0x00ac:
            r2 = -1
            android.telephony.TelephonyManager r6 = r9.f1233e     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r6 = r6.getNetworkOperator()     // Catch:{ Exception -> 0x00d3 }
            if (r6 == 0) goto L_0x00d4
            int r7 = r6.length()     // Catch:{ Exception -> 0x00d3 }
            if (r7 <= 0) goto L_0x00d4
            int r7 = r6.length()     // Catch:{ Exception -> 0x00d3 }
            r8 = 3
            if (r7 < r8) goto L_0x00d4
            java.lang.String r3 = r6.substring(r3, r8)     // Catch:{ Exception -> 0x00d3 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x00d3 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x00d3 }
            if (r3 >= 0) goto L_0x00d1
            goto L_0x00d4
        L_0x00d1:
            r2 = r3
            goto L_0x00d4
        L_0x00d3:
        L_0x00d4:
            if (r2 <= 0) goto L_0x0117
            goto L_0x00a9
        L_0x00d7:
            boolean r2 = r10 instanceof android.telephony.CellInfoLte
            if (r2 == 0) goto L_0x0116
            r2 = r10
            android.telephony.CellInfoLte r2 = (android.telephony.CellInfoLte) r2
            android.telephony.CellIdentityLte r3 = r2.getCellIdentity()
            int r6 = r3.getMcc()
            int r6 = r9.m1762a(r6)
            r1.f1720c = r6
            int r6 = r3.getMnc()
            int r6 = r9.m1762a(r6)
            r1.f1721d = r6
            int r6 = r3.getTac()
            int r6 = r9.m1762a(r6)
            r1.f1718a = r6
            int r3 = r3.getCi()
            int r3 = r9.m1762a(r3)
            r1.f1719b = r3
            r1.f1726i = r5
            android.telephony.CellSignalStrengthLte r2 = r2.getCellSignalStrength()
            int r2 = r2.getAsuLevel()
            goto L_0x0056
        L_0x0116:
            r4 = 0
        L_0x0117:
            r2 = 18
            if (r0 < r2) goto L_0x015f
            if (r4 != 0) goto L_0x015f
            boolean r0 = r10 instanceof android.telephony.CellInfoWcdma     // Catch:{ Exception -> 0x015f }
            if (r0 == 0) goto L_0x015f
            r0 = r10
            android.telephony.CellInfoWcdma r0 = (android.telephony.CellInfoWcdma) r0     // Catch:{ Exception -> 0x015f }
            android.telephony.CellIdentityWcdma r0 = r0.getCellIdentity()     // Catch:{ Exception -> 0x015f }
            int r2 = r0.getMcc()     // Catch:{ Exception -> 0x015f }
            int r2 = r9.m1762a(r2)     // Catch:{ Exception -> 0x015f }
            r1.f1720c = r2     // Catch:{ Exception -> 0x015f }
            int r2 = r0.getMnc()     // Catch:{ Exception -> 0x015f }
            int r2 = r9.m1762a(r2)     // Catch:{ Exception -> 0x015f }
            r1.f1721d = r2     // Catch:{ Exception -> 0x015f }
            int r2 = r0.getLac()     // Catch:{ Exception -> 0x015f }
            int r2 = r9.m1762a(r2)     // Catch:{ Exception -> 0x015f }
            r1.f1718a = r2     // Catch:{ Exception -> 0x015f }
            int r0 = r0.getCid()     // Catch:{ Exception -> 0x015f }
            int r0 = r9.m1762a(r0)     // Catch:{ Exception -> 0x015f }
            r1.f1719b = r0     // Catch:{ Exception -> 0x015f }
            r1.f1726i = r5     // Catch:{ Exception -> 0x015f }
            r0 = r10
            android.telephony.CellInfoWcdma r0 = (android.telephony.CellInfoWcdma) r0     // Catch:{ Exception -> 0x015f }
            android.telephony.CellSignalStrengthWcdma r0 = r0.getCellSignalStrength()     // Catch:{ Exception -> 0x015f }
            int r0 = r0.getAsuLevel()     // Catch:{ Exception -> 0x015f }
            r1.f1725h = r0     // Catch:{ Exception -> 0x015f }
        L_0x015f:
            long r2 = android.os.SystemClock.elapsedRealtimeNanos()     // Catch:{ Error -> 0x0174 }
            long r4 = r10.getTimeStamp()     // Catch:{ Error -> 0x0174 }
            long r2 = r2 - r4
            r4 = 1000000(0xf4240, double:4.940656E-318)
            long r2 = r2 / r4
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Error -> 0x0174 }
            long r4 = r4 - r2
            r1.f1724g = r4     // Catch:{ Error -> 0x0174 }
            goto L_0x017a
        L_0x0174:
            long r2 = java.lang.System.currentTimeMillis()
            r1.f1724g = r2
        L_0x017a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0727c.m1763a(android.telephony.CellInfo):com.baidu.location.e.a");
    }

    /* renamed from: a */
    private void m1765a(CellLocation cellLocation) {
        if (cellLocation != null && this.f1233e != null) {
            C0821a aVar = new C0821a();
            String networkOperator = this.f1233e.getNetworkOperator();
            if (networkOperator != null && networkOperator.length() > 0) {
                try {
                    if (networkOperator.length() >= 3) {
                        int intValue = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
                        if (intValue < 0) {
                            intValue = this.f1234f.f1720c;
                        }
                        aVar.f1720c = intValue;
                    }
                    String substring = networkOperator.substring(3);
                    if (substring != null) {
                        char[] charArray = substring.toCharArray();
                        int i = 0;
                        while (true) {
                            if (i >= charArray.length) {
                                break;
                            } else if (!Character.isDigit(charArray[i])) {
                                break;
                            } else {
                                i++;
                            }
                        }
                        int intValue2 = Integer.valueOf(substring.substring(0, i)).intValue();
                        if (intValue2 < 0) {
                            intValue2 = this.f1234f.f1721d;
                        }
                        aVar.f1721d = intValue2;
                    }
                } catch (Exception unused) {
                }
            }
            if (cellLocation instanceof GsmCellLocation) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                aVar.f1718a = gsmCellLocation.getLac();
                aVar.f1719b = gsmCellLocation.getCid();
                aVar.f1726i = 'g';
            } else if (cellLocation instanceof CdmaCellLocation) {
                aVar.f1726i = 'c';
                if (f1228i == null) {
                    try {
                        f1228i = Class.forName("android.telephony.cdma.CdmaCellLocation");
                    } catch (Exception unused2) {
                        f1228i = null;
                        return;
                    }
                }
                Class<?> cls = f1228i;
                if (cls != null && cls.isInstance(cellLocation)) {
                    try {
                        int systemId = ((CdmaCellLocation) cellLocation).getSystemId();
                        if (systemId < 0) {
                            systemId = -1;
                        }
                        aVar.f1721d = systemId;
                        aVar.f1719b = ((CdmaCellLocation) cellLocation).getBaseStationId();
                        aVar.f1718a = ((CdmaCellLocation) cellLocation).getNetworkId();
                        int baseStationLatitude = ((CdmaCellLocation) cellLocation).getBaseStationLatitude();
                        if (baseStationLatitude < Integer.MAX_VALUE) {
                            aVar.f1722e = baseStationLatitude;
                        }
                        int baseStationLongitude = ((CdmaCellLocation) cellLocation).getBaseStationLongitude();
                        if (baseStationLongitude < Integer.MAX_VALUE) {
                            aVar.f1723f = baseStationLongitude;
                        }
                    } catch (Exception unused3) {
                    }
                }
            }
            if (aVar.mo10617b()) {
                this.f1234f = aVar;
            } else {
                this.f1234f = null;
            }
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0053 */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006c A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0088 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0089  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m1767b(int r5) {
        /*
            r4 = this;
            r0 = 0
            com.baidu.location.e.a r1 = r4.m1770c()     // Catch:{ all -> 0x0052 }
            if (r1 == 0) goto L_0x0011
            boolean r2 = r1.mo10617b()     // Catch:{ all -> 0x0052 }
            if (r2 != 0) goto L_0x000e
            goto L_0x0011
        L_0x000e:
            r4.f1234f = r1     // Catch:{ all -> 0x0052 }
            goto L_0x001a
        L_0x0011:
            android.telephony.TelephonyManager r1 = r4.f1233e     // Catch:{ all -> 0x0052 }
            android.telephony.CellLocation r1 = r1.getCellLocation()     // Catch:{ all -> 0x0052 }
            r4.m1765a(r1)     // Catch:{ all -> 0x0052 }
        L_0x001a:
            com.baidu.location.e.a r1 = r4.f1234f     // Catch:{ all -> 0x0052 }
            if (r1 == 0) goto L_0x002d
            com.baidu.location.e.a r1 = r4.f1234f     // Catch:{ all -> 0x0052 }
            boolean r1 = r1.mo10617b()     // Catch:{ all -> 0x0052 }
            if (r1 == 0) goto L_0x002d
            com.baidu.location.e.a r1 = r4.f1234f     // Catch:{ all -> 0x0052 }
            java.lang.String r1 = r1.mo10623h()     // Catch:{ all -> 0x0052 }
            goto L_0x002e
        L_0x002d:
            r1 = r0
        L_0x002e:
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0053 }
            if (r2 != 0) goto L_0x0053
            com.baidu.location.e.a r2 = r4.f1234f     // Catch:{ all -> 0x0053 }
            if (r2 == 0) goto L_0x0053
            com.baidu.location.e.a r2 = r4.f1234f     // Catch:{ all -> 0x0053 }
            java.lang.String r2 = r2.f1727j     // Catch:{ all -> 0x0053 }
            if (r2 == 0) goto L_0x0053
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0053 }
            r2.<init>()     // Catch:{ all -> 0x0053 }
            r2.append(r1)     // Catch:{ all -> 0x0053 }
            com.baidu.location.e.a r3 = r4.f1234f     // Catch:{ all -> 0x0053 }
            java.lang.String r3 = r3.f1727j     // Catch:{ all -> 0x0053 }
            r2.append(r3)     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = r2.toString()     // Catch:{ all -> 0x0053 }
            goto L_0x0053
        L_0x0052:
            r1 = r0
        L_0x0053:
            r4.f1236h = r0     // Catch:{ Exception -> 0x0069 }
            com.baidu.location.a.c$c r2 = new com.baidu.location.a.c$c     // Catch:{ Exception -> 0x0069 }
            android.net.wifi.WifiManager r3 = r4.f1235g     // Catch:{ Exception -> 0x0069 }
            java.util.List r3 = r3.getScanResults()     // Catch:{ Exception -> 0x0069 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0069 }
            r4.f1236h = r2     // Catch:{ Exception -> 0x0069 }
            com.baidu.location.a.c$c r2 = r4.f1236h     // Catch:{ Exception -> 0x0069 }
            java.lang.String r5 = r2.mo10448a(r5)     // Catch:{ Exception -> 0x0069 }
            goto L_0x006a
        L_0x0069:
            r5 = r0
        L_0x006a:
            if (r1 != 0) goto L_0x0071
            if (r5 != 0) goto L_0x0071
            r4.f1240m = r0
            return r0
        L_0x0071:
            if (r5 == 0) goto L_0x0086
            if (r1 != 0) goto L_0x0077
            r1 = r5
            goto L_0x0086
        L_0x0077:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r1)
            r2.append(r5)
            java.lang.String r1 = r2.toString()
        L_0x0086:
            if (r1 != 0) goto L_0x0089
            return r0
        L_0x0089:
            r4.f1240m = r1
            java.lang.String r5 = r4.f1237j
            if (r5 == 0) goto L_0x00a4
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r0 = r4.f1240m
            r5.append(r0)
            java.lang.String r0 = r4.f1237j
            r5.append(r0)
            java.lang.String r5 = r5.toString()
            r4.f1240m = r5
        L_0x00a4:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r1)
            java.lang.String r0 = r4.f1237j
            r5.append(r0)
            java.lang.String r5 = r5.toString()
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0727c.m1767b(int):java.lang.String");
    }

    /* renamed from: c */
    private C0821a m1770c() {
        if (Integer.valueOf(Build.VERSION.SDK_INT).intValue() < 17) {
            return null;
        }
        try {
            List<CellInfo> allCellInfo = this.f1233e.getAllCellInfo();
            if (allCellInfo == null || allCellInfo.size() <= 0) {
                return null;
            }
            C0821a aVar = null;
            for (CellInfo cellInfo : allCellInfo) {
                if (cellInfo.isRegistered()) {
                    boolean z = false;
                    if (aVar != null) {
                        z = true;
                    }
                    C0821a a = m1763a(cellInfo);
                    if (a != null) {
                        if (!a.mo10617b()) {
                            a = null;
                        } else if (z && aVar != null) {
                            aVar.f1727j = a.mo10624i();
                            return aVar;
                        }
                        if (aVar == null) {
                            aVar = a;
                        }
                    }
                }
            }
            return aVar;
        } catch (Throwable unused) {
            return null;
        }
    }

    /* renamed from: a */
    public String mo10442a() {
        try {
            return m1767b(15);
        } catch (Exception unused) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.d.a.a(java.lang.String, java.util.List<android.net.wifi.ScanResult>, boolean):com.baidu.location.BDLocation
     arg types: [java.lang.String, java.util.List<android.net.wifi.ScanResult>, int]
     candidates:
      com.baidu.location.d.a.a(com.baidu.location.e.h, com.baidu.location.BDLocation, android.database.sqlite.SQLiteDatabase):void
      com.baidu.location.d.a.a(java.lang.String, com.baidu.location.e.a, android.database.sqlite.SQLiteDatabase):void
      com.baidu.location.d.a.a(java.lang.String, java.util.List<android.net.wifi.ScanResult>, boolean):com.baidu.location.BDLocation */
    /* renamed from: b */
    public void mo10443b() {
        BDLocation bDLocation;
        if (this.f1240m != null && this.f1243p) {
            BDLocation bDLocation2 = null;
            if (this.f1235g != null && this.f1238k.scanSpan >= 1000 && !this.f1238k.getAddrType().equals("all") && !this.f1238k.isNeedAptag && !this.f1238k.isNeedAptagd) {
                try {
                    String g = this.f1234f != null ? this.f1234f.mo10622g() : null;
                    if (this.f1235g != null) {
                        bDLocation = C0790a.m2063a().mo10554a(g, this.f1235g.getScanResults(), false);
                        if (bDLocation != null && bDLocation.getLocType() == 66 && Math.abs(bDLocation.getLatitude()) < 0.10000000149011612d && Math.abs(bDLocation.getLongitude()) < 0.10000000149011612d) {
                            bDLocation.setLocType(67);
                        }
                    } else {
                        bDLocation = null;
                    }
                    if (bDLocation != null) {
                        bDLocation.getLocType();
                    }
                    if (bDLocation != null) {
                        bDLocation.getLocType();
                    }
                    if (!this.f1238k.coorType.equals(CoordinateType.GCJ02) && bDLocation != null && bDLocation.getLocType() == 66) {
                        double longitude = bDLocation.getLongitude();
                        double latitude = bDLocation.getLatitude();
                        if (Math.abs(longitude) > 0.10000000149011612d && Math.abs(latitude) > 0.10000000149011612d) {
                            double[] coorEncrypt = Jni.coorEncrypt(longitude, latitude, this.f1238k.coorType);
                            bDLocation.setLongitude(coorEncrypt[0]);
                            bDLocation.setLatitude(coorEncrypt[1]);
                            bDLocation.setCoorType(this.f1238k.coorType);
                        }
                    }
                    if (bDLocation != null && bDLocation.getLocType() == 66 && Math.abs(bDLocation.getLatitude()) > 0.10000000149011612d && Math.abs(bDLocation.getLongitude()) > 0.10000000149011612d) {
                        if (!this.f1245r) {
                            this.f1239l.onReceiveLocation(bDLocation);
                        }
                        bDLocation2 = bDLocation;
                    }
                } catch (Exception unused) {
                }
            }
            if (bDLocation2 == null) {
                this.f1231c.mo10445a(this.f1240m);
            }
        }
    }
}
