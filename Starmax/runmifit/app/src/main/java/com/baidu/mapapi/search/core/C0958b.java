package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.core.b */
final class C0958b implements Parcelable.Creator<CityInfo> {
    C0958b() {
    }

    /* renamed from: a */
    public CityInfo createFromParcel(Parcel parcel) {
        return new CityInfo(parcel);
    }

    /* renamed from: a */
    public CityInfo[] newArray(int i) {
        return new CityInfo[i];
    }
}
