package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.core.c */
final class C0959c implements Parcelable.Creator<CoachInfo> {
    C0959c() {
    }

    /* renamed from: a */
    public CoachInfo createFromParcel(Parcel parcel) {
        return new CoachInfo(parcel);
    }

    /* renamed from: a */
    public CoachInfo[] newArray(int i) {
        return new CoachInfo[i];
    }
}
