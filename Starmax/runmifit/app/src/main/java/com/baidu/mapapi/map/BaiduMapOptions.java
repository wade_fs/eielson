package com.baidu.mapapi.map;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapsdkplatform.comapi.map.C1102z;

public final class BaiduMapOptions implements Parcelable {
    public static final Parcelable.Creator<BaiduMapOptions> CREATOR = new C0930g();

    /* renamed from: a */
    MapStatus f2425a;

    /* renamed from: b */
    boolean f2426b;

    /* renamed from: c */
    int f2427c;

    /* renamed from: d */
    boolean f2428d;

    /* renamed from: e */
    boolean f2429e;

    /* renamed from: f */
    boolean f2430f;

    /* renamed from: g */
    boolean f2431g;

    /* renamed from: h */
    boolean f2432h;

    /* renamed from: i */
    boolean f2433i;

    /* renamed from: j */
    LogoPosition f2434j;

    /* renamed from: k */
    Point f2435k;

    /* renamed from: l */
    Point f2436l;

    public BaiduMapOptions() {
        this.f2425a = new MapStatus(0.0f, new LatLng(39.914935d, 116.403119d), 0.0f, 12.0f, null, null);
        this.f2426b = true;
        this.f2427c = 1;
        this.f2428d = true;
        this.f2429e = true;
        this.f2430f = true;
        this.f2431g = true;
        this.f2432h = true;
        this.f2433i = true;
    }

    protected BaiduMapOptions(Parcel parcel) {
        this.f2425a = new MapStatus(0.0f, new LatLng(39.914935d, 116.403119d), 0.0f, 12.0f, null, null);
        boolean z = true;
        this.f2426b = true;
        this.f2427c = 1;
        this.f2428d = true;
        this.f2429e = true;
        this.f2430f = true;
        this.f2431g = true;
        this.f2432h = true;
        this.f2433i = true;
        this.f2425a = (MapStatus) parcel.readParcelable(MapStatus.class.getClassLoader());
        this.f2426b = parcel.readByte() != 0;
        this.f2427c = parcel.readInt();
        this.f2428d = parcel.readByte() != 0;
        this.f2429e = parcel.readByte() != 0;
        this.f2430f = parcel.readByte() != 0;
        this.f2431g = parcel.readByte() != 0;
        this.f2432h = parcel.readByte() != 0;
        this.f2433i = parcel.readByte() == 0 ? false : z;
        this.f2435k = (Point) parcel.readParcelable(Point.class.getClassLoader());
        this.f2436l = (Point) parcel.readParcelable(Point.class.getClassLoader());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C1102z mo11031a() {
        return new C1102z().mo13143a(this.f2425a.mo11181c()).mo13144a(this.f2426b).mo13142a(this.f2427c).mo13145b(this.f2428d).mo13146c(this.f2429e).mo13147d(this.f2430f).mo13148e(this.f2431g);
    }

    public BaiduMapOptions compassEnabled(boolean z) {
        this.f2426b = z;
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public BaiduMapOptions logoPosition(LogoPosition logoPosition) {
        this.f2434j = logoPosition;
        return this;
    }

    public BaiduMapOptions mapStatus(MapStatus mapStatus) {
        if (mapStatus != null) {
            this.f2425a = mapStatus;
        }
        return this;
    }

    public BaiduMapOptions mapType(int i) {
        this.f2427c = i;
        return this;
    }

    public BaiduMapOptions overlookingGesturesEnabled(boolean z) {
        this.f2430f = z;
        return this;
    }

    public BaiduMapOptions rotateGesturesEnabled(boolean z) {
        this.f2428d = z;
        return this;
    }

    public BaiduMapOptions scaleControlEnabled(boolean z) {
        this.f2433i = z;
        return this;
    }

    public BaiduMapOptions scaleControlPosition(Point point) {
        this.f2435k = point;
        return this;
    }

    public BaiduMapOptions scrollGesturesEnabled(boolean z) {
        this.f2429e = z;
        return this;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f2425a, i);
        parcel.writeByte(this.f2426b ? (byte) 1 : 0);
        parcel.writeInt(this.f2427c);
        parcel.writeByte(this.f2428d ? (byte) 1 : 0);
        parcel.writeByte(this.f2429e ? (byte) 1 : 0);
        parcel.writeByte(this.f2430f ? (byte) 1 : 0);
        parcel.writeByte(this.f2431g ? (byte) 1 : 0);
        parcel.writeByte(this.f2432h ? (byte) 1 : 0);
        parcel.writeByte(this.f2433i ? (byte) 1 : 0);
        parcel.writeParcelable(this.f2435k, i);
        parcel.writeParcelable(this.f2436l, i);
    }

    public BaiduMapOptions zoomControlsEnabled(boolean z) {
        this.f2432h = z;
        return this;
    }

    public BaiduMapOptions zoomControlsPosition(Point point) {
        this.f2436l = point;
        return this;
    }

    public BaiduMapOptions zoomGesturesEnabled(boolean z) {
        this.f2431g = z;
        return this;
    }
}
