package com.baidu.mobstat;

import android.content.Context;
import android.text.TextUtils;
import com.baidu.mobstat.Config;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class EventAnalysis {

    /* renamed from: a */
    private Map<String, C1214a> f4127a = new HashMap();

    public void onEvent(Context context, long j, String str, String str2, int i, long j2, ExtraInfo extraInfo, Map<String, String> map, boolean z) {
        m4431a(context, j, str, str2, i, j2, 0, extraInfo, map, z);
    }

    public void onEvent(Context context, long j, String str, String str2, int i, long j2, String str3, String str4, int i2, boolean z) {
        m4432a(context, j, str, str2, i, j2, 0, str3, str4, i2);
    }

    public void onEvent(Context context, long j, String str, String str2, int i, long j2, JSONArray jSONArray, JSONArray jSONArray2, String str3, String str4, String str5, Map<String, String> map, boolean z) {
        flushEvent(context, j, str, str2, i, j2, jSONArray, jSONArray2, str3, str4, str5, map, z);
    }

    public void onEventStart(Context context, String str, String str2, long j) {
        C1214a aVar = new C1214a();
        aVar.f4130c = j;
        aVar.f4128a = str;
        aVar.f4129b = str2;
        String a = m4430a(str, str2);
        if (this.f4127a.containsKey(a)) {
            C1256at c = C1256at.m4629c();
            c.mo13943b("[WARNING] eventId: " + str + ", with label: " + str2 + " is duplicated, older is removed");
        }
        this.f4127a.put(a, aVar);
    }

    public void onEventEnd(Context context, long j, String str, String str2, long j2, ExtraInfo extraInfo, Map<String, String> map, boolean z) {
        String str3 = str;
        String str4 = str2;
        String a = m4430a(str3, str4);
        C1214a aVar = this.f4127a.get(a);
        if (aVar == null) {
            C1256at c = C1256at.m4629c();
            c.mo13943b("[WARNING] eventId: " + str3 + ", with label: " + str4 + " is not started or alread ended");
        } else if ((str3 == null || str3.equals(aVar.f4128a)) && (str4 == null || str4.equals(aVar.f4129b))) {
            this.f4127a.remove(a);
            long j3 = j2 - aVar.f4130c;
            if (j3 < 0) {
                C1256at.m4629c().mo13943b("[WARNING] onEventEnd must be invoked after onEventStart");
            }
            onEventDuration(context, j, str, str2, aVar.f4130c, j3, extraInfo, map, z);
        } else {
            C1256at.m4629c().mo13943b("[WARNING] eventId/label pair not match");
        }
    }

    public void onEventDuration(Context context, long j, String str, String str2, long j2, long j3, ExtraInfo extraInfo, Map<String, String> map, boolean z) {
        if (j3 > 0) {
            m4431a(context, j, str, str2, 1, j2, j3, extraInfo, map, z);
        }
    }

    /* renamed from: a */
    private void m4431a(Context context, long j, String str, String str2, int i, long j2, long j3, ExtraInfo extraInfo, Map<String, String> map, boolean z) {
        Context context2 = context;
        DataCore.instance().putEvent(context2, getEvent(context, j, str, str2, i, j2, j3, "", "", 0, 0, extraInfo, map, z));
        DataCore.instance().flush(context2);
    }

    /* renamed from: a */
    private void m4432a(Context context, long j, String str, String str2, int i, long j2, long j3, String str3, String str4, int i2) {
        Context context2 = context;
        DataCore.instance().putEvent(context2, getEvent(context, j, str, str2, i, j2, j3, str3, str4, i2, 1, null, null, false));
        DataCore.instance().flush(context2);
    }

    public void flushEvent(Context context, long j, String str, String str2, int i, long j2, JSONArray jSONArray, JSONArray jSONArray2, String str3, String str4, String str5, Map<String, String> map, boolean z) {
        String str6 = "";
        Context context2 = context;
        DataCore.instance().putEvent(context2, getEvent(context, j, str, str2, i, j2, 0, str6, jSONArray, jSONArray2, str3, str4, str5, Config.EventViewType.EDIT.getValue(), 2, null, map, "", "", z));
        DataCore.instance().flush(context2);
    }

    /* renamed from: a */
    private String m4430a(String str, String str2) {
        return "__sdk_" + str + "$|$" + str2;
    }

    /* renamed from: com.baidu.mobstat.EventAnalysis$a */
    static class C1214a {

        /* renamed from: a */
        String f4128a;

        /* renamed from: b */
        String f4129b;

        /* renamed from: c */
        long f4130c;

        private C1214a() {
        }
    }

    public static JSONObject getEvent(Context context, long j, String str, String str2, int i, long j2, long j3, String str3, String str4, int i2, int i3, ExtraInfo extraInfo, Map<String, String> map, boolean z) {
        return getEvent(context, j, str, str2, i, j2, j3, str3, null, null, str4, null, null, i2, i3, extraInfo, map, "", "", z);
    }

    public static JSONObject getEvent(Context context, long j, String str, String str2, int i, long j2, long j3, String str3, JSONArray jSONArray, JSONArray jSONArray2, String str4, String str5, String str6, int i2, int i3, ExtraInfo extraInfo, Map<String, String> map, String str7, String str8, boolean z) {
        return getEvent(context, j, str, str2, i, j2, j3, str3, jSONArray, jSONArray2, str4, str5, str6, i2, i3, extraInfo, map, str7, str8, z, null, "");
    }

    public static JSONObject getEvent(Context context, long j, String str, String str2, int i, long j2, long j3, String str3, JSONArray jSONArray, JSONArray jSONArray2, String str4, String str5, String str6, int i2, int i3, ExtraInfo extraInfo, Map<String, String> map, String str7, String str8, boolean z, JSONObject jSONObject, String str9) {
        int i4 = i3;
        JSONObject jSONObject2 = jSONObject;
        JSONObject jSONObject3 = new JSONObject();
        try {
            jSONObject3.put("ss", j);
            String str10 = str;
            jSONObject3.put("i", str);
            String str11 = str2;
            jSONObject3.put("l", str2);
            int i5 = i;
            jSONObject3.put("c", i);
            jSONObject3.put("t", j2);
            jSONObject3.put("d", j3);
            jSONObject3.put("h", str3);
            if (i4 != 3) {
                jSONObject3.put(Config.EVENT_NATIVE_VIEW_HIERARCHY, jSONArray);
                jSONObject3.put(Config.EVENT_H5_VIEW_HIERARCHY, jSONArray2);
            } else {
                jSONObject3.put(Config.EVENT_NATIVE_VIEW_HIERARCHY, str7);
                jSONObject3.put(Config.EVENT_H5_VIEW_HIERARCHY, str8);
            }
            jSONObject3.put("p", str4);
            jSONObject3.put(Config.EVENT_H5_PAGE, str5);
            jSONObject3.put(Config.EVENT_VIEW_RES_NAME, str6);
            jSONObject3.put("v", i2);
            jSONObject3.put("at", i4);
            jSONObject3.put("h5", z ? 1 : 0);
            if (!(extraInfo == null || extraInfo.dumpToJson().length() == 0)) {
                jSONObject3.put("ext", extraInfo.dumpToJson());
            }
            if (map != null) {
                JSONArray jSONArray3 = new JSONArray();
                for (Map.Entry entry : map.entrySet()) {
                    String str12 = (String) entry.getKey();
                    String str13 = (String) entry.getValue();
                    if (!TextUtils.isEmpty(str12)) {
                        if (!TextUtils.isEmpty(str13)) {
                            if (!m4435a(str13, 1024)) {
                                JSONObject jSONObject4 = new JSONObject();
                                jSONObject4.put(Config.APP_KEY, str12);
                                jSONObject4.put("v", str13);
                                jSONArray3.put(jSONObject4);
                            }
                        }
                    }
                }
                if (jSONArray3.length() != 0) {
                    jSONObject3.put(Config.EVENT_ATTR, jSONArray3);
                }
            }
            if (!(jSONObject2 == null || jSONObject.length() == 0)) {
                JSONArray jSONArray4 = new JSONArray();
                jSONArray4.put(jSONObject2);
                jSONObject3.put(Config.EVENT_HEAT_POINT, jSONArray4);
            }
            jSONObject3.put("sign", TextUtils.isEmpty(str9) ? "" : str9);
        } catch (Exception unused) {
        }
        return jSONObject3;
    }

    /* renamed from: a */
    private static boolean m4435a(String str, int i) {
        int i2;
        if (str == null) {
            return false;
        }
        try {
            i2 = str.getBytes().length;
        } catch (Exception unused) {
            i2 = 0;
        }
        return i2 > i;
    }

    /* renamed from: b */
    private static boolean m4437b(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !new JSONObject().toString().equals(str)) {
            return true;
        }
        if (TextUtils.isEmpty(str2) || new JSONArray().toString().equals(str2)) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void doEventMerge(org.json.JSONArray r24, org.json.JSONObject r25) {
        /*
            r1 = r25
            java.lang.String r0 = "s"
            com.baidu.mobstat.Config$EventViewType r2 = com.baidu.mobstat.Config.EventViewType.EDIT
            r2.getValue()
            java.lang.String r2 = "ss"
            long r2 = r1.optLong(r2)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r4 = "i"
            java.lang.String r4 = r1.getString(r4)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r5 = "l"
            java.lang.String r5 = r1.getString(r5)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r6 = "t"
            long r6 = r1.getLong(r6)     // Catch:{ JSONException -> 0x00c3 }
            r8 = 3600000(0x36ee80, double:1.7786363E-317)
            long r7 = r6 / r8
            java.lang.String r6 = r1.optString(r0)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r9 = "at"
            int r15 = r1.optInt(r9)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r9 = "h"
            java.lang.String r9 = r1.optString(r9)     // Catch:{ JSONException -> 0x00c3 }
            r10 = 3
            java.lang.String r11 = "h3"
            java.lang.String r12 = "h2"
            java.lang.String r13 = ""
            r14 = 0
            if (r15 == r10) goto L_0x004d
            org.json.JSONArray r10 = r1.optJSONArray(r12)     // Catch:{ JSONException -> 0x00c3 }
            org.json.JSONArray r11 = r1.optJSONArray(r11)     // Catch:{ JSONException -> 0x00c3 }
            r17 = r13
            r18 = r17
            goto L_0x005b
        L_0x004d:
            java.lang.String r10 = r1.optString(r12)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r11 = r1.optString(r11)     // Catch:{ JSONException -> 0x00c3 }
            r17 = r10
            r18 = r11
            r10 = r14
            r11 = r10
        L_0x005b:
            java.lang.String r12 = "p"
            java.lang.String r12 = r1.optString(r12)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r13 = "p2"
            java.lang.String r13 = r1.optString(r13)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r14 = "rn"
            java.lang.String r14 = r1.optString(r14)     // Catch:{ JSONException -> 0x00c3 }
            r16 = r15
            java.lang.String r15 = "v"
            int r15 = r1.optInt(r15)     // Catch:{ JSONException -> 0x00c3 }
            r19 = r0
            java.lang.String r0 = "ext"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ JSONException -> 0x00c3 }
            r20 = r15
            java.lang.String r15 = "attribute"
            java.lang.String r15 = r1.optString(r15)     // Catch:{ JSONException -> 0x00c3 }
            r21 = r14
            java.lang.String r14 = "h5"
            int r22 = r1.optInt(r14)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r14 = "sign"
            java.lang.String r23 = r1.optString(r14)     // Catch:{ JSONException -> 0x00c3 }
            java.lang.String r14 = "d"
            int r14 = r1.getInt(r14)     // Catch:{ JSONException -> 0x009a }
            goto L_0x009b
        L_0x009a:
            r14 = 0
        L_0x009b:
            if (r14 != 0) goto L_0x00b3
            boolean r0 = m4437b(r0, r15)
            if (r0 != 0) goto L_0x00b3
            r0 = r24
            r1 = r25
            r14 = r21
            r15 = r20
            r19 = r22
            r20 = r23
            m4433a(r0, r1, r2, r4, r5, r6, r7, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            goto L_0x00c3
        L_0x00b3:
            int r0 = r24.length()
            java.lang.String r2 = "0"
            r3 = r19
            r1.put(r3, r2)     // Catch:{  }
            r2 = r24
            r2.put(r0, r1)     // Catch:{  }
        L_0x00c3:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.EventAnalysis.doEventMerge(org.json.JSONArray, org.json.JSONObject):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x001a, code lost:
        if (r2.equals(r8) != false) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0163, code lost:
        if (r4.equalsIgnoreCase(r5) != false) goto L_0x016a;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m4433a(org.json.JSONArray r31, org.json.JSONObject r32, long r33, java.lang.String r35, java.lang.String r36, java.lang.String r37, long r38, java.lang.String r40, org.json.JSONArray r41, org.json.JSONArray r42, java.lang.String r43, java.lang.String r44, java.lang.String r45, int r46, int r47, java.lang.String r48, java.lang.String r49, int r50, java.lang.String r51) {
        /*
            r0 = r31
            r1 = r32
            r2 = r37
            java.lang.String r3 = "t"
            java.lang.String r4 = "c"
            int r5 = r31.length()
            java.lang.String r6 = "0|"
            java.lang.String r7 = "s"
            java.lang.String r8 = ""
            if (r2 == 0) goto L_0x001c
            boolean r2 = r2.equals(r8)     // Catch:{ JSONException -> 0x001f }
            if (r2 == 0) goto L_0x001f
        L_0x001c:
            r1.put(r7, r6)     // Catch:{ JSONException -> 0x001f }
        L_0x001f:
            r10 = r5
            r9 = 0
        L_0x0021:
            if (r9 >= r5) goto L_0x0283
            org.json.JSONObject r11 = r0.getJSONObject(r9)     // Catch:{ JSONException -> 0x0252 }
            java.lang.String r12 = "ss"
            long r12 = r11.optLong(r12)     // Catch:{ JSONException -> 0x0252 }
            java.lang.String r14 = "i"
            java.lang.String r14 = r11.getString(r14)     // Catch:{ JSONException -> 0x0252 }
            java.lang.String r15 = "l"
            java.lang.String r15 = r11.getString(r15)     // Catch:{ JSONException -> 0x0252 }
            long r16 = r11.getLong(r3)     // Catch:{ JSONException -> 0x0252 }
            r18 = 3600000(0x36ee80, double:1.7786363E-317)
            long r16 = r16 / r18
            java.lang.String r2 = "d"
            int r2 = r11.getInt(r2)     // Catch:{ JSONException -> 0x004b }
            r18 = r6
            goto L_0x004e
        L_0x004b:
            r18 = r6
            r2 = 0
        L_0x004e:
            java.lang.String r6 = "h"
            java.lang.String r6 = r11.optString(r6)     // Catch:{ JSONException -> 0x0244 }
            r19 = r10
            java.lang.String r10 = "p"
            java.lang.String r10 = r11.optString(r10)     // Catch:{ JSONException -> 0x022d }
            java.lang.String r0 = "p2"
            java.lang.String r0 = r11.optString(r0)     // Catch:{ JSONException -> 0x022d }
            r20 = r5
            java.lang.String r5 = "rn"
            java.lang.String r5 = r11.optString(r5)     // Catch:{ JSONException -> 0x021f }
            r21 = r9
            java.lang.String r9 = "v"
            int r9 = r11.optInt(r9)     // Catch:{ JSONException -> 0x0209 }
            r22 = r3
            java.lang.String r3 = "at"
            int r3 = r11.optInt(r3)     // Catch:{ JSONException -> 0x01f4 }
            r23 = r8
            java.lang.String r8 = "h3"
            r25 = r7
            java.lang.String r7 = "h2"
            r26 = 0
            r1 = 3
            if (r3 == r1) goto L_0x0099
            org.json.JSONArray r26 = r11.optJSONArray(r7)     // Catch:{ JSONException -> 0x01e2 }
            org.json.JSONArray r1 = r11.optJSONArray(r8)     // Catch:{ JSONException -> 0x01e2 }
            r7 = r1
            r24 = r4
            r8 = r23
            r1 = r26
            r26 = r8
            goto L_0x00a9
        L_0x0099:
            java.lang.String r1 = r11.optString(r7)     // Catch:{ JSONException -> 0x01e2 }
            java.lang.String r8 = r11.optString(r8)     // Catch:{ JSONException -> 0x01e2 }
            r24 = r4
            r7 = r26
            r26 = r8
            r8 = r1
            r1 = r7
        L_0x00a9:
            java.lang.String r4 = "ext"
            java.lang.String r4 = r11.optString(r4)     // Catch:{ JSONException -> 0x01cc }
            r27 = r8
            java.lang.String r8 = "attribute"
            java.lang.String r8 = r11.optString(r8)     // Catch:{ JSONException -> 0x01cc }
            r28 = r3
            java.lang.String r3 = "h5"
            int r3 = r11.optInt(r3)     // Catch:{ JSONException -> 0x01cc }
            r29 = r3
            java.lang.String r3 = "sign"
            java.lang.String r3 = r11.optString(r3)     // Catch:{ JSONException -> 0x01cc }
            int r30 = (r16 > r38 ? 1 : (r16 == r38 ? 0 : -1))
            if (r30 != 0) goto L_0x01cc
            if (r2 == 0) goto L_0x00cf
            goto L_0x01cc
        L_0x00cf:
            boolean r2 = m4437b(r4, r8)     // Catch:{ JSONException -> 0x01cc }
            if (r2 == 0) goto L_0x00d7
            goto L_0x01cc
        L_0x00d7:
            int r2 = (r12 > r33 ? 1 : (r12 == r33 ? 0 : -1))
            if (r2 != 0) goto L_0x01cc
            r2 = r35
            boolean r4 = r14.equals(r2)     // Catch:{ JSONException -> 0x01cc }
            if (r4 == 0) goto L_0x01cc
            r4 = r36
            boolean r8 = r15.equals(r4)     // Catch:{ JSONException -> 0x01cc }
            if (r8 == 0) goto L_0x01cc
            r8 = r40
            boolean r6 = r6.equals(r8)     // Catch:{ JSONException -> 0x01c9 }
            if (r6 == 0) goto L_0x01c9
            r6 = r43
            boolean r10 = r10.equals(r6)     // Catch:{ JSONException -> 0x01c9 }
            if (r10 == 0) goto L_0x01c9
            r10 = r44
            boolean r0 = r0.equals(r10)     // Catch:{ JSONException -> 0x01c6 }
            if (r0 == 0) goto L_0x01c6
            r0 = r41
            boolean r1 = m4436a(r1, r0)     // Catch:{ JSONException -> 0x01c6 }
            if (r1 == 0) goto L_0x01c6
            r1 = r42
            boolean r7 = m4436a(r7, r1)     // Catch:{ JSONException -> 0x01c6 }
            if (r7 == 0) goto L_0x01c6
            r7 = r45
            boolean r5 = r5.equals(r7)     // Catch:{ JSONException -> 0x01c6 }
            if (r5 == 0) goto L_0x01c6
            r5 = r46
            if (r9 != r5) goto L_0x01c6
            r9 = r47
            r12 = r28
            if (r12 != r9) goto L_0x01c3
            r12 = r48
            r13 = r27
            boolean r13 = r13.equals(r12)     // Catch:{ JSONException -> 0x01c0 }
            if (r13 == 0) goto L_0x01c0
            r13 = r49
            r14 = r26
            boolean r14 = r14.equals(r13)     // Catch:{ JSONException -> 0x01bd }
            if (r14 == 0) goto L_0x01bd
            r14 = r50
            r15 = r29
            if (r15 != r14) goto L_0x01ba
            r15 = r51
            boolean r3 = r3.equals(r15)     // Catch:{ JSONException -> 0x01b7 }
            if (r3 == 0) goto L_0x01b7
            r3 = r32
            r0 = r24
            int r16 = r3.getInt(r0)     // Catch:{ JSONException -> 0x01de }
            int r17 = r11.getInt(r0)     // Catch:{ JSONException -> 0x01de }
            int r1 = r16 + r17
            r2 = r25
            java.lang.String r4 = r11.optString(r2)     // Catch:{ JSONException -> 0x026f }
            r5 = r23
            if (r4 == 0) goto L_0x016a
            boolean r16 = r4.equalsIgnoreCase(r5)     // Catch:{ JSONException -> 0x0166 }
            if (r16 == 0) goto L_0x016c
            goto L_0x016a
        L_0x0166:
            r23 = r5
            goto L_0x026f
        L_0x016a:
            r4 = r18
        L_0x016c:
            r23 = r5
            r5 = r22
            long r16 = r3.getLong(r5)     // Catch:{ JSONException -> 0x01b3 }
            long r24 = r11.getLong(r5)     // Catch:{ JSONException -> 0x01b3 }
            long r16 = r16 - r24
            r24 = 0
            int r22 = (r16 > r24 ? 1 : (r16 == r24 ? 0 : -1))
            if (r22 >= 0) goto L_0x0185
            r22 = r5
            r5 = r24
            goto L_0x0189
        L_0x0185:
            r22 = r5
            r5 = r16
        L_0x0189:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x026f }
            r7.<init>()     // Catch:{ JSONException -> 0x026f }
            r7.append(r4)     // Catch:{ JSONException -> 0x026f }
            r7.append(r5)     // Catch:{ JSONException -> 0x026f }
            java.lang.String r4 = "|"
            r7.append(r4)     // Catch:{ JSONException -> 0x026f }
            java.lang.String r4 = r7.toString()     // Catch:{ JSONException -> 0x026f }
            r11.remove(r0)     // Catch:{ JSONException -> 0x01af }
            r11.put(r0, r1)     // Catch:{ JSONException -> 0x01af }
            r11.put(r2, r4)     // Catch:{ JSONException -> 0x01af }
            m4434a(r11, r3)     // Catch:{ JSONException -> 0x01af }
            r0 = r20
            r1 = r21
            goto L_0x0289
        L_0x01af:
            r19 = r21
            goto L_0x026f
        L_0x01b3:
            r22 = r5
            goto L_0x026f
        L_0x01b7:
            r3 = r32
            goto L_0x01dc
        L_0x01ba:
            r3 = r32
            goto L_0x01da
        L_0x01bd:
            r3 = r32
            goto L_0x01d8
        L_0x01c0:
            r3 = r32
            goto L_0x01d6
        L_0x01c3:
            r3 = r32
            goto L_0x01d4
        L_0x01c6:
            r3 = r32
            goto L_0x01d2
        L_0x01c9:
            r3 = r32
            goto L_0x01d0
        L_0x01cc:
            r3 = r32
            r8 = r40
        L_0x01d0:
            r10 = r44
        L_0x01d2:
            r9 = r47
        L_0x01d4:
            r12 = r48
        L_0x01d6:
            r13 = r49
        L_0x01d8:
            r14 = r50
        L_0x01da:
            r15 = r51
        L_0x01dc:
            r0 = r24
        L_0x01de:
            r2 = r25
            goto L_0x026f
        L_0x01e2:
            r3 = r32
            r8 = r40
            r10 = r44
            r9 = r47
            r12 = r48
            r13 = r49
            r14 = r50
            r15 = r51
            r0 = r4
            goto L_0x01de
        L_0x01f4:
            r10 = r44
            r9 = r47
            r12 = r48
            r13 = r49
            r14 = r50
            r15 = r51
            r3 = r1
            r0 = r4
            r2 = r7
            r23 = r8
            r8 = r40
            goto L_0x026f
        L_0x0209:
            r10 = r44
            r9 = r47
            r12 = r48
            r13 = r49
            r14 = r50
            r15 = r51
            r22 = r3
            r0 = r4
            r2 = r7
            r23 = r8
            r8 = r40
            goto L_0x026e
        L_0x021f:
            r10 = r44
            r12 = r48
            r13 = r49
            r14 = r50
            r15 = r51
            r22 = r3
            r0 = r4
            goto L_0x023c
        L_0x022d:
            r10 = r44
            r12 = r48
            r13 = r49
            r14 = r50
            r15 = r51
            r22 = r3
            r0 = r4
            r20 = r5
        L_0x023c:
            r2 = r7
            r23 = r8
            r21 = r9
            r8 = r40
            goto L_0x026c
        L_0x0244:
            r12 = r48
            r13 = r49
            r14 = r50
            r15 = r51
            r22 = r3
            r0 = r4
            r20 = r5
            goto L_0x0261
        L_0x0252:
            r12 = r48
            r13 = r49
            r14 = r50
            r15 = r51
            r22 = r3
            r0 = r4
            r20 = r5
            r18 = r6
        L_0x0261:
            r2 = r7
            r23 = r8
            r21 = r9
            r19 = r10
            r8 = r40
            r10 = r44
        L_0x026c:
            r9 = r47
        L_0x026e:
            r3 = r1
        L_0x026f:
            int r1 = r21 + 1
            r4 = r0
            r9 = r1
            r7 = r2
            r1 = r3
            r6 = r18
            r10 = r19
            r5 = r20
            r3 = r22
            r8 = r23
            r0 = r31
            goto L_0x0021
        L_0x0283:
            r3 = r1
            r19 = r10
            r0 = r5
            r1 = r19
        L_0x0289:
            if (r1 >= r0) goto L_0x028c
            return
        L_0x028c:
            r1 = r31
            r1.put(r0, r3)     // Catch:{ JSONException -> 0x0291 }
        L_0x0291:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.EventAnalysis.m4433a(org.json.JSONArray, org.json.JSONObject, long, java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, org.json.JSONArray, org.json.JSONArray, java.lang.String, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String, int, java.lang.String):void");
    }

    /* renamed from: a */
    private static void m4434a(JSONObject jSONObject, JSONObject jSONObject2) {
        JSONArray optJSONArray;
        if (jSONObject != null && jSONObject2 != null) {
            JSONArray jSONArray = new JSONArray();
            JSONArray optJSONArray2 = jSONObject.optJSONArray(Config.EVENT_HEAT_POINT);
            if (!(optJSONArray2 == null || optJSONArray2.length() == 0)) {
                for (int i = 0; i < optJSONArray2.length(); i++) {
                    try {
                        jSONArray.put(optJSONArray2.getJSONObject(i));
                    } catch (Exception unused) {
                    }
                }
            }
            if (!(jSONArray.length() >= 10 || (optJSONArray = jSONObject2.optJSONArray(Config.EVENT_HEAT_POINT)) == null || optJSONArray.length() == 0)) {
                for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                    try {
                        jSONArray.put(optJSONArray.getJSONObject(i2));
                    } catch (Exception unused2) {
                    }
                }
            }
            if (jSONArray.length() != 0) {
                try {
                    jSONObject.put(Config.EVENT_HEAT_POINT, jSONArray);
                } catch (Exception unused3) {
                }
            }
        }
    }

    /* renamed from: a */
    private static boolean m4436a(JSONArray jSONArray, JSONArray jSONArray2) {
        if (jSONArray == null && jSONArray2 == null) {
            return true;
        }
        return (jSONArray == null || jSONArray2 == null || !jSONArray.toString().equals(jSONArray2.toString())) ? false : true;
    }
}
