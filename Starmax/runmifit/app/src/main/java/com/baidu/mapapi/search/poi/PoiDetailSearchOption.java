package com.baidu.mapapi.search.poi;

public class PoiDetailSearchOption {

    /* renamed from: a */
    private String f2986a = "";

    /* renamed from: b */
    private String f2987b = "";

    /* renamed from: c */
    private boolean f2988c = false;

    public String getUid() {
        return this.f2986a;
    }

    public String getUids() {
        return this.f2987b;
    }

    public boolean isSearchByUids() {
        return this.f2988c;
    }

    public PoiDetailSearchOption poiUid(String str) {
        this.f2988c = false;
        this.f2986a = str;
        return this;
    }

    public PoiDetailSearchOption poiUids(String str) {
        this.f2988c = true;
        this.f2987b = str;
        return this;
    }
}
