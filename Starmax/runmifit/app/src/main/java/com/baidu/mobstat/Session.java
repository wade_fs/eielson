package com.baidu.mobstat;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Session {

    /* renamed from: a */
    private volatile long f4205a = 0;

    /* renamed from: b */
    private volatile long f4206b = 0;

    /* renamed from: c */
    private volatile long f4207c = 0;

    /* renamed from: d */
    private volatile long f4208d = 0;

    /* renamed from: e */
    private volatile long f4209e = 0;

    /* renamed from: f */
    private volatile int f4210f = 0;

    /* renamed from: g */
    private List<C1223a> f4211g = new ArrayList();

    public void reset() {
        this.f4205a = 0;
        this.f4206b = 0;
        this.f4207c = 0;
        this.f4208d = 0;
        this.f4210f = 0;
        this.f4211g.clear();
    }

    public void setTrackStartTime(long j) {
        if (this.f4207c <= 0) {
            this.f4207c = j;
        }
    }

    public void setTrackEndTime(long j) {
        this.f4208d = j;
    }

    public void setInvokeType(int i) {
        this.f4210f = i;
    }

    public void addPageView(String str, String str2, String str3, long j, long j2, boolean z, ExtraInfo extraInfo, boolean z2) {
        m4468a(this.f4211g, new C1223a(str, str2, str3, j, j2, z, extraInfo, z2));
    }

    public void addPageView(C1223a aVar) {
        m4468a(this.f4211g, aVar);
    }

    /* renamed from: a */
    private void m4468a(List<C1223a> list, C1223a aVar) {
        if (list != null && aVar != null) {
            int size = list.size();
            if (size == 0) {
                list.add(aVar);
                return;
            }
            C1223a aVar2 = list.get(size - 1);
            if (TextUtils.isEmpty(aVar2.f4212a) || TextUtils.isEmpty(aVar.f4212a)) {
                list.add(aVar);
            } else if (!aVar2.f4212a.equals(aVar.f4212a) || aVar2.f4217f == aVar.f4217f) {
                list.add(aVar);
            } else if (aVar2.f4217f) {
                aVar2.mo13855a(aVar);
            }
        }
    }

    public void setStartTime(long j) {
        if (this.f4205a <= 0) {
            this.f4205a = j;
            this.f4209e = j;
        }
    }

    public long getStartTime() {
        return this.f4205a;
    }

    public boolean hasStart() {
        return this.f4205a > 0;
    }

    public boolean hasEnd() {
        return this.f4206b > 0;
    }

    public void setEndTime(long j) {
        this.f4206b = j;
    }

    public JSONObject constructJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("s", this.f4205a);
            jSONObject.put("e", this.f4206b);
            jSONObject.put("i", this.f4209e);
            jSONObject.put("c", 1);
            jSONObject.put(Config.SESSTION_TRACK_START_TIME, this.f4207c == 0 ? this.f4205a : this.f4207c);
            jSONObject.put(Config.SESSTION_TRACK_END_TIME, this.f4208d == 0 ? this.f4206b : this.f4208d);
            jSONObject.put(Config.SESSTION_TRIGGER_CATEGORY, this.f4210f);
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < this.f4211g.size(); i++) {
                jSONArray.put(getPVJson(this.f4211g.get(i), this.f4205a));
            }
            jSONObject.put("p", jSONArray);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public JSONObject getPageSessionHead() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("s", this.f4205a);
            jSONObject.put("e", this.f4206b);
            jSONObject.put("i", this.f4209e);
            jSONObject.put("c", 1);
            jSONObject.put(Config.SESSTION_TRACK_START_TIME, this.f4207c == 0 ? this.f4205a : this.f4207c);
            jSONObject.put(Config.SESSTION_TRACK_END_TIME, this.f4208d == 0 ? this.f4206b : this.f4208d);
            jSONObject.put(Config.SESSTION_TRIGGER_CATEGORY, this.f4210f);
        } catch (Exception unused) {
        }
        return jSONObject;
    }

    public String toString() {
        return constructJSONObject().toString();
    }

    public static JSONObject getPVJson(C1223a aVar, long j) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("n", aVar.mo13854a());
            jSONObject.put("d", aVar.mo13857c());
            long d = aVar.mo13858d() - j;
            if (d < 0) {
                d = 0;
            }
            jSONObject.put("ps", d);
            jSONObject.put("t", aVar.mo13856b());
            int i = 1;
            jSONObject.put("at", aVar.mo13860f() ? 1 : 0);
            JSONObject e = aVar.mo13859e();
            if (!(e == null || e.length() == 0)) {
                jSONObject.put("ext", e);
            }
            if (!aVar.f4219h) {
                i = 0;
            }
            jSONObject.put("h5", i);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    /* renamed from: com.baidu.mobstat.Session$a */
    static class C1223a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public String f4212a;

        /* renamed from: b */
        private String f4213b;

        /* renamed from: c */
        private String f4214c;

        /* renamed from: d */
        private long f4215d;

        /* renamed from: e */
        private long f4216e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public boolean f4217f;

        /* renamed from: g */
        private JSONObject f4218g;
        /* access modifiers changed from: private */

        /* renamed from: h */
        public boolean f4219h;

        public C1223a(String str, String str2, String str3, long j, long j2, boolean z, ExtraInfo extraInfo, boolean z2) {
            this.f4213b = str;
            this.f4214c = str2;
            this.f4212a = str3;
            this.f4215d = j;
            this.f4216e = j2;
            this.f4217f = z;
            this.f4218g = extraInfo != null ? extraInfo.dumpToJson() : new JSONObject();
            this.f4219h = z2;
        }

        /* renamed from: a */
        public String mo13854a() {
            return this.f4213b;
        }

        /* renamed from: b */
        public String mo13856b() {
            return this.f4214c;
        }

        /* renamed from: c */
        public long mo13857c() {
            return this.f4215d;
        }

        /* renamed from: d */
        public long mo13858d() {
            return this.f4216e;
        }

        /* renamed from: e */
        public JSONObject mo13859e() {
            return this.f4218g;
        }

        /* renamed from: f */
        public boolean mo13860f() {
            return this.f4217f;
        }

        /* renamed from: a */
        public void mo13855a(C1223a aVar) {
            this.f4212a = aVar.f4212a;
            this.f4213b = aVar.f4213b;
            this.f4214c = aVar.f4214c;
            this.f4215d = aVar.f4215d;
            this.f4216e = aVar.f4216e;
            this.f4217f = aVar.f4217f;
            this.f4218g = aVar.f4218g;
            this.f4219h = aVar.f4219h;
        }
    }
}
