package com.baidu.mobstat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.os.SystemClock;
import android.provider.Settings;
import android.system.ErrnoException;
import android.system.Os;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.google.common.primitives.UnsignedBytes;
import com.tencent.mid.api.MidEntity;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.crypto.Cipher;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.g */
public final class C1283g {

    /* renamed from: a */
    private static final String f4346a;

    /* renamed from: e */
    private static C1286b f4347e;

    /* renamed from: b */
    private final Context f4348b;

    /* renamed from: c */
    private int f4349c = 0;

    /* renamed from: d */
    private PublicKey f4350d;

    /* renamed from: com.baidu.mobstat.g$a */
    static class C1285a {

        /* renamed from: a */
        public ApplicationInfo f4352a;

        /* renamed from: b */
        public int f4353b;

        /* renamed from: c */
        public boolean f4354c;

        /* renamed from: d */
        public boolean f4355d;

        private C1285a() {
            this.f4353b = 0;
            this.f4354c = false;
            this.f4355d = false;
        }
    }

    /* renamed from: com.baidu.mobstat.g$b */
    static class C1286b {

        /* renamed from: a */
        public String f4356a;

        /* renamed from: b */
        public String f4357b;

        /* renamed from: c */
        public int f4358c;

        private C1286b() {
            this.f4358c = 2;
        }

        /* renamed from: a */
        public static C1286b m4789a(String str) {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                String string = jSONObject.getString("deviceid");
                String string2 = jSONObject.getString(MidEntity.TAG_IMEI);
                int i = jSONObject.getInt("ver");
                if (!TextUtils.isEmpty(string) && string2 != null) {
                    C1286b bVar = new C1286b();
                    bVar.f4356a = string;
                    bVar.f4357b = string2;
                    bVar.f4358c = i;
                    return bVar;
                }
            } catch (JSONException e) {
                C1283g.m4776b(e);
            }
            return null;
        }

        /* renamed from: a */
        public String mo13968a() {
            try {
                return new JSONObject().put("deviceid", this.f4356a).put(MidEntity.TAG_IMEI, this.f4357b).put("ver", this.f4358c).toString();
            } catch (JSONException e) {
                C1283g.m4776b(e);
                return null;
            }
        }

        /* renamed from: b */
        public String mo13969b() {
            String str = this.f4357b;
            if (TextUtils.isEmpty(str)) {
                str = "0";
            }
            String stringBuffer = new StringBuffer(str).reverse().toString();
            return this.f4356a + "|" + stringBuffer;
        }
    }

    /* renamed from: com.baidu.mobstat.g$c */
    static class C1287c {
        /* renamed from: a */
        static boolean m4792a(String str, int i) {
            try {
                Os.chmod(str, i);
                return true;
            } catch (ErrnoException e) {
                C1283g.m4776b(e);
                return false;
            }
        }
    }

    static {
        String str = new String(C1266b.m4675a(new byte[]{77, 122, 65, 121, 77, 84, 73, 120, 77, 68, 73, 61}));
        String str2 = new String(C1266b.m4675a(new byte[]{90, 71, 108, 106, 100, 87, 82, 112, 89, 87, 73, 61}));
        f4346a = str + str2;
    }

    private C1283g(Context context) {
        this.f4348b = context.getApplicationContext();
        m4765a();
    }

    /* renamed from: a */
    public static String m4761a(Context context) {
        return m4777c(context).mo13969b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0033 A[SYNTHETIC, Splitter:B:23:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003f A[SYNTHETIC, Splitter:B:30:0x003f] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m4762a(java.io.File r5) {
        /*
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            r5 = 8192(0x2000, float:1.14794E-41)
            char[] r5 = new char[r5]     // Catch:{ Exception -> 0x0027 }
            java.io.CharArrayWriter r2 = new java.io.CharArrayWriter     // Catch:{ Exception -> 0x0027 }
            r2.<init>()     // Catch:{ Exception -> 0x0027 }
        L_0x000f:
            int r3 = r1.read(r5)     // Catch:{ Exception -> 0x0027 }
            if (r3 <= 0) goto L_0x001a
            r4 = 0
            r2.write(r5, r4, r3)     // Catch:{ Exception -> 0x0027 }
            goto L_0x000f
        L_0x001a:
            java.lang.String r5 = r2.toString()     // Catch:{ Exception -> 0x0027 }
            r1.close()     // Catch:{ Exception -> 0x0022 }
            goto L_0x0026
        L_0x0022:
            r0 = move-exception
            m4776b(r0)
        L_0x0026:
            return r5
        L_0x0027:
            r5 = move-exception
            goto L_0x002e
        L_0x0029:
            r5 = move-exception
            r1 = r0
            goto L_0x003d
        L_0x002c:
            r5 = move-exception
            r1 = r0
        L_0x002e:
            m4776b(r5)     // Catch:{ all -> 0x003c }
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ Exception -> 0x0037 }
            goto L_0x003b
        L_0x0037:
            r5 = move-exception
            m4776b(r5)
        L_0x003b:
            return r0
        L_0x003c:
            r5 = move-exception
        L_0x003d:
            if (r1 == 0) goto L_0x0047
            r1.close()     // Catch:{ Exception -> 0x0043 }
            goto L_0x0047
        L_0x0043:
            r0 = move-exception
            m4776b(r0)
        L_0x0047:
            goto L_0x0049
        L_0x0048:
            throw r5
        L_0x0049:
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1283g.m4762a(java.io.File):java.lang.String");
    }

    /* renamed from: a */
    private static String m4763a(byte[] bArr) {
        StringBuilder sb;
        if (bArr != null) {
            String str = "";
            for (byte b : bArr) {
                String hexString = Integer.toHexString(b & UnsignedBytes.MAX_VALUE);
                if (hexString.length() == 1) {
                    sb = new StringBuilder();
                    sb.append(str);
                    str = "0";
                } else {
                    sb = new StringBuilder();
                }
                sb.append(str);
                sb.append(hexString);
                str = sb.toString();
            }
            return str.toLowerCase();
        }
        throw new IllegalArgumentException("Argument b ( byte array ) is null! ");
    }

    /* renamed from: a */
    private List<C1285a> m4764a(Intent intent, boolean z) {
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = this.f4348b.getPackageManager();
        List<ResolveInfo> queryBroadcastReceivers = packageManager.queryBroadcastReceivers(intent, 0);
        if (queryBroadcastReceivers != null) {
            for (ResolveInfo resolveInfo : queryBroadcastReceivers) {
                if (!(resolveInfo.activityInfo == null || resolveInfo.activityInfo.applicationInfo == null)) {
                    try {
                        Bundle bundle = packageManager.getReceiverInfo(new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name), 128).metaData;
                        if (bundle != null) {
                            String string = bundle.getString("galaxy_data");
                            if (!TextUtils.isEmpty(string)) {
                                byte[] a = C1266b.m4675a(string.getBytes("utf-8"));
                                JSONObject jSONObject = new JSONObject(new String(a));
                                C1285a aVar = new C1285a();
                                aVar.f4353b = jSONObject.getInt("priority");
                                aVar.f4352a = resolveInfo.activityInfo.applicationInfo;
                                if (this.f4348b.getPackageName().equals(resolveInfo.activityInfo.applicationInfo.packageName)) {
                                    aVar.f4355d = true;
                                }
                                if (z) {
                                    String string2 = bundle.getString("galaxy_sf");
                                    if (!TextUtils.isEmpty(string2)) {
                                        PackageInfo packageInfo = packageManager.getPackageInfo(resolveInfo.activityInfo.applicationInfo.packageName, 64);
                                        JSONArray jSONArray = jSONObject.getJSONArray("sigs");
                                        String[] strArr = new String[jSONArray.length()];
                                        for (int i = 0; i < strArr.length; i++) {
                                            strArr[i] = jSONArray.getString(i);
                                        }
                                        if (m4769a(strArr, m4771a(packageInfo.signatures))) {
                                            byte[] a2 = m4770a(C1266b.m4675a(string2.getBytes()), this.f4350d);
                                            if (a2 != null && Arrays.equals(a2, C1280d.m4758a(a))) {
                                                aVar.f4354c = true;
                                            }
                                        }
                                    }
                                }
                                arrayList.add(aVar);
                            }
                        }
                    } catch (Exception unused) {
                    }
                }
            }
        }
        Collections.sort(arrayList, new Comparator<C1285a>() {
            /* class com.baidu.mobstat.C1283g.C12841 */

            /* renamed from: a */
            public int compare(C1285a aVar, C1285a aVar2) {
                int i = aVar2.f4353b - aVar.f4353b;
                if (i == 0) {
                    if (aVar.f4355d && aVar2.f4355d) {
                        return 0;
                    }
                    if (aVar.f4355d) {
                        return -1;
                    }
                    if (aVar2.f4355d) {
                        return 1;
                    }
                }
                return i;
            }
        });
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0028 A[SYNTHETIC, Splitter:B:13:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034 A[SYNTHETIC, Splitter:B:21:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m4765a() {
        /*
            r4 = this;
            r0 = 0
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0031, all -> 0x0022 }
            byte[] r2 = com.baidu.mobstat.C1282f.m4760a()     // Catch:{ Exception -> 0x0031, all -> 0x0022 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0031, all -> 0x0022 }
            java.lang.String r0 = "X.509"
            java.security.cert.CertificateFactory r0 = java.security.cert.CertificateFactory.getInstance(r0)     // Catch:{ Exception -> 0x0020, all -> 0x001e }
            java.security.cert.Certificate r0 = r0.generateCertificate(r1)     // Catch:{ Exception -> 0x0020, all -> 0x001e }
            java.security.PublicKey r0 = r0.getPublicKey()     // Catch:{ Exception -> 0x0020, all -> 0x001e }
            r4.f4350d = r0     // Catch:{ Exception -> 0x0020, all -> 0x001e }
            r1.close()     // Catch:{ Exception -> 0x0038 }
            goto L_0x003c
        L_0x001e:
            r0 = move-exception
            goto L_0x0026
        L_0x0020:
            goto L_0x0032
        L_0x0022:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0026:
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ Exception -> 0x002c }
            goto L_0x0030
        L_0x002c:
            r1 = move-exception
            m4776b(r1)
        L_0x0030:
            throw r0
        L_0x0031:
            r1 = r0
        L_0x0032:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ Exception -> 0x0038 }
            goto L_0x003c
        L_0x0038:
            r0 = move-exception
            m4776b(r0)
        L_0x003c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1283g.m4765a():void");
    }

    /* renamed from: a */
    private boolean m4767a(String str) {
        int i = Build.VERSION.SDK_INT >= 24 ? 0 : 1;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = this.f4348b.openFileOutput("libcuid.so", i);
            fileOutputStream.write(str.getBytes());
            fileOutputStream.flush();
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e) {
                    m4776b(e);
                }
            }
            if (i == 0) {
                return C1287c.m4792a(new File(this.f4348b.getFilesDir(), "libcuid.so").getAbsolutePath(), 436);
            }
            return true;
        } catch (Exception e2) {
            m4776b(e2);
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e3) {
                    m4776b(e3);
                }
            }
            return false;
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e4) {
                    m4776b(e4);
                }
            }
            throw th;
        }
    }

    /* renamed from: a */
    private boolean m4768a(String str, String str2) {
        try {
            return Settings.System.putString(this.f4348b.getContentResolver(), str, str2);
        } catch (Exception e) {
            m4776b(e);
            return false;
        }
    }

    /* renamed from: a */
    private boolean m4769a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length != strArr2.length) {
            return false;
        }
        HashSet hashSet = new HashSet();
        for (String str : strArr) {
            hashSet.add(str);
        }
        HashSet hashSet2 = new HashSet();
        for (String str2 : strArr2) {
            hashSet2.add(str2);
        }
        return hashSet.equals(hashSet2);
    }

    /* renamed from: a */
    private static byte[] m4770a(byte[] bArr, PublicKey publicKey) throws Exception {
        Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        instance.init(2, publicKey);
        return instance.doFinal(bArr);
    }

    /* renamed from: a */
    private String[] m4771a(Signature[] signatureArr) {
        String[] strArr = new String[signatureArr.length];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = m4763a(C1280d.m4758a(signatureArr[i].toByteArray()));
        }
        return strArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.g.a(android.content.Intent, boolean):java.util.List<com.baidu.mobstat.g$a>
     arg types: [android.content.Intent, int]
     candidates:
      com.baidu.mobstat.g.a(java.lang.String, java.lang.String):boolean
      com.baidu.mobstat.g.a(java.lang.String[], java.lang.String[]):boolean
      com.baidu.mobstat.g.a(byte[], java.security.PublicKey):byte[]
      com.baidu.mobstat.g.a(android.content.Intent, boolean):java.util.List<com.baidu.mobstat.g$a> */
    /* renamed from: b */
    private C1286b m4772b() {
        boolean z;
        String str;
        String str2;
        List<C1285a> a = m4764a(new Intent("com.baidu.intent.action.GALAXY").setPackage(this.f4348b.getPackageName()), true);
        boolean z2 = false;
        if (a == null || a.size() == 0) {
            for (int i = 0; i < 3; i++) {
                Log.w("DeviceId", "galaxy lib host missing meta-data,make sure you know the right way to integrate galaxy");
            }
            z = false;
        } else {
            C1285a aVar = a.get(0);
            z = aVar.f4354c;
            if (!aVar.f4354c) {
                for (int i2 = 0; i2 < 3; i2++) {
                    Log.w("DeviceId", "galaxy config err, In the release version of the signature should be matched");
                }
            }
        }
        File file = new File(this.f4348b.getFilesDir(), "libcuid.so");
        String str3 = null;
        C1286b a2 = file.exists() ? C1286b.m4789a(m4784f(m4762a(file))) : null;
        if (a2 == null) {
            this.f4349c |= 16;
            List<C1285a> a3 = m4764a(new Intent("com.baidu.intent.action.GALAXY"), z);
            if (a3 != null) {
                String str4 = "files";
                File filesDir = this.f4348b.getFilesDir();
                if (!str4.equals(filesDir.getName())) {
                    Log.e("DeviceId", "fetal error:: app files dir name is unexpectedly :: " + filesDir.getAbsolutePath());
                    str4 = filesDir.getName();
                }
                for (C1285a aVar2 : a3) {
                    if (!aVar2.f4355d) {
                        File file2 = new File(new File(aVar2.f4352a.dataDir, str4), "libcuid.so");
                        if (file2.exists() && (a2 = C1286b.m4789a(m4784f(m4762a(file2)))) != null) {
                            break;
                        }
                    }
                }
            }
        }
        if (a2 == null) {
            a2 = C1286b.m4789a(m4784f(m4774b("com.baidu.deviceid.v2")));
        }
        boolean c = m4779c("android.permission.READ_EXTERNAL_STORAGE");
        if (a2 == null && c) {
            this.f4349c |= 2;
            a2 = m4782e();
        }
        if (a2 == null) {
            this.f4349c |= 8;
            a2 = m4780d();
        }
        if (a2 != null || !c) {
            str = null;
        } else {
            this.f4349c |= 1;
            String h = m4786h("");
            z2 = true;
            str = h;
            a2 = m4781d(h);
        }
        if (a2 == null) {
            this.f4349c |= 4;
            if (!z2) {
                str = m4786h("");
            }
            a2 = new C1286b();
            String b = m4773b(this.f4348b);
            if (Build.VERSION.SDK_INT < 23) {
                str2 = str + b + UUID.randomUUID().toString();
            } else {
                str2 = "com.baidu" + b;
            }
            a2.f4356a = C1279c.m4757a(str2.getBytes(), true);
            a2.f4357b = str;
        }
        File file3 = new File(this.f4348b.getFilesDir(), "libcuid.so");
        if ((this.f4349c & 16) != 0 || !file3.exists()) {
            if (TextUtils.isEmpty(null)) {
                str3 = m4783e(a2.mo13968a());
            }
            m4767a(str3);
        }
        boolean c2 = m4778c();
        if (c2 && ((this.f4349c & 2) != 0 || TextUtils.isEmpty(m4774b("com.baidu.deviceid.v2")))) {
            if (TextUtils.isEmpty(str3)) {
                str3 = m4783e(a2.mo13968a());
            }
            m4768a("com.baidu.deviceid.v2", str3);
        }
        if (m4779c("android.permission.WRITE_EXTERNAL_STORAGE")) {
            File file4 = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig/.cuid2");
            if ((this.f4349c & 8) != 0 || !file4.exists()) {
                if (TextUtils.isEmpty(str3)) {
                    str3 = m4783e(a2.mo13968a());
                }
                m4785g(str3);
            }
        }
        if (c2 && ((this.f4349c & 1) != 0 || TextUtils.isEmpty(m4774b("com.baidu.deviceid")))) {
            m4768a("com.baidu.deviceid", a2.f4356a);
            m4768a("bd_setting_i", a2.f4357b);
        }
        if (c2 && !TextUtils.isEmpty(a2.f4357b)) {
            File file5 = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig/.cuid");
            if ((this.f4349c & 2) != 0 || !file5.exists()) {
                m4775b(a2.f4357b, a2.f4356a);
            }
        }
        return a2;
    }

    /* renamed from: b */
    public static String m4773b(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        return TextUtils.isEmpty(string) ? "" : string;
    }

    /* renamed from: b */
    private String m4774b(String str) {
        try {
            return Settings.System.getString(this.f4348b.getContentResolver(), str);
        } catch (Exception e) {
            m4776b(e);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* renamed from: b */
    private static void m4775b(String str, String str2) {
        File file;
        if (!TextUtils.isEmpty(str)) {
            File file2 = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig");
            File file3 = new File(file2, ".cuid");
            try {
                if (file2.exists() && !file2.isDirectory()) {
                    Random random = new Random();
                    File parentFile = file2.getParentFile();
                    String name = file2.getName();
                    do {
                        file = new File(parentFile, name + random.nextInt() + ".tmp");
                    } while (file.exists());
                    file2.renameTo(file);
                    file.delete();
                }
                file2.mkdirs();
                FileWriter fileWriter = new FileWriter(file3, false);
                fileWriter.write(C1266b.m4674a(C1225a.m4495a(f4346a, f4346a, (str + "=" + str2).getBytes()), "utf-8"));
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException | Exception unused) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m4776b(Throwable th) {
    }

    /* renamed from: c */
    private static C1286b m4777c(Context context) {
        if (f4347e == null) {
            synchronized (C1286b.class) {
                if (f4347e == null) {
                    SystemClock.uptimeMillis();
                    f4347e = new C1283g(context).m4772b();
                    SystemClock.uptimeMillis();
                }
            }
        }
        return f4347e;
    }

    /* renamed from: c */
    private boolean m4778c() {
        return m4779c("android.permission.WRITE_SETTINGS");
    }

    /* renamed from: c */
    private boolean m4779c(String str) {
        return this.f4348b.checkPermission(str, Process.myPid(), Process.myUid()) == 0;
    }

    /* renamed from: d */
    private C1286b m4780d() {
        String b = m4774b("com.baidu.deviceid");
        String b2 = m4774b("bd_setting_i");
        if (TextUtils.isEmpty(b2)) {
            b2 = m4786h("");
            if (!TextUtils.isEmpty(b2)) {
                m4768a("bd_setting_i", b2);
            }
        }
        if (TextUtils.isEmpty(b)) {
            String b3 = m4773b(this.f4348b);
            b = m4774b(C1279c.m4757a(("com.baidu" + b2 + b3).getBytes(), true));
        }
        if (TextUtils.isEmpty(b)) {
            return null;
        }
        C1286b bVar = new C1286b();
        bVar.f4356a = b;
        bVar.f4357b = b2;
        return bVar;
    }

    /* renamed from: d */
    private C1286b m4781d(String str) {
        boolean z;
        String str2;
        boolean z2 = Build.VERSION.SDK_INT < 23;
        if (z2 && TextUtils.isEmpty(str)) {
            return null;
        }
        String str3 = "";
        File file = new File(Environment.getExternalStorageDirectory(), "baidu/.cuid");
        if (file.exists()) {
            z = false;
        } else {
            file = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig/.cuid");
            z = true;
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append("\r\n");
            }
            bufferedReader.close();
            String[] split = new String(C1225a.m4496b(f4346a, f4346a, C1266b.m4675a(sb.toString().getBytes()))).split("=");
            if (split != null && split.length == 2) {
                if (z2 && str.equals(split[0])) {
                    str2 = split[1];
                } else if (!z2) {
                    if (TextUtils.isEmpty(str)) {
                        str = split[1];
                    }
                    str2 = split[1];
                }
                str3 = str2;
            }
            if (!z) {
                m4775b(str, str3);
            }
        } catch (FileNotFoundException | IOException | Exception unused) {
        }
        if (TextUtils.isEmpty(str3)) {
            return null;
        }
        C1286b bVar = new C1286b();
        bVar.f4356a = str3;
        bVar.f4357b = str;
        return bVar;
    }

    /* renamed from: e */
    private C1286b m4782e() {
        File file = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig/.cuid2");
        if (!file.exists()) {
            return null;
        }
        String a = m4762a(file);
        if (TextUtils.isEmpty(a)) {
            return null;
        }
        try {
            return C1286b.m4789a(new String(C1225a.m4496b(f4346a, f4346a, C1266b.m4675a(a.getBytes()))));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: e */
    private static String m4783e(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return C1266b.m4674a(C1225a.m4495a(f4346a, f4346a, str.getBytes()), "utf-8");
        } catch (UnsupportedEncodingException | Exception e) {
            m4776b(e);
            return "";
        }
    }

    /* renamed from: f */
    private static String m4784f(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return new String(C1225a.m4496b(f4346a, f4346a, C1266b.m4675a(str.getBytes())));
        } catch (Exception e) {
            m4776b(e);
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* renamed from: g */
    private static void m4785g(String str) {
        File file;
        File file2 = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig");
        File file3 = new File(file2, ".cuid2");
        try {
            if (file2.exists() && !file2.isDirectory()) {
                Random random = new Random();
                File parentFile = file2.getParentFile();
                String name = file2.getName();
                do {
                    file = new File(parentFile, name + random.nextInt() + ".tmp");
                } while (file.exists());
                file2.renameTo(file);
                file.delete();
            }
            file2.mkdirs();
            FileWriter fileWriter = new FileWriter(file3, false);
            fileWriter.write(str);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException | Exception unused) {
        }
    }

    /* renamed from: h */
    private String m4786h(String str) {
        String str2 = null;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.f4348b.getSystemService("phone");
            if (telephonyManager != null) {
                str2 = telephonyManager.getDeviceId();
            }
        } catch (Exception e) {
            Log.e("DeviceId", "Read IMEI failed", e);
        }
        String i = m4787i(str2);
        return TextUtils.isEmpty(i) ? str : i;
    }

    /* renamed from: i */
    private static String m4787i(String str) {
        return (str == null || !str.contains(Config.TRACE_TODAY_VISIT_SPLIT)) ? str : "";
    }
}
