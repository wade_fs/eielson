package com.baidu.platform.core.p034d;

import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.route.MassTransitRoutePlanOption;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;
import com.tamic.novate.download.MimeType;

/* renamed from: com.baidu.platform.core.d.i */
public class C1357i extends C1320e {
    public C1357i(MassTransitRoutePlanOption massTransitRoutePlanOption) {
        m5070a(massTransitRoutePlanOption);
    }

    /* renamed from: a */
    private void m5070a(MassTransitRoutePlanOption massTransitRoutePlanOption) {
        LatLng location = massTransitRoutePlanOption.mFrom.getLocation();
        if (location != null) {
            if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                location = CoordTrans.gcjToBaidu(location);
            }
            C1381a aVar = this.f4435a;
            aVar.mo14094a("origin", location.latitude + "," + location.longitude);
        } else {
            this.f4435a.mo14094a("origin", massTransitRoutePlanOption.mFrom.getName());
        }
        if (massTransitRoutePlanOption.mFrom.getCity() != null) {
            this.f4435a.mo14094a("origin_region", massTransitRoutePlanOption.mFrom.getCity());
        }
        LatLng location2 = massTransitRoutePlanOption.mTo.getLocation();
        if (location2 != null) {
            if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                location2 = CoordTrans.gcjToBaidu(location2);
            }
            C1381a aVar2 = this.f4435a;
            aVar2.mo14094a("destination", location2.latitude + "," + location2.longitude);
        } else {
            this.f4435a.mo14094a("destination", massTransitRoutePlanOption.mTo.getName());
        }
        if (massTransitRoutePlanOption.mTo.getCity() != null) {
            this.f4435a.mo14094a("destination_region", massTransitRoutePlanOption.mTo.getCity());
        }
        C1381a aVar3 = this.f4435a;
        aVar3.mo14094a("tactics_incity", massTransitRoutePlanOption.mTacticsIncity.getInt() + "");
        C1381a aVar4 = this.f4435a;
        aVar4.mo14094a("tactics_intercity", massTransitRoutePlanOption.mTacticsIntercity.getInt() + "");
        C1381a aVar5 = this.f4435a;
        aVar5.mo14094a("trans_type_intercity", massTransitRoutePlanOption.mTransTypeIntercity.getInt() + "");
        C1381a aVar6 = this.f4435a;
        aVar6.mo14094a("page_index", massTransitRoutePlanOption.mPageIndex + "");
        C1381a aVar7 = this.f4435a;
        aVar7.mo14094a("page_size", massTransitRoutePlanOption.mPageSize + "");
        this.f4435a.mo14094a("coord_type", massTransitRoutePlanOption.mCoordType);
        this.f4435a.mo14094a("output", MimeType.JSON);
        this.f4435a.mo14094a("from", "android_map_sdk");
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14082g();
    }
}
