package com.baidu.location.p013a;

import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.baidu.location.Address;
import com.baidu.location.BDLocation;
import com.baidu.location.C0839f;
import com.baidu.location.Poi;
import com.baidu.location.indoor.C0865g;
import com.baidu.location.p013a.C0738i;
import com.baidu.location.p014b.C0772b;
import com.baidu.location.p016d.C0790a;
import com.baidu.location.p016d.C0804h;
import com.baidu.location.p017e.C0821a;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0826e;
import com.baidu.location.p017e.C0834h;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0843a;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import java.util.List;

/* renamed from: com.baidu.location.a.l */
public class C0750l extends C0738i {

    /* renamed from: g */
    public static boolean f1367g = false;

    /* renamed from: h */
    private static C0750l f1368h;

    /* renamed from: A */
    private boolean f1369A;

    /* renamed from: B */
    private long f1370B;

    /* renamed from: C */
    private long f1371C;

    /* renamed from: D */
    private C0751a f1372D;
    /* access modifiers changed from: private */

    /* renamed from: E */
    public boolean f1373E;
    /* access modifiers changed from: private */

    /* renamed from: F */
    public boolean f1374F;

    /* renamed from: G */
    private boolean f1375G;

    /* renamed from: H */
    private boolean f1376H;

    /* renamed from: I */
    private boolean f1377I;

    /* renamed from: J */
    private C0752b f1378J;
    /* access modifiers changed from: private */

    /* renamed from: K */
    public boolean f1379K;

    /* renamed from: L */
    private int f1380L;

    /* renamed from: M */
    private long f1381M;

    /* renamed from: N */
    private boolean f1382N;

    /* renamed from: O */
    private boolean f1383O;

    /* renamed from: e */
    public C0738i.C0740b f1384e;

    /* renamed from: f */
    public final Handler f1385f;

    /* renamed from: i */
    private boolean f1386i;

    /* renamed from: j */
    private String f1387j;

    /* renamed from: k */
    private BDLocation f1388k;

    /* renamed from: l */
    private BDLocation f1389l;

    /* renamed from: m */
    private C0834h f1390m;

    /* renamed from: n */
    private C0821a f1391n;

    /* renamed from: o */
    private C0834h f1392o;

    /* renamed from: p */
    private C0821a f1393p;

    /* renamed from: q */
    private boolean f1394q;

    /* renamed from: r */
    private volatile boolean f1395r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public boolean f1396s;

    /* renamed from: t */
    private long f1397t;

    /* renamed from: u */
    private long f1398u;

    /* renamed from: v */
    private Address f1399v;

    /* renamed from: w */
    private String f1400w;

    /* renamed from: x */
    private List<Poi> f1401x;

    /* renamed from: y */
    private double f1402y;

    /* renamed from: z */
    private double f1403z;

    /* renamed from: com.baidu.location.a.l$a */
    private class C0751a implements Runnable {
        private C0751a() {
        }

        /* synthetic */ C0751a(C0750l lVar, C0753m mVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.l.a(boolean, boolean):void
         arg types: [int, int]
         candidates:
          com.baidu.location.a.l.a(com.baidu.location.a.l, android.os.Message):void
          com.baidu.location.a.l.a(com.baidu.location.a.l, boolean):boolean
          com.baidu.location.a.l.a(boolean, boolean):void */
        public void run() {
            if (C0750l.this.f1373E) {
                boolean unused = C0750l.this.f1373E = false;
                if (!C0750l.this.f1374F && !C0826e.m2306a().mo10647j()) {
                    C0750l.this.mo10477a(false, false);
                }
            }
        }
    }

    /* renamed from: com.baidu.location.a.l$b */
    private class C0752b implements Runnable {
        private C0752b() {
        }

        /* synthetic */ C0752b(C0750l lVar, C0753m mVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.l.a(com.baidu.location.a.l, boolean):boolean
         arg types: [com.baidu.location.a.l, int]
         candidates:
          com.baidu.location.a.l.a(com.baidu.location.a.l, android.os.Message):void
          com.baidu.location.a.l.a(boolean, boolean):void
          com.baidu.location.a.l.a(com.baidu.location.a.l, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.l.b(com.baidu.location.a.l, boolean):boolean
         arg types: [com.baidu.location.a.l, int]
         candidates:
          com.baidu.location.a.l.b(com.baidu.location.a.l, android.os.Message):void
          com.baidu.location.a.l.b(com.baidu.location.a.l, boolean):boolean */
        public void run() {
            if (C0750l.this.f1379K) {
                boolean unused = C0750l.this.f1379K = false;
            }
            if (C0750l.this.f1396s) {
                boolean unused2 = C0750l.this.f1396s = false;
                C0750l.this.m1892h((Message) null);
            }
        }
    }

    private C0750l() {
        this.f1386i = true;
        this.f1384e = null;
        this.f1387j = null;
        this.f1388k = null;
        this.f1389l = null;
        this.f1390m = null;
        this.f1391n = null;
        this.f1392o = null;
        this.f1393p = null;
        this.f1394q = true;
        this.f1395r = false;
        this.f1396s = false;
        this.f1397t = 0;
        this.f1398u = 0;
        this.f1399v = null;
        this.f1400w = null;
        this.f1401x = null;
        this.f1369A = false;
        this.f1370B = 0;
        this.f1371C = 0;
        this.f1372D = null;
        this.f1373E = false;
        this.f1374F = false;
        this.f1375G = true;
        this.f1385f = new C0738i.C0739a();
        this.f1376H = false;
        this.f1377I = false;
        this.f1378J = null;
        this.f1379K = false;
        this.f1380L = 0;
        this.f1381M = 0;
        this.f1382N = false;
        this.f1383O = true;
        this.f1384e = new C0738i.C0740b();
    }

    /* renamed from: a */
    private boolean m1877a(C0821a aVar) {
        this.f1327b = C0822b.m2279a().mo10631f();
        if (this.f1327b == aVar) {
            return false;
        }
        if (this.f1327b == null || aVar == null) {
            return true;
        }
        return !aVar.mo10616a(this.f1327b);
    }

    /* renamed from: a */
    private boolean m1878a(C0834h hVar) {
        this.f1326a = C0835i.m2376a().mo10699p();
        if (hVar == this.f1326a) {
            return false;
        }
        if (this.f1326a == null || hVar == null) {
            return true;
        }
        return !hVar.mo10674c(this.f1326a);
    }

    /* renamed from: b */
    private boolean m1882b(C0821a aVar) {
        if (aVar == null) {
            return false;
        }
        C0821a aVar2 = this.f1393p;
        if (aVar2 == null) {
            return true;
        }
        return !aVar.mo10616a(aVar2);
    }

    /* renamed from: c */
    public static synchronized C0750l m1883c() {
        C0750l lVar;
        synchronized (C0750l.class) {
            if (f1368h == null) {
                f1368h = new C0750l();
            }
            lVar = f1368h;
        }
        return lVar;
    }

    /* renamed from: c */
    private void m1884c(Message message) {
        if (C0855k.m2460b()) {
            Log.d(C0843a.f1826a, "isInforbiddenTime on request location ...");
        }
        if (message.getData().getBoolean("isWaitingLocTag", false)) {
            f1367g = true;
        }
        if (!C0865g.m2519a().mo10769f()) {
            int d = C0723a.m1729a().mo10431d(message);
            if (d == 1) {
                m1887d(message);
            } else if (d == 2) {
                m1891g(message);
            } else if (d != 3) {
                throw new IllegalArgumentException(String.format("this type %d is illegal", Integer.valueOf(d)));
            } else if (C0826e.m2306a().mo10647j()) {
                m1889e(message);
            }
        }
    }

    /* renamed from: d */
    private void m1887d(Message message) {
        if (C0826e.m2306a().mo10647j()) {
            m1889e(message);
            C0754n.m1911a().mo10494c();
            return;
        }
        m1891g(message);
        C0754n.m1911a().mo10492b();
    }

    /* renamed from: e */
    private void m1889e(Message message) {
        BDLocation bDLocation = new BDLocation(C0826e.m2306a().mo10644g());
        if (C0855k.f1963g.equals("all") || C0855k.f1964h || C0855k.f1966j) {
            float[] fArr = new float[2];
            Location.distanceBetween(this.f1403z, this.f1402y, bDLocation.getLatitude(), bDLocation.getLongitude(), fArr);
            if (fArr[0] < 100.0f) {
                Address address = this.f1399v;
                if (address != null) {
                    bDLocation.setAddr(address);
                }
                String str = this.f1400w;
                if (str != null) {
                    bDLocation.setLocationDescribe(str);
                }
                List<Poi> list = this.f1401x;
                if (list != null) {
                    bDLocation.setPoiList(list);
                }
            } else {
                this.f1369A = true;
                m1891g(null);
            }
        }
        this.f1388k = bDLocation;
        this.f1389l = null;
        C0723a.m1729a().mo10422a(bDLocation);
    }

    /* renamed from: f */
    private void m1890f(Message message) {
        C0752b bVar;
        if (C0835i.m2376a().mo10691g()) {
            this.f1396s = true;
            if (this.f1378J == null) {
                this.f1378J = new C0752b(this, null);
            }
            if (this.f1379K && (bVar = this.f1378J) != null) {
                this.f1385f.removeCallbacks(bVar);
            }
            this.f1385f.postDelayed(this.f1378J, 3500);
            this.f1379K = true;
            return;
        }
        m1892h(message);
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m1891g(Message message) {
        this.f1380L = 0;
        if (this.f1394q) {
            this.f1380L = 1;
            this.f1371C = SystemClock.uptimeMillis();
            if (C0835i.m2376a().mo10694k()) {
                m1890f(message);
            } else {
                m1892h(message);
            }
        } else {
            m1890f(message);
            this.f1371C = SystemClock.uptimeMillis();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009e, code lost:
        if (r6 <= 0) goto L_0x00a0;
     */
    /* renamed from: h */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m1892h(android.os.Message r14) {
        /*
            r13 = this;
            long r0 = java.lang.System.currentTimeMillis()
            long r2 = r13.f1397t
            long r0 = r0 - r2
            boolean r2 = r13.f1395r
            if (r2 == 0) goto L_0x0012
            r2 = 12000(0x2ee0, double:5.929E-320)
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 > 0) goto L_0x0012
            return
        L_0x0012:
            long r0 = java.lang.System.currentTimeMillis()
            long r2 = r13.f1397t
            long r0 = r0 - r2
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x003d
            long r0 = java.lang.System.currentTimeMillis()
            long r4 = r13.f1397t
            long r0 = r0 - r4
            r4 = 1000(0x3e8, double:4.94E-321)
            int r6 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r6 >= 0) goto L_0x003d
            com.baidu.location.BDLocation r14 = r13.f1388k
            if (r14 == 0) goto L_0x0039
            com.baidu.location.a.a r14 = com.baidu.location.p013a.C0723a.m1729a()
            com.baidu.location.BDLocation r0 = r13.f1388k
            r14.mo10422a(r0)
        L_0x0039:
            r13.m1895m()
            return
        L_0x003d:
            r0 = 1
            r13.f1395r = r0
            com.baidu.location.e.a r1 = r13.f1391n
            boolean r1 = r13.m1877a(r1)
            r13.f1386i = r1
            com.baidu.location.e.h r1 = r13.f1390m
            boolean r1 = r13.m1878a(r1)
            r4 = 0
            if (r1 != 0) goto L_0x00d0
            boolean r1 = r13.f1386i
            if (r1 != 0) goto L_0x00d0
            com.baidu.location.BDLocation r1 = r13.f1388k
            if (r1 == 0) goto L_0x00d0
            boolean r1 = r13.f1369A
            if (r1 != 0) goto L_0x00d0
            com.baidu.location.BDLocation r1 = r13.f1389l
            if (r1 == 0) goto L_0x0074
            long r5 = java.lang.System.currentTimeMillis()
            long r7 = r13.f1398u
            long r5 = r5 - r7
            r7 = 30000(0x7530, double:1.4822E-319)
            int r1 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r1 <= 0) goto L_0x0074
            com.baidu.location.BDLocation r1 = r13.f1389l
            r13.f1388k = r1
            r13.f1389l = r4
        L_0x0074:
            com.baidu.location.a.n r1 = com.baidu.location.p013a.C0754n.m1911a()
            boolean r1 = r1.mo10495d()
            if (r1 == 0) goto L_0x008b
            com.baidu.location.BDLocation r1 = r13.f1388k
            com.baidu.location.a.n r5 = com.baidu.location.p013a.C0754n.m1911a()
            float r5 = r5.mo10496e()
            r1.setDirection(r5)
        L_0x008b:
            com.baidu.location.BDLocation r1 = r13.f1388k
            int r1 = r1.getLocType()
            r5 = 62
            if (r1 != r5) goto L_0x00a0
            long r6 = java.lang.System.currentTimeMillis()
            long r8 = r13.f1381M
            long r6 = r6 - r8
            int r1 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r1 > 0) goto L_0x00a1
        L_0x00a0:
            r6 = r2
        L_0x00a1:
            com.baidu.location.BDLocation r1 = r13.f1388k
            int r1 = r1.getLocType()
            r8 = 61
            if (r1 == r8) goto L_0x00c3
            com.baidu.location.BDLocation r1 = r13.f1388k
            int r1 = r1.getLocType()
            r8 = 161(0xa1, float:2.26E-43)
            if (r1 == r8) goto L_0x00c3
            com.baidu.location.BDLocation r1 = r13.f1388k
            int r1 = r1.getLocType()
            if (r1 != r5) goto L_0x00d0
            r8 = 15000(0x3a98, double:7.411E-320)
            int r1 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r1 >= 0) goto L_0x00d0
        L_0x00c3:
            com.baidu.location.a.a r14 = com.baidu.location.p013a.C0723a.m1729a()
            com.baidu.location.BDLocation r0 = r13.f1388k
            r14.mo10422a(r0)
            r13.m1895m()
            return
        L_0x00d0:
            long r5 = java.lang.System.currentTimeMillis()
            r13.f1397t = r5
            java.lang.String r1 = r13.mo10460a(r4)
            r5 = 0
            r13.f1377I = r5
            if (r1 != 0) goto L_0x0173
            r13.f1377I = r0
            long r6 = java.lang.System.currentTimeMillis()
            r13.f1381M = r6
            java.lang.String[] r1 = r13.m1894l()
            long r6 = java.lang.System.currentTimeMillis()
            long r8 = r13.f1370B
            long r8 = r6 - r8
            r10 = 60000(0xea60, double:2.9644E-319)
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 <= 0) goto L_0x00fc
            r13.f1370B = r6
        L_0x00fc:
            com.baidu.location.e.i r6 = com.baidu.location.p017e.C0835i.m2376a()
            java.lang.String r6 = r6.mo10696m()
            if (r6 == 0) goto L_0x011f
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r6)
            java.lang.String r6 = r13.mo10463b()
            r7.append(r6)
            r1 = r1[r5]
            r7.append(r1)
            java.lang.String r1 = r7.toString()
            goto L_0x0139
        L_0x011f:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = ""
            r6.append(r7)
            java.lang.String r7 = r13.mo10463b()
            r6.append(r7)
            r1 = r1[r5]
            r6.append(r1)
            java.lang.String r1 = r6.toString()
        L_0x0139:
            com.baidu.location.e.a r6 = r13.f1327b
            if (r6 == 0) goto L_0x015a
            com.baidu.location.e.a r6 = r13.f1327b
            java.lang.String r6 = r6.mo10623h()
            if (r6 == 0) goto L_0x015a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            com.baidu.location.e.a r7 = r13.f1327b
            java.lang.String r7 = r7.mo10623h()
            r6.append(r7)
            r6.append(r1)
            java.lang.String r1 = r6.toString()
        L_0x015a:
            com.baidu.location.g.b r6 = com.baidu.location.p019g.C0844b.m2417a()
            java.lang.String r6 = r6.mo10713a(r0)
            if (r6 == 0) goto L_0x0173
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r1)
            r7.append(r6)
            java.lang.String r1 = r7.toString()
        L_0x0173:
            java.lang.String r6 = r13.f1387j
            if (r6 == 0) goto L_0x018a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r1)
            java.lang.String r1 = r13.f1387j
            r6.append(r1)
            java.lang.String r1 = r6.toString()
            r13.f1387j = r4
        L_0x018a:
            com.baidu.location.e.h r6 = r13.f1326a
            if (r6 == 0) goto L_0x0194
            com.baidu.location.e.h r2 = r13.f1326a
            long r2 = r2.mo10678f()
        L_0x0194:
            com.baidu.location.a.i$b r6 = r13.f1384e
            r6.mo10465a(r1, r2)
            com.baidu.location.e.a r1 = r13.f1327b
            r13.f1391n = r1
            com.baidu.location.e.h r1 = r13.f1326a
            r13.f1390m = r1
            com.baidu.location.e.e r1 = com.baidu.location.p017e.C0826e.m2306a()
            boolean r1 = r1.mo10647j()
            if (r1 != 0) goto L_0x01ae
            r13.m1893k()
        L_0x01ae:
            com.baidu.location.d.h r1 = com.baidu.location.p016d.C0804h.m2193a()
            boolean r1 = r1.mo10597h()
            if (r1 == 0) goto L_0x01e0
            com.baidu.location.a.l$a r1 = r13.f1372D
            if (r1 != 0) goto L_0x01c3
            com.baidu.location.a.l$a r1 = new com.baidu.location.a.l$a
            r1.<init>(r13, r4)
            r13.f1372D = r1
        L_0x01c3:
            com.baidu.location.d.h r1 = com.baidu.location.p016d.C0804h.m2193a()
            com.baidu.location.e.b r2 = com.baidu.location.p017e.C0822b.m2279a()
            int r2 = r2.mo10630e()
            java.lang.String r2 = com.baidu.location.p017e.C0825d.m2298a(r2)
            long r1 = r1.mo10589a(r2)
            android.os.Handler r3 = r13.f1385f
            com.baidu.location.a.l$a r4 = r13.f1372D
            r3.postDelayed(r4, r1)
            r13.f1373E = r0
        L_0x01e0:
            boolean r1 = r13.f1394q
            if (r1 != r0) goto L_0x0212
            r13.f1394q = r5
            boolean r0 = com.baidu.location.p017e.C0835i.m2384j()
            if (r0 == 0) goto L_0x020b
            if (r14 == 0) goto L_0x020b
            com.baidu.location.a.a r0 = com.baidu.location.p013a.C0723a.m1729a()
            int r14 = r0.mo10433e(r14)
            r0 = 1000(0x3e8, float:1.401E-42)
            if (r14 >= r0) goto L_0x020b
            com.baidu.location.d.h r14 = com.baidu.location.p016d.C0804h.m2193a()
            boolean r14 = r14.mo10593d()
            if (r14 == 0) goto L_0x020b
            com.baidu.location.d.h r14 = com.baidu.location.p016d.C0804h.m2193a()
            r14.mo10598i()
        L_0x020b:
            com.baidu.location.b.b r14 = com.baidu.location.p014b.C0772b.m1993a()
            r14.mo10523b()
        L_0x0212:
            int r14 = r13.f1380L
            if (r14 <= 0) goto L_0x0222
            r0 = 2
            if (r14 != r0) goto L_0x0220
            com.baidu.location.e.i r14 = com.baidu.location.p017e.C0835i.m2376a()
            r14.mo10691g()
        L_0x0220:
            r13.f1380L = r5
        L_0x0222:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0750l.m1892h(android.os.Message):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b1, code lost:
        if (r0.getPoiList() == null) goto L_0x0089;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b7  */
    /* renamed from: k */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m1893k() {
        /*
            r15 = this;
            double r0 = java.lang.Math.random()
            android.os.SystemClock.uptimeMillis()
            com.baidu.location.e.b r2 = com.baidu.location.p017e.C0822b.m2279a()
            com.baidu.location.e.a r2 = r2.mo10631f()
            com.baidu.location.e.i r3 = com.baidu.location.p017e.C0835i.m2376a()
            com.baidu.location.e.h r3 = r3.mo10698o()
            if (r3 == 0) goto L_0x0024
            int r4 = r3.mo10664a()
            if (r4 <= 0) goto L_0x0024
            long r4 = r3.mo10679g()
            goto L_0x0026
        L_0x0024:
            r4 = 0
        L_0x0026:
            r6 = 1
            r7 = 0
            if (r2 == 0) goto L_0x003a
            boolean r2 = r2.mo10620e()
            if (r2 == 0) goto L_0x003a
            if (r3 == 0) goto L_0x0038
            int r2 = r3.mo10664a()
            if (r2 != 0) goto L_0x003a
        L_0x0038:
            r2 = 1
            goto L_0x003b
        L_0x003a:
            r2 = 0
        L_0x003b:
            com.baidu.location.d.h r3 = com.baidu.location.p016d.C0804h.m2193a()
            boolean r3 = r3.mo10593d()
            r8 = 0
            if (r3 == 0) goto L_0x00b8
            com.baidu.location.d.h r3 = com.baidu.location.p016d.C0804h.m2193a()
            boolean r3 = r3.mo10595f()
            if (r3 == 0) goto L_0x00b8
            r9 = 60
            int r3 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r3 >= 0) goto L_0x00b8
            if (r2 != 0) goto L_0x006a
            r2 = 0
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 >= 0) goto L_0x00b8
            com.baidu.location.d.h r2 = com.baidu.location.p016d.C0804h.m2193a()
            double r2 = r2.mo10604o()
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x00b8
        L_0x006a:
            com.baidu.location.d.h r9 = com.baidu.location.p016d.C0804h.m2193a()
            com.baidu.location.e.b r0 = com.baidu.location.p017e.C0822b.m2279a()
            com.baidu.location.e.a r10 = r0.mo10631f()
            com.baidu.location.e.i r0 = com.baidu.location.p017e.C0835i.m2376a()
            com.baidu.location.e.h r11 = r0.mo10698o()
            r12 = 0
            com.baidu.location.d.h$b r13 = com.baidu.location.p016d.C0804h.C0806b.IS_MIX_MODE
            com.baidu.location.d.h$a r14 = com.baidu.location.p016d.C0804h.C0805a.NEED_TO_LOG
            com.baidu.location.BDLocation r0 = r9.mo10590a(r10, r11, r12, r13, r14)
            if (r0 != 0) goto L_0x008b
        L_0x0089:
            r1 = 0
            goto L_0x00b4
        L_0x008b:
            java.lang.String r1 = com.baidu.location.p019g.C0855k.f1963g
            java.lang.String r2 = "all"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x009d
            java.lang.String r1 = r0.getAddrStr()
            if (r1 != 0) goto L_0x009d
            r1 = 0
            goto L_0x009e
        L_0x009d:
            r1 = 1
        L_0x009e:
            boolean r2 = com.baidu.location.p019g.C0855k.f1964h
            if (r2 == 0) goto L_0x00a9
            java.lang.String r2 = r0.getLocationDescribe()
            if (r2 != 0) goto L_0x00a9
            r1 = 0
        L_0x00a9:
            boolean r2 = com.baidu.location.p019g.C0855k.f1966j
            if (r2 == 0) goto L_0x00b4
            java.util.List r2 = r0.getPoiList()
            if (r2 != 0) goto L_0x00b4
            goto L_0x0089
        L_0x00b4:
            if (r1 != 0) goto L_0x00b7
            goto L_0x00b8
        L_0x00b7:
            r8 = r0
        L_0x00b8:
            if (r8 == 0) goto L_0x00e0
            int r0 = r8.getLocType()
            r1 = 66
            if (r0 != r1) goto L_0x00e0
            boolean r0 = r15.f1395r
            if (r0 == 0) goto L_0x00e0
            com.baidu.location.BDLocation r0 = new com.baidu.location.BDLocation
            r0.<init>(r8)
            r1 = 161(0xa1, float:2.26E-43)
            r0.setLocType(r1)
            boolean r1 = r15.f1395r
            if (r1 == 0) goto L_0x00e0
            r15.f1374F = r6
            com.baidu.location.a.a r1 = com.baidu.location.p013a.C0723a.m1729a()
            r1.mo10422a(r0)
            r15.f1388k = r0
            goto L_0x00e1
        L_0x00e0:
            r6 = 0
        L_0x00e1:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0750l.m1893k():boolean");
    }

    /* renamed from: l */
    private String[] m1894l() {
        boolean z;
        C0726b a;
        int i;
        String[] strArr = {"", "Location failed beacuse we can not get any loc information!"};
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("&apl=");
        int b = C0855k.m2459b(C0839f.getServiceContext());
        String str = "Location failed beacuse we can not get any loc information in airplane mode, you can turn it off and try again!!";
        if (b == 1) {
            strArr[1] = str;
        }
        stringBuffer.append(b);
        String d = C0855k.m2465d(C0839f.getServiceContext());
        if (d.contains("0|0|")) {
            strArr[1] = "Location failed beacuse we can not get any loc information without any location permission!";
        }
        stringBuffer.append(d);
        if (Build.VERSION.SDK_INT >= 23) {
            stringBuffer.append("&loc=");
            int c = C0855k.m2462c(C0839f.getServiceContext());
            if (c == 0) {
                strArr[1] = "Location failed beacuse we can not get any loc information with the phone loc mode is off, you can turn it on and try again!";
                z = true;
            } else {
                z = false;
            }
            stringBuffer.append(c);
        } else {
            z = false;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            stringBuffer.append("&lmd=");
            int c2 = C0855k.m2462c(C0839f.getServiceContext());
            if (c2 >= 0) {
                stringBuffer.append(c2);
            }
        }
        String g = C0822b.m2279a().mo10632g();
        String h = C0835i.m2376a().mo10692h();
        stringBuffer.append(h);
        stringBuffer.append(g);
        stringBuffer.append(C0855k.m2467e(C0839f.getServiceContext()));
        if (b == 1) {
            a = C0726b.m1759a();
            i = 7;
        } else {
            if (d.contains("0|0|")) {
                C0726b.m1759a().mo10440a(62, 4, "Location failed beacuse we can not get any loc information without any location permission!");
            } else if (z) {
                C0726b.m1759a().mo10440a(62, 5, "Location failed beacuse we can not get any loc information with the phone loc mode is off, you can turn it on and try again!");
            } else if (g == null || h == null || !g.equals("&sim=1") || h.equals("&wifio=1")) {
                C0726b.m1759a().mo10440a(62, 9, "Location failed beacuse we can not get any loc information!");
            } else {
                a = C0726b.m1759a();
                i = 6;
                str = "Location failed beacuse we can not get any loc information , you can insert a sim card or open wifi and try again!";
            }
            strArr[0] = stringBuffer.toString();
            return strArr;
        }
        a.mo10440a(62, i, str);
        strArr[0] = stringBuffer.toString();
        return strArr;
    }

    /* renamed from: m */
    private void m1895m() {
        this.f1395r = false;
        this.f1374F = false;
        this.f1375G = false;
        this.f1369A = false;
        m1896n();
        if (this.f1383O) {
            this.f1383O = false;
        }
    }

    /* renamed from: n */
    private void m1896n() {
        if (this.f1388k != null && C0835i.m2384j()) {
            C0767x.m1972a().mo10512d();
        }
    }

    /* renamed from: a */
    public Address mo10476a(BDLocation bDLocation) {
        if (C0855k.f1963g.equals("all") || C0855k.f1964h || C0855k.f1966j) {
            float[] fArr = new float[2];
            Location.distanceBetween(this.f1403z, this.f1402y, bDLocation.getLatitude(), bDLocation.getLongitude(), fArr);
            if (fArr[0] < 100.0f) {
                Address address = this.f1399v;
                if (address != null) {
                    return address;
                }
            } else {
                this.f1400w = null;
                this.f1401x = null;
                this.f1369A = true;
                this.f1385f.post(new C0753m(this));
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo10461a() {
        BDLocation bDLocation;
        C0751a aVar = this.f1372D;
        boolean z = false;
        if (aVar != null && this.f1373E) {
            this.f1373E = false;
            this.f1385f.removeCallbacks(aVar);
        }
        if (C0826e.m2306a().mo10647j()) {
            BDLocation bDLocation2 = new BDLocation(C0826e.m2306a().mo10644g());
            if (C0855k.f1963g.equals("all") || C0855k.f1964h || C0855k.f1966j) {
                float[] fArr = new float[2];
                Location.distanceBetween(this.f1403z, this.f1402y, bDLocation2.getLatitude(), bDLocation2.getLongitude(), fArr);
                if (fArr[0] < 100.0f) {
                    Address address = this.f1399v;
                    if (address != null) {
                        bDLocation2.setAddr(address);
                    }
                    String str = this.f1400w;
                    if (str != null) {
                        bDLocation2.setLocationDescribe(str);
                    }
                    List<Poi> list = this.f1401x;
                    if (list != null) {
                        bDLocation2.setPoiList(list);
                    }
                }
            }
            C0723a.m1729a().mo10422a(bDLocation2);
        } else if (this.f1374F) {
            m1895m();
            return;
        } else {
            if (!C0804h.m2193a().mo10593d() || !C0804h.m2193a().mo10594e()) {
                bDLocation = null;
            } else {
                bDLocation = C0804h.m2193a().mo10590a(C0822b.m2279a().mo10631f(), C0835i.m2376a().mo10698o(), null, C0804h.C0806b.IS_NOT_MIX_MODE, C0804h.C0805a.NEED_TO_LOG);
                if (bDLocation != null && bDLocation.getLocType() == 66) {
                    C0723a.m1729a().mo10422a(bDLocation);
                }
            }
            if (bDLocation == null || bDLocation.getLocType() == 67) {
                if (this.f1386i || this.f1388k == null) {
                    if (C0790a.m2063a().f1567a) {
                        bDLocation = C0790a.m2063a().mo10555a(false);
                    } else if (bDLocation == null) {
                        bDLocation = new BDLocation();
                        bDLocation.setLocType(67);
                    }
                    if (bDLocation != null) {
                        C0723a.m1729a().mo10422a(bDLocation);
                        if (bDLocation.getLocType() == 67 && !this.f1377I) {
                            C0726b.m1759a().mo10440a(67, 3, "Offline location failed, please check the net (wifi/cell)!");
                        }
                        boolean z2 = true;
                        if (C0855k.f1963g.equals("all") && bDLocation.getAddrStr() == null) {
                            z2 = false;
                        }
                        if (C0855k.f1964h && bDLocation.getLocationDescribe() == null) {
                            z2 = false;
                        }
                        if (!C0855k.f1966j || bDLocation.getPoiList() != null) {
                            z = z2;
                        }
                        if (!z) {
                            bDLocation.setLocType(67);
                        }
                    }
                } else {
                    C0723a.m1729a().mo10422a(this.f1388k);
                }
            }
            this.f1389l = null;
        }
        m1895m();
    }

    /* renamed from: a */
    public void mo10462a(Message message) {
        C0751a aVar = this.f1372D;
        if (aVar != null && this.f1373E) {
            this.f1373E = false;
            this.f1385f.removeCallbacks(aVar);
        }
        BDLocation bDLocation = (BDLocation) message.obj;
        if (bDLocation != null && bDLocation.getLocType() == 167 && this.f1377I) {
            bDLocation.setLocType(62);
        }
        mo10479b(bDLocation);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0044, code lost:
        if (com.baidu.location.p016d.C0790a.m2063a().f1567a == false) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x003a, code lost:
        if (r0.getLocType() != 67) goto L_0x005b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10477a(boolean r9, boolean r10) {
        /*
            r8 = this;
            com.baidu.location.d.h r0 = com.baidu.location.p016d.C0804h.m2193a()
            boolean r0 = r0.mo10593d()
            r1 = 0
            if (r0 == 0) goto L_0x0047
            com.baidu.location.d.h r0 = com.baidu.location.p016d.C0804h.m2193a()
            boolean r0 = r0.mo10596g()
            if (r0 == 0) goto L_0x0047
            com.baidu.location.d.h r2 = com.baidu.location.p016d.C0804h.m2193a()
            com.baidu.location.e.b r0 = com.baidu.location.p017e.C0822b.m2279a()
            com.baidu.location.e.a r3 = r0.mo10631f()
            com.baidu.location.e.i r0 = com.baidu.location.p017e.C0835i.m2376a()
            com.baidu.location.e.h r4 = r0.mo10698o()
            r5 = 0
            com.baidu.location.d.h$b r6 = com.baidu.location.p016d.C0804h.C0806b.IS_NOT_MIX_MODE
            com.baidu.location.d.h$a r7 = com.baidu.location.p016d.C0804h.C0805a.NEED_TO_LOG
            com.baidu.location.BDLocation r0 = r2.mo10590a(r3, r4, r5, r6, r7)
            if (r0 == 0) goto L_0x003c
            int r2 = r0.getLocType()
            r3 = 67
            if (r2 != r3) goto L_0x005b
        L_0x003c:
            if (r9 == 0) goto L_0x005b
            com.baidu.location.d.a r9 = com.baidu.location.p016d.C0790a.m2063a()
            boolean r9 = r9.f1567a
            if (r9 == 0) goto L_0x005b
            goto L_0x0051
        L_0x0047:
            if (r9 == 0) goto L_0x005a
            com.baidu.location.d.a r9 = com.baidu.location.p016d.C0790a.m2063a()
            boolean r9 = r9.f1567a
            if (r9 == 0) goto L_0x005a
        L_0x0051:
            com.baidu.location.d.a r9 = com.baidu.location.p016d.C0790a.m2063a()
            com.baidu.location.BDLocation r0 = r9.mo10555a(r1)
            goto L_0x005b
        L_0x005a:
            r0 = 0
        L_0x005b:
            if (r0 == 0) goto L_0x0098
            int r9 = r0.getLocType()
            r2 = 66
            if (r9 != r2) goto L_0x0098
            r9 = 1
            java.lang.String r2 = com.baidu.location.p019g.C0855k.f1963g
            java.lang.String r3 = "all"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0077
            java.lang.String r2 = r0.getAddrStr()
            if (r2 != 0) goto L_0x0077
            r9 = 0
        L_0x0077:
            boolean r2 = com.baidu.location.p019g.C0855k.f1964h
            if (r2 == 0) goto L_0x0082
            java.lang.String r2 = r0.getLocationDescribe()
            if (r2 != 0) goto L_0x0082
            r9 = 0
        L_0x0082:
            boolean r2 = com.baidu.location.p019g.C0855k.f1966j
            if (r2 == 0) goto L_0x008d
            java.util.List r2 = r0.getPoiList()
            if (r2 != 0) goto L_0x008d
            r9 = 0
        L_0x008d:
            if (r9 != 0) goto L_0x0091
            if (r10 == 0) goto L_0x0098
        L_0x0091:
            com.baidu.location.a.a r9 = com.baidu.location.p013a.C0723a.m1729a()
            r9.mo10422a(r0)
        L_0x0098:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0750l.mo10477a(boolean, boolean):void");
    }

    /* renamed from: b */
    public void mo10478b(Message message) {
        if (this.f1376H) {
            m1884c(message);
        }
    }

    /* renamed from: b */
    public void mo10479b(BDLocation bDLocation) {
        C0834h hVar;
        BDLocation bDLocation2;
        String h;
        int c;
        BDLocation bDLocation3 = bDLocation;
        BDLocation bDLocation4 = new BDLocation(bDLocation3);
        if (bDLocation.hasAddr()) {
            this.f1399v = bDLocation.getAddress();
            this.f1402y = bDLocation.getLongitude();
            this.f1403z = bDLocation.getLatitude();
        }
        if (bDLocation.getLocationDescribe() != null) {
            this.f1400w = bDLocation.getLocationDescribe();
            this.f1402y = bDLocation.getLongitude();
            this.f1403z = bDLocation.getLatitude();
        }
        if (bDLocation.getPoiList() != null) {
            this.f1401x = bDLocation.getPoiList();
            this.f1402y = bDLocation.getLongitude();
            this.f1403z = bDLocation.getLatitude();
        }
        boolean z = false;
        if (C0826e.m2306a().mo10647j()) {
            BDLocation bDLocation5 = new BDLocation(C0826e.m2306a().mo10644g());
            if (C0855k.f1963g.equals("all") || C0855k.f1964h || C0855k.f1966j) {
                float[] fArr = new float[2];
                Location.distanceBetween(this.f1403z, this.f1402y, bDLocation5.getLatitude(), bDLocation5.getLongitude(), fArr);
                if (fArr[0] < 100.0f) {
                    Address address = this.f1399v;
                    if (address != null) {
                        bDLocation5.setAddr(address);
                    }
                    String str = this.f1400w;
                    if (str != null) {
                        bDLocation5.setLocationDescribe(str);
                    }
                    List<Poi> list = this.f1401x;
                    if (list != null) {
                        bDLocation5.setPoiList(list);
                    }
                }
            }
            C0723a.m1729a().mo10422a(bDLocation5);
            m1895m();
        } else if (this.f1374F) {
            float[] fArr2 = new float[2];
            BDLocation bDLocation6 = this.f1388k;
            if (bDLocation6 != null) {
                Location.distanceBetween(bDLocation6.getLatitude(), this.f1388k.getLongitude(), bDLocation.getLatitude(), bDLocation.getLongitude(), fArr2);
            }
            if (fArr2[0] > 10.0f) {
                this.f1388k = bDLocation3;
                if (!this.f1375G) {
                    this.f1375G = false;
                }
                m1895m();
            }
            if (bDLocation.getUserIndoorState() > -1) {
                this.f1388k = bDLocation3;
            }
            m1895m();
            C0723a.m1729a().mo10422a(bDLocation3);
            m1895m();
        } else {
            if (bDLocation.getLocType() == 167) {
                C0726b.m1759a().mo10440a(BDLocation.TypeServerError, 8, "NetWork location failed because baidu location service can not caculate the location!");
            } else if (bDLocation.getLocType() == 161) {
                if (Build.VERSION.SDK_INT >= 19 && ((c = C0855k.m2462c(C0839f.getServiceContext())) == 0 || c == 2)) {
                    C0726b.m1759a().mo10440a(BDLocation.TypeNetWorkLocation, 1, "NetWork location successful, open gps will be better!");
                } else if (bDLocation.getRadius() >= 100.0f && bDLocation.getNetworkLocationType() != null && bDLocation.getNetworkLocationType().equals(Config.CELL_LOCATION) && (h = C0835i.m2376a().mo10692h()) != null && !h.equals("&wifio=1")) {
                    C0726b.m1759a().mo10440a(BDLocation.TypeNetWorkLocation, 2, "NetWork location successful, open wifi will be better!");
                }
            }
            String str2 = null;
            this.f1389l = null;
            if (bDLocation.getLocType() == 161 && Config.CELL_LOCATION.equals(bDLocation.getNetworkLocationType()) && (bDLocation2 = this.f1388k) != null && bDLocation2.getLocType() == 161 && "wf".equals(this.f1388k.getNetworkLocationType()) && System.currentTimeMillis() - this.f1398u < 30000) {
                this.f1389l = bDLocation3;
                z = true;
            }
            C0723a a = C0723a.m1729a();
            if (z) {
                a.mo10422a(this.f1388k);
            } else {
                a.mo10422a(bDLocation3);
                this.f1398u = System.currentTimeMillis();
            }
            if (!C0855k.m2458a(bDLocation)) {
                this.f1388k = null;
            } else if (!z) {
                this.f1388k = bDLocation3;
            }
            int a2 = C0855k.m2451a(f1325c, "ssid\":\"", "\"");
            if (!(a2 == Integer.MIN_VALUE || (hVar = this.f1390m) == null)) {
                str2 = hVar.mo10676d(a2);
            }
            this.f1387j = str2;
            if (C0804h.m2193a().mo10593d() && bDLocation.getLocType() == 161 && Config.CELL_LOCATION.equals(bDLocation.getNetworkLocationType()) && m1882b(this.f1391n)) {
                C0804h.m2193a().mo10590a(this.f1391n, null, bDLocation4, C0804h.C0806b.IS_NOT_MIX_MODE, C0804h.C0805a.NO_NEED_TO_LOG);
                this.f1393p = this.f1391n;
            }
            if (C0804h.m2193a().mo10593d() && bDLocation.getLocType() == 161 && "wf".equals(bDLocation.getNetworkLocationType())) {
                C0804h.m2193a().mo10590a(null, this.f1390m, bDLocation4, C0804h.C0806b.IS_NOT_MIX_MODE, C0804h.C0805a.NO_NEED_TO_LOG);
                this.f1392o = this.f1390m;
            }
            if (this.f1391n != null) {
                C0790a.m2063a().mo10556a(f1325c, this.f1391n, this.f1390m, bDLocation4);
            }
            if (C0835i.m2384j()) {
                C0804h.m2193a().mo10598i();
                C0804h.m2193a().mo10602m();
            }
            m1895m();
        }
    }

    /* renamed from: c */
    public void mo10480c(BDLocation bDLocation) {
        this.f1388k = new BDLocation(bDLocation);
    }

    /* renamed from: d */
    public void mo10481d() {
        this.f1394q = true;
        this.f1395r = false;
        this.f1376H = true;
    }

    /* renamed from: e */
    public void mo10482e() {
        this.f1395r = false;
        this.f1396s = false;
        this.f1374F = false;
        this.f1375G = true;
        mo10487j();
        this.f1376H = false;
    }

    /* renamed from: f */
    public String mo10483f() {
        return this.f1400w;
    }

    /* renamed from: g */
    public List<Poi> mo10484g() {
        return this.f1401x;
    }

    /* renamed from: h */
    public boolean mo10485h() {
        return this.f1386i;
    }

    /* renamed from: i */
    public void mo10486i() {
        if (this.f1396s) {
            m1892h(null);
            this.f1396s = false;
            return;
        }
        C0772b.m1993a().mo10525d();
    }

    /* renamed from: j */
    public void mo10487j() {
        this.f1388k = null;
    }
}
