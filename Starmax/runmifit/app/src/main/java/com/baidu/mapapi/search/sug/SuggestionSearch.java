package com.baidu.mapapi.search.sug;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.search.core.C0968l;
import com.baidu.platform.core.p036f.C1373a;
import com.baidu.platform.core.p036f.C1374b;

public class SuggestionSearch extends C0968l {

    /* renamed from: a */
    C1373a f3134a = new C1374b();

    /* renamed from: b */
    private boolean f3135b = false;

    private SuggestionSearch() {
    }

    public static SuggestionSearch newInstance() {
        BMapManager.init();
        return new SuggestionSearch();
    }

    public void destroy() {
        if (!this.f3135b) {
            this.f3135b = true;
            this.f3134a.mo14073a();
            BMapManager.destroy();
        }
    }

    public boolean requestSuggestion(SuggestionSearchOption suggestionSearchOption) {
        if (this.f3134a == null) {
            throw new IllegalStateException("BDMapSDKException: suggestionsearch is null, please call newInstance() first.");
        } else if (suggestionSearchOption != null && suggestionSearchOption.mKeyword != null && suggestionSearchOption.mCity != null) {
            return this.f3134a.mo14075a(suggestionSearchOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or keyword or city can not be null");
        }
    }

    public void setOnGetSuggestionResultListener(OnGetSuggestionResultListener onGetSuggestionResultListener) {
        C1373a aVar = this.f3134a;
        if (aVar == null) {
            throw new IllegalStateException("BDMapSDKException: suggestionsearch is null, please call newInstance() first.");
        } else if (onGetSuggestionResultListener != null) {
            aVar.mo14074a(onGetSuggestionResultListener);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: listener can not be null");
        }
    }
}
