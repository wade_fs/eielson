package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;

public class TransitResultNode implements Parcelable {
    public static final Parcelable.Creator<TransitResultNode> CREATOR = new C0972p();

    /* renamed from: a */
    private int f2955a;

    /* renamed from: b */
    private String f2956b = null;

    /* renamed from: c */
    private LatLng f2957c = null;

    /* renamed from: d */
    private String f2958d = null;

    public TransitResultNode(int i, String str, LatLng latLng, String str2) {
        this.f2955a = i;
        this.f2956b = str;
        this.f2957c = latLng;
        this.f2958d = str2;
    }

    protected TransitResultNode(Parcel parcel) {
        this.f2955a = parcel.readInt();
        this.f2956b = parcel.readString();
        this.f2957c = (LatLng) parcel.readValue(LatLng.class.getClassLoader());
        this.f2958d = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public int getCityId() {
        return this.f2955a;
    }

    public String getCityName() {
        return this.f2956b;
    }

    public LatLng getLocation() {
        return this.f2957c;
    }

    public String getSearchWord() {
        return this.f2958d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f2955a);
        parcel.writeString(this.f2956b);
        parcel.writeValue(this.f2957c);
        parcel.writeString(this.f2958d);
    }
}
