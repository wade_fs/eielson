package com.baidu.location.indoor.mapversion.p022c;

import android.content.Context;
import java.io.File;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.baidu.location.indoor.mapversion.c.c */
public class C0902c {

    /* renamed from: a */
    private static C0902c f2282a;

    /* renamed from: b */
    private C0903a f2283b = null;

    /* renamed from: c */
    private String f2284c = null;

    /* renamed from: d */
    private boolean f2285d = false;

    /* renamed from: e */
    private boolean f2286e = false;

    /* renamed from: f */
    private String f2287f = "slr";

    /* renamed from: g */
    private String f2288g = "";

    /* renamed from: h */
    private double f2289h = 7.0d;

    /* renamed from: i */
    private Map<String, C0904b> f2290i = Collections.synchronizedMap(new HashMap());

    /* renamed from: com.baidu.location.indoor.mapversion.c.c$a */
    public interface C0903a {
    }

    /* renamed from: com.baidu.location.indoor.mapversion.c.c$b */
    public static class C0904b implements Serializable {

        /* renamed from: a */
        public String f2291a;

        /* renamed from: b */
        public String f2292b;

        /* renamed from: c */
        public double f2293c;

        /* renamed from: d */
        public double f2294d;

        /* renamed from: e */
        public double f2295e;

        /* renamed from: f */
        public double f2296f;

        /* renamed from: g */
        public String f2297g;
    }

    private C0902c(Context context) {
        this.f2287f = new File(context.getCacheDir(), this.f2287f).getAbsolutePath();
    }

    /* renamed from: a */
    public static synchronized C0902c m2778a() {
        C0902c cVar;
        synchronized (C0902c.class) {
            cVar = f2282a;
        }
        return cVar;
    }

    /* renamed from: a */
    public static C0902c m2779a(Context context) {
        if (f2282a == null) {
            f2282a = new C0902c(context);
        }
        return f2282a;
    }

    /* renamed from: b */
    public boolean mo10831b() {
        return this.f2286e;
    }

    /* renamed from: c */
    public boolean mo10832c() {
        return this.f2288g.equals("on");
    }

    /* renamed from: d */
    public Map<String, C0904b> mo10833d() {
        return this.f2290i;
    }
}
