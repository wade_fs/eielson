package com.baidu.platform.core.p035e;

import com.baidu.mapapi.http.HttpClient;
import com.baidu.mapapi.search.share.PoiDetailShareURLOption;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;

/* renamed from: com.baidu.platform.core.e.c */
public class C1367c extends C1320e {
    public C1367c(PoiDetailShareURLOption poiDetailShareURLOption) {
        m5111a(poiDetailShareURLOption);
    }

    /* renamed from: a */
    private void m5111a(PoiDetailShareURLOption poiDetailShareURLOption) {
        this.f4435a.mo14094a("url", ("http://wapmap.baidu.com/s?tn=Detail&pid=" + poiDetailShareURLOption.mUid + "&smsf=3") + HttpClient.getPhoneInfo());
        mo14027b(false);
        mo14026a(false);
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14091p();
    }
}
