package com.baidu.mapapi.map.offline;

import com.baidu.mapsdkplatform.comapi.map.C1083i;
import com.baidu.mapsdkplatform.comapi.map.C1093q;
import com.baidu.mapsdkplatform.comapi.map.C1094r;
import com.baidu.mapsdkplatform.comapi.map.C1097u;
import com.baidu.mapsdkplatform.comapi.map.C1098v;
import java.util.ArrayList;
import java.util.Iterator;

public class MKOfflineMap {
    public static final int TYPE_DOWNLOAD_UPDATE = 0;
    public static final int TYPE_NETWORK_ERROR = 2;
    public static final int TYPE_NEW_OFFLINE = 6;
    public static final int TYPE_VER_UPDATE = 4;

    /* renamed from: a */
    private static final String f2859a = MKOfflineMap.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C1094r f2860b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public MKOfflineMapListener f2861c;

    public void destroy() {
        this.f2860b.mo13131d(0);
        this.f2860b.mo13126b((C1098v) null);
        this.f2860b.mo13125b();
        C1083i.m3652b();
    }

    public ArrayList<MKOLUpdateElement> getAllUpdateInfo() {
        ArrayList<C1097u> e = this.f2860b.mo13132e();
        if (e == null) {
            return null;
        }
        ArrayList<MKOLUpdateElement> arrayList = new ArrayList<>();
        Iterator<C1097u> it = e.iterator();
        while (it.hasNext()) {
            arrayList.add(OfflineMapUtil.getUpdatElementFromLocalMapElement(it.next().mo13137a()));
        }
        return arrayList;
    }

    public ArrayList<MKOLSearchRecord> getHotCityList() {
        ArrayList<C1093q> c = this.f2860b.mo13128c();
        if (c == null) {
            return null;
        }
        ArrayList<MKOLSearchRecord> arrayList = new ArrayList<>();
        Iterator<C1093q> it = c.iterator();
        while (it.hasNext()) {
            arrayList.add(OfflineMapUtil.getSearchRecordFromLocalCityInfo(it.next()));
        }
        return arrayList;
    }

    public ArrayList<MKOLSearchRecord> getOfflineCityList() {
        ArrayList<C1093q> d = this.f2860b.mo13130d();
        if (d == null) {
            return null;
        }
        ArrayList<MKOLSearchRecord> arrayList = new ArrayList<>();
        Iterator<C1093q> it = d.iterator();
        while (it.hasNext()) {
            arrayList.add(OfflineMapUtil.getSearchRecordFromLocalCityInfo(it.next()));
        }
        return arrayList;
    }

    public MKOLUpdateElement getUpdateInfo(int i) {
        C1097u g = this.f2860b.mo13135g(i);
        if (g == null) {
            return null;
        }
        return OfflineMapUtil.getUpdatElementFromLocalMapElement(g.mo13137a());
    }

    @Deprecated
    public int importOfflineData() {
        return importOfflineData(false);
    }

    @Deprecated
    public int importOfflineData(boolean z) {
        int i;
        ArrayList<C1097u> e = this.f2860b.mo13132e();
        int i2 = 0;
        if (e != null) {
            i2 = e.size();
            i = i2;
        } else {
            i = 0;
        }
        this.f2860b.mo13124a(z, true);
        ArrayList<C1097u> e2 = this.f2860b.mo13132e();
        if (e2 != null) {
            i = e2.size();
        }
        return i - i2;
    }

    public boolean init(MKOfflineMapListener mKOfflineMapListener) {
        C1083i.m3649a();
        this.f2860b = C1094r.m3705a();
        C1094r rVar = this.f2860b;
        if (rVar == null) {
            return false;
        }
        rVar.mo13122a(new C0940a(this));
        this.f2861c = mKOfflineMapListener;
        return true;
    }

    public boolean pause(int i) {
        return this.f2860b.mo13129c(i);
    }

    public boolean remove(int i) {
        return this.f2860b.mo13133e(i);
    }

    public ArrayList<MKOLSearchRecord> searchCity(String str) {
        ArrayList<C1093q> a = this.f2860b.mo13121a(str);
        if (a == null) {
            return null;
        }
        ArrayList<MKOLSearchRecord> arrayList = new ArrayList<>();
        Iterator<C1093q> it = a.iterator();
        while (it.hasNext()) {
            arrayList.add(OfflineMapUtil.getSearchRecordFromLocalCityInfo(it.next()));
        }
        return arrayList;
    }

    public boolean start(int i) {
        C1094r rVar = this.f2860b;
        if (rVar == null) {
            return false;
        }
        if (rVar.mo13132e() != null) {
            Iterator<C1097u> it = this.f2860b.mo13132e().iterator();
            while (it.hasNext()) {
                C1097u next = it.next();
                if (next.f3617a.f3605a == i) {
                    if (next.f3617a.f3614j || next.f3617a.f3616l == 2 || next.f3617a.f3616l == 3 || next.f3617a.f3616l == 6) {
                        return this.f2860b.mo13127b(i);
                    }
                    return false;
                }
            }
        }
        return this.f2860b.mo13123a(i);
    }

    public boolean update(int i) {
        C1094r rVar = this.f2860b;
        if (rVar != null && rVar.mo13132e() != null) {
            Iterator<C1097u> it = this.f2860b.mo13132e().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                C1097u next = it.next();
                if (next.f3617a.f3605a == i) {
                    if (next.f3617a.f3614j) {
                        return this.f2860b.mo13134f(i);
                    }
                }
            }
        }
        return false;
    }
}
