package com.baidu.mapapi;

import android.content.Context;
import com.baidu.mapsdkplatform.comapi.C1028a;

public class BMapManager {
    public static void destroy() {
        C1028a.m3342a().mo12871d();
    }

    public static Context getContext() {
        return C1028a.m3342a().mo12872e();
    }

    public static void init() {
        C1028a.m3342a().mo12869b();
    }
}
