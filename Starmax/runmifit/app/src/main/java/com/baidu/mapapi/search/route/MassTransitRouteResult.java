package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.core.TaxiInfo;
import com.baidu.mapapi.search.core.TransitResultNode;
import java.util.ArrayList;
import java.util.List;

public final class MassTransitRouteResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<MassTransitRouteResult> CREATOR = new C0996l();

    /* renamed from: a */
    private TransitResultNode f3088a;

    /* renamed from: b */
    private TransitResultNode f3089b;

    /* renamed from: c */
    private TaxiInfo f3090c;

    /* renamed from: d */
    private int f3091d;

    /* renamed from: e */
    private List<MassTransitRouteLine> f3092e;

    /* renamed from: f */
    private SuggestAddrInfo f3093f;

    public MassTransitRouteResult() {
    }

    MassTransitRouteResult(Parcel parcel) {
        this.f3088a = (TransitResultNode) parcel.readParcelable(TransitResultNode.class.getClassLoader());
        this.f3089b = (TransitResultNode) parcel.readParcelable(TransitResultNode.class.getClassLoader());
        this.f3090c = (TaxiInfo) parcel.readParcelable(TaxiInfo.class.getClassLoader());
        this.f3091d = parcel.readInt();
        this.f3092e = new ArrayList();
        parcel.readList(this.f3092e, MassTransitRouteLine.class.getClassLoader());
        this.f3093f = (SuggestAddrInfo) parcel.readParcelable(SuggestAddrInfo.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public TransitResultNode getDestination() {
        return this.f3089b;
    }

    public TransitResultNode getOrigin() {
        return this.f3088a;
    }

    public List<MassTransitRouteLine> getRouteLines() {
        return this.f3092e;
    }

    public SuggestAddrInfo getSuggestAddrInfo() {
        return this.f3093f;
    }

    public TaxiInfo getTaxiInfo() {
        return this.f3090c;
    }

    public int getTotal() {
        return this.f3091d;
    }

    public void setDestination(TransitResultNode transitResultNode) {
        this.f3089b = transitResultNode;
    }

    public void setOrigin(TransitResultNode transitResultNode) {
        this.f3088a = transitResultNode;
    }

    public void setRoutelines(List<MassTransitRouteLine> list) {
        this.f3092e = list;
    }

    public void setSuggestAddrInfo(SuggestAddrInfo suggestAddrInfo) {
        this.f3093f = suggestAddrInfo;
    }

    public void setTaxiInfo(TaxiInfo taxiInfo) {
        this.f3090c = taxiInfo;
    }

    public void setTotal(int i) {
        this.f3091d = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f3088a, 1);
        parcel.writeParcelable(this.f3089b, 1);
        parcel.writeParcelable(this.f3090c, 1);
        parcel.writeInt(this.f3091d);
        parcel.writeList(this.f3092e);
        parcel.writeParcelable(this.f3093f, 1);
    }
}
