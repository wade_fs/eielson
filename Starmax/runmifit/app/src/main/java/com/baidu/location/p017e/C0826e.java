package com.baidu.location.p017e;

import android.content.Context;
import android.location.GnssStatus;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.OnNmeaMessageListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.baidu.location.BDLocation;
import com.baidu.location.Jni;
import com.baidu.location.indoor.C0865g;
import com.baidu.location.p013a.C0723a;
import com.baidu.location.p013a.C0760t;
import com.baidu.location.p013a.C0765w;
import com.baidu.location.p013a.C0767x;
import com.baidu.location.p019g.C0846d;
import com.baidu.location.p019g.C0855k;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

/* renamed from: com.baidu.location.e.e */
public class C0826e {

    /* renamed from: C */
    private static String f1743C = null;
    /* access modifiers changed from: private */

    /* renamed from: G */
    public static double f1744G = 100.0d;

    /* renamed from: a */
    public static String f1745a = "";

    /* renamed from: b */
    public static String f1746b = "";

    /* renamed from: c */
    private static C0826e f1747c;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public static int f1748p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public static int f1749q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public static int f1750r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public static long f1751s;

    /* renamed from: A */
    private double f1752A = 0.0d;

    /* renamed from: B */
    private double f1753B = 0.0d;
    /* access modifiers changed from: private */

    /* renamed from: D */
    public Handler f1754D = null;
    /* access modifiers changed from: private */

    /* renamed from: E */
    public int f1755E;
    /* access modifiers changed from: private */

    /* renamed from: F */
    public int f1756F;

    /* renamed from: H */
    private long f1757H = 0;
    /* access modifiers changed from: private */

    /* renamed from: I */
    public long f1758I = 0;
    /* access modifiers changed from: private */

    /* renamed from: J */
    public ArrayList<ArrayList<Float>> f1759J = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: K */
    public ArrayList<ArrayList<Float>> f1760K = new ArrayList<>();

    /* renamed from: d */
    private Context f1761d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public LocationManager f1762e = null;

    /* renamed from: f */
    private Location f1763f;

    /* renamed from: g */
    private C0829c f1764g = null;

    /* renamed from: h */
    private C0831e f1765h = null;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public GpsStatus f1766i;

    /* renamed from: j */
    private C0827a f1767j;

    /* renamed from: k */
    private boolean f1768k = false;

    /* renamed from: l */
    private C0828b f1769l = null;

    /* renamed from: m */
    private boolean f1770m = false;

    /* renamed from: n */
    private C0830d f1771n = null;

    /* renamed from: o */
    private OnNmeaMessageListener f1772o = null;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public long f1773t = 0;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public boolean f1774u = false;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public boolean f1775v = false;

    /* renamed from: w */
    private String f1776w = null;

    /* renamed from: x */
    private boolean f1777x = false;
    /* access modifiers changed from: private */

    /* renamed from: y */
    public long f1778y = 0;

    /* renamed from: z */
    private double f1779z = -1.0d;

    /* renamed from: com.baidu.location.e.e$a */
    private class C0827a extends GnssStatus.Callback {
        private C0827a() {
        }

        /* synthetic */ C0827a(C0826e eVar, C0832f fVar) {
            this();
        }

        public void onFirstFix(int i) {
        }

        public void onSatelliteStatusChanged(GnssStatus gnssStatus) {
            if (C0826e.this.f1762e != null) {
                long unused = C0826e.this.f1758I = System.currentTimeMillis();
                int satelliteCount = gnssStatus.getSatelliteCount();
                C0826e.this.f1759J.clear();
                C0826e.this.f1760K.clear();
                int i = 0;
                int i2 = 0;
                int i3 = 0;
                for (int i4 = 0; i4 < satelliteCount; i4++) {
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    i3++;
                    if (gnssStatus.usedInFix(i4)) {
                        i++;
                        int constellationType = gnssStatus.getConstellationType(i4);
                        i2++;
                        arrayList.add(Float.valueOf(gnssStatus.getCn0DbHz(i4)));
                        arrayList.add(Float.valueOf(0.0f));
                        arrayList.add(Float.valueOf(gnssStatus.getAzimuthDegrees(i4)));
                        arrayList.add(Float.valueOf(gnssStatus.getElevationDegrees(i4)));
                        arrayList.add(Float.valueOf(1.0f));
                        arrayList.add(Float.valueOf((float) gnssStatus.getSvid(i4)));
                        C0826e.this.f1759J.add(arrayList);
                        C0826e.this.f1760K.add(arrayList);
                    } else {
                        int constellationType2 = gnssStatus.getConstellationType(i4);
                        arrayList2.add(Float.valueOf(gnssStatus.getCn0DbHz(i4)));
                        arrayList2.add(Float.valueOf(0.0f));
                        arrayList2.add(Float.valueOf(gnssStatus.getAzimuthDegrees(i4)));
                        arrayList2.add(Float.valueOf(gnssStatus.getElevationDegrees(i4)));
                        arrayList2.add(Float.valueOf(0.0f));
                        arrayList2.add(Float.valueOf((float) gnssStatus.getSvid(i4)));
                        C0826e.this.f1760K.add(arrayList2);
                    }
                }
                C0826e.f1745a = C0826e.this.m2337l();
                C0826e.f1746b = C0826e.this.m2338m();
                int unused2 = C0826e.f1748p = i;
                int unused3 = C0826e.f1749q = i2;
                int unused4 = C0826e.f1750r = i3;
                long unused5 = C0826e.f1751s = System.currentTimeMillis();
            }
        }

        public void onStarted() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void
         arg types: [com.baidu.location.e.e, int]
         candidates:
          com.baidu.location.e.e.a(com.baidu.location.e.e, int):int
          com.baidu.location.e.e.a(java.lang.String, java.lang.String):int
          com.baidu.location.e.e.a(com.baidu.location.e.e, long):long
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.Location):void
          com.baidu.location.e.e.a(java.lang.String, android.location.Location):void
          com.baidu.location.e.e.a(com.baidu.location.e.e, java.lang.String):boolean
          com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void */
        public void onStopped() {
            C0826e.this.m2329d((Location) null);
            C0826e.this.m2321b(false);
            int unused = C0826e.f1748p = 0;
            int unused2 = C0826e.f1749q = 0;
        }
    }

    /* renamed from: com.baidu.location.e.e$b */
    private class C0828b implements GpsStatus.Listener {

        /* renamed from: b */
        private long f1782b;

        private C0828b() {
            this.f1782b = 0;
        }

        /* synthetic */ C0828b(C0826e eVar, C0832f fVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void
         arg types: [com.baidu.location.e.e, int]
         candidates:
          com.baidu.location.e.e.a(com.baidu.location.e.e, int):int
          com.baidu.location.e.e.a(java.lang.String, java.lang.String):int
          com.baidu.location.e.e.a(com.baidu.location.e.e, long):long
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.Location):void
          com.baidu.location.e.e.a(java.lang.String, android.location.Location):void
          com.baidu.location.e.e.a(com.baidu.location.e.e, java.lang.String):boolean
          com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void */
        public void onGpsStatusChanged(int i) {
            long currentTimeMillis;
            if (C0826e.this.f1762e != null) {
                int i2 = 0;
                if (i == 2) {
                    C0826e.this.m2329d((Location) null);
                    C0826e.this.m2321b(false);
                    int unused = C0826e.f1748p = 0;
                    int unused2 = C0826e.f1749q = 0;
                } else if (i == 4 && C0826e.this.f1775v) {
                    try {
                        if (C0826e.this.f1766i == null) {
                            GpsStatus unused3 = C0826e.this.f1766i = C0826e.this.f1762e.getGpsStatus(null);
                        } else {
                            C0826e.this.f1762e.getGpsStatus(C0826e.this.f1766i);
                        }
                        int unused4 = C0826e.this.f1755E = 0;
                        int unused5 = C0826e.this.f1756F = 0;
                        double d = 0.0d;
                        C0826e.this.f1759J.clear();
                        C0826e.this.f1760K.clear();
                        long unused6 = C0826e.this.f1758I = System.currentTimeMillis();
                        int i3 = 0;
                        for (GpsSatellite gpsSatellite : C0826e.this.f1766i.getSatellites()) {
                            ArrayList arrayList = new ArrayList();
                            ArrayList arrayList2 = new ArrayList();
                            if (gpsSatellite.usedInFix()) {
                                i3++;
                                int prn = gpsSatellite.getPrn();
                                i2++;
                                double snr = (double) gpsSatellite.getSnr();
                                Double.isNaN(snr);
                                d += snr;
                                arrayList.add(Float.valueOf(0.0f));
                                arrayList.add(Float.valueOf(gpsSatellite.getSnr()));
                                arrayList.add(Float.valueOf(gpsSatellite.getAzimuth()));
                                arrayList.add(Float.valueOf(gpsSatellite.getElevation()));
                                arrayList.add(Float.valueOf(1.0f));
                                arrayList.add(Float.valueOf((float) gpsSatellite.getPrn()));
                                C0826e.this.f1759J.add(arrayList);
                                C0826e.this.f1760K.add(arrayList);
                            } else {
                                int prn2 = gpsSatellite.getPrn();
                                arrayList2.add(Float.valueOf(0.0f));
                                arrayList2.add(Float.valueOf(gpsSatellite.getSnr()));
                                arrayList2.add(Float.valueOf(gpsSatellite.getAzimuth()));
                                arrayList2.add(Float.valueOf(gpsSatellite.getElevation()));
                                arrayList2.add(Float.valueOf(0.0f));
                                arrayList2.add(Float.valueOf((float) gpsSatellite.getPrn()));
                                C0826e.this.f1760K.add(arrayList2);
                            }
                            if (gpsSatellite.getSnr() >= ((float) C0855k.f1907H)) {
                                C0826e.m2335i(C0826e.this);
                            }
                        }
                        C0826e.f1745a = C0826e.this.m2337l();
                        C0826e.f1746b = C0826e.this.m2338m();
                        if (i2 > 0) {
                            int unused7 = C0826e.f1749q = i2;
                            double d2 = (double) i2;
                            Double.isNaN(d2);
                            double unused8 = C0826e.f1744G = d / d2;
                        }
                        if (i3 > 0) {
                            currentTimeMillis = System.currentTimeMillis();
                        } else {
                            if (System.currentTimeMillis() - this.f1782b > 100) {
                                currentTimeMillis = System.currentTimeMillis();
                            }
                            long unused9 = C0826e.f1751s = System.currentTimeMillis();
                        }
                        this.f1782b = currentTimeMillis;
                        int unused10 = C0826e.f1748p = i3;
                        long unused11 = C0826e.f1751s = System.currentTimeMillis();
                    } catch (Exception unused12) {
                    }
                }
            }
        }
    }

    /* renamed from: com.baidu.location.e.e$c */
    private class C0829c implements LocationListener {
        private C0829c() {
        }

        /* synthetic */ C0829c(C0826e eVar, C0832f fVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void
         arg types: [com.baidu.location.e.e, int]
         candidates:
          com.baidu.location.e.e.a(com.baidu.location.e.e, int):int
          com.baidu.location.e.e.a(java.lang.String, java.lang.String):int
          com.baidu.location.e.e.a(com.baidu.location.e.e, long):long
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.Location):void
          com.baidu.location.e.e.a(java.lang.String, android.location.Location):void
          com.baidu.location.e.e.a(com.baidu.location.e.e, java.lang.String):boolean
          com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.e.e.b(com.baidu.location.e.e, boolean):boolean
         arg types: [com.baidu.location.e.e, int]
         candidates:
          com.baidu.location.e.e.b(com.baidu.location.e.e, int):int
          com.baidu.location.e.e.b(com.baidu.location.e.e, long):long
          com.baidu.location.e.e.b(com.baidu.location.e.e, android.location.Location):void
          com.baidu.location.e.e.b(com.baidu.location.e.e, boolean):boolean */
        public void onLocationChanged(Location location) {
            if (location != null && Math.abs(location.getLatitude()) <= 360.0d && Math.abs(location.getLongitude()) <= 360.0d) {
                long unused = C0826e.this.f1778y = System.currentTimeMillis();
                C0826e.this.m2321b(true);
                C0826e.this.m2329d(location);
                boolean unused2 = C0826e.this.f1774u = false;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void
         arg types: [com.baidu.location.e.e, int]
         candidates:
          com.baidu.location.e.e.a(com.baidu.location.e.e, int):int
          com.baidu.location.e.e.a(java.lang.String, java.lang.String):int
          com.baidu.location.e.e.a(com.baidu.location.e.e, long):long
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.Location):void
          com.baidu.location.e.e.a(java.lang.String, android.location.Location):void
          com.baidu.location.e.e.a(com.baidu.location.e.e, java.lang.String):boolean
          com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void */
        public void onProviderDisabled(String str) {
            C0826e.this.m2329d((Location) null);
            C0826e.this.m2321b(false);
        }

        public void onProviderEnabled(String str) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.e.e.b(com.baidu.location.e.e, boolean):boolean
         arg types: [com.baidu.location.e.e, int]
         candidates:
          com.baidu.location.e.e.b(com.baidu.location.e.e, int):int
          com.baidu.location.e.e.b(com.baidu.location.e.e, long):long
          com.baidu.location.e.e.b(com.baidu.location.e.e, android.location.Location):void
          com.baidu.location.e.e.b(com.baidu.location.e.e, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void
         arg types: [com.baidu.location.e.e, int]
         candidates:
          com.baidu.location.e.e.a(com.baidu.location.e.e, int):int
          com.baidu.location.e.e.a(java.lang.String, java.lang.String):int
          com.baidu.location.e.e.a(com.baidu.location.e.e, long):long
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.e.e.a(com.baidu.location.e.e, android.location.Location):void
          com.baidu.location.e.e.a(java.lang.String, android.location.Location):void
          com.baidu.location.e.e.a(com.baidu.location.e.e, java.lang.String):boolean
          com.baidu.location.e.e.a(com.baidu.location.e.e, boolean):void */
        public void onStatusChanged(String str, int i, Bundle bundle) {
            if (i == 0) {
                C0826e.this.m2329d((Location) null);
            } else if (i == 1) {
                long unused = C0826e.this.f1773t = System.currentTimeMillis();
                boolean unused2 = C0826e.this.f1774u = true;
            } else if (i == 2) {
                boolean unused3 = C0826e.this.f1774u = false;
                return;
            } else {
                return;
            }
            C0826e.this.m2321b(false);
        }
    }

    /* renamed from: com.baidu.location.e.e$d */
    private class C0830d implements GpsStatus.NmeaListener {
        private C0830d() {
        }

        /* synthetic */ C0830d(C0826e eVar, C0832f fVar) {
            this();
        }

        public void onNmeaReceived(long j, String str) {
            if (C0826e.this.m2323b(str)) {
                C0826e.this.mo10637a(str);
            }
        }
    }

    /* renamed from: com.baidu.location.e.e$e */
    private class C0831e implements LocationListener {

        /* renamed from: b */
        private long f1786b;

        private C0831e() {
            this.f1786b = 0;
        }

        /* synthetic */ C0831e(C0826e eVar, C0832f fVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.w.a(android.location.Location, boolean):boolean
         arg types: [android.location.Location, int]
         candidates:
          com.baidu.location.a.w.a(java.util.List<java.lang.String>, int):int
          com.baidu.location.a.w.a(int, boolean):void
          com.baidu.location.a.w.a(android.location.Location, com.baidu.location.e.h):boolean
          com.baidu.location.a.w.a(java.lang.String, java.util.List<java.lang.String>):boolean
          com.baidu.location.a.w.a(android.location.Location, boolean):boolean */
        public void onLocationChanged(Location location) {
            if (!C0826e.this.f1775v && location != null && location.getProvider() == "gps" && System.currentTimeMillis() - this.f1786b >= 10000 && Math.abs(location.getLatitude()) <= 360.0d && Math.abs(location.getLongitude()) <= 360.0d && C0765w.m1957a(location, false)) {
                this.f1786b = System.currentTimeMillis();
                C0826e.this.f1754D.sendMessage(C0826e.this.f1754D.obtainMessage(4, location));
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }

    private C0826e() {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Class.forName("android.location.GnssStatus");
                this.f1768k = true;
            } catch (ClassNotFoundException unused) {
                this.f1768k = false;
            }
        }
        this.f1770m = false;
    }

    /* renamed from: a */
    public static int m2302a(String str, String str2) {
        char charAt = str2.charAt(0);
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (Character.valueOf(charAt).equals(Character.valueOf(str.charAt(i2)))) {
                i++;
            }
        }
        return i;
    }

    /* renamed from: a */
    public static synchronized C0826e m2306a() {
        C0826e eVar;
        synchronized (C0826e.class) {
            if (f1747c == null) {
                f1747c = new C0826e();
            }
            eVar = f1747c;
        }
        return eVar;
    }

    /* renamed from: a */
    public static String m2307a(Location location) {
        if (location == null) {
            return null;
        }
        double speed = (double) location.getSpeed();
        Double.isNaN(speed);
        float f = (float) (speed * 3.6d);
        float f2 = -1.0f;
        if (!location.hasSpeed()) {
            f = -1.0f;
        }
        int accuracy = (int) (location.hasAccuracy() ? location.getAccuracy() : -1.0f);
        double altitude = location.hasAltitude() ? location.getAltitude() : 555.0d;
        if (location.hasBearing()) {
            f2 = location.getBearing();
        }
        return String.format(Locale.CHINA, "&ll=%.5f|%.5f&s=%.1f&d=%.1f&ll_r=%d&ll_n=%d&ll_h=%.2f&ll_t=%d&ll_sn=%d|%d&ll_snr=%.1f", Double.valueOf(location.getLongitude()), Double.valueOf(location.getLatitude()), Float.valueOf(f), Float.valueOf(f2), Integer.valueOf(accuracy), Integer.valueOf(f1748p), Double.valueOf(altitude), Long.valueOf(location.getTime() / 1000), Integer.valueOf(f1748p), Integer.valueOf(f1749q), Double.valueOf(f1744G));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2311a(String str, Location location) {
        if (location != null) {
            String str2 = str + C0723a.m1729a().mo10432d();
            boolean f = C0835i.m2376a().mo10690f();
            C0760t.m1937a(new C0821a(C0822b.m2279a().mo10631f()));
            C0760t.m1935a(System.currentTimeMillis());
            C0760t.m1936a(new Location(location));
            C0760t.m1938a(str2);
            if (!f) {
                C0765w.m1952a(C0760t.m1940c(), null, C0760t.m1941d(), str2);
            }
        }
    }

    /* renamed from: a */
    public static boolean m2312a(Location location, Location location2, boolean z) {
        if (location == location2) {
            return false;
        }
        if (location == null || location2 == null) {
            return true;
        }
        float speed = location2.getSpeed();
        if (z && ((C0855k.f1978v == 3 || !C0846d.m2434a().mo10727a(location2.getLongitude(), location2.getLatitude())) && speed < 5.0f)) {
            return true;
        }
        float distanceTo = location2.distanceTo(location);
        return speed > C0855k.f1911L ? distanceTo > C0855k.f1913N : speed > C0855k.f1910K ? distanceTo > C0855k.f1912M : distanceTo > 5.0f;
    }

    /* renamed from: b */
    public static String m2319b(Location location) {
        String a = m2307a(location);
        if (a == null) {
            return a;
        }
        return a + "&g_tp=0";
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m2321b(boolean z) {
        this.f1777x = z;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m2323b(String str) {
        int i;
        if (str.indexOf("*") != -1 && str.indexOf("$") != -1 && str.indexOf("$") <= str.indexOf("*") && str.length() >= str.indexOf("*")) {
            byte[] bytes = str.substring(0, str.indexOf("*")).getBytes();
            byte b = bytes[1];
            for (int i2 = 2; i2 < bytes.length; i2++) {
                b ^= bytes[i2];
            }
            String format = String.format("%02x", Integer.valueOf(b));
            int indexOf = str.indexOf("*");
            if (indexOf != -1 && str.length() >= (i = indexOf + 3) && format.equalsIgnoreCase(str.substring(indexOf + 1, i))) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: c */
    public static String m2327c(Location location) {
        String a = m2307a(location);
        if (a == null) {
            return a;
        }
        return a + f1743C;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m2329d(Location location) {
        this.f1754D.sendMessage(this.f1754D.obtainMessage(1, location));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.a.w.a(android.location.Location, boolean):boolean
     arg types: [android.location.Location, int]
     candidates:
      com.baidu.location.a.w.a(java.util.List<java.lang.String>, int):int
      com.baidu.location.a.w.a(int, boolean):void
      com.baidu.location.a.w.a(android.location.Location, com.baidu.location.e.h):boolean
      com.baidu.location.a.w.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.baidu.location.a.w.a(android.location.Location, boolean):boolean */
    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m2331e(Location location) {
        String str = null;
        if (location != null) {
            int i = f1748p;
            if (i == 0) {
                try {
                    i = location.getExtras().getInt("satellites");
                } catch (Exception unused) {
                }
            }
            if (i != 0 || C0855k.f1969m) {
                if (this.f1770m && ((double) location.getSpeed()) == 0.0d && this.f1752A != 0.0d) {
                    double currentTimeMillis = (double) System.currentTimeMillis();
                    double d = this.f1753B;
                    Double.isNaN(currentTimeMillis);
                    if (currentTimeMillis - d < 2000.0d) {
                        location.setSpeed((float) this.f1752A);
                    }
                }
                System.currentTimeMillis();
                this.f1763f = location;
                Location location2 = new Location(this.f1763f);
                int i2 = f1748p;
                if (this.f1763f != null) {
                    long currentTimeMillis2 = System.currentTimeMillis();
                    this.f1763f.setTime(currentTimeMillis2);
                    double speed = (double) this.f1763f.getSpeed();
                    Double.isNaN(speed);
                    float f = (float) (speed * 3.6d);
                    if (!this.f1763f.hasSpeed()) {
                        f = -1.0f;
                    }
                    if (i2 == 0) {
                        try {
                            i2 = this.f1763f.getExtras().getInt("satellites");
                        } catch (Exception unused2) {
                        }
                    }
                    str = String.format(Locale.CHINA, "&ll=%.5f|%.5f&s=%.1f&d=%.1f&ll_n=%d&ll_t=%d", Double.valueOf(this.f1763f.getLongitude()), Double.valueOf(this.f1763f.getLatitude()), Float.valueOf(f), Float.valueOf(this.f1763f.getBearing()), Integer.valueOf(i2), Long.valueOf(currentTimeMillis2));
                }
                this.f1776w = str;
                if (mo10647j() && this.f1763f != null) {
                    if (C0865g.m2519a().mo10768e()) {
                        C0865g.m2519a().mo10762a(this.f1763f, this.f1759J);
                    }
                    if (!C0865g.m2519a().mo10769f()) {
                        boolean f2 = C0865g.m2519a().mo10769f();
                        C0723a.m1729a().mo10423a(mo10644g());
                    }
                    if (f1748p > 2 && C0765w.m1957a(this.f1763f, true)) {
                        boolean f3 = C0835i.m2376a().mo10690f();
                        C0760t.m1937a(new C0821a(C0822b.m2279a().mo10631f()));
                        C0760t.m1935a(System.currentTimeMillis());
                        C0760t.m1936a(new Location(this.f1763f));
                        C0760t.m1938a(C0723a.m1729a().mo10432d());
                        if (!f3) {
                            C0767x.m1972a().mo10510b();
                        }
                    }
                }
                C0767x.m1972a().mo10509a(location2, f1748p);
                return;
            }
            return;
        }
        this.f1763f = null;
    }

    /* renamed from: i */
    static /* synthetic */ int m2335i(C0826e eVar) {
        int i = eVar.f1756F;
        eVar.f1756F = i + 1;
        return i;
    }

    /* renamed from: k */
    public static String m2336k() {
        long currentTimeMillis = System.currentTimeMillis() - f1751s;
        if (currentTimeMillis < 0 || currentTimeMillis >= 3000) {
            return null;
        }
        return String.format(Locale.US, "&gsvn=%d&gsfn=%d", Integer.valueOf(f1750r), Integer.valueOf(f1748p));
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public String m2337l() {
        StringBuilder sb = new StringBuilder();
        if (this.f1759J.size() > 32 || this.f1759J.size() == 0) {
            return sb.toString();
        }
        Iterator<ArrayList<Float>> it = this.f1759J.iterator();
        boolean z = true;
        while (it.hasNext()) {
            ArrayList next = it.next();
            if (next.size() == 6) {
                if (z) {
                    z = false;
                } else {
                    sb.append("|");
                }
                sb.append(String.format("%.1f;", next.get(0)));
                sb.append(String.format("%.1f;", next.get(1)));
                sb.append(String.format("%.0f;", next.get(2)));
                sb.append(String.format("%.0f;", next.get(3)));
                sb.append(String.format("%.0f", next.get(4)));
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public String m2338m() {
        StringBuilder sb = new StringBuilder();
        if (this.f1760K.size() == 0) {
            return sb.toString();
        }
        Iterator<ArrayList<Float>> it = this.f1760K.iterator();
        boolean z = true;
        while (it.hasNext()) {
            ArrayList next = it.next();
            if (next.size() == 6) {
                if (z) {
                    z = false;
                } else {
                    sb.append("|");
                }
                sb.append(String.format("%.1f;", next.get(0)));
                sb.append(String.format("%.1f;", next.get(1)));
                sb.append(String.format("%.0f;", next.get(2)));
                sb.append(String.format("%.0f;", next.get(3)));
                sb.append(String.format("%.0f", next.get(4)));
                sb.append(String.format("%.0f", next.get(5)));
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    public void mo10637a(String str) {
        if (str.length() == 0 || !m2323b(str)) {
            return;
        }
        if (str.startsWith("$GPPWR,") || str.startsWith("$GNGST,") || str.startsWith("$GPGST,") || str.startsWith("$GLGSV,") || str.startsWith("$GNGSV,") || str.startsWith("$BDGSV,") || str.startsWith("$GPZDA,") || str.startsWith("$GPGSA,") || str.startsWith("$GNVTG,") || str.startsWith("$GPVTG,") || str.startsWith("$GNGSA,") || str.startsWith("$GPNTR,") || str.startsWith("$GNGGA,") || str.startsWith("$GPGGA,") || str.startsWith("$GPRMC,") || str.startsWith("$GPGSV,") || str.startsWith("$BDGSA,")) {
            String[] split = str.split(",");
            m2302a(str, ",");
            if (split == null || split.length <= 0) {
                return;
            }
            if ((split[0].equalsIgnoreCase("$GPRMC") || split[0].equalsIgnoreCase("$GNRMC") || split[0].equalsIgnoreCase("$GLRMC") || split[0].equalsIgnoreCase("$BDRMC")) && split.length > 7 && split[7].trim().length() > 0) {
                this.f1752A = ((Double.valueOf(split[7]).doubleValue() * 1.852d) / 3600.0d) * 1000.0d;
                this.f1753B = (double) System.currentTimeMillis();
            }
        }
    }

    /* renamed from: a */
    public void mo10638a(boolean z) {
        if (z) {
            mo10640c();
        } else {
            mo10641d();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(12:6|7|8|9|(1:11)(1:12)|13|(2:15|(1:17)(1:18))|19|20|21|22|23) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0075 */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo10639b() {
        /*
            r8 = this;
            monitor-enter(r8)
            boolean r0 = com.baidu.location.C0839f.isServing     // Catch:{ all -> 0x007e }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r8)
            return
        L_0x0007:
            android.content.Context r0 = com.baidu.location.C0839f.getServiceContext()     // Catch:{ all -> 0x007e }
            r8.f1761d = r0     // Catch:{ all -> 0x007e }
            android.content.Context r0 = r8.f1761d     // Catch:{ Exception -> 0x0075 }
            java.lang.String r1 = "location"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0075 }
            android.location.LocationManager r0 = (android.location.LocationManager) r0     // Catch:{ Exception -> 0x0075 }
            r8.f1762e = r0     // Catch:{ Exception -> 0x0075 }
            boolean r0 = r8.f1768k     // Catch:{ Exception -> 0x0075 }
            r1 = 0
            if (r0 != 0) goto L_0x002d
            com.baidu.location.e.e$b r0 = new com.baidu.location.e.e$b     // Catch:{ Exception -> 0x0075 }
            r0.<init>(r8, r1)     // Catch:{ Exception -> 0x0075 }
            r8.f1769l = r0     // Catch:{ Exception -> 0x0075 }
            android.location.LocationManager r0 = r8.f1762e     // Catch:{ Exception -> 0x0075 }
            com.baidu.location.e.e$b r2 = r8.f1769l     // Catch:{ Exception -> 0x0075 }
            r0.addGpsStatusListener(r2)     // Catch:{ Exception -> 0x0075 }
            goto L_0x003b
        L_0x002d:
            com.baidu.location.e.e$a r0 = new com.baidu.location.e.e$a     // Catch:{ Exception -> 0x0075 }
            r0.<init>(r8, r1)     // Catch:{ Exception -> 0x0075 }
            r8.f1767j = r0     // Catch:{ Exception -> 0x0075 }
            android.location.LocationManager r0 = r8.f1762e     // Catch:{ Exception -> 0x0075 }
            com.baidu.location.e.e$a r2 = r8.f1767j     // Catch:{ Exception -> 0x0075 }
            r0.registerGnssStatusCallback(r2)     // Catch:{ Exception -> 0x0075 }
        L_0x003b:
            boolean r0 = r8.f1770m     // Catch:{ Exception -> 0x0075 }
            if (r0 == 0) goto L_0x0062
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0075 }
            r2 = 24
            if (r0 < r2) goto L_0x0054
            com.baidu.location.e.f r0 = new com.baidu.location.e.f     // Catch:{ Exception -> 0x0075 }
            r0.<init>(r8)     // Catch:{ Exception -> 0x0075 }
            r8.f1772o = r0     // Catch:{ Exception -> 0x0075 }
            android.location.LocationManager r0 = r8.f1762e     // Catch:{ Exception -> 0x0075 }
            android.location.OnNmeaMessageListener r2 = r8.f1772o     // Catch:{ Exception -> 0x0075 }
            r0.addNmeaListener(r2)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0062
        L_0x0054:
            com.baidu.location.e.e$d r0 = new com.baidu.location.e.e$d     // Catch:{ Exception -> 0x0075 }
            r0.<init>(r8, r1)     // Catch:{ Exception -> 0x0075 }
            r8.f1771n = r0     // Catch:{ Exception -> 0x0075 }
            android.location.LocationManager r0 = r8.f1762e     // Catch:{ Exception -> 0x0075 }
            com.baidu.location.e.e$d r2 = r8.f1771n     // Catch:{ Exception -> 0x0075 }
            r0.addNmeaListener(r2)     // Catch:{ Exception -> 0x0075 }
        L_0x0062:
            com.baidu.location.e.e$e r0 = new com.baidu.location.e.e$e     // Catch:{ Exception -> 0x0075 }
            r0.<init>(r8, r1)     // Catch:{ Exception -> 0x0075 }
            r8.f1765h = r0     // Catch:{ Exception -> 0x0075 }
            android.location.LocationManager r2 = r8.f1762e     // Catch:{ Exception -> 0x0075 }
            java.lang.String r3 = "passive"
            r4 = 9000(0x2328, double:4.4466E-320)
            r6 = 0
            com.baidu.location.e.e$e r7 = r8.f1765h     // Catch:{ Exception -> 0x0075 }
            r2.requestLocationUpdates(r3, r4, r6, r7)     // Catch:{ Exception -> 0x0075 }
        L_0x0075:
            com.baidu.location.e.g r0 = new com.baidu.location.e.g     // Catch:{ all -> 0x007e }
            r0.<init>(r8)     // Catch:{ all -> 0x007e }
            r8.f1754D = r0     // Catch:{ all -> 0x007e }
            monitor-exit(r8)
            return
        L_0x007e:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0826e.mo10639b():void");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:3|4|5|6|7|8|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0022 */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10640c() {
        /*
            r10 = this;
            java.lang.String r0 = com.baidu.location.p019g.C0843a.f1826a
            java.lang.String r1 = "start gps..."
            android.util.Log.d(r0, r1)
            boolean r0 = r10.f1775v
            if (r0 == 0) goto L_0x000c
            return
        L_0x000c:
            com.baidu.location.e.e$c r0 = new com.baidu.location.e.e$c     // Catch:{ Exception -> 0x0037 }
            r1 = 0
            r0.<init>(r10, r1)     // Catch:{ Exception -> 0x0037 }
            r10.f1764g = r0     // Catch:{ Exception -> 0x0037 }
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ Exception -> 0x0022 }
            r0.<init>()     // Catch:{ Exception -> 0x0022 }
            android.location.LocationManager r1 = r10.f1762e     // Catch:{ Exception -> 0x0022 }
            java.lang.String r2 = "gps"
            java.lang.String r3 = "force_xtra_injection"
            r1.sendExtraCommand(r2, r3, r0)     // Catch:{ Exception -> 0x0022 }
        L_0x0022:
            android.location.LocationManager r4 = r10.f1762e     // Catch:{ Exception -> 0x0037 }
            java.lang.String r5 = "gps"
            r6 = 1000(0x3e8, double:4.94E-321)
            r8 = 0
            com.baidu.location.e.e$c r9 = r10.f1764g     // Catch:{ Exception -> 0x0037 }
            r4.requestLocationUpdates(r5, r6, r8, r9)     // Catch:{ Exception -> 0x0037 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0037 }
            r10.f1757H = r0     // Catch:{ Exception -> 0x0037 }
            r0 = 1
            r10.f1775v = r0     // Catch:{ Exception -> 0x0037 }
        L_0x0037:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0826e.mo10640c():void");
    }

    /* renamed from: d */
    public void mo10641d() {
        if (this.f1775v) {
            LocationManager locationManager = this.f1762e;
            if (locationManager != null) {
                try {
                    if (this.f1764g != null) {
                        locationManager.removeUpdates(this.f1764g);
                    }
                } catch (Exception unused) {
                }
            }
            C0855k.f1960d = 0;
            C0855k.f1978v = 0;
            this.f1764g = null;
            this.f1775v = false;
            m2321b(false);
        }
    }

    /* renamed from: e */
    public synchronized void mo10642e() {
        mo10641d();
        if (this.f1762e != null) {
            try {
                if (this.f1769l != null) {
                    this.f1762e.removeGpsStatusListener(this.f1769l);
                }
                if (this.f1768k && this.f1767j != null) {
                    this.f1762e.unregisterGnssStatusCallback(this.f1767j);
                }
                if (this.f1770m && this.f1771n != null) {
                    this.f1762e.removeNmeaListener(this.f1771n);
                }
                this.f1762e.removeUpdates(this.f1765h);
            } catch (Exception unused) {
            }
            this.f1769l = null;
            this.f1762e = null;
        }
    }

    /* renamed from: f */
    public String mo10643f() {
        Location location;
        if (!mo10647j() || (location = this.f1763f) == null) {
            return null;
        }
        return String.format("%s&idgps_tp=%s", m2307a(location).replaceAll("ll", "idll").replaceAll("&d=", "&idd=").replaceAll("&s", "&ids="), this.f1763f.getProvider());
    }

    /* renamed from: g */
    public String mo10644g() {
        boolean z;
        double[] dArr;
        StringBuilder sb;
        String str;
        if (this.f1763f == null) {
            return null;
        }
        String str2 = "{\"result\":{\"time\":\"" + C0855k.m2452a() + "\",\"error\":\"61\"},\"content\":{\"point\":{\"x\":" + "\"%f\",\"y\":\"%f\"},\"radius\":\"%d\",\"d\":\"%f\"," + "\"s\":\"%f\",\"n\":\"%d\"";
        int accuracy = (int) (this.f1763f.hasAccuracy() ? this.f1763f.getAccuracy() : 10.0f);
        double speed = (double) this.f1763f.getSpeed();
        Double.isNaN(speed);
        float f = (float) (speed * 3.6d);
        if (!this.f1763f.hasSpeed()) {
            f = -1.0f;
        }
        double[] dArr2 = new double[2];
        if (C0846d.m2434a().mo10727a(this.f1763f.getLongitude(), this.f1763f.getLatitude())) {
            dArr = Jni.coorEncrypt(this.f1763f.getLongitude(), this.f1763f.getLatitude(), BDLocation.BDLOCATION_WGS84_TO_GCJ02);
            if (dArr[0] <= 0.0d && dArr[1] <= 0.0d) {
                dArr[0] = this.f1763f.getLongitude();
                dArr[1] = this.f1763f.getLatitude();
            }
            z = true;
        } else {
            dArr2[0] = this.f1763f.getLongitude();
            dArr2[1] = this.f1763f.getLatitude();
            dArr = Jni.coorEncrypt(this.f1763f.getLongitude(), this.f1763f.getLatitude(), BDLocation.BDLOCATION_WGS84_TO_GCJ02);
            if (dArr[0] <= 0.0d && dArr[1] <= 0.0d) {
                dArr[0] = this.f1763f.getLongitude();
                dArr[1] = this.f1763f.getLatitude();
            }
            z = false;
        }
        String format = String.format(Locale.CHINA, str2, Double.valueOf(dArr[0]), Double.valueOf(dArr[1]), Integer.valueOf(accuracy), Float.valueOf(this.f1763f.getBearing()), Float.valueOf(f), Integer.valueOf(f1748p));
        if (!z) {
            format = format + ",\"in_cn\":\"0\"";
        }
        if (this.f1763f.hasAltitude()) {
            sb = new StringBuilder();
            sb.append(format);
            str = String.format(Locale.CHINA, ",\"h\":%.2f}}", Double.valueOf(this.f1763f.getAltitude()));
        } else {
            sb = new StringBuilder();
            sb.append(format);
            str = "}}";
        }
        sb.append(str);
        return sb.toString();
    }

    /* renamed from: h */
    public Location mo10645h() {
        if (this.f1763f != null && Math.abs(System.currentTimeMillis() - this.f1763f.getTime()) <= 60000) {
            return this.f1763f;
        }
        return null;
    }

    /* renamed from: i */
    public boolean mo10646i() {
        try {
            return (this.f1763f == null || this.f1763f.getLatitude() == 0.0d || this.f1763f.getLongitude() == 0.0d || (f1748p <= 2 && this.f1763f.getExtras().getInt("satellites", 3) <= 2 && Math.abs(System.currentTimeMillis() - this.f1758I) >= 5000)) ? false : true;
        } catch (Exception unused) {
            Location location = this.f1763f;
            return (location == null || location.getLatitude() == 0.0d || this.f1763f.getLongitude() == 0.0d) ? false : true;
        }
    }

    /* renamed from: j */
    public boolean mo10647j() {
        if (!mo10646i() || System.currentTimeMillis() - this.f1778y > 10000) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (!this.f1774u || currentTimeMillis - this.f1773t >= 3000) {
            return this.f1777x;
        }
        return true;
    }
}
