package com.baidu.location.indoor;

import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;

/* renamed from: com.baidu.location.indoor.f */
class C0864f extends ScanCallback {

    /* renamed from: a */
    final /* synthetic */ C0860d f2017a;

    C0864f(C0860d dVar) {
        this.f2017a = dVar;
    }

    public void onScanResult(int i, ScanResult scanResult) {
        if (this.f2017a.f2009l != null) {
            this.f2017a.f2009l.put(scanResult.getDevice().getAddress(), scanResult);
        }
    }
}
