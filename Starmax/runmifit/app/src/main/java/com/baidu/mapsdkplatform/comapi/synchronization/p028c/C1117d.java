package com.baidu.mapsdkplatform.comapi.synchronization.p028c;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.c.d */
class C1117d implements HostnameVerifier {

    /* renamed from: a */
    final /* synthetic */ C1115c f3696a;

    C1117d(C1115c cVar) {
        this.f3696a = cVar;
    }

    public boolean verify(String str, SSLSession sSLSession) {
        return HttpsURLConnection.getDefaultHostnameVerifier().verify(str, sSLSession);
    }
}
