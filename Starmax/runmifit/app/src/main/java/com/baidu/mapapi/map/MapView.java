package com.baidu.mapapi.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.MapViewLayoutParams;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapsdkplatform.comapi.commonutils.C1053a;
import com.baidu.mapsdkplatform.comapi.map.C1074ak;
import com.baidu.mapsdkplatform.comapi.map.C1078e;
import com.baidu.mapsdkplatform.comapi.map.C1083i;
import com.baidu.mapsdkplatform.comapi.map.C1084j;
import com.baidu.mapsdkplatform.comapi.map.C1087l;
import com.baidu.mapsdkplatform.comapi.util.CustomMapStyleLoader;
import java.io.File;

public final class MapView extends ViewGroup {

    /* renamed from: a */
    private static final String f2566a = MapView.class.getSimpleName();

    /* renamed from: b */
    private static String f2567b;

    /* renamed from: c */
    private static int f2568c = 0;

    /* renamed from: d */
    private static int f2569d = 0;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public static final SparseArray<Integer> f2570q = new SparseArray<>();

    /* renamed from: A */
    private int f2571A;

    /* renamed from: B */
    private int f2572B;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C1084j f2573e;

    /* renamed from: f */
    private BaiduMap f2574f;

    /* renamed from: g */
    private ImageView f2575g;

    /* renamed from: h */
    private Bitmap f2576h;

    /* renamed from: i */
    private C1074ak f2577i;

    /* renamed from: j */
    private Point f2578j;

    /* renamed from: k */
    private Point f2579k;

    /* renamed from: l */
    private RelativeLayout f2580l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public TextView f2581m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public TextView f2582n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public ImageView f2583o;

    /* renamed from: p */
    private Context f2584p;

    /* renamed from: r */
    private int f2585r = LogoPosition.logoPostionleftBottom.ordinal();

    /* renamed from: s */
    private boolean f2586s = true;

    /* renamed from: t */
    private boolean f2587t = true;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public float f2588u;

    /* renamed from: v */
    private C1087l f2589v;

    /* renamed from: w */
    private int f2590w;

    /* renamed from: x */
    private int f2591x;

    /* renamed from: y */
    private int f2592y;

    /* renamed from: z */
    private int f2593z;

    static {
        f2570q.append(3, 2000000);
        f2570q.append(4, 1000000);
        f2570q.append(5, 500000);
        f2570q.append(6, 200000);
        f2570q.append(7, 100000);
        f2570q.append(8, 50000);
        f2570q.append(9, 25000);
        f2570q.append(10, 20000);
        f2570q.append(11, 10000);
        f2570q.append(12, 5000);
        f2570q.append(13, 2000);
        f2570q.append(14, 1000);
        f2570q.append(15, 500);
        f2570q.append(16, 200);
        f2570q.append(17, 100);
        f2570q.append(18, 50);
        f2570q.append(19, 20);
        f2570q.append(20, 10);
        f2570q.append(21, 5);
        f2570q.append(22, 2);
        f2570q.append(23, 2);
        f2570q.append(24, 2);
        f2570q.append(25, 2);
        f2570q.append(26, 2);
    }

    public MapView(Context context) {
        super(context);
        m2924a(context, (BaiduMapOptions) null);
    }

    public MapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m2924a(context, (BaiduMapOptions) null);
    }

    public MapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m2924a(context, (BaiduMapOptions) null);
    }

    public MapView(Context context, BaiduMapOptions baiduMapOptions) {
        super(context);
        m2924a(context, baiduMapOptions);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2923a(android.content.Context r10) {
        /*
            r9 = this;
            int r0 = com.baidu.mapapi.common.SysOSUtil.getDensityDpi()
            r1 = 180(0xb4, float:2.52E-43)
            if (r0 >= r1) goto L_0x000b
            java.lang.String r1 = "logo_l.png"
            goto L_0x000d
        L_0x000b:
            java.lang.String r1 = "logo_h.png"
        L_0x000d:
            android.graphics.Bitmap r2 = com.baidu.mapsdkplatform.comapi.commonutils.C1053a.m3464a(r1, r10)
            r1 = 480(0x1e0, float:6.73E-43)
            if (r0 <= r1) goto L_0x0031
            android.graphics.Matrix r7 = new android.graphics.Matrix
            r7.<init>()
            r0 = 1073741824(0x40000000, float:2.0)
        L_0x001c:
            r7.postScale(r0, r0)
            r3 = 0
            r4 = 0
            int r5 = r2.getWidth()
            int r6 = r2.getHeight()
            r8 = 1
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r2, r3, r4, r5, r6, r7, r8)
            r9.f2576h = r0
            goto L_0x0041
        L_0x0031:
            r3 = 320(0x140, float:4.48E-43)
            if (r0 <= r3) goto L_0x003f
            if (r0 > r1) goto L_0x003f
            android.graphics.Matrix r7 = new android.graphics.Matrix
            r7.<init>()
            r0 = 1069547520(0x3fc00000, float:1.5)
            goto L_0x001c
        L_0x003f:
            r9.f2576h = r2
        L_0x0041:
            android.graphics.Bitmap r0 = r9.f2576h
            if (r0 == 0) goto L_0x0058
            android.widget.ImageView r0 = new android.widget.ImageView
            r0.<init>(r10)
            r9.f2575g = r0
            android.widget.ImageView r10 = r9.f2575g
            android.graphics.Bitmap r0 = r9.f2576h
            r10.setImageBitmap(r0)
            android.widget.ImageView r10 = r9.f2575g
            r9.addView(r10)
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.map.MapView.m2923a(android.content.Context):void");
    }

    /* renamed from: a */
    private void m2924a(Context context, BaiduMapOptions baiduMapOptions) {
        this.f2584p = context;
        C1083i.m3649a();
        BMapManager.init();
        m2925a(context, baiduMapOptions, f2568c == 0 ? f2567b : CustomMapStyleLoader.getCustomStyleFilePath(), f2568c);
        this.f2574f = new BaiduMap(this.f2573e);
        m2923a(context);
        m2929b(context);
        if (baiduMapOptions != null && !baiduMapOptions.f2432h) {
            this.f2577i.setVisibility(View.INVISIBLE);
        }
        m2931c(context);
        if (baiduMapOptions != null && !baiduMapOptions.f2433i) {
            this.f2580l.setVisibility(View.INVISIBLE);
        }
        if (!(baiduMapOptions == null || baiduMapOptions.f2434j == null)) {
            this.f2585r = baiduMapOptions.f2434j.ordinal();
        }
        if (!(baiduMapOptions == null || baiduMapOptions.f2436l == null)) {
            this.f2579k = baiduMapOptions.f2436l;
        }
        if (baiduMapOptions != null && baiduMapOptions.f2435k != null) {
            this.f2578j = baiduMapOptions.f2435k;
        }
    }

    /* renamed from: a */
    private void m2925a(Context context, BaiduMapOptions baiduMapOptions, String str, int i) {
        if (baiduMapOptions == null) {
            this.f2573e = new C1084j(context, null, str, i);
        } else {
            this.f2573e = new C1084j(context, baiduMapOptions.mo11031a(), str, i);
        }
        addView(this.f2573e);
        this.f2589v = new C0935l(this);
        this.f2573e.mo13086a().mo13018a(this.f2589v);
    }

    /* renamed from: a */
    private void m2926a(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-2, -2);
        }
        int i = layoutParams.width;
        int makeMeasureSpec = i > 0 ? View.MeasureSpec.makeMeasureSpec(i, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0);
        int i2 = layoutParams.height;
        view.measure(makeMeasureSpec, i2 > 0 ? View.MeasureSpec.makeMeasureSpec(i2, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m2928b() {
        if (this.f2577i.mo12980a()) {
            float f = this.f2573e.mo13086a().mo12989E().f3414a;
            boolean z = true;
            this.f2577i.mo12983b(f > this.f2573e.mo13086a().f3527b);
            C1074ak akVar = this.f2577i;
            if (f >= this.f2573e.mo13086a().f3505a) {
                z = false;
            }
            akVar.mo12979a(z);
        }
    }

    /* renamed from: b */
    private void m2929b(Context context) {
        this.f2577i = new C1074ak(context, false);
        if (this.f2577i.mo12980a()) {
            this.f2577i.mo12982b(new C0936m(this));
            this.f2577i.mo12978a(new C0937n(this));
            addView(this.f2577i);
        }
    }

    /* renamed from: c */
    private void m2931c(Context context) {
        this.f2580l = new RelativeLayout(context);
        this.f2580l.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.f2581m = new TextView(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        this.f2581m.setTextColor(Color.parseColor("#FFFFFF"));
        this.f2581m.setTextSize(2, 11.0f);
        TextView textView = this.f2581m;
        textView.setTypeface(textView.getTypeface(), 1);
        this.f2581m.setLayoutParams(layoutParams);
        this.f2581m.setId(Integer.MAX_VALUE);
        this.f2580l.addView(this.f2581m);
        this.f2582n = new TextView(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.width = -2;
        layoutParams2.height = -2;
        layoutParams2.addRule(14);
        this.f2582n.setTextColor(Color.parseColor("#000000"));
        this.f2582n.setTextSize(2, 11.0f);
        this.f2582n.setLayoutParams(layoutParams2);
        this.f2580l.addView(this.f2582n);
        this.f2583o = new ImageView(context);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.width = -2;
        layoutParams3.height = -2;
        layoutParams3.addRule(14);
        layoutParams3.addRule(3, this.f2581m.getId());
        this.f2583o.setLayoutParams(layoutParams3);
        Bitmap a = C1053a.m3464a("icon_scale.9.png", context);
        byte[] ninePatchChunk = a.getNinePatchChunk();
        NinePatch.isNinePatchChunk(ninePatchChunk);
        this.f2583o.setBackgroundDrawable(new NinePatchDrawable(a, ninePatchChunk, new Rect(), null));
        this.f2580l.addView(this.f2583o);
        addView(this.f2580l);
    }

    public static void setCustomMapStylePath(String str) {
        if (str == null || str.length() == 0) {
            throw new RuntimeException("BDMapSDKException: customMapStylePath String is illegal");
        } else if (new File(str).exists()) {
            f2567b = str;
        } else {
            throw new RuntimeException("BDMapSDKException: please check whether the customMapStylePath file exits");
        }
    }

    @Deprecated
    public static void setIconCustom(int i) {
        f2569d = i;
    }

    public static void setLoadCustomMapStyleFileMode(int i) {
        f2568c = i;
    }

    public static void setMapCustomEnable(boolean z) {
        C1083i.m3651a(z);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof MapViewLayoutParams) {
            super.addView(view, layoutParams);
        }
    }

    public void cancelRenderMap() {
        this.f2573e.mo13086a().mo13081w(false);
        this.f2573e.mo13086a().mo13000P().clear();
    }

    public final LogoPosition getLogoPosition() {
        int i = this.f2585r;
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? i != 5 ? LogoPosition.logoPostionleftBottom : LogoPosition.logoPostionRightTop : LogoPosition.logoPostionRightBottom : LogoPosition.logoPostionCenterTop : LogoPosition.logoPostionCenterBottom : LogoPosition.logoPostionleftTop;
    }

    public final BaiduMap getMap() {
        BaiduMap baiduMap = this.f2574f;
        baiduMap.f2400a = this;
        return baiduMap;
    }

    public final int getMapLevel() {
        return f2570q.get((int) this.f2573e.mo13086a().mo12989E().f3414a).intValue();
    }

    public int getScaleControlViewHeight() {
        return this.f2571A;
    }

    public int getScaleControlViewWidth() {
        return this.f2572B;
    }

    public boolean handleMultiTouch(float f, float f2, float f3, float f4) {
        C1084j jVar = this.f2573e;
        return jVar != null && jVar.mo13090a(f, f2, f3, f4);
    }

    public void handleTouchDown(float f, float f2) {
        C1084j jVar = this.f2573e;
        if (jVar != null) {
            jVar.mo13087a(f, f2);
        }
    }

    public boolean handleTouchMove(float f, float f2) {
        C1084j jVar = this.f2573e;
        return jVar != null && jVar.mo13095c(f, f2);
    }

    public boolean handleTouchUp(float f, float f2) {
        C1084j jVar = this.f2573e;
        if (jVar == null) {
            return false;
        }
        return jVar.mo13093b(f, f2);
    }

    public boolean inRangeOfView(float f, float f2) {
        C1084j jVar = this.f2573e;
        return jVar != null && jVar.mo13097d(f, f2);
    }

    public void onCreate(Context context, Bundle bundle) {
        if (bundle != null) {
            MapStatus mapStatus = (MapStatus) bundle.getParcelable("mapstatus");
            if (this.f2578j != null) {
                this.f2578j = (Point) bundle.getParcelable("scalePosition");
            }
            if (this.f2579k != null) {
                this.f2579k = (Point) bundle.getParcelable("zoomPosition");
            }
            this.f2586s = bundle.getBoolean("mZoomControlEnabled");
            this.f2587t = bundle.getBoolean("mScaleControlEnabled");
            this.f2585r = bundle.getInt("logoPosition");
            setPadding(bundle.getInt("paddingLeft"), bundle.getInt("paddingTop"), bundle.getInt("paddingRight"), bundle.getInt("paddingBottom"));
            m2924a(context, new BaiduMapOptions().mapStatus(mapStatus));
        }
    }

    public final void onDestroy() {
        Context context = this.f2584p;
        if (context != null) {
            this.f2573e.mo13092b(context.hashCode());
        }
        Bitmap bitmap = this.f2576h;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f2576h.recycle();
            this.f2576h = null;
        }
        if (f2567b != null) {
            f2567b = null;
        }
        this.f2577i.mo12981b();
        BMapManager.destroy();
        C1083i.m3652b();
        this.f2584p = null;
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        float f;
        Point point;
        int i5;
        int i6;
        int height;
        int childCount = getChildCount();
        m2926a(this.f2575g);
        float f2 = 1.0f;
        if (((getWidth() - this.f2590w) - this.f2591x) - this.f2575g.getMeasuredWidth() <= 0 || ((getHeight() - this.f2592y) - this.f2593z) - this.f2575g.getMeasuredHeight() <= 0) {
            this.f2590w = 0;
            this.f2591x = 0;
            this.f2593z = 0;
            this.f2592y = 0;
            f = 1.0f;
        } else {
            f2 = ((float) ((getWidth() - this.f2590w) - this.f2591x)) / ((float) getWidth());
            f = ((float) ((getHeight() - this.f2592y) - this.f2593z)) / ((float) getHeight());
        }
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt != null) {
                C1084j jVar = this.f2573e;
                if (childAt == jVar) {
                    jVar.layout(0, 0, getWidth(), getHeight());
                } else {
                    ImageView imageView = this.f2575g;
                    if (childAt == imageView) {
                        float f3 = f2 * 5.0f;
                        int i8 = (int) (((float) this.f2590w) + f3);
                        int i9 = (int) (((float) this.f2591x) + f3);
                        float f4 = 5.0f * f;
                        int i10 = (int) (((float) this.f2592y) + f4);
                        int i11 = (int) (((float) this.f2593z) + f4);
                        int i12 = this.f2585r;
                        if (i12 != 1) {
                            if (i12 == 2) {
                                height = getHeight() - i11;
                                i10 = height - this.f2575g.getMeasuredHeight();
                            } else if (i12 != 3) {
                                if (i12 == 4) {
                                    i6 = getHeight() - i11;
                                    i10 = i6 - this.f2575g.getMeasuredHeight();
                                } else if (i12 != 5) {
                                    i6 = getHeight() - i11;
                                    i5 = this.f2575g.getMeasuredWidth() + i8;
                                    i10 = i6 - this.f2575g.getMeasuredHeight();
                                } else {
                                    i6 = i10 + imageView.getMeasuredHeight();
                                }
                                i5 = getWidth() - i9;
                                i8 = i5 - this.f2575g.getMeasuredWidth();
                            } else {
                                height = i10 + imageView.getMeasuredHeight();
                            }
                            i8 = (((getWidth() - this.f2575g.getMeasuredWidth()) + this.f2590w) - this.f2591x) / 2;
                            i5 = (((getWidth() + this.f2575g.getMeasuredWidth()) + this.f2590w) - this.f2591x) / 2;
                        } else {
                            i6 = imageView.getMeasuredHeight() + i10;
                            i5 = this.f2575g.getMeasuredWidth() + i8;
                        }
                        this.f2575g.layout(i8, i10, i5, i6);
                    } else {
                        C1074ak akVar = this.f2577i;
                        if (childAt != akVar) {
                            RelativeLayout relativeLayout = this.f2580l;
                            if (childAt == relativeLayout) {
                                m2926a(relativeLayout);
                                Point point2 = this.f2578j;
                                if (point2 == null) {
                                    this.f2572B = this.f2580l.getMeasuredWidth();
                                    this.f2571A = this.f2580l.getMeasuredHeight();
                                    int i13 = (int) (((float) this.f2590w) + (5.0f * f2));
                                    int height2 = (getHeight() - ((int) ((((float) this.f2593z) + (f * 5.0f)) + 56.0f))) - this.f2575g.getMeasuredHeight();
                                    this.f2580l.layout(i13, height2, this.f2572B + i13, this.f2571A + height2);
                                } else {
                                    this.f2580l.layout(point2.x, this.f2578j.y, this.f2578j.x + this.f2580l.getMeasuredWidth(), this.f2578j.y + this.f2580l.getMeasuredHeight());
                                }
                            } else {
                                ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
                                if (layoutParams == null) {
                                    Log.e("test", "lp == null");
                                }
                                if (layoutParams instanceof MapViewLayoutParams) {
                                    MapViewLayoutParams mapViewLayoutParams = (MapViewLayoutParams) layoutParams;
                                    if (mapViewLayoutParams.f2596c == MapViewLayoutParams.ELayoutMode.absoluteMode) {
                                        point = mapViewLayoutParams.f2595b;
                                    } else {
                                        point = this.f2573e.mo13086a().mo13005a(CoordUtil.ll2mc(mapViewLayoutParams.f2594a));
                                    }
                                    m2926a(childAt);
                                    int measuredWidth = childAt.getMeasuredWidth();
                                    int measuredHeight = childAt.getMeasuredHeight();
                                    float f5 = mapViewLayoutParams.f2597d;
                                    float f6 = mapViewLayoutParams.f2598e;
                                    int i14 = (int) (((float) point.x) - (f5 * ((float) measuredWidth)));
                                    int i15 = ((int) (((float) point.y) - (f6 * ((float) measuredHeight)))) + mapViewLayoutParams.f2599f;
                                    childAt.layout(i14, i15, measuredWidth + i14, measuredHeight + i15);
                                }
                            }
                        } else if (akVar.mo12980a()) {
                            m2926a(this.f2577i);
                            Point point3 = this.f2579k;
                            if (point3 == null) {
                                int height3 = (int) ((((float) (getHeight() - 15)) * f) + ((float) this.f2592y));
                                int width = (int) ((((float) (getWidth() - 15)) * f2) + ((float) this.f2590w));
                                int measuredWidth2 = width - this.f2577i.getMeasuredWidth();
                                int measuredHeight2 = height3 - this.f2577i.getMeasuredHeight();
                                if (this.f2585r == 4) {
                                    height3 -= this.f2575g.getMeasuredHeight();
                                    measuredHeight2 -= this.f2575g.getMeasuredHeight();
                                }
                                this.f2577i.layout(measuredWidth2, measuredHeight2, width, height3);
                            } else {
                                this.f2577i.layout(point3.x, this.f2579k.y, this.f2579k.x + this.f2577i.getMeasuredWidth(), this.f2579k.y + this.f2577i.getMeasuredHeight());
                            }
                        }
                    }
                }
            }
        }
    }

    public final void onPause() {
        this.f2573e.onPause();
    }

    public final void onResume() {
        this.f2573e.onResume();
    }

    public void onSaveInstanceState(Bundle bundle) {
        BaiduMap baiduMap;
        if (bundle != null && (baiduMap = this.f2574f) != null) {
            bundle.putParcelable("mapstatus", baiduMap.getMapStatus());
            Point point = this.f2578j;
            if (point != null) {
                bundle.putParcelable("scalePosition", point);
            }
            Point point2 = this.f2579k;
            if (point2 != null) {
                bundle.putParcelable("zoomPosition", point2);
            }
            bundle.putBoolean("mZoomControlEnabled", this.f2586s);
            bundle.putBoolean("mScaleControlEnabled", this.f2587t);
            bundle.putInt("logoPosition", this.f2585r);
            bundle.putInt("paddingLeft", this.f2590w);
            bundle.putInt("paddingTop", this.f2592y);
            bundle.putInt("paddingRight", this.f2591x);
            bundle.putInt("paddingBottom", this.f2593z);
        }
    }

    public void removeView(View view) {
        if (view != this.f2575g) {
            super.removeView(view);
        }
    }

    public void renderMap() {
        C1078e a = this.f2573e.mo13086a();
        a.mo13081w(true);
        a.mo13001Q();
    }

    public final void setLogoPosition(LogoPosition logoPosition) {
        if (logoPosition == null) {
            logoPosition = LogoPosition.logoPostionleftBottom;
        }
        this.f2585r = logoPosition.ordinal();
        requestLayout();
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.f2590w = i;
        this.f2592y = i2;
        this.f2591x = i3;
        this.f2593z = i4;
    }

    public void setScaleControlPosition(Point point) {
        if (point != null && point.x >= 0 && point.y >= 0 && point.x <= getWidth() && point.y <= getHeight()) {
            this.f2578j = point;
            requestLayout();
        }
    }

    public void setUpViewEventToMapView(MotionEvent motionEvent) {
        this.f2573e.onTouchEvent(motionEvent);
    }

    public final void setZOrderMediaOverlay(boolean z) {
        C1084j jVar = this.f2573e;
        if (jVar != null) {
            jVar.setZOrderMediaOverlay(z);
        }
    }

    public void setZoomControlsPosition(Point point) {
        if (point != null && point.x >= 0 && point.y >= 0 && point.x <= getWidth() && point.y <= getHeight()) {
            this.f2579k = point;
            requestLayout();
        }
    }

    public void showScaleControl(boolean z) {
        this.f2580l.setVisibility(z ? 0 : 8);
        this.f2587t = z;
    }

    public void showZoomControls(boolean z) {
        if (this.f2577i.mo12980a()) {
            this.f2577i.setVisibility(z ? 0 : 8);
            this.f2586s = z;
        }
    }
}
