package com.baidu.location.p013a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import com.baidu.location.C0839f;
import com.baidu.location.Jni;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0825d;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0845c;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0854j;
import com.baidu.location.p019g.C0855k;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

/* renamed from: com.baidu.location.a.o */
public class C0755o extends C0849e {

    /* renamed from: q */
    private static C0755o f1415q;

    /* renamed from: a */
    String f1416a;

    /* renamed from: b */
    String f1417b;

    /* renamed from: c */
    String f1418c;

    /* renamed from: d */
    String f1419d;

    /* renamed from: e */
    int f1420e;

    /* renamed from: f */
    Handler f1421f;

    private C0755o() {
        this.f1416a = null;
        this.f1417b = null;
        this.f1418c = null;
        this.f1419d = null;
        this.f1420e = 1;
        this.f1421f = null;
        this.f1421f = new Handler();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0040  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m1919a(java.io.File r4, java.io.File r5) throws java.io.IOException {
        /*
            r0 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0037 }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ all -> 0x0037 }
            r2.<init>(r4)     // Catch:{ all -> 0x0037 }
            r1.<init>(r2)     // Catch:{ all -> 0x0037 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0035 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ all -> 0x0035 }
            r3.<init>(r5)     // Catch:{ all -> 0x0035 }
            r2.<init>(r3)     // Catch:{ all -> 0x0035 }
            r5 = 5120(0x1400, float:7.175E-42)
            byte[] r5 = new byte[r5]     // Catch:{ all -> 0x0032 }
        L_0x0019:
            int r0 = r1.read(r5)     // Catch:{ all -> 0x0032 }
            r3 = -1
            if (r0 == r3) goto L_0x0025
            r3 = 0
            r2.write(r5, r3, r0)     // Catch:{ all -> 0x0032 }
            goto L_0x0019
        L_0x0025:
            r2.flush()     // Catch:{ all -> 0x0032 }
            r4.delete()     // Catch:{ all -> 0x0032 }
            r1.close()
            r2.close()
            return
        L_0x0032:
            r4 = move-exception
            r0 = r2
            goto L_0x0039
        L_0x0035:
            r4 = move-exception
            goto L_0x0039
        L_0x0037:
            r4 = move-exception
            r1 = r0
        L_0x0039:
            if (r1 == 0) goto L_0x003e
            r1.close()
        L_0x003e:
            if (r0 == 0) goto L_0x0043
            r0.close()
        L_0x0043:
            goto L_0x0045
        L_0x0044:
            throw r4
        L_0x0045:
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0755o.m1919a(java.io.File, java.io.File):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m1920a(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || activeNetworkInfo.getType() != 0) {
                return false;
            }
            String a = C0825d.m2298a(C0822b.m2279a().mo10630e());
            return a.equals("3G") || a.equals("4G");
        } catch (Exception unused) {
            return false;
        }
    }

    /* renamed from: a */
    public static boolean m1922a(String str, String str2) {
        File file = new File(C0855k.m2472j() + File.separator + "tmp");
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[4096];
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            while (true) {
                int read = bufferedInputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
            }
            httpURLConnection.disconnect();
            fileOutputStream.close();
            bufferedInputStream.close();
            if (file.length() < 10240) {
                file.delete();
                return false;
            }
            file.renameTo(new File(C0855k.m2472j() + File.separator + str2));
            return true;
        } catch (Exception unused) {
            file.delete();
            return false;
        }
    }

    /* renamed from: b */
    public static C0755o m1923b() {
        if (f1415q == null) {
            f1415q = new C0755o();
        }
        return f1415q;
    }

    /* renamed from: d */
    private Handler m1926d() {
        return this.f1421f;
    }

    /* renamed from: e */
    private void m1927e() {
        try {
            File file = new File(C0855k.m2472j() + "/grtcfrsa.dat");
            if (!file.exists()) {
                File file2 = new File(C0854j.f1898a);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (file.createNewFile()) {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(2);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.seek(8);
                    byte[] bytes = "1980_01_01:0".getBytes();
                    randomAccessFile.writeInt(bytes.length);
                    randomAccessFile.write(bytes);
                    randomAccessFile.seek(200);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.seek(800);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.close();
                } else {
                    return;
                }
            }
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
            randomAccessFile2.seek(200);
            randomAccessFile2.writeBoolean(true);
            if (this.f1420e == 1) {
                randomAccessFile2.writeBoolean(true);
            } else {
                randomAccessFile2.writeBoolean(false);
            }
            if (this.f1419d != null) {
                byte[] bytes2 = this.f1419d.getBytes();
                randomAccessFile2.writeInt(bytes2.length);
                randomAccessFile2.write(bytes2);
            } else if (Math.abs(C0839f.getFrameVersion() - 7.82f) < 1.0E-8f) {
                randomAccessFile2.writeInt(0);
            }
            randomAccessFile2.close();
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m1928f() {
        if (this.f1416a != null) {
            new C0759s(this).start();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public boolean m1929g() {
        if (this.f1418c == null) {
            return true;
        }
        if (new File(C0855k.m2472j() + File.separator + this.f1418c).exists()) {
            return true;
        }
        return m1922a("http://" + this.f1416a + "/" + this.f1418c, this.f1418c);
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public void m1930h() {
        if (this.f1417b != null) {
            File file = new File(C0855k.m2472j() + File.separator + this.f1417b);
            if (!file.exists()) {
                if (m1922a("http://" + this.f1416a + "/" + this.f1417b, this.f1417b)) {
                    String a = C0855k.m2455a(file, "SHA-256");
                    String str = this.f1419d;
                    if (str != null && a != null && C0855k.m2461b(a, str, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCiP7BS5IjEOzrKGR9/Ww9oSDhdX1ir26VOsYjT1T6tk2XumRpkHRwZbrucDcNnvSB4QsqiEJnvTSRi7YMbh2H9sLMkcvHlMV5jAErNvnuskWfcvf7T2mq7EUZI/Hf4oVZhHV0hQJRFVdTcjWI6q2uaaKM3VMh+roDesiE7CR2biQIDAQAB")) {
                        File file2 = new File(C0855k.m2472j() + File.separator + C0839f.replaceFileName);
                        if (file2.exists()) {
                            file2.delete();
                        }
                        try {
                            m1919a(file, file2);
                        } catch (Exception unused) {
                            file2.delete();
                        }
                    }
                }
            }
        }
    }

    /* renamed from: a */
    public void mo10444a() {
        String str;
        StringBuffer stringBuffer = new StringBuffer(128);
        stringBuffer.append("&sdk=");
        stringBuffer.append(7.82f);
        stringBuffer.append("&fw=");
        stringBuffer.append(C0839f.getFrameVersion());
        stringBuffer.append("&suit=");
        stringBuffer.append(2);
        if (C0844b.m2417a().f1856c == null) {
            stringBuffer.append("&im=");
            str = C0844b.m2417a().f1854a;
        } else {
            stringBuffer.append("&cu=");
            str = C0844b.m2417a().f1856c;
        }
        stringBuffer.append(str);
        stringBuffer.append("&mb=");
        stringBuffer.append(Build.MODEL);
        stringBuffer.append("&sv=");
        String str2 = Build.VERSION.RELEASE;
        if (str2 != null && str2.length() > 10) {
            str2 = str2.substring(0, 10);
        }
        stringBuffer.append(str2);
        String str3 = null;
        try {
            if (Build.VERSION.SDK_INT > 20) {
                String[] strArr = Build.SUPPORTED_ABIS;
                String str4 = null;
                for (int i = 0; i < strArr.length; i++) {
                    if (i == 0) {
                        str4 = strArr[i] + ";";
                    } else {
                        str4 = str4 + strArr[i] + ";";
                    }
                }
                str3 = str4;
            } else {
                str3 = Build.CPU_ABI2;
            }
        } catch (Error | Exception unused) {
        }
        if (str3 != null) {
            stringBuffer.append("&cpuabi=");
            stringBuffer.append(str3);
        }
        stringBuffer.append("&pack=");
        stringBuffer.append(C0844b.f1848e);
        this.f1882h = C0855k.m2468f() + "?&it=" + Jni.en1(stringBuffer.toString());
    }

    /* renamed from: a */
    public void mo10446a(boolean z) {
        if (z) {
            try {
                JSONObject jSONObject = new JSONObject(this.f1884j);
                if ("up".equals(jSONObject.getString("res"))) {
                    this.f1416a = jSONObject.getString("upath");
                    if (jSONObject.has("u1")) {
                        this.f1417b = jSONObject.getString("u1");
                    }
                    if (jSONObject.has("u2")) {
                        this.f1418c = jSONObject.getString("u2");
                    }
                    if (jSONObject.has("u1_rsa")) {
                        this.f1419d = jSONObject.getString("u1_rsa");
                    }
                    m1926d().post(new C0758r(this));
                }
                if (jSONObject.has("ison")) {
                    this.f1420e = jSONObject.getInt("ison");
                }
                m1927e();
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: c */
    public void mo10499c() {
        if (System.currentTimeMillis() - C0845c.m2426a().mo10721b() > 86400000) {
            C0845c.m2426a().mo10720a(System.currentTimeMillis());
            m1926d().postDelayed(new C0756p(this), 10000);
            m1926d().postDelayed(new C0757q(this), 5000);
        }
    }
}
