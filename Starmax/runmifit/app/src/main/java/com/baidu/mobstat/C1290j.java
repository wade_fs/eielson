package com.baidu.mobstat;

import android.content.Context;

/* renamed from: com.baidu.mobstat.j */
public class C1290j {
    /* renamed from: a */
    public static void m4800a(Context context) {
        C1289i.f4359a.mo13970a(context);
        C1236af.m4534a(context).mo13890a(C1296n.AP_LIST, System.currentTimeMillis());
    }

    /* renamed from: a */
    public static void m4801a(Context context, boolean z) {
        C1293l.f4365a.mo13973a(context, z);
        C1236af.m4534a(context).mo13890a(z ? C1296n.APP_SYS_LIST : C1296n.APP_USER_LIST, System.currentTimeMillis());
    }

    /* renamed from: b */
    public static void m4803b(Context context, boolean z) {
        C1294m.f4366a.mo13974a(context, z);
        C1236af.m4534a(context).mo13890a(z ? C1296n.APP_TRACE_CURRENT : C1296n.APP_TRACE_HIS, System.currentTimeMillis());
    }

    /* renamed from: b */
    public static void m4802b(Context context) {
        C1291k.f4360a.mo13971a(context);
        C1236af.m4534a(context).mo13890a(C1296n.APP_APK, System.currentTimeMillis());
    }
}
