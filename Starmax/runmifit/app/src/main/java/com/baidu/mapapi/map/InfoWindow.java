package com.baidu.mapapi.map;

import android.view.View;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapapi.model.LatLng;

public class InfoWindow {

    /* renamed from: a */
    BitmapDescriptor f2512a;

    /* renamed from: b */
    View f2513b;

    /* renamed from: c */
    LatLng f2514c;

    /* renamed from: d */
    OnInfoWindowClickListener f2515d;

    /* renamed from: e */
    C0920a f2516e;

    /* renamed from: f */
    int f2517f;

    /* renamed from: g */
    boolean f2518g = false;

    /* renamed from: h */
    int f2519h = SysOSUtil.getDensityDpi();

    /* renamed from: i */
    boolean f2520i = false;

    /* renamed from: j */
    boolean f2521j = false;

    /* renamed from: k */
    boolean f2522k = false;

    /* renamed from: l */
    private String f2523l = "";

    public interface OnInfoWindowClickListener {
        void onInfoWindowClick();
    }

    /* renamed from: com.baidu.mapapi.map.InfoWindow$a */
    interface C0920a {
        /* renamed from: a */
        void mo11150a(InfoWindow infoWindow);

        /* renamed from: b */
        void mo11151b(InfoWindow infoWindow);
    }

    public InfoWindow(View view, LatLng latLng, int i) {
        if (view == null || latLng == null) {
            throw new IllegalArgumentException("BDMapSDKException: view and position can not be null");
        }
        this.f2513b = view;
        this.f2514c = latLng;
        this.f2517f = i;
        this.f2521j = true;
    }

    public InfoWindow(View view, LatLng latLng, int i, boolean z, int i2) {
        if (view == null || latLng == null) {
            throw new IllegalArgumentException("BDMapSDKException: view and position can not be null");
        }
        this.f2513b = view;
        this.f2514c = latLng;
        this.f2517f = i;
        this.f2518g = z;
        this.f2519h = i2;
        this.f2521j = true;
    }

    public InfoWindow(BitmapDescriptor bitmapDescriptor, LatLng latLng, int i, OnInfoWindowClickListener onInfoWindowClickListener) {
        if (bitmapDescriptor == null || latLng == null) {
            throw new IllegalArgumentException("BDMapSDKException: bitmapDescriptor and position can not be null");
        }
        this.f2512a = bitmapDescriptor;
        this.f2514c = latLng;
        this.f2515d = onInfoWindowClickListener;
        this.f2517f = i;
        this.f2522k = true;
    }

    public BitmapDescriptor getBitmapDescriptor() {
        return this.f2512a;
    }

    public LatLng getPosition() {
        return this.f2514c;
    }

    public String getTag() {
        return this.f2523l;
    }

    public View getView() {
        return this.f2513b;
    }

    public int getYOffset() {
        return this.f2517f;
    }

    public void setBitmapDescriptor(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f2512a = bitmapDescriptor;
            this.f2516e.mo11151b(this);
        }
    }

    public void setPosition(LatLng latLng) {
        if (latLng != null) {
            this.f2514c = latLng;
            this.f2516e.mo11151b(this);
        }
    }

    public void setTag(String str) {
        this.f2523l = str;
    }

    public void setView(View view) {
        if (view != null) {
            this.f2513b = view;
            this.f2516e.mo11151b(this);
        }
    }

    public void setYOffset(int i) {
        this.f2517f = i;
        this.f2516e.mo11151b(this);
    }
}
