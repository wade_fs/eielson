package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.route.TransitRouteLine;

/* renamed from: com.baidu.mapapi.search.route.p */
final class C1000p implements Parcelable.Creator<TransitRouteLine.TransitStep> {
    C1000p() {
    }

    /* renamed from: a */
    public TransitRouteLine.TransitStep createFromParcel(Parcel parcel) {
        return new TransitRouteLine.TransitStep(parcel);
    }

    /* renamed from: a */
    public TransitRouteLine.TransitStep[] newArray(int i) {
        return new TransitRouteLine.TransitStep[i];
    }
}
