package com.baidu.location.p018f;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.util.Log;
import com.baidu.location.C0839f;
import com.baidu.location.LLSInterface;
import com.baidu.location.indoor.C0865g;
import com.baidu.location.p013a.C0723a;
import com.baidu.location.p013a.C0726b;
import com.baidu.location.p013a.C0736h;
import com.baidu.location.p013a.C0741j;
import com.baidu.location.p013a.C0750l;
import com.baidu.location.p013a.C0755o;
import com.baidu.location.p013a.C0761u;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p013a.C0765w;
import com.baidu.location.p013a.C0767x;
import com.baidu.location.p014b.C0770a;
import com.baidu.location.p014b.C0772b;
import com.baidu.location.p014b.C0775d;
import com.baidu.location.p014b.C0777e;
import com.baidu.location.p014b.C0780g;
import com.baidu.location.p016d.C0790a;
import com.baidu.location.p016d.C0804h;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0826e;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0855k;
import java.lang.ref.WeakReference;

/* renamed from: com.baidu.location.f.a */
public class C0840a extends Service implements LLSInterface {

    /* renamed from: a */
    static C0841a f1815a;

    /* renamed from: c */
    public static long f1816c;

    /* renamed from: g */
    private static long f1817g;

    /* renamed from: b */
    Messenger f1818b = null;

    /* renamed from: d */
    private Looper f1819d = null;

    /* renamed from: e */
    private HandlerThread f1820e = null;

    /* renamed from: f */
    private boolean f1821f = false;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public int f1822h = 0;

    /* renamed from: com.baidu.location.f.a$a */
    public static class C0841a extends Handler {

        /* renamed from: a */
        private final WeakReference<C0840a> f1823a;

        public C0841a(Looper looper, C0840a aVar) {
            super(looper);
            this.f1823a = new WeakReference<>(aVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.l.a(boolean, boolean):void
         arg types: [int, int]
         candidates:
          com.baidu.location.a.l.a(com.baidu.location.a.l, android.os.Message):void
          com.baidu.location.a.l.a(com.baidu.location.a.l, boolean):boolean
          com.baidu.location.a.l.a(boolean, boolean):void */
        public void handleMessage(Message message) {
            C0840a aVar = this.f1823a.get();
            if (aVar != null) {
                if (C0839f.isServing) {
                    int i = message.what;
                    if (i == 11) {
                        aVar.m2406a(message);
                    } else if (i == 12) {
                        aVar.m2409b(message);
                    } else if (i == 15) {
                        aVar.m2413c(message);
                    } else if (i == 22) {
                        C0750l.m1883c().mo10478b(message);
                    } else if (i == 28) {
                        C0750l.m1883c().mo10477a(true, true);
                    } else if (i == 41) {
                        C0750l.m1883c().mo10486i();
                    } else if (i == 705) {
                        C0723a.m1729a().mo10424a(message.getData().getBoolean("foreground"));
                    } else if (i != 406) {
                        if (!(i == 407 || i == 802 || i == 803)) {
                            switch (i) {
                                case 110:
                                    C0865g.m2519a().mo10766c();
                                    break;
                                case 111:
                                    C0865g.m2519a().mo10767d();
                                    break;
                                case 112:
                                    C0865g.m2519a().mo10764b();
                                    break;
                                case 113:
                                    Object obj = message.obj;
                                    if (obj != null) {
                                        C0865g.m2519a().mo10763a((Bundle) obj);
                                        break;
                                    }
                                    break;
                                case 114:
                                    Object obj2 = message.obj;
                                    if (obj2 != null) {
                                        C0865g.m2519a().mo10765b((Bundle) obj2);
                                        break;
                                    }
                                    break;
                                default:
                                    switch (i) {
                                        case 401:
                                            try {
                                                message.getData();
                                                break;
                                            } catch (Exception unused) {
                                                break;
                                            }
                                    }
                            }
                        }
                    } else {
                        C0736h.m1819a().mo10458e();
                    }
                }
                if (message.what == 1) {
                    aVar.m2416d();
                }
                if (message.what == 0) {
                    aVar.m2412c();
                }
                super.handleMessage(message);
            }
        }
    }

    /* renamed from: a */
    public static Handler m2405a() {
        return f1815a;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2406a(Message message) {
        Log.d("baidu_location_service", "baidu location service register ...");
        C0723a.m1729a().mo10421a(message);
        C0804h.m2193a();
        C0777e.m2013a().mo10537d();
        if (!C0855k.m2460b()) {
            C0755o.m1923b().mo10499c();
        }
    }

    /* renamed from: b */
    public static long m2408b() {
        return f1817g;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m2409b(Message message) {
        C0723a.m1729a().mo10426b(message);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m2412c() {
        C0741j.m1844a().mo10466a(C0839f.getServiceContext());
        C0777e.m2013a().mo10535b();
        C0844b.m2417a();
        try {
            C0767x.m1972a().mo10513e();
        } catch (Exception unused) {
        }
        C0736h.m1819a().mo10455b();
        C0826e.m2306a().mo10639b();
        C0822b.m2279a().mo10627b();
        C0750l.m1883c().mo10481d();
        C0790a.m2063a().mo10558c();
        C0775d.m2005a().mo10529b();
        C0780g.m2035a().mo10540b();
        C0770a.m1980a().mo10520b();
        C0835i.m2376a().mo10687c();
        this.f1822h = 2;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m2413c(Message message) {
        C0723a.m1729a().mo10430c(message);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m2416d() {
        C0826e.m2306a().mo10642e();
        C0835i.m2376a().mo10689e();
        C0804h.m2193a().mo10603n();
        C0767x.m1972a().mo10514f();
        C0777e.m2013a().mo10536c();
        C0775d.m2005a().mo10530c();
        C0772b.m1993a().mo10524c();
        C0770a.m1980a().mo10521c();
        C0726b.m1759a().mo10441b();
        C0822b.m2279a().mo10628c();
        C0750l.m1883c().mo10482e();
        C0865g.m2519a().mo10767d();
        C0736h.m1819a().mo10456c();
        C0765w.m1962d();
        C0723a.m1729a().mo10425b();
        C0762v.m1943a().mo10506d();
        this.f1822h = 4;
        Log.d("baidu_location_service", "baidu location service has stoped ...");
        if (!this.f1821f) {
            Process.killProcess(Process.myPid());
        }
    }

    public double getVersion() {
        return 7.820000171661377d;
    }

    public IBinder onBind(Intent intent) {
        boolean z;
        Bundle extras = intent.getExtras();
        if (extras != null) {
            C0844b.f1851h = extras.getString("key");
            C0844b.f1850g = extras.getString("sign");
            this.f1821f = extras.getBoolean("kill_process");
            z = extras.getBoolean("cache_exception");
        } else {
            z = false;
        }
        if (!z) {
            Thread.setDefaultUncaughtExceptionHandler(C0780g.m2035a());
        }
        return this.f1818b.getBinder();
    }

    public void onCreate(Context context) {
        try {
            C0855k.f1955ax = context.getPackageName();
        } catch (Exception unused) {
        }
        f1817g = System.currentTimeMillis();
        this.f1820e = C0761u.m1942a();
        HandlerThread handlerThread = this.f1820e;
        if (handlerThread != null) {
            this.f1819d = handlerThread.getLooper();
        }
        Looper looper = this.f1819d;
        if (looper == null) {
            f1815a = new C0841a(Looper.getMainLooper(), this);
        } else {
            f1815a = new C0841a(looper, this);
        }
        f1816c = System.currentTimeMillis();
        this.f1818b = new Messenger(f1815a);
        f1815a.sendEmptyMessage(0);
        this.f1822h = 1;
        Log.d("baidu_location_service", "baidu location service start1 ...20190617..." + Process.myPid());
    }

    public void onDestroy() {
        try {
            f1815a.sendEmptyMessage(1);
        } catch (Exception unused) {
            Log.d("baidu_location_service", "baidu location service stop exception...");
            m2416d();
            Process.killProcess(Process.myPid());
        }
        this.f1822h = 3;
        new Handler(Looper.getMainLooper()).postDelayed(new C0842b(this, new WeakReference(this)), 1000);
        Log.d("baidu_location_service", "baidu location service stop ...");
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return 1;
    }

    public void onTaskRemoved(Intent intent) {
        Log.d("baidu_location_service", "baidu location service remove task...");
    }

    public boolean onUnBind(Intent intent) {
        return false;
    }
}
