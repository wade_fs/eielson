package com.baidu.location.indoor;

import com.baidu.location.indoor.mapversion.p021b.C0895a;
import com.baidu.location.indoor.mapversion.p022c.C0896a;

/* renamed from: com.baidu.location.indoor.j */
class C0878j implements C0896a.C0899c {

    /* renamed from: a */
    final /* synthetic */ String f2151a;

    /* renamed from: b */
    final /* synthetic */ String f2152b;

    /* renamed from: c */
    final /* synthetic */ C0865g f2153c;

    C0878j(C0865g gVar, String str, String str2) {
        this.f2153c = gVar;
        this.f2151a = str;
        this.f2152b = str2;
    }

    /* renamed from: a */
    public void mo10781a(boolean z, String str) {
        boolean unused = this.f2153c.f2047ab = z;
        if (z) {
            boolean unused2 = this.f2153c.f2048ac = C0895a.m2734a(this.f2152b);
        }
    }
}
