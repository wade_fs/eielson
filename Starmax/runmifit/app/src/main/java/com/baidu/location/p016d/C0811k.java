package com.baidu.location.p016d;

import android.database.sqlite.SQLiteDatabase;
import com.baidu.location.Jni;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p019g.C0849e;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import org.json.JSONObject;

/* renamed from: com.baidu.location.d.k */
final class C0811k {

    /* renamed from: d */
    private static final String f1688d = String.format(Locale.US, "DELETE FROM LOG WHERE timestamp NOT IN (SELECT timestamp FROM LOG ORDER BY timestamp DESC LIMIT %d);", 3000);

    /* renamed from: e */
    private static final String f1689e = String.format(Locale.US, "SELECT * FROM LOG ORDER BY timestamp DESC LIMIT %d;", 3);

    /* renamed from: a */
    private String f1690a = null;

    /* renamed from: b */
    private final SQLiteDatabase f1691b;

    /* renamed from: c */
    private final C0812a f1692c;

    /* renamed from: com.baidu.location.d.k$a */
    private class C0812a extends C0849e {

        /* renamed from: b */
        private int f1694b;

        /* renamed from: c */
        private long f1695c;

        /* renamed from: d */
        private String f1696d = null;

        /* renamed from: e */
        private boolean f1697e = false;

        /* renamed from: f */
        private boolean f1698f = false;

        /* renamed from: q */
        private C0811k f1699q;

        C0812a(C0811k kVar) {
            this.f1699q = kVar;
            this.f1885k = new HashMap();
            this.f1694b = 0;
            this.f1695c = -1;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m2230b() {
            if (!this.f1697e) {
                this.f1696d = this.f1699q.m2226b();
                long j = this.f1695c;
                if (j != -1 && j + 86400000 <= System.currentTimeMillis()) {
                    this.f1694b = 0;
                    this.f1695c = -1;
                }
                if (this.f1696d != null && this.f1694b < 2) {
                    this.f1697e = true;
                    ExecutorService c = C0762v.m1943a().mo10505c();
                    if (c != null) {
                        mo10731a(c, "https://ofloc.map.baidu.com/offline_loc");
                    } else {
                        mo10734c("https://ofloc.map.baidu.com/offline_loc");
                    }
                }
            }
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1885k.clear();
            this.f1885k.put("qt", "ofbh");
            this.f1885k.put("req", this.f1696d);
            this.f1882h = C0804h.f1655b;
        }

        /* renamed from: a */
        public void mo10446a(boolean z) {
            this.f1698f = false;
            if (z && this.f1884j != null) {
                try {
                    JSONObject jSONObject = new JSONObject(this.f1884j);
                    if (jSONObject.has("error") && jSONObject.getInt("error") == 161) {
                        this.f1698f = true;
                    }
                } catch (Exception unused) {
                }
            }
            if (!this.f1698f) {
                this.f1694b++;
                this.f1695c = System.currentTimeMillis();
            }
            this.f1699q.m2225a(this.f1698f);
            this.f1697e = false;
        }
    }

    C0811k(SQLiteDatabase sQLiteDatabase) {
        this.f1691b = sQLiteDatabase;
        this.f1692c = new C0812a(this);
        SQLiteDatabase sQLiteDatabase2 = this.f1691b;
        if (sQLiteDatabase2 != null && sQLiteDatabase2.isOpen()) {
            try {
                this.f1691b.execSQL("CREATE TABLE IF NOT EXISTS LOG(timestamp LONG PRIMARY KEY, log VARCHAR(4000))");
            } catch (Exception unused) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2225a(boolean z) {
        String str;
        if (z && (str = this.f1690a) != null) {
            String format = String.format("DELETE FROM LOG WHERE timestamp in (%s);", str);
            try {
                if (this.f1690a.length() > 0) {
                    this.f1691b.execSQL(format);
                }
            } catch (Exception unused) {
            }
        }
        this.f1690a = null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:7|(4:10|(2:12|38)(1:39)|13|8)|37|14|15|16|17|18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005d, code lost:
        if (r3 != null) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006c, code lost:
        if (r3 == null) goto L_0x006f;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0052 */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0067 A[SYNTHETIC, Splitter:B:28:0x0067] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String m2226b() {
        /*
            r7 = this;
            org.json.JSONArray r0 = new org.json.JSONArray
            r0.<init>()
            org.json.JSONObject r1 = new org.json.JSONObject
            r1.<init>()
            r2 = 0
            android.database.sqlite.SQLiteDatabase r3 = r7.f1691b     // Catch:{ Exception -> 0x006b, all -> 0x0063 }
            java.lang.String r4 = com.baidu.location.p016d.C0811k.f1689e     // Catch:{ Exception -> 0x006b, all -> 0x0063 }
            android.database.Cursor r3 = r3.rawQuery(r4, r2)     // Catch:{ Exception -> 0x006b, all -> 0x0063 }
            if (r3 == 0) goto L_0x005d
            int r4 = r3.getCount()     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            if (r4 <= 0) goto L_0x005d
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            r4.<init>()     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            r3.moveToFirst()     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
        L_0x0023:
            boolean r5 = r3.isAfterLast()     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            if (r5 != 0) goto L_0x0048
            r5 = 1
            java.lang.String r5 = r3.getString(r5)     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            r0.put(r5)     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            int r5 = r4.length()     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            if (r5 == 0) goto L_0x003c
            java.lang.String r5 = ","
            r4.append(r5)     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
        L_0x003c:
            r5 = 0
            long r5 = r3.getLong(r5)     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            r4.append(r5)     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            r3.moveToNext()     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            goto L_0x0023
        L_0x0048:
            java.lang.String r5 = "ofloc"
            r1.put(r5, r0)     // Catch:{ JSONException -> 0x0052 }
            java.lang.String r0 = r1.toString()     // Catch:{ JSONException -> 0x0052 }
            r2 = r0
        L_0x0052:
            java.lang.String r0 = r4.toString()     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            r7.f1690a = r0     // Catch:{ Exception -> 0x005b, all -> 0x0059 }
            goto L_0x005d
        L_0x0059:
            r0 = move-exception
            goto L_0x0065
        L_0x005b:
            goto L_0x006c
        L_0x005d:
            if (r3 == 0) goto L_0x006f
        L_0x005f:
            r3.close()     // Catch:{ Exception -> 0x006f }
            goto L_0x006f
        L_0x0063:
            r0 = move-exception
            r3 = r2
        L_0x0065:
            if (r3 == 0) goto L_0x006a
            r3.close()     // Catch:{ Exception -> 0x006a }
        L_0x006a:
            throw r0
        L_0x006b:
            r3 = r2
        L_0x006c:
            if (r3 == 0) goto L_0x006f
            goto L_0x005f
        L_0x006f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0811k.m2226b():java.lang.String");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10607a() {
        this.f1692c.m2230b();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10608a(String str) {
        String encodeOfflineLocationUpdateRequest = Jni.encodeOfflineLocationUpdateRequest(str);
        try {
            this.f1691b.execSQL(String.format(Locale.US, "INSERT OR IGNORE INTO LOG VALUES (%d,\"%s\");", Long.valueOf(System.currentTimeMillis()), encodeOfflineLocationUpdateRequest));
            this.f1691b.execSQL(f1688d);
        } catch (Exception unused) {
        }
    }
}
