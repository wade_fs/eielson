package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.g */
class C1035g implements Animator.AnimatorListener {

    /* renamed from: a */
    final /* synthetic */ C1034f f3328a;

    C1035g(C1034f fVar) {
        this.f3328a = fVar;
    }

    public void onAnimationCancel(Animator animator) {
        if (this.f3328a.f3324d != null) {
            this.f3328a.f3324d.onAnimationCancel();
        }
    }

    public void onAnimationEnd(Animator animator) {
        if (this.f3328a.f3324d != null) {
            this.f3328a.f3324d.onAnimationEnd();
        }
    }

    public void onAnimationRepeat(Animator animator) {
        if (this.f3328a.f3324d != null) {
            this.f3328a.f3324d.onAnimationRepeat();
        }
    }

    public void onAnimationStart(Animator animator) {
        if (this.f3328a.f3324d != null) {
            this.f3328a.f3324d.onAnimationStart();
        }
    }
}
