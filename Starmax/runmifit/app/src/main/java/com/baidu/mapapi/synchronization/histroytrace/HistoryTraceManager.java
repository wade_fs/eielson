package com.baidu.mapapi.synchronization.histroytrace;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1105b;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;

public class HistoryTraceManager {

    /* renamed from: a */
    private C1105b f3227a = new C1105b();

    public boolean isHttpsEnable() {
        C1105b bVar = this.f3227a;
        if (bVar != null) {
            return bVar.mo13184b();
        }
        C1120a.m3855b("HistoryTraceManager", "The implement instance is null");
        return true;
    }

    public void queryHistoryTraceData(HistoryTraceQueryOptions historyTraceQueryOptions) {
        C1105b bVar = this.f3227a;
        if (bVar != null) {
            bVar.mo13181a(historyTraceQueryOptions);
        }
    }

    public void release() {
        C1105b bVar = this.f3227a;
        if (bVar != null) {
            bVar.mo13179a();
        }
    }

    public void renderHistoryTrace(BaiduMap baiduMap, HistoryTraceData historyTraceData, HistoryTraceDisplayOptions historyTraceDisplayOptions, int i) {
        C1105b bVar = this.f3227a;
        if (bVar != null) {
            bVar.mo13180a(baiduMap, historyTraceData, historyTraceDisplayOptions, i);
        }
    }

    public void setHttpsEnable(boolean z) {
        C1105b bVar = this.f3227a;
        if (bVar == null) {
            C1120a.m3855b("HistoryTraceManager", "The implement instance is null");
        } else {
            bVar.mo13183a(z);
        }
    }

    public void setOnHistoryTraceListener(OnHistoryTraceListener onHistoryTraceListener) {
        if (onHistoryTraceListener != null) {
            C1105b bVar = this.f3227a;
            if (bVar != null) {
                bVar.mo13182a(onHistoryTraceListener);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("HistoryTraceManager-- OnHistoryTraceListener must not be null.");
    }
}
