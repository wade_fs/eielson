package com.baidu.mapapi.map;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import com.baidu.mapapi.model.LatLng;

public final class DotOptions extends OverlayOptions {

    /* renamed from: a */
    int f2456a;

    /* renamed from: b */
    boolean f2457b = true;

    /* renamed from: c */
    Bundle f2458c;

    /* renamed from: d */
    private LatLng f2459d;

    /* renamed from: e */
    private int f2460e = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: f */
    private int f2461f = 5;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Overlay mo10909a() {
        Dot dot = new Dot();
        dot.f2662A = this.f2457b;
        dot.f2665z = this.f2456a;
        dot.f2663B = this.f2458c;
        dot.f2454b = this.f2460e;
        dot.f2453a = this.f2459d;
        dot.f2455c = this.f2461f;
        return dot;
    }

    public DotOptions center(LatLng latLng) {
        if (latLng != null) {
            this.f2459d = latLng;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: dot center can not be null");
    }

    public DotOptions color(int i) {
        this.f2460e = i;
        return this;
    }

    public DotOptions extraInfo(Bundle bundle) {
        this.f2458c = bundle;
        return this;
    }

    public LatLng getCenter() {
        return this.f2459d;
    }

    public int getColor() {
        return this.f2460e;
    }

    public Bundle getExtraInfo() {
        return this.f2458c;
    }

    public int getRadius() {
        return this.f2461f;
    }

    public int getZIndex() {
        return this.f2456a;
    }

    public boolean isVisible() {
        return this.f2457b;
    }

    public DotOptions radius(int i) {
        if (i > 0) {
            this.f2461f = i;
        }
        return this;
    }

    public DotOptions visible(boolean z) {
        this.f2457b = z;
        return this;
    }

    public DotOptions zIndex(int i) {
        this.f2456a = i;
        return this;
    }
}
