package com.baidu.mapapi.map;

import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapsdkplatform.comapi.map.C1063aa;

/* renamed from: com.baidu.mapapi.map.f */
/* synthetic */ class C0929f {

    /* renamed from: a */
    static final /* synthetic */ int[] f2840a = new int[MyLocationConfiguration.LocationMode.values().length];

    /* renamed from: b */
    static final /* synthetic */ int[] f2841b = new int[C1063aa.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|5|6|7|9|10|11|12|13|14|16) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003c */
    static {
        /*
            com.baidu.mapsdkplatform.comapi.map.aa[] r0 = com.baidu.mapsdkplatform.comapi.map.C1063aa.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.baidu.mapapi.map.C0929f.f2841b = r0
            r0 = 1
            int[] r1 = com.baidu.mapapi.map.C0929f.f2841b     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.baidu.mapsdkplatform.comapi.map.aa r2 = com.baidu.mapsdkplatform.comapi.map.C1063aa.TextureView     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.baidu.mapapi.map.C0929f.f2841b     // Catch:{ NoSuchFieldError -> 0x001f }
            com.baidu.mapsdkplatform.comapi.map.aa r3 = com.baidu.mapsdkplatform.comapi.map.C1063aa.GLSurfaceView     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            com.baidu.mapapi.map.MyLocationConfiguration$LocationMode[] r2 = com.baidu.mapapi.map.MyLocationConfiguration.LocationMode.values()
            int r2 = r2.length
            int[] r2 = new int[r2]
            com.baidu.mapapi.map.C0929f.f2840a = r2
            int[] r2 = com.baidu.mapapi.map.C0929f.f2840a     // Catch:{ NoSuchFieldError -> 0x0032 }
            com.baidu.mapapi.map.MyLocationConfiguration$LocationMode r3 = com.baidu.mapapi.map.MyLocationConfiguration.LocationMode.COMPASS     // Catch:{ NoSuchFieldError -> 0x0032 }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
            r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
        L_0x0032:
            int[] r0 = com.baidu.mapapi.map.C0929f.f2840a     // Catch:{ NoSuchFieldError -> 0x003c }
            com.baidu.mapapi.map.MyLocationConfiguration$LocationMode r2 = com.baidu.mapapi.map.MyLocationConfiguration.LocationMode.FOLLOWING     // Catch:{ NoSuchFieldError -> 0x003c }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
            r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
        L_0x003c:
            int[] r0 = com.baidu.mapapi.map.C0929f.f2840a     // Catch:{ NoSuchFieldError -> 0x0047 }
            com.baidu.mapapi.map.MyLocationConfiguration$LocationMode r1 = com.baidu.mapapi.map.MyLocationConfiguration.LocationMode.NORMAL     // Catch:{ NoSuchFieldError -> 0x0047 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0047 }
        L_0x0047:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.map.C0929f.<clinit>():void");
    }
}
