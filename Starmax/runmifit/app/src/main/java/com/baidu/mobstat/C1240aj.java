package com.baidu.mobstat;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.aj */
public class C1240aj {

    /* renamed from: a */
    public boolean f4275a = false;

    /* renamed from: b */
    public String f4276b = "";

    /* renamed from: c */
    public boolean f4277c = false;

    public C1240aj() {
    }

    public C1240aj(JSONObject jSONObject) {
        try {
            this.f4275a = jSONObject.getBoolean("SDK_BPLUS_SERVICE");
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
        }
        try {
            this.f4276b = jSONObject.getString("SDK_PRODUCT_LY");
        } catch (Exception e2) {
            C1255as.m4626c().mo13944b(e2);
        }
        try {
            this.f4277c = jSONObject.getBoolean("SDK_LOCAL_SERVER");
        } catch (Exception e3) {
            C1255as.m4626c().mo13944b(e3);
        }
    }

    /* renamed from: a */
    public JSONObject mo13913a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("SDK_BPLUS_SERVICE", this.f4275a);
        } catch (JSONException e) {
            C1255as.m4626c().mo13944b(e);
        }
        try {
            jSONObject.put("SDK_PRODUCT_LY", this.f4276b);
        } catch (JSONException e2) {
            C1255as.m4626c().mo13944b(e2);
        }
        try {
            jSONObject.put("SDK_LOCAL_SERVER", this.f4277c);
        } catch (JSONException e3) {
            C1255as.m4626c().mo13944b(e3);
        }
        return jSONObject;
    }
}
