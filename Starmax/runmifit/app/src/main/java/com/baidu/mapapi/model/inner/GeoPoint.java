package com.baidu.mapapi.model.inner;

public class GeoPoint {

    /* renamed from: a */
    private double f2888a;

    /* renamed from: b */
    private double f2889b;

    public GeoPoint(double d, double d2) {
        this.f2888a = d;
        this.f2889b = d2;
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        GeoPoint geoPoint = (GeoPoint) obj;
        return this.f2888a == geoPoint.f2888a && this.f2889b == geoPoint.f2889b;
    }

    public double getLatitudeE6() {
        return this.f2888a;
    }

    public double getLongitudeE6() {
        return this.f2889b;
    }

    public void setLatitudeE6(double d) {
        this.f2888a = d;
    }

    public void setLongitudeE6(double d) {
        this.f2889b = d;
    }

    public String toString() {
        return "GeoPoint: Latitude: " + this.f2888a + ", Longitude: " + this.f2889b;
    }
}
