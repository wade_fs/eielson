package com.baidu.mapapi.favorite;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.Point;
import com.baidu.mapsdkplatform.comapi.favrite.FavSyncPoi;
import com.baidu.mobstat.Config;
import org.json.JSONObject;

/* renamed from: com.baidu.mapapi.favorite.a */
class C0915a {
    /* renamed from: a */
    static FavoritePoiInfo m2808a(FavSyncPoi favSyncPoi) {
        if (favSyncPoi == null || favSyncPoi.f3377c == null || favSyncPoi.f3376b.equals("")) {
            return null;
        }
        FavoritePoiInfo favoritePoiInfo = new FavoritePoiInfo();
        favoritePoiInfo.f2343a = favSyncPoi.f3375a;
        favoritePoiInfo.f2344b = favSyncPoi.f3376b;
        double d = (double) favSyncPoi.f3377c.f2891y;
        Double.isNaN(d);
        double d2 = (double) favSyncPoi.f3377c.f2890x;
        Double.isNaN(d2);
        favoritePoiInfo.f2345c = new LatLng(d / 1000000.0d, d2 / 1000000.0d);
        favoritePoiInfo.f2347e = favSyncPoi.f3379e;
        favoritePoiInfo.f2348f = favSyncPoi.f3380f;
        favoritePoiInfo.f2346d = favSyncPoi.f3378d;
        favoritePoiInfo.f2349g = Long.parseLong(favSyncPoi.f3382h);
        return favoritePoiInfo;
    }

    /* renamed from: a */
    static FavoritePoiInfo m2809a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        FavoritePoiInfo favoritePoiInfo = new FavoritePoiInfo();
        JSONObject optJSONObject = jSONObject.optJSONObject(Config.PLATFORM_TYPE);
        if (optJSONObject != null) {
            int optInt = optJSONObject.optInt(Config.EVENT_HEAT_X);
            double optInt2 = (double) optJSONObject.optInt("y");
            Double.isNaN(optInt2);
            double d = (double) optInt;
            Double.isNaN(d);
            favoritePoiInfo.f2345c = new LatLng(optInt2 / 1000000.0d, d / 1000000.0d);
        }
        favoritePoiInfo.f2344b = jSONObject.optString("uspoiname");
        favoritePoiInfo.f2349g = Long.parseLong(jSONObject.optString("addtimesec"));
        favoritePoiInfo.f2346d = jSONObject.optString("addr");
        favoritePoiInfo.f2348f = jSONObject.optString("uspoiuid");
        favoritePoiInfo.f2347e = jSONObject.optString("ncityid");
        favoritePoiInfo.f2343a = jSONObject.optString("key");
        return favoritePoiInfo;
    }

    /* renamed from: a */
    static FavSyncPoi m2810a(FavoritePoiInfo favoritePoiInfo) {
        if (favoritePoiInfo == null || favoritePoiInfo.f2345c == null || favoritePoiInfo.f2344b == null || favoritePoiInfo.f2344b.equals("")) {
            return null;
        }
        FavSyncPoi favSyncPoi = new FavSyncPoi();
        favSyncPoi.f3376b = favoritePoiInfo.f2344b;
        favSyncPoi.f3377c = new Point((int) (favoritePoiInfo.f2345c.longitude * 1000000.0d), (int) (favoritePoiInfo.f2345c.latitude * 1000000.0d));
        favSyncPoi.f3378d = favoritePoiInfo.f2346d;
        favSyncPoi.f3379e = favoritePoiInfo.f2347e;
        favSyncPoi.f3380f = favoritePoiInfo.f2348f;
        favSyncPoi.f3383i = false;
        return favSyncPoi;
    }
}
