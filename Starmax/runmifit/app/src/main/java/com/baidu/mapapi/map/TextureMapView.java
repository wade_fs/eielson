package com.baidu.mapapi.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.MapViewLayoutParams;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapsdkplatform.comapi.commonutils.C1053a;
import com.baidu.mapsdkplatform.comapi.map.C1066ac;
import com.baidu.mapsdkplatform.comapi.map.C1074ak;
import com.baidu.mapsdkplatform.comapi.map.C1083i;
import com.baidu.mapsdkplatform.comapi.map.C1087l;
import com.baidu.mapsdkplatform.comapi.util.CustomMapStyleLoader;
import java.io.File;

public final class TextureMapView extends ViewGroup {

    /* renamed from: a */
    private static final String f2745a = TextureMapView.class.getSimpleName();

    /* renamed from: i */
    private static String f2746i;

    /* renamed from: j */
    private static int f2747j = 0;

    /* renamed from: k */
    private static int f2748k = 0;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public static final SparseArray<Integer> f2749q = new SparseArray<>();

    /* renamed from: A */
    private int f2750A;

    /* renamed from: B */
    private int f2751B;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C1066ac f2752b;

    /* renamed from: c */
    private BaiduMap f2753c;

    /* renamed from: d */
    private ImageView f2754d;

    /* renamed from: e */
    private Bitmap f2755e;

    /* renamed from: f */
    private C1074ak f2756f;

    /* renamed from: g */
    private Point f2757g;

    /* renamed from: h */
    private Point f2758h;

    /* renamed from: l */
    private RelativeLayout f2759l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public TextView f2760m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public TextView f2761n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public ImageView f2762o;

    /* renamed from: p */
    private Context f2763p;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public float f2764r;

    /* renamed from: s */
    private C1087l f2765s;

    /* renamed from: t */
    private int f2766t = LogoPosition.logoPostionleftBottom.ordinal();

    /* renamed from: u */
    private boolean f2767u = true;

    /* renamed from: v */
    private boolean f2768v = true;

    /* renamed from: w */
    private int f2769w;

    /* renamed from: x */
    private int f2770x;

    /* renamed from: y */
    private int f2771y;

    /* renamed from: z */
    private int f2772z;

    static {
        f2749q.append(3, 2000000);
        f2749q.append(4, 1000000);
        f2749q.append(5, 500000);
        f2749q.append(6, 200000);
        f2749q.append(7, 100000);
        f2749q.append(8, 50000);
        f2749q.append(9, 25000);
        f2749q.append(10, 20000);
        f2749q.append(11, 10000);
        f2749q.append(12, 5000);
        f2749q.append(13, 2000);
        f2749q.append(14, 1000);
        f2749q.append(15, 500);
        f2749q.append(16, 200);
        f2749q.append(17, 100);
        f2749q.append(18, 50);
        f2749q.append(19, 20);
        f2749q.append(20, 10);
        f2749q.append(21, 5);
        f2749q.append(22, 2);
    }

    public TextureMapView(Context context) {
        super(context);
        m2968a(context, (BaiduMapOptions) null);
    }

    public TextureMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        m2968a(context, (BaiduMapOptions) null);
    }

    public TextureMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        m2968a(context, (BaiduMapOptions) null);
    }

    public TextureMapView(Context context, BaiduMapOptions baiduMapOptions) {
        super(context);
        m2968a(context, baiduMapOptions);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2967a(android.content.Context r10) {
        /*
            r9 = this;
            int r0 = com.baidu.mapapi.common.SysOSUtil.getDensityDpi()
            r1 = 180(0xb4, float:2.52E-43)
            if (r0 >= r1) goto L_0x000b
            java.lang.String r1 = "logo_l.png"
            goto L_0x000d
        L_0x000b:
            java.lang.String r1 = "logo_h.png"
        L_0x000d:
            android.graphics.Bitmap r2 = com.baidu.mapsdkplatform.comapi.commonutils.C1053a.m3464a(r1, r10)
            r1 = 480(0x1e0, float:6.73E-43)
            if (r0 <= r1) goto L_0x0031
            android.graphics.Matrix r7 = new android.graphics.Matrix
            r7.<init>()
            r0 = 1073741824(0x40000000, float:2.0)
        L_0x001c:
            r7.postScale(r0, r0)
            r3 = 0
            r4 = 0
            int r5 = r2.getWidth()
            int r6 = r2.getHeight()
            r8 = 1
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r2, r3, r4, r5, r6, r7, r8)
            r9.f2755e = r0
            goto L_0x0041
        L_0x0031:
            r3 = 320(0x140, float:4.48E-43)
            if (r0 <= r3) goto L_0x003f
            if (r0 > r1) goto L_0x003f
            android.graphics.Matrix r7 = new android.graphics.Matrix
            r7.<init>()
            r0 = 1069547520(0x3fc00000, float:1.5)
            goto L_0x001c
        L_0x003f:
            r9.f2755e = r2
        L_0x0041:
            android.graphics.Bitmap r0 = r9.f2755e
            if (r0 == 0) goto L_0x0058
            android.widget.ImageView r0 = new android.widget.ImageView
            r0.<init>(r10)
            r9.f2754d = r0
            android.widget.ImageView r10 = r9.f2754d
            android.graphics.Bitmap r0 = r9.f2755e
            r10.setImageBitmap(r0)
            android.widget.ImageView r10 = r9.f2754d
            r9.addView(r10)
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.map.TextureMapView.m2967a(android.content.Context):void");
    }

    /* renamed from: a */
    private void m2968a(Context context, BaiduMapOptions baiduMapOptions) {
        setBackgroundColor(-1);
        this.f2763p = context;
        C1083i.m3649a();
        BMapManager.init();
        m2969a(context, baiduMapOptions, f2747j == 0 ? f2746i : CustomMapStyleLoader.getCustomStyleFilePath(), f2748k);
        this.f2753c = new BaiduMap(this.f2752b);
        m2967a(context);
        m2973b(context);
        if (baiduMapOptions != null && !baiduMapOptions.f2432h) {
            this.f2756f.setVisibility(View.INVISIBLE);
        }
        m2975c(context);
        if (baiduMapOptions != null && !baiduMapOptions.f2433i) {
            this.f2759l.setVisibility(View.INVISIBLE);
        }
        if (!(baiduMapOptions == null || baiduMapOptions.f2434j == null)) {
            this.f2766t = baiduMapOptions.f2434j.ordinal();
        }
        if (!(baiduMapOptions == null || baiduMapOptions.f2436l == null)) {
            this.f2758h = baiduMapOptions.f2436l;
        }
        if (baiduMapOptions != null && baiduMapOptions.f2435k != null) {
            this.f2757g = baiduMapOptions.f2435k;
        }
    }

    /* renamed from: a */
    private void m2969a(Context context, BaiduMapOptions baiduMapOptions, String str, int i) {
        if (baiduMapOptions == null) {
            this.f2752b = new C1066ac(context, null, str, i);
        } else {
            this.f2752b = new C1066ac(context, baiduMapOptions.mo11031a(), str, i);
        }
        addView(this.f2752b);
        this.f2765s = new C0945t(this);
        this.f2752b.mo12954b().mo13018a(this.f2765s);
    }

    /* renamed from: a */
    private void m2970a(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-2, -2);
        }
        int i = layoutParams.width;
        int makeMeasureSpec = i > 0 ? View.MeasureSpec.makeMeasureSpec(i, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0);
        int i2 = layoutParams.height;
        view.measure(makeMeasureSpec, i2 > 0 ? View.MeasureSpec.makeMeasureSpec(i2, 1073741824) : View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m2972b() {
        if (this.f2756f.mo12980a()) {
            float f = this.f2752b.mo12954b().mo12989E().f3414a;
            boolean z = true;
            this.f2756f.mo12983b(f > this.f2752b.mo12954b().f3527b);
            C1074ak akVar = this.f2756f;
            if (f >= this.f2752b.mo12954b().f3505a) {
                z = false;
            }
            akVar.mo12979a(z);
        }
    }

    /* renamed from: b */
    private void m2973b(Context context) {
        this.f2756f = new C1074ak(context);
        if (this.f2756f.mo12980a()) {
            this.f2756f.mo12982b(new C0946u(this));
            this.f2756f.mo12978a(new C0947v(this));
            addView(this.f2756f);
        }
    }

    /* renamed from: c */
    private void m2975c(Context context) {
        this.f2759l = new RelativeLayout(context);
        this.f2759l.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.f2760m = new TextView(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        this.f2760m.setTextColor(Color.parseColor("#FFFFFF"));
        this.f2760m.setTextSize(2, 11.0f);
        TextView textView = this.f2760m;
        textView.setTypeface(textView.getTypeface(), 1);
        this.f2760m.setLayoutParams(layoutParams);
        this.f2760m.setId(Integer.MAX_VALUE);
        this.f2759l.addView(this.f2760m);
        this.f2761n = new TextView(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.width = -2;
        layoutParams2.height = -2;
        layoutParams2.addRule(14);
        this.f2761n.setTextColor(Color.parseColor("#000000"));
        this.f2761n.setTextSize(2, 11.0f);
        this.f2761n.setLayoutParams(layoutParams2);
        this.f2759l.addView(this.f2761n);
        this.f2762o = new ImageView(context);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.width = -2;
        layoutParams3.height = -2;
        layoutParams3.addRule(14);
        layoutParams3.addRule(3, this.f2760m.getId());
        this.f2762o.setLayoutParams(layoutParams3);
        Bitmap a = C1053a.m3464a("icon_scale.9.png", context);
        byte[] ninePatchChunk = a.getNinePatchChunk();
        NinePatch.isNinePatchChunk(ninePatchChunk);
        this.f2762o.setBackgroundDrawable(new NinePatchDrawable(a, ninePatchChunk, new Rect(), null));
        this.f2759l.addView(this.f2762o);
        addView(this.f2759l);
    }

    public static void setCustomMapStylePath(String str) {
        if (str == null || str.length() == 0) {
            throw new RuntimeException("BDMapSDKException: customMapStylePath String is illegal");
        } else if (new File(str).exists()) {
            f2746i = str;
        } else {
            throw new RuntimeException("BDMapSDKException: please check whether the customMapStylePath file exits");
        }
    }

    @Deprecated
    public static void setIconCustom(int i) {
        f2748k = i;
    }

    public static void setLoadCustomMapStyleFileMode(int i) {
        f2747j = i;
    }

    @Deprecated
    public static void setMapCustomEnable(boolean z) {
        C1083i.m3651a(z);
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof MapViewLayoutParams) {
            super.addView(view, layoutParams);
        }
    }

    public final LogoPosition getLogoPosition() {
        int i = this.f2766t;
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? i != 5 ? LogoPosition.logoPostionleftBottom : LogoPosition.logoPostionRightTop : LogoPosition.logoPostionRightBottom : LogoPosition.logoPostionCenterTop : LogoPosition.logoPostionCenterBottom : LogoPosition.logoPostionleftTop;
    }

    public final BaiduMap getMap() {
        BaiduMap baiduMap = this.f2753c;
        baiduMap.f2401b = this;
        return baiduMap;
    }

    public final int getMapLevel() {
        return f2749q.get((int) this.f2752b.mo12954b().mo12989E().f3414a).intValue();
    }

    public int getScaleControlViewHeight() {
        return this.f2751B;
    }

    public int getScaleControlViewWidth() {
        return this.f2751B;
    }

    public void onCreate(Context context, Bundle bundle) {
        if (bundle != null) {
            MapStatus mapStatus = (MapStatus) bundle.getParcelable("mapstatus");
            if (this.f2757g != null) {
                this.f2757g = (Point) bundle.getParcelable("scalePosition");
            }
            if (this.f2758h != null) {
                this.f2758h = (Point) bundle.getParcelable("zoomPosition");
            }
            this.f2767u = bundle.getBoolean("mZoomControlEnabled");
            this.f2768v = bundle.getBoolean("mScaleControlEnabled");
            this.f2766t = bundle.getInt("logoPosition");
            setPadding(bundle.getInt("paddingLeft"), bundle.getInt("paddingTop"), bundle.getInt("paddingRight"), bundle.getInt("paddingBottom"));
            m2968a(context, new BaiduMapOptions().mapStatus(mapStatus));
        }
    }

    public final void onDestroy() {
        Context context = this.f2763p;
        if (context != null) {
            this.f2752b.mo12952a(context.hashCode());
        }
        Bitmap bitmap = this.f2755e;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f2755e.recycle();
        }
        this.f2756f.mo12981b();
        BMapManager.destroy();
        C1083i.m3652b();
        this.f2763p = null;
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        float f;
        Point point;
        int i5;
        int i6;
        int height;
        int childCount = getChildCount();
        m2970a(this.f2754d);
        float f2 = 1.0f;
        if (((getWidth() - this.f2769w) - this.f2770x) - this.f2754d.getMeasuredWidth() <= 0 || ((getHeight() - this.f2771y) - this.f2772z) - this.f2754d.getMeasuredHeight() <= 0) {
            this.f2769w = 0;
            this.f2770x = 0;
            this.f2772z = 0;
            this.f2771y = 0;
            f = 1.0f;
        } else {
            f2 = ((float) ((getWidth() - this.f2769w) - this.f2770x)) / ((float) getWidth());
            f = ((float) ((getHeight() - this.f2771y) - this.f2772z)) / ((float) getHeight());
        }
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            C1066ac acVar = this.f2752b;
            if (childAt == acVar) {
                acVar.layout(0, 0, getWidth(), getHeight());
            } else {
                ImageView imageView = this.f2754d;
                if (childAt == imageView) {
                    float f3 = f2 * 5.0f;
                    int i8 = (int) (((float) this.f2769w) + f3);
                    int i9 = (int) (((float) this.f2770x) + f3);
                    float f4 = 5.0f * f;
                    int i10 = (int) (((float) this.f2771y) + f4);
                    int i11 = (int) (((float) this.f2772z) + f4);
                    int i12 = this.f2766t;
                    if (i12 != 1) {
                        if (i12 == 2) {
                            height = getHeight() - i11;
                            i10 = height - this.f2754d.getMeasuredHeight();
                        } else if (i12 != 3) {
                            if (i12 == 4) {
                                i6 = getHeight() - i11;
                                i10 = i6 - this.f2754d.getMeasuredHeight();
                            } else if (i12 != 5) {
                                i6 = getHeight() - i11;
                                i5 = this.f2754d.getMeasuredWidth() + i8;
                                i10 = i6 - this.f2754d.getMeasuredHeight();
                            } else {
                                i6 = i10 + imageView.getMeasuredHeight();
                            }
                            i5 = getWidth() - i9;
                            i8 = i5 - this.f2754d.getMeasuredWidth();
                        } else {
                            height = i10 + imageView.getMeasuredHeight();
                        }
                        i8 = (((getWidth() - this.f2754d.getMeasuredWidth()) + this.f2769w) - this.f2770x) / 2;
                        i5 = (((getWidth() + this.f2754d.getMeasuredWidth()) + this.f2769w) - this.f2770x) / 2;
                    } else {
                        i6 = imageView.getMeasuredHeight() + i10;
                        i5 = this.f2754d.getMeasuredWidth() + i8;
                    }
                    this.f2754d.layout(i8, i10, i5, i6);
                } else {
                    C1074ak akVar = this.f2756f;
                    if (childAt != akVar) {
                        RelativeLayout relativeLayout = this.f2759l;
                        if (childAt == relativeLayout) {
                            m2970a(relativeLayout);
                            Point point2 = this.f2757g;
                            if (point2 == null) {
                                this.f2751B = this.f2759l.getMeasuredWidth();
                                this.f2750A = this.f2759l.getMeasuredHeight();
                                int i13 = (int) (((float) this.f2769w) + (5.0f * f2));
                                int height2 = (getHeight() - ((int) ((((float) this.f2772z) + (f * 5.0f)) + 56.0f))) - this.f2754d.getMeasuredHeight();
                                this.f2759l.layout(i13, height2, this.f2751B + i13, this.f2750A + height2);
                            } else {
                                this.f2759l.layout(point2.x, this.f2757g.y, this.f2757g.x + this.f2759l.getMeasuredWidth(), this.f2757g.y + this.f2759l.getMeasuredHeight());
                            }
                        } else {
                            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
                            if (layoutParams instanceof MapViewLayoutParams) {
                                MapViewLayoutParams mapViewLayoutParams = (MapViewLayoutParams) layoutParams;
                                if (mapViewLayoutParams.f2596c == MapViewLayoutParams.ELayoutMode.absoluteMode) {
                                    point = mapViewLayoutParams.f2595b;
                                } else {
                                    point = this.f2752b.mo12954b().mo13005a(CoordUtil.ll2mc(mapViewLayoutParams.f2594a));
                                }
                                m2970a(childAt);
                                int measuredWidth = childAt.getMeasuredWidth();
                                int measuredHeight = childAt.getMeasuredHeight();
                                float f5 = mapViewLayoutParams.f2597d;
                                float f6 = mapViewLayoutParams.f2598e;
                                int i14 = (int) (((float) point.x) - (f5 * ((float) measuredWidth)));
                                int i15 = ((int) (((float) point.y) - (f6 * ((float) measuredHeight)))) + mapViewLayoutParams.f2599f;
                                childAt.layout(i14, i15, measuredWidth + i14, measuredHeight + i15);
                            }
                        }
                    } else if (akVar.mo12980a()) {
                        m2970a(this.f2756f);
                        Point point3 = this.f2758h;
                        if (point3 == null) {
                            int height3 = (int) ((((float) (getHeight() - 15)) * f) + ((float) this.f2771y));
                            int width = (int) ((((float) (getWidth() - 15)) * f2) + ((float) this.f2769w));
                            int measuredWidth2 = width - this.f2756f.getMeasuredWidth();
                            int measuredHeight2 = height3 - this.f2756f.getMeasuredHeight();
                            if (this.f2766t == 4) {
                                height3 -= this.f2754d.getMeasuredHeight();
                                measuredHeight2 -= this.f2754d.getMeasuredHeight();
                            }
                            this.f2756f.layout(measuredWidth2, measuredHeight2, width, height3);
                        } else {
                            this.f2756f.layout(point3.x, this.f2758h.y, this.f2758h.x + this.f2756f.getMeasuredWidth(), this.f2758h.y + this.f2756f.getMeasuredHeight());
                        }
                    }
                }
            }
        }
    }

    public final void onPause() {
        this.f2752b.mo12956d();
    }

    public final void onResume() {
        this.f2752b.mo12955c();
    }

    public void onSaveInstanceState(Bundle bundle) {
        BaiduMap baiduMap;
        if (bundle != null && (baiduMap = this.f2753c) != null) {
            bundle.putParcelable("mapstatus", baiduMap.getMapStatus());
            Point point = this.f2757g;
            if (point != null) {
                bundle.putParcelable("scalePosition", point);
            }
            Point point2 = this.f2758h;
            if (point2 != null) {
                bundle.putParcelable("zoomPosition", point2);
            }
            bundle.putBoolean("mZoomControlEnabled", this.f2767u);
            bundle.putBoolean("mScaleControlEnabled", this.f2768v);
            bundle.putInt("logoPosition", this.f2766t);
            bundle.putInt("paddingLeft", this.f2769w);
            bundle.putInt("paddingTop", this.f2771y);
            bundle.putInt("paddingRight", this.f2770x);
            bundle.putInt("paddingBottom", this.f2772z);
        }
    }

    public void removeView(View view) {
        if (view != this.f2754d) {
            super.removeView(view);
        }
    }

    public final void setLogoPosition(LogoPosition logoPosition) {
        if (logoPosition == null) {
            logoPosition = LogoPosition.logoPostionleftBottom;
        }
        this.f2766t = logoPosition.ordinal();
        requestLayout();
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.f2769w = i;
        this.f2771y = i2;
        this.f2770x = i3;
        this.f2772z = i4;
    }

    public void setScaleControlPosition(Point point) {
        if (point != null && point.x >= 0 && point.y >= 0 && point.x <= getWidth() && point.y <= getHeight()) {
            this.f2757g = point;
            requestLayout();
        }
    }

    public void setZoomControlsPosition(Point point) {
        if (point != null && point.x >= 0 && point.y >= 0 && point.x <= getWidth() && point.y <= getHeight()) {
            this.f2758h = point;
            requestLayout();
        }
    }

    public void showScaleControl(boolean z) {
        this.f2759l.setVisibility(z ? 0 : 8);
        this.f2768v = z;
    }

    public void showZoomControls(boolean z) {
        if (this.f2756f.mo12980a()) {
            this.f2756f.setVisibility(z ? 0 : 8);
            this.f2767u = z;
        }
    }
}
