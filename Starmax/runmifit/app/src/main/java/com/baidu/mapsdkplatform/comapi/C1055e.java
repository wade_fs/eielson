package com.baidu.mapsdkplatform.comapi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.baidu.mapapi.NetworkUtil;
import com.baidu.mapsdkplatform.comapi.util.C1175i;

/* renamed from: com.baidu.mapsdkplatform.comapi.e */
public class C1055e extends BroadcastReceiver {

    /* renamed from: a */
    public static final String f3374a = C1055e.class.getSimpleName();

    /* renamed from: a */
    public void mo12930a(Context context) {
        String currentNetMode = NetworkUtil.getCurrentNetMode(context);
        String e = C1175i.m4274e();
        if (e != null && !e.equals(currentNetMode)) {
            C1175i.m4265a(currentNetMode);
        }
    }

    public void onReceive(Context context, Intent intent) {
        mo12930a(context);
        NetworkUtil.updateNetworkProxy(context);
    }
}
