package com.baidu.mapapi.map;

import android.graphics.Point;
import com.baidu.mapapi.map.C0938o.C0939a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: com.baidu.mapapi.map.o */
class C0938o<T extends C0939a> {

    /* renamed from: a */
    private final C0931h f2855a;

    /* renamed from: b */
    private final int f2856b;

    /* renamed from: c */
    private List<T> f2857c;

    /* renamed from: d */
    private List<C0938o<T>> f2858d;

    /* renamed from: com.baidu.mapapi.map.o$a */
    static abstract class C0939a {
        C0939a() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract Point mo11535a();
    }

    private C0938o(double d, double d2, double d3, double d4, int i) {
        this(new C0931h(d, d2, d3, d4), i);
    }

    public C0938o(C0931h hVar) {
        this(hVar, 0);
    }

    private C0938o(C0931h hVar, int i) {
        this.f2858d = null;
        this.f2855a = hVar;
        this.f2856b = i;
    }

    /* renamed from: a */
    private void m3076a() {
        this.f2858d = new ArrayList(4);
        this.f2858d.add(new C0938o(this.f2855a.f2842a, this.f2855a.f2846e, this.f2855a.f2843b, this.f2855a.f2847f, this.f2856b + 1));
        this.f2858d.add(new C0938o(this.f2855a.f2846e, this.f2855a.f2844c, this.f2855a.f2843b, this.f2855a.f2847f, this.f2856b + 1));
        this.f2858d.add(new C0938o(this.f2855a.f2842a, this.f2855a.f2846e, this.f2855a.f2847f, this.f2855a.f2845d, this.f2856b + 1));
        this.f2858d.add(new C0938o(this.f2855a.f2846e, this.f2855a.f2844c, this.f2855a.f2847f, this.f2855a.f2845d, this.f2856b + 1));
        List<T> list = this.f2857c;
        this.f2857c = null;
        for (T t : list) {
            m3077a((double) t.mo11535a().x, (double) t.mo11535a().y, t);
        }
    }

    /* renamed from: a */
    private void m3077a(double d, double d2, T t) {
        int i;
        List<C0938o<T>> list;
        if (this.f2858d != null) {
            int i2 = (d2 > this.f2855a.f2847f ? 1 : (d2 == this.f2855a.f2847f ? 0 : -1));
            double d3 = this.f2855a.f2846e;
            if (i2 < 0) {
                int i3 = (d > d3 ? 1 : (d == d3 ? 0 : -1));
                list = this.f2858d;
                i = i3 < 0 ? 0 : 1;
            } else {
                int i4 = (d > d3 ? 1 : (d == d3 ? 0 : -1));
                list = this.f2858d;
                i = i4 < 0 ? 2 : 3;
            }
            list.get(i).m3077a(d, d2, t);
            return;
        }
        if (this.f2857c == null) {
            this.f2857c = new ArrayList();
        }
        this.f2857c.add(t);
        if (this.f2857c.size() > 40 && this.f2856b < 40) {
            m3076a();
        }
    }

    /* renamed from: a */
    private void m3078a(C0931h hVar, Collection<T> collection) {
        if (this.f2855a.mo11568a(hVar)) {
            List<C0938o<T>> list = this.f2858d;
            if (list != null) {
                for (C0938o<T> oVar : list) {
                    oVar.m3078a(hVar, collection);
                }
            } else if (this.f2857c == null) {
            } else {
                if (hVar.mo11569b(this.f2855a)) {
                    collection.addAll(this.f2857c);
                    return;
                }
                for (T t : this.f2857c) {
                    if (hVar.mo11567a(t.mo11535a())) {
                        collection.add(t);
                    }
                }
            }
        }
    }

    /* renamed from: a */
    public Collection<T> mo11578a(C0931h hVar) {
        ArrayList arrayList = new ArrayList();
        m3078a(hVar, arrayList);
        return arrayList;
    }

    /* renamed from: a */
    public void mo11579a(WeightedLatLng weightedLatLng) {
        Point a = weightedLatLng.mo11535a();
        if (this.f2855a.mo11565a((double) a.x, (double) a.y)) {
            m3077a((double) a.x, (double) a.y, weightedLatLng);
        }
    }
}
