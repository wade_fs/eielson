package com.baidu.mapsdkplatform.comapi.synchronization.render;

import android.content.Context;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.e */
public class C1154e {

    /* renamed from: a */
    private static final String f3846a = C1154e.class.getSimpleName();

    /* renamed from: b */
    private C1155f f3847b = C1155f.m4182a();

    public C1154e(Context context, BaiduMap baiduMap) {
        this.f3847b.mo13372a(baiduMap);
    }

    /* renamed from: a */
    public void mo13357a() {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13380e();
        }
    }

    /* renamed from: a */
    public void mo13358a(int i) {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13370a(i);
        }
    }

    /* renamed from: a */
    public void mo13359a(int i, int i2, int i3, int i4) {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13371a(i, i2, i3, i4);
        }
    }

    /* renamed from: a */
    public void mo13360a(C1153d dVar) {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13373a(dVar);
        }
    }

    /* renamed from: b */
    public void mo13361b() {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13381f();
        }
    }

    /* renamed from: b */
    public void mo13362b(int i) {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13379d(i);
        }
    }

    /* renamed from: c */
    public Marker mo13363c() {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            return fVar.mo13374b();
        }
        C1120a.m3855b(f3846a, "The mSyncRenderStrategic created failed");
        return null;
    }

    /* renamed from: c */
    public void mo13364c(int i) {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13375b(i);
        }
    }

    /* renamed from: d */
    public Marker mo13365d() {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            return fVar.mo13376c();
        }
        C1120a.m3855b(f3846a, "The mSyncRenderStrategic created failed");
        return null;
    }

    /* renamed from: d */
    public void mo13366d(int i) {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13377c(i);
        }
    }

    /* renamed from: e */
    public Marker mo13367e() {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            return fVar.mo13378d();
        }
        C1120a.m3855b(f3846a, "The mSyncRenderStrategic created failed");
        return null;
    }

    /* renamed from: f */
    public void mo13368f() {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13382g();
            this.f3847b = null;
        }
    }

    /* renamed from: g */
    public void mo13369g() {
        C1155f fVar = this.f3847b;
        if (fVar != null) {
            fVar.mo13383h();
        }
    }
}
