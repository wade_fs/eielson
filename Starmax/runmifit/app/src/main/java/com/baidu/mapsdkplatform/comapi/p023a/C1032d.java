package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.AlphaAnimation;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.animation.RotateAnimation;
import com.baidu.mapapi.animation.ScaleAnimation;
import com.baidu.mapapi.animation.SingleScaleAnimation;
import com.baidu.mapapi.animation.Transformation;
import com.baidu.mapapi.map.Marker;
import java.util.ArrayList;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.d */
public class C1032d extends C1031c {

    /* renamed from: a */
    private Animator f3314a = null;

    /* renamed from: b */
    private long f3315b = 0;

    /* renamed from: c */
    private Interpolator f3316c = null;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Animation.AnimationListener f3317d = null;

    /* renamed from: e */
    private int f3318e = 0;

    /* renamed from: f */
    private ArrayList<Animation> f3319f = new ArrayList<>();

    /* renamed from: b */
    private ObjectAnimator m3378b(Marker marker, Animation animation) {
        if (animation instanceof AlphaAnimation) {
            return ((C1029a) animation.bdAnimation).mo12873a(marker);
        }
        if (animation instanceof RotateAnimation) {
            return ((C1034f) animation.bdAnimation).mo12894a(marker);
        }
        if (animation instanceof Transformation) {
            return ((C1040l) animation.bdAnimation).mo12912a(marker);
        }
        if (animation instanceof ScaleAnimation) {
            return ((C1036h) animation.bdAnimation).mo12900a(marker);
        }
        if (animation instanceof SingleScaleAnimation) {
            return ((C1038j) animation.bdAnimation).mo12906a(marker);
        }
        return null;
    }

    /* renamed from: a */
    public void mo12874a() {
        Animator animator = this.f3314a;
        if (animator != null) {
            animator.start();
        }
    }

    /* renamed from: a */
    public void mo12875a(int i) {
    }

    /* renamed from: a */
    public void mo12876a(long j) {
        if (j < 0) {
            j = 0;
        }
        this.f3315b = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12877a(Animator animator) {
        if (animator != null) {
            animator.addListener(new C1033e(this));
        }
    }

    /* renamed from: a */
    public void mo12878a(Interpolator interpolator) {
        this.f3316c = interpolator;
    }

    /* renamed from: a */
    public void mo12879a(Animation.AnimationListener animationListener) {
        this.f3317d = animationListener;
    }

    /* renamed from: a */
    public void mo12889a(Animation animation) {
        if (!this.f3319f.contains(animation)) {
            this.f3319f.add(animation);
        }
    }

    /* renamed from: a */
    public void mo12880a(Marker marker, Animation animation) {
        ObjectAnimator b;
        this.f3314a = new AnimatorSet();
        ArrayList<Animation> arrayList = this.f3319f;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.clear();
        for (int i = 0; i < arrayList.size(); i++) {
            Animation animation2 = arrayList.get(i);
            if (!(animation2 == null || (b = m3378b(marker, animation2)) == null)) {
                arrayList2.add(b);
            }
        }
        long j = this.f3315b;
        if (j != 0) {
            this.f3314a.setDuration(j);
        }
        Interpolator interpolator = this.f3316c;
        if (interpolator != null) {
            this.f3314a.setInterpolator(interpolator);
        }
        if (arrayList2.size() != 0) {
            int i2 = this.f3318e;
            if (i2 == 0) {
                ((AnimatorSet) this.f3314a).playTogether(arrayList2);
            } else if (i2 == 1) {
                ((AnimatorSet) this.f3314a).playSequentially(arrayList2);
            }
        }
        mo12877a(this.f3314a);
    }

    /* renamed from: b */
    public void mo12881b() {
        Animator animator = this.f3314a;
        if (animator != null) {
            animator.cancel();
            this.f3314a = null;
        }
    }

    /* renamed from: b */
    public void mo12882b(int i) {
    }

    /* renamed from: c */
    public void mo12884c(int i) {
        this.f3318e = i;
    }
}
