package com.baidu.mapapi.utils;

import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;

public class CoordinateConverter {

    /* renamed from: a */
    private LatLng f3234a;

    /* renamed from: b */
    private CoordType f3235b;

    public enum CoordType {
        GPS,
        COMMON,
        BD09LL,
        BD09MC
    }

    /* renamed from: a */
    private static LatLng m3260a(LatLng latLng) {
        return m3261a(latLng, CoordinateType.WGS84);
    }

    /* renamed from: a */
    private static LatLng m3261a(LatLng latLng, String str) {
        if (latLng == null) {
            return null;
        }
        return CoordUtil.Coordinate_encryptEx((float) latLng.longitude, (float) latLng.latitude, str);
    }

    /* renamed from: b */
    private static LatLng m3262b(LatLng latLng) {
        return m3261a(latLng, CoordinateType.GCJ02);
    }

    /* renamed from: c */
    private static LatLng m3263c(LatLng latLng) {
        return m3261a(latLng, CoordinateType.BD09MC);
    }

    /* renamed from: d */
    private static LatLng m3264d(LatLng latLng) {
        if (latLng == null) {
            return null;
        }
        return CoordTrans.baiduToGcj(latLng);
    }

    public LatLng convert() {
        if (this.f3234a == null) {
            return null;
        }
        if (this.f3235b == null) {
            this.f3235b = CoordType.GPS;
        }
        int i = C1011a.f3236a[this.f3235b.ordinal()];
        if (i == 1) {
            return m3262b(this.f3234a);
        }
        if (i == 2) {
            return m3260a(this.f3234a);
        }
        if (i == 3) {
            return m3264d(this.f3234a);
        }
        if (i != 4) {
            return null;
        }
        return m3263c(this.f3234a);
    }

    public CoordinateConverter coord(LatLng latLng) {
        this.f3234a = latLng;
        return this;
    }

    public CoordinateConverter from(CoordType coordType) {
        this.f3235b = coordType;
        return this;
    }
}
