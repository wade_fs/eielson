package com.baidu.platform.core.p036f;

import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.PoiChildrenInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1319d;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.core.f.c */
public class C1375c extends C1319d {

    /* renamed from: b */
    private static final String f4471b = C1375c.class.getSimpleName();

    /* renamed from: a */
    private LatLng m5131a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        double optDouble = jSONObject.optDouble("lat");
        double optDouble2 = jSONObject.optDouble("lng");
        return SDKInitializer.getCoordType() == CoordType.GCJ02 ? CoordTrans.baiduToGcj(new LatLng(optDouble, optDouble2)) : new LatLng(optDouble, optDouble2);
    }

    /* renamed from: a */
    private List<PoiChildrenInfo> m5132a(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (!(optJSONObject == null || optJSONObject.length() == 0)) {
                PoiChildrenInfo poiChildrenInfo = new PoiChildrenInfo();
                poiChildrenInfo.setUid(optJSONObject.optString(Config.CUSTOM_USER_ID));
                poiChildrenInfo.setName(optJSONObject.optString(Config.FEED_LIST_NAME));
                poiChildrenInfo.setShowName(optJSONObject.optString("show_name"));
                poiChildrenInfo.setTag(optJSONObject.optString("tag"));
                poiChildrenInfo.setAddress(optJSONObject.optString("address"));
                arrayList.add(poiChildrenInfo);
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    private boolean m5133a(String str, SuggestionResult suggestionResult) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.length() != 0) {
                int optInt = jSONObject.optInt(NotificationCompat.CATEGORY_STATUS);
                if (optInt == 0) {
                    return m5134a(jSONObject, suggestionResult);
                }
                suggestionResult.error = optInt != 1 ? optInt != 2 ? SearchResult.ERRORNO.RESULT_NOT_FOUND : SearchResult.ERRORNO.SEARCH_OPTION_ERROR : SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR;
                return false;
            }
        } catch (JSONException e) {
            Log.e(f4471b, "Parse sug search error", e);
        }
        suggestionResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
        return false;
    }

    /* renamed from: a */
    private boolean m5134a(JSONObject jSONObject, SuggestionResult suggestionResult) {
        if (!(jSONObject == null || jSONObject.length() == 0)) {
            suggestionResult.error = SearchResult.ERRORNO.NO_ERROR;
            JSONArray optJSONArray = jSONObject.optJSONArray("result");
            if (optJSONArray == null || optJSONArray.length() == 0) {
                suggestionResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
            } else {
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject jSONObject2 = (JSONObject) optJSONArray.opt(i);
                    if (!(jSONObject2 == null || jSONObject2.length() == 0)) {
                        SuggestionResult.SuggestionInfo suggestionInfo = new SuggestionResult.SuggestionInfo();
                        suggestionInfo.setKey(jSONObject2.optString(Config.FEED_LIST_NAME));
                        suggestionInfo.setCity(jSONObject2.optString("city"));
                        suggestionInfo.setDistrict(jSONObject2.optString("district"));
                        suggestionInfo.setUid(jSONObject2.optString(Config.CUSTOM_USER_ID));
                        suggestionInfo.setTag(jSONObject2.optString("tag"));
                        suggestionInfo.setAddress(jSONObject2.optString("address"));
                        suggestionInfo.setPt(m5131a(jSONObject2.optJSONObject("location")));
                        JSONArray optJSONArray2 = jSONObject2.optJSONArray("children");
                        if (!(optJSONArray2 == null || optJSONArray2.length() == 0)) {
                            suggestionInfo.setPoiChildrenInfoList(m5132a(optJSONArray2));
                        }
                        arrayList.add(suggestionInfo);
                    }
                }
                suggestionResult.setSuggestionInfo(arrayList);
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0073  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.baidu.mapapi.search.core.SearchResult mo14018a(java.lang.String r6) {
        /*
            r5 = this;
            com.baidu.mapapi.search.sug.SuggestionResult r0 = new com.baidu.mapapi.search.sug.SuggestionResult
            r0.<init>()
            if (r6 == 0) goto L_0x0019
            boolean r1 = r6.isEmpty()
            if (r1 == 0) goto L_0x000e
            goto L_0x0019
        L_0x000e:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0081 }
            r1.<init>(r6)     // Catch:{ JSONException -> 0x0081 }
            int r2 = r1.length()
            if (r2 != 0) goto L_0x001e
        L_0x0019:
            com.baidu.mapapi.search.core.SearchResult$ERRORNO r6 = com.baidu.mapapi.search.core.SearchResult.ERRORNO.RESULT_NOT_FOUND
        L_0x001b:
            r0.error = r6
            return r0
        L_0x001e:
            java.lang.String r2 = "SDK_InnerError"
            boolean r3 = r1.has(r2)
            r4 = 1
            if (r3 == 0) goto L_0x0077
            org.json.JSONObject r1 = r1.optJSONObject(r2)
            java.lang.String r2 = "PermissionCheckError"
            boolean r2 = r1.has(r2)
            if (r2 == 0) goto L_0x0036
            com.baidu.mapapi.search.core.SearchResult$ERRORNO r6 = com.baidu.mapapi.search.core.SearchResult.ERRORNO.PERMISSION_UNFINISHED
            goto L_0x001b
        L_0x0036:
            java.lang.String r2 = "httpStateError"
            boolean r3 = r1.has(r2)
            if (r3 == 0) goto L_0x0077
            java.lang.String r6 = r1.optString(r2)
            r1 = -1
            int r2 = r6.hashCode()
            r3 = -879828873(0xffffffffcb8ee077, float:-1.872715E7)
            if (r2 == r3) goto L_0x005c
            r3 = 1470557208(0x57a6ec18, float:3.6706589E14)
            if (r2 == r3) goto L_0x0052
            goto L_0x0066
        L_0x0052:
            java.lang.String r2 = "REQUEST_ERROR"
            boolean r6 = r6.equals(r2)
            if (r6 == 0) goto L_0x0066
            r6 = 1
            goto L_0x0067
        L_0x005c:
            java.lang.String r2 = "NETWORK_ERROR"
            boolean r6 = r6.equals(r2)
            if (r6 == 0) goto L_0x0066
            r6 = 0
            goto L_0x0067
        L_0x0066:
            r6 = -1
        L_0x0067:
            if (r6 == 0) goto L_0x0073
            if (r6 == r4) goto L_0x0070
            com.baidu.mapapi.search.core.SearchResult$ERRORNO r6 = com.baidu.mapapi.search.core.SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR
        L_0x006d:
            r0.error = r6
            goto L_0x0076
        L_0x0070:
            com.baidu.mapapi.search.core.SearchResult$ERRORNO r6 = com.baidu.mapapi.search.core.SearchResult.ERRORNO.REQUEST_ERROR
            goto L_0x006d
        L_0x0073:
            com.baidu.mapapi.search.core.SearchResult$ERRORNO r6 = com.baidu.mapapi.search.core.SearchResult.ERRORNO.NETWORK_ERROR
            goto L_0x006d
        L_0x0076:
            return r0
        L_0x0077:
            boolean r1 = r5.mo14022a(r6, r0, r4)
            if (r1 != 0) goto L_0x0080
            r5.m5133a(r6, r0)
        L_0x0080:
            return r0
        L_0x0081:
            r6 = move-exception
            java.lang.String r1 = com.baidu.platform.core.p036f.C1375c.f4471b
            java.lang.String r2 = "Parse suggestion search result error"
            android.util.Log.e(r1, r2, r6)
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.platform.core.p036f.C1375c.mo14018a(java.lang.String):com.baidu.mapapi.search.core.SearchResult");
    }

    /* renamed from: a */
    public void mo14020a(SearchResult searchResult, Object obj) {
        if (obj != null && (obj instanceof OnGetSuggestionResultListener)) {
            ((OnGetSuggestionResultListener) obj).onGetSuggestionResult((SuggestionResult) searchResult);
        }
    }
}
