package com.baidu.location.p014b;

import android.net.wifi.WifiConfiguration;
import android.os.Handler;
import com.baidu.android.bbalbs.common.p012a.C0692b;
import com.baidu.location.Jni;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0855k;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.baidu.location.b.h */
public class C0781h {

    /* renamed from: a */
    private static Object f1531a = new Object();

    /* renamed from: b */
    private static C0781h f1532b = null;

    /* renamed from: c */
    private Handler f1533c = null;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public String f1534d = null;

    /* renamed from: e */
    private int f1535e = 24;

    /* renamed from: f */
    private C0782a f1536f = null;

    /* renamed from: g */
    private long f1537g = 0;

    /* renamed from: com.baidu.location.b.h$a */
    private class C0782a extends C0849e {

        /* renamed from: b */
        private boolean f1539b = false;

        /* renamed from: c */
        private int f1540c = 0;

        /* renamed from: d */
        private JSONArray f1541d = null;

        /* renamed from: e */
        private JSONArray f1542e = null;

        C0782a() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1882h = C0855k.m2469g();
            this.f1885k.clear();
            this.f1885k.put("qt", "cltrw");
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("data", this.f1541d);
                jSONObject.put("frt", this.f1540c);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String encodeOfflineLocationUpdateRequest = Jni.encodeOfflineLocationUpdateRequest(jSONObject.toString());
            Map map = this.f1885k;
            map.put("cltr[0]", "" + encodeOfflineLocationUpdateRequest);
            this.f1885k.put("cfg", 1);
            this.f1885k.put("info", Jni.encode(C0844b.m2417a().mo10718c()));
            this.f1885k.put("trtm", String.format(Locale.CHINA, "%d", Long.valueOf(System.currentTimeMillis())));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
          ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
        /* renamed from: a */
        public void mo10446a(boolean z) {
            boolean z2;
            JSONObject jSONObject;
            if (z && this.f1884j != null) {
                try {
                    jSONObject = new JSONObject(this.f1884j);
                    z2 = true;
                } catch (Exception unused) {
                    jSONObject = null;
                    z2 = false;
                }
                if (z2 && jSONObject != null) {
                    try {
                        jSONObject.put("tt", System.currentTimeMillis());
                        jSONObject.put("data", this.f1542e);
                        try {
                            File file = new File(C0781h.this.f1534d, "wcnf.dat");
                            if (!file.exists()) {
                                file.createNewFile();
                            }
                            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, false));
                            bufferedWriter.write(C0692b.m1539a(jSONObject.toString().getBytes(), "UTF-8"));
                            bufferedWriter.flush();
                            bufferedWriter.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception unused2) {
                    }
                }
            }
            this.f1539b = false;
        }

        /* renamed from: a */
        public void mo10544a(boolean z, JSONArray jSONArray, JSONArray jSONArray2) {
            if (!this.f1539b) {
                this.f1539b = true;
                if (z) {
                    this.f1540c = 1;
                } else {
                    this.f1540c = 0;
                }
                this.f1541d = jSONArray;
                this.f1542e = jSONArray2;
                ExecutorService c = C0762v.m1943a().mo10505c();
                if (c != null) {
                    mo10731a(c, C0855k.m2469g());
                } else {
                    mo10734c(C0855k.m2469g());
                }
            }
        }
    }

    /* renamed from: com.baidu.location.b.h$b */
    private class C0783b {

        /* renamed from: a */
        public String f1543a = null;

        /* renamed from: b */
        public int f1544b = 0;

        C0783b(String str, int i) {
            this.f1543a = str;
            this.f1544b = i;
        }
    }

    /* renamed from: a */
    public static C0781h m2040a() {
        C0781h hVar;
        synchronized (f1531a) {
            if (f1532b == null) {
                f1532b = new C0781h();
            }
            hVar = f1532b;
        }
        return hVar;
    }

    /* renamed from: a */
    private Object m2041a(Object obj, String str) throws Exception {
        return obj.getClass().getField(str).get(obj);
    }

    /* renamed from: a */
    private List<C0783b> m2042a(List<WifiConfiguration> list) {
        int i;
        if (list == null || list.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (WifiConfiguration wifiConfiguration : list) {
            String str = wifiConfiguration.SSID;
            try {
                i = ((Integer) m2041a(wifiConfiguration, "numAssociation")).intValue();
            } catch (Throwable unused) {
                i = 0;
            }
            if (i > 0 && str != null) {
                arrayList.add(new C0783b(str, i));
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    private void m2044a(boolean z, JSONArray jSONArray, JSONArray jSONArray2) {
        if (this.f1536f == null) {
            this.f1536f = new C0782a();
        }
        if (!C0855k.m2460b()) {
            this.f1536f.mo10544a(z, jSONArray, jSONArray2);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x015d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:107:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:108:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00df A[Catch:{ Exception -> 0x01c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00f7 A[Catch:{ Exception -> 0x01c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0197 A[Catch:{ Exception -> 0x01c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01c1 A[ADDED_TO_REGION, Catch:{ Exception -> 0x01c7 }] */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m2046d() {
        /*
            r17 = this;
            r1 = r17
            java.lang.String r0 = "data"
            java.lang.String r2 = "frq"
            java.lang.String r3 = "tt"
            java.lang.String r4 = "cfg"
            java.lang.String r5 = "ison"
            java.lang.String r6 = r1.f1534d
            if (r6 == 0) goto L_0x01cb
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x01c7 }
            java.lang.String r8 = "wcnf.dat"
            r7.<init>(r6, r8)     // Catch:{ Exception -> 0x01c7 }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01c7 }
            boolean r6 = r7.exists()     // Catch:{ Exception -> 0x01c7 }
            r10 = 0
            java.lang.String r14 = "num"
            java.lang.String r12 = "ssid"
            if (r6 == 0) goto L_0x00da
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00d3 }
            java.io.FileReader r13 = new java.io.FileReader     // Catch:{ Exception -> 0x00d3 }
            r13.<init>(r7)     // Catch:{ Exception -> 0x00d3 }
            r6.<init>(r13)     // Catch:{ Exception -> 0x00d3 }
            java.lang.StringBuffer r7 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00d3 }
            r7.<init>()     // Catch:{ Exception -> 0x00d3 }
        L_0x0036:
            java.lang.String r13 = r6.readLine()     // Catch:{ Exception -> 0x00d3 }
            if (r13 == 0) goto L_0x0040
            r7.append(r13)     // Catch:{ Exception -> 0x00d3 }
            goto L_0x0036
        L_0x0040:
            r6.close()     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r6 = r7.toString()     // Catch:{ Exception -> 0x00d3 }
            if (r6 == 0) goto L_0x00db
            java.lang.String r7 = new java.lang.String     // Catch:{ Exception -> 0x00d3 }
            byte[] r6 = r6.getBytes()     // Catch:{ Exception -> 0x00d3 }
            byte[] r6 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r6)     // Catch:{ Exception -> 0x00d3 }
            r7.<init>(r6)     // Catch:{ Exception -> 0x00d3 }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x00d3 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x00d3 }
            boolean r7 = r6.has(r5)     // Catch:{ Exception -> 0x00d3 }
            if (r7 == 0) goto L_0x0069
            int r5 = r6.getInt(r5)     // Catch:{ Exception -> 0x00d3 }
            if (r5 != 0) goto L_0x0069
            r5 = 0
            goto L_0x006a
        L_0x0069:
            r5 = 1
        L_0x006a:
            boolean r7 = r6.has(r4)     // Catch:{ Exception -> 0x00cf }
            if (r7 == 0) goto L_0x0080
            org.json.JSONObject r4 = r6.getJSONObject(r4)     // Catch:{ Exception -> 0x00cf }
            boolean r7 = r4.has(r2)     // Catch:{ Exception -> 0x00cf }
            if (r7 == 0) goto L_0x0080
            int r2 = r4.getInt(r2)     // Catch:{ Exception -> 0x00cf }
            r1.f1535e = r2     // Catch:{ Exception -> 0x00cf }
        L_0x0080:
            boolean r2 = r6.has(r3)     // Catch:{ Exception -> 0x00cf }
            if (r2 == 0) goto L_0x008a
            long r8 = r6.getLong(r3)     // Catch:{ Exception -> 0x00cf }
        L_0x008a:
            boolean r2 = r6.has(r0)     // Catch:{ Exception -> 0x00cf }
            if (r2 == 0) goto L_0x00cc
            org.json.JSONArray r0 = r6.getJSONArray(r0)     // Catch:{ Exception -> 0x00cf }
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ Exception -> 0x00cf }
            r2.<init>()     // Catch:{ Exception -> 0x00cf }
            int r3 = r0.length()     // Catch:{ Exception -> 0x00c9 }
            r4 = 0
        L_0x009e:
            if (r4 >= r3) goto L_0x00c7
            org.json.JSONObject r6 = r0.getJSONObject(r4)     // Catch:{ Exception -> 0x00c9 }
            boolean r7 = r6.has(r12)     // Catch:{ Exception -> 0x00c9 }
            if (r7 == 0) goto L_0x00c4
            boolean r7 = r6.has(r14)     // Catch:{ Exception -> 0x00c9 }
            if (r7 == 0) goto L_0x00c4
            com.baidu.location.b.h$b r7 = new com.baidu.location.b.h$b     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r13 = r6.getString(r12)     // Catch:{ Exception -> 0x00c9 }
            int r15 = r6.getInt(r14)     // Catch:{ Exception -> 0x00c9 }
            r7.<init>(r13, r15)     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r6 = r6.getString(r12)     // Catch:{ Exception -> 0x00c9 }
            r2.put(r6, r7)     // Catch:{ Exception -> 0x00c9 }
        L_0x00c4:
            int r4 = r4 + 1
            goto L_0x009e
        L_0x00c7:
            r15 = r5
            goto L_0x00dd
        L_0x00c9:
            r0 = move-exception
            r15 = r5
            goto L_0x00d6
        L_0x00cc:
            r15 = r5
            r2 = 0
            goto L_0x00dd
        L_0x00cf:
            r0 = move-exception
            r15 = r5
            r2 = 0
            goto L_0x00d6
        L_0x00d3:
            r0 = move-exception
            r2 = 0
            r15 = 1
        L_0x00d6:
            r0.printStackTrace()     // Catch:{ Exception -> 0x01c7 }
            goto L_0x00dd
        L_0x00da:
            r8 = r10
        L_0x00db:
            r2 = 0
            r15 = 1
        L_0x00dd:
            if (r15 != 0) goto L_0x00e5
            int r0 = r1.f1535e     // Catch:{ Exception -> 0x01c7 }
            int r0 = r0 * 4
            r1.f1535e = r0     // Catch:{ Exception -> 0x01c7 }
        L_0x00e5:
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01c7 }
            long r3 = r3 - r8
            int r0 = r1.f1535e     // Catch:{ Exception -> 0x01c7 }
            int r0 = r0 * 60
            int r0 = r0 * 60
            int r0 = r0 * 1000
            long r5 = (long) r0     // Catch:{ Exception -> 0x01c7 }
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x01cb
            com.baidu.location.e.i r0 = com.baidu.location.p017e.C0835i.m2376a()     // Catch:{ Exception -> 0x01c7 }
            java.util.List r0 = r0.mo10688d()     // Catch:{ Exception -> 0x01c7 }
            java.util.List r0 = r1.m2042a(r0)     // Catch:{ Exception -> 0x01c7 }
            int r3 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r3 != 0) goto L_0x0142
            if (r0 == 0) goto L_0x01bc
            int r2 = r0.size()     // Catch:{ Exception -> 0x01c7 }
            if (r2 <= 0) goto L_0x01bc
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x01c7 }
            r2.<init>()     // Catch:{ Exception -> 0x01c7 }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Exception -> 0x01c7 }
            r3.<init>()     // Catch:{ Exception -> 0x01c7 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x01c7 }
        L_0x011d:
            boolean r4 = r0.hasNext()     // Catch:{ Exception -> 0x01c7 }
            if (r4 == 0) goto L_0x013f
            java.lang.Object r4 = r0.next()     // Catch:{ Exception -> 0x01c7 }
            com.baidu.location.b.h$b r4 = (com.baidu.location.p014b.C0781h.C0783b) r4     // Catch:{ Exception -> 0x01c7 }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x01c7 }
            r5.<init>()     // Catch:{ Exception -> 0x01c7 }
            java.lang.String r6 = r4.f1543a     // Catch:{ Exception -> 0x01c7 }
            r5.put(r12, r6)     // Catch:{ Exception -> 0x01c7 }
            int r4 = r4.f1544b     // Catch:{ Exception -> 0x01c7 }
            r5.put(r14, r4)     // Catch:{ Exception -> 0x01c7 }
            r2.put(r5)     // Catch:{ Exception -> 0x01c7 }
            r3.put(r5)     // Catch:{ Exception -> 0x01c7 }
            goto L_0x011d
        L_0x013f:
            r0 = 1
            goto L_0x01bf
        L_0x0142:
            if (r0 == 0) goto L_0x01bc
            int r3 = r0.size()     // Catch:{ Exception -> 0x01c7 }
            if (r3 <= 0) goto L_0x01bc
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Exception -> 0x01c7 }
            r3.<init>()     // Catch:{ Exception -> 0x01c7 }
            if (r2 == 0) goto L_0x01b9
            int r4 = r2.size()     // Catch:{ Exception -> 0x01c7 }
            if (r4 <= 0) goto L_0x01b9
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x01c7 }
            r16 = 0
        L_0x015d:
            boolean r4 = r0.hasNext()     // Catch:{ Exception -> 0x01c7 }
            if (r4 == 0) goto L_0x01b5
            java.lang.Object r4 = r0.next()     // Catch:{ Exception -> 0x01c7 }
            com.baidu.location.b.h$b r4 = (com.baidu.location.p014b.C0781h.C0783b) r4     // Catch:{ Exception -> 0x01c7 }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x01c7 }
            r5.<init>()     // Catch:{ Exception -> 0x01c7 }
            java.lang.String r6 = r4.f1543a     // Catch:{ Exception -> 0x01c7 }
            r5.put(r12, r6)     // Catch:{ Exception -> 0x01c7 }
            int r6 = r4.f1544b     // Catch:{ Exception -> 0x01c7 }
            r5.put(r14, r6)     // Catch:{ Exception -> 0x01c7 }
            r3.put(r5)     // Catch:{ Exception -> 0x01c7 }
            java.lang.String r5 = r4.f1543a     // Catch:{ Exception -> 0x01c7 }
            boolean r5 = r2.containsKey(r5)     // Catch:{ Exception -> 0x01c7 }
            if (r5 == 0) goto L_0x0194
            int r5 = r4.f1544b     // Catch:{ Exception -> 0x01c7 }
            java.lang.String r6 = r4.f1543a     // Catch:{ Exception -> 0x01c7 }
            java.lang.Object r6 = r2.get(r6)     // Catch:{ Exception -> 0x01c7 }
            com.baidu.location.b.h$b r6 = (com.baidu.location.p014b.C0781h.C0783b) r6     // Catch:{ Exception -> 0x01c7 }
            int r6 = r6.f1544b     // Catch:{ Exception -> 0x01c7 }
            if (r5 == r6) goto L_0x0192
            goto L_0x0194
        L_0x0192:
            r5 = 0
            goto L_0x0195
        L_0x0194:
            r5 = 1
        L_0x0195:
            if (r5 == 0) goto L_0x015d
            if (r16 != 0) goto L_0x019e
            org.json.JSONArray r16 = new org.json.JSONArray     // Catch:{ Exception -> 0x01c7 }
            r16.<init>()     // Catch:{ Exception -> 0x01c7 }
        L_0x019e:
            r5 = r16
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x01c7 }
            r6.<init>()     // Catch:{ Exception -> 0x01c7 }
            java.lang.String r7 = r4.f1543a     // Catch:{ Exception -> 0x01c7 }
            r6.put(r12, r7)     // Catch:{ Exception -> 0x01c7 }
            int r4 = r4.f1544b     // Catch:{ Exception -> 0x01c7 }
            r6.put(r14, r4)     // Catch:{ Exception -> 0x01c7 }
            r5.put(r6)     // Catch:{ Exception -> 0x01c7 }
            r16 = r5
            goto L_0x015d
        L_0x01b5:
            r2 = r16
            r0 = 0
            goto L_0x01bf
        L_0x01b9:
            r0 = 0
            r2 = 0
            goto L_0x01bf
        L_0x01bc:
            r0 = 0
            r2 = 0
            r3 = 0
        L_0x01bf:
            if (r2 == 0) goto L_0x01cb
            if (r3 == 0) goto L_0x01cb
            r1.m2044a(r0, r2, r3)     // Catch:{ Exception -> 0x01c7 }
            goto L_0x01cb
        L_0x01c7:
            r0 = move-exception
            r0.printStackTrace()
        L_0x01cb:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p014b.C0781h.m2046d():void");
    }

    /* renamed from: b */
    public void mo10542b() {
        if (this.f1533c == null) {
            this.f1533c = new C0784i(this);
        }
        this.f1534d = C0855k.m2471i();
    }

    /* renamed from: c */
    public void mo10543c() {
        Handler handler;
        if (System.currentTimeMillis() - this.f1537g > 3600000 && (handler = this.f1533c) != null) {
            handler.sendEmptyMessage(1);
            this.f1537g = System.currentTimeMillis();
        }
    }
}
