package com.baidu.mapapi.map;

import android.os.Bundle;

public final class Stroke {
    public final int color;
    public final int strokeWidth;

    public Stroke(int i, int i2) {
        this.strokeWidth = i <= 0 ? 5 : i;
        this.color = i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo11396a(Bundle bundle) {
        bundle.putInt("width", this.strokeWidth);
        Overlay.m2939a(this.color, bundle);
        return bundle;
    }
}
