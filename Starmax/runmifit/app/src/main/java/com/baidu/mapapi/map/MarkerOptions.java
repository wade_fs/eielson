package com.baidu.mapapi.map;

import android.graphics.Point;
import android.os.Bundle;
import com.baidu.mapapi.model.LatLng;
import java.util.ArrayList;

public final class MarkerOptions extends OverlayOptions {

    /* renamed from: a */
    int f2632a;

    /* renamed from: b */
    boolean f2633b = true;

    /* renamed from: c */
    Bundle f2634c;

    /* renamed from: d */
    private LatLng f2635d;

    /* renamed from: e */
    private BitmapDescriptor f2636e;

    /* renamed from: f */
    private float f2637f = 0.5f;

    /* renamed from: g */
    private float f2638g = 1.0f;

    /* renamed from: h */
    private boolean f2639h = true;

    /* renamed from: i */
    private boolean f2640i = false;

    /* renamed from: j */
    private float f2641j;

    /* renamed from: k */
    private String f2642k;

    /* renamed from: l */
    private int f2643l;

    /* renamed from: m */
    private boolean f2644m = false;

    /* renamed from: n */
    private ArrayList<BitmapDescriptor> f2645n;

    /* renamed from: o */
    private int f2646o = 20;

    /* renamed from: p */
    private float f2647p = 1.0f;

    /* renamed from: q */
    private float f2648q = 1.0f;

    /* renamed from: r */
    private float f2649r = 1.0f;

    /* renamed from: s */
    private int f2650s = MarkerAnimateType.none.ordinal();

    /* renamed from: t */
    private boolean f2651t = false;

    /* renamed from: u */
    private Point f2652u;

    /* renamed from: v */
    private InfoWindow f2653v;

    public enum MarkerAnimateType {
        none,
        drop,
        grow,
        jump
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Overlay mo10909a() {
        Marker marker = new Marker();
        marker.f2662A = this.f2633b;
        marker.f2665z = this.f2632a;
        marker.f2663B = this.f2634c;
        LatLng latLng = this.f2635d;
        if (latLng != null) {
            marker.f2608a = latLng;
            if (this.f2636e == null && this.f2645n == null) {
                throw new IllegalStateException("BDMapSDKException: when you add marker, you must set the icon or icons");
            }
            marker.f2609b = this.f2636e;
            marker.f2610c = this.f2637f;
            marker.f2611d = this.f2638g;
            marker.f2612e = this.f2639h;
            marker.f2613f = this.f2640i;
            marker.f2614g = this.f2641j;
            marker.f2615h = this.f2642k;
            marker.f2616i = this.f2643l;
            marker.f2617j = this.f2644m;
            marker.f2622o = this.f2645n;
            marker.f2623p = this.f2646o;
            marker.f2619l = this.f2649r;
            marker.f2625r = this.f2647p;
            marker.f2626s = this.f2648q;
            marker.f2620m = this.f2650s;
            marker.f2621n = this.f2651t;
            marker.f2629v = this.f2653v;
            Point point = this.f2652u;
            if (point != null) {
                marker.f2628u = point;
            }
            return marker;
        }
        throw new IllegalStateException("BDMapSDKException: when you add marker, you must set the position");
    }

    public MarkerOptions alpha(float f) {
        if (f < 0.0f || f > 1.0f) {
            this.f2649r = 1.0f;
            return this;
        }
        this.f2649r = f;
        return this;
    }

    public MarkerOptions anchor(float f, float f2) {
        if (f >= 0.0f && f <= 1.0f && f2 >= 0.0f && f2 <= 1.0f) {
            this.f2637f = f;
            this.f2638g = f2;
        }
        return this;
    }

    public MarkerOptions animateType(MarkerAnimateType markerAnimateType) {
        if (markerAnimateType == null) {
            markerAnimateType = MarkerAnimateType.none;
        }
        this.f2650s = markerAnimateType.ordinal();
        return this;
    }

    public MarkerOptions draggable(boolean z) {
        this.f2640i = z;
        return this;
    }

    public MarkerOptions extraInfo(Bundle bundle) {
        this.f2634c = bundle;
        return this;
    }

    public MarkerOptions fixedScreenPosition(Point point) {
        this.f2652u = point;
        this.f2651t = true;
        return this;
    }

    public MarkerOptions flat(boolean z) {
        this.f2644m = z;
        return this;
    }

    public float getAlpha() {
        return this.f2649r;
    }

    public float getAnchorX() {
        return this.f2637f;
    }

    public float getAnchorY() {
        return this.f2638g;
    }

    public MarkerAnimateType getAnimateType() {
        int i = this.f2650s;
        return i != 1 ? i != 2 ? i != 3 ? MarkerAnimateType.none : MarkerAnimateType.jump : MarkerAnimateType.grow : MarkerAnimateType.drop;
    }

    public Bundle getExtraInfo() {
        return this.f2634c;
    }

    public BitmapDescriptor getIcon() {
        return this.f2636e;
    }

    public ArrayList<BitmapDescriptor> getIcons() {
        return this.f2645n;
    }

    public int getPeriod() {
        return this.f2646o;
    }

    public LatLng getPosition() {
        return this.f2635d;
    }

    public float getRotate() {
        return this.f2641j;
    }

    @Deprecated
    public String getTitle() {
        return this.f2642k;
    }

    public int getZIndex() {
        return this.f2632a;
    }

    public MarkerOptions icon(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f2636e = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: marker's icon can not be null");
    }

    public MarkerOptions icons(ArrayList<BitmapDescriptor> arrayList) {
        if (arrayList == null) {
            throw new IllegalArgumentException("BDMapSDKException: marker's icons can not be null");
        } else if (arrayList.size() == 0) {
            return this;
        } else {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i) == null || arrayList.get(i).f2437a == null) {
                    return this;
                }
            }
            this.f2645n = arrayList;
            return this;
        }
    }

    public MarkerOptions infoWindow(InfoWindow infoWindow) {
        this.f2653v = infoWindow;
        return this;
    }

    public boolean isDraggable() {
        return this.f2640i;
    }

    public boolean isFlat() {
        return this.f2644m;
    }

    public boolean isPerspective() {
        return this.f2639h;
    }

    public boolean isVisible() {
        return this.f2633b;
    }

    public MarkerOptions period(int i) {
        if (i > 0) {
            this.f2646o = i;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: marker's period must be greater than zero ");
    }

    public MarkerOptions perspective(boolean z) {
        this.f2639h = z;
        return this;
    }

    public MarkerOptions position(LatLng latLng) {
        if (latLng != null) {
            this.f2635d = latLng;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: marker's position can not be null");
    }

    public MarkerOptions rotate(float f) {
        while (f < 0.0f) {
            f += 360.0f;
        }
        this.f2641j = f % 360.0f;
        return this;
    }

    public MarkerOptions scaleX(float f) {
        if (f < 0.0f) {
            return this;
        }
        this.f2647p = f;
        return this;
    }

    public MarkerOptions scaleY(float f) {
        if (f < 0.0f) {
            return this;
        }
        this.f2648q = f;
        return this;
    }

    @Deprecated
    public MarkerOptions title(String str) {
        this.f2642k = str;
        return this;
    }

    public MarkerOptions visible(boolean z) {
        this.f2633b = z;
        return this;
    }

    public MarkerOptions yOffset(int i) {
        this.f2643l = i;
        return this;
    }

    public MarkerOptions zIndex(int i) {
        this.f2632a = i;
        return this;
    }
}
