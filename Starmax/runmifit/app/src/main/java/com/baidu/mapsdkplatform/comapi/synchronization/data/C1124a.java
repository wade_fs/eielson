package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.a */
final class C1124a implements Parcelable.Creator<DriverPosition> {
    C1124a() {
    }

    /* renamed from: a */
    public DriverPosition createFromParcel(Parcel parcel) {
        return new DriverPosition(parcel);
    }

    /* renamed from: a */
    public DriverPosition[] newArray(int i) {
        return new DriverPosition[i];
    }
}
