package com.baidu.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;
import com.baidu.mapsdkplatform.comapi.map.C1087l;
import com.baidu.mobstat.Config;
import java.util.Iterator;
import java.util.Set;
import javax.microedition.khronos.opengles.GL10;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapapi.map.c */
class C0926c implements C1087l {

    /* renamed from: a */
    final /* synthetic */ BaiduMap f2837a;

    C0926c(BaiduMap baiduMap) {
        this.f2837a = baiduMap;
    }

    /* renamed from: a */
    public void mo11538a() {
    }

    /* renamed from: a */
    public void mo11539a(Bitmap bitmap) {
        if (this.f2837a.f2381B != null) {
            this.f2837a.f2381B.onSnapshotReady(bitmap);
        }
    }

    /* renamed from: a */
    public void mo11540a(MotionEvent motionEvent) {
        if (this.f2837a.f2416r != null) {
            this.f2837a.f2416r.onTouch(motionEvent);
        }
    }

    /* renamed from: a */
    public void mo11541a(GeoPoint geoPoint) {
        if (this.f2837a.f2417s != null) {
            this.f2837a.f2417s.onMapClick(CoordUtil.mc2ll(geoPoint));
        }
    }

    /* renamed from: a */
    public void mo11542a(C1064ab abVar) {
        if (!this.f2837a.f2390K.values().isEmpty()) {
            for (InfoWindow infoWindow : this.f2837a.f2390K.values()) {
                if (infoWindow.f2513b != null && !infoWindow.f2520i) {
                    infoWindow.f2513b.setVisibility(View.INVISIBLE);
                }
            }
        }
        int i = (BaiduMap.mapStatusReason & 256) == 256 ? 3 : (BaiduMap.mapStatusReason & 16) == 16 ? 2 : 1;
        if (this.f2837a.f2415q != null) {
            MapStatus a = MapStatus.m2907a(abVar);
            this.f2837a.f2415q.onMapStatusChangeStart(a);
            this.f2837a.f2415q.onMapStatusChangeStart(a, i);
        }
        if (this.f2837a.f2385F != null) {
            this.f2837a.f2385F.onMapStatusChangeReason(i);
        }
        BaiduMap.mapStatusReason = 0;
    }

    /* renamed from: a */
    public void mo11543a(String str) {
        C1064ab E;
        try {
            JSONObject jSONObject = new JSONObject(str);
            JSONObject optJSONObject = jSONObject.optJSONArray("dataset").optJSONObject(0);
            GeoPoint b = this.f2837a.f2407i.mo13030b(jSONObject.optInt("px"), jSONObject.optInt("py"));
            int optInt = optJSONObject.optInt(Config.EXCEPTION_CRASH_CHANNEL);
            if (optInt != 17) {
                if (optInt == 18) {
                    if (this.f2837a.f2380A != null) {
                        this.f2837a.f2380A.onMyLocationClick();
                        return;
                    }
                } else if (optInt == 19) {
                    if (this.f2837a.f2407i != null && (E = this.f2837a.f2407i.mo12989E()) != null) {
                        E.f3416c = 0;
                        E.f3415b = 0;
                        BaiduMap.mapStatusReason |= 16;
                        this.f2837a.f2407i.mo13016a(E, 300);
                        return;
                    }
                    return;
                } else if (optInt == 90909) {
                    String optString = optJSONObject.optString("marker_id");
                    Set<String> keySet = this.f2837a.f2390K.keySet();
                    if (keySet.isEmpty() || !keySet.contains(optString)) {
                        for (Overlay overlay : this.f2837a.f2409k) {
                            if ((overlay instanceof Marker) && overlay.f2664y.equals(optString)) {
                                if (!this.f2837a.f2422x.isEmpty()) {
                                    Iterator it = this.f2837a.f2422x.iterator();
                                    while (it.hasNext()) {
                                        ((BaiduMap.OnMarkerClickListener) it.next()).onMarkerClick((Marker) overlay);
                                    }
                                    return;
                                }
                                mo11541a(b);
                            }
                        }
                        return;
                    }
                    for (String str2 : keySet) {
                        if (str2 != null && str2.equals(optString)) {
                            InfoWindow infoWindow = (InfoWindow) this.f2837a.f2390K.get(str2);
                            if (!(infoWindow == null || infoWindow.f2515d == null)) {
                                infoWindow.f2515d.onInfoWindowClick();
                                return;
                            }
                        }
                    }
                    return;
                } else if (optInt == 90910) {
                    String optString2 = optJSONObject.optString("polyline_id");
                    for (Overlay overlay2 : this.f2837a.f2409k) {
                        if ((overlay2 instanceof Polyline) && overlay2.f2664y.equals(optString2)) {
                            if (!this.f2837a.f2423y.isEmpty()) {
                                Iterator it2 = this.f2837a.f2423y.iterator();
                                while (it2.hasNext()) {
                                    ((BaiduMap.OnPolylineClickListener) it2.next()).onPolylineClick((Polyline) overlay2);
                                }
                            } else {
                                mo11541a(b);
                            }
                        }
                    }
                    return;
                } else {
                    return;
                }
                mo11541a(b);
            } else if (this.f2837a.f2417s != null) {
                MapPoi mapPoi = new MapPoi();
                mapPoi.mo11174a(optJSONObject);
                this.f2837a.f2417s.onMapPoiClick(mapPoi);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public void mo11544a(GL10 gl10, C1064ab abVar) {
        if (this.f2837a.f2382C != null) {
            this.f2837a.f2382C.onMapDrawFrame(MapStatus.m2907a(abVar));
        }
    }

    /* renamed from: a */
    public void mo11545a(boolean z) {
        if (this.f2837a.f2383D != null) {
            this.f2837a.f2383D.onBaseIndoorMapMode(z, this.f2837a.getFocusedBaseIndoorMapInfo());
        }
    }

    /* renamed from: a */
    public void mo11546a(boolean z, int i) {
        if (this.f2837a.f2384E != null) {
            this.f2837a.f2384E.onMapRenderValidData(z, i, this.f2837a.m2824a(i));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.BaiduMap.a(com.baidu.mapapi.map.BaiduMap, boolean):boolean
     arg types: [com.baidu.mapapi.map.BaiduMap, int]
     candidates:
      com.baidu.mapapi.map.BaiduMap.a(com.baidu.mapapi.map.BaiduMap, com.baidu.mapapi.map.Marker):com.baidu.mapapi.map.Marker
      com.baidu.mapapi.map.BaiduMap.a(com.baidu.mapapi.map.BaiduMap, com.baidu.mapapi.map.Projection):com.baidu.mapapi.map.Projection
      com.baidu.mapapi.map.BaiduMap.a(com.baidu.mapapi.map.BaiduMap, int):java.lang.String
      com.baidu.mapapi.map.BaiduMap.a(com.baidu.mapapi.map.BaiduMap, com.baidu.mapapi.map.InfoWindow):void
      com.baidu.mapapi.map.BaiduMap.a(com.baidu.mapapi.map.MyLocationData, com.baidu.mapapi.map.MyLocationConfiguration):void
      com.baidu.mapapi.map.BaiduMap.a(com.baidu.mapapi.map.BaiduMap, boolean):boolean */
    /* renamed from: b */
    public void mo11547b() {
        BaiduMap baiduMap = this.f2837a;
        Projection unused = baiduMap.f2404f = new Projection(baiduMap.f2407i);
        boolean unused2 = this.f2837a.f2398S = true;
        if (this.f2837a.f2418t != null) {
            this.f2837a.f2418t.onMapLoaded();
        }
    }

    /* renamed from: b */
    public void mo11548b(GeoPoint geoPoint) {
        if (this.f2837a.f2420v != null) {
            this.f2837a.f2420v.onMapDoubleClick(CoordUtil.mc2ll(geoPoint));
        }
    }

    /* renamed from: b */
    public void mo11549b(C1064ab abVar) {
        if (this.f2837a.f2415q != null) {
            this.f2837a.f2415q.onMapStatusChange(MapStatus.m2907a(abVar));
        }
    }

    /* renamed from: b */
    public boolean mo11550b(String str) {
        try {
            JSONObject optJSONObject = new JSONObject(str).optJSONArray("dataset").optJSONObject(0);
            if (optJSONObject.optInt(Config.EXCEPTION_CRASH_CHANNEL) != 90909) {
                return false;
            }
            String optString = optJSONObject.optString("marker_id");
            Set keySet = this.f2837a.f2390K.keySet();
            if (!keySet.isEmpty() && keySet.contains(optString)) {
                return false;
            }
            for (Overlay overlay : this.f2837a.f2409k) {
                if ((overlay instanceof Marker) && overlay.f2664y.equals(optString)) {
                    Marker marker = (Marker) overlay;
                    if (!marker.f2613f) {
                        return false;
                    }
                    Marker unused = this.f2837a.f2392M = marker;
                    Point screenLocation = this.f2837a.f2404f.toScreenLocation(this.f2837a.f2392M.f2608a);
                    this.f2837a.f2392M.setPosition(this.f2837a.f2404f.fromScreenLocation(new Point(screenLocation.x, screenLocation.y - 60)));
                    if (this.f2837a.f2424z != null) {
                        this.f2837a.f2424z.onMarkerDragStart(this.f2837a.f2392M);
                    }
                    return true;
                }
            }
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: c */
    public void mo11551c() {
        if (this.f2837a.f2419u != null) {
            this.f2837a.f2419u.onMapRenderFinished();
        }
    }

    /* renamed from: c */
    public void mo11552c(GeoPoint geoPoint) {
        if (this.f2837a.f2421w != null) {
            this.f2837a.f2421w.onMapLongClick(CoordUtil.mc2ll(geoPoint));
        }
    }

    /* renamed from: c */
    public void mo11553c(C1064ab abVar) {
        if (!this.f2837a.f2390K.values().isEmpty()) {
            for (InfoWindow infoWindow : this.f2837a.f2390K.values()) {
                View view = infoWindow.f2513b;
                if (view != null) {
                    view.setVisibility(View.VISIBLE);
                }
            }
        }
        if (this.f2837a.f2415q != null) {
            this.f2837a.f2415q.onMapStatusChangeFinish(MapStatus.m2907a(abVar));
        }
    }

    /* renamed from: d */
    public void mo11554d() {
        this.f2837a.f2388I.lock();
        try {
            if (this.f2837a.f2387H != null) {
                this.f2837a.f2387H.mo11129a();
            }
        } finally {
            this.f2837a.f2388I.unlock();
        }
    }

    /* renamed from: d */
    public void mo11555d(GeoPoint geoPoint) {
        if (this.f2837a.f2392M != null && this.f2837a.f2392M.f2613f) {
            Point screenLocation = this.f2837a.f2404f.toScreenLocation(CoordUtil.mc2ll(geoPoint));
            this.f2837a.f2392M.setPosition(this.f2837a.f2404f.fromScreenLocation(new Point(screenLocation.x, screenLocation.y - 60)));
            if (this.f2837a.f2424z != null && this.f2837a.f2392M.f2613f) {
                this.f2837a.f2424z.onMarkerDrag(this.f2837a.f2392M);
            }
        }
    }

    /* renamed from: e */
    public void mo11556e() {
        this.f2837a.f2388I.lock();
        try {
            if (this.f2837a.f2387H != null) {
                this.f2837a.f2387H.mo11129a();
                this.f2837a.f2407i.mo13065o();
            }
        } finally {
            this.f2837a.f2388I.unlock();
        }
    }

    /* renamed from: e */
    public void mo11557e(GeoPoint geoPoint) {
        if (this.f2837a.f2392M != null && this.f2837a.f2392M.f2613f) {
            Point screenLocation = this.f2837a.f2404f.toScreenLocation(CoordUtil.mc2ll(geoPoint));
            this.f2837a.f2392M.setPosition(this.f2837a.f2404f.fromScreenLocation(new Point(screenLocation.x, screenLocation.y - 60)));
            if (this.f2837a.f2424z != null && this.f2837a.f2392M.f2613f) {
                this.f2837a.f2424z.onMarkerDragEnd(this.f2837a.f2392M);
            }
            Marker unused = this.f2837a.f2392M = (Marker) null;
        }
    }

    /* renamed from: f */
    public void mo11558f() {
        this.f2837a.f2407i.mo13035b(false);
        this.f2837a.f2388I.lock();
        try {
            if (this.f2837a.f2387H != null) {
                this.f2837a.mo10925a(this.f2837a.f2387H);
            }
        } finally {
            this.f2837a.f2388I.unlock();
        }
    }
}
