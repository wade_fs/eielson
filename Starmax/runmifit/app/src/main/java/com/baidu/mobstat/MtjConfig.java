package com.baidu.mobstat;

public class MtjConfig {

    public enum FeedTrackStrategy {
        TRACK_ALL,
        TRACK_SINGLE,
        TRACK_NONE
    }

    public enum PushPlatform {
        BAIDUYUN("baiduyun", 0),
        JIGUANG("jiguang", 1),
        GETUI("getui", 2),
        HUAWEI("huawei", 3),
        XIAOMI("xiaomi", 4),
        UMENG("umeng", 5),
        XINGE("xinge", 6),
        ALIYUN("aliyun", 7),
        OPPO("oppo", 8),
        MEIZU("meizu", 9);
        

        /* renamed from: a */
        private String f4200a;

        /* renamed from: b */
        private int f4201b;

        private PushPlatform(String str, int i) {
            this.f4200a = str;
            this.f4201b = i;
        }

        public String value() {
            return "p" + this.f4201b;
        }

        public String showName() {
            return this.f4200a;
        }
    }
}
