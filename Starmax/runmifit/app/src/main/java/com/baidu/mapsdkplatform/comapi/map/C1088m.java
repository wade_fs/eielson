package com.baidu.mapsdkplatform.comapi.map;

import android.graphics.SurfaceTexture;
import android.opengl.GLUtils;
import java.lang.Thread;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL10;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.m */
public class C1088m extends Thread {

    /* renamed from: a */
    private AtomicBoolean f3583a;

    /* renamed from: b */
    private SurfaceTexture f3584b;

    /* renamed from: c */
    private C1089a f3585c;

    /* renamed from: d */
    private EGL10 f3586d;

    /* renamed from: e */
    private EGLDisplay f3587e = EGL10.EGL_NO_DISPLAY;

    /* renamed from: f */
    private EGLContext f3588f = EGL10.EGL_NO_CONTEXT;

    /* renamed from: g */
    private EGLSurface f3589g = EGL10.EGL_NO_SURFACE;

    /* renamed from: h */
    private GL10 f3590h;

    /* renamed from: i */
    private int f3591i = 1;

    /* renamed from: j */
    private boolean f3592j = false;

    /* renamed from: k */
    private final C1066ac f3593k;

    /* renamed from: com.baidu.mapsdkplatform.comapi.map.m$a */
    public interface C1089a {
        /* renamed from: a */
        int mo12951a();
    }

    public C1088m(SurfaceTexture surfaceTexture, C1089a aVar, AtomicBoolean atomicBoolean, C1066ac acVar) {
        this.f3584b = surfaceTexture;
        this.f3585c = aVar;
        this.f3583a = atomicBoolean;
        this.f3593k = acVar;
    }

    /* renamed from: a */
    private boolean m3694a(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f3586d = (EGL10) EGLContext.getEGL();
        this.f3587e = this.f3586d.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        if (this.f3587e != EGL10.EGL_NO_DISPLAY) {
            if (this.f3586d.eglInitialize(this.f3587e, new int[2])) {
                EGLConfig[] eGLConfigArr = new EGLConfig[100];
                int[] iArr = new int[1];
                if (!this.f3586d.eglChooseConfig(this.f3587e, new int[]{12352, 4, 12324, i, 12323, i2, 12322, i3, 12321, i4, 12325, i5, 12326, i6, 12344}, eGLConfigArr, 100, iArr) || iArr[0] <= 0) {
                    return false;
                }
                this.f3588f = this.f3586d.eglCreateContext(this.f3587e, eGLConfigArr[0], EGL10.EGL_NO_CONTEXT, new int[]{12440, 2, 12344});
                this.f3589g = this.f3586d.eglCreateWindowSurface(this.f3587e, eGLConfigArr[0], this.f3584b, null);
                if (this.f3589g == EGL10.EGL_NO_SURFACE || this.f3588f == EGL10.EGL_NO_CONTEXT) {
                    if (this.f3586d.eglGetError() != 12299) {
                        GLUtils.getEGLErrorString(this.f3586d.eglGetError());
                    } else {
                        throw new RuntimeException("eglCreateWindowSurface returned EGL_BAD_NATIVE_WINDOW. ");
                    }
                }
                EGL10 egl10 = this.f3586d;
                EGLDisplay eGLDisplay = this.f3587e;
                EGLSurface eGLSurface = this.f3589g;
                if (egl10.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, this.f3588f)) {
                    this.f3590h = (GL10) this.f3588f.getGL();
                    return true;
                }
                String eGLErrorString = GLUtils.getEGLErrorString(this.f3586d.eglGetError());
                throw new RuntimeException("eglMakeCurrent failed : " + eGLErrorString);
            }
            throw new RuntimeException("eglInitialize failed : " + GLUtils.getEGLErrorString(this.f3586d.eglGetError()));
        }
        throw new RuntimeException("eglGetdisplay failed : " + GLUtils.getEGLErrorString(this.f3586d.eglGetError()));
    }

    /* renamed from: b */
    private static boolean m3695b(int i, int i2, int i3, int i4, int i5, int i6) {
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        egl10.eglInitialize(eglGetDisplay, new int[2]);
        int[] iArr = new int[1];
        return egl10.eglChooseConfig(eglGetDisplay, new int[]{12324, i, 12323, i2, 12322, i3, 12321, i4, 12325, i5, 12326, i6, 12344}, new EGLConfig[100], 100, iArr) && iArr[0] > 0;
    }

    /* renamed from: d */
    private void m3696d() {
        try {
            if (m3695b(5, 6, 5, 0, 24, 0)) {
                m3694a(8, 8, 8, 0, 24, 0);
            } else {
                m3694a(8, 8, 8, 0, 24, 0);
            }
        } catch (IllegalArgumentException unused) {
            m3694a(8, 8, 8, 0, 24, 0);
        }
        if (this.f3593k.mo12954b() != null) {
            MapRenderer.nativeInit(this.f3593k.mo12954b().f3534j);
            MapRenderer.nativeResize(this.f3593k.mo12954b().f3534j, C1066ac.f3442a, C1066ac.f3443b);
        }
    }

    /* renamed from: e */
    private void m3697e() {
        if (this.f3589g != EGL10.EGL_NO_SURFACE) {
            this.f3586d.eglMakeCurrent(this.f3587e, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            this.f3586d.eglDestroySurface(this.f3587e, this.f3589g);
            this.f3589g = EGL10.EGL_NO_SURFACE;
        }
        if (this.f3588f != EGL10.EGL_NO_CONTEXT) {
            this.f3586d.eglDestroyContext(this.f3587e, this.f3588f);
            this.f3588f = EGL10.EGL_NO_CONTEXT;
        }
        if (this.f3587e != EGL10.EGL_NO_DISPLAY) {
            this.f3586d.eglTerminate(this.f3587e);
            this.f3587e = EGL10.EGL_NO_DISPLAY;
        }
    }

    /* renamed from: a */
    public void mo13115a() {
        this.f3591i = 1;
        this.f3592j = false;
        synchronized (this) {
            if (getState() == Thread.State.WAITING) {
                notify();
            }
        }
    }

    /* renamed from: b */
    public void mo13116b() {
        this.f3591i = 0;
        synchronized (this) {
            this.f3592j = true;
        }
    }

    /* renamed from: c */
    public void mo13117c() {
        this.f3592j = true;
        synchronized (this) {
            if (getState() == Thread.State.WAITING) {
                notify();
            }
        }
    }

    public void run() {
        m3696d();
        while (this.f3585c != null) {
            if (this.f3591i != 1 || this.f3592j) {
                try {
                    synchronized (this) {
                        wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (this.f3593k.mo12954b() == null) {
                break;
            } else {
                synchronized (this.f3593k.mo12954b()) {
                    synchronized (this) {
                        if (!this.f3592j) {
                            this.f3591i = this.f3585c.mo12951a();
                        }
                        C1078e b = this.f3593k.mo12954b();
                        if (!(b == null || b.f3532h == null)) {
                            for (C1087l lVar : b.f3532h) {
                                if (lVar != null) {
                                    C1064ab J = b.mo12994J();
                                    if (this.f3590h != null) {
                                        if (lVar != null) {
                                            lVar.mo11544a(this.f3590h, J);
                                        }
                                    } else {
                                        return;
                                    }
                                }
                            }
                        }
                        this.f3586d.eglSwapBuffers(this.f3587e, this.f3589g);
                    }
                }
            }
            if (this.f3592j) {
                break;
            }
        }
        m3697e();
    }
}
