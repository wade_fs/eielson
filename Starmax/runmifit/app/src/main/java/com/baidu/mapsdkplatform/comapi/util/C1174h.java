package com.baidu.mapsdkplatform.comapi.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.storage.StorageManager;
import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.baidu.mapsdkplatform.comapi.util.h */
public final class C1174h {

    /* renamed from: a */
    private static volatile C1174h f3901a;

    /* renamed from: b */
    private boolean f3902b = false;

    /* renamed from: c */
    private boolean f3903c = true;

    /* renamed from: d */
    private final List<C1173g> f3904d = new ArrayList();

    /* renamed from: e */
    private C1173g f3905e = null;

    /* renamed from: f */
    private String f3906f;

    private C1174h() {
    }

    /* renamed from: a */
    public static C1174h m4256a() {
        if (f3901a == null) {
            synchronized (C1174h.class) {
                if (f3901a == null) {
                    f3901a = new C1174h();
                }
            }
        }
        return f3901a;
    }

    /* renamed from: a */
    private boolean m4257a(String str) {
        boolean z = false;
        try {
            File file = new File(str + "/test.0");
            if (file.exists()) {
                file.delete();
            }
            z = file.createNewFile();
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return z;
    }

    /* renamed from: c */
    private void m4258c(Context context) {
        String str;
        boolean z;
        Object[] objArr;
        Context context2 = context;
        try {
            StorageManager storageManager = (StorageManager) context2.getSystemService("storage");
            Method method = storageManager.getClass().getMethod("getVolumeList", new Class[0]);
            int i = 1;
            Method method2 = storageManager.getClass().getMethod("getVolumeState", String.class);
            Class<?> cls = Class.forName("android.os.storage.StorageVolume");
            Method method3 = cls.getMethod("isRemovable", new Class[0]);
            Method method4 = cls.getMethod("getPath", new Class[0]);
            Object[] objArr2 = (Object[]) method.invoke(storageManager, new Object[0]);
            if (objArr2 != null) {
                int length = objArr2.length;
                int i2 = 0;
                while (true) {
                    str = "外置存储卡";
                    if (i2 >= length) {
                        break;
                    }
                    Object obj = objArr2[i2];
                    String str2 = (String) method4.invoke(obj, new Object[0]);
                    if (str2 == null || str2.length() <= 0) {
                        objArr = objArr2;
                    } else {
                        objArr = objArr2;
                        Object[] objArr3 = new Object[i];
                        objArr3[0] = str2;
                        if ("mounted".equals(method2.invoke(storageManager, objArr3))) {
                            boolean z2 = !((Boolean) method3.invoke(obj, new Object[0])).booleanValue();
                            if (Build.VERSION.SDK_INT <= 19 && m4257a(str2)) {
                                List<C1173g> list = this.f3904d;
                                boolean z3 = !z2;
                                if (z2) {
                                    str = "内置存储卡";
                                }
                                list.add(new C1173g(str2, z3, str, context2));
                            } else if (Build.VERSION.SDK_INT >= 19) {
                                if (new File(str2 + File.separator + "BaiduMapSDKNew").exists() && str2.equals(context2.getSharedPreferences("map_pref", 0).getString("PREFFERED_SD_CARD", ""))) {
                                    this.f3906f = str2 + File.separator + "BaiduMapSDKNew";
                                }
                            }
                        }
                    }
                    i2++;
                    objArr2 = objArr;
                    i = 1;
                }
                if (Build.VERSION.SDK_INT >= 19) {
                    File[] externalFilesDirs = context2.getExternalFilesDirs(null);
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(this.f3904d);
                    int i3 = 0;
                    while (true) {
                        if (i3 >= externalFilesDirs.length) {
                            break;
                        } else if (externalFilesDirs[i3] == null) {
                            break;
                        } else {
                            String absolutePath = externalFilesDirs[i3].getAbsolutePath();
                            Iterator<C1173g> it = this.f3904d.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if (absolutePath.startsWith(it.next().mo13396a())) {
                                        z = true;
                                        break;
                                    }
                                } else {
                                    z = false;
                                    break;
                                }
                            }
                            String str3 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
                            if (str3 != null && !z && absolutePath.indexOf(str3) != -1) {
                                arrayList.add(new C1173g(absolutePath, true, str, context2));
                            }
                            i3++;
                        }
                    }
                    this.f3904d.clear();
                    this.f3904d.addAll(arrayList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:85:? A[RETURN, SYNTHETIC] */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m4259d(android.content.Context r13) {
        /*
            r12 = this;
            java.lang.String r0 = ":"
            java.lang.String r1 = "Auto"
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r4 = 0
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r6 = "/proc/mounts"
            r5.<init>(r6)     // Catch:{ Exception -> 0x00f5 }
            boolean r6 = r5.exists()     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r7 = " "
            r8 = 32
            r9 = 9
            if (r6 == 0) goto L_0x0059
            java.util.Scanner r6 = new java.util.Scanner     // Catch:{ Exception -> 0x00f5 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x00f5 }
        L_0x0027:
            boolean r5 = r6.hasNext()     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r5 == 0) goto L_0x004d
            java.lang.String r5 = r6.nextLine()     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            java.lang.String r10 = "/dev/block/vold/"
            boolean r10 = r5.startsWith(r10)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r10 == 0) goto L_0x0027
            java.lang.String r5 = r5.replace(r9, r8)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            java.lang.String[] r5 = r5.split(r7)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r5 == 0) goto L_0x0027
            int r10 = r5.length     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r10 <= 0) goto L_0x0027
            r10 = 1
            r5 = r5[r10]     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            r2.add(r5)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            goto L_0x0027
        L_0x004d:
            r6.close()     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            goto L_0x0059
        L_0x0051:
            r13 = move-exception
            r4 = r6
            goto L_0x00ff
        L_0x0055:
            r13 = move-exception
            r4 = r6
            goto L_0x00f6
        L_0x0059:
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r6 = "/system/etc/vold.fstab"
            r5.<init>(r6)     // Catch:{ Exception -> 0x00f5 }
            boolean r6 = r5.exists()     // Catch:{ Exception -> 0x00f5 }
            r10 = 0
            if (r6 == 0) goto L_0x00a3
            java.util.Scanner r6 = new java.util.Scanner     // Catch:{ Exception -> 0x00f5 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x00f5 }
        L_0x006c:
            boolean r5 = r6.hasNext()     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r5 == 0) goto L_0x00a0
            java.lang.String r5 = r6.nextLine()     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            java.lang.String r11 = "dev_mount"
            boolean r11 = r5.startsWith(r11)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r11 == 0) goto L_0x006c
            java.lang.String r5 = r5.replace(r9, r8)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            java.lang.String[] r5 = r5.split(r7)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r5 == 0) goto L_0x006c
            int r11 = r5.length     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r11 <= 0) goto L_0x006c
            r11 = 2
            r5 = r5[r11]     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            boolean r11 = r5.contains(r0)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            if (r11 == 0) goto L_0x009c
            int r11 = r5.indexOf(r0)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            java.lang.String r5 = r5.substring(r10, r11)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
        L_0x009c:
            r3.add(r5)     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
            goto L_0x006c
        L_0x00a0:
            r6.close()     // Catch:{ Exception -> 0x0055, all -> 0x0051 }
        L_0x00a3:
            java.io.File r0 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x00f5 }
            java.util.List<com.baidu.mapsdkplatform.comapi.util.g> r5 = r12.f3904d     // Catch:{ Exception -> 0x00f5 }
            com.baidu.mapsdkplatform.comapi.util.g r6 = new com.baidu.mapsdkplatform.comapi.util.g     // Catch:{ Exception -> 0x00f5 }
            r6.<init>(r0, r10, r1, r13)     // Catch:{ Exception -> 0x00f5 }
            r5.add(r6)     // Catch:{ Exception -> 0x00f5 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x00f5 }
        L_0x00b9:
            boolean r5 = r2.hasNext()     // Catch:{ Exception -> 0x00f5 }
            if (r5 == 0) goto L_0x00fe
            java.lang.Object r5 = r2.next()     // Catch:{ Exception -> 0x00f5 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x00f5 }
            boolean r6 = r3.contains(r5)     // Catch:{ Exception -> 0x00f5 }
            if (r6 == 0) goto L_0x00b9
            boolean r6 = r5.equals(r0)     // Catch:{ Exception -> 0x00f5 }
            if (r6 != 0) goto L_0x00b9
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x00f5 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x00f5 }
            boolean r7 = r6.exists()     // Catch:{ Exception -> 0x00f5 }
            if (r7 == 0) goto L_0x00b9
            boolean r7 = r6.isDirectory()     // Catch:{ Exception -> 0x00f5 }
            if (r7 == 0) goto L_0x00b9
            boolean r6 = r6.canWrite()     // Catch:{ Exception -> 0x00f5 }
            if (r6 == 0) goto L_0x00b9
            java.util.List<com.baidu.mapsdkplatform.comapi.util.g> r6 = r12.f3904d     // Catch:{ Exception -> 0x00f5 }
            com.baidu.mapsdkplatform.comapi.util.g r7 = new com.baidu.mapsdkplatform.comapi.util.g     // Catch:{ Exception -> 0x00f5 }
            r7.<init>(r5, r10, r1, r13)     // Catch:{ Exception -> 0x00f5 }
            r6.add(r7)     // Catch:{ Exception -> 0x00f5 }
            goto L_0x00b9
        L_0x00f3:
            r13 = move-exception
            goto L_0x00ff
        L_0x00f5:
            r13 = move-exception
        L_0x00f6:
            r13.printStackTrace()     // Catch:{ all -> 0x00f3 }
            if (r4 == 0) goto L_0x00fe
            r4.close()
        L_0x00fe:
            return
        L_0x00ff:
            if (r4 == 0) goto L_0x0104
            r4.close()
        L_0x0104:
            goto L_0x0106
        L_0x0105:
            throw r13
        L_0x0106:
            goto L_0x0105
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.util.C1174h.m4259d(android.content.Context):void");
    }

    /* renamed from: a */
    public void mo13401a(Context context) {
        if (!this.f3902b) {
            this.f3902b = true;
            try {
                if (Build.VERSION.SDK_INT >= 14) {
                    m4258c(context);
                } else {
                    m4259d(context);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (this.f3904d.size() > 0) {
                    C1173g gVar = null;
                    int i = 0;
                    for (C1173g gVar2 : this.f3904d) {
                        if (new File(gVar2.mo13397b()).exists()) {
                            i++;
                            gVar = gVar2;
                        }
                    }
                    if (i == 0) {
                        this.f3905e = mo13404b(context);
                        if (this.f3905e == null) {
                            Iterator<C1173g> it = this.f3904d.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                C1173g next = it.next();
                                if (mo13402a(context, next)) {
                                    this.f3905e = next;
                                    break;
                                }
                            }
                        }
                    } else if (i != 1) {
                        this.f3905e = mo13404b(context);
                    } else if (mo13402a(context, gVar)) {
                        this.f3905e = gVar;
                    }
                    if (this.f3905e == null) {
                        this.f3905e = this.f3904d.get(0);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                if (this.f3905e == null || !m4257a(this.f3905e.mo13396a())) {
                    this.f3903c = false;
                    this.f3905e = new C1173g(context);
                    this.f3904d.clear();
                    this.f3904d.add(this.f3905e);
                    return;
                }
                File file = new File(this.f3905e.mo13397b());
                if (!file.exists()) {
                    file.mkdirs();
                }
                File file2 = new File(this.f3905e.mo13398c());
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                File file3 = new File(file2, ".nomedia");
                if (!file3.exists()) {
                    file3.createNewFile();
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public boolean mo13402a(Context context, C1173g gVar) {
        String a = gVar.mo13396a();
        if (!m4257a(a)) {
            return false;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("map_pref", 0).edit();
        edit.putString("PREFFERED_SD_CARD", a);
        return edit.commit();
    }

    /* renamed from: b */
    public C1173g mo13403b() {
        return this.f3905e;
    }

    /* renamed from: b */
    public C1173g mo13404b(Context context) {
        String string = context.getSharedPreferences("map_pref", 0).getString("PREFFERED_SD_CARD", "");
        if (string == null || string.length() <= 0) {
            return null;
        }
        for (C1173g gVar : this.f3904d) {
            if (gVar.mo13396a().equals(string)) {
                return gVar;
            }
        }
        return null;
    }
}
