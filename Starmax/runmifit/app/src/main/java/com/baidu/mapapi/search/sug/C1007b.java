package com.baidu.mapapi.search.sug;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.sug.SuggestionResult;

/* renamed from: com.baidu.mapapi.search.sug.b */
final class C1007b implements Parcelable.Creator<SuggestionResult.SuggestionInfo> {
    C1007b() {
    }

    /* renamed from: a */
    public SuggestionResult.SuggestionInfo createFromParcel(Parcel parcel) {
        return new SuggestionResult.SuggestionInfo(parcel);
    }

    /* renamed from: a */
    public SuggestionResult.SuggestionInfo[] newArray(int i) {
        return new SuggestionResult.SuggestionInfo[i];
    }
}
