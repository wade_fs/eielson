package com.baidu.mobstat;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

/* renamed from: com.baidu.mobstat.ba */
public final class C1267ba {

    /* renamed from: a */
    private static final Proxy f4337a = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));

    /* renamed from: b */
    private static final Proxy f4338b = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));

    /* renamed from: a */
    public static String m4678a() {
        try {
            return Environment.getExternalStorageState();
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: a */
    public static File m4677a(String str) {
        File file;
        if (!"mounted".equals(m4678a())) {
            return null;
        }
        try {
            file = Environment.getExternalStorageDirectory();
        } catch (Exception unused) {
            file = null;
        }
        if (file == null) {
            return null;
        }
        return new File(file, str);
    }

    /* renamed from: a */
    public static void m4681a(Context context, String str, String str2, boolean z) {
        if (context != null) {
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = context.openFileOutput(str, z ? 32768 : 0);
                C1274bf.m4705a(new ByteArrayInputStream(str2.getBytes("utf-8")), fileOutputStream);
            } catch (Exception unused) {
            } catch (Throwable th) {
                C1274bf.m4704a(fileOutputStream);
                throw th;
            }
            C1274bf.m4704a(fileOutputStream);
        }
    }

    /* renamed from: a */
    public static void m4682a(String str, String str2, boolean z) {
        File parentFile;
        FileOutputStream fileOutputStream = null;
        try {
            File a = m4677a(str);
            if (a != null) {
                if (!a.exists() && (parentFile = a.getParentFile()) != null) {
                    parentFile.mkdirs();
                }
                FileOutputStream fileOutputStream2 = new FileOutputStream(a, z);
                try {
                    C1274bf.m4705a(new ByteArrayInputStream(str2.getBytes("utf-8")), fileOutputStream2);
                } catch (Exception unused) {
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = fileOutputStream2;
                    C1274bf.m4704a(fileOutputStream);
                    throw th;
                }
                fileOutputStream = fileOutputStream2;
            }
        } catch (Exception unused2) {
        } catch (Throwable th2) {
            th = th2;
            C1274bf.m4704a(fileOutputStream);
            throw th;
        }
        C1274bf.m4704a(fileOutputStream);
    }

    /* renamed from: a */
    public static String m4679a(Context context, String str) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = context.openFileInput(str);
            byte[] a = m4683a(fileInputStream);
            if (a != null) {
                String str2 = new String(a, "utf-8");
                C1274bf.m4704a(fileInputStream);
                return str2;
            }
        } catch (Exception unused) {
        } catch (Throwable th) {
            C1274bf.m4704a(null);
            throw th;
        }
        C1274bf.m4704a(fileInputStream);
        return "";
    }

    /* renamed from: b */
    public static String m4684b(String str) {
        FileInputStream fileInputStream;
        File a = m4677a(str);
        if (a == null || !a.exists()) {
            return "";
        }
        try {
            fileInputStream = new FileInputStream(a);
            try {
                byte[] a2 = m4683a(fileInputStream);
                if (a2 != null) {
                    String str2 = new String(a2, "utf-8");
                    C1274bf.m4704a(fileInputStream);
                    return str2;
                }
            } catch (Exception unused) {
            } catch (Throwable th) {
                th = th;
                C1274bf.m4704a(fileInputStream);
                throw th;
            }
        } catch (Exception unused2) {
            fileInputStream = null;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            C1274bf.m4704a(fileInputStream);
            throw th;
        }
        C1274bf.m4704a(fileInputStream);
        return "";
    }

    /* renamed from: a */
    private static byte[] m4683a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (C1274bf.m4705a(inputStream, byteArrayOutputStream)) {
            return byteArrayOutputStream.toByteArray();
        }
        return null;
    }

    /* renamed from: b */
    public static boolean m4685b(Context context, String str) {
        return context.deleteFile(str);
    }

    /* renamed from: c */
    public static boolean m4687c(String str) {
        File a = m4677a(str);
        if (a == null || !a.isFile()) {
            return false;
        }
        return a.delete();
    }

    /* renamed from: c */
    public static boolean m4686c(Context context, String str) {
        return context.getFileStreamPath(str).exists();
    }

    /* renamed from: d */
    public static HttpURLConnection m4688d(Context context, String str) throws IOException {
        return m4680a(context, str, 50000, 50000);
    }

    /* renamed from: a */
    public static HttpURLConnection m4680a(Context context, String str, int i, int i2) throws IOException {
        HttpURLConnection httpURLConnection;
        URL url = new URL(str);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
        if (networkInfo2 == null || !networkInfo2.isAvailable()) {
            if (networkInfo != null && networkInfo.isAvailable()) {
                String extraInfo = networkInfo.getExtraInfo();
                String lowerCase = extraInfo != null ? extraInfo.toLowerCase() : "";
                if (lowerCase.startsWith("cmwap") || lowerCase.startsWith("uniwap") || lowerCase.startsWith("3gwap")) {
                    httpURLConnection = (HttpURLConnection) url.openConnection(f4337a);
                } else if (lowerCase.startsWith("ctwap")) {
                    httpURLConnection = (HttpURLConnection) url.openConnection(f4338b);
                }
            }
            httpURLConnection = null;
        } else {
            httpURLConnection = (HttpURLConnection) url.openConnection();
        }
        if (httpURLConnection == null) {
            httpURLConnection = (HttpURLConnection) url.openConnection();
        }
        httpURLConnection.setConnectTimeout(i);
        httpURLConnection.setReadTimeout(i2);
        return httpURLConnection;
    }

    /* renamed from: e */
    public static boolean m4689e(Context context, String str) {
        boolean z = false;
        try {
            if (context.checkCallingOrSelfPermission(str) == 0) {
                z = true;
            }
        } catch (Exception unused) {
        }
        if (!z) {
            C1256at c = C1256at.m4629c();
            c.mo13943b("[WARNING] not have permission " + str + ", please add it in AndroidManifest.xml according our developer doc");
        }
        return z;
    }
}
