package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.j */
final class C1140j implements Parcelable.Creator<SyncResponseResult> {
    C1140j() {
    }

    /* renamed from: a */
    public SyncResponseResult createFromParcel(Parcel parcel) {
        return new SyncResponseResult(parcel);
    }

    /* renamed from: a */
    public SyncResponseResult[] newArray(int i) {
        return new SyncResponseResult[i];
    }
}
