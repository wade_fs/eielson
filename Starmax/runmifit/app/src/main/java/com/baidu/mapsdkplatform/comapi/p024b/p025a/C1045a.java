package com.baidu.mapsdkplatform.comapi.p024b.p025a;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.net.URLEncoder;

/* renamed from: com.baidu.mapsdkplatform.comapi.b.a.a */
public class C1045a implements Thread.UncaughtExceptionHandler {

    /* renamed from: b */
    private static volatile boolean f3357b = false;

    /* renamed from: a */
    private String f3358a;

    /* renamed from: c */
    private Thread.UncaughtExceptionHandler f3359c;

    /* renamed from: com.baidu.mapsdkplatform.comapi.b.a.a$a */
    private static class C1046a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1045a f3360a = new C1045a();
    }

    private C1045a() {
        this.f3358a = "";
        this.f3359c = Thread.getDefaultUncaughtExceptionHandler();
    }

    /* renamed from: a */
    public static C1045a m3442a() {
        return C1046a.f3360a;
    }

    /* renamed from: a */
    private void m3443a(Throwable th) {
        if (th != null) {
            String th2 = th.toString();
            if (!th2.isEmpty() && !th2.contains("BDMapSDKException")) {
                if (th2.contains("com.baidu.platform") || th2.contains("com.baidu.mapsdkplatform") || th2.contains("com.baidu.mapsdkvi")) {
                    try {
                        StringWriter stringWriter = new StringWriter();
                        PrintWriter printWriter = new PrintWriter(stringWriter);
                        th.printStackTrace(printWriter);
                        Throwable cause = th.getCause();
                        if (cause != null) {
                            cause.printStackTrace(printWriter);
                        }
                        printWriter.close();
                        String obj = stringWriter.toString();
                        if (obj.isEmpty() || this.f3358a == null) {
                            return;
                        }
                        if (!this.f3358a.isEmpty()) {
                            File file = new File(URLEncoder.encode(this.f3358a + (System.currentTimeMillis() / 1000) + ".txt", "UTF-8"));
                            if (file.exists() || file.createNewFile()) {
                                FileOutputStream fileOutputStream = new FileOutputStream(file);
                                fileOutputStream.write(obj.getBytes());
                                fileOutputStream.close();
                            }
                        }
                    } catch (Exception unused) {
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12921a(String str) {
        this.f3358a = str;
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof C1045a)) {
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        if (!f3357b) {
            f3357b = true;
            m3443a(th);
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.f3359c;
            if (uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, th);
            }
        }
    }
}
