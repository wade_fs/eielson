package com.baidu.mapapi.map.offline;

import com.baidu.mapsdkplatform.comapi.map.C1098v;
import java.util.ArrayList;

/* renamed from: com.baidu.mapapi.map.offline.a */
class C0940a implements C1098v {

    /* renamed from: a */
    final /* synthetic */ MKOfflineMap f2862a;

    C0940a(MKOfflineMap mKOfflineMap) {
        this.f2862a = mKOfflineMap;
    }

    /* renamed from: a */
    public void mo11594a(int i, int i2) {
        MKOfflineMapListener mKOfflineMapListener;
        if (i != 4) {
            int i3 = 6;
            if (i == 6) {
                mKOfflineMapListener = this.f2862a.f2861c;
            } else if (i == 8) {
                this.f2862a.f2861c.onGetOfflineMapState(0, i2 >> 8);
                return;
            } else if (i == 10) {
                mKOfflineMapListener = this.f2862a.f2861c;
                i3 = 2;
            } else if (i == 12) {
                this.f2862a.f2860b.mo13124a(true, false);
                return;
            } else {
                return;
            }
            mKOfflineMapListener.onGetOfflineMapState(i3, i2);
            return;
        }
        ArrayList<MKOLUpdateElement> allUpdateInfo = this.f2862a.getAllUpdateInfo();
        if (allUpdateInfo != null) {
            for (MKOLUpdateElement mKOLUpdateElement : allUpdateInfo) {
                if (mKOLUpdateElement.update) {
                    this.f2862a.f2861c.onGetOfflineMapState(4, mKOLUpdateElement.cityID);
                }
            }
        }
    }
}
