package com.baidu.location;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.location.d */
final class C0789d implements Parcelable.Creator<Poi> {
    C0789d() {
    }

    public Poi createFromParcel(Parcel parcel) {
        return new Poi(parcel.readString(), parcel.readString(), parcel.readDouble());
    }

    public Poi[] newArray(int i) {
        return new Poi[i];
    }
}
