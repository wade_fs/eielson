package com.baidu.platform.base;

import android.support.v4.app.NotificationCompat;
import com.baidu.mapapi.search.core.SearchResult;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.base.d */
public abstract class C1319d {

    /* renamed from: a */
    protected SearchType f4434a;

    /* renamed from: a */
    public abstract SearchResult mo14018a(String str);

    /* renamed from: a */
    public SearchType mo14019a() {
        return this.f4434a;
    }

    /* renamed from: a */
    public abstract void mo14020a(SearchResult searchResult, Object obj);

    /* renamed from: a */
    public void mo14021a(SearchType searchType) {
        this.f4434a = searchType;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo14022a(String str, SearchResult searchResult, boolean z) {
        SearchResult.ERRORNO errorno;
        if (str != null) {
            try {
                if (str.length() > 0) {
                    int optInt = new JSONObject(str).optInt(z ? NotificationCompat.CATEGORY_STATUS : "status_sp");
                    if (optInt == 0) {
                        return false;
                    }
                    if (optInt != 200 && optInt != 230) {
                        switch (optInt) {
                            case 104:
                            case 105:
                            case 106:
                            case 107:
                            case 108:
                                errorno = SearchResult.ERRORNO.PERMISSION_UNFINISHED;
                                break;
                            default:
                                errorno = SearchResult.ERRORNO.RESULT_NOT_FOUND;
                                break;
                        }
                    } else {
                        errorno = SearchResult.ERRORNO.KEY_ERROR;
                    }
                    searchResult.error = errorno;
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                searchResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
                return true;
            }
        }
        searchResult.error = SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR;
        return true;
    }
}
