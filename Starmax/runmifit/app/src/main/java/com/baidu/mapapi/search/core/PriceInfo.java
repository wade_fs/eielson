package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

public class PriceInfo implements Parcelable {
    public static final Parcelable.Creator<PriceInfo> CREATOR = new C0965i();

    /* renamed from: a */
    private int f2925a;

    /* renamed from: b */
    private double f2926b;

    public PriceInfo() {
    }

    protected PriceInfo(Parcel parcel) {
        this.f2925a = parcel.readInt();
        this.f2926b = parcel.readDouble();
    }

    public int describeContents() {
        return 0;
    }

    public double getTicketPrice() {
        return this.f2926b;
    }

    public int getTicketType() {
        return this.f2925a;
    }

    public void setTicketPrice(double d) {
        this.f2926b = d;
    }

    public void setTicketType(int i) {
        this.f2925a = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f2925a);
        parcel.writeDouble(this.f2926b);
    }
}
