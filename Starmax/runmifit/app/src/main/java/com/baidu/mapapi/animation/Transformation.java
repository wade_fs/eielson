package com.baidu.mapapi.animation;

import android.graphics.Point;
import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapsdkplatform.comapi.p023a.C1031c;
import com.baidu.mapsdkplatform.comapi.p023a.C1040l;

public class Transformation extends Animation {
    public Transformation(Point... pointArr) {
        if (pointArr == null || pointArr.length == 0) {
            throw new NullPointerException("BDMapSDKException: the points is null");
        }
        this.bdAnimation = new C1040l(pointArr);
    }

    public Transformation(LatLng... latLngArr) {
        if (latLngArr == null || latLngArr.length == 0) {
            throw new NullPointerException("BDMapSDKException: the latlngs is null");
        }
        this.bdAnimation = new C1040l(latLngArr);
    }

    public void cancel() {
        this.bdAnimation.mo12881b();
    }

    public void setAnimationListener(Animation.AnimationListener animationListener) {
        this.bdAnimation.mo12879a(animationListener);
    }

    public void setDuration(long j) {
        this.bdAnimation.mo12876a(j);
    }

    public void setInterpolator(Interpolator interpolator) {
        this.bdAnimation.mo12878a(interpolator);
    }

    public void setRepeatCount(int i) {
        this.bdAnimation.mo12882b(i);
    }

    public void setRepeatMode(Animation.RepeatMode repeatMode) {
        C1031c cVar;
        int i;
        if (repeatMode == Animation.RepeatMode.RESTART) {
            cVar = this.bdAnimation;
            i = 1;
        } else if (repeatMode == Animation.RepeatMode.REVERSE) {
            cVar = this.bdAnimation;
            i = 2;
        } else {
            return;
        }
        cVar.mo12875a(i);
    }
}
