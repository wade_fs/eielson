package com.baidu.mobstat;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.baidu.mobstat.C1262ay;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class CooperService implements ICooperService {

    /* renamed from: a */
    private static CooperService f4115a;

    /* renamed from: b */
    private HeadObject f4116b = new HeadObject();

    public String getMTJSDKVersion() {
        return "3.9.2.1";
    }

    public int getTagValue() {
        return 1;
    }

    public static synchronized CooperService instance() {
        CooperService cooperService;
        synchronized (CooperService.class) {
            if (f4115a == null) {
                f4115a = new CooperService();
            }
            cooperService = f4115a;
        }
        return cooperService;
    }

    public HeadObject getHeadObject() {
        return this.f4116b;
    }

    public String getHost() {
        return Config.LOG_SEND_URL;
    }

    public void installHeader(Context context, JSONObject jSONObject) {
        this.f4116b.installHeader(context, jSONObject);
    }

    public JSONObject getHeaderExt(Context context) {
        String headerExt = BasicStoreTools.getInstance().getHeaderExt(context);
        if (!TextUtils.isEmpty(headerExt)) {
            try {
                return new JSONObject(headerExt);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    public void setHeaderExt(Context context, ExtraInfo extraInfo) {
        String str;
        JSONObject jSONObject = new JSONObject();
        if (extraInfo != null) {
            jSONObject = extraInfo.dumpToJson();
        }
        this.f4116b.setHeaderExt(jSONObject);
        BasicStoreTools.getInstance().setHeaderExt(context, jSONObject.toString());
        if (extraInfo != null) {
            str = "Set global ExtraInfo: " + jSONObject;
        } else {
            str = "Clear global ExtraInfo";
        }
        C1256at.m4629c().mo13941a(str);
    }

    public JSONObject getPushId(Context context) {
        String pushId = BasicStoreTools.getInstance().getPushId(context);
        if (!TextUtils.isEmpty(pushId)) {
            try {
                return new JSONObject(pushId);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    public void setPushId(Context context, String str, String str2, String str3) {
        String str4;
        JSONObject pushId = getPushId(context);
        if (pushId == null) {
            pushId = new JSONObject();
        }
        try {
            if (!TextUtils.isEmpty(str3)) {
                pushId.put(str, str3);
            } else {
                pushId.remove(str);
            }
        } catch (Exception unused) {
        }
        this.f4116b.setPushInfo(pushId);
        BasicStoreTools.getInstance().setPushId(context, pushId.toString());
        if (str3 != null) {
            str4 = "Set platform:" + str2 + " pushId: " + str3;
        } else {
            str4 = "Clear platform:" + str2 + " pushId";
        }
        C1256at.m4629c().mo13941a(str4);
    }

    /* renamed from: a */
    private static String m4412a(Context context) {
        String k = C1276bh.m4738k(context);
        return !TextUtils.isEmpty(k) ? k.replaceAll(Config.TRACE_TODAY_VISIT_SPLIT, "") : k;
    }

    /* renamed from: b */
    private static String m4414b(Context context) {
        String j = C1276bh.m4737j(context);
        return !TextUtils.isEmpty(j) ? j.replaceAll(Config.TRACE_TODAY_VISIT_SPLIT, "") : j;
    }

    /* renamed from: c */
    private static String m4415c(Context context) {
        String m = C1276bh.m4740m(context);
        return !TextUtils.isEmpty(m) ? m.replaceAll(Config.TRACE_TODAY_VISIT_SPLIT, "") : m;
    }

    public String getMacAddress(Context context, boolean z) {
        String replace = Config.DEF_MAC_ID.replace(Config.TRACE_TODAY_VISIT_SPLIT, "");
        if (!z && Build.VERSION.SDK_INT >= 23) {
            return getSecretValue(replace);
        }
        if (!TextUtils.isEmpty(this.f4116b.f4167s)) {
            return this.f4116b.f4167s;
        }
        String appDeviceMac = BasicStoreTools.getInstance().getAppDeviceMac(context);
        if (!TextUtils.isEmpty(appDeviceMac)) {
            HeadObject headObject = this.f4116b;
            headObject.f4167s = appDeviceMac;
            return headObject.f4167s;
        }
        String a = m4413a(context, z);
        if (TextUtils.isEmpty(a) || replace.equals(a)) {
            HeadObject headObject2 = this.f4116b;
            headObject2.f4167s = "";
            return headObject2.f4167s;
        }
        this.f4116b.f4167s = getSecretValue(a);
        BasicStoreTools.getInstance().setAppDeviceMac(context, this.f4116b.f4167s);
        return this.f4116b.f4167s;
    }

    /* renamed from: a */
    private String m4413a(Context context, boolean z) {
        String str;
        if (z) {
            str = m4414b(context);
        } else {
            str = m4412a(context);
        }
        return TextUtils.isEmpty(str) ? "" : str;
    }

    public String getMacIdForTv(Context context) {
        if (!TextUtils.isEmpty(this.f4116b.f4168t)) {
            return this.f4116b.f4168t;
        }
        String appDeviceMacTv = BasicStoreTools.getInstance().getAppDeviceMacTv(context);
        if (!TextUtils.isEmpty(appDeviceMacTv)) {
            HeadObject headObject = this.f4116b;
            headObject.f4168t = appDeviceMacTv;
            return headObject.f4168t;
        }
        String c = C1276bh.m4724c(1, context);
        if (!TextUtils.isEmpty(c)) {
            this.f4116b.f4168t = c;
            BasicStoreTools.getInstance().setAppDeviceMacTv(context, c);
            return this.f4116b.f4168t;
        }
        HeadObject headObject2 = this.f4116b;
        headObject2.f4168t = "";
        return headObject2.f4168t;
    }

    public String getCUID(Context context, boolean z) {
        if (this.f4116b.f4154f == null) {
            this.f4116b.f4154f = BasicStoreTools.getInstance().getGenerateDeviceCUID(context);
            if (this.f4116b.f4154f == null || "".equalsIgnoreCase(this.f4116b.f4154f)) {
                try {
                    this.f4116b.f4154f = C1278bi.m4753a(context);
                    Matcher matcher = Pattern.compile("\\s*|\t|\r|\n").matcher(this.f4116b.f4154f);
                    this.f4116b.f4154f = matcher.replaceAll("");
                    this.f4116b.f4154f = getSecretValue(this.f4116b.f4154f);
                    BasicStoreTools.getInstance().setGenerateDeviceCUID(context, this.f4116b.f4154f);
                } catch (Exception unused) {
                }
            }
        }
        if (z) {
            return this.f4116b.f4154f;
        }
        try {
            String str = this.f4116b.f4154f;
            if (!TextUtils.isEmpty(str)) {
                return new String(C1262ay.C1264b.m4671b(1, C1268bb.m4691a(str.getBytes())));
            }
            return null;
        } catch (Exception unused2) {
            return null;
        }
    }

    public String getDevicImei(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception unused) {
            return "";
        }
    }

    public String getDeviceId(TelephonyManager telephonyManager, Context context) {
        String str = this.f4116b.f4157i;
        if (!TextUtils.isEmpty(str)) {
            return this.f4116b.f4157i;
        }
        if (BasicStoreTools.getInstance().getForTV(context)) {
            this.f4116b.f4157i = getMacIdForTv(context);
            return this.f4116b.f4157i;
        } else if (telephonyManager == null) {
            return this.f4116b.f4157i;
        } else {
            Pattern compile = Pattern.compile("\\s*|\t|\r|\n");
            try {
                String deviceId = telephonyManager.getDeviceId();
                if (deviceId != null) {
                    str = compile.matcher(deviceId).replaceAll("");
                }
            } catch (Exception unused) {
            }
            if (str == null || str.equals(Config.NULL_DEVICE_ID)) {
                str = m4412a(context);
            }
            if (C1276bh.m4748u(context) && (TextUtils.isEmpty(str) || str.equals(Config.NULL_DEVICE_ID))) {
                try {
                    str = m4415c(context);
                } catch (Exception unused2) {
                }
            }
            if (TextUtils.isEmpty(str) || str.equals(Config.NULL_DEVICE_ID)) {
                str = m4416d(context);
            }
            HeadObject headObject = this.f4116b;
            headObject.f4157i = str;
            headObject.f4157i = getSecretValue(headObject.f4157i);
            return this.f4116b.f4157i;
        }
    }

    /* renamed from: d */
    private String m4416d(Context context) {
        String generateDeviceId = BasicStoreTools.getInstance().getGenerateDeviceId(context);
        if (!TextUtils.isEmpty(generateDeviceId) && !generateDeviceId.equals(Config.NULL_DEVICE_ID)) {
            return generateDeviceId;
        }
        String str = new Date().getTime() + "";
        String str2 = "hol" + str.hashCode() + "mes";
        BasicStoreTools.getInstance().setGenerateDeviceId(context, str2);
        return str2;
    }

    public String getPlainDeviceIdForCar(Context context) {
        String optUUID = CarUUID.optUUID(context);
        if (TextUtils.isEmpty(optUUID)) {
            optUUID = m4416d(context);
        }
        return TextUtils.isEmpty(optUUID) ? "" : optUUID;
    }

    public String getAppChannel(Context context) {
        return m4417e(context);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0010, code lost:
        if (r4.f4116b.f4160l.equals("") != false) goto L_0x0012;
     */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m4417e(android.content.Context r5) {
        /*
            r4 = this;
            com.baidu.mobstat.HeadObject r0 = r4.f4116b     // Catch:{ Exception -> 0x0044 }
            java.lang.String r0 = r0.f4160l     // Catch:{ Exception -> 0x0044 }
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x0012
            com.baidu.mobstat.HeadObject r0 = r4.f4116b     // Catch:{ Exception -> 0x0044 }
            java.lang.String r0 = r0.f4160l     // Catch:{ Exception -> 0x0044 }
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0044 }
            if (r0 == 0) goto L_0x0044
        L_0x0012:
            com.baidu.mobstat.BasicStoreTools r0 = com.baidu.mobstat.BasicStoreTools.getInstance()     // Catch:{ Exception -> 0x0044 }
            boolean r0 = r0.getAppChannelWithCode(r5)     // Catch:{ Exception -> 0x0044 }
            if (r0 == 0) goto L_0x0028
            com.baidu.mobstat.HeadObject r2 = r4.f4116b     // Catch:{ Exception -> 0x0044 }
            com.baidu.mobstat.BasicStoreTools r3 = com.baidu.mobstat.BasicStoreTools.getInstance()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r3 = r3.getAppChannelWithPreference(r5)     // Catch:{ Exception -> 0x0044 }
            r2.f4160l = r3     // Catch:{ Exception -> 0x0044 }
        L_0x0028:
            if (r0 == 0) goto L_0x003a
            com.baidu.mobstat.HeadObject r0 = r4.f4116b     // Catch:{ Exception -> 0x0044 }
            java.lang.String r0 = r0.f4160l     // Catch:{ Exception -> 0x0044 }
            if (r0 == 0) goto L_0x003a
            com.baidu.mobstat.HeadObject r0 = r4.f4116b     // Catch:{ Exception -> 0x0044 }
            java.lang.String r0 = r0.f4160l     // Catch:{ Exception -> 0x0044 }
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0044 }
            if (r0 == 0) goto L_0x0044
        L_0x003a:
            com.baidu.mobstat.HeadObject r0 = r4.f4116b     // Catch:{ Exception -> 0x0044 }
            java.lang.String r1 = "BaiduMobAd_CHANNEL"
            java.lang.String r5 = com.baidu.mobstat.C1276bh.m4716a(r5, r1)     // Catch:{ Exception -> 0x0044 }
            r0.f4160l = r5     // Catch:{ Exception -> 0x0044 }
        L_0x0044:
            com.baidu.mobstat.HeadObject r5 = r4.f4116b
            java.lang.String r5 = r5.f4160l
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.CooperService.m4417e(android.content.Context):java.lang.String");
    }

    public String getAppKey(Context context) {
        if (this.f4116b.f4153e == null) {
            this.f4116b.f4153e = C1276bh.m4716a(context, Config.APPKEY_META_NAME);
        }
        return this.f4116b.f4153e;
    }

    public int getAppVersionCode(Context context) {
        if (this.f4116b.f4155g == -1) {
            this.f4116b.f4155g = C1276bh.m4730f(context);
        }
        return this.f4116b.f4155g;
    }

    public String getAppVersionName(Context context) {
        if (TextUtils.isEmpty(this.f4116b.f4156h)) {
            this.f4116b.f4156h = C1276bh.m4733g(context);
        }
        return this.f4116b.f4156h;
    }

    public void setAppVersionName(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f4116b.f4156h = str;
        }
    }

    public String getOperator(TelephonyManager telephonyManager) {
        if (TextUtils.isEmpty(this.f4116b.f4161m)) {
            this.f4116b.f4161m = telephonyManager.getNetworkOperator();
        }
        return this.f4116b.f4161m;
    }

    public String getLinkedWay(Context context) {
        if (TextUtils.isEmpty(this.f4116b.f4166r)) {
            this.f4116b.f4166r = C1276bh.m4744q(context);
        }
        return this.f4116b.f4166r;
    }

    public String getOSVersion() {
        if (TextUtils.isEmpty(this.f4116b.f4150b)) {
            this.f4116b.f4150b = Integer.toString(Build.VERSION.SDK_INT);
        }
        return this.f4116b.f4150b;
    }

    public String getOSSysVersion() {
        if (TextUtils.isEmpty(this.f4116b.f4151c)) {
            this.f4116b.f4151c = Build.VERSION.RELEASE;
        }
        return this.f4116b.f4151c;
    }

    public String getPhoneModel() {
        if (TextUtils.isEmpty(this.f4116b.f4162n)) {
            this.f4116b.f4162n = Build.MODEL;
        }
        return this.f4116b.f4162n;
    }

    public String getManufacturer() {
        if (TextUtils.isEmpty(this.f4116b.f4163o)) {
            this.f4116b.f4163o = Build.MANUFACTURER;
        }
        return this.f4116b.f4163o;
    }

    public boolean checkWifiLocationSetting(Context context) {
        return "true".equalsIgnoreCase(C1276bh.m4716a(context, Config.GET_WIFI_LOCATION));
    }

    public boolean checkGPSLocationSetting(Context context) {
        return "true".equals(C1276bh.m4716a(context, Config.GET_GPS_LOCATION));
    }

    public boolean checkCellLocationSetting(Context context) {
        return "true".equalsIgnoreCase(C1276bh.m4716a(context, Config.GET_CELL_LOCATION));
    }

    public String getSecretValue(String str) {
        return C1262ay.C1264b.m4672c(1, str.getBytes());
    }

    public String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public void resetHeadSign() {
        this.f4116b.f4174z = instance().getUUID();
    }

    public void enableDeviceMac(Context context, boolean z) {
        BasicStoreTools.getInstance().setMacEnabledTrick(context, z);
    }

    public boolean isDeviceMacEnabled(Context context) {
        return BasicStoreTools.getInstance().getMacEnabledTrick(context);
    }

    public void setUserId(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            str = "";
        }
        if (str.length() > 256) {
            str = str.substring(0, 256);
        }
        BasicStoreTools.getInstance().setUserId(context, str);
        this.f4116b.setUserId(str);
        C1256at.m4629c().mo13941a("Set user id " + str);
    }

    public String getUserId(Context context) {
        return BasicStoreTools.getInstance().getUserId(context);
    }
}
