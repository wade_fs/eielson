package com.baidu.location.p013a;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.tencent.tauth.AuthActivity;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

/* renamed from: com.baidu.location.a.k */
public class C0742k {
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static long f1346j = 12000;

    /* renamed from: a */
    public C0748e f1347a;

    /* renamed from: b */
    private Context f1348b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public WebView f1349c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public LocationClient f1350d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C0744a f1351e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public List<C0745b> f1352f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f1353g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public long f1354h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public BDLocation f1355i;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public C0749f f1356k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public boolean f1357l;

    /* renamed from: com.baidu.location.a.k$a */
    private class C0744a extends Handler {
        C0744a(Looper looper) {
            super(looper);
        }

        /* renamed from: a */
        private String m1869a(BDLocation bDLocation) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("latitude", bDLocation.getLatitude());
                jSONObject.put("longitude", bDLocation.getLongitude());
                jSONObject.put("radius", (double) bDLocation.getRadius());
                jSONObject.put("errorcode", 1);
                if (bDLocation.hasAltitude()) {
                    jSONObject.put("altitude", bDLocation.getAltitude());
                }
                if (bDLocation.hasSpeed()) {
                    jSONObject.put("speed", (double) (bDLocation.getSpeed() / 3.6f));
                }
                if (bDLocation.getLocType() == 61) {
                    jSONObject.put("direction", (double) bDLocation.getDirection());
                }
                if (bDLocation.getBuildingName() != null) {
                    jSONObject.put("buildingname", bDLocation.getBuildingName());
                }
                if (bDLocation.getBuildingID() != null) {
                    jSONObject.put("buildingid", bDLocation.getBuildingID());
                }
                if (bDLocation.getFloor() != null) {
                    jSONObject.put("floor", bDLocation.getFloor());
                }
                return jSONObject.toString();
            } catch (Exception unused) {
                return null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.k.a(com.baidu.location.a.k, boolean):boolean
         arg types: [com.baidu.location.a.k, int]
         candidates:
          com.baidu.location.a.k.a(com.baidu.location.a.k, long):long
          com.baidu.location.a.k.a(com.baidu.location.a.k, com.baidu.location.BDLocation):com.baidu.location.BDLocation
          com.baidu.location.a.k.a(com.baidu.location.a.k, com.baidu.location.a.k$f):com.baidu.location.a.k$f
          com.baidu.location.a.k.a(com.baidu.location.a.k, java.util.List):java.util.List
          com.baidu.location.a.k.a(com.baidu.location.a.k, boolean):boolean */
        /* renamed from: a */
        private void m1870a(String str) {
            if (C0742k.this.f1357l) {
                C0742k.this.f1351e.removeCallbacks(C0742k.this.f1356k);
                boolean unused = C0742k.this.f1357l = false;
            }
            if (C0742k.this.f1352f != null && C0742k.this.f1352f.size() > 0) {
                Iterator it = C0742k.this.f1352f.iterator();
                while (it.hasNext()) {
                    try {
                        C0745b bVar = (C0745b) it.next();
                        if (bVar.mo10472b() != null) {
                            C0742k.this.f1349c.loadUrl("javascript:" + bVar.mo10472b() + "('" + str + "')");
                        }
                        it.remove();
                    } catch (Exception unused2) {
                        return;
                    }
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.k.a(com.baidu.location.a.k, boolean):boolean
         arg types: [com.baidu.location.a.k, int]
         candidates:
          com.baidu.location.a.k.a(com.baidu.location.a.k, long):long
          com.baidu.location.a.k.a(com.baidu.location.a.k, com.baidu.location.BDLocation):com.baidu.location.BDLocation
          com.baidu.location.a.k.a(com.baidu.location.a.k, com.baidu.location.a.k$f):com.baidu.location.a.k$f
          com.baidu.location.a.k.a(com.baidu.location.a.k, java.util.List):java.util.List
          com.baidu.location.a.k.a(com.baidu.location.a.k, boolean):boolean */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x011c  */
        /* JADX WARNING: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r10) {
            /*
                r9 = this;
                int r0 = r10.what
                r1 = 2
                java.lang.String r2 = "errorcode"
                r3 = 0
                r4 = 0
                switch(r0) {
                    case 1: goto L_0x00bf;
                    case 2: goto L_0x00b2;
                    case 3: goto L_0x0087;
                    case 4: goto L_0x0032;
                    case 5: goto L_0x001d;
                    case 6: goto L_0x000c;
                    default: goto L_0x000a;
                }
            L_0x000a:
                goto L_0x0162
            L_0x000c:
                org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Exception -> 0x0019 }
                r10.<init>()     // Catch:{ Exception -> 0x0019 }
                r10.put(r2, r1)     // Catch:{ Exception -> 0x0019 }
                java.lang.String r4 = r10.toString()     // Catch:{ Exception -> 0x0019 }
                goto L_0x001a
            L_0x0019:
            L_0x001a:
                if (r4 == 0) goto L_0x0162
                goto L_0x002d
            L_0x001d:
                org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ Exception -> 0x002a }
                r10.<init>()     // Catch:{ Exception -> 0x002a }
                r10.put(r2, r3)     // Catch:{ Exception -> 0x002a }
                java.lang.String r4 = r10.toString()     // Catch:{ Exception -> 0x002a }
                goto L_0x002b
            L_0x002a:
            L_0x002b:
                if (r4 == 0) goto L_0x0162
            L_0x002d:
                r9.m1870a(r4)
                goto L_0x0162
            L_0x0032:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                java.util.List r10 = r10.f1352f
                if (r10 == 0) goto L_0x0048
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                java.util.List r10 = r10.f1352f
                r10.clear()
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                java.util.List unused = r10.f1352f = r4
            L_0x0048:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.LocationClient r10 = r10.f1350d
                com.baidu.location.a.k r0 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$e r0 = r0.f1347a
                r10.unRegisterLocationListener(r0)
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                r0 = 0
                long unused = r10.f1354h = r0
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.BDLocation unused = r10.f1355i = r4
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$f r10 = r10.f1356k
                if (r10 == 0) goto L_0x0080
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                boolean r10 = r10.f1357l
                if (r10 == 0) goto L_0x0080
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$a r10 = r10.f1351e
                com.baidu.location.a.k r0 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$f r0 = r0.f1356k
                r10.removeCallbacks(r0)
            L_0x0080:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                boolean unused = r10.f1357l = r3
                goto L_0x0162
            L_0x0087:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                java.util.List r10 = r10.f1352f
                if (r10 != 0) goto L_0x009a
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
                java.util.List unused = r10.f1352f = r0
                goto L_0x00a3
            L_0x009a:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                java.util.List r10 = r10.f1352f
                r10.clear()
            L_0x00a3:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.LocationClient r10 = r10.f1350d
                com.baidu.location.a.k r0 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$e r0 = r0.f1347a
                r10.registerLocationListener(r0)
                goto L_0x0162
            L_0x00b2:
                java.lang.Object r10 = r10.obj
                com.baidu.location.BDLocation r10 = (com.baidu.location.BDLocation) r10
                java.lang.String r10 = r9.m1869a(r10)
                r9.m1870a(r10)
                goto L_0x0162
            L_0x00bf:
                java.lang.Object r10 = r10.obj
                com.baidu.location.a.k$b r10 = (com.baidu.location.p013a.C0742k.C0745b) r10
                com.baidu.location.a.k r0 = com.baidu.location.p013a.C0742k.this
                java.util.List r0 = r0.f1352f
                if (r0 == 0) goto L_0x00d4
                com.baidu.location.a.k r0 = com.baidu.location.p013a.C0742k.this
                java.util.List r0 = r0.f1352f
                r0.add(r10)
            L_0x00d4:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.LocationClient r10 = r10.f1350d
                if (r10 == 0) goto L_0x0162
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.LocationClient r10 = r10.f1350d
                int r10 = r10.requestLocation()
                r0 = 1
                if (r10 == 0) goto L_0x0119
                long r5 = java.lang.System.currentTimeMillis()
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                long r7 = r10.f1354h
                long r5 = r5 - r7
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.BDLocation r10 = r10.f1355i
                if (r10 == 0) goto L_0x0119
                r7 = 10000(0x2710, double:4.9407E-320)
                int r10 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                if (r10 > 0) goto L_0x0119
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$a r10 = r10.f1351e
                android.os.Message r10 = r10.obtainMessage(r1)
                com.baidu.location.a.k r1 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.BDLocation r1 = r1.f1355i
                r10.obj = r1
                r10.sendToTarget()
                r10 = 0
                goto L_0x011a
            L_0x0119:
                r10 = 1
            L_0x011a:
                if (r10 == 0) goto L_0x0162
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                boolean r10 = r10.f1357l
                if (r10 == 0) goto L_0x0138
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$a r10 = r10.f1351e
                com.baidu.location.a.k r1 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$f r1 = r1.f1356k
                r10.removeCallbacks(r1)
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                boolean unused = r10.f1357l = r3
            L_0x0138:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$f r10 = r10.f1356k
                if (r10 != 0) goto L_0x014a
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$f r1 = new com.baidu.location.a.k$f
                r1.<init>()
                com.baidu.location.p013a.C0742k.C0749f unused = r10.f1356k = r1
            L_0x014a:
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$a r10 = r10.f1351e
                com.baidu.location.a.k r1 = com.baidu.location.p013a.C0742k.this
                com.baidu.location.a.k$f r1 = r1.f1356k
                long r2 = com.baidu.location.p013a.C0742k.f1346j
                r10.postDelayed(r1, r2)
                com.baidu.location.a.k r10 = com.baidu.location.p013a.C0742k.this
                boolean unused = r10.f1357l = r0
            L_0x0162:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0742k.C0744a.handleMessage(android.os.Message):void");
        }
    }

    /* renamed from: com.baidu.location.a.k$b */
    private class C0745b {

        /* renamed from: b */
        private String f1360b = null;

        /* renamed from: c */
        private String f1361c = null;

        /* renamed from: d */
        private long f1362d = 0;

        C0745b(String str) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has(AuthActivity.ACTION_KEY)) {
                    this.f1360b = jSONObject.getString(AuthActivity.ACTION_KEY);
                }
                if (jSONObject.has("callback")) {
                    this.f1361c = jSONObject.getString("callback");
                }
                if (jSONObject.has("timeout")) {
                    long j = jSONObject.getLong("timeout");
                    if (j >= 1000) {
                        long unused = C0742k.f1346j = j;
                    }
                }
                this.f1362d = System.currentTimeMillis();
            } catch (Exception unused2) {
                this.f1360b = null;
                this.f1361c = null;
            }
        }

        /* renamed from: a */
        public String mo10471a() {
            return this.f1360b;
        }

        /* renamed from: b */
        public String mo10472b() {
            return this.f1361c;
        }
    }

    /* renamed from: com.baidu.location.a.k$c */
    private static final class C0746c {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C0742k f1363a = new C0742k();
    }

    /* renamed from: com.baidu.location.a.k$d */
    private class C0747d {
        private C0747d() {
        }

        @JavascriptInterface
        public void sendMessage(String str) {
            if (str != null && C0742k.this.f1353g) {
                C0745b bVar = new C0745b(str);
                if (bVar.mo10471a() != null && bVar.mo10471a().equals("requestLoc") && C0742k.this.f1351e != null) {
                    Message obtainMessage = C0742k.this.f1351e.obtainMessage(1);
                    obtainMessage.obj = bVar;
                    obtainMessage.sendToTarget();
                }
            }
        }

        @JavascriptInterface
        public void showLog(String str) {
        }
    }

    /* renamed from: com.baidu.location.a.k$e */
    public class C0748e extends BDAbstractLocationListener {
        public C0748e() {
        }

        public void onReceiveLocation(BDLocation bDLocation) {
            Message message;
            String str;
            if (C0742k.this.f1353g && bDLocation != null) {
                BDLocation bDLocation2 = new BDLocation(bDLocation);
                int locType = bDLocation2.getLocType();
                String coorType = bDLocation2.getCoorType();
                if (locType == 61 || locType == 161 || locType == 66) {
                    if (coorType != null) {
                        if (coorType.equals(CoordinateType.GCJ02)) {
                            bDLocation2 = LocationClient.getBDLocationInCoorType(bDLocation2, "gcj2wgs");
                        } else {
                            if (coorType.equals(BDLocation.BDLOCATION_GCJ02_TO_BD09)) {
                                str = BDLocation.BDLOCATION_BD09_TO_GCJ02;
                            } else if (coorType.equals("bd09ll")) {
                                str = BDLocation.BDLOCATION_BD09LL_TO_GCJ02;
                            }
                            bDLocation2 = LocationClient.getBDLocationInCoorType(LocationClient.getBDLocationInCoorType(bDLocation2, str), "gcj2wgs");
                        }
                    }
                    long unused = C0742k.this.f1354h = System.currentTimeMillis();
                    BDLocation unused2 = C0742k.this.f1355i = new BDLocation(bDLocation2);
                    message = C0742k.this.f1351e.obtainMessage(2);
                    message.obj = bDLocation2;
                } else {
                    message = C0742k.this.f1351e.obtainMessage(5);
                }
                message.sendToTarget();
            }
        }
    }

    /* renamed from: com.baidu.location.a.k$f */
    private class C0749f implements Runnable {
        private C0749f() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.k.a(com.baidu.location.a.k, boolean):boolean
         arg types: [com.baidu.location.a.k, int]
         candidates:
          com.baidu.location.a.k.a(com.baidu.location.a.k, long):long
          com.baidu.location.a.k.a(com.baidu.location.a.k, com.baidu.location.BDLocation):com.baidu.location.BDLocation
          com.baidu.location.a.k.a(com.baidu.location.a.k, com.baidu.location.a.k$f):com.baidu.location.a.k$f
          com.baidu.location.a.k.a(com.baidu.location.a.k, java.util.List):java.util.List
          com.baidu.location.a.k.a(com.baidu.location.a.k, boolean):boolean */
        public void run() {
            boolean unused = C0742k.this.f1357l = false;
            C0742k.this.f1351e.obtainMessage(6).sendToTarget();
        }
    }

    private C0742k() {
        this.f1348b = null;
        this.f1350d = null;
        this.f1347a = new C0748e();
        this.f1351e = null;
        this.f1352f = null;
        this.f1353g = false;
        this.f1354h = 0;
        this.f1355i = null;
        this.f1356k = null;
        this.f1357l = false;
    }

    /* renamed from: a */
    public static C0742k m1853a() {
        return C0746c.f1363a;
    }

    /* renamed from: a */
    private void m1855a(WebView webView) {
        webView.addJavascriptInterface(new C0747d(), "BaiduLocAssistant");
    }

    /* renamed from: a */
    public void mo10468a(Context context, WebView webView, LocationClient locationClient) {
        if (!this.f1353g && Integer.valueOf(Build.VERSION.SDK_INT).intValue() >= 17) {
            this.f1348b = context;
            this.f1349c = webView;
            this.f1350d = locationClient;
            this.f1351e = new C0744a(Looper.getMainLooper());
            this.f1351e.obtainMessage(3).sendToTarget();
            webView.getSettings().setJavaScriptEnabled(true);
            m1855a(this.f1349c);
            this.f1353g = true;
        }
    }

    /* renamed from: b */
    public void mo10469b() {
        if (this.f1353g) {
            this.f1351e.obtainMessage(4).sendToTarget();
            this.f1353g = false;
        }
    }
}
