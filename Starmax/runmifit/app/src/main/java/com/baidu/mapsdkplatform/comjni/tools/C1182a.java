package com.baidu.mapsdkplatform.comjni.tools;

import android.os.Bundle;
import com.baidu.mapapi.model.inner.C0955a;
import com.baidu.mapapi.model.inner.Point;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;

/* renamed from: com.baidu.mapsdkplatform.comjni.tools.a */
public class C1182a {
    /* renamed from: a */
    public static double m4373a(Point point, Point point2) {
        Bundle bundle = new Bundle();
        bundle.putDouble("x1", (double) point.f2890x);
        bundle.putDouble("y1", (double) point.f2891y);
        bundle.putDouble("x2", (double) point2.f2890x);
        bundle.putDouble("y2", (double) point2.f2891y);
        JNITools.GetDistanceByMC(bundle);
        return bundle.getDouble("distance");
    }

    /* renamed from: a */
    public static C0955a m4374a(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("strkey", str);
        JNITools.TransGeoStr2ComplexPt(bundle);
        C0955a aVar = new C0955a();
        Bundle bundle2 = bundle.getBundle("map_bound");
        if (bundle2 != null) {
            Bundle bundle3 = bundle2.getBundle("ll");
            if (bundle3 != null) {
                aVar.f2893b = new Point((int) bundle3.getDouble("ptx"), (int) bundle3.getDouble("pty"));
            }
            Bundle bundle4 = bundle2.getBundle("ru");
            if (bundle4 != null) {
                aVar.f2894c = new Point((int) bundle4.getDouble("ptx"), (int) bundle4.getDouble("pty"));
            }
        }
        for (ParcelItem parcelItem : (ParcelItem[]) bundle.getParcelableArray("poly_line")) {
            if (aVar.f2895d == null) {
                aVar.f2895d = new ArrayList<>();
            }
            Bundle bundle5 = parcelItem.getBundle();
            if (bundle5 != null) {
                ParcelItem[] parcelItemArr = (ParcelItem[]) bundle5.getParcelableArray("point_array");
                ArrayList arrayList = new ArrayList();
                for (ParcelItem parcelItem2 : parcelItemArr) {
                    Bundle bundle6 = parcelItem2.getBundle();
                    if (bundle6 != null) {
                        arrayList.add(new Point((int) bundle6.getDouble("ptx"), (int) bundle6.getDouble("pty")));
                    }
                }
                arrayList.trimToSize();
                aVar.f2895d.add(arrayList);
            }
        }
        aVar.f2895d.trimToSize();
        aVar.f2892a = (int) bundle.getDouble(SocialConstants.PARAM_TYPE);
        return aVar;
    }

    /* renamed from: a */
    public static String m4375a() {
        return JNITools.GetToken();
    }

    /* renamed from: a */
    public static void m4376a(boolean z, int i) {
        JNITools.openLogEnable(z, i);
    }

    /* renamed from: b */
    public static void m4377b() {
        JNITools.initClass(new Bundle(), 0);
    }
}
