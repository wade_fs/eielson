package com.baidu.mapapi.utils;

import android.content.Context;

/* renamed from: com.baidu.mapapi.utils.f */
final class C1016f implements Runnable {

    /* renamed from: a */
    final /* synthetic */ Context f3261a;

    /* renamed from: b */
    final /* synthetic */ int f3262b;

    C1016f(Context context, int i) {
        this.f3261a = context;
        this.f3262b = i;
    }

    public void run() {
        long currentTimeMillis = System.currentTimeMillis();
        do {
            if (System.currentTimeMillis() - currentTimeMillis > 3000) {
                C1012b.m3270a(this.f3261a);
                C1012b.m3269a(this.f3262b, this.f3261a);
            }
        } while (!C1012b.f3258v.isInterrupted());
    }
}
