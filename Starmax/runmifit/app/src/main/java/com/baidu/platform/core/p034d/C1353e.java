package com.baidu.platform.core.p034d;

import com.baidu.mapapi.search.route.BikingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.IndoorRoutePlanOption;
import com.baidu.mapapi.search.route.MassTransitRoutePlanOption;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.TransitRoutePlanOption;
import com.baidu.mapapi.search.route.WalkingRoutePlanOption;

/* renamed from: com.baidu.platform.core.d.e */
public interface C1353e {
    /* renamed from: a */
    void mo14057a();

    /* renamed from: a */
    void mo14058a(OnGetRoutePlanResultListener onGetRoutePlanResultListener);

    /* renamed from: a */
    boolean mo14059a(BikingRoutePlanOption bikingRoutePlanOption);

    /* renamed from: a */
    boolean mo14060a(DrivingRoutePlanOption drivingRoutePlanOption);

    /* renamed from: a */
    boolean mo14061a(IndoorRoutePlanOption indoorRoutePlanOption);

    /* renamed from: a */
    boolean mo14062a(MassTransitRoutePlanOption massTransitRoutePlanOption);

    /* renamed from: a */
    boolean mo14063a(TransitRoutePlanOption transitRoutePlanOption);

    /* renamed from: a */
    boolean mo14064a(WalkingRoutePlanOption walkingRoutePlanOption);
}
