package com.baidu.mapsdkplatform.comapi.map;

import android.os.Handler;
import com.baidu.mapsdkplatform.comjni.engine.C1176a;

public class MessageCenter {
    public static void registMessage(int i, Handler handler) {
        C1176a.m4288a(i, handler);
    }

    public static void unregistMessage(int i, Handler handler) {
        C1176a.m4289b(i, handler);
    }
}
