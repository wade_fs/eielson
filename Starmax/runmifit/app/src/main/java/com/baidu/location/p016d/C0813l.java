package com.baidu.location.p016d;

import android.database.sqlite.SQLiteDatabase;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;

/* renamed from: com.baidu.location.d.l */
final class C0813l {
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static final double[] f1700b = {45.0d, 135.0d, 225.0d, 315.0d};

    /* renamed from: a */
    private final C0804h f1701a;

    /* renamed from: c */
    private final int f1702c;

    /* renamed from: d */
    private final SQLiteDatabase f1703d;

    /* renamed from: e */
    private int f1704e = -1;

    /* renamed from: f */
    private int f1705f = -1;

    /* renamed from: com.baidu.location.d.l$a */
    private static final class C0815a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public double f1706a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public double f1707b;

        private C0815a(double d, double d2) {
            this.f1706a = d;
            this.f1707b = d2;
        }
    }

    /* renamed from: com.baidu.location.d.l$b */
    private enum C0816b {
        AREA("RGCAREA", "area", "addrv", 0, 1000),
        ROAD("RGCROAD", "road", "addrv", 1000, 10000),
        SITE("RGCSITE", "site", "addrv", 100, 50000),
        POI("RGCPOI", "poi", "poiv", 1000, 5000);
        
        /* access modifiers changed from: private */

        /* renamed from: e */
        public final int f1713e;

        /* renamed from: f */
        private final String f1714f;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public final String f1715g;
        /* access modifiers changed from: private */

        /* renamed from: h */
        public final String f1716h;
        /* access modifiers changed from: private */

        /* renamed from: i */
        public final int f1717i;

        private C0816b(String str, String str2, String str3, int i, int i2) {
            this.f1714f = str;
            this.f1715g = str2;
            this.f1716h = str3;
            this.f1713e = i;
            this.f1717i = i2;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public String m2250a(int i, double d, double d2) {
            HashSet hashSet = new HashSet();
            hashSet.add(C0813l.m2237b(i, d, d2));
            int i2 = this.f1713e;
            double d3 = (double) i2;
            Double.isNaN(d3);
            double d4 = d3 * 1.414d;
            if (i2 > 0) {
                for (int i3 = 0; i3 < C0813l.f1700b.length; i3++) {
                    double[] a = C0813l.m2238b(d2, d, d4, C0813l.f1700b[i3]);
                    hashSet.add(C0813l.m2237b(i, a[1], a[0]));
                }
            }
            StringBuffer stringBuffer = new StringBuffer();
            Iterator it = hashSet.iterator();
            boolean z = true;
            while (it.hasNext()) {
                String str = (String) it.next();
                if (z) {
                    z = false;
                } else {
                    stringBuffer.append(',');
                }
                stringBuffer.append("\"");
                stringBuffer.append(str);
                stringBuffer.append("\"");
            }
            return String.format("SELECT * FROM %s WHERE gridkey IN (%s);", this.f1714f, stringBuffer.toString());
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public String m2254a(JSONObject jSONObject) {
            Iterator<String> keys = jSONObject.keys();
            StringBuffer stringBuffer = new StringBuffer();
            while (keys.hasNext()) {
                String next = keys.next();
                if (stringBuffer.length() != 0) {
                    stringBuffer.append(",");
                }
                stringBuffer.append("\"");
                stringBuffer.append(next);
                stringBuffer.append("\"");
            }
            return String.format(Locale.US, "DELETE FROM %s WHERE gridkey IN (%s)", this.f1714f, stringBuffer);
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public static void m2257b(StringBuffer stringBuffer, String str, String str2, int i) {
            if (stringBuffer.length() > 0) {
                stringBuffer.append(",");
            }
            stringBuffer.append("(\"");
            stringBuffer.append(str);
            stringBuffer.append("\",\"");
            stringBuffer.append(str2);
            stringBuffer.append("\",");
            stringBuffer.append(i);
            stringBuffer.append(",");
            stringBuffer.append(System.currentTimeMillis() / 86400000);
            stringBuffer.append(")");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract List<String> mo10614a(JSONObject jSONObject, String str, int i);
    }

    C0813l(C0804h hVar, SQLiteDatabase sQLiteDatabase, int i) {
        this.f1701a = hVar;
        this.f1703d = sQLiteDatabase;
        this.f1702c = i;
        SQLiteDatabase sQLiteDatabase2 = this.f1703d;
        if (sQLiteDatabase2 != null && sQLiteDatabase2.isOpen()) {
            try {
                this.f1703d.execSQL("CREATE TABLE IF NOT EXISTS RGCAREA(gridkey VARCHAR(10) PRIMARY KEY, country VARCHAR(100),countrycode VARCHAR(100), province VARCHAR(100), city VARCHAR(100), citycode VARCHAR(100), district VARCHAR(100), timestamp INTEGER, version VARCHAR(50))");
                this.f1703d.execSQL("CREATE TABLE IF NOT EXISTS RGCROAD(_id INTEGER PRIMARY KEY AUTOINCREMENT, gridkey VARCHAR(10), street VARCHAR(100), x1 DOUBLE, y1 DOUBLE, x2 DOUBLE, y2 DOUBLE)");
                this.f1703d.execSQL("CREATE TABLE IF NOT EXISTS RGCSITE(_id INTEGER PRIMARY KEY AUTOINCREMENT, gridkey VARCHAR(10), street VARCHAR(100), streetnumber VARCHAR(100), x DOUBLE, y DOUBLE)");
                this.f1703d.execSQL("CREATE TABLE IF NOT EXISTS RGCPOI(pid VARCHAR(50) PRIMARY KEY , gridkey VARCHAR(10), name VARCHAR(100), type VARCHAR(50), x DOUBLE, y DOUBLE, rank INTEGER)");
                this.f1703d.execSQL("CREATE TABLE IF NOT EXISTS RGCUPDATE(gridkey VARCHAR(10), version VARCHAR(50), type INTEGER, timestamp INTEGER, PRIMARY KEY(gridkey, type))");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    private double m2233a(double d, double d2, double d3, double d4, double d5, double d6) {
        double d7 = d5 - d3;
        double d8 = d - d3;
        double d9 = d6 - d4;
        double d10 = d2 - d4;
        double d11 = (d7 * d8) + (d9 * d10);
        if (d11 <= 0.0d) {
            return Math.sqrt((d8 * d8) + (d10 * d10));
        }
        double d12 = (d7 * d7) + (d9 * d9);
        if (d11 >= d12) {
            double d13 = d - d5;
            double d14 = d2 - d6;
            return Math.sqrt((d13 * d13) + (d14 * d14));
        }
        double d15 = d11 / d12;
        double d16 = d - (d3 + (d7 * d15));
        double d17 = (d4 + (d9 * d15)) - d2;
        return Math.sqrt((d16 * d16) + (d17 * d17));
    }

    /* renamed from: a */
    private static int m2234a(int i, int i2) {
        int i3;
        double d;
        if (100 > i2) {
            d = -0.1d;
            i3 = 60000;
        } else if (500 > i2) {
            d = -0.75d;
            i3 = 55500;
        } else {
            d = -0.5d;
            i3 = 0;
        }
        double d2 = (double) i2;
        Double.isNaN(d2);
        double d3 = (double) i3;
        Double.isNaN(d3);
        return ((int) ((d * d2) + d3)) + i;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static String m2237b(int i, double d, double d2) {
        double d3;
        C0815a aVar;
        int i2 = i;
        int i3 = i2 * 5;
        char[] cArr = new char[(i2 + 1)];
        C0815a aVar2 = new C0815a(90.0d, -90.0d);
        C0815a aVar3 = new C0815a(180.0d, -180.0d);
        int i4 = 1;
        boolean z = true;
        int i5 = 0;
        for (int i6 = 1; i6 <= i3; i6++) {
            if (z) {
                d3 = d;
                aVar = aVar3;
            } else {
                d3 = d2;
                aVar = aVar2;
            }
            double a = (aVar.f1707b + aVar.f1706a) / 2.0d;
            i5 <<= i4;
            if (((int) (d3 * 1000000.0d)) > ((int) (a * 1000000.0d))) {
                double unused = aVar.f1707b = a;
                i5 |= 1;
            } else {
                double unused2 = aVar.f1706a = a;
            }
            if (i6 % 5 == 0) {
                i4 = 1;
                cArr[(i6 / 5) - 1] = "0123456789bcdefghjkmnpqrstuvwxyz".charAt(i5);
                i5 = 0;
            } else {
                i4 = 1;
            }
            z = !z;
        }
        cArr[i2] = 0;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i7 = 0; i7 < i2; i7++) {
            stringBuffer.append(cArr[i7]);
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static double[] m2238b(double d, double d2, double d3, double d4) {
        double radians = Math.toRadians(d);
        double radians2 = Math.toRadians(d2);
        double radians3 = Math.toRadians(d4);
        double d5 = d3 / 6378137.0d;
        double asin = Math.asin((Math.sin(radians) * Math.cos(d5)) + (Math.cos(radians) * Math.sin(d5) * Math.cos(radians3)));
        return new double[]{Math.toDegrees(asin), Math.toDegrees(radians2 + Math.atan2(Math.sin(radians3) * Math.sin(d5) * Math.cos(radians), Math.cos(d5) - (Math.sin(radians) * Math.sin(asin))))};
    }

    /* renamed from: c */
    private double m2239c(double d, double d2, double d3, double d4) {
        double d5 = d4 - d2;
        double d6 = d3 - d;
        double radians = Math.toRadians(d);
        Math.toRadians(d2);
        double radians2 = Math.toRadians(d3);
        Math.toRadians(d4);
        double radians3 = Math.toRadians(d5);
        double radians4 = Math.toRadians(d6) / 2.0d;
        double d7 = radians3 / 2.0d;
        double sin = (Math.sin(radians4) * Math.sin(radians4)) + (Math.cos(radians) * Math.cos(radians2) * Math.sin(d7) * Math.sin(d7));
        return Math.atan2(Math.sqrt(sin), Math.sqrt(1.0d - sin)) * 2.0d * 6378137.0d;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:148:0x01e9 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:114:? */
    /* JADX INFO: additional move instructions added (4) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:124:0x01c4 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:86:0x0155 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:89:? */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v1, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v3, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v4, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v2, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v3, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v4, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v5, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v10, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v21, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v12, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v11, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v14, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v13, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v12, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v11, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v27, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v16, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v18, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v15, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v17, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v14, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v16, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v13, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v15, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v11, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v8, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v9, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v10, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v12, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v13, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v16, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v17, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v19, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v10, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v11, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v12, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v20, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v21, resolved type: java.lang.String[]} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x01b7, code lost:
        r4 = r15;
        r11 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x01b9, code lost:
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x01bb, code lost:
        r2 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x01bd, code lost:
        r1 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x01cc, code lost:
        r0 = th;
        r11 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x01eb, code lost:
        r15 = new java.lang.String(com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r15.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x01fb, code lost:
        r0 = new java.lang.String(com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r1.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0209, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x020c, code lost:
        r1 = new java.lang.String(com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r2.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x021a, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x021d, code lost:
        r2 = new java.lang.String(com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r3.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x022b, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x022e, code lost:
        r3 = new java.lang.String(com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r4.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x023c, code lost:
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x023f, code lost:
        r4 = new java.lang.String(com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r5.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x024d, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0250, code lost:
        r5 = new java.lang.String(com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r21.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x025e, code lost:
        r5 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x0262, code lost:
        r6 = new java.lang.String(com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r18.getBytes()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0270, code lost:
        r6 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0072, code lost:
        r26 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007b, code lost:
        if (r26 != null) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r26.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0083, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0084, code lost:
        r26 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r26.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009d, code lost:
        if (r26 != null) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0133, code lost:
        r24 = r8;
        r15 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x013a, code lost:
        if (r24 != null) goto L_0x013c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r24.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0140, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0141, code lost:
        r24 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r24.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0155, code lost:
        if (r24 != null) goto L_0x013c;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x01c6 A[SYNTHETIC, Splitter:B:125:0x01c6] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x01cc A[ExcHandler: all (th java.lang.Throwable), Splitter:B:92:0x016b] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x01d8 A[SYNTHETIC, Splitter:B:137:0x01d8] */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x01e4 A[SYNTHETIC, Splitter:B:144:0x01e4] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x01eb  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x01fb  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0209  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x020c  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x021a  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x021d  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x022b  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x023c  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x023f  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0262  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0270  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0083 A[ExcHandler: all (th java.lang.Throwable), PHI: r8 10  PHI: (r8v7 android.database.Cursor) = (r8v6 android.database.Cursor), (r8v8 android.database.Cursor) binds: [B:3:0x0020, B:7:0x002c] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0020] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0093 A[SYNTHETIC, Splitter:B:37:0x0093] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0140 A[ExcHandler: all (th java.lang.Throwable), PHI: r8 10  PHI: (r8v1 android.database.Cursor) = (r8v0 android.database.Cursor), (r8v3 android.database.Cursor), (r8v3 android.database.Cursor) binds: [B:47:0x00b4, B:54:0x00d0, B:55:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:47:0x00b4] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x014e A[SYNTHETIC, Splitter:B:80:0x014e] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.baidu.location.Address mo10609a(double r35, double r37) {
        /*
            r34 = this;
            r14 = r34
            java.lang.String r0 = "wgs842mc"
            r15 = 4
            r12 = 5
            r13 = 3
            r10 = 2
            r16 = 9218868437227405311(0x7fefffffffffffff, double:1.7976931348623157E308)
            r11 = 0
            com.baidu.location.d.l$b r1 = com.baidu.location.p016d.C0813l.C0816b.SITE     // Catch:{ Exception -> 0x0097, all -> 0x008e }
            int r2 = r14.f1702c     // Catch:{ Exception -> 0x0097, all -> 0x008e }
            r3 = r35
            r5 = r37
            java.lang.String r1 = r1.m2250a(r2, r3, r5)     // Catch:{ Exception -> 0x0097, all -> 0x008e }
            android.database.sqlite.SQLiteDatabase r2 = r14.f1703d     // Catch:{ Exception -> 0x0097, all -> 0x008e }
            android.database.Cursor r8 = r2.rawQuery(r1, r11)     // Catch:{ Exception -> 0x0097, all -> 0x008e }
            boolean r1 = r8.moveToFirst()     // Catch:{ Exception -> 0x0087, all -> 0x0083 }
            if (r1 == 0) goto L_0x0075
            r18 = r11
            r21 = r18
            r19 = r16
        L_0x002c:
            boolean r1 = r8.isAfterLast()     // Catch:{ Exception -> 0x0072, all -> 0x0083 }
            if (r1 != 0) goto L_0x006f
            java.lang.String r22 = r8.getString(r10)     // Catch:{ Exception -> 0x0072, all -> 0x0083 }
            java.lang.String r23 = r8.getString(r13)     // Catch:{ Exception -> 0x0072, all -> 0x0083 }
            double r6 = r8.getDouble(r12)     // Catch:{ Exception -> 0x0072, all -> 0x0083 }
            double r24 = r8.getDouble(r15)     // Catch:{ Exception -> 0x0072, all -> 0x0083 }
            r1 = r34
            r2 = r37
            r4 = r35
            r26 = r8
            r8 = r24
            double r1 = r1.m2239c(r2, r4, r6, r8)     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            int r3 = (r1 > r19 ? 1 : (r1 == r19 ? 0 : -1))
            if (r3 >= 0) goto L_0x0065
            com.baidu.location.d.l$b r3 = com.baidu.location.p016d.C0813l.C0816b.SITE     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            int r3 = r3.f1713e     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            double r3 = (double) r3     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 > 0) goto L_0x0065
            r19 = r1
            r21 = r22
            r18 = r23
        L_0x0065:
            r26.moveToNext()     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            r8 = r26
            goto L_0x002c
        L_0x006b:
            r0 = move-exception
            goto L_0x0091
        L_0x006d:
            goto L_0x009d
        L_0x006f:
            r26 = r8
            goto L_0x007b
        L_0x0072:
            r26 = r8
            goto L_0x009d
        L_0x0075:
            r26 = r8
            r18 = r11
            r21 = r18
        L_0x007b:
            if (r26 == 0) goto L_0x00a0
        L_0x007d:
            r26.close()     // Catch:{ Exception -> 0x0081 }
            goto L_0x00a0
        L_0x0081:
            goto L_0x00a0
        L_0x0083:
            r0 = move-exception
            r26 = r8
            goto L_0x0091
        L_0x0087:
            r26 = r8
            r18 = r11
            r21 = r18
            goto L_0x009d
        L_0x008e:
            r0 = move-exception
            r26 = r11
        L_0x0091:
            if (r26 == 0) goto L_0x0096
            r26.close()     // Catch:{ Exception -> 0x0096 }
        L_0x0096:
            throw r0
        L_0x0097:
            r18 = r11
            r21 = r18
            r26 = r21
        L_0x009d:
            if (r26 == 0) goto L_0x00a0
            goto L_0x007d
        L_0x00a0:
            if (r18 != 0) goto L_0x0158
            com.baidu.location.d.l$b r1 = com.baidu.location.p016d.C0813l.C0816b.ROAD     // Catch:{ Exception -> 0x0152, all -> 0x0148 }
            int r2 = r14.f1702c     // Catch:{ Exception -> 0x0152, all -> 0x0148 }
            r3 = r35
            r5 = r37
            java.lang.String r1 = r1.m2250a(r2, r3, r5)     // Catch:{ Exception -> 0x0152, all -> 0x0148 }
            android.database.sqlite.SQLiteDatabase r2 = r14.f1703d     // Catch:{ Exception -> 0x0152, all -> 0x0148 }
            android.database.Cursor r8 = r2.rawQuery(r1, r11)     // Catch:{ Exception -> 0x0152, all -> 0x0148 }
            boolean r1 = r8.moveToFirst()     // Catch:{ Exception -> 0x0144, all -> 0x0140 }
            if (r1 == 0) goto L_0x0137
            r6 = r35
            r4 = r37
            double[] r19 = com.baidu.location.Jni.coorEncrypt(r6, r4, r0)     // Catch:{ Exception -> 0x0144, all -> 0x0140 }
        L_0x00c2:
            boolean r1 = r8.isAfterLast()     // Catch:{ Exception -> 0x0144, all -> 0x0140 }
            if (r1 != 0) goto L_0x0137
            java.lang.String r20 = r8.getString(r10)     // Catch:{ Exception -> 0x0144, all -> 0x0140 }
            double r1 = r8.getDouble(r13)     // Catch:{ Exception -> 0x0144, all -> 0x0140 }
            double r10 = r8.getDouble(r15)     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            double[] r1 = com.baidu.location.Jni.coorEncrypt(r1, r10, r0)     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            double r2 = r8.getDouble(r12)     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            r9 = 6
            double r9 = r8.getDouble(r9)     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            double[] r2 = com.baidu.location.Jni.coorEncrypt(r2, r9, r0)     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            r3 = 0
            r9 = r19[r3]     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            r11 = 1
            r24 = r19[r11]     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            r26 = r1[r3]     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            r28 = r1[r11]     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            r30 = r2[r3]     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            r32 = r2[r11]     // Catch:{ Exception -> 0x0133, all -> 0x0140 }
            r1 = r34
            r2 = r9
            r4 = r24
            r6 = r26
            r24 = r8
            r8 = r28
            r15 = 0
            r22 = 2
            r10 = r30
            r25 = 5
            r26 = 3
            r12 = r32
            double r1 = r1.m2233a(r2, r4, r6, r8, r10, r12)     // Catch:{ Exception -> 0x0131, all -> 0x012f }
            int r3 = (r1 > r16 ? 1 : (r1 == r16 ? 0 : -1))
            if (r3 >= 0) goto L_0x0120
            com.baidu.location.d.l$b r3 = com.baidu.location.p016d.C0813l.C0816b.ROAD     // Catch:{ Exception -> 0x0131, all -> 0x012f }
            int r3 = r3.f1713e     // Catch:{ Exception -> 0x0131, all -> 0x012f }
            double r3 = (double) r3     // Catch:{ Exception -> 0x0131, all -> 0x012f }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 > 0) goto L_0x0120
            r16 = r1
            r21 = r20
        L_0x0120:
            r24.moveToNext()     // Catch:{ Exception -> 0x0131, all -> 0x012f }
            r6 = r35
            r4 = r37
            r11 = r15
            r8 = r24
            r10 = 2
            r12 = 5
            r13 = 3
            r15 = 4
            goto L_0x00c2
        L_0x012f:
            r0 = move-exception
            goto L_0x014c
        L_0x0131:
            goto L_0x0155
        L_0x0133:
            r24 = r8
            r15 = 0
            goto L_0x0155
        L_0x0137:
            r24 = r8
            r15 = r11
            if (r24 == 0) goto L_0x0159
        L_0x013c:
            r24.close()     // Catch:{ Exception -> 0x0159 }
            goto L_0x0159
        L_0x0140:
            r0 = move-exception
            r24 = r8
            goto L_0x014c
        L_0x0144:
            r24 = r8
            r15 = r11
            goto L_0x0155
        L_0x0148:
            r0 = move-exception
            r15 = r11
            r24 = r15
        L_0x014c:
            if (r24 == 0) goto L_0x0151
            r24.close()     // Catch:{ Exception -> 0x0151 }
        L_0x0151:
            throw r0
        L_0x0152:
            r15 = r11
            r24 = r15
        L_0x0155:
            if (r24 == 0) goto L_0x0159
            goto L_0x013c
        L_0x0158:
            r15 = r11
        L_0x0159:
            com.baidu.location.d.l$b r2 = com.baidu.location.p016d.C0813l.C0816b.AREA
            int r3 = r14.f1702c
            r4 = r35
            r6 = r37
            java.lang.String r0 = r2.m2250a(r3, r4, r6)
            android.database.sqlite.SQLiteDatabase r1 = r14.f1703d     // Catch:{ Exception -> 0x01dc, all -> 0x01d4 }
            android.database.Cursor r11 = r1.rawQuery(r0, r15)     // Catch:{ Exception -> 0x01dc, all -> 0x01d4 }
            boolean r0 = r11.moveToFirst()     // Catch:{ Exception -> 0x01ce, all -> 0x01cc }
            if (r0 == 0) goto L_0x01bf
            boolean r0 = r11.isAfterLast()     // Catch:{ Exception -> 0x01ce, all -> 0x01cc }
            if (r0 != 0) goto L_0x01bf
            java.lang.String r0 = "country"
            int r0 = r11.getColumnIndex(r0)     // Catch:{ Exception -> 0x01ce, all -> 0x01cc }
            java.lang.String r0 = r11.getString(r0)     // Catch:{ Exception -> 0x01ce, all -> 0x01cc }
            java.lang.String r1 = "countrycode"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ Exception -> 0x01bd, all -> 0x01cc }
            java.lang.String r1 = r11.getString(r1)     // Catch:{ Exception -> 0x01bd, all -> 0x01cc }
            java.lang.String r2 = "province"
            int r2 = r11.getColumnIndex(r2)     // Catch:{ Exception -> 0x01bb, all -> 0x01cc }
            java.lang.String r2 = r11.getString(r2)     // Catch:{ Exception -> 0x01bb, all -> 0x01cc }
            java.lang.String r3 = "city"
            int r3 = r11.getColumnIndex(r3)     // Catch:{ Exception -> 0x01b9, all -> 0x01cc }
            java.lang.String r3 = r11.getString(r3)     // Catch:{ Exception -> 0x01b9, all -> 0x01cc }
            java.lang.String r4 = "citycode"
            int r4 = r11.getColumnIndex(r4)     // Catch:{ Exception -> 0x01b7, all -> 0x01cc }
            java.lang.String r4 = r11.getString(r4)     // Catch:{ Exception -> 0x01b7, all -> 0x01cc }
            java.lang.String r5 = "district"
            int r5 = r11.getColumnIndex(r5)     // Catch:{ Exception -> 0x01b5, all -> 0x01cc }
            java.lang.String r5 = r11.getString(r5)     // Catch:{ Exception -> 0x01b5, all -> 0x01cc }
            r15 = r0
            goto L_0x01c4
        L_0x01b5:
            goto L_0x01e2
        L_0x01b7:
            r4 = r15
            goto L_0x01e2
        L_0x01b9:
            r3 = r15
            goto L_0x01d2
        L_0x01bb:
            r2 = r15
            goto L_0x01d1
        L_0x01bd:
            r1 = r15
            goto L_0x01d0
        L_0x01bf:
            r1 = r15
            r2 = r1
            r3 = r2
            r4 = r3
            r5 = r4
        L_0x01c4:
            if (r11 == 0) goto L_0x01e9
            r11.close()     // Catch:{ Exception -> 0x01ca }
            goto L_0x01e9
        L_0x01ca:
            goto L_0x01e9
        L_0x01cc:
            r0 = move-exception
            goto L_0x01d6
        L_0x01ce:
            r0 = r15
            r1 = r0
        L_0x01d0:
            r2 = r1
        L_0x01d1:
            r3 = r2
        L_0x01d2:
            r4 = r3
            goto L_0x01e2
        L_0x01d4:
            r0 = move-exception
            r11 = r15
        L_0x01d6:
            if (r11 == 0) goto L_0x01db
            r11.close()     // Catch:{ Exception -> 0x01db }
        L_0x01db:
            throw r0
        L_0x01dc:
            r0 = r15
            r1 = r0
            r2 = r1
            r3 = r2
            r4 = r3
            r11 = r4
        L_0x01e2:
            if (r11 == 0) goto L_0x01e7
            r11.close()     // Catch:{ Exception -> 0x01e7 }
        L_0x01e7:
            r5 = r15
            r15 = r0
        L_0x01e9:
            if (r15 == 0) goto L_0x01f9
            java.lang.String r0 = new java.lang.String
            byte[] r6 = r15.getBytes()
            byte[] r6 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r6)
            r0.<init>(r6)
            r15 = r0
        L_0x01f9:
            if (r1 == 0) goto L_0x0209
            java.lang.String r0 = new java.lang.String
            byte[] r1 = r1.getBytes()
            byte[] r1 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r1)
            r0.<init>(r1)
            goto L_0x020a
        L_0x0209:
            r0 = r1
        L_0x020a:
            if (r2 == 0) goto L_0x021a
            java.lang.String r1 = new java.lang.String
            byte[] r2 = r2.getBytes()
            byte[] r2 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r2)
            r1.<init>(r2)
            goto L_0x021b
        L_0x021a:
            r1 = r2
        L_0x021b:
            if (r3 == 0) goto L_0x022b
            java.lang.String r2 = new java.lang.String
            byte[] r3 = r3.getBytes()
            byte[] r3 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r3)
            r2.<init>(r3)
            goto L_0x022c
        L_0x022b:
            r2 = r3
        L_0x022c:
            if (r4 == 0) goto L_0x023c
            java.lang.String r3 = new java.lang.String
            byte[] r4 = r4.getBytes()
            byte[] r4 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r4)
            r3.<init>(r4)
            goto L_0x023d
        L_0x023c:
            r3 = r4
        L_0x023d:
            if (r5 == 0) goto L_0x024d
            java.lang.String r4 = new java.lang.String
            byte[] r5 = r5.getBytes()
            byte[] r5 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r5)
            r4.<init>(r5)
            goto L_0x024e
        L_0x024d:
            r4 = r5
        L_0x024e:
            if (r21 == 0) goto L_0x025e
            java.lang.String r5 = new java.lang.String
            byte[] r6 = r21.getBytes()
            byte[] r6 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r6)
            r5.<init>(r6)
            goto L_0x0260
        L_0x025e:
            r5 = r21
        L_0x0260:
            if (r18 == 0) goto L_0x0270
            java.lang.String r6 = new java.lang.String
            byte[] r7 = r18.getBytes()
            byte[] r7 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r7)
            r6.<init>(r7)
            goto L_0x0272
        L_0x0270:
            r6 = r18
        L_0x0272:
            com.baidu.location.Address$Builder r7 = new com.baidu.location.Address$Builder
            r7.<init>()
            com.baidu.location.Address$Builder r7 = r7.country(r15)
            com.baidu.location.Address$Builder r0 = r7.countryCode(r0)
            com.baidu.location.Address$Builder r0 = r0.province(r1)
            com.baidu.location.Address$Builder r0 = r0.city(r2)
            com.baidu.location.Address$Builder r0 = r0.cityCode(r3)
            com.baidu.location.Address$Builder r0 = r0.district(r4)
            com.baidu.location.Address$Builder r0 = r0.street(r5)
            com.baidu.location.Address$Builder r0 = r0.streetNumber(r6)
            com.baidu.location.Address r0 = r0.build()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0813l.mo10609a(double, double):com.baidu.location.Address");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0079 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10610a(org.json.JSONObject r9) {
        /*
            r8 = this;
            android.database.sqlite.SQLiteDatabase r0 = r8.f1703d
            if (r0 == 0) goto L_0x0086
            boolean r0 = r0.isOpen()
            if (r0 == 0) goto L_0x0086
            android.database.sqlite.SQLiteDatabase r0 = r8.f1703d     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r0.beginTransaction()     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            com.baidu.location.d.l$b[] r0 = com.baidu.location.p016d.C0813l.C0816b.values()     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            int r1 = r0.length     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r2 = 0
        L_0x0015:
            if (r2 >= r1) goto L_0x006f
            r3 = r0[r2]     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            java.lang.String r4 = r3.f1715g     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            boolean r4 = r9.has(r4)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            if (r4 == 0) goto L_0x006c
            java.lang.String r4 = ""
            java.lang.String r5 = r3.f1716h     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            boolean r5 = r9.has(r5)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            if (r5 == 0) goto L_0x0037
            java.lang.String r4 = r3.f1716h     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            java.lang.String r4 = r9.getString(r4)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
        L_0x0037:
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r5.<init>()     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            java.lang.String r6 = r3.f1715g     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            org.json.JSONObject r6 = r9.getJSONObject(r6)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            java.lang.String r7 = r3.m2254a(r6)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r5.add(r7)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            int r7 = r3.f1717i     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            java.util.List r3 = r3.mo10614a(r6, r4, r7)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r5.addAll(r3)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            java.util.Iterator r3 = r5.iterator()     // Catch:{ Exception -> 0x0079, all -> 0x007f }
        L_0x005a:
            boolean r4 = r3.hasNext()     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            if (r4 == 0) goto L_0x006c
            java.lang.Object r4 = r3.next()     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            android.database.sqlite.SQLiteDatabase r5 = r8.f1703d     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r5.execSQL(r4)     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            goto L_0x005a
        L_0x006c:
            int r2 = r2 + 1
            goto L_0x0015
        L_0x006f:
            android.database.sqlite.SQLiteDatabase r9 = r8.f1703d     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r9.setTransactionSuccessful()     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r9 = -1
            r8.f1704e = r9     // Catch:{ Exception -> 0x0079, all -> 0x007f }
            r8.f1705f = r9     // Catch:{ Exception -> 0x0079, all -> 0x007f }
        L_0x0079:
            android.database.sqlite.SQLiteDatabase r9 = r8.f1703d     // Catch:{ Exception -> 0x0086 }
            r9.endTransaction()     // Catch:{ Exception -> 0x0086 }
            goto L_0x0086
        L_0x007f:
            r9 = move-exception
            android.database.sqlite.SQLiteDatabase r0 = r8.f1703d     // Catch:{ Exception -> 0x0085 }
            r0.endTransaction()     // Catch:{ Exception -> 0x0085 }
        L_0x0085:
            throw r9
        L_0x0086:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0813l.mo10610a(org.json.JSONObject):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        if (r0 == null) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x006d, code lost:
        if (r0 != null) goto L_0x004c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005a A[SYNTHETIC, Splitter:B:28:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0061 A[SYNTHETIC, Splitter:B:32:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0068 A[SYNTHETIC, Splitter:B:39:0x0068] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo10611a() {
        /*
            r5 = this;
            com.baidu.location.d.h r0 = r5.f1701a
            com.baidu.location.d.f r0 = r0.mo10601l()
            boolean r0 = r0.mo10582l()
            r1 = 0
            if (r0 == 0) goto L_0x0070
            int r0 = r5.f1705f
            r2 = -1
            if (r0 != r2) goto L_0x0070
            int r0 = r5.f1704e
            if (r0 != r2) goto L_0x0070
            android.database.sqlite.SQLiteDatabase r0 = r5.f1703d
            if (r0 == 0) goto L_0x0070
            boolean r0 = r0.isOpen()
            if (r0 == 0) goto L_0x0070
            r0 = 0
            android.database.sqlite.SQLiteDatabase r2 = r5.f1703d     // Catch:{ Exception -> 0x0065, all -> 0x0056 }
            java.lang.String r3 = "SELECT COUNT(*) FROM RGCSITE;"
            android.database.Cursor r2 = r2.rawQuery(r3, r0)     // Catch:{ Exception -> 0x0065, all -> 0x0056 }
            r2.moveToFirst()     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            int r3 = r2.getInt(r1)     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            r5.f1705f = r3     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            android.database.sqlite.SQLiteDatabase r3 = r5.f1703d     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            java.lang.String r4 = "SELECT COUNT(*) FROM RGCAREA;"
            android.database.Cursor r0 = r3.rawQuery(r4, r0)     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            r0.moveToFirst()     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            int r3 = r0.getInt(r1)     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            r5.f1704e = r3     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            if (r2 == 0) goto L_0x004a
            r2.close()     // Catch:{ Exception -> 0x0049 }
            goto L_0x004a
        L_0x0049:
        L_0x004a:
            if (r0 == 0) goto L_0x0070
        L_0x004c:
            r0.close()     // Catch:{ Exception -> 0x0050 }
            goto L_0x0070
        L_0x0050:
            goto L_0x0070
        L_0x0052:
            r1 = move-exception
            goto L_0x0058
        L_0x0054:
            goto L_0x0066
        L_0x0056:
            r1 = move-exception
            r2 = r0
        L_0x0058:
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ Exception -> 0x005e }
            goto L_0x005f
        L_0x005e:
        L_0x005f:
            if (r0 == 0) goto L_0x0064
            r0.close()     // Catch:{ Exception -> 0x0064 }
        L_0x0064:
            throw r1
        L_0x0065:
            r2 = r0
        L_0x0066:
            if (r2 == 0) goto L_0x006d
            r2.close()     // Catch:{ Exception -> 0x006c }
            goto L_0x006d
        L_0x006c:
        L_0x006d:
            if (r0 == 0) goto L_0x0070
            goto L_0x004c
        L_0x0070:
            int r0 = r5.f1705f
            if (r0 != 0) goto L_0x007a
            int r0 = r5.f1704e
            if (r0 != 0) goto L_0x007a
            r0 = 1
            return r0
        L_0x007a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0813l.mo10611a():boolean");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x008d, code lost:
        if (r11 != null) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0095, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a3, code lost:
        if (r11 == null) goto L_0x00a6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0095 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x001a] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009d A[SYNTHETIC, Splitter:B:29:0x009d] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.baidu.location.Poi> mo10612b(double r18, double r20) {
        /*
            r17 = this;
            r10 = r17
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.baidu.location.d.l$b r1 = com.baidu.location.p016d.C0813l.C0816b.POI
            int r2 = r10.f1702c
            r3 = r18
            r5 = r20
            java.lang.String r1 = r1.m2250a(r2, r3, r5)
            r2 = 0
            android.database.sqlite.SQLiteDatabase r3 = r10.f1703d     // Catch:{ Exception -> 0x00a1, all -> 0x0099 }
            android.database.Cursor r11 = r3.rawQuery(r1, r2)     // Catch:{ Exception -> 0x00a1, all -> 0x0099 }
            boolean r1 = r11.moveToFirst()     // Catch:{ Exception -> 0x0097, all -> 0x0095 }
            if (r1 == 0) goto L_0x008c
            r12 = 0
            r13 = r2
            r14 = 0
        L_0x0023:
            boolean r1 = r11.isAfterLast()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            if (r1 != 0) goto L_0x008d
            java.lang.String r15 = r11.getString(r12)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r1 = 2
            java.lang.String r16 = r11.getString(r1)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r1 = 4
            double r8 = r11.getDouble(r1)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r1 = 5
            double r6 = r11.getDouble(r1)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r1 = 6
            int r4 = r11.getInt(r1)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r1 = r17
            r2 = r20
            r12 = r4
            r4 = r18
            double r1 = r1.m2239c(r2, r4, r6, r8)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            com.baidu.location.d.l$b r3 = com.baidu.location.p016d.C0813l.C0816b.POI     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            int r3 = r3.f1713e     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            double r3 = (double) r3     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x0085
            com.baidu.location.Poi r3 = new com.baidu.location.Poi     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            byte[] r5 = r15.getBytes()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            byte[] r5 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r5)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            byte[] r6 = r16.getBytes()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            byte[] r6 = com.baidu.android.bbalbs.common.p012a.C0692b.m1540a(r6)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r6 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r3.<init>(r4, r5, r6)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            float r1 = (float) r1     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            int r1 = java.lang.Math.round(r1)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            int r1 = m2234a(r12, r1)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            if (r1 <= r14) goto L_0x0085
            r14 = r1
            r13 = r3
        L_0x0085:
            r11.moveToNext()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r12 = 0
            goto L_0x0023
        L_0x008a:
            goto L_0x00a3
        L_0x008c:
            r13 = r2
        L_0x008d:
            if (r11 == 0) goto L_0x00a6
        L_0x008f:
            r11.close()     // Catch:{ Exception -> 0x0093 }
            goto L_0x00a6
        L_0x0093:
            goto L_0x00a6
        L_0x0095:
            r0 = move-exception
            goto L_0x009b
        L_0x0097:
            r13 = r2
            goto L_0x00a3
        L_0x0099:
            r0 = move-exception
            r11 = r2
        L_0x009b:
            if (r11 == 0) goto L_0x00a0
            r11.close()     // Catch:{ Exception -> 0x00a0 }
        L_0x00a0:
            throw r0
        L_0x00a1:
            r11 = r2
            r13 = r11
        L_0x00a3:
            if (r11 == 0) goto L_0x00a6
            goto L_0x008f
        L_0x00a6:
            if (r13 == 0) goto L_0x00ab
            r0.add(r13)
        L_0x00ab:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0813l.mo10612b(double, double):java.util.List");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0143, code lost:
        r1 = r6;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x017e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x017f, code lost:
        r16 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x01ed A[SYNTHETIC, Splitter:B:100:0x01ed] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01f4 A[SYNTHETIC, Splitter:B:104:0x01f4] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0212 A[SYNTHETIC, Splitter:B:118:0x0212] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0219 A[SYNTHETIC, Splitter:B:122:0x0219] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0222 A[SYNTHETIC, Splitter:B:129:0x0222] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0229 A[SYNTHETIC, Splitter:B:133:0x0229] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0232  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x017e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:11:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01a0 A[SYNTHETIC, Splitter:B:86:0x01a0] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01c3 A[Catch:{ Exception -> 0x0205, all -> 0x01fa }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01cb A[Catch:{ Exception -> 0x0205, all -> 0x01fa }] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject mo10613b() {
        /*
            r22 = this;
            r1 = r22
            java.lang.String r0 = "addr"
            java.lang.String r2 = "poi"
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            long r6 = java.lang.System.currentTimeMillis()
            r8 = 86400000(0x5265c00, double:4.2687272E-316)
            long r6 = r6 / r8
            int r7 = (int) r6
            java.lang.String r6 = "SELECT * FROM RGCUPDATE WHERE type=%d AND %d > timestamp+%d ORDER BY gridkey"
            android.database.sqlite.SQLiteDatabase r9 = r1.f1703d     // Catch:{ Exception -> 0x021d, all -> 0x020b }
            r12 = 0
            if (r9 == 0) goto L_0x0196
            android.database.sqlite.SQLiteDatabase r9 = r1.f1703d     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            boolean r9 = r9.isOpen()     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            if (r9 == 0) goto L_0x0196
            org.json.JSONArray r9 = new org.json.JSONArray     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r9.<init>()     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            org.json.JSONArray r13 = new org.json.JSONArray     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r13.<init>()     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            org.json.JSONArray r14 = new org.json.JSONArray     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r14.<init>()     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            org.json.JSONArray r15 = new org.json.JSONArray     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r15.<init>()     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            android.database.sqlite.SQLiteDatabase r8 = r1.f1703d     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r10 = 3
            java.lang.Object[] r11 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            java.lang.Integer r19 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r11[r12] = r19     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            java.lang.Integer r19 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r18 = 1
            r11[r18] = r19     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            com.baidu.location.d.h r12 = r1.f1701a     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            com.baidu.location.d.f r12 = r12.mo10601l()     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            int r12 = r12.mo10586p()     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r17 = 2
            r11[r17] = r12     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            java.lang.String r11 = java.lang.String.format(r6, r11)     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            r12 = 0
            android.database.Cursor r8 = r8.rawQuery(r11, r12)     // Catch:{ Exception -> 0x0190, all -> 0x0188 }
            android.database.sqlite.SQLiteDatabase r11 = r1.f1703d     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            r12 = 1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            r19 = 0
            r10[r19] = r18     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            r10[r12] = r7     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            com.baidu.location.d.h r7 = r1.f1701a     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            com.baidu.location.d.f r7 = r7.mo10601l()     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            int r7 = r7.mo10587q()     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            r12 = 2
            r10[r12] = r7     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            java.lang.String r6 = java.lang.String.format(r6, r10)     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            r12 = 0
            android.database.Cursor r6 = r11.rawQuery(r6, r12)     // Catch:{ Exception -> 0x0186, all -> 0x0184 }
            boolean r7 = r8.moveToFirst()     // Catch:{ Exception -> 0x0182, all -> 0x017e }
            java.lang.String r10 = ","
            java.lang.String r11 = "\""
            if (r7 == 0) goto L_0x00f5
            java.util.HashSet r7 = new java.util.HashSet     // Catch:{ Exception -> 0x0182, all -> 0x017e }
            r7.<init>()     // Catch:{ Exception -> 0x0182, all -> 0x017e }
        L_0x00ab:
            boolean r16 = r8.isAfterLast()     // Catch:{ Exception -> 0x0182, all -> 0x017e }
            if (r16 != 0) goto L_0x00de
            r12 = 0
            java.lang.String r1 = r8.getString(r12)     // Catch:{ Exception -> 0x0182, all -> 0x017e }
            r20 = r2
            r12 = 1
            java.lang.String r2 = r8.getString(r12)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r14.put(r1)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r7.add(r2)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            int r2 = r5.length()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            if (r2 <= 0) goto L_0x00cc
            r5.append(r10)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
        L_0x00cc:
            r5.append(r11)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r5.append(r1)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r5.append(r11)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r8.moveToNext()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r12 = 0
            r1 = r22
            r2 = r20
            goto L_0x00ab
        L_0x00de:
            r20 = r2
            int r1 = r7.size()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r7.toArray(r1)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r2 = 0
        L_0x00ea:
            int r7 = r1.length     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            if (r2 >= r7) goto L_0x00f7
            r7 = r1[r2]     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r15.put(r7)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            int r2 = r2 + 1
            goto L_0x00ea
        L_0x00f5:
            r20 = r2
        L_0x00f7:
            boolean r1 = r6.moveToFirst()     // Catch:{ Exception -> 0x017b, all -> 0x017e }
            if (r1 == 0) goto L_0x0147
            java.util.HashSet r1 = new java.util.HashSet     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r1.<init>()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
        L_0x0102:
            boolean r2 = r6.isAfterLast()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            if (r2 != 0) goto L_0x012e
            r2 = 0
            java.lang.String r7 = r6.getString(r2)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r2 = 1
            java.lang.String r12 = r6.getString(r2)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r9.put(r7)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r1.add(r12)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            int r2 = r4.length()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            if (r2 <= 0) goto L_0x0121
            r4.append(r10)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
        L_0x0121:
            r4.append(r11)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r4.append(r7)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r4.append(r11)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r6.moveToNext()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            goto L_0x0102
        L_0x012e:
            int r2 = r1.size()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r1.toArray(r2)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r1 = 0
        L_0x0138:
            int r7 = r2.length     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            if (r1 >= r7) goto L_0x0147
            r7 = r2[r1]     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r13.put(r7)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            int r1 = r1 + 1
            goto L_0x0138
        L_0x0143:
            r1 = r6
            r2 = r20
            goto L_0x0192
        L_0x0147:
            int r1 = r14.length()     // Catch:{ Exception -> 0x017b, all -> 0x017e }
            java.lang.String r2 = "ver"
            java.lang.String r7 = "gk"
            if (r1 == 0) goto L_0x015f
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r1.<init>()     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r1.put(r7, r14)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r1.put(r2, r15)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
            r3.put(r0, r1)     // Catch:{ Exception -> 0x0143, all -> 0x017e }
        L_0x015f:
            int r1 = r9.length()     // Catch:{ Exception -> 0x017b, all -> 0x017e }
            if (r1 == 0) goto L_0x0176
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x017b, all -> 0x017e }
            r1.<init>()     // Catch:{ Exception -> 0x017b, all -> 0x017e }
            r1.put(r7, r9)     // Catch:{ Exception -> 0x017b, all -> 0x017e }
            r1.put(r2, r13)     // Catch:{ Exception -> 0x017b, all -> 0x017e }
            r2 = r20
            r3.put(r2, r1)     // Catch:{ Exception -> 0x0182, all -> 0x017e }
            goto L_0x0178
        L_0x0176:
            r2 = r20
        L_0x0178:
            r1 = r8
            r8 = r6
            goto L_0x0198
        L_0x017b:
            r2 = r20
            goto L_0x0182
        L_0x017e:
            r0 = move-exception
            r16 = r6
            goto L_0x018c
        L_0x0182:
            r1 = r6
            goto L_0x0192
        L_0x0184:
            r0 = move-exception
            goto L_0x018a
        L_0x0186:
            r1 = 0
            goto L_0x0192
        L_0x0188:
            r0 = move-exception
            r8 = 0
        L_0x018a:
            r16 = 0
        L_0x018c:
            r6 = r22
            goto L_0x0210
        L_0x0190:
            r1 = 0
            r8 = 0
        L_0x0192:
            r6 = r22
            goto L_0x0220
        L_0x0196:
            r1 = 0
            r8 = 0
        L_0x0198:
            int r6 = r5.length()     // Catch:{ Exception -> 0x0203, all -> 0x01fc }
            java.lang.String r7 = "UPDATE RGCUPDATE SET timestamp=timestamp+1 WHERE type = %d AND gridkey IN (%s)"
            if (r6 <= 0) goto L_0x01c3
            java.util.Locale r6 = java.util.Locale.US     // Catch:{ Exception -> 0x0203, all -> 0x01fc }
            r9 = 2
            java.lang.Object[] r10 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x0203, all -> 0x01fc }
            r9 = 0
            java.lang.Integer r11 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0203, all -> 0x01fc }
            r10[r9] = r11     // Catch:{ Exception -> 0x0203, all -> 0x01fc }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0203, all -> 0x01fc }
            r9 = 1
            r10[r9] = r5     // Catch:{ Exception -> 0x0203, all -> 0x01fc }
            java.lang.String r5 = java.lang.String.format(r6, r7, r10)     // Catch:{ Exception -> 0x0203, all -> 0x01fc }
            r6 = r22
            android.database.sqlite.SQLiteDatabase r9 = r6.f1703d     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            if (r9 == 0) goto L_0x01c5
            android.database.sqlite.SQLiteDatabase r9 = r6.f1703d     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            r9.execSQL(r5)     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            goto L_0x01c5
        L_0x01c3:
            r6 = r22
        L_0x01c5:
            int r5 = r4.length()     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            if (r5 <= 0) goto L_0x01eb
            java.util.Locale r5 = java.util.Locale.US     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            r9 = 2
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            r10 = 1
            java.lang.Integer r11 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            r12 = 0
            r9[r12] = r11     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            r9[r10] = r4     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            java.lang.String r4 = java.lang.String.format(r5, r7, r9)     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            android.database.sqlite.SQLiteDatabase r5 = r6.f1703d     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            if (r5 == 0) goto L_0x01eb
            android.database.sqlite.SQLiteDatabase r5 = r6.f1703d     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
            r5.execSQL(r4)     // Catch:{ Exception -> 0x0205, all -> 0x01fa }
        L_0x01eb:
            if (r1 == 0) goto L_0x01f2
            r1.close()     // Catch:{ Exception -> 0x01f1 }
            goto L_0x01f2
        L_0x01f1:
        L_0x01f2:
            if (r8 == 0) goto L_0x022c
            r8.close()     // Catch:{ Exception -> 0x01f8 }
            goto L_0x022c
        L_0x01f8:
            goto L_0x022c
        L_0x01fa:
            r0 = move-exception
            goto L_0x01ff
        L_0x01fc:
            r0 = move-exception
            r6 = r22
        L_0x01ff:
            r16 = r8
            r8 = r1
            goto L_0x0210
        L_0x0203:
            r6 = r22
        L_0x0205:
            r21 = r8
            r8 = r1
            r1 = r21
            goto L_0x0220
        L_0x020b:
            r0 = move-exception
            r6 = r1
            r8 = 0
            r16 = 0
        L_0x0210:
            if (r8 == 0) goto L_0x0217
            r8.close()     // Catch:{ Exception -> 0x0216 }
            goto L_0x0217
        L_0x0216:
        L_0x0217:
            if (r16 == 0) goto L_0x021c
            r16.close()     // Catch:{ Exception -> 0x021c }
        L_0x021c:
            throw r0
        L_0x021d:
            r6 = r1
            r1 = 0
            r8 = 0
        L_0x0220:
            if (r8 == 0) goto L_0x0227
            r8.close()     // Catch:{ Exception -> 0x0226 }
            goto L_0x0227
        L_0x0226:
        L_0x0227:
            if (r1 == 0) goto L_0x022c
            r1.close()     // Catch:{ Exception -> 0x01f8 }
        L_0x022c:
            boolean r1 = r3.has(r2)
            if (r1 != 0) goto L_0x023b
            boolean r0 = r3.has(r0)
            if (r0 != 0) goto L_0x023b
            r16 = 0
            goto L_0x023d
        L_0x023b:
            r16 = r3
        L_0x023d:
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0813l.mo10613b():org.json.JSONObject");
    }
}
