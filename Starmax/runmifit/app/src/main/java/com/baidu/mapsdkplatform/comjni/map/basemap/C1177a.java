package com.baidu.mapsdkplatform.comjni.map.basemap;

import android.os.Bundle;
import com.baidu.mapapi.map.MapBaseIndoorMapInfo;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapsdkplatform.comjni.map.basemap.a */
public class C1177a {

    /* renamed from: a */
    private static final String f3941a = C1177a.class.getSimpleName();

    /* renamed from: d */
    private static Set<Integer> f3942d = new HashSet();

    /* renamed from: e */
    private static List<JNIBaseMap> f3943e = new ArrayList();

    /* renamed from: b */
    private long f3944b;

    /* renamed from: c */
    private JNIBaseMap f3945c;

    public C1177a() {
        this.f3944b = 0;
        this.f3945c = null;
        this.f3945c = new JNIBaseMap();
    }

    /* renamed from: a */
    public static int m4290a(long j, int i, int i2, int i3) {
        return JNIBaseMap.MapProc(j, i, i2, i3);
    }

    /* renamed from: b */
    public static List<JNIBaseMap> m4291b() {
        return f3943e;
    }

    /* renamed from: b */
    public static void m4292b(long j, boolean z) {
        JNIBaseMap.SetMapCustomEnable(j, z);
    }

    /* renamed from: a */
    public long mo13468a() {
        return this.f3944b;
    }

    /* renamed from: a */
    public long mo13469a(int i, int i2, String str) {
        return this.f3945c.AddLayer(this.f3944b, i, i2, str);
    }

    /* renamed from: a */
    public String mo13470a(int i, int i2) {
        return this.f3945c.ScrPtToGeoPoint(this.f3944b, i, i2);
    }

    /* renamed from: a */
    public String mo13471a(int i, int i2, int i3, int i4) {
        return this.f3945c.GetNearlyObjID(this.f3944b, (long) i, i2, i3, i4);
    }

    /* renamed from: a */
    public String mo13472a(String str) {
        return this.f3945c.OnSchcityGet(this.f3944b, str);
    }

    /* renamed from: a */
    public void mo13473a(long j, long j2, long j3, long j4, boolean z) {
        this.f3945c.setCustomTrafficColor(this.f3944b, j, j2, j3, j4, z);
    }

    /* renamed from: a */
    public void mo13474a(long j, boolean z) {
        this.f3945c.ShowLayers(this.f3944b, j, z);
    }

    /* renamed from: a */
    public void mo13475a(Bundle bundle) {
        this.f3945c.SetMapStatus(this.f3944b, bundle);
    }

    /* renamed from: a */
    public void mo13476a(String str, Bundle bundle) {
        this.f3945c.SaveScreenToLocal(this.f3944b, str, bundle);
    }

    /* renamed from: a */
    public void mo13477a(boolean z) {
        this.f3945c.ShowSatelliteMap(this.f3944b, z);
    }

    /* renamed from: a */
    public void mo13478a(Bundle[] bundleArr) {
        this.f3945c.addOverlayItems(this.f3944b, bundleArr, bundleArr.length);
    }

    /* renamed from: a */
    public boolean mo13479a(int i) {
        this.f3944b = f3943e.size() == 0 ? this.f3945c.Create() : this.f3945c.CreateDuplicate(f3943e.get(0).f3940a);
        JNIBaseMap jNIBaseMap = this.f3945c;
        jNIBaseMap.f3940a = this.f3944b;
        f3943e.add(jNIBaseMap);
        f3942d.add(Integer.valueOf(i));
        this.f3945c.SetCallback(this.f3944b, null);
        return true;
    }

    /* renamed from: a */
    public boolean mo13480a(int i, boolean z) {
        return this.f3945c.OnRecordReload(this.f3944b, i, z);
    }

    /* renamed from: a */
    public boolean mo13481a(int i, boolean z, int i2) {
        return this.f3945c.OnRecordStart(this.f3944b, i, z, i2);
    }

    /* renamed from: a */
    public boolean mo13482a(long j) {
        return this.f3945c.LayersIsShow(this.f3944b, j);
    }

    /* renamed from: a */
    public boolean mo13483a(long j, long j2) {
        return this.f3945c.SwitchLayer(this.f3944b, j, j2);
    }

    /* renamed from: a */
    public boolean mo13484a(String str, String str2) {
        return this.f3945c.SwitchBaseIndoorMapFloor(this.f3944b, str, str2);
    }

    /* renamed from: a */
    public boolean mo13485a(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, String str8, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        return this.f3945c.Init(this.f3944b, str, str2, str3, str4, str5, str6, str7, i, str8, i2, i3, i4, i5, i6, i7, i8);
    }

    /* renamed from: a */
    public boolean mo13486a(boolean z, boolean z2) {
        return this.f3945c.OnRecordImport(this.f3944b, z, z2);
    }

    /* renamed from: a */
    public int[] mo13487a(int[] iArr, int i, int i2) {
        return this.f3945c.GetScreenBuf(this.f3944b, iArr, i, i2);
    }

    /* renamed from: b */
    public String mo13488b(int i, int i2) {
        return this.f3945c.GeoPtToScrPoint(this.f3944b, i, i2);
    }

    /* renamed from: b */
    public void mo13489b(long j) {
        this.f3945c.UpdateLayers(this.f3944b, j);
    }

    /* renamed from: b */
    public void mo13490b(Bundle bundle) {
        this.f3945c.setMapStatusLimits(this.f3944b, bundle);
    }

    /* renamed from: b */
    public void mo13491b(boolean z) {
        this.f3945c.ShowHotMap(this.f3944b, z);
    }

    /* renamed from: b */
    public boolean mo13492b(int i) {
        this.f3945c.Release(this.f3944b);
        f3943e.remove(this.f3945c);
        f3942d.remove(Integer.valueOf(i));
        this.f3944b = 0;
        return true;
    }

    /* renamed from: b */
    public boolean mo13493b(int i, boolean z) {
        return this.f3945c.OnRecordRemove(this.f3944b, i, z);
    }

    /* renamed from: b */
    public boolean mo13494b(int i, boolean z, int i2) {
        return this.f3945c.OnRecordSuspend(this.f3944b, i, z, i2);
    }

    /* renamed from: c */
    public float mo13495c(Bundle bundle) {
        return this.f3945c.GetZoomToBound(this.f3944b, bundle);
    }

    /* renamed from: c */
    public int mo13496c(int i) {
        return this.f3945c.SetMapControlMode(this.f3944b, i);
    }

    /* renamed from: c */
    public void mo13497c() {
        this.f3945c.OnPause(this.f3944b);
    }

    /* renamed from: c */
    public void mo13498c(boolean z) {
        this.f3945c.ShowTrafficMap(this.f3944b, z);
    }

    /* renamed from: c */
    public boolean mo13499c(long j) {
        return this.f3945c.cleanSDKTileDataCache(this.f3944b, j);
    }

    /* renamed from: d */
    public void mo13500d() {
        this.f3945c.OnResume(this.f3944b);
    }

    /* renamed from: d */
    public void mo13501d(long j) {
        this.f3945c.ClearLayer(this.f3944b, j);
    }

    /* renamed from: d */
    public void mo13502d(boolean z) {
        this.f3945c.enableDrawHouseHeight(this.f3944b, z);
    }

    /* renamed from: d */
    public boolean mo13503d(int i) {
        return this.f3945c.OnRecordAdd(this.f3944b, i);
    }

    /* renamed from: d */
    public boolean mo13504d(Bundle bundle) {
        return this.f3945c.updateSDKTile(this.f3944b, bundle);
    }

    /* renamed from: e */
    public String mo13505e(int i) {
        return this.f3945c.OnRecordGetAt(this.f3944b, i);
    }

    /* renamed from: e */
    public String mo13506e(long j) {
        return this.f3945c.getCompassPosition(this.f3944b, j);
    }

    /* renamed from: e */
    public void mo13507e() {
        this.f3945c.OnBackground(this.f3944b);
    }

    /* renamed from: e */
    public void mo13508e(boolean z) {
        this.f3945c.ShowBaseIndoorMap(this.f3944b, z);
    }

    /* renamed from: e */
    public boolean mo13509e(Bundle bundle) {
        return this.f3945c.addtileOverlay(this.f3944b, bundle);
    }

    /* renamed from: f */
    public void mo13510f() {
        this.f3945c.OnForeground(this.f3944b);
    }

    /* renamed from: f */
    public void mo13511f(Bundle bundle) {
        this.f3945c.addOneOverlayItem(this.f3944b, bundle);
    }

    /* renamed from: g */
    public void mo13512g() {
        this.f3945c.ResetImageRes(this.f3944b);
    }

    /* renamed from: g */
    public void mo13513g(Bundle bundle) {
        this.f3945c.updateOneOverlayItem(this.f3944b, bundle);
    }

    /* renamed from: h */
    public Bundle mo13514h() {
        return this.f3945c.GetMapStatus(this.f3944b);
    }

    /* renamed from: h */
    public void mo13515h(Bundle bundle) {
        this.f3945c.removeOneOverlayItem(this.f3944b, bundle);
    }

    /* renamed from: i */
    public Bundle mo13516i() {
        return this.f3945c.getMapStatusLimits(this.f3944b);
    }

    /* renamed from: j */
    public Bundle mo13517j() {
        return this.f3945c.getDrawingMapStatus(this.f3944b);
    }

    /* renamed from: k */
    public boolean mo13518k() {
        return this.f3945c.GetBaiduHotMapCityInfo(this.f3944b);
    }

    /* renamed from: l */
    public String mo13519l() {
        return this.f3945c.OnRecordGetAll(this.f3944b);
    }

    /* renamed from: m */
    public String mo13520m() {
        return this.f3945c.OnHotcityGet(this.f3944b);
    }

    /* renamed from: n */
    public void mo13521n() {
        this.f3945c.PostStatInfo(this.f3944b);
    }

    /* renamed from: o */
    public boolean mo13522o() {
        return this.f3945c.isDrawHouseHeightEnable(this.f3944b);
    }

    /* renamed from: p */
    public void mo13523p() {
        this.f3945c.clearHeatMapLayerCache(this.f3944b);
    }

    /* renamed from: q */
    public MapBaseIndoorMapInfo mo13524q() {
        String str = this.f3945c.getfocusedBaseIndoorMapInfo(this.f3944b);
        if (str == null) {
            return null;
        }
        String str2 = "";
        String str3 = new String();
        ArrayList arrayList = new ArrayList(1);
        try {
            JSONObject jSONObject = new JSONObject(str);
            str2 = jSONObject.optString("focusindoorid");
            str3 = jSONObject.optString("curfloor");
            JSONArray optJSONArray = jSONObject.optJSONArray("floorlist");
            if (optJSONArray == null) {
                return null;
            }
            for (int i = 0; i < optJSONArray.length(); i++) {
                arrayList.add(optJSONArray.get(i).toString());
            }
            return new MapBaseIndoorMapInfo(str2, str3, arrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: r */
    public boolean mo13525r() {
        return this.f3945c.IsBaseIndoorMapMode(this.f3944b);
    }

    /* renamed from: s */
    public void mo13526s() {
        this.f3945c.setBackgroundTransparent(this.f3944b);
    }

    /* renamed from: t */
    public void mo13527t() {
        this.f3945c.resetBackgroundTransparent(this.f3944b);
    }

    /* renamed from: u */
    public float[] mo13528u() {
        JNIBaseMap jNIBaseMap = this.f3945c;
        if (jNIBaseMap == null) {
            return null;
        }
        float[] fArr = new float[16];
        jNIBaseMap.getProjectionMatrix(this.f3944b, fArr, 16);
        return fArr;
    }

    /* renamed from: v */
    public float[] mo13529v() {
        JNIBaseMap jNIBaseMap = this.f3945c;
        if (jNIBaseMap == null) {
            return null;
        }
        float[] fArr = new float[16];
        jNIBaseMap.getViewMatrix(this.f3944b, fArr, 16);
        return fArr;
    }
}
