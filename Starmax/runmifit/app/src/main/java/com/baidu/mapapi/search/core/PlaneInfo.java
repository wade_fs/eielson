package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

public class PlaneInfo extends TransitBaseInfo {
    public static final Parcelable.Creator<PlaneInfo> CREATOR = new C0960d();

    /* renamed from: a */
    private double f2920a;

    /* renamed from: b */
    private String f2921b;

    /* renamed from: c */
    private double f2922c;

    /* renamed from: d */
    private String f2923d;

    public PlaneInfo() {
    }

    protected PlaneInfo(Parcel parcel) {
        super(parcel);
        this.f2920a = parcel.readDouble();
        this.f2921b = parcel.readString();
        this.f2922c = parcel.readDouble();
        this.f2923d = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public String getAirlines() {
        return this.f2921b;
    }

    public String getBooking() {
        return this.f2923d;
    }

    public double getDiscount() {
        return this.f2920a;
    }

    public double getPrice() {
        return this.f2922c;
    }

    public void setAirlines(String str) {
        this.f2921b = str;
    }

    public void setBooking(String str) {
        this.f2923d = str;
    }

    public void setDiscount(double d) {
        this.f2920a = d;
    }

    public void setPrice(double d) {
        this.f2922c = d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeDouble(this.f2920a);
        parcel.writeString(this.f2921b);
        parcel.writeDouble(this.f2922c);
        parcel.writeString(this.f2923d);
    }
}
