package com.baidu.mapsdkplatform.comapi.favrite;

import android.os.Bundle;
import android.text.TextUtils;
import com.baidu.mapapi.UIMsg;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapapi.model.inner.Point;
import com.baidu.mapsdkplatform.comjni.map.favorite.C1180a;
import com.baidu.mobstat.Config;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapsdkplatform.comapi.favrite.a */
public class C1056a {

    /* renamed from: b */
    private static C1056a f3385b;

    /* renamed from: a */
    private C1180a f3386a = null;

    /* renamed from: c */
    private boolean f3387c = false;

    /* renamed from: d */
    private boolean f3388d = false;

    /* renamed from: e */
    private Vector<String> f3389e = null;

    /* renamed from: f */
    private Vector<String> f3390f = null;

    /* renamed from: g */
    private boolean f3391g = false;

    /* renamed from: h */
    private C1059c f3392h = new C1059c();

    /* renamed from: i */
    private C1058b f3393i = new C1058b();

    /* renamed from: com.baidu.mapsdkplatform.comapi.favrite.a$a */
    class C1057a implements Comparator<String> {
        C1057a() {
        }

        /* renamed from: a */
        public int compare(String str, String str2) {
            return str2.compareTo(str);
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.favrite.a$b */
    private class C1058b {

        /* renamed from: b */
        private long f3396b;

        /* renamed from: c */
        private long f3397c;

        private C1058b() {
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m3485a() {
            this.f3396b = System.currentTimeMillis();
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m3487b() {
            this.f3397c = System.currentTimeMillis();
        }

        /* access modifiers changed from: private */
        /* renamed from: c */
        public boolean m3490c() {
            return this.f3397c - this.f3396b > 1000;
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.favrite.a$c */
    private class C1059c {

        /* renamed from: b */
        private String f3399b;

        /* renamed from: c */
        private long f3400c;

        /* renamed from: d */
        private long f3401d;

        private C1059c() {
            this.f3400c = 5000;
            this.f3401d = 0;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public String m3491a() {
            return this.f3399b;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m3493a(String str) {
            this.f3399b = str;
            this.f3401d = System.currentTimeMillis();
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public boolean m3495b() {
            return TextUtils.isEmpty(this.f3399b);
        }

        /* access modifiers changed from: private */
        /* renamed from: c */
        public boolean m3498c() {
            return true;
        }
    }

    private C1056a() {
    }

    /* renamed from: a */
    public static C1056a m3469a() {
        if (f3385b == null) {
            synchronized (C1056a.class) {
                if (f3385b == null) {
                    f3385b = new C1056a();
                    f3385b.m3471h();
                }
            }
        }
        return f3385b;
    }

    /* renamed from: g */
    public static boolean m3470g() {
        C1180a aVar;
        C1056a aVar2 = f3385b;
        return (aVar2 == null || (aVar = aVar2.f3386a) == null || !aVar.mo13557d()) ? false : true;
    }

    /* renamed from: h */
    private boolean m3471h() {
        if (this.f3386a == null) {
            this.f3386a = new C1180a();
            if (this.f3386a.mo13547a() == 0) {
                this.f3386a = null;
                return false;
            }
            m3473j();
            m3472i();
        }
        return true;
    }

    /* renamed from: i */
    private boolean m3472i() {
        if (this.f3386a == null) {
            return false;
        }
        String str = SysOSUtil.getModuleFileName() + "/";
        this.f3386a.mo13548a(1);
        return this.f3386a.mo13551a(str, "fav_poi", "fifo", 10, UIMsg.d_ResultType.VERSION_CHECK, -1);
    }

    /* renamed from: j */
    private void m3473j() {
        this.f3387c = false;
        this.f3388d = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0109, code lost:
        return -1;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int mo12932a(java.lang.String r7, com.baidu.mapsdkplatform.comapi.favrite.FavSyncPoi r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            com.baidu.mapsdkplatform.comjni.map.favorite.a r0 = r6.f3386a     // Catch:{ all -> 0x010a }
            r1 = 0
            if (r0 != 0) goto L_0x0008
            monitor-exit(r6)
            return r1
        L_0x0008:
            r0 = -1
            if (r7 == 0) goto L_0x0108
            java.lang.String r2 = ""
            boolean r2 = r7.equals(r2)     // Catch:{ all -> 0x010a }
            if (r2 != 0) goto L_0x0108
            if (r8 != 0) goto L_0x0017
            goto L_0x0108
        L_0x0017:
            r6.m3473j()     // Catch:{ all -> 0x010a }
            java.util.ArrayList r2 = r6.mo12940e()     // Catch:{ all -> 0x010a }
            if (r2 == 0) goto L_0x0025
            int r3 = r2.size()     // Catch:{ all -> 0x010a }
            goto L_0x0026
        L_0x0025:
            r3 = 0
        L_0x0026:
            r4 = 1
            int r3 = r3 + r4
            r5 = 500(0x1f4, float:7.0E-43)
            if (r3 <= r5) goto L_0x002f
            r7 = -2
            monitor-exit(r6)
            return r7
        L_0x002f:
            if (r2 == 0) goto L_0x0058
            int r3 = r2.size()     // Catch:{ all -> 0x010a }
            if (r3 <= 0) goto L_0x0058
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x010a }
        L_0x003b:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x010a }
            if (r3 == 0) goto L_0x0058
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x010a }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x010a }
            com.baidu.mapsdkplatform.comapi.favrite.FavSyncPoi r3 = r6.mo12934b(r3)     // Catch:{ all -> 0x010a }
            if (r3 != 0) goto L_0x004e
            goto L_0x003b
        L_0x004e:
            java.lang.String r3 = r3.f3376b     // Catch:{ all -> 0x010a }
            boolean r3 = r7.equals(r3)     // Catch:{ all -> 0x010a }
            if (r3 == 0) goto L_0x003b
            monitor-exit(r6)
            return r0
        L_0x0058:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r0.<init>()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r8.f3376b = r7     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = java.lang.String.valueOf(r2)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r2.<init>()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r2.append(r7)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r3 = "_"
            r2.append(r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            int r3 = r8.hashCode()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r2.append(r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r8.f3382h = r7     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r8.f3375a = r2     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = "bdetail"
            boolean r3 = r8.f3383i     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r0.put(r7, r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = "uspoiname"
            java.lang.String r3 = r8.f3376b     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r0.put(r7, r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r7.<init>()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r3 = "x"
            com.baidu.mapapi.model.inner.Point r5 = r8.f3377c     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            int r5 = r5.getmPtx()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r7.put(r3, r5)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r3 = "y"
            com.baidu.mapapi.model.inner.Point r5 = r8.f3377c     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            int r5 = r5.getmPty()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r7.put(r3, r5)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r3 = "pt"
            r0.put(r3, r7)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = "ncityid"
            java.lang.String r3 = r8.f3379e     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r0.put(r7, r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = "npoitype"
            int r3 = r8.f3381g     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r0.put(r7, r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = "uspoiuid"
            java.lang.String r3 = r8.f3380f     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r0.put(r7, r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = "addr"
            java.lang.String r3 = r8.f3378d     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r0.put(r7, r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = "addtimesec"
            java.lang.String r3 = r8.f3382h     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r0.put(r7, r3)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r7.<init>()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r3 = "Fav_Sync"
            r7.put(r3, r0)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r0 = "Fav_Content"
            java.lang.String r8 = r8.f3384j     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            r7.put(r0, r8)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            com.baidu.mapsdkplatform.comjni.map.favorite.a r8 = r6.f3386a     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            java.lang.String r7 = r7.toString()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            boolean r7 = r8.mo13550a(r2, r7)     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            if (r7 == 0) goto L_0x00f9
            r6.m3473j()     // Catch:{ JSONException -> 0x0103, all -> 0x00fe }
            m3470g()     // Catch:{ all -> 0x010a }
            monitor-exit(r6)
            return r4
        L_0x00f9:
            m3470g()     // Catch:{ all -> 0x010a }
            monitor-exit(r6)
            return r1
        L_0x00fe:
            r7 = move-exception
            m3470g()     // Catch:{ all -> 0x010a }
            throw r7     // Catch:{ all -> 0x010a }
        L_0x0103:
            m3470g()     // Catch:{ all -> 0x010a }
            monitor-exit(r6)
            return r1
        L_0x0108:
            monitor-exit(r6)
            return r0
        L_0x010a:
            r7 = move-exception
            monitor-exit(r6)
            goto L_0x010e
        L_0x010d:
            throw r7
        L_0x010e:
            goto L_0x010d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.favrite.C1056a.mo12932a(java.lang.String, com.baidu.mapsdkplatform.comapi.favrite.FavSyncPoi):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0027, code lost:
        return false;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean mo12933a(java.lang.String r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.baidu.mapsdkplatform.comjni.map.favorite.a r0 = r2.f3386a     // Catch:{ all -> 0x0028 }
            r1 = 0
            if (r0 != 0) goto L_0x0008
            monitor-exit(r2)
            return r1
        L_0x0008:
            if (r3 == 0) goto L_0x0026
            java.lang.String r0 = ""
            boolean r0 = r3.equals(r0)     // Catch:{ all -> 0x0028 }
            if (r0 == 0) goto L_0x0013
            goto L_0x0026
        L_0x0013:
            boolean r0 = r2.mo12938c(r3)     // Catch:{ all -> 0x0028 }
            if (r0 != 0) goto L_0x001b
            monitor-exit(r2)
            return r1
        L_0x001b:
            r2.m3473j()     // Catch:{ all -> 0x0028 }
            com.baidu.mapsdkplatform.comjni.map.favorite.a r0 = r2.f3386a     // Catch:{ all -> 0x0028 }
            boolean r3 = r0.mo13549a(r3)     // Catch:{ all -> 0x0028 }
            monitor-exit(r2)
            return r3
        L_0x0026:
            monitor-exit(r2)
            return r1
        L_0x0028:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.favrite.C1056a.mo12933a(java.lang.String):boolean");
    }

    /* renamed from: b */
    public FavSyncPoi mo12934b(String str) {
        if (!(this.f3386a == null || str == null || str.equals(""))) {
            try {
                if (!mo12938c(str)) {
                    return null;
                }
                FavSyncPoi favSyncPoi = new FavSyncPoi();
                String b = this.f3386a.mo13553b(str);
                if (b != null) {
                    if (!b.equals("")) {
                        JSONObject jSONObject = new JSONObject(b);
                        JSONObject optJSONObject = jSONObject.optJSONObject("Fav_Sync");
                        String optString = jSONObject.optString("Fav_Content");
                        favSyncPoi.f3376b = optJSONObject.optString("uspoiname");
                        JSONObject optJSONObject2 = optJSONObject.optJSONObject(Config.PLATFORM_TYPE);
                        favSyncPoi.f3377c = new Point(optJSONObject2.optInt(Config.EVENT_HEAT_X), optJSONObject2.optInt("y"));
                        favSyncPoi.f3379e = optJSONObject.optString("ncityid");
                        favSyncPoi.f3380f = optJSONObject.optString("uspoiuid");
                        favSyncPoi.f3381g = optJSONObject.optInt("npoitype");
                        favSyncPoi.f3378d = optJSONObject.optString("addr");
                        favSyncPoi.f3382h = optJSONObject.optString("addtimesec");
                        favSyncPoi.f3383i = optJSONObject.optBoolean("bdetail");
                        favSyncPoi.f3384j = optString;
                        favSyncPoi.f3375a = str;
                        return favSyncPoi;
                    }
                }
                return null;
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
                return null;
            }
        }
        return null;
    }

    /* renamed from: b */
    public void mo12935b() {
        C1056a aVar = f3385b;
        if (aVar != null) {
            C1180a aVar2 = aVar.f3386a;
            if (aVar2 != null) {
                aVar2.mo13552b();
                f3385b.f3386a = null;
            }
            f3385b = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a0, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00a4, code lost:
        return false;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean mo12936b(java.lang.String r6, com.baidu.mapsdkplatform.comapi.favrite.FavSyncPoi r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.baidu.mapsdkplatform.comjni.map.favorite.a r0 = r5.f3386a     // Catch:{ all -> 0x00a5 }
            r1 = 0
            if (r0 == 0) goto L_0x00a3
            if (r6 == 0) goto L_0x00a3
            java.lang.String r0 = ""
            boolean r0 = r6.equals(r0)     // Catch:{ all -> 0x00a5 }
            if (r0 != 0) goto L_0x00a3
            if (r7 != 0) goto L_0x0014
            goto L_0x00a3
        L_0x0014:
            boolean r0 = r5.mo12938c(r6)     // Catch:{ all -> 0x00a5 }
            if (r0 != 0) goto L_0x001c
            monitor-exit(r5)
            return r1
        L_0x001c:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00a1 }
            r0.<init>()     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r2 = "uspoiname"
            java.lang.String r3 = r7.f3376b     // Catch:{ JSONException -> 0x00a1 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x00a1 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00a1 }
            r2.<init>()     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r3 = "x"
            com.baidu.mapapi.model.inner.Point r4 = r7.f3377c     // Catch:{ JSONException -> 0x00a1 }
            int r4 = r4.getmPtx()     // Catch:{ JSONException -> 0x00a1 }
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r3 = "y"
            com.baidu.mapapi.model.inner.Point r4 = r7.f3377c     // Catch:{ JSONException -> 0x00a1 }
            int r4 = r4.getmPty()     // Catch:{ JSONException -> 0x00a1 }
            r2.put(r3, r4)     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r3 = "pt"
            r0.put(r3, r2)     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r2 = "ncityid"
            java.lang.String r3 = r7.f3379e     // Catch:{ JSONException -> 0x00a1 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r2 = "npoitype"
            int r3 = r7.f3381g     // Catch:{ JSONException -> 0x00a1 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r2 = "uspoiuid"
            java.lang.String r3 = r7.f3380f     // Catch:{ JSONException -> 0x00a1 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r2 = "addr"
            java.lang.String r3 = r7.f3378d     // Catch:{ JSONException -> 0x00a1 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x00a1 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ JSONException -> 0x00a1 }
            r7.f3382h = r2     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r2 = "addtimesec"
            java.lang.String r3 = r7.f3382h     // Catch:{ JSONException -> 0x00a1 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r2 = "bdetail"
            r0.put(r2, r1)     // Catch:{ JSONException -> 0x00a1 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00a1 }
            r2.<init>()     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r3 = "Fav_Sync"
            r2.put(r3, r0)     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r0 = "Fav_Content"
            java.lang.String r7 = r7.f3384j     // Catch:{ JSONException -> 0x00a1 }
            r2.put(r0, r7)     // Catch:{ JSONException -> 0x00a1 }
            r5.m3473j()     // Catch:{ JSONException -> 0x00a1 }
            com.baidu.mapsdkplatform.comjni.map.favorite.a r7 = r5.f3386a     // Catch:{ JSONException -> 0x00a1 }
            if (r7 == 0) goto L_0x009f
            com.baidu.mapsdkplatform.comjni.map.favorite.a r7 = r5.f3386a     // Catch:{ JSONException -> 0x00a1 }
            java.lang.String r0 = r2.toString()     // Catch:{ JSONException -> 0x00a1 }
            boolean r6 = r7.mo13554b(r6, r0)     // Catch:{ JSONException -> 0x00a1 }
            if (r6 == 0) goto L_0x009f
            r1 = 1
        L_0x009f:
            monitor-exit(r5)
            return r1
        L_0x00a1:
            monitor-exit(r5)
            return r1
        L_0x00a3:
            monitor-exit(r5)
            return r1
        L_0x00a5:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.favrite.C1056a.mo12936b(java.lang.String, com.baidu.mapsdkplatform.comapi.favrite.FavSyncPoi):boolean");
    }

    /* renamed from: c */
    public synchronized boolean mo12937c() {
        if (this.f3386a == null) {
            return false;
        }
        m3473j();
        boolean c = this.f3386a.mo13555c();
        m3470g();
        return c;
    }

    /* renamed from: c */
    public boolean mo12938c(String str) {
        return this.f3386a != null && str != null && !str.equals("") && this.f3386a.mo13556c(str);
    }

    /* renamed from: d */
    public ArrayList<String> mo12939d() {
        String b;
        Vector<String> vector;
        if (this.f3386a == null) {
            return null;
        }
        if (this.f3388d && (vector = this.f3390f) != null) {
            return new ArrayList<>(vector);
        }
        try {
            Bundle bundle = new Bundle();
            this.f3386a.mo13546a(bundle);
            String[] stringArray = bundle.getStringArray("rstString");
            if (stringArray != null) {
                if (this.f3390f == null) {
                    this.f3390f = new Vector<>();
                } else {
                    this.f3390f.clear();
                }
                for (int i = 0; i < stringArray.length; i++) {
                    if (!stringArray[i].equals("data_version") && (b = this.f3386a.mo13553b(stringArray[i])) != null) {
                        if (!b.equals("")) {
                            this.f3390f.add(stringArray[i]);
                        }
                    }
                }
                if (this.f3390f.size() > 0) {
                    try {
                        Collections.sort(this.f3390f, new C1057a());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    this.f3388d = true;
                }
            } else if (this.f3390f != null) {
                this.f3390f.clear();
                this.f3390f = null;
            }
            if (this.f3390f != null) {
                if (!this.f3390f.isEmpty()) {
                    return new ArrayList<>(this.f3390f);
                }
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: e */
    public ArrayList<String> mo12940e() {
        Vector<String> vector;
        if (this.f3386a == null) {
            return null;
        }
        if (this.f3387c && (vector = this.f3389e) != null) {
            return new ArrayList<>(vector);
        }
        try {
            Bundle bundle = new Bundle();
            this.f3386a.mo13546a(bundle);
            String[] stringArray = bundle.getStringArray("rstString");
            if (stringArray != null) {
                if (this.f3389e == null) {
                    this.f3389e = new Vector<>();
                } else {
                    this.f3389e.clear();
                }
                for (String str : stringArray) {
                    if (!str.equals("data_version")) {
                        this.f3389e.add(str);
                    }
                }
                if (this.f3389e.size() > 0) {
                    try {
                        Collections.sort(this.f3389e, new C1057a());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    this.f3387c = true;
                }
            } else if (this.f3389e != null) {
                this.f3389e.clear();
                this.f3389e = null;
            }
            Vector<String> vector2 = this.f3389e;
            if (vector2 == null || vector2.size() == 0) {
                return null;
            }
            return new ArrayList<>(this.f3389e);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: f */
    public String mo12941f() {
        String b;
        if (this.f3393i.m3490c() && !this.f3392h.m3498c() && !this.f3392h.m3495b()) {
            return this.f3392h.m3491a();
        }
        this.f3393i.m3485a();
        if (this.f3386a == null) {
            return null;
        }
        ArrayList<String> d = mo12939d();
        JSONObject jSONObject = new JSONObject();
        if (d != null) {
            try {
                JSONArray jSONArray = new JSONArray();
                int i = 0;
                Iterator<String> it = d.iterator();
                while (it.hasNext()) {
                    String next = it.next();
                    if (next != null && !next.equals("data_version") && (b = this.f3386a.mo13553b(next)) != null && !b.equals("")) {
                        JSONObject optJSONObject = new JSONObject(b).optJSONObject("Fav_Sync");
                        optJSONObject.put("key", next);
                        jSONArray.put(i, optJSONObject);
                        i++;
                    }
                }
                if (i > 0) {
                    jSONObject.put("favcontents", jSONArray);
                    jSONObject.put("favpoinum", i);
                }
            } catch (JSONException unused) {
                return null;
            }
        }
        this.f3393i.m3487b();
        this.f3392h.m3493a(jSONObject.toString());
        return this.f3392h.m3491a();
    }
}
