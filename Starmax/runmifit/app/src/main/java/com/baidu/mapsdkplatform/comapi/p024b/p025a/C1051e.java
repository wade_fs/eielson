package com.baidu.mapsdkplatform.comapi.p024b.p025a;

import java.io.File;
import java.util.Comparator;

/* renamed from: com.baidu.mapsdkplatform.comapi.b.a.e */
public class C1051e implements Comparator {
    public int compare(Object obj, Object obj2) {
        try {
            return ((File) obj2).getName().split("_")[2].compareTo(((File) obj).getName().split("_")[2]);
        } catch (Exception unused) {
            return 0;
        }
    }
}
