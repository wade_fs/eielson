package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

public class TrainInfo extends TransitBaseInfo {
    public static final Parcelable.Creator<TrainInfo> CREATOR = new C0970n();

    /* renamed from: a */
    private double f2948a;

    /* renamed from: b */
    private String f2949b;

    public TrainInfo() {
    }

    protected TrainInfo(Parcel parcel) {
        super(parcel);
        this.f2948a = parcel.readDouble();
        this.f2949b = parcel.readString();
    }

    /* renamed from: a */
    public void mo11901a(double d) {
        this.f2948a = d;
    }

    /* renamed from: a */
    public void mo11902a(String str) {
        this.f2949b = str;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeDouble(this.f2948a);
        parcel.writeString(this.f2949b);
    }
}
