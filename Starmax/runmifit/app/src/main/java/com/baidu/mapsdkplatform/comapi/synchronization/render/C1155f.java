package com.baidu.mapsdkplatform.comapi.synchronization.render;

import android.os.HandlerThread;
import android.os.Message;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.synchronization.DisplayOptions;
import com.baidu.mapapi.synchronization.RoleOptions;
import com.baidu.mapsdkplatform.comapi.synchronization.data.C1135g;
import com.baidu.mapsdkplatform.comapi.synchronization.data.C1139i;
import com.baidu.mapsdkplatform.comapi.synchronization.data.SyncResponseResult;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.f */
public class C1155f {

    /* renamed from: a */
    private static final String f3848a = C1155f.class.getSimpleName();

    /* renamed from: d */
    private static HandlerThread f3849d;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public int f3850b;

    /* renamed from: c */
    private int f3851c;

    /* renamed from: e */
    private C1146b f3852e;

    /* renamed from: f */
    private C1135g f3853f;

    /* renamed from: g */
    private C1139i f3854g;

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.f$a */
    private class C1156a implements C1139i {
        private C1156a() {
        }

        /* renamed from: a */
        public void mo13306a() {
            C1155f.this.m4190j();
            C1155f fVar = C1155f.this;
            fVar.m4187e(fVar.f3850b);
        }

        /* renamed from: b */
        public void mo13307b() {
            C1155f.this.m4189i();
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.f$b */
    private static class C1157b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1155f f3856a = new C1155f();
    }

    private C1155f() {
        this.f3850b = 0;
        this.f3851c = 5;
    }

    /* renamed from: a */
    static C1155f m4182a() {
        return C1157b.f3856a;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m4187e(int i) {
        String str = f3848a;
        C1120a.m3856c(str, "The order state is: " + i);
        if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5) {
            m4188f(i);
            return;
        }
        String str2 = f3848a;
        C1120a.m3856c(str2, "Undefined order state: " + i);
    }

    /* renamed from: f */
    private void m4188f(int i) {
        C1146b bVar = this.f3852e;
        if (bVar == null) {
            C1120a.m3855b(f3848a, "SyncRenderHandler is null");
            return;
        }
        Message obtainMessage = bVar.obtainMessage();
        obtainMessage.what = i;
        this.f3852e.sendMessage(obtainMessage);
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m4189i() {
        RoleOptions e = this.f3853f.mo13301e();
        DisplayOptions f = this.f3853f.mo13302f();
        C1146b bVar = this.f3852e;
        if (bVar == null) {
            C1120a.m3855b(f3848a, "SyncRenderHandler is null");
        } else {
            bVar.mo13344a(e, f, (SyncResponseResult) null, this.f3851c);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m4190j() {
        SyncResponseResult syncResponseResult;
        RoleOptions e = this.f3853f.mo13301e();
        DisplayOptions f = this.f3853f.mo13302f();
        try {
            syncResponseResult = this.f3853f.mo13303g().take();
        } catch (InterruptedException e2) {
            C1120a.m3852a(f3848a, "Get result when InterruptedException happened.", e2);
            syncResponseResult = null;
        }
        C1146b bVar = this.f3852e;
        if (bVar == null) {
            C1120a.m3855b(f3848a, "SyncRenderHandler is null");
        } else {
            bVar.mo13344a(e, f, syncResponseResult, this.f3851c);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13370a(int i) {
        this.f3850b = i;
        m4187e(this.f3850b);
    }

    /* renamed from: a */
    public void mo13371a(int i, int i2, int i3, int i4) {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            bVar.mo13342a(i, i2, i3, i4);
        }
    }

    /* renamed from: a */
    public void mo13372a(BaiduMap baiduMap) {
        this.f3853f = C1135g.m3945a();
        this.f3854g = new C1156a();
        this.f3853f.mo13294a(this.f3854g);
        f3849d = new HandlerThread("SynchronizationRenderStrategy");
        f3849d.start();
        this.f3852e = new C1146b(f3849d.getLooper());
        this.f3852e.mo13343a(baiduMap, this.f3853f.mo13301e(), this.f3853f.mo13302f());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13373a(C1153d dVar) {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            bVar.mo13345a(dVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Marker mo13374b() {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            return bVar.mo13340a();
        }
        C1120a.m3855b(f3848a, "SyncRenderHandler created failed");
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13375b(int i) {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            bVar.mo13341a(i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public Marker mo13376c() {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            return bVar.mo13346b();
        }
        C1120a.m3855b(f3848a, "SyncRenderHandler created failed");
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo13377c(int i) {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            bVar.mo13347b(i);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public Marker mo13378d() {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            return bVar.mo13348c();
        }
        C1120a.m3855b(f3848a, "SyncRenderHandler created failed");
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void mo13379d(int i) {
        this.f3851c = i;
    }

    /* renamed from: e */
    public void mo13380e() {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            bVar.mo13349d();
        }
    }

    /* renamed from: f */
    public void mo13381f() {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            bVar.mo13350e();
        }
    }

    /* renamed from: g */
    public void mo13382g() {
        C1135g gVar = this.f3853f;
        if (gVar != null) {
            gVar.mo13298c();
        }
        if (this.f3854g != null) {
            this.f3854g = null;
        }
        C1135g gVar2 = this.f3853f;
        if (gVar2 != null) {
            gVar2.mo13304h();
            this.f3853f = null;
        }
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            bVar.mo13351f();
            this.f3852e.removeCallbacksAndMessages(null);
            this.f3852e = null;
        }
        HandlerThread handlerThread = f3849d;
        if (handlerThread != null) {
            handlerThread.quit();
            f3849d = null;
        }
    }

    /* renamed from: h */
    public void mo13383h() {
        C1146b bVar = this.f3852e;
        if (bVar != null) {
            bVar.mo13352g();
        }
    }
}
