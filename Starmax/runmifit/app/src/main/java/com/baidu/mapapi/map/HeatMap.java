package com.baidu.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v4.util.LongSparseArray;
import android.util.SparseIntArray;
import com.baidu.mapapi.model.LatLng;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

public class HeatMap {
    public static final Gradient DEFAULT_GRADIENT = new Gradient(f2492d, f2493e);
    public static final double DEFAULT_OPACITY = 0.6d;
    public static final int DEFAULT_RADIUS = 12;

    /* renamed from: b */
    private static final String f2490b = HeatMap.class.getSimpleName();

    /* renamed from: c */
    private static final SparseIntArray f2491c = new SparseIntArray();

    /* renamed from: d */
    private static final int[] f2492d = {Color.rgb(0, 0, 200), Color.rgb(0, 225, 0), Color.rgb(255, 0, 0)};

    /* renamed from: e */
    private static final float[] f2493e = {0.08f, 0.4f, 1.0f};

    /* renamed from: r */
    private static int f2494r = 0;

    /* renamed from: a */
    BaiduMap f2495a;

    /* renamed from: f */
    private C0938o<WeightedLatLng> f2496f;

    /* renamed from: g */
    private Collection<WeightedLatLng> f2497g;

    /* renamed from: h */
    private int f2498h;

    /* renamed from: i */
    private Gradient f2499i;

    /* renamed from: j */
    private double f2500j;

    /* renamed from: k */
    private C0931h f2501k;

    /* renamed from: l */
    private int[] f2502l;

    /* renamed from: m */
    private double[] f2503m;

    /* renamed from: n */
    private double[] f2504n;

    /* renamed from: o */
    private HashMap<String, Tile> f2505o;

    /* renamed from: p */
    private ExecutorService f2506p;

    /* renamed from: q */
    private HashSet<String> f2507q;

    public static class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public Collection<WeightedLatLng> f2508a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public int f2509b = 12;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public Gradient f2510c = HeatMap.DEFAULT_GRADIENT;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public double f2511d = 0.6d;

        public HeatMap build() {
            if (this.f2508a != null) {
                return new HeatMap(this, null);
            }
            throw new IllegalStateException("BDMapSDKException: No input data: you must use either .data or .weightedData before building");
        }

        public Builder data(Collection<LatLng> collection) {
            if (collection == null || collection.isEmpty()) {
                throw new IllegalArgumentException("BDMapSDKException: No input points.");
            } else if (!collection.contains(null)) {
                return weightedData(HeatMap.m2893c(collection));
            } else {
                throw new IllegalArgumentException("BDMapSDKException: input points can not contain null.");
            }
        }

        public Builder gradient(Gradient gradient) {
            if (gradient != null) {
                this.f2510c = gradient;
                return this;
            }
            throw new IllegalArgumentException("BDMapSDKException: gradient can not be null");
        }

        public Builder opacity(double d) {
            this.f2511d = d;
            double d2 = this.f2511d;
            if (d2 >= 0.0d && d2 <= 1.0d) {
                return this;
            }
            throw new IllegalArgumentException("BDMapSDKException: Opacity must be in range [0, 1]");
        }

        public Builder radius(int i) {
            this.f2509b = i;
            int i2 = this.f2509b;
            if (i2 >= 10 && i2 <= 50) {
                return this;
            }
            throw new IllegalArgumentException("BDMapSDKException: Radius not within bounds.");
        }

        public Builder weightedData(Collection<WeightedLatLng> collection) {
            if (collection == null || collection.isEmpty()) {
                throw new IllegalArgumentException("BDMapSDKException: No input points.");
            } else if (!collection.contains(null)) {
                ArrayList arrayList = new ArrayList();
                for (WeightedLatLng weightedLatLng : collection) {
                    LatLng latLng = weightedLatLng.latLng;
                    if (latLng.latitude < 0.37532d || latLng.latitude > 54.562495d || latLng.longitude < 72.508319d || latLng.longitude > 135.942198d) {
                        arrayList.add(weightedLatLng);
                    }
                }
                collection.removeAll(arrayList);
                this.f2508a = collection;
                return this;
            } else {
                throw new IllegalArgumentException("BDMapSDKException: input points can not contain null.");
            }
        }
    }

    static {
        f2491c.put(3, 8388608);
        f2491c.put(4, 4194304);
        f2491c.put(5, 2097152);
        f2491c.put(6, 1048576);
        f2491c.put(7, 524288);
        f2491c.put(8, 262144);
        f2491c.put(9, 131072);
        f2491c.put(10, 65536);
        f2491c.put(11, 32768);
        f2491c.put(12, 16384);
        f2491c.put(13, 8192);
        f2491c.put(14, 4096);
        f2491c.put(15, 2048);
        f2491c.put(16, 1024);
        f2491c.put(17, 512);
        f2491c.put(18, 256);
        f2491c.put(19, 128);
        f2491c.put(20, 64);
    }

    private HeatMap(Builder builder) {
        this.f2505o = new HashMap<>();
        this.f2506p = Executors.newFixedThreadPool(1);
        this.f2507q = new HashSet<>();
        this.f2497g = builder.f2508a;
        this.f2498h = builder.f2509b;
        this.f2499i = builder.f2510c;
        this.f2500j = builder.f2511d;
        int i = this.f2498h;
        double d = (double) i;
        Double.isNaN(d);
        this.f2503m = m2887a(i, d / 3.0d);
        m2882a(this.f2499i);
        m2891b(this.f2497g);
    }

    /* synthetic */ HeatMap(Builder builder, C0933j jVar) {
        this(builder);
    }

    /* renamed from: a */
    private static double m2878a(Collection<WeightedLatLng> collection, C0931h hVar, int i, int i2) {
        C0931h hVar2 = hVar;
        double d = hVar2.f2842a;
        double d2 = hVar2.f2844c;
        double d3 = hVar2.f2843b;
        double d4 = d2 - d;
        double d5 = hVar2.f2845d - d3;
        if (d4 <= d5) {
            d4 = d5;
        }
        double d6 = (double) (i2 / (i * 2));
        Double.isNaN(d6);
        double d7 = (double) ((int) (d6 + 0.5d));
        Double.isNaN(d7);
        double d8 = d7 / d4;
        LongSparseArray longSparseArray = new LongSparseArray();
        double d9 = 0.0d;
        for (WeightedLatLng weightedLatLng : collection) {
            double d10 = (double) weightedLatLng.mo11535a().x;
            double d11 = (double) weightedLatLng.mo11535a().y;
            Double.isNaN(d10);
            Double.isNaN(d11);
            int i3 = (int) ((d11 - d3) * d8);
            long j = (long) ((int) ((d10 - d) * d8));
            LongSparseArray longSparseArray2 = (LongSparseArray) longSparseArray.get(j);
            if (longSparseArray2 == null) {
                longSparseArray2 = new LongSparseArray();
                longSparseArray.put(j, longSparseArray2);
            }
            long j2 = (long) i3;
            Double d12 = (Double) longSparseArray2.get(j2);
            if (d12 == null) {
                d12 = Double.valueOf(0.0d);
            }
            LongSparseArray longSparseArray3 = longSparseArray;
            double d13 = d;
            Double valueOf = Double.valueOf(d12.doubleValue() + weightedLatLng.intensity);
            longSparseArray2.put(j2, valueOf);
            if (valueOf.doubleValue() > d9) {
                d9 = valueOf.doubleValue();
            }
            longSparseArray = longSparseArray3;
            d = d13;
        }
        return d9;
    }

    /* renamed from: a */
    private static Bitmap m2879a(double[][] dArr, int[] iArr, double d) {
        double[][] dArr2 = dArr;
        int[] iArr2 = iArr;
        int i = iArr2[iArr2.length - 1];
        double length = (double) (iArr2.length - 1);
        Double.isNaN(length);
        double d2 = length / d;
        int length2 = dArr2.length;
        int[] iArr3 = new int[(length2 * length2)];
        for (int i2 = 0; i2 < length2; i2++) {
            for (int i3 = 0; i3 < length2; i3++) {
                double d3 = dArr2[i3][i2];
                int i4 = (i2 * length2) + i3;
                int i5 = (int) (d3 * d2);
                if (d3 == 0.0d) {
                    iArr3[i4] = 0;
                } else if (i5 < iArr2.length) {
                    iArr3[i4] = iArr2[i5];
                } else {
                    iArr3[i4] = i;
                }
            }
        }
        Bitmap createBitmap = Bitmap.createBitmap(length2, length2, Bitmap.Config.ARGB_8888);
        createBitmap.setPixels(iArr3, 0, length2, 0, 0, length2, length2);
        return createBitmap;
    }

    /* renamed from: a */
    private static Tile m2880a(Bitmap bitmap) {
        ByteBuffer allocate = ByteBuffer.allocate(bitmap.getWidth() * bitmap.getHeight() * 4);
        bitmap.copyPixelsToBuffer(allocate);
        return new Tile(256, 256, allocate.array());
    }

    /* renamed from: a */
    private void m2882a(Gradient gradient) {
        this.f2499i = gradient;
        this.f2502l = gradient.mo11091a(this.f2500j);
    }

    /* renamed from: a */
    private synchronized void m2884a(String str, Tile tile) {
        this.f2505o.put(str, tile);
    }

    /* renamed from: a */
    private synchronized boolean m2885a(String str) {
        return this.f2507q.contains(str);
    }

    /* renamed from: a */
    private double[] m2886a(int i) {
        int i2;
        double[] dArr = new double[20];
        int i3 = 5;
        while (true) {
            if (i3 >= 11) {
                break;
            }
            dArr[i3] = m2878a(this.f2497g, this.f2501k, i, (int) (Math.pow(2.0d, (double) (i3 - 3)) * 1280.0d));
            if (i3 == 5) {
                for (int i4 = 0; i4 < i3; i4++) {
                    dArr[i4] = dArr[i3];
                }
            }
            i3++;
        }
        for (i2 = 11; i2 < 20; i2++) {
            dArr[i2] = dArr[10];
        }
        return dArr;
    }

    /* renamed from: a */
    private static double[] m2887a(int i, double d) {
        double[] dArr = new double[((i * 2) + 1)];
        for (int i2 = -i; i2 <= i; i2++) {
            double d2 = (double) ((-i2) * i2);
            Double.isNaN(d2);
            dArr[i2 + i] = Math.exp(d2 / ((2.0d * d) * d));
        }
        return dArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    /* renamed from: a */
    private static double[][] m2888a(double[][] dArr, double[] dArr2) {
        double[][] dArr3 = dArr;
        double[] dArr4 = dArr2;
        Class<double> cls = double.class;
        double length = (double) dArr4.length;
        Double.isNaN(length);
        int floor = (int) Math.floor(length / 2.0d);
        int length2 = dArr3.length;
        int i = length2 - (floor * 2);
        int i2 = (floor + i) - 1;
        double[][] dArr5 = (double[][]) Array.newInstance((Class<?>) cls, length2, length2);
        int i3 = 0;
        while (true) {
            double d = 0.0d;
            if (i3 >= length2) {
                break;
            }
            int i4 = 0;
            while (i4 < length2) {
                double d2 = dArr3[i3][i4];
                if (d2 != d) {
                    int i5 = i3 + floor;
                    if (i2 < i5) {
                        i5 = i2;
                    }
                    int i6 = i5 + 1;
                    int i7 = i3 - floor;
                    for (int i8 = floor > i7 ? floor : i7; i8 < i6; i8++) {
                        double[] dArr6 = dArr5[i8];
                        dArr6[i4] = dArr6[i4] + (dArr4[i8 - i7] * d2);
                    }
                }
                i4++;
                d = 0.0d;
            }
            i3++;
        }
        double[][] dArr7 = (double[][]) Array.newInstance((Class<?>) cls, i, i);
        for (int i9 = floor; i9 < i2 + 1; i9++) {
            for (int i10 = 0; i10 < length2; i10++) {
                double d3 = dArr5[i9][i10];
                if (d3 != 0.0d) {
                    int i11 = i10 + floor;
                    if (i2 < i11) {
                        i11 = i2;
                    }
                    int i12 = i11 + 1;
                    int i13 = i10 - floor;
                    for (int i14 = floor > i13 ? floor : i13; i14 < i12; i14++) {
                        double[] dArr8 = dArr7[i9 - floor];
                        int i15 = i14 - floor;
                        dArr8[i15] = dArr8[i15] + (dArr4[i14 - i13] * d3);
                    }
                }
            }
        }
        return dArr7;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m2889b(int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        double d = (double) f2491c.get(i6);
        int i7 = this.f2498h;
        double d2 = (double) i7;
        Double.isNaN(d);
        Double.isNaN(d2);
        double d3 = (d2 * d) / 256.0d;
        Double.isNaN(d);
        double d4 = (double) ((i7 * 2) + 256);
        Double.isNaN(d4);
        double d5 = ((2.0d * d3) + d) / d4;
        if (i4 >= 0 && i5 >= 0) {
            double d6 = (double) i4;
            Double.isNaN(d6);
            Double.isNaN(d);
            double d7 = (d6 * d) - d3;
            double d8 = (double) (i4 + 1);
            Double.isNaN(d8);
            Double.isNaN(d);
            double d9 = (d8 * d) + d3;
            double d10 = (double) i5;
            Double.isNaN(d10);
            Double.isNaN(d);
            double d11 = (d10 * d) - d3;
            double d12 = (double) (i5 + 1);
            Double.isNaN(d12);
            Double.isNaN(d);
            double d13 = (d12 * d) + d3;
            C0931h hVar = new C0931h(d7, d9, d11, d13);
            if (hVar.mo11568a(new C0931h(this.f2501k.f2842a - d3, this.f2501k.f2844c + d3, this.f2501k.f2843b - d3, this.f2501k.f2845d + d3))) {
                Collection<WeightedLatLng> a = this.f2496f.mo11578a(hVar);
                if (!a.isEmpty()) {
                    int i8 = this.f2498h;
                    double[][] dArr = (double[][]) Array.newInstance(double.class, (i8 * 2) + 256, (i8 * 2) + 256);
                    for (WeightedLatLng weightedLatLng : a) {
                        Point a2 = weightedLatLng.mo11535a();
                        double d14 = (double) a2.x;
                        Double.isNaN(d14);
                        int i9 = (int) ((d14 - d7) / d5);
                        double d15 = d7;
                        double d16 = (double) a2.y;
                        Double.isNaN(d16);
                        int i10 = (int) ((d13 - d16) / d5);
                        int i11 = this.f2498h;
                        if (i9 >= (i11 * 2) + 256) {
                            i9 = ((i11 * 2) + 256) - 1;
                        }
                        int i12 = this.f2498h;
                        if (i10 >= (i12 * 2) + 256) {
                            i10 = ((i12 * 2) + 256) - 1;
                        }
                        double[] dArr2 = dArr[i9];
                        dArr2[i10] = dArr2[i10] + weightedLatLng.intensity;
                        d7 = d15;
                        d13 = d13;
                    }
                    Bitmap a3 = m2879a(m2888a(dArr, this.f2503m), this.f2502l, this.f2504n[i6 - 1]);
                    Tile a4 = m2880a(a3);
                    a3.recycle();
                    m2884a(i4 + "_" + i5 + "_" + i6, a4);
                    if (this.f2505o.size() > f2494r) {
                        mo11129a();
                    }
                    BaiduMap baiduMap = this.f2495a;
                    if (baiduMap != null) {
                        baiduMap.mo10924a();
                    }
                }
            }
        }
    }

    /* renamed from: b */
    private synchronized void m2890b(String str) {
        this.f2507q.add(str);
    }

    /* renamed from: b */
    private void m2891b(Collection<WeightedLatLng> collection) {
        this.f2497g = collection;
        if (!this.f2497g.isEmpty()) {
            this.f2501k = m2894d(this.f2497g);
            this.f2496f = new C0938o<>(this.f2501k);
            for (WeightedLatLng weightedLatLng : this.f2497g) {
                this.f2496f.mo11579a(weightedLatLng);
            }
            this.f2504n = m2886a(this.f2498h);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: No input points.");
    }

    /* renamed from: c */
    private synchronized Tile m2892c(String str) {
        if (!this.f2505o.containsKey(str)) {
            return null;
        }
        Tile tile = this.f2505o.get(str);
        this.f2505o.remove(str);
        return tile;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static Collection<WeightedLatLng> m2893c(Collection<LatLng> collection) {
        ArrayList arrayList = new ArrayList();
        for (LatLng latLng : collection) {
            arrayList.add(new WeightedLatLng(latLng));
        }
        return arrayList;
    }

    /* renamed from: d */
    private static C0931h m2894d(Collection<WeightedLatLng> collection) {
        Iterator<WeightedLatLng> it = collection.iterator();
        WeightedLatLng next = it.next();
        double d = (double) next.mo11535a().x;
        double d2 = (double) next.mo11535a().x;
        double d3 = (double) next.mo11535a().y;
        double d4 = (double) next.mo11535a().y;
        while (it.hasNext()) {
            WeightedLatLng next2 = it.next();
            double d5 = (double) next2.mo11535a().x;
            double d6 = (double) next2.mo11535a().y;
            if (d5 < d) {
                d = d5;
            }
            if (d5 > d2) {
                d2 = d5;
            }
            if (d6 < d3) {
                d3 = d6;
            }
            if (d6 > d4) {
                d4 = d6;
            }
        }
        return new C0931h(d, d2, d3, d4);
    }

    /* renamed from: d */
    private synchronized void m2895d() {
        this.f2505o.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Tile mo11128a(int i, int i2, int i3) {
        String str = i + "_" + i2 + "_" + i3;
        Tile c = m2892c(str);
        if (c != null) {
            return c;
        }
        if (m2885a(str)) {
            return null;
        }
        BaiduMap baiduMap = this.f2495a;
        if (baiduMap != null && f2494r == 0) {
            MapStatus mapStatus = baiduMap.getMapStatus();
            f2494r = (((mapStatus.f2538a.f3423j.right - mapStatus.f2538a.f3423j.left) / 256) + 2) * (((mapStatus.f2538a.f3423j.bottom - mapStatus.f2538a.f3423j.top) / 256) + 2) * 4;
        }
        if (this.f2505o.size() > f2494r) {
            mo11129a();
        }
        if (this.f2506p.isShutdown()) {
            return null;
        }
        try {
            this.f2506p.execute(new C0933j(this, i, i2, i3));
            m2890b(str);
            return null;
        } catch (RejectedExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo11129a() {
        this.f2507q.clear();
        this.f2505o.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo11130b() {
        m2895d();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo11131c() {
        this.f2506p.shutdownNow();
    }

    public void removeHeatMap() {
        BaiduMap baiduMap = this.f2495a;
        if (baiduMap != null) {
            baiduMap.mo10925a(this);
        }
    }
}
