package com.baidu.mapapi.search.busline;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.RouteNode;
import com.baidu.mapapi.search.core.RouteStep;
import com.baidu.mapapi.search.core.SearchResult;
import java.util.Date;
import java.util.List;

public class BusLineResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<BusLineResult> CREATOR = new C0956a();

    /* renamed from: a */
    private String f2901a = null;

    /* renamed from: b */
    private String f2902b = null;

    /* renamed from: c */
    private boolean f2903c;

    /* renamed from: d */
    private Date f2904d;

    /* renamed from: e */
    private Date f2905e;

    /* renamed from: f */
    private String f2906f;

    /* renamed from: g */
    private List<BusStation> f2907g = null;

    /* renamed from: h */
    private List<BusStep> f2908h = null;

    /* renamed from: i */
    private float f2909i;

    /* renamed from: j */
    private float f2910j;

    /* renamed from: k */
    private String f2911k = null;

    public static class BusStation extends RouteNode {
    }

    public static class BusStep extends RouteStep {
    }

    public BusLineResult() {
    }

    BusLineResult(Parcel parcel) {
        this.f2901a = parcel.readString();
        this.f2902b = parcel.readString();
        this.f2903c = ((Boolean) parcel.readValue(Boolean.class.getClassLoader())).booleanValue();
        this.f2904d = (Date) parcel.readValue(Date.class.getClassLoader());
        this.f2905e = (Date) parcel.readValue(Date.class.getClassLoader());
        this.f2906f = parcel.readString();
        this.f2907g = parcel.readArrayList(BusStation.class.getClassLoader());
        this.f2908h = parcel.readArrayList(RouteStep.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public float getBasePrice() {
        return this.f2909i;
    }

    public String getBusCompany() {
        return this.f2901a;
    }

    public String getBusLineName() {
        return this.f2902b;
    }

    public Date getEndTime() {
        return this.f2905e;
    }

    public String getLineDirection() {
        return this.f2911k;
    }

    public float getMaxPrice() {
        return this.f2910j;
    }

    public Date getStartTime() {
        return this.f2904d;
    }

    public List<BusStation> getStations() {
        return this.f2907g;
    }

    public List<BusStep> getSteps() {
        return this.f2908h;
    }

    public String getUid() {
        return this.f2906f;
    }

    public boolean isMonthTicket() {
        return this.f2903c;
    }

    public void setBasePrice(float f) {
        this.f2909i = f;
    }

    public void setBusLineName(String str) {
        this.f2902b = str;
    }

    public void setEndTime(Date date) {
        this.f2905e = date;
    }

    public void setLineDirection(String str) {
        this.f2911k = str;
    }

    public void setMaxPrice(float f) {
        this.f2910j = f;
    }

    public void setMonthTicket(boolean z) {
        this.f2903c = z;
    }

    public void setStartTime(Date date) {
        this.f2904d = date;
    }

    public void setStations(List<BusStation> list) {
        this.f2907g = list;
    }

    public void setSteps(List<BusStep> list) {
        this.f2908h = list;
    }

    public void setUid(String str) {
        this.f2906f = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2901a);
        parcel.writeString(this.f2902b);
        parcel.writeValue(Boolean.valueOf(this.f2903c));
        parcel.writeValue(this.f2904d);
        parcel.writeValue(this.f2905e);
        parcel.writeString(this.f2906f);
        parcel.writeList(this.f2907g);
        parcel.writeList(this.f2908h);
    }
}
