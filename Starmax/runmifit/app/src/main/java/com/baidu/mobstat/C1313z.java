package com.baidu.mobstat;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.baidu.mobstat.C1262ay;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.download.MimeType;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Comparator;
import java.util.zip.GZIPOutputStream;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.z */
public class C1313z {

    /* renamed from: a */
    private static String f4395a = (Build.VERSION.SDK_INT < 9 ? "http://openrcv.baidu.com/1010/bplus.gif" : "https://openrcv.baidu.com/1010/bplus.gif");

    /* renamed from: b */
    private static C1313z f4396b;

    /* renamed from: c */
    private Handler f4397c;

    private C1313z() {
        HandlerThread handlerThread = new HandlerThread("LogSender");
        handlerThread.start();
        this.f4397c = new Handler(handlerThread.getLooper());
    }

    /* renamed from: a */
    public static C1313z m4882a() {
        if (f4396b == null) {
            synchronized (C1313z.class) {
                if (f4396b == null) {
                    f4396b = new C1313z();
                }
            }
        }
        return f4396b;
    }

    /* renamed from: a */
    public void mo14012a(final Context context, final String str) {
        C1255as c = C1255as.m4626c();
        c.mo13941a("data = " + str);
        if (str != null && !"".equals(str)) {
            this.f4397c.post(new Runnable() {
                /* class com.baidu.mobstat.C1313z.C13141 */

                public void run() {
                    try {
                        C1313z.this.m4887a(str);
                        if (context != null) {
                            C1313z.this.m4884a(context.getApplicationContext());
                        }
                    } catch (Throwable th) {
                        C1255as.m4626c().mo13944b(th);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m4887a(String str) {
        C1267ba.m4682a("backups/system" + File.separator + "__send_log_data_" + System.currentTimeMillis(), str, false);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m4884a(Context context) {
        File[] listFiles;
        if ("mounted".equals(C1267ba.m4678a())) {
            File file = new File(Environment.getExternalStorageDirectory(), "backups/system");
            if (file.exists() && (listFiles = file.listFiles()) != null && listFiles.length != 0) {
                try {
                    Arrays.sort(listFiles, new Comparator<File>() {
                        /* class com.baidu.mobstat.C1313z.C13152 */

                        /* renamed from: a */
                        public int compare(File file, File file2) {
                            return (int) (file2.lastModified() - file.lastModified());
                        }
                    });
                } catch (Exception e) {
                    C1255as.m4626c().mo13944b(e);
                }
                int i = 0;
                for (File file2 : listFiles) {
                    if (file2.isFile()) {
                        String name = file2.getName();
                        if (!TextUtils.isEmpty(name) && name.startsWith("__send_log_data_")) {
                            String str = "backups/system" + File.separator + name;
                            String b = C1267ba.m4684b(str);
                            if (m4890b(context, b)) {
                                C1267ba.m4687c(str);
                                i = 0;
                            } else {
                                m4888a(b, str);
                                i++;
                                if (i >= 5) {
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: a */
    private void m4888a(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            JSONObject jSONObject = null;
            try {
                jSONObject = new JSONObject(str);
            } catch (Exception unused) {
            }
            JSONObject a = C1297o.m4827a(jSONObject);
            if (a != null) {
                C1297o.m4828b(a);
                C1267ba.m4682a(str2, jSONObject.toString(), false);
            }
        }
    }

    /* renamed from: b */
    private boolean m4890b(Context context, String str) {
        if (context != null && !TextUtils.isEmpty(str)) {
            try {
                m4883a(context, f4395a, str);
                return true;
            } catch (Exception e) {
                C1255as.m4626c().mo13947c(e);
            }
        }
        return false;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    private String m4883a(Context context, String str, String str2) throws IOException {
        byte[] bArr;
        String str3 = str2;
        boolean z = !str.startsWith("https:");
        HttpURLConnection d = C1267ba.m4688d(context, str);
        d.setDoOutput(true);
        d.setInstanceFollowRedirects(false);
        d.setUseCaches(false);
        d.setRequestProperty(HttpHeaders.CONTENT_ENCODING, MimeType.GZIP);
        d.connect();
        try {
            OutputStream outputStream = d.getOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
            gZIPOutputStream.write(new byte[]{72, 77, 48, 49});
            gZIPOutputStream.write(new byte[]{0, 0, 0, 1});
            gZIPOutputStream.write(new byte[]{0, 0, 3, -14});
            gZIPOutputStream.write(new byte[]{0, 0, 0, 0, 0, 0, 0, 0});
            gZIPOutputStream.write(new byte[]{0, 2});
            if (z) {
                gZIPOutputStream.write(new byte[]{0, 1});
            } else {
                gZIPOutputStream.write(new byte[]{0, 0});
            }
            gZIPOutputStream.write(new byte[]{72, 77, 48, 49});
            if (z) {
                byte[] a = C1262ay.C1263a.m4666a();
                byte[] a2 = C1275bg.m4709a(false, C1269bc.m4694a(), a);
                gZIPOutputStream.write(m4889a((long) a2.length, 4));
                gZIPOutputStream.write(a2);
                bArr = C1262ay.C1263a.m4667a(a, new byte[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, str3.getBytes("utf-8"));
                gZIPOutputStream.write(m4889a((long) bArr.length, 2));
            } else {
                bArr = str3.getBytes("utf-8");
            }
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            outputStream.close();
            int responseCode = d.getResponseCode();
            int contentLength = d.getContentLength();
            C1255as.m4626c().mo13946c("code: " + responseCode + "; len: " + contentLength);
            if (responseCode == 200 && contentLength == 0) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(d.getInputStream()));
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        String sb2 = sb.toString();
                        d.disconnect();
                        return sb2;
                    }
                    sb.append(readLine);
                }
            } else {
                throw new IOException("Response code = " + d.getResponseCode());
            }
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            d.disconnect();
            return "";
        } catch (Throwable th) {
            d.disconnect();
            throw th;
        }
    }

    /* renamed from: a */
    private static byte[] m4889a(long j, int i) {
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            bArr[(i - i2) - 1] = (byte) ((int) (255 & j));
            j >>= 8;
        }
        return bArr;
    }
}
