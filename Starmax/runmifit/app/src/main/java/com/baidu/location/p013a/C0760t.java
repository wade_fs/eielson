package com.baidu.location.p013a;

import android.location.Location;
import com.baidu.location.p017e.C0821a;

/* renamed from: com.baidu.location.a.t */
public class C0760t {

    /* renamed from: a */
    private static long f1426a;

    /* renamed from: b */
    private static C0821a f1427b;

    /* renamed from: c */
    private static Location f1428c;

    /* renamed from: d */
    private static String f1429d;

    /* renamed from: a */
    public static String m1934a() {
        return f1429d;
    }

    /* renamed from: a */
    public static void m1935a(long j) {
        f1426a = j;
    }

    /* renamed from: a */
    public static void m1936a(Location location) {
        f1428c = location;
    }

    /* renamed from: a */
    public static void m1937a(C0821a aVar) {
        f1427b = aVar;
    }

    /* renamed from: a */
    public static void m1938a(String str) {
        f1429d = str;
    }

    /* renamed from: b */
    public static long m1939b() {
        return f1426a;
    }

    /* renamed from: c */
    public static C0821a m1940c() {
        return f1427b;
    }

    /* renamed from: d */
    public static Location m1941d() {
        return f1428c;
    }
}
