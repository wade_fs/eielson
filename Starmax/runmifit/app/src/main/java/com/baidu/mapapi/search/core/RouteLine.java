package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.RouteStep;
import com.baidu.mapapi.search.route.BikingRouteLine;
import com.baidu.mapapi.search.route.DrivingRouteLine;
import com.baidu.mapapi.search.route.TransitRouteLine;
import com.baidu.mapapi.search.route.WalkingRouteLine;
import java.util.List;

public class RouteLine<T extends RouteStep> implements Parcelable {

    /* renamed from: a */
    TYPE f2927a;

    /* renamed from: b */
    private RouteNode f2928b;

    /* renamed from: c */
    private RouteNode f2929c;

    /* renamed from: d */
    private String f2930d;

    /* renamed from: e */
    private List<T> f2931e;

    /* renamed from: f */
    private int f2932f;

    /* renamed from: g */
    private int f2933g;

    protected enum TYPE {
        DRIVESTEP(0),
        TRANSITSTEP(1),
        WALKSTEP(2),
        BIKINGSTEP(3);
        

        /* renamed from: a */
        private int f2935a;

        private TYPE(int i) {
            this.f2935a = i;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public int m3137a() {
            return this.f2935a;
        }
    }

    protected RouteLine() {
    }

    protected RouteLine(Parcel parcel) {
        Object obj;
        int readInt = parcel.readInt();
        this.f2928b = (RouteNode) parcel.readValue(RouteNode.class.getClassLoader());
        this.f2929c = (RouteNode) parcel.readValue(RouteNode.class.getClassLoader());
        this.f2930d = parcel.readString();
        if (readInt == 0) {
            obj = DrivingRouteLine.DrivingStep.CREATOR;
        } else if (readInt == 1) {
            obj = TransitRouteLine.TransitStep.CREATOR;
        } else if (readInt != 2) {
            if (readInt == 3) {
                obj = BikingRouteLine.BikingStep.CREATOR;
            }
            this.f2932f = parcel.readInt();
            this.f2933g = parcel.readInt();
        } else {
            obj = WalkingRouteLine.WalkingStep.CREATOR;
        }
        this.f2931e = parcel.createTypedArrayList(obj);
        this.f2932f = parcel.readInt();
        this.f2933g = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public List<T> getAllStep() {
        return this.f2931e;
    }

    public int getDistance() {
        return this.f2932f;
    }

    public int getDuration() {
        return this.f2933g;
    }

    public RouteNode getStarting() {
        return this.f2928b;
    }

    public RouteNode getTerminal() {
        return this.f2929c;
    }

    public String getTitle() {
        return this.f2930d;
    }

    /* access modifiers changed from: protected */
    public TYPE getType() {
        return this.f2927a;
    }

    public void setDistance(int i) {
        this.f2932f = i;
    }

    public void setDuration(int i) {
        this.f2933g = i;
    }

    public void setStarting(RouteNode routeNode) {
        this.f2928b = routeNode;
    }

    public void setSteps(List<T> list) {
        this.f2931e = list;
    }

    public void setTerminal(RouteNode routeNode) {
        this.f2929c = routeNode;
    }

    public void setTitle(String str) {
        this.f2930d = str;
    }

    /* access modifiers changed from: protected */
    public void setType(TYPE type) {
        this.f2927a = type;
    }

    public void writeToParcel(Parcel parcel, int i) {
        TYPE type = this.f2927a;
        parcel.writeInt(type != null ? type.m3137a() : 10);
        parcel.writeValue(this.f2928b);
        parcel.writeValue(this.f2929c);
        parcel.writeString(this.f2930d);
        if (this.f2927a != null) {
            parcel.writeTypedList(this.f2931e);
        }
        parcel.writeInt(this.f2932f);
        parcel.writeInt(this.f2933g);
    }
}
