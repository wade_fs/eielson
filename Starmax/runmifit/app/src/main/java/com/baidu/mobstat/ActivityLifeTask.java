package com.baidu.mobstat;

import android.content.Context;
import com.baidu.mobstat.ActivityLifeObserver;
import com.baidu.mobstat.AutoTrack;
import com.baidu.mobstat.C1243am;
import com.baidu.mobstat.C1248ao;

public class ActivityLifeTask {

    /* renamed from: a */
    private static boolean f3967a = false;

    /* renamed from: b */
    private static ActivityLifeObserver.IActivityLifeCallback f3968b;

    /* renamed from: c */
    private static ActivityLifeObserver.IActivityLifeCallback f3969c;

    /* renamed from: d */
    private static ActivityLifeObserver.IActivityLifeCallback f3970d;

    /* renamed from: e */
    private static ActivityLifeObserver.IActivityLifeCallback f3971e;

    public static synchronized void registerActivityLifeCallback(Context context) {
        synchronized (ActivityLifeTask.class) {
            if (!f3967a) {
                m4386a(context);
                ActivityLifeObserver.instance().clearObservers();
                ActivityLifeObserver.instance().addObserver(f3968b);
                ActivityLifeObserver.instance().addObserver(f3971e);
                ActivityLifeObserver.instance().registerActivityLifeCallback(context);
                f3967a = true;
            }
        }
    }

    /* renamed from: a */
    private static synchronized void m4386a(Context context) {
        synchronized (ActivityLifeTask.class) {
            f3968b = new AutoTrack.MyActivityLifeCallback(1);
            f3970d = new C1243am.C1244a();
            f3969c = new C1248ao.C1249a();
            f3971e = new AutoTrack.MyActivityLifeCallback(2);
        }
    }
}
