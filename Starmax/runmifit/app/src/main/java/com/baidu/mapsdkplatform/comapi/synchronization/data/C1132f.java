package com.baidu.mapsdkplatform.comapi.synchronization.data;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.f */
public final class C1132f {

    /* renamed from: a */
    private String f3736a;

    /* renamed from: b */
    private String f3737b;

    /* renamed from: c */
    private String f3738c;

    /* renamed from: d */
    private C1134b f3739d;

    /* renamed from: e */
    private int f3740e;

    /* renamed from: f */
    private int f3741f;

    /* renamed from: g */
    private C1133a f3742g;

    /* renamed from: h */
    private String f3743h;

    /* renamed from: i */
    private String f3744i;

    /* renamed from: j */
    private int f3745j;

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.f$a */
    public enum C1133a {
        GPS,
        COMMON,
        BD09LL,
        BD09MC
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.f$b */
    public enum C1134b {
        DRIVING,
        RIDING
    }

    public C1132f() {
        this.f3739d = C1134b.DRIVING;
        this.f3741f = 15;
        this.f3742g = C1133a.BD09LL;
        this.f3739d = C1134b.DRIVING;
        this.f3742g = C1133a.BD09LL;
        this.f3741f = 15;
    }

    /* renamed from: a */
    public String mo13274a() {
        return this.f3736a;
    }

    /* renamed from: a */
    public void mo13275a(int i) {
        this.f3740e = i;
    }

    /* renamed from: a */
    public void mo13276a(String str) {
        this.f3736a = str;
    }

    /* renamed from: b */
    public String mo13277b() {
        return this.f3737b;
    }

    /* renamed from: b */
    public void mo13278b(String str) {
        this.f3737b = str;
    }

    /* renamed from: c */
    public String mo13279c() {
        return this.f3738c;
    }

    /* renamed from: c */
    public void mo13280c(String str) {
        this.f3738c = str;
    }

    /* renamed from: d */
    public String mo13281d() {
        return this.f3743h;
    }

    /* renamed from: d */
    public void mo13282d(String str) {
        this.f3743h = str;
    }

    /* renamed from: e */
    public String mo13283e() {
        return this.f3744i;
    }

    /* renamed from: e */
    public void mo13284e(String str) {
        this.f3744i = str;
    }

    /* renamed from: f */
    public C1133a mo13285f() {
        return this.f3742g;
    }

    /* renamed from: g */
    public C1134b mo13286g() {
        return this.f3739d;
    }

    /* renamed from: h */
    public int mo13287h() {
        return this.f3740e;
    }

    /* renamed from: i */
    public int mo13288i() {
        return this.f3741f;
    }

    /* renamed from: j */
    public int mo13289j() {
        return this.f3745j;
    }
}
