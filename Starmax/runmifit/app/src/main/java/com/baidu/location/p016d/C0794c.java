package com.baidu.location.p016d;

import android.database.sqlite.SQLiteDatabase;
import com.baidu.location.BDLocation;
import com.baidu.location.Jni;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0849e;
import com.baidu.mobstat.Config;
import com.tencent.open.SocialConstants;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

/* renamed from: com.baidu.location.d.c */
final class C0794c {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final C0804h f1581a;

    /* renamed from: b */
    private int f1582b;

    /* renamed from: c */
    private double f1583c;

    /* renamed from: d */
    private double f1584d;

    /* renamed from: e */
    private Long f1585e;

    /* renamed from: f */
    private final C0797c f1586f = new C0797c(this, true);
    /* access modifiers changed from: private */

    /* renamed from: g */
    public final C0797c f1587g = new C0797c(this, false);
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final SQLiteDatabase f1588h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public final SQLiteDatabase f1589i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public StringBuffer f1590j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public StringBuffer f1591k;

    /* renamed from: l */
    private HashSet<Long> f1592l;

    /* renamed from: m */
    private ConcurrentHashMap<Long, Integer> f1593m;

    /* renamed from: n */
    private ConcurrentHashMap<Long, String> f1594n;

    /* renamed from: o */
    private StringBuffer f1595o = new StringBuffer();

    /* renamed from: p */
    private boolean f1596p = false;

    /* renamed from: com.baidu.location.d.c$a */
    private static final class C0795a {

        /* renamed from: a */
        double f1597a;

        /* renamed from: b */
        double f1598b;

        /* renamed from: c */
        double f1599c;

        private C0795a(double d, double d2, double d3) {
            this.f1597a = d;
            this.f1598b = d2;
            this.f1599c = d3;
        }

        /* synthetic */ C0795a(double d, double d2, double d3, C0798d dVar) {
            this(d, d2, d3);
        }
    }

    /* renamed from: com.baidu.location.d.c$b */
    private class C0796b extends Thread {

        /* renamed from: a */
        private String f1600a;

        /* renamed from: c */
        private Long f1602c;

        /* renamed from: d */
        private BDLocation f1603d;

        /* renamed from: e */
        private BDLocation f1604e;

        /* renamed from: f */
        private BDLocation f1605f;

        /* renamed from: g */
        private String f1606g;

        /* renamed from: h */
        private LinkedHashMap<String, Integer> f1607h;

        private C0796b(String str, Long l, BDLocation bDLocation, BDLocation bDLocation2, BDLocation bDLocation3, String str2, LinkedHashMap<String, Integer> linkedHashMap) {
            this.f1600a = str;
            this.f1602c = l;
            this.f1603d = bDLocation;
            this.f1604e = bDLocation2;
            this.f1605f = bDLocation3;
            this.f1606g = str2;
            this.f1607h = linkedHashMap;
        }

        /* synthetic */ C0796b(C0794c cVar, String str, Long l, BDLocation bDLocation, BDLocation bDLocation2, BDLocation bDLocation3, String str2, LinkedHashMap linkedHashMap, C0798d dVar) {
            this(str, l, bDLocation, bDLocation2, bDLocation3, str2, linkedHashMap);
        }

        public void run() {
            try {
                C0794c.this.m2095a(this.f1600a, this.f1602c, this.f1603d);
                StringBuffer unused = C0794c.this.f1590j = (StringBuffer) null;
                StringBuffer unused2 = C0794c.this.f1591k = null;
                C0794c.this.m2097a(this.f1607h);
                C0794c.this.m2089a(this.f1605f, this.f1603d, this.f1604e, this.f1600a, this.f1602c);
                if (this.f1606g != null) {
                    C0794c.this.f1581a.mo10599j().mo10608a(this.f1606g);
                }
            } catch (Exception unused3) {
            }
            this.f1607h = null;
            this.f1600a = null;
            this.f1606g = null;
            this.f1602c = null;
            this.f1603d = null;
            this.f1604e = null;
            this.f1605f = null;
        }
    }

    /* renamed from: com.baidu.location.d.c$c */
    private final class C0797c extends C0849e {

        /* renamed from: b */
        private String f1609b;

        /* renamed from: c */
        private final String f1610c;

        /* renamed from: d */
        private String f1611d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public C0794c f1612e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public boolean f1613f = false;

        /* renamed from: q */
        private int f1614q = 0;

        /* renamed from: r */
        private long f1615r = -1;
        /* access modifiers changed from: private */

        /* renamed from: s */
        public long f1616s = -1;

        /* renamed from: t */
        private long f1617t = -1;

        /* renamed from: u */
        private long f1618u = -1;

        C0797c(C0794c cVar, boolean z) {
            this.f1612e = cVar;
            this.f1610c = z ? "load" : "update";
            this.f1885k = new HashMap();
            this.f1609b = C0804h.f1655b;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.g.e.a(java.util.concurrent.ExecutorService, boolean, java.lang.String):void
         arg types: [java.util.concurrent.ExecutorService, int, java.lang.String]
         candidates:
          com.baidu.location.d.c.c.a(java.lang.String, java.lang.String, java.lang.String):void
          com.baidu.location.g.e.a(java.util.concurrent.ExecutorService, boolean, java.lang.String):void */
        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m2109a(String str, String str2, String str3) {
            this.f1611d = str3;
            this.f1609b = String.format("http://%s/%s", str, str2);
            mo10732a(C0762v.m1943a().mo10505c(), false, "ofloc.map.baidu.com");
        }

        /* access modifiers changed from: private */
        /* renamed from: c */
        public void m2112c() {
            this.f1614q++;
            this.f1615r = System.currentTimeMillis();
        }

        /* renamed from: d */
        private boolean m2113d() {
            if (this.f1614q < 2) {
                return true;
            }
            if (this.f1615r + 43200000 >= System.currentTimeMillis()) {
                return false;
            }
            this.f1614q = 0;
            this.f1615r = -1;
            return true;
        }

        /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x005a  */
        /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
        /* renamed from: e */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void m2114e() {
            /*
                r9 = this;
                r0 = 0
                r9.f1611d = r0
                boolean r0 = r9.m2119j()
                r1 = 86400000(0x5265c00, double:4.2687272E-316)
                r3 = -1
                if (r0 == 0) goto L_0x0022
                long r5 = r9.f1616s
                int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
                if (r0 == 0) goto L_0x001d
                long r5 = r5 + r1
                long r7 = java.lang.System.currentTimeMillis()
                int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                if (r0 > 0) goto L_0x0028
            L_0x001d:
                java.lang.String r0 = r9.m2115f()
                goto L_0x0026
            L_0x0022:
                java.lang.String r0 = r9.m2116g()
            L_0x0026:
                r9.f1611d = r0
            L_0x0028:
                java.lang.String r0 = r9.f1611d
                if (r0 != 0) goto L_0x0056
                long r5 = r9.f1617t
                int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
                if (r0 == 0) goto L_0x003b
                long r5 = r5 + r1
                long r0 = java.lang.System.currentTimeMillis()
                int r2 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
                if (r2 > 0) goto L_0x0056
            L_0x003b:
                com.baidu.location.d.c r0 = com.baidu.location.p016d.C0794c.this
                com.baidu.location.d.h r0 = r0.f1581a
                com.baidu.location.d.l r0 = r0.mo10600k()
                boolean r0 = r0.mo10611a()
                if (r0 == 0) goto L_0x0050
                java.lang.String r0 = r9.m2117h()
                goto L_0x0054
            L_0x0050:
                java.lang.String r0 = r9.m2118i()
            L_0x0054:
                r9.f1611d = r0
            L_0x0056:
                java.lang.String r0 = r9.f1611d
                if (r0 == 0) goto L_0x006d
                com.baidu.location.a.v r0 = com.baidu.location.p013a.C0762v.m1943a()
                java.util.concurrent.ExecutorService r0 = r0.mo10505c()
                java.lang.String r1 = "https://ofloc.map.baidu.com/offline_loc"
                if (r0 == 0) goto L_0x006a
                r9.mo10731a(r0, r1)
                goto L_0x006d
            L_0x006a:
                r9.mo10734c(r1)
            L_0x006d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.C0797c.m2114e():void");
        }

        /* renamed from: f */
        private String m2115f() {
            JSONObject jSONObject;
            try {
                jSONObject = new JSONObject();
                jSONObject.put(SocialConstants.PARAM_TYPE, "0");
                jSONObject.put("cuid", C0844b.m2417a().f1856c);
                jSONObject.put("ver", "1");
                jSONObject.put("prod", C0844b.f1849f + Config.TRACE_TODAY_VISIT_SPLIT + C0844b.f1848e);
            } catch (Exception unused) {
                jSONObject = null;
            }
            if (jSONObject != null) {
                return Jni.encodeOfflineLocationUpdateRequest(jSONObject.toString());
            }
            return null;
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v2, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v4, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v5, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v8, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v9, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v10, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v12, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v13, resolved type: android.database.Cursor} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v18, resolved type: java.lang.String} */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f4, code lost:
            if (r7 != null) goto L_0x00f6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
            r7.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x010c, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x010d, code lost:
            r7 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x011c, code lost:
            r8 = null;
            r6 = null;
            r7 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x0126, code lost:
            if (r7 != null) goto L_0x00f6;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x008f A[SYNTHETIC, Splitter:B:25:0x008f] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00ef A[SYNTHETIC, Splitter:B:38:0x00ef] */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x010c A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0009] */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x0111 A[SYNTHETIC, Splitter:B:60:0x0111] */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0118 A[SYNTHETIC, Splitter:B:64:0x0118] */
        /* JADX WARNING: Removed duplicated region for block: B:71:0x0121 A[SYNTHETIC, Splitter:B:71:0x0121] */
        /* renamed from: g */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private java.lang.String m2116g() {
            /*
                r17 = this;
                r1 = r17
                java.lang.String r0 = "1"
                java.lang.String r2 = "SELECT * FROM %s WHERE frequency>%d ORDER BY frequency DESC LIMIT %d;"
                java.lang.String r3 = "model"
                r4 = 0
                org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x011c, all -> 0x010c }
                r5.<init>()     // Catch:{ Exception -> 0x011c, all -> 0x010c }
                org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x011c, all -> 0x010c }
                r6.<init>()     // Catch:{ Exception -> 0x011c, all -> 0x010c }
                com.baidu.location.d.c r7 = com.baidu.location.p016d.C0794c.this     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                android.database.sqlite.SQLiteDatabase r7 = r7.f1589i     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                r8 = 3
                java.lang.Object[] r9 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                java.lang.String r10 = "CL"
                r11 = 0
                r9[r11] = r10     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                r10 = 5
                java.lang.Integer r12 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                r13 = 1
                r9[r13] = r12     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                r12 = 50
                java.lang.Integer r14 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                r15 = 2
                r9[r15] = r14     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                java.lang.String r9 = java.lang.String.format(r2, r9)     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                android.database.Cursor r7 = r7.rawQuery(r9, r4)     // Catch:{ Exception -> 0x0109, all -> 0x010c }
                if (r7 == 0) goto L_0x006b
                boolean r9 = r7.moveToFirst()     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
                if (r9 == 0) goto L_0x006b
                int r9 = r7.getCount()     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
                org.json.JSONArray r14 = new org.json.JSONArray     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
                r14.<init>()     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
            L_0x004b:
                boolean r16 = r7.isAfterLast()     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
                if (r16 != 0) goto L_0x005d
                java.lang.String r4 = r7.getString(r13)     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
                r14.put(r4)     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
                r7.moveToNext()     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
                r4 = 0
                goto L_0x004b
            L_0x005d:
                java.lang.String r4 = "cell"
                r5.put(r4, r14)     // Catch:{ Exception -> 0x0067, all -> 0x0063 }
                goto L_0x006c
            L_0x0063:
                r0 = move-exception
                r4 = 0
                goto L_0x010f
            L_0x0067:
                r4 = 0
                r8 = 0
                goto L_0x011f
            L_0x006b:
                r9 = 0
            L_0x006c:
                com.baidu.location.d.c r4 = com.baidu.location.p016d.C0794c.this     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                android.database.sqlite.SQLiteDatabase r4 = r4.f1589i     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                java.lang.String r14 = "AP"
                r8[r11] = r14     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                r8[r13] = r10     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                java.lang.Integer r10 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                r8[r15] = r10     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                java.lang.String r2 = java.lang.String.format(r2, r8)     // Catch:{ Exception -> 0x0106, all -> 0x0102 }
                r8 = 0
                android.database.Cursor r4 = r4.rawQuery(r2, r8)     // Catch:{ Exception -> 0x0107, all -> 0x0100 }
                if (r4 == 0) goto L_0x00b4
                boolean r2 = r4.moveToFirst()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                if (r2 == 0) goto L_0x00b4
                int r11 = r4.getCount()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                r2.<init>()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            L_0x009e:
                boolean r10 = r4.isAfterLast()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                if (r10 != 0) goto L_0x00af
                java.lang.String r10 = r4.getString(r13)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                r2.put(r10)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                r4.moveToNext()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                goto L_0x009e
            L_0x00af:
                java.lang.String r10 = "ap"
                r5.put(r10, r2)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            L_0x00b4:
                java.lang.String r2 = "type"
                r6.put(r2, r0)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                java.lang.String r2 = "cuid"
                com.baidu.location.g.b r10 = com.baidu.location.p019g.C0844b.m2417a()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                java.lang.String r10 = r10.f1856c     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                r6.put(r2, r10)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                java.lang.String r2 = "ver"
                r6.put(r2, r0)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                java.lang.String r0 = "prod"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                r2.<init>()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                java.lang.String r10 = com.baidu.location.p019g.C0844b.f1849f     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                r2.append(r10)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                java.lang.String r10 = ":"
                r2.append(r10)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                java.lang.String r10 = com.baidu.location.p019g.C0844b.f1848e     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                r2.append(r10)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                r6.put(r0, r2)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
                if (r9 != 0) goto L_0x00ea
                if (r11 == 0) goto L_0x00ed
            L_0x00ea:
                r6.put(r3, r5)     // Catch:{ Exception -> 0x00fe, all -> 0x00fc }
            L_0x00ed:
                if (r4 == 0) goto L_0x00f4
                r4.close()     // Catch:{ Exception -> 0x00f3 }
                goto L_0x00f4
            L_0x00f3:
            L_0x00f4:
                if (r7 == 0) goto L_0x0129
            L_0x00f6:
                r7.close()     // Catch:{ Exception -> 0x00fa }
                goto L_0x0129
            L_0x00fa:
                goto L_0x0129
            L_0x00fc:
                r0 = move-exception
                goto L_0x010f
            L_0x00fe:
                goto L_0x011f
            L_0x0100:
                r0 = move-exception
                goto L_0x0104
            L_0x0102:
                r0 = move-exception
                r8 = 0
            L_0x0104:
                r4 = r8
                goto L_0x010f
            L_0x0106:
                r8 = 0
            L_0x0107:
                r4 = r8
                goto L_0x011f
            L_0x0109:
                r8 = r4
                r7 = r4
                goto L_0x011f
            L_0x010c:
                r0 = move-exception
                r8 = r4
                r7 = r4
            L_0x010f:
                if (r4 == 0) goto L_0x0116
                r4.close()     // Catch:{ Exception -> 0x0115 }
                goto L_0x0116
            L_0x0115:
            L_0x0116:
                if (r7 == 0) goto L_0x011b
                r7.close()     // Catch:{ Exception -> 0x011b }
            L_0x011b:
                throw r0
            L_0x011c:
                r8 = r4
                r6 = r4
                r7 = r6
            L_0x011f:
                if (r4 == 0) goto L_0x0126
                r4.close()     // Catch:{ Exception -> 0x0125 }
                goto L_0x0126
            L_0x0125:
            L_0x0126:
                if (r7 == 0) goto L_0x0129
                goto L_0x00f6
            L_0x0129:
                if (r6 == 0) goto L_0x0154
                boolean r0 = r6.has(r3)
                if (r0 != 0) goto L_0x0154
                long r4 = r1.f1618u
                r9 = -1
                int r0 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
                if (r0 == 0) goto L_0x0145
                r9 = 86400000(0x5265c00, double:4.2687272E-316)
                long r4 = r4 + r9
                long r9 = java.lang.System.currentTimeMillis()
                int r0 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
                if (r0 >= 0) goto L_0x0154
            L_0x0145:
                java.lang.String r0 = r6.toString()
                java.lang.String r4 = com.baidu.location.Jni.encodeOfflineLocationUpdateRequest(r0)
                long r7 = java.lang.System.currentTimeMillis()
                r1.f1618u = r7
                goto L_0x0155
            L_0x0154:
                r4 = r8
            L_0x0155:
                if (r6 == 0) goto L_0x0165
                boolean r0 = r6.has(r3)
                if (r0 == 0) goto L_0x0165
                java.lang.String r0 = r6.toString()
                java.lang.String r4 = com.baidu.location.Jni.encodeOfflineLocationUpdateRequest(r0)
            L_0x0165:
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.C0797c.m2116g():java.lang.String");
        }

        /* renamed from: h */
        private String m2117h() {
            JSONObject jSONObject;
            try {
                jSONObject = new JSONObject();
                try {
                    jSONObject.put(SocialConstants.PARAM_TYPE, "2");
                    jSONObject.put("ver", "1");
                    jSONObject.put("cuid", C0844b.m2417a().f1856c);
                    jSONObject.put("prod", C0844b.f1849f + Config.TRACE_TODAY_VISIT_SPLIT + C0844b.f1848e);
                    this.f1617t = System.currentTimeMillis();
                } catch (Exception unused) {
                }
            } catch (Exception unused2) {
                jSONObject = null;
            }
            if (jSONObject != null) {
                return Jni.encodeOfflineLocationUpdateRequest(jSONObject.toString());
            }
            return null;
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x005d  */
        /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
        /* renamed from: i */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private java.lang.String m2118i() {
            /*
                r6 = this;
                r0 = 0
                com.baidu.location.d.c r1 = com.baidu.location.p016d.C0794c.this     // Catch:{ Exception -> 0x005a }
                com.baidu.location.d.h r1 = r1.f1581a     // Catch:{ Exception -> 0x005a }
                com.baidu.location.d.l r1 = r1.mo10600k()     // Catch:{ Exception -> 0x005a }
                org.json.JSONObject r1 = r1.mo10613b()     // Catch:{ Exception -> 0x005a }
                if (r1 == 0) goto L_0x005a
                org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x005a }
                r2.<init>()     // Catch:{ Exception -> 0x005a }
                java.lang.String r3 = "type"
                java.lang.String r4 = "3"
                r2.put(r3, r4)     // Catch:{ Exception -> 0x0058 }
                java.lang.String r3 = "ver"
                java.lang.String r4 = "1"
                r2.put(r3, r4)     // Catch:{ Exception -> 0x0058 }
                java.lang.String r3 = "cuid"
                com.baidu.location.g.b r4 = com.baidu.location.p019g.C0844b.m2417a()     // Catch:{ Exception -> 0x0058 }
                java.lang.String r4 = r4.f1856c     // Catch:{ Exception -> 0x0058 }
                r2.put(r3, r4)     // Catch:{ Exception -> 0x0058 }
                java.lang.String r3 = "prod"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0058 }
                r4.<init>()     // Catch:{ Exception -> 0x0058 }
                java.lang.String r5 = com.baidu.location.p019g.C0844b.f1849f     // Catch:{ Exception -> 0x0058 }
                r4.append(r5)     // Catch:{ Exception -> 0x0058 }
                java.lang.String r5 = ":"
                r4.append(r5)     // Catch:{ Exception -> 0x0058 }
                java.lang.String r5 = com.baidu.location.p019g.C0844b.f1848e     // Catch:{ Exception -> 0x0058 }
                r4.append(r5)     // Catch:{ Exception -> 0x0058 }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0058 }
                r2.put(r3, r4)     // Catch:{ Exception -> 0x0058 }
                java.lang.String r3 = "rgc"
                r2.put(r3, r1)     // Catch:{ Exception -> 0x0058 }
                long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0058 }
                r6.f1617t = r3     // Catch:{ Exception -> 0x0058 }
                goto L_0x005b
            L_0x0058:
                goto L_0x005b
            L_0x005a:
                r2 = r0
            L_0x005b:
                if (r2 == 0) goto L_0x0065
                java.lang.String r0 = r2.toString()
                java.lang.String r0 = com.baidu.location.Jni.encodeOfflineLocationUpdateRequest(r0)
            L_0x0065:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.C0797c.m2118i():java.lang.String");
        }

        /* JADX WARN: Failed to insert an additional move for type inference into block B:20:0x003f */
        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.String[], android.database.Cursor] */
        /* JADX WARN: Type inference failed for: r0v1, types: [android.database.Cursor] */
        /* JADX WARN: Type inference failed for: r0v2, types: [android.database.Cursor] */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
            if (r0 == 0) goto L_0x0063;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
            r0.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0060, code lost:
            if (r0 != 0) goto L_0x0041;
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x004d A[SYNTHETIC, Splitter:B:28:0x004d] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0054 A[SYNTHETIC, Splitter:B:32:0x0054] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x005b A[SYNTHETIC, Splitter:B:39:0x005b] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* renamed from: j */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean m2119j() {
            /*
                r5 = this;
                r0 = 0
                r1 = 1
                com.baidu.location.d.c r2 = com.baidu.location.p016d.C0794c.this     // Catch:{ Exception -> 0x0058, all -> 0x0049 }
                android.database.sqlite.SQLiteDatabase r2 = r2.f1588h     // Catch:{ Exception -> 0x0058, all -> 0x0049 }
                java.lang.String r3 = "SELECT COUNT(*) FROM AP;"
                android.database.Cursor r2 = r2.rawQuery(r3, r0)     // Catch:{ Exception -> 0x0058, all -> 0x0049 }
                com.baidu.location.d.c r3 = com.baidu.location.p016d.C0794c.this     // Catch:{ Exception -> 0x0047, all -> 0x0045 }
                android.database.sqlite.SQLiteDatabase r3 = r3.f1588h     // Catch:{ Exception -> 0x0047, all -> 0x0045 }
                java.lang.String r4 = "SELECT COUNT(*) FROM CL"
                android.database.Cursor r0 = r3.rawQuery(r4, r0)     // Catch:{ Exception -> 0x0047, all -> 0x0045 }
                r3 = 0
                if (r2 == 0) goto L_0x0038
                boolean r4 = r2.moveToFirst()     // Catch:{ Exception -> 0x0047, all -> 0x0045 }
                if (r4 == 0) goto L_0x0038
                if (r0 == 0) goto L_0x0038
                boolean r4 = r0.moveToFirst()     // Catch:{ Exception -> 0x0047, all -> 0x0045 }
                if (r4 == 0) goto L_0x0038
                int r4 = r2.getInt(r3)     // Catch:{ Exception -> 0x0047, all -> 0x0045 }
                if (r4 != 0) goto L_0x0037
                int r4 = r0.getInt(r3)     // Catch:{ Exception -> 0x0047, all -> 0x0045 }
                if (r4 == 0) goto L_0x0038
            L_0x0037:
                r1 = 0
            L_0x0038:
                if (r2 == 0) goto L_0x003f
                r2.close()     // Catch:{ Exception -> 0x003e }
                goto L_0x003f
            L_0x003e:
            L_0x003f:
                if (r0 == 0) goto L_0x0063
            L_0x0041:
                r0.close()     // Catch:{ Exception -> 0x0063 }
                goto L_0x0063
            L_0x0045:
                r1 = move-exception
                goto L_0x004b
            L_0x0047:
                goto L_0x0059
            L_0x0049:
                r1 = move-exception
                r2 = r0
            L_0x004b:
                if (r2 == 0) goto L_0x0052
                r2.close()     // Catch:{ Exception -> 0x0051 }
                goto L_0x0052
            L_0x0051:
            L_0x0052:
                if (r0 == 0) goto L_0x0057
                r0.close()     // Catch:{ Exception -> 0x0057 }
            L_0x0057:
                throw r1
            L_0x0058:
                r2 = r0
            L_0x0059:
                if (r2 == 0) goto L_0x0060
                r2.close()     // Catch:{ Exception -> 0x005f }
                goto L_0x0060
            L_0x005f:
            L_0x0060:
                if (r0 == 0) goto L_0x0063
                goto L_0x0041
            L_0x0063:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.C0797c.m2119j():boolean");
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1613f = true;
            this.f1882h = this.f1609b;
            this.f1885k.clear();
            this.f1885k.put("qt", this.f1610c);
            this.f1885k.put("req", this.f1611d);
        }

        /* renamed from: a */
        public void mo10446a(boolean z) {
            if (!z || this.f1884j == null) {
                this.f1613f = false;
                m2112c();
                return;
            }
            new C0799e(this).start();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo10508b() {
            if (m2113d() && !this.f1613f) {
                C0794c.this.f1587g.m2114e();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:0|(4:1|2|(1:4)|5)|8|(3:10|11|12)|13|14|(1:16)|17|19|(4:21|22|23|25)(1:27)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0066 */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0079 A[Catch:{ Exception -> 0x0081 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    C0794c(com.baidu.location.p016d.C0804h r4) {
        /*
            r3 = this;
            r3.<init>()
            r3.f1581a = r4
            r4 = 0
            r3.f1596p = r4
            com.baidu.location.d.c$c r0 = new com.baidu.location.d.c$c
            r1 = 1
            r0.<init>(r3, r1)
            r3.f1586f = r0
            com.baidu.location.d.c$c r0 = new com.baidu.location.d.c$c
            r0.<init>(r3, r4)
            r3.f1587g = r0
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            r3.f1595o = r4
            r4 = 0
            r3.f1590j = r4
            r3.f1591k = r4
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r3.f1592l = r0
            java.util.concurrent.ConcurrentHashMap r0 = new java.util.concurrent.ConcurrentHashMap
            r0.<init>()
            r3.f1593m = r0
            java.util.concurrent.ConcurrentHashMap r0 = new java.util.concurrent.ConcurrentHashMap
            r0.<init>()
            r3.f1594n = r0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0053 }
            com.baidu.location.d.h r1 = r3.f1581a     // Catch:{ Exception -> 0x0053 }
            java.io.File r1 = r1.mo10592c()     // Catch:{ Exception -> 0x0053 }
            java.lang.String r2 = "ofl_location.db"
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0053 }
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x0053 }
            if (r1 != 0) goto L_0x004e
            r0.createNewFile()     // Catch:{ Exception -> 0x0053 }
        L_0x004e:
            android.database.sqlite.SQLiteDatabase r0 = android.database.sqlite.SQLiteDatabase.openOrCreateDatabase(r0, r4)     // Catch:{ Exception -> 0x0053 }
            goto L_0x0054
        L_0x0053:
            r0 = r4
        L_0x0054:
            r3.f1588h = r0
            android.database.sqlite.SQLiteDatabase r0 = r3.f1588h
            if (r0 == 0) goto L_0x0066
            java.lang.String r1 = "CREATE TABLE IF NOT EXISTS AP (id LONG PRIMARY KEY,x DOUBLE,y DOUBLE,r INTEGER,cl DOUBLE,timestamp INTEGER, frequency INTEGER DEFAULT 0);"
            r0.execSQL(r1)     // Catch:{ Exception -> 0x0066 }
            android.database.sqlite.SQLiteDatabase r0 = r3.f1588h     // Catch:{ Exception -> 0x0066 }
            java.lang.String r1 = "CREATE TABLE IF NOT EXISTS CL (id LONG PRIMARY KEY,x DOUBLE,y DOUBLE,r INTEGER,cl DOUBLE,timestamp INTEGER, frequency INTEGER DEFAULT 0);"
            r0.execSQL(r1)     // Catch:{ Exception -> 0x0066 }
        L_0x0066:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0081 }
            com.baidu.location.d.h r1 = r3.f1581a     // Catch:{ Exception -> 0x0081 }
            java.io.File r1 = r1.mo10592c()     // Catch:{ Exception -> 0x0081 }
            java.lang.String r2 = "ofl_statistics.db"
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0081 }
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x0081 }
            if (r1 != 0) goto L_0x007c
            r0.createNewFile()     // Catch:{ Exception -> 0x0081 }
        L_0x007c:
            android.database.sqlite.SQLiteDatabase r4 = android.database.sqlite.SQLiteDatabase.openOrCreateDatabase(r0, r4)     // Catch:{ Exception -> 0x0081 }
            goto L_0x0082
        L_0x0081:
        L_0x0082:
            r3.f1589i = r4
            android.database.sqlite.SQLiteDatabase r4 = r3.f1589i
            if (r4 == 0) goto L_0x0094
            java.lang.String r0 = "CREATE TABLE IF NOT EXISTS AP (id LONG PRIMARY KEY, originid VARCHAR(15), frequency INTEGER DEFAULT 0);"
            r4.execSQL(r0)     // Catch:{ Exception -> 0x0094 }
            android.database.sqlite.SQLiteDatabase r4 = r3.f1589i     // Catch:{ Exception -> 0x0094 }
            java.lang.String r0 = "CREATE TABLE IF NOT EXISTS CL (id LONG PRIMARY KEY, originid VARCHAR(40), frequency INTEGER DEFAULT 0);"
            r4.execSQL(r0)     // Catch:{ Exception -> 0x0094 }
        L_0x0094:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.<init>(com.baidu.location.d.h):void");
    }

    /* renamed from: a */
    private double m2083a(double d, double d2, double d3, double d4) {
        double d5 = d4 - d2;
        double d6 = d3 - d;
        double radians = Math.toRadians(d);
        Math.toRadians(d2);
        double radians2 = Math.toRadians(d3);
        Math.toRadians(d4);
        double radians3 = Math.toRadians(d5);
        double radians4 = Math.toRadians(d6) / 2.0d;
        double d7 = radians3 / 2.0d;
        double sin = (Math.sin(radians4) * Math.sin(radians4)) + (Math.cos(radians) * Math.cos(radians2) * Math.sin(d7) * Math.sin(d7));
        return Math.atan2(Math.sqrt(sin), Math.sqrt(1.0d - sin)) * 2.0d * 6378137.0d;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0092 A[LOOP:0: B:4:0x000b->B:26:0x0092, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0091 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m2084a(java.util.ArrayList<com.baidu.location.p016d.C0794c.C0795a> r22, double r23) {
        /*
            r21 = this;
            r0 = r22
            int r1 = r22.size()
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            r1 = 0
        L_0x000b:
            int r3 = r22.size()
            r4 = 3
            r5 = 1
            if (r3 < r4) goto L_0x008d
            r3 = 0
            r6 = r3
            r8 = r6
            r3 = 0
        L_0x0018:
            int r4 = r22.size()
            if (r3 >= r4) goto L_0x0033
            java.lang.Object r4 = r0.get(r3)
            com.baidu.location.d.c$a r4 = (com.baidu.location.p016d.C0794c.C0795a) r4
            double r10 = r4.f1597a
            double r6 = r6 + r10
            java.lang.Object r4 = r0.get(r3)
            com.baidu.location.d.c$a r4 = (com.baidu.location.p016d.C0794c.C0795a) r4
            double r10 = r4.f1598b
            double r8 = r8 + r10
            int r3 = r3 + 1
            goto L_0x0018
        L_0x0033:
            int r3 = r22.size()
            double r3 = (double) r3
            java.lang.Double.isNaN(r3)
            double r3 = r6 / r3
            int r6 = r22.size()
            double r6 = (double) r6
            java.lang.Double.isNaN(r6)
            double r6 = r8 / r6
            r8 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r10 = -1
            r19 = r8
            r8 = 0
            r9 = -1
        L_0x004e:
            int r10 = r22.size()
            if (r8 >= r10) goto L_0x0079
            java.lang.Object r10 = r0.get(r8)
            com.baidu.location.d.c$a r10 = (com.baidu.location.p016d.C0794c.C0795a) r10
            double r13 = r10.f1598b
            java.lang.Object r10 = r0.get(r8)
            com.baidu.location.d.c$a r10 = (com.baidu.location.p016d.C0794c.C0795a) r10
            double r11 = r10.f1597a
            r10 = r21
            r17 = r11
            r11 = r6
            r15 = r13
            r13 = r3
            double r10 = r10.m2083a(r11, r13, r15, r17)
            int r12 = (r10 > r19 ? 1 : (r10 == r19 ? 0 : -1))
            if (r12 <= 0) goto L_0x0076
            r9 = r8
            r19 = r10
        L_0x0076:
            int r8 = r8 + 1
            goto L_0x004e
        L_0x0079:
            int r3 = (r19 > r23 ? 1 : (r19 == r23 ? 0 : -1))
            if (r3 <= 0) goto L_0x008d
            if (r9 < 0) goto L_0x008d
            int r3 = r22.size()
            if (r9 >= r3) goto L_0x008d
            int r1 = r1 + 1
            r0.remove(r9)
            r3 = r1
            r1 = 1
            goto L_0x008f
        L_0x008d:
            r3 = r1
            r1 = 0
        L_0x008f:
            if (r1 == r5) goto L_0x0092
            return r3
        L_0x0092:
            r1 = r3
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.m2084a(java.util.ArrayList, double):int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b1, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b3, code lost:
        r9 = 0.0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b6, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00b6 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00bd A[SYNTHETIC, Splitter:B:42:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00c7 A[SYNTHETIC, Splitter:B:49:0x00c7] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00d1 A[SYNTHETIC, Splitter:B:58:0x00d1] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.baidu.location.BDLocation m2085a(java.lang.Long r20) {
        /*
            r19 = this;
            r1 = r19
            r0 = r20
            r2 = 0
            r1.f1596p = r2
            java.lang.Long r3 = r1.f1585e
            java.lang.String r4 = "cl"
            r5 = 0
            r6 = 1
            if (r3 == 0) goto L_0x001d
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x001d
            double r2 = r1.f1583c
            double r7 = r1.f1584d
            int r0 = r1.f1582b
            goto L_0x00d5
        L_0x001d:
            java.util.Locale r3 = java.util.Locale.US
            r7 = 3
            java.lang.Object[] r7 = new java.lang.Object[r7]
            r7[r2] = r0
            r8 = 15552000(0xed4e00, float:2.1792994E-38)
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r7[r6] = r8
            r8 = 2
            long r9 = java.lang.System.currentTimeMillis()
            r11 = 1000(0x3e8, double:4.94E-321)
            long r9 = r9 / r11
            java.lang.Long r9 = java.lang.Long.valueOf(r9)
            r7[r8] = r9
            java.lang.String r8 = "SELECT * FROM CL WHERE id = %d AND timestamp + %d > %d;"
            java.lang.String r3 = java.lang.String.format(r3, r8, r7)
            r7 = 0
            android.database.sqlite.SQLiteDatabase r9 = r1.f1588h     // Catch:{ Exception -> 0x00cb, all -> 0x00c3 }
            android.database.Cursor r3 = r9.rawQuery(r3, r5)     // Catch:{ Exception -> 0x00cb, all -> 0x00c3 }
            if (r3 == 0) goto L_0x00b8
            boolean r9 = r3.moveToFirst()     // Catch:{ Exception -> 0x00cc, all -> 0x00b6 }
            if (r9 == 0) goto L_0x00b8
            int r9 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x00cc, all -> 0x00b6 }
            double r9 = r3.getDouble(r9)     // Catch:{ Exception -> 0x00cc, all -> 0x00b6 }
            int r13 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r13 <= 0) goto L_0x00b8
            java.lang.String r9 = "x"
            int r9 = r3.getColumnIndex(r9)     // Catch:{ Exception -> 0x00b3, all -> 0x00b6 }
            double r9 = r3.getDouble(r9)     // Catch:{ Exception -> 0x00b3, all -> 0x00b6 }
            java.lang.String r13 = "y"
            int r13 = r3.getColumnIndex(r13)     // Catch:{ Exception -> 0x00b4, all -> 0x00b6 }
            double r7 = r3.getDouble(r13)     // Catch:{ Exception -> 0x00b4, all -> 0x00b6 }
            java.lang.String r13 = "r"
            int r13 = r3.getColumnIndex(r13)     // Catch:{ Exception -> 0x00b4, all -> 0x00b6 }
            int r2 = r3.getInt(r13)     // Catch:{ Exception -> 0x00b4, all -> 0x00b6 }
            java.lang.String r13 = "timestamp"
            int r13 = r3.getColumnIndex(r13)     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
            int r13 = r3.getInt(r13)     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
            r14 = 604800(0x93a80, float:8.47505E-40)
            int r13 = r13 + r14
            long r13 = (long) r13     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
            long r15 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
            long r15 = r15 / r11
            int r11 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r11 >= 0) goto L_0x0095
            r1.f1596p = r6     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
        L_0x0095:
            r11 = 2000(0x7d0, float:2.803E-42)
            r12 = 300(0x12c, float:4.2E-43)
            if (r2 >= r12) goto L_0x009e
            r2 = 300(0x12c, float:4.2E-43)
            goto L_0x00a2
        L_0x009e:
            if (r11 >= r2) goto L_0x00a2
            r2 = 2000(0x7d0, float:2.803E-42)
        L_0x00a2:
            r1.f1583c = r9     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
            r1.f1584d = r7     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
            r1.f1582b = r2     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
            r1.f1585e = r0     // Catch:{ Exception -> 0x00b1, all -> 0x00b6 }
            r0 = r2
            r17 = r7
            r7 = r9
            r9 = r17
            goto L_0x00bb
        L_0x00b1:
            r0 = r2
            goto L_0x00cf
        L_0x00b3:
            r9 = r7
        L_0x00b4:
            r0 = 0
            goto L_0x00cf
        L_0x00b6:
            r0 = move-exception
            goto L_0x00c5
        L_0x00b8:
            r9 = r7
            r0 = 0
            r6 = 0
        L_0x00bb:
            if (r3 == 0) goto L_0x00c0
            r3.close()     // Catch:{ Exception -> 0x00c0 }
        L_0x00c0:
            r2 = r7
            r7 = r9
            goto L_0x00d5
        L_0x00c3:
            r0 = move-exception
            r3 = r5
        L_0x00c5:
            if (r3 == 0) goto L_0x00ca
            r3.close()     // Catch:{ Exception -> 0x00ca }
        L_0x00ca:
            throw r0
        L_0x00cb:
            r3 = r5
        L_0x00cc:
            r9 = r7
            r0 = 0
            r6 = 0
        L_0x00cf:
            if (r3 == 0) goto L_0x00d4
            r3.close()     // Catch:{ Exception -> 0x00d4 }
        L_0x00d4:
            r2 = r9
        L_0x00d5:
            if (r6 == 0) goto L_0x00ee
            com.baidu.location.BDLocation r5 = new com.baidu.location.BDLocation
            r5.<init>()
            float r0 = (float) r0
            r5.setRadius(r0)
            r5.setLatitude(r7)
            r5.setLongitude(r2)
            r5.setNetworkLocationType(r4)
            r0 = 66
            r5.setLocType(r0)
        L_0x00ee:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.m2085a(java.lang.Long):com.baidu.location.BDLocation");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:115:0x02af, code lost:
        r25 = 0;
        r36 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x02fe, code lost:
        if (m2083a(r17, r15, r19, r12) <= 10000.0d) goto L_0x0304;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0301, code lost:
        r36 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x030a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x030c, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x030d, code lost:
        r35 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0329, code lost:
        if (r35 == null) goto L_0x034a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:?, code lost:
        r35.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0345, code lost:
        if (r35 == null) goto L_0x034a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:?, code lost:
        r35.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0123, code lost:
        r35 = r4;
        r12 = 0.0d;
        r19 = 0.0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0129, code lost:
        r25 = 0;
        r36 = 0;
        r37 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x013f, code lost:
        r35 = r4;
        r12 = 0.0d;
        r19 = 0.0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01fb, code lost:
        r35 = r4;
        r37 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0200, code lost:
        r35 = r4;
        r37 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0204, code lost:
        r12 = 0.0d;
        r19 = 0.0d;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x030a A[ExcHandler: all (th java.lang.Throwable), PHI: r35 10  PHI: (r35v13 android.database.Cursor) = (r35v12 android.database.Cursor), (r35v12 android.database.Cursor), (r35v12 android.database.Cursor), (r35v12 android.database.Cursor), (r35v12 android.database.Cursor), (r35v12 android.database.Cursor), (r35v12 android.database.Cursor), (r35v22 android.database.Cursor), (r35v24 android.database.Cursor) binds: [B:83:0x0216, B:136:0x02f8, B:127:0x02d8, B:119:0x02be, B:101:0x0262, B:106:0x0291, B:87:0x0226, B:71:0x01b8, B:59:0x0171] A[DONT_GENERATE, DONT_INLINE], Splitter:B:59:0x0171] */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x030c A[ExcHandler: all (th java.lang.Throwable), PHI: r4 10  PHI: (r4v5 android.database.Cursor) = (r4v4 android.database.Cursor), (r4v6 android.database.Cursor), (r4v6 android.database.Cursor), (r4v6 android.database.Cursor), (r4v6 android.database.Cursor), (r4v6 android.database.Cursor), (r4v6 android.database.Cursor) binds: [B:22:0x00a7, B:29:0x00bf, B:31:0x00dd, B:32:?, B:45:0x0137, B:46:?, B:35:0x00f1] A[DONT_GENERATE, DONT_INLINE], Splitter:B:22:0x00a7] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0333 A[SYNTHETIC, Splitter:B:158:0x0333] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.baidu.location.BDLocation m2086a(java.util.LinkedHashMap<java.lang.String, java.lang.Integer> r41, com.baidu.location.BDLocation r42, int r43) {
        /*
            r40 = this;
            r10 = r40
            java.lang.StringBuffer r0 = r10.f1595o
            r11 = 0
            r0.setLength(r11)
            r0 = 1
            if (r42 == 0) goto L_0x0018
            double r1 = r42.getLatitude()
            double r3 = r42.getLongitude()
            r17 = r1
            r15 = r3
            r14 = 1
            goto L_0x001d
        L_0x0018:
            r14 = 0
            r15 = 0
            r17 = 0
        L_0x001d:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.util.Set r2 = r41.entrySet()
            java.util.Iterator r2 = r2.iterator()
            r3 = 0
            r4 = 1
        L_0x002c:
            int r5 = r41.size()
            r8 = 30
            int r5 = java.lang.Math.min(r5, r8)
            if (r3 >= r5) goto L_0x0079
            java.lang.Object r5 = r2.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            java.lang.Object r6 = r5.getKey()
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r5 = r5.getValue()
            java.lang.Integer r5 = (java.lang.Integer) r5
            int r7 = r5.intValue()
            if (r7 >= 0) goto L_0x0059
            int r5 = r5.intValue()
            int r5 = -r5
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
        L_0x0059:
            java.lang.Long r7 = com.baidu.location.Jni.encode3(r6)
            if (r7 != 0) goto L_0x0060
            goto L_0x0076
        L_0x0060:
            java.util.concurrent.ConcurrentHashMap<java.lang.Long, java.lang.String> r8 = r10.f1594n
            r8.put(r7, r6)
            if (r4 == 0) goto L_0x0069
            r4 = 0
            goto L_0x006e
        L_0x0069:
            r6 = 44
            r1.append(r6)
        L_0x006e:
            java.util.concurrent.ConcurrentHashMap<java.lang.Long, java.lang.Integer> r6 = r10.f1593m
            r6.put(r7, r5)
            r1.append(r7)
        L_0x0076:
            int r3 = r3 + 1
            goto L_0x002c
        L_0x0079:
            java.util.Locale r2 = java.util.Locale.US
            r9 = 3
            java.lang.Object[] r3 = new java.lang.Object[r9]
            r3[r11] = r1
            r1 = 7776000(0x76a700, float:1.0896497E-38)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r3[r0] = r1
            long r4 = java.lang.System.currentTimeMillis()
            r19 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 / r19
            java.lang.Long r1 = java.lang.Long.valueOf(r4)
            r6 = 2
            r3[r6] = r1
            java.lang.String r1 = "SELECT * FROM AP WHERE id IN (%s) AND timestamp+%d>%d;"
            java.lang.String r1 = java.lang.String.format(r2, r1, r3)
            r7 = 0
            android.database.sqlite.SQLiteDatabase r2 = r10.f1588h     // Catch:{ Exception -> 0x0337, all -> 0x032c }
            android.database.Cursor r4 = r2.rawQuery(r1, r7)     // Catch:{ Exception -> 0x0337, all -> 0x032c }
            if (r4 == 0) goto L_0x031b
            boolean r1 = r4.moveToFirst()     // Catch:{ Exception -> 0x0310, all -> 0x030c }
            if (r1 == 0) goto L_0x031b
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x0310, all -> 0x030c }
            r5.<init>()     // Catch:{ Exception -> 0x0310, all -> 0x030c }
        L_0x00b2:
            boolean r1 = r4.isAfterLast()     // Catch:{ Exception -> 0x0310, all -> 0x030c }
            r21 = 4666723172467343360(0x40c3880000000000, double:10000.0)
            r23 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            if (r1 != 0) goto L_0x020a
            long r25 = r4.getLong(r11)     // Catch:{ Exception -> 0x0200, all -> 0x030c }
            java.lang.Long r3 = java.lang.Long.valueOf(r25)     // Catch:{ Exception -> 0x0200, all -> 0x030c }
            double r26 = r4.getDouble(r0)     // Catch:{ Exception -> 0x0200, all -> 0x030c }
            double r28 = r4.getDouble(r6)     // Catch:{ Exception -> 0x0200, all -> 0x030c }
            int r1 = r4.getInt(r9)     // Catch:{ Exception -> 0x0200, all -> 0x030c }
            r2 = 4
            double r30 = r4.getDouble(r2)     // Catch:{ Exception -> 0x0200, all -> 0x030c }
            r2 = 5
            int r2 = r4.getInt(r2)     // Catch:{ Exception -> 0x0200, all -> 0x030c }
            java.util.HashSet<java.lang.Long> r7 = r10.f1592l     // Catch:{ Exception -> 0x01fb, all -> 0x030c }
            r7.add(r3)     // Catch:{ Exception -> 0x01fb, all -> 0x030c }
            r7 = 604800(0x93a80, float:8.47505E-40)
            int r2 = r2 + r7
            long r12 = (long) r2     // Catch:{ Exception -> 0x01fb, all -> 0x030c }
            long r35 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01fb, all -> 0x030c }
            long r35 = r35 / r19
            int r2 = (r12 > r35 ? 1 : (r12 == r35 ? 0 : -1))
            if (r2 >= 0) goto L_0x0131
            java.lang.StringBuffer r2 = r10.f1595o     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            int r2 = r2.length()     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            if (r2 <= 0) goto L_0x0100
            java.lang.StringBuffer r2 = r10.f1595o     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            java.lang.String r7 = ","
            r2.append(r7)     // Catch:{ Exception -> 0x0123, all -> 0x030c }
        L_0x0100:
            java.lang.StringBuffer r2 = r10.f1595o     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            java.util.Locale r7 = java.util.Locale.US     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            java.lang.String r12 = "(%d,\"%s\",%d)"
            java.lang.Object[] r13 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            r13[r11] = r3     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            java.util.concurrent.ConcurrentHashMap<java.lang.Long, java.lang.String> r8 = r10.f1594n     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            java.lang.Object r8 = r8.get(r3)     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            r13[r0] = r8     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            r8 = 100000(0x186a0, float:1.4013E-40)
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            r13[r6] = r8     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            java.lang.String r7 = java.lang.String.format(r7, r12, r13)     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            r2.append(r7)     // Catch:{ Exception -> 0x0123, all -> 0x030c }
            goto L_0x0131
        L_0x0123:
            r35 = r4
            r12 = 0
            r19 = 0
        L_0x0129:
            r25 = 0
            r36 = 0
            r37 = 0
            goto L_0x0345
        L_0x0131:
            r33 = 0
            int r2 = (r30 > r33 ? 1 : (r30 == r33 ? 0 : -1))
            if (r2 > 0) goto L_0x0146
            r4.moveToNext()     // Catch:{ Exception -> 0x013f, all -> 0x030c }
            r7 = 0
            r8 = 30
            goto L_0x00b2
        L_0x013f:
            r35 = r4
            r12 = r33
            r19 = r12
            goto L_0x0129
        L_0x0146:
            int r2 = (r26 > r33 ? 1 : (r26 == r33 ? 0 : -1))
            if (r2 <= 0) goto L_0x01e2
            int r2 = (r28 > r33 ? 1 : (r28 == r33 ? 0 : -1))
            if (r2 <= 0) goto L_0x01e2
            if (r1 <= 0) goto L_0x01e2
            r2 = 1000(0x3e8, float:1.401E-42)
            if (r1 < r2) goto L_0x0156
            goto L_0x01e2
        L_0x0156:
            if (r14 != r0) goto L_0x017a
            r12 = r1
            r1 = r40
            r13 = r3
            r8 = 100
            r2 = r17
            r35 = r4
            r7 = r5
            r4 = r15
            r11 = r7
            r37 = 0
            r38 = 2
            r6 = r28
            r0 = 30
            r39 = 3
            r8 = r26
            double r1 = r1.m2083a(r2, r4, r6, r8)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            int r3 = (r1 > r21 ? 1 : (r1 == r21 ? 0 : -1))
            if (r3 <= 0) goto L_0x0187
            goto L_0x01de
        L_0x017a:
            r12 = r1
            r13 = r3
            r35 = r4
            r11 = r5
            r0 = 30
            r37 = 0
            r38 = 2
            r39 = 3
        L_0x0187:
            java.util.concurrent.ConcurrentHashMap<java.lang.Long, java.lang.Integer> r1 = r10.f1593m     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            java.lang.Object r1 = r1.get(r13)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            int r1 = java.lang.Math.max(r0, r1)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            r2 = 100
            int r1 = java.lang.Math.min(r2, r1)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            r2 = 4632233691727265792(0x4049000000000000, double:50.0)
            r4 = 70
            if (r1 <= r4) goto L_0x01af
            int r1 = r1 + -70
            double r4 = (double) r1
            r6 = 4629137466983448576(0x403e000000000000, double:30.0)
            java.lang.Double.isNaN(r4)
            double r4 = r4 / r6
        L_0x01ac:
            double r4 = r4 + r23
            goto L_0x01b7
        L_0x01af:
            int r1 = r1 + -70
            double r4 = (double) r1
            java.lang.Double.isNaN(r4)
            double r4 = r4 / r2
            goto L_0x01ac
        L_0x01b7:
            double r6 = (double) r12
            double r1 = java.lang.Math.max(r2, r6)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            r6 = 4603579539098121011(0x3fe3333333333333, double:0.6)
            double r1 = java.lang.Math.pow(r1, r6)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            r6 = -4634023872579145564(0xbfb0a3d70a3d70a4, double:-0.065)
            double r1 = r1 * r6
            double r1 = r1 * r4
            double r30 = java.lang.Math.exp(r1)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            com.baidu.location.d.c$a r1 = new com.baidu.location.d.c$a     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            r32 = 0
            r25 = r1
            r25.<init>(r26, r28, r30, r32)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            r11.add(r1)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
        L_0x01de:
            r35.moveToNext()     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            goto L_0x01ee
        L_0x01e2:
            r35 = r4
            r11 = r5
            r0 = 30
            r37 = 0
            r38 = 2
            r39 = 3
            goto L_0x01de
        L_0x01ee:
            r5 = r11
            r4 = r35
            r7 = r37
            r0 = 1
            r6 = 2
            r8 = 30
            r9 = 3
            r11 = 0
            goto L_0x00b2
        L_0x01fb:
            r35 = r4
            r37 = 0
            goto L_0x0204
        L_0x0200:
            r35 = r4
            r37 = r7
        L_0x0204:
            r12 = 0
            r19 = 0
            goto L_0x0341
        L_0x020a:
            r35 = r4
            r11 = r5
            r37 = r7
            r0 = 30
            r1 = 4652007308841189376(0x408f400000000000, double:1000.0)
            r10.m2084a(r11, r1)     // Catch:{ Exception -> 0x0314, all -> 0x030a }
            r1 = 0
            r2 = 0
            r4 = 0
            r6 = 0
        L_0x0220:
            int r8 = r11.size()     // Catch:{ Exception -> 0x0314, all -> 0x030a }
            if (r1 >= r8) goto L_0x024d
            java.lang.Object r8 = r11.get(r1)     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            com.baidu.location.d.c$a r8 = (com.baidu.location.p016d.C0794c.C0795a) r8     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            double r12 = r8.f1599c     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            r19 = 0
            int r9 = (r12 > r19 ? 1 : (r12 == r19 ? 0 : -1))
            if (r9 > 0) goto L_0x0236
            r9 = r1
            goto L_0x0248
        L_0x0236:
            double r12 = r8.f1597a     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            r9 = r1
            double r0 = r8.f1599c     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            double r12 = r12 * r0
            double r4 = r4 + r12
            double r0 = r8.f1598b     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            double r12 = r8.f1599c     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            double r0 = r0 * r12
            double r6 = r6 + r0
            double r0 = r8.f1599c     // Catch:{ Exception -> 0x0204, all -> 0x030a }
            double r2 = r2 + r0
        L_0x0248:
            int r1 = r9 + 1
            r0 = 30
            goto L_0x0220
        L_0x024d:
            r33 = 0
            int r0 = (r2 > r33 ? 1 : (r2 == r33 ? 0 : -1))
            if (r0 <= 0) goto L_0x02b5
            int r0 = (r4 > r33 ? 1 : (r4 == r33 ? 0 : -1))
            if (r0 <= 0) goto L_0x02b5
            int r0 = (r6 > r33 ? 1 : (r6 == r33 ? 0 : -1))
            if (r0 <= 0) goto L_0x02b5
            double r12 = r4 / r2
            double r19 = r6 / r2
            r0 = 0
            r0 = 0
            r1 = 0
        L_0x0262:
            int r2 = r11.size()     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            if (r0 >= r2) goto L_0x0291
            double r8 = (double) r1     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            java.lang.Object r1 = r11.get(r0)     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            com.baidu.location.d.c$a r1 = (com.baidu.location.p016d.C0794c.C0795a) r1     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            double r6 = r1.f1597a     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            java.lang.Object r1 = r11.get(r0)     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            com.baidu.location.d.c$a r1 = (com.baidu.location.p016d.C0794c.C0795a) r1     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            double r4 = r1.f1598b     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            r1 = r40
            r2 = r12
            r25 = r4
            r4 = r19
            r27 = r8
            r8 = r25
            double r1 = r1.m2083a(r2, r4, r6, r8)     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            java.lang.Double.isNaN(r27)
            double r8 = r27 + r1
            float r1 = (float) r8
            int r0 = r0 + 1
            goto L_0x0262
        L_0x0291:
            int r0 = r11.size()     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            float r0 = (float) r0     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            float r1 = r1 / r0
            int r0 = java.lang.Math.round(r1)     // Catch:{ Exception -> 0x02af, all -> 0x030a }
            r1 = 30
            if (r0 >= r1) goto L_0x02a3
            r0 = 1
            r25 = 30
            goto L_0x02bc
        L_0x02a3:
            r1 = 100
            if (r1 >= r0) goto L_0x02ab
            r0 = 1
            r25 = 100
            goto L_0x02bc
        L_0x02ab:
            r25 = r0
            r0 = 1
            goto L_0x02bc
        L_0x02af:
            r25 = 0
            r36 = 1
            goto L_0x0345
        L_0x02b5:
            r12 = r33
            r19 = r12
            r0 = 0
            r25 = 0
        L_0x02bc:
            if (r14 != 0) goto L_0x02c6
            int r1 = r11.size()     // Catch:{ Exception -> 0x0307, all -> 0x030a }
            r2 = 1
            if (r1 > r2) goto L_0x02c6
            r0 = 0
        L_0x02c6:
            int r1 = r11.size()     // Catch:{ Exception -> 0x0307, all -> 0x030a }
            r2 = r43
            if (r1 >= r2) goto L_0x02ea
            int r1 = r11.size()     // Catch:{ Exception -> 0x0307, all -> 0x030a }
            double r1 = (double) r1
            java.lang.Double.isNaN(r1)
            double r1 = r1 * r23
            int r3 = r41.size()     // Catch:{ Exception -> 0x0307, all -> 0x030a }
            double r3 = (double) r3
            java.lang.Double.isNaN(r3)
            double r1 = r1 / r3
            r3 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x02ea
            r0 = 1
            r11 = 0
            goto L_0x02ec
        L_0x02ea:
            r11 = r0
            r0 = 1
        L_0x02ec:
            if (r14 != r0) goto L_0x0304
            if (r11 != r0) goto L_0x0304
            r1 = r40
            r2 = r17
            r4 = r15
            r6 = r19
            r8 = r12
            double r0 = r1.m2083a(r2, r4, r6, r8)     // Catch:{ Exception -> 0x0301, all -> 0x030a }
            int r2 = (r0 > r21 ? 1 : (r0 == r21 ? 0 : -1))
            if (r2 <= 0) goto L_0x0304
            goto L_0x0327
        L_0x0301:
            r36 = r11
            goto L_0x0345
        L_0x0304:
            r36 = r11
            goto L_0x0329
        L_0x0307:
            r36 = r0
            goto L_0x0345
        L_0x030a:
            r0 = move-exception
            goto L_0x0331
        L_0x030c:
            r0 = move-exception
            r35 = r4
            goto L_0x0331
        L_0x0310:
            r35 = r4
            r37 = r7
        L_0x0314:
            r33 = 0
            r12 = r33
            r19 = r12
            goto L_0x0341
        L_0x031b:
            r35 = r4
            r37 = r7
            r33 = 0
            r12 = r33
            r19 = r12
            r25 = 0
        L_0x0327:
            r36 = 0
        L_0x0329:
            if (r35 == 0) goto L_0x034a
            goto L_0x0347
        L_0x032c:
            r0 = move-exception
            r37 = r7
            r35 = r37
        L_0x0331:
            if (r35 == 0) goto L_0x0336
            r35.close()     // Catch:{ Exception -> 0x0336 }
        L_0x0336:
            throw r0
        L_0x0337:
            r37 = r7
            r33 = 0
            r12 = r33
            r19 = r12
            r35 = r37
        L_0x0341:
            r25 = 0
            r36 = 0
        L_0x0345:
            if (r35 == 0) goto L_0x034a
        L_0x0347:
            r35.close()     // Catch:{ Exception -> 0x034a }
        L_0x034a:
            r1 = r19
            r0 = r25
            if (r36 == 0) goto L_0x036b
            com.baidu.location.BDLocation r7 = new com.baidu.location.BDLocation
            r7.<init>()
            float r0 = (float) r0
            r7.setRadius(r0)
            r7.setLatitude(r1)
            r7.setLongitude(r12)
            java.lang.String r0 = "wf"
            r7.setNetworkLocationType(r0)
            r0 = 66
            r7.setLocType(r0)
            r37 = r7
        L_0x036b:
            return r37
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.m2086a(java.util.LinkedHashMap, com.baidu.location.BDLocation, int):com.baidu.location.BDLocation");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2089a(BDLocation bDLocation, BDLocation bDLocation2, BDLocation bDLocation3, String str, Long l) {
        if (bDLocation != null && bDLocation.getLocType() == 161) {
            if (bDLocation2 != null && bDLocation.getNetworkLocationType() != null && bDLocation.getNetworkLocationType().equals(Config.CELL_LOCATION) && m2083a(bDLocation2.getLatitude(), bDLocation2.getLongitude(), bDLocation.getLatitude(), bDLocation.getLongitude()) > 300.0d) {
                String format = String.format(Locale.US, "UPDATE CL SET cl = 0 WHERE id = %d;", l);
                String format2 = String.format(Locale.US, "INSERT OR REPLACE INTO CL VALUES (%d,\"%s\",%d);", l, str, 100000);
                try {
                    this.f1588h.execSQL(format);
                    this.f1589i.execSQL(format2);
                } catch (Exception unused) {
                }
            }
            if (bDLocation3 != null && bDLocation.getNetworkLocationType() != null && bDLocation.getNetworkLocationType().equals("wf") && m2083a(bDLocation3.getLatitude(), bDLocation3.getLongitude(), bDLocation.getLatitude(), bDLocation.getLongitude()) > 100.0d) {
                try {
                    String format3 = String.format("UPDATE AP SET cl = 0 WHERE id In (%s);", this.f1590j.toString());
                    String format4 = String.format("INSERT OR REPLACE INTO AP VALUES %s;", this.f1591k.toString());
                    this.f1588h.execSQL(format3);
                    this.f1589i.execSQL(format4);
                } catch (Exception unused2) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2095a(String str, Long l, BDLocation bDLocation) {
        if (str != null) {
            if (bDLocation != null) {
                try {
                    this.f1588h.execSQL(String.format(Locale.US, "UPDATE CL SET frequency=frequency+1 WHERE id = %d;", l));
                } catch (Exception unused) {
                }
            } else {
                String format = String.format(Locale.US, "INSERT OR IGNORE INTO CL VALUES (%d,\"%s\",0);", l, str);
                String format2 = String.format(Locale.US, "UPDATE CL SET frequency=frequency+1 WHERE id = %d;", l);
                this.f1589i.execSQL(format);
                this.f1589i.execSQL(format2);
            }
            if (this.f1596p) {
                try {
                    this.f1589i.execSQL(String.format(Locale.US, "INSERT OR IGNORE INTO CL VALUES (%d,\"%s\",%d);", l, str, 100000));
                } catch (Exception unused2) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2096a(String str, String str2, String str3) {
        this.f1586f.m2109a(str, str2, str3);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2097a(LinkedHashMap<String, Integer> linkedHashMap) {
        if (linkedHashMap != null && linkedHashMap.size() > 0) {
            this.f1590j = new StringBuffer();
            this.f1591k = new StringBuffer();
            StringBuffer stringBuffer = new StringBuffer();
            StringBuffer stringBuffer2 = new StringBuffer();
            ConcurrentHashMap<Long, Integer> concurrentHashMap = this.f1593m;
            if (!(concurrentHashMap == null || concurrentHashMap.keySet() == null)) {
                boolean z = true;
                boolean z2 = true;
                for (Long l : this.f1593m.keySet()) {
                    try {
                        if (this.f1592l.contains(l)) {
                            if (z) {
                                z = false;
                            } else {
                                this.f1590j.append(',');
                                this.f1591k.append(',');
                            }
                            this.f1590j.append(l);
                            StringBuffer stringBuffer3 = this.f1591k;
                            stringBuffer3.append('(');
                            stringBuffer3.append(l);
                            stringBuffer3.append(',');
                            stringBuffer3.append('\"');
                            stringBuffer3.append(this.f1594n.get(l));
                            stringBuffer3.append('\"');
                            stringBuffer3.append(',');
                            stringBuffer3.append(100000);
                            stringBuffer3.append(')');
                        } else {
                            String str = this.f1594n.get(l);
                            if (z2) {
                                z2 = false;
                            } else {
                                stringBuffer.append(',');
                                stringBuffer2.append(',');
                            }
                            stringBuffer.append(l);
                            stringBuffer2.append('(');
                            stringBuffer2.append(l);
                            stringBuffer2.append(',');
                            stringBuffer2.append('\"');
                            stringBuffer2.append(str);
                            stringBuffer2.append('\"');
                            stringBuffer2.append(",0)");
                        }
                    } catch (Exception unused) {
                    }
                }
            }
            try {
                this.f1588h.execSQL(String.format(Locale.US, "UPDATE AP SET frequency=frequency+1 WHERE id IN(%s)", this.f1590j.toString()));
            } catch (Exception unused2) {
            }
            StringBuffer stringBuffer4 = this.f1595o;
            if (stringBuffer4 != null && stringBuffer4.length() > 0) {
                if (stringBuffer2.length() > 0) {
                    stringBuffer2.append(",");
                }
                stringBuffer2.append(this.f1595o);
            }
            try {
                String format = String.format("INSERT OR IGNORE INTO AP VALUES %s;", stringBuffer2.toString());
                String format2 = String.format("UPDATE AP SET frequency=frequency+1 WHERE id in (%s);", stringBuffer.toString());
                if (stringBuffer2.length() > 0) {
                    this.f1589i.execSQL(format);
                }
                if (stringBuffer.length() > 0) {
                    this.f1589i.execSQL(format2);
                }
            } catch (Exception unused3) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2098a(String[] strArr) {
        this.f1581a.mo10601l().mo10571a(strArr);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (r4 != null) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x010f, code lost:
        if (r4 != null) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0111, code lost:
        r11 = r4;
        r6 = r5;
        r5 = r18;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01d5  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.database.Cursor mo10564a(com.baidu.location.p016d.C0809j.C0810a r21) {
        /*
            r20 = this;
            r9 = r20
            r0 = r21
            com.baidu.location.BDLocation r1 = new com.baidu.location.BDLocation
            r1.<init>()
            r2 = 67
            r1.setLocType(r2)
            boolean r2 = r0.f1681c
            if (r2 == 0) goto L_0x0200
            java.lang.String r2 = r0.f1680b
            java.util.LinkedHashMap<java.lang.String, java.lang.Integer> r8 = r0.f1687i
            int r3 = r0.f1684f
            com.baidu.location.BDLocation r6 = r0.f1685g
            r4 = -9223372036854775808
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            if (r2 == 0) goto L_0x0036
            android.database.sqlite.SQLiteDatabase r7 = r9.f1588h
            if (r7 == 0) goto L_0x0036
            java.lang.Long r4 = com.baidu.location.Jni.encode3(r2)
            if (r4 == 0) goto L_0x0036
            com.baidu.location.BDLocation r7 = r9.m2085a(r4)
            r19 = r7
            r7 = r4
            r4 = r19
            goto L_0x0038
        L_0x0036:
            r7 = r4
            r4 = 0
        L_0x0038:
            if (r8 == 0) goto L_0x0059
            int r10 = r8.size()
            if (r10 <= 0) goto L_0x0059
            android.database.sqlite.SQLiteDatabase r10 = r9.f1588h
            if (r10 == 0) goto L_0x0059
            java.util.concurrent.ConcurrentHashMap<java.lang.Long, java.lang.Integer> r10 = r9.f1593m
            r10.clear()
            java.util.HashSet<java.lang.Long> r10 = r9.f1592l
            r10.clear()
            java.util.concurrent.ConcurrentHashMap<java.lang.Long, java.lang.String> r10 = r9.f1594n
            r10.clear()
            com.baidu.location.BDLocation r3 = r9.m2086a(r8, r4, r3)
            r10 = r3
            goto L_0x005a
        L_0x0059:
            r10 = 0
        L_0x005a:
            java.lang.String r3 = "gcj"
            java.lang.String r11 = "bd09ll2gcj"
            r12 = 1
            if (r4 == 0) goto L_0x0097
            double r14 = r4.getLongitude()
            java.lang.Double r14 = java.lang.Double.valueOf(r14)
            double r15 = r4.getLatitude()
            java.lang.Double r15 = java.lang.Double.valueOf(r15)
            r16 = r6
            double r5 = r4.getLongitude()
            r17 = r14
            double r13 = r4.getLatitude()
            double[] r5 = com.baidu.location.Jni.coorEncrypt(r5, r13, r11)
            r4.setCoorType(r3)
            r13 = r5[r12]
            r4.setLatitude(r13)
            r6 = 0
            r13 = r5[r6]
            r4.setLongitude(r13)
            java.lang.String r5 = "cl"
            r4.setNetworkLocationType(r5)
            r5 = r17
            goto L_0x009b
        L_0x0097:
            r16 = r6
            r5 = 0
            r15 = 0
        L_0x009b:
            if (r10 == 0) goto L_0x00d2
            double r13 = r10.getLongitude()
            java.lang.Double r6 = java.lang.Double.valueOf(r13)
            double r13 = r10.getLatitude()
            java.lang.Double r13 = java.lang.Double.valueOf(r13)
            r17 = r13
            double r12 = r10.getLongitude()
            r18 = r15
            double r14 = r10.getLatitude()
            double[] r11 = com.baidu.location.Jni.coorEncrypt(r12, r14, r11)
            r10.setCoorType(r3)
            r3 = 1
            r12 = r11[r3]
            r10.setLatitude(r12)
            r3 = 0
            r12 = r11[r3]
            r10.setLongitude(r12)
            java.lang.String r3 = "wf"
            r10.setNetworkLocationType(r3)
            goto L_0x00d7
        L_0x00d2:
            r18 = r15
            r6 = 0
            r17 = 0
        L_0x00d7:
            if (r4 == 0) goto L_0x00dd
            if (r10 != 0) goto L_0x00dd
            r13 = 1
            goto L_0x00ec
        L_0x00dd:
            if (r4 != 0) goto L_0x00e4
            if (r10 == 0) goto L_0x00e4
            r12 = 2
            r13 = 2
            goto L_0x00ec
        L_0x00e4:
            if (r4 == 0) goto L_0x00eb
            if (r10 == 0) goto L_0x00eb
            r12 = 4
            r13 = 4
            goto L_0x00ec
        L_0x00eb:
            r13 = 0
        L_0x00ec:
            int r3 = r0.f1684f
            if (r3 <= 0) goto L_0x00f2
            r3 = 1
            goto L_0x00f3
        L_0x00f2:
            r3 = 0
        L_0x00f3:
            if (r8 == 0) goto L_0x00fe
            int r11 = r8.size()
            if (r11 > 0) goto L_0x00fc
            goto L_0x00fe
        L_0x00fc:
            r11 = 0
            goto L_0x00ff
        L_0x00fe:
            r11 = 1
        L_0x00ff:
            if (r3 == 0) goto L_0x0109
            if (r10 == 0) goto L_0x0104
            goto L_0x010b
        L_0x0104:
            if (r11 == 0) goto L_0x0116
            if (r4 == 0) goto L_0x0116
            goto L_0x0111
        L_0x0109:
            if (r10 == 0) goto L_0x010f
        L_0x010b:
            r11 = r10
            r5 = r17
            goto L_0x0119
        L_0x010f:
            if (r4 == 0) goto L_0x0116
        L_0x0111:
            r11 = r4
            r6 = r5
            r5 = r18
            goto L_0x0119
        L_0x0116:
            r11 = r1
            r5 = 0
            r6 = 0
        L_0x0119:
            boolean r12 = r0.f1683e
            if (r12 == 0) goto L_0x0147
            com.baidu.location.d.h r12 = r9.f1581a
            com.baidu.location.d.f r12 = r12.mo10601l()
            boolean r12 = r12.mo10582l()
            if (r12 == 0) goto L_0x0147
            if (r5 == 0) goto L_0x0147
            if (r6 == 0) goto L_0x0147
            com.baidu.location.d.h r12 = r9.f1581a
            com.baidu.location.d.l r12 = r12.mo10600k()
            double r14 = r6.doubleValue()
            r18 = r6
            r17 = r7
            double r6 = r5.doubleValue()
            com.baidu.location.Address r6 = r12.mo10609a(r14, r6)
            r11.setAddr(r6)
            goto L_0x014b
        L_0x0147:
            r18 = r6
            r17 = r7
        L_0x014b:
            if (r3 == 0) goto L_0x015c
            boolean r6 = r0.f1683e
            if (r6 == 0) goto L_0x015c
            java.lang.String r6 = r11.getAddrStr()
            if (r6 != 0) goto L_0x015c
            r11 = r1
            r5 = 0
            r13 = 0
            r18 = 0
        L_0x015c:
            boolean r6 = r0.f1682d
            if (r6 != 0) goto L_0x0164
            boolean r6 = r0.f1686h
            if (r6 == 0) goto L_0x0183
        L_0x0164:
            if (r5 == 0) goto L_0x0183
            if (r18 == 0) goto L_0x0183
            com.baidu.location.d.h r6 = r9.f1581a
            com.baidu.location.d.l r6 = r6.mo10600k()
            double r14 = r18.doubleValue()
            r7 = r13
            double r12 = r5.doubleValue()
            java.util.List r5 = r6.mo10612b(r14, r12)
            boolean r6 = r0.f1682d
            if (r6 == 0) goto L_0x0185
            r11.setPoiList(r5)
            goto L_0x0185
        L_0x0183:
            r7 = r13
            r5 = 0
        L_0x0185:
            if (r3 == 0) goto L_0x0196
            boolean r6 = r0.f1682d
            if (r6 == 0) goto L_0x0196
            if (r5 == 0) goto L_0x0193
            int r6 = r5.size()
            if (r6 > 0) goto L_0x0196
        L_0x0193:
            r11 = r1
            r13 = 0
            goto L_0x0197
        L_0x0196:
            r13 = r7
        L_0x0197:
            boolean r6 = r0.f1686h
            if (r6 == 0) goto L_0x01bf
            if (r5 == 0) goto L_0x01bf
            int r6 = r5.size()
            if (r6 <= 0) goto L_0x01bf
            java.util.Locale r6 = java.util.Locale.CHINA
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]
            r12 = 0
            java.lang.Object r5 = r5.get(r12)
            com.baidu.location.Poi r5 = (com.baidu.location.Poi) r5
            java.lang.String r5 = r5.getName()
            r7[r12] = r5
            java.lang.String r5 = "在%s附近"
            java.lang.String r5 = java.lang.String.format(r6, r5, r7)
            r11.setLocationDescribe(r5)
            goto L_0x01c1
        L_0x01bf:
            r12 = 0
            r5 = 0
        L_0x01c1:
            if (r3 == 0) goto L_0x01cb
            boolean r3 = r0.f1686h
            if (r3 == 0) goto L_0x01cb
            if (r5 != 0) goto L_0x01cb
            r11 = r1
            goto L_0x01cc
        L_0x01cb:
            r12 = r13
        L_0x01cc:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r3 = r0.f1679a
            if (r3 == 0) goto L_0x01ee
            java.lang.String r3 = r0.f1679a
            r1.append(r3)
            java.lang.String r0 = com.baidu.location.p016d.C0809j.m2221a(r10, r4, r0)
            r1.append(r0)
            java.lang.String r0 = com.baidu.location.p016d.C0809j.m2220a(r11, r12)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r7 = r0
            goto L_0x01ef
        L_0x01ee:
            r7 = 0
        L_0x01ef:
            com.baidu.location.d.d r12 = new com.baidu.location.d.d
            r0 = r12
            r1 = r20
            r3 = r17
            r5 = r10
            r6 = r16
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r12.start()
            r1 = r11
        L_0x0200:
            android.database.Cursor r0 = com.baidu.location.p016d.C0809j.m2218a(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0794c.mo10564a(com.baidu.location.d.j$a):android.database.Cursor");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public SQLiteDatabase mo10565a() {
        return this.f1589i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo10566b() {
        this.f1587g.mo10508b();
    }
}
