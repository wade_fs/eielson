package com.baidu.platform.core.p036f;

import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;
import com.baidu.platform.base.C1316a;
import com.baidu.platform.base.SearchType;

/* renamed from: com.baidu.platform.core.f.b */
public class C1374b extends C1316a implements C1373a {

    /* renamed from: b */
    private OnGetSuggestionResultListener f4470b = null;

    /* renamed from: a */
    public void mo14073a() {
        this.f4422a.lock();
        this.f4470b = null;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public void mo14074a(OnGetSuggestionResultListener onGetSuggestionResultListener) {
        this.f4422a.lock();
        this.f4470b = onGetSuggestionResultListener;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public boolean mo14075a(SuggestionSearchOption suggestionSearchOption) {
        C1375c cVar = new C1375c();
        cVar.mo14021a(SearchType.SUGGESTION_SEARCH_TYPE);
        return mo14016a(new C1376d(suggestionSearchOption), this.f4470b, cVar);
    }
}
