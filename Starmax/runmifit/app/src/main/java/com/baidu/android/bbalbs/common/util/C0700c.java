package com.baidu.android.bbalbs.common.util;

import com.baidu.android.bbalbs.common.util.C0696b;
import java.util.Comparator;

/* renamed from: com.baidu.android.bbalbs.common.util.c */
class C0700c implements Comparator<C0696b.C0697a> {

    /* renamed from: a */
    final /* synthetic */ C0696b f1065a;

    C0700c(C0696b bVar) {
        this.f1065a = bVar;
    }

    /* renamed from: a */
    public int compare(C0696b.C0697a aVar, C0696b.C0697a aVar2) {
        int i = aVar2.f1059b - aVar.f1059b;
        if (i == 0) {
            if (aVar.f1061d && aVar2.f1061d) {
                return 0;
            }
            if (aVar.f1061d) {
                return -1;
            }
            if (aVar2.f1061d) {
                return 1;
            }
        }
        return i;
    }
}
