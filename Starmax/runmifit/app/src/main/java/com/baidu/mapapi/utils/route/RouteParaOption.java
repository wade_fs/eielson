package com.baidu.mapapi.utils.route;

import com.baidu.mapapi.model.LatLng;

public class RouteParaOption {

    /* renamed from: a */
    LatLng f3273a;

    /* renamed from: b */
    LatLng f3274b;

    /* renamed from: c */
    String f3275c;

    /* renamed from: d */
    String f3276d;

    /* renamed from: e */
    String f3277e;

    /* renamed from: f */
    EBusStrategyType f3278f = EBusStrategyType.bus_recommend_way;

    public enum EBusStrategyType {
        bus_time_first,
        bus_transfer_little,
        bus_walk_little,
        bus_no_subway,
        bus_recommend_way
    }

    public RouteParaOption busStrategyType(EBusStrategyType eBusStrategyType) {
        this.f3278f = eBusStrategyType;
        return this;
    }

    public RouteParaOption cityName(String str) {
        this.f3277e = str;
        return this;
    }

    public RouteParaOption endName(String str) {
        this.f3276d = str;
        return this;
    }

    public RouteParaOption endPoint(LatLng latLng) {
        this.f3274b = latLng;
        return this;
    }

    public EBusStrategyType getBusStrategyType() {
        return this.f3278f;
    }

    public String getCityName() {
        return this.f3277e;
    }

    public String getEndName() {
        return this.f3276d;
    }

    public LatLng getEndPoint() {
        return this.f3274b;
    }

    public String getStartName() {
        return this.f3275c;
    }

    public LatLng getStartPoint() {
        return this.f3273a;
    }

    public RouteParaOption startName(String str) {
        this.f3275c = str;
        return this;
    }

    public RouteParaOption startPoint(LatLng latLng) {
        this.f3273a = latLng;
        return this;
    }
}
