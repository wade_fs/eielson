package com.baidu.mapapi.search.geocode;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import java.util.List;

public class ReverseGeoCodeResult extends SearchResult {
    public static final Parcelable.Creator<ReverseGeoCodeResult> CREATOR = new C0976b();

    /* renamed from: a */
    private String f2978a;

    /* renamed from: b */
    private String f2979b;

    /* renamed from: c */
    private AddressComponent f2980c;

    /* renamed from: d */
    private LatLng f2981d;

    /* renamed from: e */
    private int f2982e;

    /* renamed from: f */
    private List<PoiInfo> f2983f;

    /* renamed from: g */
    private String f2984g;

    /* renamed from: h */
    private List<PoiRegionsInfo> f2985h;

    public static class AddressComponent implements Parcelable {
        public static final Parcelable.Creator<AddressComponent> CREATOR = new C0977c();
        public int adcode;
        public String city;
        public int countryCode;
        public String countryName;
        public String direction;
        public String distance;
        public String district;
        public String province;
        public String street;
        public String streetNumber;
        public String town;

        public AddressComponent() {
        }

        protected AddressComponent(Parcel parcel) {
            this.streetNumber = parcel.readString();
            this.street = parcel.readString();
            this.town = parcel.readString();
            this.district = parcel.readString();
            this.city = parcel.readString();
            this.province = parcel.readString();
            this.countryName = parcel.readString();
            this.countryCode = parcel.readInt();
            this.adcode = parcel.readInt();
            this.direction = parcel.readString();
            this.distance = parcel.readString();
        }

        public int describeContents() {
            return 0;
        }

        public String getDirection() {
            return this.direction;
        }

        public String getDistance() {
            return this.distance;
        }

        public String getTown() {
            return this.town;
        }

        public void setDirection(String str) {
            this.direction = str;
        }

        public void setDistance(String str) {
            this.distance = str;
        }

        public void setTown(String str) {
            this.town = str;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.streetNumber);
            parcel.writeString(this.street);
            parcel.writeString(this.town);
            parcel.writeString(this.district);
            parcel.writeString(this.city);
            parcel.writeString(this.province);
            parcel.writeString(this.countryName);
            parcel.writeInt(this.countryCode);
            parcel.writeInt(this.adcode);
            parcel.writeString(this.direction);
            parcel.writeString(this.distance);
        }
    }

    public static class PoiRegionsInfo implements Parcelable {
        public static final Parcelable.Creator<PoiRegionsInfo> CREATOR = new C0978d();
        public String directionDesc;
        public String regionName;
        public String regionTag;

        public PoiRegionsInfo() {
        }

        protected PoiRegionsInfo(Parcel parcel) {
            this.directionDesc = parcel.readString();
            this.regionName = parcel.readString();
            this.regionTag = parcel.readString();
        }

        public int describeContents() {
            return 0;
        }

        public String getDirectionDesc() {
            return this.directionDesc;
        }

        public String getRegionName() {
            return this.regionName;
        }

        public String getRegionTag() {
            return this.regionTag;
        }

        public void setDirectionDesc(String str) {
            this.directionDesc = str;
        }

        public void setRegionName(String str) {
            this.regionName = str;
        }

        public void setRegionTag(String str) {
            this.regionTag = str;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.directionDesc);
            parcel.writeString(this.regionName);
            parcel.writeString(this.regionTag);
        }
    }

    public ReverseGeoCodeResult() {
    }

    protected ReverseGeoCodeResult(Parcel parcel) {
        super(parcel);
        this.f2978a = parcel.readString();
        this.f2979b = parcel.readString();
        this.f2980c = (AddressComponent) parcel.readParcelable(AddressComponent.class.getClassLoader());
        this.f2981d = (LatLng) parcel.readValue(LatLng.class.getClassLoader());
        this.f2983f = parcel.createTypedArrayList(PoiInfo.CREATOR);
        this.f2984g = parcel.readString();
        this.f2985h = parcel.createTypedArrayList(PoiRegionsInfo.CREATOR);
    }

    public int describeContents() {
        return 0;
    }

    public int getAdcode() {
        return this.f2980c.adcode;
    }

    public String getAddress() {
        return this.f2979b;
    }

    public AddressComponent getAddressDetail() {
        return this.f2980c;
    }

    public String getBusinessCircle() {
        return this.f2978a;
    }

    public int getCityCode() {
        return this.f2982e;
    }

    public LatLng getLocation() {
        return this.f2981d;
    }

    public List<PoiInfo> getPoiList() {
        return this.f2983f;
    }

    public List<PoiRegionsInfo> getPoiRegionsInfoList() {
        return this.f2985h;
    }

    public String getSematicDescription() {
        return this.f2984g;
    }

    public void setAdcode(int i) {
        this.f2980c.adcode = i;
    }

    public void setAddress(String str) {
        this.f2979b = str;
    }

    public void setAddressDetail(AddressComponent addressComponent) {
        this.f2980c = addressComponent;
    }

    public void setBusinessCircle(String str) {
        this.f2978a = str;
    }

    public void setCityCode(int i) {
        this.f2982e = i;
    }

    public void setLocation(LatLng latLng) {
        this.f2981d = latLng;
    }

    public void setPoiList(List<PoiInfo> list) {
        this.f2983f = list;
    }

    public void setPoiRegionsInfoList(List<PoiRegionsInfo> list) {
        this.f2985h = list;
    }

    public void setSematicDescription(String str) {
        this.f2984g = str;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("ReverseGeoCodeResult: \n");
        stringBuffer.append("businessCircle = ");
        stringBuffer.append(this.f2978a);
        stringBuffer.append("; address = ");
        stringBuffer.append(this.f2979b);
        stringBuffer.append("; location = ");
        stringBuffer.append(this.f2981d);
        stringBuffer.append("; sematicDescription = ");
        stringBuffer.append(this.f2984g);
        if (this.f2980c != null) {
            stringBuffer.append("\n#AddressComponent Info BEGIN# \n");
            stringBuffer.append("streetNumber = ");
            stringBuffer.append(this.f2980c.streetNumber);
            stringBuffer.append("; street = ");
            stringBuffer.append(this.f2980c.street);
            stringBuffer.append("; town = ");
            stringBuffer.append(this.f2980c.town);
            stringBuffer.append("; district = ");
            stringBuffer.append(this.f2980c.district);
            stringBuffer.append("; city = ");
            stringBuffer.append(this.f2980c.city);
            stringBuffer.append("; province = ");
            stringBuffer.append(this.f2980c.province);
            stringBuffer.append("; countryName = ");
            stringBuffer.append(this.f2980c.countryName);
            stringBuffer.append("; countryCode = ");
            stringBuffer.append(this.f2980c.countryCode);
            stringBuffer.append("; adcode = ");
            stringBuffer.append(this.f2980c.adcode);
            stringBuffer.append("; direction = ");
            stringBuffer.append(this.f2980c.direction);
            stringBuffer.append("; distance = ");
            stringBuffer.append(this.f2980c.distance);
            stringBuffer.append("\n#AddressComponent Info END# \n");
        }
        List<PoiRegionsInfo> list = this.f2985h;
        if (list != null && !list.isEmpty()) {
            stringBuffer.append("\n#PoiRegions Info  BEGIN#");
            for (int i = 0; i < this.f2985h.size(); i++) {
                PoiRegionsInfo poiRegionsInfo = this.f2985h.get(i);
                if (poiRegionsInfo != null) {
                    stringBuffer.append("\ndirectionDesc = ");
                    stringBuffer.append(poiRegionsInfo.getDirectionDesc());
                    stringBuffer.append("; regionName = ");
                    stringBuffer.append(poiRegionsInfo.getRegionName());
                    stringBuffer.append("; regionTag = ");
                    stringBuffer.append(poiRegionsInfo.getRegionTag());
                }
            }
            stringBuffer.append("\n#PoiRegions Info  END# \n");
        }
        List<PoiInfo> list2 = this.f2983f;
        if (list2 != null && !list2.isEmpty()) {
            stringBuffer.append("\n #PoiList Info  BEGIN#");
            for (int i2 = 0; i2 < this.f2983f.size(); i2++) {
                PoiInfo poiInfo = this.f2983f.get(i2);
                if (poiInfo != null) {
                    stringBuffer.append("\n address = ");
                    stringBuffer.append(poiInfo.getAddress());
                    stringBuffer.append("; phoneNumber = ");
                    stringBuffer.append(poiInfo.getPhoneNum());
                    stringBuffer.append("; uid = ");
                    stringBuffer.append(poiInfo.getUid());
                    stringBuffer.append("; postCode = ");
                    stringBuffer.append(poiInfo.getPostCode());
                    stringBuffer.append("; name = ");
                    stringBuffer.append(poiInfo.getName());
                    stringBuffer.append("; location = ");
                    stringBuffer.append(poiInfo.getLocation());
                    stringBuffer.append("; city = ");
                    stringBuffer.append(poiInfo.getCity());
                    stringBuffer.append("; direction = ");
                    stringBuffer.append(poiInfo.getDirection());
                    stringBuffer.append("; distance = ");
                    stringBuffer.append(poiInfo.getDistance());
                    if (poiInfo.getParentPoi() != null) {
                        stringBuffer.append("\n parentPoiAddress = ");
                        stringBuffer.append(poiInfo.getParentPoi().getParentPoiAddress());
                        stringBuffer.append("; parentPoiDirection = ");
                        stringBuffer.append(poiInfo.getParentPoi().getParentPoiDirection());
                        stringBuffer.append("; parentPoiDistance = ");
                        stringBuffer.append(poiInfo.getParentPoi().getParentPoiDistance());
                        stringBuffer.append("; parentPoiName = ");
                        stringBuffer.append(poiInfo.getParentPoi().getParentPoiName());
                        stringBuffer.append("; parentPoiTag = ");
                        stringBuffer.append(poiInfo.getParentPoi().getParentPoiTag());
                        stringBuffer.append("; parentPoiUid = ");
                        stringBuffer.append(poiInfo.getParentPoi().getParentPoiUid());
                        stringBuffer.append("; parentPoiLocation = ");
                        stringBuffer.append(poiInfo.getParentPoi().getParentPoiLocation());
                    }
                }
            }
            stringBuffer.append("\n #PoiList Info  END# \n");
        }
        return stringBuffer.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(this.f2978a);
        parcel.writeString(this.f2979b);
        parcel.writeParcelable(this.f2980c, 0);
        parcel.writeValue(this.f2981d);
        parcel.writeTypedList(this.f2983f);
        parcel.writeString(this.f2984g);
        parcel.writeTypedList(this.f2985h);
    }
}
