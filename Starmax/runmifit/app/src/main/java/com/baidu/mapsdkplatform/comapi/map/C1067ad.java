package com.baidu.mapsdkplatform.comapi.map;

import android.os.Handler;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.ad */
class C1067ad extends Handler {

    /* renamed from: a */
    final /* synthetic */ C1066ac f3451a;

    C1067ad(C1066ac acVar) {
        this.f3451a = acVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0103, code lost:
        if (r12.f3451a.f3449h != null) goto L_0x0105;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0119, code lost:
        if (r12.f3451a.f3449h != null) goto L_0x0105;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.os.Message r13) {
        /*
            r12 = this;
            super.handleMessage(r13)
            com.baidu.mapsdkplatform.comapi.map.ac r0 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r0 = r0.f3450i
            if (r0 == 0) goto L_0x0301
            com.baidu.mapsdkplatform.comapi.map.ac r0 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r0 = r0.f3450i
            com.baidu.mapsdkplatform.comjni.map.basemap.a r0 = r0.f3533i
            if (r0 != 0) goto L_0x0017
            goto L_0x0301
        L_0x0017:
            java.lang.Object r0 = r13.obj
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
            com.baidu.mapsdkplatform.comapi.map.ac r2 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r2 = r2.f3450i
            long r2 = r2.f3534j
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto L_0x002c
            return
        L_0x002c:
            int r0 = r13.what
            r1 = 4000(0xfa0, float:5.605E-42)
            r2 = 0
            r3 = 1
            if (r0 != r1) goto L_0x00c8
            com.baidu.mapsdkplatform.comapi.map.ac r0 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r0 = r0.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r0 = r0.f3532h
            if (r0 != 0) goto L_0x003f
            return
        L_0x003f:
            com.baidu.mapsdkplatform.comapi.map.ac r0 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r0 = r0.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r0 = r0.f3532h
            java.util.Iterator r0 = r0.iterator()
        L_0x004b:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0301
            java.lang.Object r1 = r0.next()
            com.baidu.mapsdkplatform.comapi.map.l r1 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r1
            r4 = 0
            int r5 = r13.arg2
            if (r5 != r3) goto L_0x00c2
            int r4 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3442a
            int r5 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3443b
            int r4 = r4 * r5
            int[] r4 = new int[r4]
            int r5 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3442a
            int r6 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3443b
            int r5 = r5 * r6
            int[] r5 = new int[r5]
            com.baidu.mapsdkplatform.comapi.map.ac r6 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r6 = r6.f3450i
            com.baidu.mapsdkplatform.comjni.map.basemap.a r6 = r6.f3533i
            if (r6 != 0) goto L_0x0077
            return
        L_0x0077:
            com.baidu.mapsdkplatform.comapi.map.ac r6 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r6 = r6.f3450i
            com.baidu.mapsdkplatform.comjni.map.basemap.a r6 = r6.f3533i
            int r7 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3442a
            int r8 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3443b
            int[] r4 = r6.mo13487a(r4, r7, r8)
            r6 = 0
        L_0x0088:
            int r7 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3443b
            if (r6 >= r7) goto L_0x00b8
            r7 = 0
        L_0x008d:
            int r8 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3442a
            if (r7 >= r8) goto L_0x00b5
            int r8 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3442a
            int r8 = r8 * r6
            int r8 = r8 + r7
            r8 = r4[r8]
            int r9 = r8 >> 16
            r9 = r9 & 255(0xff, float:3.57E-43)
            int r10 = r8 << 16
            r11 = 16711680(0xff0000, float:2.3418052E-38)
            r10 = r10 & r11
            r11 = -16711936(0xffffffffff00ff00, float:-1.7146522E38)
            r8 = r8 & r11
            r8 = r8 | r10
            r8 = r8 | r9
            int r9 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3443b
            int r9 = r9 - r6
            int r9 = r9 - r3
            int r10 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3442a
            int r9 = r9 * r10
            int r9 = r9 + r7
            r5[r9] = r8
            int r7 = r7 + 1
            goto L_0x008d
        L_0x00b5:
            int r6 = r6 + 1
            goto L_0x0088
        L_0x00b8:
            int r4 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3442a
            int r6 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3443b
            android.graphics.Bitmap$Config r7 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r4 = android.graphics.Bitmap.createBitmap(r5, r4, r6, r7)
        L_0x00c2:
            if (r1 == 0) goto L_0x004b
            r1.mo11539a(r4)
            goto L_0x004b
        L_0x00c8:
            int r0 = r13.what
            r1 = 39
            r4 = 1099956224(0x41900000, float:18.0)
            if (r0 != r1) goto L_0x020c
            com.baidu.mapsdkplatform.comapi.map.ac r0 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r0 = r0.f3450i
            if (r0 != 0) goto L_0x00d9
            return
        L_0x00d9:
            int r0 = r13.arg1
            r1 = 100
            if (r0 != r1) goto L_0x00e9
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            r13.mo12986B()
            goto L_0x014a
        L_0x00e9:
            int r0 = r13.arg1
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x00f9
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            r13.mo12996L()
            goto L_0x014a
        L_0x00f9:
            int r0 = r13.arg1
            if (r0 != r3) goto L_0x010f
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.m r13 = r13.f3449h
            if (r13 == 0) goto L_0x014a
        L_0x0105:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.m r13 = r13.f3449h
            r13.mo13115a()
            goto L_0x014a
        L_0x010f:
            int r0 = r13.arg1
            if (r0 != 0) goto L_0x011c
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.m r13 = r13.f3449h
            if (r13 == 0) goto L_0x014a
            goto L_0x0105
        L_0x011c:
            int r13 = r13.arg1
            r0 = 2
            if (r13 != r0) goto L_0x014a
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            if (r13 != 0) goto L_0x012c
            return
        L_0x012c:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            java.util.Iterator r13 = r13.iterator()
        L_0x0138:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x014a
            java.lang.Object r0 = r13.next()
            com.baidu.mapsdkplatform.comapi.map.l r0 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r0
            if (r0 == 0) goto L_0x0138
            r0.mo11551c()
            goto L_0x0138
        L_0x014a:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            boolean r13 = r13.f3535k
            if (r13 != 0) goto L_0x0199
            int r13 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3443b
            if (r13 <= 0) goto L_0x0199
            int r13 = com.baidu.mapsdkplatform.comapi.map.C1066ac.f3442a
            if (r13 <= 0) goto L_0x0199
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            com.baidu.mapapi.model.inner.GeoPoint r13 = r13.mo13030b(r2, r2)
            if (r13 == 0) goto L_0x0199
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            r13.f3535k = r3
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            if (r13 != 0) goto L_0x017b
            return
        L_0x017b:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            java.util.Iterator r13 = r13.iterator()
        L_0x0187:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x0199
            java.lang.Object r0 = r13.next()
            com.baidu.mapsdkplatform.comapi.map.l r0 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r0
            if (r0 == 0) goto L_0x0187
            r0.mo11547b()
            goto L_0x0187
        L_0x0199:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            if (r13 != 0) goto L_0x01a4
            return
        L_0x01a4:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            java.util.Iterator r13 = r13.iterator()
        L_0x01b0:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x01c2
            java.lang.Object r0 = r13.next()
            com.baidu.mapsdkplatform.comapi.map.l r0 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r0
            if (r0 == 0) goto L_0x01b0
            r0.mo11538a()
            goto L_0x01b0
        L_0x01c2:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            boolean r13 = r13.mo13070q()
            if (r13 == 0) goto L_0x0301
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            if (r13 != 0) goto L_0x01d9
            return
        L_0x01d9:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            java.util.Iterator r13 = r13.iterator()
        L_0x01e5:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x0301
            java.lang.Object r0 = r13.next()
            com.baidu.mapsdkplatform.comapi.map.l r0 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r0
            if (r0 != 0) goto L_0x01f4
            goto L_0x01e5
        L_0x01f4:
            com.baidu.mapsdkplatform.comapi.map.ac r1 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r1 = r1.f3450i
            com.baidu.mapsdkplatform.comapi.map.ab r1 = r1.mo12989E()
            float r1 = r1.f3414a
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 < 0) goto L_0x0208
            r0.mo11545a(r3)
            goto L_0x01e5
        L_0x0208:
            r0.mo11545a(r2)
            goto L_0x01e5
        L_0x020c:
            int r0 = r13.what
            r1 = 41
            if (r0 != r1) goto L_0x0286
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            if (r13 != 0) goto L_0x021b
            return
        L_0x021b:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            boolean r13 = r13.f3537n
            if (r13 != 0) goto L_0x022f
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            boolean r13 = r13.f3538o
            if (r13 == 0) goto L_0x0301
        L_0x022f:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            if (r13 != 0) goto L_0x023a
            return
        L_0x023a:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            java.util.Iterator r13 = r13.iterator()
        L_0x0246:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x0301
            java.lang.Object r0 = r13.next()
            com.baidu.mapsdkplatform.comapi.map.l r0 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r0
            if (r0 != 0) goto L_0x0255
            goto L_0x0246
        L_0x0255:
            com.baidu.mapsdkplatform.comapi.map.ac r1 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r1 = r1.f3450i
            com.baidu.mapsdkplatform.comapi.map.ab r1 = r1.mo12989E()
            r0.mo11549b(r1)
            com.baidu.mapsdkplatform.comapi.map.ac r1 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r1 = r1.f3450i
            boolean r1 = r1.mo13070q()
            if (r1 == 0) goto L_0x0246
            com.baidu.mapsdkplatform.comapi.map.ac r1 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r1 = r1.f3450i
            com.baidu.mapsdkplatform.comapi.map.ab r1 = r1.mo12989E()
            float r1 = r1.f3414a
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 < 0) goto L_0x0282
            r0.mo11545a(r3)
            goto L_0x0246
        L_0x0282:
            r0.mo11545a(r2)
            goto L_0x0246
        L_0x0286:
            int r0 = r13.what
            r1 = 999(0x3e7, float:1.4E-42)
            if (r0 != r1) goto L_0x02b5
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            if (r13 != 0) goto L_0x0297
            return
        L_0x0297:
            com.baidu.mapsdkplatform.comapi.map.ac r13 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r13 = r13.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r13 = r13.f3532h
            java.util.Iterator r13 = r13.iterator()
        L_0x02a3:
            boolean r0 = r13.hasNext()
            if (r0 == 0) goto L_0x0301
            java.lang.Object r0 = r13.next()
            com.baidu.mapsdkplatform.comapi.map.l r0 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r0
            if (r0 == 0) goto L_0x02a3
            r0.mo11556e()
            goto L_0x02a3
        L_0x02b5:
            int r0 = r13.what
            r1 = 50
            if (r0 != r1) goto L_0x0301
            com.baidu.mapsdkplatform.comapi.map.ac r0 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r0 = r0.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r0 = r0.f3532h
            if (r0 != 0) goto L_0x02c6
            return
        L_0x02c6:
            com.baidu.mapsdkplatform.comapi.map.ac r0 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r0 = r0.f3450i
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r0 = r0.f3532h
            java.util.Iterator r0 = r0.iterator()
        L_0x02d2:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0301
            java.lang.Object r1 = r0.next()
            com.baidu.mapsdkplatform.comapi.map.l r1 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r1
            if (r1 != 0) goto L_0x02e1
            goto L_0x02d2
        L_0x02e1:
            int r5 = r13.arg1
            if (r5 != 0) goto L_0x02e9
        L_0x02e5:
            r1.mo11545a(r2)
            goto L_0x02d2
        L_0x02e9:
            int r5 = r13.arg1
            if (r5 != r3) goto L_0x02d2
            com.baidu.mapsdkplatform.comapi.map.ac r5 = r12.f3451a
            com.baidu.mapsdkplatform.comapi.map.e r5 = r5.f3450i
            com.baidu.mapsdkplatform.comapi.map.ab r5 = r5.mo12989E()
            float r5 = r5.f3414a
            int r5 = (r5 > r4 ? 1 : (r5 == r4 ? 0 : -1))
            if (r5 < 0) goto L_0x02e5
            r1.mo11545a(r3)
            goto L_0x02d2
        L_0x0301:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.map.C1067ad.handleMessage(android.os.Message):void");
    }
}
