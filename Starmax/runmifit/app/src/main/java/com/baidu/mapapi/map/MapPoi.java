package com.baidu.mapapi.map;

import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import org.json.JSONObject;

public final class MapPoi {

    /* renamed from: d */
    private static final String f2534d = MapPoi.class.getSimpleName();

    /* renamed from: a */
    String f2535a;

    /* renamed from: b */
    LatLng f2536b;

    /* renamed from: c */
    String f2537c;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo11174a(JSONObject jSONObject) {
        this.f2535a = jSONObject.optString("tx");
        String str = this.f2535a;
        if (str != null && !str.equals("")) {
            this.f2535a = this.f2535a.replaceAll("\\\\", "").replaceAll("/?[a-zA-Z]{1,10};", "").replaceAll("<[^>]*>", "").replaceAll("[(/>)<]", "");
        }
        this.f2536b = CoordUtil.decodeNodeLocation(jSONObject.optString("geo"));
        this.f2537c = jSONObject.optString("ud");
    }

    public String getName() {
        return this.f2535a;
    }

    public LatLng getPosition() {
        return this.f2536b;
    }

    public String getUid() {
        return this.f2537c;
    }
}
