package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.map.Overlay;

/* renamed from: com.baidu.mapapi.map.a */
class C0923a implements Overlay.C0921a {

    /* renamed from: a */
    final /* synthetic */ BaiduMap f2833a;

    C0923a(BaiduMap baiduMap) {
        this.f2833a = baiduMap;
    }

    /* renamed from: a */
    public void mo11329a(Overlay overlay) {
        if (overlay != null && this.f2833a.f2409k.contains(overlay)) {
            Bundle a = overlay.mo11321a();
            if (this.f2833a.f2407i != null) {
                this.f2833a.f2407i.mo13043d(a);
            }
            this.f2833a.f2409k.remove(overlay);
        }
        if (overlay != null && this.f2833a.f2411m.contains(overlay)) {
            this.f2833a.f2411m.remove(overlay);
        }
        if (overlay != null && this.f2833a.f2410l.contains(overlay)) {
            Marker marker = (Marker) overlay;
            if (marker.f2622o != null) {
                this.f2833a.f2410l.remove(marker);
                if (this.f2833a.f2410l.size() == 0 && this.f2833a.f2407i != null) {
                    this.f2833a.f2407i.mo13035b(false);
                }
            }
        }
    }

    /* renamed from: b */
    public void mo11330b(Overlay overlay) {
        if (overlay != null && this.f2833a.f2409k.contains(overlay)) {
            boolean z = false;
            if (overlay instanceof Marker) {
                Marker marker = (Marker) overlay;
                if (marker.f2609b != null) {
                    if (marker.f2622o != null && marker.f2622o.size() > 1) {
                        Bundle bundle = new Bundle();
                        if (this.f2833a.f2407i != null) {
                            marker.remove();
                            marker.f2622o.clear();
                            this.f2833a.f2407i.mo13033b(overlay.mo10900a(bundle));
                            this.f2833a.f2409k.add(overlay);
                            z = true;
                        }
                    }
                } else if (!(marker.f2622o == null || marker.f2622o.size() == 0)) {
                    if (this.f2833a.f2410l.contains(marker)) {
                        this.f2833a.f2410l.remove(marker);
                    }
                    this.f2833a.f2410l.add(marker);
                    if (this.f2833a.f2407i != null) {
                        this.f2833a.f2407i.mo13035b(true);
                    }
                }
            }
            if (this.f2833a.f2407i != null && !z) {
                this.f2833a.f2407i.mo13038c(overlay.mo10900a(new Bundle()));
            }
        }
        if (this.f2833a.f2411m.contains(overlay)) {
            this.f2833a.f2411m.remove(overlay);
        }
        if (overlay instanceof Marker) {
            this.f2833a.f2411m.add((Marker) overlay);
        }
    }
}
