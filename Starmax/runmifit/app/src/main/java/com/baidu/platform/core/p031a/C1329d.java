package com.baidu.platform.core.p031a;

import com.baidu.mapapi.search.district.DistrictSearchOption;
import com.baidu.mapapi.search.district.OnGetDistricSearchResultListener;
import com.baidu.platform.base.C1316a;
import com.baidu.platform.base.SearchType;

/* renamed from: com.baidu.platform.core.a.d */
public class C1329d extends C1316a implements C1330e {

    /* renamed from: b */
    private OnGetDistricSearchResultListener f4452b = null;

    /* renamed from: a */
    public void mo14035a() {
        this.f4422a.lock();
        this.f4452b = null;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public void mo14036a(OnGetDistricSearchResultListener onGetDistricSearchResultListener) {
        this.f4422a.lock();
        this.f4452b = onGetDistricSearchResultListener;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public boolean mo14037a(DistrictSearchOption districtSearchOption) {
        C1327b bVar = new C1327b();
        bVar.mo14021a(SearchType.DISTRICT_SEARCH);
        return mo14016a(new C1326a(districtSearchOption), this.f4452b, bVar);
    }
}
