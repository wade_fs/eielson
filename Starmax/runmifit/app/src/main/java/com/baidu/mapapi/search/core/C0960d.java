package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.core.d */
final class C0960d implements Parcelable.Creator<PlaneInfo> {
    C0960d() {
    }

    /* renamed from: a */
    public PlaneInfo createFromParcel(Parcel parcel) {
        return new PlaneInfo(parcel);
    }

    /* renamed from: a */
    public PlaneInfo[] newArray(int i) {
        return new PlaneInfo[i];
    }
}
