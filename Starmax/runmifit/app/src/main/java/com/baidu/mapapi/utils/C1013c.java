package com.baidu.mapapi.utils;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.baidu.mapframework.open.aidl.C1024b;
import com.baidu.mapframework.open.aidl.IComOpenClient;

/* renamed from: com.baidu.mapapi.utils.c */
final class C1013c extends C1024b.C1025a {

    /* renamed from: a */
    final /* synthetic */ int f3259a;

    C1013c(int i) {
        this.f3259a = i;
    }

    /* renamed from: a */
    public void mo12827a(IBinder iBinder) throws RemoteException {
        Log.d(C1012b.f3239c, "onClientReady");
        if (C1012b.f3241e != null) {
            IComOpenClient unused = C1012b.f3241e = (IComOpenClient) null;
        }
        IComOpenClient unused2 = C1012b.f3241e = IComOpenClient.C1019a.m3316a(iBinder);
        C1012b.m3272a(this.f3259a);
        boolean unused3 = C1012b.f3256t = true;
    }
}
