package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;

public class PlanNode implements Parcelable {
    public static final Parcelable.Creator<PlanNode> CREATOR = new C0997m();

    /* renamed from: a */
    private LatLng f3094a = null;

    /* renamed from: b */
    private String f3095b = null;

    /* renamed from: c */
    private String f3096c = null;

    protected PlanNode(Parcel parcel) {
        this.f3094a = (LatLng) parcel.readValue(LatLng.class.getClassLoader());
        this.f3095b = parcel.readString();
        this.f3096c = parcel.readString();
    }

    PlanNode(LatLng latLng, String str, String str2) {
        this.f3094a = latLng;
        this.f3095b = str;
        this.f3096c = str2;
    }

    public static PlanNode withCityCodeAndPlaceName(int i, String str) {
        return new PlanNode(null, String.valueOf(i), str);
    }

    public static PlanNode withCityNameAndPlaceName(String str, String str2) {
        return new PlanNode(null, str, str2);
    }

    public static PlanNode withLocation(LatLng latLng) {
        return new PlanNode(latLng, null, null);
    }

    public int describeContents() {
        return 0;
    }

    public String getCity() {
        return this.f3095b;
    }

    public LatLng getLocation() {
        return this.f3094a;
    }

    public String getName() {
        return this.f3096c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.f3094a);
        parcel.writeString(this.f3095b);
        parcel.writeString(this.f3096c);
    }
}
