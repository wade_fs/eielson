package com.baidu.mobstat;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONArray;
import org.json.JSONObject;

public class ExceptionAnalysis {

    /* renamed from: a */
    private static ExceptionAnalysis f4131a = new ExceptionAnalysis();

    /* renamed from: b */
    private boolean f4132b = false;

    /* renamed from: c */
    private Context f4133c;

    /* renamed from: d */
    private HeadObject f4134d = new HeadObject();
    public Callback mCallback;

    public interface Callback {
        void onCallback(JSONObject jSONObject);
    }

    public static ExceptionAnalysis getInstance() {
        return f4131a;
    }

    private ExceptionAnalysis() {
    }

    public ExceptionAnalysis(Callback callback) {
        this.mCallback = callback;
    }

    public void openExceptionAnalysis(Context context, boolean z) {
        if (context != null) {
            this.f4133c = context.getApplicationContext();
        }
        if (this.f4133c != null && !this.f4132b) {
            this.f4132b = true;
            C1241ak.m4570a().mo13914a(this.f4133c);
            if (!z) {
                NativeCrashHandler.init(this.f4133c);
            }
        }
    }

    public void saveCrashInfo(Context context, Throwable th, boolean z) {
        int i;
        if (context != null) {
            this.f4133c = context.getApplicationContext();
        }
        if (this.f4133c != null) {
            String th2 = th.toString();
            String str = "";
            if (!TextUtils.isEmpty(th2)) {
                try {
                    str = th2.length() > 1 ? th2.split(Config.TRACE_TODAY_VISIT_SPLIT)[0] : th2;
                } catch (Exception unused) {
                }
            }
            String str2 = TextUtils.isEmpty(str) ? th2 : str;
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            String obj = stringWriter.toString();
            if (!z) {
                i = th instanceof Exception ? 11 : th instanceof Error ? 12 : 13;
            } else {
                i = 0;
            }
            saveCrashInfo(this.f4133c, System.currentTimeMillis(), obj, str2, 0, i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    public void saveCrashInfo(Context context, long j, String str, String str2, int i, int i2) {
        BDStatCore.instance().autoTrackSessionEndTime(context);
        if (context != null && str != null && !str.trim().equals("")) {
            try {
                String appVersionName = CooperService.instance().getAppVersionName(context);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("t", j);
                jSONObject.put("c", str);
                jSONObject.put("y", str2);
                jSONObject.put("v", appVersionName);
                jSONObject.put(Config.EXCEPTION_CRASH_TYPE, i);
                jSONObject.put("mem", m4439a(context));
                jSONObject.put(Config.EXCEPTION_CRASH_CHANNEL, i2);
                JSONArray jSONArray = new JSONArray();
                jSONArray.put(jSONObject);
                JSONObject jSONObject2 = new JSONObject();
                this.f4134d.installHeader(context, jSONObject2);
                jSONObject2.put("ss", 0);
                jSONObject2.put(Config.SEQUENCE_INDEX, 0);
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put(Config.HEADER_PART, jSONObject2);
                jSONObject3.put(Config.PRINCIPAL_PART, new JSONArray());
                jSONObject3.put(Config.EVENT_PART, new JSONArray());
                jSONObject3.put(Config.EXCEPTION_PART, jSONArray);
                jSONObject3.put(Config.TRACE_PART, m4438a());
                if (this.mCallback != null) {
                    this.mCallback.onCallback(jSONObject3);
                }
                C1267ba.m4681a(context, Config.PREFIX_SEND_DATA + System.currentTimeMillis(), jSONObject3.toString(), false);
                C1256at.m4629c().mo13941a("dump exception, exception: " + str);
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: a */
    private JSONObject m4438a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Config.TRACE_APPLICATION_SESSION, 0);
        } catch (Exception unused) {
        }
        try {
            jSONObject.put(Config.TRACE_FAILED_CNT, 0);
        } catch (Exception unused2) {
        }
        return jSONObject;
    }

    /* renamed from: a */
    private JSONObject m4439a(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager == null) {
            return null;
        }
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        JSONObject jSONObject = new JSONObject();
        try {
            if (Build.VERSION.SDK_INT >= 16) {
                jSONObject.put(Config.EXCEPTION_MEMORY_TOTAL, memoryInfo.totalMem);
            }
            jSONObject.put(Config.EXCEPTION_MEMORY_FREE, memoryInfo.availMem);
            jSONObject.put(Config.EXCEPTION_MEMORY_LOW, memoryInfo.lowMemory ? 1 : 0);
        } catch (Exception unused) {
        }
        return jSONObject;
    }
}
