package com.baidu.location.p016d;

import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import com.baidu.location.Jni;
import com.baidu.location.p018f.C0840a;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0855k;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;
import org.json.JSONObject;

/* renamed from: com.baidu.location.d.f */
final class C0800f {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final C0804h f1621a;

    /* renamed from: b */
    private final SQLiteDatabase f1622b;

    /* renamed from: c */
    private final C0802a f1623c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f1624d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public boolean f1625e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public boolean f1626f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public boolean f1627g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public boolean f1628h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public String[] f1629i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public boolean f1630j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public boolean f1631k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public int f1632l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public int f1633m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public int f1634n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public double f1635o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public double f1636p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public double f1637q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public double f1638r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public double f1639s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public int f1640t;

    /* renamed from: u */
    private boolean f1641u = true;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public long f1642v = 8000;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public long f1643w = 5000;
    /* access modifiers changed from: private */

    /* renamed from: x */
    public long f1644x = 5000;
    /* access modifiers changed from: private */

    /* renamed from: y */
    public long f1645y = 5000;
    /* access modifiers changed from: private */

    /* renamed from: z */
    public long f1646z = 5000;

    /* renamed from: com.baidu.location.d.f$a */
    private final class C0802a extends C0849e {

        /* renamed from: b */
        private int f1648b;

        /* renamed from: c */
        private long f1649c;

        /* renamed from: d */
        private long f1650d;

        /* renamed from: e */
        private boolean f1651e;

        /* renamed from: f */
        private final String f1652f;

        private C0802a() {
            this.f1648b = 0;
            this.f1651e = false;
            this.f1649c = -1;
            this.f1650d = -1;
            this.f1885k = new HashMap();
            this.f1652f = Jni.encodeOfflineLocationUpdateRequest(String.format(Locale.US, "&ver=%s&cuid=%s&prod=%s:%s&sdk=%.2f&mb=%s&os=A%s", "1", C0844b.m2417a().f1856c, C0844b.f1849f, C0844b.f1848e, Float.valueOf(7.82f), Build.MODEL, Build.VERSION.SDK));
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m2187b() {
            boolean z;
            if (!this.f1651e) {
                try {
                    File file = new File(C0800f.this.f1621a.mo10592c(), "ofl.config");
                    if (this.f1650d == -1 && file.exists()) {
                        Scanner scanner = new Scanner(file);
                        String next = scanner.next();
                        scanner.close();
                        JSONObject jSONObject = new JSONObject(next);
                        boolean unused = C0800f.this.f1624d = jSONObject.getBoolean("ol");
                        boolean unused2 = C0800f.this.f1625e = jSONObject.getBoolean("fl");
                        boolean unused3 = C0800f.this.f1626f = jSONObject.getBoolean("on");
                        boolean unused4 = C0800f.this.f1627g = jSONObject.getBoolean("wn");
                        boolean unused5 = C0800f.this.f1628h = jSONObject.getBoolean("oc");
                        this.f1650d = jSONObject.getLong("t");
                        if (jSONObject.has("ol")) {
                            boolean unused6 = C0800f.this.f1631k = jSONObject.getBoolean("olv2");
                        }
                        if (jSONObject.has("cplist")) {
                            String[] unused7 = C0800f.this.f1629i = jSONObject.getString("cplist").split(";");
                        }
                        if (jSONObject.has("rgcgp")) {
                            int unused8 = C0800f.this.f1632l = jSONObject.getInt("rgcgp");
                        }
                        if (jSONObject.has("rgcon")) {
                            boolean unused9 = C0800f.this.f1630j = jSONObject.getBoolean("rgcon");
                        }
                        if (jSONObject.has("addrup")) {
                            int unused10 = C0800f.this.f1634n = jSONObject.getInt("addrup");
                        }
                        if (jSONObject.has("poiup")) {
                            int unused11 = C0800f.this.f1633m = jSONObject.getInt("poiup");
                        }
                        if (jSONObject.has("oflp")) {
                            JSONObject jSONObject2 = jSONObject.getJSONObject("oflp");
                            if (jSONObject2.has("0")) {
                                double unused12 = C0800f.this.f1635o = jSONObject2.getDouble("0");
                            }
                            if (jSONObject2.has("1")) {
                                double unused13 = C0800f.this.f1636p = jSONObject2.getDouble("1");
                            }
                            if (jSONObject2.has("2")) {
                                double unused14 = C0800f.this.f1637q = jSONObject2.getDouble("2");
                            }
                            if (jSONObject2.has("3")) {
                                double unused15 = C0800f.this.f1638r = jSONObject2.getDouble("3");
                            }
                            if (jSONObject2.has("4")) {
                                double unused16 = C0800f.this.f1639s = jSONObject2.getDouble("4");
                            }
                        }
                        if (jSONObject.has("onlt")) {
                            JSONObject jSONObject3 = jSONObject.getJSONObject("onlt");
                            if (jSONObject3.has("0")) {
                                long unused17 = C0800f.this.f1646z = jSONObject3.getLong("0");
                            }
                            if (jSONObject3.has("1")) {
                                long unused18 = C0800f.this.f1645y = jSONObject3.getLong("1");
                            }
                            if (jSONObject3.has("2")) {
                                long unused19 = C0800f.this.f1642v = jSONObject3.getLong("2");
                            }
                            if (jSONObject3.has("3")) {
                                long unused20 = C0800f.this.f1643w = jSONObject3.getLong("3");
                            }
                            if (jSONObject3.has("4")) {
                                long unused21 = C0800f.this.f1644x = jSONObject3.getLong("4");
                            }
                        }
                        if (jSONObject.has("minapn")) {
                            int unused22 = C0800f.this.f1640t = jSONObject.getInt("minapn");
                        }
                    }
                    if (this.f1650d == -1) {
                        file.exists();
                    }
                    z = this.f1650d != -1 && this.f1650d + 86400000 <= System.currentTimeMillis();
                } catch (Exception unused23) {
                    z = false;
                }
                if ((this.f1650d == -1 || z) && m2188c() && C0855k.m2457a(C0800f.this.f1621a.mo10591b())) {
                    this.f1651e = true;
                    if (!C0855k.m2460b()) {
                        C0840a.m2405a().postDelayed(new C0803g(this), 1000);
                    }
                }
            }
        }

        /* renamed from: c */
        private boolean m2188c() {
            if (this.f1648b < 2) {
                return true;
            }
            if (this.f1649c + 86400000 >= System.currentTimeMillis()) {
                return false;
            }
            this.f1648b = 0;
            this.f1649c = -1;
            return true;
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1885k.clear();
            this.f1885k.put("qt", "conf");
            this.f1885k.put("req", this.f1652f);
            this.f1882h = C0804h.f1655b;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.d.f.a(com.baidu.location.d.f, boolean):boolean
         arg types: [com.baidu.location.d.f, int]
         candidates:
          com.baidu.location.d.f.a(com.baidu.location.d.f, double):double
          com.baidu.location.d.f.a(com.baidu.location.d.f, int):int
          com.baidu.location.d.f.a(com.baidu.location.d.f, long):long
          com.baidu.location.d.f.a(com.baidu.location.d.f, java.lang.String[]):java.lang.String[]
          com.baidu.location.d.f.a(com.baidu.location.d.f, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.d.f.b(com.baidu.location.d.f, boolean):boolean
         arg types: [com.baidu.location.d.f, int]
         candidates:
          com.baidu.location.d.f.b(com.baidu.location.d.f, double):double
          com.baidu.location.d.f.b(com.baidu.location.d.f, int):int
          com.baidu.location.d.f.b(com.baidu.location.d.f, long):long
          com.baidu.location.d.f.b(com.baidu.location.d.f, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.d.f.c(com.baidu.location.d.f, boolean):boolean
         arg types: [com.baidu.location.d.f, int]
         candidates:
          com.baidu.location.d.f.c(com.baidu.location.d.f, double):double
          com.baidu.location.d.f.c(com.baidu.location.d.f, int):int
          com.baidu.location.d.f.c(com.baidu.location.d.f, long):long
          com.baidu.location.d.f.c(com.baidu.location.d.f, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.d.f.d(com.baidu.location.d.f, boolean):boolean
         arg types: [com.baidu.location.d.f, int]
         candidates:
          com.baidu.location.d.f.d(com.baidu.location.d.f, double):double
          com.baidu.location.d.f.d(com.baidu.location.d.f, int):int
          com.baidu.location.d.f.d(com.baidu.location.d.f, long):long
          com.baidu.location.d.f.d(com.baidu.location.d.f, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.d.f.e(com.baidu.location.d.f, boolean):boolean
         arg types: [com.baidu.location.d.f, int]
         candidates:
          com.baidu.location.d.f.e(com.baidu.location.d.f, double):double
          com.baidu.location.d.f.e(com.baidu.location.d.f, long):long
          com.baidu.location.d.f.e(com.baidu.location.d.f, boolean):boolean */
        /* renamed from: a */
        public void mo10446a(boolean z) {
            int i;
            int i2;
            String str;
            String str2;
            String str3;
            if (!z || this.f1884j == null) {
                i = 1;
                i2 = this.f1648b;
            } else {
                try {
                    JSONObject jSONObject = new JSONObject(this.f1884j);
                    long j = 0;
                    if (jSONObject.has("ofl")) {
                        j = jSONObject.getLong("ofl");
                    }
                    String string = jSONObject.has("ver") ? jSONObject.getString("ver") : "1";
                    if ((1 & j) == 1) {
                        boolean unused = C0800f.this.f1624d = true;
                    }
                    if ((2 & j) == 2) {
                        boolean unused2 = C0800f.this.f1625e = true;
                    }
                    if ((4 & j) == 4) {
                        boolean unused3 = C0800f.this.f1626f = true;
                    }
                    if ((8 & j) == 8) {
                        boolean unused4 = C0800f.this.f1627g = true;
                    }
                    if ((16 & j) == 16) {
                        boolean unused5 = C0800f.this.f1628h = true;
                    }
                    if ((32 & j) == 32) {
                        boolean unused6 = C0800f.this.f1630j = true;
                    }
                    if ((j & 64) == 64) {
                        boolean unused7 = C0800f.this.f1631k = true;
                    }
                    JSONObject jSONObject2 = new JSONObject();
                    if (jSONObject.has("cplist")) {
                        String[] unused8 = C0800f.this.f1629i = jSONObject.getString("cplist").split(";");
                        jSONObject2.put("cplist", jSONObject.getString("cplist"));
                    }
                    if (jSONObject.has("bklist")) {
                        C0800f.this.mo10571a(jSONObject.getString("bklist").split(";"));
                    }
                    String str4 = string;
                    if (jSONObject.has("para")) {
                        JSONObject jSONObject3 = jSONObject.getJSONObject("para");
                        if (jSONObject3.has("rgcgp")) {
                            str3 = "ver";
                            int unused9 = C0800f.this.f1632l = jSONObject3.getInt("rgcgp");
                        } else {
                            str3 = "ver";
                        }
                        if (jSONObject3.has("addrup")) {
                            int unused10 = C0800f.this.f1634n = jSONObject3.getInt("addrup");
                        }
                        if (jSONObject3.has("poiup")) {
                            int unused11 = C0800f.this.f1633m = jSONObject3.getInt("poiup");
                        }
                        if (jSONObject3.has("oflp")) {
                            JSONObject jSONObject4 = jSONObject3.getJSONObject("oflp");
                            if (jSONObject4.has("0")) {
                                str2 = "poiup";
                                str = "addrup";
                                double unused12 = C0800f.this.f1635o = jSONObject4.getDouble("0");
                            } else {
                                str2 = "poiup";
                                str = "addrup";
                            }
                            if (jSONObject4.has("1")) {
                                double unused13 = C0800f.this.f1636p = jSONObject4.getDouble("1");
                            }
                            if (jSONObject4.has("2")) {
                                double unused14 = C0800f.this.f1637q = jSONObject4.getDouble("2");
                            }
                            if (jSONObject4.has("3")) {
                                double unused15 = C0800f.this.f1638r = jSONObject4.getDouble("3");
                            }
                            if (jSONObject4.has("4")) {
                                double unused16 = C0800f.this.f1639s = jSONObject4.getDouble("4");
                            }
                        } else {
                            str2 = "poiup";
                            str = "addrup";
                        }
                        if (jSONObject3.has("onlt")) {
                            JSONObject jSONObject5 = jSONObject3.getJSONObject("onlt");
                            if (jSONObject5.has("0")) {
                                long unused17 = C0800f.this.f1646z = jSONObject5.getLong("0");
                            }
                            if (jSONObject5.has("1")) {
                                long unused18 = C0800f.this.f1645y = jSONObject5.getLong("1");
                            }
                            if (jSONObject5.has("2")) {
                                long unused19 = C0800f.this.f1642v = jSONObject5.getLong("2");
                            }
                            if (jSONObject5.has("3")) {
                                long unused20 = C0800f.this.f1643w = jSONObject5.getLong("3");
                            }
                            if (jSONObject5.has("4")) {
                                long unused21 = C0800f.this.f1644x = jSONObject5.getLong("4");
                            }
                        }
                        if (jSONObject3.has("minapn")) {
                            int unused22 = C0800f.this.f1640t = jSONObject3.getInt("minapn");
                        }
                    } else {
                        str3 = "ver";
                        str2 = "poiup";
                        str = "addrup";
                    }
                    jSONObject2.put("ol", C0800f.this.f1624d);
                    jSONObject2.put("olv2", C0800f.this.f1631k);
                    jSONObject2.put("fl", C0800f.this.f1625e);
                    jSONObject2.put("on", C0800f.this.f1626f);
                    jSONObject2.put("wn", C0800f.this.f1627g);
                    jSONObject2.put("oc", C0800f.this.f1628h);
                    this.f1650d = System.currentTimeMillis();
                    jSONObject2.put("t", this.f1650d);
                    jSONObject2.put(str3, str4);
                    jSONObject2.put("rgcon", C0800f.this.f1630j);
                    jSONObject2.put("rgcgp", C0800f.this.f1632l);
                    JSONObject jSONObject6 = new JSONObject();
                    jSONObject6.put("0", C0800f.this.f1635o);
                    jSONObject6.put("1", C0800f.this.f1636p);
                    jSONObject6.put("2", C0800f.this.f1637q);
                    jSONObject6.put("3", C0800f.this.f1638r);
                    jSONObject6.put("4", C0800f.this.f1639s);
                    jSONObject2.put("oflp", jSONObject6);
                    JSONObject jSONObject7 = new JSONObject();
                    jSONObject7.put("0", C0800f.this.f1646z);
                    jSONObject7.put("1", C0800f.this.f1645y);
                    jSONObject7.put("2", C0800f.this.f1642v);
                    jSONObject7.put("3", C0800f.this.f1643w);
                    jSONObject7.put("4", C0800f.this.f1644x);
                    jSONObject2.put("onlt", jSONObject7);
                    jSONObject2.put(str, C0800f.this.f1634n);
                    jSONObject2.put(str2, C0800f.this.f1633m);
                    jSONObject2.put("minapn", C0800f.this.f1640t);
                    File file = new File(C0800f.this.f1621a.mo10592c(), "ofl.config");
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileWriter fileWriter = new FileWriter(file);
                    fileWriter.write(jSONObject2.toString());
                    fileWriter.close();
                } catch (Exception unused23) {
                    i2 = this.f1648b;
                    i = 1;
                }
                this.f1651e = false;
            }
            this.f1648b = i2 + i;
            this.f1649c = System.currentTimeMillis();
            this.f1651e = false;
        }
    }

    C0800f(C0804h hVar, SQLiteDatabase sQLiteDatabase) {
        this.f1621a = hVar;
        this.f1624d = false;
        this.f1625e = false;
        this.f1626f = false;
        this.f1627g = false;
        this.f1628h = false;
        this.f1630j = false;
        this.f1631k = false;
        this.f1632l = 6;
        this.f1633m = 30;
        this.f1634n = 30;
        this.f1635o = 0.0d;
        this.f1636p = 0.0d;
        this.f1637q = 0.0d;
        this.f1638r = 0.0d;
        this.f1639s = 0.0d;
        this.f1640t = 8;
        this.f1629i = new String[0];
        this.f1622b = sQLiteDatabase;
        this.f1623c = new C0802a();
        SQLiteDatabase sQLiteDatabase2 = this.f1622b;
        if (sQLiteDatabase2 != null && sQLiteDatabase2.isOpen()) {
            try {
                this.f1622b.execSQL("CREATE TABLE IF NOT EXISTS BLACK (name VARCHAR(100) PRIMARY KEY);");
            } catch (Exception unused) {
            }
        }
        mo10577g();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo10569a() {
        return this.f1640t;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public long mo10570a(String str) {
        if (str.equals("2G")) {
            return this.f1642v;
        }
        if (str.equals("3G")) {
            return this.f1643w;
        }
        if (str.equals("4G")) {
            return this.f1644x;
        }
        if (str.equals("WIFI")) {
            return this.f1645y;
        }
        if (str.equals("unknown")) {
            return this.f1646z;
        }
        return 5000;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10571a(String[] strArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strArr.length; i++) {
            if (i > 0) {
                stringBuffer.append(",");
            }
            stringBuffer.append("(\"");
            stringBuffer.append(strArr[i]);
            stringBuffer.append("\")");
        }
        SQLiteDatabase sQLiteDatabase = this.f1622b;
        if (sQLiteDatabase != null && sQLiteDatabase.isOpen() && stringBuffer.length() > 0) {
            try {
                this.f1622b.execSQL(String.format(Locale.US, "INSERT OR IGNORE INTO BLACK VALUES %s;", stringBuffer.toString()));
            } catch (Exception unused) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public double mo10572b() {
        return this.f1635o;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public double mo10573c() {
        return this.f1636p;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public double mo10574d() {
        return this.f1637q;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public double mo10575e() {
        return this.f1638r;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public double mo10576f() {
        return this.f1639s;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public void mo10577g() {
        this.f1623c.m2187b();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public boolean mo10578h() {
        return this.f1624d;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public boolean mo10579i() {
        return this.f1626f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j */
    public boolean mo10580j() {
        return this.f1627g;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k */
    public boolean mo10581k() {
        return this.f1625e;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public boolean mo10582l() {
        return this.f1630j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m */
    public boolean mo10583m() {
        return this.f1641u;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: n */
    public int mo10584n() {
        return this.f1632l;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: o */
    public String[] mo10585o() {
        return this.f1629i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: p */
    public int mo10586p() {
        return this.f1634n;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: q */
    public int mo10587q() {
        return this.f1633m;
    }
}
