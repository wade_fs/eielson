package com.baidu.location.p016d;

import android.database.Cursor;
import android.database.MatrixCursor;
import com.baidu.location.Address;
import com.baidu.location.BDLocation;
import com.baidu.location.Poi;
import com.baidu.location.p017e.C0821a;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0834h;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0855k;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

/* renamed from: com.baidu.location.d.j */
final class C0809j {

    /* renamed from: a */
    private static final String[] f1678a = {"CoorType", "Time", "LocType", "Longitude", "Latitude", "Radius", "NetworkLocationType", "Country", "CountryCode", "Province", "City", "CityCode", "District", "Street", "StreetNumber", "PoiList", "LocationDescription"};

    /* renamed from: com.baidu.location.d.j$a */
    static final class C0810a {

        /* renamed from: a */
        final String f1679a;

        /* renamed from: b */
        final String f1680b;

        /* renamed from: c */
        final boolean f1681c;

        /* renamed from: d */
        final boolean f1682d;

        /* renamed from: e */
        final boolean f1683e;

        /* renamed from: f */
        final int f1684f;

        /* renamed from: g */
        final BDLocation f1685g;

        /* renamed from: h */
        final boolean f1686h;

        /* renamed from: i */
        final LinkedHashMap<String, Integer> f1687i;

        public C0810a(String[] strArr) {
            boolean z;
            String str;
            String str2;
            BDLocation bDLocation;
            String str3;
            String[] strArr2;
            String[] strArr3 = strArr;
            if (strArr3 == null) {
                this.f1679a = null;
                this.f1680b = null;
                this.f1687i = null;
                this.f1681c = false;
                this.f1682d = false;
                this.f1683e = false;
                this.f1685g = null;
                this.f1686h = false;
                this.f1684f = 8;
                return;
            }
            LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<>();
            String str4 = null;
            String str5 = null;
            BDLocation bDLocation2 = null;
            int i = 0;
            boolean z2 = false;
            boolean z3 = false;
            boolean z4 = false;
            boolean z5 = false;
            int i2 = 8;
            while (i < strArr3.length) {
                try {
                    if (strArr3[i].equals("-loc")) {
                        str4 = strArr3[i + 1];
                        try {
                            String[] split = str4.split("&");
                            String str6 = str5;
                            int i3 = 0;
                            while (i3 < split.length) {
                                try {
                                    if (split[i3].startsWith("cl=")) {
                                        try {
                                            strArr2 = split;
                                            str6 = split[i3].substring(3);
                                        } catch (Exception unused) {
                                            str2 = str4;
                                            str5 = str6;
                                            str = str2;
                                            z = false;
                                            this.f1679a = str;
                                            this.f1680b = str5;
                                            this.f1687i = linkedHashMap;
                                            this.f1681c = z;
                                            this.f1682d = z3;
                                            this.f1683e = z4;
                                            this.f1684f = i2;
                                            this.f1685g = bDLocation2;
                                            this.f1686h = z5;
                                        }
                                    } else if (split[i3].startsWith("wf=")) {
                                        String[] split2 = split[i3].substring(3).split("\\|");
                                        strArr2 = split;
                                        int i4 = 0;
                                        while (i4 < split2.length) {
                                            String[] split3 = split2[i4].split(";");
                                            String[] strArr4 = split2;
                                            str3 = str4;
                                            if (split3.length >= 2) {
                                                try {
                                                    linkedHashMap.put(split3[0], Integer.valueOf(split3[1]));
                                                } catch (Exception unused2) {
                                                }
                                            }
                                            i4++;
                                            split2 = strArr4;
                                            str4 = str3;
                                        }
                                    } else {
                                        strArr2 = split;
                                    }
                                    i3++;
                                    split = strArr2;
                                    str4 = str4;
                                } catch (Exception unused3) {
                                    str3 = str4;
                                    str5 = str6;
                                    str2 = str3;
                                    str = str2;
                                    z = false;
                                    this.f1679a = str;
                                    this.f1680b = str5;
                                    this.f1687i = linkedHashMap;
                                    this.f1681c = z;
                                    this.f1682d = z3;
                                    this.f1683e = z4;
                                    this.f1684f = i2;
                                    this.f1685g = bDLocation2;
                                    this.f1686h = z5;
                                }
                            }
                            str5 = str6;
                        } catch (Exception unused4) {
                            str3 = str4;
                            str2 = str3;
                            str = str2;
                            z = false;
                            this.f1679a = str;
                            this.f1680b = str5;
                            this.f1687i = linkedHashMap;
                            this.f1681c = z;
                            this.f1682d = z3;
                            this.f1683e = z4;
                            this.f1684f = i2;
                            this.f1685g = bDLocation2;
                            this.f1686h = z5;
                        }
                    } else if (strArr3[i].equals("-com")) {
                        String[] split4 = strArr3[i + 1].split(";");
                        if (split4.length > 0) {
                            bDLocation = new BDLocation();
                            try {
                                bDLocation.setLatitude(Double.valueOf(split4[0]).doubleValue());
                                bDLocation.setLongitude(Double.valueOf(split4[1]).doubleValue());
                                bDLocation.setLocType(Integer.valueOf(split4[2]).intValue());
                                bDLocation.setNetworkLocationType(split4[3]);
                            } catch (Exception unused5) {
                                bDLocation2 = bDLocation;
                            }
                        } else {
                            bDLocation = bDLocation2;
                        }
                        bDLocation2 = bDLocation;
                    } else if (strArr3[i].equals("-log")) {
                        if (strArr3[i + 1].equals("true")) {
                            z2 = true;
                        }
                    } else if (strArr3[i].equals("-rgc")) {
                        if (strArr3[i + 1].equals("true")) {
                            z4 = true;
                        }
                    } else if (strArr3[i].equals("-poi")) {
                        if (strArr3[i + 1].equals("true")) {
                            z3 = true;
                        }
                    } else if (strArr3[i].equals("-minap")) {
                        try {
                            i2 = Integer.valueOf(strArr3[i + 1]).intValue();
                        } catch (Exception unused6) {
                        }
                    } else if (strArr3[i].equals("-des") && strArr3[i + 1].equals("true")) {
                        z5 = true;
                    }
                    i += 2;
                } catch (Exception unused7) {
                    str2 = str4;
                    str = str2;
                    z = false;
                    this.f1679a = str;
                    this.f1680b = str5;
                    this.f1687i = linkedHashMap;
                    this.f1681c = z;
                    this.f1682d = z3;
                    this.f1683e = z4;
                    this.f1684f = i2;
                    this.f1685g = bDLocation2;
                    this.f1686h = z5;
                }
            }
            z = true;
            str = !z2 ? null : str4;
            this.f1679a = str;
            this.f1680b = str5;
            this.f1687i = linkedHashMap;
            this.f1681c = z;
            this.f1682d = z3;
            this.f1683e = z4;
            this.f1684f = i2;
            this.f1685g = bDLocation2;
            this.f1686h = z5;
        }
    }

    /* renamed from: a */
    static Cursor m2218a(BDLocation bDLocation) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date(System.currentTimeMillis()));
        MatrixCursor matrixCursor = new MatrixCursor(f1678a);
        Object[] objArr = new Object[f1678a.length];
        objArr[matrixCursor.getColumnIndex("CoorType")] = CoordinateType.GCJ02;
        objArr[matrixCursor.getColumnIndex("Time")] = format;
        objArr[matrixCursor.getColumnIndex("LocType")] = Integer.valueOf(bDLocation.getLocType());
        objArr[matrixCursor.getColumnIndex("Longitude")] = Double.valueOf(bDLocation.getLongitude());
        objArr[matrixCursor.getColumnIndex("Latitude")] = Double.valueOf(bDLocation.getLatitude());
        objArr[matrixCursor.getColumnIndex("Radius")] = Float.valueOf(bDLocation.getRadius());
        objArr[matrixCursor.getColumnIndex("NetworkLocationType")] = bDLocation.getNetworkLocationType();
        Address address = bDLocation.getAddress();
        if (address != null) {
            str7 = address.country;
            str6 = address.countryCode;
            str5 = address.province;
            str4 = address.city;
            str3 = address.cityCode;
            str2 = address.district;
            str = address.street;
            str8 = address.streetNumber;
        } else {
            str8 = null;
            str7 = null;
            str6 = null;
            str5 = null;
            str4 = null;
            str3 = null;
            str2 = null;
            str = null;
        }
        objArr[matrixCursor.getColumnIndex("Country")] = str7;
        objArr[matrixCursor.getColumnIndex("CountryCode")] = str6;
        objArr[matrixCursor.getColumnIndex("Province")] = str5;
        objArr[matrixCursor.getColumnIndex("City")] = str4;
        objArr[matrixCursor.getColumnIndex("CityCode")] = str3;
        objArr[matrixCursor.getColumnIndex("District")] = str2;
        objArr[matrixCursor.getColumnIndex("Street")] = str;
        objArr[matrixCursor.getColumnIndex("StreetNumber")] = str8;
        List<Poi> poiList = bDLocation.getPoiList();
        if (poiList == null) {
            objArr[matrixCursor.getColumnIndex("PoiList")] = null;
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < poiList.size(); i++) {
                Poi poi = poiList.get(i);
                stringBuffer.append(poi.getId());
                stringBuffer.append(";");
                stringBuffer.append(poi.getName());
                stringBuffer.append(";");
                stringBuffer.append(poi.getRank());
                stringBuffer.append(";|");
            }
            objArr[matrixCursor.getColumnIndex("PoiList")] = stringBuffer.toString();
        }
        objArr[matrixCursor.getColumnIndex("LocationDescription")] = bDLocation.getLocationDescribe();
        matrixCursor.addRow(objArr);
        return matrixCursor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01be, code lost:
        if (r12.size() != 0) goto L_0x01c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01c5, code lost:
        if (r12.size() == 0) goto L_0x01c7;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.baidu.location.BDLocation m2219a(android.database.Cursor r23) {
        /*
            r0 = r23
            com.baidu.location.BDLocation r1 = new com.baidu.location.BDLocation
            r1.<init>()
            if (r0 == 0) goto L_0x0202
            int r2 = r23.getCount()
            if (r2 <= 0) goto L_0x0202
            boolean r2 = r23.moveToFirst()
            if (r2 == 0) goto L_0x0202
            r2 = 0
            java.lang.String r3 = "LocType"
            int r4 = r0.getColumnIndex(r3)
            r6 = -1
            if (r4 == r6) goto L_0x0028
            int r3 = r0.getColumnIndex(r3)
            int r3 = r0.getInt(r3)
            goto L_0x0029
        L_0x0028:
            r3 = 0
        L_0x0029:
            java.lang.String r4 = "Latitude"
            int r7 = r0.getColumnIndex(r4)
            r8 = 0
            if (r7 == r6) goto L_0x003c
            int r4 = r0.getColumnIndex(r4)
            double r10 = r0.getDouble(r4)
            goto L_0x003d
        L_0x003c:
            r10 = r8
        L_0x003d:
            java.lang.String r4 = "Longitude"
            int r7 = r0.getColumnIndex(r4)
            if (r7 == r6) goto L_0x004d
            int r4 = r0.getColumnIndex(r4)
            double r8 = r0.getDouble(r4)
        L_0x004d:
            java.lang.String r4 = "CoorType"
            int r7 = r0.getColumnIndex(r4)
            if (r7 == r6) goto L_0x005e
            int r4 = r0.getColumnIndex(r4)
            java.lang.String r4 = r0.getString(r4)
            goto L_0x005f
        L_0x005e:
            r4 = 0
        L_0x005f:
            java.lang.String r7 = "NetworkLocationType"
            int r13 = r0.getColumnIndex(r7)
            if (r13 == r6) goto L_0x0070
            int r7 = r0.getColumnIndex(r7)
            java.lang.String r7 = r0.getString(r7)
            goto L_0x0071
        L_0x0070:
            r7 = 0
        L_0x0071:
            java.lang.String r13 = "Radius"
            int r14 = r0.getColumnIndex(r13)
            if (r14 == r6) goto L_0x0081
            int r2 = r0.getColumnIndex(r13)
            float r2 = r0.getFloat(r2)
        L_0x0081:
            java.lang.String r13 = "Time"
            int r14 = r0.getColumnIndex(r13)
            if (r14 == r6) goto L_0x0092
            int r13 = r0.getColumnIndex(r13)
            java.lang.String r13 = r0.getString(r13)
            goto L_0x0093
        L_0x0092:
            r13 = 0
        L_0x0093:
            java.lang.String r14 = "Country"
            int r15 = r0.getColumnIndex(r14)
            if (r15 == r6) goto L_0x00a4
            int r14 = r0.getColumnIndex(r14)
            java.lang.String r14 = r0.getString(r14)
            goto L_0x00a5
        L_0x00a4:
            r14 = 0
        L_0x00a5:
            java.lang.String r15 = "CountryCode"
            int r12 = r0.getColumnIndex(r15)
            if (r12 == r6) goto L_0x00b6
            int r12 = r0.getColumnIndex(r15)
            java.lang.String r12 = r0.getString(r12)
            goto L_0x00b7
        L_0x00b6:
            r12 = 0
        L_0x00b7:
            java.lang.String r15 = "Province"
            int r5 = r0.getColumnIndex(r15)
            if (r5 == r6) goto L_0x00c8
            int r5 = r0.getColumnIndex(r15)
            java.lang.String r5 = r0.getString(r5)
            goto L_0x00c9
        L_0x00c8:
            r5 = 0
        L_0x00c9:
            java.lang.String r15 = "City"
            r17 = r7
            int r7 = r0.getColumnIndex(r15)
            if (r7 == r6) goto L_0x00dc
            int r7 = r0.getColumnIndex(r15)
            java.lang.String r7 = r0.getString(r7)
            goto L_0x00dd
        L_0x00dc:
            r7 = 0
        L_0x00dd:
            java.lang.String r15 = "CityCode"
            int r15 = r0.getColumnIndex(r15)
            if (r15 == r6) goto L_0x00f2
            java.lang.String r15 = "CityCode"
            int r15 = r0.getColumnIndex(r15)
            java.lang.String r15 = r0.getString(r15)
            r18 = r8
            goto L_0x00f5
        L_0x00f2:
            r18 = r8
            r15 = 0
        L_0x00f5:
            java.lang.String r8 = "District"
            int r8 = r0.getColumnIndex(r8)
            if (r8 == r6) goto L_0x0108
            java.lang.String r8 = "District"
            int r8 = r0.getColumnIndex(r8)
            java.lang.String r8 = r0.getString(r8)
            goto L_0x0109
        L_0x0108:
            r8 = 0
        L_0x0109:
            java.lang.String r9 = "Street"
            int r9 = r0.getColumnIndex(r9)
            if (r9 == r6) goto L_0x011e
            java.lang.String r9 = "Street"
            int r9 = r0.getColumnIndex(r9)
            java.lang.String r9 = r0.getString(r9)
            r20 = r10
            goto L_0x0121
        L_0x011e:
            r20 = r10
            r9 = 0
        L_0x0121:
            java.lang.String r10 = "StreetNumber"
            int r10 = r0.getColumnIndex(r10)
            if (r10 == r6) goto L_0x0134
            java.lang.String r10 = "StreetNumber"
            int r10 = r0.getColumnIndex(r10)
            java.lang.String r10 = r0.getString(r10)
            goto L_0x0135
        L_0x0134:
            r10 = 0
        L_0x0135:
            com.baidu.location.Address$Builder r11 = new com.baidu.location.Address$Builder
            r11.<init>()
            com.baidu.location.Address$Builder r11 = r11.country(r14)
            com.baidu.location.Address$Builder r11 = r11.countryCode(r12)
            com.baidu.location.Address$Builder r5 = r11.province(r5)
            com.baidu.location.Address$Builder r5 = r5.city(r7)
            com.baidu.location.Address$Builder r5 = r5.cityCode(r15)
            com.baidu.location.Address$Builder r5 = r5.district(r8)
            com.baidu.location.Address$Builder r5 = r5.street(r9)
            com.baidu.location.Address$Builder r5 = r5.streetNumber(r10)
            com.baidu.location.Address r5 = r5.build()
            java.lang.String r7 = "PoiList"
            int r7 = r0.getColumnIndex(r7)
            if (r7 == r6) goto L_0x01c7
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            java.lang.String r7 = "PoiList"
            int r7 = r0.getColumnIndex(r7)
            java.lang.String r7 = r0.getString(r7)
            if (r7 == 0) goto L_0x01c1
            java.lang.String r8 = "\\|"
            java.lang.String[] r7 = r7.split(r8)     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            r8 = 0
        L_0x017e:
            int r9 = r7.length     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            if (r8 >= r9) goto L_0x01c1
            r9 = r7[r8]     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            java.lang.String r10 = ";"
            java.lang.String[] r9 = r9.split(r10)     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            int r10 = r9.length     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            r11 = 3
            if (r10 < r11) goto L_0x01aa
            com.baidu.location.Poi r10 = new com.baidu.location.Poi     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            r11 = 0
            r14 = r9[r11]     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            r15 = 1
            r15 = r9[r15]     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            r16 = 2
            r9 = r9[r16]     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            java.lang.Double r9 = java.lang.Double.valueOf(r9)     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            r22 = r7
            double r6 = r9.doubleValue()     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            r10.<init>(r14, r15, r6)     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            r12.add(r10)     // Catch:{ Exception -> 0x01b9, all -> 0x01b3 }
            goto L_0x01ad
        L_0x01aa:
            r22 = r7
            r11 = 0
        L_0x01ad:
            int r8 = r8 + 1
            r7 = r22
            r6 = -1
            goto L_0x017e
        L_0x01b3:
            r0 = move-exception
            int r1 = r12.size()
            throw r0
        L_0x01b9:
            int r6 = r12.size()
            if (r6 != 0) goto L_0x01c8
            goto L_0x01c7
        L_0x01c1:
            int r6 = r12.size()
            if (r6 != 0) goto L_0x01c8
        L_0x01c7:
            r12 = 0
        L_0x01c8:
            java.lang.String r6 = "LocationDescription"
            int r6 = r0.getColumnIndex(r6)
            r7 = -1
            if (r6 == r7) goto L_0x01dc
            java.lang.String r6 = "LocationDescription"
            int r6 = r0.getColumnIndex(r6)
            java.lang.String r0 = r0.getString(r6)
            goto L_0x01dd
        L_0x01dc:
            r0 = 0
        L_0x01dd:
            r1.setTime(r13)
            r1.setRadius(r2)
            r1.setLocType(r3)
            r1.setCoorType(r4)
            r8 = r20
            r1.setLatitude(r8)
            r8 = r18
            r1.setLongitude(r8)
            r7 = r17
            r1.setNetworkLocationType(r7)
            r1.setAddr(r5)
            r1.setPoiList(r12)
            r1.setLocationDescribe(r0)
            goto L_0x0207
        L_0x0202:
            r0 = 67
            r1.setLocType(r0)
        L_0x0207:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0809j.m2219a(android.database.Cursor):com.baidu.location.BDLocation");
    }

    /* renamed from: a */
    static String m2220a(BDLocation bDLocation, int i) {
        if (bDLocation == null || bDLocation.getLocType() == 67) {
            return String.format(Locale.CHINA, "&ofl=%s|%d", "1", Integer.valueOf(i));
        }
        String format = String.format(Locale.CHINA, "&ofl=%s|%d|%f|%f|%d", "1", Integer.valueOf(i), Double.valueOf(bDLocation.getLongitude()), Double.valueOf(bDLocation.getLatitude()), Integer.valueOf((int) bDLocation.getRadius()));
        if (bDLocation.getAddress() != null) {
            format = format + "&ofaddr=" + bDLocation.getAddress().address;
        }
        if (bDLocation.getPoiList() != null && bDLocation.getPoiList().size() > 0) {
            Poi poi = bDLocation.getPoiList().get(0);
            format = format + String.format(Locale.US, "&ofpoi=%s|%s", poi.getId(), poi.getName());
        }
        if (C0844b.f1848e == null) {
            return format;
        }
        return format + String.format(Locale.US, "&pack=%s&sdk=%.3f", C0844b.f1848e, Float.valueOf(7.82f));
    }

    /* renamed from: a */
    static String m2221a(BDLocation bDLocation, BDLocation bDLocation2, C0810a aVar) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(bDLocation2 == null ? "&ofcl=0" : String.format(Locale.US, "&ofcl=1|%f|%f|%d", Double.valueOf(bDLocation2.getLongitude()), Double.valueOf(bDLocation2.getLatitude()), Integer.valueOf((int) bDLocation2.getRadius())));
        stringBuffer.append(bDLocation == null ? "&ofwf=0" : String.format(Locale.US, "&ofwf=1|%f|%f|%d", Double.valueOf(bDLocation.getLongitude()), Double.valueOf(bDLocation.getLatitude()), Integer.valueOf((int) bDLocation.getRadius())));
        stringBuffer.append((aVar == null || !aVar.f1683e) ? "&rgcn=0" : "&rgcn=1");
        stringBuffer.append((aVar == null || !aVar.f1682d) ? "&poin=0" : "&poin=1");
        stringBuffer.append((aVar == null || !aVar.f1686h) ? "&desc=0" : "&desc=1");
        if (aVar != null) {
            stringBuffer.append(String.format(Locale.US, "&aps=%d", Integer.valueOf(aVar.f1684f)));
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    static String[] m2222a(C0821a aVar, C0834h hVar, BDLocation bDLocation, String str, boolean z, int i) {
        ArrayList arrayList = new ArrayList();
        StringBuffer stringBuffer = new StringBuffer();
        if (aVar != null) {
            stringBuffer.append(C0822b.m2279a().mo10626b(aVar));
        }
        if (hVar != null) {
            stringBuffer.append(hVar.mo10665a(30));
        }
        if (stringBuffer.length() > 0) {
            if (str != null) {
                stringBuffer.append(str);
            }
            arrayList.add("-loc");
            arrayList.add(stringBuffer.toString());
        }
        if (bDLocation != null) {
            String format = String.format(Locale.US, "%f;%f;%d;%s", Double.valueOf(bDLocation.getLatitude()), Double.valueOf(bDLocation.getLongitude()), Integer.valueOf(bDLocation.getLocType()), bDLocation.getNetworkLocationType());
            arrayList.add("-com");
            arrayList.add(format);
        }
        if (z) {
            arrayList.add("-log");
            arrayList.add("true");
        }
        if (C0855k.f1963g.equals("all")) {
            arrayList.add("-rgc");
            arrayList.add("true");
        }
        if (C0855k.f1966j) {
            arrayList.add("-poi");
            arrayList.add("true");
        }
        if (C0855k.f1964h) {
            arrayList.add("-des");
            arrayList.add("true");
        }
        arrayList.add("-minap");
        arrayList.add(Integer.toString(i));
        String[] strArr = new String[arrayList.size()];
        arrayList.toArray(strArr);
        return strArr;
    }
}
