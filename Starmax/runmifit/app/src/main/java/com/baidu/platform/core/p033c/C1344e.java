package com.baidu.platform.core.p033c;

import android.util.Log;
import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.tamic.novate.download.MimeType;
import com.tencent.connect.common.Constants;

/* renamed from: com.baidu.platform.core.c.e */
public class C1344e extends C1320e {
    C1344e(PoiDetailSearchOption poiDetailSearchOption) {
        m5003a(poiDetailSearchOption);
    }

    /* renamed from: a */
    private void m5003a(PoiDetailSearchOption poiDetailSearchOption) {
        if (poiDetailSearchOption == null) {
            Log.e(C1344e.class.getSimpleName(), "Option is null");
            return;
        }
        if (!poiDetailSearchOption.isSearchByUids()) {
            poiDetailSearchOption.poiUids(poiDetailSearchOption.getUid());
        }
        this.f4435a.mo14094a("uids", poiDetailSearchOption.getUids());
        this.f4435a.mo14094a("output", MimeType.JSON);
        this.f4435a.mo14094a(Constants.PARAM_SCOPE, "2");
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14077b();
    }
}
