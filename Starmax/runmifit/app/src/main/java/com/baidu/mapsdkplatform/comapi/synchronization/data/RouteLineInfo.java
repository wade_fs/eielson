package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class RouteLineInfo implements Parcelable {
    public static final Parcelable.Creator<RouteLineInfo> CREATOR = new C1125b();

    /* renamed from: a */
    private boolean f3705a;

    /* renamed from: b */
    private String f3706b;

    /* renamed from: c */
    private CopyOnWriteArrayList<RouteSectionInfo> f3707c;

    public static final class RouteSectionInfo implements Parcelable {
        public static final Parcelable.Creator<RouteSectionInfo> CREATOR = new C1126c();

        /* renamed from: a */
        private LatLng f3708a;

        /* renamed from: b */
        private LatLng f3709b;

        public RouteSectionInfo() {
            this.f3708a = null;
            this.f3709b = null;
            this.f3708a = null;
            this.f3709b = null;
        }

        protected RouteSectionInfo(Parcel parcel) {
            this.f3708a = null;
            this.f3709b = null;
            this.f3708a = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
            this.f3709b = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
        }

        /* renamed from: a */
        public LatLng mo13221a() {
            return this.f3708a;
        }

        /* renamed from: a */
        public void mo13222a(LatLng latLng) {
            this.f3708a = latLng;
        }

        /* renamed from: b */
        public LatLng mo13223b() {
            return this.f3709b;
        }

        /* renamed from: b */
        public void mo13224b(LatLng latLng) {
            this.f3709b = latLng;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.f3708a, i);
            parcel.writeParcelable(this.f3709b, i);
        }
    }

    public RouteLineInfo() {
        this.f3705a = false;
        this.f3706b = null;
        this.f3707c = new CopyOnWriteArrayList<>();
    }

    protected RouteLineInfo(Parcel parcel) {
        this.f3705a = parcel.readByte() != 0;
        this.f3706b = parcel.readString();
    }

    /* renamed from: a */
    public String mo13214a() {
        return this.f3706b;
    }

    /* renamed from: a */
    public void mo13215a(RouteSectionInfo routeSectionInfo) {
        CopyOnWriteArrayList<RouteSectionInfo> copyOnWriteArrayList = this.f3707c;
        if (copyOnWriteArrayList != null) {
            copyOnWriteArrayList.add(routeSectionInfo);
        }
    }

    /* renamed from: a */
    public void mo13216a(String str) {
        this.f3706b = str;
    }

    /* renamed from: a */
    public void mo13217a(boolean z) {
        this.f3705a = z;
    }

    /* renamed from: b */
    public List<RouteSectionInfo> mo13218b() {
        return this.f3707c;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.f3705a ? (byte) 1 : 0);
        parcel.writeString(this.f3706b);
        parcel.writeTypedList(this.f3707c);
    }
}
