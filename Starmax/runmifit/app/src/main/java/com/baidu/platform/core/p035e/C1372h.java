package com.baidu.platform.core.p035e;

import com.baidu.mapapi.search.share.LocationShareURLOption;
import com.baidu.mapapi.search.share.OnGetShareUrlResultListener;
import com.baidu.mapapi.search.share.PoiDetailShareURLOption;
import com.baidu.mapapi.search.share.RouteShareURLOption;
import com.baidu.platform.base.C1316a;
import com.baidu.platform.base.SearchType;

/* renamed from: com.baidu.platform.core.e.h */
public class C1372h extends C1316a implements C1365a {

    /* renamed from: b */
    OnGetShareUrlResultListener f4469b = null;

    /* renamed from: a */
    public void mo14068a() {
        this.f4422a.lock();
        this.f4469b = null;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public void mo14069a(OnGetShareUrlResultListener onGetShareUrlResultListener) {
        this.f4422a.lock();
        this.f4469b = onGetShareUrlResultListener;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public boolean mo14070a(LocationShareURLOption locationShareURLOption) {
        C1370f fVar = new C1370f();
        fVar.mo14021a(SearchType.LOCATION_SEARCH_SHARE);
        return mo14016a(new C1366b(locationShareURLOption), this.f4469b, fVar);
    }

    /* renamed from: a */
    public boolean mo14071a(PoiDetailShareURLOption poiDetailShareURLOption) {
        C1370f fVar = new C1370f();
        fVar.mo14021a(SearchType.POI_DETAIL_SHARE);
        return mo14016a(new C1367c(poiDetailShareURLOption), this.f4469b, fVar);
    }

    /* renamed from: a */
    public boolean mo14072a(RouteShareURLOption routeShareURLOption) {
        C1368d dVar = new C1368d();
        dVar.mo14021a(SearchType.ROUTE_PLAN_SHARE);
        return mo14016a(new C1369e(routeShareURLOption), this.f4469b, dVar);
    }
}
