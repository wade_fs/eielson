package com.baidu.mapapi.http;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

/* renamed from: com.baidu.mapapi.http.b */
class C0918b implements HostnameVerifier {

    /* renamed from: a */
    final /* synthetic */ HttpClient f2363a;

    C0918b(HttpClient httpClient) {
        this.f2363a = httpClient;
    }

    public boolean verify(String str, SSLSession sSLSession) {
        return HttpsURLConnection.getDefaultHostnameVerifier().verify(str, sSLSession);
    }
}
