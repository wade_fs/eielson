package com.baidu.mapapi.map;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;
import mapsdkvi.com.gdi.bgl.android.java.EnvDrawText;

public final class Text extends Overlay {

    /* renamed from: k */
    private static final String f2719k = Text.class.getSimpleName();

    /* renamed from: a */
    String f2720a;

    /* renamed from: b */
    LatLng f2721b;

    /* renamed from: c */
    int f2722c;

    /* renamed from: d */
    int f2723d;

    /* renamed from: e */
    int f2724e;

    /* renamed from: f */
    Typeface f2725f;

    /* renamed from: g */
    int f2726g;

    /* renamed from: h */
    int f2727h;

    /* renamed from: i */
    float f2728i;

    /* renamed from: j */
    int f2729j;

    Text() {
        this.type = C1082h.text;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo11321a() {
        Typeface typeface = this.f2725f;
        if (typeface != null) {
            EnvDrawText.removeFontCache(typeface.hashCode());
        }
        return super.mo11321a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        super.mo10900a(bundle);
        if (this.f2721b != null) {
            bundle.putString("text", this.f2720a);
            GeoPoint ll2mc = CoordUtil.ll2mc(this.f2721b);
            bundle.putDouble("location_x", ll2mc.getLongitudeE6());
            bundle.putDouble("location_y", ll2mc.getLatitudeE6());
            int i = this.f2723d;
            bundle.putInt("font_color", Color.argb(i >>> 24, i & 255, (i >> 8) & 255, (i >> 16) & 255));
            int i2 = this.f2722c;
            bundle.putInt("bg_color", Color.argb(i2 >>> 24, i2 & 255, (i2 >> 8) & 255, (i2 >> 16) & 255));
            bundle.putInt("font_size", this.f2724e);
            Typeface typeface = this.f2725f;
            if (typeface != null) {
                EnvDrawText.registFontCache(typeface.hashCode(), this.f2725f);
                bundle.putInt("type_face", this.f2725f.hashCode());
            }
            int i3 = this.f2726g;
            float f = 0.5f;
            bundle.putFloat("align_x", i3 != 1 ? i3 != 2 ? 0.5f : 1.0f : 0.0f);
            int i4 = this.f2727h;
            if (i4 == 8) {
                f = 0.0f;
            } else if (i4 == 16) {
                f = 1.0f;
            }
            bundle.putFloat("align_y", f);
            bundle.putFloat("rotate", this.f2728i);
            bundle.putInt("update", this.f2729j);
            return bundle;
        }
        throw new IllegalStateException("BDMapSDKException: when you add a text overlay, you must provide text and the position info.");
    }

    public float getAlignX() {
        return (float) this.f2726g;
    }

    public float getAlignY() {
        return (float) this.f2727h;
    }

    public int getBgColor() {
        return this.f2722c;
    }

    public int getFontColor() {
        return this.f2723d;
    }

    public int getFontSize() {
        return this.f2724e;
    }

    public LatLng getPosition() {
        return this.f2721b;
    }

    public float getRotate() {
        return this.f2728i;
    }

    public String getText() {
        return this.f2720a;
    }

    public Typeface getTypeface() {
        return this.f2725f;
    }

    public void setAlign(int i, int i2) {
        this.f2726g = i;
        this.f2727h = i2;
        this.f2729j = 1;
        this.listener.mo11330b(super);
    }

    public void setBgColor(int i) {
        this.f2722c = i;
        this.f2729j = 1;
        this.listener.mo11330b(super);
    }

    public void setFontColor(int i) {
        this.f2723d = i;
        this.f2729j = 1;
        this.listener.mo11330b(super);
    }

    public void setFontSize(int i) {
        this.f2724e = i;
        this.f2729j = 1;
        this.listener.mo11330b(super);
    }

    public void setPosition(LatLng latLng) {
        if (latLng != null) {
            this.f2721b = latLng;
            this.f2729j = 1;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: position can not be null");
    }

    public void setRotate(float f) {
        this.f2728i = f;
        this.f2729j = 1;
        this.listener.mo11330b(super);
    }

    public void setText(String str) {
        if (str == null || str.equals("")) {
            throw new IllegalArgumentException("BDMapSDKException: text can not be null or empty");
        }
        this.f2720a = str;
        this.f2729j = 1;
        this.listener.mo11330b(super);
    }

    public void setTypeface(Typeface typeface) {
        this.f2725f = typeface;
        this.f2729j = 1;
        this.listener.mo11330b(super);
    }
}
