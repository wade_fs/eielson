package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;

public final class Circle extends Overlay {

    /* renamed from: a */
    LatLng f2441a;

    /* renamed from: b */
    int f2442b;

    /* renamed from: c */
    int f2443c;

    /* renamed from: d */
    Stroke f2444d;

    Circle() {
        this.type = C1082h.circle;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        super.mo10900a(bundle);
        GeoPoint ll2mc = CoordUtil.ll2mc(this.f2441a);
        bundle.putDouble("location_x", ll2mc.getLongitudeE6());
        bundle.putDouble("location_y", ll2mc.getLatitudeE6());
        bundle.putInt("radius", CoordUtil.getMCDistanceByOneLatLngAndRadius(this.f2441a, this.f2443c));
        Overlay.m2939a(this.f2442b, bundle);
        if (this.f2444d == null) {
            bundle.putInt("has_stroke", 0);
        } else {
            bundle.putInt("has_stroke", 1);
            bundle.putBundle("stroke", this.f2444d.mo11396a(new Bundle()));
        }
        return bundle;
    }

    public LatLng getCenter() {
        return this.f2441a;
    }

    public int getFillColor() {
        return this.f2442b;
    }

    public int getRadius() {
        return this.f2443c;
    }

    public Stroke getStroke() {
        return this.f2444d;
    }

    public void setCenter(LatLng latLng) {
        if (latLng != null) {
            this.f2441a = latLng;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: circle center can not be null");
    }

    public void setFillColor(int i) {
        this.f2442b = i;
        this.listener.mo11330b(super);
    }

    public void setRadius(int i) {
        this.f2443c = i;
        this.listener.mo11330b(super);
    }

    public void setStroke(Stroke stroke) {
        this.f2444d = stroke;
        this.listener.mo11330b(super);
    }
}
