package com.baidu.platform.core.p032b;

import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.platform.base.C1319d;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.core.b.b */
public class C1332b extends C1319d {

    /* renamed from: b */
    private static final String f4454b = C1332b.class.getSimpleName();

    /* renamed from: c */
    private String f4455c;

    /* renamed from: a */
    private LatLng m4950a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        double optDouble = jSONObject.optDouble("lat");
        double optDouble2 = jSONObject.optDouble("lng");
        return SDKInitializer.getCoordType() == CoordType.GCJ02 ? CoordTrans.baiduToGcj(new LatLng(optDouble, optDouble2)) : new LatLng(optDouble, optDouble2);
    }

    /* renamed from: a */
    private boolean m4951a(String str, GeoCodeResult geoCodeResult) {
        if (TextUtils.isEmpty(str) || geoCodeResult == null) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            int optInt = jSONObject.optInt(NotificationCompat.CATEGORY_STATUS);
            if (optInt != 0) {
                geoCodeResult.error = optInt != 1 ? optInt != 2 ? SearchResult.ERRORNO.RESULT_NOT_FOUND : SearchResult.ERRORNO.SEARCH_OPTION_ERROR : SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR;
                return false;
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("result");
            if (optJSONObject == null) {
                geoCodeResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
                return false;
            }
            geoCodeResult.setLocation(m4950a(optJSONObject.optJSONObject("location")));
            geoCodeResult.setAddress(this.f4455c);
            geoCodeResult.setPrecise(optJSONObject.optInt("precise"));
            geoCodeResult.setConfidence(optJSONObject.optInt("confidence"));
            geoCodeResult.setLevel(optJSONObject.optString("level"));
            geoCodeResult.error = SearchResult.ERRORNO.NO_ERROR;
            return true;
        } catch (JSONException e) {
            geoCodeResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
            Log.e(f4454b, "Parse GeoCodeResult catch JSONException", e);
            return true;
        }
    }

    /* renamed from: a */
    public SearchResult mo14018a(String str) {
        SearchResult.ERRORNO errorno;
        GeoCodeResult geoCodeResult = new GeoCodeResult();
        if (str != null && !str.equals("")) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.has("SDK_InnerError")) {
                    if (!mo14022a(str, geoCodeResult, false) && !m4951a(str, geoCodeResult)) {
                        geoCodeResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
                    }
                    return geoCodeResult;
                }
                JSONObject optJSONObject = jSONObject.optJSONObject("SDK_InnerError");
                if (optJSONObject.has("PermissionCheckError")) {
                    errorno = SearchResult.ERRORNO.PERMISSION_UNFINISHED;
                    geoCodeResult.error = errorno;
                    return geoCodeResult;
                } else if (optJSONObject.has("httpStateError")) {
                    String optString = optJSONObject.optString("httpStateError");
                    char c = 65535;
                    int hashCode = optString.hashCode();
                    if (hashCode != -879828873) {
                        if (hashCode == 1470557208 && optString.equals("REQUEST_ERROR")) {
                            c = 1;
                        }
                    } else if (optString.equals("NETWORK_ERROR")) {
                        c = 0;
                    }
                    geoCodeResult.error = c != 0 ? c != 1 ? SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR : SearchResult.ERRORNO.REQUEST_ERROR : SearchResult.ERRORNO.NETWORK_ERROR;
                    return geoCodeResult;
                }
            } catch (JSONException e) {
                Log.e(f4454b, "JSONException caught", e);
            }
        }
        errorno = SearchResult.ERRORNO.RESULT_NOT_FOUND;
        geoCodeResult.error = errorno;
        return geoCodeResult;
    }

    /* renamed from: a */
    public void mo14020a(SearchResult searchResult, Object obj) {
        if (obj != null && (obj instanceof OnGetGeoCoderResultListener)) {
            ((OnGetGeoCoderResultListener) obj).onGetGeoCodeResult((GeoCodeResult) searchResult);
        }
    }

    /* renamed from: b */
    public void mo14042b(String str) {
        this.f4455c = str;
    }
}
