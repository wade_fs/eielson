package com.baidu.location.p013a;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: com.baidu.location.a.v */
public class C0762v {

    /* renamed from: a */
    private ExecutorService f1431a;

    /* renamed from: b */
    private ExecutorService f1432b;

    /* renamed from: com.baidu.location.a.v$a */
    private static class C0764a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static C0762v f1433a = new C0762v();
    }

    private C0762v() {
    }

    /* renamed from: a */
    public static C0762v m1943a() {
        return C0764a.f1433a;
    }

    /* renamed from: b */
    public synchronized ExecutorService mo10504b() {
        if (this.f1431a == null || this.f1431a.isShutdown()) {
            this.f1431a = null;
            this.f1431a = Executors.newSingleThreadExecutor();
        }
        return this.f1431a;
    }

    /* renamed from: c */
    public synchronized ExecutorService mo10505c() {
        if (this.f1432b == null || this.f1432b.isShutdown()) {
            this.f1432b = null;
            this.f1432b = Executors.newFixedThreadPool(2);
        }
        return this.f1432b;
    }

    /* renamed from: d */
    public void mo10506d() {
        ExecutorService executorService = this.f1431a;
        if (executorService != null) {
            executorService.shutdown();
        }
        ExecutorService executorService2 = this.f1432b;
        if (executorService2 != null) {
            executorService2.shutdown();
        }
    }
}
