package com.baidu.mapapi.search.poi;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.search.core.C0968l;
import com.baidu.platform.core.p033c.C1340a;
import com.baidu.platform.core.p033c.C1345f;

public class PoiSearch extends C0968l {

    /* renamed from: a */
    private C1340a f3015a = new C1345f();

    /* renamed from: b */
    private boolean f3016b = false;

    PoiSearch() {
    }

    public static PoiSearch newInstance() {
        BMapManager.init();
        return new PoiSearch();
    }

    public void destroy() {
        if (!this.f3016b) {
            this.f3016b = true;
            this.f3015a.mo14047a();
            BMapManager.destroy();
        }
    }

    public boolean searchInBound(PoiBoundSearchOption poiBoundSearchOption) {
        if (this.f3015a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (poiBoundSearchOption != null && poiBoundSearchOption.mBound != null && poiBoundSearchOption.mKeyword != null) {
            return this.f3015a.mo14049a(poiBoundSearchOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or bound or keyworld can not be null");
        }
    }

    public boolean searchInCity(PoiCitySearchOption poiCitySearchOption) {
        if (this.f3015a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (poiCitySearchOption != null && poiCitySearchOption.mCity != null && poiCitySearchOption.mKeyword != null) {
            return this.f3015a.mo14050a(poiCitySearchOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or city or keyworld can not be null");
        }
    }

    public boolean searchNearby(PoiNearbySearchOption poiNearbySearchOption) {
        if (this.f3015a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (poiNearbySearchOption == null || poiNearbySearchOption.mLocation == null || poiNearbySearchOption.mKeyword == null) {
            throw new IllegalArgumentException("BDMapSDKException: option or location or keyworld can not be null");
        } else if (poiNearbySearchOption.mRadius <= 0) {
            return false;
        } else {
            return this.f3015a.mo14053a(poiNearbySearchOption);
        }
    }

    public boolean searchPoiDetail(PoiDetailSearchOption poiDetailSearchOption) {
        if (this.f3015a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (poiDetailSearchOption != null && poiDetailSearchOption.getUid() != null) {
            return this.f3015a.mo14051a(poiDetailSearchOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or uid can not be null");
        }
    }

    public boolean searchPoiIndoor(PoiIndoorOption poiIndoorOption) {
        if (this.f3015a == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (poiIndoorOption != null && poiIndoorOption.bid != null && poiIndoorOption.f3005wd != null) {
            return this.f3015a.mo14052a(poiIndoorOption);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: option or indoor bid or keyword can not be null");
        }
    }

    public void setOnGetPoiSearchResultListener(OnGetPoiSearchResultListener onGetPoiSearchResultListener) {
        C1340a aVar = this.f3015a;
        if (aVar == null) {
            throw new IllegalStateException("BDMapSDKException: searcher is null, please call newInstance first.");
        } else if (onGetPoiSearchResultListener != null) {
            aVar.mo14048a(onGetPoiSearchResultListener);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: listener can not be null");
        }
    }
}
