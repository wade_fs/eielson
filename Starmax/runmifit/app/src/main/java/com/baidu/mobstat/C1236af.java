package com.baidu.mobstat;

import android.content.Context;
import android.text.TextUtils;
import java.text.SimpleDateFormat;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.af */
public class C1236af {

    /* renamed from: a */
    private static C1236af f4249a;

    /* renamed from: b */
    private Context f4250b;

    /* renamed from: c */
    private JSONObject f4251c = new JSONObject();

    /* renamed from: d */
    private long f4252d = 24;

    /* renamed from: e */
    private long f4253e = 0;

    /* renamed from: f */
    private long f4254f = 0;

    /* renamed from: g */
    private long f4255g = 0;

    /* renamed from: h */
    private long f4256h = 5;

    /* renamed from: i */
    private long f4257i = 24;

    /* renamed from: j */
    private long f4258j = 15;

    /* renamed from: k */
    private long f4259k = 15;

    /* renamed from: l */
    private long f4260l = 30;

    /* renamed from: m */
    private long f4261m = 12;

    /* renamed from: n */
    private long f4262n = 1;

    /* renamed from: o */
    private long f4263o = 24;

    /* renamed from: p */
    private String f4264p = "";

    /* renamed from: q */
    private String f4265q = "";

    /* renamed from: a */
    public static C1236af m4534a(Context context) {
        if (f4249a == null) {
            synchronized (C1236af.class) {
                if (f4249a == null) {
                    f4249a = new C1236af(context);
                }
            }
        }
        return f4249a;
    }

    private C1236af(Context context) {
        this.f4250b = context;
        m4536m();
        mo13904j();
        mo13905k();
    }

    /* renamed from: m */
    private void m4536m() {
        String b = C1267ba.m4684b("backups/system/.timestamp");
        try {
            if (!TextUtils.isEmpty(b)) {
                this.f4251c = new JSONObject(b);
            }
        } catch (Exception unused) {
        }
    }

    /* renamed from: a */
    public boolean mo13892a() {
        return this.f4253e != 0;
    }

    /* renamed from: b */
    public boolean mo13895b() {
        return this.f4254f != 0;
    }

    /* renamed from: c */
    public long mo13896c() {
        return this.f4252d * 60 * 60 * 1000;
    }

    /* renamed from: d */
    public long mo13898d() {
        return this.f4263o * 60 * 60 * 1000;
    }

    /* renamed from: e */
    public long mo13899e() {
        return this.f4256h * 60 * 1000;
    }

    /* renamed from: f */
    public long mo13900f() {
        return this.f4257i * 60 * 60 * 1000;
    }

    /* renamed from: g */
    public long mo13901g() {
        return this.f4258j * 24 * 60 * 60 * 1000;
    }

    /* renamed from: h */
    public long mo13902h() {
        return this.f4259k * 24 * 60 * 60 * 1000;
    }

    /* renamed from: i */
    public long mo13903i() {
        return this.f4261m * 60 * 60 * 1000;
    }

    /* renamed from: j */
    public void mo13904j() {
        try {
            String str = new String(C1275bg.m4710b(false, C1269bc.m4694a(), C1268bb.m4691a(C1267ba.m4679a(this.f4250b, ".config2").getBytes())));
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                try {
                    this.f4253e = jSONObject.getLong("c");
                } catch (JSONException e) {
                    C1255as.m4626c().mo13944b(e);
                }
                try {
                    this.f4256h = jSONObject.getLong("d");
                } catch (JSONException e2) {
                    C1255as.m4626c().mo13944b(e2);
                }
                try {
                    this.f4257i = jSONObject.getLong("e");
                } catch (JSONException e3) {
                    C1255as.m4626c().mo13944b(e3);
                }
                try {
                    this.f4258j = jSONObject.getLong("i");
                } catch (JSONException e4) {
                    C1255as.m4626c().mo13944b(e4);
                }
                try {
                    this.f4252d = jSONObject.getLong("f");
                } catch (JSONException e5) {
                    C1255as.m4626c().mo13944b(e5);
                }
                try {
                    this.f4263o = jSONObject.getLong("s");
                } catch (JSONException e6) {
                    C1255as.m4626c().mo13944b(e6);
                }
                try {
                    this.f4259k = jSONObject.getLong("pk");
                } catch (JSONException e7) {
                    C1255as.m4626c().mo13944b(e7);
                }
                try {
                    this.f4260l = jSONObject.getLong("at");
                } catch (JSONException e8) {
                    C1255as.m4626c().mo13944b(e8);
                }
                try {
                    this.f4261m = jSONObject.getLong("as");
                } catch (JSONException e9) {
                    C1255as.m4626c().mo13944b(e9);
                }
                try {
                    this.f4262n = jSONObject.getLong("ac");
                } catch (JSONException e10) {
                    C1255as.m4626c().mo13944b(e10);
                }
                try {
                    this.f4254f = jSONObject.getLong("mc");
                } catch (JSONException e11) {
                    C1255as.m4626c().mo13944b(e11);
                }
                try {
                    this.f4255g = jSONObject.getLong("lsc");
                } catch (JSONException e12) {
                    C1255as.m4626c().mo13944b(e12);
                }
            }
        } catch (Exception e13) {
            C1255as.m4626c().mo13944b(e13);
        }
    }

    /* renamed from: k */
    public void mo13905k() {
        try {
            String str = new String(C1275bg.m4710b(false, C1269bc.m4694a(), C1268bb.m4691a(C1267ba.m4679a(this.f4250b, ".sign").getBytes())));
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                try {
                    this.f4265q = jSONObject.getString("sign");
                } catch (Exception e) {
                    C1255as.m4626c().mo13944b(e);
                }
                try {
                    this.f4264p = jSONObject.getString("ver");
                } catch (Exception e2) {
                    C1255as.m4626c().mo13944b(e2);
                }
            }
        } catch (Exception e3) {
            C1255as.m4626c().mo13944b(e3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    /* renamed from: a */
    public void mo13891a(String str) {
        C1267ba.m4681a(this.f4250b, ".config2", str, false);
        mo13904j();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    /* renamed from: b */
    public void mo13894b(String str) {
        C1267ba.m4681a(this.f4250b, ".sign", str, false);
        mo13905k();
    }

    /* renamed from: c */
    public String mo13897c(String str) {
        return (TextUtils.isEmpty(this.f4264p) || !this.f4264p.equals(str) || TextUtils.isEmpty(this.f4265q)) ? "" : this.f4265q;
    }

    /* renamed from: a */
    public long mo13889a(C1296n nVar) {
        long j = nVar.f4381j;
        try {
            String nVar2 = nVar.toString();
            if (this.f4251c.has(nVar2)) {
                j = this.f4251c.getLong(nVar2);
            }
        } catch (Exception e) {
            C1255as.m4626c().mo13942a(e);
        }
        return m4535b(j);
    }

    /* renamed from: a */
    public void mo13890a(C1296n nVar, long j) {
        nVar.f4381j = j;
        try {
            this.f4251c.put(nVar.toString(), j);
        } catch (Exception e) {
            C1255as.m4626c().mo13942a(e);
        }
        try {
            C1267ba.m4682a("backups/system/.timestamp", this.f4251c.toString(), false);
        } catch (Exception e2) {
            C1255as.m4626c().mo13942a(e2);
        }
    }

    /* renamed from: l */
    public boolean mo13906l() {
        long currentTimeMillis = System.currentTimeMillis();
        long a = mo13889a(C1296n.LAST_SEND);
        long d = mo13898d();
        C1255as c = C1255as.m4626c();
        c.mo13941a("canSend now=" + currentTimeMillis + ";lastSendTime=" + a + ";sendLogTimeInterval=" + d);
        return currentTimeMillis - a > d || !mo13893a(a);
    }

    /* renamed from: a */
    public boolean mo13893a(long j) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        return simpleDateFormat.format(Long.valueOf(j)).equals(simpleDateFormat.format(Long.valueOf(System.currentTimeMillis())));
    }

    /* renamed from: b */
    private long m4535b(long j) {
        if (j - System.currentTimeMillis() > 0) {
            return 0;
        }
        return j;
    }
}
