package com.baidu.android.bbalbs.common.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.os.SystemClock;
import android.provider.Settings;
import android.system.ErrnoException;
import android.system.Os;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.android.bbalbs.common.p012a.C0691a;
import com.baidu.android.bbalbs.common.p012a.C0692b;
import com.baidu.android.bbalbs.common.p012a.C0693c;
import com.baidu.android.bbalbs.common.p012a.C0694d;
import com.google.common.primitives.UnsignedBytes;
import com.tencent.mid.api.MidEntity;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.crypto.Cipher;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.android.bbalbs.common.util.b */
public final class C0696b {

    /* renamed from: a */
    private static final String f1053a;

    /* renamed from: e */
    private static C0698b f1054e;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Context f1055b;

    /* renamed from: c */
    private int f1056c = 0;

    /* renamed from: d */
    private PublicKey f1057d;

    /* renamed from: com.baidu.android.bbalbs.common.util.b$a */
    private static class C0697a {

        /* renamed from: a */
        public ApplicationInfo f1058a;

        /* renamed from: b */
        public int f1059b;

        /* renamed from: c */
        public boolean f1060c;

        /* renamed from: d */
        public boolean f1061d;

        private C0697a() {
            this.f1059b = 0;
            this.f1060c = false;
            this.f1061d = false;
        }

        /* synthetic */ C0697a(C0700c cVar) {
            this();
        }
    }

    /* renamed from: com.baidu.android.bbalbs.common.util.b$b */
    private static class C0698b {

        /* renamed from: a */
        public String f1062a;

        /* renamed from: b */
        public String f1063b;

        /* renamed from: c */
        public int f1064c;

        private C0698b() {
            this.f1064c = 2;
        }

        /* synthetic */ C0698b(C0700c cVar) {
            this();
        }

        /* renamed from: a */
        public static C0698b m1589a(String str) {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                String string = jSONObject.getString("deviceid");
                String string2 = jSONObject.getString(MidEntity.TAG_IMEI);
                int i = jSONObject.getInt("ver");
                if (!TextUtils.isEmpty(string) && string2 != null) {
                    C0698b bVar = new C0698b();
                    bVar.f1062a = string;
                    bVar.f1063b = string2;
                    bVar.f1064c = i;
                    return bVar;
                }
            } catch (JSONException e) {
                C0696b.m1568b(e);
            }
            return null;
        }

        /* renamed from: a */
        public boolean mo10191a() {
            return "0".equalsIgnoreCase(this.f1063b);
        }

        /* renamed from: b */
        public String mo10192b() {
            try {
                return new JSONObject().put("deviceid", this.f1062a).put(MidEntity.TAG_IMEI, this.f1063b).put("ver", this.f1064c).toString();
            } catch (JSONException e) {
                C0696b.m1568b(e);
                return null;
            }
        }

        /* renamed from: c */
        public String mo10193c() {
            String str = this.f1063b;
            if (TextUtils.isEmpty(str)) {
                str = "0";
            }
            String stringBuffer = new StringBuffer(str).reverse().toString();
            return this.f1062a + "|" + stringBuffer;
        }
    }

    /* renamed from: com.baidu.android.bbalbs.common.util.b$c */
    static class C0699c {
        /* renamed from: a */
        static boolean m1593a(String str, int i) {
            try {
                Os.chmod(str, i);
                return true;
            } catch (ErrnoException e) {
                C0696b.m1568b(e);
                return false;
            }
        }
    }

    static {
        String str = new String(C0692b.m1540a(new byte[]{77, 122, 65, 121, 77, 84, 73, 120, 77, 68, 73, 61}));
        String str2 = new String(C0692b.m1540a(new byte[]{90, 71, 108, 106, 100, 87, 82, 112, 89, 87, 73, 61}));
        f1053a = str + str2;
    }

    private C0696b(Context context) {
        this.f1055b = context.getApplicationContext();
        m1553a();
    }

    /* renamed from: a */
    public static String m1548a(Context context) {
        return m1571c(context).mo10193c();
    }

    /* renamed from: a */
    private static String m1551a(byte[] bArr) {
        StringBuilder sb;
        if (bArr != null) {
            String str = "";
            for (byte b : bArr) {
                String hexString = Integer.toHexString(b & UnsignedBytes.MAX_VALUE);
                if (hexString.length() == 1) {
                    sb = new StringBuilder();
                    sb.append(str);
                    str = "0";
                } else {
                    sb = new StringBuilder();
                }
                sb.append(str);
                sb.append(hexString);
                str = sb.toString();
            }
            return str.toLowerCase();
        }
        throw new IllegalArgumentException("Argument b ( byte array ) is null! ");
    }

    /* renamed from: a */
    private List<C0697a> m1552a(Intent intent, boolean z) {
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = this.f1055b.getPackageManager();
        List<ResolveInfo> queryBroadcastReceivers = packageManager.queryBroadcastReceivers(intent, 0);
        if (queryBroadcastReceivers != null) {
            for (ResolveInfo resolveInfo : queryBroadcastReceivers) {
                if (!(resolveInfo.activityInfo == null || resolveInfo.activityInfo.applicationInfo == null)) {
                    try {
                        Bundle bundle = packageManager.getReceiverInfo(new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name), 128).metaData;
                        if (bundle != null) {
                            String string = bundle.getString("galaxy_data");
                            if (!TextUtils.isEmpty(string)) {
                                byte[] a = C0692b.m1540a(string.getBytes("utf-8"));
                                JSONObject jSONObject = new JSONObject(new String(a));
                                C0697a aVar = new C0697a(null);
                                aVar.f1059b = jSONObject.getInt("priority");
                                aVar.f1058a = resolveInfo.activityInfo.applicationInfo;
                                if (this.f1055b.getPackageName().equals(resolveInfo.activityInfo.applicationInfo.packageName)) {
                                    aVar.f1061d = true;
                                }
                                if (z) {
                                    String string2 = bundle.getString("galaxy_sf");
                                    if (!TextUtils.isEmpty(string2)) {
                                        PackageInfo packageInfo = packageManager.getPackageInfo(resolveInfo.activityInfo.applicationInfo.packageName, 64);
                                        JSONArray jSONArray = jSONObject.getJSONArray("sigs");
                                        String[] strArr = new String[jSONArray.length()];
                                        for (int i = 0; i < strArr.length; i++) {
                                            strArr[i] = jSONArray.getString(i);
                                        }
                                        if (m1559a(strArr, m1561a(packageInfo.signatures))) {
                                            byte[] a2 = m1560a(C0692b.m1540a(string2.getBytes()), this.f1057d);
                                            if (a2 != null && Arrays.equals(a2, C0694d.m1544a(a))) {
                                                aVar.f1060c = true;
                                            }
                                        }
                                    }
                                }
                                arrayList.add(aVar);
                            }
                        }
                    } catch (Exception unused) {
                    }
                }
            }
        }
        Collections.sort(arrayList, new C0700c(this));
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0028 A[SYNTHETIC, Splitter:B:13:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034 A[SYNTHETIC, Splitter:B:21:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m1553a() {
        /*
            r4 = this;
            r0 = 0
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0031, all -> 0x0022 }
            byte[] r2 = com.baidu.android.bbalbs.common.util.C0695a.m1546a()     // Catch:{ Exception -> 0x0031, all -> 0x0022 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0031, all -> 0x0022 }
            java.lang.String r0 = "X.509"
            java.security.cert.CertificateFactory r0 = java.security.cert.CertificateFactory.getInstance(r0)     // Catch:{ Exception -> 0x0020, all -> 0x001e }
            java.security.cert.Certificate r0 = r0.generateCertificate(r1)     // Catch:{ Exception -> 0x0020, all -> 0x001e }
            java.security.PublicKey r0 = r0.getPublicKey()     // Catch:{ Exception -> 0x0020, all -> 0x001e }
            r4.f1057d = r0     // Catch:{ Exception -> 0x0020, all -> 0x001e }
            r1.close()     // Catch:{ Exception -> 0x0038 }
            goto L_0x003c
        L_0x001e:
            r0 = move-exception
            goto L_0x0026
        L_0x0020:
            goto L_0x0032
        L_0x0022:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0026:
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ Exception -> 0x002c }
            goto L_0x0030
        L_0x002c:
            r1 = move-exception
            m1568b(r1)
        L_0x0030:
            throw r0
        L_0x0031:
            r1 = r0
        L_0x0032:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ Exception -> 0x0038 }
            goto L_0x003c
        L_0x0038:
            r0 = move-exception
            m1568b(r0)
        L_0x003c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.bbalbs.common.util.C0696b.m1553a():void");
    }

    /* renamed from: a */
    private synchronized void m1554a(C0698b bVar) {
        new Thread(m1563b(bVar)).start();
    }

    /* renamed from: a */
    private boolean m1559a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length != strArr2.length) {
            return false;
        }
        HashSet hashSet = new HashSet();
        for (String str : strArr) {
            hashSet.add(str);
        }
        HashSet hashSet2 = new HashSet();
        for (String str2 : strArr2) {
            hashSet2.add(str2);
        }
        return hashSet.equals(hashSet2);
    }

    /* renamed from: a */
    private static byte[] m1560a(byte[] bArr, PublicKey publicKey) throws Exception {
        Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        instance.init(2, publicKey);
        return instance.doFinal(bArr);
    }

    /* renamed from: a */
    private String[] m1561a(Signature[] signatureArr) {
        String[] strArr = new String[signatureArr.length];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = m1551a(C0694d.m1544a(signatureArr[i].toByteArray()));
        }
        return strArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.bbalbs.common.util.b.a(android.content.Intent, boolean):java.util.List<com.baidu.android.bbalbs.common.util.b$a>
     arg types: [android.content.Intent, int]
     candidates:
      com.baidu.android.bbalbs.common.util.b.a(java.lang.String, java.lang.String):void
      com.baidu.android.bbalbs.common.util.b.a(com.baidu.android.bbalbs.common.util.b, java.lang.String):boolean
      com.baidu.android.bbalbs.common.util.b.a(java.lang.String[], java.lang.String[]):boolean
      com.baidu.android.bbalbs.common.util.b.a(byte[], java.security.PublicKey):byte[]
      com.baidu.android.bbalbs.common.util.b.a(android.content.Intent, boolean):java.util.List<com.baidu.android.bbalbs.common.util.b$a> */
    /* renamed from: b */
    private C0698b m1562b() {
        boolean z;
        String str;
        String str2;
        List<C0697a> a = m1552a(new Intent("com.baidu.intent.action.GALAXY").setPackage(this.f1055b.getPackageName()), true);
        boolean z2 = false;
        if (a == null || a.size() == 0) {
            for (int i = 0; i < 3; i++) {
                Log.w("DeviceId", "galaxy lib host missing meta-data,make sure you know the right way to integrate galaxy");
            }
            z = false;
        } else {
            C0697a aVar = a.get(0);
            z = aVar.f1060c;
            if (!aVar.f1060c) {
                for (int i2 = 0; i2 < 3; i2++) {
                    Log.w("DeviceId", "galaxy config err, In the release version of the signature should be matched");
                }
            }
        }
        File file = new File(this.f1055b.getFilesDir(), "libcuid.so");
        C0698b a2 = file.exists() ? C0698b.m1589a(m1586i(m1566b(file))) : null;
        if (a2 == null) {
            this.f1056c |= 16;
            List<C0697a> a3 = m1552a(new Intent("com.baidu.intent.action.GALAXY"), z);
            if (a3 != null) {
                String str3 = "files";
                File filesDir = this.f1055b.getFilesDir();
                if (!str3.equals(filesDir.getName())) {
                    Log.e("DeviceId", "fetal error:: app files dir name is unexpectedly :: " + filesDir.getAbsolutePath());
                    str3 = filesDir.getName();
                }
                for (C0697a aVar2 : a3) {
                    if (!aVar2.f1061d) {
                        File file2 = new File(new File(aVar2.f1058a.dataDir, str3), "libcuid.so");
                        if (file2.exists() && (a2 = C0698b.m1589a(m1586i(m1566b(file2)))) != null) {
                            break;
                        }
                    }
                }
            }
        }
        if (a2 == null) {
            a2 = C0698b.m1589a(m1586i(m1582e("com.baidu.deviceid.v2")));
        }
        boolean f = m1583f("android.permission.READ_EXTERNAL_STORAGE");
        if (a2 == null && f) {
            this.f1056c |= 2;
            a2 = m1580e();
        }
        if (a2 == null) {
            this.f1056c |= 8;
            a2 = m1577d();
        }
        if (a2 != null || !f) {
            str = null;
        } else {
            this.f1056c |= 1;
            String k = m1588k("");
            z2 = true;
            str = k;
            a2 = m1584g(k);
        }
        if (a2 == null) {
            this.f1056c |= 4;
            if (!z2) {
                str = m1588k("");
            }
            a2 = new C0698b(null);
            String b = m1564b(this.f1055b);
            if (Build.VERSION.SDK_INT < 23) {
                str2 = str + b + UUID.randomUUID().toString();
            } else {
                str2 = "com.baidu" + b;
            }
            a2.f1062a = C0693c.m1543a(str2.getBytes(), true);
            a2.f1063b = str;
        }
        m1554a(a2);
        a2.f1063b = "0";
        return a2;
    }

    /* renamed from: b */
    private Runnable m1563b(C0698b bVar) {
        return new C0701d(this, bVar);
    }

    /* renamed from: b */
    public static String m1564b(Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        return TextUtils.isEmpty(string) ? "" : string;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0033 A[SYNTHETIC, Splitter:B:23:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x003f A[SYNTHETIC, Splitter:B:30:0x003f] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m1566b(java.io.File r5) {
        /*
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x002c, all -> 0x0029 }
            r5 = 8192(0x2000, float:1.14794E-41)
            char[] r5 = new char[r5]     // Catch:{ Exception -> 0x0027 }
            java.io.CharArrayWriter r2 = new java.io.CharArrayWriter     // Catch:{ Exception -> 0x0027 }
            r2.<init>()     // Catch:{ Exception -> 0x0027 }
        L_0x000f:
            int r3 = r1.read(r5)     // Catch:{ Exception -> 0x0027 }
            if (r3 <= 0) goto L_0x001a
            r4 = 0
            r2.write(r5, r4, r3)     // Catch:{ Exception -> 0x0027 }
            goto L_0x000f
        L_0x001a:
            java.lang.String r5 = r2.toString()     // Catch:{ Exception -> 0x0027 }
            r1.close()     // Catch:{ Exception -> 0x0022 }
            goto L_0x0026
        L_0x0022:
            r0 = move-exception
            m1568b(r0)
        L_0x0026:
            return r5
        L_0x0027:
            r5 = move-exception
            goto L_0x002e
        L_0x0029:
            r5 = move-exception
            r1 = r0
            goto L_0x003d
        L_0x002c:
            r5 = move-exception
            r1 = r0
        L_0x002e:
            m1568b(r5)     // Catch:{ all -> 0x003c }
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ Exception -> 0x0037 }
            goto L_0x003b
        L_0x0037:
            r5 = move-exception
            m1568b(r5)
        L_0x003b:
            return r0
        L_0x003c:
            r5 = move-exception
        L_0x003d:
            if (r1 == 0) goto L_0x0047
            r1.close()     // Catch:{ Exception -> 0x0043 }
            goto L_0x0047
        L_0x0043:
            r0 = move-exception
            m1568b(r0)
        L_0x0047:
            goto L_0x0049
        L_0x0048:
            throw r5
        L_0x0049:
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.bbalbs.common.util.C0696b.m1566b(java.io.File):java.lang.String");
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m1568b(Throwable th) {
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m1570b(String str, String str2) {
        try {
            return Settings.System.putString(this.f1055b.getContentResolver(), str, str2);
        } catch (Exception e) {
            m1568b(e);
            return false;
        }
    }

    /* renamed from: c */
    private static C0698b m1571c(Context context) {
        if (f1054e == null) {
            synchronized (C0698b.class) {
                if (f1054e == null) {
                    SystemClock.uptimeMillis();
                    f1054e = new C0696b(context).m1562b();
                    SystemClock.uptimeMillis();
                }
            }
        }
        return f1054e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* access modifiers changed from: private */
    /* renamed from: c */
    public static void m1574c(String str, String str2) {
        File file;
        if (!TextUtils.isEmpty(str)) {
            File file2 = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig");
            File file3 = new File(file2, ".cuid");
            try {
                if (file2.exists() && !file2.isDirectory()) {
                    Random random = new Random();
                    File parentFile = file2.getParentFile();
                    String name = file2.getName();
                    do {
                        file = new File(parentFile, name + random.nextInt() + ".tmp");
                    } while (file.exists());
                    file2.renameTo(file);
                    file.delete();
                }
                file2.mkdirs();
                FileWriter fileWriter = new FileWriter(file3, false);
                fileWriter.write(C0692b.m1539a(C0691a.m1537a(f1053a, f1053a, (str + "=" + str2).getBytes()), "utf-8"));
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException | Exception unused) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public boolean m1575c() {
        return m1583f("android.permission.WRITE_SETTINGS");
    }

    /* renamed from: d */
    private C0698b m1577d() {
        String e = m1582e("com.baidu.deviceid");
        String e2 = m1582e("bd_setting_i");
        if (TextUtils.isEmpty(e2)) {
            e2 = m1588k("");
            if (!TextUtils.isEmpty(e2)) {
                m1570b("bd_setting_i", e2);
            }
        }
        if (TextUtils.isEmpty(e)) {
            String b = m1564b(this.f1055b);
            e = m1582e(C0693c.m1543a(("com.baidu" + e2 + b).getBytes(), true));
        }
        if (TextUtils.isEmpty(e)) {
            return null;
        }
        C0698b bVar = new C0698b(null);
        bVar.f1062a = e;
        bVar.f1063b = e2;
        return bVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public boolean m1579d(String str) {
        int i = Build.VERSION.SDK_INT >= 24 ? 0 : 1;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = this.f1055b.openFileOutput("libcuid.so", i);
            fileOutputStream.write(str.getBytes());
            fileOutputStream.flush();
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e) {
                    m1568b(e);
                }
            }
            if (i == 0) {
                return C0699c.m1593a(new File(this.f1055b.getFilesDir(), "libcuid.so").getAbsolutePath(), 436);
            }
            return true;
        } catch (Exception e2) {
            m1568b(e2);
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e3) {
                    m1568b(e3);
                }
            }
            return false;
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (Exception e4) {
                    m1568b(e4);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public C0698b m1580e() {
        File file = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig/.cuid2");
        if (!file.exists()) {
            return null;
        }
        String b = m1566b(file);
        if (TextUtils.isEmpty(b)) {
            return null;
        }
        try {
            return C0698b.m1589a(new String(C0691a.m1538b(f1053a, f1053a, C0692b.m1540a(b.getBytes()))));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public String m1582e(String str) {
        try {
            return Settings.System.getString(this.f1055b.getContentResolver(), str);
        } catch (Exception e) {
            m1568b(e);
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public boolean m1583f(String str) {
        return this.f1055b.checkPermission(str, Process.myPid(), Process.myUid()) == 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public C0698b m1584g(String str) {
        boolean z = false;
        String str2 = "";
        File file = new File(Environment.getExternalStorageDirectory(), "baidu/.cuid");
        if (!file.exists()) {
            file = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig/.cuid");
            z = true;
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append("\r\n");
            }
            bufferedReader.close();
            String[] split = new String(C0691a.m1538b(f1053a, f1053a, C0692b.m1540a(sb.toString().getBytes()))).split("=");
            if (split != null && split.length == 2) {
                if (TextUtils.isEmpty(str)) {
                    str = split[1];
                }
                str2 = split[1];
            }
            if (!z) {
                m1574c(str, str2);
            }
        } catch (FileNotFoundException | IOException | Exception unused) {
        }
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        C0698b bVar = new C0698b(null);
        bVar.f1062a = str2;
        bVar.f1063b = str;
        return bVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public static String m1585h(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return C0692b.m1539a(C0691a.m1537a(f1053a, f1053a, str.getBytes()), "utf-8");
        } catch (UnsupportedEncodingException | Exception e) {
            m1568b(e);
            return "";
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public static String m1586i(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return new String(C0691a.m1538b(f1053a, f1053a, C0692b.m1540a(str.getBytes())));
        } catch (Exception e) {
            m1568b(e);
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* access modifiers changed from: private */
    /* renamed from: j */
    public static void m1587j(String str) {
        File file;
        File file2 = new File(Environment.getExternalStorageDirectory(), "backups/.SystemConfig");
        File file3 = new File(file2, ".cuid2");
        try {
            if (file2.exists() && !file2.isDirectory()) {
                Random random = new Random();
                File parentFile = file2.getParentFile();
                String name = file2.getName();
                do {
                    file = new File(parentFile, name + random.nextInt() + ".tmp");
                } while (file.exists());
                file2.renameTo(file);
                file.delete();
            }
            file2.mkdirs();
            FileWriter fileWriter = new FileWriter(file3, false);
            fileWriter.write(str);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException | Exception unused) {
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public String m1588k(String str) {
        return "0";
    }
}
