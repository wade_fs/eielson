package com.baidu.location.p016d;

import com.baidu.location.p016d.C0813l;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: com.baidu.location.d.o */
final class C0819o extends C0813l.C0816b {
    C0819o(String str, int i, String str2, String str3, String str4, int i2, int i3) {
        super(str, i, str2, str3, str4, i2, i3);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00f1  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.String> mo10614a(org.json.JSONObject r25, java.lang.String r26, int r27) {
        /*
            r24 = this;
            java.lang.String r0 = "\",\""
            java.lang.String r1 = "y"
            java.lang.String r2 = "x"
            java.lang.String r3 = "stn"
            java.lang.String r4 = "st"
            java.util.Iterator r5 = r25.keys()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.lang.StringBuffer r7 = new java.lang.StringBuffer
            r7.<init>()
        L_0x0018:
            boolean r8 = r5.hasNext()
            java.lang.String r10 = "INSERT OR REPLACE INTO %s VALUES %s"
            r12 = 0
            if (r8 == 0) goto L_0x015d
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            r8.<init>()
            java.lang.Object r13 = r5.next()
            java.lang.String r13 = (java.lang.String) r13
            r14 = r26
            com.baidu.location.p016d.C0813l.C0816b.m2257b(r7, r13, r14, r12)
            r15 = r25
            org.json.JSONArray r16 = r15.getJSONArray(r13)     // Catch:{ JSONException -> 0x003a }
            r11 = r16
            goto L_0x003b
        L_0x003a:
            r11 = 0
        L_0x003b:
            if (r11 == 0) goto L_0x0147
        L_0x003d:
            int r9 = r11.length()
            java.lang.String r17 = "RGCSITE"
            if (r12 >= r9) goto L_0x0120
            org.json.JSONObject r9 = r11.getJSONObject(r12)     // Catch:{ JSONException -> 0x00e1 }
            boolean r18 = r9.has(r4)     // Catch:{ JSONException -> 0x00e1 }
            if (r18 == 0) goto L_0x005a
            java.lang.String r18 = r9.getString(r4)     // Catch:{ JSONException -> 0x00e1 }
            r23 = r18
            r18 = r4
            r4 = r23
            goto L_0x005d
        L_0x005a:
            r18 = r4
            r4 = 0
        L_0x005d:
            boolean r19 = r9.has(r3)     // Catch:{ JSONException -> 0x00da }
            if (r19 == 0) goto L_0x006e
            java.lang.String r19 = r9.getString(r3)     // Catch:{ JSONException -> 0x00da }
            r23 = r19
            r19 = r3
            r3 = r23
            goto L_0x0071
        L_0x006e:
            r19 = r3
            r3 = 0
        L_0x0071:
            boolean r20 = r9.has(r2)     // Catch:{ JSONException -> 0x00d5 }
            if (r20 == 0) goto L_0x0086
            double r20 = r9.getDouble(r2)     // Catch:{ JSONException -> 0x00d5 }
            java.lang.Double r20 = java.lang.Double.valueOf(r20)     // Catch:{ JSONException -> 0x00d5 }
            r23 = r20
            r20 = r2
            r2 = r23
            goto L_0x0089
        L_0x0086:
            r20 = r2
            r2 = 0
        L_0x0089:
            boolean r21 = r9.has(r1)     // Catch:{ JSONException -> 0x00d2 }
            if (r21 == 0) goto L_0x0098
            double r21 = r9.getDouble(r1)     // Catch:{ JSONException -> 0x00d2 }
            java.lang.Double r9 = java.lang.Double.valueOf(r21)     // Catch:{ JSONException -> 0x00d2 }
            goto L_0x0099
        L_0x0098:
            r9 = 0
        L_0x0099:
            int r21 = r8.length()     // Catch:{ JSONException -> 0x00d2 }
            r22 = r1
            java.lang.String r1 = ","
            if (r21 <= 0) goto L_0x00a6
            r8.append(r1)     // Catch:{ JSONException -> 0x00e9 }
        L_0x00a6:
            r21 = r5
            java.lang.String r5 = "(NULL,\""
            r8.append(r5)     // Catch:{ JSONException -> 0x00d0 }
            r8.append(r13)     // Catch:{ JSONException -> 0x00d0 }
            r8.append(r0)     // Catch:{ JSONException -> 0x00d0 }
            r8.append(r4)     // Catch:{ JSONException -> 0x00d0 }
            r8.append(r0)     // Catch:{ JSONException -> 0x00d0 }
            r8.append(r3)     // Catch:{ JSONException -> 0x00d0 }
            java.lang.String r3 = "\","
            r8.append(r3)     // Catch:{ JSONException -> 0x00d0 }
            r8.append(r2)     // Catch:{ JSONException -> 0x00d0 }
            r8.append(r1)     // Catch:{ JSONException -> 0x00d0 }
            r8.append(r9)     // Catch:{ JSONException -> 0x00d0 }
            java.lang.String r1 = ")"
            r8.append(r1)     // Catch:{ JSONException -> 0x00d0 }
            goto L_0x00eb
        L_0x00d0:
            goto L_0x00eb
        L_0x00d2:
            r22 = r1
            goto L_0x00e9
        L_0x00d5:
            r22 = r1
            r20 = r2
            goto L_0x00e9
        L_0x00da:
            r22 = r1
            r20 = r2
            r19 = r3
            goto L_0x00e9
        L_0x00e1:
            r22 = r1
            r20 = r2
            r19 = r3
            r18 = r4
        L_0x00e9:
            r21 = r5
        L_0x00eb:
            int r1 = r12 % 50
            r2 = 49
            if (r1 != r2) goto L_0x0111
            int r1 = r8.length()
            if (r1 <= 0) goto L_0x0111
            java.util.Locale r1 = java.util.Locale.US
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]
            r2 = 0
            r3[r2] = r17
            java.lang.String r4 = r8.toString()
            r5 = 1
            r3[r5] = r4
            java.lang.String r1 = java.lang.String.format(r1, r10, r3)
            r6.add(r1)
            r8.setLength(r2)
            goto L_0x0112
        L_0x0111:
            r2 = 0
        L_0x0112:
            int r12 = r12 + 1
            r4 = r18
            r3 = r19
            r2 = r20
            r5 = r21
            r1 = r22
            goto L_0x003d
        L_0x0120:
            r22 = r1
            r20 = r2
            r19 = r3
            r18 = r4
            r21 = r5
            r2 = 0
            int r1 = r8.length()
            if (r1 <= 0) goto L_0x0151
            java.util.Locale r1 = java.util.Locale.US
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r3[r2] = r17
            java.lang.String r2 = r8.toString()
            r4 = 1
            r3[r4] = r2
            java.lang.String r1 = java.lang.String.format(r1, r10, r3)
            r6.add(r1)
            goto L_0x0151
        L_0x0147:
            r22 = r1
            r20 = r2
            r19 = r3
            r18 = r4
            r21 = r5
        L_0x0151:
            r4 = r18
            r3 = r19
            r2 = r20
            r5 = r21
            r1 = r22
            goto L_0x0018
        L_0x015d:
            int r0 = r7.length()
            if (r0 <= 0) goto L_0x0178
            java.util.Locale r0 = java.util.Locale.US
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "RGCUPDATE"
            r3 = 0
            r1[r3] = r2
            r2 = 1
            r1[r2] = r7
            java.lang.String r0 = java.lang.String.format(r0, r10, r1)
            r6.add(r0)
            goto L_0x017a
        L_0x0178:
            r2 = 1
            r3 = 0
        L_0x017a:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.Object[] r1 = new java.lang.Object[r2]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r27)
            r1[r3] = r2
            java.lang.String r2 = "DELETE FROM RGCSITE WHERE _id NOT IN (SELECT _id FROM RGCSITE LIMIT %d);"
            java.lang.String r0 = java.lang.String.format(r0, r2, r1)
            r6.add(r0)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0819o.mo10614a(org.json.JSONObject, java.lang.String, int):java.util.List");
    }
}
