package com.baidu.mobstat;

import android.content.Context;
import org.json.JSONObject;

public class HeadObject {

    /* renamed from: A */
    JSONObject f4146A;

    /* renamed from: B */
    JSONObject f4147B;

    /* renamed from: C */
    String f4148C;

    /* renamed from: a */
    boolean f4149a = false;

    /* renamed from: b */
    String f4150b;

    /* renamed from: c */
    String f4151c;

    /* renamed from: d */
    String f4152d = "0";

    /* renamed from: e */
    String f4153e = null;

    /* renamed from: f */
    String f4154f = null;

    /* renamed from: g */
    int f4155g = -1;

    /* renamed from: h */
    String f4156h;

    /* renamed from: i */
    String f4157i;

    /* renamed from: j */
    int f4158j;

    /* renamed from: k */
    int f4159k;

    /* renamed from: l */
    String f4160l = null;

    /* renamed from: m */
    String f4161m;

    /* renamed from: n */
    String f4162n;

    /* renamed from: o */
    String f4163o;

    /* renamed from: p */
    String f4164p;

    /* renamed from: q */
    String f4165q;

    /* renamed from: r */
    String f4166r;

    /* renamed from: s */
    String f4167s;

    /* renamed from: t */
    String f4168t;

    /* renamed from: u */
    String f4169u;

    /* renamed from: v */
    String f4170v;

    /* renamed from: w */
    String f4171w;

    /* renamed from: x */
    String f4172x;

    /* renamed from: y */
    String f4173y;

    /* renamed from: z */
    String f4174z;

    public synchronized void installHeader(Context context, JSONObject jSONObject) {
        m4442a(context);
        if (jSONObject.length() <= 10) {
            updateHeader(context, jSONObject);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(36:6|7|8|(1:10)(1:11)|12|(1:14)|15|(2:16|17)|18|20|21|22|23|24|25|26|27|28|29|(1:31)|32|33|34|35|36|37|(1:39)(1:40)|41|42|(1:44)(1:45)|46|47|48|49|50|51) */
    /* JADX WARNING: Can't wrap try/catch for region: R(37:6|7|8|(1:10)(1:11)|12|(1:14)|15|16|17|18|20|21|22|23|24|25|26|27|28|29|(1:31)|32|33|34|35|36|37|(1:39)(1:40)|41|42|(1:44)(1:45)|46|47|48|49|50|51) */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x00bc */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x00c2 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x00d6 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x0104 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:36:0x012c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x0141 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:46:0x0156 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:48:0x0160 */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ef A[Catch:{ Exception -> 0x0104 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0136 A[Catch:{ Exception -> 0x0141 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013d A[Catch:{ Exception -> 0x0141 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x014b A[Catch:{ Exception -> 0x0156 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0152 A[Catch:{ Exception -> 0x0156 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m4442a(android.content.Context r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.f4149a     // Catch:{ all -> 0x017c }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r3)
            return
        L_0x0007:
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            com.baidu.mobstat.C1267ba.m4689e(r4, r0)     // Catch:{ all -> 0x017c }
            java.lang.String r0 = "android.permission.INTERNET"
            com.baidu.mobstat.C1267ba.m4689e(r4, r0)     // Catch:{ all -> 0x017c }
            java.lang.String r0 = "android.permission.ACCESS_NETWORK_STATE"
            com.baidu.mobstat.C1267ba.m4689e(r4, r0)     // Catch:{ all -> 0x017c }
            java.lang.String r0 = "android.permission.WRITE_SETTINGS"
            com.baidu.mobstat.C1267ba.m4689e(r4, r0)     // Catch:{ all -> 0x017c }
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r4.getSystemService(r0)     // Catch:{ all -> 0x017c }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r1 = r1.getOSVersion()     // Catch:{ all -> 0x017c }
            r3.f4150b = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r1 = r1.getOSSysVersion()     // Catch:{ all -> 0x017c }
            r3.f4151c = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r1 = r1.getPhoneModel()     // Catch:{ all -> 0x017c }
            r3.f4162n = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r1 = r1.getManufacturer()     // Catch:{ all -> 0x017c }
            r3.f4163o = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r1 = r1.getUUID()     // Catch:{ all -> 0x017c }
            r3.f4174z = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            org.json.JSONObject r1 = r1.getHeaderExt(r4)     // Catch:{ all -> 0x017c }
            r3.f4146A = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            org.json.JSONObject r1 = r1.getPushId(r4)     // Catch:{ all -> 0x017c }
            r3.f4147B = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r1 = r1.getDeviceId(r0, r4)     // Catch:{ all -> 0x017c }
            r3.f4157i = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.BasicStoreTools r1 = com.baidu.mobstat.BasicStoreTools.getInstance()     // Catch:{ all -> 0x017c }
            boolean r1 = r1.getForTV(r4)     // Catch:{ all -> 0x017c }
            if (r1 == 0) goto L_0x0080
            java.lang.String r1 = "1"
            goto L_0x0082
        L_0x0080:
            java.lang.String r1 = "0"
        L_0x0082:
            r3.f4152d = r1     // Catch:{ all -> 0x017c }
            boolean r1 = com.baidu.mobstat.C1276bh.m4748u(r4)     // Catch:{ all -> 0x017c }
            if (r1 == 0) goto L_0x008e
            java.lang.String r1 = "2"
            r3.f4152d = r1     // Catch:{ all -> 0x017c }
        L_0x008e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x017c }
            r1.<init>()     // Catch:{ all -> 0x017c }
            java.lang.String r2 = r3.f4152d     // Catch:{ all -> 0x017c }
            r1.append(r2)     // Catch:{ all -> 0x017c }
            java.lang.String r2 = "-0"
            r1.append(r2)     // Catch:{ all -> 0x017c }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x017c }
            r3.f4152d = r1     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r1 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x00b5 }
            boolean r1 = r1.isDeviceMacEnabled(r4)     // Catch:{ Exception -> 0x00b5 }
            com.baidu.mobstat.CooperService r2 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x00b5 }
            java.lang.String r1 = r2.getMacAddress(r4, r1)     // Catch:{ Exception -> 0x00b5 }
            r3.f4167s = r1     // Catch:{ Exception -> 0x00b5 }
        L_0x00b5:
            r1 = 1
            java.lang.String r2 = com.baidu.mobstat.C1276bh.m4731f(r1, r4)     // Catch:{ Exception -> 0x00bc }
            r3.f4169u = r2     // Catch:{ Exception -> 0x00bc }
        L_0x00bc:
            java.lang.String r2 = com.baidu.mobstat.C1276bh.m4715a(r4, r1)     // Catch:{ Exception -> 0x00c2 }
            r3.f4170v = r2     // Catch:{ Exception -> 0x00c2 }
        L_0x00c2:
            com.baidu.mobstat.CooperService r2 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r2 = r2.getCUID(r4, r1)     // Catch:{ all -> 0x017c }
            r3.f4154f = r2     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r2 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x00d6 }
            java.lang.String r0 = r2.getOperator(r0)     // Catch:{ Exception -> 0x00d6 }
            r3.f4161m = r0     // Catch:{ Exception -> 0x00d6 }
        L_0x00d6:
            int r0 = com.baidu.mobstat.C1276bh.m4722c(r4)     // Catch:{ Exception -> 0x0104 }
            r3.f4158j = r0     // Catch:{ Exception -> 0x0104 }
            int r0 = com.baidu.mobstat.C1276bh.m4726d(r4)     // Catch:{ Exception -> 0x0104 }
            r3.f4159k = r0     // Catch:{ Exception -> 0x0104 }
            android.content.res.Resources r0 = r4.getResources()     // Catch:{ Exception -> 0x0104 }
            android.content.res.Configuration r0 = r0.getConfiguration()     // Catch:{ Exception -> 0x0104 }
            int r0 = r0.orientation     // Catch:{ Exception -> 0x0104 }
            r2 = 2
            if (r0 != r2) goto L_0x0104
            int r0 = r3.f4158j     // Catch:{ Exception -> 0x0104 }
            int r2 = r3.f4159k     // Catch:{ Exception -> 0x0104 }
            r0 = r0 ^ r2
            r3.f4158j = r0     // Catch:{ Exception -> 0x0104 }
            int r0 = r3.f4158j     // Catch:{ Exception -> 0x0104 }
            int r2 = r3.f4159k     // Catch:{ Exception -> 0x0104 }
            r0 = r0 ^ r2
            r3.f4159k = r0     // Catch:{ Exception -> 0x0104 }
            int r0 = r3.f4158j     // Catch:{ Exception -> 0x0104 }
            int r2 = r3.f4159k     // Catch:{ Exception -> 0x0104 }
            r0 = r0 ^ r2
            r3.f4158j = r0     // Catch:{ Exception -> 0x0104 }
        L_0x0104:
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r0 = r0.getAppChannel(r4)     // Catch:{ all -> 0x017c }
            r3.f4160l = r0     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r0 = r0.getAppKey(r4)     // Catch:{ all -> 0x017c }
            r3.f4153e = r0     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x012c }
            int r0 = r0.getAppVersionCode(r4)     // Catch:{ Exception -> 0x012c }
            r3.f4155g = r0     // Catch:{ Exception -> 0x012c }
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x012c }
            java.lang.String r0 = r0.getAppVersionName(r4)     // Catch:{ Exception -> 0x012c }
            r3.f4156h = r0     // Catch:{ Exception -> 0x012c }
        L_0x012c:
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x0141 }
            boolean r0 = r0.checkCellLocationSetting(r4)     // Catch:{ Exception -> 0x0141 }
            if (r0 == 0) goto L_0x013d
            java.lang.String r0 = com.baidu.mobstat.C1276bh.m4735h(r4)     // Catch:{ Exception -> 0x0141 }
            r3.f4164p = r0     // Catch:{ Exception -> 0x0141 }
            goto L_0x0141
        L_0x013d:
            java.lang.String r0 = "0_0_0"
            r3.f4164p = r0     // Catch:{ Exception -> 0x0141 }
        L_0x0141:
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x0156 }
            boolean r0 = r0.checkGPSLocationSetting(r4)     // Catch:{ Exception -> 0x0156 }
            if (r0 == 0) goto L_0x0152
            java.lang.String r0 = com.baidu.mobstat.C1276bh.m4736i(r4)     // Catch:{ Exception -> 0x0156 }
            r3.f4165q = r0     // Catch:{ Exception -> 0x0156 }
            goto L_0x0156
        L_0x0152:
            java.lang.String r0 = ""
            r3.f4165q = r0     // Catch:{ Exception -> 0x0156 }
        L_0x0156:
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r0.getLinkedWay(r4)     // Catch:{ Exception -> 0x0160 }
            r3.f4166r = r0     // Catch:{ Exception -> 0x0160 }
        L_0x0160:
            java.lang.String r0 = com.baidu.mobstat.C1276bh.m4718b()     // Catch:{ all -> 0x017c }
            r3.f4171w = r0     // Catch:{ all -> 0x017c }
            java.lang.String r0 = android.os.Build.BOARD     // Catch:{ all -> 0x017c }
            r3.f4172x = r0     // Catch:{ all -> 0x017c }
            java.lang.String r0 = android.os.Build.BRAND     // Catch:{ all -> 0x017c }
            r3.f4173y = r0     // Catch:{ all -> 0x017c }
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()     // Catch:{ all -> 0x017c }
            java.lang.String r4 = r0.getUserId(r4)     // Catch:{ all -> 0x017c }
            r3.f4148C = r4     // Catch:{ all -> 0x017c }
            r3.f4149a = r1     // Catch:{ all -> 0x017c }
            monitor-exit(r3)
            return
        L_0x017c:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.HeadObject.m4442a(android.content.Context):void");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void updateHeader(android.content.Context r5, org.json.JSONObject r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.lang.String r0 = "o"
            java.lang.String r1 = "Android"
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "st"
            r1 = 0
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "s"
            java.lang.String r2 = r4.f4150b     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x0017
            java.lang.String r2 = ""
            goto L_0x0019
        L_0x0017:
            java.lang.String r2 = r4.f4150b     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0019:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "sv"
            java.lang.String r2 = r4.f4151c     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x0025
            java.lang.String r2 = ""
            goto L_0x0027
        L_0x0025:
            java.lang.String r2 = r4.f4151c     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0027:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "k"
            java.lang.String r2 = r4.f4153e     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x0033
            java.lang.String r2 = ""
            goto L_0x0035
        L_0x0033:
            java.lang.String r2 = r4.f4153e     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0035:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "pt"
            java.lang.String r2 = r4.f4152d     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x0041
            java.lang.String r2 = "0"
            goto L_0x0043
        L_0x0041:
            java.lang.String r2 = r4.f4152d     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0043:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "i"
            java.lang.String r2 = ""
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "v"
            java.lang.String r2 = "3.9.2.1"
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "sc"
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "a"
            int r1 = r4.f4155g     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "n"
            java.lang.String r1 = r4.f4156h     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r1 != 0) goto L_0x0069
            java.lang.String r1 = ""
            goto L_0x006b
        L_0x0069:
            java.lang.String r1 = r4.f4156h     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x006b:
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "d"
            java.lang.String r1 = ""
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "mc"
            java.lang.String r1 = r4.f4167s     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r1 != 0) goto L_0x007e
            java.lang.String r1 = ""
            goto L_0x0080
        L_0x007e:
            java.lang.String r1 = r4.f4167s     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0080:
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "bm"
            java.lang.String r1 = r4.f4169u     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r1 != 0) goto L_0x008c
            java.lang.String r1 = ""
            goto L_0x008e
        L_0x008c:
            java.lang.String r1 = r4.f4169u     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x008e:
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "dd"
            java.lang.String r1 = r4.f4157i     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r1 != 0) goto L_0x009a
            java.lang.String r1 = ""
            goto L_0x009c
        L_0x009a:
            java.lang.String r1 = r4.f4157i     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x009c:
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "ii"
            java.lang.String r1 = r4.f4154f     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r1 != 0) goto L_0x00a8
            java.lang.String r1 = ""
            goto L_0x00aa
        L_0x00a8:
            java.lang.String r1 = r4.f4154f     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x00aa:
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "tg"
            r1 = 1
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "w"
            int r2 = r4.f4158j     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "h"
            int r2 = r4.f4159k     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "dn"
            java.lang.String r2 = r4.f4170v     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x00ca
            java.lang.String r2 = ""
            goto L_0x00cc
        L_0x00ca:
            java.lang.String r2 = r4.f4170v     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x00cc:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "c"
            java.lang.String r2 = r4.f4160l     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x00d8
            java.lang.String r2 = ""
            goto L_0x00da
        L_0x00d8:
            java.lang.String r2 = r4.f4160l     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x00da:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "op"
            java.lang.String r2 = r4.f4161m     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x00e6
            java.lang.String r2 = ""
            goto L_0x00e8
        L_0x00e6:
            java.lang.String r2 = r4.f4161m     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x00e8:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "m"
            java.lang.String r2 = r4.f4162n     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x00f4
            java.lang.String r2 = ""
            goto L_0x00f6
        L_0x00f4:
            java.lang.String r2 = r4.f4162n     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x00f6:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "ma"
            java.lang.String r2 = r4.f4163o     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x0102
            java.lang.String r2 = ""
            goto L_0x0104
        L_0x0102:
            java.lang.String r2 = r4.f4163o     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0104:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "cl"
            java.lang.String r2 = r4.f4164p     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "gl"
            java.lang.String r2 = r4.f4165q     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x0117
            java.lang.String r2 = ""
            goto L_0x0119
        L_0x0117:
            java.lang.String r2 = r4.f4165q     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0119:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "l"
            java.lang.String r2 = r4.f4166r     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r2 != 0) goto L_0x0125
            java.lang.String r2 = ""
            goto L_0x0127
        L_0x0125:
            java.lang.String r2 = r4.f4166r     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0127:
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "t"
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r0, r2)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "pn"
            java.lang.String r1 = com.baidu.mobstat.C1276bh.m4734h(r1, r5)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "rom"
            java.lang.String r1 = r4.f4171w     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r1 != 0) goto L_0x0145
            java.lang.String r1 = ""
            goto L_0x0147
        L_0x0145:
            java.lang.String r1 = r4.f4171w     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0147:
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "bo"
            java.lang.String r1 = r4.f4172x     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r1 != 0) goto L_0x0153
            java.lang.String r1 = ""
            goto L_0x0155
        L_0x0153:
            java.lang.String r1 = r4.f4172x     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0155:
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "bd"
            java.lang.String r1 = r4.f4173y     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r1 != 0) goto L_0x0161
            java.lang.String r1 = ""
            goto L_0x0163
        L_0x0161:
            java.lang.String r1 = r4.f4173y     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x0163:
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "td"
            java.lang.String r1 = com.baidu.mobstat.C1276bh.m4720b(r5)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = "at"
            java.lang.String r1 = "0"
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r0 = com.baidu.mobstat.C1276bh.m4746s(r5)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r1 = "pl"
            r6.put(r1, r0)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r1 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r0 != 0) goto L_0x018a
            java.lang.String r1 = com.baidu.mobstat.C1276bh.m4747t(r5)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x018a:
            java.lang.String r5 = "scl"
            if (r1 != 0) goto L_0x0190
            java.lang.String r1 = ""
        L_0x0190:
            r6.put(r5, r1)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r5 = "sign"
            java.lang.String r0 = r4.f4174z     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r0 != 0) goto L_0x019c
            java.lang.String r0 = ""
            goto L_0x019e
        L_0x019c:
            java.lang.String r0 = r4.f4174z     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x019e:
            r6.put(r5, r0)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            org.json.JSONObject r5 = r4.f4146A     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r5 == 0) goto L_0x01b5
            org.json.JSONObject r5 = r4.f4146A     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            int r5 = r5.length()     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r5 == 0) goto L_0x01b5
            java.lang.String r5 = "ext"
            org.json.JSONObject r0 = r4.f4146A     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r5, r0)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            goto L_0x01ba
        L_0x01b5:
            java.lang.String r5 = "ext"
            r6.remove(r5)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x01ba:
            org.json.JSONObject r5 = r4.f4147B     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            if (r5 != 0) goto L_0x01c5
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r5.<init>()     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r4.f4147B = r5     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
        L_0x01c5:
            java.lang.String r5 = "push"
            org.json.JSONObject r0 = r4.f4147B     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r5, r0)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            java.lang.String r5 = "uid"
            java.lang.String r0 = r4.f4148C     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            r6.put(r5, r0)     // Catch:{ Exception -> 0x01d7, all -> 0x01d4 }
            goto L_0x01d7
        L_0x01d4:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x01d7:
            monitor-exit(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.HeadObject.updateHeader(android.content.Context, org.json.JSONObject):void");
    }

    public void setHeaderExt(JSONObject jSONObject) {
        this.f4146A = jSONObject;
    }

    public void setPushInfo(JSONObject jSONObject) {
        this.f4147B = jSONObject;
    }

    public void setUserId(String str) {
        this.f4148C = str;
    }
}
