package com.baidu.mobstat;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import com.baidu.mobstat.C1262ay;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.m */
class C1294m {

    /* renamed from: a */
    static C1294m f4366a = new C1294m();

    /* renamed from: b */
    private String f4367b = "";

    /* renamed from: a */
    private boolean m4818a(int i) {
        return i == 100 || i == 200 || i == 130;
    }

    C1294m() {
    }

    /* renamed from: a */
    public synchronized void mo13974a(Context context, boolean z) {
        int i = 1;
        if (!z) {
            i = 20;
        }
        m4817a(context, z, i);
    }

    /* renamed from: a */
    private void m4817a(Context context, boolean z, int i) {
        ArrayList<C1295a> a = m4815a(context, i);
        if (a != null && a.size() != 0) {
            if (z) {
                String b = a.get(0).mo13976b();
                if (m4819a(b, this.f4367b)) {
                    this.f4367b = b;
                }
            }
            m4816a(context, a, z);
        }
    }

    /* renamed from: a */
    private ArrayList<C1295a> m4815a(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return m4822c(context, i);
        }
        return m4820b(context, i);
    }

    /* renamed from: b */
    private ArrayList<C1295a> m4820b(Context context, int i) {
        List<ActivityManager.RunningTaskInfo> list;
        try {
            list = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(50);
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            list = null;
        }
        if (list == null) {
            return new ArrayList<>();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (ActivityManager.RunningTaskInfo runningTaskInfo : list) {
            if (linkedHashMap.size() > i) {
                break;
            }
            ComponentName componentName = runningTaskInfo.topActivity;
            if (componentName != null) {
                String packageName = componentName.getPackageName();
                if (!TextUtils.isEmpty(packageName) && !m4821b(context, packageName) && !linkedHashMap.containsKey(packageName)) {
                    linkedHashMap.put(packageName, new C1295a(packageName, m4814a(context, packageName), ""));
                }
            }
        }
        return new ArrayList<>(linkedHashMap.values());
    }

    /* renamed from: c */
    private ArrayList<C1295a> m4822c(Context context, int i) {
        String[] strArr;
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return new ArrayList<>();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (int i2 = 0; i2 < runningAppProcesses.size() && linkedHashMap.size() <= i; i2++) {
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = runningAppProcesses.get(i2);
            if (!(!m4818a(runningAppProcessInfo.importance) || (strArr = runningAppProcessInfo.pkgList) == null || strArr.length == 0)) {
                String str = runningAppProcessInfo.pkgList[0];
                if (!TextUtils.isEmpty(str) && !m4821b(context, str) && !linkedHashMap.containsKey(str)) {
                    linkedHashMap.put(str, new C1295a(str, m4814a(context, str), String.valueOf(runningAppProcessInfo.importance)));
                }
            }
        }
        return new ArrayList<>(linkedHashMap.values());
    }

    /* renamed from: a */
    private boolean m4819a(String str, String str2) {
        return !TextUtils.isEmpty(str) && !str.equals(this.f4367b);
    }

    /* renamed from: a */
    private String m4814a(Context context, String str) {
        String str2;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return "";
        }
        try {
            str2 = packageManager.getPackageInfo(str, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            C1255as.m4626c().mo13944b(e);
            str2 = "";
        }
        if (str2 == null) {
            return "";
        }
        return str2;
    }

    /* renamed from: b */
    private boolean m4821b(Context context, String str) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return false;
        }
        try {
            ApplicationInfo applicationInfo = packageManager.getPackageInfo(str, 0).applicationInfo;
            if (applicationInfo == null || (applicationInfo.flags & 1) == 0) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            C1255as.m4626c().mo13944b(e);
            return false;
        }
    }

    /* renamed from: a */
    private void m4816a(Context context, ArrayList<C1295a> arrayList, boolean z) {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(System.currentTimeMillis() + "|");
        sb.append(z ? 1 : 0);
        try {
            JSONArray jSONArray = new JSONArray();
            Iterator<C1295a> it = arrayList.iterator();
            while (it.hasNext()) {
                JSONObject a = it.next().mo13975a();
                if (a != null) {
                    jSONArray.put(a);
                }
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("app_trace", jSONArray);
            jSONObject.put("meta-data", sb.toString());
            str = C1262ay.C1263a.m4665a(jSONObject.toString().getBytes());
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            str = "";
        }
        if (!TextUtils.isEmpty(str)) {
            C1300r.APP_TRACE.mo13990a(System.currentTimeMillis(), str);
        }
    }

    /* renamed from: com.baidu.mobstat.m$a */
    static class C1295a {

        /* renamed from: a */
        private String f4368a;

        /* renamed from: b */
        private String f4369b;

        /* renamed from: c */
        private String f4370c;

        public C1295a(String str, String str2, String str3) {
            this.f4368a = str == null ? "" : str;
            this.f4369b = str2 == null ? "" : str2;
            this.f4370c = str3 == null ? "" : str3;
        }

        /* renamed from: a */
        public JSONObject mo13975a() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("n", this.f4368a);
                jSONObject.put("v", this.f4369b);
                jSONObject.put(Config.DEVICE_WIDTH, this.f4370c);
                return jSONObject;
            } catch (JSONException e) {
                C1255as.m4626c().mo13944b(e);
                return null;
            }
        }

        /* renamed from: b */
        public String mo13976b() {
            return this.f4368a;
        }
    }
}
