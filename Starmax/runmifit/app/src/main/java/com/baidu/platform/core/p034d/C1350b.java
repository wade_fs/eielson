package com.baidu.platform.core.p034d;

import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.route.BikingRoutePlanOption;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;
import com.tamic.novate.download.MimeType;

/* renamed from: com.baidu.platform.core.d.b */
public class C1350b extends C1320e {
    public C1350b(BikingRoutePlanOption bikingRoutePlanOption) {
        m5032a(bikingRoutePlanOption);
    }

    /* renamed from: a */
    private void m5032a(BikingRoutePlanOption bikingRoutePlanOption) {
        this.f4435a.mo14094a("mode", "riding");
        LatLng location = bikingRoutePlanOption.mFrom.getLocation();
        if (location != null) {
            if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                location = CoordTrans.gcjToBaidu(location);
            }
            C1381a aVar = this.f4435a;
            aVar.mo14094a("origin", location.latitude + "," + location.longitude);
        } else {
            this.f4435a.mo14094a("origin", bikingRoutePlanOption.mFrom.getName());
        }
        LatLng location2 = bikingRoutePlanOption.mTo.getLocation();
        if (location2 != null) {
            if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                location2 = CoordTrans.gcjToBaidu(location2);
            }
            C1381a aVar2 = this.f4435a;
            aVar2.mo14094a("destination", location2.latitude + "," + location2.longitude);
        } else {
            this.f4435a.mo14094a("destination", bikingRoutePlanOption.mTo.getName());
        }
        this.f4435a.mo14094a("origin_region", bikingRoutePlanOption.mFrom.getCity());
        this.f4435a.mo14094a("destination_region", bikingRoutePlanOption.mTo.getCity());
        if (bikingRoutePlanOption.mRidingType == 1) {
            this.f4435a.mo14094a("riding_type", String.valueOf(bikingRoutePlanOption.mRidingType));
        }
        this.f4435a.mo14094a("output", MimeType.JSON);
        this.f4435a.mo14094a("from", "android_map_sdk");
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14085j();
    }
}
