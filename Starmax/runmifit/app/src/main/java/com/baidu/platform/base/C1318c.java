package com.baidu.platform.base;

import com.baidu.mapapi.search.core.SearchResult;

/* renamed from: com.baidu.platform.base.c */
class C1318c implements Runnable {

    /* renamed from: a */
    final /* synthetic */ C1319d f4430a;

    /* renamed from: b */
    final /* synthetic */ SearchResult f4431b;

    /* renamed from: c */
    final /* synthetic */ Object f4432c;

    /* renamed from: d */
    final /* synthetic */ C1316a f4433d;

    C1318c(C1316a aVar, C1319d dVar, SearchResult searchResult, Object obj) {
        this.f4433d = aVar;
        this.f4430a = dVar;
        this.f4431b = searchResult;
        this.f4432c = obj;
    }

    public void run() {
        if (this.f4430a != null) {
            this.f4433d.f4422a.lock();
            try {
                this.f4430a.mo14020a(this.f4431b, this.f4432c);
            } finally {
                this.f4433d.f4422a.unlock();
            }
        }
    }
}
