package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.map.Marker;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.f */
public class C1034f extends C1031c {

    /* renamed from: a */
    private Animator f3321a = null;

    /* renamed from: b */
    private long f3322b = 0;

    /* renamed from: c */
    private Interpolator f3323c = null;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Animation.AnimationListener f3324d = null;

    /* renamed from: e */
    private int f3325e = 1;

    /* renamed from: f */
    private int f3326f = 0;

    /* renamed from: g */
    private float[] f3327g;

    public C1034f(float... fArr) {
        this.f3327g = fArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ObjectAnimator mo12894a(Marker marker) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(marker, "rotate", this.f3327g);
        if (ofFloat != null) {
            ofFloat.setRepeatCount(this.f3326f);
            ofFloat.setRepeatMode(mo12895c());
            ofFloat.setDuration(this.f3322b);
            Interpolator interpolator = this.f3323c;
            if (interpolator != null) {
                ofFloat.setInterpolator(interpolator);
            }
        }
        return ofFloat;
    }

    /* renamed from: a */
    public void mo12874a() {
        Animator animator = this.f3321a;
        if (animator != null) {
            animator.start();
        }
    }

    /* renamed from: a */
    public void mo12875a(int i) {
        this.f3325e = i;
    }

    /* renamed from: a */
    public void mo12876a(long j) {
        if (j < 0) {
            j = 0;
        }
        this.f3322b = j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo12877a(Animator animator) {
        if (animator != null) {
            animator.addListener(new C1035g(this));
        }
    }

    /* renamed from: a */
    public void mo12878a(Interpolator interpolator) {
        this.f3323c = interpolator;
    }

    /* renamed from: a */
    public void mo12879a(Animation.AnimationListener animationListener) {
        this.f3324d = animationListener;
    }

    /* renamed from: a */
    public void mo12880a(Marker marker, Animation animation) {
        this.f3321a = mo12894a(marker);
        mo12877a(this.f3321a);
    }

    /* renamed from: b */
    public void mo12881b() {
        Animator animator = this.f3321a;
        if (animator != null) {
            animator.cancel();
            this.f3321a = null;
        }
    }

    /* renamed from: b */
    public void mo12882b(int i) {
        if (i > 0 || i == -1) {
            this.f3326f = i;
        }
    }

    /* renamed from: c */
    public int mo12895c() {
        return this.f3325e;
    }

    /* renamed from: c */
    public void mo12884c(int i) {
    }
}
