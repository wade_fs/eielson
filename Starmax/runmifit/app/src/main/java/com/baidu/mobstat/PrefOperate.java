package com.baidu.mobstat;

import android.content.Context;
import android.text.TextUtils;

public class PrefOperate {
    public static void loadMetaDataConfig(Context context) {
        SendStrategyEnum sendStrategyEnum = SendStrategyEnum.APP_START;
        try {
            String a = C1276bh.m4716a(context, Config.EXCEPTION_LOG_META_NAME);
            if (!TextUtils.isEmpty(a) && "true".equals(a)) {
                ExceptionAnalysis.getInstance().openExceptionAnalysis(context, false);
            }
        } catch (Exception unused) {
        }
        try {
            String a2 = C1276bh.m4716a(context, Config.SEND_STRATEGY_META_NAME);
            if (!TextUtils.isEmpty(a2)) {
                if (a2.equals(SendStrategyEnum.APP_START.name())) {
                    sendStrategyEnum = SendStrategyEnum.APP_START;
                    BasicStoreTools.getInstance().setSendStrategy(context, sendStrategyEnum.ordinal());
                } else if (a2.equals(SendStrategyEnum.ONCE_A_DAY.name())) {
                    sendStrategyEnum = SendStrategyEnum.ONCE_A_DAY;
                    BasicStoreTools.getInstance().setSendStrategy(context, sendStrategyEnum.ordinal());
                    BasicStoreTools.getInstance().setSendStrategyTime(context, 24);
                } else if (a2.equals(SendStrategyEnum.SET_TIME_INTERVAL.name())) {
                    sendStrategyEnum = SendStrategyEnum.SET_TIME_INTERVAL;
                    BasicStoreTools.getInstance().setSendStrategy(context, sendStrategyEnum.ordinal());
                }
            }
        } catch (Exception unused2) {
        }
        try {
            String a3 = C1276bh.m4716a(context, Config.TIME_INTERVAL_META_NAME);
            if (!TextUtils.isEmpty(a3)) {
                int parseInt = Integer.parseInt(a3);
                if (sendStrategyEnum.ordinal() == SendStrategyEnum.SET_TIME_INTERVAL.ordinal() && parseInt > 0 && parseInt <= 24) {
                    BasicStoreTools.getInstance().setSendStrategyTime(context, parseInt);
                }
            }
        } catch (Exception unused3) {
        }
        try {
            String a4 = C1276bh.m4716a(context, Config.ONLY_WIFI_META_NAME);
            if (TextUtils.isEmpty(a4)) {
                return;
            }
            if ("true".equals(a4)) {
                BasicStoreTools.getInstance().setOnlyWifi(context, true);
            } else if ("false".equals(a4)) {
                BasicStoreTools.getInstance().setOnlyWifi(context, false);
            }
        } catch (Exception unused4) {
        }
    }

    public static void setAppKey(String str) {
        CooperService.instance().getHeadObject().f4153e = str;
    }

    public static String getAppKey(Context context) {
        return CooperService.instance().getAppKey(context);
    }

    public static void setAppChannel(String str) {
        if (str == null || str.equals("")) {
            C1256at.m4629c().mo13946c("[WARNING] The channel you have set is empty");
        }
        CooperService.instance().getHeadObject().f4160l = str;
    }

    public static void setAppChannel(Context context, String str, boolean z) {
        if (str == null || str.equals("")) {
            C1256at.m4629c().mo13946c("[WARNING] The channel you have set is empty");
        }
        CooperService.instance().getHeadObject().f4160l = str;
        if (z && str != null && !str.equals("")) {
            BasicStoreTools.getInstance().setAppChannelWithPreference(context, str);
            BasicStoreTools.getInstance().setAppChannelWithCode(context, true);
        }
        if (!z) {
            BasicStoreTools.getInstance().setAppChannelWithPreference(context, "");
            BasicStoreTools.getInstance().setAppChannelWithCode(context, false);
        }
    }
}
