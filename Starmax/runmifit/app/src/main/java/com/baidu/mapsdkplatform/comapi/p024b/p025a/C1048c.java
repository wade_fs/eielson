package com.baidu.mapsdkplatform.comapi.p024b.p025a;

import android.content.Context;
import android.os.Build;
import com.baidu.mapapi.NetworkUtil;
import com.baidu.mapsdkplatform.comapi.util.C1174h;
import com.baidu.mapsdkplatform.comapi.util.C1175i;
import com.baidu.mapsdkplatform.comapi.util.SyncSysInfo;
import com.baidu.mapsdkplatform.comjni.util.JNIHandler;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.download.MimeType;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.zip.GZIPOutputStream;

/* renamed from: com.baidu.mapsdkplatform.comapi.b.a.c */
public class C1048c {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static String f3361a = "";
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static String f3362b = "";

    /* renamed from: c */
    private static String f3363c = "";

    /* renamed from: d */
    private Context f3364d;

    /* renamed from: com.baidu.mapsdkplatform.comapi.b.a.c$a */
    private static final class C1049a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1048c f3365a = new C1048c();
    }

    /* renamed from: a */
    public static C1048c m3446a() {
        return C1049a.f3365a;
    }

    /* renamed from: a */
    private void m3448a(InputStream inputStream, OutputStream outputStream) throws Exception {
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr, 0, 1024);
            if (read != -1) {
                gZIPOutputStream.write(bArr, 0, read);
            } else {
                gZIPOutputStream.flush();
                gZIPOutputStream.close();
                try {
                    outputStream.close();
                    inputStream.close();
                    return;
                } catch (Exception unused) {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3449a(File[] fileArr) {
        int length = fileArr.length;
        for (int i = 0; i < length - 10; i++) {
            int i2 = i + 10;
            if (fileArr[i2] != null && fileArr[i2].exists()) {
                fileArr[i2].delete();
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:44:? */
    /* JADX INFO: additional move instructions added (6) to help type inference */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v10 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e0, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00e1, code lost:
        r1 = r5;
        r3 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e3, code lost:
        r1 = r5;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:85:0x012b */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0146 A[SYNTHETIC, Splitter:B:101:0x0146] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e0 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:23:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0115 A[SYNTHETIC, Splitter:B:74:0x0115] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0128 A[SYNTHETIC, Splitter:B:83:0x0128] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0131 A[SYNTHETIC, Splitter:B:91:0x0131] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean m3451a(java.io.File r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            r0 = 0
            r1 = 0
            java.net.HttpURLConnection r2 = r9.m3459g()     // Catch:{ Exception -> 0x012c, all -> 0x010f }
            if (r2 != 0) goto L_0x0010
            if (r2 == 0) goto L_0x000e
            r2.disconnect()     // Catch:{ Exception -> 0x000e }
        L_0x000e:
            monitor-exit(r9)
            return r0
        L_0x0010:
            r2.connect()     // Catch:{ Exception -> 0x010d, all -> 0x010a }
            java.io.OutputStream r3 = r2.getOutputStream()     // Catch:{ Exception -> 0x010d, all -> 0x010a }
            java.lang.StringBuilder r4 = r9.m3454b(r10)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            byte[] r4 = r4.getBytes()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            byte[] r4 = r9.m3452a(r4)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r3.write(r4)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r4.<init>()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.lang.String r5 = "--bd_map_sdk_cc"
            r4.append(r5)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.lang.String r5 = "\r\n"
            r4.append(r5)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.lang.String r5 = "Content-Disposition: form-data; name=\"file\"; filename=\"c.txt\"\r\n"
            r4.append(r5)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.lang.String r5 = "\r\n"
            r4.append(r5)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            byte[] r4 = r4.getBytes()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            byte[] r4 = r9.m3452a(r4)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r3.write(r4)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            long r5 = r10.length()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            int r6 = (int) r5     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.io.DataInputStream r5 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r6.<init>(r10)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
        L_0x006a:
            int r7 = r5.read(r6)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r8 = -1
            if (r7 == r8) goto L_0x0075
            r4.write(r6, r0, r7)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            goto L_0x006a
        L_0x0075:
            byte[] r6 = r4.toByteArray()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            byte[] r6 = r9.m3452a(r6)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r3.write(r6)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r5.close()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r4.close()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.lang.String r4 = "\r\n--bd_map_sdk_cc--\r\n"
            byte[] r4 = r4.getBytes()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            byte[] r4 = r9.m3452a(r4)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r3.write(r4)     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r3.flush()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            int r4 = r2.getResponseCode()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 != r5) goto L_0x00e9
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ Exception -> 0x0108, all -> 0x0105 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00e7, all -> 0x00e5 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00e7, all -> 0x00e5 }
            r6.<init>(r4)     // Catch:{ Exception -> 0x00e7, all -> 0x00e5 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x00e7, all -> 0x00e5 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r1.<init>()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
        L_0x00b1:
            int r6 = r5.read()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            if (r6 == r8) goto L_0x00bc
            char r6 = (char) r6     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            r1.append(r6)     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            goto L_0x00b1
        L_0x00bc:
            java.lang.String r0 = r1.toString()     // Catch:{ Exception -> 0x00e3, all -> 0x00e0 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00de, all -> 0x00e0 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00de, all -> 0x00e0 }
            java.lang.String r0 = "status"
            boolean r0 = r1.has(r0)     // Catch:{ Exception -> 0x00de, all -> 0x00e0 }
            if (r0 == 0) goto L_0x00de
            java.lang.String r0 = "status"
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x00de, all -> 0x00e0 }
            if (r0 != 0) goto L_0x00de
            boolean r0 = r10.exists()     // Catch:{ Exception -> 0x00de, all -> 0x00e0 }
            if (r0 == 0) goto L_0x00de
            r10.delete()     // Catch:{ Exception -> 0x00de, all -> 0x00e0 }
        L_0x00de:
            r1 = r5
            goto L_0x00ea
        L_0x00e0:
            r10 = move-exception
            r1 = r5
            goto L_0x0113
        L_0x00e3:
            r1 = r5
            goto L_0x012f
        L_0x00e5:
            r10 = move-exception
            goto L_0x0113
        L_0x00e7:
            goto L_0x012f
        L_0x00e9:
            r4 = r1
        L_0x00ea:
            if (r3 == 0) goto L_0x00f1
            r3.close()     // Catch:{ Exception -> 0x00f0 }
            goto L_0x00f1
        L_0x00f0:
        L_0x00f1:
            if (r4 == 0) goto L_0x00fd
            if (r1 == 0) goto L_0x00fd
            r4.close()     // Catch:{ Exception -> 0x00fc }
            r1.close()     // Catch:{ Exception -> 0x00fc }
            goto L_0x00fd
        L_0x00fc:
        L_0x00fd:
            if (r2 == 0) goto L_0x0102
            r2.disconnect()     // Catch:{ Exception -> 0x0102 }
        L_0x0102:
            r10 = 1
            monitor-exit(r9)
            return r10
        L_0x0105:
            r10 = move-exception
            r4 = r1
            goto L_0x0113
        L_0x0108:
            r4 = r1
            goto L_0x012f
        L_0x010a:
            r10 = move-exception
            r3 = r1
            goto L_0x0112
        L_0x010d:
            r3 = r1
            goto L_0x012e
        L_0x010f:
            r10 = move-exception
            r2 = r1
            r3 = r2
        L_0x0112:
            r4 = r3
        L_0x0113:
            if (r3 == 0) goto L_0x011a
            r3.close()     // Catch:{ Exception -> 0x0119 }
            goto L_0x011a
        L_0x0119:
        L_0x011a:
            if (r4 == 0) goto L_0x0126
            if (r1 == 0) goto L_0x0126
            r4.close()     // Catch:{ Exception -> 0x0125 }
            r1.close()     // Catch:{ Exception -> 0x0125 }
            goto L_0x0126
        L_0x0125:
        L_0x0126:
            if (r2 == 0) goto L_0x012b
            r2.disconnect()     // Catch:{ Exception -> 0x012b }
        L_0x012b:
            throw r10     // Catch:{ all -> 0x0135 }
        L_0x012c:
            r2 = r1
            r3 = r2
        L_0x012e:
            r4 = r3
        L_0x012f:
            if (r3 == 0) goto L_0x0138
            r3.close()     // Catch:{ Exception -> 0x0137 }
            goto L_0x0138
        L_0x0135:
            r10 = move-exception
            goto L_0x014a
        L_0x0137:
        L_0x0138:
            if (r4 == 0) goto L_0x0144
            if (r1 == 0) goto L_0x0144
            r4.close()     // Catch:{ Exception -> 0x0143 }
            r1.close()     // Catch:{ Exception -> 0x0143 }
            goto L_0x0144
        L_0x0143:
        L_0x0144:
            if (r2 == 0) goto L_0x014c
            r2.disconnect()     // Catch:{ Exception -> 0x014c }
            goto L_0x014c
        L_0x014a:
            monitor-exit(r9)
            throw r10
        L_0x014c:
            monitor-exit(r9)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.p024b.p025a.C1048c.m3451a(java.io.File):boolean");
    }

    /* renamed from: a */
    private byte[] m3452a(byte[] bArr) throws Exception {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length);
        m3448a(byteArrayInputStream, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.flush();
        byteArrayOutputStream.close();
        byteArrayInputStream.close();
        return byteArray;
    }

    /* renamed from: b */
    private StringBuilder m3454b(File file) {
        String[] split = file.getName().substring(0, file.getName().length() - 4).split("_");
        StringBuilder sb = new StringBuilder();
        sb.append("--bd_map_sdk_cc");
        sb.append("\r\n");
        sb.append("Content-Disposition: form-data; name=\"phoneinfo\"\r\n");
        sb.append("\r\n");
        sb.append(URLDecoder.decode(SyncSysInfo.getPhoneInfo() + "&abi=" + f3363c));
        sb.append("\r\n");
        sb.append("--bd_map_sdk_cc");
        sb.append("\r\n");
        if (split[0] != null && !split[0].isEmpty()) {
            sb.append("Content-Disposition: form-data; name=\"packname\"\r\n");
            sb.append("\r\n");
            sb.append(split[0]);
            sb.append("\r\n");
            sb.append("--bd_map_sdk_cc");
            sb.append("\r\n");
        }
        if (split[1] != null && !split[1].isEmpty()) {
            sb.append("Content-Disposition: form-data; name=\"version\"\r\n");
            sb.append("\r\n");
            sb.append(split[1]);
            sb.append("\r\n");
            sb.append("--bd_map_sdk_cc");
            sb.append("\r\n");
        }
        if (split[2] != null && !split[2].isEmpty()) {
            sb.append("Content-Disposition: form-data; name=\"timestamp\"\r\n");
            sb.append("\r\n");
            sb.append(split[2]);
            sb.append("\r\n");
            sb.append("--bd_map_sdk_cc");
            sb.append("\r\n");
        }
        sb.append("Content-Disposition: form-data; name=\"os\"\r\n");
        sb.append("\r\n");
        sb.append("android");
        sb.append("\r\n");
        sb.append("--bd_map_sdk_cc");
        sb.append("\r\n");
        return sb;
    }

    /* renamed from: d */
    private void m3456d() {
        if (C1174h.m4256a().mo13403b() != null) {
            String b = C1174h.m4256a().mo13403b().mo13397b();
            if (!b.isEmpty()) {
                String str = b + File.separator + "crash";
                File file = new File(str);
                if (!file.exists() && !file.mkdir()) {
                    f3361a = b;
                } else {
                    f3361a = str;
                }
            }
        }
    }

    /* renamed from: e */
    private void m3457e() {
        String str;
        String str2 = f3361a;
        if (str2 != null && !str2.isEmpty() && (str = f3362b) != null && !str.isEmpty()) {
            String str3 = f3361a + File.separator + f3362b;
            C1045a.m3442a().mo12921a(str3);
            JNIHandler.registerNativeHandler(str3);
        }
    }

    /* renamed from: f */
    private void m3458f() {
        if (NetworkUtil.isNetworkAvailable(this.f3364d)) {
            new Thread(new C1050d(this)).start();
        }
    }

    /* renamed from: g */
    private HttpURLConnection m3459g() {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("https://api.map.baidu.com/lbs_sdkcc/report").openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty(HttpHeaders.CONNECTION, "keep-alive");
            httpURLConnection.setRequestProperty(HttpHeaders.CONTENT_TYPE, "multipart/form-data; boundary=bd_map_sdk_cc");
            httpURLConnection.setRequestProperty(HttpHeaders.CACHE_CONTROL, "no-cache");
            httpURLConnection.setRequestProperty(HttpHeaders.CONTENT_ENCODING, MimeType.GZIP);
            httpURLConnection.setConnectTimeout(10000);
            return httpURLConnection;
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: a */
    public void mo12923a(Context context) {
        if (Build.VERSION.SDK_INT >= 21) {
            if (Build.SUPPORTED_ABIS.length > 0) {
                f3363c = Build.SUPPORTED_ABIS[0];
            }
            this.f3364d = context;
            String n = C1175i.m4283n();
            if (!n.isEmpty()) {
                if (n.contains("_")) {
                    n = n.replaceAll("_", "");
                }
                f3362b = n + "_" + C1175i.m4278i() + "_";
                m3456d();
                m3457e();
                m3458f();
            }
        }
    }
}
