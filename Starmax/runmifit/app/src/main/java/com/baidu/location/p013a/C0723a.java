package com.baidu.location.p013a;

import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import com.baidu.location.Address;
import com.baidu.location.BDLocation;
import com.baidu.location.C0839f;
import com.baidu.location.Jni;
import com.baidu.location.LocationClientOption;
import com.baidu.location.Poi;
import com.baidu.location.indoor.C0865g;
import com.baidu.location.p017e.C0826e;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0855k;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: com.baidu.location.a.a */
public class C0723a {

    /* renamed from: c */
    public static long f1203c = 0;

    /* renamed from: d */
    public static int f1204d = -1;

    /* renamed from: f */
    private static C0723a f1205f;

    /* renamed from: a */
    public boolean f1206a;

    /* renamed from: b */
    boolean f1207b;

    /* renamed from: e */
    int f1208e;

    /* renamed from: g */
    private ArrayList<C0724a> f1209g;

    /* renamed from: h */
    private boolean f1210h;

    /* renamed from: i */
    private BDLocation f1211i;

    /* renamed from: j */
    private BDLocation f1212j;

    /* renamed from: k */
    private BDLocation f1213k;

    /* renamed from: l */
    private boolean f1214l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public boolean f1215m;

    /* renamed from: n */
    private C0725b f1216n;

    /* renamed from: com.baidu.location.a.a$a */
    private class C0724a {

        /* renamed from: a */
        public String f1217a = null;

        /* renamed from: b */
        public Messenger f1218b = null;

        /* renamed from: c */
        public LocationClientOption f1219c = new LocationClientOption();

        /* renamed from: d */
        public int f1220d;

        public C0724a(Message message) {
            boolean z = false;
            this.f1220d = 0;
            this.f1218b = message.replyTo;
            this.f1217a = message.getData().getString("packName");
            this.f1219c.prodName = message.getData().getString("prodName");
            C0844b.m2417a().mo10716a(this.f1219c.prodName, this.f1217a);
            this.f1219c.coorType = message.getData().getString("coorType");
            this.f1219c.addrType = message.getData().getString("addrType");
            this.f1219c.enableSimulateGps = message.getData().getBoolean("enableSimulateGps", false);
            C0855k.f1969m = C0855k.f1969m || this.f1219c.enableSimulateGps;
            if (!C0855k.f1963g.equals("all")) {
                C0855k.f1963g = this.f1219c.addrType;
            }
            this.f1219c.openGps = message.getData().getBoolean("openGPS");
            this.f1219c.scanSpan = message.getData().getInt("scanSpan");
            this.f1219c.timeOut = message.getData().getInt("timeOut");
            this.f1219c.priority = message.getData().getInt("priority");
            this.f1219c.location_change_notify = message.getData().getBoolean("location_change_notify");
            this.f1219c.mIsNeedDeviceDirect = message.getData().getBoolean("needDirect", false);
            this.f1219c.isNeedAltitude = message.getData().getBoolean("isneedaltitude", false);
            this.f1219c.isNeedNewVersionRgc = message.getData().getBoolean("isneednewrgc", false);
            C0855k.f1965i = C0855k.f1965i || this.f1219c.isNeedNewVersionRgc;
            C0855k.f1964h = C0855k.f1964h || message.getData().getBoolean("isneedaptag", false);
            C0855k.f1966j = C0855k.f1966j || message.getData().getBoolean("isneedaptagd", false);
            C0855k.f1918S = message.getData().getFloat("autoNotifyLocSensitivity", 0.5f);
            int i = message.getData().getInt("wifitimeout", Integer.MAX_VALUE);
            if (i < C0855k.f1938ag) {
                C0855k.f1938ag = i;
            }
            int i2 = message.getData().getInt("autoNotifyMaxInterval", 0);
            if (i2 >= C0855k.f1923X) {
                C0855k.f1923X = i2;
            }
            int i3 = message.getData().getInt("autoNotifyMinDistance", 0);
            if (i3 >= C0855k.f1925Z) {
                C0855k.f1925Z = i3;
            }
            int i4 = message.getData().getInt("autoNotifyMinTimeInterval", 0);
            if (i4 >= C0855k.f1924Y) {
                C0855k.f1924Y = i4;
            }
            if (this.f1219c.mIsNeedDeviceDirect || this.f1219c.isNeedAltitude) {
                C0754n.m1911a().mo10491a(this.f1219c.mIsNeedDeviceDirect);
                C0754n.m1911a().mo10492b();
            }
            C0723a.this.f1207b = (C0723a.this.f1207b || this.f1219c.isNeedAltitude) ? true : z;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1750a(int i) {
            Message obtain = Message.obtain((Handler) null, i);
            try {
                if (this.f1218b != null) {
                    this.f1218b.send(obtain);
                }
                this.f1220d = 0;
            } catch (Exception e) {
                if (e instanceof DeadObjectException) {
                    this.f1220d++;
                }
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m1751a(int i, Bundle bundle) {
            Message obtain = Message.obtain((Handler) null, i);
            obtain.setData(bundle);
            try {
                if (this.f1218b != null) {
                    this.f1218b.send(obtain);
                }
                this.f1220d = 0;
            } catch (Exception e) {
                if (e instanceof DeadObjectException) {
                    this.f1220d++;
                }
                e.printStackTrace();
            }
        }

        /* renamed from: a */
        private void m1752a(int i, String str, BDLocation bDLocation) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(str, bDLocation);
            bundle.setClassLoader(BDLocation.class.getClassLoader());
            Message obtain = Message.obtain((Handler) null, i);
            obtain.setData(bundle);
            try {
                if (this.f1218b != null) {
                    this.f1218b.send(obtain);
                }
                this.f1220d = 0;
            } catch (Exception e) {
                if (e instanceof DeadObjectException) {
                    this.f1220d++;
                }
            }
        }

        /* renamed from: a */
        public void mo10435a() {
            m1750a(111);
        }

        /* renamed from: a */
        public void mo10436a(BDLocation bDLocation) {
            mo10437a(bDLocation, 21);
        }

        /* renamed from: a */
        public void mo10437a(BDLocation bDLocation, int i) {
            String str;
            BDLocation bDLocation2 = new BDLocation(bDLocation);
            if (C0865g.m2519a().mo10768e()) {
                bDLocation2.setIndoorLocMode(true);
            }
            if (i == 21) {
                m1752a(27, "locStr", bDLocation2);
            }
            if (this.f1219c.coorType != null && !this.f1219c.coorType.equals(CoordinateType.GCJ02)) {
                double longitude = bDLocation2.getLongitude();
                double latitude = bDLocation2.getLatitude();
                if (!(longitude == Double.MIN_VALUE || latitude == Double.MIN_VALUE)) {
                    if ((bDLocation2.getCoorType() != null && bDLocation2.getCoorType().equals(CoordinateType.GCJ02)) || bDLocation2.getCoorType() == null) {
                        double[] coorEncrypt = Jni.coorEncrypt(longitude, latitude, this.f1219c.coorType);
                        bDLocation2.setLongitude(coorEncrypt[0]);
                        bDLocation2.setLatitude(coorEncrypt[1]);
                        str = this.f1219c.coorType;
                    } else if (bDLocation2.getCoorType() != null && bDLocation2.getCoorType().equals(CoordinateType.WGS84)) {
                        double[] coorEncrypt2 = Jni.coorEncrypt(longitude, latitude, this.f1219c.coorType);
                        bDLocation2.setLongitude(coorEncrypt2[0]);
                        bDLocation2.setLatitude(coorEncrypt2[1]);
                        if (!this.f1219c.coorType.equals("bd09ll")) {
                            str = "wgs84mc";
                        }
                    }
                    bDLocation2.setCoorType(str);
                }
            }
            m1752a(i, "locStr", bDLocation2);
        }

        /* renamed from: b */
        public void mo10438b() {
            if (this.f1219c.location_change_notify) {
                m1750a(C0855k.f1958b ? 54 : 55);
            }
        }
    }

    /* renamed from: com.baidu.location.a.a$b */
    private class C0725b implements Runnable {

        /* renamed from: a */
        final /* synthetic */ C0723a f1222a;

        /* renamed from: b */
        private int f1223b;

        /* renamed from: c */
        private boolean f1224c;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.a.a.a(com.baidu.location.a.a, boolean):boolean
         arg types: [com.baidu.location.a.a, int]
         candidates:
          com.baidu.location.a.a.a(android.os.Bundle, int):void
          com.baidu.location.a.a.a(com.baidu.location.a.a, boolean):boolean */
        public void run() {
            if (!this.f1224c) {
                this.f1223b++;
                boolean unused = this.f1222a.f1215m = false;
            }
        }
    }

    private C0723a() {
        this.f1209g = null;
        this.f1210h = false;
        this.f1206a = false;
        this.f1207b = false;
        this.f1211i = null;
        this.f1212j = null;
        this.f1208e = 0;
        this.f1213k = null;
        this.f1214l = false;
        this.f1215m = false;
        this.f1216n = null;
        this.f1209g = new ArrayList<>();
    }

    /* renamed from: a */
    private C0724a m1728a(Messenger messenger) {
        ArrayList<C0724a> arrayList = this.f1209g;
        if (arrayList == null) {
            return null;
        }
        Iterator<C0724a> it = arrayList.iterator();
        while (it.hasNext()) {
            C0724a next = it.next();
            if (next.f1218b.equals(messenger)) {
                return next;
            }
        }
        return null;
    }

    /* renamed from: a */
    public static C0723a m1729a() {
        if (f1205f == null) {
            f1205f = new C0723a();
        }
        return f1205f;
    }

    /* renamed from: a */
    private void m1730a(C0724a aVar) {
        int i;
        if (aVar != null) {
            if (m1728a(aVar.f1218b) != null) {
                i = 14;
            } else {
                this.f1209g.add(aVar);
                i = 13;
            }
            aVar.m1750a(i);
        }
    }

    /* renamed from: b */
    private void m1732b(String str) {
        Intent intent = new Intent("com.baidu.location.flp.log");
        intent.setPackage("com.baidu.baidulocationdemo");
        intent.putExtra("data", str);
        intent.putExtra("pack", C0844b.f1848e);
        intent.putExtra("tag", "state");
        C0839f.getServiceContext().sendBroadcast(intent);
    }

    /* renamed from: f */
    private void m1733f() {
        m1734g();
        mo10434e();
    }

    /* renamed from: g */
    private void m1734g() {
        Iterator<C0724a> it = this.f1209g.iterator();
        boolean z = false;
        boolean z2 = false;
        while (it.hasNext()) {
            C0724a next = it.next();
            if (next.f1219c.openGps) {
                z2 = true;
            }
            if (next.f1219c.location_change_notify) {
                z = true;
            }
        }
        C0855k.f1926a = z;
        if (this.f1210h != z2) {
            this.f1210h = z2;
            C0826e.m2306a().mo10638a(this.f1210h);
        }
    }

    /* renamed from: a */
    public void mo10420a(Bundle bundle, int i) {
        Iterator<C0724a> it = this.f1209g.iterator();
        while (it.hasNext()) {
            try {
                C0724a next = it.next();
                next.m1751a(i, bundle);
                if (next.f1220d > 4) {
                    it.remove();
                }
            } catch (Exception unused) {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo10421a(Message message) {
        if (message != null && message.replyTo != null) {
            f1203c = System.currentTimeMillis();
            this.f1206a = true;
            C0835i.m2376a().mo10686b();
            m1730a(new C0724a(message));
            m1733f();
            if (this.f1214l) {
                m1732b("start");
                this.f1208e = 0;
            }
        }
    }

    /* renamed from: a */
    public void mo10422a(BDLocation bDLocation) {
        mo10427b(bDLocation);
    }

    /* renamed from: a */
    public void mo10423a(String str) {
        mo10429c(new BDLocation(str));
    }

    /* renamed from: a */
    public void mo10424a(boolean z) {
        this.f1206a = z;
        f1204d = z ? 1 : 0;
    }

    /* renamed from: b */
    public void mo10425b() {
        this.f1209g.clear();
        this.f1211i = null;
        m1733f();
    }

    /* renamed from: b */
    public void mo10426b(Message message) {
        C0724a a = m1728a(message.replyTo);
        if (a != null) {
            this.f1209g.remove(a);
        }
        C0754n.m1911a().mo10494c();
        m1733f();
        if (this.f1214l) {
            m1732b("stop");
            this.f1208e = 0;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:10:0x0032=Splitter:B:10:0x0032, B:32:0x009b=Splitter:B:32:0x009b} */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10427b(com.baidu.location.BDLocation r14) {
        /*
            r13 = this;
            r0 = 4
            r1 = 66
            r2 = 61
            r3 = 0
            r4 = 161(0xa1, float:2.26E-43)
            if (r14 == 0) goto L_0x004d
            int r5 = r14.getLocType()
            if (r5 != r4) goto L_0x004d
            com.baidu.location.a.j r5 = com.baidu.location.p013a.C0741j.m1844a()
            boolean r5 = r5.mo10467b()
            if (r5 != 0) goto L_0x004d
            com.baidu.location.BDLocation r5 = r13.f1212j
            if (r5 != 0) goto L_0x002c
            com.baidu.location.BDLocation r5 = new com.baidu.location.BDLocation
            r5.<init>()
            r13.f1212j = r5
            com.baidu.location.BDLocation r5 = r13.f1212j
            r6 = 505(0x1f9, float:7.08E-43)
            r5.setLocType(r6)
        L_0x002c:
            java.util.ArrayList<com.baidu.location.a.a$a> r5 = r13.f1209g
            java.util.Iterator r5 = r5.iterator()
        L_0x0032:
            boolean r6 = r5.hasNext()     // Catch:{ Exception -> 0x004b }
            if (r6 == 0) goto L_0x00b2
            java.lang.Object r6 = r5.next()     // Catch:{ Exception -> 0x004b }
            com.baidu.location.a.a$a r6 = (com.baidu.location.p013a.C0723a.C0724a) r6     // Catch:{ Exception -> 0x004b }
            com.baidu.location.BDLocation r7 = r13.f1212j     // Catch:{ Exception -> 0x004b }
            r6.mo10436a(r7)     // Catch:{ Exception -> 0x004b }
            int r6 = r6.f1220d     // Catch:{ Exception -> 0x004b }
            if (r6 <= r0) goto L_0x0032
            r5.remove()     // Catch:{ Exception -> 0x004b }
            goto L_0x0032
        L_0x004b:
            goto L_0x00b2
        L_0x004d:
            boolean r5 = r14.hasAltitude()
            if (r5 != 0) goto L_0x0084
            boolean r5 = r13.f1207b
            if (r5 == 0) goto L_0x0084
            int r5 = r14.getLocType()
            if (r5 == r4) goto L_0x0063
            int r5 = r14.getLocType()
            if (r5 != r1) goto L_0x0084
        L_0x0063:
            com.baidu.location.b.a r5 = com.baidu.location.p014b.C0770a.m1980a()
            double r6 = r14.getLongitude()
            double r8 = r14.getLatitude()
            double[] r5 = r5.mo10519a(r6, r8)
            r6 = r5[r3]
            com.baidu.location.p014b.C0770a.m1980a()
            r8 = 4666722622711529472(0x40c3878000000000, double:9999.0)
            int r5 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r5 >= 0) goto L_0x0084
            r14.setAltitude(r6)
        L_0x0084:
            int r5 = r14.getLocType()
            if (r5 != r2) goto L_0x0095
            com.baidu.location.b.a r5 = com.baidu.location.p014b.C0770a.m1980a()
            int r5 = r5.mo10518a(r14)
            r14.setGpsAccuracyStatus(r5)
        L_0x0095:
            java.util.ArrayList<com.baidu.location.a.a$a> r5 = r13.f1209g
            java.util.Iterator r5 = r5.iterator()
        L_0x009b:
            boolean r6 = r5.hasNext()     // Catch:{ Exception -> 0x004b }
            if (r6 == 0) goto L_0x00b2
            java.lang.Object r6 = r5.next()     // Catch:{ Exception -> 0x004b }
            com.baidu.location.a.a$a r6 = (com.baidu.location.p013a.C0723a.C0724a) r6     // Catch:{ Exception -> 0x004b }
            r6.mo10436a(r14)     // Catch:{ Exception -> 0x004b }
            int r6 = r6.f1220d     // Catch:{ Exception -> 0x004b }
            if (r6 <= r0) goto L_0x009b
            r5.remove()     // Catch:{ Exception -> 0x004b }
            goto L_0x009b
        L_0x00b2:
            boolean r0 = com.baidu.location.p013a.C0750l.f1367g
            if (r0 == 0) goto L_0x00b8
            com.baidu.location.p013a.C0750l.f1367g = r3
        L_0x00b8:
            int r5 = com.baidu.location.p019g.C0855k.f1923X
            r6 = 10000(0x2710, float:1.4013E-41)
            if (r5 < r6) goto L_0x0109
            int r5 = r14.getLocType()
            if (r5 == r2) goto L_0x00d0
            int r2 = r14.getLocType()
            if (r2 == r4) goto L_0x00d0
            int r2 = r14.getLocType()
            if (r2 != r1) goto L_0x0109
        L_0x00d0:
            com.baidu.location.BDLocation r1 = r13.f1211i
            if (r1 == 0) goto L_0x0102
            r2 = 1
            float[] r2 = new float[r2]
            double r4 = r1.getLatitude()
            com.baidu.location.BDLocation r1 = r13.f1211i
            double r6 = r1.getLongitude()
            double r8 = r14.getLatitude()
            double r10 = r14.getLongitude()
            r12 = r2
            android.location.Location.distanceBetween(r4, r6, r8, r10, r12)
            r1 = r2[r3]
            int r2 = com.baidu.location.p019g.C0855k.f1925Z
            float r2 = (float) r2
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 > 0) goto L_0x00f9
            if (r0 != 0) goto L_0x00f9
            return
        L_0x00f9:
            r0 = 0
            r13.f1211i = r0
            com.baidu.location.BDLocation r0 = new com.baidu.location.BDLocation
            r0.<init>(r14)
            goto L_0x0107
        L_0x0102:
            com.baidu.location.BDLocation r0 = new com.baidu.location.BDLocation
            r0.<init>(r14)
        L_0x0107:
            r13.f1211i = r0
        L_0x0109:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0723a.mo10427b(com.baidu.location.BDLocation):void");
    }

    /* renamed from: c */
    public void mo10428c() {
        Iterator<C0724a> it = this.f1209g.iterator();
        while (it.hasNext()) {
            it.next().mo10435a();
        }
    }

    /* renamed from: c */
    public void mo10429c(BDLocation bDLocation) {
        Address a = C0750l.m1883c().mo10476a(bDLocation);
        String f = C0750l.m1883c().mo10483f();
        List<Poi> g = C0750l.m1883c().mo10484g();
        if (a != null) {
            bDLocation.setAddr(a);
        }
        if (f != null) {
            bDLocation.setLocationDescribe(f);
        }
        if (g != null) {
            bDLocation.setPoiList(g);
        }
        if (C0865g.m2519a().mo10769f() && C0865g.m2519a().mo10770g() != null) {
            bDLocation.setFloor(C0865g.m2519a().mo10770g());
            bDLocation.setIndoorLocMode(true);
            if (C0865g.m2519a().mo10771h() != null) {
                bDLocation.setBuildingID(C0865g.m2519a().mo10771h());
            }
        }
        mo10422a(bDLocation);
        C0750l.m1883c().mo10480c(bDLocation);
    }

    /* renamed from: c */
    public boolean mo10430c(Message message) {
        C0724a a = m1728a(message.replyTo);
        boolean z = false;
        if (a == null) {
            return false;
        }
        int i = a.f1219c.scanSpan;
        a.f1219c.scanSpan = message.getData().getInt("scanSpan", a.f1219c.scanSpan);
        if (a.f1219c.scanSpan < 1000) {
            C0754n.m1911a().mo10494c();
            this.f1206a = false;
        } else {
            this.f1206a = true;
        }
        if (a.f1219c.scanSpan > 999 && i < 1000) {
            if (a.f1219c.mIsNeedDeviceDirect || a.f1219c.isNeedAltitude) {
                C0754n.m1911a().mo10491a(a.f1219c.mIsNeedDeviceDirect);
                C0754n.m1911a().mo10492b();
            }
            if (this.f1207b || a.f1219c.isNeedAltitude) {
                z = true;
            }
            this.f1207b = z;
            z = true;
        }
        a.f1219c.openGps = message.getData().getBoolean("openGPS", a.f1219c.openGps);
        String string = message.getData().getString("coorType");
        LocationClientOption locationClientOption = a.f1219c;
        if (string == null || string.equals("")) {
            string = a.f1219c.coorType;
        }
        locationClientOption.coorType = string;
        String string2 = message.getData().getString("addrType");
        LocationClientOption locationClientOption2 = a.f1219c;
        if (string2 == null || string2.equals("")) {
            string2 = a.f1219c.addrType;
        }
        locationClientOption2.addrType = string2;
        if (!C0855k.f1963g.equals(a.f1219c.addrType)) {
            C0750l.m1883c().mo10487j();
        }
        a.f1219c.timeOut = message.getData().getInt("timeOut", a.f1219c.timeOut);
        a.f1219c.location_change_notify = message.getData().getBoolean("location_change_notify", a.f1219c.location_change_notify);
        a.f1219c.priority = message.getData().getInt("priority", a.f1219c.priority);
        int i2 = message.getData().getInt("wifitimeout", Integer.MAX_VALUE);
        if (i2 < C0855k.f1938ag) {
            C0855k.f1938ag = i2;
        }
        m1733f();
        return z;
    }

    /* renamed from: d */
    public int mo10431d(Message message) {
        C0724a a;
        if (message == null || message.replyTo == null || (a = m1728a(message.replyTo)) == null || a.f1219c == null) {
            return 1;
        }
        return a.f1219c.priority;
    }

    /* renamed from: d */
    public String mo10432d() {
        StringBuffer stringBuffer = new StringBuffer(256);
        if (this.f1209g.isEmpty()) {
            return "&prod=" + C0844b.f1849f + Config.TRACE_TODAY_VISIT_SPLIT + C0844b.f1848e;
        }
        C0724a aVar = this.f1209g.get(0);
        if (aVar.f1219c.prodName != null) {
            stringBuffer.append(aVar.f1219c.prodName);
        }
        if (aVar.f1217a != null) {
            stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
            stringBuffer.append(aVar.f1217a);
            stringBuffer.append("|");
        }
        String stringBuffer2 = stringBuffer.toString();
        if (stringBuffer2 == null || stringBuffer2.equals("")) {
            return null;
        }
        return "&prod=" + stringBuffer2;
    }

    /* renamed from: e */
    public int mo10433e(Message message) {
        C0724a a;
        if (message == null || message.replyTo == null || (a = m1728a(message.replyTo)) == null || a.f1219c == null) {
            return 1000;
        }
        return a.f1219c.scanSpan;
    }

    /* renamed from: e */
    public void mo10434e() {
        Iterator<C0724a> it = this.f1209g.iterator();
        while (it.hasNext()) {
            it.next().mo10438b();
        }
    }
}
