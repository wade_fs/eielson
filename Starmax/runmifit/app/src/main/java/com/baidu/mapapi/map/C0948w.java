package com.baidu.mapapi.map;

import android.util.Log;

/* renamed from: com.baidu.mapapi.map.w */
class C0948w implements Runnable {

    /* renamed from: a */
    final /* synthetic */ int f2873a;

    /* renamed from: b */
    final /* synthetic */ int f2874b;

    /* renamed from: c */
    final /* synthetic */ int f2875c;

    /* renamed from: d */
    final /* synthetic */ String f2876d;

    /* renamed from: e */
    final /* synthetic */ TileOverlay f2877e;

    C0948w(TileOverlay tileOverlay, int i, int i2, int i3, String str) {
        this.f2877e = tileOverlay;
        this.f2873a = i;
        this.f2874b = i2;
        this.f2875c = i3;
        this.f2876d = str;
    }

    public void run() {
        String str;
        String str2;
        Tile tile = ((FileTileProvider) this.f2877e.f2782g).getTile(this.f2873a, this.f2874b, this.f2875c);
        if (tile == null) {
            str2 = TileOverlay.f2776b;
            str = "FileTile pic is null";
        } else if (tile.width == 256 && tile.height == 256) {
            this.f2877e.m2982a(this.f2873a + "_" + this.f2874b + "_" + this.f2875c, tile);
            this.f2877e.f2781e.remove(this.f2876d);
        } else {
            str2 = TileOverlay.f2776b;
            str = "FileTile pic must be 256 * 256";
        }
        Log.e(str2, str);
        this.f2877e.f2781e.remove(this.f2876d);
    }
}
