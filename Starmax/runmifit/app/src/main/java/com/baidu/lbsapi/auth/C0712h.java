package com.baidu.lbsapi.auth;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

/* renamed from: com.baidu.lbsapi.auth.h */
class C0712h implements HostnameVerifier {

    /* renamed from: a */
    final /* synthetic */ C0711g f1091a;

    C0712h(C0711g gVar) {
        this.f1091a = gVar;
    }

    public boolean verify(String str, SSLSession sSLSession) {
        if ("api.map.baidu.com".equals(str)) {
            return true;
        }
        return HttpsURLConnection.getDefaultHostnameVerifier().verify(str, sSLSession);
    }
}
