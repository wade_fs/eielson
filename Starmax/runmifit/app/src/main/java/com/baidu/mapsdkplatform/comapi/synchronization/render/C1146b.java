package com.baidu.mapsdkplatform.comapi.synchronization.render;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.map.WinRound;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.synchronization.DisplayOptions;
import com.baidu.mapapi.synchronization.RoleOptions;
import com.baidu.mapapi.synchronization.SynchronizationConstants;
import com.baidu.mapsdkplatform.comapi.synchronization.data.RouteLineInfo;
import com.baidu.mapsdkplatform.comapi.synchronization.data.SyncResponseResult;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1121b;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.b */
public class C1146b extends Handler {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final String f3784a = C1146b.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static RoleOptions f3785d = null;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static DisplayOptions f3786e = null;

    /* renamed from: f */
    private static Marker f3787f = null;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static volatile SyncResponseResult f3788g = null;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public static int f3789h = 1000;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public static volatile int f3790p = 0;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public static LatLng f3791r = null;

    /* renamed from: A */
    private boolean f3792A = false;

    /* renamed from: B */
    private Thread f3793B = new Thread(new C1147a(), "Adjust visible span");
    /* access modifiers changed from: private */

    /* renamed from: C */
    public boolean f3794C = true;
    /* access modifiers changed from: private */

    /* renamed from: D */
    public int f3795D = 10;
    /* access modifiers changed from: private */

    /* renamed from: E */
    public int f3796E = 10;
    /* access modifiers changed from: private */

    /* renamed from: F */
    public LatLngBounds f3797F = null;

    /* renamed from: G */
    private C1151e f3798G;
    /* access modifiers changed from: private */

    /* renamed from: H */
    public volatile long f3799H = 10000;
    /* access modifiers changed from: private */

    /* renamed from: I */
    public boolean f3800I = true;
    /* access modifiers changed from: private */

    /* renamed from: J */
    public volatile boolean f3801J = false;
    /* access modifiers changed from: private */

    /* renamed from: K */
    public volatile long f3802K = 0;
    /* access modifiers changed from: private */

    /* renamed from: L */
    public volatile int f3803L = 0;

    /* renamed from: M */
    private List<LatLng> f3804M = new CopyOnWriteArrayList();

    /* renamed from: N */
    private List<BitmapDescriptor> f3805N = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */

    /* renamed from: O */
    public Polyline f3806O = null;

    /* renamed from: P */
    private List<Integer> f3807P = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public volatile boolean f3808Q = true;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public int f3809R = 5;

    /* renamed from: S */
    private String f3810S = null;

    /* renamed from: T */
    private String f3811T = null;

    /* renamed from: U */
    private boolean f3812U = true;

    /* renamed from: V */
    private boolean f3813V = false;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public volatile int f3814W = 0;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public volatile boolean f3815X = false;

    /* renamed from: Y */
    private double f3816Y = 0.0d;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public BaiduMap f3817b;

    /* renamed from: c */
    private C1153d f3818c;

    /* renamed from: i */
    private Marker f3819i = null;

    /* renamed from: j */
    private Marker f3820j = null;

    /* renamed from: k */
    private Marker f3821k = null;

    /* renamed from: l */
    private Marker f3822l = null;

    /* renamed from: m */
    private Marker f3823m = null;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public Marker f3824n = null;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public List<LinkPointPolyLineInfo> f3825o = new CopyOnWriteArrayList();

    /* renamed from: q */
    private Thread f3826q = new Thread(new C1148b(), "Car moving");
    /* access modifiers changed from: private */

    /* renamed from: s */
    public int f3827s = 0;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public double f3828t = 0.0d;

    /* renamed from: u */
    private int f3829u = 5;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public boolean f3830v = false;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public volatile boolean f3831w = false;

    /* renamed from: x */
    private Thread f3832x = new Thread(new C1149c(), "Passenger marker");
    /* access modifiers changed from: private */

    /* renamed from: y */
    public boolean f3833y = false;

    /* renamed from: z */
    private boolean f3834z = true;

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.b$a */
    private class C1147a implements Runnable {
        private C1147a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean
         arg types: [com.baidu.mapsdkplatform.comapi.synchronization.render.b, int]
         candidates:
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, long):long
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, int):void
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, com.baidu.mapapi.model.LatLng):void
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapapi.model.LatLng, com.baidu.mapapi.model.LatLng):boolean
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.mapsdkplatform.comapi.synchronization.render.b.c(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean
         arg types: [com.baidu.mapsdkplatform.comapi.synchronization.render.b, int]
         candidates:
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.c(com.baidu.mapsdkplatform.comapi.synchronization.render.b, int):int
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.c(com.baidu.mapsdkplatform.comapi.synchronization.render.b, com.baidu.mapapi.model.LatLng):void
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.c(com.baidu.mapapi.model.LatLng, com.baidu.mapapi.model.LatLng):boolean
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.c(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, long):long
         arg types: [com.baidu.mapsdkplatform.comapi.synchronization.render.b, int]
         candidates:
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, int):void
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, com.baidu.mapapi.model.LatLng):void
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapapi.model.LatLng, com.baidu.mapapi.model.LatLng):boolean
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.b(com.baidu.mapsdkplatform.comapi.synchronization.render.b, long):long */
        public void run() {
            while (!C1146b.this.f3794C) {
                if (!(C1146b.f3788g == null || C1146b.f3788g.mo13227a() == null)) {
                    if (!C1146b.this.f3815X) {
                        C1146b.this.m4078ak();
                        boolean unused = C1146b.this.f3800I = false;
                        C1146b bVar = C1146b.this;
                        bVar.m4058a(bVar.f3797F);
                    }
                    boolean unused2 = C1146b.this.f3815X = false;
                    try {
                        long unused3 = C1146b.this.f3802K = System.currentTimeMillis();
                        if (C1146b.this.f3799H <= 0) {
                            long unused4 = C1146b.this.f3799H = 10000L;
                        }
                        Thread.sleep(C1146b.this.f3799H);
                    } catch (InterruptedException unused5) {
                        C1120a.m3855b(C1146b.f3784a, "Sleep InterruptedException");
                    }
                }
            }
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.b$b */
    private class C1148b implements Runnable {
        private C1148b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean
         arg types: [com.baidu.mapsdkplatform.comapi.synchronization.render.b, int]
         candidates:
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(double, double):double
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(double, com.baidu.mapapi.model.LatLng):double
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, double):double
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(int, java.util.ArrayList<java.lang.Integer>):int
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, int):int
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, long):long
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, com.baidu.mapapi.map.Marker):com.baidu.mapapi.map.Marker
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, com.baidu.mapapi.model.LatLng):com.baidu.mapapi.model.LatLng
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, com.baidu.mapapi.model.LatLngBounds):void
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapapi.model.LatLng, com.baidu.mapapi.model.LatLng):boolean
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, com.baidu.mapapi.map.MyLocationData):boolean
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.a(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean */
        public void run() {
            LatLng j;
            LatLng endPosition;
            while (!C1146b.this.f3808Q) {
                LatLng b = C1146b.this.m4070ac();
                if (b == null) {
                    C1120a.m3855b(C1146b.f3784a, "Driver position is null, return");
                    return;
                }
                LatLng a = C1146b.this.m4054a(b);
                if (a == null) {
                    C1120a.m3855b(C1146b.f3784a, "Driver position not bind to route");
                    C1146b.m4094c(C1146b.this);
                    if (!C1146b.this.f3830v || C1146b.this.f3833y) {
                        String h = C1146b.f3784a;
                        C1120a.m3855b(h, "Driver position not bind to route times = " + C1146b.this.f3814W);
                        if (2 <= C1146b.this.f3814W) {
                            if (2 >= C1146b.f3789h) {
                                j = C1146b.f3791r;
                                endPosition = C1146b.f3785d.getStartPosition();
                            } else if (4 == C1146b.f3789h) {
                                j = C1146b.f3791r;
                                endPosition = C1146b.f3785d.getEndPosition();
                            } else {
                                int unused = C1146b.this.f3814W = 0;
                                return;
                            }
                            double a2 = C1121b.m3857a(j, endPosition);
                            String h2 = C1146b.f3784a;
                            C1120a.m3855b(h2, "Latest driver postion to end position distance = " + a2);
                            if (300.0d >= a2) {
                                if (C1146b.this.f3806O != null) {
                                    C1146b.this.f3806O.remove();
                                }
                                boolean unused2 = C1146b.this.f3808Q = true;
                                C1146b.this.m4073af();
                            }
                            int unused3 = C1146b.this.f3814W = 0;
                        }
                        C1146b.this.m4086b(b);
                        C1146b.this.m4074ag();
                        C1146b.this.m4097c(b);
                        return;
                    }
                    return;
                } else if (C1146b.this.f3827s != 0) {
                    C1146b bVar = C1146b.this;
                    double unused4 = bVar.f3828t = bVar.m4071ad();
                    if (C1146b.this.f3828t > 500.0d) {
                        C1146b.this.m4086b(a);
                        C1146b.this.m4074ag();
                        C1146b.this.m4097c(a);
                        C1146b.this.m4108e(C1146b.f3790p - 1);
                        int unused5 = C1146b.this.f3803L = C1146b.f3790p - 1;
                        return;
                    }
                    if (!(C1146b.f3788g == null || C1146b.f3788g.mo13236c() == null)) {
                        C1146b.f3788g.mo13236c().setPoint(null);
                    }
                    try {
                        C1146b.this.m4072ae();
                    } catch (Exception e) {
                        C1120a.m3852a(C1146b.f3784a, "Catch exception when car moving", e);
                    }
                    if (C1146b.f3790p >= C1146b.this.f3825o.size()) {
                        boolean unused6 = C1146b.this.f3808Q = true;
                        C1146b.this.m4073af();
                    }
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.b$c */
    private class C1149c implements Runnable {
        private C1149c() {
        }

        public void run() {
            BitmapDescriptor passengerIcon = C1146b.f3786e != null ? C1146b.f3786e.getPassengerIcon() : null;
            if (passengerIcon == null) {
                passengerIcon = new DisplayOptions().getPassengerIcon();
            }
            int i = 10;
            if (C1146b.f3786e != null) {
                i = C1146b.f3786e.getPassengerMarkerZIndex();
            }
            while (!C1146b.this.f3831w) {
                MyLocationData locationData = C1146b.this.f3817b.getLocationData();
                if (locationData != null && C1146b.this.m4061a(locationData)) {
                    C1120a.m3855b(C1146b.f3784a, "Get location data success");
                    LatLng latLng = new LatLng(locationData.latitude, locationData.longitude);
                    if (C1146b.this.f3824n == null) {
                        MarkerOptions zIndex = new MarkerOptions().position(latLng).anchor(0.5f, 0.5f).rotate(locationData.direction).icon(passengerIcon).zIndex(i);
                        C1146b bVar = C1146b.this;
                        Marker unused = bVar.f3824n = (Marker) bVar.f3817b.addOverlay(zIndex);
                    } else {
                        C1146b.this.f3824n.setPosition(latLng);
                        C1146b.this.f3824n.setRotate(locationData.direction);
                    }
                }
                try {
                    Thread.sleep((long) (C1146b.this.f3809R * 1000));
                } catch (InterruptedException unused2) {
                    C1120a.m3855b(C1146b.f3784a, "Sleep interrupt");
                }
            }
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.b$d */
    private enum C1150d {
        NO_NEED_RENDER,
        RENDER_NEW_LINE,
        UPDATE_TRAFFIC
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.render.b$e */
    private class C1151e implements BaiduMap.OnSynchronizationListener {

        /* renamed from: b */
        private int f3843b = 1;

        /* renamed from: c */
        private int f3844c = 2;

        /* renamed from: d */
        private int f3845d = 3;

        C1151e() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.mapsdkplatform.comapi.synchronization.render.b.d(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean
         arg types: [com.baidu.mapsdkplatform.comapi.synchronization.render.b, int]
         candidates:
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.d(com.baidu.mapapi.model.LatLng, com.baidu.mapapi.model.LatLng):double
          com.baidu.mapsdkplatform.comapi.synchronization.render.b.d(com.baidu.mapsdkplatform.comapi.synchronization.render.b, boolean):boolean */
        public void onMapStatusChangeReason(int i) {
            if (this.f3843b == i || this.f3844c == i) {
                long currentTimeMillis = System.currentTimeMillis() - C1146b.this.f3802K;
                if (currentTimeMillis <= 0) {
                    C1146b bVar = C1146b.this;
                    long unused = bVar.f3799H = (long) (bVar.f3795D * 1000);
                } else {
                    C1146b bVar2 = C1146b.this;
                    long unused2 = bVar2.f3799H = ((long) (bVar2.f3795D * 1000)) - (C1146b.this.f3799H - currentTimeMillis);
                }
                boolean unused3 = C1146b.this.f3801J = true;
            } else if (this.f3845d == i) {
                C1146b bVar3 = C1146b.this;
                long unused4 = bVar3.f3799H = (long) (bVar3.f3796E * 1000);
            } else {
                String h = C1146b.f3784a;
                C1120a.m3855b(h, "Undefined reason type: " + i);
            }
        }
    }

    C1146b(Looper looper) {
        super(looper);
    }

    /* renamed from: A */
    private void m4020A() {
        Marker marker = this.f3821k;
        if (marker != null) {
            marker.remove();
            this.f3821k = null;
        }
        Marker marker2 = this.f3822l;
        if (marker2 != null) {
            marker2.remove();
            this.f3822l = null;
        }
    }

    /* renamed from: B */
    private void m4021B() {
        Marker marker = this.f3819i;
        if (marker != null) {
            marker.remove();
            this.f3819i = null;
        }
        Marker marker2 = this.f3820j;
        if (marker2 != null) {
            marker2.remove();
            this.f3820j = null;
        }
    }

    /* renamed from: C */
    private void m4022C() {
        Marker marker = this.f3824n;
        if (marker != null) {
            marker.remove();
            this.f3824n = null;
        }
    }

    /* renamed from: D */
    private void m4023D() {
        Marker marker = this.f3823m;
        if (marker != null) {
            marker.remove();
            this.f3823m = null;
        }
        Marker marker2 = f3787f;
        if (marker2 != null) {
            marker2.remove();
            f3787f = null;
        }
    }

    /* renamed from: E */
    private void m4024E() {
        if (this.f3830v && !this.f3792A) {
            Polyline polyline = this.f3806O;
            if (polyline != null) {
                polyline.remove();
                this.f3825o.clear();
                this.f3804M.clear();
                this.f3805N.clear();
                this.f3807P.clear();
                f3788g = null;
            }
            this.f3810S = null;
            this.f3811T = null;
        }
    }

    /* renamed from: F */
    private void m4025F() {
        Marker marker = this.f3820j;
        if (marker != null) {
            marker.remove();
            this.f3820j = null;
        }
    }

    /* renamed from: G */
    private void m4026G() {
        Marker marker = f3787f;
        if (marker != null) {
            marker.remove();
            f3787f = null;
        }
    }

    /* renamed from: H */
    private void m4027H() {
        if (this.f3830v && !this.f3833y) {
            this.f3808Q = true;
        }
    }

    /* renamed from: I */
    private void m4028I() {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null) {
            C1120a.m3851a(f3784a, "renderStartPositionMarker DisplayOptions is null");
        } else if (!displayOptions.isShowStartPositionMarker()) {
            C1120a.m3851a(f3784a, "User set start position marker not show");
            Marker marker = this.f3819i;
            if (marker != null) {
                marker.remove();
                this.f3819i = null;
            }
        } else if (this.f3819i == null || this.f3830v) {
            LatLng J = m4029J();
            if (J == null) {
                C1120a.m3855b(f3784a, "No startPosition");
                return;
            }
            BitmapDescriptor startPositionIcon = f3786e.getStartPositionIcon();
            if (startPositionIcon == null) {
                C1120a.m3855b(f3784a, "No startPositionIcon, use default");
                startPositionIcon = new DisplayOptions().getStartPositionIcon();
            }
            if (startPositionIcon == null) {
                C1120a.m3855b(f3784a, "There is no startPositionIcon");
                return;
            }
            MarkerOptions perspective = new MarkerOptions().position(J).icon(startPositionIcon).zIndex(f3786e.getStartPositionMarkerZIndex()).perspective(false);
            Marker marker2 = this.f3819i;
            if (marker2 == null) {
                this.f3819i = (Marker) this.f3817b.addOverlay(perspective);
                return;
            }
            marker2.setIcon(startPositionIcon);
            this.f3819i.setPosition(J);
        } else {
            C1120a.m3851a(f3784a, "Start position marker already render ok");
        }
    }

    /* renamed from: J */
    private LatLng m4029J() {
        LatLng startPosition = f3785d.getStartPosition();
        if (startPosition == null) {
            C1120a.m3855b(f3784a, "The start position is null");
            C1153d dVar = this.f3818c;
            if (dVar != null) {
                dVar.mo13153a(100001, "Start position is null");
            }
        }
        return startPosition;
    }

    /* renamed from: K */
    private void m4030K() {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null) {
            C1120a.m3851a(f3784a, "renderStartPositionInfoWindow DisplayOptions is null");
        } else if (!displayOptions.isShowStartPositionInfoWindow()) {
            C1120a.m3851a(f3784a, "User set start position infoWindow not show");
            Marker marker = this.f3820j;
            if (marker != null) {
                marker.remove();
                this.f3820j = null;
            }
        } else {
            LatLng startPosition = f3785d.getStartPosition();
            if (startPosition == null) {
                C1120a.m3855b(f3784a, "No startPosition");
                return;
            }
            View startPositionInfoWindowView = f3786e.getStartPositionInfoWindowView();
            if (startPositionInfoWindowView == null) {
                C1120a.m3855b(f3784a, "Start position infoWindow view is null, cannot display");
                Marker marker2 = this.f3820j;
                if (marker2 != null) {
                    marker2.remove();
                    this.f3820j = null;
                    return;
                }
                return;
            }
            MarkerOptions perspective = new MarkerOptions().position(startPosition).icon(BitmapDescriptorFactory.fromView(startPositionInfoWindowView)).zIndex(f3786e.getStartPositionInfoWindowZIndex()).alpha(0.9f).perspective(false);
            Marker marker3 = this.f3820j;
            if (marker3 == null) {
                this.f3820j = (Marker) this.f3817b.addOverlay(perspective);
                return;
            }
            marker3.setPosition(startPosition);
            this.f3820j.setIcon(BitmapDescriptorFactory.fromView(startPositionInfoWindowView));
        }
    }

    /* renamed from: L */
    private void m4031L() {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null) {
            C1120a.m3851a(f3784a, "renderEndPositionMarker DisplayOptions is null");
        } else if (!displayOptions.isShowEndPositionMarker()) {
            C1120a.m3851a(f3784a, "User set endPositionMarker not show");
            Marker marker = this.f3821k;
            if (marker != null) {
                marker.remove();
                this.f3821k = null;
            }
        } else if (this.f3821k == null || this.f3830v) {
            LatLng endPosition = f3785d.getEndPosition();
            if (endPosition == null) {
                C1120a.m3855b(f3784a, "End position coord is null");
                return;
            }
            BitmapDescriptor endPositionIcon = f3786e.getEndPositionIcon();
            if (endPositionIcon == null) {
                C1120a.m3855b(f3784a, "The end position icon is null");
                endPositionIcon = new DisplayOptions().getEndPositionIcon();
            }
            if (endPositionIcon == null) {
                C1120a.m3855b(f3784a, "There is no endPositionIcon");
                return;
            }
            MarkerOptions perspective = new MarkerOptions().position(endPosition).icon(endPositionIcon).zIndex(f3786e.getEndPositionMarkerZIndex()).perspective(false);
            Marker marker2 = this.f3821k;
            if (marker2 == null) {
                this.f3821k = (Marker) this.f3817b.addOverlay(perspective);
                return;
            }
            marker2.setIcon(endPositionIcon);
            this.f3821k.setPosition(endPosition);
        } else {
            C1120a.m3855b(f3784a, "EndPositionMarker already render ok");
        }
    }

    /* renamed from: M */
    private void m4032M() {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null) {
            C1120a.m3851a(f3784a, "renderEndPositionInfoWindow DisplayOptions is null");
        } else if (!displayOptions.isShowEndPositionInfoWindow()) {
            C1120a.m3851a(f3784a, "User set end position infoWindow not show");
            Marker marker = this.f3822l;
            if (marker != null) {
                marker.remove();
                this.f3822l = null;
            }
        } else {
            LatLng endPosition = f3785d.getEndPosition();
            if (endPosition == null) {
                C1120a.m3855b(f3784a, "End position coord is null when render end position infoWindow");
                return;
            }
            View endPositionInfoWindowView = f3786e.getEndPositionInfoWindowView();
            if (endPositionInfoWindowView == null) {
                C1120a.m3855b(f3784a, "End position infoWindow view is null, cannot display");
                Marker marker2 = this.f3822l;
                if (marker2 != null) {
                    marker2.remove();
                    this.f3822l = null;
                    return;
                }
                return;
            }
            MarkerOptions perspective = new MarkerOptions().position(endPosition).icon(BitmapDescriptorFactory.fromView(endPositionInfoWindowView)).anchor(0.5f, 1.0f).zIndex(f3786e.getEndPositionInfoWindowZIndex()).perspective(false);
            Marker marker3 = this.f3822l;
            if (marker3 == null) {
                this.f3822l = (Marker) this.f3817b.addOverlay(perspective);
                return;
            }
            marker3.setPosition(endPosition);
            this.f3822l.setIcon(BitmapDescriptorFactory.fromView(endPositionInfoWindowView));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
        return;
     */
    /* renamed from: N */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m4033N() {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.Thread r0 = r3.f3832x     // Catch:{ all -> 0x004e }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r3)
            return
        L_0x0007:
            com.baidu.mapapi.synchronization.DisplayOptions r0 = com.baidu.mapsdkplatform.comapi.synchronization.render.C1146b.f3786e     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x004c
            com.baidu.mapapi.synchronization.DisplayOptions r0 = com.baidu.mapsdkplatform.comapi.synchronization.render.C1146b.f3786e     // Catch:{ all -> 0x004e }
            boolean r0 = r0.isShowPassengerIcon()     // Catch:{ all -> 0x004e }
            if (r0 != 0) goto L_0x0014
            goto L_0x004c
        L_0x0014:
            r0 = 0
            r3.f3831w = r0     // Catch:{ all -> 0x004e }
            java.lang.Thread$State r0 = java.lang.Thread.State.NEW     // Catch:{ all -> 0x004e }
            java.lang.Thread r1 = r3.f3832x     // Catch:{ all -> 0x004e }
            java.lang.Thread$State r1 = r1.getState()     // Catch:{ all -> 0x004e }
            if (r0 != r1) goto L_0x0026
            java.lang.Thread r0 = r3.f3832x     // Catch:{ all -> 0x004e }
            r0.start()     // Catch:{ all -> 0x004e }
        L_0x0026:
            boolean r0 = r3.f3830v     // Catch:{ all -> 0x004e }
            if (r0 == 0) goto L_0x004a
            java.lang.Thread$State r0 = java.lang.Thread.State.TERMINATED     // Catch:{ all -> 0x004e }
            java.lang.Thread r1 = r3.f3832x     // Catch:{ all -> 0x004e }
            java.lang.Thread$State r1 = r1.getState()     // Catch:{ all -> 0x004e }
            if (r0 != r1) goto L_0x004a
            r0 = 0
            r3.f3832x = r0     // Catch:{ all -> 0x004e }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ all -> 0x004e }
            com.baidu.mapsdkplatform.comapi.synchronization.render.b$c r2 = new com.baidu.mapsdkplatform.comapi.synchronization.render.b$c     // Catch:{ all -> 0x004e }
            r2.<init>()     // Catch:{ all -> 0x004e }
            java.lang.String r0 = "Passenger marker"
            r1.<init>(r2, r0)     // Catch:{ all -> 0x004e }
            r3.f3832x = r1     // Catch:{ all -> 0x004e }
            java.lang.Thread r0 = r3.f3832x     // Catch:{ all -> 0x004e }
            r0.start()     // Catch:{ all -> 0x004e }
        L_0x004a:
            monitor-exit(r3)
            return
        L_0x004c:
            monitor-exit(r3)
            return
        L_0x004e:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.render.C1146b.m4033N():void");
    }

    /* renamed from: O */
    private void m4034O() {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null || displayOptions.isShowPassengerIcon()) {
            m4033N();
            return;
        }
        C1120a.m3851a(f3784a, "User set not show passenger icon");
        Marker marker = this.f3824n;
        if (marker != null) {
            marker.remove();
            this.f3824n = null;
        }
    }

    /* renamed from: P */
    private synchronized void m4035P() {
        this.f3831w = true;
    }

    /* renamed from: Q */
    private void m4036Q() {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null) {
            C1120a.m3851a(f3784a, "renderCarMarker DisplayOptions is null");
        } else if (!displayOptions.isShowCarMarker()) {
            C1120a.m3851a(f3784a, "User set carMarker not show");
            Marker marker = this.f3823m;
            if (marker != null) {
                marker.remove();
                this.f3823m = null;
            }
        } else if (this.f3823m == null || this.f3830v) {
            LatLng T = m4039T();
            if (T == null) {
                C1120a.m3855b(f3784a, "The car(driver) position is null");
                this.f3818c.mo13153a(2004, SynchronizationConstants.LBS_STATUS_MESSAGE_QUERY_TRACK_DRIVER_POSITION_FAILED);
                return;
            }
            BitmapDescriptor carIcon = (!f3786e.get3DCarMarkerEnable() || f3786e.get3DCarMarkerIconList() == null || f3786e.get3DCarMarkerIconList().isEmpty()) ? f3786e.getCarIcon() : f3786e.get3DCarMarkerIconList().get(0);
            if (carIcon == null) {
                C1120a.m3855b(f3784a, "The car icon is null, use default 2D car icon");
                carIcon = new DisplayOptions().getCarIcon();
            }
            if (carIcon == null) {
                C1120a.m3855b(f3784a, "There is no car icon");
                return;
            }
            MarkerOptions perspective = new MarkerOptions().position(T).icon(carIcon).flat(false).rotate(0.0f).zIndex(f3786e.getCarPositionMarkerZIndex()).anchor(0.5f, 0.5f).perspective(false);
            Marker marker2 = this.f3823m;
            if (marker2 == null) {
                this.f3823m = (Marker) this.f3817b.addOverlay(perspective);
                return;
            }
            marker2.setPosition(T);
            this.f3823m.setIcon(carIcon);
        } else {
            C1120a.m3851a(f3784a, "CarIcon already render ok");
        }
    }

    /* renamed from: R */
    private void m4037R() {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null) {
            C1120a.m3851a(f3784a, "renderCarInfoWindow DisplayOptions is null");
        } else if (!displayOptions.isShowCarInfoWindow()) {
            C1120a.m3851a(f3784a, "User set carInfoWindow not show");
            Marker marker = f3787f;
            if (marker != null) {
                marker.remove();
                f3787f = null;
            }
        } else {
            View carInfoWindowView = f3786e.getCarInfoWindowView();
            if (carInfoWindowView == null) {
                C1120a.m3855b(f3784a, "car position infoWindow view is null, cannot display");
                Marker marker2 = f3787f;
                if (marker2 != null) {
                    marker2.remove();
                    f3787f = null;
                    return;
                }
                return;
            }
            LatLng S = m4038S();
            if (S == null) {
                C1120a.m3855b(f3784a, "CarPosition is null");
                return;
            }
            MarkerOptions alpha = new MarkerOptions().position(S).icon(BitmapDescriptorFactory.fromView(carInfoWindowView)).zIndex(f3786e.getCarPositionInfoWindowZIndex()).anchor(0.5f, 1.0f).alpha(0.9f);
            Marker marker3 = f3787f;
            if (marker3 == null) {
                f3787f = (Marker) this.f3817b.addOverlay(alpha);
                return;
            }
            marker3.setPosition(S);
            f3787f.setIcon(BitmapDescriptorFactory.fromView(carInfoWindowView));
        }
    }

    /* renamed from: S */
    private LatLng m4038S() {
        if (this.f3823m == null && f3788g != null) {
            return f3788g.mo13236c().getPoint();
        }
        Marker marker = this.f3823m;
        if (marker != null) {
            return marker.getPosition();
        }
        return null;
    }

    /* renamed from: T */
    private LatLng m4039T() {
        LatLng latLng = f3791r;
        return latLng != null ? latLng : (f3788g == null || f3788g.mo13236c() == null || f3788g.mo13236c().getPoint() == null) ? f3785d.getDriverPosition() : f3788g.mo13236c().getPoint();
    }

    /* renamed from: U */
    private void m4040U() {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions != null) {
            if (!displayOptions.isShowRoutePlan()) {
                C1120a.m3851a(f3784a, "User set route line not show");
                Polyline polyline = this.f3806O;
                if (polyline != null) {
                    polyline.remove();
                    this.f3806O = null;
                }
            } else if (f3788g == null) {
                C1120a.m3855b(f3784a, "No route line data");
            } else {
                C1150d V = m4041V();
                if (C1150d.NO_NEED_RENDER == V) {
                    this.f3834z = false;
                } else if (C1150d.UPDATE_TRAFFIC == V) {
                    this.f3834z = false;
                    m4042W();
                } else {
                    this.f3834z = true;
                    if (!this.f3808Q) {
                        this.f3808Q = true;
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {
                            C1120a.m3852a(f3784a, "Exception caught when renderRouteLine", e);
                        }
                    }
                    f3790p = 0;
                    this.f3827s = 0;
                    this.f3803L = 0;
                    this.f3825o.clear();
                    this.f3804M.clear();
                    this.f3805N.clear();
                    this.f3807P.clear();
                    Polyline polyline2 = this.f3806O;
                    if (polyline2 != null) {
                        polyline2.remove();
                        this.f3806O = null;
                    }
                    m4043X();
                    List<LinkPointPolyLineInfo> list = this.f3825o;
                    if (list == null || list.isEmpty()) {
                        C1120a.m3855b(f3784a, "LinkPointPolyline info is null");
                    } else {
                        m4044Y();
                    }
                }
            }
        }
    }

    /* renamed from: V */
    private C1150d m4041V() {
        String a = f3788g.mo13227a().mo13214a();
        String a2 = f3788g.mo13232b().mo13241a();
        if (this.f3830v && !this.f3792A) {
            this.f3810S = null;
            this.f3811T = null;
        }
        if (a != null && (TextUtils.isEmpty(this.f3810S) || !this.f3810S.equals(a) || this.f3806O == null)) {
            this.f3810S = a;
            this.f3811T = a2;
            C1120a.m3855b(f3784a, "Route line or order state changed or no render, need render");
            return C1150d.RENDER_NEW_LINE;
        } else if (a2 == null || this.f3806O == null || (!TextUtils.isEmpty(this.f3811T) && this.f3811T.equals(a2))) {
            return C1150d.NO_NEED_RENDER;
        } else {
            this.f3811T = a2;
            C1120a.m3855b(f3784a, "Route line only need update traffic");
            return C1150d.UPDATE_TRAFFIC;
        }
    }

    /* renamed from: W */
    private void m4042W() {
        ArrayList<Integer> b = f3788g.mo13232b().mo13245b();
        if (b == null || b.isEmpty()) {
            C1120a.m3855b(f3784a, "Traffic status data is null");
            return;
        }
        if (!this.f3808Q) {
            this.f3808Q = true;
        }
        if (f3790p - this.f3827s >= 0) {
            try {
                if (b.size() == this.f3825o.size()) {
                    for (int i = f3790p - this.f3827s; i < this.f3825o.size(); i++) {
                        this.f3807P.set(i, b.get(i));
                    }
                } else {
                    for (int i2 = f3790p - this.f3827s; i2 < this.f3825o.size(); i2++) {
                        this.f3807P.set(i2, b.get((b.size() + i2) - this.f3825o.size()));
                    }
                }
                int[] iArr = new int[((this.f3807P.size() - f3790p) + this.f3827s)];
                for (int i3 = 0; i3 < (this.f3807P.size() - f3790p) + this.f3827s; i3++) {
                    iArr[i3] = this.f3807P.get((f3790p + i3) - this.f3827s).intValue();
                }
                if (iArr.length > 0) {
                    this.f3806O.setIndexs(iArr);
                    if (this.f3808Q) {
                        this.f3808Q = false;
                    }
                }
            } catch (Exception e) {
                C1120a.m3852a(f3784a, "Exception caught when updateTrafficStatus", e);
            }
        }
    }

    /* renamed from: X */
    private void m4043X() {
        if (f3788g == null || f3788g.mo13227a() == null) {
            C1120a.m3855b(f3784a, "Route info or syncResponseResult is null");
            return;
        }
        List<RouteLineInfo.RouteSectionInfo> b = f3788g.mo13227a().mo13218b();
        ArrayList<Integer> b2 = f3788g.mo13232b().mo13245b();
        if (b == null || b.isEmpty()) {
            C1120a.m3855b(f3784a, "route section info is null");
            this.f3818c.mo13153a(2001, SynchronizationConstants.LBS_STATUS_MESSAGE_ROUTE_PLAN_FAILED);
        } else if (b.isEmpty() || b2 == null || b2.isEmpty() || b2.size() == b.size()) {
            for (int i = 0; i < b.size(); i++) {
                if (b.get(i) != null) {
                    LatLng a = b.get(i).mo13221a();
                    LatLng b3 = b.get(i).mo13223b();
                    int a2 = m4050a(i, b2);
                    LinkPointPolyLineInfo linkPointPolyLineInfo = new LinkPointPolyLineInfo();
                    linkPointPolyLineInfo.mo13330a(a);
                    linkPointPolyLineInfo.mo13332b(b3);
                    linkPointPolyLineInfo.mo13329a(a2);
                    this.f3825o.add(linkPointPolyLineInfo);
                    this.f3804M.add(a);
                }
            }
            this.f3804M.add(b.get(b.size() - 1).mo13223b());
        } else {
            C1120a.m3855b(f3784a, "route section info or traffic status info is invalid");
        }
    }

    /* renamed from: Y */
    private void m4044Y() {
        if (!this.f3808Q) {
            this.f3808Q = true;
        }
        LatLng latLng = null;
        LatLng point = (f3788g == null || f3788g.mo13236c() == null) ? null : f3788g.mo13236c().getPoint();
        if (point != null) {
            latLng = m4054a(point);
        }
        if (latLng != null) {
            this.f3827s = 0;
            try {
                this.f3825o = this.f3825o.subList(f3790p, this.f3825o.size());
                this.f3804M = this.f3804M.subList(f3790p, this.f3804M.size());
            } catch (Exception e) {
                C1120a.m3852a(f3784a, "Caught exception when renderRoutePolyLine", e);
            }
            m4086b(latLng);
            m4074ag();
            m4097c(latLng);
            this.f3803L = f3790p;
        }
        f3790p = 0;
        int size = this.f3804M.size();
        if (size >= 3) {
            for (int i = 0; i < size - 1; i++) {
                if (this.f3804M.get(i) != null) {
                    this.f3807P.add(Integer.valueOf(this.f3825o.get(i).mo13333c()));
                }
            }
            if (f3786e == null) {
                f3786e = new DisplayOptions();
            }
            if (this.f3805N.isEmpty()) {
                this.f3805N.addAll(f3786e.getTrafficTextureList());
            }
            DisplayOptions displayOptions = f3786e;
            if (displayOptions == null) {
                displayOptions = new DisplayOptions();
            }
            PolylineOptions zIndex = new PolylineOptions().points(this.f3804M).dottedLine(true).width(displayOptions.getRouteLineWidth()).customTextureList(this.f3805N).textureIndex(this.f3807P).zIndex(f3786e.getRouteLineZIndex());
            if (!f3786e.isShowRoutePlan()) {
                C1120a.m3855b(f3784a, "User set route line not display");
                zIndex.visible(false);
            }
            this.f3806O = (Polyline) this.f3817b.addOverlay(zIndex);
            if (this.f3808Q) {
                this.f3808Q = false;
            }
        }
    }

    /* renamed from: Z */
    private void m4045Z() {
        List<LinkPointPolyLineInfo> list;
        if (this.f3823m == null) {
            C1120a.m3855b(f3784a, "CarMarker is null");
        } else if (f3791r != null || (list = this.f3825o) == null || list.isEmpty()) {
            m4068aa();
        } else {
            f3791r = this.f3825o.get(0).mo13328a();
            m4086b(f3791r);
            m4097c(f3791r);
        }
    }

    /* renamed from: a */
    private double m4046a(double d) {
        if (this.f3829u == 0) {
            this.f3829u = 5;
        }
        double abs = Math.abs(d);
        double d2 = (double) this.f3829u;
        Double.isNaN(d2);
        return (abs * d2) / this.f3828t;
    }

    /* renamed from: a */
    private double m4047a(double d, double d2) {
        return d2 == Double.MAX_VALUE ? d : Math.abs((d * d2) / Math.sqrt((d2 * d2) + 1.0d));
    }

    /* renamed from: a */
    private double m4048a(double d, LatLng latLng) {
        return latLng.latitude - (d * latLng.longitude);
    }

    /* renamed from: a */
    private int m4050a(int i, ArrayList<Integer> arrayList) {
        if (arrayList == null || arrayList.isEmpty() || i >= arrayList.size()) {
            return 0;
        }
        return arrayList.get(i).intValue();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public LatLng m4054a(LatLng latLng) {
        if (3 == f3789h) {
            C1120a.m3851a(f3784a, "WAIT_PASSENGER State, no need calculate");
            return null;
        }
        List<LinkPointPolyLineInfo> list = this.f3825o;
        if (list == null || list.isEmpty()) {
            String str = f3784a;
            C1120a.m3855b(str, "mLinkPolyLineInfos size = " + this.f3825o.size());
            return null;
        }
        int i = f3790p;
        while (i < this.f3825o.size()) {
            LatLng a = this.f3825o.get(i).mo13328a();
            LatLng b = this.f3825o.get(i).mo13331b();
            if (!m4062a(latLng, a)) {
                if (m4062a(latLng, b)) {
                    this.f3827s = m4103d(i + 1);
                    return b;
                }
                boolean a2 = m4064a(a, b, latLng);
                boolean b2 = m4092b(a, b, latLng);
                if (!a2 || !b2) {
                    i++;
                }
            }
            this.f3827s = m4103d(i);
            return a;
        }
        return null;
    }

    /* renamed from: a */
    private void m4056a(float f, LatLng latLng, LatLng latLng2) {
        DisplayOptions displayOptions;
        Marker marker;
        DisplayOptions displayOptions2 = f3786e;
        if (displayOptions2 == null) {
            C1120a.m3855b(f3784a, "User not set DisplayOptions, use default 2D marker");
            displayOptions = new DisplayOptions();
        } else {
            List<BitmapDescriptor> list = displayOptions2.get3DCarMarkerIconList();
            if (list == null || list.isEmpty()) {
                C1120a.m3855b(f3784a, "User not set 3D car marker list, use default 2D marker");
                displayOptions = new DisplayOptions();
            } else {
                double atan2 = (Math.atan2(latLng2.latitude - latLng.latitude, latLng2.longitude - latLng.longitude) / 3.141592653589793d) * 180.0d;
                if (atan2 < 0.0d) {
                    atan2 += 360.0d;
                }
                if (atan2 != this.f3816Y) {
                    this.f3816Y = atan2;
                    int size = list.size();
                    int i = 360 / size;
                    double d = (double) i;
                    Double.isNaN(d);
                    int i2 = (int) (atan2 / d);
                    if (size == i2) {
                        i2 = 0;
                    }
                    BitmapDescriptor bitmapDescriptor = list.get(i2);
                    if (bitmapDescriptor != null && (marker = this.f3823m) != null) {
                        marker.setIcon(bitmapDescriptor);
                        Marker marker2 = this.f3823m;
                        double d2 = (double) (i2 * i);
                        Double.isNaN(d2);
                        marker2.setRotate(((float) (atan2 - d2)) - 1.0f);
                        return;
                    }
                    return;
                }
                return;
            }
        }
        this.f3823m.setIcon(displayOptions.getCarIcon());
        this.f3823m.setRotate(f);
    }

    /* renamed from: a */
    private void m4057a(LatLngBounds.Builder builder) {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null || displayOptions.isShowStartPositionMarkerInSpan()) {
            builder.include(m4029J());
        } else {
            C1120a.m3855b(f3784a, "User set not show startPositionMarker in span");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public synchronized void m4058a(LatLngBounds latLngBounds) {
        int i;
        int i2;
        int i3;
        if (!this.f3801J || this.f3815X) {
            MapStatus mapStatus = this.f3817b.getMapStatus();
            if (mapStatus == null) {
                C1120a.m3855b(f3784a, "Get map status failed");
                return;
            }
            WinRound winRound = mapStatus.winRound;
            int abs = Math.abs(winRound.right - winRound.left);
            int abs2 = Math.abs(winRound.bottom - winRound.top);
            int i4 = 50;
            if (f3786e != null) {
                i4 = f3786e.getPaddingLeft();
                i3 = f3786e.getPaddingTop();
                i2 = f3786e.getPaddingRight();
                i = f3786e.getPaddingBottom();
            } else {
                i3 = 50;
                i2 = 50;
                i = 50;
            }
            int i5 = (abs - i4) - i2;
            int i6 = (abs2 - i3) - i;
            if (i5 < 0 || i6 < 0 || i5 > abs || i6 > abs2) {
                C1120a.m3855b(f3784a, "Invalid padding，use default padding");
            }
            this.f3817b.animateMapStatus(MapStatusUpdateFactory.newLatLngBounds(latLngBounds, i4, i3, i2, i));
            return;
        }
        this.f3801J = false;
    }

    /* renamed from: a */
    private synchronized void m4059a(LatLngBounds latLngBounds, int i, int i2, int i3, int i4) {
        if (f3786e == null) {
            f3786e = new DisplayOptions();
        }
        f3786e.setMapViewPadding(i, i2, i3, i4);
        this.f3817b.animateMapStatus(MapStatusUpdateFactory.newLatLngBounds(latLngBounds, i, i2, i3, i4));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0025  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m4061a(com.baidu.mapapi.map.MyLocationData r8) {
        /*
            r7 = this;
            r0 = 0
            double r2 = r8.latitude     // Catch:{ NumberFormatException -> 0x0017 }
            java.lang.Double r2 = java.lang.Double.valueOf(r2)     // Catch:{ NumberFormatException -> 0x0017 }
            long r2 = r2.longValue()     // Catch:{ NumberFormatException -> 0x0017 }
            double r4 = r8.longitude     // Catch:{ NumberFormatException -> 0x0018 }
            java.lang.Double r8 = java.lang.Double.valueOf(r4)     // Catch:{ NumberFormatException -> 0x0018 }
            long r4 = r8.longValue()     // Catch:{ NumberFormatException -> 0x0018 }
            goto L_0x0020
        L_0x0017:
            r2 = r0
        L_0x0018:
            java.lang.String r8 = com.baidu.mapsdkplatform.comapi.synchronization.render.C1146b.f3784a
            java.lang.String r4 = "Trans latitude and longitude failed"
            com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a.m3855b(r8, r4)
            r4 = r0
        L_0x0020:
            r8 = 0
            int r6 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r6 != 0) goto L_0x002a
            int r6 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r6 != 0) goto L_0x002a
            return r8
        L_0x002a:
            r0 = -90
            int r6 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r6 < 0) goto L_0x0044
            r0 = 90
            int r6 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r6 > 0) goto L_0x0044
            r0 = -180(0xffffffffffffff4c, double:NaN)
            int r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r2 < 0) goto L_0x0044
            r0 = 180(0xb4, double:8.9E-322)
            int r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0043
            goto L_0x0044
        L_0x0043:
            r8 = 1
        L_0x0044:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.render.C1146b.m4061a(com.baidu.mapapi.map.MyLocationData):boolean");
    }

    /* renamed from: a */
    private boolean m4062a(LatLng latLng, LatLng latLng2) {
        return (latLng == null || latLng2 == null) ? latLng == null && latLng2 == null : Math.abs(latLng.latitude - latLng2.latitude) < 1.0E-4d && Math.abs(latLng.longitude - latLng2.longitude) < 1.0E-4d;
    }

    /* renamed from: a */
    private boolean m4063a(LatLng latLng, LatLng latLng2, double d) {
        double d2;
        LatLng latLng3;
        LatLng latLng4 = latLng;
        LatLng latLng5 = latLng2;
        double d3 = d;
        boolean z = latLng4.latitude > latLng5.latitude;
        double a = m4048a(d3, latLng4);
        double f = m4111f(latLng, latLng2);
        double a2 = z ? m4047a(f, d3) : m4047a(f, d3) * -1.0d;
        double a3 = m4046a(a2);
        double d4 = latLng4.latitude;
        double d5 = a2;
        while (true) {
            if ((d4 > latLng5.latitude) != z) {
                return true;
            }
            if (this.f3808Q) {
                return false;
            }
            if (Double.MAX_VALUE == d3) {
                d2 = a3;
                latLng3 = new LatLng(d4, latLng4.longitude);
            } else {
                d2 = a3;
                latLng3 = new LatLng(d4, (d4 - a) / d3);
            }
            m4086b(latLng3);
            m4097c(latLng3);
            double d6 = d2;
            if (!m4090b(d6)) {
                return false;
            }
            d4 -= d5;
            a3 = d6;
        }
    }

    /* renamed from: a */
    private boolean m4064a(LatLng latLng, LatLng latLng2, LatLng latLng3) {
        double d = latLng.latitude;
        double d2 = latLng.longitude;
        double d3 = latLng2.latitude;
        return Math.abs(((latLng3.latitude - d) * (latLng2.longitude - d2)) - ((d3 - d) * (latLng3.longitude - d2))) < 1.0E-4d;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003c, code lost:
        return;
     */
    /* renamed from: aa */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m4068aa() {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.Thread r0 = r3.f3826q     // Catch:{ all -> 0x003d }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r3)
            return
        L_0x0007:
            r0 = 0
            r3.f3808Q = r0     // Catch:{ all -> 0x003d }
            java.lang.Thread$State r0 = java.lang.Thread.State.NEW     // Catch:{ all -> 0x003d }
            java.lang.Thread r1 = r3.f3826q     // Catch:{ all -> 0x003d }
            java.lang.Thread$State r1 = r1.getState()     // Catch:{ all -> 0x003d }
            if (r0 != r1) goto L_0x001b
            java.lang.Thread r0 = r3.f3826q     // Catch:{ all -> 0x003d }
            r0.start()     // Catch:{ all -> 0x003d }
            monitor-exit(r3)
            return
        L_0x001b:
            java.lang.Thread$State r0 = java.lang.Thread.State.TERMINATED     // Catch:{ all -> 0x003d }
            java.lang.Thread r1 = r3.f3826q     // Catch:{ all -> 0x003d }
            java.lang.Thread$State r1 = r1.getState()     // Catch:{ all -> 0x003d }
            if (r0 != r1) goto L_0x003b
            r0 = 0
            r3.f3826q = r0     // Catch:{ all -> 0x003d }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ all -> 0x003d }
            com.baidu.mapsdkplatform.comapi.synchronization.render.b$b r2 = new com.baidu.mapsdkplatform.comapi.synchronization.render.b$b     // Catch:{ all -> 0x003d }
            r2.<init>()     // Catch:{ all -> 0x003d }
            java.lang.String r0 = "Car moving"
            r1.<init>(r2, r0)     // Catch:{ all -> 0x003d }
            r3.f3826q = r1     // Catch:{ all -> 0x003d }
            java.lang.Thread r0 = r3.f3826q     // Catch:{ all -> 0x003d }
            r0.start()     // Catch:{ all -> 0x003d }
        L_0x003b:
            monitor-exit(r3)
            return
        L_0x003d:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.render.C1146b.m4068aa():void");
    }

    /* renamed from: ab */
    private synchronized void m4069ab() {
        this.f3808Q = true;
    }

    /* access modifiers changed from: private */
    /* renamed from: ac */
    public LatLng m4070ac() {
        if (f3788g == null || f3788g.mo13236c() == null || f3788g.mo13236c().getPoint() == null || this.f3813V) {
            return null;
        }
        return f3788g.mo13236c().getPoint();
    }

    /* access modifiers changed from: private */
    /* renamed from: ad */
    public double m4071ad() {
        List<LinkPointPolyLineInfo> list = this.f3825o;
        if (list == null || list.isEmpty() || f3790p > this.f3825o.size()) {
            return 1.0d;
        }
        double d = 0.0d;
        for (int i = f3790p - this.f3827s; i < f3790p; i++) {
            d += m4111f(this.f3825o.get(i).mo13328a(), this.f3825o.get(i).mo13331b());
        }
        return d;
    }

    /* access modifiers changed from: private */
    /* renamed from: ae */
    public void m4072ae() {
        if (f3790p != 0) {
            for (int i = f3790p - this.f3827s; i < f3790p; i++) {
                LatLng a = this.f3825o.get(i).mo13328a();
                LatLng b = this.f3825o.get(i).mo13331b();
                double f = m4111f(a, b) / 2.0d;
                double d = (b.latitude - a.latitude) / f;
                double d2 = (b.longitude - a.longitude) / f;
                double e = m4107e(a, b);
                LatLng latLng = a;
                int i2 = 1;
                while (((double) i2) <= f) {
                    double d3 = latLng.longitude;
                    double d4 = f;
                    double d5 = latLng.latitude;
                    if (0.0d == e) {
                        d3 = latLng.longitude + d2;
                    } else {
                        if (Double.MAX_VALUE != e) {
                            d3 = latLng.longitude + d2;
                        }
                        d5 = latLng.latitude + d;
                    }
                    LatLng latLng2 = new LatLng(d5, d3);
                    List<LatLng> list = this.f3804M;
                    if (list != null && !list.isEmpty()) {
                        if (this.f3834z) {
                            this.f3808Q = true;
                            return;
                        }
                        this.f3804M.set(i, latLng2);
                    }
                    List<Integer> list2 = this.f3807P;
                    if (list2 != null && !list2.isEmpty()) {
                        this.f3807P.set(i, Integer.valueOf(this.f3825o.get(i).mo13333c()));
                    }
                    if (m4091b(latLng, latLng2)) {
                        this.f3803L = i;
                        m4108e(i);
                        i2++;
                        latLng = latLng2;
                        f = d4;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: af */
    public void m4073af() {
        f3790p = 0;
        this.f3827s = 0;
        this.f3825o.clear();
        this.f3804M.clear();
        this.f3807P.clear();
    }

    /* access modifiers changed from: private */
    /* renamed from: ag */
    public void m4074ag() {
        if (this.f3823m != null) {
            float f = 0.0f;
            try {
                f = Double.valueOf(360.0d - f3788g.mo13236c().getAngle()).floatValue();
            } catch (NumberFormatException e) {
                C1120a.m3852a(f3784a, "Get DriverPosition Angle failed", e);
            }
            this.f3823m.setRotate(f);
        }
    }

    /* renamed from: ah */
    private synchronized void m4075ah() {
        this.f3794C = true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0058, code lost:
        return;
     */
    /* renamed from: ai */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m4076ai() {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.Thread r0 = r3.f3793B     // Catch:{ all -> 0x0059 }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r3)
            return
        L_0x0007:
            boolean r0 = r3.f3794C     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x000e
            r0 = 0
            r3.f3794C = r0     // Catch:{ all -> 0x0059 }
        L_0x000e:
            java.lang.Thread$State r0 = java.lang.Thread.State.NEW     // Catch:{ all -> 0x0059 }
            java.lang.Thread r1 = r3.f3793B     // Catch:{ all -> 0x0059 }
            java.lang.Thread$State r1 = r1.getState()     // Catch:{ all -> 0x0059 }
            r2 = 1
            if (r0 != r1) goto L_0x0020
            r3.f3800I = r2     // Catch:{ all -> 0x0059 }
            java.lang.Thread r0 = r3.f3793B     // Catch:{ all -> 0x0059 }
            r0.start()     // Catch:{ all -> 0x0059 }
        L_0x0020:
            boolean r0 = r3.f3830v     // Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0035
            java.lang.Thread$State r0 = java.lang.Thread.State.TIMED_WAITING     // Catch:{ all -> 0x0059 }
            java.lang.Thread r1 = r3.f3793B     // Catch:{ all -> 0x0059 }
            java.lang.Thread$State r1 = r1.getState()     // Catch:{ all -> 0x0059 }
            if (r0 != r1) goto L_0x0035
            java.lang.Thread r0 = r3.f3793B     // Catch:{ all -> 0x0059 }
            r0.interrupt()     // Catch:{ all -> 0x0059 }
            r3.f3800I = r2     // Catch:{ all -> 0x0059 }
        L_0x0035:
            java.lang.Thread$State r0 = java.lang.Thread.State.TERMINATED     // Catch:{ all -> 0x0059 }
            java.lang.Thread r1 = r3.f3793B     // Catch:{ all -> 0x0059 }
            java.lang.Thread$State r1 = r1.getState()     // Catch:{ all -> 0x0059 }
            if (r0 != r1) goto L_0x0057
            r0 = 0
            r3.f3793B = r0     // Catch:{ all -> 0x0059 }
            r3.f3800I = r2     // Catch:{ all -> 0x0059 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ all -> 0x0059 }
            com.baidu.mapsdkplatform.comapi.synchronization.render.b$a r2 = new com.baidu.mapsdkplatform.comapi.synchronization.render.b$a     // Catch:{ all -> 0x0059 }
            r2.<init>()     // Catch:{ all -> 0x0059 }
            java.lang.String r0 = "Adjust visible span"
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0059 }
            r3.f3793B = r1     // Catch:{ all -> 0x0059 }
            java.lang.Thread r0 = r3.f3793B     // Catch:{ all -> 0x0059 }
            r0.start()     // Catch:{ all -> 0x0059 }
        L_0x0057:
            monitor-exit(r3)
            return
        L_0x0059:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.render.C1146b.m4076ai():void");
    }

    /* renamed from: aj */
    private void m4077aj() {
        this.f3817b.setOnSynchronizationListener(this.f3798G);
    }

    /* access modifiers changed from: private */
    /* renamed from: ak */
    public void m4078ak() {
        int i = f3789h;
        if (i != 0) {
            if (i == 1) {
                m4079al();
                return;
            } else if (i == 2) {
                m4080am();
                return;
            } else if (i == 3) {
                m4081an();
                return;
            } else if (i == 4) {
                m4082ao();
                return;
            } else if (i != 5) {
                return;
            }
        }
        m4075ah();
    }

    /* renamed from: al */
    private void m4079al() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        m4057a(builder);
        m4104d(builder);
        m4098c(builder);
        m4109e(builder);
        this.f3797F = m4116h(builder);
    }

    /* renamed from: am */
    private void m4080am() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        m4057a(builder);
        m4098c(builder);
        m4104d(builder);
        m4109e(builder);
        this.f3797F = m4116h(builder);
    }

    /* renamed from: an */
    private void m4081an() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        m4057a(builder);
        m4098c(builder);
        m4104d(builder);
        this.f3797F = m4116h(builder);
    }

    /* renamed from: ao */
    private void m4082ao() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        m4087b(builder);
        m4098c(builder);
        m4109e(builder);
        this.f3797F = m4116h(builder);
    }

    /* renamed from: b */
    private double m4083b(LatLng latLng, LatLng latLng2, double d) {
        if (Double.MAX_VALUE == d) {
            return latLng2.latitude > latLng.latitude ? 360.0d : 180.0d;
        }
        double d2 = 0.0d;
        if (0.0d == d) {
            return latLng2.longitude > latLng.longitude ? 270.0d : 90.0d;
        }
        if ((latLng2.latitude - latLng.latitude) * d < 0.0d) {
            d2 = 180.0d;
        }
        return (((Math.atan(d) / 3.141592653589793d) * 180.0d) + d2) - 90.0d;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m4086b(LatLng latLng) {
        Marker marker = this.f3823m;
        if (marker != null) {
            marker.setPosition(latLng);
        }
    }

    /* renamed from: b */
    private void m4087b(LatLngBounds.Builder builder) {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null || displayOptions.isShowEndPositionMarkerInSpan()) {
            builder.include(f3785d.getEndPosition());
        } else {
            C1120a.m3855b(f3784a, "User set not show endPositionMarker in span");
        }
    }

    /* renamed from: b */
    private boolean m4090b(double d) {
        try {
            Thread.sleep(Double.valueOf((d * 1000.0d) + 50.0d).longValue());
            return true;
        } catch (InterruptedException unused) {
            return false;
        } catch (NumberFormatException e) {
            C1120a.m3852a(f3784a, "Calc sleep interval failed", e);
            return false;
        }
    }

    /* renamed from: b */
    private boolean m4091b(LatLng latLng, LatLng latLng2) {
        if (this.f3808Q) {
            return false;
        }
        m4086b(latLng);
        m4097c(latLng);
        double e = m4107e(latLng, latLng2);
        float b = (float) m4083b(latLng, latLng2, e);
        if (this.f3823m != null) {
            DisplayOptions displayOptions = f3786e;
            if (displayOptions == null || !displayOptions.get3DCarMarkerEnable()) {
                this.f3823m.setRotate(b);
            } else {
                m4056a(b, latLng, latLng2);
            }
        }
        boolean c = 0.0d == e ? m4100c(latLng, latLng2) : m4063a(latLng, latLng2, e);
        if (c) {
            f3791r = latLng2;
        }
        return c;
    }

    /* renamed from: b */
    private boolean m4092b(LatLng latLng, LatLng latLng2, LatLng latLng3) {
        LatLng latLng4 = latLng;
        LatLng latLng5 = latLng2;
        LatLng latLng6 = latLng3;
        double d = latLng4.latitude;
        double d2 = latLng4.longitude;
        double d3 = latLng5.latitude;
        double d4 = latLng5.longitude;
        double d5 = latLng6.latitude;
        double d6 = latLng6.longitude;
        return Math.min(d, d3) - 1.0E-4d <= d5 && d5 <= Math.max(d, d3) + 1.0E-4d && Math.min(d2, d4) - 1.0E-4d <= d6 && d6 <= Math.max(d2, d4) + 1.0E-4d;
    }

    /* renamed from: c */
    static /* synthetic */ int m4094c(C1146b bVar) {
        int i = bVar.f3814W;
        bVar.f3814W = i + 1;
        return i;
    }

    /* renamed from: c */
    private void m4096c(int i) {
        this.f3833y = 1000 == f3789h;
        this.f3792A = (1 == f3789h && 2 == i) || (1 == i && 2 == f3789h);
        if (f3789h != i) {
            f3789h = i;
            this.f3830v = true;
            return;
        }
        this.f3830v = false;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m4097c(LatLng latLng) {
        Marker marker = f3787f;
        if (marker != null) {
            marker.setPosition(latLng);
        }
    }

    /* renamed from: c */
    private void m4098c(LatLngBounds.Builder builder) {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null || displayOptions.isShowCarMarkerInSpan()) {
            Marker marker = this.f3823m;
            builder.include((marker == null || this.f3800I) ? m4039T() : marker.getPosition());
            return;
        }
        C1120a.m3855b(f3784a, "User set not show carMarker in span");
    }

    /* renamed from: c */
    private boolean m4100c(LatLng latLng, LatLng latLng2) {
        double d = m4102d(latLng, latLng2);
        double a = m4046a(d);
        for (double d2 = latLng.longitude; d2 <= latLng2.longitude; d2 += d) {
            if (this.f3808Q) {
                return false;
            }
            LatLng latLng3 = new LatLng(latLng.latitude, d2);
            m4086b(latLng3);
            m4097c(latLng3);
            if (!m4090b(a)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: d */
    private double m4102d(LatLng latLng, LatLng latLng2) {
        return C1121b.m3857a(latLng, latLng2);
    }

    /* renamed from: d */
    private synchronized int m4103d(int i) {
        int i2;
        i2 = i - f3790p;
        f3790p = i;
        return i2;
    }

    /* renamed from: d */
    private void m4104d(LatLngBounds.Builder builder) {
        LatLng latLng;
        String str;
        String str2;
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null || displayOptions.isShowPassengerIconInSpan()) {
            Marker marker = this.f3824n;
            if (marker == null) {
                MyLocationData locationData = this.f3817b.getLocationData();
                if (locationData == null) {
                    str = f3784a;
                    str2 = "No passenger location data";
                } else if (m4061a(locationData)) {
                    latLng = new LatLng(locationData.latitude, locationData.longitude);
                } else {
                    return;
                }
            } else {
                latLng = marker.getPosition();
            }
            builder.include(latLng);
            return;
        }
        str = f3784a;
        str2 = "User set not show passengerMarker in span";
        C1120a.m3855b(str, str2);
    }

    /* renamed from: e */
    private double m4107e(LatLng latLng, LatLng latLng2) {
        if (latLng2.longitude == latLng.longitude) {
            return Double.MAX_VALUE;
        }
        return (latLng2.latitude - latLng.latitude) / (latLng2.longitude - latLng.longitude);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m4108e(int i) {
        List<LatLng> list = this.f3804M;
        if (list == null || list.isEmpty()) {
            C1120a.m3855b(f3784a, "Route polyline points is null when remove");
        } else if (this.f3804M.size() <= 2 || i >= this.f3804M.size() - 2) {
            Polyline polyline = this.f3806O;
            if (polyline != null) {
                polyline.remove();
            }
        } else {
            List<Integer> list2 = this.f3807P;
            if (list2 == null || list2.isEmpty()) {
                C1120a.m3855b(f3784a, "No need removeTravelledPolyLine");
            } else if (!this.f3834z) {
                try {
                    List<Integer> subList = this.f3807P.subList(i, this.f3807P.size());
                    int[] iArr = new int[subList.size()];
                    for (int i2 = 0; i2 < subList.size(); i2++) {
                        iArr[i2] = subList.get(i2).intValue();
                    }
                    if (this.f3806O == null) {
                        return;
                    }
                    if (!this.f3808Q) {
                        this.f3806O.setIndexs(iArr);
                        try {
                            this.f3806O.setPoints(this.f3804M.subList(i, this.f3804M.size()));
                        } catch (Exception e) {
                            C1120a.m3852a(f3784a, "Get subList of PolyLinePointList failed", e);
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /* renamed from: e */
    private void m4109e(LatLngBounds.Builder builder) {
        DisplayOptions displayOptions = f3786e;
        if (displayOptions == null || displayOptions.isShowRoutePlanInSpan()) {
            List<LinkPointPolyLineInfo> list = this.f3825o;
            if (list == null || list.isEmpty()) {
                m4115g(builder);
            } else {
                m4113f(builder);
            }
        } else {
            C1120a.m3855b(f3784a, "User set not show routeLine in span");
        }
    }

    /* renamed from: f */
    private double m4111f(LatLng latLng, LatLng latLng2) {
        return C1121b.m3857a(latLng, latLng2);
    }

    /* renamed from: f */
    private void m4113f(LatLngBounds.Builder builder) {
        for (int i = this.f3803L; i < this.f3825o.size(); i++) {
            builder.include(this.f3825o.get(i).mo13328a());
        }
        List<LinkPointPolyLineInfo> list = this.f3825o;
        builder.include(list.get(list.size() - 1).mo13331b());
    }

    /* renamed from: g */
    private void m4115g(LatLngBounds.Builder builder) {
        if (f3788g == null || f3788g.mo13227a() == null) {
            C1120a.m3855b(f3784a, "There no routeLine info, no need show in span");
            return;
        }
        List<RouteLineInfo.RouteSectionInfo> b = f3788g.mo13227a().mo13218b();
        if (b == null || b.isEmpty()) {
            C1120a.m3855b(f3784a, "There no routeLine position, no need show in span");
            return;
        }
        for (int i = 0; i < b.size(); i++) {
            builder.include(b.get(i).mo13221a());
        }
        builder.include(b.get(b.size() - 1).mo13223b());
    }

    /* renamed from: h */
    private LatLngBounds m4116h(LatLngBounds.Builder builder) {
        return builder.build();
    }

    /* renamed from: o */
    private void m4131o() {
        if (f3786e != null) {
            Marker marker = f3787f;
            if (marker != null) {
                marker.remove();
                f3787f = null;
            }
            Marker marker2 = this.f3823m;
            if (marker2 != null) {
                marker2.remove();
                this.f3823m = null;
            }
            Marker marker3 = this.f3819i;
            if (marker3 != null) {
                marker3.remove();
                this.f3819i = null;
            }
            Marker marker4 = this.f3820j;
            if (marker4 != null) {
                marker4.remove();
                this.f3820j = null;
            }
            Marker marker5 = this.f3822l;
            if (marker5 != null) {
                marker5.remove();
                this.f3822l = null;
            }
            f3786e.getStartPositionIcon().recycle();
            f3786e.getCarIcon().recycle();
            if (f3786e.getEndPositionIcon() != null) {
                f3786e.getEndPositionIcon().recycle();
            }
            f3786e = null;
        }
    }

    /* renamed from: p */
    private void m4134p() {
        f3790p = 0;
        this.f3829u = 0;
        this.f3808Q = true;
        Thread thread = this.f3826q;
        if (thread != null) {
            try {
                thread.join();
            } catch (InterruptedException unused) {
                C1120a.m3855b(f3784a, "InterruptedException when release CarMoveThread");
            }
            this.f3826q = null;
        }
    }

    /* renamed from: q */
    private void m4136q() {
        this.f3831w = true;
        if (this.f3832x != null) {
            this.f3832x = null;
        }
    }

    /* renamed from: r */
    private void m4138r() {
        this.f3794C = true;
        if (this.f3793B != null) {
            this.f3793B = null;
        }
    }

    /* renamed from: s */
    private void m4139s() {
        f3785d = null;
        f3788g = null;
        f3789h = 1000;
        this.f3808Q = false;
        this.f3812U = true;
        this.f3813V = false;
        this.f3825o.clear();
        f3790p = 0;
        f3791r = null;
        this.f3827s = 0;
        this.f3828t = 0.0d;
        this.f3804M.clear();
        this.f3807P.clear();
        Polyline polyline = this.f3806O;
        if (polyline != null) {
            polyline.remove();
            this.f3806O = null;
        }
        for (int i = 0; i < this.f3805N.size(); i++) {
            this.f3805N.get(i).recycle();
        }
        this.f3805N.clear();
    }

    /* renamed from: t */
    private void m4141t() {
        m4035P();
        m4027H();
        m4075ah();
        m4024E();
        m4020A();
        m4021B();
        m4022C();
        m4023D();
        BaiduMap baiduMap = this.f3817b;
        if (baiduMap != null) {
            baiduMap.clear();
        }
    }

    /* renamed from: u */
    private void m4143u() {
        if (m4154z()) {
            m4024E();
            m4027H();
            m4076ai();
            m4028I();
            m4030K();
            m4031L();
            m4032M();
            m4034O();
            m4036Q();
            m4037R();
            m4040U();
            m4045Z();
        }
    }

    /* renamed from: v */
    private void m4146v() {
        if (m4154z()) {
            m4024E();
            m4027H();
            m4076ai();
            m4028I();
            m4030K();
            m4031L();
            m4032M();
            m4034O();
            m4036Q();
            m4037R();
            m4040U();
            m4045Z();
        }
    }

    /* renamed from: w */
    private void m4148w() {
        if (m4154z()) {
            m4024E();
            m4026G();
            m4025F();
            m4027H();
            m4076ai();
            m4028I();
            m4030K();
            m4031L();
            m4032M();
            m4034O();
            m4036Q();
            m4037R();
            m4045Z();
        }
    }

    /* renamed from: x */
    private void m4150x() {
        if (m4154z()) {
            m4035P();
            m4024E();
            m4021B();
            m4022C();
            m4027H();
            m4076ai();
            m4028I();
            m4030K();
            m4031L();
            m4032M();
            m4036Q();
            m4037R();
            m4040U();
            m4045Z();
        }
    }

    /* renamed from: y */
    private void m4152y() {
        m4035P();
        m4027H();
        m4075ah();
        m4020A();
        m4021B();
        m4022C();
        m4023D();
    }

    /* renamed from: z */
    private boolean m4154z() {
        if (f3785d == null || f3786e == null) {
            C1120a.m3855b(f3784a, "No render data");
            C1153d dVar = this.f3818c;
            if (dVar != null) {
                dVar.mo13153a(100001, "Get render data failed");
            }
            return false;
        } else if (this.f3817b != null) {
            return true;
        } else {
            C1120a.m3855b(f3784a, "BaiduMap is null");
            C1153d dVar2 = this.f3818c;
            if (dVar2 != null) {
                dVar2.mo13153a(100002, "BaiduMap instance is null.");
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Marker mo13340a() {
        return this.f3819i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13341a(int i) {
        this.f3795D = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13342a(int i, int i2, int i3, int i4) {
        this.f3815X = true;
        m4078ak();
        m4059a(this.f3797F, i, i2, i3, i4);
    }

    /* renamed from: a */
    public void mo13343a(BaiduMap baiduMap, RoleOptions roleOptions, DisplayOptions displayOptions) {
        this.f3817b = baiduMap;
        f3785d = roleOptions;
        f3786e = displayOptions;
        this.f3798G = new C1151e();
        m4077aj();
        f3791r = null;
        this.f3817b.getUiSettings().setRotateGesturesEnabled(false);
        this.f3817b.getUiSettings().setCompassEnabled(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo13344a(RoleOptions roleOptions, DisplayOptions displayOptions, SyncResponseResult syncResponseResult, int i) {
        f3785d = roleOptions;
        f3786e = displayOptions;
        if (f3786e == null) {
            f3786e = new DisplayOptions();
        }
        f3788g = syncResponseResult;
        this.f3829u = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo13345a(C1153d dVar) {
        this.f3818c = dVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Marker mo13346b() {
        return this.f3821k;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13347b(int i) {
        this.f3796E = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public Marker mo13348c() {
        return this.f3823m;
    }

    /* renamed from: d */
    public void mo13349d() {
        this.f3813V = false;
        if (this.f3812U) {
            this.f3812U = false;
            return;
        }
        m4033N();
        m4076ai();
        m4068aa();
    }

    /* renamed from: e */
    public void mo13350e() {
        this.f3813V = true;
        m4069ab();
        m4035P();
        m4075ah();
    }

    /* renamed from: f */
    public void mo13351f() {
        m4134p();
        m4136q();
        m4138r();
        m4131o();
        m4139s();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public void mo13352g() {
        this.f3815X = true;
        m4078ak();
        m4058a(this.f3797F);
    }

    public void handleMessage(Message message) {
        String str = f3784a;
        C1120a.m3856c(str, "The orderState in message is: " + message.what);
        m4096c(message.what);
        int i = message.what;
        if (i == 0) {
            m4141t();
        } else if (i == 1) {
            m4143u();
        } else if (i == 2) {
            m4146v();
        } else if (i == 3) {
            m4148w();
        } else if (i == 4) {
            m4150x();
        } else if (i != 5) {
            String str2 = f3784a;
            C1120a.m3856c(str2, "Undefined Message type: " + message.what);
        } else {
            m4152y();
        }
    }
}
