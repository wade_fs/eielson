package com.baidu.mapsdkplatform.comjni.map.basemap;

import android.os.Bundle;
import android.util.LongSparseArray;

public class BaseMapCallback {

    /* renamed from: a */
    private static LongSparseArray<C1178b> f3939a = new LongSparseArray<>();

    public static int ReqLayerData(Bundle bundle, long j, int i, Bundle bundle2) {
        int size = f3939a.size();
        for (int i2 = 0; i2 < size; i2++) {
            C1178b valueAt = f3939a.valueAt(i2);
            if (valueAt != null && valueAt.mo13025a(j)) {
                return valueAt.mo13004a(bundle, j, i, bundle2);
            }
        }
        return 0;
    }

    public static void addLayerDataInterface(long j, C1178b bVar) {
        f3939a.put(j, bVar);
    }

    public static void removeLayerDataInterface(long j) {
        f3939a.remove(j);
    }
}
