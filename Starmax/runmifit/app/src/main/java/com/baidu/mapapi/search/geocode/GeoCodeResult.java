package com.baidu.mapapi.search.geocode;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.SearchResult;

public class GeoCodeResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<GeoCodeResult> CREATOR = new C0975a();

    /* renamed from: a */
    private LatLng f2966a;

    /* renamed from: b */
    private String f2967b;

    /* renamed from: c */
    private int f2968c;

    /* renamed from: d */
    private int f2969d;

    /* renamed from: e */
    private String f2970e;

    public GeoCodeResult() {
    }

    protected GeoCodeResult(Parcel parcel) {
        this.f2966a = (LatLng) parcel.readValue(LatLng.class.getClassLoader());
        this.f2967b = parcel.readString();
        this.f2968c = parcel.readInt();
        this.f2969d = parcel.readInt();
        this.f2970e = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    @Deprecated
    public String getAddress() {
        return this.f2967b;
    }

    public int getConfidence() {
        return this.f2969d;
    }

    public String getLevel() {
        return this.f2970e;
    }

    public LatLng getLocation() {
        return this.f2966a;
    }

    public int getPrecise() {
        return this.f2968c;
    }

    @Deprecated
    public void setAddress(String str) {
        this.f2967b = str;
    }

    public void setConfidence(int i) {
        this.f2969d = i;
    }

    public void setLevel(String str) {
        this.f2970e = str;
    }

    public void setLocation(LatLng latLng) {
        this.f2966a = latLng;
    }

    public void setPrecise(int i) {
        this.f2968c = i;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("GeoCodeResult: \n");
        stringBuffer.append("location = ");
        stringBuffer.append(this.f2966a);
        stringBuffer.append("; precise = ");
        stringBuffer.append(this.f2968c);
        stringBuffer.append("; confidence = ");
        stringBuffer.append(this.f2969d);
        stringBuffer.append("; level = ");
        stringBuffer.append(this.f2970e);
        return stringBuffer.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(this.f2966a);
        parcel.writeString(this.f2967b);
        parcel.writeInt(this.f2968c);
        parcel.writeInt(this.f2969d);
        parcel.writeString(this.f2970e);
    }
}
