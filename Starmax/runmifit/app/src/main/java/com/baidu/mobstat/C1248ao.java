package com.baidu.mobstat;

import android.app.Activity;
import android.os.Bundle;
import com.baidu.mobstat.ActivityLifeObserver;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.ao */
public class C1248ao {

    /* renamed from: com.baidu.mobstat.ao$a */
    public static class C1249a implements ActivityLifeObserver.IActivityLifeCallback {
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    /* renamed from: a */
    public static void m4591a(JSONObject jSONObject) {
    }
}
