package com.baidu.mobstat;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.baidu.mobstat.C1245an;
import com.baidu.mobstat.Config;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.ap */
public class C1250ap {

    /* renamed from: b */
    private static C1250ap f4296b = new C1250ap();

    /* renamed from: a */
    public C1252a f4297a;

    /* renamed from: c */
    private HandlerThread f4298c = new HandlerThread("fullTraceHandleThread");

    /* renamed from: d */
    private Handler f4299d;

    /* renamed from: e */
    private volatile int f4300e;

    /* renamed from: f */
    private int f4301f;

    /* renamed from: g */
    private JSONObject f4302g = new JSONObject();

    /* renamed from: h */
    private JSONArray f4303h = new JSONArray();

    /* renamed from: i */
    private JSONArray f4304i = new JSONArray();

    /* renamed from: j */
    private JSONArray f4305j = new JSONArray();

    /* renamed from: k */
    private JSONArray f4306k = new JSONArray();

    /* renamed from: com.baidu.mobstat.ap$a */
    public interface C1252a {
        /* renamed from: a */
        void mo13938a(JSONObject jSONObject);
    }

    /* renamed from: a */
    private void m4597a(JSONObject jSONObject) {
    }

    /* renamed from: a */
    public static C1250ap m4592a() {
        return f4296b;
    }

    private C1250ap() {
        this.f4298c.start();
        this.f4298c.setPriority(10);
        this.f4299d = new Handler(this.f4298c.getLooper());
    }

    /* renamed from: a */
    public void mo13933a(Context context, String str, String str2, int i, long j, String str3, JSONArray jSONArray, String str4, JSONArray jSONArray2, String str5, Map<String, String> map, boolean z, JSONObject jSONObject, String str6) {
        final Context context2 = context;
        final String str7 = str;
        final String str8 = str2;
        final int i2 = i;
        final long j2 = j;
        final String str9 = str3;
        final JSONArray jSONArray3 = jSONArray;
        final String str10 = str4;
        final JSONArray jSONArray4 = jSONArray2;
        final String str11 = str5;
        final Map<String, String> map2 = map;
        final boolean z2 = z;
        final JSONObject jSONObject2 = jSONObject;
        final String str12 = str6;
        C12511 r19 = r0;
        C12511 r0 = new Runnable() {
            /* class com.baidu.mobstat.C1250ap.C12511 */

            public void run() {
                long sessionStartTime = BDStatCore.instance().getSessionStartTime();
                if (sessionStartTime > 0) {
                    C1250ap.this.m4594a(context2, sessionStartTime, str7, str8, i2, j2, str9, jSONArray3, str10, jSONArray4, str11, map2, z2, jSONObject2, str12);
                }
            }
        };
        this.f4299d.post(r19);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m4594a(Context context, long j, String str, String str2, int i, long j2, String str3, JSONArray jSONArray, String str4, JSONArray jSONArray2, String str5, Map<String, String> map, boolean z, JSONObject jSONObject, String str6) {
        String c = C1259aw.m4655c(jSONArray);
        String d = C1259aw.m4658d(jSONArray2);
        Context context2 = context;
        m4595a(context2, EventAnalysis.getEvent(context, j, str, str2, i, j2, 0, "", null, null, C1259aw.m4645a(str3), C1259aw.m4645a(str4), str5, Config.EventViewType.EDIT.getValue(), 3, null, map, c, d, z, jSONObject, str6));
        mo13935b(context);
    }

    /* renamed from: a */
    private void m4595a(Context context, JSONObject jSONObject) {
        if (jSONObject != null) {
            if (C1258av.m4636c().mo13945b()) {
                C1258av c = C1258av.m4636c();
                c.mo13941a("putEvent: " + jSONObject.toString());
            }
            String jSONObject2 = jSONObject.toString();
            if (m4598a(context, jSONObject2)) {
                if (C1258av.m4636c().mo13945b()) {
                    C1258av c2 = C1258av.m4636c();
                    c2.mo13941a("checkExceedLogLimit exceed:true; mCacheLogSize: " + this.f4300e + "; addedSize:" + jSONObject2.length());
                }
                m4603c(context);
            }
            EventAnalysis.doEventMerge(this.f4303h, jSONObject);
        }
    }

    /* renamed from: a */
    private boolean m4598a(Context context, String str) {
        if ((str != null ? str.getBytes().length : 0) + this.f4300e > 184320) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public void mo13934a(Context context, boolean z) {
        if (z) {
            m4602c();
        }
        try {
            m4601b(context, this.f4302g);
        } catch (Exception unused) {
        }
        if (this.f4303h.length() != 0 || this.f4304i.length() != 0 || this.f4305j.length() != 0 || this.f4306k.length() != 0) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(Config.HEADER_PART, this.f4302g);
            } catch (Exception unused2) {
            }
            try {
                jSONObject.put(Config.PRINCIPAL_PART, this.f4304i);
            } catch (Exception unused3) {
            }
            try {
                jSONObject.put(Config.EVENT_PART, this.f4303h);
            } catch (Exception unused4) {
            }
            try {
                jSONObject.put(Config.FEED_LIST_PART, this.f4305j);
            } catch (Exception unused5) {
            }
            try {
                jSONObject.put("sv", this.f4306k);
            } catch (Exception unused6) {
            }
            try {
                jSONObject.put(Config.EVENT_PAGE_MAPPING, C1245an.m4584a().mo13927a(C1245an.C1247a.f4294b));
            } catch (Exception unused7) {
            }
            try {
                jSONObject.put(Config.EVENT_PATH_MAPPING, C1245an.m4584a().mo13927a(C1245an.C1247a.f4293a));
            } catch (Exception unused8) {
            }
            try {
                jSONObject.put(Config.FEED_LIST_MAPPING, C1245an.m4584a().mo13927a(C1245an.C1247a.f4295c));
            } catch (Exception unused9) {
            }
            m4604c(context, jSONObject);
            m4597a(jSONObject);
            String jSONObject2 = jSONObject.toString();
            if (C1258av.m4636c().mo13945b()) {
                C1258av c = C1258av.m4636c();
                c.mo13941a("saveCurrentCacheToSend content: " + jSONObject2);
            }
            m4600b(context, jSONObject2);
            mo13936b(context, !z);
        }
    }

    /* renamed from: b */
    public void mo13936b(Context context, boolean z) {
        this.f4302g = new JSONObject();
        mo13932a(context);
        this.f4304i = new JSONArray();
        this.f4303h = new JSONArray();
        this.f4305j = new JSONArray();
        this.f4306k = new JSONArray();
        if (!z) {
            C1245an.m4584a().mo13928b();
        }
        mo13935b(context);
    }

    /* renamed from: a */
    public void mo13932a(Context context) {
        CooperService.instance().getHeadObject().installHeader(context, this.f4302g);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    /* renamed from: b */
    public void mo13935b(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Config.HEADER_PART, new JSONObject(this.f4302g.toString()));
            jSONObject.put(Config.PRINCIPAL_PART, new JSONArray(this.f4304i.toString()));
            jSONObject.put(Config.EVENT_PART, new JSONArray(this.f4303h.toString()));
            jSONObject.put(Config.FEED_LIST_PART, new JSONArray(this.f4305j.toString()));
            jSONObject.put("sv", new JSONArray(this.f4306k.toString()));
            jSONObject.put(Config.EVENT_PAGE_MAPPING, C1245an.m4584a().mo13927a(C1245an.C1247a.f4294b));
            jSONObject.put(Config.EVENT_PATH_MAPPING, C1245an.m4584a().mo13927a(C1245an.C1247a.f4293a));
            jSONObject.put(Config.FEED_LIST_MAPPING, C1245an.m4584a().mo13927a(C1245an.C1247a.f4295c));
        } catch (Exception unused) {
        }
        String jSONObject2 = jSONObject.toString();
        int length = jSONObject2.getBytes().length;
        if (length < 184320) {
            this.f4300e = length;
            String s = C1276bh.m4746s(context);
            C1267ba.m4681a(context, s + Config.STAT_FULL_CACHE_FILE_NAME, jSONObject2, false);
        }
    }

    /* renamed from: b */
    private void m4601b(Context context, JSONObject jSONObject) {
        CooperService.instance().getHeadObject().installHeader(context, jSONObject);
        try {
            jSONObject.put("t", System.currentTimeMillis());
            jSONObject.put(Config.SEQUENCE_INDEX, this.f4301f);
            jSONObject.put("ss", BDStatCore.instance().getSessionStartTime());
            jSONObject.put("at", "1");
            jSONObject.put("sign", CooperService.instance().getUUID());
        } catch (Exception unused) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ap.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.mobstat.ap.a(org.json.JSONArray, org.json.JSONObject):org.json.JSONArray
      com.baidu.mobstat.ap.a(android.content.Context, org.json.JSONObject):void
      com.baidu.mobstat.ap.a(android.content.Context, java.lang.String):boolean
      com.baidu.mobstat.ap.a(android.content.Context, boolean):void */
    /* renamed from: c */
    private void m4603c(Context context) {
        this.f4304i = m4593a(this.f4304i, BDStatCore.instance().getPageSessionHead());
        mo13934a(context, false);
        m4599b();
    }

    /* renamed from: b */
    private void m4599b() {
        this.f4301f++;
    }

    /* renamed from: c */
    private void m4602c() {
        this.f4301f = 0;
    }

    /* renamed from: c */
    private void m4604c(Context context, JSONObject jSONObject) {
        if (jSONObject != null) {
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put(Config.TRACE_FAILED_CNT, 0);
            } catch (Exception unused) {
            }
            try {
                jSONObject.put(Config.TRACE_PART, jSONObject2);
            } catch (Exception unused2) {
            }
        }
    }

    /* renamed from: b */
    private void m4600b(Context context, String str) {
        LogSender.instance().saveLogData(context, str, true);
        if (this.f4297a != null) {
            try {
                this.f4297a.mo13938a(new JSONObject(str));
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: a */
    private JSONArray m4593a(JSONArray jSONArray, JSONObject jSONObject) {
        JSONObject jSONObject2;
        JSONArray jSONArray2;
        JSONObject jSONObject3;
        JSONObject jSONObject4;
        if (jSONObject == null || jSONArray == null || jSONObject.optLong("s") <= 0) {
            return jSONArray;
        }
        JSONArray jSONArray3 = new JSONArray();
        if (jSONArray.length() == 0) {
            try {
                jSONObject4 = new JSONObject(jSONObject.toString());
                try {
                    jSONObject4.put("p", new JSONArray());
                } catch (Exception unused) {
                }
            } catch (Exception unused2) {
                jSONObject4 = null;
            }
            if (jSONObject4 != null) {
                jSONArray3.put(jSONObject4);
            }
        } else {
            try {
                jSONObject2 = jSONArray.getJSONObject(0);
            } catch (Exception unused3) {
                jSONObject2 = null;
            }
            try {
                jSONArray2 = jSONObject2.getJSONArray("p");
            } catch (Exception unused4) {
                jSONArray2 = null;
            }
            try {
                jSONObject3 = new JSONObject(jSONObject.toString());
                if (jSONArray2 != null) {
                    try {
                        jSONObject3.put("p", jSONArray2);
                    } catch (Exception unused5) {
                    }
                }
            } catch (Exception unused6) {
                jSONObject3 = null;
            }
            if (jSONObject3 != null) {
                jSONArray3.put(jSONObject3);
            }
        }
        return jSONArray3;
    }
}
