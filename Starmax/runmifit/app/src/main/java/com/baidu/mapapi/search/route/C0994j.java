package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.route.MassTransitRouteLine;

/* renamed from: com.baidu.mapapi.search.route.j */
final class C0994j implements Parcelable.Creator<MassTransitRouteLine.TransitStep> {
    C0994j() {
    }

    /* renamed from: a */
    public MassTransitRouteLine.TransitStep createFromParcel(Parcel parcel) {
        return new MassTransitRouteLine.TransitStep(parcel);
    }

    /* renamed from: a */
    public MassTransitRouteLine.TransitStep[] newArray(int i) {
        return new MassTransitRouteLine.TransitStep[i];
    }
}
