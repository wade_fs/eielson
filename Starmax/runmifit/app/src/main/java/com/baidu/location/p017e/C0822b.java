package com.baidu.location.p017e;

import android.os.Build;
import android.os.Handler;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.baidu.location.p019g.C0855k;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/* renamed from: com.baidu.location.e.b */
public class C0822b {

    /* renamed from: a */
    public static int f1729a;

    /* renamed from: b */
    public static int f1730b;

    /* renamed from: c */
    private static C0822b f1731c;

    /* renamed from: k */
    private static Class<?> f1732k;

    /* renamed from: d */
    private TelephonyManager f1733d = null;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C0821a f1734e = new C0821a();

    /* renamed from: f */
    private C0821a f1735f = null;

    /* renamed from: g */
    private List<C0821a> f1736g = null;

    /* renamed from: h */
    private C0823a f1737h = null;

    /* renamed from: i */
    private boolean f1738i = false;

    /* renamed from: j */
    private boolean f1739j = false;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public Handler f1740l = new Handler();

    /* renamed from: com.baidu.location.e.b$a */
    private class C0823a extends PhoneStateListener {
        public C0823a() {
        }

        public void onCellLocationChanged(CellLocation cellLocation) {
            if (cellLocation != null) {
                C0822b.this.f1740l.post(new C0824c(this));
            }
        }

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            C0821a c;
            int cdmaDbm;
            if (C0822b.this.f1734e != null) {
                if (C0822b.this.f1734e.f1726i == 'g') {
                    c = C0822b.this.f1734e;
                    cdmaDbm = signalStrength.getGsmSignalStrength();
                } else if (C0822b.this.f1734e.f1726i == 'c') {
                    c = C0822b.this.f1734e;
                    cdmaDbm = signalStrength.getCdmaDbm();
                } else {
                    return;
                }
                c.f1725h = cdmaDbm;
            }
        }
    }

    private C0822b() {
    }

    /* renamed from: a */
    private int m2275a(int i) {
        if (i == Integer.MAX_VALUE) {
            return -1;
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d4, code lost:
        if (r2 <= 0) goto L_0x0117;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x015f */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0121 A[Catch:{ Exception -> 0x015f }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.baidu.location.p017e.C0821a m2276a(android.telephony.CellInfo r10) {
        /*
            r9 = this;
            int r0 = android.os.Build.VERSION.SDK_INT
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            int r0 = r0.intValue()
            r1 = 17
            if (r0 >= r1) goto L_0x0010
            r10 = 0
            return r10
        L_0x0010:
            com.baidu.location.e.a r1 = new com.baidu.location.e.a
            r1.<init>()
            boolean r2 = r10 instanceof android.telephony.CellInfoGsm
            r3 = 0
            r4 = 1
            r5 = 103(0x67, float:1.44E-43)
            if (r2 == 0) goto L_0x005a
            r2 = r10
            android.telephony.CellInfoGsm r2 = (android.telephony.CellInfoGsm) r2
            android.telephony.CellIdentityGsm r3 = r2.getCellIdentity()
            int r6 = r3.getMcc()
            int r6 = r9.m2275a(r6)
            r1.f1720c = r6
            int r6 = r3.getMnc()
            int r6 = r9.m2275a(r6)
            r1.f1721d = r6
            int r6 = r3.getLac()
            int r6 = r9.m2275a(r6)
            r1.f1718a = r6
            int r3 = r3.getCid()
            int r3 = r9.m2275a(r3)
            r1.f1719b = r3
            r1.f1726i = r5
            android.telephony.CellSignalStrengthGsm r2 = r2.getCellSignalStrength()
            int r2 = r2.getAsuLevel()
        L_0x0056:
            r1.f1725h = r2
            goto L_0x0117
        L_0x005a:
            boolean r2 = r10 instanceof android.telephony.CellInfoCdma
            if (r2 == 0) goto L_0x00d7
            r2 = r10
            android.telephony.CellInfoCdma r2 = (android.telephony.CellInfoCdma) r2
            android.telephony.CellIdentityCdma r6 = r2.getCellIdentity()
            int r7 = r6.getLatitude()
            r1.f1722e = r7
            int r7 = r6.getLongitude()
            r1.f1723f = r7
            int r7 = r6.getSystemId()
            int r7 = r9.m2275a(r7)
            r1.f1721d = r7
            int r7 = r6.getNetworkId()
            int r7 = r9.m2275a(r7)
            r1.f1718a = r7
            int r6 = r6.getBasestationId()
            int r6 = r9.m2275a(r6)
            r1.f1719b = r6
            r6 = 99
            r1.f1726i = r6
            android.telephony.CellSignalStrengthCdma r2 = r2.getCellSignalStrength()
            int r2 = r2.getCdmaDbm()
            r1.f1725h = r2
            com.baidu.location.e.a r2 = r9.f1734e
            if (r2 == 0) goto L_0x00ac
            int r2 = r2.f1720c
            if (r2 <= 0) goto L_0x00ac
            com.baidu.location.e.a r2 = r9.f1734e
            int r2 = r2.f1720c
        L_0x00a9:
            r1.f1720c = r2
            goto L_0x0117
        L_0x00ac:
            r2 = -1
            android.telephony.TelephonyManager r6 = r9.f1733d     // Catch:{ Exception -> 0x00d3 }
            java.lang.String r6 = r6.getNetworkOperator()     // Catch:{ Exception -> 0x00d3 }
            if (r6 == 0) goto L_0x00d4
            int r7 = r6.length()     // Catch:{ Exception -> 0x00d3 }
            if (r7 <= 0) goto L_0x00d4
            int r7 = r6.length()     // Catch:{ Exception -> 0x00d3 }
            r8 = 3
            if (r7 < r8) goto L_0x00d4
            java.lang.String r3 = r6.substring(r3, r8)     // Catch:{ Exception -> 0x00d3 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x00d3 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x00d3 }
            if (r3 >= 0) goto L_0x00d1
            goto L_0x00d4
        L_0x00d1:
            r2 = r3
            goto L_0x00d4
        L_0x00d3:
        L_0x00d4:
            if (r2 <= 0) goto L_0x0117
            goto L_0x00a9
        L_0x00d7:
            boolean r2 = r10 instanceof android.telephony.CellInfoLte
            if (r2 == 0) goto L_0x0116
            r2 = r10
            android.telephony.CellInfoLte r2 = (android.telephony.CellInfoLte) r2
            android.telephony.CellIdentityLte r3 = r2.getCellIdentity()
            int r6 = r3.getMcc()
            int r6 = r9.m2275a(r6)
            r1.f1720c = r6
            int r6 = r3.getMnc()
            int r6 = r9.m2275a(r6)
            r1.f1721d = r6
            int r6 = r3.getTac()
            int r6 = r9.m2275a(r6)
            r1.f1718a = r6
            int r3 = r3.getCi()
            int r3 = r9.m2275a(r3)
            r1.f1719b = r3
            r1.f1726i = r5
            android.telephony.CellSignalStrengthLte r2 = r2.getCellSignalStrength()
            int r2 = r2.getAsuLevel()
            goto L_0x0056
        L_0x0116:
            r4 = 0
        L_0x0117:
            r2 = 18
            if (r0 < r2) goto L_0x015f
            if (r4 != 0) goto L_0x015f
            boolean r0 = r10 instanceof android.telephony.CellInfoWcdma     // Catch:{ Exception -> 0x015f }
            if (r0 == 0) goto L_0x015f
            r0 = r10
            android.telephony.CellInfoWcdma r0 = (android.telephony.CellInfoWcdma) r0     // Catch:{ Exception -> 0x015f }
            android.telephony.CellIdentityWcdma r0 = r0.getCellIdentity()     // Catch:{ Exception -> 0x015f }
            int r2 = r0.getMcc()     // Catch:{ Exception -> 0x015f }
            int r2 = r9.m2275a(r2)     // Catch:{ Exception -> 0x015f }
            r1.f1720c = r2     // Catch:{ Exception -> 0x015f }
            int r2 = r0.getMnc()     // Catch:{ Exception -> 0x015f }
            int r2 = r9.m2275a(r2)     // Catch:{ Exception -> 0x015f }
            r1.f1721d = r2     // Catch:{ Exception -> 0x015f }
            int r2 = r0.getLac()     // Catch:{ Exception -> 0x015f }
            int r2 = r9.m2275a(r2)     // Catch:{ Exception -> 0x015f }
            r1.f1718a = r2     // Catch:{ Exception -> 0x015f }
            int r0 = r0.getCid()     // Catch:{ Exception -> 0x015f }
            int r0 = r9.m2275a(r0)     // Catch:{ Exception -> 0x015f }
            r1.f1719b = r0     // Catch:{ Exception -> 0x015f }
            r1.f1726i = r5     // Catch:{ Exception -> 0x015f }
            r0 = r10
            android.telephony.CellInfoWcdma r0 = (android.telephony.CellInfoWcdma) r0     // Catch:{ Exception -> 0x015f }
            android.telephony.CellSignalStrengthWcdma r0 = r0.getCellSignalStrength()     // Catch:{ Exception -> 0x015f }
            int r0 = r0.getAsuLevel()     // Catch:{ Exception -> 0x015f }
            r1.f1725h = r0     // Catch:{ Exception -> 0x015f }
        L_0x015f:
            long r2 = android.os.SystemClock.elapsedRealtimeNanos()     // Catch:{ Error -> 0x0174 }
            long r4 = r10.getTimeStamp()     // Catch:{ Error -> 0x0174 }
            long r2 = r2 - r4
            r4 = 1000000(0xf4240, double:4.940656E-318)
            long r2 = r2 / r4
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Error -> 0x0174 }
            long r4 = r4 - r2
            r1.f1724g = r4     // Catch:{ Error -> 0x0174 }
            goto L_0x017a
        L_0x0174:
            long r2 = java.lang.System.currentTimeMillis()
            r1.f1724g = r2
        L_0x017a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0822b.m2276a(android.telephony.CellInfo):com.baidu.location.e.a");
    }

    /* renamed from: a */
    private C0821a m2277a(CellLocation cellLocation) {
        return m2278a(cellLocation, false);
    }

    /* renamed from: a */
    private C0821a m2278a(CellLocation cellLocation, boolean z) {
        if (cellLocation == null || this.f1733d == null) {
            return null;
        }
        C0821a aVar = new C0821a();
        if (z) {
            aVar.mo10621f();
        }
        aVar.f1724g = System.currentTimeMillis();
        try {
            String networkOperator = this.f1733d.getNetworkOperator();
            if (networkOperator != null && networkOperator.length() > 0) {
                int i = -1;
                if (networkOperator.length() >= 3) {
                    i = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
                    aVar.f1720c = i < 0 ? this.f1734e.f1720c : i;
                }
                String substring = networkOperator.substring(3);
                if (substring != null) {
                    char[] charArray = substring.toCharArray();
                    int i2 = 0;
                    while (true) {
                        if (i2 >= charArray.length) {
                            break;
                        } else if (!Character.isDigit(charArray[i2])) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                    i = Integer.valueOf(substring.substring(0, i2)).intValue();
                }
                if (i < 0) {
                    i = this.f1734e.f1721d;
                }
                aVar.f1721d = i;
            }
            f1729a = this.f1733d.getSimState();
        } catch (Exception unused) {
            f1730b = 1;
        }
        if (cellLocation instanceof GsmCellLocation) {
            GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
            aVar.f1718a = gsmCellLocation.getLac();
            aVar.f1719b = gsmCellLocation.getCid();
            aVar.f1726i = 'g';
        } else if (cellLocation instanceof CdmaCellLocation) {
            aVar.f1726i = 'c';
            if (f1732k == null) {
                try {
                    f1732k = Class.forName("android.telephony.cdma.CdmaCellLocation");
                } catch (Exception unused2) {
                    f1732k = null;
                    return aVar;
                }
            }
            Class<?> cls = f1732k;
            if (cls != null && cls.isInstance(cellLocation)) {
                try {
                    int systemId = ((CdmaCellLocation) cellLocation).getSystemId();
                    if (systemId < 0) {
                        systemId = this.f1734e.f1721d;
                    }
                    aVar.f1721d = systemId;
                    aVar.f1719b = ((CdmaCellLocation) cellLocation).getBaseStationId();
                    aVar.f1718a = ((CdmaCellLocation) cellLocation).getNetworkId();
                    int baseStationLatitude = ((CdmaCellLocation) cellLocation).getBaseStationLatitude();
                    if (baseStationLatitude < Integer.MAX_VALUE) {
                        aVar.f1722e = baseStationLatitude;
                    }
                    int baseStationLongitude = ((CdmaCellLocation) cellLocation).getBaseStationLongitude();
                    if (baseStationLongitude < Integer.MAX_VALUE) {
                        aVar.f1723f = baseStationLongitude;
                    }
                } catch (Exception unused3) {
                    f1730b = 3;
                    return aVar;
                }
            }
        }
        m2283c(aVar);
        return aVar;
    }

    /* renamed from: a */
    public static synchronized C0822b m2279a() {
        C0822b bVar;
        synchronized (C0822b.class) {
            if (f1731c == null) {
                f1731c = new C0822b();
            }
            bVar = f1731c;
        }
        return bVar;
    }

    /* renamed from: c */
    private void m2283c(C0821a aVar) {
        if (aVar.mo10617b()) {
            C0821a aVar2 = this.f1734e;
            if (aVar2 == null || !aVar2.mo10616a(aVar)) {
                this.f1734e = aVar;
                if (aVar.mo10617b()) {
                    int size = this.f1736g.size();
                    C0821a aVar3 = size == 0 ? null : this.f1736g.get(size - 1);
                    if (aVar3 == null || aVar3.f1719b != this.f1734e.f1719b || aVar3.f1718a != this.f1734e.f1718a) {
                        this.f1736g.add(this.f1734e);
                        if (this.f1736g.size() > 3) {
                            this.f1736g.remove(0);
                        }
                        m2286j();
                        this.f1739j = false;
                        return;
                    }
                    return;
                }
                List<C0821a> list = this.f1736g;
                if (list != null) {
                    list.clear();
                }
            }
        }
    }

    /* renamed from: d */
    private String m2284d(C0821a aVar) {
        StringBuilder sb;
        StringBuilder sb2 = new StringBuilder();
        if (Integer.valueOf(Build.VERSION.SDK_INT).intValue() >= 17) {
            try {
                List<CellInfo> allCellInfo = this.f1733d.getAllCellInfo();
                if (allCellInfo != null && allCellInfo.size() > 0) {
                    sb2.append("&nc=");
                    for (CellInfo cellInfo : allCellInfo) {
                        if (!cellInfo.isRegistered()) {
                            C0821a a = m2276a(cellInfo);
                            if (a != null) {
                                if (!(a.f1718a == -1 || a.f1719b == -1)) {
                                    if (aVar.f1718a != a.f1718a) {
                                        sb = new StringBuilder();
                                        sb.append(a.f1718a);
                                        sb.append("|");
                                        sb.append(a.f1719b);
                                        sb.append("|");
                                        sb.append(a.f1725h);
                                        sb.append(";");
                                    } else {
                                        sb = new StringBuilder();
                                        sb.append("|");
                                        sb.append(a.f1719b);
                                        sb.append("|");
                                        sb.append(a.f1725h);
                                        sb.append(";");
                                    }
                                    sb2.append(sb.toString());
                                }
                            }
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
        return sb2.toString();
    }

    /* renamed from: i */
    private void m2285i() {
        String i = C0855k.m2471i();
        if (i != null) {
            File file = new File(i + File.separator + "lcvif.dat");
            if (file.exists()) {
                try {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    long j = 0;
                    randomAccessFile.seek(0);
                    if (System.currentTimeMillis() - randomAccessFile.readLong() > 60000) {
                        randomAccessFile.close();
                        file.delete();
                        return;
                    }
                    randomAccessFile.readInt();
                    int i2 = 0;
                    while (i2 < 3) {
                        long readLong = randomAccessFile.readLong();
                        int readInt = randomAccessFile.readInt();
                        int readInt2 = randomAccessFile.readInt();
                        int readInt3 = randomAccessFile.readInt();
                        int readInt4 = randomAccessFile.readInt();
                        int readInt5 = randomAccessFile.readInt();
                        char c = readInt5 == 2 ? 'c' : readInt5 == 1 ? 'g' : 0;
                        if (readLong != j) {
                            C0821a aVar = r9;
                            C0821a aVar2 = new C0821a(readInt3, readInt4, readInt, readInt2, 0, c);
                            aVar.f1724g = readLong;
                            if (aVar.mo10617b()) {
                                this.f1739j = true;
                                this.f1736g.add(aVar);
                            }
                        }
                        i2++;
                        j = 0;
                    }
                    randomAccessFile.close();
                } catch (Exception unused) {
                    file.delete();
                }
            }
        }
    }

    /* renamed from: j */
    private void m2286j() {
        if (this.f1736g != null || this.f1735f != null) {
            if (this.f1736g == null && this.f1735f != null) {
                this.f1736g = new LinkedList();
                this.f1736g.add(this.f1735f);
            }
            String i = C0855k.m2471i();
            if (i != null && this.f1736g != null) {
                File file = new File(i + File.separator + "lcvif.dat");
                int size = this.f1736g.size();
                try {
                    if (file.exists()) {
                        file.delete();
                    }
                    file.createNewFile();
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(0);
                    randomAccessFile.writeLong(this.f1736g.get(size - 1).f1724g);
                    randomAccessFile.writeInt(size);
                    for (int i2 = 0; i2 < 3 - size; i2++) {
                        randomAccessFile.writeLong(0);
                        randomAccessFile.writeInt(-1);
                        randomAccessFile.writeInt(-1);
                        randomAccessFile.writeInt(-1);
                        randomAccessFile.writeInt(-1);
                        randomAccessFile.writeInt(2);
                    }
                    for (int i3 = 0; i3 < size; i3++) {
                        randomAccessFile.writeLong(this.f1736g.get(i3).f1724g);
                        randomAccessFile.writeInt(this.f1736g.get(i3).f1720c);
                        randomAccessFile.writeInt(this.f1736g.get(i3).f1721d);
                        randomAccessFile.writeInt(this.f1736g.get(i3).f1718a);
                        randomAccessFile.writeInt(this.f1736g.get(i3).f1719b);
                        if (this.f1736g.get(i3).f1726i == 'g') {
                            randomAccessFile.writeInt(1);
                        } else if (this.f1736g.get(i3).f1726i == 'c') {
                            randomAccessFile.writeInt(2);
                        } else {
                            randomAccessFile.writeInt(3);
                        }
                    }
                    randomAccessFile.close();
                } catch (Exception unused) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m2287k() {
        CellLocation cellLocation;
        C0821a l = m2288l();
        if (l != null) {
            m2283c(l);
        }
        if (l == null || !l.mo10617b()) {
            try {
                cellLocation = this.f1733d.getCellLocation();
            } catch (Throwable unused) {
                cellLocation = null;
            }
            if (cellLocation != null) {
                m2277a(cellLocation);
            }
        }
    }

    /* renamed from: l */
    private C0821a m2288l() {
        if (Integer.valueOf(Build.VERSION.SDK_INT).intValue() < 17) {
            return null;
        }
        try {
            f1729a = this.f1733d.getSimState();
            List<CellInfo> allCellInfo = this.f1733d.getAllCellInfo();
            if (allCellInfo == null || allCellInfo.size() <= 0) {
                return null;
            }
            C0821a aVar = null;
            for (CellInfo cellInfo : allCellInfo) {
                if (cellInfo.isRegistered()) {
                    boolean z = false;
                    if (aVar != null) {
                        z = true;
                    }
                    C0821a a = m2276a(cellInfo);
                    if (a != null) {
                        if (!a.mo10617b()) {
                            a = null;
                        } else if (z && aVar != null) {
                            aVar.f1727j = a.mo10624i();
                            return aVar;
                        }
                        if (aVar == null) {
                            aVar = a;
                        }
                    }
                }
            }
            return aVar;
        } catch (Throwable unused) {
            return null;
        }
    }

    /* renamed from: a */
    public String mo10625a(C0821a aVar) {
        String str;
        try {
            str = m2284d(aVar);
            int intValue = Integer.valueOf(Build.VERSION.SDK_INT).intValue();
            if ((str != null && !str.equals("") && !str.equals("&nc=")) || intValue >= 17) {
                return str;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            str = "";
        }
        if (str == null || !str.equals("&nc=")) {
            return str;
        }
        return null;
    }

    /* renamed from: b */
    public String mo10626b(C0821a aVar) {
        StringBuffer stringBuffer = new StringBuffer(128);
        stringBuffer.append("&nw=");
        stringBuffer.append(aVar.f1726i);
        stringBuffer.append(String.format(Locale.CHINA, "&cl=%d|%d|%d|%d&cl_s=%d", Integer.valueOf(aVar.f1720c), Integer.valueOf(aVar.f1721d), Integer.valueOf(aVar.f1718a), Integer.valueOf(aVar.f1719b), Integer.valueOf(aVar.f1725h)));
        if (aVar.f1722e < Integer.MAX_VALUE && aVar.f1723f < Integer.MAX_VALUE) {
            Locale locale = Locale.CHINA;
            double d = (double) aVar.f1723f;
            Double.isNaN(d);
            double d2 = (double) aVar.f1722e;
            Double.isNaN(d2);
            stringBuffer.append(String.format(locale, "&cdmall=%.6f|%.6f", Double.valueOf(d / 14400.0d), Double.valueOf(d2 / 14400.0d)));
        }
        stringBuffer.append("&cl_t=");
        stringBuffer.append(aVar.f1724g);
        try {
            if (this.f1736g != null && this.f1736g.size() > 0) {
                int size = this.f1736g.size();
                stringBuffer.append("&clt=");
                for (int i = 0; i < size; i++) {
                    C0821a aVar2 = this.f1736g.get(i);
                    if (aVar2 != null) {
                        if (aVar2.f1720c != aVar.f1720c) {
                            stringBuffer.append(aVar2.f1720c);
                        }
                        stringBuffer.append("|");
                        if (aVar2.f1721d != aVar.f1721d) {
                            stringBuffer.append(aVar2.f1721d);
                        }
                        stringBuffer.append("|");
                        if (aVar2.f1718a != aVar.f1718a) {
                            stringBuffer.append(aVar2.f1718a);
                        }
                        stringBuffer.append("|");
                        if (aVar2.f1719b != aVar.f1719b) {
                            stringBuffer.append(aVar2.f1719b);
                        }
                        stringBuffer.append("|");
                        stringBuffer.append((System.currentTimeMillis() - aVar2.f1724g) / 1000);
                        stringBuffer.append(";");
                    }
                }
            }
        } catch (Exception unused) {
        }
        if (f1729a > 100) {
            f1729a = 0;
        }
        int i2 = f1729a + (f1730b << 8);
        stringBuffer.append("&cs=" + i2);
        if (aVar.f1727j != null) {
            stringBuffer.append(aVar.f1727j);
        }
        return stringBuffer.toString();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:17|18|19|20|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x003f */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo10627b() {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.f1738i     // Catch:{ all -> 0x0045 }
            r1 = 1
            if (r0 != r1) goto L_0x0008
            monitor-exit(r4)
            return
        L_0x0008:
            boolean r0 = com.baidu.location.C0839f.isServing     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x000e
            monitor-exit(r4)
            return
        L_0x000e:
            android.content.Context r0 = com.baidu.location.C0839f.getServiceContext()     // Catch:{ all -> 0x0045 }
            java.lang.String r2 = "phone"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ all -> 0x0045 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ all -> 0x0045 }
            r4.f1733d = r0     // Catch:{ all -> 0x0045 }
            java.util.LinkedList r0 = new java.util.LinkedList     // Catch:{ all -> 0x0045 }
            r0.<init>()     // Catch:{ all -> 0x0045 }
            r4.f1736g = r0     // Catch:{ all -> 0x0045 }
            com.baidu.location.e.b$a r0 = new com.baidu.location.e.b$a     // Catch:{ all -> 0x0045 }
            r0.<init>()     // Catch:{ all -> 0x0045 }
            r4.f1737h = r0     // Catch:{ all -> 0x0045 }
            r4.m2285i()     // Catch:{ all -> 0x0045 }
            android.telephony.TelephonyManager r0 = r4.f1733d     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x0043
            com.baidu.location.e.b$a r0 = r4.f1737h     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0036
            goto L_0x0043
        L_0x0036:
            android.telephony.TelephonyManager r0 = r4.f1733d     // Catch:{ Exception -> 0x003f }
            com.baidu.location.e.b$a r2 = r4.f1737h     // Catch:{ Exception -> 0x003f }
            r3 = 272(0x110, float:3.81E-43)
            r0.listen(r2, r3)     // Catch:{ Exception -> 0x003f }
        L_0x003f:
            r4.f1738i = r1     // Catch:{ all -> 0x0045 }
            monitor-exit(r4)
            return
        L_0x0043:
            monitor-exit(r4)
            return
        L_0x0045:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p017e.C0822b.mo10627b():void");
    }

    /* renamed from: c */
    public synchronized void mo10628c() {
        if (this.f1738i) {
            if (!(this.f1737h == null || this.f1733d == null)) {
                this.f1733d.listen(this.f1737h, 0);
            }
            this.f1737h = null;
            this.f1733d = null;
            this.f1736g.clear();
            this.f1736g = null;
            m2286j();
            this.f1738i = false;
        }
    }

    /* renamed from: d */
    public boolean mo10629d() {
        return this.f1739j;
    }

    /* renamed from: e */
    public int mo10630e() {
        TelephonyManager telephonyManager = this.f1733d;
        if (telephonyManager == null) {
            return 0;
        }
        try {
            return telephonyManager.getNetworkType();
        } catch (Exception unused) {
            return 0;
        }
    }

    /* renamed from: f */
    public C0821a mo10631f() {
        C0821a aVar = this.f1734e;
        if ((aVar == null || !aVar.mo10615a() || !this.f1734e.mo10617b()) && this.f1733d != null) {
            try {
                m2287k();
            } catch (Exception unused) {
            }
        }
        C0821a aVar2 = this.f1734e;
        if (aVar2 != null && aVar2.mo10620e()) {
            this.f1735f = null;
            this.f1735f = new C0821a(this.f1734e.f1718a, this.f1734e.f1719b, this.f1734e.f1720c, this.f1734e.f1721d, this.f1734e.f1725h, this.f1734e.f1726i);
        }
        C0821a aVar3 = this.f1734e;
        if (aVar3 != null && aVar3.mo10619d() && this.f1735f != null && this.f1734e.f1726i == 'g') {
            this.f1734e.f1721d = this.f1735f.f1721d;
            this.f1734e.f1720c = this.f1735f.f1720c;
        }
        return this.f1734e;
    }

    /* renamed from: g */
    public String mo10632g() {
        int i = -1;
        try {
            if (this.f1733d != null) {
                i = this.f1733d.getSimState();
            }
        } catch (Exception unused) {
        }
        return "&sim=" + i;
    }

    /* renamed from: h */
    public int mo10633h() {
        return 0;
    }
}
