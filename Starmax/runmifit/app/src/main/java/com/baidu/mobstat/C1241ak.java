package com.baidu.mobstat;

import android.content.Context;
import java.lang.Thread;

/* renamed from: com.baidu.mobstat.ak */
class C1241ak implements Thread.UncaughtExceptionHandler {

    /* renamed from: a */
    private static final C1241ak f4278a = new C1241ak();

    /* renamed from: b */
    private Thread.UncaughtExceptionHandler f4279b;

    /* renamed from: c */
    private Context f4280c;

    /* renamed from: a */
    public static C1241ak m4570a() {
        return f4278a;
    }

    private C1241ak() {
    }

    /* renamed from: a */
    public void mo13914a(Context context) {
        this.f4280c = context;
        if (this.f4279b == null) {
            this.f4279b = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        ExceptionAnalysis.getInstance().saveCrashInfo(this.f4280c, th, true);
        if (!this.f4279b.equals(this)) {
            this.f4279b.uncaughtException(thread, th);
        }
    }
}
