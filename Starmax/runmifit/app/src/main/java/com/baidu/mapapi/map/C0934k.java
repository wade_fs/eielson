package com.baidu.mapapi.map;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.map.k */
final class C0934k implements Parcelable.Creator<MapStatus> {
    C0934k() {
    }

    /* renamed from: a */
    public MapStatus createFromParcel(Parcel parcel) {
        return new MapStatus(parcel);
    }

    /* renamed from: a */
    public MapStatus[] newArray(int i) {
        return new MapStatus[i];
    }
}
