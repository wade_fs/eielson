package com.baidu.location.p013a;

import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.baidu.location.C0839f;
import com.baidu.location.Jni;
import com.baidu.location.p017e.C0821a;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0826e;
import com.baidu.location.p017e.C0834h;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ExecutorService;

/* renamed from: com.baidu.location.a.i */
public abstract class C0738i {

    /* renamed from: c */
    public static String f1325c;

    /* renamed from: a */
    public C0834h f1326a = null;

    /* renamed from: b */
    public C0821a f1327b = null;

    /* renamed from: d */
    final Handler f1328d = new C0739a();

    /* renamed from: e */
    private boolean f1329e = true;

    /* renamed from: f */
    private boolean f1330f = true;

    /* renamed from: g */
    private boolean f1331g = false;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public String f1332h = null;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public String f1333i = null;

    /* renamed from: com.baidu.location.a.i$a */
    public class C0739a extends Handler {
        public C0739a() {
        }

        public void handleMessage(Message message) {
            if (C0839f.isServing) {
                int i = message.what;
                if (i == 21) {
                    C0738i.this.mo10462a(message);
                } else if (i == 62 || i == 63) {
                    C0738i.this.mo10461a();
                }
            }
        }
    }

    /* renamed from: com.baidu.location.a.i$b */
    class C0740b extends C0849e {

        /* renamed from: a */
        String f1335a = null;

        /* renamed from: b */
        String f1336b = null;

        /* renamed from: c */
        long f1337c = 0;

        /* renamed from: d */
        long f1338d = 0;

        public C0740b() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1882h = C0855k.m2466e();
            if (!((!C0855k.f1964h && !C0855k.f1966j) || C0738i.this.f1332h == null || C0738i.this.f1333i == null)) {
                this.f1336b += String.format(Locale.CHINA, "&ki=%s&sn=%s", C0738i.this.f1332h, C0738i.this.f1333i);
            }
            String encodeTp4 = Jni.encodeTp4(this.f1336b);
            this.f1336b = null;
            if (this.f1335a == null) {
                this.f1335a = C0765w.m1959b();
            }
            this.f1885k.put("bloc", encodeTp4);
            if (this.f1335a != null) {
                this.f1885k.put("up", this.f1335a);
            }
            this.f1885k.put("trtm", String.format(Locale.CHINA, "%d", Long.valueOf(System.currentTimeMillis())));
        }

        /* renamed from: a */
        public void mo10465a(String str, long j) {
            this.f1336b = str;
            this.f1338d = System.currentTimeMillis();
            this.f1337c = j;
            ExecutorService b = C0762v.m1943a().mo10504b();
            if (C0855k.m2460b()) {
                mo10732a(b, false, null);
            } else if (b != null) {
                mo10731a(b, C0855k.f1962f);
            } else {
                mo10734c(C0855k.f1962f);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:35:0x00b6  */
        /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo10446a(boolean r10) {
            /*
                r9 = this;
                java.lang.String r0 = "HttpStatus error"
                r1 = 63
                if (r10 == 0) goto L_0x00a5
                java.lang.String r10 = r9.f1884j
                if (r10 == 0) goto L_0x00a5
                java.lang.String r10 = r9.f1884j     // Catch:{ Exception -> 0x00a5 }
                com.baidu.location.p013a.C0738i.f1325c = r10     // Catch:{ Exception -> 0x00a5 }
                com.baidu.location.BDLocation r2 = new com.baidu.location.BDLocation     // Catch:{ Exception -> 0x0043 }
                r2.<init>(r10)     // Catch:{ Exception -> 0x0043 }
                int r3 = r2.getLocType()     // Catch:{ Exception -> 0x0043 }
                r4 = 161(0xa1, float:2.26E-43)
                if (r3 != r4) goto L_0x0022
                com.baidu.location.a.h r3 = com.baidu.location.p013a.C0736h.m1819a()     // Catch:{ Exception -> 0x0043 }
                r3.mo10454a(r10)     // Catch:{ Exception -> 0x0043 }
            L_0x0022:
                com.baidu.location.e.b r10 = com.baidu.location.p017e.C0822b.m2279a()     // Catch:{ Exception -> 0x0043 }
                int r10 = r10.mo10633h()     // Catch:{ Exception -> 0x0043 }
                r2.setOperators(r10)     // Catch:{ Exception -> 0x0043 }
                com.baidu.location.a.n r10 = com.baidu.location.p013a.C0754n.m1911a()     // Catch:{ Exception -> 0x0043 }
                boolean r10 = r10.mo10495d()     // Catch:{ Exception -> 0x0043 }
                if (r10 == 0) goto L_0x0050
                com.baidu.location.a.n r10 = com.baidu.location.p013a.C0754n.m1911a()     // Catch:{ Exception -> 0x0043 }
                float r10 = r10.mo10496e()     // Catch:{ Exception -> 0x0043 }
                r2.setDirection(r10)     // Catch:{ Exception -> 0x0043 }
                goto L_0x0050
            L_0x0043:
                r10 = move-exception
                r10.printStackTrace()     // Catch:{ Exception -> 0x00a5 }
                com.baidu.location.BDLocation r2 = new com.baidu.location.BDLocation     // Catch:{ Exception -> 0x00a5 }
                r2.<init>()     // Catch:{ Exception -> 0x00a5 }
                r10 = 0
                r2.setLocType(r10)     // Catch:{ Exception -> 0x00a5 }
            L_0x0050:
                r10 = 0
                r9.f1335a = r10     // Catch:{ Exception -> 0x00a5 }
                int r10 = r2.getLocType()     // Catch:{ Exception -> 0x00a5 }
                if (r10 != 0) goto L_0x0079
                double r3 = r2.getLatitude()     // Catch:{ Exception -> 0x00a5 }
                r5 = 1
                int r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r10 != 0) goto L_0x0079
                double r3 = r2.getLongitude()     // Catch:{ Exception -> 0x00a5 }
                int r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r10 != 0) goto L_0x0079
                com.baidu.location.a.i r10 = com.baidu.location.p013a.C0738i.this     // Catch:{ Exception -> 0x00a5 }
                android.os.Handler r10 = r10.f1328d     // Catch:{ Exception -> 0x00a5 }
                android.os.Message r10 = r10.obtainMessage(r1)     // Catch:{ Exception -> 0x00a5 }
                r10.obj = r0     // Catch:{ Exception -> 0x00a5 }
            L_0x0075:
                r10.sendToTarget()     // Catch:{ Exception -> 0x00a5 }
                goto L_0x00b2
            L_0x0079:
                long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00a5 }
                long r5 = r9.f1338d     // Catch:{ Exception -> 0x00a5 }
                long r3 = r3 - r5
                r5 = 1000(0x3e8, double:4.94E-321)
                long r3 = r3 / r5
                r5 = 0
                int r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r10 >= 0) goto L_0x008a
                r3 = r5
            L_0x008a:
                long r7 = r9.f1337c     // Catch:{ Exception -> 0x00a5 }
                int r10 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
                if (r10 >= 0) goto L_0x0092
                r9.f1337c = r5     // Catch:{ Exception -> 0x00a5 }
            L_0x0092:
                long r5 = r9.f1337c     // Catch:{ Exception -> 0x00a5 }
                long r5 = r5 + r3
                r2.setDelayTime(r5)     // Catch:{ Exception -> 0x00a5 }
                com.baidu.location.a.i r10 = com.baidu.location.p013a.C0738i.this     // Catch:{ Exception -> 0x00a5 }
                android.os.Handler r10 = r10.f1328d     // Catch:{ Exception -> 0x00a5 }
                r3 = 21
                android.os.Message r10 = r10.obtainMessage(r3)     // Catch:{ Exception -> 0x00a5 }
                r10.obj = r2     // Catch:{ Exception -> 0x00a5 }
                goto L_0x0075
            L_0x00a5:
                com.baidu.location.a.i r10 = com.baidu.location.p013a.C0738i.this
                android.os.Handler r10 = r10.f1328d
                android.os.Message r10 = r10.obtainMessage(r1)
                r10.obj = r0
                r10.sendToTarget()
            L_0x00b2:
                java.util.Map r10 = r9.f1885k
                if (r10 == 0) goto L_0x00bb
                java.util.Map r10 = r9.f1885k
                r10.clear()
            L_0x00bb:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0738i.C0740b.mo10446a(boolean):void");
        }
    }

    /* renamed from: a */
    public String mo10460a(String str) {
        String m;
        C0834h hVar;
        if (this.f1332h == null) {
            this.f1332h = C0741j.m1845b(C0839f.getServiceContext());
        }
        if (this.f1333i == null) {
            this.f1333i = C0741j.m1846c(C0839f.getServiceContext());
        }
        C0821a aVar = this.f1327b;
        if (aVar == null || !aVar.mo10615a()) {
            this.f1327b = C0822b.m2279a().mo10631f();
        }
        C0834h hVar2 = this.f1326a;
        if (hVar2 == null || !hVar2.mo10683k()) {
            this.f1326a = C0835i.m2376a().mo10699p();
        }
        Location h = C0826e.m2306a().mo10647j() ? C0826e.m2306a().mo10645h() : null;
        C0821a aVar2 = this.f1327b;
        if ((aVar2 == null || aVar2.mo10619d() || this.f1327b.mo10618c()) && (((hVar = this.f1326a) == null || hVar.mo10664a() == 0) && h == null)) {
            return null;
        }
        String b = mo10463b();
        if (C0736h.m1819a().mo10457d() == -2) {
            b = b + "&imo=1";
        }
        int c = C0855k.m2462c(C0839f.getServiceContext());
        if (c >= 0) {
            b = b + "&lmd=" + c;
        }
        C0834h hVar3 = this.f1326a;
        if ((hVar3 == null || hVar3.mo10664a() == 0) && (m = C0835i.m2376a().mo10696m()) != null) {
            b = m + b;
        }
        String str2 = b;
        if (!this.f1330f) {
            return C0855k.m2453a(this.f1327b, this.f1326a, h, str2, 0);
        }
        this.f1330f = false;
        return C0855k.m2454a(this.f1327b, this.f1326a, h, str2, 0, true);
    }

    /* renamed from: a */
    public abstract void mo10461a();

    /* renamed from: a */
    public abstract void mo10462a(Message message);

    /* renamed from: b */
    public String mo10463b() {
        String str;
        String d = C0723a.m1729a().mo10432d();
        if (C0835i.m2384j()) {
            str = "&cn=32";
        } else {
            str = String.format(Locale.CHINA, "&cn=%d", Integer.valueOf(C0822b.m2279a().mo10630e()));
        }
        if (Build.VERSION.SDK_INT >= 18) {
            String d2 = C0855k.m2464d();
            if (!TextUtils.isEmpty(d2)) {
                str = str + "&qcip6c=" + d2;
            }
        }
        if (this.f1329e) {
            this.f1329e = false;
            String s = C0835i.m2376a().mo10702s();
            if (!TextUtils.isEmpty(s) && !s.equals(Config.DEF_MAC_ID)) {
                str = String.format(Locale.CHINA, "%s&mac=%s", str, s.replace(Config.TRACE_TODAY_VISIT_SPLIT, ""));
            }
            int i = Build.VERSION.SDK_INT;
        } else if (!this.f1331g) {
            String e = C0765w.m1964e();
            if (e != null) {
                str = str + e;
            }
            this.f1331g = true;
        }
        return str + d;
    }
}
