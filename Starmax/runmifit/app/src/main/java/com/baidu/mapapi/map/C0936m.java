package com.baidu.mapapi.map;

import android.view.View;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;

/* renamed from: com.baidu.mapapi.map.m */
class C0936m implements View.OnClickListener {

    /* renamed from: a */
    final /* synthetic */ MapView f2853a;

    C0936m(MapView mapView) {
        this.f2853a = mapView;
    }

    public void onClick(View view) {
        float f = this.f2853a.f2573e.mo13086a().f3527b;
        C1064ab E = this.f2853a.f2573e.mo13086a().mo12989E();
        E.f3414a -= 1.0f;
        if (E.f3414a >= f) {
            f = E.f3414a;
        }
        E.f3414a = f;
        BaiduMap.mapStatusReason |= 16;
        this.f2853a.f2573e.mo13086a().mo13016a(E, 300);
    }
}
