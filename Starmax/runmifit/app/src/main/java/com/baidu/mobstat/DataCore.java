package com.baidu.mobstat;

import android.content.Context;
import android.text.TextUtils;
import com.baidu.mobstat.StatService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataCore {

    /* renamed from: a */
    private static JSONObject f4117a = new JSONObject();

    /* renamed from: b */
    private static DataCore f4118b = new DataCore();

    /* renamed from: c */
    private JSONArray f4119c = new JSONArray();

    /* renamed from: d */
    private JSONArray f4120d = new JSONArray();

    /* renamed from: e */
    private JSONArray f4121e = new JSONArray();

    /* renamed from: f */
    private boolean f4122f = false;

    /* renamed from: g */
    private volatile int f4123g = 0;

    /* renamed from: h */
    private StatService.WearListener f4124h;

    /* renamed from: i */
    private JSONObject f4125i;

    /* renamed from: j */
    private Object f4126j = new Object();

    /* renamed from: b */
    private void m4429b(JSONObject jSONObject) {
    }

    public static DataCore instance() {
        return f4118b;
    }

    private DataCore() {
    }

    public void init(Context context) {
        instance().loadStatData(context);
        instance().loadLastSession(context);
        instance().installHeader(context);
    }

    public int getCacheFileSzie() {
        return this.f4123g;
    }

    public JSONObject getLogData() {
        return this.f4125i;
    }

    public void putSession(Session session) {
        putSession(session.constructJSONObject());
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|9|10|11|12|13) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0025 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void putSession(org.json.JSONObject r4) {
        /*
            r3 = this;
            if (r4 != 0) goto L_0x0003
            return
        L_0x0003:
            java.lang.String r0 = r4.toString()
            boolean r0 = r3.m4428a(r0)
            if (r0 == 0) goto L_0x0017
            com.baidu.mobstat.at r4 = com.baidu.mobstat.C1256at.m4629c()
            java.lang.String r0 = "[WARNING] data to put exceed limit, ignored"
            r4.mo13943b(r0)
            return
        L_0x0017:
            org.json.JSONArray r0 = r3.f4119c
            monitor-enter(r0)
            org.json.JSONArray r1 = r3.f4119c     // Catch:{ all -> 0x0027 }
            int r1 = r1.length()     // Catch:{ all -> 0x0027 }
            org.json.JSONArray r2 = r3.f4119c     // Catch:{ JSONException -> 0x0025 }
            r2.put(r1, r4)     // Catch:{ JSONException -> 0x0025 }
        L_0x0025:
            monitor-exit(r0)     // Catch:{ all -> 0x0027 }
            return
        L_0x0027:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0027 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.DataCore.putSession(org.json.JSONObject):void");
    }

    public void putSession(String str) {
        if (!TextUtils.isEmpty(str) && !str.equals(new JSONObject().toString())) {
            try {
                putSession(new JSONObject(str));
            } catch (JSONException unused) {
            }
        }
    }

    /* renamed from: a */
    private boolean m4428a(String str) {
        return (str.getBytes().length + BDStatCore.instance().getSessionSize()) + this.f4123g > 184320;
    }

    public void putEvent(Context context, JSONObject jSONObject) {
        if (jSONObject != null) {
            if (m4428a(jSONObject.toString())) {
                C1256at.m4629c().mo13943b("[WARNING] data to put exceed limit, ignored");
                return;
            }
            synchronized (this.f4120d) {
                EventAnalysis.doEventMerge(this.f4120d, jSONObject);
            }
        }
    }

    public void installHeader(Context context) {
        synchronized (f4117a) {
            CooperService.instance().getHeadObject().installHeader(context, f4117a);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    public void flush(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            synchronized (this.f4119c) {
                jSONObject.put(Config.PRINCIPAL_PART, new JSONArray(this.f4119c.toString()));
            }
            synchronized (this.f4120d) {
                jSONObject.put(Config.EVENT_PART, new JSONArray(this.f4120d.toString()));
            }
            synchronized (f4117a) {
                jSONObject.put(Config.HEADER_PART, new JSONObject(f4117a.toString()));
            }
        } catch (Exception unused) {
        }
        String jSONObject2 = jSONObject.toString();
        if (m4427a()) {
            C1256at.m4629c().mo13941a("[WARNING] stat cache exceed 184320 Bytes, ignored");
            return;
        }
        int length = jSONObject2.getBytes().length;
        if (length >= 184320) {
            m4426a(true);
            return;
        }
        this.f4123g = length;
        String s = C1276bh.m4746s(context);
        C1267ba.m4681a(context, s + Config.STAT_CACHE_FILE_NAME, jSONObject2, false);
        synchronized (this.f4121e) {
            C1267ba.m4681a(context, Config.LAST_AP_INFO_FILE_NAME, this.f4121e.toString(), false);
        }
    }

    /* renamed from: a */
    private void m4426a(boolean z) {
        this.f4122f = z;
    }

    /* renamed from: a */
    private boolean m4427a() {
        return this.f4122f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.mobstat.ba.a(android.content.Context, java.lang.String, java.lang.String, boolean):void */
    public void loadLastSession(Context context) {
        if (context != null) {
            String str = C1276bh.m4746s(context) + Config.LAST_SESSION_FILE_NAME;
            if (C1267ba.m4686c(context, str)) {
                String a = C1267ba.m4679a(context, str);
                if (!TextUtils.isEmpty(a)) {
                    C1267ba.m4681a(context, str, new JSONObject().toString(), false);
                    putSession(a);
                    flush(context);
                }
            }
        }
    }

    public void loadWifiData(Context context) {
        if (context != null && C1267ba.m4686c(context, Config.LAST_AP_INFO_FILE_NAME)) {
            try {
                JSONArray jSONArray = new JSONArray(C1267ba.m4679a(context, Config.LAST_AP_INFO_FILE_NAME));
                int length = jSONArray.length();
                if (length >= 10) {
                    JSONArray jSONArray2 = new JSONArray();
                    for (int i = length - 10; i < length; i++) {
                        jSONArray2.put(jSONArray.get(i));
                    }
                    jSONArray = jSONArray2;
                }
                String g = C1276bh.m4732g(1, context);
                if (!TextUtils.isEmpty(g)) {
                    jSONArray.put(g);
                }
                synchronized (this.f4121e) {
                    this.f4121e = jSONArray;
                }
            } catch (JSONException unused) {
            }
        }
    }

    public void loadStatData(Context context) {
        if (context != null) {
            String str = C1276bh.m4746s(context) + Config.STAT_CACHE_FILE_NAME;
            if (C1267ba.m4686c(context, str)) {
                String a = C1267ba.m4679a(context, str);
                if (!TextUtils.isEmpty(a)) {
                    JSONObject jSONObject = null;
                    try {
                        jSONObject = new JSONObject(a);
                    } catch (Exception unused) {
                    }
                    if (jSONObject != null) {
                        long currentTimeMillis = System.currentTimeMillis();
                        try {
                            JSONArray jSONArray = jSONObject.getJSONArray(Config.PRINCIPAL_PART);
                            if (jSONArray != null) {
                                for (int i = 0; i < jSONArray.length(); i++) {
                                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                                    if (currentTimeMillis - jSONObject2.getLong("s") <= Config.MAX_LOG_DATA_EXSIT_TIME) {
                                        putSession(jSONObject2);
                                    }
                                }
                            }
                        } catch (Exception unused2) {
                        }
                        try {
                            JSONArray jSONArray2 = jSONObject.getJSONArray(Config.EVENT_PART);
                            if (jSONArray2 != null) {
                                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                                    JSONObject jSONObject3 = jSONArray2.getJSONObject(i2);
                                    if (currentTimeMillis - jSONObject3.getLong("t") <= Config.MAX_LOG_DATA_EXSIT_TIME) {
                                        putEvent(context, jSONObject3);
                                    }
                                }
                            }
                        } catch (Exception unused3) {
                        }
                        try {
                            JSONObject jSONObject4 = jSONObject.getJSONObject(Config.HEADER_PART);
                            if (jSONObject4 != null) {
                                synchronized (f4117a) {
                                    f4117a = jSONObject4;
                                }
                            }
                        } catch (Exception unused4) {
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String constructLogWithEmptyBody(android.content.Context r7, java.lang.String r8) {
        /*
            r6 = this;
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            org.json.JSONObject r1 = new org.json.JSONObject
            r1.<init>()
            com.baidu.mobstat.CooperService r2 = com.baidu.mobstat.CooperService.instance()
            com.baidu.mobstat.HeadObject r2 = r2.getHeadObject()
            java.lang.String r3 = r2.f4153e
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 == 0) goto L_0x001e
            r2.installHeader(r7, r1)
            goto L_0x0021
        L_0x001e:
            r2.updateHeader(r7, r1)
        L_0x0021:
            org.json.JSONArray r7 = new org.json.JSONArray
            r7.<init>()
            long r2 = java.lang.System.currentTimeMillis()
            r4 = 0
            java.lang.String r5 = "t"
            r1.put(r5, r2)     // Catch:{ Exception -> 0x006b }
            java.lang.String r5 = "ss"
            r1.put(r5, r2)     // Catch:{ Exception -> 0x006b }
            java.lang.String r2 = "wl2"
            r1.put(r2, r7)     // Catch:{ Exception -> 0x006b }
            java.lang.String r2 = "sq"
            r3 = 0
            r1.put(r2, r3)     // Catch:{ Exception -> 0x006b }
            java.lang.String r2 = "sign"
            com.baidu.mobstat.CooperService r3 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x006b }
            java.lang.String r3 = r3.getUUID()     // Catch:{ Exception -> 0x006b }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x006b }
            java.lang.String r2 = "k"
            r1.put(r2, r8)     // Catch:{ Exception -> 0x006b }
            java.lang.String r8 = "he"
            r0.put(r8, r1)     // Catch:{ Exception -> 0x006b }
            java.lang.String r8 = "pr"
            r0.put(r8, r7)     // Catch:{  }
            java.lang.String r8 = "ev"
            r0.put(r8, r7)     // Catch:{  }
            java.lang.String r8 = "ex"
            r0.put(r8, r7)     // Catch:{  }
            java.lang.String r7 = r0.toString()
            return r7
        L_0x006b:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.DataCore.constructLogWithEmptyBody(android.content.Context, java.lang.String):java.lang.String");
    }

    /* renamed from: a */
    private void m4424a(Context context, JSONObject jSONObject, boolean z) {
        if (jSONObject != null) {
            JSONObject jSONObject2 = new JSONObject();
            boolean z2 = true;
            try {
                jSONObject2.put(Config.TRACE_APPLICATION_SESSION, z ? 1 : 0);
            } catch (Exception unused) {
            }
            try {
                jSONObject2.put(Config.TRACE_FAILED_CNT, 0);
            } catch (Exception unused2) {
            }
            try {
                jSONObject2.put(Config.TRACE_CIRCLE, C1243am.m4583a());
            } catch (Exception unused3) {
            }
            try {
                jSONObject.put(Config.TRACE_PART, jSONObject2);
            } catch (Exception unused4) {
                z2 = false;
            }
            if (z2) {
                m4423a(context, jSONObject, jSONObject2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001a  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m4423a(android.content.Context r7, org.json.JSONObject r8, org.json.JSONObject r9) {
        /*
            r6 = this;
            int r5 = r6.m4418a(r8)
            r0 = 0
            java.lang.String r2 = "he"
            org.json.JSONObject r8 = r8.getJSONObject(r2)     // Catch:{ Exception -> 0x0015 }
            if (r8 == 0) goto L_0x0015
            java.lang.String r2 = "ss"
            long r2 = r8.getLong(r2)     // Catch:{ Exception -> 0x0015 }
            goto L_0x0016
        L_0x0015:
            r2 = r0
        L_0x0016:
            int r8 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r8 != 0) goto L_0x0020
            long r0 = java.lang.System.currentTimeMillis()
            r3 = r0
            goto L_0x0021
        L_0x0020:
            r3 = r2
        L_0x0021:
            r0 = r6
            r1 = r7
            r2 = r9
            r0.m4422a(r1, r2, r3, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.DataCore.m4423a(android.content.Context, org.json.JSONObject, org.json.JSONObject):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0037 A[Catch:{ Exception -> 0x0056 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m4418a(org.json.JSONObject r10) {
        /*
            r9 = this;
            r0 = 0
            if (r10 != 0) goto L_0x0004
            return r0
        L_0x0004:
            r1 = 0
            java.lang.String r3 = "he"
            org.json.JSONObject r3 = r10.getJSONObject(r3)     // Catch:{ Exception -> 0x0022 }
            java.lang.String r4 = "sq"
            long r4 = r3.getLong(r4)     // Catch:{ Exception -> 0x0022 }
            java.lang.String r6 = "ss"
            long r6 = r3.getLong(r6)     // Catch:{ Exception -> 0x0022 }
            int r3 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r3 <= 0) goto L_0x0022
            int r3 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r3 != 0) goto L_0x0022
            r3 = 1
            goto L_0x0023
        L_0x0022:
            r3 = 0
        L_0x0023:
            java.lang.String r4 = "pr"
            org.json.JSONArray r10 = r10.getJSONArray(r4)     // Catch:{ Exception -> 0x0056 }
            if (r10 == 0) goto L_0x0056
            int r4 = r10.length()     // Catch:{ Exception -> 0x0056 }
            if (r4 == 0) goto L_0x0056
        L_0x0031:
            int r4 = r10.length()     // Catch:{ Exception -> 0x0056 }
            if (r0 >= r4) goto L_0x0056
            java.lang.Object r4 = r10.get(r0)     // Catch:{ Exception -> 0x0056 }
            org.json.JSONObject r4 = (org.json.JSONObject) r4     // Catch:{ Exception -> 0x0056 }
            java.lang.String r5 = "c"
            long r5 = r4.getLong(r5)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r7 = "e"
            long r7 = r4.getLong(r7)     // Catch:{ Exception -> 0x0056 }
            int r4 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r4 == 0) goto L_0x0053
            int r4 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r4 != 0) goto L_0x0053
            int r3 = r3 + 1
        L_0x0053:
            int r0 = r0 + 1
            goto L_0x0031
        L_0x0056:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.DataCore.m4418a(org.json.JSONObject):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:57:0x0133 A[SYNTHETIC, Splitter:B:57:0x0133] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x013d  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m4422a(android.content.Context r15, org.json.JSONObject r16, long r17, int r19) {
        /*
            r14 = this;
            r6 = r14
            r7 = r15
            r8 = r16
            r0 = r17
            com.baidu.mobstat.al r2 = com.baidu.mobstat.C1242al.m4572a()
            java.lang.Long r2 = r2.mo13916a(r15)
            long r2 = r2.longValue()
            r4 = 0
            int r9 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r9 > 0) goto L_0x0022
            if (r19 == 0) goto L_0x0022
            com.baidu.mobstat.al r2 = com.baidu.mobstat.C1242al.m4572a()
            r2.mo13917a(r15, r0)
            r2 = r0
        L_0x0022:
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            java.lang.String r3 = "first"
            r14.m4425a(r8, r3, r2)
            if (r19 == 0) goto L_0x005c
            com.baidu.mobstat.al r2 = com.baidu.mobstat.C1242al.m4572a()
            java.lang.Long r2 = r2.mo13919b(r15)
            long r2 = r2.longValue()
            long r9 = r0 - r2
            int r11 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r11 == 0) goto L_0x0046
            int r11 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r11 > 0) goto L_0x0046
            r2 = -1
            goto L_0x004d
        L_0x0046:
            int r11 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r11 != 0) goto L_0x004c
            r2 = r4
            goto L_0x004d
        L_0x004c:
            r2 = r9
        L_0x004d:
            com.baidu.mobstat.al r9 = com.baidu.mobstat.C1242al.m4572a()
            r9.mo13920b(r15, r0)
            com.baidu.mobstat.al r9 = com.baidu.mobstat.C1242al.m4572a()
            r9.mo13923c(r15, r2)
            goto L_0x0068
        L_0x005c:
            com.baidu.mobstat.al r2 = com.baidu.mobstat.C1242al.m4572a()
            java.lang.Long r2 = r2.mo13922c(r15)
            long r2 = r2.longValue()
        L_0x0068:
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            java.lang.String r3 = "session_last_interval"
            r14.m4425a(r8, r3, r2)
            com.baidu.mobstat.al r2 = com.baidu.mobstat.C1242al.m4572a()
            java.lang.String r2 = r2.mo13924d(r15)
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            r9 = 0
            java.lang.String r10 = ""
            java.lang.String r11 = ":"
            if (r3 != 0) goto L_0x009d
            boolean r3 = r2.contains(r11)
            if (r3 == 0) goto L_0x009d
            java.lang.String[] r2 = r2.split(r11)
            if (r2 == 0) goto L_0x009d
            int r3 = r2.length
            r12 = 2
            if (r3 != r12) goto L_0x009d
            r10 = r2[r9]
            r3 = 1
            r2 = r2[r3]
            r13 = r10
            r10 = r2
            r2 = r13
            goto L_0x009e
        L_0x009d:
            r2 = r10
        L_0x009e:
            boolean r3 = android.text.TextUtils.isEmpty(r10)
            if (r3 != 0) goto L_0x00ae
            java.lang.Integer r3 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00ad }
            int r9 = r3.intValue()     // Catch:{ Exception -> 0x00ad }
            goto L_0x00ae
        L_0x00ad:
        L_0x00ae:
            java.lang.String r0 = com.baidu.mobstat.C1278bi.m4752a(r17)
            boolean r1 = android.text.TextUtils.isEmpty(r2)
            if (r1 != 0) goto L_0x00c2
            boolean r1 = r0.equals(r2)
            if (r1 == 0) goto L_0x00bf
            goto L_0x00c2
        L_0x00bf:
            r1 = r19
            goto L_0x00c4
        L_0x00c2:
            int r1 = r19 + r9
        L_0x00c4:
            if (r19 == 0) goto L_0x00df
            com.baidu.mobstat.al r3 = com.baidu.mobstat.C1242al.m4572a()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r10.append(r0)
            r10.append(r11)
            r10.append(r1)
            java.lang.String r10 = r10.toString()
            r3.mo13918a(r15, r10)
        L_0x00df:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.String r3 = "session_today_cnt"
            r14.m4425a(r8, r3, r1)
            boolean r1 = android.text.TextUtils.isEmpty(r2)
            if (r1 != 0) goto L_0x00f8
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x00f8 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x00f8 }
            long r10 = (long) r1
            goto L_0x00f9
        L_0x00f8:
            r10 = r4
        L_0x00f9:
            java.lang.String r12 = "recent"
            int r1 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x0124
            boolean r1 = android.text.TextUtils.isEmpty(r2)
            if (r1 != 0) goto L_0x0124
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0124
            if (r19 == 0) goto L_0x0124
            long r4 = (long) r9
            r0 = r14
            r1 = r15
            r2 = r10
            org.json.JSONArray r0 = r0.m4419a(r1, r2, r4)
            com.baidu.mobstat.al r1 = com.baidu.mobstat.C1242al.m4572a()
            java.lang.String r2 = r0.toString()
            r1.mo13921b(r15, r2)
            r14.m4425a(r8, r12, r0)
            goto L_0x0145
        L_0x0124:
            com.baidu.mobstat.al r0 = com.baidu.mobstat.C1242al.m4572a()
            java.lang.String r0 = r0.mo13925e(r15)
            r1 = 0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x013b
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x013a }
            r2.<init>(r0)     // Catch:{ Exception -> 0x013a }
            r1 = r2
            goto L_0x013b
        L_0x013a:
        L_0x013b:
            if (r1 != 0) goto L_0x0142
            org.json.JSONArray r1 = new org.json.JSONArray
            r1.<init>()
        L_0x0142:
            r14.m4425a(r8, r12, r1)
        L_0x0145:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.DataCore.m4422a(android.content.Context, org.json.JSONObject, long, int):void");
    }

    /* renamed from: a */
    private JSONArray m4419a(Context context, long j, long j2) {
        List arrayList = new ArrayList();
        String e = C1242al.m4572a().mo13925e(context);
        boolean z = false;
        if (!TextUtils.isEmpty(e)) {
            try {
                JSONArray jSONArray = new JSONArray(e);
                if (jSONArray.length() != 0) {
                    for (int i = 0; i < jSONArray.length(); i++) {
                        arrayList.add((JSONObject) jSONArray.get(i));
                    }
                }
            } catch (Exception unused) {
            }
        }
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = true;
                break;
            }
            try {
                if (((JSONObject) it.next()).getLong(Config.TRACE_VISIT_RECENT_DAY) == j) {
                    break;
                }
            } catch (Exception unused2) {
            }
        }
        if (z) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(Config.TRACE_VISIT_RECENT_DAY, j);
                jSONObject.put(Config.TRACE_VISIT_RECENT_COUNT, j2);
                arrayList.add(jSONObject);
            } catch (Exception unused3) {
            }
        }
        int size = arrayList.size();
        if (size > 5) {
            arrayList = arrayList.subList(size - 5, size);
        }
        return new JSONArray((Collection) arrayList);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:2|(2:4|5)|6|7|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0013 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m4425a(org.json.JSONObject r3, java.lang.String r4, java.lang.Object r5) {
        /*
            r2 = this;
            if (r3 != 0) goto L_0x0003
            return
        L_0x0003:
            java.lang.String r0 = "visit"
            boolean r1 = r3.has(r0)
            if (r1 != 0) goto L_0x0013
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0013 }
            r1.<init>()     // Catch:{ Exception -> 0x0013 }
            r3.put(r0, r1)     // Catch:{ Exception -> 0x0013 }
        L_0x0013:
            java.lang.Object r3 = r3.get(r0)     // Catch:{ Exception -> 0x001c }
            org.json.JSONObject r3 = (org.json.JSONObject) r3     // Catch:{ Exception -> 0x001c }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x001c }
        L_0x001c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.DataCore.m4425a(org.json.JSONObject, java.lang.String, java.lang.Object):void");
    }

    public void saveLogDataAndSendForRaven(Context context) {
        synchronized (this.f4126j) {
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:57|58|59|60|61|62|63) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:65|66|67|68|69|70|71) */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00b6, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00bc, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x00b3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:65:0x00b9 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:92:0x00c9 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void saveLogData(android.content.Context r7, boolean r8, boolean r9, long r10, boolean r12) {
        /*
            r6 = this;
            com.baidu.mobstat.CooperService r0 = com.baidu.mobstat.CooperService.instance()
            com.baidu.mobstat.HeadObject r0 = r0.getHeadObject()
            if (r0 == 0) goto L_0x0036
            org.json.JSONObject r1 = com.baidu.mobstat.DataCore.f4117a
            monitor-enter(r1)
            java.lang.String r2 = r0.f4153e     // Catch:{ all -> 0x0033 }
            boolean r2 = android.text.TextUtils.isEmpty(r2)     // Catch:{ all -> 0x0033 }
            if (r2 == 0) goto L_0x001b
            org.json.JSONObject r2 = com.baidu.mobstat.DataCore.f4117a     // Catch:{ all -> 0x0033 }
            r0.installHeader(r7, r2)     // Catch:{ all -> 0x0033 }
            goto L_0x0020
        L_0x001b:
            org.json.JSONObject r2 = com.baidu.mobstat.DataCore.f4117a     // Catch:{ all -> 0x0033 }
            r0.updateHeader(r7, r2)     // Catch:{ all -> 0x0033 }
        L_0x0020:
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            java.lang.String r0 = r0.f4153e
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0036
            com.baidu.mobstat.at r7 = com.baidu.mobstat.C1256at.m4629c()
            java.lang.String r8 = "[WARNING] 无法找到有效APP Key, 请参考文档配置"
            r7.mo13946c(r8)
            return
        L_0x0033:
            r7 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0033 }
            throw r7
        L_0x0036:
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            org.json.JSONObject r1 = com.baidu.mobstat.DataCore.f4117a
            monitor-enter(r1)
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00cb }
            org.json.JSONObject r4 = com.baidu.mobstat.DataCore.f4117a     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r5 = "t"
            r4.put(r5, r2)     // Catch:{ Exception -> 0x00c9 }
            org.json.JSONObject r2 = com.baidu.mobstat.DataCore.f4117a     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r3 = "sq"
            if (r8 == 0) goto L_0x0051
            r4 = 0
            goto L_0x0052
        L_0x0051:
            r4 = 1
        L_0x0052:
            r2.put(r3, r4)     // Catch:{ Exception -> 0x00c9 }
            org.json.JSONObject r2 = com.baidu.mobstat.DataCore.f4117a     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r3 = "ss"
            r2.put(r3, r10)     // Catch:{ Exception -> 0x00c9 }
            org.json.JSONArray r10 = r6.f4121e     // Catch:{ Exception -> 0x00c9 }
            monitor-enter(r10)     // Catch:{ Exception -> 0x00c9 }
            org.json.JSONObject r11 = com.baidu.mobstat.DataCore.f4117a     // Catch:{ all -> 0x00c6 }
            java.lang.String r2 = "wl2"
            org.json.JSONArray r3 = r6.f4121e     // Catch:{ all -> 0x00c6 }
            r11.put(r2, r3)     // Catch:{ all -> 0x00c6 }
            monitor-exit(r10)     // Catch:{ all -> 0x00c6 }
            org.json.JSONObject r10 = com.baidu.mobstat.DataCore.f4117a     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r11 = "sign"
            com.baidu.mobstat.CooperService r2 = com.baidu.mobstat.CooperService.instance()     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r2 = r2.getUUID()     // Catch:{ Exception -> 0x00c9 }
            r10.put(r11, r2)     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r10 = "he"
            org.json.JSONObject r11 = com.baidu.mobstat.DataCore.f4117a     // Catch:{ Exception -> 0x00c9 }
            r0.put(r10, r11)     // Catch:{ Exception -> 0x00c9 }
            org.json.JSONArray r10 = r6.f4119c     // Catch:{ all -> 0x00cb }
            monitor-enter(r10)     // Catch:{ all -> 0x00cb }
            java.lang.String r11 = "pr"
            org.json.JSONArray r2 = r6.f4119c     // Catch:{ JSONException -> 0x00c1 }
            r0.put(r11, r2)     // Catch:{ JSONException -> 0x00c1 }
            org.json.JSONArray r11 = r6.f4120d     // Catch:{ all -> 0x00bf }
            monitor-enter(r11)     // Catch:{ all -> 0x00bf }
            java.lang.String r2 = "ev"
            org.json.JSONArray r3 = r6.f4120d     // Catch:{ JSONException -> 0x00b9 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x00b9 }
            java.lang.String r2 = "ex"
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ JSONException -> 0x00b3 }
            r3.<init>()     // Catch:{ JSONException -> 0x00b3 }
            r0.put(r2, r3)     // Catch:{ JSONException -> 0x00b3 }
            r6.m4424a(r7, r0, r9)     // Catch:{ all -> 0x00b7 }
            r6.m4429b(r0)     // Catch:{ all -> 0x00b7 }
            java.lang.String r9 = r0.toString()     // Catch:{ all -> 0x00b7 }
            r6.m4421a(r7, r9, r8, r12)     // Catch:{ all -> 0x00b7 }
            r6.f4125i = r0     // Catch:{ all -> 0x00b7 }
            r6.clearCache(r7)     // Catch:{ all -> 0x00b7 }
            monitor-exit(r11)     // Catch:{ all -> 0x00b7 }
            monitor-exit(r10)     // Catch:{ all -> 0x00bf }
            monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            return
        L_0x00b3:
            monitor-exit(r11)     // Catch:{ all -> 0x00b7 }
            monitor-exit(r10)     // Catch:{ all -> 0x00bf }
            monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            return
        L_0x00b7:
            r7 = move-exception
            goto L_0x00bd
        L_0x00b9:
            monitor-exit(r11)     // Catch:{ all -> 0x00b7 }
            monitor-exit(r10)     // Catch:{ all -> 0x00bf }
            monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            return
        L_0x00bd:
            monitor-exit(r11)     // Catch:{ all -> 0x00b7 }
            throw r7     // Catch:{ all -> 0x00bf }
        L_0x00bf:
            r7 = move-exception
            goto L_0x00c4
        L_0x00c1:
            monitor-exit(r10)     // Catch:{ all -> 0x00bf }
            monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            return
        L_0x00c4:
            monitor-exit(r10)     // Catch:{ all -> 0x00bf }
            throw r7     // Catch:{ all -> 0x00cb }
        L_0x00c6:
            r7 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x00c6 }
            throw r7     // Catch:{ Exception -> 0x00c9 }
        L_0x00c9:
            monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            return
        L_0x00cb:
            r7 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00cb }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.DataCore.saveLogData(android.content.Context, boolean, boolean, long, boolean):void");
    }

    /* renamed from: a */
    private void m4421a(Context context, String str, boolean z, boolean z2) {
        StatService.WearListener wearListener = this.f4124h;
        if (wearListener == null || !wearListener.onSendLogData(str)) {
            LogSender.instance().saveLogData(context, str, false);
            C1256at c = C1256at.m4629c();
            c.mo13941a("Save log: " + str);
            return;
        }
        C1256at c2 = C1256at.m4629c();
        c2.mo13941a("Log has been passed to app level, log: " + str);
    }

    public void clearCache(Context context) {
        m4426a(false);
        synchronized (f4117a) {
            f4117a = new JSONObject();
        }
        installHeader(context);
        m4420a(context);
    }

    /* renamed from: a */
    private void m4420a(Context context) {
        synchronized (this.f4120d) {
            this.f4120d = new JSONArray();
        }
        synchronized (this.f4119c) {
            this.f4119c = new JSONArray();
        }
        synchronized (this.f4121e) {
            this.f4121e = new JSONArray();
        }
        flush(context);
    }
}
