package com.baidu.mapapi.synchronization.histroytrace;

public class HistoryTraceQueryOptions {

    /* renamed from: a */
    private String f3228a = null;

    /* renamed from: b */
    private int f3229b = 0;

    /* renamed from: c */
    private String f3230c = null;

    /* renamed from: d */
    private String f3231d = null;

    /* renamed from: e */
    private int f3232e = 4;

    /* renamed from: f */
    private int f3233f = 5;

    public int getCurrentOrderState() {
        return this.f3233f;
    }

    public String getDriverId() {
        return this.f3231d;
    }

    public String getOrderId() {
        return this.f3228a;
    }

    public int getQueryOrderState() {
        return this.f3232e;
    }

    public int getRoleType() {
        return this.f3229b;
    }

    public String getUserId() {
        return this.f3230c;
    }

    public HistoryTraceQueryOptions setCurrentOrderState(int i) {
        this.f3233f = i;
        return this;
    }

    public HistoryTraceQueryOptions setDriverId(String str) {
        this.f3231d = str;
        return this;
    }

    public HistoryTraceQueryOptions setOrderId(String str) {
        this.f3228a = str;
        return this;
    }

    public HistoryTraceQueryOptions setQueryOrderState(int i) {
        this.f3232e = i;
        return this;
    }

    public HistoryTraceQueryOptions setRoleType(int i) {
        this.f3229b = i;
        return this;
    }

    public HistoryTraceQueryOptions setUserId(String str) {
        this.f3230c = str;
        return this;
    }
}
