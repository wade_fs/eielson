package com.baidu.mapapi.map;

import android.graphics.Point;
import android.view.ViewGroup;
import com.baidu.mapapi.model.LatLng;

public final class MapViewLayoutParams extends ViewGroup.LayoutParams {
    public static final int ALIGN_BOTTOM = 16;
    public static final int ALIGN_CENTER_HORIZONTAL = 4;
    public static final int ALIGN_CENTER_VERTICAL = 32;
    public static final int ALIGN_LEFT = 1;
    public static final int ALIGN_RIGHT = 2;
    public static final int ALIGN_TOP = 8;

    /* renamed from: a */
    LatLng f2594a;

    /* renamed from: b */
    Point f2595b;

    /* renamed from: c */
    ELayoutMode f2596c;

    /* renamed from: d */
    float f2597d;

    /* renamed from: e */
    float f2598e;

    /* renamed from: f */
    int f2599f;

    public static final class Builder {

        /* renamed from: a */
        private int f2600a;

        /* renamed from: b */
        private int f2601b;

        /* renamed from: c */
        private LatLng f2602c;

        /* renamed from: d */
        private Point f2603d;

        /* renamed from: e */
        private ELayoutMode f2604e = ELayoutMode.absoluteMode;

        /* renamed from: f */
        private int f2605f = 4;

        /* renamed from: g */
        private int f2606g = 16;

        /* renamed from: h */
        private int f2607h;

        public Builder align(int i, int i2) {
            if (i == 1 || i == 2 || i == 4) {
                this.f2605f = i;
            }
            if (i2 == 8 || i2 == 16 || i2 == 32) {
                this.f2606g = i2;
            }
            return this;
        }

        public MapViewLayoutParams build() {
            boolean z = true;
            if (this.f2604e != ELayoutMode.mapMode ? !(this.f2604e == ELayoutMode.absoluteMode && this.f2603d == null) : this.f2602c != null) {
                z = false;
            }
            if (!z) {
                return new MapViewLayoutParams(this.f2600a, this.f2601b, this.f2602c, this.f2603d, this.f2604e, this.f2605f, this.f2606g, this.f2607h);
            }
            throw new IllegalStateException("BDMapSDKException: if it is map mode, you must supply position info; else if it is absolute mode, you must supply the point info");
        }

        public Builder height(int i) {
            this.f2601b = i;
            return this;
        }

        public Builder layoutMode(ELayoutMode eLayoutMode) {
            this.f2604e = eLayoutMode;
            return this;
        }

        public Builder point(Point point) {
            this.f2603d = point;
            return this;
        }

        public Builder position(LatLng latLng) {
            this.f2602c = latLng;
            return this;
        }

        public Builder width(int i) {
            this.f2600a = i;
            return this;
        }

        public Builder yOffset(int i) {
            this.f2607h = i;
            return this;
        }
    }

    public enum ELayoutMode {
        mapMode,
        absoluteMode
    }

    MapViewLayoutParams(int i, int i2, LatLng latLng, Point point, ELayoutMode eLayoutMode, int i3, int i4, int i5) {
        super(i, i2);
        this.f2594a = latLng;
        this.f2595b = point;
        this.f2596c = eLayoutMode;
        if (i3 == 1) {
            this.f2597d = 0.0f;
        } else if (i3 != 2) {
            this.f2597d = 0.5f;
        } else {
            this.f2597d = 1.0f;
        }
        if (i4 == 8) {
            this.f2598e = 0.0f;
        } else if (i4 == 16 || i4 != 32) {
            this.f2598e = 1.0f;
        } else {
            this.f2598e = 0.5f;
        }
        this.f2599f = i5;
    }
}
