package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.e */
class C1033e implements Animator.AnimatorListener {

    /* renamed from: a */
    final /* synthetic */ C1032d f3320a;

    C1033e(C1032d dVar) {
        this.f3320a = dVar;
    }

    public void onAnimationCancel(Animator animator) {
        if (this.f3320a.f3317d != null) {
            this.f3320a.f3317d.onAnimationCancel();
        }
    }

    public void onAnimationEnd(Animator animator) {
        if (this.f3320a.f3317d != null) {
            this.f3320a.f3317d.onAnimationEnd();
        }
    }

    public void onAnimationRepeat(Animator animator) {
        if (this.f3320a.f3317d != null) {
            this.f3320a.f3317d.onAnimationRepeat();
        }
    }

    public void onAnimationStart(Animator animator) {
        if (this.f3320a.f3317d != null) {
            this.f3320a.f3317d.onAnimationStart();
        }
    }
}
