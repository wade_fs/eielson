package com.baidu.location.indoor;

import com.baidu.location.BDLocation;

/* renamed from: com.baidu.location.indoor.r */
class C0912r implements Runnable {

    /* renamed from: a */
    final /* synthetic */ C0908p f2319a;

    C0912r(C0908p pVar) {
        this.f2319a = pVar;
    }

    public void run() {
        if (!(this.f2319a.f2309j == null || this.f2319a.f2300a == null)) {
            BDLocation bDLocation = new BDLocation(this.f2319a.f2302c);
            bDLocation.setLatitude(this.f2319a.f2309j.getLatitude());
            bDLocation.setLongitude(this.f2319a.f2309j.getLongitude());
            this.f2319a.f2300a.mo10779a(bDLocation);
        }
        this.f2319a.f2312m.postDelayed(this.f2319a.f2314o, this.f2319a.f2301b);
    }
}
