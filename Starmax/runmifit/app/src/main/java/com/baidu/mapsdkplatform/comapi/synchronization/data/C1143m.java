package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.support.v4.app.NotificationCompat;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mapsdkplatform.comapi.synchronization.data.C1132f;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1119f;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1122c;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1123d;
import com.baidu.mapsdkplatform.comapi.util.PermissionCheck;
import com.baidu.mapsdkplatform.comapi.util.SyncSysInfo;
import com.baidu.mapsdkplatform.comjni.util.AppMD5;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.m */
public class C1143m {

    /* renamed from: a */
    private static final String f3775a = C1143m.class.getSimpleName();

    /* renamed from: e */
    private static boolean f3776e = true;

    /* renamed from: b */
    private C1123d f3777b = new C1123d();

    /* renamed from: c */
    private boolean f3778c = true;

    /* renamed from: d */
    private boolean f3779d = true;

    public C1143m(C1132f fVar) {
        m4003a(fVar);
    }

    /* renamed from: a */
    private void m4003a(C1132f fVar) {
        this.f3777b.mo13200a("order_id", m4004b(fVar));
        this.f3777b.mo13200a("company", fVar.mo13279c());
        this.f3777b.mo13200a("order_attr", fVar.mo13277b());
        this.f3777b.mo13200a(NotificationCompat.CATEGORY_STATUS, String.valueOf(fVar.mo13287h()));
        this.f3777b.mo13200a("pull_type", String.valueOf(fVar.mo13288i()));
        this.f3777b.mo13200a("route_finger", fVar.mo13281d());
        this.f3777b.mo13200a("traffic_finger", fVar.mo13283e());
        this.f3777b.mo13200a("pos_num", String.valueOf(fVar.mo13289j()));
        m4007c(fVar);
        m4008d(fVar);
        if (this.f3778c) {
            m4005b();
        }
    }

    /* renamed from: b */
    private String m4004b(C1132f fVar) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(fVar.mo13279c().toLowerCase());
        stringBuffer.append("-");
        stringBuffer.append(fVar.mo13274a().toLowerCase());
        stringBuffer.append("-");
        stringBuffer.append("9sc87244121ip32590fq234mn6641tx7".toLowerCase());
        String a = C1122c.m3858a(stringBuffer.toString());
        String str = f3775a;
        C1120a.m3851a(str, "The orderId = " + stringBuffer.toString() + "; result = " + a);
        return a;
    }

    /* renamed from: b */
    private void m4005b() {
        String authToken = SyncSysInfo.getAuthToken();
        if (authToken == null) {
            C1120a.m3855b(f3775a, "Token is null, permission check again");
            int permissionCheck = PermissionCheck.permissionCheck();
            if (permissionCheck != 0) {
                String str = f3775a;
                C1120a.m3855b(str, "Permission check result is: " + permissionCheck);
            }
            authToken = SyncSysInfo.getAuthToken();
        }
        this.f3777b.mo13200a("token", authToken);
    }

    /* renamed from: c */
    private String m4006c() {
        return f3776e ? C1119f.m3848a() : C1119f.m3849b();
    }

    /* renamed from: c */
    private void m4007c(C1132f fVar) {
        C1132f.C1134b g = fVar.mo13286g();
        if (C1132f.C1134b.DRIVING != g && C1132f.C1134b.RIDING == g) {
            this.f3777b.mo13200a("trip_mode", "riding");
        } else {
            this.f3777b.mo13200a("trip_mode", "driving");
        }
    }

    /* renamed from: d */
    private void m4008d(C1132f fVar) {
        C1123d dVar;
        String str;
        C1132f.C1133a f = fVar.mo13285f();
        if (C1132f.C1133a.BD09LL != f) {
            if (C1132f.C1133a.BD09MC == f) {
                dVar = this.f3777b;
                str = CoordinateType.BD09MC;
            } else if (C1132f.C1133a.GPS == f) {
                dVar = this.f3777b;
                str = CoordinateType.WGS84;
            } else if (C1132f.C1133a.COMMON == f) {
                dVar = this.f3777b;
                str = CoordinateType.GCJ02;
            }
            dVar.mo13200a("coord_type", str);
            return;
        }
        this.f3777b.mo13200a("coord_type", "bd09ll");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public String mo13323a() {
        StringBuffer stringBuffer = new StringBuffer(this.f3777b.mo13201a());
        stringBuffer.append(SyncSysInfo.getPhoneInfo());
        if (this.f3779d) {
            String signMD5String = AppMD5.getSignMD5String(stringBuffer.toString());
            stringBuffer.append("&sign=");
            stringBuffer.append(signMD5String);
        }
        StringBuffer stringBuffer2 = new StringBuffer(m4006c());
        stringBuffer2.append("?");
        stringBuffer2.append(stringBuffer);
        return stringBuffer2.toString();
    }
}
