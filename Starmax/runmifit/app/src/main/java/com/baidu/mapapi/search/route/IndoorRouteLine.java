package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.RouteNode;
import com.baidu.mapapi.search.core.RouteStep;
import java.util.ArrayList;
import java.util.List;

public class IndoorRouteLine extends RouteLine<IndoorRouteStep> {
    public static final Parcelable.Creator<IndoorRouteLine> CREATOR = new C0991g();

    public static class IndoorRouteStep extends RouteStep {

        /* renamed from: d */
        private RouteNode f3053d;

        /* renamed from: e */
        private RouteNode f3054e;

        /* renamed from: f */
        private String f3055f;

        /* renamed from: g */
        private String f3056g;

        /* renamed from: h */
        private String f3057h;

        /* renamed from: i */
        private List<IndoorStepNode> f3058i;

        /* renamed from: j */
        private List<Double> f3059j;

        public static class IndoorStepNode {

            /* renamed from: a */
            private String f3060a;

            /* renamed from: b */
            private int f3061b;

            /* renamed from: c */
            private LatLng f3062c;

            /* renamed from: d */
            private String f3063d;

            public String getDetail() {
                return this.f3063d;
            }

            public LatLng getLocation() {
                return this.f3062c;
            }

            public String getName() {
                return this.f3060a;
            }

            public int getType() {
                return this.f3061b;
            }

            public void setDetail(String str) {
                this.f3063d = str;
            }

            public void setLocation(LatLng latLng) {
                this.f3062c = latLng;
            }

            public void setName(String str) {
                this.f3060a = str;
            }

            public void setType(int i) {
                this.f3061b = i;
            }
        }

        /* renamed from: a */
        private List<LatLng> m3197a(List<Double> list) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i += 2) {
                arrayList.add(new LatLng(list.get(i).doubleValue(), list.get(i + 1).doubleValue()));
            }
            return arrayList;
        }

        public String getBuildingId() {
            return this.f3057h;
        }

        public RouteNode getEntrace() {
            return this.f3053d;
        }

        public RouteNode getExit() {
            return this.f3054e;
        }

        public String getFloorId() {
            return this.f3056g;
        }

        public String getInstructions() {
            return this.f3055f;
        }

        public List<IndoorStepNode> getStepNodes() {
            return this.f3058i;
        }

        public List<LatLng> getWayPoints() {
            if (this.mWayPoints == null) {
                this.mWayPoints = m3197a(this.f3059j);
            }
            return this.mWayPoints;
        }

        public void setBuildingId(String str) {
            this.f3057h = str;
        }

        public void setEntrace(RouteNode routeNode) {
            this.f3053d = routeNode;
        }

        public void setExit(RouteNode routeNode) {
            this.f3054e = routeNode;
        }

        public void setFloorId(String str) {
            this.f3056g = str;
        }

        public void setInstructions(String str) {
            this.f3055f = str;
        }

        public void setPath(List<Double> list) {
            this.f3059j = list;
        }

        public void setStepNodes(List<IndoorStepNode> list) {
            this.f3058i = list;
        }
    }

    public IndoorRouteLine() {
        setType(RouteLine.TYPE.WALKSTEP);
    }

    protected IndoorRouteLine(Parcel parcel) {
        super(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public List<IndoorRouteStep> getAllStep() {
        return super.getAllStep();
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
    }
}
