package com.baidu.mobstat;

import android.content.Context;
import android.text.TextUtils;

/* renamed from: com.baidu.mobstat.i */
class C1289i {

    /* renamed from: a */
    static C1289i f4359a = new C1289i();

    C1289i() {
    }

    /* renamed from: a */
    public synchronized void mo13970a(Context context) {
        String n = C1276bh.m4741n(context);
        if (!TextUtils.isEmpty(n)) {
            C1300r.AP_LIST.mo13990a(System.currentTimeMillis(), n);
        }
    }
}
