package com.zhy.autolayout.utils;

import android.util.Log;

/* renamed from: com.zhy.autolayout.utils.L */
public class C3266L {
    private static final String TAG = "AUTO_LAYOUT";
    public static boolean debug = false;

    /* renamed from: e */
    public static void m8138e(String str) {
        if (debug) {
            Log.e(TAG, str);
        }
    }
}
