package com.zhy.autolayout.attr;

import android.os.Build;
import android.view.View;
import java.lang.reflect.Field;

public class MinHeightAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 32768;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public MinHeightAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        try {
            view.setMinimumHeight(i);
        } catch (Exception unused) {
        }
    }

    public static MinHeightAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new MinHeightAttr(i, 32768, 0);
        }
        if (i2 == 2) {
            return new MinHeightAttr(i, 0, 32768);
        }
        if (i2 != 3) {
            return null;
        }
        return new MinHeightAttr(i, 0, 0);
    }

    public static int getMinHeight(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumHeight();
        }
        try {
            Field field = view.getClass().getField("mMinHeight");
            field.setAccessible(true);
            return ((Integer) field.get(view)).intValue();
        } catch (Exception unused) {
            return 0;
        }
    }
}
