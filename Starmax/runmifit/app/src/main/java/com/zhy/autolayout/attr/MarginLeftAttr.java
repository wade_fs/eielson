package com.zhy.autolayout.attr;

import android.view.View;
import android.view.ViewGroup;

public class MarginLeftAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 32;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return true;
    }

    public MarginLeftAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).leftMargin = i;
        }
    }

    public static MarginLeftAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new MarginLeftAttr(i, 32, 0);
        }
        if (i2 == 2) {
            return new MarginLeftAttr(i, 0, 32);
        }
        if (i2 != 3) {
            return null;
        }
        return new MarginLeftAttr(i, 0, 0);
    }
}
