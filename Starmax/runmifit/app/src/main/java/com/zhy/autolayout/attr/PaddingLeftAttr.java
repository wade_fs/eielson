package com.zhy.autolayout.attr;

import android.view.View;

public class PaddingLeftAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 512;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return true;
    }

    public PaddingLeftAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        view.setPadding(i, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
    }

    public static PaddingLeftAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new PaddingLeftAttr(i, 512, 0);
        }
        if (i2 == 2) {
            return new PaddingLeftAttr(i, 0, 512);
        }
        if (i2 != 3) {
            return null;
        }
        return new PaddingLeftAttr(i, 0, 0);
    }
}
