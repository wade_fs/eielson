package com.zhy.autolayout.attr;

import android.view.View;
import android.view.ViewGroup;

public class MarginTopAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 64;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public MarginTopAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).topMargin = i;
        }
    }

    public static MarginTopAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new MarginTopAttr(i, 64, 0);
        }
        if (i2 == 2) {
            return new MarginTopAttr(i, 0, 64);
        }
        if (i2 != 3) {
            return null;
        }
        return new MarginTopAttr(i, 0, 0);
    }
}
