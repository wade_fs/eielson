package com.zhy.autolayout.attr;

import android.view.View;
import android.widget.TextView;

public class TextSizeAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 4;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public TextSizeAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            textView.setIncludeFontPadding(false);
            textView.setTextSize(0, (float) i);
        }
    }

    public static TextSizeAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new TextSizeAttr(i, 4, 0);
        }
        if (i2 == 2) {
            return new TextSizeAttr(i, 0, 4);
        }
        if (i2 != 3) {
            return null;
        }
        return new TextSizeAttr(i, 0, 0);
    }
}
