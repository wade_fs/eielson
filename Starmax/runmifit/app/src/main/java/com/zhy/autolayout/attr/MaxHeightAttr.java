package com.zhy.autolayout.attr;

import android.view.View;

public class MaxHeightAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 65536;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public MaxHeightAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        try {
            view.getClass().getMethod("setMaxHeight", Integer.TYPE).invoke(view, Integer.valueOf(i));
        } catch (Exception unused) {
        }
    }

    public static MaxHeightAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new MaxHeightAttr(i, 65536, 0);
        }
        if (i2 == 2) {
            return new MaxHeightAttr(i, 0, 65536);
        }
        if (i2 != 3) {
            return null;
        }
        return new MaxHeightAttr(i, 0, 0);
    }

    public static int getMaxHeight(View view) {
        try {
            return ((Integer) view.getClass().getMethod("getMaxHeight", new Class[0]).invoke(view, new Object[0])).intValue();
        } catch (Exception unused) {
            return 0;
        }
    }
}
