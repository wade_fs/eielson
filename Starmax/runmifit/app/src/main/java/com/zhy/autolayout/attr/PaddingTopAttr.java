package com.zhy.autolayout.attr;

import android.view.View;

public class PaddingTopAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 1024;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public PaddingTopAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        view.setPadding(view.getPaddingLeft(), i, view.getPaddingRight(), view.getPaddingBottom());
    }

    public static PaddingTopAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new PaddingTopAttr(i, 1024, 0);
        }
        if (i2 == 2) {
            return new PaddingTopAttr(i, 0, 1024);
        }
        if (i2 != 3) {
            return null;
        }
        return new PaddingTopAttr(i, 0, 0);
    }
}
