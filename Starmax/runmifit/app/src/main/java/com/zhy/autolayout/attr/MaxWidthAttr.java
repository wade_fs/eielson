package com.zhy.autolayout.attr;

import android.view.View;

public class MaxWidthAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 16384;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return true;
    }

    public MaxWidthAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        try {
            view.getClass().getMethod("setMaxWidth", Integer.TYPE).invoke(view, Integer.valueOf(i));
        } catch (Exception unused) {
        }
    }

    public static MaxWidthAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new MaxWidthAttr(i, 16384, 0);
        }
        if (i2 == 2) {
            return new MaxWidthAttr(i, 0, 16384);
        }
        if (i2 != 3) {
            return null;
        }
        return new MaxWidthAttr(i, 0, 0);
    }

    public static int getMaxWidth(View view) {
        try {
            return ((Integer) view.getClass().getMethod("getMaxWidth", new Class[0]).invoke(view, new Object[0])).intValue();
        } catch (Exception unused) {
            return 0;
        }
    }
}
