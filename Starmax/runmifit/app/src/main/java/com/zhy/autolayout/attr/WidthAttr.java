package com.zhy.autolayout.attr;

import android.view.View;

public class WidthAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return true;
    }

    public WidthAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        view.getLayoutParams().width = i;
    }

    public static WidthAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new WidthAttr(i, 1, 0);
        }
        if (i2 == 2) {
            return new WidthAttr(i, 0, 1);
        }
        if (i2 != 3) {
            return null;
        }
        return new WidthAttr(i, 0, 0);
    }
}
