package com.zhy.autolayout.attr;

import android.view.View;

public class PaddingBottomAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 4096;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public PaddingBottomAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), i);
    }

    public static PaddingBottomAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new PaddingBottomAttr(i, 4096, 0);
        }
        if (i2 == 2) {
            return new PaddingBottomAttr(i, 0, 4096);
        }
        if (i2 != 3) {
            return null;
        }
        return new PaddingBottomAttr(i, 0, 0);
    }
}
