package com.zhy.autolayout.attr;

import android.view.View;

public class HeightAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public HeightAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        view.getLayoutParams().height = i;
    }

    public static HeightAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new HeightAttr(i, 2, 0);
        }
        if (i2 == 2) {
            return new HeightAttr(i, 0, 2);
        }
        if (i2 != 3) {
            return null;
        }
        return new HeightAttr(i, 0, 0);
    }
}
