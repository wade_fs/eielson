package com.zhy.autolayout.attr;

import android.view.View;
import android.view.ViewGroup;

public class MarginAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 16;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public MarginAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    public void apply(View view) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            if (useDefault()) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                int percentWidthSize = getPercentWidthSize();
                marginLayoutParams.rightMargin = percentWidthSize;
                marginLayoutParams.leftMargin = percentWidthSize;
                int percentHeightSize = getPercentHeightSize();
                marginLayoutParams.bottomMargin = percentHeightSize;
                marginLayoutParams.topMargin = percentHeightSize;
                return;
            }
            super.apply(view);
        }
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        marginLayoutParams.bottomMargin = i;
        marginLayoutParams.topMargin = i;
        marginLayoutParams.rightMargin = i;
        marginLayoutParams.leftMargin = i;
    }
}
