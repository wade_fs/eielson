package com.zhy.autolayout.attr;

import android.view.View;
import android.view.ViewGroup;

public class MarginRightAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 128;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return true;
    }

    public MarginRightAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).rightMargin = i;
        }
    }

    public static MarginRightAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new MarginRightAttr(i, 128, 0);
        }
        if (i2 == 2) {
            return new MarginRightAttr(i, 0, 128);
        }
        if (i2 != 3) {
            return null;
        }
        return new MarginRightAttr(i, 0, 0);
    }
}
