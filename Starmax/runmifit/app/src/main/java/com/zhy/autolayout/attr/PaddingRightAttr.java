package com.zhy.autolayout.attr;

import android.view.View;

public class PaddingRightAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 2048;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return true;
    }

    public PaddingRightAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), i, view.getPaddingBottom());
    }

    public static PaddingRightAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new PaddingRightAttr(i, 2048, 0);
        }
        if (i2 == 2) {
            return new PaddingRightAttr(i, 0, 2048);
        }
        if (i2 != 3) {
            return null;
        }
        return new PaddingRightAttr(i, 0, 0);
    }
}
