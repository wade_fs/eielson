package com.zhy.autolayout.attr;

public interface Attrs {
    public static final int HEIGHT = 2;
    public static final int MARGIN = 16;
    public static final int MARGIN_BOTTOM = 256;
    public static final int MARGIN_LEFT = 32;
    public static final int MARGIN_RIGHT = 128;
    public static final int MARGIN_TOP = 64;
    public static final int MAX_HEIGHT = 65536;
    public static final int MAX_WIDTH = 16384;
    public static final int MIN_HEIGHT = 32768;
    public static final int MIN_WIDTH = 8192;
    public static final int PADDING = 8;
    public static final int PADDING_BOTTOM = 4096;
    public static final int PADDING_LEFT = 512;
    public static final int PADDING_RIGHT = 2048;
    public static final int PADDING_TOP = 1024;
    public static final int TEXTSIZE = 4;
    public static final int WIDTH = 1;
}
