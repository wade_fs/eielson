package com.zhy.autolayout.attr;

import android.view.View;
import android.view.ViewGroup;

public class MarginBottomAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 256;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public MarginBottomAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).bottomMargin = i;
        }
    }

    public static MarginBottomAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new MarginBottomAttr(i, 256, 0);
        }
        if (i2 == 2) {
            return new MarginBottomAttr(i, 0, 256);
        }
        if (i2 != 3) {
            return null;
        }
        return new MarginBottomAttr(i, 0, 0);
    }
}
