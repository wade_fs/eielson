package com.zhy.autolayout.attr;

import android.os.Build;
import android.view.View;
import java.lang.reflect.Field;

public class MinWidthAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 8192;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return true;
    }

    public MinWidthAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        view.setMinimumWidth(i);
    }

    public static int getMinWidth(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumWidth();
        }
        try {
            Field field = view.getClass().getField("mMinWidth");
            field.setAccessible(true);
            return ((Integer) field.get(view)).intValue();
        } catch (Exception unused) {
            return 0;
        }
    }

    public static MinWidthAttr generate(int i, int i2) {
        if (i2 == 1) {
            return new MinWidthAttr(i, 8192, 0);
        }
        if (i2 == 2) {
            return new MinWidthAttr(i, 0, 8192);
        }
        if (i2 != 3) {
            return null;
        }
        return new MinWidthAttr(i, 0, 0);
    }
}
