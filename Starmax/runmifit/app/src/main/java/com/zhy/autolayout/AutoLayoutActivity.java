package com.zhy.autolayout;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;

public class AutoLayoutActivity extends AppCompatActivity {
    private static final String LAYOUT_FRAMELAYOUT = "FrameLayout";
    private static final String LAYOUT_LINEARLAYOUT = "LinearLayout";
    private static final String LAYOUT_RELATIVELAYOUT = "RelativeLayout";

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View autoFrameLayout = str.equals(LAYOUT_FRAMELAYOUT) ? new AutoFrameLayout(context, attributeSet) : null;
        if (str.equals(LAYOUT_LINEARLAYOUT)) {
            autoFrameLayout = new AutoLinearLayout(context, attributeSet);
        }
        if (str.equals(LAYOUT_RELATIVELAYOUT)) {
            autoFrameLayout = new AutoRelativeLayout(context, attributeSet);
        }
        if (autoFrameLayout != null) {
            return autoFrameLayout;
        }
        return super.onCreateView(str, context, attributeSet);
    }
}
