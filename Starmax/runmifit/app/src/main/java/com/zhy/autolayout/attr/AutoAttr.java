package com.zhy.autolayout.attr;

import android.view.View;
import com.zhy.autolayout.utils.AutoUtils;
import com.zhy.autolayout.utils.C3266L;
import freemarker.log.Logger;

public abstract class AutoAttr {
    public static final int BASE_DEFAULT = 3;
    public static final int BASE_HEIGHT = 2;
    public static final int BASE_WIDTH = 1;
    protected int baseHeight;
    protected int baseWidth;
    protected int pxVal;

    /* access modifiers changed from: protected */
    public abstract int attrVal();

    /* access modifiers changed from: protected */
    public boolean contains(int i, int i2) {
        return (i & i2) != 0;
    }

    /* access modifiers changed from: protected */
    public abstract boolean defaultBaseWidth();

    /* access modifiers changed from: protected */
    public abstract void execute(View view, int i);

    public AutoAttr(int i, int i2, int i3) {
        this.pxVal = i;
        this.baseWidth = i2;
        this.baseHeight = i3;
    }

    public void apply(View view) {
        int i;
        boolean z = view.getTag() != null && view.getTag().toString().equals(Logger.LIBRARY_NAME_AUTO);
        if (z) {
            C3266L.m8138e(" pxVal = " + this.pxVal + " ," + getClass().getSimpleName());
        }
        if (useDefault()) {
            i = defaultBaseWidth() ? getPercentWidthSize() : getPercentHeightSize();
            if (z) {
                C3266L.m8138e(" useDefault val= " + i);
            }
        } else if (baseWidth()) {
            i = getPercentWidthSize();
            if (z) {
                C3266L.m8138e(" baseWidth val= " + i);
            }
        } else {
            i = getPercentHeightSize();
            if (z) {
                C3266L.m8138e(" baseHeight val= " + i);
            }
        }
        if (i > 0) {
            i = Math.max(i, 1);
        }
        execute(view, i);
    }

    /* access modifiers changed from: protected */
    public int getPercentWidthSize() {
        return AutoUtils.getPercentWidthSizeBigger(this.pxVal);
    }

    /* access modifiers changed from: protected */
    public int getPercentHeightSize() {
        return AutoUtils.getPercentHeightSizeBigger(this.pxVal);
    }

    /* access modifiers changed from: protected */
    public boolean baseWidth() {
        return contains(this.baseWidth, attrVal());
    }

    /* access modifiers changed from: protected */
    public boolean useDefault() {
        return !contains(this.baseHeight, attrVal()) && !contains(this.baseWidth, attrVal());
    }

    public String toString() {
        return "AutoAttr{pxVal=" + this.pxVal + ", baseWidth=" + baseWidth() + ", defaultBaseWidth=" + defaultBaseWidth() + '}';
    }
}
