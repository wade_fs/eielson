package com.zhy.autolayout.attr;

import android.view.View;

public class PaddingAttr extends AutoAttr {
    /* access modifiers changed from: protected */
    public int attrVal() {
        return 8;
    }

    /* access modifiers changed from: protected */
    public boolean defaultBaseWidth() {
        return false;
    }

    public PaddingAttr(int i, int i2, int i3) {
        super(i, i2, i3);
    }

    public void apply(View view) {
        if (useDefault()) {
            int percentWidthSize = getPercentWidthSize();
            int percentHeightSize = getPercentHeightSize();
            view.setPadding(percentWidthSize, percentHeightSize, percentWidthSize, percentHeightSize);
            return;
        }
        super.apply(view);
    }

    /* access modifiers changed from: protected */
    public void execute(View view, int i) {
        view.setPadding(i, i, i, i);
    }
}
