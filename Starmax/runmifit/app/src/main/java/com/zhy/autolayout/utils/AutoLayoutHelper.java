package com.zhy.autolayout.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.zhy.autolayout.AutoLayoutInfo;
import com.zhy.autolayout.C3262R;
import com.zhy.autolayout.attr.HeightAttr;
import com.zhy.autolayout.attr.MarginAttr;
import com.zhy.autolayout.attr.MarginBottomAttr;
import com.zhy.autolayout.attr.MarginLeftAttr;
import com.zhy.autolayout.attr.MarginRightAttr;
import com.zhy.autolayout.attr.MarginTopAttr;
import com.zhy.autolayout.attr.MaxHeightAttr;
import com.zhy.autolayout.attr.MaxWidthAttr;
import com.zhy.autolayout.attr.MinHeightAttr;
import com.zhy.autolayout.attr.MinWidthAttr;
import com.zhy.autolayout.attr.PaddingAttr;
import com.zhy.autolayout.attr.PaddingBottomAttr;
import com.zhy.autolayout.attr.PaddingLeftAttr;
import com.zhy.autolayout.attr.PaddingRightAttr;
import com.zhy.autolayout.attr.PaddingTopAttr;
import com.zhy.autolayout.attr.TextSizeAttr;
import com.zhy.autolayout.attr.WidthAttr;
import com.zhy.autolayout.config.AutoLayoutConifg;

public class AutoLayoutHelper {
    private static final int INDEX_HEIGHT = 7;
    private static final int INDEX_MARGIN = 8;
    private static final int INDEX_MARGIN_BOTTOM = 12;
    private static final int INDEX_MARGIN_LEFT = 9;
    private static final int INDEX_MARGIN_RIGHT = 11;
    private static final int INDEX_MARGIN_TOP = 10;
    private static final int INDEX_MAX_HEIGHT = 14;
    private static final int INDEX_MAX_WIDTH = 13;
    private static final int INDEX_MIN_HEIGHT = 16;
    private static final int INDEX_MIN_WIDTH = 15;
    private static final int INDEX_PADDING = 1;
    private static final int INDEX_PADDING_BOTTOM = 5;
    private static final int INDEX_PADDING_LEFT = 2;
    private static final int INDEX_PADDING_RIGHT = 4;
    private static final int INDEX_PADDING_TOP = 3;
    private static final int INDEX_TEXT_SIZE = 0;
    private static final int INDEX_WIDTH = 6;

    /* renamed from: LL */
    private static final int[] f7436LL = {16842901, 16842965, 16842966, 16842967, 16842968, 16842969, 16842996, 16842997, 16842998, 16842999, 16843000, 16843001, 16843002, 16843039, 16843040, 16843071, 16843072};
    private static AutoLayoutConifg mAutoLayoutConifg;
    private final ViewGroup mHost;

    public interface AutoLayoutParams {
        AutoLayoutInfo getAutoLayoutInfo();
    }

    public AutoLayoutHelper(ViewGroup viewGroup) {
        this.mHost = viewGroup;
        if (mAutoLayoutConifg == null) {
            initAutoLayoutConfig(viewGroup);
        }
    }

    private void initAutoLayoutConfig(ViewGroup viewGroup) {
        mAutoLayoutConifg = AutoLayoutConifg.getInstance();
        mAutoLayoutConifg.init(viewGroup.getContext());
    }

    public void adjustChildren() {
        AutoLayoutInfo autoLayoutInfo;
        AutoLayoutConifg.getInstance().checkParams();
        int childCount = this.mHost.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.mHost.getChildAt(i);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof AutoLayoutParams) && (autoLayoutInfo = ((AutoLayoutParams) layoutParams).getAutoLayoutInfo()) != null) {
                autoLayoutInfo.fillAttrs(childAt);
            }
        }
    }

    public static AutoLayoutInfo getAutoLayoutInfo(Context context, AttributeSet attributeSet) {
        AutoLayoutInfo autoLayoutInfo = new AutoLayoutInfo();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C3262R.styleable.AutoLayout_Layout);
        int i = obtainStyledAttributes.getInt(C3262R.styleable.AutoLayout_Layout_layout_auto_basewidth, 0);
        int i2 = obtainStyledAttributes.getInt(C3262R.styleable.AutoLayout_Layout_layout_auto_baseheight, 0);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, f7436LL);
        int indexCount = obtainStyledAttributes2.getIndexCount();
        for (int i3 = 0; i3 < indexCount; i3++) {
            int index = obtainStyledAttributes2.getIndex(i3);
            if (DimenUtils.isPxVal(obtainStyledAttributes2.peekValue(index))) {
                try {
                    int dimensionPixelOffset = obtainStyledAttributes2.getDimensionPixelOffset(index, 0);
                    switch (index) {
                        case 0:
                            autoLayoutInfo.addAttr(new TextSizeAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 1:
                            autoLayoutInfo.addAttr(new PaddingAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 2:
                            autoLayoutInfo.addAttr(new PaddingLeftAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 3:
                            autoLayoutInfo.addAttr(new PaddingTopAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 4:
                            autoLayoutInfo.addAttr(new PaddingRightAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 5:
                            autoLayoutInfo.addAttr(new PaddingBottomAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 6:
                            autoLayoutInfo.addAttr(new WidthAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 7:
                            autoLayoutInfo.addAttr(new HeightAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 8:
                            autoLayoutInfo.addAttr(new MarginAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 9:
                            autoLayoutInfo.addAttr(new MarginLeftAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 10:
                            autoLayoutInfo.addAttr(new MarginTopAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 11:
                            autoLayoutInfo.addAttr(new MarginRightAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 12:
                            autoLayoutInfo.addAttr(new MarginBottomAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 13:
                            autoLayoutInfo.addAttr(new MaxWidthAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 14:
                            autoLayoutInfo.addAttr(new MaxHeightAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 15:
                            autoLayoutInfo.addAttr(new MinWidthAttr(dimensionPixelOffset, i, i2));
                            continue;
                        case 16:
                            autoLayoutInfo.addAttr(new MinHeightAttr(dimensionPixelOffset, i, i2));
                            continue;
                    }
                } catch (Exception unused) {
                }
            }
        }
        obtainStyledAttributes2.recycle();
        C3266L.m8138e(" getAutoLayoutInfo " + autoLayoutInfo.toString());
        return autoLayoutInfo;
    }
}
