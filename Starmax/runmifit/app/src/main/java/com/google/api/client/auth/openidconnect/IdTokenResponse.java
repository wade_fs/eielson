package com.google.api.client.auth.openidconnect;

import com.google.api.client.auth.oauth2.TokenRequest;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.util.Key;
import com.google.api.client.util.Preconditions;
import java.io.IOException;

public class IdTokenResponse extends TokenResponse {
    @Key("id_token")
    private String idToken;

    public final String getIdToken() {
        return this.idToken;
    }

    public IdTokenResponse setIdToken(String str) {
        this.idToken = (String) Preconditions.checkNotNull(str);
        return this;
    }

    public IdTokenResponse setAccessToken(String str) {
        super.setAccessToken(str);
        return this;
    }

    public IdTokenResponse setTokenType(String str) {
        super.setTokenType(str);
        return this;
    }

    public IdTokenResponse setExpiresInSeconds(Long l) {
        super.setExpiresInSeconds(l);
        return this;
    }

    public IdTokenResponse setRefreshToken(String str) {
        super.setRefreshToken(str);
        return this;
    }

    public IdTokenResponse setScope(String str) {
        super.setScope(str);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.auth.openidconnect.IdToken.parse(com.google.api.client.json.JsonFactory, java.lang.String):com.google.api.client.auth.openidconnect.IdToken
     arg types: [com.google.api.client.json.JsonFactory, java.lang.String]
     candidates:
      com.google.api.client.json.webtoken.JsonWebSignature.parse(com.google.api.client.json.JsonFactory, java.lang.String):com.google.api.client.json.webtoken.JsonWebSignature
      com.google.api.client.auth.openidconnect.IdToken.parse(com.google.api.client.json.JsonFactory, java.lang.String):com.google.api.client.auth.openidconnect.IdToken */
    public IdToken parseIdToken() throws IOException {
        return IdToken.parse(getFactory(), this.idToken);
    }

    public static IdTokenResponse execute(TokenRequest tokenRequest) throws IOException {
        return (IdTokenResponse) tokenRequest.executeUnparsed().parseAs(IdTokenResponse.class);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.TokenResponse
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.client.auth.openidconnect.IdTokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.auth.openidconnect.IdTokenResponse
      com.google.api.client.auth.openidconnect.IdTokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
      com.google.api.client.auth.openidconnect.IdTokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
      com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.TokenResponse */
    public IdTokenResponse set(String str, Object obj) {
        return (IdTokenResponse) super.set(str, obj);
    }

    public IdTokenResponse clone() {
        return (IdTokenResponse) super.clone();
    }
}
