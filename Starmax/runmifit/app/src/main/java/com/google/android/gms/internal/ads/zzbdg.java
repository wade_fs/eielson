package com.google.android.gms.internal.ads;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class zzbdg {
    private static final zzbdg zzdxa = new zzbdg();
    private final zzbdn zzdxb;
    private final ConcurrentMap<Class<?>, zzbdm<?>> zzdxc = new ConcurrentHashMap();

    private zzbdg() {
        String[] strArr = {"com.google.protobuf.AndroidProto3SchemaFactory"};
        zzbdn zzbdn = null;
        for (int i = 0; i <= 0; i++) {
            zzbdn = zzeq(strArr[0]);
            if (zzbdn != null) {
                break;
            }
        }
        this.zzdxb = zzbdn == null ? new zzbcj() : zzbdn;
    }

    public static zzbdg zzaeo() {
        return zzdxa;
    }

    private static zzbdn zzeq(String str) {
        try {
            return (zzbdn) Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Throwable unused) {
            return null;
        }
    }

    public final <T> zzbdm<T> zzab(T t) {
        return zze(t.getClass());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbbq.zza(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.ads.zzbbq.zza(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.ads.zzbbq.zza(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbbq.zza(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.ads.zzbdm<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.ads.zzbbq.zza(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.ads.zzbbq.zza(java.lang.Object, java.lang.String):T */
    public final <T> zzbdm<T> zze(Class<T> cls) {
        zzbbq.zza((Object) cls, "messageType");
        zzbdm<T> zzbdm = this.zzdxc.get(cls);
        if (zzbdm != null) {
            return zzbdm;
        }
        zzbdm<T> zzd = this.zzdxb.zzd(cls);
        zzbbq.zza((Object) cls, "messageType");
        zzbbq.zza((Object) zzd, "schema");
        zzbdm<T> putIfAbsent = this.zzdxc.putIfAbsent(cls, zzd);
        return putIfAbsent != null ? putIfAbsent : zzd;
    }
}
