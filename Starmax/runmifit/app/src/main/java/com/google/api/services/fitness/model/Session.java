package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonString;
import com.google.api.client.util.Key;

public final class Session extends GenericJson {
    @JsonString
    @Key
    private Long activeTimeMillis;
    @Key
    private Integer activityType;
    @Key
    private Application application;
    @Key
    private String description;
    @JsonString
    @Key
    private Long endTimeMillis;
    @Key

    /* renamed from: id */
    private String f4557id;
    @JsonString
    @Key
    private Long modifiedTimeMillis;
    @Key
    private String name;
    @JsonString
    @Key
    private Long startTimeMillis;

    public Long getActiveTimeMillis() {
        return this.activeTimeMillis;
    }

    public Session setActiveTimeMillis(Long l) {
        this.activeTimeMillis = l;
        return this;
    }

    public Integer getActivityType() {
        return this.activityType;
    }

    public Session setActivityType(Integer num) {
        this.activityType = num;
        return this;
    }

    public Application getApplication() {
        return this.application;
    }

    public Session setApplication(Application application2) {
        this.application = application2;
        return this;
    }

    public String getDescription() {
        return this.description;
    }

    public Session setDescription(String str) {
        this.description = str;
        return this;
    }

    public Long getEndTimeMillis() {
        return this.endTimeMillis;
    }

    public Session setEndTimeMillis(Long l) {
        this.endTimeMillis = l;
        return this;
    }

    public String getId() {
        return this.f4557id;
    }

    public Session setId(String str) {
        this.f4557id = str;
        return this;
    }

    public Long getModifiedTimeMillis() {
        return this.modifiedTimeMillis;
    }

    public Session setModifiedTimeMillis(Long l) {
        this.modifiedTimeMillis = l;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Session setName(String str) {
        this.name = str;
        return this;
    }

    public Long getStartTimeMillis() {
        return this.startTimeMillis;
    }

    public Session setStartTimeMillis(Long l) {
        this.startTimeMillis = l;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.Session.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.Session.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.Session
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public Session set(String str, Object obj) {
        return (Session) super.set(str, obj);
    }

    public Session clone() {
        return (Session) super.clone();
    }
}
