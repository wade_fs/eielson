package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzae extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzae> CREATOR = new zzaf();
    private final DataSet zzeb;
    private final Session zzz;

    public zzae(Session session, DataSet dataSet) {
        this.zzz = session;
        this.zzeb = dataSet;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzae)) {
            return false;
        }
        zzae zzae = (zzae) obj;
        return Objects.equal(this.zzz, zzae.zzz) && Objects.equal(this.zzeb, zzae.zzeb);
    }

    public final DataSet getDataSet() {
        return this.zzeb;
    }

    public final Session getSession() {
        return this.zzz;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzz, this.zzeb);
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("session", this.zzz).add("dataSet", this.zzeb).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, this.zzz, i, false);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzeb, i, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
