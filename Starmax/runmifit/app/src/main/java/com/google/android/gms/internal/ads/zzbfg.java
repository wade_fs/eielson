package com.google.android.gms.internal.ads;

import com.runmifit.android.util.ChangeCharset;
import java.nio.charset.Charset;

public final class zzbfg {
    private static final Charset ISO_8859_1 = Charset.forName(ChangeCharset.ISO_8859_1);
    protected static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final Object zzebs = new Object();

    public static void zza(zzbfc zzbfc, zzbfc zzbfc2) {
        if (zzbfc.zzebk != null) {
            zzbfc2.zzebk = (zzbfe) zzbfc.zzebk.clone();
        }
    }
}
