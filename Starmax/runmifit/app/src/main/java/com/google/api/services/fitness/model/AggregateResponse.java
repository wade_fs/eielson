package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Data;
import com.google.api.client.util.Key;
import java.util.List;

public final class AggregateResponse extends GenericJson {
    @Key
    private List<AggregateBucket> bucket;

    static {
        Data.nullOf(AggregateBucket.class);
    }

    public List<AggregateBucket> getBucket() {
        return this.bucket;
    }

    public AggregateResponse setBucket(List<AggregateBucket> list) {
        this.bucket = list;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.AggregateResponse.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.AggregateResponse.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.AggregateResponse
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public AggregateResponse set(String str, Object obj) {
        return (AggregateResponse) super.set(str, obj);
    }

    public AggregateResponse clone() {
        return (AggregateResponse) super.clone();
    }
}
