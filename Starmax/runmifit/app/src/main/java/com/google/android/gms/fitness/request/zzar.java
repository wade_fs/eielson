package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.fitness.data.zzt;
import com.google.android.gms.fitness.data.zzu;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzcr;

public final class zzar extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzar> CREATOR = new zzas();
    private final zzcq zzgj;
    private final PendingIntent zzhg;
    private final zzt zzhp;

    zzar(IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2) {
        this.zzhp = iBinder == null ? null : zzu.zza(iBinder);
        this.zzhg = pendingIntent;
        this.zzgj = zzcr.zzj(iBinder2);
    }

    public zzar(zzt zzt, PendingIntent pendingIntent, zzcq zzcq) {
        this.zzhp = zzt;
        this.zzhg = pendingIntent;
        this.zzgj = zzcq;
    }

    public final String toString() {
        return String.format("SensorUnregistrationRequest{%s}", this.zzhp);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        zzt zzt = this.zzhp;
        IBinder iBinder = null;
        SafeParcelWriter.writeIBinder(parcel, 1, zzt == null ? null : zzt.asBinder(), false);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzhg, i, false);
        zzcq zzcq = this.zzgj;
        if (zzcq != null) {
            iBinder = zzcq.asBinder();
        }
        SafeParcelWriter.writeIBinder(parcel, 3, iBinder, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
