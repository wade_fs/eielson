package com.google.api.services.fitness;

import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Key;

public abstract class FitnessRequest<T> extends AbstractGoogleJsonClientRequest<T> {
    @Key
    private String alt;
    @Key
    private String fields;
    @Key
    private String key;
    @Key("oauth_token")
    private String oauthToken;
    @Key
    private Boolean prettyPrint;
    @Key
    private String quotaUser;
    @Key
    private String userIp;

    public FitnessRequest(Fitness fitness, String str, String str2, Object obj, Class<T> cls) {
        super(fitness, str, str2, obj, cls);
    }

    public String getAlt() {
        return this.alt;
    }

    public FitnessRequest<T> setAlt(String str) {
        this.alt = str;
        return this;
    }

    public String getFields() {
        return this.fields;
    }

    public FitnessRequest<T> setFields(String str) {
        this.fields = str;
        return this;
    }

    public String getKey() {
        return this.key;
    }

    public FitnessRequest<T> setKey(String str) {
        this.key = str;
        return this;
    }

    public String getOauthToken() {
        return this.oauthToken;
    }

    public FitnessRequest<T> setOauthToken(String str) {
        this.oauthToken = str;
        return this;
    }

    public Boolean getPrettyPrint() {
        return this.prettyPrint;
    }

    public FitnessRequest<T> setPrettyPrint(Boolean bool) {
        this.prettyPrint = bool;
        return this;
    }

    public String getQuotaUser() {
        return this.quotaUser;
    }

    public FitnessRequest<T> setQuotaUser(String str) {
        this.quotaUser = str;
        return this;
    }

    public String getUserIp() {
        return this.userIp;
    }

    public FitnessRequest<T> setUserIp(String str) {
        this.userIp = str;
        return this;
    }

    public final Fitness getAbstractGoogleClient() {
        return (Fitness) super.getAbstractGoogleClient();
    }

    public FitnessRequest<T> setDisableGZipContent(boolean z) {
        return (FitnessRequest) super.setDisableGZipContent(z);
    }

    public FitnessRequest<T> setRequestHeaders(HttpHeaders httpHeaders) {
        return (FitnessRequest) super.setRequestHeaders(httpHeaders);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T> */
    public FitnessRequest<T> set(String str, Object obj) {
        return (FitnessRequest) super.set(str, obj);
    }
}
