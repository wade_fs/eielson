package com.google.android.gms.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.BleScanCallback;
import com.google.android.gms.fitness.request.StartBleScanRequest;
import com.google.android.gms.fitness.request.zzae;
import com.google.android.gms.fitness.request.zzd;
import com.google.android.gms.internal.fitness.zzbt;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzen;
import com.google.android.gms.internal.fitness.zzp;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.List;

final class zzb extends RegisterListenerMethod<zzp, BleScanCallback> {
    private final /* synthetic */ ListenerHolder zzg;
    private final /* synthetic */ List zzh;
    private final /* synthetic */ int zzi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzb(BleClient bleClient, ListenerHolder listenerHolder, ListenerHolder listenerHolder2, List list, int i) {
        super(listenerHolder);
        this.zzg = listenerHolder2;
        this.zzh = list;
        this.zzi = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.StartBleScanRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, com.google.android.gms.fitness.request.zzae, int, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [java.util.List, com.google.android.gms.fitness.request.zza, int, com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.StartBleScanRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, android.os.IBinder, int, android.os.IBinder):void
      com.google.android.gms.fitness.request.StartBleScanRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, com.google.android.gms.fitness.request.zzae, int, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void registerListener(Api.AnyClient anyClient, TaskCompletionSource taskCompletionSource) throws RemoteException {
        ((zzbt) ((zzp) anyClient).getService()).zza(new StartBleScanRequest((List<DataType>) this.zzh, (zzae) zzd.zzt().zza(this.zzg), this.zzi, (zzcq) zzen.zza(taskCompletionSource)));
    }
}
