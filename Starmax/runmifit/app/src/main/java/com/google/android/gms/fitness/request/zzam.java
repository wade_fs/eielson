package com.google.android.gms.fitness.request;

import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.fitness.data.DataPoint;

final class zzam implements ListenerHolder.Notifier<OnDataPointListener> {
    private final /* synthetic */ DataPoint zzhm;

    zzam(zzal zzal, DataPoint dataPoint) {
        this.zzhm = dataPoint;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((OnDataPointListener) obj).onDataPoint(this.zzhm);
    }

    public final void onNotifyListenerFailed() {
    }
}
