package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzcr;

public final class zzbn extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzbn> CREATOR = new zzbo();
    private final zzcq zzgj;
    private final DataType zzq;
    private final DataSource zzr;

    zzbn(DataType dataType, DataSource dataSource, IBinder iBinder) {
        this.zzq = dataType;
        this.zzr = dataSource;
        this.zzgj = zzcr.zzj(iBinder);
    }

    public zzbn(DataType dataType, DataSource dataSource, zzcq zzcq) {
        this.zzq = dataType;
        this.zzr = dataSource;
        this.zzgj = zzcq;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof zzbn) {
                zzbn zzbn = (zzbn) obj;
                if (Objects.equal(this.zzr, zzbn.zzr) && Objects.equal(this.zzq, zzbn.zzq)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzr, this.zzq);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, this.zzq, i, false);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzr, i, false);
        zzcq zzcq = this.zzgj;
        SafeParcelWriter.writeIBinder(parcel, 3, zzcq == null ? null : zzcq.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
