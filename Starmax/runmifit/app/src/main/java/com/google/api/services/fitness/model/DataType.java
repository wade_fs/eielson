package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;
import java.util.List;

public final class DataType extends GenericJson {
    @Key
    private List<DataTypeField> field;
    @Key
    private String name;

    public List<DataTypeField> getField() {
        return this.field;
    }

    public DataType setField(List<DataTypeField> list) {
        this.field = list;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public DataType setName(String str) {
        this.name = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.DataType.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.DataType.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.DataType
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public DataType set(String str, Object obj) {
        return (DataType) super.set(str, obj);
    }

    public DataType clone() {
        return (DataType) super.clone();
    }
}
