package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonString;
import com.google.api.client.util.Data;
import com.google.api.client.util.Key;
import java.util.List;

public final class Dataset extends GenericJson {
    @Key
    private String dataSourceId;
    @JsonString
    @Key
    private Long maxEndTimeNs;
    @JsonString
    @Key
    private Long minStartTimeNs;
    @Key
    private String nextPageToken;
    @Key
    private List<DataPoint> point;

    static {
        Data.nullOf(DataPoint.class);
    }

    public String getDataSourceId() {
        return this.dataSourceId;
    }

    public Dataset setDataSourceId(String str) {
        this.dataSourceId = str;
        return this;
    }

    public Long getMaxEndTimeNs() {
        return this.maxEndTimeNs;
    }

    public Dataset setMaxEndTimeNs(Long l) {
        this.maxEndTimeNs = l;
        return this;
    }

    public Long getMinStartTimeNs() {
        return this.minStartTimeNs;
    }

    public Dataset setMinStartTimeNs(Long l) {
        this.minStartTimeNs = l;
        return this;
    }

    public String getNextPageToken() {
        return this.nextPageToken;
    }

    public Dataset setNextPageToken(String str) {
        this.nextPageToken = str;
        return this;
    }

    public List<DataPoint> getPoint() {
        return this.point;
    }

    public Dataset setPoint(List<DataPoint> list) {
        this.point = list;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.Dataset.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.Dataset.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.Dataset
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public Dataset set(String str, Object obj) {
        return (Dataset) super.set(str, obj);
    }

    public Dataset clone() {
        return (Dataset) super.clone();
    }
}
