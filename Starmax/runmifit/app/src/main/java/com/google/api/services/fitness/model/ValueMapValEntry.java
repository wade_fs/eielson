package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class ValueMapValEntry extends GenericJson {
    @Key
    private String key;
    @Key
    private MapValue value;

    public String getKey() {
        return this.key;
    }

    public ValueMapValEntry setKey(String str) {
        this.key = str;
        return this;
    }

    public MapValue getValue() {
        return this.value;
    }

    public ValueMapValEntry setValue(MapValue mapValue) {
        this.value = mapValue;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.ValueMapValEntry.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.ValueMapValEntry.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.ValueMapValEntry
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public ValueMapValEntry set(String str, Object obj) {
        return (ValueMapValEntry) super.set(str, obj);
    }

    public ValueMapValEntry clone() {
        return (ValueMapValEntry) super.clone();
    }
}
