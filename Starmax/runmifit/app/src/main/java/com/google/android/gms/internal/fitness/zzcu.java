package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.request.StartBleScanRequest;

final class zzcu extends zzu {
    private final /* synthetic */ StartBleScanRequest zzex;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcu(zzct zzct, GoogleApiClient googleApiClient, StartBleScanRequest startBleScanRequest) {
        super(googleApiClient);
        this.zzex = startBleScanRequest;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbt) ((zzp) anyClient).getService()).zza(new StartBleScanRequest(this.zzex, new zzen(this)));
    }
}
