package com.google.api.client.repackaged.com.google.common.base;

public interface Supplier<T> {
    T get();
}
