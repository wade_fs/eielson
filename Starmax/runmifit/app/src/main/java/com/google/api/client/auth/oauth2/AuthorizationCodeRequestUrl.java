package com.google.api.client.auth.oauth2;

import java.util.Collection;
import java.util.Collections;

public class AuthorizationCodeRequestUrl extends AuthorizationRequestUrl {
    public AuthorizationCodeRequestUrl(String str, String str2) {
        super(str, str2, Collections.singleton("code"));
    }

    public AuthorizationCodeRequestUrl setResponseTypes(Collection<String> collection) {
        return (AuthorizationCodeRequestUrl) super.setResponseTypes(collection);
    }

    public AuthorizationCodeRequestUrl setRedirectUri(String str) {
        return (AuthorizationCodeRequestUrl) super.setRedirectUri(str);
    }

    public AuthorizationCodeRequestUrl setScopes(Collection<String> collection) {
        return (AuthorizationCodeRequestUrl) super.setScopes(collection);
    }

    public AuthorizationCodeRequestUrl setClientId(String str) {
        return (AuthorizationCodeRequestUrl) super.setClientId(str);
    }

    public AuthorizationCodeRequestUrl setState(String str) {
        return (AuthorizationCodeRequestUrl) super.setState(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.AuthorizationRequestUrl
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl
      com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl
      com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl
      com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.http.GenericUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl
      com.google.api.client.http.GenericUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.AuthorizationRequestUrl */
    public AuthorizationCodeRequestUrl set(String str, Object obj) {
        return (AuthorizationCodeRequestUrl) super.set(str, obj);
    }

    public AuthorizationCodeRequestUrl clone() {
        return (AuthorizationCodeRequestUrl) super.clone();
    }
}
