package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.request.zzbb;
import com.google.android.gms.fitness.result.SessionStopResult;
import java.util.Collections;

final class zzeg extends zzbb<SessionStopResult> {
    private final /* synthetic */ String val$name = null;
    private final /* synthetic */ String zzfy;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzeg(zzee zzee, GoogleApiClient googleApiClient, String str, String str2) {
        super(googleApiClient);
        this.zzfy = str2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return new SessionStopResult(status, Collections.emptyList());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzbb.<init>(java.lang.String, java.lang.String, com.google.android.gms.internal.fitness.zzcn):void
     arg types: [java.lang.String, java.lang.String, com.google.android.gms.internal.fitness.zzem]
     candidates:
      com.google.android.gms.fitness.request.zzbb.<init>(java.lang.String, java.lang.String, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzbb.<init>(java.lang.String, java.lang.String, com.google.android.gms.internal.fitness.zzcn):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcf) ((zzay) anyClient).getService()).zza(new zzbb(this.val$name, this.zzfy, (zzcn) new zzem(this, null)));
    }
}
