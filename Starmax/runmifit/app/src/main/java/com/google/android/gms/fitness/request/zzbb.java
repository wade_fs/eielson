package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.fitness.zzcn;
import com.google.android.gms.internal.fitness.zzco;

public final class zzbb extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzbb> CREATOR = new zzbc();
    private final String name;
    private final String zzdz;
    private final zzcn zzih;

    zzbb(String str, String str2, IBinder iBinder) {
        this.name = str;
        this.zzdz = str2;
        this.zzih = zzco.zzi(iBinder);
    }

    public zzbb(String str, String str2, zzcn zzcn) {
        this.name = str;
        this.zzdz = str2;
        this.zzih = zzcn;
    }

    public final boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof zzbb) {
                zzbb zzbb = (zzbb) obj;
                if (Objects.equal(this.name, zzbb.name) && Objects.equal(this.zzdz, zzbb.zzdz)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Objects.hashCode(this.name, this.zzdz);
    }

    public final String toString() {
        return Objects.toStringHelper(this).add(Config.FEED_LIST_NAME, this.name).add("identifier", this.zzdz).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.name, false);
        SafeParcelWriter.writeString(parcel, 2, this.zzdz, false);
        zzcn zzcn = this.zzih;
        SafeParcelWriter.writeIBinder(parcel, 3, zzcn == null ? null : zzcn.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
