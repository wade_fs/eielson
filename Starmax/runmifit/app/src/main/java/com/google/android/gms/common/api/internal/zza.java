package com.google.android.gms.common.api.internal;

import android.app.Activity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public final class zza extends ActivityLifecycleObserver {
    private final WeakReference<C4050zza> zzds;

    /* renamed from: com.google.android.gms.common.api.internal.zza$zza  reason: collision with other inner class name */
    static class C4050zza extends LifecycleCallback {
        private List<Runnable> zzdt = new ArrayList();

        private C4050zza(LifecycleFragment lifecycleFragment) {
            super(lifecycleFragment);
            this.mLifecycleFragment.addCallback("LifecycleObserverOnStop", super);
        }

        /* access modifiers changed from: private */
        public static C4050zza zza(Activity activity) {
            C4050zza zza;
            synchronized (activity) {
                LifecycleFragment fragment = getFragment(activity);
                zza = (C4050zza) fragment.getCallbackOrNull("LifecycleObserverOnStop", C4050zza.class);
                if (zza == null) {
                    zza = new C4050zza(fragment);
                }
            }
            return zza;
        }

        /* access modifiers changed from: private */
        public final synchronized void zza(Runnable runnable) {
            this.zzdt.add(runnable);
        }

        public void onStop() {
            List<Runnable> list;
            synchronized (this) {
                list = this.zzdt;
                this.zzdt = new ArrayList();
            }
            for (Runnable runnable : list) {
                runnable.run();
            }
        }
    }

    public zza(Activity activity) {
        this(C4050zza.zza(activity));
    }

    private zza(C4050zza zza) {
        this.zzds = new WeakReference<>(zza);
    }

    public final ActivityLifecycleObserver onStopCallOnce(Runnable runnable) {
        C4050zza zza = this.zzds.get();
        if (zza != null) {
            zza.zza(runnable);
            return super;
        }
        throw new IllegalStateException("The target activity has already been GC'd");
    }
}
