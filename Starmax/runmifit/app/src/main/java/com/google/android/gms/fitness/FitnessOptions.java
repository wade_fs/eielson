package com.google.android.gms.fitness;

import android.os.Bundle;
import android.util.SparseArray;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FitnessOptions implements GoogleSignInOptionsExtension, Api.ApiOptions.HasGoogleSignInAccountOptions {
    public static final int ACCESS_READ = 0;
    public static final int ACCESS_WRITE = 1;
    private final SparseArray<List<DataType>> zzk;
    private final Set<Scope> zzl;
    private final GoogleSignInAccount zzm;

    public static final class Builder {
        private GoogleSignInAccount zzm;
        private final SparseArray<List<DataType>> zzn;

        private Builder() {
            this.zzn = new SparseArray<>();
        }

        /* access modifiers changed from: private */
        public final Builder zzb(GoogleSignInAccount googleSignInAccount) {
            this.zzm = googleSignInAccount;
            return this;
        }

        public final Builder addDataType(DataType dataType) {
            return addDataType(dataType, 0);
        }

        public final Builder addDataType(DataType dataType, int i) {
            boolean z = true;
            if (!(i == 0 || i == 1)) {
                z = false;
            }
            Preconditions.checkArgument(z, "valid access types are FitnessOptions.ACCESS_READ or FitnessOptions.ACCESS_WRITE");
            Object obj = this.zzn.get(i);
            if (obj == null) {
                obj = new ArrayList();
                this.zzn.put(i, obj);
            }
            obj.add(dataType);
            return this;
        }

        public final FitnessOptions build() {
            return new FitnessOptions(this.zzn, this.zzm);
        }
    }

    private FitnessOptions(SparseArray<List<DataType>> sparseArray, GoogleSignInAccount googleSignInAccount) {
        Scope scope;
        this.zzk = sparseArray;
        this.zzm = googleSignInAccount;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < sparseArray.size(); i++) {
            int keyAt = sparseArray.keyAt(i);
            for (DataType dataType : sparseArray.valueAt(i)) {
                if (keyAt != 0 || dataType.zzk() == null) {
                    scope = (keyAt == 1 && dataType.zzl() != null) ? new Scope(dataType.zzl()) : scope;
                } else {
                    new Scope(dataType.zzk());
                }
                arrayList.add(scope);
            }
        }
        this.zzl = zzg.zza(arrayList);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder zza(GoogleSignInAccount googleSignInAccount) {
        return googleSignInAccount != null ? new Builder().zzb(googleSignInAccount) : new Builder();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            FitnessOptions fitnessOptions = (FitnessOptions) obj;
            return Objects.equal(this.zzk, fitnessOptions.zzk) && Objects.equal(this.zzm, fitnessOptions.zzm);
        }
    }

    public int getExtensionType() {
        return 3;
    }

    public GoogleSignInAccount getGoogleSignInAccount() {
        return this.zzm;
    }

    public List<Scope> getImpliedScopes() {
        return new ArrayList(this.zzl);
    }

    public int hashCode() {
        return Objects.hashCode(this.zzk, this.zzm);
    }

    public Bundle toBundle() {
        return new Bundle();
    }
}
