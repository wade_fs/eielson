package com.google.android.gms.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.UnregisterListenerMethod;
import com.google.android.gms.fitness.request.BleScanCallback;
import com.google.android.gms.fitness.request.zza;
import com.google.android.gms.fitness.request.zzae;
import com.google.android.gms.fitness.request.zzbh;
import com.google.android.gms.fitness.request.zzd;
import com.google.android.gms.internal.fitness.zzbt;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzen;
import com.google.android.gms.internal.fitness.zzp;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzc extends UnregisterListenerMethod<zzp, BleScanCallback> {
    private final /* synthetic */ ListenerHolder zzg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzc(BleClient bleClient, ListenerHolder.ListenerKey listenerKey, ListenerHolder listenerHolder) {
        super(listenerKey);
        this.zzg = listenerHolder;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzbh.<init>(com.google.android.gms.fitness.request.zzae, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [com.google.android.gms.fitness.request.zza, com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zzbh.<init>(android.os.IBinder, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzbh.<init>(com.google.android.gms.fitness.request.BleScanCallback, com.google.android.gms.internal.fitness.zzcq):void
      com.google.android.gms.fitness.request.zzbh.<init>(com.google.android.gms.fitness.request.zzae, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void unregisterListener(Api.AnyClient anyClient, TaskCompletionSource taskCompletionSource) throws RemoteException {
        zzp zzp = (zzp) anyClient;
        zza zzb = zzd.zzt().zzb(this.zzg);
        if (zzb == null) {
            taskCompletionSource.setResult(false);
            return;
        }
        ((zzbt) zzp.getService()).zza(new zzbh((zzae) zzb, (zzcq) zzen.zzb(taskCompletionSource)));
    }
}
