package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.fitness.result.SessionReadResult;

final class zzel extends zzcl {
    private final BaseImplementation.ResultHolder<SessionReadResult> zzev;

    private zzel(BaseImplementation.ResultHolder<SessionReadResult> resultHolder) {
        this.zzev = resultHolder;
    }

    /* synthetic */ zzel(BaseImplementation.ResultHolder resultHolder, zzef zzef) {
        this(resultHolder);
    }

    public final void zza(SessionReadResult sessionReadResult) throws RemoteException {
        this.zzev.setResult(sessionReadResult);
    }
}
