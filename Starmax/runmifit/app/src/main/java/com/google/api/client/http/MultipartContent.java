package com.google.api.client.http;

import com.google.api.client.util.Preconditions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public class MultipartContent extends AbstractHttpContent {
    static final String NEWLINE = "\r\n";
    private static final String TWO_DASHES = "--";
    private ArrayList<Part> parts = new ArrayList<>();

    public MultipartContent() {
        super(new HttpMediaType("multipart/related").setParameter("boundary", "__END_OF_PART__"));
    }

    /* JADX WARN: Type inference failed for: r9v4, types: [com.google.api.client.http.HttpEncodingStreamingContent] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.http.HttpHeaders.set(java.lang.String, java.lang.Object):com.google.api.client.http.HttpHeaders
     arg types: [java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      com.google.api.client.http.HttpHeaders.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.http.HttpHeaders.set(java.lang.String, java.lang.Object):com.google.api.client.http.HttpHeaders */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.http.HttpHeaders.set(java.lang.String, java.lang.Object):com.google.api.client.http.HttpHeaders
     arg types: [java.lang.String, java.util.List]
     candidates:
      com.google.api.client.http.HttpHeaders.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.http.HttpHeaders.set(java.lang.String, java.lang.Object):com.google.api.client.http.HttpHeaders */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeTo(java.io.OutputStream r14) throws java.io.IOException {
        /*
            r13 = this;
            java.io.OutputStreamWriter r0 = new java.io.OutputStreamWriter
            java.nio.charset.Charset r1 = r13.getCharset()
            r0.<init>(r14, r1)
            java.lang.String r1 = r13.getBoundary()
            java.util.ArrayList<com.google.api.client.http.MultipartContent$Part> r2 = r13.parts
            java.util.Iterator r2 = r2.iterator()
        L_0x0013:
            boolean r3 = r2.hasNext()
            java.lang.String r4 = "--"
            java.lang.String r5 = "\r\n"
            if (r3 == 0) goto L_0x00a9
            java.lang.Object r3 = r2.next()
            com.google.api.client.http.MultipartContent$Part r3 = (com.google.api.client.http.MultipartContent.Part) r3
            com.google.api.client.http.HttpHeaders r6 = new com.google.api.client.http.HttpHeaders
            r6.<init>()
            r7 = 0
            com.google.api.client.http.HttpHeaders r6 = r6.setAcceptEncoding(r7)
            com.google.api.client.http.HttpHeaders r8 = r3.headers
            if (r8 == 0) goto L_0x0036
            com.google.api.client.http.HttpHeaders r8 = r3.headers
            r6.fromHttpHeaders(r8)
        L_0x0036:
            com.google.api.client.http.HttpHeaders r8 = r6.setContentEncoding(r7)
            com.google.api.client.http.HttpHeaders r8 = r8.setUserAgent(r7)
            com.google.api.client.http.HttpHeaders r8 = r8.setContentType(r7)
            com.google.api.client.http.HttpHeaders r8 = r8.setContentLength(r7)
            java.lang.String r9 = "Content-Transfer-Encoding"
            r8.set(r9, r7)
            com.google.api.client.http.HttpContent r8 = r3.content
            if (r8 == 0) goto L_0x008c
            java.lang.String r10 = "binary"
            java.lang.String[] r10 = new java.lang.String[]{r10}
            java.util.List r10 = java.util.Arrays.asList(r10)
            r6.set(r9, r10)
            java.lang.String r9 = r8.getType()
            r6.setContentType(r9)
            com.google.api.client.http.HttpEncoding r3 = r3.encoding
            if (r3 != 0) goto L_0x006c
            long r9 = r8.getLength()
            goto L_0x007e
        L_0x006c:
            java.lang.String r9 = r3.getName()
            r6.setContentEncoding(r9)
            com.google.api.client.http.HttpEncodingStreamingContent r9 = new com.google.api.client.http.HttpEncodingStreamingContent
            r9.<init>(r8, r3)
            long r10 = com.google.api.client.http.AbstractHttpContent.computeLength(r8)
            r8 = r9
            r9 = r10
        L_0x007e:
            r11 = -1
            int r3 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r3 == 0) goto L_0x008d
            java.lang.Long r3 = java.lang.Long.valueOf(r9)
            r6.setContentLength(r3)
            goto L_0x008d
        L_0x008c:
            r8 = r7
        L_0x008d:
            r0.write(r4)
            r0.write(r1)
            r0.write(r5)
            com.google.api.client.http.HttpHeaders.serializeHeadersForMultipartRequests(r6, r7, r7, r0)
            if (r8 == 0) goto L_0x00a4
            r0.write(r5)
            r0.flush()
            r8.writeTo(r14)
        L_0x00a4:
            r0.write(r5)
            goto L_0x0013
        L_0x00a9:
            r0.write(r4)
            r0.write(r1)
            r0.write(r4)
            r0.write(r5)
            r0.flush()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.api.client.http.MultipartContent.writeTo(java.io.OutputStream):void");
    }

    public boolean retrySupported() {
        Iterator<Part> it = this.parts.iterator();
        while (it.hasNext()) {
            if (!it.next().content.retrySupported()) {
                return false;
            }
        }
        return true;
    }

    public MultipartContent setMediaType(HttpMediaType httpMediaType) {
        super.setMediaType(httpMediaType);
        return this;
    }

    public final Collection<Part> getParts() {
        return Collections.unmodifiableCollection(this.parts);
    }

    public MultipartContent addPart(Part part) {
        this.parts.add(Preconditions.checkNotNull(part));
        return this;
    }

    public MultipartContent setParts(Collection<Part> collection) {
        this.parts = new ArrayList<>(collection);
        return this;
    }

    public MultipartContent setContentParts(Collection<? extends HttpContent> collection) {
        this.parts = new ArrayList<>(collection.size());
        for (HttpContent httpContent : collection) {
            addPart(new Part(httpContent));
        }
        return this;
    }

    public final String getBoundary() {
        return getMediaType().getParameter("boundary");
    }

    public MultipartContent setBoundary(String str) {
        getMediaType().setParameter("boundary", (String) Preconditions.checkNotNull(str));
        return this;
    }

    public static final class Part {
        HttpContent content;
        HttpEncoding encoding;
        HttpHeaders headers;

        public Part() {
            this(null);
        }

        public Part(HttpContent httpContent) {
            this(null, httpContent);
        }

        public Part(HttpHeaders httpHeaders, HttpContent httpContent) {
            setHeaders(httpHeaders);
            setContent(httpContent);
        }

        public Part setContent(HttpContent httpContent) {
            this.content = httpContent;
            return this;
        }

        public HttpContent getContent() {
            return this.content;
        }

        public Part setHeaders(HttpHeaders httpHeaders) {
            this.headers = httpHeaders;
            return this;
        }

        public HttpHeaders getHeaders() {
            return this.headers;
        }

        public Part setEncoding(HttpEncoding httpEncoding) {
            this.encoding = httpEncoding;
            return this;
        }

        public HttpEncoding getEncoding() {
            return this.encoding;
        }
    }
}
