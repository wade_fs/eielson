package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzcr;

public final class zzbl extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzbl> CREATOR = new zzbm();
    private final String deviceAddress;
    private final zzcq zzgj;

    zzbl(String str, IBinder iBinder) {
        this.deviceAddress = str;
        this.zzgj = zzcr.zzj(iBinder);
    }

    public zzbl(String str, zzcq zzcq) {
        this.deviceAddress = str;
        this.zzgj = zzcq;
    }

    public final String toString() {
        return String.format("UnclaimBleDeviceRequest{%s}", this.deviceAddress);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.deviceAddress, false);
        zzcq zzcq = this.zzgj;
        SafeParcelWriter.writeIBinder(parcel, 3, zzcq == null ? null : zzcq.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
