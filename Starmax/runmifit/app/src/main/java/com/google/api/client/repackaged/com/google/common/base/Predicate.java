package com.google.api.client.repackaged.com.google.common.base;

import javax.annotation.Nullable;

public interface Predicate<T> {
    boolean apply(@Nullable Object obj);

    boolean equals(@Nullable Object obj);
}
