package com.google.android.gms.fitness;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.internal.fitness.zzam;
import com.google.android.gms.internal.fitness.zzdt;
import com.google.android.gms.tasks.Task;
import java.util.List;

public class RecordingClient extends GoogleApi<FitnessOptions> {
    private static final RecordingApi zzw = new zzdt();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$Settings):void
     arg types: [android.app.Activity, com.google.android.gms.common.api.Api<com.google.android.gms.fitness.FitnessOptions>, com.google.android.gms.fitness.FitnessOptions, com.google.android.gms.common.api.GoogleApi$Settings]
     candidates:
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.StatusExceptionMapper):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$Settings):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.StatusExceptionMapper):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$Settings):void */
    RecordingClient(Activity activity, FitnessOptions fitnessOptions) {
        super(activity, (Api) zzam.zzew, (Api.ApiOptions) fitnessOptions, GoogleApi.Settings.DEFAULT_SETTINGS);
    }

    RecordingClient(Context context, FitnessOptions fitnessOptions) {
        super(context, zzam.zzew, fitnessOptions, GoogleApi.Settings.DEFAULT_SETTINGS);
    }

    public Task<List<Subscription>> listSubscriptions() {
        return PendingResultUtil.toTask(zzw.listSubscriptions(asGoogleApiClient()), zzk.zzf);
    }

    public Task<List<Subscription>> listSubscriptions(DataType dataType) {
        return PendingResultUtil.toTask(zzw.listSubscriptions(asGoogleApiClient(), dataType), zzl.zzf);
    }

    public Task<Void> subscribe(DataSource dataSource) {
        return PendingResultUtil.toVoidTask(zzw.subscribe(asGoogleApiClient(), dataSource));
    }

    public Task<Void> subscribe(DataType dataType) {
        return PendingResultUtil.toVoidTask(zzw.subscribe(asGoogleApiClient(), dataType));
    }

    public Task<Void> unsubscribe(DataSource dataSource) {
        return PendingResultUtil.toVoidTask(zzw.unsubscribe(asGoogleApiClient(), dataSource));
    }

    public Task<Void> unsubscribe(DataType dataType) {
        return PendingResultUtil.toVoidTask(zzw.unsubscribe(asGoogleApiClient(), dataType));
    }

    public Task<Void> unsubscribe(Subscription subscription) {
        return PendingResultUtil.toVoidTask(zzw.unsubscribe(asGoogleApiClient(), subscription));
    }
}
