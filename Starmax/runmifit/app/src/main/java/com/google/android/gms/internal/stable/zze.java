package com.google.android.gms.internal.stable;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;
import java.util.HashMap;

public final class zze {

    public static class zza implements BaseColumns {
        private static HashMap<Uri, zzh> zzagq = new HashMap<>();

        private static zzh zza(ContentResolver contentResolver, Uri uri) {
            zzh zzh = zzagq.get(uri);
            if (zzh == null) {
                zzh zzh2 = new zzh();
                zzagq.put(uri, zzh2);
                contentResolver.registerContentObserver(uri, true, new zzf(null, zzh2));
                return zzh2;
            } else if (!zzh.zzagu.getAndSet(false)) {
                return zzh;
            } else {
                synchronized (zzh) {
                    zzh.zzags.clear();
                    zzh.zzagt = new Object();
                }
                return zzh;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
            r2 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r11 = r11.query(r12, new java.lang.String[]{"value"}, "name=?", new java.lang.String[]{r13}, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0034, code lost:
            if (r11 == null) goto L_0x004b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x003a, code lost:
            if (r11.moveToFirst() != false) goto L_0x003d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x003d, code lost:
            r2 = r11.getString(0);
            zza(r1, r0, r13, r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0044, code lost:
            if (r11 == null) goto L_0x0049;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0046, code lost:
            r11.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
            zza(r1, r0, r13, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x004e, code lost:
            if (r11 == null) goto L_0x0053;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0050, code lost:
            r11.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0053, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0054, code lost:
            r12 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0055, code lost:
            r2 = r11;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0057, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0058, code lost:
            r2 = r11;
            r11 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x005c, code lost:
            r12 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x005e, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x005f, code lost:
            r11 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
            android.util.Log.e("GoogleSettings", "Can't get key " + r13 + " from " + r12, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x007b, code lost:
            if (r2 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x007d, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0081, code lost:
            if (r2 != null) goto L_0x0083;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0083, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0086, code lost:
            throw r12;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
            return r11;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
            return r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
            return r11;
         */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x007d  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x0083  */
        /* JADX WARNING: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        protected static java.lang.String zza(android.content.ContentResolver r11, android.net.Uri r12, java.lang.String r13) {
            /*
                java.lang.Class<com.google.android.gms.internal.stable.zze$zza> r0 = com.google.android.gms.internal.stable.zze.zza.class
                monitor-enter(r0)
                com.google.android.gms.internal.stable.zzh r1 = zza(r11, r12)     // Catch:{ all -> 0x008a }
                monitor-exit(r0)     // Catch:{ all -> 0x008a }
                monitor-enter(r1)
                java.lang.Object r0 = r1.zzagt     // Catch:{ all -> 0x0087 }
                java.util.HashMap<java.lang.String, java.lang.String> r2 = r1.zzags     // Catch:{ all -> 0x0087 }
                boolean r2 = r2.containsKey(r13)     // Catch:{ all -> 0x0087 }
                if (r2 == 0) goto L_0x001d
                java.util.HashMap<java.lang.String, java.lang.String> r11 = r1.zzags     // Catch:{ all -> 0x0087 }
                java.lang.Object r11 = r11.get(r13)     // Catch:{ all -> 0x0087 }
                java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x0087 }
                monitor-exit(r1)     // Catch:{ all -> 0x0087 }
                return r11
            L_0x001d:
                monitor-exit(r1)     // Catch:{ all -> 0x0087 }
                r2 = 0
                java.lang.String r3 = "value"
                java.lang.String[] r6 = new java.lang.String[]{r3}     // Catch:{ SQLException -> 0x005e }
                java.lang.String r7 = "name=?"
                r3 = 1
                java.lang.String[] r8 = new java.lang.String[r3]     // Catch:{ SQLException -> 0x005e }
                r3 = 0
                r8[r3] = r13     // Catch:{ SQLException -> 0x005e }
                r9 = 0
                r4 = r11
                r5 = r12
                android.database.Cursor r11 = r4.query(r5, r6, r7, r8, r9)     // Catch:{ SQLException -> 0x005e }
                if (r11 == 0) goto L_0x004b
                boolean r4 = r11.moveToFirst()     // Catch:{ SQLException -> 0x0057, all -> 0x0054 }
                if (r4 != 0) goto L_0x003d
                goto L_0x004b
            L_0x003d:
                java.lang.String r2 = r11.getString(r3)     // Catch:{ SQLException -> 0x0057, all -> 0x0054 }
                zza(r1, r0, r13, r2)     // Catch:{ SQLException -> 0x0057, all -> 0x0054 }
                if (r11 == 0) goto L_0x0049
                r11.close()
            L_0x0049:
                r11 = r2
                goto L_0x0080
            L_0x004b:
                zza(r1, r0, r13, r2)     // Catch:{ SQLException -> 0x0057, all -> 0x0054 }
                if (r11 == 0) goto L_0x0053
                r11.close()
            L_0x0053:
                return r2
            L_0x0054:
                r12 = move-exception
                r2 = r11
                goto L_0x0081
            L_0x0057:
                r0 = move-exception
                r10 = r2
                r2 = r11
                r11 = r10
                goto L_0x0060
            L_0x005c:
                r12 = move-exception
                goto L_0x0081
            L_0x005e:
                r0 = move-exception
                r11 = r2
            L_0x0060:
                java.lang.String r1 = "GoogleSettings"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x005c }
                java.lang.String r4 = "Can't get key "
                r3.<init>(r4)     // Catch:{ all -> 0x005c }
                r3.append(r13)     // Catch:{ all -> 0x005c }
                java.lang.String r13 = " from "
                r3.append(r13)     // Catch:{ all -> 0x005c }
                r3.append(r12)     // Catch:{ all -> 0x005c }
                java.lang.String r12 = r3.toString()     // Catch:{ all -> 0x005c }
                android.util.Log.e(r1, r12, r0)     // Catch:{ all -> 0x005c }
                if (r2 == 0) goto L_0x0080
                r2.close()
            L_0x0080:
                return r11
            L_0x0081:
                if (r2 == 0) goto L_0x0086
                r2.close()
            L_0x0086:
                throw r12
            L_0x0087:
                r11 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0087 }
                throw r11
            L_0x008a:
                r11 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x008a }
                throw r11
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.stable.zze.zza.zza(android.content.ContentResolver, android.net.Uri, java.lang.String):java.lang.String");
        }

        private static void zza(zzh zzh, Object obj, String str, String str2) {
            synchronized (zzh) {
                if (obj == zzh.zzagt) {
                    zzh.zzags.put(str, str2);
                }
            }
        }
    }
}
