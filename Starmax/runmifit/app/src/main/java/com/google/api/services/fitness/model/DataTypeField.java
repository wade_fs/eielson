package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class DataTypeField extends GenericJson {
    @Key
    private String format;
    @Key
    private String name;
    @Key
    private Boolean optional;

    public String getFormat() {
        return this.format;
    }

    public DataTypeField setFormat(String str) {
        this.format = str;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public DataTypeField setName(String str) {
        this.name = str;
        return this;
    }

    public Boolean getOptional() {
        return this.optional;
    }

    public DataTypeField setOptional(Boolean bool) {
        this.optional = bool;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.DataTypeField.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.DataTypeField.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.DataTypeField
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public DataTypeField set(String str, Object obj) {
        return (DataTypeField) super.set(str, obj);
    }

    public DataTypeField clone() {
        return (DataTypeField) super.clone();
    }
}
