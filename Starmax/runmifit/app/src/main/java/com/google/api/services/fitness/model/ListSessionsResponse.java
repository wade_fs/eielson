package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;
import java.util.List;

public final class ListSessionsResponse extends GenericJson {
    @Key
    private List<Session> deletedSession;
    @Key
    private String nextPageToken;
    @Key
    private List<Session> session;

    public List<Session> getDeletedSession() {
        return this.deletedSession;
    }

    public ListSessionsResponse setDeletedSession(List<Session> list) {
        this.deletedSession = list;
        return this;
    }

    public String getNextPageToken() {
        return this.nextPageToken;
    }

    public ListSessionsResponse setNextPageToken(String str) {
        this.nextPageToken = str;
        return this;
    }

    public List<Session> getSession() {
        return this.session;
    }

    public ListSessionsResponse setSession(List<Session> list) {
        this.session = list;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.ListSessionsResponse.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.ListSessionsResponse.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.ListSessionsResponse
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public ListSessionsResponse set(String str, Object obj) {
        return (ListSessionsResponse) super.set(str, obj);
    }

    public ListSessionsResponse clone() {
        return (ListSessionsResponse) super.clone();
    }
}
