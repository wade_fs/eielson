package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.fitness.zzbn;
import com.google.android.gms.internal.fitness.zzbo;

public final class zzs extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzs> CREATOR = new zzt();
    private final String name;
    private final zzbn zzhf;

    zzs(String str, IBinder iBinder) {
        this.name = str;
        this.zzhf = zzbo.zze(iBinder);
    }

    public zzs(String str, zzbn zzbn) {
        this.name = str;
        this.zzhf = zzbn;
    }

    public final boolean equals(Object obj) {
        if (obj != this) {
            return (obj instanceof zzs) && Objects.equal(this.name, ((zzs) obj).name);
        }
        return true;
    }

    public final int hashCode() {
        return Objects.hashCode(this.name);
    }

    public final String toString() {
        return Objects.toStringHelper(this).add(Config.FEED_LIST_NAME, this.name).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.name, false);
        SafeParcelWriter.writeIBinder(parcel, 3, this.zzhf.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
