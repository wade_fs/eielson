package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Data;
import com.google.api.client.util.Key;
import java.util.List;

public final class ListDataSourcesResponse extends GenericJson {
    @Key
    private List<DataSource> dataSource;

    static {
        Data.nullOf(DataSource.class);
    }

    public List<DataSource> getDataSource() {
        return this.dataSource;
    }

    public ListDataSourcesResponse setDataSource(List<DataSource> list) {
        this.dataSource = list;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.ListDataSourcesResponse.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.ListDataSourcesResponse.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.ListDataSourcesResponse
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public ListDataSourcesResponse set(String str, Object obj) {
        return (ListDataSourcesResponse) super.set(str, obj);
    }

    public ListDataSourcesResponse clone() {
        return (ListDataSourcesResponse) super.clone();
    }
}
