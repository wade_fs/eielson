package com.google.api.client.util;

public interface NanoClock {
    public static final NanoClock SYSTEM = new NanoClock() {
        /* class com.google.api.client.util.NanoClock.C16051 */

        public long nanoTime() {
            return System.nanoTime();
        }
    };

    long nanoTime();
}
