package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.zzaj;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;

final class zzdv extends zzap<ListSubscriptionsResult> {
    private final /* synthetic */ DataType zzfm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdv(zzdt zzdt, GoogleApiClient googleApiClient, DataType dataType) {
        super(googleApiClient);
        this.zzfm = dataType;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return ListSubscriptionsResult.zzd(status);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzaj.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.internal.fitness.zzch):void
     arg types: [com.google.android.gms.fitness.data.DataType, com.google.android.gms.internal.fitness.zzdz]
     candidates:
      com.google.android.gms.fitness.request.zzaj.<init>(com.google.android.gms.fitness.data.DataType, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzaj.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.internal.fitness.zzch):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcb) ((zzam) anyClient).getService()).zza(new zzaj(this.zzfm, (zzch) new zzdz(this, null)));
    }
}
