package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.request.zzk;

final class zzdk extends zzal {
    private final /* synthetic */ DataSet zzff;
    private final /* synthetic */ boolean zzfg = false;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdk(zzdj zzdj, GoogleApiClient googleApiClient, DataSet dataSet, boolean z) {
        super(googleApiClient);
        this.zzff = dataSet;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzk.<init>(com.google.android.gms.fitness.data.DataSet, com.google.android.gms.internal.fitness.zzcq, boolean):void
     arg types: [com.google.android.gms.fitness.data.DataSet, com.google.android.gms.internal.fitness.zzen, boolean]
     candidates:
      com.google.android.gms.fitness.request.zzk.<init>(com.google.android.gms.fitness.data.DataSet, android.os.IBinder, boolean):void
      com.google.android.gms.fitness.request.zzk.<init>(com.google.android.gms.fitness.data.DataSet, com.google.android.gms.internal.fitness.zzcq, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbz) ((zzag) anyClient).getService()).zza(new zzk(this.zzff, (zzcq) new zzen(this), this.zzfg));
    }
}
