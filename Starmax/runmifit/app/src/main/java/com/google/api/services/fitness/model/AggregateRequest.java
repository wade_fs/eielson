package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonString;
import com.google.api.client.util.Data;
import com.google.api.client.util.Key;
import java.util.List;

public final class AggregateRequest extends GenericJson {
    @Key
    private List<AggregateBy> aggregateBy;
    @Key
    private BucketByActivity bucketByActivitySegment;
    @Key
    private BucketByActivity bucketByActivityType;
    @Key
    private BucketBySession bucketBySession;
    @Key
    private BucketByTime bucketByTime;
    @JsonString
    @Key
    private Long endTimeMillis;
    @JsonString
    @Key
    private Long startTimeMillis;

    static {
        Data.nullOf(AggregateBy.class);
    }

    public List<AggregateBy> getAggregateBy() {
        return this.aggregateBy;
    }

    public AggregateRequest setAggregateBy(List<AggregateBy> list) {
        this.aggregateBy = list;
        return this;
    }

    public BucketByActivity getBucketByActivitySegment() {
        return this.bucketByActivitySegment;
    }

    public AggregateRequest setBucketByActivitySegment(BucketByActivity bucketByActivity) {
        this.bucketByActivitySegment = bucketByActivity;
        return this;
    }

    public BucketByActivity getBucketByActivityType() {
        return this.bucketByActivityType;
    }

    public AggregateRequest setBucketByActivityType(BucketByActivity bucketByActivity) {
        this.bucketByActivityType = bucketByActivity;
        return this;
    }

    public BucketBySession getBucketBySession() {
        return this.bucketBySession;
    }

    public AggregateRequest setBucketBySession(BucketBySession bucketBySession2) {
        this.bucketBySession = bucketBySession2;
        return this;
    }

    public BucketByTime getBucketByTime() {
        return this.bucketByTime;
    }

    public AggregateRequest setBucketByTime(BucketByTime bucketByTime2) {
        this.bucketByTime = bucketByTime2;
        return this;
    }

    public Long getEndTimeMillis() {
        return this.endTimeMillis;
    }

    public AggregateRequest setEndTimeMillis(Long l) {
        this.endTimeMillis = l;
        return this;
    }

    public Long getStartTimeMillis() {
        return this.startTimeMillis;
    }

    public AggregateRequest setStartTimeMillis(Long l) {
        this.startTimeMillis = l;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.AggregateRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.AggregateRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.AggregateRequest
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public AggregateRequest set(String str, Object obj) {
        return (AggregateRequest) super.set(str, obj);
    }

    public AggregateRequest clone() {
        return (AggregateRequest) super.clone();
    }
}
