package com.google.common.collect;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMapEntry;
import java.util.Map;

public abstract class ImmutableBiMap<K, V> extends ImmutableMap<K, V> implements BiMap<K, V> {
    private static final Map.Entry<?, ?>[] EMPTY_ENTRY_ARRAY = new Map.Entry[0];

    public abstract ImmutableBiMap<V, K> inverse();

    /* renamed from: of */
    public static <K, V> ImmutableBiMap<K, V> m5310of() {
        return EmptyImmutableBiMap.INSTANCE;
    }

    /* renamed from: of */
    public static <K, V> ImmutableBiMap<K, V> m5311of(Object obj, Object obj2) {
        return new SingletonImmutableBiMap(obj, obj2);
    }

    /* renamed from: of */
    public static <K, V> ImmutableBiMap<K, V> m5312of(K k, V v, K k2, V v2) {
        return new RegularImmutableBiMap((ImmutableMapEntry.TerminalEntry<?, ?>[]) new ImmutableMapEntry.TerminalEntry[]{entryOf(k, v), entryOf(k2, v2)});
    }

    /* renamed from: of */
    public static <K, V> ImmutableBiMap<K, V> m5313of(K k, V v, K k2, V v2, K k3, V v3) {
        return new RegularImmutableBiMap((ImmutableMapEntry.TerminalEntry<?, ?>[]) new ImmutableMapEntry.TerminalEntry[]{entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3)});
    }

    /* renamed from: of */
    public static <K, V> ImmutableBiMap<K, V> m5314of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return new RegularImmutableBiMap((ImmutableMapEntry.TerminalEntry<?, ?>[]) new ImmutableMapEntry.TerminalEntry[]{entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4)});
    }

    /* renamed from: of */
    public static <K, V> ImmutableBiMap<K, V> m5315of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return new RegularImmutableBiMap((ImmutableMapEntry.TerminalEntry<?, ?>[]) new ImmutableMapEntry.TerminalEntry[]{entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4), entryOf(k5, v5)});
    }

    public static <K, V> Builder<K, V> builder() {
        return new Builder<>();
    }

    public static final class Builder<K, V> extends ImmutableMap.Builder<K, V> {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.ImmutableMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder<K, V>
         arg types: [K, V]
         candidates:
          com.google.common.collect.ImmutableBiMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableBiMap$Builder<K, V>
          com.google.common.collect.ImmutableMap.Builder.put(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap$Builder<K, V> */
        public Builder<K, V> put(K k, V v) {
            super.put((Object) k, (Object) v);
            return this;
        }

        public Builder<K, V> putAll(Map<? extends K, ? extends V> map) {
            super.putAll((Map) map);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.collect.ImmutableBiMap.of(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableBiMap<K, V>
         arg types: [java.lang.Object, java.lang.Object]
         candidates:
          com.google.common.collect.ImmutableMap.of(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap<K, V>
          com.google.common.collect.ImmutableBiMap.of(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableBiMap<K, V> */
        public ImmutableBiMap<K, V> build() {
            int i = this.size;
            if (i == 0) {
                return ImmutableBiMap.m5310of();
            }
            if (i != 1) {
                return new RegularImmutableBiMap(this.size, this.entries);
            }
            return ImmutableBiMap.m5311of(this.entries[0].getKey(), this.entries[0].getValue());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.ImmutableBiMap.of(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableBiMap<K, V>
     arg types: [java.lang.Object, java.lang.Object]
     candidates:
      com.google.common.collect.ImmutableMap.of(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableMap<K, V>
      com.google.common.collect.ImmutableBiMap.of(java.lang.Object, java.lang.Object):com.google.common.collect.ImmutableBiMap<K, V> */
    public static <K, V> ImmutableBiMap<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if (map instanceof ImmutableBiMap) {
            ImmutableBiMap<K, V> immutableBiMap = (ImmutableBiMap) map;
            if (!immutableBiMap.isPartialView()) {
                return immutableBiMap;
            }
        }
        Map.Entry[] entryArr = (Map.Entry[]) map.entrySet().toArray(EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return m5310of();
        }
        if (length != 1) {
            return new RegularImmutableBiMap(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return m5311of(entry.getKey(), entry.getValue());
    }

    ImmutableBiMap() {
    }

    public ImmutableSet<V> values() {
        return inverse().keySet();
    }

    @Deprecated
    public V forcePut(K k, V v) {
        throw new UnsupportedOperationException();
    }

    private static class SerializedForm extends ImmutableMap.SerializedForm {
        private static final long serialVersionUID = 0;

        SerializedForm(ImmutableBiMap<?, ?> immutableBiMap) {
            super(immutableBiMap);
        }

        /* access modifiers changed from: package-private */
        public Object readResolve() {
            return createMap(new Builder());
        }
    }

    /* access modifiers changed from: package-private */
    public Object writeReplace() {
        return new SerializedForm(this);
    }
}
