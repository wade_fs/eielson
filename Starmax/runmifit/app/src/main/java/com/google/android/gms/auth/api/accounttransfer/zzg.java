package com.google.android.gms.auth.api.accounttransfer;

import android.os.RemoteException;
import com.google.android.gms.auth.api.accounttransfer.AccountTransferClient;
import com.google.android.gms.internal.auth.zzac;
import com.google.android.gms.internal.auth.zzai;

final class zzg extends AccountTransferClient.zzf {
    private final /* synthetic */ zzai zzaz;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzg(AccountTransferClient accountTransferClient, zzai zzai) {
        super(null);
        this.zzaz = zzai;
    }

    /* access modifiers changed from: protected */
    public final void zzd(zzac zzac) throws RemoteException {
        zzac.zzd(this.zzbi, this.zzaz);
    }
}
