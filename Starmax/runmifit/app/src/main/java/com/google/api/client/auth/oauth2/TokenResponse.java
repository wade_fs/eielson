package com.google.api.client.auth.oauth2;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;
import com.google.api.client.util.Preconditions;
import com.tencent.connect.common.Constants;

public class TokenResponse extends GenericJson {
    @Key(Constants.PARAM_ACCESS_TOKEN)
    private String accessToken;
    @Key(Constants.PARAM_EXPIRES_IN)
    private Long expiresInSeconds;
    @Key("refresh_token")
    private String refreshToken;
    @Key
    private String scope;
    @Key("token_type")
    private String tokenType;

    public final String getAccessToken() {
        return this.accessToken;
    }

    public TokenResponse setAccessToken(String str) {
        this.accessToken = (String) Preconditions.checkNotNull(str);
        return this;
    }

    public final String getTokenType() {
        return this.tokenType;
    }

    public TokenResponse setTokenType(String str) {
        this.tokenType = (String) Preconditions.checkNotNull(str);
        return this;
    }

    public final Long getExpiresInSeconds() {
        return this.expiresInSeconds;
    }

    public TokenResponse setExpiresInSeconds(Long l) {
        this.expiresInSeconds = l;
        return this;
    }

    public final String getRefreshToken() {
        return this.refreshToken;
    }

    public TokenResponse setRefreshToken(String str) {
        this.refreshToken = str;
        return this;
    }

    public final String getScope() {
        return this.scope;
    }

    public TokenResponse setScope(String str) {
        this.scope = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.TokenResponse
      com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public TokenResponse set(String str, Object obj) {
        return (TokenResponse) super.set(str, obj);
    }

    public TokenResponse clone() {
        return (TokenResponse) super.clone();
    }
}
