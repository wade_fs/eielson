package com.google.common.p039io;

import com.google.common.base.Preconditions;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import javax.annotation.Nullable;

/* renamed from: com.google.common.io.MultiInputStream */
final class MultiInputStream extends InputStream {

    /* renamed from: in */
    private InputStream f4605in;

    /* renamed from: it */
    private Iterator<? extends ByteSource> f4606it;

    public boolean markSupported() {
        return false;
    }

    public MultiInputStream(Iterator<? extends ByteSource> it) throws IOException {
        this.f4606it = (Iterator) Preconditions.checkNotNull(it);
        advance();
    }

    public void close() throws IOException {
        InputStream inputStream = this.f4605in;
        if (inputStream != null) {
            try {
                super.close();
            } finally {
                this.f4605in = null;
            }
        }
    }

    private void advance() throws IOException {
        close();
        if (this.f4606it.hasNext()) {
            this.f4605in = ((ByteSource) this.f4606it.next()).openStream();
        }
    }

    public int available() throws IOException {
        InputStream inputStream = this.f4605in;
        if (inputStream == null) {
            return 0;
        }
        return super.available();
    }

    public int read() throws IOException {
        InputStream inputStream = this.f4605in;
        if (inputStream == null) {
            return -1;
        }
        int read = super.read();
        if (read != -1) {
            return read;
        }
        advance();
        return read();
    }

    public int read(@Nullable byte[] bArr, int i, int i2) throws IOException {
        InputStream inputStream = this.f4605in;
        if (inputStream == null) {
            return -1;
        }
        int read = super.read(bArr, i, i2);
        if (read != -1) {
            return read;
        }
        advance();
        return read(bArr, i, i2);
    }

    public long skip(long j) throws IOException {
        InputStream inputStream = this.f4605in;
        if (inputStream == null || j <= 0) {
            return 0;
        }
        long skip = super.skip(j);
        if (skip != 0) {
            return skip;
        }
        if (read() == -1) {
            return 0;
        }
        return this.f4605in.skip(j - 1) + 1;
    }
}
