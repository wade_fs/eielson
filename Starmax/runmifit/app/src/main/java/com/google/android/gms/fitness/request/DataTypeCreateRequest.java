package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.internal.fitness.zzbn;
import com.google.android.gms.internal.fitness.zzbo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataTypeCreateRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<DataTypeCreateRequest> CREATOR = new zzr();
    private final String name;
    private final List<Field> zzbu;
    private final zzbn zzhf;

    public static class Builder {
        /* access modifiers changed from: private */
        public String name;
        /* access modifiers changed from: private */
        public List<Field> zzbu = new ArrayList();

        public Builder addField(Field field) {
            if (!this.zzbu.contains(field)) {
                this.zzbu.add(field);
            }
            return this;
        }

        public Builder addField(String str, int i) {
            Preconditions.checkArgument(str != null && !str.isEmpty(), "Invalid name specified");
            return addField(Field.zza(str, i));
        }

        public DataTypeCreateRequest build() {
            Preconditions.checkState(this.name != null, "Must set the name");
            Preconditions.checkState(!this.zzbu.isEmpty(), "Must specify the data fields");
            return new DataTypeCreateRequest(this);
        }

        public Builder setName(String str) {
            this.name = str;
            return this;
        }
    }

    private DataTypeCreateRequest(Builder builder) {
        this(builder.name, builder.zzbu, (zzbn) null);
    }

    public DataTypeCreateRequest(DataTypeCreateRequest dataTypeCreateRequest, zzbn zzbn) {
        this(dataTypeCreateRequest.name, dataTypeCreateRequest.zzbu, zzbn);
    }

    DataTypeCreateRequest(String str, List<Field> list, IBinder iBinder) {
        this.name = str;
        this.zzbu = Collections.unmodifiableList(list);
        this.zzhf = zzbo.zze(iBinder);
    }

    private DataTypeCreateRequest(String str, List<Field> list, zzbn zzbn) {
        this.name = str;
        this.zzbu = Collections.unmodifiableList(list);
        this.zzhf = zzbn;
    }

    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof DataTypeCreateRequest) {
                DataTypeCreateRequest dataTypeCreateRequest = (DataTypeCreateRequest) obj;
                if (Objects.equal(this.name, dataTypeCreateRequest.name) && Objects.equal(this.zzbu, dataTypeCreateRequest.zzbu)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public List<Field> getFields() {
        return this.zzbu;
    }

    public String getName() {
        return this.name;
    }

    public int hashCode() {
        return Objects.hashCode(this.name, this.zzbu);
    }

    public String toString() {
        return Objects.toStringHelper(this).add(Config.FEED_LIST_NAME, this.name).add("fields", this.zzbu).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, getName(), false);
        SafeParcelWriter.writeTypedList(parcel, 2, getFields(), false);
        zzbn zzbn = this.zzhf;
        SafeParcelWriter.writeIBinder(parcel, 3, zzbn == null ? null : zzbn.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
