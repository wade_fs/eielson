package com.google.api.client.googleapis.auth.oauth2;

import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.util.Key;
import com.google.api.client.util.Preconditions;
import java.io.IOException;

public class GoogleTokenResponse extends TokenResponse {
    @Key("id_token")
    private String idToken;

    public GoogleTokenResponse setAccessToken(String str) {
        return (GoogleTokenResponse) super.setAccessToken(str);
    }

    public GoogleTokenResponse setTokenType(String str) {
        return (GoogleTokenResponse) super.setTokenType(str);
    }

    public GoogleTokenResponse setExpiresInSeconds(Long l) {
        return (GoogleTokenResponse) super.setExpiresInSeconds(l);
    }

    public GoogleTokenResponse setRefreshToken(String str) {
        return (GoogleTokenResponse) super.setRefreshToken(str);
    }

    public GoogleTokenResponse setScope(String str) {
        return (GoogleTokenResponse) super.setScope(str);
    }

    public final String getIdToken() {
        return this.idToken;
    }

    public GoogleTokenResponse setIdToken(String str) {
        this.idToken = (String) Preconditions.checkNotNull(str);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.parse(com.google.api.client.json.JsonFactory, java.lang.String):com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
     arg types: [com.google.api.client.json.JsonFactory, java.lang.String]
     candidates:
      com.google.api.client.auth.openidconnect.IdToken.parse(com.google.api.client.json.JsonFactory, java.lang.String):com.google.api.client.auth.openidconnect.IdToken
      com.google.api.client.json.webtoken.JsonWebSignature.parse(com.google.api.client.json.JsonFactory, java.lang.String):com.google.api.client.json.webtoken.JsonWebSignature
      com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.parse(com.google.api.client.json.JsonFactory, java.lang.String):com.google.api.client.googleapis.auth.oauth2.GoogleIdToken */
    public GoogleIdToken parseIdToken() throws IOException {
        return GoogleIdToken.parse(getFactory(), getIdToken());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.TokenResponse
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse
      com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
      com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
      com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.TokenResponse.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.TokenResponse */
    public GoogleTokenResponse set(String str, Object obj) {
        return (GoogleTokenResponse) super.set(str, obj);
    }

    public GoogleTokenResponse clone() {
        return (GoogleTokenResponse) super.clone();
    }
}
