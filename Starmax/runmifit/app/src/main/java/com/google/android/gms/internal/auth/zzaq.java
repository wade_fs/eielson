package com.google.android.gms.internal.auth;

import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

final class zzaq extends zzam {
    private final /* synthetic */ zzap zzds;

    zzaq(zzap zzap) {
        this.zzds = zzap;
    }

    public final void zzd(Status status, Credential credential) {
        this.zzds.setResult((Result) new zzan(status, credential));
    }

    public final void zze(Status status) {
        this.zzds.setResult((Result) zzan.zzf(status));
    }
}
