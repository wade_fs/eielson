package com.google.android.gms.internal.ads;

import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;

final class zzack implements ViewTreeObserver.OnGlobalLayoutListener {
    private final /* synthetic */ zzace zzcbi;
    private final /* synthetic */ WeakReference zzcbj;

    zzack(zzace zzace, WeakReference weakReference) {
        this.zzcbi = zzace;
        this.zzcbj = weakReference;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzace.zza(com.google.android.gms.internal.ads.zzace, java.lang.ref.WeakReference, boolean):void
     arg types: [com.google.android.gms.internal.ads.zzace, java.lang.ref.WeakReference, int]
     candidates:
      com.google.android.gms.internal.ads.zzace.zza(com.google.android.gms.internal.ads.zzaoj, com.google.android.gms.internal.ads.zzaqw, boolean):void
      com.google.android.gms.internal.ads.zzace.zza(com.google.android.gms.internal.ads.zzace, java.lang.ref.WeakReference, boolean):void */
    public final void onGlobalLayout() {
        this.zzcbi.zza(this.zzcbj, false);
    }
}
