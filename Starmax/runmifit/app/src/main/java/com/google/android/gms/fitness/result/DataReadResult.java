package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.RawBucket;
import com.google.android.gms.fitness.data.RawDataSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DataReadResult extends AbstractSafeParcelable implements Result {
    public static final Parcelable.Creator<DataReadResult> CREATOR = new zzc();
    private final List<DataSet> zzaj;
    private final List<DataSource> zzav;
    private final Status zzin;
    private final List<Bucket> zzio;
    private int zzip;

    DataReadResult(List<RawDataSet> list, Status status, List<RawBucket> list2, int i, List<DataSource> list3) {
        this.zzin = status;
        this.zzip = i;
        this.zzav = list3;
        this.zzaj = new ArrayList(list.size());
        for (RawDataSet rawDataSet : list) {
            this.zzaj.add(new DataSet(rawDataSet, list3));
        }
        this.zzio = new ArrayList(list2.size());
        for (RawBucket rawBucket : list2) {
            this.zzio.add(new Bucket(rawBucket, list3));
        }
    }

    private DataReadResult(List<DataSet> list, List<Bucket> list2, Status status) {
        this.zzaj = list;
        this.zzin = status;
        this.zzio = list2;
        this.zzip = 1;
        this.zzav = new ArrayList();
    }

    public static DataReadResult zza(Status status, List<DataType> list, List<DataSource> list2) {
        ArrayList arrayList = new ArrayList();
        for (DataSource dataSource : list2) {
            arrayList.add(DataSet.create(dataSource));
        }
        for (DataType dataType : list) {
            arrayList.add(DataSet.create(new DataSource.Builder().setDataType(dataType).setType(1).setName("Default").build()));
        }
        return new DataReadResult(arrayList, Collections.emptyList(), status);
    }

    private static void zza(DataSet dataSet, List<DataSet> list) {
        for (DataSet dataSet2 : list) {
            if (dataSet2.getDataSource().equals(dataSet.getDataSource())) {
                dataSet2.zza(dataSet.getDataPoints());
                return;
            }
        }
        list.add(dataSet);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DataReadResult) {
                DataReadResult dataReadResult = (DataReadResult) obj;
                if (this.zzin.equals(dataReadResult.zzin) && Objects.equal(this.zzaj, dataReadResult.zzaj) && Objects.equal(this.zzio, dataReadResult.zzio)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public List<Bucket> getBuckets() {
        return this.zzio;
    }

    public DataSet getDataSet(DataSource dataSource) {
        for (DataSet dataSet : this.zzaj) {
            if (dataSource.equals(dataSet.getDataSource())) {
                return dataSet;
            }
        }
        return DataSet.create(dataSource);
    }

    public DataSet getDataSet(DataType dataType) {
        for (DataSet dataSet : this.zzaj) {
            if (dataType.equals(dataSet.getDataType())) {
                return dataSet;
            }
        }
        return DataSet.create(new DataSource.Builder().setDataType(dataType).setType(1).build());
    }

    public List<DataSet> getDataSets() {
        return this.zzaj;
    }

    public Status getStatus() {
        return this.zzin;
    }

    public int hashCode() {
        return Objects.hashCode(this.zzin, this.zzaj, this.zzio);
    }

    public String toString() {
        Object obj;
        Object obj2;
        Objects.ToStringHelper add = Objects.toStringHelper(this).add(NotificationCompat.CATEGORY_STATUS, this.zzin);
        if (this.zzaj.size() > 5) {
            int size = this.zzaj.size();
            StringBuilder sb = new StringBuilder(21);
            sb.append(size);
            sb.append(" data sets");
            obj = sb.toString();
        } else {
            obj = this.zzaj;
        }
        Objects.ToStringHelper add2 = add.add("dataSets", obj);
        if (this.zzio.size() > 5) {
            int size2 = this.zzio.size();
            StringBuilder sb2 = new StringBuilder(19);
            sb2.append(size2);
            sb2.append(" buckets");
            obj2 = sb2.toString();
        } else {
            obj2 = this.zzio;
        }
        return add2.add("buckets", obj2).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        ArrayList arrayList = new ArrayList(this.zzaj.size());
        for (DataSet dataSet : this.zzaj) {
            arrayList.add(new RawDataSet(dataSet, this.zzav));
        }
        SafeParcelWriter.writeList(parcel, 1, arrayList, false);
        SafeParcelWriter.writeParcelable(parcel, 2, getStatus(), i, false);
        ArrayList arrayList2 = new ArrayList(this.zzio.size());
        for (Bucket bucket : this.zzio) {
            arrayList2.add(new RawBucket(bucket, this.zzav));
        }
        SafeParcelWriter.writeList(parcel, 3, arrayList2, false);
        SafeParcelWriter.writeInt(parcel, 5, this.zzip);
        SafeParcelWriter.writeTypedList(parcel, 6, this.zzav, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final void zzb(DataReadResult dataReadResult) {
        for (DataSet dataSet : dataReadResult.getDataSets()) {
            zza(dataSet, this.zzaj);
        }
        for (Bucket bucket : dataReadResult.getBuckets()) {
            Iterator<Bucket> it = this.zzio.iterator();
            while (true) {
                if (!it.hasNext()) {
                    this.zzio.add(bucket);
                    break;
                }
                Bucket next = it.next();
                if (next.zza(bucket)) {
                    for (DataSet dataSet2 : bucket.getDataSets()) {
                        zza(dataSet2, next.getDataSets());
                    }
                }
            }
        }
    }

    public final int zzz() {
        return this.zzip;
    }
}
