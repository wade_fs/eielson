package com.google.api.client.util;

public interface Clock {
    public static final Clock SYSTEM = new Clock() {
        /* class com.google.api.client.util.Clock.C16041 */

        public long currentTimeMillis() {
            return System.currentTimeMillis();
        }
    };

    long currentTimeMillis();
}
