package com.google.android.gms.fitness;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.UnregisterListenerMethod;
import com.google.android.gms.fitness.data.zzt;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.zzal;
import com.google.android.gms.fitness.request.zzan;
import com.google.android.gms.fitness.request.zzar;
import com.google.android.gms.internal.fitness.zzas;
import com.google.android.gms.internal.fitness.zzcd;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzen;
import com.google.android.gms.tasks.TaskCompletionSource;

final class zzo extends UnregisterListenerMethod<zzas, OnDataPointListener> {
    private final /* synthetic */ ListenerHolder zzg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzo(SensorsClient sensorsClient, ListenerHolder.ListenerKey listenerKey, ListenerHolder listenerHolder) {
        super(listenerKey);
        this.zzg = listenerHolder;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzar.<init>(com.google.android.gms.fitness.data.zzt, android.app.PendingIntent, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [com.google.android.gms.fitness.request.zzal, ?[OBJECT, ARRAY], com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zzar.<init>(android.os.IBinder, android.app.PendingIntent, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzar.<init>(com.google.android.gms.fitness.data.zzt, android.app.PendingIntent, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void unregisterListener(Api.AnyClient anyClient, TaskCompletionSource taskCompletionSource) throws RemoteException {
        zzas zzas = (zzas) anyClient;
        zzal zzd = zzan.zzw().zzd(this.zzg);
        if (zzd == null) {
            taskCompletionSource.setResult(false);
            return;
        }
        ((zzcd) zzas.getService()).zza(new zzar((zzt) zzd, (PendingIntent) null, (zzcq) zzen.zzb(taskCompletionSource)));
    }
}
