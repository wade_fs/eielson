package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzcr;

public class DataUpdateListenerRegistrationRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<DataUpdateListenerRegistrationRequest> CREATOR = new zzv();
    private final zzcq zzgj;
    private final PendingIntent zzhg;
    private DataType zzq;
    private DataSource zzr;

    public static class Builder {
        /* access modifiers changed from: private */
        public PendingIntent zzhg;
        /* access modifiers changed from: private */
        public DataType zzq;
        /* access modifiers changed from: private */
        public DataSource zzr;

        public DataUpdateListenerRegistrationRequest build() {
            Preconditions.checkState((this.zzr == null && this.zzq == null) ? false : true, "Set either dataSource or dataTYpe");
            Preconditions.checkNotNull(this.zzhg, "pendingIntent must be set");
            return new DataUpdateListenerRegistrationRequest(this);
        }

        public Builder setDataSource(DataSource dataSource) throws NullPointerException {
            Preconditions.checkNotNull(dataSource);
            this.zzr = dataSource;
            return this;
        }

        public Builder setDataType(DataType dataType) {
            Preconditions.checkNotNull(dataType);
            this.zzq = dataType;
            return this;
        }

        public Builder setPendingIntent(PendingIntent pendingIntent) {
            Preconditions.checkNotNull(pendingIntent);
            this.zzhg = pendingIntent;
            return this;
        }
    }

    public DataUpdateListenerRegistrationRequest(DataSource dataSource, DataType dataType, PendingIntent pendingIntent, IBinder iBinder) {
        this.zzr = dataSource;
        this.zzq = dataType;
        this.zzhg = pendingIntent;
        this.zzgj = zzcr.zzj(iBinder);
    }

    private DataUpdateListenerRegistrationRequest(Builder builder) {
        this(builder.zzr, builder.zzq, builder.zzhg, null);
    }

    public DataUpdateListenerRegistrationRequest(DataUpdateListenerRegistrationRequest dataUpdateListenerRegistrationRequest, IBinder iBinder) {
        this(dataUpdateListenerRegistrationRequest.zzr, dataUpdateListenerRegistrationRequest.zzq, dataUpdateListenerRegistrationRequest.zzhg, iBinder);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DataUpdateListenerRegistrationRequest) {
                DataUpdateListenerRegistrationRequest dataUpdateListenerRegistrationRequest = (DataUpdateListenerRegistrationRequest) obj;
                if (Objects.equal(this.zzr, dataUpdateListenerRegistrationRequest.zzr) && Objects.equal(this.zzq, dataUpdateListenerRegistrationRequest.zzq) && Objects.equal(this.zzhg, dataUpdateListenerRegistrationRequest.zzhg)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public DataSource getDataSource() {
        return this.zzr;
    }

    public DataType getDataType() {
        return this.zzq;
    }

    public PendingIntent getIntent() {
        return this.zzhg;
    }

    public int hashCode() {
        return Objects.hashCode(this.zzr, this.zzq, this.zzhg);
    }

    public String toString() {
        return Objects.toStringHelper(this).add("dataSource", this.zzr).add("dataType", this.zzq).add(BaseGmsClient.KEY_PENDING_INTENT, this.zzhg).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, getDataSource(), i, false);
        SafeParcelWriter.writeParcelable(parcel, 2, getDataType(), i, false);
        SafeParcelWriter.writeParcelable(parcel, 3, getIntent(), i, false);
        zzcq zzcq = this.zzgj;
        SafeParcelWriter.writeIBinder(parcel, 4, zzcq == null ? null : zzcq.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
