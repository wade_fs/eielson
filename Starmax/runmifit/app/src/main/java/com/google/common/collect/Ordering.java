package com.google.common.collect;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nullable;

public abstract class Ordering<T> implements Comparator<T> {
    static final int LEFT_IS_GREATER = 1;
    static final int RIGHT_IS_GREATER = -1;

    public abstract int compare(@Nullable T t, @Nullable T t2);

    public static <C extends Comparable> Ordering<C> natural() {
        return NaturalOrdering.INSTANCE;
    }

    public static <T> Ordering<T> from(Comparator comparator) {
        return comparator instanceof Ordering ? (Ordering) comparator : new ComparatorOrdering(comparator);
    }

    @Deprecated
    public static <T> Ordering<T> from(Ordering ordering) {
        return (Ordering) Preconditions.checkNotNull(ordering);
    }

    public static <T> Ordering<T> explicit(List<T> list) {
        return new ExplicitOrdering(list);
    }

    public static <T> Ordering<T> explicit(T t, T... tArr) {
        return explicit(Lists.asList(t, tArr));
    }

    public static Ordering<Object> allEqual() {
        return AllEqualOrdering.INSTANCE;
    }

    public static Ordering<Object> usingToString() {
        return UsingToStringOrdering.INSTANCE;
    }

    public static Ordering<Object> arbitrary() {
        return ArbitraryOrderingHolder.ARBITRARY_ORDERING;
    }

    private static class ArbitraryOrderingHolder {
        static final Ordering<Object> ARBITRARY_ORDERING = new ArbitraryOrdering();

        private ArbitraryOrderingHolder() {
        }
    }

    static class ArbitraryOrdering extends Ordering<Object> {
        private Map<Object, Integer> uids = Platform.tryWeakKeys(new MapMaker()).makeComputingMap(new Function<Object, Integer>() {
            /* class com.google.common.collect.Ordering.ArbitraryOrdering.C18501 */
            final AtomicInteger counter = new AtomicInteger(0);

            public Integer apply(Object obj) {
                return Integer.valueOf(this.counter.getAndIncrement());
            }
        });

        public String toString() {
            return "Ordering.arbitrary()";
        }

        ArbitraryOrdering() {
        }

        public int compare(Object obj, Object obj2) {
            if (obj == obj2) {
                return 0;
            }
            if (obj == null) {
                return -1;
            }
            if (obj2 == null) {
                return 1;
            }
            int identityHashCode = identityHashCode(obj);
            int identityHashCode2 = identityHashCode(obj2);
            if (identityHashCode == identityHashCode2) {
                int compareTo = this.uids.get(obj).compareTo(this.uids.get(obj2));
                if (compareTo != 0) {
                    return compareTo;
                }
                throw new AssertionError();
            } else if (identityHashCode < identityHashCode2) {
                return -1;
            } else {
                return 1;
            }
        }

        /* access modifiers changed from: package-private */
        public int identityHashCode(Object obj) {
            return System.identityHashCode(obj);
        }
    }

    protected Ordering() {
    }

    public <S extends T> Ordering<S> reverse() {
        return new ReverseOrdering(this);
    }

    public <S extends T> Ordering<S> nullsFirst() {
        return new NullsFirstOrdering(this);
    }

    public <S extends T> Ordering<S> nullsLast() {
        return new NullsLastOrdering(this);
    }

    public <F> Ordering<F> onResultOf(Function<F, ? extends T> function) {
        return new ByFunctionOrdering(function, this);
    }

    /* access modifiers changed from: package-private */
    public <T2 extends T> Ordering<Map.Entry<T2, ?>> onKeys() {
        return onResultOf(Maps.keyFunction());
    }

    public <U extends T> Ordering<U> compound(Comparator<? super U> comparator) {
        return new CompoundOrdering(this, (Comparator) Preconditions.checkNotNull(comparator));
    }

    public static <T> Ordering<T> compound(Iterable<? extends Comparator<? super T>> iterable) {
        return new CompoundOrdering(iterable);
    }

    public <S extends T> Ordering<Iterable<S>> lexicographical() {
        return new LexicographicalOrdering(this);
    }

    public <E extends T> E min(Iterator<E> it) {
        E next = it.next();
        while (it.hasNext()) {
            next = min(next, it.next());
        }
        return next;
    }

    public <E extends T> E min(Iterable<E> iterable) {
        return min(iterable.iterator());
    }

    public <E extends T> E min(@Nullable E e, @Nullable E e2) {
        return compare(e, e2) <= 0 ? e : e2;
    }

    public <E extends T> E min(@Nullable E e, @Nullable E e2, @Nullable E e3, E... eArr) {
        E min = min(min(e, e2), e3);
        for (E e4 : eArr) {
            min = min(min, e4);
        }
        return min;
    }

    public <E extends T> E max(Iterator<E> it) {
        E next = it.next();
        while (it.hasNext()) {
            next = max(next, it.next());
        }
        return next;
    }

    public <E extends T> E max(Iterable<E> iterable) {
        return max(iterable.iterator());
    }

    public <E extends T> E max(@Nullable E e, @Nullable E e2) {
        return compare(e, e2) >= 0 ? e : e2;
    }

    public <E extends T> E max(@Nullable E e, @Nullable E e2, @Nullable E e3, E... eArr) {
        E max = max(max(e, e2), e3);
        for (E e4 : eArr) {
            max = max(max, e4);
        }
        return max;
    }

    public <E extends T> List<E> leastOf(Iterable<E> iterable, int i) {
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (((long) collection.size()) <= ((long) i) * 2) {
                Object[] array = collection.toArray();
                Arrays.sort(array, this);
                if (array.length > i) {
                    array = ObjectArrays.arraysCopyOf(array, i);
                }
                return Collections.unmodifiableList(Arrays.asList(array));
            }
        }
        return leastOf(iterable.iterator(), i);
    }

    /* JADX WARN: Type inference failed for: r9v0, types: [java.util.Iterator<E>, java.util.Iterator, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <E extends T> java.util.List<E> leastOf(java.util.Iterator<E> r9, int r10) {
        /*
            r8 = this;
            com.google.common.base.Preconditions.checkNotNull(r9)
            java.lang.String r0 = "k"
            com.google.common.collect.CollectPreconditions.checkNonnegative(r10, r0)
            if (r10 == 0) goto L_0x00b9
            boolean r0 = r9.hasNext()
            if (r0 != 0) goto L_0x0012
            goto L_0x00b9
        L_0x0012:
            r0 = 1073741823(0x3fffffff, float:1.9999999)
            if (r10 < r0) goto L_0x0037
            java.util.ArrayList r9 = com.google.common.collect.Lists.newArrayList(r9)
            java.util.Collections.sort(r9, r8)
            int r0 = r9.size()
            if (r0 <= r10) goto L_0x002f
            int r0 = r9.size()
            java.util.List r10 = r9.subList(r10, r0)
            r10.clear()
        L_0x002f:
            r9.trimToSize()
            java.util.List r9 = java.util.Collections.unmodifiableList(r9)
            return r9
        L_0x0037:
            int r0 = r10 * 2
            java.lang.Object[] r1 = new java.lang.Object[r0]
            java.lang.Object[] r1 = (java.lang.Object[]) r1
            java.lang.Object r2 = r9.next()
            r3 = 0
            r1[r3] = r2
            r4 = 1
            r5 = r2
            r2 = 1
        L_0x0047:
            if (r2 >= r10) goto L_0x005d
            boolean r6 = r9.hasNext()
            if (r6 == 0) goto L_0x005d
            java.lang.Object r6 = r9.next()
            int r7 = r2 + 1
            r1[r2] = r6
            java.lang.Object r5 = r8.max(r5, r6)
            r2 = r7
            goto L_0x0047
        L_0x005d:
            boolean r6 = r9.hasNext()
            if (r6 == 0) goto L_0x00a5
            java.lang.Object r6 = r9.next()
            int r7 = r8.compare(r6, r5)
            if (r7 < 0) goto L_0x006e
            goto L_0x005d
        L_0x006e:
            int r7 = r2 + 1
            r1[r2] = r6
            if (r7 != r0) goto L_0x00a3
            int r2 = r0 + -1
            r5 = r2
            r2 = 0
            r6 = 0
        L_0x0079:
            if (r2 >= r5) goto L_0x0093
            int r7 = r2 + r5
            int r7 = r7 + r4
            int r7 = r7 >>> r4
            int r7 = r8.partition(r1, r2, r5, r7)
            if (r7 <= r10) goto L_0x0089
            int r7 = r7 + -1
            r5 = r7
            goto L_0x0079
        L_0x0089:
            if (r7 >= r10) goto L_0x0093
            int r2 = r2 + 1
            int r2 = java.lang.Math.max(r7, r2)
            r6 = r7
            goto L_0x0079
        L_0x0093:
            r2 = r1[r6]
        L_0x0095:
            int r6 = r6 + 1
            if (r6 >= r10) goto L_0x00a0
            r5 = r1[r6]
            java.lang.Object r2 = r8.max(r2, r5)
            goto L_0x0095
        L_0x00a0:
            r5 = r2
            r2 = r10
            goto L_0x005d
        L_0x00a3:
            r2 = r7
            goto L_0x005d
        L_0x00a5:
            java.util.Arrays.sort(r1, r3, r2, r8)
            int r9 = java.lang.Math.min(r2, r10)
            java.lang.Object[] r9 = com.google.common.collect.ObjectArrays.arraysCopyOf(r1, r9)
            java.util.List r9 = java.util.Arrays.asList(r9)
            java.util.List r9 = java.util.Collections.unmodifiableList(r9)
            return r9
        L_0x00b9:
            com.google.common.collect.ImmutableList r9 = com.google.common.collect.ImmutableList.m5316of()
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.Ordering.leastOf(java.util.Iterator, int):java.util.List");
    }

    private <E extends T> int partition(E[] eArr, int i, int i2, int i3) {
        E e = eArr[i3];
        eArr[i3] = eArr[i2];
        eArr[i2] = e;
        int i4 = i;
        while (i < i2) {
            if (compare(eArr[i], e) < 0) {
                ObjectArrays.swap(eArr, i4, i);
                i4++;
            }
            i++;
        }
        ObjectArrays.swap(eArr, i2, i4);
        return i4;
    }

    public <E extends T> List<E> greatestOf(Iterable<E> iterable, int i) {
        return reverse().leastOf(iterable, i);
    }

    public <E extends T> List<E> greatestOf(Iterator<E> it, int i) {
        return reverse().leastOf(it, i);
    }

    public <E extends T> List<E> sortedCopy(Iterable<E> iterable) {
        Object[] array = Iterables.toArray(iterable);
        Arrays.sort(array, this);
        return Lists.newArrayList(Arrays.asList(array));
    }

    public <E extends T> ImmutableList<E> immutableSortedCopy(Iterable<E> iterable) {
        Object[] array = Iterables.toArray(iterable);
        for (Object obj : array) {
            Preconditions.checkNotNull(obj);
        }
        Arrays.sort(array, this);
        return ImmutableList.asImmutableList(array);
    }

    public boolean isOrdered(Iterable<? extends T> iterable) {
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return true;
        }
        Object next = it.next();
        while (it.hasNext()) {
            Object next2 = it.next();
            if (compare(next, next2) > 0) {
                return false;
            }
            next = next2;
        }
        return true;
    }

    public boolean isStrictlyOrdered(Iterable<? extends T> iterable) {
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return true;
        }
        Object next = it.next();
        while (it.hasNext()) {
            Object next2 = it.next();
            if (compare(next, next2) >= 0) {
                return false;
            }
            next = next2;
        }
        return true;
    }

    public int binarySearch(List<? extends T> list, @Nullable T t) {
        return Collections.binarySearch(list, t, this);
    }

    static class IncomparableValueException extends ClassCastException {
        private static final long serialVersionUID = 0;
        final Object value;

        IncomparableValueException(Object obj) {
            super("Cannot compare value: " + obj);
            this.value = obj;
        }
    }
}
