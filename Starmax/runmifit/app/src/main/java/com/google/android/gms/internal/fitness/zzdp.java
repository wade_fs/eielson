package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

final class zzdp extends zzaj<DataReadResult> {
    private final /* synthetic */ DataReadRequest zzfl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdp(zzdj zzdj, GoogleApiClient googleApiClient, DataReadRequest dataReadRequest) {
        super(googleApiClient);
        this.zzfl = dataReadRequest;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return DataReadResult.zza(status, this.zzfl.getDataTypes(), this.zzfl.getDataSources());
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbz) ((zzag) anyClient).getService()).zza(new DataReadRequest(this.zzfl, new zzds(this, null)));
    }
}
