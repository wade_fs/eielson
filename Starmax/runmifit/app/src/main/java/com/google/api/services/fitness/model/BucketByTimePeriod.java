package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class BucketByTimePeriod extends GenericJson {
    @Key
    private String timeZoneId;
    @Key
    private String type;
    @Key
    private Integer value;

    public String getTimeZoneId() {
        return this.timeZoneId;
    }

    public BucketByTimePeriod setTimeZoneId(String str) {
        this.timeZoneId = str;
        return this;
    }

    public String getType() {
        return this.type;
    }

    public BucketByTimePeriod setType(String str) {
        this.type = str;
        return this;
    }

    public Integer getValue() {
        return this.value;
    }

    public BucketByTimePeriod setValue(Integer num) {
        this.value = num;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.BucketByTimePeriod.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.BucketByTimePeriod.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.BucketByTimePeriod
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public BucketByTimePeriod set(String str, Object obj) {
        return (BucketByTimePeriod) super.set(str, obj);
    }

    public BucketByTimePeriod clone() {
        return (BucketByTimePeriod) super.clone();
    }
}
