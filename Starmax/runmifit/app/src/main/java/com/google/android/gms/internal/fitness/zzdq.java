package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.zzg;
import com.google.android.gms.fitness.result.DailyTotalResult;

final class zzdq extends zzaj<DailyTotalResult> {
    private final /* synthetic */ DataType zzfm;
    private final /* synthetic */ boolean zzfn;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdq(zzdj zzdj, GoogleApiClient googleApiClient, DataType dataType, boolean z) {
        super(googleApiClient);
        this.zzfm = dataType;
        this.zzfn = z;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return DailyTotalResult.zza(status, this.zzfm);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzg.<init>(com.google.android.gms.internal.fitness.zzbe, com.google.android.gms.fitness.data.DataType, boolean):void
     arg types: [com.google.android.gms.internal.fitness.zzdr, com.google.android.gms.fitness.data.DataType, boolean]
     candidates:
      com.google.android.gms.fitness.request.zzg.<init>(android.os.IBinder, com.google.android.gms.fitness.data.DataType, boolean):void
      com.google.android.gms.fitness.request.zzg.<init>(com.google.android.gms.internal.fitness.zzbe, com.google.android.gms.fitness.data.DataType, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbz) ((zzag) anyClient).getService()).zza(new zzg((zzbe) new zzdr(this), this.zzfm, this.zzfn));
    }
}
