package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonString;
import com.google.api.client.util.Key;
import java.util.List;

public final class AggregateBucket extends GenericJson {
    @Key
    private Integer activity;
    @Key
    private List<Dataset> dataset;
    @JsonString
    @Key
    private Long endTimeMillis;
    @Key
    private Session session;
    @JsonString
    @Key
    private Long startTimeMillis;
    @Key
    private String type;

    public Integer getActivity() {
        return this.activity;
    }

    public AggregateBucket setActivity(Integer num) {
        this.activity = num;
        return this;
    }

    public List<Dataset> getDataset() {
        return this.dataset;
    }

    public AggregateBucket setDataset(List<Dataset> list) {
        this.dataset = list;
        return this;
    }

    public Long getEndTimeMillis() {
        return this.endTimeMillis;
    }

    public AggregateBucket setEndTimeMillis(Long l) {
        this.endTimeMillis = l;
        return this;
    }

    public Session getSession() {
        return this.session;
    }

    public AggregateBucket setSession(Session session2) {
        this.session = session2;
        return this;
    }

    public Long getStartTimeMillis() {
        return this.startTimeMillis;
    }

    public AggregateBucket setStartTimeMillis(Long l) {
        this.startTimeMillis = l;
        return this;
    }

    public String getType() {
        return this.type;
    }

    public AggregateBucket setType(String str) {
        this.type = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.AggregateBucket.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.AggregateBucket.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.AggregateBucket
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public AggregateBucket set(String str, Object obj) {
        return (AggregateBucket) super.set(str, obj);
    }

    public AggregateBucket clone() {
        return (AggregateBucket) super.clone();
    }
}
