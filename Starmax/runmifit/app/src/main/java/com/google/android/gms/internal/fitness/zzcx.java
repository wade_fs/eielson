package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.BleDevice;
import com.google.android.gms.fitness.request.zze;

final class zzcx extends zzu {
    private final /* synthetic */ BleDevice zzfa;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcx(zzct zzct, GoogleApiClient googleApiClient, BleDevice bleDevice) {
        super(googleApiClient);
        this.zzfa = bleDevice;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zze.<init>(java.lang.String, com.google.android.gms.fitness.data.BleDevice, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [java.lang.String, com.google.android.gms.fitness.data.BleDevice, com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zze.<init>(java.lang.String, com.google.android.gms.fitness.data.BleDevice, android.os.IBinder):void
      com.google.android.gms.fitness.request.zze.<init>(java.lang.String, com.google.android.gms.fitness.data.BleDevice, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbt) ((zzp) anyClient).getService()).zza(new zze(this.zzfa.getAddress(), this.zzfa, (zzcq) new zzen(this)));
    }
}
