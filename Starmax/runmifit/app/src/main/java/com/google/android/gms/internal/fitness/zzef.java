package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.zzaz;

final class zzef extends zzbd {
    private final /* synthetic */ Session zzfx;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzef(zzee zzee, GoogleApiClient googleApiClient, Session session) {
        super(googleApiClient);
        this.zzfx = session;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzaz.<init>(com.google.android.gms.fitness.data.Session, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [com.google.android.gms.fitness.data.Session, com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zzaz.<init>(com.google.android.gms.fitness.data.Session, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzaz.<init>(com.google.android.gms.fitness.data.Session, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcf) ((zzay) anyClient).getService()).zza(new zzaz(this.zzfx, (zzcq) new zzen(this)));
    }
}
