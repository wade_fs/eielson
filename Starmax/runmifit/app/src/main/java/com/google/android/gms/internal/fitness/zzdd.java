package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.request.zzs;
import com.google.android.gms.fitness.result.DataTypeResult;

final class zzdd extends zzy<DataTypeResult> {
    private final /* synthetic */ String zzfc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdd(zzdb zzdb, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient);
        this.zzfc = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return DataTypeResult.zzc(status);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzs.<init>(java.lang.String, com.google.android.gms.internal.fitness.zzbn):void
     arg types: [java.lang.String, com.google.android.gms.internal.fitness.zzdf]
     candidates:
      com.google.android.gms.fitness.request.zzs.<init>(java.lang.String, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzs.<init>(java.lang.String, com.google.android.gms.internal.fitness.zzbn):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbv) ((zzv) anyClient).getService()).zza(new zzs(this.zzfc, (zzbn) new zzdf(this, null)));
    }
}
