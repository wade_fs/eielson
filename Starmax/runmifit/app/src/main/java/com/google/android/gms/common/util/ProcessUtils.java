package com.google.android.gms.common.util;

import android.os.Binder;
import android.os.Process;
import android.os.StrictMode;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileReader;
import java.io.IOException;
import javax.annotation.Nullable;

public class ProcessUtils {
    private static String zzaai;
    private static int zzaaj;

    public static class SystemGroupsNotAvailableException extends Exception {
        SystemGroupsNotAvailableException(String str) {
            super(str);
        }

        SystemGroupsNotAvailableException(String str, Throwable th) {
            super(str, th);
        }
    }

    private ProcessUtils() {
    }

    @Nullable
    public static String getCallingProcessName() {
        int callingPid = Binder.getCallingPid();
        return callingPid == zzde() ? getMyProcessName() : zzl(callingPid);
    }

    @Nullable
    public static String getMyProcessName() {
        if (zzaai == null) {
            zzaai = zzl(zzde());
        }
        return zzaai;
    }

    public static boolean hasSystemGroups() throws SystemGroupsNotAvailableException {
        try {
            int zzde = zzde();
            StringBuilder sb = new StringBuilder(24);
            sb.append("/proc/");
            sb.append(zzde);
            sb.append("/status");
            BufferedReader zzm = zzm(sb.toString());
            boolean zzk = zzk(zzm);
            IOUtils.closeQuietly(zzm);
            return zzk;
        } catch (IOException e) {
            throw new SystemGroupsNotAvailableException("Unable to access /proc/pid/status.", e);
        } catch (Throwable th) {
            IOUtils.closeQuietly((Closeable) null);
            throw th;
        }
    }

    private static int zzde() {
        if (zzaaj == 0) {
            zzaaj = Process.myPid();
        }
        return zzaaj;
    }

    private static boolean zzk(BufferedReader bufferedReader) throws IOException, SystemGroupsNotAvailableException {
        String trim;
        do {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                trim = readLine.trim();
            } else {
                throw new SystemGroupsNotAvailableException("Missing Groups entry from proc/pid/status.");
            }
        } while (!trim.startsWith("Groups:"));
        for (String str : trim.substring(7).trim().split("\\s", -1)) {
            try {
                long parseLong = Long.parseLong(str);
                if (parseLong >= 1000 && parseLong < 2000) {
                    return true;
                }
            } catch (NumberFormatException unused) {
            }
        }
        return false;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.io.Closeable] */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    @javax.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String zzl(int r4) {
        /*
            r0 = 0
            if (r4 > 0) goto L_0x0004
            return r0
        L_0x0004:
            r1 = 25
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0033, all -> 0x002e }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0033, all -> 0x002e }
            java.lang.String r1 = "/proc/"
            r2.append(r1)     // Catch:{ IOException -> 0x0033, all -> 0x002e }
            r2.append(r4)     // Catch:{ IOException -> 0x0033, all -> 0x002e }
            java.lang.String r4 = "/cmdline"
            r2.append(r4)     // Catch:{ IOException -> 0x0033, all -> 0x002e }
            java.lang.String r4 = r2.toString()     // Catch:{ IOException -> 0x0033, all -> 0x002e }
            java.io.BufferedReader r4 = zzm(r4)     // Catch:{ IOException -> 0x0033, all -> 0x002e }
            java.lang.String r1 = r4.readLine()     // Catch:{ IOException -> 0x0034, all -> 0x0029 }
            java.lang.String r0 = r1.trim()     // Catch:{ IOException -> 0x0034, all -> 0x0029 }
            goto L_0x0034
        L_0x0029:
            r0 = move-exception
            r3 = r0
            r0 = r4
            r4 = r3
            goto L_0x002f
        L_0x002e:
            r4 = move-exception
        L_0x002f:
            com.google.android.gms.common.util.IOUtils.closeQuietly(r0)
            throw r4
        L_0x0033:
            r4 = r0
        L_0x0034:
            com.google.android.gms.common.util.IOUtils.closeQuietly(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.util.ProcessUtils.zzl(int):java.lang.String");
    }

    private static BufferedReader zzm(String str) throws IOException {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return new BufferedReader(new FileReader(str));
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }
}
