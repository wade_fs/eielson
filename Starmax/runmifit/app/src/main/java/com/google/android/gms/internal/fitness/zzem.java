package com.google.android.gms.internal.fitness;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.fitness.result.SessionStopResult;

final class zzem extends zzco {
    private final BaseImplementation.ResultHolder<SessionStopResult> zzev;

    private zzem(BaseImplementation.ResultHolder<SessionStopResult> resultHolder) {
        this.zzev = resultHolder;
    }

    /* synthetic */ zzem(BaseImplementation.ResultHolder resultHolder, zzef zzef) {
        this(resultHolder);
    }

    public final void zza(SessionStopResult sessionStopResult) {
        this.zzev.setResult(sessionStopResult);
    }
}
