package com.google.android.gms.fitness;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataTypeCreateRequest;
import com.google.android.gms.internal.fitness.zzdb;
import com.google.android.gms.internal.fitness.zzv;
import com.google.android.gms.tasks.Task;

public class ConfigClient extends GoogleApi<FitnessOptions> {
    private static final ConfigApi zzj = new zzdb();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$Settings):void
     arg types: [android.app.Activity, com.google.android.gms.common.api.Api<com.google.android.gms.fitness.FitnessOptions>, com.google.android.gms.fitness.FitnessOptions, com.google.android.gms.common.api.GoogleApi$Settings]
     candidates:
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.StatusExceptionMapper):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$Settings):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.content.Context, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.internal.StatusExceptionMapper):void
      com.google.android.gms.common.api.GoogleApi.<init>(android.app.Activity, com.google.android.gms.common.api.Api, com.google.android.gms.common.api.Api$ApiOptions, com.google.android.gms.common.api.GoogleApi$Settings):void */
    ConfigClient(Activity activity, FitnessOptions fitnessOptions) {
        super(activity, (Api) zzv.zzew, (Api.ApiOptions) fitnessOptions, GoogleApi.Settings.DEFAULT_SETTINGS);
    }

    ConfigClient(Context context, FitnessOptions fitnessOptions) {
        super(context, zzv.zzew, fitnessOptions, GoogleApi.Settings.DEFAULT_SETTINGS);
    }

    public Task<DataType> createCustomDataType(DataTypeCreateRequest dataTypeCreateRequest) {
        return PendingResultUtil.toTask(zzj.createCustomDataType(asGoogleApiClient(), dataTypeCreateRequest), zzd.zzf);
    }

    public Task<Void> disableFit() {
        return PendingResultUtil.toVoidTask(zzj.disableFit(asGoogleApiClient()));
    }

    public Task<DataType> readDataType(String str) {
        return PendingResultUtil.toTask(zzj.readDataType(asGoogleApiClient(), str), zze.zzf);
    }
}
