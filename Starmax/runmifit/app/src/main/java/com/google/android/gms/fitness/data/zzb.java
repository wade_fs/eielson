package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;

public final class zzb extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzb> CREATOR = new zzc();
    public static final zzb zzad = new zzb("com.google.android.gms", null);
    private final String packageName;
    private final String zzae;

    public zzb(String str, String str2) {
        this.packageName = (String) Preconditions.checkNotNull(str);
        this.zzae = str2;
    }

    public static zzb zza(String str) {
        return "com.google.android.gms".equals(str) ? zzad : new zzb(str, null);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzb)) {
            return false;
        }
        zzb zzb = (zzb) obj;
        return this.packageName.equals(zzb.packageName) && Objects.equal(this.zzae, zzb.zzae);
    }

    public final String getPackageName() {
        return this.packageName;
    }

    public final int hashCode() {
        return Objects.hashCode(this.packageName, this.zzae);
    }

    public final String toString() {
        return String.format("Application{%s:%s}", this.packageName, this.zzae);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.packageName, false);
        SafeParcelWriter.writeString(parcel, 3, this.zzae, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
