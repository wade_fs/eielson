package com.google.android.gms.internal.fitness;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.request.zzw;

final class zzdo extends zzal {
    private final /* synthetic */ PendingIntent zzfk;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdo(zzdj zzdj, GoogleApiClient googleApiClient, PendingIntent pendingIntent) {
        super(googleApiClient);
        this.zzfk = pendingIntent;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbz) ((zzag) anyClient).getService()).zza(new zzw(this.zzfk, new zzen(this)));
    }
}
