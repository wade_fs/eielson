package com.google.android.gms.fitness.request;

import com.google.android.gms.common.api.internal.ListenerHolder;

final class zzc implements ListenerHolder.Notifier<BleScanCallback> {
    zzc(zza zza) {
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((BleScanCallback) obj).onScanStopped();
    }

    public final void onNotifyListenerFailed() {
    }
}
