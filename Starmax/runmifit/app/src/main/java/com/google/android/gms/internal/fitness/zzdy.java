package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.zzbn;

final class zzdy extends zzar {
    private final /* synthetic */ DataSource zzfs;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdy(zzdt zzdt, GoogleApiClient googleApiClient, DataSource dataSource) {
        super(googleApiClient);
        this.zzfs = dataSource;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzbn.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.fitness.data.DataSource, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [?[OBJECT, ARRAY], com.google.android.gms.fitness.data.DataSource, com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zzbn.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.fitness.data.DataSource, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzbn.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.fitness.data.DataSource, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcb) ((zzam) anyClient).getService()).zza(new zzbn((DataType) null, this.zzfs, (zzcq) new zzen(this)));
    }
}
