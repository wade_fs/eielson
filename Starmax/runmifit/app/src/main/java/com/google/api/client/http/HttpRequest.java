package com.google.api.client.http;

import com.google.api.client.util.ObjectParser;
import com.google.api.client.util.Preconditions;
import com.google.api.client.util.Sleeper;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public final class HttpRequest {
    public static final int DEFAULT_NUMBER_OF_RETRIES = 10;
    public static final String USER_AGENT_SUFFIX = "Google-HTTP-Java-Client/1.22.0 (gzip)";
    public static final String VERSION = "1.22.0";
    @Deprecated
    private BackOffPolicy backOffPolicy;
    private int connectTimeout = 20000;
    private HttpContent content;
    private int contentLoggingLimit = 16384;
    private boolean curlLoggingEnabled = true;
    private HttpEncoding encoding;
    private HttpExecuteInterceptor executeInterceptor;
    private boolean followRedirects = true;
    private HttpHeaders headers = new HttpHeaders();
    private HttpIOExceptionHandler ioExceptionHandler;
    private boolean loggingEnabled = true;
    private int numRetries = 10;
    private ObjectParser objectParser;
    private int readTimeout = 20000;
    private String requestMethod;
    private HttpHeaders responseHeaders = new HttpHeaders();
    private HttpResponseInterceptor responseInterceptor;
    @Deprecated
    private boolean retryOnExecuteIOException = false;
    private Sleeper sleeper = Sleeper.DEFAULT;
    private boolean suppressUserAgentSuffix;
    private boolean throwExceptionOnExecuteError = true;
    private final HttpTransport transport;
    private HttpUnsuccessfulResponseHandler unsuccessfulResponseHandler;
    private GenericUrl url;

    HttpRequest(HttpTransport httpTransport, String str) {
        this.transport = httpTransport;
        setRequestMethod(str);
    }

    public HttpTransport getTransport() {
        return this.transport;
    }

    public String getRequestMethod() {
        return this.requestMethod;
    }

    public HttpRequest setRequestMethod(String str) {
        Preconditions.checkArgument(str == null || HttpMediaType.matchesToken(str));
        this.requestMethod = str;
        return this;
    }

    public GenericUrl getUrl() {
        return this.url;
    }

    public HttpRequest setUrl(GenericUrl genericUrl) {
        this.url = (GenericUrl) Preconditions.checkNotNull(genericUrl);
        return this;
    }

    public HttpContent getContent() {
        return this.content;
    }

    public HttpRequest setContent(HttpContent httpContent) {
        this.content = httpContent;
        return this;
    }

    public HttpEncoding getEncoding() {
        return this.encoding;
    }

    public HttpRequest setEncoding(HttpEncoding httpEncoding) {
        this.encoding = httpEncoding;
        return this;
    }

    @Deprecated
    public BackOffPolicy getBackOffPolicy() {
        return this.backOffPolicy;
    }

    @Deprecated
    public HttpRequest setBackOffPolicy(BackOffPolicy backOffPolicy2) {
        this.backOffPolicy = backOffPolicy2;
        return this;
    }

    public int getContentLoggingLimit() {
        return this.contentLoggingLimit;
    }

    public HttpRequest setContentLoggingLimit(int i) {
        Preconditions.checkArgument(i >= 0, "The content logging limit must be non-negative.");
        this.contentLoggingLimit = i;
        return this;
    }

    public boolean isLoggingEnabled() {
        return this.loggingEnabled;
    }

    public HttpRequest setLoggingEnabled(boolean z) {
        this.loggingEnabled = z;
        return this;
    }

    public boolean isCurlLoggingEnabled() {
        return this.curlLoggingEnabled;
    }

    public HttpRequest setCurlLoggingEnabled(boolean z) {
        this.curlLoggingEnabled = z;
        return this;
    }

    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    public HttpRequest setConnectTimeout(int i) {
        Preconditions.checkArgument(i >= 0);
        this.connectTimeout = i;
        return this;
    }

    public int getReadTimeout() {
        return this.readTimeout;
    }

    public HttpRequest setReadTimeout(int i) {
        Preconditions.checkArgument(i >= 0);
        this.readTimeout = i;
        return this;
    }

    public HttpHeaders getHeaders() {
        return this.headers;
    }

    public HttpRequest setHeaders(HttpHeaders httpHeaders) {
        this.headers = (HttpHeaders) Preconditions.checkNotNull(httpHeaders);
        return this;
    }

    public HttpHeaders getResponseHeaders() {
        return this.responseHeaders;
    }

    public HttpRequest setResponseHeaders(HttpHeaders httpHeaders) {
        this.responseHeaders = (HttpHeaders) Preconditions.checkNotNull(httpHeaders);
        return this;
    }

    public HttpExecuteInterceptor getInterceptor() {
        return this.executeInterceptor;
    }

    public HttpRequest setInterceptor(HttpExecuteInterceptor httpExecuteInterceptor) {
        this.executeInterceptor = httpExecuteInterceptor;
        return this;
    }

    public HttpUnsuccessfulResponseHandler getUnsuccessfulResponseHandler() {
        return this.unsuccessfulResponseHandler;
    }

    public HttpRequest setUnsuccessfulResponseHandler(HttpUnsuccessfulResponseHandler httpUnsuccessfulResponseHandler) {
        this.unsuccessfulResponseHandler = httpUnsuccessfulResponseHandler;
        return this;
    }

    public HttpIOExceptionHandler getIOExceptionHandler() {
        return this.ioExceptionHandler;
    }

    public HttpRequest setIOExceptionHandler(HttpIOExceptionHandler httpIOExceptionHandler) {
        this.ioExceptionHandler = httpIOExceptionHandler;
        return this;
    }

    public HttpResponseInterceptor getResponseInterceptor() {
        return this.responseInterceptor;
    }

    public HttpRequest setResponseInterceptor(HttpResponseInterceptor httpResponseInterceptor) {
        this.responseInterceptor = httpResponseInterceptor;
        return this;
    }

    public int getNumberOfRetries() {
        return this.numRetries;
    }

    public HttpRequest setNumberOfRetries(int i) {
        Preconditions.checkArgument(i >= 0);
        this.numRetries = i;
        return this;
    }

    public HttpRequest setParser(ObjectParser objectParser2) {
        this.objectParser = objectParser2;
        return this;
    }

    public final ObjectParser getParser() {
        return this.objectParser;
    }

    public boolean getFollowRedirects() {
        return this.followRedirects;
    }

    public HttpRequest setFollowRedirects(boolean z) {
        this.followRedirects = z;
        return this;
    }

    public boolean getThrowExceptionOnExecuteError() {
        return this.throwExceptionOnExecuteError;
    }

    public HttpRequest setThrowExceptionOnExecuteError(boolean z) {
        this.throwExceptionOnExecuteError = z;
        return this;
    }

    @Deprecated
    public boolean getRetryOnExecuteIOException() {
        return this.retryOnExecuteIOException;
    }

    @Deprecated
    public HttpRequest setRetryOnExecuteIOException(boolean z) {
        this.retryOnExecuteIOException = z;
        return this;
    }

    public boolean getSuppressUserAgentSuffix() {
        return this.suppressUserAgentSuffix;
    }

    public HttpRequest setSuppressUserAgentSuffix(boolean z) {
        this.suppressUserAgentSuffix = z;
        return this;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0226 A[SYNTHETIC, Splitter:B:107:0x0226] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0280  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02a7 A[LOOP:0: B:8:0x0021->B:159:0x02a7, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0286 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.api.client.http.HttpResponse execute() throws java.io.IOException {
        /*
            r19 = this;
            r1 = r19
            int r0 = r1.numRetries
            if (r0 < 0) goto L_0x0008
            r0 = 1
            goto L_0x0009
        L_0x0008:
            r0 = 0
        L_0x0009:
            com.google.api.client.util.Preconditions.checkArgument(r0)
            int r0 = r1.numRetries
            com.google.api.client.http.BackOffPolicy r4 = r1.backOffPolicy
            if (r4 == 0) goto L_0x0015
            r4.reset()
        L_0x0015:
            java.lang.String r4 = r1.requestMethod
            com.google.api.client.util.Preconditions.checkNotNull(r4)
            com.google.api.client.http.GenericUrl r4 = r1.url
            com.google.api.client.util.Preconditions.checkNotNull(r4)
            r5 = r0
            r0 = 0
        L_0x0021:
            if (r0 == 0) goto L_0x0026
            r0.ignore()
        L_0x0026:
            com.google.api.client.http.HttpExecuteInterceptor r0 = r1.executeInterceptor
            if (r0 == 0) goto L_0x002d
            r0.intercept(r1)
        L_0x002d:
            com.google.api.client.http.GenericUrl r0 = r1.url
            java.lang.String r0 = r0.build()
            com.google.api.client.http.HttpTransport r6 = r1.transport
            java.lang.String r7 = r1.requestMethod
            com.google.api.client.http.LowLevelHttpRequest r6 = r6.buildRequest(r7, r0)
            java.util.logging.Logger r7 = com.google.api.client.http.HttpTransport.LOGGER
            boolean r8 = r1.loggingEnabled
            if (r8 == 0) goto L_0x004b
            java.util.logging.Level r8 = java.util.logging.Level.CONFIG
            boolean r8 = r7.isLoggable(r8)
            if (r8 == 0) goto L_0x004b
            r8 = 1
            goto L_0x004c
        L_0x004b:
            r8 = 0
        L_0x004c:
            if (r8 == 0) goto L_0x008f
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "-------------- REQUEST  --------------"
            r9.append(r10)
            java.lang.String r10 = com.google.api.client.util.StringUtils.LINE_SEPARATOR
            r9.append(r10)
            java.lang.String r10 = r1.requestMethod
            r9.append(r10)
            r10 = 32
            r9.append(r10)
            r9.append(r0)
            java.lang.String r10 = com.google.api.client.util.StringUtils.LINE_SEPARATOR
            r9.append(r10)
            boolean r10 = r1.curlLoggingEnabled
            if (r10 == 0) goto L_0x0090
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "curl -v --compressed"
            r10.<init>(r11)
            java.lang.String r11 = r1.requestMethod
            java.lang.String r12 = "GET"
            boolean r11 = r11.equals(r12)
            if (r11 != 0) goto L_0x0091
            java.lang.String r11 = " -X "
            r10.append(r11)
            java.lang.String r11 = r1.requestMethod
            r10.append(r11)
            goto L_0x0091
        L_0x008f:
            r9 = 0
        L_0x0090:
            r10 = 0
        L_0x0091:
            com.google.api.client.http.HttpHeaders r11 = r1.headers
            java.lang.String r11 = r11.getUserAgent()
            boolean r12 = r1.suppressUserAgentSuffix
            if (r12 != 0) goto L_0x00be
            java.lang.String r12 = "Google-HTTP-Java-Client/1.22.0 (gzip)"
            if (r11 != 0) goto L_0x00a5
            com.google.api.client.http.HttpHeaders r13 = r1.headers
            r13.setUserAgent(r12)
            goto L_0x00be
        L_0x00a5:
            com.google.api.client.http.HttpHeaders r13 = r1.headers
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r11)
            java.lang.String r15 = " "
            r14.append(r15)
            r14.append(r12)
            java.lang.String r12 = r14.toString()
            r13.setUserAgent(r12)
        L_0x00be:
            com.google.api.client.http.HttpHeaders r12 = r1.headers
            com.google.api.client.http.HttpHeaders.serializeHeaders(r12, r9, r10, r7, r6)
            boolean r12 = r1.suppressUserAgentSuffix
            if (r12 != 0) goto L_0x00cc
            com.google.api.client.http.HttpHeaders r12 = r1.headers
            r12.setUserAgent(r11)
        L_0x00cc:
            com.google.api.client.http.HttpContent r11 = r1.content
            if (r11 == 0) goto L_0x00d9
            boolean r12 = r11.retrySupported()
            if (r12 == 0) goto L_0x00d7
            goto L_0x00d9
        L_0x00d7:
            r12 = 0
            goto L_0x00da
        L_0x00d9:
            r12 = 1
        L_0x00da:
            java.lang.String r15 = "'"
            if (r11 == 0) goto L_0x01b7
            com.google.api.client.http.HttpContent r2 = r1.content
            java.lang.String r2 = r2.getType()
            if (r8 == 0) goto L_0x00f2
            com.google.api.client.util.LoggingStreamingContent r3 = new com.google.api.client.util.LoggingStreamingContent
            java.util.logging.Logger r4 = com.google.api.client.http.HttpTransport.LOGGER
            java.util.logging.Level r13 = java.util.logging.Level.CONFIG
            int r14 = r1.contentLoggingLimit
            r3.<init>(r11, r4, r13, r14)
            goto L_0x00f3
        L_0x00f2:
            r3 = r11
        L_0x00f3:
            com.google.api.client.http.HttpEncoding r4 = r1.encoding
            if (r4 != 0) goto L_0x0100
            com.google.api.client.http.HttpContent r4 = r1.content
            long r13 = r4.getLength()
            r11 = r3
            r4 = 0
            goto L_0x0114
        L_0x0100:
            java.lang.String r4 = r4.getName()
            com.google.api.client.http.HttpEncodingStreamingContent r11 = new com.google.api.client.http.HttpEncodingStreamingContent
            com.google.api.client.http.HttpEncoding r13 = r1.encoding
            r11.<init>(r3, r13)
            if (r12 == 0) goto L_0x0112
            long r13 = com.google.api.client.util.IOUtils.computeLength(r11)
            goto L_0x0114
        L_0x0112:
            r13 = -1
        L_0x0114:
            if (r8 == 0) goto L_0x01a1
            java.lang.String r3 = " -H '"
            if (r2 == 0) goto L_0x014d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r16 = r5
            java.lang.String r5 = "Content-Type: "
            r1.append(r5)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.append(r1)
            java.lang.String r5 = com.google.api.client.util.StringUtils.LINE_SEPARATOR
            r9.append(r5)
            if (r10 == 0) goto L_0x014f
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r3)
            r5.append(r1)
            r5.append(r15)
            java.lang.String r1 = r5.toString()
            r10.append(r1)
            goto L_0x014f
        L_0x014d:
            r16 = r5
        L_0x014f:
            if (r4 == 0) goto L_0x0181
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r5 = "Content-Encoding: "
            r1.append(r5)
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            r9.append(r1)
            java.lang.String r5 = com.google.api.client.util.StringUtils.LINE_SEPARATOR
            r9.append(r5)
            if (r10 == 0) goto L_0x0181
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r3)
            r5.append(r1)
            r5.append(r15)
            java.lang.String r1 = r5.toString()
            r10.append(r1)
        L_0x0181:
            r17 = 0
            int r1 = (r13 > r17 ? 1 : (r13 == r17 ? 0 : -1))
            if (r1 < 0) goto L_0x01a3
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Content-Length: "
            r1.append(r3)
            r1.append(r13)
            java.lang.String r1 = r1.toString()
            r9.append(r1)
            java.lang.String r1 = com.google.api.client.util.StringUtils.LINE_SEPARATOR
            r9.append(r1)
            goto L_0x01a3
        L_0x01a1:
            r16 = r5
        L_0x01a3:
            if (r10 == 0) goto L_0x01aa
            java.lang.String r1 = " -d '@-'"
            r10.append(r1)
        L_0x01aa:
            r6.setContentType(r2)
            r6.setContentEncoding(r4)
            r6.setContentLength(r13)
            r6.setStreamingContent(r11)
            goto L_0x01b9
        L_0x01b7:
            r16 = r5
        L_0x01b9:
            if (r8 == 0) goto L_0x01e3
            java.lang.String r1 = r9.toString()
            r7.config(r1)
            if (r10 == 0) goto L_0x01e3
            java.lang.String r1 = " -- '"
            r10.append(r1)
            java.lang.String r1 = "'\"'\"'"
            java.lang.String r0 = r0.replaceAll(r15, r1)
            r10.append(r0)
            r10.append(r15)
            if (r11 == 0) goto L_0x01dc
            java.lang.String r0 = " << $$$"
            r10.append(r0)
        L_0x01dc:
            java.lang.String r0 = r10.toString()
            r7.config(r0)
        L_0x01e3:
            if (r12 == 0) goto L_0x01e9
            if (r16 <= 0) goto L_0x01e9
            r2 = 1
            goto L_0x01ea
        L_0x01e9:
            r2 = 0
        L_0x01ea:
            r1 = r19
            int r0 = r1.connectTimeout
            int r3 = r1.readTimeout
            r6.setTimeout(r0, r3)
            com.google.api.client.http.LowLevelHttpResponse r3 = r6.execute()     // Catch:{ IOException -> 0x020a }
            com.google.api.client.http.HttpResponse r0 = new com.google.api.client.http.HttpResponse     // Catch:{ all -> 0x01ff }
            r0.<init>(r1, r3)     // Catch:{ all -> 0x01ff }
            r3 = r0
            r4 = 0
            goto L_0x0224
        L_0x01ff:
            r0 = move-exception
            java.io.InputStream r3 = r3.getContent()     // Catch:{ IOException -> 0x020a }
            if (r3 == 0) goto L_0x0209
            r3.close()     // Catch:{ IOException -> 0x020a }
        L_0x0209:
            throw r0     // Catch:{ IOException -> 0x020a }
        L_0x020a:
            r0 = move-exception
            r4 = r0
            boolean r0 = r1.retryOnExecuteIOException
            if (r0 != 0) goto L_0x021c
            com.google.api.client.http.HttpIOExceptionHandler r0 = r1.ioExceptionHandler
            if (r0 == 0) goto L_0x021b
            boolean r0 = r0.handleIOException(r1, r2)
            if (r0 == 0) goto L_0x021b
            goto L_0x021c
        L_0x021b:
            throw r4
        L_0x021c:
            java.util.logging.Level r0 = java.util.logging.Level.WARNING
            java.lang.String r3 = "exception thrown while executing request"
            r7.log(r0, r3, r4)
            r3 = 0
        L_0x0224:
            if (r3 == 0) goto L_0x027c
            boolean r0 = r3.isSuccessStatusCode()     // Catch:{ all -> 0x0275 }
            if (r0 != 0) goto L_0x027c
            com.google.api.client.http.HttpUnsuccessfulResponseHandler r0 = r1.unsuccessfulResponseHandler     // Catch:{ all -> 0x0275 }
            if (r0 == 0) goto L_0x0237
            com.google.api.client.http.HttpUnsuccessfulResponseHandler r0 = r1.unsuccessfulResponseHandler     // Catch:{ all -> 0x0275 }
            boolean r0 = r0.handleResponse(r1, r3, r2)     // Catch:{ all -> 0x0275 }
            goto L_0x0238
        L_0x0237:
            r0 = 0
        L_0x0238:
            if (r0 != 0) goto L_0x026e
            int r5 = r3.getStatusCode()     // Catch:{ all -> 0x0275 }
            com.google.api.client.http.HttpHeaders r6 = r3.getHeaders()     // Catch:{ all -> 0x0275 }
            boolean r5 = r1.handleRedirect(r5, r6)     // Catch:{ all -> 0x0275 }
            if (r5 == 0) goto L_0x024a
        L_0x0248:
            r0 = 1
            goto L_0x026e
        L_0x024a:
            if (r2 == 0) goto L_0x026e
            com.google.api.client.http.BackOffPolicy r5 = r1.backOffPolicy     // Catch:{ all -> 0x0275 }
            if (r5 == 0) goto L_0x026e
            com.google.api.client.http.BackOffPolicy r5 = r1.backOffPolicy     // Catch:{ all -> 0x0275 }
            int r6 = r3.getStatusCode()     // Catch:{ all -> 0x0275 }
            boolean r5 = r5.isBackOffRequired(r6)     // Catch:{ all -> 0x0275 }
            if (r5 == 0) goto L_0x026e
            com.google.api.client.http.BackOffPolicy r5 = r1.backOffPolicy     // Catch:{ all -> 0x0275 }
            long r5 = r5.getNextBackOffMillis()     // Catch:{ all -> 0x0275 }
            r7 = -1
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x026e
            com.google.api.client.util.Sleeper r0 = r1.sleeper     // Catch:{ InterruptedException -> 0x0248 }
            r0.sleep(r5)     // Catch:{ InterruptedException -> 0x0248 }
            goto L_0x0248
        L_0x026e:
            r0 = r0 & r2
            if (r0 == 0) goto L_0x0282
            r3.ignore()     // Catch:{ all -> 0x0275 }
            goto L_0x0282
        L_0x0275:
            r0 = move-exception
            if (r3 == 0) goto L_0x027b
            r3.disconnect()
        L_0x027b:
            throw r0
        L_0x027c:
            if (r3 != 0) goto L_0x0280
            r0 = 1
            goto L_0x0281
        L_0x0280:
            r0 = 0
        L_0x0281:
            r0 = r0 & r2
        L_0x0282:
            int r5 = r16 + -1
            if (r0 != 0) goto L_0x02a7
            if (r3 == 0) goto L_0x02a6
            com.google.api.client.http.HttpResponseInterceptor r0 = r1.responseInterceptor
            if (r0 == 0) goto L_0x028f
            r0.interceptResponse(r3)
        L_0x028f:
            boolean r0 = r1.throwExceptionOnExecuteError
            if (r0 == 0) goto L_0x02a5
            boolean r0 = r3.isSuccessStatusCode()
            if (r0 == 0) goto L_0x029a
            goto L_0x02a5
        L_0x029a:
            com.google.api.client.http.HttpResponseException r0 = new com.google.api.client.http.HttpResponseException     // Catch:{ all -> 0x02a0 }
            r0.<init>(r3)     // Catch:{ all -> 0x02a0 }
            throw r0     // Catch:{ all -> 0x02a0 }
        L_0x02a0:
            r0 = move-exception
            r3.disconnect()
            throw r0
        L_0x02a5:
            return r3
        L_0x02a6:
            throw r4
        L_0x02a7:
            r0 = r3
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.api.client.http.HttpRequest.execute():com.google.api.client.http.HttpResponse");
    }

    public Future<HttpResponse> executeAsync(Executor executor) {
        FutureTask futureTask = new FutureTask(new Callable<HttpResponse>() {
            /* class com.google.api.client.http.HttpRequest.C15911 */

            public HttpResponse call() throws Exception {
                return HttpRequest.this.execute();
            }
        });
        executor.execute(futureTask);
        return futureTask;
    }

    public Future<HttpResponse> executeAsync() {
        return executeAsync(Executors.newSingleThreadExecutor());
    }

    public boolean handleRedirect(int i, HttpHeaders httpHeaders) {
        String location = httpHeaders.getLocation();
        if (!getFollowRedirects() || !HttpStatusCodes.isRedirect(i) || location == null) {
            return false;
        }
        setUrl(new GenericUrl(this.url.toURL(location)));
        if (i == 303) {
            setRequestMethod("GET");
            setContent(null);
        }
        String str = null;
        this.headers.setAuthorization(str);
        this.headers.setIfMatch(str);
        this.headers.setIfNoneMatch(str);
        this.headers.setIfModifiedSince(str);
        this.headers.setIfUnmodifiedSince(str);
        this.headers.setIfRange(str);
        return true;
    }

    public Sleeper getSleeper() {
        return this.sleeper;
    }

    public HttpRequest setSleeper(Sleeper sleeper2) {
        this.sleeper = (Sleeper) Preconditions.checkNotNull(sleeper2);
        return this;
    }
}
