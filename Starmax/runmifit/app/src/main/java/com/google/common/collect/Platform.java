package com.google.common.collect;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import java.lang.reflect.Array;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

final class Platform {
    static <T> T[] newArray(T[] tArr, int i) {
        return (Object[]) Array.newInstance(tArr.getClass().getComponentType(), i);
    }

    static <E> Set<E> newSetFromMap(Map<E, Boolean> map) {
        return Sets.newSetFromMap(map);
    }

    static MapMaker tryWeakKeys(MapMaker mapMaker) {
        return mapMaker.weakKeys();
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [com.google.common.collect.Maps$EntryTransformer, com.google.common.collect.Maps$EntryTransformer<? super K, ? super V1, V2>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static <K, V1, V2> java.util.SortedMap<K, V2> mapsTransformEntriesSortedMap(java.util.SortedMap<K, V1> r0, com.google.common.collect.Maps.EntryTransformer<? super K, ? super V1, V2> r1) {
        /*
            java.util.SortedMap r0 = com.google.common.collect.Maps.transformEntriesIgnoreNavigable(r0, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.Platform.mapsTransformEntriesSortedMap(java.util.SortedMap, com.google.common.collect.Maps$EntryTransformer):java.util.SortedMap");
    }

    static <K, V> SortedMap<K, V> mapsAsMapSortedSet(SortedSet<K> sortedSet, Function<? super K, V> function) {
        return Maps.asMapSortedIgnoreNavigable(sortedSet, function);
    }

    static <E> SortedSet<E> setsFilterSortedSet(SortedSet<E> sortedSet, Predicate<? super E> predicate) {
        return Sets.filterSortedIgnoreNavigable(sortedSet, predicate);
    }

    static <K, V> SortedMap<K, V> mapsFilterSortedMap(SortedMap<K, V> sortedMap, Predicate<? super Map.Entry<K, V>> predicate) {
        return Maps.filterSortedIgnoreNavigable(sortedMap, predicate);
    }

    private Platform() {
    }
}
