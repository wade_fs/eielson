package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class MapValue extends GenericJson {
    @Key
    private Double fpVal;

    public Double getFpVal() {
        return this.fpVal;
    }

    public MapValue setFpVal(Double d) {
        this.fpVal = d;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.MapValue.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.MapValue.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.MapValue
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public MapValue set(String str, Object obj) {
        return (MapValue) super.set(str, obj);
    }

    public MapValue clone() {
        return (MapValue) super.clone();
    }
}
