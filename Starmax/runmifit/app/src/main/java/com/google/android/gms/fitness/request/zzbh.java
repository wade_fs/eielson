package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.fitness.zzcq;

public final class zzbh extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzbh> CREATOR = new zzbi();
    private final zzcq zzgj;
    private final zzae zzii;

    /* JADX WARN: Type inference failed for: r0v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    zzbh(android.os.IBinder r3, android.os.IBinder r4) {
        /*
            r2 = this;
            r2.<init>()
            if (r3 != 0) goto L_0x0007
            r3 = 0
            goto L_0x001b
        L_0x0007:
            java.lang.String r0 = "com.google.android.gms.fitness.request.IBleScanCallback"
            android.os.IInterface r0 = r3.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.google.android.gms.fitness.request.zzae
            if (r1 == 0) goto L_0x0015
            r3 = r0
            com.google.android.gms.fitness.request.zzae r3 = (com.google.android.gms.fitness.request.zzae) r3
            goto L_0x001b
        L_0x0015:
            com.google.android.gms.fitness.request.zzag r0 = new com.google.android.gms.fitness.request.zzag
            r0.<init>(r3)
            r3 = r0
        L_0x001b:
            r2.zzii = r3
            com.google.android.gms.internal.fitness.zzcq r3 = com.google.android.gms.internal.fitness.zzcr.zzj(r4)
            r2.zzgj = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.fitness.request.zzbh.<init>(android.os.IBinder, android.os.IBinder):void");
    }

    public zzbh(BleScanCallback bleScanCallback, zzcq zzcq) {
        this(zzd.zzt().zzb(bleScanCallback), zzcq);
    }

    public zzbh(zzae zzae, zzcq zzcq) {
        this.zzii = zzae;
        this.zzgj = zzcq;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeIBinder(parcel, 1, this.zzii.asBinder(), false);
        zzcq zzcq = this.zzgj;
        SafeParcelWriter.writeIBinder(parcel, 2, zzcq == null ? null : zzcq.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
