package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.request.DataUpdateRequest;

final class zzdm extends zzal {
    private final /* synthetic */ DataUpdateRequest zzfi;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdm(zzdj zzdj, GoogleApiClient googleApiClient, DataUpdateRequest dataUpdateRequest) {
        super(googleApiClient);
        this.zzfi = dataUpdateRequest;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbz) ((zzag) anyClient).getService()).zza(new DataUpdateRequest(this.zzfi, new zzen(this)));
    }
}
