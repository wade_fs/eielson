package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.request.zzbl;

final class zzcy extends zzu {
    private final /* synthetic */ String zzez;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcy(zzct zzct, GoogleApiClient googleApiClient, String str) {
        super(googleApiClient);
        this.zzez = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzbl.<init>(java.lang.String, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [java.lang.String, com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zzbl.<init>(java.lang.String, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzbl.<init>(java.lang.String, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbt) ((zzp) anyClient).getService()).zza(new zzbl(this.zzez, (zzcq) new zzen(this)));
    }
}
