package com.google.api.client.json;

import com.google.api.client.util.GenericData;
import com.google.api.client.util.Throwables;
import java.io.IOException;

public class GenericJson extends GenericData implements Cloneable {
    private JsonFactory jsonFactory;

    public final JsonFactory getFactory() {
        return this.jsonFactory;
    }

    public final void setFactory(JsonFactory jsonFactory2) {
        this.jsonFactory = jsonFactory2;
    }

    public String toString() {
        JsonFactory jsonFactory2 = this.jsonFactory;
        if (jsonFactory2 == null) {
            return super.toString();
        }
        try {
            return jsonFactory2.toString(this);
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    public String toPrettyString() throws IOException {
        JsonFactory jsonFactory2 = this.jsonFactory;
        if (jsonFactory2 != null) {
            return jsonFactory2.toPrettyString(this);
        }
        return super.toString();
    }

    public GenericJson clone() {
        return (GenericJson) super.clone();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData */
    public GenericJson set(String str, Object obj) {
        return (GenericJson) super.set(str, obj);
    }
}
