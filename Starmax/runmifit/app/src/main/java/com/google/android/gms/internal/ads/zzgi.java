package com.google.android.gms.internal.ads;

final class zzgi implements Runnable {
    private final /* synthetic */ zzgh zzahx;

    zzgi(zzgh zzgh) {
        this.zzahx = zzgh;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzgh.zza(com.google.android.gms.internal.ads.zzgh, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzgh, int]
     candidates:
      com.google.android.gms.internal.ads.zzgh.zza(android.app.Application, android.content.Context):void
      com.google.android.gms.internal.ads.zzgh.zza(com.google.android.gms.internal.ads.zzgh, boolean):boolean */
    public final void run() {
        synchronized (this.zzahx.mLock) {
            if (!this.zzahx.zzahr || !this.zzahx.zzahs) {
                zzakb.zzck("App is still foreground");
            } else {
                boolean unused = this.zzahx.zzahr = false;
                zzakb.zzck("App went background");
                for (zzgj zzgj : this.zzahx.zzaht) {
                    try {
                        zzgj.zzh(false);
                    } catch (Exception e) {
                        zzane.zzb("", e);
                    }
                }
            }
        }
    }
}
