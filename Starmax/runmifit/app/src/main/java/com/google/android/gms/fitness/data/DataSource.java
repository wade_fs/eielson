package com.google.android.gms.fitness.data;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import org.apache.commons.math3.geometry.VectorFormat;

public class DataSource extends AbstractSafeParcelable {
    public static final Parcelable.Creator<DataSource> CREATOR = new zzk();
    public static final int DATA_QUALITY_BLOOD_GLUCOSE_ISO151972003 = 8;
    public static final int DATA_QUALITY_BLOOD_GLUCOSE_ISO151972013 = 9;
    public static final int DATA_QUALITY_BLOOD_PRESSURE_AAMI = 3;
    public static final int DATA_QUALITY_BLOOD_PRESSURE_BHS_A_A = 4;
    public static final int DATA_QUALITY_BLOOD_PRESSURE_BHS_A_B = 5;
    public static final int DATA_QUALITY_BLOOD_PRESSURE_BHS_B_A = 6;
    public static final int DATA_QUALITY_BLOOD_PRESSURE_BHS_B_B = 7;
    public static final int DATA_QUALITY_BLOOD_PRESSURE_ESH2002 = 1;
    public static final int DATA_QUALITY_BLOOD_PRESSURE_ESH2010 = 2;
    public static final String EXTRA_DATA_SOURCE = "vnd.google.fitness.data_source";
    public static final int TYPE_DERIVED = 1;
    public static final int TYPE_RAW = 0;
    private static final int[] zzaw = new int[0];
    private final String name;
    private final int type;
    private final Device zzax;
    private final zzb zzay;
    private final String zzaz;
    private final int[] zzba;
    private final String zzbb;
    private final DataType zzq;

    public static final class Builder {
        /* access modifiers changed from: private */
        public String name;
        /* access modifiers changed from: private */
        public int type = -1;
        /* access modifiers changed from: private */
        public Device zzax;
        /* access modifiers changed from: private */
        public zzb zzay;
        /* access modifiers changed from: private */
        public String zzaz = "";
        /* access modifiers changed from: private */
        public int[] zzba;
        /* access modifiers changed from: private */
        public DataType zzq;

        public final DataSource build() {
            boolean z = true;
            Preconditions.checkState(this.zzq != null, "Must set data type");
            if (this.type < 0) {
                z = false;
            }
            Preconditions.checkState(z, "Must set data source type");
            return new DataSource(this);
        }

        public final Builder setAppPackageName(Context context) {
            return setAppPackageName(context.getPackageName());
        }

        public final Builder setAppPackageName(String str) {
            this.zzay = zzb.zza(str);
            return this;
        }

        public final Builder setDataQualityStandards(int... iArr) {
            this.zzba = iArr;
            return this;
        }

        public final Builder setDataType(DataType dataType) {
            this.zzq = dataType;
            return this;
        }

        public final Builder setDevice(Device device) {
            this.zzax = device;
            return this;
        }

        public final Builder setName(String str) {
            this.name = str;
            return this;
        }

        public final Builder setStreamName(String str) {
            Preconditions.checkArgument(str != null, "Must specify a valid stream name");
            this.zzaz = str;
            return this;
        }

        public final Builder setType(int i) {
            this.type = i;
            return this;
        }
    }

    private DataSource(Builder builder) {
        this.zzq = builder.zzq;
        this.type = builder.type;
        this.name = builder.name;
        this.zzax = builder.zzax;
        this.zzay = builder.zzay;
        this.zzaz = builder.zzaz;
        this.zzbb = zzj();
        this.zzba = builder.zzba;
    }

    public DataSource(DataType dataType, String str, int i, Device device, zzb zzb, String str2, int[] iArr) {
        this.zzq = dataType;
        this.type = i;
        this.name = str;
        this.zzax = device;
        this.zzay = zzb;
        this.zzaz = str2;
        this.zzbb = zzj();
        this.zzba = iArr == null ? zzaw : iArr;
    }

    public static DataSource extract(Intent intent) {
        if (intent == null) {
            return null;
        }
        return (DataSource) SafeParcelableSerializer.deserializeFromIntentExtra(intent, EXTRA_DATA_SOURCE, CREATOR);
    }

    private final String getTypeString() {
        int i = this.type;
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "derived" : "converted" : "cleaned" : "derived" : "raw";
    }

    public static String zzd(int i) {
        switch (i) {
            case 1:
                return "blood_pressure_esh2002";
            case 2:
                return "blood_pressure_esh2010";
            case 3:
                return "blood_pressure_aami";
            case 4:
                return "blood_pressure_bhs_a_a";
            case 5:
                return "blood_pressure_bhs_a_b";
            case 6:
                return "blood_pressure_bhs_b_a";
            case 7:
                return "blood_pressure_bhs_b_b";
            case 8:
                return "blood_glucose_iso151972003";
            case 9:
                return "blood_glucose_iso151972013";
            default:
                return "unknown";
        }
    }

    private final String zzj() {
        StringBuilder sb = new StringBuilder();
        sb.append(getTypeString());
        sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
        sb.append(this.zzq.getName());
        if (this.zzay != null) {
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(this.zzay.getPackageName());
        }
        if (this.zzax != null) {
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(this.zzax.getStreamIdentifier());
        }
        if (this.zzaz != null) {
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(this.zzaz);
        }
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof DataSource)) {
            return false;
        }
        return this.zzbb.equals(((DataSource) obj).zzbb);
    }

    public String getAppPackageName() {
        zzb zzb = this.zzay;
        if (zzb == null) {
            return null;
        }
        return zzb.getPackageName();
    }

    public int[] getDataQualityStandards() {
        return this.zzba;
    }

    public DataType getDataType() {
        return this.zzq;
    }

    public Device getDevice() {
        return this.zzax;
    }

    public String getName() {
        return this.name;
    }

    public String getStreamIdentifier() {
        return this.zzbb;
    }

    public String getStreamName() {
        return this.zzaz;
    }

    public int getType() {
        return this.type;
    }

    public int hashCode() {
        return this.zzbb.hashCode();
    }

    public final String toDebugString() {
        String str;
        String str2;
        int i = this.type;
        String str3 = i != 0 ? i != 1 ? i != 2 ? i != 3 ? "?" : "v" : "c" : "d" : "r";
        String zzm = this.zzq.zzm();
        zzb zzb = this.zzay;
        String str4 = "";
        if (zzb == null) {
            str = str4;
        } else if (zzb.equals(zzb.zzad)) {
            str = ":gms";
        } else {
            String valueOf = String.valueOf(this.zzay.getPackageName());
            str = valueOf.length() != 0 ? Config.TRACE_TODAY_VISIT_SPLIT.concat(valueOf) : new String(Config.TRACE_TODAY_VISIT_SPLIT);
        }
        Device device = this.zzax;
        if (device != null) {
            String model = device.getModel();
            String uid = this.zzax.getUid();
            StringBuilder sb = new StringBuilder(String.valueOf(model).length() + 2 + String.valueOf(uid).length());
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(model);
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(uid);
            str2 = sb.toString();
        } else {
            str2 = str4;
        }
        String str5 = this.zzaz;
        if (str5 != null) {
            String valueOf2 = String.valueOf(str5);
            str4 = valueOf2.length() != 0 ? Config.TRACE_TODAY_VISIT_SPLIT.concat(valueOf2) : new String(Config.TRACE_TODAY_VISIT_SPLIT);
        }
        StringBuilder sb2 = new StringBuilder(str3.length() + 1 + String.valueOf(zzm).length() + String.valueOf(str).length() + String.valueOf(str2).length() + String.valueOf(str4).length());
        sb2.append(str3);
        sb2.append(Config.TRACE_TODAY_VISIT_SPLIT);
        sb2.append(zzm);
        sb2.append(str);
        sb2.append(str2);
        sb2.append(str4);
        return sb2.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("DataSource{");
        sb.append(getTypeString());
        if (this.name != null) {
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(this.name);
        }
        if (this.zzay != null) {
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(this.zzay);
        }
        if (this.zzax != null) {
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(this.zzax);
        }
        if (this.zzaz != null) {
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(this.zzaz);
        }
        sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
        sb.append(this.zzq);
        sb.append(VectorFormat.DEFAULT_SUFFIX);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, getDataType(), i, false);
        SafeParcelWriter.writeString(parcel, 2, getName(), false);
        SafeParcelWriter.writeInt(parcel, 3, getType());
        SafeParcelWriter.writeParcelable(parcel, 4, getDevice(), i, false);
        SafeParcelWriter.writeParcelable(parcel, 5, this.zzay, i, false);
        SafeParcelWriter.writeString(parcel, 6, getStreamName(), false);
        SafeParcelWriter.writeIntArray(parcel, 8, getDataQualityStandards(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final zzb zzi() {
        return this.zzay;
    }
}
