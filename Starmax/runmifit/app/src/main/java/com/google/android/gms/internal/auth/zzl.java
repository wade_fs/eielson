package com.google.android.gms.internal.auth;

import android.os.RemoteException;
import com.google.android.gms.auth.account.zzf;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;

final class zzl extends BaseImplementation.ApiMethodImpl<Result, zzu> {
    private final /* synthetic */ boolean zzag;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzl(zzk zzk, Api api, GoogleApiClient googleApiClient, boolean z) {
        super(api, googleApiClient);
        this.zzag = z;
    }

    /* access modifiers changed from: protected */
    public final Result createFailedResult(Status status) {
        return new zzs(status);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzf) ((zzu) anyClient).getService()).zze(this.zzag);
        setResult((Result) new zzs(Status.RESULT_SUCCESS));
    }
}
