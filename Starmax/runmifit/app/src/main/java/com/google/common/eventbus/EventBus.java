package com.google.common.eventbus;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.tamic.novate.util.FileUtil;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.greenrobot.greendao.generator.Schema;

public class EventBus {
    private static final LoadingCache<Class<?>, Set<Class<?>>> flattenHierarchyCache = CacheBuilder.newBuilder().weakKeys().build(new CacheLoader<Class<?>, Set<Class<?>>>() {
        /* class com.google.common.eventbus.EventBus.C19171 */

        public /* bridge */ /* synthetic */ Object load(Object obj) throws Exception {
            return load((Class<?>) ((Class) obj));
        }

        public Set<Class<?>> load(Class<?> cls) {
            return TypeToken.m5409of((Class) cls).getTypes().rawTypes();
        }
    });
    private final ThreadLocal<Queue<EventWithSubscriber>> eventsToDispatch;
    private final SubscriberFindingStrategy finder;
    private final ThreadLocal<Boolean> isDispatching;
    private SubscriberExceptionHandler subscriberExceptionHandler;
    private final SetMultimap<Class<?>, EventSubscriber> subscribersByType;
    private final ReadWriteLock subscribersByTypeLock;

    public EventBus() {
        this(Schema.DEFAULT_NAME);
    }

    public EventBus(String str) {
        this(new LoggingSubscriberExceptionHandler(str));
    }

    public EventBus(SubscriberExceptionHandler subscriberExceptionHandler2) {
        this.subscribersByType = HashMultimap.create();
        this.subscribersByTypeLock = new ReentrantReadWriteLock();
        this.finder = new AnnotatedSubscriberFinder();
        this.eventsToDispatch = new ThreadLocal<Queue<EventWithSubscriber>>() {
            /* class com.google.common.eventbus.EventBus.C19182 */

            /* access modifiers changed from: protected */
            public Queue<EventWithSubscriber> initialValue() {
                return new LinkedList();
            }
        };
        this.isDispatching = new ThreadLocal<Boolean>() {
            /* class com.google.common.eventbus.EventBus.C19193 */

            /* access modifiers changed from: protected */
            public Boolean initialValue() {
                return false;
            }
        };
        this.subscriberExceptionHandler = (SubscriberExceptionHandler) Preconditions.checkNotNull(subscriberExceptionHandler2);
    }

    public void register(Object obj) {
        Multimap<Class<?>, EventSubscriber> findAllSubscribers = this.finder.findAllSubscribers(obj);
        this.subscribersByTypeLock.writeLock().lock();
        try {
            this.subscribersByType.putAll(findAllSubscribers);
        } finally {
            this.subscribersByTypeLock.writeLock().unlock();
        }
    }

    public void unregister(Object obj) {
        for (Map.Entry entry : this.finder.findAllSubscribers(obj).asMap().entrySet()) {
            Class cls = (Class) entry.getKey();
            Collection collection = (Collection) entry.getValue();
            this.subscribersByTypeLock.writeLock().lock();
            try {
                Set<EventSubscriber> set = this.subscribersByType.get((Object) cls);
                if (set.containsAll(collection)) {
                    set.removeAll(collection);
                } else {
                    throw new IllegalArgumentException("missing event subscriber for an annotated method. Is " + obj + " registered?");
                }
            } finally {
                this.subscribersByTypeLock.writeLock().unlock();
            }
        }
    }

    public void post(Object obj) {
        boolean z = false;
        for (Class cls : flattenHierarchy(obj.getClass())) {
            this.subscribersByTypeLock.readLock().lock();
            try {
                Set<EventSubscriber> set = this.subscribersByType.get((Object) cls);
                if (!set.isEmpty()) {
                    z = true;
                    for (EventSubscriber eventSubscriber : set) {
                        enqueueEvent(obj, eventSubscriber);
                    }
                }
            } finally {
                this.subscribersByTypeLock.readLock().unlock();
            }
        }
        if (!z && !(obj instanceof DeadEvent)) {
            post(new DeadEvent(this, obj));
        }
        dispatchQueuedEvents();
    }

    /* access modifiers changed from: package-private */
    public void enqueueEvent(Object obj, EventSubscriber eventSubscriber) {
        this.eventsToDispatch.get().offer(new EventWithSubscriber(obj, eventSubscriber));
    }

    /* access modifiers changed from: package-private */
    public void dispatchQueuedEvents() {
        if (!this.isDispatching.get().booleanValue()) {
            this.isDispatching.set(true);
            try {
                Queue queue = this.eventsToDispatch.get();
                while (true) {
                    EventWithSubscriber eventWithSubscriber = (EventWithSubscriber) queue.poll();
                    if (eventWithSubscriber != null) {
                        dispatch(eventWithSubscriber.event, eventWithSubscriber.subscriber);
                    } else {
                        return;
                    }
                }
            } finally {
                this.isDispatching.remove();
                this.eventsToDispatch.remove();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatch(Object obj, EventSubscriber eventSubscriber) {
        try {
            eventSubscriber.handleEvent(obj);
        } catch (InvocationTargetException e) {
            this.subscriberExceptionHandler.handleException(e.getCause(), new SubscriberExceptionContext(this, obj, eventSubscriber.getSubscriber(), eventSubscriber.getMethod()));
        } catch (Throwable th) {
            Logger.getLogger(EventBus.class.getName()).log(Level.SEVERE, String.format("Exception %s thrown while handling exception: %s", th, e.getCause()), th);
        }
    }

    /* access modifiers changed from: package-private */
    public Set<Class<?>> flattenHierarchy(Class<?> cls) {
        try {
            return flattenHierarchyCache.getUnchecked(cls);
        } catch (UncheckedExecutionException e) {
            throw Throwables.propagate(e.getCause());
        }
    }

    private static final class LoggingSubscriberExceptionHandler implements SubscriberExceptionHandler {
        private final Logger logger;

        public LoggingSubscriberExceptionHandler(String str) {
            this.logger = Logger.getLogger(EventBus.class.getName() + FileUtil.HIDDEN_PREFIX + ((String) Preconditions.checkNotNull(str)));
        }

        public void handleException(Throwable th, SubscriberExceptionContext subscriberExceptionContext) {
            Logger logger2 = this.logger;
            Level level = Level.SEVERE;
            logger2.log(level, "Could not dispatch event: " + subscriberExceptionContext.getSubscriber() + " to " + subscriberExceptionContext.getSubscriberMethod(), th.getCause());
        }
    }

    static class EventWithSubscriber {
        final Object event;
        final EventSubscriber subscriber;

        public EventWithSubscriber(Object obj, EventSubscriber eventSubscriber) {
            this.event = Preconditions.checkNotNull(obj);
            this.subscriber = (EventSubscriber) Preconditions.checkNotNull(eventSubscriber);
        }
    }
}
