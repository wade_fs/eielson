package com.google.android.gms.common.util;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.math3.geometry.VectorFormat;

public class MapUtils {
    public static <K, V> K getKeyFromMap(Map<K, V> map, K k) {
        if (!map.containsKey(k)) {
            return null;
        }
        for (K k2 : map.keySet()) {
            if (k2.equals(k)) {
                return k2;
            }
        }
        return null;
    }

    public static void writeStringMapToJson(StringBuilder sb, HashMap<String, String> hashMap) {
        sb.append(VectorFormat.DEFAULT_PREFIX);
        boolean z = true;
        for (String str : hashMap.keySet()) {
            if (!z) {
                sb.append(",");
            } else {
                z = false;
            }
            String str2 = hashMap.get(str);
            sb.append("\"");
            sb.append(str);
            sb.append("\":");
            if (str2 == null) {
                sb.append("null");
            } else {
                sb.append("\"");
                sb.append(str2);
                sb.append("\"");
            }
        }
        sb.append(VectorFormat.DEFAULT_SUFFIX);
    }
}
