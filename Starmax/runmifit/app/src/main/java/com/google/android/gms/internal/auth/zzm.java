package com.google.android.gms.internal.auth;

import android.os.RemoteException;
import com.google.android.gms.auth.account.WorkAccountApi;
import com.google.android.gms.auth.account.zzf;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;

final class zzm extends BaseImplementation.ApiMethodImpl<WorkAccountApi.AddAccountResult, zzu> {
    private final /* synthetic */ String zzt;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzm(zzk zzk, Api api, GoogleApiClient googleApiClient, String str) {
        super(api, googleApiClient);
        this.zzt = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return new zzr(status, null);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzf) ((zzu) anyClient).getService()).zzd(new zzn(this), this.zzt);
    }
}
