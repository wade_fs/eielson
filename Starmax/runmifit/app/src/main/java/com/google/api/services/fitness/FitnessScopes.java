package com.google.api.services.fitness;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class FitnessScopes {
    public static final String FITNESS_ACTIVITY_READ = "https://www.googleapis.com/auth/fitness.activity.read";
    public static final String FITNESS_ACTIVITY_WRITE = "https://www.googleapis.com/auth/fitness.activity.write";
    public static final String FITNESS_BODY_READ = "https://www.googleapis.com/auth/fitness.body.read";
    public static final String FITNESS_BODY_WRITE = "https://www.googleapis.com/auth/fitness.body.write";
    public static final String FITNESS_LOCATION_READ = "https://www.googleapis.com/auth/fitness.location.read";
    public static final String FITNESS_LOCATION_WRITE = "https://www.googleapis.com/auth/fitness.location.write";

    public static Set<String> all() {
        HashSet hashSet = new HashSet();
        hashSet.add("https://www.googleapis.com/auth/fitness.activity.read");
        hashSet.add("https://www.googleapis.com/auth/fitness.activity.write");
        hashSet.add("https://www.googleapis.com/auth/fitness.body.read");
        hashSet.add("https://www.googleapis.com/auth/fitness.body.write");
        hashSet.add("https://www.googleapis.com/auth/fitness.location.read");
        hashSet.add("https://www.googleapis.com/auth/fitness.location.write");
        return Collections.unmodifiableSet(hashSet);
    }

    private FitnessScopes() {
    }
}
