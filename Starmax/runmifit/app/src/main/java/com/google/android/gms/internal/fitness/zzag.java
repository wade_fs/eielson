package com.google.android.gms.internal.fitness;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.fitness.FitnessOptions;

public final class zzag extends zzn<zzbz> {
    public static final Api<Api.ApiOptions.NoOptions> API = new Api<>("Fitness.API", new zzai(), CLIENT_KEY);
    private static final Api.ClientKey<zzag> CLIENT_KEY = new Api.ClientKey<>();
    public static final Api<FitnessOptions> zzew = new Api<>("Fitness.CLIENT", new zzak(), CLIENT_KEY);

    private zzag(Context context, Looper looper, ClientSettings clientSettings, GoogleApiClient.ConnectionCallbacks connectionCallbacks, GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 57, connectionCallbacks, onConnectionFailedListener, clientSettings);
    }

    public final /* synthetic */ IInterface createServiceInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
        return queryLocalInterface instanceof zzbz ? (zzbz) queryLocalInterface : new zzca(iBinder);
    }

    public final int getMinApkVersion() {
        return GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    public final String getServiceDescriptor() {
        return "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi";
    }

    public final String getStartServiceAction() {
        return "com.google.android.gms.fitness.HistoryApi";
    }
}
