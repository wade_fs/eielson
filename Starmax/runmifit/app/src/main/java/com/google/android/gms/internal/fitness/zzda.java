package com.google.android.gms.internal.fitness;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.fitness.result.BleDevicesResult;

final class zzda extends zzes {
    private final BaseImplementation.ResultHolder<BleDevicesResult> zzev;

    private zzda(BaseImplementation.ResultHolder<BleDevicesResult> resultHolder) {
        this.zzev = resultHolder;
    }

    /* synthetic */ zzda(BaseImplementation.ResultHolder resultHolder, zzcu zzcu) {
        this(resultHolder);
    }

    public final void zza(BleDevicesResult bleDevicesResult) {
        this.zzev.setResult(bleDevicesResult);
    }
}
