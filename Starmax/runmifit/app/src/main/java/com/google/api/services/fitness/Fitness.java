package com.google.api.services.fitness;

import com.google.api.client.googleapis.GoogleUtils;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClient;
import com.google.api.client.http.HttpMethods;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.Key;
import com.google.api.client.util.Preconditions;
import com.google.api.services.fitness.model.AggregateRequest;
import com.google.api.services.fitness.model.AggregateResponse;
import com.google.api.services.fitness.model.DataSource;
import com.google.api.services.fitness.model.ListDataSourcesResponse;
import com.google.api.services.fitness.model.ListSessionsResponse;
import com.google.api.services.fitness.model.Session;
import java.io.IOException;

public class Fitness extends AbstractGoogleJsonClient {
    public static final String DEFAULT_BASE_URL = "https://www.googleapis.com/fitness/v1/users/";
    public static final String DEFAULT_ROOT_URL = "https://www.googleapis.com/";
    public static final String DEFAULT_SERVICE_PATH = "fitness/v1/users/";

    static {
        Preconditions.checkState(GoogleUtils.MAJOR_VERSION.intValue() == 1 && GoogleUtils.MINOR_VERSION.intValue() >= 15, "You are currently running with version %s of google-api-client. You need at least version 1.15 of google-api-client to run version 1.22.0 of the Fitness library.", GoogleUtils.VERSION);
    }

    public Fitness(HttpTransport httpTransport, JsonFactory jsonFactory, HttpRequestInitializer httpRequestInitializer) {
        this(new Builder(httpTransport, jsonFactory, httpRequestInitializer));
    }

    Fitness(Builder builder) {
        super(builder);
    }

    /* access modifiers changed from: protected */
    public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
        super.initialize(abstractGoogleClientRequest);
    }

    public Users users() {
        return new Users();
    }

    public class Users {
        public Users() {
        }

        public DataSources dataSources() {
            return new DataSources();
        }

        public class DataSources {
            public DataSources() {
            }

            public Create create(String str, DataSource dataSource) throws IOException {
                Create create = new Create(str, dataSource);
                Fitness.this.initialize(create);
                return create;
            }

            public class Create extends FitnessRequest<DataSource> {
                private static final String REST_PATH = "{userId}/dataSources";
                @Key
                private String userId;

                protected Create(String str, DataSource dataSource) {
                    super(Fitness.this, "POST", REST_PATH, dataSource, DataSource.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                    checkRequiredParameter(dataSource, "content");
                    checkRequiredParameter(dataSource.getType(), "DataSource.getType()");
                }

                public Create setAlt(String str) {
                    return (Create) super.setAlt(str);
                }

                public Create setFields(String str) {
                    return (Create) super.setFields(str);
                }

                public Create setKey(String str) {
                    return (Create) super.setKey(str);
                }

                public Create setOauthToken(String str) {
                    return (Create) super.setOauthToken(str);
                }

                public Create setPrettyPrint(Boolean bool) {
                    return (Create) super.setPrettyPrint(bool);
                }

                public Create setQuotaUser(String str) {
                    return (Create) super.setQuotaUser(str);
                }

                public Create setUserIp(String str) {
                    return (Create) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public Create setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.DataSources.Create.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Create.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Create.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.DataSources.Create.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$Create
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public Create set(String str, Object obj) {
                    return (Create) super.set(str, obj);
                }
            }

            public Delete delete(String str, String str2) throws IOException {
                Delete delete = new Delete(str, str2);
                Fitness.this.initialize(delete);
                return delete;
            }

            public class Delete extends FitnessRequest<DataSource> {
                private static final String REST_PATH = "{userId}/dataSources/{dataSourceId}";
                @Key
                private String dataSourceId;
                @Key
                private String userId;

                protected Delete(String str, String str2) {
                    super(Fitness.this, HttpMethods.DELETE, REST_PATH, null, DataSource.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                    this.dataSourceId = (String) Preconditions.checkNotNull(str2, "Required parameter dataSourceId must be specified.");
                }

                public Delete setAlt(String str) {
                    return (Delete) super.setAlt(str);
                }

                public Delete setFields(String str) {
                    return (Delete) super.setFields(str);
                }

                public Delete setKey(String str) {
                    return (Delete) super.setKey(str);
                }

                public Delete setOauthToken(String str) {
                    return (Delete) super.setOauthToken(str);
                }

                public Delete setPrettyPrint(Boolean bool) {
                    return (Delete) super.setPrettyPrint(bool);
                }

                public Delete setQuotaUser(String str) {
                    return (Delete) super.setQuotaUser(str);
                }

                public Delete setUserIp(String str) {
                    return (Delete) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public Delete setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                public String getDataSourceId() {
                    return this.dataSourceId;
                }

                public Delete setDataSourceId(String str) {
                    this.dataSourceId = str;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.DataSources.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.DataSources.Delete.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$Delete
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public Delete set(String str, Object obj) {
                    return (Delete) super.set(str, obj);
                }
            }

            public Get get(String str, String str2) throws IOException {
                Get get = new Get(str, str2);
                Fitness.this.initialize(get);
                return get;
            }

            public class Get extends FitnessRequest<DataSource> {
                private static final String REST_PATH = "{userId}/dataSources/{dataSourceId}";
                @Key
                private String dataSourceId;
                @Key
                private String userId;

                protected Get(String str, String str2) {
                    super(Fitness.this, "GET", REST_PATH, null, DataSource.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                    this.dataSourceId = (String) Preconditions.checkNotNull(str2, "Required parameter dataSourceId must be specified.");
                }

                public HttpResponse executeUsingHead() throws IOException {
                    return super.executeUsingHead();
                }

                public HttpRequest buildHttpRequestUsingHead() throws IOException {
                    return super.buildHttpRequestUsingHead();
                }

                public Get setAlt(String str) {
                    return (Get) super.setAlt(str);
                }

                public Get setFields(String str) {
                    return (Get) super.setFields(str);
                }

                public Get setKey(String str) {
                    return (Get) super.setKey(str);
                }

                public Get setOauthToken(String str) {
                    return (Get) super.setOauthToken(str);
                }

                public Get setPrettyPrint(Boolean bool) {
                    return (Get) super.setPrettyPrint(bool);
                }

                public Get setQuotaUser(String str) {
                    return (Get) super.setQuotaUser(str);
                }

                public Get setUserIp(String str) {
                    return (Get) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public Get setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                public String getDataSourceId() {
                    return this.dataSourceId;
                }

                public Get setDataSourceId(String str) {
                    this.dataSourceId = str;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.DataSources.Get.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Get.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Get.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.DataSources.Get.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$Get
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public Get set(String str, Object obj) {
                    return (Get) super.set(str, obj);
                }
            }

            public List list(String str) throws IOException {
                List list = new List(str);
                Fitness.this.initialize(list);
                return list;
            }

            public class List extends FitnessRequest<ListDataSourcesResponse> {
                private static final String REST_PATH = "{userId}/dataSources";
                @Key
                private java.util.List<String> dataTypeName;
                @Key
                private String userId;

                protected List(String str) {
                    super(Fitness.this, "GET", REST_PATH, null, ListDataSourcesResponse.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                }

                public HttpResponse executeUsingHead() throws IOException {
                    return super.executeUsingHead();
                }

                public HttpRequest buildHttpRequestUsingHead() throws IOException {
                    return super.buildHttpRequestUsingHead();
                }

                public List setAlt(String str) {
                    return (List) super.setAlt(str);
                }

                public List setFields(String str) {
                    return (List) super.setFields(str);
                }

                public List setKey(String str) {
                    return (List) super.setKey(str);
                }

                public List setOauthToken(String str) {
                    return (List) super.setOauthToken(str);
                }

                public List setPrettyPrint(Boolean bool) {
                    return (List) super.setPrettyPrint(bool);
                }

                public List setQuotaUser(String str) {
                    return (List) super.setQuotaUser(str);
                }

                public List setUserIp(String str) {
                    return (List) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public List setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                public java.util.List<String> getDataTypeName() {
                    return this.dataTypeName;
                }

                public List setDataTypeName(java.util.List<String> list) {
                    this.dataTypeName = list;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.DataSources.List.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.List.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.List.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.DataSources.List.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$List
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public List set(String str, Object obj) {
                    return (List) super.set(str, obj);
                }
            }

            public Patch patch(String str, String str2, DataSource dataSource) throws IOException {
                Patch patch = new Patch(str, str2, dataSource);
                Fitness.this.initialize(patch);
                return patch;
            }

            public class Patch extends FitnessRequest<DataSource> {
                private static final String REST_PATH = "{userId}/dataSources/{dataSourceId}";
                @Key
                private String dataSourceId;
                @Key
                private String userId;

                protected Patch(String str, String str2, DataSource dataSource) {
                    super(Fitness.this, HttpMethods.PATCH, REST_PATH, dataSource, DataSource.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                    this.dataSourceId = (String) Preconditions.checkNotNull(str2, "Required parameter dataSourceId must be specified.");
                }

                public Patch setAlt(String str) {
                    return (Patch) super.setAlt(str);
                }

                public Patch setFields(String str) {
                    return (Patch) super.setFields(str);
                }

                public Patch setKey(String str) {
                    return (Patch) super.setKey(str);
                }

                public Patch setOauthToken(String str) {
                    return (Patch) super.setOauthToken(str);
                }

                public Patch setPrettyPrint(Boolean bool) {
                    return (Patch) super.setPrettyPrint(bool);
                }

                public Patch setQuotaUser(String str) {
                    return (Patch) super.setQuotaUser(str);
                }

                public Patch setUserIp(String str) {
                    return (Patch) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public Patch setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                public String getDataSourceId() {
                    return this.dataSourceId;
                }

                public Patch setDataSourceId(String str) {
                    this.dataSourceId = str;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.DataSources.Patch.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Patch.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Patch.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.DataSources.Patch.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$Patch
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public Patch set(String str, Object obj) {
                    return (Patch) super.set(str, obj);
                }
            }

            public Update update(String str, String str2, DataSource dataSource) throws IOException {
                Update update = new Update(str, str2, dataSource);
                Fitness.this.initialize(update);
                return update;
            }

            public class Update extends FitnessRequest<DataSource> {
                private static final String REST_PATH = "{userId}/dataSources/{dataSourceId}";
                @Key
                private String dataSourceId;
                @Key
                private String userId;

                protected Update(String str, String str2, DataSource dataSource) {
                    super(Fitness.this, HttpMethods.PUT, REST_PATH, dataSource, DataSource.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                    this.dataSourceId = (String) Preconditions.checkNotNull(str2, "Required parameter dataSourceId must be specified.");
                    checkRequiredParameter(dataSource, "content");
                    checkRequiredParameter(dataSource.getDataStreamId(), "DataSource.getDataStreamId()");
                    checkRequiredParameter(dataSource, "content");
                    checkRequiredParameter(dataSource.getType(), "DataSource.getType()");
                }

                public Update setAlt(String str) {
                    return (Update) super.setAlt(str);
                }

                public Update setFields(String str) {
                    return (Update) super.setFields(str);
                }

                public Update setKey(String str) {
                    return (Update) super.setKey(str);
                }

                public Update setOauthToken(String str) {
                    return (Update) super.setOauthToken(str);
                }

                public Update setPrettyPrint(Boolean bool) {
                    return (Update) super.setPrettyPrint(bool);
                }

                public Update setQuotaUser(String str) {
                    return (Update) super.setQuotaUser(str);
                }

                public Update setUserIp(String str) {
                    return (Update) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public Update setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                public String getDataSourceId() {
                    return this.dataSourceId;
                }

                public Update setDataSourceId(String str) {
                    this.dataSourceId = str;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.DataSources.Update.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Update.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.DataSources.Update.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.DataSources.Update.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$Update
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public Update set(String str, Object obj) {
                    return (Update) super.set(str, obj);
                }
            }

            public Datasets datasets() {
                return new Datasets();
            }

            public class Datasets {
                public Datasets() {
                }

                public Delete delete(String str, String str2, String str3) throws IOException {
                    Delete delete = new Delete(str, str2, str3);
                    Fitness.this.initialize(delete);
                    return delete;
                }

                public class Delete extends FitnessRequest<Void> {
                    private static final String REST_PATH = "{userId}/dataSources/{dataSourceId}/datasets/{datasetId}";
                    @Key
                    private Long currentTimeMillis;
                    @Key
                    private String dataSourceId;
                    @Key
                    private String datasetId;
                    @Key
                    private Long modifiedTimeMillis;
                    @Key
                    private String userId;

                    protected Delete(String str, String str2, String str3) {
                        super(Fitness.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                        this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                        this.dataSourceId = (String) Preconditions.checkNotNull(str2, "Required parameter dataSourceId must be specified.");
                        this.datasetId = (String) Preconditions.checkNotNull(str3, "Required parameter datasetId must be specified.");
                    }

                    public Delete setAlt(String str) {
                        return (Delete) super.setAlt(str);
                    }

                    public Delete setFields(String str) {
                        return (Delete) super.setFields(str);
                    }

                    public Delete setKey(String str) {
                        return (Delete) super.setKey(str);
                    }

                    public Delete setOauthToken(String str) {
                        return (Delete) super.setOauthToken(str);
                    }

                    public Delete setPrettyPrint(Boolean bool) {
                        return (Delete) super.setPrettyPrint(bool);
                    }

                    public Delete setQuotaUser(String str) {
                        return (Delete) super.setQuotaUser(str);
                    }

                    public Delete setUserIp(String str) {
                        return (Delete) super.setUserIp(str);
                    }

                    public String getUserId() {
                        return this.userId;
                    }

                    public Delete setUserId(String str) {
                        this.userId = str;
                        return this;
                    }

                    public String getDataSourceId() {
                        return this.dataSourceId;
                    }

                    public Delete setDataSourceId(String str) {
                        this.dataSourceId = str;
                        return this;
                    }

                    public String getDatasetId() {
                        return this.datasetId;
                    }

                    public Delete setDatasetId(String str) {
                        this.datasetId = str;
                        return this;
                    }

                    public Long getCurrentTimeMillis() {
                        return this.currentTimeMillis;
                    }

                    public Delete setCurrentTimeMillis(Long l) {
                        this.currentTimeMillis = l;
                        return this;
                    }

                    public Long getModifiedTimeMillis() {
                        return this.modifiedTimeMillis;
                    }

                    public Delete setModifiedTimeMillis(Long l) {
                        this.modifiedTimeMillis = l;
                        return this;
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                     arg types: [java.lang.String, java.lang.Object]
                     candidates:
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Delete.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$Datasets$Delete
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                    public Delete set(String str, Object obj) {
                        return (Delete) super.set(str, obj);
                    }
                }

                public Get get(String str, String str2, String str3) throws IOException {
                    Get get = new Get(str, str2, str3);
                    Fitness.this.initialize(get);
                    return get;
                }

                public class Get extends FitnessRequest<com.google.api.services.fitness.model.Dataset> {
                    private static final String REST_PATH = "{userId}/dataSources/{dataSourceId}/datasets/{datasetId}";
                    @Key
                    private String dataSourceId;
                    @Key
                    private String datasetId;
                    @Key
                    private Integer limit;
                    @Key
                    private String pageToken;
                    @Key
                    private String userId;

                    protected Get(String str, String str2, String str3) {
                        super(Fitness.this, "GET", REST_PATH, null, com.google.api.services.fitness.model.Dataset.class);
                        this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                        this.dataSourceId = (String) Preconditions.checkNotNull(str2, "Required parameter dataSourceId must be specified.");
                        this.datasetId = (String) Preconditions.checkNotNull(str3, "Required parameter datasetId must be specified.");
                    }

                    public HttpResponse executeUsingHead() throws IOException {
                        return super.executeUsingHead();
                    }

                    public HttpRequest buildHttpRequestUsingHead() throws IOException {
                        return super.buildHttpRequestUsingHead();
                    }

                    public Get setAlt(String str) {
                        return (Get) super.setAlt(str);
                    }

                    public Get setFields(String str) {
                        return (Get) super.setFields(str);
                    }

                    public Get setKey(String str) {
                        return (Get) super.setKey(str);
                    }

                    public Get setOauthToken(String str) {
                        return (Get) super.setOauthToken(str);
                    }

                    public Get setPrettyPrint(Boolean bool) {
                        return (Get) super.setPrettyPrint(bool);
                    }

                    public Get setQuotaUser(String str) {
                        return (Get) super.setQuotaUser(str);
                    }

                    public Get setUserIp(String str) {
                        return (Get) super.setUserIp(str);
                    }

                    public String getUserId() {
                        return this.userId;
                    }

                    public Get setUserId(String str) {
                        this.userId = str;
                        return this;
                    }

                    public String getDataSourceId() {
                        return this.dataSourceId;
                    }

                    public Get setDataSourceId(String str) {
                        this.dataSourceId = str;
                        return this;
                    }

                    public String getDatasetId() {
                        return this.datasetId;
                    }

                    public Get setDatasetId(String str) {
                        this.datasetId = str;
                        return this;
                    }

                    public Integer getLimit() {
                        return this.limit;
                    }

                    public Get setLimit(Integer num) {
                        this.limit = num;
                        return this;
                    }

                    public String getPageToken() {
                        return this.pageToken;
                    }

                    public Get setPageToken(String str) {
                        this.pageToken = str;
                        return this;
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                     arg types: [java.lang.String, java.lang.Object]
                     candidates:
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Get.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Get.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Get.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Get.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$Datasets$Get
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                    public Get set(String str, Object obj) {
                        return (Get) super.set(str, obj);
                    }
                }

                public Patch patch(String str, String str2, String str3, com.google.api.services.fitness.model.Dataset dataset) throws IOException {
                    Patch patch = new Patch(str, str2, str3, dataset);
                    Fitness.this.initialize(patch);
                    return patch;
                }

                public class Patch extends FitnessRequest<com.google.api.services.fitness.model.Dataset> {
                    private static final String REST_PATH = "{userId}/dataSources/{dataSourceId}/datasets/{datasetId}";
                    @Key
                    private Long currentTimeMillis;
                    @Key
                    private String dataSourceId;
                    @Key
                    private String datasetId;
                    @Key
                    private String userId;

                    protected Patch(String str, String str2, String str3, com.google.api.services.fitness.model.Dataset dataset) {
                        super(Fitness.this, HttpMethods.PATCH, REST_PATH, dataset, com.google.api.services.fitness.model.Dataset.class);
                        this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                        this.dataSourceId = (String) Preconditions.checkNotNull(str2, "Required parameter dataSourceId must be specified.");
                        this.datasetId = (String) Preconditions.checkNotNull(str3, "Required parameter datasetId must be specified.");
                        checkRequiredParameter(dataset, "content");
                        checkRequiredParameter(dataset.getDataSourceId(), "Dataset.getDataSourceId()");
                        checkRequiredParameter(dataset, "content");
                        checkRequiredParameter(dataset.getMaxEndTimeNs(), "Dataset.getMaxEndTimeNs()");
                        checkRequiredParameter(dataset, "content");
                        checkRequiredParameter(dataset.getMinStartTimeNs(), "Dataset.getMinStartTimeNs()");
                    }

                    public Patch setAlt(String str) {
                        return (Patch) super.setAlt(str);
                    }

                    public Patch setFields(String str) {
                        return (Patch) super.setFields(str);
                    }

                    public Patch setKey(String str) {
                        return (Patch) super.setKey(str);
                    }

                    public Patch setOauthToken(String str) {
                        return (Patch) super.setOauthToken(str);
                    }

                    public Patch setPrettyPrint(Boolean bool) {
                        return (Patch) super.setPrettyPrint(bool);
                    }

                    public Patch setQuotaUser(String str) {
                        return (Patch) super.setQuotaUser(str);
                    }

                    public Patch setUserIp(String str) {
                        return (Patch) super.setUserIp(str);
                    }

                    public String getUserId() {
                        return this.userId;
                    }

                    public Patch setUserId(String str) {
                        this.userId = str;
                        return this;
                    }

                    public String getDataSourceId() {
                        return this.dataSourceId;
                    }

                    public Patch setDataSourceId(String str) {
                        this.dataSourceId = str;
                        return this;
                    }

                    public String getDatasetId() {
                        return this.datasetId;
                    }

                    public Patch setDatasetId(String str) {
                        this.datasetId = str;
                        return this;
                    }

                    public Long getCurrentTimeMillis() {
                        return this.currentTimeMillis;
                    }

                    public Patch setCurrentTimeMillis(Long l) {
                        this.currentTimeMillis = l;
                        return this;
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                     arg types: [java.lang.String, java.lang.Object]
                     candidates:
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Patch.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Patch.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Patch.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.services.fitness.Fitness.Users.DataSources.Datasets.Patch.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$DataSources$Datasets$Patch
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                      com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                      com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                    public Patch set(String str, Object obj) {
                        return (Patch) super.set(str, obj);
                    }
                }
            }
        }

        public Dataset dataset() {
            return new Dataset();
        }

        public class Dataset {
            public Dataset() {
            }

            public Aggregate aggregate(String str, AggregateRequest aggregateRequest) throws IOException {
                Aggregate aggregate = new Aggregate(str, aggregateRequest);
                Fitness.this.initialize(aggregate);
                return aggregate;
            }

            public class Aggregate extends FitnessRequest<AggregateResponse> {
                private static final String REST_PATH = "{userId}/dataset:aggregate";
                @Key
                private String userId;

                protected Aggregate(String str, AggregateRequest aggregateRequest) {
                    super(Fitness.this, "POST", REST_PATH, aggregateRequest, AggregateResponse.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                }

                public Aggregate setAlt(String str) {
                    return (Aggregate) super.setAlt(str);
                }

                public Aggregate setFields(String str) {
                    return (Aggregate) super.setFields(str);
                }

                public Aggregate setKey(String str) {
                    return (Aggregate) super.setKey(str);
                }

                public Aggregate setOauthToken(String str) {
                    return (Aggregate) super.setOauthToken(str);
                }

                public Aggregate setPrettyPrint(Boolean bool) {
                    return (Aggregate) super.setPrettyPrint(bool);
                }

                public Aggregate setQuotaUser(String str) {
                    return (Aggregate) super.setQuotaUser(str);
                }

                public Aggregate setUserIp(String str) {
                    return (Aggregate) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public Aggregate setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.Dataset.Aggregate.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.Dataset.Aggregate.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.Dataset.Aggregate.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.Dataset.Aggregate.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$Dataset$Aggregate
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public Aggregate set(String str, Object obj) {
                    return (Aggregate) super.set(str, obj);
                }
            }
        }

        public Sessions sessions() {
            return new Sessions();
        }

        public class Sessions {
            public Sessions() {
            }

            public Delete delete(String str, String str2) throws IOException {
                Delete delete = new Delete(str, str2);
                Fitness.this.initialize(delete);
                return delete;
            }

            public class Delete extends FitnessRequest<Void> {
                private static final String REST_PATH = "{userId}/sessions/{sessionId}";
                @Key
                private Long currentTimeMillis;
                @Key
                private String sessionId;
                @Key
                private String userId;

                protected Delete(String str, String str2) {
                    super(Fitness.this, HttpMethods.DELETE, REST_PATH, null, Void.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                    this.sessionId = (String) Preconditions.checkNotNull(str2, "Required parameter sessionId must be specified.");
                }

                public Delete setAlt(String str) {
                    return (Delete) super.setAlt(str);
                }

                public Delete setFields(String str) {
                    return (Delete) super.setFields(str);
                }

                public Delete setKey(String str) {
                    return (Delete) super.setKey(str);
                }

                public Delete setOauthToken(String str) {
                    return (Delete) super.setOauthToken(str);
                }

                public Delete setPrettyPrint(Boolean bool) {
                    return (Delete) super.setPrettyPrint(bool);
                }

                public Delete setQuotaUser(String str) {
                    return (Delete) super.setQuotaUser(str);
                }

                public Delete setUserIp(String str) {
                    return (Delete) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public Delete setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                public String getSessionId() {
                    return this.sessionId;
                }

                public Delete setSessionId(String str) {
                    this.sessionId = str;
                    return this;
                }

                public Long getCurrentTimeMillis() {
                    return this.currentTimeMillis;
                }

                public Delete setCurrentTimeMillis(Long l) {
                    this.currentTimeMillis = l;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.Sessions.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.Sessions.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.Sessions.Delete.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.Sessions.Delete.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$Sessions$Delete
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public Delete set(String str, Object obj) {
                    return (Delete) super.set(str, obj);
                }
            }

            public List list(String str) throws IOException {
                List list = new List(str);
                Fitness.this.initialize(list);
                return list;
            }

            public class List extends FitnessRequest<ListSessionsResponse> {
                private static final String REST_PATH = "{userId}/sessions";
                @Key
                private String endTime;
                @Key
                private Boolean includeDeleted;
                @Key
                private String pageToken;
                @Key
                private String startTime;
                @Key
                private String userId;

                protected List(String str) {
                    super(Fitness.this, "GET", REST_PATH, null, ListSessionsResponse.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                }

                public HttpResponse executeUsingHead() throws IOException {
                    return super.executeUsingHead();
                }

                public HttpRequest buildHttpRequestUsingHead() throws IOException {
                    return super.buildHttpRequestUsingHead();
                }

                public List setAlt(String str) {
                    return (List) super.setAlt(str);
                }

                public List setFields(String str) {
                    return (List) super.setFields(str);
                }

                public List setKey(String str) {
                    return (List) super.setKey(str);
                }

                public List setOauthToken(String str) {
                    return (List) super.setOauthToken(str);
                }

                public List setPrettyPrint(Boolean bool) {
                    return (List) super.setPrettyPrint(bool);
                }

                public List setQuotaUser(String str) {
                    return (List) super.setQuotaUser(str);
                }

                public List setUserIp(String str) {
                    return (List) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public List setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                public String getEndTime() {
                    return this.endTime;
                }

                public List setEndTime(String str) {
                    this.endTime = str;
                    return this;
                }

                public Boolean getIncludeDeleted() {
                    return this.includeDeleted;
                }

                public List setIncludeDeleted(Boolean bool) {
                    this.includeDeleted = bool;
                    return this;
                }

                public String getPageToken() {
                    return this.pageToken;
                }

                public List setPageToken(String str) {
                    this.pageToken = str;
                    return this;
                }

                public String getStartTime() {
                    return this.startTime;
                }

                public List setStartTime(String str) {
                    this.startTime = str;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.Sessions.List.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.Sessions.List.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.Sessions.List.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.Sessions.List.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$Sessions$List
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public List set(String str, Object obj) {
                    return (List) super.set(str, obj);
                }
            }

            public Update update(String str, String str2, Session session) throws IOException {
                Update update = new Update(str, str2, session);
                Fitness.this.initialize(update);
                return update;
            }

            public class Update extends FitnessRequest<Session> {
                private static final String REST_PATH = "{userId}/sessions/{sessionId}";
                @Key
                private Long currentTimeMillis;
                @Key
                private String sessionId;
                @Key
                private String userId;

                protected Update(String str, String str2, Session session) {
                    super(Fitness.this, HttpMethods.PUT, REST_PATH, session, Session.class);
                    this.userId = (String) Preconditions.checkNotNull(str, "Required parameter userId must be specified.");
                    this.sessionId = (String) Preconditions.checkNotNull(str2, "Required parameter sessionId must be specified.");
                    checkRequiredParameter(session, "content");
                    checkRequiredParameter(session.getActivityType(), "Session.getActivityType()");
                    checkRequiredParameter(session, "content");
                    checkRequiredParameter(session.getEndTimeMillis(), "Session.getEndTimeMillis()");
                    checkRequiredParameter(session, "content");
                    checkRequiredParameter(session.getId(), "Session.getId()");
                    checkRequiredParameter(session, "content");
                    checkRequiredParameter(session.getName(), "Session.getName()");
                    checkRequiredParameter(session, "content");
                    checkRequiredParameter(session.getStartTimeMillis(), "Session.getStartTimeMillis()");
                }

                public Update setAlt(String str) {
                    return (Update) super.setAlt(str);
                }

                public Update setFields(String str) {
                    return (Update) super.setFields(str);
                }

                public Update setKey(String str) {
                    return (Update) super.setKey(str);
                }

                public Update setOauthToken(String str) {
                    return (Update) super.setOauthToken(str);
                }

                public Update setPrettyPrint(Boolean bool) {
                    return (Update) super.setPrettyPrint(bool);
                }

                public Update setQuotaUser(String str) {
                    return (Update) super.setQuotaUser(str);
                }

                public Update setUserIp(String str) {
                    return (Update) super.setUserIp(str);
                }

                public String getUserId() {
                    return this.userId;
                }

                public Update setUserId(String str) {
                    this.userId = str;
                    return this;
                }

                public String getSessionId() {
                    return this.sessionId;
                }

                public Update setSessionId(String str) {
                    this.sessionId = str;
                    return this;
                }

                public Long getCurrentTimeMillis() {
                    return this.currentTimeMillis;
                }

                public Update setCurrentTimeMillis(Long l) {
                    this.currentTimeMillis = l;
                    return this;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T>
                 arg types: [java.lang.String, java.lang.Object]
                 candidates:
                  com.google.api.services.fitness.Fitness.Users.Sessions.Update.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.Fitness.Users.Sessions.Update.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.Fitness.Users.Sessions.Update.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.Fitness.Users.Sessions.Update.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.Fitness$Users$Sessions$Update
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest<T>
                  com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
                  com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
                  com.google.api.services.fitness.FitnessRequest.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.FitnessRequest<T> */
                public Update set(String str, Object obj) {
                    return (Update) super.set(str, obj);
                }
            }
        }
    }

    public static final class Builder extends AbstractGoogleJsonClient.Builder {
        public Builder(HttpTransport httpTransport, JsonFactory jsonFactory, HttpRequestInitializer httpRequestInitializer) {
            super(httpTransport, jsonFactory, Fitness.DEFAULT_ROOT_URL, Fitness.DEFAULT_SERVICE_PATH, httpRequestInitializer, false);
        }

        public Fitness build() {
            return new Fitness(this);
        }

        public Builder setRootUrl(String str) {
            return (Builder) super.setRootUrl(str);
        }

        public Builder setServicePath(String str) {
            return (Builder) super.setServicePath(str);
        }

        public Builder setHttpRequestInitializer(HttpRequestInitializer httpRequestInitializer) {
            return (Builder) super.setHttpRequestInitializer(httpRequestInitializer);
        }

        public Builder setApplicationName(String str) {
            return (Builder) super.setApplicationName(str);
        }

        public Builder setSuppressPatternChecks(boolean z) {
            return (Builder) super.setSuppressPatternChecks(z);
        }

        public Builder setSuppressRequiredParameterChecks(boolean z) {
            return (Builder) super.setSuppressRequiredParameterChecks(z);
        }

        public Builder setSuppressAllChecks(boolean z) {
            return (Builder) super.setSuppressAllChecks(z);
        }

        public Builder setFitnessRequestInitializer(FitnessRequestInitializer fitnessRequestInitializer) {
            return (Builder) super.setGoogleClientRequestInitializer((GoogleClientRequestInitializer) fitnessRequestInitializer);
        }

        public Builder setGoogleClientRequestInitializer(GoogleClientRequestInitializer googleClientRequestInitializer) {
            return (Builder) super.setGoogleClientRequestInitializer(googleClientRequestInitializer);
        }
    }
}
