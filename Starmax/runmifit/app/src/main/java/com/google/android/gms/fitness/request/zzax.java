package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzcr;

public final class zzax extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzax> CREATOR = new zzay();
    private final zzcq zzgj;
    private final PendingIntent zzhg;
    private final int zzig;

    zzax(PendingIntent pendingIntent, IBinder iBinder, int i) {
        this.zzhg = pendingIntent;
        this.zzgj = iBinder == null ? null : zzcr.zzj(iBinder);
        this.zzig = i;
    }

    public zzax(PendingIntent pendingIntent, zzcq zzcq, int i) {
        this.zzhg = pendingIntent;
        this.zzgj = zzcq;
        this.zzig = i;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof zzax) {
                zzax zzax = (zzax) obj;
                if (this.zzig == zzax.zzig && Objects.equal(this.zzhg, zzax.zzhg)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzhg, Integer.valueOf(this.zzig));
    }

    public final String toString() {
        return Objects.toStringHelper(this).add(BaseGmsClient.KEY_PENDING_INTENT, this.zzhg).add("sessionRegistrationOption", Integer.valueOf(this.zzig)).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, this.zzhg, i, false);
        zzcq zzcq = this.zzgj;
        SafeParcelWriter.writeIBinder(parcel, 2, zzcq == null ? null : zzcq.asBinder(), false);
        SafeParcelWriter.writeInt(parcel, 4, this.zzig);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
