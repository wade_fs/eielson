package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzcr;
import java.util.Collections;
import java.util.List;

public class StartBleScanRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<StartBleScanRequest> CREATOR = new zzbg();
    private final List<DataType> zzah;
    private final zzcq zzgj;
    private final zzae zzii;
    private final int zzij;

    public static class Builder {
        /* access modifiers changed from: private */
        public DataType[] zzhd = new DataType[0];
        /* access modifiers changed from: private */
        public zzae zzii;
        /* access modifiers changed from: private */
        public int zzij = 10;

        public StartBleScanRequest build() {
            Preconditions.checkState(this.zzii != null, "Must set BleScanCallback");
            return new StartBleScanRequest(this);
        }

        public Builder setBleScanCallback(BleScanCallback bleScanCallback) {
            this.zzii = zzd.zzt().zza(bleScanCallback);
            return this;
        }

        public Builder setDataTypes(DataType... dataTypeArr) {
            this.zzhd = dataTypeArr;
            return this;
        }

        public Builder setTimeoutSecs(int i) {
            boolean z = true;
            Preconditions.checkArgument(i > 0, "Stop time must be greater than zero");
            if (i > 60) {
                z = false;
            }
            Preconditions.checkArgument(z, "Stop time must be less than 1 minute");
            this.zzij = i;
            return this;
        }
    }

    private StartBleScanRequest(Builder builder) {
        this(ArrayUtils.toArrayList(builder.zzhd), builder.zzii, builder.zzij, (zzcq) null);
    }

    public StartBleScanRequest(StartBleScanRequest startBleScanRequest, zzcq zzcq) {
        this(startBleScanRequest.zzah, startBleScanRequest.zzii, startBleScanRequest.zzij, zzcq);
    }

    StartBleScanRequest(List<DataType> list, IBinder iBinder, int i, IBinder iBinder2) {
        zzae zzae;
        this.zzah = list;
        if (iBinder == null) {
            zzae = null;
        } else {
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.request.IBleScanCallback");
            zzae = queryLocalInterface instanceof zzae ? (zzae) queryLocalInterface : new zzag(iBinder);
        }
        this.zzii = zzae;
        this.zzij = i;
        this.zzgj = zzcr.zzj(iBinder2);
    }

    public StartBleScanRequest(List<DataType> list, zzae zzae, int i, zzcq zzcq) {
        this.zzah = list;
        this.zzii = zzae;
        this.zzij = i;
        this.zzgj = zzcq;
    }

    public List<DataType> getDataTypes() {
        return Collections.unmodifiableList(this.zzah);
    }

    public int getTimeoutSecs() {
        return this.zzij;
    }

    public String toString() {
        return Objects.toStringHelper(this).add("dataTypes", this.zzah).add("timeoutSecs", Integer.valueOf(this.zzij)).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, getDataTypes(), false);
        SafeParcelWriter.writeIBinder(parcel, 2, this.zzii.asBinder(), false);
        SafeParcelWriter.writeInt(parcel, 3, getTimeoutSecs());
        zzcq zzcq = this.zzgj;
        SafeParcelWriter.writeIBinder(parcel, 4, zzcq == null ? null : zzcq.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
