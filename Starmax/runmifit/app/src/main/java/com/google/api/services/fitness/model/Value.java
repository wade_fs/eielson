package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;
import java.util.List;

public final class Value extends GenericJson {
    @Key
    private Double fpVal;
    @Key
    private Integer intVal;
    @Key
    private List<ValueMapValEntry> mapVal;
    @Key
    private String stringVal;

    public Double getFpVal() {
        return this.fpVal;
    }

    public Value setFpVal(Double d) {
        this.fpVal = d;
        return this;
    }

    public Integer getIntVal() {
        return this.intVal;
    }

    public Value setIntVal(Integer num) {
        this.intVal = num;
        return this;
    }

    public List<ValueMapValEntry> getMapVal() {
        return this.mapVal;
    }

    public Value setMapVal(List<ValueMapValEntry> list) {
        this.mapVal = list;
        return this;
    }

    public String getStringVal() {
        return this.stringVal;
    }

    public Value setStringVal(String str) {
        this.stringVal = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.Value.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.Value.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.Value
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public Value set(String str, Object obj) {
        return (Value) super.set(str, obj);
    }

    public Value clone() {
        return (Value) super.clone();
    }
}
