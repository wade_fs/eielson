package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class Device extends GenericJson {
    @Key
    private String manufacturer;
    @Key
    private String model;
    @Key
    private String type;
    @Key
    private String uid;
    @Key
    private String version;

    public String getManufacturer() {
        return this.manufacturer;
    }

    public Device setManufacturer(String str) {
        this.manufacturer = str;
        return this;
    }

    public String getModel() {
        return this.model;
    }

    public Device setModel(String str) {
        this.model = str;
        return this;
    }

    public String getType() {
        return this.type;
    }

    public Device setType(String str) {
        this.type = str;
        return this;
    }

    public String getUid() {
        return this.uid;
    }

    public Device setUid(String str) {
        this.uid = str;
        return this;
    }

    public String getVersion() {
        return this.version;
    }

    public Device setVersion(String str) {
        this.version = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.Device.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.Device.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.Device
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public Device set(String str, Object obj) {
        return (Device) super.set(str, obj);
    }

    public Device clone() {
        return (Device) super.clone();
    }
}
