package com.google.android.gms.fitness.request;

import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.fitness.data.BleDevice;

final class zzb implements ListenerHolder.Notifier<BleScanCallback> {
    private final /* synthetic */ BleDevice zzgf;

    zzb(zza zza, BleDevice bleDevice) {
        this.zzgf = bleDevice;
    }

    public final /* synthetic */ void notifyListener(Object obj) {
        ((BleScanCallback) obj).onDeviceFound(this.zzgf);
    }

    public final void onNotifyListenerFailed() {
    }
}
