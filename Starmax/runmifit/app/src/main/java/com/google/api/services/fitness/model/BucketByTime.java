package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonString;
import com.google.api.client.util.Key;

public final class BucketByTime extends GenericJson {
    @JsonString
    @Key
    private Long durationMillis;
    @Key
    private BucketByTimePeriod period;

    public Long getDurationMillis() {
        return this.durationMillis;
    }

    public BucketByTime setDurationMillis(Long l) {
        this.durationMillis = l;
        return this;
    }

    public BucketByTimePeriod getPeriod() {
        return this.period;
    }

    public BucketByTime setPeriod(BucketByTimePeriod bucketByTimePeriod) {
        this.period = bucketByTimePeriod;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.BucketByTime.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.BucketByTime.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.BucketByTime
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public BucketByTime set(String str, Object obj) {
        return (BucketByTime) super.set(str, obj);
    }

    public BucketByTime clone() {
        return (BucketByTime) super.clone();
    }
}
