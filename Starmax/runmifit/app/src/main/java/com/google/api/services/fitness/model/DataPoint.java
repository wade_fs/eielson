package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonString;
import com.google.api.client.util.Key;
import java.util.List;

public final class DataPoint extends GenericJson {
    @JsonString
    @Key
    private Long computationTimeMillis;
    @Key
    private String dataTypeName;
    @JsonString
    @Key
    private Long endTimeNanos;
    @JsonString
    @Key
    private Long modifiedTimeMillis;
    @Key
    private String originDataSourceId;
    @JsonString
    @Key
    private Long rawTimestampNanos;
    @JsonString
    @Key
    private Long startTimeNanos;
    @Key
    private List<Value> value;

    public Long getComputationTimeMillis() {
        return this.computationTimeMillis;
    }

    public DataPoint setComputationTimeMillis(Long l) {
        this.computationTimeMillis = l;
        return this;
    }

    public String getDataTypeName() {
        return this.dataTypeName;
    }

    public DataPoint setDataTypeName(String str) {
        this.dataTypeName = str;
        return this;
    }

    public Long getEndTimeNanos() {
        return this.endTimeNanos;
    }

    public DataPoint setEndTimeNanos(Long l) {
        this.endTimeNanos = l;
        return this;
    }

    public Long getModifiedTimeMillis() {
        return this.modifiedTimeMillis;
    }

    public DataPoint setModifiedTimeMillis(Long l) {
        this.modifiedTimeMillis = l;
        return this;
    }

    public String getOriginDataSourceId() {
        return this.originDataSourceId;
    }

    public DataPoint setOriginDataSourceId(String str) {
        this.originDataSourceId = str;
        return this;
    }

    public Long getRawTimestampNanos() {
        return this.rawTimestampNanos;
    }

    public DataPoint setRawTimestampNanos(Long l) {
        this.rawTimestampNanos = l;
        return this;
    }

    public Long getStartTimeNanos() {
        return this.startTimeNanos;
    }

    public DataPoint setStartTimeNanos(Long l) {
        this.startTimeNanos = l;
        return this;
    }

    public List<Value> getValue() {
        return this.value;
    }

    public DataPoint setValue(List<Value> list) {
        this.value = list;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.DataPoint.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.DataPoint.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.DataPoint
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public DataPoint set(String str, Object obj) {
        return (DataPoint) super.set(str, obj);
    }

    public DataPoint clone() {
        return (DataPoint) super.clone();
    }
}
