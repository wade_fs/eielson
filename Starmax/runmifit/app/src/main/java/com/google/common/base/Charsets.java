package com.google.common.base;

import com.runmifit.android.util.ChangeCharset;
import java.nio.charset.Charset;

public final class Charsets {
    public static final Charset ISO_8859_1 = Charset.forName(ChangeCharset.ISO_8859_1);
    public static final Charset US_ASCII = Charset.forName(ChangeCharset.US_ASCII);
    public static final Charset UTF_16 = Charset.forName(ChangeCharset.UTF_16);
    public static final Charset UTF_16BE = Charset.forName(ChangeCharset.UTF_16BE);
    public static final Charset UTF_16LE = Charset.forName(ChangeCharset.UTF_16LE);
    public static final Charset UTF_8 = Charset.forName("UTF-8");

    private Charsets() {
    }
}
