package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class AggregateBy extends GenericJson {
    @Key
    private String dataSourceId;
    @Key
    private String dataTypeName;

    public String getDataSourceId() {
        return this.dataSourceId;
    }

    public AggregateBy setDataSourceId(String str) {
        this.dataSourceId = str;
        return this;
    }

    public String getDataTypeName() {
        return this.dataTypeName;
    }

    public AggregateBy setDataTypeName(String str) {
        this.dataTypeName = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.AggregateBy.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.AggregateBy.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.AggregateBy
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public AggregateBy set(String str, Object obj) {
        return (AggregateBy) super.set(str, obj);
    }

    public AggregateBy clone() {
        return (AggregateBy) super.clone();
    }
}
