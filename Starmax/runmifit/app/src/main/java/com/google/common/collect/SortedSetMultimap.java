package com.google.common.collect;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.SortedSet;
import javax.annotation.Nullable;

public interface SortedSetMultimap<K, V> extends SetMultimap<K, V> {
    Map<K, Collection<V>> asMap();

    SortedSet<V> get(@Nullable Object obj);

    SortedSet<V> removeAll(@Nullable Object obj);

    SortedSet<V> replaceValues(Object obj, Iterable iterable);

    Comparator<? super V> valueComparator();
}
