package com.google.android.gms.internal.auth;

import com.google.android.gms.auth.api.proxy.ProxyResponse;
import com.google.android.gms.common.api.Result;

final class zzbp extends zzbg {
    private final /* synthetic */ zzbo zzed;

    zzbp(zzbo zzbo) {
        this.zzed = zzbo;
    }

    public final void zzd(ProxyResponse proxyResponse) {
        this.zzed.setResult((Result) new zzbq(proxyResponse));
    }
}
