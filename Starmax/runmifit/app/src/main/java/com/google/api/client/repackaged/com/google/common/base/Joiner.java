package com.google.api.client.repackaged.com.google.common.base;

import java.io.IOException;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.CheckReturnValue;
import javax.annotation.Nullable;

public class Joiner {
    /* access modifiers changed from: private */
    public final String separator;

    /* renamed from: on */
    public static Joiner m5240on(String str) {
        return new Joiner(str);
    }

    /* renamed from: on */
    public static Joiner m5239on(char c) {
        return new Joiner(String.valueOf(c));
    }

    private Joiner(String str) {
        this.separator = (String) Preconditions.checkNotNull(str);
    }

    private Joiner(Joiner joiner) {
        this.separator = joiner.separator;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.util.Iterator<?>):A
     arg types: [A, java.util.Iterator<?>]
     candidates:
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.util.Iterator<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.util.Iterator<?>):A */
    public <A extends Appendable> A appendTo(A a, Iterable<?> iterable) throws IOException {
        return appendTo((Appendable) a, iterable.iterator());
    }

    public <A extends Appendable> A appendTo(A a, Iterator<?> it) throws IOException {
        Preconditions.checkNotNull(a);
        if (it.hasNext()) {
            a.append(toString(it.next()));
            while (it.hasNext()) {
                a.append(this.separator);
                a.append(toString(it.next()));
            }
        }
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
     arg types: [A, java.util.List]
     candidates:
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.util.Iterator<?>):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.util.Iterator<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A */
    public final <A extends Appendable> A appendTo(A a, Object[] objArr) throws IOException {
        return appendTo((Appendable) a, (Iterable<?>) Arrays.asList(objArr));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
     arg types: [A, java.lang.Iterable<java.lang.Object>]
     candidates:
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.util.Iterator<?>):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.util.Iterator<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A */
    public final <A extends Appendable> A appendTo(A a, @Nullable Object obj, @Nullable Object obj2, Object... objArr) throws IOException {
        return appendTo((Appendable) a, (Iterable<?>) iterable(obj, obj2, objArr));
    }

    public final StringBuilder appendTo(StringBuilder sb, Iterable<?> iterable) {
        return appendTo(sb, iterable.iterator());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.util.Iterator<?>):A
     arg types: [java.lang.StringBuilder, java.util.Iterator<?>]
     candidates:
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.util.Iterator<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.util.Iterator<?>):A */
    public final StringBuilder appendTo(StringBuilder sb, Iterator<?> it) {
        try {
            appendTo((Appendable) sb, it);
            return sb;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public final StringBuilder appendTo(StringBuilder sb, Object[] objArr) {
        return appendTo(sb, (Iterable<?>) Arrays.asList(objArr));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
     arg types: [java.lang.StringBuilder, java.lang.Iterable<java.lang.Object>]
     candidates:
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.util.Iterator<?>):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.util.Iterator<?>):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.api.client.repackaged.com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder */
    public final StringBuilder appendTo(StringBuilder sb, @Nullable Object obj, @Nullable Object obj2, Object... objArr) {
        return appendTo(sb, (Iterable<?>) iterable(obj, obj2, objArr));
    }

    public final String join(Iterable<?> iterable) {
        return join(iterable.iterator());
    }

    public final String join(Iterator<?> it) {
        return appendTo(new StringBuilder(), it).toString();
    }

    public final String join(Object[] objArr) {
        return join(Arrays.asList(objArr));
    }

    public final String join(@Nullable Object obj, @Nullable Object obj2, Object... objArr) {
        return join(iterable(obj, obj2, objArr));
    }

    @CheckReturnValue
    public Joiner useForNull(final String str) {
        Preconditions.checkNotNull(str);
        return new Joiner(this) {
            /* class com.google.api.client.repackaged.com.google.common.base.Joiner.C14621 */

            /* access modifiers changed from: package-private */
            public CharSequence toString(@Nullable Object obj) {
                return obj == null ? str : Joiner.this.toString(obj);
            }

            public Joiner useForNull(String str) {
                throw new UnsupportedOperationException("already specified useForNull");
            }

            public Joiner skipNulls() {
                throw new UnsupportedOperationException("already specified useForNull");
            }
        };
    }

    @CheckReturnValue
    public Joiner skipNulls() {
        return new Joiner(this) {
            /* class com.google.api.client.repackaged.com.google.common.base.Joiner.C14632 */

            public <A extends Appendable> A appendTo(A a, Iterator<?> it) throws IOException {
                Preconditions.checkNotNull(a, "appendable");
                Preconditions.checkNotNull(it, "parts");
                while (true) {
                    if (it.hasNext()) {
                        Object next = it.next();
                        if (next != null) {
                            a.append(Joiner.this.toString(next));
                            break;
                        }
                    } else {
                        break;
                    }
                }
                while (it.hasNext()) {
                    Object next2 = it.next();
                    if (next2 != null) {
                        a.append(Joiner.this.separator);
                        a.append(Joiner.this.toString(next2));
                    }
                }
                return a;
            }

            public Joiner useForNull(String str) {
                throw new UnsupportedOperationException("already specified skipNulls");
            }

            public MapJoiner withKeyValueSeparator(String str) {
                throw new UnsupportedOperationException("can't use .skipNulls() with maps");
            }
        };
    }

    @CheckReturnValue
    public MapJoiner withKeyValueSeparator(String str) {
        return new MapJoiner(str);
    }

    public static final class MapJoiner {
        private final Joiner joiner;
        private final String keyValueSeparator;

        private MapJoiner(Joiner joiner2, String str) {
            this.joiner = joiner2;
            this.keyValueSeparator = (String) Preconditions.checkNotNull(str);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):A
         arg types: [A, java.util.Set<java.util.Map$Entry<?, ?>>]
         candidates:
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Map<?, ?>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Map<?, ?>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):A */
        public <A extends Appendable> A appendTo(A a, Map<?, ?> map) throws IOException {
            return appendTo((Appendable) a, (Iterable<? extends Map.Entry<?, ?>>) map.entrySet());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder
         arg types: [java.lang.StringBuilder, java.util.Set<java.util.Map$Entry<?, ?>>]
         candidates:
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Map<?, ?>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Map<?, ?>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder */
        public StringBuilder appendTo(StringBuilder sb, Map<?, ?> map) {
            return appendTo(sb, (Iterable<? extends Map.Entry<?, ?>>) map.entrySet());
        }

        public String join(Map<?, ?> map) {
            return join(map.entrySet());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):A
         arg types: [A, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>]
         candidates:
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Map<?, ?>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Map<?, ?>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):A */
        public <A extends Appendable> A appendTo(A a, Iterable<? extends Map.Entry<?, ?>> iterable) throws IOException {
            return appendTo((Appendable) a, iterable.iterator());
        }

        public <A extends Appendable> A appendTo(A a, Iterator<? extends Map.Entry<?, ?>> it) throws IOException {
            Preconditions.checkNotNull(a);
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                a.append(this.joiner.toString(entry.getKey()));
                a.append(this.keyValueSeparator);
                a.append(this.joiner.toString(entry.getValue()));
                while (it.hasNext()) {
                    a.append(this.joiner.separator);
                    Map.Entry entry2 = (Map.Entry) it.next();
                    a.append(this.joiner.toString(entry2.getKey()));
                    a.append(this.keyValueSeparator);
                    a.append(this.joiner.toString(entry2.getValue()));
                }
            }
            return a;
        }

        public StringBuilder appendTo(StringBuilder sb, Iterable<? extends Map.Entry<?, ?>> iterable) {
            return appendTo(sb, iterable.iterator());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):A
         arg types: [java.lang.StringBuilder, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>]
         candidates:
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Map<?, ?>):A
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Map<?, ?>):java.lang.StringBuilder
          com.google.api.client.repackaged.com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Iterator<? extends java.util.Map$Entry<?, ?>>):A */
        public StringBuilder appendTo(StringBuilder sb, Iterator<? extends Map.Entry<?, ?>> it) {
            try {
                appendTo((Appendable) sb, it);
                return sb;
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }

        public String join(Iterable<? extends Map.Entry<?, ?>> iterable) {
            return join(iterable.iterator());
        }

        public String join(Iterator<? extends Map.Entry<?, ?>> it) {
            return appendTo(new StringBuilder(), it).toString();
        }

        @CheckReturnValue
        public MapJoiner useForNull(String str) {
            return new MapJoiner(this.joiner.useForNull(str), this.keyValueSeparator);
        }
    }

    /* access modifiers changed from: package-private */
    public CharSequence toString(Object obj) {
        Preconditions.checkNotNull(obj);
        return obj instanceof CharSequence ? (CharSequence) obj : obj.toString();
    }

    private static Iterable<Object> iterable(final Object obj, final Object obj2, final Object[] objArr) {
        Preconditions.checkNotNull(objArr);
        return new AbstractList<Object>() {
            /* class com.google.api.client.repackaged.com.google.common.base.Joiner.C14643 */

            public int size() {
                return objArr.length + 2;
            }

            public Object get(int i) {
                if (i == 0) {
                    return obj;
                }
                if (i != 1) {
                    return objArr[i - 2];
                }
                return obj2;
            }
        };
    }
}
