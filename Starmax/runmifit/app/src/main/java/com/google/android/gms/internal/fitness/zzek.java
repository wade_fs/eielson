package com.google.android.gms.internal.fitness;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.request.zzbd;

final class zzek extends zzbd {
    private final /* synthetic */ PendingIntent zzfv;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzek(zzee zzee, GoogleApiClient googleApiClient, PendingIntent pendingIntent) {
        super(googleApiClient);
        this.zzfv = pendingIntent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzbd.<init>(android.app.PendingIntent, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [android.app.PendingIntent, com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zzbd.<init>(android.app.PendingIntent, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzbd.<init>(android.app.PendingIntent, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcf) ((zzay) anyClient).getService()).zza(new zzbd(this.zzfv, (zzcq) new zzen(this)));
    }
}
