package com.google.api.client.auth.oauth2;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.util.Joiner;
import com.google.api.client.util.Key;
import com.google.api.client.util.Preconditions;
import com.tencent.connect.common.Constants;
import java.util.Collection;

public class AuthorizationRequestUrl extends GenericUrl {
    @Key(Constants.PARAM_CLIENT_ID)
    private String clientId;
    @Key("redirect_uri")
    private String redirectUri;
    @Key("response_type")
    private String responseTypes;
    @Key(Constants.PARAM_SCOPE)
    private String scopes;
    @Key
    private String state;

    public AuthorizationRequestUrl(String str, String str2, Collection<String> collection) {
        super(str);
        Preconditions.checkArgument(getFragment() == null);
        setClientId(str2);
        setResponseTypes(collection);
    }

    public final String getResponseTypes() {
        return this.responseTypes;
    }

    public AuthorizationRequestUrl setResponseTypes(Collection<String> collection) {
        this.responseTypes = Joiner.m5291on(' ').join(collection);
        return this;
    }

    public final String getRedirectUri() {
        return this.redirectUri;
    }

    public AuthorizationRequestUrl setRedirectUri(String str) {
        this.redirectUri = str;
        return this;
    }

    public final String getScopes() {
        return this.scopes;
    }

    public AuthorizationRequestUrl setScopes(Collection<String> collection) {
        this.scopes = (collection == null || !collection.iterator().hasNext()) ? null : Joiner.m5291on(' ').join(collection);
        return this;
    }

    public final String getClientId() {
        return this.clientId;
    }

    public AuthorizationRequestUrl setClientId(String str) {
        this.clientId = (String) Preconditions.checkNotNull(str);
        return this;
    }

    public final String getState() {
        return this.state;
    }

    public AuthorizationRequestUrl setState(String str) {
        this.state = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.http.GenericUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.AuthorizationRequestUrl
      com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.http.GenericUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.http.GenericUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl */
    public AuthorizationRequestUrl set(String str, Object obj) {
        return (AuthorizationRequestUrl) super.set(str, obj);
    }

    public AuthorizationRequestUrl clone() {
        return (AuthorizationRequestUrl) super.clone();
    }
}
