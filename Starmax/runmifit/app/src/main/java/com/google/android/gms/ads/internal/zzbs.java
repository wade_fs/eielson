package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzch;
import com.google.android.gms.internal.ads.zzci;
import java.util.concurrent.Callable;

final class zzbs implements Callable<zzci> {
    private final /* synthetic */ zzbp zzaba;

    zzbs(zzbp zzbp) {
        this.zzaba = zzbp;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzch.zza(java.lang.String, android.content.Context, boolean):com.google.android.gms.internal.ads.zzch
     arg types: [java.lang.String, android.content.Context, int]
     candidates:
      com.google.android.gms.internal.ads.zzch.zza(com.google.android.gms.internal.ads.zzcz, com.google.android.gms.internal.ads.zzba, com.google.android.gms.internal.ads.zzax):java.util.List<java.util.concurrent.Callable<java.lang.Void>>
      com.google.android.gms.internal.ads.zzcg.zza(com.google.android.gms.internal.ads.zzcz, android.view.MotionEvent, android.util.DisplayMetrics):com.google.android.gms.internal.ads.zzdf
      com.google.android.gms.internal.ads.zzcg.zza(android.content.Context, android.view.View, android.app.Activity):com.google.android.gms.internal.ads.zzba
      com.google.android.gms.internal.ads.zzcg.zza(com.google.android.gms.internal.ads.zzcz, com.google.android.gms.internal.ads.zzba, com.google.android.gms.internal.ads.zzax):java.util.List<java.util.concurrent.Callable<java.lang.Void>>
      com.google.android.gms.internal.ads.zzcf.zza(android.content.Context, android.view.View, android.app.Activity):com.google.android.gms.internal.ads.zzba
      com.google.android.gms.internal.ads.zzcf.zza(android.content.Context, java.lang.String, android.view.View):java.lang.String
      com.google.android.gms.internal.ads.zzcf.zza(int, int, int):void
      com.google.android.gms.internal.ads.zzce.zza(android.content.Context, java.lang.String, android.view.View):java.lang.String
      com.google.android.gms.internal.ads.zzce.zza(int, int, int):void
      com.google.android.gms.internal.ads.zzch.zza(java.lang.String, android.content.Context, boolean):com.google.android.gms.internal.ads.zzch */
    public final /* synthetic */ Object call() throws Exception {
        return new zzci(zzch.zza(this.zzaba.zzyf.zzcw, this.zzaba.mContext, false));
    }
}
