package com.google.android.gms.auth.api.accounttransfer;

import android.os.RemoteException;
import com.google.android.gms.auth.api.accounttransfer.AccountTransferClient;
import com.google.android.gms.internal.auth.zzac;
import com.google.android.gms.internal.auth.zzak;

final class zzl extends AccountTransferClient.zzf {
    private final /* synthetic */ zzak zzbe;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzl(AccountTransferClient accountTransferClient, zzak zzak) {
        super(null);
        this.zzbe = zzak;
    }

    /* access modifiers changed from: protected */
    public final void zzd(zzac zzac) throws RemoteException {
        zzac.zzd(this.zzbi, this.zzbe);
    }
}
