package com.google.android.gms.internal.auth;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;

final class zzp extends zzq {
    private final /* synthetic */ zzo zzai;

    zzp(zzo zzo) {
        this.zzai = zzo;
    }

    public final void zzd(boolean z) {
        this.zzai.setResult((Result) new zzt(z ? Status.RESULT_SUCCESS : zzk.zzaf));
    }
}
