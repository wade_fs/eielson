package com.google.android.gms.fitness.request;

import android.os.Looper;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.ListenerHolders;
import java.util.HashMap;
import java.util.Map;

public final class zzd {
    private static final zzd zzgg = new zzd();
    private final Map<ListenerHolder.ListenerKey<BleScanCallback>, zza> zzgh = new HashMap();

    private zzd() {
    }

    private static ListenerHolder<BleScanCallback> zzc(BleScanCallback bleScanCallback) {
        return ListenerHolders.createListenerHolder(bleScanCallback, Looper.getMainLooper(), BleScanCallback.class.getSimpleName());
    }

    public static zzd zzt() {
        return zzgg;
    }

    public final zza zza(ListenerHolder<BleScanCallback> listenerHolder) {
        zza zza;
        synchronized (this.zzgh) {
            zza = this.zzgh.get(listenerHolder.getListenerKey());
            if (zza == null) {
                zza = new zza(listenerHolder, null);
                this.zzgh.put(listenerHolder.getListenerKey(), zza);
            }
        }
        return zza;
    }

    public final zza zza(BleScanCallback bleScanCallback) {
        return zza(zzc(bleScanCallback));
    }

    public final zza zzb(ListenerHolder<BleScanCallback> listenerHolder) {
        zza zza;
        synchronized (this.zzgh) {
            zza = this.zzgh.get(listenerHolder.getListenerKey());
            if (zza != null) {
                zza.release();
            }
        }
        return zza;
    }

    public final zza zzb(BleScanCallback bleScanCallback) {
        return zzb(zzc(bleScanCallback));
    }
}
