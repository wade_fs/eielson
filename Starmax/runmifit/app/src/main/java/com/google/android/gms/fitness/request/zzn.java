package com.google.android.gms.fitness.request;

import android.os.Parcelable;

public final class zzn implements Parcelable.Creator<DataReadRequest> {
    /* JADX WARN: Type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r26) {
        /*
            r25 = this;
            r0 = r26
            int r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.validateObjectHeader(r26)
            r2 = 0
            r4 = 0
            r5 = 0
            r9 = r2
            r11 = r9
            r16 = r11
            r7 = r5
            r8 = r7
            r13 = r8
            r14 = r13
            r18 = r14
            r22 = r18
            r23 = r22
            r24 = r23
            r15 = 0
            r19 = 0
            r20 = 0
            r21 = 0
        L_0x0021:
            int r2 = r26.dataPosition()
            if (r2 >= r1) goto L_0x0091
            int r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readHeader(r26)
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.getFieldId(r2)
            switch(r3) {
                case 1: goto L_0x008a;
                case 2: goto L_0x0083;
                case 3: goto L_0x007e;
                case 4: goto L_0x0079;
                case 5: goto L_0x0072;
                case 6: goto L_0x006b;
                case 7: goto L_0x0066;
                case 8: goto L_0x0061;
                case 9: goto L_0x0056;
                case 10: goto L_0x0051;
                case 11: goto L_0x0032;
                case 12: goto L_0x004c;
                case 13: goto L_0x0047;
                case 14: goto L_0x0042;
                case 15: goto L_0x0032;
                case 16: goto L_0x003b;
                case 17: goto L_0x0036;
                default: goto L_0x0032;
            }
        L_0x0032:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.skipUnknownField(r0, r2)
            goto L_0x0021
        L_0x0036:
            java.util.ArrayList r24 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createIntegerList(r0, r2)
            goto L_0x0021
        L_0x003b:
            android.os.Parcelable$Creator<com.google.android.gms.fitness.data.Device> r3 = com.google.android.gms.fitness.data.Device.CREATOR
            java.util.ArrayList r23 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createTypedList(r0, r2, r3)
            goto L_0x0021
        L_0x0042:
            android.os.IBinder r22 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readIBinder(r0, r2)
            goto L_0x0021
        L_0x0047:
            boolean r21 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readBoolean(r0, r2)
            goto L_0x0021
        L_0x004c:
            boolean r20 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readBoolean(r0, r2)
            goto L_0x0021
        L_0x0051:
            int r19 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readInt(r0, r2)
            goto L_0x0021
        L_0x0056:
            android.os.Parcelable$Creator<com.google.android.gms.fitness.data.DataSource> r3 = com.google.android.gms.fitness.data.DataSource.CREATOR
            android.os.Parcelable r2 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createParcelable(r0, r2, r3)
            r18 = r2
            com.google.android.gms.fitness.data.DataSource r18 = (com.google.android.gms.fitness.data.DataSource) r18
            goto L_0x0021
        L_0x0061:
            long r16 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readLong(r0, r2)
            goto L_0x0021
        L_0x0066:
            int r15 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readInt(r0, r2)
            goto L_0x0021
        L_0x006b:
            android.os.Parcelable$Creator<com.google.android.gms.fitness.data.DataSource> r3 = com.google.android.gms.fitness.data.DataSource.CREATOR
            java.util.ArrayList r14 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createTypedList(r0, r2, r3)
            goto L_0x0021
        L_0x0072:
            android.os.Parcelable$Creator<com.google.android.gms.fitness.data.DataType> r3 = com.google.android.gms.fitness.data.DataType.CREATOR
            java.util.ArrayList r13 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createTypedList(r0, r2, r3)
            goto L_0x0021
        L_0x0079:
            long r11 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readLong(r0, r2)
            goto L_0x0021
        L_0x007e:
            long r9 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.readLong(r0, r2)
            goto L_0x0021
        L_0x0083:
            android.os.Parcelable$Creator<com.google.android.gms.fitness.data.DataSource> r3 = com.google.android.gms.fitness.data.DataSource.CREATOR
            java.util.ArrayList r8 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createTypedList(r0, r2, r3)
            goto L_0x0021
        L_0x008a:
            android.os.Parcelable$Creator<com.google.android.gms.fitness.data.DataType> r3 = com.google.android.gms.fitness.data.DataType.CREATOR
            java.util.ArrayList r7 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.createTypedList(r0, r2, r3)
            goto L_0x0021
        L_0x0091:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.ensureAtEnd(r0, r1)
            com.google.android.gms.fitness.request.DataReadRequest r0 = new com.google.android.gms.fitness.request.DataReadRequest
            r6 = r0
            r6.<init>(r7, r8, r9, r11, r13, r14, r15, r16, r18, r19, r20, r21, r22, r23, r24)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.fitness.request.zzn.createFromParcel(android.os.Parcel):java.lang.Object");
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new DataReadRequest[i];
    }
}
