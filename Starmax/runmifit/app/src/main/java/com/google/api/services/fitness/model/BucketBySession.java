package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonString;
import com.google.api.client.util.Key;

public final class BucketBySession extends GenericJson {
    @JsonString
    @Key
    private Long minDurationMillis;

    public Long getMinDurationMillis() {
        return this.minDurationMillis;
    }

    public BucketBySession setMinDurationMillis(Long l) {
        this.minDurationMillis = l;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.BucketBySession.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.BucketBySession.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.BucketBySession
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public BucketBySession set(String str, Object obj) {
        return (BucketBySession) super.set(str, obj);
    }

    public BucketBySession clone() {
        return (BucketBySession) super.clone();
    }
}
