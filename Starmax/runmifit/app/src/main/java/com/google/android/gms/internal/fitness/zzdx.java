package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.zzbn;

final class zzdx extends zzar {
    private final /* synthetic */ DataType zzfm;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdx(zzdt zzdt, GoogleApiClient googleApiClient, DataType dataType) {
        super(googleApiClient);
        this.zzfm = dataType;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzbn.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.fitness.data.DataSource, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [com.google.android.gms.fitness.data.DataType, ?[OBJECT, ARRAY], com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zzbn.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.fitness.data.DataSource, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzbn.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.fitness.data.DataSource, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcb) ((zzam) anyClient).getService()).zza(new zzbn(this.zzfm, (DataSource) null, (zzcq) new zzen(this)));
    }
}
