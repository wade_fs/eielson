package com.google.android.gms.dynamite;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamite.IDynamiteLoader;
import com.google.android.gms.dynamite.IDynamiteLoaderV2;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public final class DynamiteModule {
    public static final VersionPolicy PREFER_HIGHEST_OR_LOCAL_VERSION = new zzd();
    public static final VersionPolicy PREFER_HIGHEST_OR_LOCAL_VERSION_NO_FORCE_STAGING = new zze();
    public static final VersionPolicy PREFER_HIGHEST_OR_REMOTE_VERSION = new zzf();
    public static final VersionPolicy PREFER_HIGHEST_OR_REMOTE_VERSION_NO_FORCE_STAGING = new zzg();
    public static final VersionPolicy PREFER_LOCAL = new zzc();
    public static final VersionPolicy PREFER_REMOTE = new zzb();
    private static Boolean zzabr;
    private static IDynamiteLoader zzabs;
    private static IDynamiteLoaderV2 zzabt;
    private static String zzabu;
    private static final ThreadLocal<zza> zzabv = new ThreadLocal<>();
    private static final VersionPolicy.IVersions zzabw = new zza();
    private final Context zzabx;

    public static class DynamiteLoaderClassLoader {
        public static ClassLoader sClassLoader;
    }

    public static class LoadingException extends Exception {
        private LoadingException(String str) {
            super(str);
        }

        /* synthetic */ LoadingException(String str, zza zza) {
            this(str);
        }

        private LoadingException(String str, Throwable th) {
            super(str, th);
        }

        /* synthetic */ LoadingException(String str, Throwable th, zza zza) {
            this(str, th);
        }
    }

    public interface VersionPolicy {

        public interface IVersions {
            int getLocalVersion(Context context, String str);

            int getRemoteVersion(Context context, String str, boolean z) throws LoadingException;
        }

        public static class SelectionResult {
            public int localVersion = 0;
            public int remoteVersion = 0;
            public int selection = 0;
        }

        SelectionResult selectModule(Context context, String str, IVersions iVersions) throws LoadingException;
    }

    private static class zza {
        public Cursor zzaby;

        private zza() {
        }

        /* synthetic */ zza(zza zza) {
            this();
        }
    }

    private static class zzb implements VersionPolicy.IVersions {
        private final int zzabz;
        private final int zzaca = 0;

        public zzb(int i, int i2) {
            this.zzabz = i;
        }

        public final int getLocalVersion(Context context, String str) {
            return this.zzabz;
        }

        public final int getRemoteVersion(Context context, String str, boolean z) {
            return 0;
        }
    }

    private DynamiteModule(Context context) {
        this.zzabx = (Context) Preconditions.checkNotNull(context);
    }

    public static int getLocalVersion(Context context, String str) {
        try {
            ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 61);
            sb.append("com.google.android.gms.dynamite.descriptors.");
            sb.append(str);
            sb.append(".ModuleDescriptor");
            Class<?> loadClass = classLoader.loadClass(sb.toString());
            Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (declaredField.get(null).equals(str)) {
                return declaredField2.getInt(null);
            }
            String valueOf = String.valueOf(declaredField.get(null));
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 51 + String.valueOf(str).length());
            sb2.append("Module descriptor id '");
            sb2.append(valueOf);
            sb2.append("' didn't match expected id '");
            sb2.append(str);
            sb2.append("'");
            Log.e("DynamiteModule", sb2.toString());
            return 0;
        } catch (ClassNotFoundException unused) {
            StringBuilder sb3 = new StringBuilder(String.valueOf(str).length() + 45);
            sb3.append("Local module descriptor class for ");
            sb3.append(str);
            sb3.append(" not found.");
            Log.w("DynamiteModule", sb3.toString());
            return 0;
        } catch (Exception e) {
            String valueOf2 = String.valueOf(e.getMessage());
            Log.e("DynamiteModule", valueOf2.length() != 0 ? "Failed to load module descriptor class: ".concat(valueOf2) : new String("Failed to load module descriptor class: "));
            return 0;
        }
    }

    public static Uri getQueryUri(String str, boolean z) {
        String str2 = z ? ProviderConstants.API_PATH_FORCE_STAGING : ProviderConstants.API_PATH;
        StringBuilder sb = new StringBuilder(str2.length() + 42 + String.valueOf(str).length());
        sb.append("content://com.google.android.gms.chimera/");
        sb.append(str2);
        sb.append("/");
        sb.append(str);
        return Uri.parse(sb.toString());
    }

    public static int getRemoteVersion(Context context, String str) {
        return getRemoteVersion(context, str, false);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:39|40) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:15|16|17|18) */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r2.set(null, java.lang.ClassLoader.getSystemClassLoader());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0085, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0035 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x007c */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00be A[SYNTHETIC, Splitter:B:55:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00e5  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0050=Splitter:B:22:0x0050, B:17:0x0035=Splitter:B:17:0x0035, B:34:0x0079=Splitter:B:34:0x0079} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int getRemoteVersion(android.content.Context r8, java.lang.String r9, boolean r10) {
        /*
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r0 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r0)
            java.lang.Boolean r1 = com.google.android.gms.dynamite.DynamiteModule.zzabr     // Catch:{ all -> 0x00ea }
            if (r1 != 0) goto L_0x00b7
            android.content.Context r1 = r8.getApplicationContext()     // Catch:{ ClassNotFoundException -> 0x008e, IllegalAccessException -> 0x008c, NoSuchFieldException -> 0x008a }
            java.lang.ClassLoader r1 = r1.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x008e, IllegalAccessException -> 0x008c, NoSuchFieldException -> 0x008a }
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule$DynamiteLoaderClassLoader> r2 = com.google.android.gms.dynamite.DynamiteModule.DynamiteLoaderClassLoader.class
            java.lang.String r2 = r2.getName()     // Catch:{ ClassNotFoundException -> 0x008e, IllegalAccessException -> 0x008c, NoSuchFieldException -> 0x008a }
            java.lang.Class r1 = r1.loadClass(r2)     // Catch:{ ClassNotFoundException -> 0x008e, IllegalAccessException -> 0x008c, NoSuchFieldException -> 0x008a }
            java.lang.String r2 = "sClassLoader"
            java.lang.reflect.Field r2 = r1.getDeclaredField(r2)     // Catch:{ ClassNotFoundException -> 0x008e, IllegalAccessException -> 0x008c, NoSuchFieldException -> 0x008a }
            monitor-enter(r1)     // Catch:{ ClassNotFoundException -> 0x008e, IllegalAccessException -> 0x008c, NoSuchFieldException -> 0x008a }
            r3 = 0
            java.lang.Object r4 = r2.get(r3)     // Catch:{ all -> 0x0087 }
            java.lang.ClassLoader r4 = (java.lang.ClassLoader) r4     // Catch:{ all -> 0x0087 }
            if (r4 == 0) goto L_0x0038
            java.lang.ClassLoader r2 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x0087 }
            if (r4 != r2) goto L_0x0032
        L_0x002f:
            java.lang.Boolean r2 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x0087 }
            goto L_0x0084
        L_0x0032:
            zza(r4)     // Catch:{ LoadingException -> 0x0035 }
        L_0x0035:
            java.lang.Boolean r2 = java.lang.Boolean.TRUE     // Catch:{ all -> 0x0087 }
            goto L_0x0084
        L_0x0038:
            java.lang.String r4 = "com.google.android.gms"
            android.content.Context r5 = r8.getApplicationContext()     // Catch:{ all -> 0x0087 }
            java.lang.String r5 = r5.getPackageName()     // Catch:{ all -> 0x0087 }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x0087 }
            if (r4 == 0) goto L_0x0050
            java.lang.ClassLoader r4 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x0087 }
            r2.set(r3, r4)     // Catch:{ all -> 0x0087 }
            goto L_0x002f
        L_0x0050:
            int r4 = zzb(r8, r9, r10)     // Catch:{ LoadingException -> 0x007c }
            java.lang.String r5 = com.google.android.gms.dynamite.DynamiteModule.zzabu     // Catch:{ LoadingException -> 0x007c }
            if (r5 == 0) goto L_0x0079
            java.lang.String r5 = com.google.android.gms.dynamite.DynamiteModule.zzabu     // Catch:{ LoadingException -> 0x007c }
            boolean r5 = r5.isEmpty()     // Catch:{ LoadingException -> 0x007c }
            if (r5 == 0) goto L_0x0061
            goto L_0x0079
        L_0x0061:
            com.google.android.gms.dynamite.zzh r5 = new com.google.android.gms.dynamite.zzh     // Catch:{ LoadingException -> 0x007c }
            java.lang.String r6 = com.google.android.gms.dynamite.DynamiteModule.zzabu     // Catch:{ LoadingException -> 0x007c }
            java.lang.ClassLoader r7 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ LoadingException -> 0x007c }
            r5.<init>(r6, r7)     // Catch:{ LoadingException -> 0x007c }
            zza(r5)     // Catch:{ LoadingException -> 0x007c }
            r2.set(r3, r5)     // Catch:{ LoadingException -> 0x007c }
            java.lang.Boolean r5 = java.lang.Boolean.TRUE     // Catch:{ LoadingException -> 0x007c }
            com.google.android.gms.dynamite.DynamiteModule.zzabr = r5     // Catch:{ LoadingException -> 0x007c }
            monitor-exit(r1)     // Catch:{ all -> 0x0087 }
            monitor-exit(r0)     // Catch:{ all -> 0x00ea }
            return r4
        L_0x0079:
            monitor-exit(r1)     // Catch:{ all -> 0x0087 }
            monitor-exit(r0)     // Catch:{ all -> 0x00ea }
            return r4
        L_0x007c:
            java.lang.ClassLoader r4 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x0087 }
            r2.set(r3, r4)     // Catch:{ all -> 0x0087 }
            goto L_0x002f
        L_0x0084:
            monitor-exit(r1)     // Catch:{ all -> 0x0087 }
            r1 = r2
            goto L_0x00b5
        L_0x0087:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0087 }
            throw r2     // Catch:{ ClassNotFoundException -> 0x008e, IllegalAccessException -> 0x008c, NoSuchFieldException -> 0x008a }
        L_0x008a:
            r1 = move-exception
            goto L_0x008f
        L_0x008c:
            r1 = move-exception
            goto L_0x008f
        L_0x008e:
            r1 = move-exception
        L_0x008f:
            java.lang.String r2 = "DynamiteModule"
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x00ea }
            int r3 = r3.length()     // Catch:{ all -> 0x00ea }
            int r3 = r3 + 30
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            r4.<init>(r3)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = "Failed to load module via V2: "
            r4.append(r3)     // Catch:{ all -> 0x00ea }
            r4.append(r1)     // Catch:{ all -> 0x00ea }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x00ea }
            android.util.Log.w(r2, r1)     // Catch:{ all -> 0x00ea }
            java.lang.Boolean r1 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x00ea }
        L_0x00b5:
            com.google.android.gms.dynamite.DynamiteModule.zzabr = r1     // Catch:{ all -> 0x00ea }
        L_0x00b7:
            monitor-exit(r0)     // Catch:{ all -> 0x00ea }
            boolean r0 = r1.booleanValue()
            if (r0 == 0) goto L_0x00e5
            int r8 = zzb(r8, r9, r10)     // Catch:{ LoadingException -> 0x00c3 }
            return r8
        L_0x00c3:
            r8 = move-exception
            java.lang.String r9 = "Failed to retrieve remote module version: "
            java.lang.String r8 = r8.getMessage()
            java.lang.String r8 = java.lang.String.valueOf(r8)
            int r10 = r8.length()
            if (r10 == 0) goto L_0x00d9
            java.lang.String r8 = r9.concat(r8)
            goto L_0x00de
        L_0x00d9:
            java.lang.String r8 = new java.lang.String
            r8.<init>(r9)
        L_0x00de:
            java.lang.String r9 = "DynamiteModule"
            android.util.Log.w(r9, r8)
            r8 = 0
            return r8
        L_0x00e5:
            int r8 = zza(r8, r9, r10)
            return r8
        L_0x00ea:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00ea }
            goto L_0x00ee
        L_0x00ed:
            throw r8
        L_0x00ee:
            goto L_0x00ed
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.getRemoteVersion(android.content.Context, java.lang.String, boolean):int");
    }

    public static synchronized Boolean getUseV2ForTesting() {
        Boolean bool;
        synchronized (DynamiteModule.class) {
            bool = zzabr;
        }
        return bool;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x007c, code lost:
        if (r3.zzaby != null) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0095, code lost:
        if (r3.zzaby != null) goto L_0x007e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d1, code lost:
        if (r3.zzaby != null) goto L_0x007e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.dynamite.DynamiteModule load(android.content.Context r10, com.google.android.gms.dynamite.DynamiteModule.VersionPolicy r11, java.lang.String r12) throws com.google.android.gms.dynamite.DynamiteModule.LoadingException {
        /*
            java.lang.String r0 = ":"
            java.lang.String r1 = "DynamiteModule"
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r2 = com.google.android.gms.dynamite.DynamiteModule.zzabv
            java.lang.Object r2 = r2.get()
            com.google.android.gms.dynamite.DynamiteModule$zza r2 = (com.google.android.gms.dynamite.DynamiteModule.zza) r2
            com.google.android.gms.dynamite.DynamiteModule$zza r3 = new com.google.android.gms.dynamite.DynamiteModule$zza
            r4 = 0
            r3.<init>(r4)
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r5 = com.google.android.gms.dynamite.DynamiteModule.zzabv
            r5.set(r3)
            com.google.android.gms.dynamite.DynamiteModule$VersionPolicy$IVersions r5 = com.google.android.gms.dynamite.DynamiteModule.zzabw     // Catch:{ all -> 0x0121 }
            com.google.android.gms.dynamite.DynamiteModule$VersionPolicy$SelectionResult r5 = r11.selectModule(r10, r12, r5)     // Catch:{ all -> 0x0121 }
            int r6 = r5.localVersion     // Catch:{ all -> 0x0121 }
            int r7 = r5.remoteVersion     // Catch:{ all -> 0x0121 }
            java.lang.String r8 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0121 }
            int r8 = r8.length()     // Catch:{ all -> 0x0121 }
            int r8 = r8 + 68
            java.lang.String r9 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0121 }
            int r9 = r9.length()     // Catch:{ all -> 0x0121 }
            int r8 = r8 + r9
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0121 }
            r9.<init>(r8)     // Catch:{ all -> 0x0121 }
            java.lang.String r8 = "Considering local module "
            r9.append(r8)     // Catch:{ all -> 0x0121 }
            r9.append(r12)     // Catch:{ all -> 0x0121 }
            r9.append(r0)     // Catch:{ all -> 0x0121 }
            r9.append(r6)     // Catch:{ all -> 0x0121 }
            java.lang.String r6 = " and remote module "
            r9.append(r6)     // Catch:{ all -> 0x0121 }
            r9.append(r12)     // Catch:{ all -> 0x0121 }
            r9.append(r0)     // Catch:{ all -> 0x0121 }
            r9.append(r7)     // Catch:{ all -> 0x0121 }
            java.lang.String r0 = r9.toString()     // Catch:{ all -> 0x0121 }
            android.util.Log.i(r1, r0)     // Catch:{ all -> 0x0121 }
            int r0 = r5.selection     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x00f7
            int r0 = r5.selection     // Catch:{ all -> 0x0121 }
            r6 = -1
            if (r0 != r6) goto L_0x0069
            int r0 = r5.localVersion     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x00f7
        L_0x0069:
            int r0 = r5.selection     // Catch:{ all -> 0x0121 }
            r7 = 1
            if (r0 != r7) goto L_0x0072
            int r0 = r5.remoteVersion     // Catch:{ all -> 0x0121 }
            if (r0 == 0) goto L_0x00f7
        L_0x0072:
            int r0 = r5.selection     // Catch:{ all -> 0x0121 }
            if (r0 != r6) goto L_0x0089
            com.google.android.gms.dynamite.DynamiteModule r10 = zzd(r10, r12)     // Catch:{ all -> 0x0121 }
            android.database.Cursor r11 = r3.zzaby
            if (r11 == 0) goto L_0x0083
        L_0x007e:
            android.database.Cursor r11 = r3.zzaby
            r11.close()
        L_0x0083:
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r11 = com.google.android.gms.dynamite.DynamiteModule.zzabv
            r11.set(r2)
            return r10
        L_0x0089:
            int r0 = r5.selection     // Catch:{ all -> 0x0121 }
            if (r0 != r7) goto L_0x00dc
            int r0 = r5.remoteVersion     // Catch:{ LoadingException -> 0x0098 }
            com.google.android.gms.dynamite.DynamiteModule r10 = zza(r10, r12, r0)     // Catch:{ LoadingException -> 0x0098 }
            android.database.Cursor r11 = r3.zzaby
            if (r11 == 0) goto L_0x0083
            goto L_0x007e
        L_0x0098:
            r0 = move-exception
            java.lang.String r7 = "Failed to load remote module: "
            java.lang.String r8 = r0.getMessage()     // Catch:{ all -> 0x0121 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0121 }
            int r9 = r8.length()     // Catch:{ all -> 0x0121 }
            if (r9 == 0) goto L_0x00ae
            java.lang.String r7 = r7.concat(r8)     // Catch:{ all -> 0x0121 }
            goto L_0x00b4
        L_0x00ae:
            java.lang.String r8 = new java.lang.String     // Catch:{ all -> 0x0121 }
            r8.<init>(r7)     // Catch:{ all -> 0x0121 }
            r7 = r8
        L_0x00b4:
            android.util.Log.w(r1, r7)     // Catch:{ all -> 0x0121 }
            int r1 = r5.localVersion     // Catch:{ all -> 0x0121 }
            if (r1 == 0) goto L_0x00d4
            com.google.android.gms.dynamite.DynamiteModule$zzb r1 = new com.google.android.gms.dynamite.DynamiteModule$zzb     // Catch:{ all -> 0x0121 }
            int r5 = r5.localVersion     // Catch:{ all -> 0x0121 }
            r7 = 0
            r1.<init>(r5, r7)     // Catch:{ all -> 0x0121 }
            com.google.android.gms.dynamite.DynamiteModule$VersionPolicy$SelectionResult r11 = r11.selectModule(r10, r12, r1)     // Catch:{ all -> 0x0121 }
            int r11 = r11.selection     // Catch:{ all -> 0x0121 }
            if (r11 != r6) goto L_0x00d4
            com.google.android.gms.dynamite.DynamiteModule r10 = zzd(r10, r12)     // Catch:{ all -> 0x0121 }
            android.database.Cursor r11 = r3.zzaby
            if (r11 == 0) goto L_0x0083
            goto L_0x007e
        L_0x00d4:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r10 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch:{ all -> 0x0121 }
            java.lang.String r11 = "Remote load failed. No local fallback found."
            r10.<init>(r11, r0, r4)     // Catch:{ all -> 0x0121 }
            throw r10     // Catch:{ all -> 0x0121 }
        L_0x00dc:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r10 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch:{ all -> 0x0121 }
            int r11 = r5.selection     // Catch:{ all -> 0x0121 }
            r12 = 47
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0121 }
            r0.<init>(r12)     // Catch:{ all -> 0x0121 }
            java.lang.String r12 = "VersionPolicy returned invalid code:"
            r0.append(r12)     // Catch:{ all -> 0x0121 }
            r0.append(r11)     // Catch:{ all -> 0x0121 }
            java.lang.String r11 = r0.toString()     // Catch:{ all -> 0x0121 }
            r10.<init>(r11, r4)     // Catch:{ all -> 0x0121 }
            throw r10     // Catch:{ all -> 0x0121 }
        L_0x00f7:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r10 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch:{ all -> 0x0121 }
            int r11 = r5.localVersion     // Catch:{ all -> 0x0121 }
            int r12 = r5.remoteVersion     // Catch:{ all -> 0x0121 }
            r0 = 91
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0121 }
            r1.<init>(r0)     // Catch:{ all -> 0x0121 }
            java.lang.String r0 = "No acceptable module found. Local version is "
            r1.append(r0)     // Catch:{ all -> 0x0121 }
            r1.append(r11)     // Catch:{ all -> 0x0121 }
            java.lang.String r11 = " and remote version is "
            r1.append(r11)     // Catch:{ all -> 0x0121 }
            r1.append(r12)     // Catch:{ all -> 0x0121 }
            java.lang.String r11 = "."
            r1.append(r11)     // Catch:{ all -> 0x0121 }
            java.lang.String r11 = r1.toString()     // Catch:{ all -> 0x0121 }
            r10.<init>(r11, r4)     // Catch:{ all -> 0x0121 }
            throw r10     // Catch:{ all -> 0x0121 }
        L_0x0121:
            r10 = move-exception
            android.database.Cursor r11 = r3.zzaby
            if (r11 == 0) goto L_0x012b
            android.database.Cursor r11 = r3.zzaby
            r11.close()
        L_0x012b:
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r11 = com.google.android.gms.dynamite.DynamiteModule.zzabv
            r11.set(r2)
            goto L_0x0132
        L_0x0131:
            throw r10
        L_0x0132:
            goto L_0x0131
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.load(android.content.Context, com.google.android.gms.dynamite.DynamiteModule$VersionPolicy, java.lang.String):com.google.android.gms.dynamite.DynamiteModule");
    }

    public static Cursor queryForDynamiteModule(Context context, String str, boolean z) {
        return context.getContentResolver().query(getQueryUri(str, z), null, null, null, null);
    }

    public static synchronized void resetInternalStateForTesting() {
        synchronized (DynamiteModule.class) {
            zzabs = null;
            zzabt = null;
            zzabu = null;
            zzabr = null;
            synchronized (DynamiteLoaderClassLoader.class) {
                DynamiteLoaderClassLoader.sClassLoader = null;
            }
        }
    }

    public static synchronized void setUseV2ForTesting(Boolean bool) {
        synchronized (DynamiteModule.class) {
            zzabr = bool;
        }
    }

    private static int zza(Context context, String str, boolean z) {
        IDynamiteLoader zzg = zzg(context);
        if (zzg == null) {
            return 0;
        }
        try {
            return zzg.getModuleVersion2(ObjectWrapper.wrap(context), str, z);
        } catch (RemoteException e) {
            String valueOf = String.valueOf(e.getMessage());
            Log.w("DynamiteModule", valueOf.length() != 0 ? "Failed to retrieve remote module version: ".concat(valueOf) : new String("Failed to retrieve remote module version: "));
            return 0;
        }
    }

    private static Context zza(Context context, String str, int i, Cursor cursor, IDynamiteLoaderV2 iDynamiteLoaderV2) {
        try {
            return (Context) ObjectWrapper.unwrap(iDynamiteLoaderV2.loadModule2(ObjectWrapper.wrap(context), str, i, ObjectWrapper.wrap(cursor)));
        } catch (Exception e) {
            String valueOf = String.valueOf(e.toString());
            Log.e("DynamiteModule", valueOf.length() != 0 ? "Failed to load DynamiteLoader: ".concat(valueOf) : new String("Failed to load DynamiteLoader: "));
            return null;
        }
    }

    private static DynamiteModule zza(Context context, String str, int i) throws LoadingException {
        Boolean bool;
        synchronized (DynamiteModule.class) {
            bool = zzabr;
        }
        if (bool != null) {
            return bool.booleanValue() ? zzc(context, str, i) : zzb(context, str, i);
        }
        throw new LoadingException("Failed to determine which loading route to use.", (zza) null);
    }

    private static void zza(ClassLoader classLoader) throws LoadingException {
        try {
            zzabt = IDynamiteLoaderV2.Stub.asInterface((IBinder) classLoader.loadClass("com.google.android.gms.dynamiteloader.DynamiteLoaderV2").getConstructor(new Class[0]).newInstance(new Object[0]));
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new LoadingException("Failed to instantiate dynamite loader", e, null);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int zzb(android.content.Context r2, java.lang.String r3, boolean r4) throws com.google.android.gms.dynamite.DynamiteModule.LoadingException {
        /*
            r0 = 0
            android.database.Cursor r2 = queryForDynamiteModule(r2, r3, r4)     // Catch:{ Exception -> 0x004e, all -> 0x004b }
            if (r2 == 0) goto L_0x003a
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x0049 }
            if (r3 == 0) goto L_0x003a
            r3 = 0
            int r3 = r2.getInt(r3)     // Catch:{ Exception -> 0x0049 }
            if (r3 <= 0) goto L_0x0034
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r4 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r4)     // Catch:{ Exception -> 0x0049 }
            r1 = 2
            java.lang.String r1 = r2.getString(r1)     // Catch:{ all -> 0x0031 }
            com.google.android.gms.dynamite.DynamiteModule.zzabu = r1     // Catch:{ all -> 0x0031 }
            monitor-exit(r4)     // Catch:{ all -> 0x0031 }
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$zza> r4 = com.google.android.gms.dynamite.DynamiteModule.zzabv     // Catch:{ Exception -> 0x0049 }
            java.lang.Object r4 = r4.get()     // Catch:{ Exception -> 0x0049 }
            com.google.android.gms.dynamite.DynamiteModule$zza r4 = (com.google.android.gms.dynamite.DynamiteModule.zza) r4     // Catch:{ Exception -> 0x0049 }
            if (r4 == 0) goto L_0x0034
            android.database.Cursor r1 = r4.zzaby     // Catch:{ Exception -> 0x0049 }
            if (r1 != 0) goto L_0x0034
            r4.zzaby = r2     // Catch:{ Exception -> 0x0049 }
            r2 = r0
            goto L_0x0034
        L_0x0031:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0031 }
            throw r3     // Catch:{ Exception -> 0x0049 }
        L_0x0034:
            if (r2 == 0) goto L_0x0039
            r2.close()
        L_0x0039:
            return r3
        L_0x003a:
            java.lang.String r3 = "DynamiteModule"
            java.lang.String r4 = "Failed to retrieve remote module version."
            android.util.Log.w(r3, r4)     // Catch:{ Exception -> 0x0049 }
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r3 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch:{ Exception -> 0x0049 }
            java.lang.String r4 = "Failed to connect to dynamite module ContentResolver."
            r3.<init>(r4, r0)     // Catch:{ Exception -> 0x0049 }
            throw r3     // Catch:{ Exception -> 0x0049 }
        L_0x0049:
            r3 = move-exception
            goto L_0x0050
        L_0x004b:
            r3 = move-exception
            r2 = r0
            goto L_0x005e
        L_0x004e:
            r3 = move-exception
            r2 = r0
        L_0x0050:
            boolean r4 = r3 instanceof com.google.android.gms.dynamite.DynamiteModule.LoadingException     // Catch:{ all -> 0x005d }
            if (r4 == 0) goto L_0x0055
            throw r3     // Catch:{ all -> 0x005d }
        L_0x0055:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r4 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch:{ all -> 0x005d }
            java.lang.String r1 = "V2 version check failed"
            r4.<init>(r1, r3, r0)     // Catch:{ all -> 0x005d }
            throw r4     // Catch:{ all -> 0x005d }
        L_0x005d:
            r3 = move-exception
        L_0x005e:
            if (r2 == 0) goto L_0x0063
            r2.close()
        L_0x0063:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.zzb(android.content.Context, java.lang.String, boolean):int");
    }

    private static DynamiteModule zzb(Context context, String str, int i) throws LoadingException {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51);
        sb.append("Selected remote version of ");
        sb.append(str);
        sb.append(", version >= ");
        sb.append(i);
        Log.i("DynamiteModule", sb.toString());
        IDynamiteLoader zzg = zzg(context);
        if (zzg != null) {
            try {
                IObjectWrapper createModuleContext = zzg.createModuleContext(ObjectWrapper.wrap(context), str, i);
                if (ObjectWrapper.unwrap(createModuleContext) != null) {
                    return new DynamiteModule((Context) ObjectWrapper.unwrap(createModuleContext));
                }
                throw new LoadingException("Failed to load remote module.", (zza) null);
            } catch (RemoteException e) {
                throw new LoadingException("Failed to load remote module.", e, null);
            }
        } else {
            throw new LoadingException("Failed to create IDynamiteLoader.", (zza) null);
        }
    }

    private static DynamiteModule zzc(Context context, String str, int i) throws LoadingException {
        IDynamiteLoaderV2 iDynamiteLoaderV2;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51);
        sb.append("Selected remote version of ");
        sb.append(str);
        sb.append(", version >= ");
        sb.append(i);
        Log.i("DynamiteModule", sb.toString());
        synchronized (DynamiteModule.class) {
            iDynamiteLoaderV2 = zzabt;
        }
        if (iDynamiteLoaderV2 != null) {
            zza zza2 = zzabv.get();
            if (zza2 == null || zza2.zzaby == null) {
                throw new LoadingException("No result cursor", (zza) null);
            }
            Context zza3 = zza(context.getApplicationContext(), str, i, zza2.zzaby, iDynamiteLoaderV2);
            if (zza3 != null) {
                return new DynamiteModule(zza3);
            }
            throw new LoadingException("Failed to get module context", (zza) null);
        }
        throw new LoadingException("DynamiteLoaderV2 was not cached.", (zza) null);
    }

    private static DynamiteModule zzd(Context context, String str) {
        String valueOf = String.valueOf(str);
        Log.i("DynamiteModule", valueOf.length() != 0 ? "Selected local version of ".concat(valueOf) : new String("Selected local version of "));
        return new DynamiteModule(context.getApplicationContext());
    }

    private static IDynamiteLoader zzg(Context context) {
        synchronized (DynamiteModule.class) {
            if (zzabs != null) {
                IDynamiteLoader iDynamiteLoader = zzabs;
                return iDynamiteLoader;
            } else if (GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(context) != 0) {
                return null;
            } else {
                try {
                    IDynamiteLoader asInterface = IDynamiteLoader.Stub.asInterface((IBinder) context.createPackageContext("com.google.android.gms", 3).getClassLoader().loadClass("com.google.android.gms.chimera.container.DynamiteLoaderImpl").newInstance());
                    if (asInterface != null) {
                        zzabs = asInterface;
                        return asInterface;
                    }
                } catch (Exception e) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.e("DynamiteModule", valueOf.length() != 0 ? "Failed to load IDynamiteLoader from GmsCore: ".concat(valueOf) : new String("Failed to load IDynamiteLoader from GmsCore: "));
                }
            }
        }
        return null;
    }

    public final Context getModuleContext() {
        return this.zzabx;
    }

    public final IBinder instantiate(String str) throws LoadingException {
        try {
            return (IBinder) this.zzabx.getClassLoader().loadClass(str).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            String valueOf = String.valueOf(str);
            throw new LoadingException(valueOf.length() != 0 ? "Failed to instantiate module class: ".concat(valueOf) : new String("Failed to instantiate module class: "), e, null);
        }
    }
}
