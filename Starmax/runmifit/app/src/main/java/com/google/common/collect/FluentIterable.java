package com.google.common.collect;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import javax.annotation.CheckReturnValue;
import javax.annotation.Nullable;

public abstract class FluentIterable<E> implements Iterable<E> {
    private final Iterable<E> iterable;

    protected FluentIterable() {
        this.iterable = this;
    }

    FluentIterable(Iterable<E> iterable2) {
        this.iterable = (Iterable) Preconditions.checkNotNull(iterable2);
    }

    public static <E> FluentIterable<E> from(final Iterable<E> iterable2) {
        return iterable2 instanceof FluentIterable ? (FluentIterable) iterable2 : new FluentIterable<E>(iterable2) {
            /* class com.google.common.collect.FluentIterable.C15071 */

            public Iterator<E> iterator() {
                return iterable2.iterator();
            }
        };
    }

    @Deprecated
    public static <E> FluentIterable<E> from(FluentIterable<E> fluentIterable) {
        return (FluentIterable) Preconditions.checkNotNull(fluentIterable);
    }

    public String toString() {
        return Iterables.toString(this.iterable);
    }

    public final int size() {
        return Iterables.size(this.iterable);
    }

    public final boolean contains(@Nullable Object obj) {
        return Iterables.contains(this.iterable, obj);
    }

    @CheckReturnValue
    public final FluentIterable<E> cycle() {
        return from(Iterables.cycle(this.iterable));
    }

    @CheckReturnValue
    public final FluentIterable<E> filter(Predicate<? super E> predicate) {
        return from(Iterables.filter(this.iterable, predicate));
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Class, java.lang.Class<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @javax.annotation.CheckReturnValue
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> com.google.common.collect.FluentIterable<T> filter(java.lang.Class<T> r2) {
        /*
            r1 = this;
            java.lang.Iterable<E> r0 = r1.iterable
            java.lang.Iterable r2 = com.google.common.collect.Iterables.filter(r0, r2)
            com.google.common.collect.FluentIterable r2 = from(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.FluentIterable.filter(java.lang.Class):com.google.common.collect.FluentIterable");
    }

    public final boolean anyMatch(Predicate<? super E> predicate) {
        return Iterables.any(this.iterable, predicate);
    }

    public final boolean allMatch(Predicate<? super E> predicate) {
        return Iterables.all(this.iterable, predicate);
    }

    public final Optional<E> firstMatch(Predicate<? super E> predicate) {
        return Iterables.tryFind(this.iterable, predicate);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.common.base.Function<? super E, T>, com.google.common.base.Function] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> com.google.common.collect.FluentIterable<T> transform(com.google.common.base.Function<? super E, T> r2) {
        /*
            r1 = this;
            java.lang.Iterable<E> r0 = r1.iterable
            java.lang.Iterable r2 = com.google.common.collect.Iterables.transform(r0, r2)
            com.google.common.collect.FluentIterable r2 = from(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.FluentIterable.transform(com.google.common.base.Function):com.google.common.collect.FluentIterable");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [com.google.common.base.Function, com.google.common.base.Function<? super E, ? extends java.lang.Iterable<? extends T>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.google.common.collect.FluentIterable<T> transformAndConcat(com.google.common.base.Function<? super E, ? extends java.lang.Iterable<? extends T>> r1) {
        /*
            r0 = this;
            com.google.common.collect.FluentIterable r1 = r0.transform(r1)
            java.lang.Iterable r1 = com.google.common.collect.Iterables.concat(r1)
            com.google.common.collect.FluentIterable r1 = from(r1)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.FluentIterable.transformAndConcat(com.google.common.base.Function):com.google.common.collect.FluentIterable");
    }

    public final Optional<E> first() {
        Iterator<E> it = this.iterable.iterator();
        return it.hasNext() ? Optional.m5296of(it.next()) : Optional.absent();
    }

    public final Optional<E> last() {
        E next;
        Iterable<E> iterable2 = this.iterable;
        if (iterable2 instanceof List) {
            List list = (List) iterable2;
            if (list.isEmpty()) {
                return Optional.absent();
            }
            return Optional.m5296of(list.get(list.size() - 1));
        }
        Iterator<E> it = iterable2.iterator();
        if (!it.hasNext()) {
            return Optional.absent();
        }
        Iterable<E> iterable3 = this.iterable;
        if (iterable3 instanceof SortedSet) {
            return Optional.m5296of(((SortedSet) iterable3).last());
        }
        do {
            next = it.next();
        } while (it.hasNext());
        return Optional.m5296of(next);
    }

    @CheckReturnValue
    public final FluentIterable<E> skip(int i) {
        return from(Iterables.skip(this.iterable, i));
    }

    @CheckReturnValue
    public final FluentIterable<E> limit(int i) {
        return from(Iterables.limit(this.iterable, i));
    }

    public final boolean isEmpty() {
        return !this.iterable.iterator().hasNext();
    }

    public final ImmutableList<E> toList() {
        return ImmutableList.copyOf(this.iterable);
    }

    public final ImmutableList<E> toSortedList(Comparator<? super E> comparator) {
        return Ordering.from(comparator).immutableSortedCopy(this.iterable);
    }

    public final ImmutableSet<E> toSet() {
        return ImmutableSet.copyOf(this.iterable);
    }

    public final ImmutableSortedSet<E> toSortedSet(Comparator<? super E> comparator) {
        return ImmutableSortedSet.copyOf(comparator, this.iterable);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.common.base.Function, com.google.common.base.Function<? super E, V>] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.Maps.toMap(java.lang.Iterable, com.google.common.base.Function):com.google.common.collect.ImmutableMap<K, V>
     arg types: [java.lang.Object, ?]
     candidates:
      com.google.common.collect.Maps.toMap(java.util.Iterator, com.google.common.base.Function):com.google.common.collect.ImmutableMap<K, V>
      com.google.common.collect.Maps.toMap(java.lang.Iterable, com.google.common.base.Function):com.google.common.collect.ImmutableMap<K, V> */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <V> com.google.common.collect.ImmutableMap<E, V> toMap(com.google.common.base.Function<? super E, V> r2) {
        /*
            r1 = this;
            java.lang.Iterable<E> r0 = r1.iterable
            com.google.common.collect.ImmutableMap r2 = com.google.common.collect.Maps.toMap(r0, r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.FluentIterable.toMap(com.google.common.base.Function):com.google.common.collect.ImmutableMap");
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.common.base.Function, com.google.common.base.Function<? super E, K>] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.Multimaps.index(java.lang.Iterable, com.google.common.base.Function):com.google.common.collect.ImmutableListMultimap<K, V>
     arg types: [java.lang.Object, ?]
     candidates:
      com.google.common.collect.Multimaps.index(java.util.Iterator, com.google.common.base.Function):com.google.common.collect.ImmutableListMultimap<K, V>
      com.google.common.collect.Multimaps.index(java.lang.Iterable, com.google.common.base.Function):com.google.common.collect.ImmutableListMultimap<K, V> */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <K> com.google.common.collect.ImmutableListMultimap<K, E> index(com.google.common.base.Function<? super E, K> r2) {
        /*
            r1 = this;
            java.lang.Iterable<E> r0 = r1.iterable
            com.google.common.collect.ImmutableListMultimap r2 = com.google.common.collect.Multimaps.index(r0, r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.FluentIterable.index(com.google.common.base.Function):com.google.common.collect.ImmutableListMultimap");
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.google.common.base.Function, com.google.common.base.Function<? super E, K>] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.collect.Maps.uniqueIndex(java.lang.Iterable, com.google.common.base.Function):com.google.common.collect.ImmutableMap<K, V>
     arg types: [java.lang.Object, ?]
     candidates:
      com.google.common.collect.Maps.uniqueIndex(java.util.Iterator, com.google.common.base.Function):com.google.common.collect.ImmutableMap<K, V>
      com.google.common.collect.Maps.uniqueIndex(java.lang.Iterable, com.google.common.base.Function):com.google.common.collect.ImmutableMap<K, V> */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <K> com.google.common.collect.ImmutableMap<K, E> uniqueIndex(com.google.common.base.Function<? super E, K> r2) {
        /*
            r1 = this;
            java.lang.Iterable<E> r0 = r1.iterable
            com.google.common.collect.ImmutableMap r2 = com.google.common.collect.Maps.uniqueIndex(r0, r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.FluentIterable.uniqueIndex(com.google.common.base.Function):com.google.common.collect.ImmutableMap");
    }

    public final E[] toArray(Class<E> cls) {
        return Iterables.toArray(this.iterable, cls);
    }

    public final <C extends Collection<? super E>> C copyInto(C c) {
        Preconditions.checkNotNull(c);
        Iterable<E> iterable2 = this.iterable;
        if (iterable2 instanceof Collection) {
            c.addAll(Collections2.cast(iterable2));
        } else {
            for (E e : iterable2) {
                c.add(e);
            }
        }
        return c;
    }

    public final E get(int i) {
        return Iterables.get(this.iterable, i);
    }

    private static class FromIterableFunction<E> implements Function<Iterable<E>, FluentIterable<E>> {
        private FromIterableFunction() {
        }

        public FluentIterable<E> apply(Iterable<E> iterable) {
            return FluentIterable.from(iterable);
        }
    }
}
