package com.google.android.gms.internal.fitness;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.request.zzax;

final class zzej extends zzbd {
    private final /* synthetic */ PendingIntent zzfv;
    private final /* synthetic */ int zzgb = 0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzej(zzee zzee, GoogleApiClient googleApiClient, PendingIntent pendingIntent, int i) {
        super(googleApiClient);
        this.zzfv = pendingIntent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzax.<init>(android.app.PendingIntent, com.google.android.gms.internal.fitness.zzcq, int):void
     arg types: [android.app.PendingIntent, com.google.android.gms.internal.fitness.zzen, int]
     candidates:
      com.google.android.gms.fitness.request.zzax.<init>(android.app.PendingIntent, android.os.IBinder, int):void
      com.google.android.gms.fitness.request.zzax.<init>(android.app.PendingIntent, com.google.android.gms.internal.fitness.zzcq, int):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcf) ((zzay) anyClient).getService()).zza(new zzax(this.zzfv, (zzcq) new zzen(this), this.zzgb));
    }
}
