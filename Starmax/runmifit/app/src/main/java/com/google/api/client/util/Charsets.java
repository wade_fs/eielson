package com.google.api.client.util;

import com.runmifit.android.util.ChangeCharset;
import java.nio.charset.Charset;

public final class Charsets {
    public static final Charset ISO_8859_1 = Charset.forName(ChangeCharset.ISO_8859_1);
    public static final Charset UTF_8 = Charset.forName("UTF-8");

    private Charsets() {
    }
}
