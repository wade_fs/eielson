package com.google.api.client.googleapis.testing.services;

import com.google.api.client.googleapis.services.AbstractGoogleClient;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpHeaders;

public class MockGoogleClientRequest<T> extends AbstractGoogleClientRequest<T> {
    public MockGoogleClientRequest(AbstractGoogleClient abstractGoogleClient, String str, String str2, HttpContent httpContent, Class<T> cls) {
        super(abstractGoogleClient, str, str2, httpContent, cls);
    }

    public MockGoogleClientRequest<T> setDisableGZipContent(boolean z) {
        return (MockGoogleClientRequest) super.setDisableGZipContent(z);
    }

    public MockGoogleClientRequest<T> setRequestHeaders(HttpHeaders httpHeaders) {
        return (MockGoogleClientRequest) super.setRequestHeaders(httpHeaders);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T>
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.client.googleapis.testing.services.MockGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.testing.services.MockGoogleClientRequest<T>
      com.google.api.client.googleapis.testing.services.MockGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.googleapis.services.AbstractGoogleClientRequest.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.services.AbstractGoogleClientRequest<T> */
    public MockGoogleClientRequest<T> set(String str, Object obj) {
        return (MockGoogleClientRequest) super.set(str, obj);
    }
}
