package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.request.BleScanCallback;
import com.google.android.gms.fitness.request.zzbh;

final class zzcv extends zzu {
    private final /* synthetic */ BleScanCallback zzey;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzcv(zzct zzct, GoogleApiClient googleApiClient, BleScanCallback bleScanCallback) {
        super(googleApiClient);
        this.zzey = bleScanCallback;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzbt) ((zzp) anyClient).getService()).zza(new zzbh(this.zzey, new zzen(this)));
    }
}
