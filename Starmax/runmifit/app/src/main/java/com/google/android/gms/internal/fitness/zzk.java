package com.google.android.gms.internal.fitness;

import com.google.android.gms.fitness.data.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class zzk {
    private static final double zzel;
    private static final double zzem;
    private static final double zzen;
    private static final double zzeo;
    public static final Set<String> zzep = Collections.unmodifiableSet(new HashSet(Arrays.asList("altitude", "duration", "food_item", "meal_type", "repetitions", "resistance", "resistance_type", "debug_session", "google.android.fitness.SessionV2")));
    private static final zzk zzes = new zzk();
    private final Map<String, Map<String, zzm>> zzeq;
    private final Map<String, zzm> zzer;

    static {
        double nanos = (double) TimeUnit.SECONDS.toNanos(1);
        Double.isNaN(nanos);
        zzel = 10.0d / nanos;
        double nanos2 = (double) TimeUnit.SECONDS.toNanos(1);
        Double.isNaN(nanos2);
        zzem = 1000.0d / nanos2;
        double nanos3 = (double) TimeUnit.HOURS.toNanos(1);
        Double.isNaN(nanos3);
        zzen = 2000.0d / nanos3;
        double nanos4 = (double) TimeUnit.SECONDS.toNanos(1);
        Double.isNaN(nanos4);
        zzeo = 100.0d / nanos4;
    }

    private zzk() {
        HashMap hashMap = new HashMap();
        hashMap.put("latitude", new zzm(-90.0d, 90.0d));
        hashMap.put("longitude", new zzm(-180.0d, 180.0d));
        hashMap.put("accuracy", new zzm(0.0d, 10000.0d));
        hashMap.put("bpm", new zzm(0.0d, 1000.0d));
        hashMap.put("altitude", new zzm(-100000.0d, 100000.0d));
        hashMap.put("percentage", new zzm(0.0d, 100.0d));
        hashMap.put("confidence", new zzm(0.0d, 100.0d));
        hashMap.put("duration", new zzm(0.0d, 9.223372036854776E18d));
        hashMap.put("height", new zzm(0.0d, 3.0d));
        hashMap.put("weight", new zzm(0.0d, 1000.0d));
        hashMap.put("speed", new zzm(0.0d, 11000.0d));
        this.zzer = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("com.google.step_count.delta", zza("steps", new zzm(0.0d, zzel)));
        hashMap2.put("com.google.calories.consumed", zza(Field.NUTRIENT_CALORIES, new zzm(0.0d, zzem)));
        hashMap2.put("com.google.calories.expended", zza(Field.NUTRIENT_CALORIES, new zzm(0.0d, zzen)));
        hashMap2.put("com.google.distance.delta", zza("distance", new zzm(0.0d, zzeo)));
        this.zzeq = Collections.unmodifiableMap(hashMap2);
    }

    private static <K, V> Map<K, V> zza(Object obj, Object obj2) {
        HashMap hashMap = new HashMap();
        hashMap.put(obj, obj2);
        return hashMap;
    }

    public static zzk zzs() {
        return zzes;
    }

    /* access modifiers changed from: package-private */
    public final zzm zza(String str, String str2) {
        Map map = this.zzeq.get(str);
        if (map != null) {
            return (zzm) map.get(str2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final zzm zzk(String str) {
        return this.zzer.get(str);
    }
}
