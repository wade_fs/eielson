package com.google.api.services.fitness;

import com.google.api.client.googleapis.services.json.AbstractGoogleJsonClientRequest;
import com.google.api.client.googleapis.services.json.CommonGoogleJsonClientRequestInitializer;
import java.io.IOException;

public class FitnessRequestInitializer extends CommonGoogleJsonClientRequestInitializer {
    /* access modifiers changed from: protected */
    public void initializeFitnessRequest(FitnessRequest<?> fitnessRequest) throws IOException {
    }

    public FitnessRequestInitializer() {
    }

    public FitnessRequestInitializer(String str) {
        super(str);
    }

    public FitnessRequestInitializer(String str, String str2) {
        super(str, str2);
    }

    public final void initializeJsonRequest(AbstractGoogleJsonClientRequest<?> abstractGoogleJsonClientRequest) throws IOException {
        super.initializeJsonRequest(abstractGoogleJsonClientRequest);
        initializeFitnessRequest((FitnessRequest) abstractGoogleJsonClientRequest);
    }
}
