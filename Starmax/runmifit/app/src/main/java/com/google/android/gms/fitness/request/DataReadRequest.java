package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Device;
import com.google.android.gms.internal.fitness.zzbh;
import com.google.android.gms.internal.fitness.zzbi;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.apache.commons.math3.geometry.VectorFormat;

public class DataReadRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<DataReadRequest> CREATOR = new zzn();
    public static final int NO_LIMIT = 0;
    private final int limit;
    private final List<DataType> zzah;
    private final int zzak;
    private final List<DataSource> zzgm;
    private final List<DataType> zzgr;
    private final List<DataSource> zzgs;
    private final long zzgt;
    private final DataSource zzgu;
    private final boolean zzgv;
    private final boolean zzgw;
    private final zzbh zzgx;
    private final List<Device> zzgy;
    private final List<Integer> zzgz;
    private final long zzs;
    private final long zzt;

    public static class Builder {
        /* access modifiers changed from: private */
        public int limit = 0;
        /* access modifiers changed from: private */
        public List<DataType> zzah = new ArrayList();
        /* access modifiers changed from: private */
        public int zzak = 0;
        /* access modifiers changed from: private */
        public List<DataSource> zzgm = new ArrayList();
        /* access modifiers changed from: private */
        public List<DataType> zzgr = new ArrayList();
        /* access modifiers changed from: private */
        public List<DataSource> zzgs = new ArrayList();
        /* access modifiers changed from: private */
        public long zzgt = 0;
        /* access modifiers changed from: private */
        public DataSource zzgu;
        private boolean zzgv = false;
        /* access modifiers changed from: private */
        public boolean zzgw = false;
        /* access modifiers changed from: private */
        public final List<Device> zzgy = new ArrayList();
        /* access modifiers changed from: private */
        public final List<Integer> zzgz = new ArrayList();
        /* access modifiers changed from: private */
        public long zzs;
        /* access modifiers changed from: private */
        public long zzt;

        public Builder addFilteredDataQualityStandard(int i) {
            Preconditions.checkArgument(this.zzgy.isEmpty(), "Cannot add data quality standard filter when filtering by device.");
            this.zzgz.add(Integer.valueOf(i));
            return this;
        }

        public Builder aggregate(DataSource dataSource, DataType dataType) {
            Preconditions.checkNotNull(dataSource, "Attempting to add a null data source");
            Preconditions.checkState(!this.zzgm.contains(dataSource), "Cannot add the same data source for aggregated and detailed");
            DataType dataType2 = dataSource.getDataType();
            List<DataType> aggregatesForInput = DataType.getAggregatesForInput(dataType2);
            Preconditions.checkArgument(!aggregatesForInput.isEmpty(), "Unsupported input data type specified for aggregation: %s", dataType2);
            Preconditions.checkArgument(aggregatesForInput.contains(dataType), "Invalid output aggregate data type specified: %s -> %s", dataType2, dataType);
            if (!this.zzgs.contains(dataSource)) {
                this.zzgs.add(dataSource);
            }
            return this;
        }

        public Builder aggregate(DataType dataType, DataType dataType2) {
            Preconditions.checkNotNull(dataType, "Attempting to use a null data type");
            Preconditions.checkState(!this.zzah.contains(dataType), "Cannot add the same data type as aggregated and detailed");
            List<DataType> aggregatesForInput = DataType.getAggregatesForInput(dataType);
            Preconditions.checkArgument(!aggregatesForInput.isEmpty(), "Unsupported input data type specified for aggregation: %s", dataType);
            Preconditions.checkArgument(aggregatesForInput.contains(dataType2), "Invalid output aggregate data type specified: %s -> %s", dataType, dataType2);
            if (!this.zzgr.contains(dataType)) {
                this.zzgr.add(dataType);
            }
            return this;
        }

        public Builder bucketByActivitySegment(int i, TimeUnit timeUnit) {
            Preconditions.checkArgument(this.zzak == 0, "Bucketing strategy already set to %s", Integer.valueOf(this.zzak));
            Preconditions.checkArgument(i > 0, "Must specify a valid minimum duration for an activity segment: %d", Integer.valueOf(i));
            this.zzak = 4;
            this.zzgt = timeUnit.toMillis((long) i);
            return this;
        }

        public Builder bucketByActivitySegment(int i, TimeUnit timeUnit, DataSource dataSource) {
            Preconditions.checkArgument(this.zzak == 0, "Bucketing strategy already set to %s", Integer.valueOf(this.zzak));
            Preconditions.checkArgument(i > 0, "Must specify a valid minimum duration for an activity segment: %d", Integer.valueOf(i));
            Preconditions.checkArgument(dataSource != null, "Invalid activity data source specified");
            Preconditions.checkArgument(dataSource.getDataType().equals(DataType.TYPE_ACTIVITY_SEGMENT), "Invalid activity data source specified: %s", dataSource);
            this.zzgu = dataSource;
            this.zzak = 4;
            this.zzgt = timeUnit.toMillis((long) i);
            return this;
        }

        public Builder bucketByActivityType(int i, TimeUnit timeUnit) {
            Preconditions.checkArgument(this.zzak == 0, "Bucketing strategy already set to %s", Integer.valueOf(this.zzak));
            Preconditions.checkArgument(i > 0, "Must specify a valid minimum duration for an activity segment: %d", Integer.valueOf(i));
            this.zzak = 3;
            this.zzgt = timeUnit.toMillis((long) i);
            return this;
        }

        public Builder bucketByActivityType(int i, TimeUnit timeUnit, DataSource dataSource) {
            Preconditions.checkArgument(this.zzak == 0, "Bucketing strategy already set to %s", Integer.valueOf(this.zzak));
            Preconditions.checkArgument(i > 0, "Must specify a valid minimum duration for an activity segment: %d", Integer.valueOf(i));
            Preconditions.checkArgument(dataSource != null, "Invalid activity data source specified");
            Preconditions.checkArgument(dataSource.getDataType().equals(DataType.TYPE_ACTIVITY_SEGMENT), "Invalid activity data source specified: %s", dataSource);
            this.zzgu = dataSource;
            this.zzak = 3;
            this.zzgt = timeUnit.toMillis((long) i);
            return this;
        }

        public Builder bucketBySession(int i, TimeUnit timeUnit) {
            Preconditions.checkArgument(this.zzak == 0, "Bucketing strategy already set to %s", Integer.valueOf(this.zzak));
            Preconditions.checkArgument(i > 0, "Must specify a valid minimum duration for an activity segment: %d", Integer.valueOf(i));
            this.zzak = 2;
            this.zzgt = timeUnit.toMillis((long) i);
            return this;
        }

        public Builder bucketByTime(int i, TimeUnit timeUnit) {
            Preconditions.checkArgument(this.zzak == 0, "Bucketing strategy already set to %s", Integer.valueOf(this.zzak));
            Preconditions.checkArgument(i > 0, "Must specify a valid minimum duration for an activity segment: %d", Integer.valueOf(i));
            this.zzak = 1;
            this.zzgt = timeUnit.toMillis((long) i);
            return this;
        }

        public DataReadRequest build() {
            boolean z = false;
            Preconditions.checkState(!this.zzgm.isEmpty() || !this.zzah.isEmpty() || !this.zzgs.isEmpty() || !this.zzgr.isEmpty(), "Must add at least one data source (aggregated or detailed)");
            Preconditions.checkState(this.zzs > 0, "Invalid start time: %s", Long.valueOf(this.zzs));
            long j = this.zzt;
            Preconditions.checkState(j > 0 && j > this.zzs, "Invalid end time: %s", Long.valueOf(this.zzt));
            boolean z2 = this.zzgs.isEmpty() && this.zzgr.isEmpty();
            if ((z2 && this.zzak == 0) || (!z2 && this.zzak != 0)) {
                z = true;
            }
            Preconditions.checkState(z, "Must specify a valid bucketing strategy while requesting aggregation");
            return new DataReadRequest(this);
        }

        public Builder enableServerQueries() {
            this.zzgw = true;
            return this;
        }

        public Builder read(DataSource dataSource) {
            Preconditions.checkNotNull(dataSource, "Attempting to add a null data source");
            Preconditions.checkArgument(!this.zzgs.contains(dataSource), "Cannot add the same data source as aggregated and detailed");
            if (!this.zzgm.contains(dataSource)) {
                this.zzgm.add(dataSource);
            }
            return this;
        }

        public Builder read(DataType dataType) {
            Preconditions.checkNotNull(dataType, "Attempting to use a null data type");
            Preconditions.checkState(!this.zzgr.contains(dataType), "Cannot add the same data type as aggregated and detailed");
            if (!this.zzah.contains(dataType)) {
                this.zzah.add(dataType);
            }
            return this;
        }

        public Builder setLimit(int i) {
            Preconditions.checkArgument(i > 0, "Invalid limit %d is specified", Integer.valueOf(i));
            this.limit = i;
            return this;
        }

        public Builder setTimeRange(long j, long j2, TimeUnit timeUnit) {
            this.zzs = timeUnit.toMillis(j);
            this.zzt = timeUnit.toMillis(j2);
            return this;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.DataReadRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<com.google.android.gms.fitness.data.DataSource>, long, long, java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<com.google.android.gms.fitness.data.DataSource>, int, long, com.google.android.gms.fitness.data.DataSource, int, boolean, boolean, com.google.android.gms.internal.fitness.zzbh, java.util.List<com.google.android.gms.fitness.data.Device>, java.util.List<java.lang.Integer>):void
     arg types: [java.util.List, java.util.List, long, long, java.util.List, java.util.List, int, long, com.google.android.gms.fitness.data.DataSource, int, int, boolean, ?[OBJECT, ARRAY], java.util.List, java.util.List]
     candidates:
      com.google.android.gms.fitness.request.DataReadRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<com.google.android.gms.fitness.data.DataSource>, long, long, java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<com.google.android.gms.fitness.data.DataSource>, int, long, com.google.android.gms.fitness.data.DataSource, int, boolean, boolean, android.os.IBinder, java.util.List<com.google.android.gms.fitness.data.Device>, java.util.List<java.lang.Integer>):void
      com.google.android.gms.fitness.request.DataReadRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<com.google.android.gms.fitness.data.DataSource>, long, long, java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<com.google.android.gms.fitness.data.DataSource>, int, long, com.google.android.gms.fitness.data.DataSource, int, boolean, boolean, com.google.android.gms.internal.fitness.zzbh, java.util.List<com.google.android.gms.fitness.data.Device>, java.util.List<java.lang.Integer>):void */
    private DataReadRequest(Builder builder) {
        this((List<DataType>) builder.zzah, (List<DataSource>) builder.zzgm, builder.zzs, builder.zzt, (List<DataType>) builder.zzgr, (List<DataSource>) builder.zzgs, builder.zzak, builder.zzgt, builder.zzgu, builder.limit, false, builder.zzgw, (zzbh) null, (List<Device>) builder.zzgy, (List<Integer>) builder.zzgz);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public DataReadRequest(com.google.android.gms.fitness.request.DataReadRequest r22, com.google.android.gms.internal.fitness.zzbh r23) {
        /*
            r21 = this;
            r0 = r22
            r1 = r21
            r17 = r23
            java.util.List<com.google.android.gms.fitness.data.DataType> r2 = r0.zzah
            java.util.List<com.google.android.gms.fitness.data.DataSource> r3 = r0.zzgm
            long r4 = r0.zzs
            long r6 = r0.zzt
            java.util.List<com.google.android.gms.fitness.data.DataType> r8 = r0.zzgr
            java.util.List<com.google.android.gms.fitness.data.DataSource> r9 = r0.zzgs
            int r10 = r0.zzak
            long r11 = r0.zzgt
            com.google.android.gms.fitness.data.DataSource r13 = r0.zzgu
            int r14 = r0.limit
            boolean r15 = r0.zzgv
            r20 = r1
            boolean r1 = r0.zzgw
            r16 = r1
            java.util.List<com.google.android.gms.fitness.data.Device> r1 = r0.zzgy
            r18 = r1
            java.util.List<java.lang.Integer> r0 = r0.zzgz
            r19 = r0
            r1 = r20
            r1.<init>(r2, r3, r4, r6, r8, r9, r10, r11, r13, r14, r15, r16, r17, r18, r19)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.fitness.request.DataReadRequest.<init>(com.google.android.gms.fitness.request.DataReadRequest, com.google.android.gms.internal.fitness.zzbh):void");
    }

    DataReadRequest(List<DataType> list, List<DataSource> list2, long j, long j2, List<DataType> list3, List<DataSource> list4, int i, long j3, DataSource dataSource, int i2, boolean z, boolean z2, IBinder iBinder, List<Device> list5, List<Integer> list6) {
        this.zzah = list;
        this.zzgm = list2;
        this.zzs = j;
        this.zzt = j2;
        this.zzgr = list3;
        this.zzgs = list4;
        this.zzak = i;
        this.zzgt = j3;
        this.zzgu = dataSource;
        this.limit = i2;
        this.zzgv = z;
        this.zzgw = z2;
        this.zzgx = iBinder == null ? null : zzbi.zzc(iBinder);
        this.zzgy = list5 == null ? Collections.emptyList() : list5;
        this.zzgz = list6 == null ? Collections.emptyList() : list6;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    private DataReadRequest(List<DataType> list, List<DataSource> list2, long j, long j2, List<DataType> list3, List<DataSource> list4, int i, long j3, DataSource dataSource, int i2, boolean z, boolean z2, zzbh zzbh, List<Device> list5, List<Integer> list6) {
        this(list, list2, j, j2, list3, list4, i, j3, dataSource, i2, z, z2, zzbh == null ? null : zzbh.asBinder(), list5, list6);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DataReadRequest) {
                DataReadRequest dataReadRequest = (DataReadRequest) obj;
                if (this.zzah.equals(dataReadRequest.zzah) && this.zzgm.equals(dataReadRequest.zzgm) && this.zzs == dataReadRequest.zzs && this.zzt == dataReadRequest.zzt && this.zzak == dataReadRequest.zzak && this.zzgs.equals(dataReadRequest.zzgs) && this.zzgr.equals(dataReadRequest.zzgr) && Objects.equal(this.zzgu, dataReadRequest.zzgu) && this.zzgt == dataReadRequest.zzgt && this.zzgw == dataReadRequest.zzgw && this.limit == dataReadRequest.limit && this.zzgv == dataReadRequest.zzgv && Objects.equal(this.zzgx, dataReadRequest.zzgx) && Objects.equal(this.zzgy, dataReadRequest.zzgy) && Objects.equal(this.zzgz, dataReadRequest.zzgz)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public DataSource getActivityDataSource() {
        return this.zzgu;
    }

    public List<DataSource> getAggregatedDataSources() {
        return this.zzgs;
    }

    public List<DataType> getAggregatedDataTypes() {
        return this.zzgr;
    }

    public long getBucketDuration(TimeUnit timeUnit) {
        return timeUnit.convert(this.zzgt, TimeUnit.MILLISECONDS);
    }

    public int getBucketType() {
        return this.zzak;
    }

    public List<DataSource> getDataSources() {
        return this.zzgm;
    }

    public List<DataType> getDataTypes() {
        return this.zzah;
    }

    public long getEndTime(TimeUnit timeUnit) {
        return timeUnit.convert(this.zzt, TimeUnit.MILLISECONDS);
    }

    public List<Integer> getFilteredDataQualityStandards() {
        return this.zzgz;
    }

    public int getLimit() {
        return this.limit;
    }

    public long getStartTime(TimeUnit timeUnit) {
        return timeUnit.convert(this.zzs, TimeUnit.MILLISECONDS);
    }

    public int hashCode() {
        return Objects.hashCode(Integer.valueOf(this.zzak), Long.valueOf(this.zzs), Long.valueOf(this.zzt));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DataReadRequest{");
        if (!this.zzah.isEmpty()) {
            for (DataType dataType : this.zzah) {
                sb.append(dataType.zzm());
                sb.append(" ");
            }
        }
        if (!this.zzgm.isEmpty()) {
            for (DataSource dataSource : this.zzgm) {
                sb.append(dataSource.toDebugString());
                sb.append(" ");
            }
        }
        if (this.zzak != 0) {
            sb.append("bucket by ");
            sb.append(Bucket.zza(this.zzak));
            if (this.zzgt > 0) {
                sb.append(" >");
                sb.append(this.zzgt);
                sb.append("ms");
            }
            sb.append(": ");
        }
        if (!this.zzgr.isEmpty()) {
            for (DataType dataType2 : this.zzgr) {
                sb.append(dataType2.zzm());
                sb.append(" ");
            }
        }
        if (!this.zzgs.isEmpty()) {
            for (DataSource dataSource2 : this.zzgs) {
                sb.append(dataSource2.toDebugString());
                sb.append(" ");
            }
        }
        sb.append(String.format(Locale.US, "(%tF %tT - %tF %tT)", Long.valueOf(this.zzs), Long.valueOf(this.zzs), Long.valueOf(this.zzt), Long.valueOf(this.zzt)));
        if (this.zzgu != null) {
            sb.append("activities: ");
            sb.append(this.zzgu.toDebugString());
        }
        if (!this.zzgz.isEmpty()) {
            sb.append("quality: ");
            for (Integer num : this.zzgz) {
                sb.append(DataSource.zzd(num.intValue()));
                sb.append(" ");
            }
        }
        if (this.zzgw) {
            sb.append(" +server");
        }
        sb.append(VectorFormat.DEFAULT_SUFFIX);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, getDataTypes(), false);
        SafeParcelWriter.writeTypedList(parcel, 2, getDataSources(), false);
        SafeParcelWriter.writeLong(parcel, 3, this.zzs);
        SafeParcelWriter.writeLong(parcel, 4, this.zzt);
        SafeParcelWriter.writeTypedList(parcel, 5, getAggregatedDataTypes(), false);
        SafeParcelWriter.writeTypedList(parcel, 6, getAggregatedDataSources(), false);
        SafeParcelWriter.writeInt(parcel, 7, getBucketType());
        SafeParcelWriter.writeLong(parcel, 8, this.zzgt);
        SafeParcelWriter.writeParcelable(parcel, 9, getActivityDataSource(), i, false);
        SafeParcelWriter.writeInt(parcel, 10, getLimit());
        SafeParcelWriter.writeBoolean(parcel, 12, this.zzgv);
        SafeParcelWriter.writeBoolean(parcel, 13, this.zzgw);
        zzbh zzbh = this.zzgx;
        SafeParcelWriter.writeIBinder(parcel, 14, zzbh == null ? null : zzbh.asBinder(), false);
        SafeParcelWriter.writeTypedList(parcel, 16, this.zzgy, false);
        SafeParcelWriter.writeIntegerList(parcel, 17, getFilteredDataQualityStandards(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
