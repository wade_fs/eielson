package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.request.SessionInsertRequest;

final class zzeh extends zzbd {
    private final /* synthetic */ SessionInsertRequest zzfz;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzeh(zzee zzee, GoogleApiClient googleApiClient, SessionInsertRequest sessionInsertRequest) {
        super(googleApiClient);
        this.zzfz = sessionInsertRequest;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcf) ((zzay) anyClient).getService()).zza(new SessionInsertRequest(this.zzfz, new zzen(this)));
    }
}
