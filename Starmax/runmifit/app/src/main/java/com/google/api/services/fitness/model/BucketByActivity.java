package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonString;
import com.google.api.client.util.Key;

public final class BucketByActivity extends GenericJson {
    @Key
    private String activityDataSourceId;
    @JsonString
    @Key
    private Long minDurationMillis;

    public String getActivityDataSourceId() {
        return this.activityDataSourceId;
    }

    public BucketByActivity setActivityDataSourceId(String str) {
        this.activityDataSourceId = str;
        return this;
    }

    public Long getMinDurationMillis() {
        return this.minDurationMillis;
    }

    public BucketByActivity setMinDurationMillis(Long l) {
        this.minDurationMillis = l;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.BucketByActivity.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.BucketByActivity.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.BucketByActivity
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public BucketByActivity set(String str, Object obj) {
        return (BucketByActivity) super.set(str, obj);
    }

    public BucketByActivity clone() {
        return (BucketByActivity) super.clone();
    }
}
