package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

public final class Application extends GenericJson {
    @Key
    private String detailsUrl;
    @Key
    private String name;
    @Key
    private String packageName;
    @Key
    private String version;

    public String getDetailsUrl() {
        return this.detailsUrl;
    }

    public Application setDetailsUrl(String str) {
        this.detailsUrl = str;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Application setName(String str) {
        this.name = str;
        return this;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public Application setPackageName(String str) {
        this.packageName = str;
        return this;
    }

    public String getVersion() {
        return this.version;
    }

    public Application setVersion(String str) {
        this.version = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.Application.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.Application.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.Application
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public Application set(String str, Object obj) {
        return (Application) super.set(str, obj);
    }

    public Application clone() {
        return (Application) super.clone();
    }
}
