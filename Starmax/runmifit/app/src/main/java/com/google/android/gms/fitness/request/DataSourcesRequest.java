package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.internal.fitness.zzbk;
import com.google.android.gms.internal.fitness.zzbl;
import java.util.Arrays;
import java.util.List;

public class DataSourcesRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<DataSourcesRequest> CREATOR = new zzp();
    private final List<DataType> zzah;
    private final List<Integer> zzha;
    private final boolean zzhb;
    private final zzbk zzhc;

    public static class Builder {
        private boolean zzhb = false;
        /* access modifiers changed from: private */
        public DataType[] zzhd = new DataType[0];
        /* access modifiers changed from: private */
        public int[] zzhe = {0, 1};

        public DataSourcesRequest build() {
            boolean z = true;
            Preconditions.checkState(this.zzhd.length > 0, "Must add at least one data type");
            if (this.zzhe.length <= 0) {
                z = false;
            }
            Preconditions.checkState(z, "Must add at least one data source type");
            return new DataSourcesRequest(this);
        }

        public Builder setDataSourceTypes(int... iArr) {
            this.zzhe = iArr;
            return this;
        }

        public Builder setDataTypes(DataType... dataTypeArr) {
            this.zzhd = dataTypeArr;
            return this;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.DataSourcesRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<java.lang.Integer>, boolean, com.google.android.gms.internal.fitness.zzbk):void
     arg types: [java.util.ArrayList, java.util.List, int, ?[OBJECT, ARRAY]]
     candidates:
      com.google.android.gms.fitness.request.DataSourcesRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<java.lang.Integer>, boolean, android.os.IBinder):void
      com.google.android.gms.fitness.request.DataSourcesRequest.<init>(java.util.List<com.google.android.gms.fitness.data.DataType>, java.util.List<java.lang.Integer>, boolean, com.google.android.gms.internal.fitness.zzbk):void */
    private DataSourcesRequest(Builder builder) {
        this((List<DataType>) ArrayUtils.toArrayList(builder.zzhd), (List<Integer>) Arrays.asList(ArrayUtils.toWrapperArray(builder.zzhe)), false, (zzbk) null);
    }

    public DataSourcesRequest(DataSourcesRequest dataSourcesRequest, zzbk zzbk) {
        this(dataSourcesRequest.zzah, dataSourcesRequest.zzha, dataSourcesRequest.zzhb, zzbk);
    }

    DataSourcesRequest(List<DataType> list, List<Integer> list2, boolean z, IBinder iBinder) {
        this.zzah = list;
        this.zzha = list2;
        this.zzhb = z;
        this.zzhc = zzbl.zzd(iBinder);
    }

    private DataSourcesRequest(List<DataType> list, List<Integer> list2, boolean z, zzbk zzbk) {
        this.zzah = list;
        this.zzha = list2;
        this.zzhb = z;
        this.zzhc = zzbk;
    }

    public List<DataType> getDataTypes() {
        return this.zzah;
    }

    public String toString() {
        Objects.ToStringHelper add = Objects.toStringHelper(this).add("dataTypes", this.zzah).add("sourceTypes", this.zzha);
        if (this.zzhb) {
            add.add("includeDbOnlySources", "true");
        }
        return add.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, getDataTypes(), false);
        SafeParcelWriter.writeIntegerList(parcel, 2, this.zzha, false);
        SafeParcelWriter.writeBoolean(parcel, 3, this.zzhb);
        zzbk zzbk = this.zzhc;
        SafeParcelWriter.writeIBinder(parcel, 4, zzbk == null ? null : zzbk.asBinder(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
