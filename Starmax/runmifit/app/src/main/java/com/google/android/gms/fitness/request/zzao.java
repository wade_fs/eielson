package com.google.android.gms.fitness.request;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ClientIdentity;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.zzt;
import com.google.android.gms.fitness.data.zzu;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzcr;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;

public final class zzao extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzao> CREATOR = new zzap();
    private final long zzec;
    private final int zzed;
    private final zzcq zzgj;
    private final PendingIntent zzhg;
    private zzt zzhp;
    private final long zzhq;
    private final long zzhr;
    private final List<LocationRequest> zzhs;
    private final long zzht;
    private final List<ClientIdentity> zzhu;
    private DataType zzq;
    private DataSource zzr;

    zzao(DataSource dataSource, DataType dataType, IBinder iBinder, int i, int i2, long j, long j2, PendingIntent pendingIntent, long j3, int i3, List<LocationRequest> list, long j4, IBinder iBinder2) {
        this.zzr = dataSource;
        this.zzq = dataType;
        this.zzhp = iBinder == null ? null : zzu.zza(iBinder);
        this.zzec = j == 0 ? (long) i : j;
        this.zzhr = j3;
        this.zzhq = j2 == 0 ? (long) i2 : j2;
        this.zzhs = list;
        this.zzhg = pendingIntent;
        this.zzed = i3;
        this.zzhu = Collections.emptyList();
        this.zzht = j4;
        this.zzgj = zzcr.zzj(iBinder2);
    }

    private zzao(DataSource dataSource, DataType dataType, zzt zzt, PendingIntent pendingIntent, long j, long j2, long j3, int i, List<LocationRequest> list, List<ClientIdentity> list2, long j4, zzcq zzcq) {
        this.zzr = dataSource;
        this.zzq = dataType;
        this.zzhp = zzt;
        this.zzhg = pendingIntent;
        this.zzec = j;
        this.zzhr = j2;
        this.zzhq = j3;
        this.zzed = i;
        this.zzhs = null;
        this.zzhu = list2;
        this.zzht = j4;
        this.zzgj = zzcq;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzao(com.google.android.gms.fitness.request.SensorRequest r19, com.google.android.gms.fitness.data.zzt r20, android.app.PendingIntent r21, com.google.android.gms.internal.fitness.zzcq r22) {
        /*
            r18 = this;
            r0 = r19
            r1 = r18
            r4 = r20
            r5 = r21
            r17 = r22
            com.google.android.gms.fitness.data.DataSource r2 = r19.getDataSource()
            com.google.android.gms.fitness.data.DataType r3 = r19.getDataType()
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MICROSECONDS
            long r6 = r0.getSamplingRate(r6)
            java.util.concurrent.TimeUnit r8 = java.util.concurrent.TimeUnit.MICROSECONDS
            long r8 = r0.getFastestRate(r8)
            java.util.concurrent.TimeUnit r10 = java.util.concurrent.TimeUnit.MICROSECONDS
            long r10 = r0.getMaxDeliveryLatency(r10)
            int r12 = r19.getAccuracyMode()
            java.util.List r14 = java.util.Collections.emptyList()
            long r15 = r19.zzx()
            r13 = 0
            r1.<init>(r2, r3, r4, r5, r6, r8, r10, r12, r13, r14, r15, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.fitness.request.zzao.<init>(com.google.android.gms.fitness.request.SensorRequest, com.google.android.gms.fitness.data.zzt, android.app.PendingIntent, com.google.android.gms.internal.fitness.zzcq):void");
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof zzao) {
                zzao zzao = (zzao) obj;
                if (Objects.equal(this.zzr, zzao.zzr) && Objects.equal(this.zzq, zzao.zzq) && Objects.equal(this.zzhp, zzao.zzhp) && this.zzec == zzao.zzec && this.zzhr == zzao.zzhr && this.zzhq == zzao.zzhq && this.zzed == zzao.zzed && Objects.equal(this.zzhs, zzao.zzhs)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzr, this.zzq, this.zzhp, Long.valueOf(this.zzec), Long.valueOf(this.zzhr), Long.valueOf(this.zzhq), Integer.valueOf(this.zzed), this.zzhs);
    }

    public final String toString() {
        return String.format("SensorRegistrationRequest{type %s source %s interval %s fastest %s latency %s}", this.zzq, this.zzr, Long.valueOf(this.zzec), Long.valueOf(this.zzhr), Long.valueOf(this.zzhq));
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, this.zzr, i, false);
        SafeParcelWriter.writeParcelable(parcel, 2, this.zzq, i, false);
        zzt zzt = this.zzhp;
        IBinder iBinder = null;
        SafeParcelWriter.writeIBinder(parcel, 3, zzt == null ? null : zzt.asBinder(), false);
        SafeParcelWriter.writeInt(parcel, 4, 0);
        SafeParcelWriter.writeInt(parcel, 5, 0);
        SafeParcelWriter.writeLong(parcel, 6, this.zzec);
        SafeParcelWriter.writeLong(parcel, 7, this.zzhq);
        SafeParcelWriter.writeParcelable(parcel, 8, this.zzhg, i, false);
        SafeParcelWriter.writeLong(parcel, 9, this.zzhr);
        SafeParcelWriter.writeInt(parcel, 10, this.zzed);
        SafeParcelWriter.writeTypedList(parcel, 11, this.zzhs, false);
        SafeParcelWriter.writeLong(parcel, 12, this.zzht);
        zzcq zzcq = this.zzgj;
        if (zzcq != null) {
            iBinder = zzcq.asBinder();
        }
        SafeParcelWriter.writeIBinder(parcel, 13, iBinder, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
