package com.google.android.gms.internal.auth;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.auth.api.zzf;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;

abstract class zzbm extends BaseImplementation.ApiMethodImpl<ProxyApi.ProxyResult, zzbh> {
    public zzbm(GoogleApiClient googleApiClient) {
        super(zzf.API, googleApiClient);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ Result createFailedResult(Status status) {
        return new zzbq(status);
    }

    /* access modifiers changed from: protected */
    public /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        zzbh zzbh = (zzbh) anyClient;
        zzd(zzbh.getContext(), (zzbk) zzbh.getService());
    }

    /* access modifiers changed from: protected */
    public abstract void zzd(Context context, zzbk zzbk) throws RemoteException;
}
