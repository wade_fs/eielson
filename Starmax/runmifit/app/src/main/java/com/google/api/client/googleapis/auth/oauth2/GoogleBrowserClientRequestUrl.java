package com.google.api.client.googleapis.auth.oauth2;

import com.google.api.client.auth.oauth2.BrowserClientRequestUrl;
import com.google.api.client.util.Key;
import com.google.api.client.util.Preconditions;
import java.util.Collection;

public class GoogleBrowserClientRequestUrl extends BrowserClientRequestUrl {
    @Key("approval_prompt")
    private String approvalPrompt;

    public GoogleBrowserClientRequestUrl(String str, String str2, Collection<String> collection) {
        super(GoogleOAuthConstants.AUTHORIZATION_SERVER_URL, str);
        setRedirectUri(str2);
        setScopes(collection);
    }

    public GoogleBrowserClientRequestUrl(GoogleClientSecrets googleClientSecrets, String str, Collection<String> collection) {
        this(googleClientSecrets.getDetails().getClientId(), str, collection);
    }

    public final String getApprovalPrompt() {
        return this.approvalPrompt;
    }

    public GoogleBrowserClientRequestUrl setApprovalPrompt(String str) {
        this.approvalPrompt = str;
        return this;
    }

    public GoogleBrowserClientRequestUrl setResponseTypes(Collection<String> collection) {
        return (GoogleBrowserClientRequestUrl) super.setResponseTypes(collection);
    }

    public GoogleBrowserClientRequestUrl setRedirectUri(String str) {
        return (GoogleBrowserClientRequestUrl) super.setRedirectUri(str);
    }

    public GoogleBrowserClientRequestUrl setScopes(Collection<String> collection) {
        Preconditions.checkArgument(collection.iterator().hasNext());
        return (GoogleBrowserClientRequestUrl) super.setScopes(collection);
    }

    public GoogleBrowserClientRequestUrl setClientId(String str) {
        return (GoogleBrowserClientRequestUrl) super.setClientId(str);
    }

    public GoogleBrowserClientRequestUrl setState(String str) {
        return (GoogleBrowserClientRequestUrl) super.setState(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.auth.oauth2.BrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.BrowserClientRequestUrl
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.AuthorizationRequestUrl
      com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl
      com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl
      com.google.api.client.googleapis.auth.oauth2.GoogleBrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.BrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.AuthorizationRequestUrl
      com.google.api.client.auth.oauth2.BrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl
      com.google.api.client.auth.oauth2.BrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.AuthorizationRequestUrl
      com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl
      com.google.api.client.auth.oauth2.AuthorizationRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.http.GenericUrl.set(java.lang.String, java.lang.Object):com.google.api.client.http.GenericUrl
      com.google.api.client.http.GenericUrl.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.auth.oauth2.BrowserClientRequestUrl.set(java.lang.String, java.lang.Object):com.google.api.client.auth.oauth2.BrowserClientRequestUrl */
    public GoogleBrowserClientRequestUrl set(String str, Object obj) {
        return (GoogleBrowserClientRequestUrl) super.set(str, obj);
    }

    public GoogleBrowserClientRequestUrl clone() {
        return (GoogleBrowserClientRequestUrl) super.clone();
    }
}
