package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class zzakp extends BroadcastReceiver {
    private final /* synthetic */ zzakk zzcru;

    private zzakp(zzakk zzakk) {
        this.zzcru = zzakk;
    }

    /* synthetic */ zzakp(zzakk zzakk, zzakl zzakl) {
        this(zzakk);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzakk.zza(com.google.android.gms.internal.ads.zzakk, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzakk, int]
     candidates:
      com.google.android.gms.internal.ads.zzakk.zza(com.google.android.gms.internal.ads.zzakk, java.lang.String):java.lang.String
      com.google.android.gms.internal.ads.zzakk.zza(android.content.Context, android.content.Intent):void
      com.google.android.gms.internal.ads.zzakk.zza(android.content.Context, android.net.Uri):void
      com.google.android.gms.internal.ads.zzakk.zza(android.content.Context, java.lang.Throwable):void
      com.google.android.gms.internal.ads.zzakk.zza(org.json.JSONArray, java.lang.Object):void
      com.google.android.gms.internal.ads.zzakk.zza(android.app.Activity, android.content.res.Configuration):boolean
      com.google.android.gms.internal.ads.zzakk.zza(android.os.Bundle, org.json.JSONObject):org.json.JSONObject
      com.google.android.gms.internal.ads.zzakk.zza(android.content.Context, java.util.List<java.lang.String>):void
      com.google.android.gms.internal.ads.zzakk.zza(android.view.View, android.content.Context):boolean
      com.google.android.gms.internal.ads.zzakk.zza(com.google.android.gms.internal.ads.zzakk, boolean):boolean */
    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            boolean unused = this.zzcru.zzcrn = true;
        } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            boolean unused2 = this.zzcru.zzcrn = false;
        }
    }
}
