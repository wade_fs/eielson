package com.google.common.hash;

import com.google.common.base.Preconditions;
import com.google.common.hash.AbstractStreamingHashFunction;
import java.io.Serializable;
import java.nio.ByteBuffer;
import javax.annotation.Nullable;

final class SipHashFunction extends AbstractStreamingHashFunction implements Serializable {
    private static final long serialVersionUID = 0;

    /* renamed from: c */
    private final int f4594c;

    /* renamed from: d */
    private final int f4595d;

    /* renamed from: k0 */
    private final long f4596k0;

    /* renamed from: k1 */
    private final long f4597k1;

    public int bits() {
        return 64;
    }

    SipHashFunction(int i, int i2, long j, long j2) {
        Preconditions.checkArgument(i > 0, "The number of SipRound iterations (c=%s) during Compression must be positive.", Integer.valueOf(i));
        Preconditions.checkArgument(i2 > 0, "The number of SipRound iterations (d=%s) during Finalization must be positive.", Integer.valueOf(i2));
        this.f4594c = i;
        this.f4595d = i2;
        this.f4596k0 = j;
        this.f4597k1 = j2;
    }

    public Hasher newHasher() {
        return new SipHasher(this.f4594c, this.f4595d, this.f4596k0, this.f4597k1);
    }

    public String toString() {
        return "Hashing.sipHash" + this.f4594c + "" + this.f4595d + "(" + this.f4596k0 + ", " + this.f4597k1 + ")";
    }

    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof SipHashFunction)) {
            return false;
        }
        SipHashFunction sipHashFunction = (SipHashFunction) obj;
        if (this.f4594c == sipHashFunction.f4594c && this.f4595d == sipHashFunction.f4595d && this.f4596k0 == sipHashFunction.f4596k0 && this.f4597k1 == sipHashFunction.f4597k1) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (int) ((((long) ((getClass().hashCode() ^ this.f4594c) ^ this.f4595d)) ^ this.f4596k0) ^ this.f4597k1);
    }

    private static final class SipHasher extends AbstractStreamingHashFunction.AbstractStreamingHasher {
        private static final int CHUNK_SIZE = 8;

        /* renamed from: b */
        private long f4598b = 0;

        /* renamed from: c */
        private final int f4599c;

        /* renamed from: d */
        private final int f4600d;
        private long finalM = 0;

        /* renamed from: v0 */
        private long f4601v0 = 8317987319222330741L;

        /* renamed from: v1 */
        private long f4602v1 = 7237128888997146477L;

        /* renamed from: v2 */
        private long f4603v2 = 7816392313619706465L;

        /* renamed from: v3 */
        private long f4604v3 = 8387220255154660723L;

        SipHasher(int i, int i2, long j, long j2) {
            super(8);
            this.f4599c = i;
            this.f4600d = i2;
            this.f4601v0 ^= j;
            this.f4602v1 ^= j2;
            this.f4603v2 ^= j;
            this.f4604v3 ^= j2;
        }

        /* access modifiers changed from: protected */
        public void process(ByteBuffer byteBuffer) {
            this.f4598b += 8;
            processM(byteBuffer.getLong());
        }

        /* access modifiers changed from: protected */
        public void processRemaining(ByteBuffer byteBuffer) {
            this.f4598b += (long) byteBuffer.remaining();
            int i = 0;
            while (byteBuffer.hasRemaining()) {
                this.finalM ^= (((long) byteBuffer.get()) & 255) << i;
                i += 8;
            }
        }

        public HashCode makeHash() {
            this.finalM ^= this.f4598b << 56;
            processM(this.finalM);
            this.f4603v2 ^= 255;
            sipRound(this.f4600d);
            return HashCode.fromLong(((this.f4601v0 ^ this.f4602v1) ^ this.f4603v2) ^ this.f4604v3);
        }

        private void processM(long j) {
            this.f4604v3 ^= j;
            sipRound(this.f4599c);
            this.f4601v0 = j ^ this.f4601v0;
        }

        private void sipRound(int i) {
            for (int i2 = 0; i2 < i; i2++) {
                long j = this.f4601v0;
                long j2 = this.f4602v1;
                this.f4601v0 = j + j2;
                this.f4603v2 += this.f4604v3;
                this.f4602v1 = Long.rotateLeft(j2, 13);
                this.f4604v3 = Long.rotateLeft(this.f4604v3, 16);
                long j3 = this.f4602v1;
                long j4 = this.f4601v0;
                this.f4602v1 = j3 ^ j4;
                this.f4604v3 ^= this.f4603v2;
                this.f4601v0 = Long.rotateLeft(j4, 32);
                long j5 = this.f4603v2;
                long j6 = this.f4602v1;
                this.f4603v2 = j5 + j6;
                this.f4601v0 += this.f4604v3;
                this.f4602v1 = Long.rotateLeft(j6, 17);
                this.f4604v3 = Long.rotateLeft(this.f4604v3, 21);
                long j7 = this.f4602v1;
                long j8 = this.f4603v2;
                this.f4602v1 = j7 ^ j8;
                this.f4604v3 ^= this.f4601v0;
                this.f4603v2 = Long.rotateLeft(j8, 32);
            }
        }
    }
}
