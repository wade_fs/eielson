package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.fitness.request.zzbj;

final class zzdw extends zzar {
    private final /* synthetic */ Subscription zzfr;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzdw(zzdt zzdt, GoogleApiClient googleApiClient, Subscription subscription) {
        super(googleApiClient);
        this.zzfr = subscription;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzbj.<init>(com.google.android.gms.fitness.data.Subscription, boolean, com.google.android.gms.internal.fitness.zzcq):void
     arg types: [com.google.android.gms.fitness.data.Subscription, int, com.google.android.gms.internal.fitness.zzen]
     candidates:
      com.google.android.gms.fitness.request.zzbj.<init>(com.google.android.gms.fitness.data.Subscription, boolean, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzbj.<init>(com.google.android.gms.fitness.data.Subscription, boolean, com.google.android.gms.internal.fitness.zzcq):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcb) ((zzam) anyClient).getService()).zza(new zzbj(this.zzfr, false, (zzcq) new zzen(this)));
    }
}
