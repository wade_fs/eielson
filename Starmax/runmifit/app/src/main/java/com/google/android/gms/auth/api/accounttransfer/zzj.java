package com.google.android.gms.auth.api.accounttransfer;

import android.os.RemoteException;
import com.google.android.gms.auth.api.accounttransfer.AccountTransferClient;
import com.google.android.gms.internal.auth.zzac;
import com.google.android.gms.internal.auth.zzy;

final class zzj extends AccountTransferClient.zze<DeviceMetaData> {
    private final /* synthetic */ zzy zzbc;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzj(AccountTransferClient accountTransferClient, zzy zzy) {
        super(null);
        this.zzbc = zzy;
    }

    /* access modifiers changed from: protected */
    public final void zzd(zzac zzac) throws RemoteException {
        zzac.zzd(new zzk(this, super), this.zzbc);
    }
}
