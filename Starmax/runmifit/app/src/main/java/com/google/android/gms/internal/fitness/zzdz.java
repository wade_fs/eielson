package com.google.android.gms.internal.fitness;

import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;

final class zzdz extends zzci {
    private final BaseImplementation.ResultHolder<ListSubscriptionsResult> zzev;

    private zzdz(BaseImplementation.ResultHolder<ListSubscriptionsResult> resultHolder) {
        this.zzev = resultHolder;
    }

    /* synthetic */ zzdz(BaseImplementation.ResultHolder resultHolder, zzdu zzdu) {
        this(resultHolder);
    }

    public final void zza(ListSubscriptionsResult listSubscriptionsResult) {
        this.zzev.setResult(listSubscriptionsResult);
    }
}
