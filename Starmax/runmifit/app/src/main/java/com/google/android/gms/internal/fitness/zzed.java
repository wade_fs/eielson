package com.google.android.gms.internal.fitness;

import android.app.PendingIntent;
import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.zzt;
import com.google.android.gms.fitness.request.zzar;

final class zzed extends zzax {
    private final /* synthetic */ PendingIntent zzfk;
    private final /* synthetic */ zzt zzfw;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    zzed(zzea zzea, GoogleApiClient googleApiClient, zzt zzt, PendingIntent pendingIntent) {
        super(googleApiClient);
        this.zzfw = zzt;
        this.zzfk = pendingIntent;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a_ */
    public final Status mo21195a_(Status status) {
        return status;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return createFailedResult(status);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcd) ((zzas) anyClient).getService()).zza(new zzar(this.zzfw, this.zzfk, new zzen(this)));
    }
}
