package com.google.android.gms.fitness.request;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.internal.fitness.zzcq;
import com.google.android.gms.internal.fitness.zzcr;

public final class zzk extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzk> CREATOR = new zzl();
    private final DataSet zzeb;
    private final zzcq zzgj;
    private final boolean zzgq;

    zzk(DataSet dataSet, IBinder iBinder, boolean z) {
        this.zzeb = dataSet;
        this.zzgj = zzcr.zzj(iBinder);
        this.zzgq = z;
    }

    public zzk(DataSet dataSet, zzcq zzcq, boolean z) {
        this.zzeb = dataSet;
        this.zzgj = zzcq;
        this.zzgq = z;
    }

    public final boolean equals(Object obj) {
        if (obj != this) {
            return (obj instanceof zzk) && Objects.equal(this.zzeb, ((zzk) obj).zzeb);
        }
        return true;
    }

    public final int hashCode() {
        return Objects.hashCode(this.zzeb);
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("dataSet", this.zzeb).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, this.zzeb, i, false);
        zzcq zzcq = this.zzgj;
        SafeParcelWriter.writeIBinder(parcel, 2, zzcq == null ? null : zzcq.asBinder(), false);
        SafeParcelWriter.writeBoolean(parcel, 4, this.zzgq);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
