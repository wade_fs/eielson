package com.google.android.gms.internal.fitness;

import android.os.RemoteException;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.zzaj;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;

final class zzdu extends zzap<ListSubscriptionsResult> {
    zzdu(zzdt zzdt, GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Result createFailedResult(Status status) {
        return ListSubscriptionsResult.zzd(status);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.fitness.request.zzaj.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.internal.fitness.zzch):void
     arg types: [?[OBJECT, ARRAY], com.google.android.gms.internal.fitness.zzdz]
     candidates:
      com.google.android.gms.fitness.request.zzaj.<init>(com.google.android.gms.fitness.data.DataType, android.os.IBinder):void
      com.google.android.gms.fitness.request.zzaj.<init>(com.google.android.gms.fitness.data.DataType, com.google.android.gms.internal.fitness.zzch):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void doExecute(Api.AnyClient anyClient) throws RemoteException {
        ((zzcb) ((zzam) anyClient).getService()).zza(new zzaj((DataType) null, (zzch) new zzdz(this, null)));
    }
}
