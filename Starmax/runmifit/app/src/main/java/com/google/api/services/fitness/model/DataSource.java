package com.google.api.services.fitness.model;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;
import java.util.List;

public final class DataSource extends GenericJson {
    @Key
    private Application application;
    @Key
    private List<String> dataQualityStandard;
    @Key
    private String dataStreamId;
    @Key
    private String dataStreamName;
    @Key
    private DataType dataType;
    @Key
    private Device device;
    @Key
    private String name;
    @Key
    private String type;

    public Application getApplication() {
        return this.application;
    }

    public DataSource setApplication(Application application2) {
        this.application = application2;
        return this;
    }

    public List<String> getDataQualityStandard() {
        return this.dataQualityStandard;
    }

    public DataSource setDataQualityStandard(List<String> list) {
        this.dataQualityStandard = list;
        return this;
    }

    public String getDataStreamId() {
        return this.dataStreamId;
    }

    public DataSource setDataStreamId(String str) {
        this.dataStreamId = str;
        return this;
    }

    public String getDataStreamName() {
        return this.dataStreamName;
    }

    public DataSource setDataStreamName(String str) {
        this.dataStreamName = str;
        return this;
    }

    public DataType getDataType() {
        return this.dataType;
    }

    public DataSource setDataType(DataType dataType2) {
        this.dataType = dataType2;
        return this;
    }

    public Device getDevice() {
        return this.device;
    }

    public DataSource setDevice(Device device2) {
        this.device = device2;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public DataSource setName(String str) {
        this.name = str;
        return this;
    }

    public String getType() {
        return this.type;
    }

    public DataSource setType(String str) {
        this.type = str;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.api.services.fitness.model.DataSource.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.services.fitness.model.DataSource.set(java.lang.String, java.lang.Object):com.google.api.services.fitness.model.DataSource
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.util.GenericData.set(java.lang.String, java.lang.Object):com.google.api.client.util.GenericData
      com.google.api.client.json.GenericJson.set(java.lang.String, java.lang.Object):com.google.api.client.json.GenericJson */
    public DataSource set(String str, Object obj) {
        return (DataSource) super.set(str, obj);
    }

    public DataSource clone() {
        return (DataSource) super.clone();
    }
}
