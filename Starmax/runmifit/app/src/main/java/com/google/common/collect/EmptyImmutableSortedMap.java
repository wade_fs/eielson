package com.google.common.collect;

import com.google.common.base.Preconditions;
import java.util.Map;
import javax.annotation.Nullable;

final class EmptyImmutableSortedMap<K, V> extends ImmutableSortedMap<K, V> {
    private final transient ImmutableSortedSet<K> keySet;

    public V get(@Nullable Object obj) {
        return null;
    }

    public boolean isEmpty() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean isPartialView() {
        return false;
    }

    public int size() {
        return 0;
    }

    public String toString() {
        return "{}";
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [java.util.Comparator<? super K>, java.util.Comparator] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    EmptyImmutableSortedMap(java.util.Comparator<? super K> r1) {
        /*
            r0 = this;
            r0.<init>()
            com.google.common.collect.ImmutableSortedSet r1 = com.google.common.collect.ImmutableSortedSet.emptySet(r1)
            r0.keySet = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.EmptyImmutableSortedMap.<init>(java.util.Comparator):void");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [java.util.Comparator<? super K>, java.util.Comparator] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    EmptyImmutableSortedMap(java.util.Comparator<? super K> r1, com.google.common.collect.ImmutableSortedMap<K, V> r2) {
        /*
            r0 = this;
            r0.<init>(r2)
            com.google.common.collect.ImmutableSortedSet r1 = com.google.common.collect.ImmutableSortedSet.emptySet(r1)
            r0.keySet = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.collect.EmptyImmutableSortedMap.<init>(java.util.Comparator, com.google.common.collect.ImmutableSortedMap):void");
    }

    public ImmutableSortedSet<K> keySet() {
        return this.keySet;
    }

    public ImmutableCollection<V> values() {
        return ImmutableList.m5316of();
    }

    public ImmutableSet<Map.Entry<K, V>> entrySet() {
        return ImmutableSet.m5354of();
    }

    /* access modifiers changed from: package-private */
    public ImmutableSet<Map.Entry<K, V>> createEntrySet() {
        throw new AssertionError("should never be called");
    }

    public ImmutableSetMultimap<K, V> asMultimap() {
        return ImmutableSetMultimap.m5361of();
    }

    public ImmutableSortedMap<K, V> headMap(K k, boolean z) {
        Preconditions.checkNotNull(k);
        return super;
    }

    public ImmutableSortedMap<K, V> tailMap(K k, boolean z) {
        Preconditions.checkNotNull(k);
        return super;
    }

    /* access modifiers changed from: package-private */
    public ImmutableSortedMap<K, V> createDescendingMap() {
        return new EmptyImmutableSortedMap(Ordering.from(comparator()).reverse(), super);
    }
}
