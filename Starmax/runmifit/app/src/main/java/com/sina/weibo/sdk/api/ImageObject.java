package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mobstat.Config;
import java.io.File;

public class ImageObject implements Parcelable {
    public static final Parcelable.Creator<ImageObject> CREATOR = new Parcelable.Creator<ImageObject>() {
        /* class com.sina.weibo.sdk.api.ImageObject.C27471 */

        public ImageObject createFromParcel(Parcel parcel) {
            return new ImageObject(parcel);
        }

        public ImageObject[] newArray(int i) {
            return new ImageObject[i];
        }
    };
    private static final int DATA_SIZE = 2097152;
    public byte[] imageData;
    public String imagePath;

    public int describeContents() {
        return 0;
    }

    public int getObjType() {
        return 2;
    }

    public ImageObject() {
    }

    public ImageObject(Parcel parcel) {
        this.imageData = parcel.createByteArray();
        this.imagePath = parcel.readString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0024 A[SYNTHETIC, Splitter:B:13:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setImageObject(android.graphics.Bitmap r4) {
        /*
            r3 = this;
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0019 }
            r1.<init>()     // Catch:{ all -> 0x0019 }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ all -> 0x0017 }
            r2 = 85
            r4.compress(r0, r2, r1)     // Catch:{ all -> 0x0017 }
            byte[] r4 = r1.toByteArray()     // Catch:{ all -> 0x0017 }
            r3.imageData = r4     // Catch:{ all -> 0x0017 }
            r1.close()     // Catch:{ all -> 0x0028 }
            goto L_0x0030
        L_0x0017:
            r4 = move-exception
            goto L_0x001b
        L_0x0019:
            r4 = move-exception
            r1 = r0
        L_0x001b:
            com.mob.tools.log.NLog r0 = p004cn.sharesdk.framework.utils.SSDKLog.m553b()     // Catch:{ all -> 0x0031 }
            r0.mo30700d(r4)     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ all -> 0x0028 }
            goto L_0x0030
        L_0x0028:
            r4 = move-exception
            com.mob.tools.log.NLog r0 = p004cn.sharesdk.framework.utils.SSDKLog.m553b()
            r0.mo30700d(r4)
        L_0x0030:
            return
        L_0x0031:
            r4 = move-exception
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ all -> 0x0038 }
            goto L_0x0040
        L_0x0038:
            r0 = move-exception
            com.mob.tools.log.NLog r1 = p004cn.sharesdk.framework.utils.SSDKLog.m553b()
            r1.mo30700d(r0)
        L_0x0040:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.api.ImageObject.setImageObject(android.graphics.Bitmap):void");
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByteArray(this.imageData);
        parcel.writeString(this.imagePath);
    }

    public boolean checkArgs() {
        if (this.imageData == null && this.imagePath == null) {
            new Throwable("imageData and imagePath are null").printStackTrace();
            return false;
        }
        byte[] bArr = this.imageData;
        if (bArr == null || bArr.length <= DATA_SIZE) {
            String str = this.imagePath;
            if (str == null || str.length() <= 512) {
                String str2 = this.imagePath;
                if (str2 == null) {
                    return true;
                }
                try {
                    File file = new File(str2);
                    if (file.exists() && file.length() != 0 && file.length() <= Config.FULL_TRACE_LOG_LIMIT) {
                        return true;
                    }
                    new Throwable("checkArgs fail, image content is too large or not exists").printStackTrace();
                    return false;
                } catch (Throwable unused) {
                    new Throwable("checkArgs fail, image content is too large or not exists").printStackTrace();
                    return false;
                }
            } else {
                new Throwable("imagePath is too length").printStackTrace();
                return false;
            }
        } else {
            new Throwable("imageData is too large").printStackTrace();
            return false;
        }
    }
}
