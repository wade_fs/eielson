package com.sina.weibo.sdk.api;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class MultiImageObject implements Parcelable {
    public static final Parcelable.Creator<MultiImageObject> CREATOR = new Parcelable.Creator<MultiImageObject>() {
        /* class com.sina.weibo.sdk.api.MultiImageObject.C27481 */

        public MultiImageObject createFromParcel(Parcel parcel) {
            return new MultiImageObject(parcel);
        }

        public MultiImageObject[] newArray(int i) {
            return new MultiImageObject[i];
        }
    };
    public String actionUrl;
    public String defaultText;
    public String description;
    public String identify;
    public ArrayList<Uri> imageList;
    public String schema;
    public byte[] thumbData;
    public String title;

    public int describeContents() {
        return 0;
    }

    public int getObjType() {
        return 0;
    }

    public void setImageList(ArrayList<Uri> arrayList) {
        this.imageList = arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0024 A[SYNTHETIC, Splitter:B:13:0x0024] */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setThumbImage(android.graphics.Bitmap r4) {
        /*
            r3 = this;
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0019 }
            r1.<init>()     // Catch:{ all -> 0x0019 }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ all -> 0x0017 }
            r2 = 85
            r4.compress(r0, r2, r1)     // Catch:{ all -> 0x0017 }
            byte[] r4 = r1.toByteArray()     // Catch:{ all -> 0x0017 }
            r3.thumbData = r4     // Catch:{ all -> 0x0017 }
            r1.close()     // Catch:{ all -> 0x0028 }
            goto L_0x0030
        L_0x0017:
            r4 = move-exception
            goto L_0x001b
        L_0x0019:
            r4 = move-exception
            r1 = r0
        L_0x001b:
            com.mob.tools.log.NLog r0 = p004cn.sharesdk.framework.utils.SSDKLog.m553b()     // Catch:{ all -> 0x0031 }
            r0.mo30700d(r4)     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ all -> 0x0028 }
            goto L_0x0030
        L_0x0028:
            r4 = move-exception
            com.mob.tools.log.NLog r0 = p004cn.sharesdk.framework.utils.SSDKLog.m553b()
            r0.mo30700d(r4)
        L_0x0030:
            return
        L_0x0031:
            r4 = move-exception
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ all -> 0x0038 }
            goto L_0x0040
        L_0x0038:
            r0 = move-exception
            com.mob.tools.log.NLog r1 = p004cn.sharesdk.framework.utils.SSDKLog.m553b()
            r1.mo30700d(r0)
        L_0x0040:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.api.MultiImageObject.setThumbImage(android.graphics.Bitmap):void");
    }

    public ArrayList<Uri> getImageList() {
        return this.imageList;
    }

    public MultiImageObject() {
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.actionUrl);
        parcel.writeString(this.schema);
        parcel.writeString(this.identify);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeByteArray(this.thumbData);
        parcel.writeTypedList(this.imageList);
    }

    protected MultiImageObject(Parcel parcel) {
        this.actionUrl = parcel.readString();
        this.schema = parcel.readString();
        this.identify = parcel.readString();
        this.title = parcel.readString();
        this.description = parcel.readString();
        this.thumbData = parcel.createByteArray();
        this.imageList = parcel.createTypedArrayList(Uri.CREATOR);
    }
}
