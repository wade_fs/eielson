package com.bumptech.glide.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<T, Y> {
    private final LinkedHashMap<T, Y> cache = new LinkedHashMap<>(100, 0.75f, true);
    private int currentSize = 0;
    private final int initialMaxSize;
    private int maxSize;

    /* access modifiers changed from: protected */
    public int getSize(Object obj) {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void onItemEvicted(Object obj, Object obj2) {
    }

    public LruCache(int i) {
        this.initialMaxSize = i;
        this.maxSize = i;
    }

    public void setSizeMultiplier(float f) {
        if (f >= 0.0f) {
            this.maxSize = Math.round(((float) this.initialMaxSize) * f);
            evict();
            return;
        }
        throw new IllegalArgumentException("Multiplier must be >= 0");
    }

    public int getMaxSize() {
        return this.maxSize;
    }

    public int getCurrentSize() {
        return this.currentSize;
    }

    public boolean contains(T t) {
        return this.cache.containsKey(t);
    }

    public Y get(T t) {
        return this.cache.get(t);
    }

    public Y put(Object obj, Object obj2) {
        if (getSize(obj2) >= this.maxSize) {
            onItemEvicted(obj, obj2);
            return null;
        }
        Y put = this.cache.put(obj, obj2);
        if (obj2 != null) {
            this.currentSize += getSize(obj2);
        }
        if (put != null) {
            this.currentSize -= getSize(put);
        }
        evict();
        return put;
    }

    public Y remove(Object obj) {
        Y remove = this.cache.remove(obj);
        if (remove != null) {
            this.currentSize -= getSize(remove);
        }
        return remove;
    }

    public void clearMemory() {
        trimToSize(0);
    }

    /* access modifiers changed from: protected */
    public void trimToSize(int i) {
        while (this.currentSize > i) {
            Map.Entry next = this.cache.entrySet().iterator().next();
            Object value = next.getValue();
            this.currentSize -= getSize(value);
            Object key = next.getKey();
            this.cache.remove(key);
            onItemEvicted(key, value);
        }
    }

    private void evict() {
        trimToSize(this.maxSize);
    }
}
