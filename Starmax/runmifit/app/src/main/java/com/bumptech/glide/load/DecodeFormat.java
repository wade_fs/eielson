package com.bumptech.glide.load;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class DecodeFormat extends Enum<DecodeFormat> {
    private static final /* synthetic */ DecodeFormat[] $VALUES;
    @Deprecated
    public static final DecodeFormat ALWAYS_ARGB_8888 = new DecodeFormat("ALWAYS_ARGB_8888", 0);
    public static final DecodeFormat DEFAULT;
    public static final DecodeFormat PREFER_ARGB_8888 = new DecodeFormat("PREFER_ARGB_8888", 1);
    public static final DecodeFormat PREFER_RGB_565 = new DecodeFormat("PREFER_RGB_565", 2);

    private DecodeFormat(String str, int i) {
    }

    public static DecodeFormat valueOf(String str) {
        return (DecodeFormat) Enum.valueOf(DecodeFormat.class, str);
    }

    public static DecodeFormat[] values() {
        return (DecodeFormat[]) $VALUES.clone();
    }

    static {
        DecodeFormat decodeFormat = PREFER_RGB_565;
        $VALUES = new DecodeFormat[]{ALWAYS_ARGB_8888, PREFER_ARGB_8888, decodeFormat};
        DEFAULT = decodeFormat;
    }
}
