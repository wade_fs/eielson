package com.bumptech.glide.gifdecoder;

class GifFrame {
    int bufferFrameStart;
    int delay;
    int dispose;

    /* renamed from: ih */
    int f4476ih;
    boolean interlace;

    /* renamed from: iw */
    int f4477iw;

    /* renamed from: ix */
    int f4478ix;

    /* renamed from: iy */
    int f4479iy;
    int[] lct;
    int transIndex;
    boolean transparency;

    GifFrame() {
    }
}
