package com.orhanobut.logger;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.math3.geometry.VectorFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class LoggerPrinter implements Printer {
    private static final int JSON_INDENT = 2;
    private final ThreadLocal<String> localTag = new ThreadLocal<>();
    private final List<LogAdapter> logAdapters = new ArrayList();

    LoggerPrinter() {
    }

    /* renamed from: t */
    public Printer mo31284t(String str) {
        if (str != null) {
            this.localTag.set(str);
        }
        return this;
    }

    /* renamed from: d */
    public void mo31278d(String str, Object... objArr) {
        log(3, (Throwable) null, str, objArr);
    }

    /* renamed from: d */
    public void mo31277d(Object obj) {
        log(3, (Throwable) null, Utils.toString(obj), new Object[0]);
    }

    /* renamed from: e */
    public void mo31279e(String str, Object... objArr) {
        mo31280e(null, str, objArr);
    }

    /* renamed from: e */
    public void mo31280e(Throwable th, String str, Object... objArr) {
        log(6, th, str, objArr);
    }

    /* renamed from: w */
    public void mo31286w(String str, Object... objArr) {
        log(5, (Throwable) null, str, objArr);
    }

    /* renamed from: i */
    public void mo31281i(String str, Object... objArr) {
        log(4, (Throwable) null, str, objArr);
    }

    /* renamed from: v */
    public void mo31285v(String str, Object... objArr) {
        log(2, (Throwable) null, str, objArr);
    }

    public void wtf(String str, Object... objArr) {
        log(7, (Throwable) null, str, objArr);
    }

    public void json(String str) {
        if (Utils.isEmpty(str)) {
            mo31277d("Empty/Null json content");
            return;
        }
        try {
            String trim = str.trim();
            if (trim.startsWith(VectorFormat.DEFAULT_PREFIX)) {
                mo31277d(new JSONObject(trim).toString(2));
            } else if (trim.startsWith("[")) {
                mo31277d(new JSONArray(trim).toString(2));
            } else {
                mo31279e("Invalid Json", new Object[0]);
            }
        } catch (JSONException unused) {
            mo31279e("Invalid Json", new Object[0]);
        }
    }

    public void xml(String str) {
        if (Utils.isEmpty(str)) {
            mo31277d("Empty/Null xml content");
            return;
        }
        try {
            StreamSource streamSource = new StreamSource(new StringReader(str));
            StreamResult streamResult = new StreamResult(new StringWriter());
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            newTransformer.transform(streamSource, streamResult);
            mo31277d(streamResult.getWriter().toString().replaceFirst(">", ">\n"));
        } catch (TransformerException unused) {
            mo31279e("Invalid xml", new Object[0]);
        }
    }

    public synchronized void log(int i, String str, String str2, Throwable th) {
        if (!(th == null || str2 == null)) {
            str2 = str2 + " : " + Utils.getStackTraceString(th);
        }
        if (th != null && str2 == null) {
            str2 = Utils.getStackTraceString(th);
        }
        if (Utils.isEmpty(str2)) {
            str2 = "Empty/NULL log message";
        }
        for (LogAdapter logAdapter : this.logAdapters) {
            if (logAdapter.isLoggable(i, str)) {
                logAdapter.log(i, str, str2);
            }
        }
    }

    public void clearLogAdapters() {
        this.logAdapters.clear();
    }

    public void addAdapter(LogAdapter logAdapter) {
        this.logAdapters.add(logAdapter);
    }

    private synchronized void log(int i, Throwable th, String str, Object... objArr) {
        log(i, getTag(), createMessage(str, objArr), th);
    }

    private String getTag() {
        String str = this.localTag.get();
        if (str == null) {
            return null;
        }
        this.localTag.remove();
        return str;
    }

    private String createMessage(String str, Object... objArr) {
        return (objArr == null || objArr.length == 0) ? str : String.format(str, objArr);
    }
}
