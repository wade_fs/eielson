package com.orhanobut.logger;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DiskLogStrategy implements LogStrategy {
    private final Handler handler;

    public DiskLogStrategy(Handler handler2) {
        this.handler = handler2;
    }

    public void log(int i, String str, String str2) {
        Handler handler2 = this.handler;
        handler2.sendMessage(handler2.obtainMessage(i, str2));
    }

    static class WriteHandler extends Handler {
        private final String folder;
        private final int maxFileSize;

        WriteHandler(Looper looper, String str, int i) {
            super(looper);
            this.folder = str;
            this.maxFileSize = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
          ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
        public void handleMessage(Message message) {
            FileWriter fileWriter;
            String str = (String) message.obj;
            try {
                fileWriter = new FileWriter(getLogFile(this.folder, "logs"), true);
                try {
                    writeLog(fileWriter, str);
                    fileWriter.flush();
                    fileWriter.close();
                } catch (IOException unused) {
                }
            } catch (IOException unused2) {
                fileWriter = null;
                if (fileWriter != null) {
                    try {
                        fileWriter.flush();
                        fileWriter.close();
                    } catch (IOException unused3) {
                    }
                }
            }
        }

        private void writeLog(FileWriter fileWriter, String str) throws IOException {
            fileWriter.append((CharSequence) str);
        }

        private File getLogFile(String str, String str2) {
            File file;
            File file2 = new File(str);
            if (!file2.exists()) {
                file2.mkdirs();
            }
            File file3 = null;
            File file4 = new File(file2, String.format("%s_%s.csv", str2, 0));
            int i = 0;
            while (true) {
                File file5 = file4;
                file = file3;
                file3 = file5;
                if (!file3.exists()) {
                    break;
                }
                i++;
                file4 = new File(file2, String.format("%s_%s.csv", str2, Integer.valueOf(i)));
            }
            return (file == null || file.length() >= ((long) this.maxFileSize)) ? file3 : file;
        }
    }
}
