package com.orhanobut.logger;

public interface Printer {
    void addAdapter(LogAdapter logAdapter);

    void clearLogAdapters();

    /* renamed from: d */
    void mo31277d(Object obj);

    /* renamed from: d */
    void mo31278d(String str, Object... objArr);

    /* renamed from: e */
    void mo31279e(String str, Object... objArr);

    /* renamed from: e */
    void mo31280e(Throwable th, String str, Object... objArr);

    /* renamed from: i */
    void mo31281i(String str, Object... objArr);

    void json(String str);

    void log(int i, String str, String str2, Throwable th);

    /* renamed from: t */
    Printer mo31284t(String str);

    /* renamed from: v */
    void mo31285v(String str, Object... objArr);

    /* renamed from: w */
    void mo31286w(String str, Object... objArr);

    void wtf(String str, Object... objArr);

    void xml(String str);
}
