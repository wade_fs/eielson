package com.orhanobut.logger;

public final class Logger {
    public static final int ASSERT = 7;
    public static final int DEBUG = 3;
    public static final int ERROR = 6;
    public static final int INFO = 4;
    public static final int VERBOSE = 2;
    public static final int WARN = 5;
    private static Printer printer = new LoggerPrinter();

    private Logger() {
    }

    public static void printer(Printer printer2) {
        printer = printer2;
    }

    public static void addLogAdapter(LogAdapter logAdapter) {
        printer.addAdapter(logAdapter);
    }

    public static void clearLogAdapters() {
        printer.clearLogAdapters();
    }

    /* renamed from: t */
    public static Printer m6182t(String str) {
        return printer.mo31284t(str);
    }

    public static void log(int i, String str, String str2, Throwable th) {
        printer.log(i, str, str2, th);
    }

    /* renamed from: d */
    public static void m6178d(String str, Object... objArr) {
        printer.mo31278d(str, objArr);
    }

    /* renamed from: d */
    public static void m6177d(Object obj) {
        printer.mo31277d(obj);
    }

    /* renamed from: e */
    public static void m6179e(String str, Object... objArr) {
        printer.mo31280e(null, str, objArr);
    }

    /* renamed from: e */
    public static void m6180e(Throwable th, String str, Object... objArr) {
        printer.mo31280e(th, str, objArr);
    }

    /* renamed from: i */
    public static void m6181i(String str, Object... objArr) {
        printer.mo31281i(str, objArr);
    }

    /* renamed from: v */
    public static void m6183v(String str, Object... objArr) {
        printer.mo31285v(str, objArr);
    }

    /* renamed from: w */
    public static void m6184w(String str, Object... objArr) {
        printer.mo31286w(str, objArr);
    }

    public static void wtf(String str, Object... objArr) {
        printer.wtf(str, objArr);
    }

    public static void json(String str) {
        printer.json(str);
    }

    public static void xml(String str) {
        printer.xml(str);
    }
}
