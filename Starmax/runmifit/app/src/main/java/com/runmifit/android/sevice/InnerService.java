package com.wade.fit.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.internal.view.SupportMenu;
import com.wade.fit.R;

public class InnerService extends Service {
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        Notification.Builder builder = new Notification.Builder(this);
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel("com.hearth.notification", "otifition_one", 4);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(SupportMenu.CATEGORY_MASK);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(1);
            ((NotificationManager) getSystemService("notification")).createNotificationChannel(notificationChannel);
            builder.setChannelId("com.hearth.notification");
        }
        builder.setContentTitle("Foreground Service");
        builder.setContentText("Foreground Service Started.");
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon((int) R.mipmap.ic_launcher);
        startForeground(1, builder.build());
        new Handler().postDelayed(new Runnable() {
            /* class com.wade.fit.service.$$Lambda$InnerService$qBnvrq8SXpR60QDRNcGVWNCv2Bg */

            public final void run() {
                InnerService.this.lambda$onCreate$0$InnerService();
            }
        }, 100);
    }

    public /* synthetic */ void lambda$onCreate$0$InnerService() {
        stopForeground(true);
        ((NotificationManager) getSystemService("notification")).cancel(1);
        stopSelf();
    }
}
