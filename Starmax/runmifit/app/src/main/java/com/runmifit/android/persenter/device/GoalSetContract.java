package com.wade.fit.persenter.device;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.Goal;

public interface GoalSetContract {

    public interface Presenter {
        void saveGold(int i, Goal goal);
    }

    public interface View extends IBaseView {
        void saveFaild(int i);

        void saveSuccess(int i);
    }
}
