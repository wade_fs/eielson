package com.wade.fit.util.ble;

import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.Goal;
import com.wade.fit.util.SPHelper;

public class TargeDataHandler {
    public void handlerTargeData(byte[] bArr) {
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        Goal goal = new Goal();
        byte[] bArr2 = new byte[3];
        bArr2[2] = bArr[6];
        bArr2[1] = bArr[7];
        bArr2[0] = bArr[8];
        goal.goalStep = ByteDataConvertUtil.BinToInt(bArr2, 0, 3);
        byte[] bArr3 = new byte[3];
        bArr3[2] = bArr[14];
        bArr3[1] = bArr[15];
        bArr3[0] = bArr[16];
        goal.goalDistanceKm = ByteDataConvertUtil.BinToInt(bArr3, 0, 3);
        byte[] bArr4 = new byte[3];
        bArr4[2] = bArr[10];
        bArr4[1] = bArr[11];
        bArr4[0] = bArr[12];
        goal.goalCal = ByteDataConvertUtil.BinToInt(bArr4, 0, 3);
        deviceConfig.goal = goal;
        SPHelper.saveDeviceConfig(deviceConfig);
    }
}
