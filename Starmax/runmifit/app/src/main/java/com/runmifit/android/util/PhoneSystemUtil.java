package com.wade.fit.util;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import java.util.Locale;
import java.util.regex.Pattern;

public class PhoneSystemUtil {
    public static String getPhoneSystemLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public static Locale[] getPhoneSystemLanguageList() {
        return Locale.getAvailableLocales();
    }

    public static String getPhoneSystemVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String getPhoneSystemModel() {
        return Build.MODEL;
    }

    public static String getPhoneDeviceBrand() {
        return Build.BRAND;
    }

    public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null || ActivityCompat.checkSelfPermission(context, "android.permission.READ_PHONE_STATE") == 0) {
            return null;
        }
        return telephonyManager.getDeviceId();
    }

    public static boolean validPhoneNum(String str) {
        Pattern compile = Pattern.compile("^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\\d{8})?$");
        Pattern compile2 = Pattern.compile("^(0[0-9]{2,3}\\-)?([1-9][0-9]{6,7})$");
        if (str.length() != 11 || !compile.matcher(str).matches()) {
            return str.length() < 16 && compile2.matcher(str).matches();
        }
        return true;
    }

    public static boolean isZh(Context context) {
        return context.getResources().getConfiguration().locale.getLanguage().endsWith("zh");
    }
}
