package com.wade.fit.views.pullscrollview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.wade.fit.R;
import com.wade.fit.views.pullscrollview.internal.FlipLoadingLayout;
import com.wade.fit.views.pullscrollview.internal.LoadingLayout;
import com.wade.fit.views.pullscrollview.internal.RotateLoadingLayout;
import com.wade.fit.views.pullscrollview.internal.Utils;
import com.wade.fit.views.pullscrollview.internal.ViewCompat;

public abstract class PullToRefreshBase<T extends View> extends LinearLayout implements IPullToRefresh<T> {
    static final boolean DEBUG = true;
    static final int DEMO_SCROLL_INTERVAL = 225;
    static final float FRICTION = 2.0f;
    static final String LOG_TAG = "PullToRefresh";
    public static final int SMOOTH_SCROLL_DURATION_MS = 200;
    public static final int SMOOTH_SCROLL_LONG_DURATION_MS = 325;
    static final String STATE_CURRENT_MODE = "ptr_current_mode";
    static final String STATE_MODE = "ptr_mode";
    static final String STATE_SCROLLING_REFRESHING_ENABLED = "ptr_disable_scrolling";
    static final String STATE_SHOW_REFRESHING_VIEW = "ptr_show_refreshing_view";
    static final String STATE_STATE = "ptr_state";
    static final String STATE_SUPER = "ptr_super";
    static final boolean USE_HW_LAYERS = false;
    private Mode mCurrentMode;
    private PullToRefreshBase<T>.SmoothScrollRunnable mCurrentSmoothScrollRunnable;
    private boolean mFilterTouchEvents = true;
    private LoadingLayout mFooterLayout;
    private LoadingLayout mHeaderLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private boolean mIsBeingDragged = false;
    private float mLastMotionX;
    private float mLastMotionY;
    private boolean mLayoutVisibilityChangesEnabled = true;
    private AnimationStyle mLoadingAnimationStyle = AnimationStyle.getDefault();
    private Mode mMode = Mode.getDefault();
    private OnPullEventListener<T> mOnPullEventListener;
    private OnRefreshListener<T> mOnRefreshListener;
    private OnRefreshListener2<T> mOnRefreshListener2;
    private boolean mOverScrollEnabled = true;
    T mRefreshableView;
    private FrameLayout mRefreshableViewWrapper;
    /* access modifiers changed from: private */
    public Interpolator mScrollAnimationInterpolator;
    private boolean mScrollingWhileRefreshingEnabled = false;
    private boolean mShowViewWhileRefreshing = true;
    private State mState = State.RESET;
    private int mTouchSlop;

    public interface OnLastItemVisibleListener {
        void onLastItemVisible();
    }

    public interface OnPullEventListener<V extends View> {
        void onPullEvent(PullToRefreshBase<V> pullToRefreshBase, State state, Mode mode);
    }

    public interface OnRefreshListener<V extends View> {
        void onRefresh(PullToRefreshBase<V> pullToRefreshBase);
    }

    public interface OnRefreshListener2<V extends View> {
        void onPullDownToRefresh(PullToRefreshBase<V> pullToRefreshBase);

        void onPullUpToRefresh(PullToRefreshBase<V> pullToRefreshBase);
    }

    interface OnSmoothScrollFinishedListener {
        void onSmoothScrollFinished();
    }

    public enum Orientation {
        VERTICAL,
        HORIZONTAL
    }

    /* access modifiers changed from: protected */
    public abstract T createRefreshableView(Context context, AttributeSet attributeSet);

    public abstract Orientation getPullToRefreshScrollDirection();

    /* access modifiers changed from: protected */
    public int getPullToRefreshScrollDuration() {
        return 200;
    }

    /* access modifiers changed from: protected */
    public int getPullToRefreshScrollDurationLonger() {
        return SMOOTH_SCROLL_LONG_DURATION_MS;
    }

    /* access modifiers changed from: protected */
    public void handleStyledAttributes(TypedArray typedArray) {
    }

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForPullEnd();

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForPullStart();

    /* access modifiers changed from: protected */
    public void onPtrRestoreInstanceState(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public void onPtrSaveInstanceState(Bundle bundle) {
    }

    public PullToRefreshBase(Context context) {
        super(context);
        init(context, null);
    }

    public PullToRefreshBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet);
    }

    public PullToRefreshBase(Context context, Mode mode) {
        super(context);
        this.mMode = mode;
        init(context, null);
    }

    public PullToRefreshBase(Context context, Mode mode, AnimationStyle animationStyle) {
        super(context);
        this.mMode = mode;
        this.mLoadingAnimationStyle = animationStyle;
        init(context, null);
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        Log.d(LOG_TAG, "addView: " + view.getClass().getSimpleName());
        View refreshableView = getRefreshableView();
        if (refreshableView instanceof ViewGroup) {
            ((ViewGroup) refreshableView).addView(view, i, layoutParams);
            return;
        }
        throw new UnsupportedOperationException("Refreshable View is not a ViewGroup so can't addView");
    }

    public final boolean demo() {
        if (this.mMode.showHeaderLoadingLayout() && isReadyForPullStart()) {
            smoothScrollToAndBack((-getHeaderSize()) * 2);
            return true;
        } else if (!this.mMode.showFooterLoadingLayout() || !isReadyForPullEnd()) {
            return false;
        } else {
            smoothScrollToAndBack(getFooterSize() * 2);
            return true;
        }
    }

    public final Mode getCurrentMode() {
        return this.mCurrentMode;
    }

    public final boolean getFilterTouchEvents() {
        return this.mFilterTouchEvents;
    }

    public final ILoadingLayout getLoadingLayoutProxy() {
        return getLoadingLayoutProxy(true, true);
    }

    public final ILoadingLayout getLoadingLayoutProxy(boolean z, boolean z2) {
        return createLoadingLayoutProxy(z, z2);
    }

    public final Mode getMode() {
        return this.mMode;
    }

    public final T getRefreshableView() {
        return this.mRefreshableView;
    }

    public final boolean getShowViewWhileRefreshing() {
        return this.mShowViewWhileRefreshing;
    }

    public final State getState() {
        return this.mState;
    }

    public final boolean isDisableScrollingWhileRefreshing() {
        return !isScrollingWhileRefreshingEnabled();
    }

    public final boolean isPullToRefreshEnabled() {
        return this.mMode.permitsPullToRefresh();
    }

    public final boolean isPullToRefreshOverScrollEnabled() {
        return Build.VERSION.SDK_INT >= 9 && this.mOverScrollEnabled && OverscrollHelper.isAndroidOverScrollEnabled(this.mRefreshableView);
    }

    public final boolean isRefreshing() {
        return this.mState == State.REFRESHING || this.mState == State.MANUAL_REFRESHING;
    }

    public final boolean isScrollingWhileRefreshingEnabled() {
        return this.mScrollingWhileRefreshingEnabled;
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        float f;
        float f2;
        if (!isPullToRefreshEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 3 || action == 1) {
            this.mIsBeingDragged = false;
            return false;
        } else if (action != 0 && this.mIsBeingDragged) {
            return true;
        } else {
            if (action != 0) {
                if (action == 2) {
                    if (!this.mScrollingWhileRefreshingEnabled && isRefreshing()) {
                        return true;
                    }
                    if (isReadyForPull()) {
                        float y = motionEvent.getY();
                        float x = motionEvent.getX();
                        if (C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()] != 1) {
                            f2 = y - this.mLastMotionY;
                            f = x - this.mLastMotionX;
                        } else {
                            f2 = x - this.mLastMotionX;
                            f = y - this.mLastMotionY;
                        }
                        float abs = Math.abs(f2);
                        if (abs > ((float) this.mTouchSlop) && (!this.mFilterTouchEvents || abs > Math.abs(f))) {
                            if (this.mMode.showHeaderLoadingLayout() && f2 >= 1.0f && isReadyForPullStart()) {
                                this.mLastMotionY = y;
                                this.mLastMotionX = x;
                                this.mIsBeingDragged = true;
                                if (this.mMode == Mode.BOTH) {
                                    this.mCurrentMode = Mode.PULL_FROM_START;
                                }
                            } else if (this.mMode.showFooterLoadingLayout() && f2 <= -1.0f && isReadyForPullEnd()) {
                                this.mLastMotionY = y;
                                this.mLastMotionX = x;
                                this.mIsBeingDragged = true;
                                if (this.mMode == Mode.BOTH) {
                                    this.mCurrentMode = Mode.PULL_FROM_END;
                                }
                            }
                        }
                    }
                }
            } else if (isReadyForPull()) {
                float y2 = motionEvent.getY();
                this.mInitialMotionY = y2;
                this.mLastMotionY = y2;
                float x2 = motionEvent.getX();
                this.mInitialMotionX = x2;
                this.mLastMotionX = x2;
                this.mIsBeingDragged = false;
            }
            return this.mIsBeingDragged;
        }
    }

    public final void onRefreshComplete() {
        if (isRefreshing()) {
            setState(State.RESET, new boolean[0]);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002d, code lost:
        if (r0 != 3) goto L_0x008b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean onTouchEvent(android.view.MotionEvent r5) {
        /*
            r4 = this;
            boolean r0 = r4.isPullToRefreshEnabled()
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            boolean r0 = r4.mScrollingWhileRefreshingEnabled
            r2 = 1
            if (r0 != 0) goto L_0x0014
            boolean r0 = r4.isRefreshing()
            if (r0 == 0) goto L_0x0014
            return r2
        L_0x0014:
            int r0 = r5.getAction()
            if (r0 != 0) goto L_0x0021
            int r0 = r5.getEdgeFlags()
            if (r0 == 0) goto L_0x0021
            return r1
        L_0x0021:
            int r0 = r5.getAction()
            if (r0 == 0) goto L_0x0074
            if (r0 == r2) goto L_0x0044
            r3 = 2
            if (r0 == r3) goto L_0x0030
            r5 = 3
            if (r0 == r5) goto L_0x0044
            goto L_0x008b
        L_0x0030:
            boolean r0 = r4.mIsBeingDragged
            if (r0 == 0) goto L_0x008b
            float r0 = r5.getY()
            r4.mLastMotionY = r0
            float r5 = r5.getX()
            r4.mLastMotionX = r5
            r4.pullEvent()
            return r2
        L_0x0044:
            boolean r5 = r4.mIsBeingDragged
            if (r5 == 0) goto L_0x008b
            r4.mIsBeingDragged = r1
            com.wade.fit.views.pullscrollview.PullToRefreshBase$State r5 = r4.mState
            com.wade.fit.views.pullscrollview.PullToRefreshBase$State r0 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.RELEASE_TO_REFRESH
            if (r5 != r0) goto L_0x0062
            com.wade.fit.views.pullscrollview.PullToRefreshBase$OnRefreshListener<T> r5 = r4.mOnRefreshListener
            if (r5 != 0) goto L_0x0058
            com.wade.fit.views.pullscrollview.PullToRefreshBase$OnRefreshListener2<T> r5 = r4.mOnRefreshListener2
            if (r5 == 0) goto L_0x0062
        L_0x0058:
            com.wade.fit.views.pullscrollview.PullToRefreshBase$State r5 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.REFRESHING
            boolean[] r0 = new boolean[r2]
            r0[r1] = r2
            r4.setState(r5, r0)
            return r2
        L_0x0062:
            boolean r5 = r4.isRefreshing()
            if (r5 == 0) goto L_0x006c
            r4.smoothScrollTo(r1)
            return r2
        L_0x006c:
            com.wade.fit.views.pullscrollview.PullToRefreshBase$State r5 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.RESET
            boolean[] r0 = new boolean[r1]
            r4.setState(r5, r0)
            return r2
        L_0x0074:
            boolean r0 = r4.isReadyForPull()
            if (r0 == 0) goto L_0x008b
            float r0 = r5.getY()
            r4.mInitialMotionY = r0
            r4.mLastMotionY = r0
            float r5 = r5.getX()
            r4.mInitialMotionX = r5
            r4.mLastMotionX = r5
            return r2
        L_0x008b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.pullscrollview.PullToRefreshBase.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public final void setScrollingWhileRefreshingEnabled(boolean z) {
        this.mScrollingWhileRefreshingEnabled = z;
    }

    public void setDisableScrollingWhileRefreshing(boolean z) {
        setScrollingWhileRefreshingEnabled(!z);
    }

    public final void setFilterTouchEvents(boolean z) {
        this.mFilterTouchEvents = z;
    }

    public void setLastUpdatedLabel(CharSequence charSequence) {
        getLoadingLayoutProxy().setLastUpdatedLabel(charSequence);
    }

    public void setLoadingDrawable(Drawable drawable) {
        getLoadingLayoutProxy().setLoadingDrawable(drawable);
    }

    public void setLoadingDrawable(Drawable drawable, Mode mode) {
        getLoadingLayoutProxy(mode.showHeaderLoadingLayout(), mode.showFooterLoadingLayout()).setLoadingDrawable(drawable);
    }

    public void setLongClickable(boolean z) {
        getRefreshableView().setLongClickable(z);
    }

    public final void setMode(Mode mode) {
        if (mode != this.mMode) {
            Log.d(LOG_TAG, "Setting mode to: " + mode);
            this.mMode = mode;
            updateUIForMode();
        }
    }

    public void setOnPullEventListener(OnPullEventListener<T> onPullEventListener) {
        this.mOnPullEventListener = onPullEventListener;
    }

    public final void setOnRefreshListener(OnRefreshListener onRefreshListener) {
        this.mOnRefreshListener = onRefreshListener;
        this.mOnRefreshListener2 = null;
    }

    public final void setOnRefreshListener(OnRefreshListener2 onRefreshListener2) {
        this.mOnRefreshListener2 = onRefreshListener2;
        this.mOnRefreshListener = null;
    }

    public void setPullLabel(CharSequence charSequence) {
        getLoadingLayoutProxy().setPullLabel(charSequence);
    }

    public void setPullLabel(CharSequence charSequence, Mode mode) {
        getLoadingLayoutProxy(mode.showHeaderLoadingLayout(), mode.showFooterLoadingLayout()).setPullLabel(charSequence);
    }

    public final void setPullToRefreshEnabled(boolean z) {
        setMode(z ? Mode.getDefault() : Mode.DISABLED);
    }

    public final void setPullToRefreshOverScrollEnabled(boolean z) {
        this.mOverScrollEnabled = z;
    }

    public final void setRefreshing() {
        setRefreshing(true);
    }

    public final void setRefreshing(boolean z) {
        if (!isRefreshing()) {
            setState(State.MANUAL_REFRESHING, z);
        }
    }

    public void setRefreshingLabel(CharSequence charSequence) {
        getLoadingLayoutProxy().setRefreshingLabel(charSequence);
    }

    public void setRefreshingLabel(CharSequence charSequence, Mode mode) {
        getLoadingLayoutProxy(mode.showHeaderLoadingLayout(), mode.showFooterLoadingLayout()).setRefreshingLabel(charSequence);
    }

    public void setReleaseLabel(CharSequence charSequence) {
        setReleaseLabel(charSequence, Mode.BOTH);
    }

    public void setReleaseLabel(CharSequence charSequence, Mode mode) {
        getLoadingLayoutProxy(mode.showHeaderLoadingLayout(), mode.showFooterLoadingLayout()).setReleaseLabel(charSequence);
    }

    public void setScrollAnimationInterpolator(Interpolator interpolator) {
        this.mScrollAnimationInterpolator = interpolator;
    }

    public final void setShowViewWhileRefreshing(boolean z) {
        this.mShowViewWhileRefreshing = z;
    }

    /* access modifiers changed from: package-private */
    public final void setState(State state, boolean... zArr) {
        this.mState = state;
        Log.d(LOG_TAG, "State: " + this.mState.name());
        int i = C27404.f5271xb5ae42ee[this.mState.ordinal()];
        if (i == 1) {
            onReset();
        } else if (i == 2) {
            onPullToRefresh();
        } else if (i == 3) {
            onReleaseToRefresh();
        } else if (i == 4 || i == 5) {
            onRefreshing(zArr[0]);
        }
        OnPullEventListener<T> onPullEventListener = this.mOnPullEventListener;
        if (onPullEventListener != null) {
            onPullEventListener.onPullEvent(this, this.mState, this.mCurrentMode);
        }
    }

    /* access modifiers changed from: protected */
    public final void addViewInternal(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
    }

    /* access modifiers changed from: protected */
    public final void addViewInternal(View view, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, -1, layoutParams);
    }

    /* access modifiers changed from: protected */
    public LoadingLayout createLoadingLayout(Context context, Mode mode, TypedArray typedArray) {
        LoadingLayout createLoadingLayout = this.mLoadingAnimationStyle.createLoadingLayout(context, mode, getPullToRefreshScrollDirection(), typedArray);
        createLoadingLayout.setVisibility(View.INVISIBLE);
        return createLoadingLayout;
    }

    /* access modifiers changed from: protected */
    public LoadingLayoutProxy createLoadingLayoutProxy(boolean z, boolean z2) {
        LoadingLayoutProxy loadingLayoutProxy = new LoadingLayoutProxy();
        if (z && this.mMode.showHeaderLoadingLayout()) {
            loadingLayoutProxy.addLayout(this.mHeaderLayout);
        }
        if (z2 && this.mMode.showFooterLoadingLayout()) {
            loadingLayoutProxy.addLayout(this.mFooterLayout);
        }
        return loadingLayoutProxy;
    }

    /* access modifiers changed from: protected */
    public final void disableLoadingLayoutVisibilityChanges() {
        this.mLayoutVisibilityChangesEnabled = false;
    }

    /* access modifiers changed from: protected */
    public final LoadingLayout getFooterLayout() {
        return this.mFooterLayout;
    }

    /* access modifiers changed from: protected */
    public final int getFooterSize() {
        return this.mFooterLayout.getContentSize();
    }

    /* access modifiers changed from: protected */
    public final LoadingLayout getHeaderLayout() {
        return this.mHeaderLayout;
    }

    /* access modifiers changed from: protected */
    public final int getHeaderSize() {
        return this.mHeaderLayout.getContentSize();
    }

    /* access modifiers changed from: protected */
    public FrameLayout getRefreshableViewWrapper() {
        return this.mRefreshableViewWrapper;
    }

    /* access modifiers changed from: protected */
    public void onPullToRefresh() {
        int i = C27404.f5269x3fa7fba6[this.mCurrentMode.ordinal()];
        if (i == 1) {
            this.mFooterLayout.pullToRefresh();
        } else if (i == 2) {
            this.mHeaderLayout.pullToRefresh();
        }
    }

    /* access modifiers changed from: protected */
    public void onRefreshing(boolean z) {
        if (this.mMode.showHeaderLoadingLayout()) {
            this.mHeaderLayout.refreshing();
        }
        if (this.mMode.showFooterLoadingLayout()) {
            this.mFooterLayout.refreshing();
        }
        if (!z) {
            callRefreshListener();
        } else if (this.mShowViewWhileRefreshing) {
            C27371 r3 = new OnSmoothScrollFinishedListener() {
                /* class com.wade.fit.views.pullscrollview.PullToRefreshBase.C27371 */

                public void onSmoothScrollFinished() {
                    PullToRefreshBase.this.callRefreshListener();
                }
            };
            int i = C27404.f5269x3fa7fba6[this.mCurrentMode.ordinal()];
            if (i == 1 || i == 3) {
                smoothScrollTo(getFooterSize(), r3);
            } else {
                smoothScrollTo(-getHeaderSize(), r3);
            }
        } else {
            smoothScrollTo(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onReleaseToRefresh() {
        int i = C27404.f5269x3fa7fba6[this.mCurrentMode.ordinal()];
        if (i == 1) {
            this.mFooterLayout.releaseToRefresh();
        } else if (i == 2) {
            this.mHeaderLayout.releaseToRefresh();
        }
    }

    /* access modifiers changed from: protected */
    public void onReset() {
        this.mIsBeingDragged = false;
        this.mLayoutVisibilityChangesEnabled = true;
        this.mHeaderLayout.reset();
        this.mFooterLayout.reset();
        smoothScrollTo(0);
    }

    /* access modifiers changed from: protected */
    public final void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            setMode(Mode.mapIntToValue(bundle.getInt(STATE_MODE, 0)));
            this.mCurrentMode = Mode.mapIntToValue(bundle.getInt(STATE_CURRENT_MODE, 0));
            this.mScrollingWhileRefreshingEnabled = bundle.getBoolean(STATE_SCROLLING_REFRESHING_ENABLED, false);
            this.mShowViewWhileRefreshing = bundle.getBoolean(STATE_SHOW_REFRESHING_VIEW, true);
            super.onRestoreInstanceState(bundle.getParcelable(STATE_SUPER));
            State mapIntToValue = State.mapIntToValue(bundle.getInt(STATE_STATE, 0));
            if (mapIntToValue == State.REFRESHING || mapIntToValue == State.MANUAL_REFRESHING) {
                setState(mapIntToValue, true);
            }
            onPtrRestoreInstanceState(bundle);
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public final Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        onPtrSaveInstanceState(bundle);
        bundle.putInt(STATE_STATE, this.mState.getIntValue());
        bundle.putInt(STATE_MODE, this.mMode.getIntValue());
        bundle.putInt(STATE_CURRENT_MODE, this.mCurrentMode.getIntValue());
        bundle.putBoolean(STATE_SCROLLING_REFRESHING_ENABLED, this.mScrollingWhileRefreshingEnabled);
        bundle.putBoolean(STATE_SHOW_REFRESHING_VIEW, this.mShowViewWhileRefreshing);
        bundle.putParcelable(STATE_SUPER, super.onSaveInstanceState());
        return bundle;
    }

    /* access modifiers changed from: protected */
    public final void onSizeChanged(int i, int i2, int i3, int i4) {
        Log.d(LOG_TAG, String.format("onSizeChanged. W: %d, H: %d", Integer.valueOf(i), Integer.valueOf(i2)));
        super.onSizeChanged(i, i2, i3, i4);
        refreshLoadingViewsSize();
        refreshRefreshableViewSize(i, i2);
        post(new Runnable() {
            /* class com.wade.fit.views.pullscrollview.PullToRefreshBase.C27382 */

            public void run() {
                PullToRefreshBase.this.requestLayout();
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void refreshLoadingViewsSize() {
        int maximumPullScroll = (int) (((float) getMaximumPullScroll()) * 1.2f);
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int i = C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()];
        if (i == 1) {
            if (this.mMode.showHeaderLoadingLayout()) {
                this.mHeaderLayout.setWidth(maximumPullScroll);
                paddingLeft = -maximumPullScroll;
            } else {
                paddingLeft = 0;
            }
            if (this.mMode.showFooterLoadingLayout()) {
                this.mFooterLayout.setWidth(maximumPullScroll);
                paddingRight = -maximumPullScroll;
            } else {
                paddingRight = 0;
            }
        } else if (i == 2) {
            if (this.mMode.showHeaderLoadingLayout()) {
                this.mHeaderLayout.setHeight(maximumPullScroll);
                paddingTop = -maximumPullScroll;
            } else {
                paddingTop = 0;
            }
            if (this.mMode.showFooterLoadingLayout()) {
                this.mFooterLayout.setHeight(maximumPullScroll);
                paddingBottom = -maximumPullScroll;
            } else {
                paddingBottom = 0;
            }
        }
        Log.d(LOG_TAG, String.format("Setting Padding. L: %d, T: %d, R: %d, B: %d", Integer.valueOf(paddingLeft), Integer.valueOf(paddingTop), Integer.valueOf(paddingRight), Integer.valueOf(paddingBottom)));
        setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    /* access modifiers changed from: protected */
    public final void refreshRefreshableViewSize(int i, int i2) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.mRefreshableViewWrapper.getLayoutParams();
        int i3 = C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()];
        if (i3 != 1) {
            if (i3 == 2 && layoutParams.height != i2) {
                layoutParams.height = i2;
                this.mRefreshableViewWrapper.requestLayout();
            }
        } else if (layoutParams.width != i) {
            layoutParams.width = i;
            this.mRefreshableViewWrapper.requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public final void setHeaderScroll(int i) {
        Log.d(LOG_TAG, "setHeaderScroll: " + i);
        int maximumPullScroll = getMaximumPullScroll();
        int min = Math.min(maximumPullScroll, Math.max(-maximumPullScroll, i));
        if (this.mLayoutVisibilityChangesEnabled) {
            if (min < 0) {
                this.mHeaderLayout.setVisibility(View.VISIBLE);
            } else if (min > 0) {
                this.mFooterLayout.setVisibility(View.VISIBLE);
            } else {
                this.mHeaderLayout.setVisibility(View.INVISIBLE);
                this.mFooterLayout.setVisibility(View.INVISIBLE);
            }
        }
        int i2 = C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()];
        if (i2 == 1) {
            scrollTo(min, 0);
        } else if (i2 == 2) {
            scrollTo(0, min);
        }
    }

    /* access modifiers changed from: protected */
    public final void smoothScrollTo(int i) {
        smoothScrollTo(i, (long) getPullToRefreshScrollDuration());
    }

    /* access modifiers changed from: protected */
    public final void smoothScrollTo(int i, OnSmoothScrollFinishedListener onSmoothScrollFinishedListener) {
        smoothScrollTo(i, (long) getPullToRefreshScrollDuration(), 0, onSmoothScrollFinishedListener);
    }

    /* access modifiers changed from: protected */
    public final void smoothScrollToLonger(int i) {
        smoothScrollTo(i, (long) getPullToRefreshScrollDurationLonger());
    }

    /* access modifiers changed from: protected */
    public void updateUIForMode() {
        LinearLayout.LayoutParams loadingLayoutLayoutParams = getLoadingLayoutLayoutParams();
        if (this == this.mHeaderLayout.getParent()) {
            removeView(this.mHeaderLayout);
        }
        if (this.mMode.showHeaderLoadingLayout()) {
            addViewInternal(this.mHeaderLayout, 0, loadingLayoutLayoutParams);
        }
        if (this == this.mFooterLayout.getParent()) {
            removeView(this.mFooterLayout);
        }
        if (this.mMode.showFooterLoadingLayout()) {
            addViewInternal(this.mFooterLayout, loadingLayoutLayoutParams);
        }
        refreshLoadingViewsSize();
        this.mCurrentMode = this.mMode != Mode.BOTH ? this.mMode : Mode.PULL_FROM_START;
    }

    private void addRefreshableView(Context context, T t) {
        this.mRefreshableViewWrapper = new FrameLayout(context);
        this.mRefreshableViewWrapper.addView(t, -1, -1);
        addViewInternal(this.mRefreshableViewWrapper, new LinearLayout.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: private */
    public void callRefreshListener() {
        OnRefreshListener<T> onRefreshListener = this.mOnRefreshListener;
        if (onRefreshListener != null) {
            onRefreshListener.onRefresh(this);
        } else if (this.mOnRefreshListener2 == null) {
        } else {
            if (this.mCurrentMode == Mode.PULL_FROM_START) {
                this.mOnRefreshListener2.onPullDownToRefresh(this);
            } else if (this.mCurrentMode == Mode.PULL_FROM_END) {
                this.mOnRefreshListener2.onPullUpToRefresh(this);
            }
        }
    }

    private void init(Context context, AttributeSet attributeSet) {
        if (C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()] != 1) {
            setOrientation(1);
        } else {
            setOrientation(0);
        }
        setGravity(17);
        this.mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.PullToRefresh);
        if (obtainStyledAttributes.hasValue(12)) {
            this.mMode = Mode.mapIntToValue(obtainStyledAttributes.getInteger(12, 0));
        }
        if (obtainStyledAttributes.hasValue(1)) {
            this.mLoadingAnimationStyle = AnimationStyle.mapIntToValue(obtainStyledAttributes.getInteger(1, 0));
        }
        this.mRefreshableView = createRefreshableView(context, attributeSet);
        addRefreshableView(context, this.mRefreshableView);
        this.mHeaderLayout = createLoadingLayout(context, Mode.PULL_FROM_START, obtainStyledAttributes);
        this.mFooterLayout = createLoadingLayout(context, Mode.PULL_FROM_END, obtainStyledAttributes);
        if (obtainStyledAttributes.hasValue(14)) {
            Drawable drawable = obtainStyledAttributes.getDrawable(14);
            if (drawable != null) {
                this.mRefreshableView.setBackgroundDrawable(drawable);
            }
        } else if (obtainStyledAttributes.hasValue(0)) {
            Utils.warnDeprecation("ptrAdapterViewBackground", "ptrRefreshableViewBackground");
            Drawable drawable2 = obtainStyledAttributes.getDrawable(0);
            if (drawable2 != null) {
                this.mRefreshableView.setBackgroundDrawable(drawable2);
            }
        }
        if (obtainStyledAttributes.hasValue(13)) {
            this.mOverScrollEnabled = obtainStyledAttributes.getBoolean(13, true);
        }
        if (obtainStyledAttributes.hasValue(16)) {
            this.mScrollingWhileRefreshingEnabled = obtainStyledAttributes.getBoolean(16, false);
        }
        handleStyledAttributes(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        updateUIForMode();
    }

    private boolean isReadyForPull() {
        int i = C27404.f5269x3fa7fba6[this.mMode.ordinal()];
        if (i == 1) {
            return isReadyForPullEnd();
        }
        if (i == 2) {
            return isReadyForPullStart();
        }
        if (i != 4) {
            return false;
        }
        if (isReadyForPullEnd() || isReadyForPullStart()) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void pullEvent() {
        float f;
        float f2;
        int i;
        int i2;
        if (C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()] != 1) {
            f2 = this.mInitialMotionY;
            f = this.mLastMotionY;
        } else {
            f2 = this.mInitialMotionX;
            f = this.mLastMotionX;
        }
        if (C27404.f5269x3fa7fba6[this.mCurrentMode.ordinal()] != 1) {
            i2 = Math.round(Math.min(f2 - f, 0.0f) / FRICTION);
            i = getHeaderSize();
        } else {
            i2 = Math.round(Math.max(f2 - f, 0.0f) / FRICTION);
            i = getFooterSize();
        }
        setHeaderScroll(i2);
        if (i2 != 0 && !isRefreshing()) {
            float abs = ((float) Math.abs(i2)) / ((float) i);
            if (C27404.f5269x3fa7fba6[this.mCurrentMode.ordinal()] != 1) {
                this.mHeaderLayout.onPull(abs);
            } else {
                this.mFooterLayout.onPull(abs);
            }
            if (this.mState != State.PULL_TO_REFRESH && i >= Math.abs(i2)) {
                setState(State.PULL_TO_REFRESH, new boolean[0]);
            } else if (this.mState == State.PULL_TO_REFRESH && i < Math.abs(i2)) {
                setState(State.RELEASE_TO_REFRESH, new boolean[0]);
            }
        }
    }

    private LinearLayout.LayoutParams getLoadingLayoutLayoutParams() {
        if (C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()] != 1) {
            return new LinearLayout.LayoutParams(-1, -2);
        }
        return new LinearLayout.LayoutParams(-2, -1);
    }

    private int getMaximumPullScroll() {
        if (C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()] != 1) {
            return Math.round(((float) getHeight()) / FRICTION);
        }
        return Math.round(((float) getWidth()) / FRICTION);
    }

    private final void smoothScrollTo(int i, long j) {
        smoothScrollTo(i, j, 0, null);
    }

    /* access modifiers changed from: private */
    public final void smoothScrollTo(int i, long j, long j2, OnSmoothScrollFinishedListener onSmoothScrollFinishedListener) {
        int scrollX;
        PullToRefreshBase<T>.SmoothScrollRunnable smoothScrollRunnable = this.mCurrentSmoothScrollRunnable;
        if (smoothScrollRunnable != null) {
            smoothScrollRunnable.stop();
        }
        if (C27404.f5270xbbe37a6d[getPullToRefreshScrollDirection().ordinal()] != 1) {
            scrollX = getScrollY();
        } else {
            scrollX = getScrollX();
        }
        int i2 = scrollX;
        if (i2 != i) {
            if (this.mScrollAnimationInterpolator == null) {
                this.mScrollAnimationInterpolator = new DecelerateInterpolator();
            }
            this.mCurrentSmoothScrollRunnable = new SmoothScrollRunnable(i2, i, j, onSmoothScrollFinishedListener);
            if (j2 > 0) {
                postDelayed(this.mCurrentSmoothScrollRunnable, j2);
            } else {
                post(this.mCurrentSmoothScrollRunnable);
            }
        }
    }

    private final void smoothScrollToAndBack(int i) {
        smoothScrollTo(i, 200, 0, new OnSmoothScrollFinishedListener() {
            /* class com.wade.fit.views.pullscrollview.PullToRefreshBase.C27393 */

            public void onSmoothScrollFinished() {
                PullToRefreshBase.this.smoothScrollTo(0, 200, 225, null);
            }
        });
    }

    public enum AnimationStyle {
        ROTATE,
        FLIP;

        static AnimationStyle getDefault() {
            return ROTATE;
        }

        static AnimationStyle mapIntToValue(int i) {
            if (i != 1) {
                return ROTATE;
            }
            return FLIP;
        }

        /* access modifiers changed from: package-private */
        public LoadingLayout createLoadingLayout(Context context, Mode mode, Orientation orientation, TypedArray typedArray) {
            if (C27404.f5268x35adfed0[ordinal()] != 2) {
                return new RotateLoadingLayout(context, mode, orientation, typedArray);
            }
            return new FlipLoadingLayout(context, mode, orientation, typedArray);
        }
    }

    /* renamed from: com.wade.fit.views.pullscrollview.PullToRefreshBase$4 */
    static /* synthetic */ class C27404 {

        /* renamed from: $SwitchMap$com$wade.fit$views$pullscrollview$PullToRefreshBase$AnimationStyle */
        static final /* synthetic */ int[] f5268x35adfed0 = new int[AnimationStyle.values().length];

        /* renamed from: $SwitchMap$com$wade.fit$views$pullscrollview$PullToRefreshBase$Mode */
        static final /* synthetic */ int[] f5269x3fa7fba6 = new int[Mode.values().length];

        /* renamed from: $SwitchMap$com$wade.fit$views$pullscrollview$PullToRefreshBase$Orientation */
        static final /* synthetic */ int[] f5270xbbe37a6d = new int[Orientation.values().length];

        /* renamed from: $SwitchMap$com$wade.fit$views$pullscrollview$PullToRefreshBase$State */
        static final /* synthetic */ int[] f5271xb5ae42ee = new int[State.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|(2:1|2)|3|(2:5|6)|7|9|10|(2:11|12)|13|(2:15|16)|17|(2:19|20)|21|23|24|25|26|27|28|29|30|31|32|(2:33|34)|35|37|38|(3:39|40|42)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(33:0|1|2|3|(2:5|6)|7|9|10|(2:11|12)|13|15|16|17|(2:19|20)|21|23|24|25|26|27|28|29|30|31|32|33|34|35|37|38|39|40|42) */
        /* JADX WARNING: Can't wrap try/catch for region: R(34:0|1|2|3|(2:5|6)|7|9|10|(2:11|12)|13|15|16|17|19|20|21|23|24|25|26|27|28|29|30|31|32|33|34|35|37|38|39|40|42) */
        /* JADX WARNING: Can't wrap try/catch for region: R(35:0|1|2|3|5|6|7|9|10|(2:11|12)|13|15|16|17|19|20|21|23|24|25|26|27|28|29|30|31|32|33|34|35|37|38|39|40|42) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0065 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x006f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0079 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0083 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x008e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00ac */
        static {
            /*
                com.wade.fit.views.pullscrollview.PullToRefreshBase$AnimationStyle[] r0 = com.wade.fit.views.pullscrollview.PullToRefreshBase.AnimationStyle.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5268x35adfed0 = r0
                r0 = 1
                int[] r1 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5268x35adfed0     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$AnimationStyle r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.AnimationStyle.ROTATE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5268x35adfed0     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$AnimationStyle r3 = com.wade.fit.views.pullscrollview.PullToRefreshBase.AnimationStyle.FLIP     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Mode[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Mode.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5269x3fa7fba6 = r2
                int[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5269x3fa7fba6     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Mode r3 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Mode.PULL_FROM_END     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                int[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5269x3fa7fba6     // Catch:{ NoSuchFieldError -> 0x003c }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Mode r3 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Mode.PULL_FROM_START     // Catch:{ NoSuchFieldError -> 0x003c }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
            L_0x003c:
                r2 = 3
                int[] r3 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5269x3fa7fba6     // Catch:{ NoSuchFieldError -> 0x0047 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Mode r4 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Mode.MANUAL_REFRESH_ONLY     // Catch:{ NoSuchFieldError -> 0x0047 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0047 }
            L_0x0047:
                r3 = 4
                int[] r4 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5269x3fa7fba6     // Catch:{ NoSuchFieldError -> 0x0052 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Mode r5 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Mode.BOTH     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                com.wade.fit.views.pullscrollview.PullToRefreshBase$State[] r4 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5271xb5ae42ee = r4
                int[] r4 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5271xb5ae42ee     // Catch:{ NoSuchFieldError -> 0x0065 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$State r5 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.RESET     // Catch:{ NoSuchFieldError -> 0x0065 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0065 }
                r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0065 }
            L_0x0065:
                int[] r4 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5271xb5ae42ee     // Catch:{ NoSuchFieldError -> 0x006f }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$State r5 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.PULL_TO_REFRESH     // Catch:{ NoSuchFieldError -> 0x006f }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x006f }
                r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x006f }
            L_0x006f:
                int[] r4 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5271xb5ae42ee     // Catch:{ NoSuchFieldError -> 0x0079 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$State r5 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.RELEASE_TO_REFRESH     // Catch:{ NoSuchFieldError -> 0x0079 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0079 }
                r4[r5] = r2     // Catch:{ NoSuchFieldError -> 0x0079 }
            L_0x0079:
                int[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5271xb5ae42ee     // Catch:{ NoSuchFieldError -> 0x0083 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$State r4 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.REFRESHING     // Catch:{ NoSuchFieldError -> 0x0083 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0083 }
                r2[r4] = r3     // Catch:{ NoSuchFieldError -> 0x0083 }
            L_0x0083:
                int[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5271xb5ae42ee     // Catch:{ NoSuchFieldError -> 0x008e }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$State r3 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.MANUAL_REFRESHING     // Catch:{ NoSuchFieldError -> 0x008e }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x008e }
                r4 = 5
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x008e }
            L_0x008e:
                int[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5271xb5ae42ee     // Catch:{ NoSuchFieldError -> 0x0099 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$State r3 = com.wade.fit.views.pullscrollview.PullToRefreshBase.State.OVERSCROLLING     // Catch:{ NoSuchFieldError -> 0x0099 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0099 }
                r4 = 6
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x0099 }
            L_0x0099:
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Orientation[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Orientation.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5270xbbe37a6d = r2
                int[] r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5270xbbe37a6d     // Catch:{ NoSuchFieldError -> 0x00ac }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Orientation r3 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Orientation.HORIZONTAL     // Catch:{ NoSuchFieldError -> 0x00ac }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ac }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x00ac }
            L_0x00ac:
                int[] r0 = com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.f5270xbbe37a6d     // Catch:{ NoSuchFieldError -> 0x00b6 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Orientation r2 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Orientation.VERTICAL     // Catch:{ NoSuchFieldError -> 0x00b6 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b6 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x00b6 }
            L_0x00b6:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.pullscrollview.PullToRefreshBase.C27404.<clinit>():void");
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class Mode extends Enum<Mode> {
        private static final /* synthetic */ Mode[] $VALUES;
        public static final Mode BOTH = new Mode("BOTH", 3, 3);
        public static final Mode DISABLED = new Mode("DISABLED", 0, 0);
        public static final Mode MANUAL_REFRESH_ONLY = new Mode("MANUAL_REFRESH_ONLY", 4, 4);
        public static Mode PULL_DOWN_TO_REFRESH;
        public static final Mode PULL_FROM_END = new Mode("PULL_FROM_END", 2, 2);
        public static final Mode PULL_FROM_START = new Mode("PULL_FROM_START", 1, 1);
        public static Mode PULL_UP_TO_REFRESH;
        private int mIntValue;

        public static Mode valueOf(String str) {
            return (Mode) Enum.valueOf(Mode.class, str);
        }

        public static Mode[] values() {
            return (Mode[]) $VALUES.clone();
        }

        static {
            Mode mode = PULL_FROM_START;
            Mode mode2 = PULL_FROM_END;
            $VALUES = new Mode[]{DISABLED, mode, mode2, BOTH, MANUAL_REFRESH_ONLY};
            PULL_DOWN_TO_REFRESH = mode;
            PULL_UP_TO_REFRESH = mode2;
        }

        static Mode mapIntToValue(int i) {
            Mode[] values = values();
            for (Mode mode : values) {
                if (i == mode.getIntValue()) {
                    return mode;
                }
            }
            return getDefault();
        }

        static Mode getDefault() {
            return PULL_FROM_START;
        }

        private Mode(String str, int i, int i2) {
            this.mIntValue = i2;
        }

        /* access modifiers changed from: package-private */
        public boolean permitsPullToRefresh() {
            return (this == DISABLED || this == MANUAL_REFRESH_ONLY) ? false : true;
        }

        public boolean showHeaderLoadingLayout() {
            return this == PULL_FROM_START || this == BOTH;
        }

        public boolean showFooterLoadingLayout() {
            return this == PULL_FROM_END || this == BOTH || this == MANUAL_REFRESH_ONLY;
        }

        /* access modifiers changed from: package-private */
        public int getIntValue() {
            return this.mIntValue;
        }
    }

    public enum State {
        RESET(0),
        PULL_TO_REFRESH(1),
        RELEASE_TO_REFRESH(2),
        REFRESHING(8),
        MANUAL_REFRESHING(9),
        OVERSCROLLING(16);
        
        private int mIntValue;

        static State mapIntToValue(int i) {
            State[] values = values();
            for (State state : values) {
                if (i == state.getIntValue()) {
                    return state;
                }
            }
            return RESET;
        }

        private State(int i) {
            this.mIntValue = i;
        }

        /* access modifiers changed from: package-private */
        public int getIntValue() {
            return this.mIntValue;
        }
    }

    final class SmoothScrollRunnable implements Runnable {
        private boolean mContinueRunning = true;
        private int mCurrentY = -1;
        private final long mDuration;
        private final Interpolator mInterpolator;
        private OnSmoothScrollFinishedListener mListener;
        private final int mScrollFromY;
        private final int mScrollToY;
        private long mStartTime = -1;

        public SmoothScrollRunnable(int i, int i2, long j, OnSmoothScrollFinishedListener onSmoothScrollFinishedListener) {
            this.mScrollFromY = i;
            this.mScrollToY = i2;
            this.mInterpolator = PullToRefreshBase.this.mScrollAnimationInterpolator;
            this.mDuration = j;
            this.mListener = onSmoothScrollFinishedListener;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        public void run() {
            if (this.mStartTime == -1) {
                this.mStartTime = System.currentTimeMillis();
            } else {
                this.mCurrentY = this.mScrollFromY - Math.round(((float) (this.mScrollFromY - this.mScrollToY)) * this.mInterpolator.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.mStartTime) * 1000) / this.mDuration, 1000L), 0L)) / 1000.0f));
                PullToRefreshBase.this.setHeaderScroll(this.mCurrentY);
            }
            if (!this.mContinueRunning || this.mScrollToY == this.mCurrentY) {
                OnSmoothScrollFinishedListener onSmoothScrollFinishedListener = this.mListener;
                if (onSmoothScrollFinishedListener != null) {
                    onSmoothScrollFinishedListener.onSmoothScrollFinished();
                    return;
                }
                return;
            }
            ViewCompat.postOnAnimation(PullToRefreshBase.this, this);
        }

        public void stop() {
            this.mContinueRunning = false;
            PullToRefreshBase.this.removeCallbacks(this);
        }
    }
}
