package com.wade.fit.persenter.main;

import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.http.LoginHttp;
import com.wade.fit.persenter.main.LoginContract;
import com.wade.fit.util.Md5Util;
import com.tamic.novate.util.Utils;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import p041io.reactivex.disposables.Disposable;

public class LoginPresenter extends BasePersenter<LoginContract.View> implements LoginContract.Presenter {
    public void requestLogin(String str, String str2) {
        LoginHttp.loginByEmail(RequestBody.create(MediaType.parse(Utils.MULTIPART_JSON_DATA), "{\"email\":\"" + str + "\",\"password\":\"" + Md5Util.stringToMD5(str2) + "\"}"), new ApiCallback<BaseBean<UserBean>>() {
            /* class com.wade.fit.persenter.main.LoginPresenter.C23971 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<UserBean> baseBean) {
                ((LoginContract.View) LoginPresenter.this.mView).LoginSuccess(baseBean.getCode(), baseBean);
            }

            public void onFailure(int i, String str) {
                ((LoginContract.View) LoginPresenter.this.mView).loginFaild(i);
                ((LoginContract.View) LoginPresenter.this.mView).hideLoading();
            }

            public void onSubscribe(Disposable disposable) {
                LoginPresenter.this.addDisposable(disposable);
            }
        });
    }

    public void requestLoginByWechat(String str, String str2) {
        LoginHttp.loginByWechat(RequestBody.create(MediaType.parse(Utils.MULTIPART_JSON_DATA), "{\"wxOpenid\":\"" + str + "\",\"accessToken\":\"" + str2 + "\"}"), new ApiCallback<BaseBean<UserBean>>() {
            /* class com.wade.fit.persenter.main.LoginPresenter.C23982 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<UserBean> baseBean) {
                ((LoginContract.View) LoginPresenter.this.mView).LoginSuccess(baseBean.getCode(), baseBean);
            }

            public void onFailure(int i, String str) {
                ((LoginContract.View) LoginPresenter.this.mView).loginFaild(i);
                ((LoginContract.View) LoginPresenter.this.mView).hideLoading();
            }

            public void onSubscribe(Disposable disposable) {
                LoginPresenter.this.addDisposable(disposable);
            }
        });
    }

    public void loginByFacebook(String str, String str2) {
        LoginHttp.loginByFacebook(RequestBody.create(MediaType.parse(Utils.MULTIPART_JSON_DATA), "{\"facebookId\":\"" + str + "\",\"accessToken\":\"" + str2 + "\"}"), new ApiCallback<BaseBean<UserBean>>() {
            /* class com.wade.fit.persenter.main.LoginPresenter.C23993 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<UserBean> baseBean) {
                ((LoginContract.View) LoginPresenter.this.mView).LoginSuccess(baseBean.getCode(), baseBean);
            }

            public void onFailure(int i, String str) {
                ((LoginContract.View) LoginPresenter.this.mView).loginFaild(i);
                ((LoginContract.View) LoginPresenter.this.mView).hideLoading();
            }

            public void onSubscribe(Disposable disposable) {
                LoginPresenter.this.addDisposable(disposable);
            }
        });
    }

    public void sendCode(String str) {
        ((LoginContract.View) this.mView).showLoadingFalse();
        LoginHttp.sendCode(str, new ApiCallback<BaseBean>() {
            /* class com.wade.fit.persenter.main.LoginPresenter.C24004 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean baseBean) {
                ((LoginContract.View) LoginPresenter.this.mView).hideLoading();
                ((LoginContract.View) LoginPresenter.this.mView).sendCodeSuccess();
            }

            public void onFailure(int i, String str) {
                ((LoginContract.View) LoginPresenter.this.mView).hideLoading();
                ((LoginContract.View) LoginPresenter.this.mView).sendCodeFaild(i);
            }

            public void onSubscribe(Disposable disposable) {
                LoginPresenter.this.addDisposable(disposable);
            }
        });
    }

    public void regiestAccount(String str, String str2, String str3) {
        ((LoginContract.View) this.mView).showLoadingFalse();
        LoginHttp.registerAccount(RequestBody.create(MediaType.parse("application/json"), "{\"email\":\"" + str + "\",\"password\":\"" + Md5Util.stringToMD5(str2) + "\",\"verifyCode\":\"" + str3 + "\"}"), new ApiCallback<BaseBean<UserBean>>() {
            /* class com.wade.fit.persenter.main.LoginPresenter.C24015 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<UserBean> baseBean) {
                ((LoginContract.View) LoginPresenter.this.mView).registerSuccess();
            }

            public void onFailure(int i, String str) {
                ((LoginContract.View) LoginPresenter.this.mView).hideLoading();
                ((LoginContract.View) LoginPresenter.this.mView).registerFaild(i);
            }

            public void onSubscribe(Disposable disposable) {
                LoginPresenter.this.addDisposable(disposable);
            }
        });
    }
}
