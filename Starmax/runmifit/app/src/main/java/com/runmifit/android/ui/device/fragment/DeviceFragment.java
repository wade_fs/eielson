package com.wade.fit.ui.device.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseFragment;
import com.wade.fit.greendao.bean.HealthSleepItem;
import com.wade.fit.greendao.gen.HealthSleepItemDao;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.ui.device.activity.CameraActivity;
import com.wade.fit.ui.device.activity.DeviceSetActivity;
import com.wade.fit.ui.device.activity.GoalSetActivity;
import com.wade.fit.ui.device.activity.MusicControlActivity;
import com.wade.fit.ui.device.activity.NoticeActivity;
import com.wade.fit.ui.device.activity.ScanDeviceReadyActivity;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.PermissionUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ToastUtil;
import com.wade.fit.util.ble.BleCallback;
import com.wade.fit.util.ble.BleCallbackWrapper;
import com.wade.fit.util.ble.BleClient;
import com.wade.fit.util.ble.BleScanTool;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.CustomToggleButton;
import com.wade.fit.views.ItemLableValue;
import com.wade.fit.views.ItemToggleLayout;
import com.wade.fit.views.dialog.CommonDialog;
import java.util.Calendar;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.fragment.DeviceFragment */
public class DeviceFragment extends BaseFragment {
    static final int REQUEST_CODE_CAMER_PERMISSION = 1;
    public static final int REQUEST_DEVICE = 1;
    RelativeLayout barBg;
    RelativeLayout barBgTitle;
    private BLEDevice bleDevice;
    private DeviceConfig config;
    ItemLableValue ilFindDevice;
    ItemLableValue ilGoalSet;
    ItemLableValue ilMusicControl;
    ItemLableValue ilPhoto;
    ItemLableValue ilReset;
    ItemToggleLayout ilTimeTest;
    ItemToggleLayout ilUpHand;
    ItemLableValue ilVersion;
    ItemLableValue il_remind_phone;
    ItemLableValue il_setting;
    ImageView ivDevice;
    private Dialog mDialog;
    private String[] permissionsCamer = {"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    View rlBind;
    RelativeLayout rlContent;
    View rlUnbid;
    TextView tvDeviceName;
    TextView tvMac;
    TextView tvPower;
    Button tvUnbind;

    static /* synthetic */ void lambda$bindClick$2(DialogInterface dialogInterface, int i) {
    }

    static /* synthetic */ void lambda$bindDevice$18(DialogInterface dialogInterface, int i) {
    }

    static /* synthetic */ void lambda$null$6(View view) {
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.fragment_device;
    }

    /* access modifiers changed from: package-private */
    public void findDevice() {
        BleSdkWrapper.findPhone(null);
        ToastUtil.showToast(getResources().getString(R.string.find_success));
    }

    private void reSetClick() {
        if (!BleSdkWrapper.isConnected()) {
            showToast(getResources().getString(R.string.disConnected));
        } else if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else {
            new CommonDialog.Builder(getContext()).setMessage((int) R.string.reset_device_title).setLeftButton(R.string.cancel).setRightButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$RVzV0pY6uTtJ4XVxMJJinlod1HE */

                public final void onClick(DialogInterface dialogInterface, int i) {
                    DeviceFragment.this.lambda$reSetClick$0$DeviceFragment(dialogInterface, i);
                }
            }).create().show();
        }
    }

    public /* synthetic */ void lambda$reSetClick$0$DeviceFragment(DialogInterface dialogInterface, int i) {
        BleSdkWrapper.recoverSet(new BleCallbackWrapper() {
            /* class com.wade.fit.ui.device.fragment.DeviceFragment.C24811 */

            public void setSuccess() {
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void bindClick() {
        if (BleSdkWrapper.isBind()) {
            if (AppApplication.getInstance().isSysndata()) {
                showToast(getResources().getString(R.string.server_sync_data));
            } else {
                new CommonDialog.Builder(getContext()).setRightButton((int) R.string.ok_unbind_device, new DialogInterface.OnClickListener() {
                    /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$Tib2GDiIYQffUziz5dMWmpv6uY */

                    public final void onClick(DialogInterface dialogInterface, int i) {
                        DeviceFragment.this.lambda$bindClick$1$DeviceFragment(dialogInterface, i);
                    }
                }).setLeftButton(R.string.cancel).setType(1).setRightTextColor(R.color.red).create().show();
            }
        } else if (!BleScanTool.getInstance().isBluetoothOpen()) {
            LogUtil.dAndSave("扫描设备..蓝牙未打开.", Constants.LOG_PATH);
            new CommonDialog.Builder(getActivity()).isVertical(false).setTitle((int) R.string.fresh_ble_close).setLeftButton((int) R.string.cancel, $$Lambda$DeviceFragment$XciYSPH1Ig_id6UPBdBlz8Tyrio.INSTANCE).setMessage((int) R.string.open_ble_tips).setRightButton((int) R.string.setting, new DialogInterface.OnClickListener() {
                /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$TceDy3tqiL29uvzMhIcxrkqJVo */

                public final void onClick(DialogInterface dialogInterface, int i) {
                    DeviceFragment.this.lambda$bindClick$3$DeviceFragment(dialogInterface, i);
                }
            }).create().show();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("from", "MainActivity");
            IntentUtil.goToActivityForResult(getActivity(), ScanDeviceReadyActivity.class, bundle, 1);
        }
    }

    public /* synthetic */ void lambda$bindClick$1$DeviceFragment(DialogInterface dialogInterface, int i) {
        BleSdkWrapper.unBind();
        this.tvUnbind.setText((int) R.string.unbind);
        deleteSleepData();
        showToast(getResources().getString(R.string.unbind));
        updateUI();
    }

    public /* synthetic */ void lambda$bindClick$3$DeviceFragment(DialogInterface dialogInterface, int i) {
        if (BleScanTool.getInstance().openBluetooth()) {
            Bundle bundle = new Bundle();
            bundle.putString("from", "MainActivity");
            IntentUtil.goToActivityForResult(getActivity(), ScanDeviceReadyActivity.class, bundle, 1);
        }
    }

    private void deleteSleepData() {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(1);
        int i2 = instance.get(5);
        HealthSleepItemDao healthSleepItemDao = AppApplication.getInstance().getDaoSession().getHealthSleepItemDao();
        healthSleepItemDao.queryBuilder().where(HealthSleepItemDao.Properties.Year.eq(Integer.valueOf(i)), HealthSleepItemDao.Properties.Month.eq(Integer.valueOf(instance.get(2) + 1)), HealthSleepItemDao.Properties.Day.eq(Integer.valueOf(i2))).buildDelete().executeDeleteWithoutDetachingEntities();
        instance.set(5, i2 - 1);
        List list = healthSleepItemDao.queryBuilder().where(HealthSleepItemDao.Properties.Year.eq(Integer.valueOf(instance.get(1))), HealthSleepItemDao.Properties.Month.eq(Integer.valueOf(instance.get(2) + 1)), HealthSleepItemDao.Properties.Day.eq(Integer.valueOf(instance.get(5)))).orderDesc(HealthSleepItemDao.Properties.Date).build().list();
        if (list != null && list.size() > 0) {
            for (int size = list.size() - 1; size > list.size() - 19; size--) {
                ((HealthSleepItem) list.get(size)).setSleepStatus(0);
            }
            healthSleepItemDao.insertOrReplaceInTx(list);
        }
    }

    public void initView() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.barBg.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = ScreenUtil.getStatusHeight(getActivity());
        this.barBg.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.barBgTitle.getLayoutParams();
        layoutParams2.width = -1;
        layoutParams2.height = ScreenUtil.getStatusHeight(getActivity());
        this.barBgTitle.setLayoutParams(layoutParams);
        this.config = SPHelper.getDeviceConfig();
        this.bleDevice = SPHelper.getBindBLEDevice();
        this.ilTimeTest.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$fG6r0iXzohc1g6FsJWfFJjpNB4 */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                DeviceFragment.this.lambda$initView$4$DeviceFragment(itemToggleLayout, z);
            }
        });
        this.ilTimeTest.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$2fc3l4JAIr4Zsyb7smWVz750lT0 */

            public final boolean handerEvent() {
                return DeviceFragment.this.lambda$initView$5$DeviceFragment();
            }
        });
        this.ilUpHand.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$cNNWYAYVnQVRiqlx6IfnzbGzfkc */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                DeviceFragment.this.lambda$initView$7$DeviceFragment(itemToggleLayout, z);
            }
        });
        this.ilUpHand.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$FcsiRa_xvebycZYzMyHX4euqlY */

            public final boolean handerEvent() {
                return DeviceFragment.this.lambda$initView$8$DeviceFragment();
            }
        });
        this.ilPhoto.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$76i6qn3YFyUQusQzzvAKddWfIds */

            public final void onClick(View view) {
                DeviceFragment.this.lambda$initView$9$DeviceFragment(view);
            }
        });
        this.ilReset.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$y5xXnebu6dL4tv39W2ZPFEqNlWE */

            public final void onClick(View view) {
                DeviceFragment.this.lambda$initView$10$DeviceFragment(view);
            }
        });
        this.il_remind_phone.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$kDaYOlOGt8rt7WYuVSzvR7d_iI */

            public final void onClick(View view) {
                DeviceFragment.this.lambda$initView$11$DeviceFragment(view);
            }
        });
        this.il_setting.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$H7B4qv1AfgwRbjVx5Lx1cyTZI */

            public final void onClick(View view) {
                DeviceFragment.this.lambda$initView$12$DeviceFragment(view);
            }
        });
        this.ilFindDevice.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$MmhyAyW1_1yfEhLyXrzYJ_MdY8M */

            public final void onClick(View view) {
                DeviceFragment.this.lambda$initView$13$DeviceFragment(view);
            }
        });
        this.ilGoalSet.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$IcAavAtb5ebYjf1RbyadlJpwdXo */

            public final void onClick(View view) {
                DeviceFragment.this.lambda$initView$14$DeviceFragment(view);
            }
        });
        this.ilMusicControl.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$9zwrQ78gjpQvqACUkTri1NZWVzA */

            public final void onClick(View view) {
                DeviceFragment.this.lambda$initView$15$DeviceFragment(view);
            }
        });
        this.ilPhoto.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$B_P__2w3n_IMbEdy1zOuxju4Sp0 */

            public final void onClick(View view) {
                DeviceFragment.this.lambda$initView$16$DeviceFragment(view);
            }
        });
        this.rlContent.setPadding(0, ScreenUtil.getStatusHeight(getContext()), 0, 0);
        this.ivDevice.setOnLongClickListener(new View.OnLongClickListener() {
            /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$CcwPcBzrgxSC0Vlw_BQkKSwyQzM */

            public final boolean onLongClick(View view) {
                return DeviceFragment.this.lambda$initView$17$DeviceFragment(view);
            }
        });
    }

    public /* synthetic */ void lambda$initView$4$DeviceFragment(ItemToggleLayout itemToggleLayout, boolean z) {
        DeviceConfig deviceConfig = this.config;
        deviceConfig.isTestTime = z;
        SPHelper.saveDeviceConfig(deviceConfig);
        BleSdkWrapper.setHeartTest(z, null);
    }

    public /* synthetic */ boolean lambda$initView$5$DeviceFragment() {
        if (!BleSdkWrapper.isConnected() || !BleSdkWrapper.isBind()) {
            showToast(getResources().getString(R.string.disConnected));
            return true;
        } else if (!AppApplication.getInstance().isSysndata()) {
            return false;
        } else {
            showToast(getResources().getString(R.string.server_sync_data));
            return true;
        }
    }

    public /* synthetic */ void lambda$initView$7$DeviceFragment(ItemToggleLayout itemToggleLayout, boolean z) {
        if (z) {
            this.config.deviceState.upHander = 1;
            SPHelper.saveDeviceState(this.config.deviceState);
            BleSdkWrapper.setDeviceState(this.config.deviceState, null);
            DialogHelperNew.showTipsDialog(getActivity(), getResources().getString(R.string.tips_title), getResources().getString(R.string.uphandContent), getResources().getString(R.string.dialog_sure), $$Lambda$DeviceFragment$TVw10Ag918tuT2_yZYy0sKzzgqo.INSTANCE);
            return;
        }
        this.config.deviceState.upHander = 0;
        SPHelper.saveDeviceState(this.config.deviceState);
        BleSdkWrapper.setDeviceState(this.config.deviceState, null);
    }

    public /* synthetic */ boolean lambda$initView$8$DeviceFragment() {
        if (!BleSdkWrapper.isConnected() || !BleSdkWrapper.isBind()) {
            showToast(getResources().getString(R.string.disConnected));
            return true;
        } else if (!AppApplication.getInstance().isSysndata()) {
            return false;
        } else {
            showToast(getResources().getString(R.string.server_sync_data));
            return true;
        }
    }

    public /* synthetic */ void lambda$initView$9$DeviceFragment(View view) {
        boolean z = ContextCompat.checkSelfPermission(getActivity(), "android.permission.CAMERA") != 0;
        DebugLog.m6203d("isPermissions:" + z);
        boolean equalsIgnoreCase = Build.MANUFACTURER.equalsIgnoreCase("Meizu");
        if (z || equalsIgnoreCase) {
            requestPermissions(new String[]{"android.permission.CAMERA"}, 3);
        } else {
            IntentUtil.goToActivity(getActivity(), CameraActivity.class);
        }
    }

    public /* synthetic */ void lambda$initView$10$DeviceFragment(View view) {
        if (!ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            reSetClick();
        }
    }

    public /* synthetic */ void lambda$initView$11$DeviceFragment(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            return;
        }
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected()) {
            showToast(getResources().getString(R.string.disConnected));
        } else if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else {
            IntentUtil.goToActivity(getActivity(), NoticeActivity.class);
        }
    }

    public /* synthetic */ void lambda$initView$12$DeviceFragment(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            return;
        }
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected()) {
            showToast(getResources().getString(R.string.disConnected));
        } else if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else {
            IntentUtil.goToActivity(getActivity(), DeviceSetActivity.class);
        }
    }

    public /* synthetic */ void lambda$initView$13$DeviceFragment(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            return;
        }
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected()) {
            showToast(getResources().getString(R.string.disConnected));
        } else if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else {
            findDevice();
        }
    }

    public /* synthetic */ void lambda$initView$14$DeviceFragment(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            return;
        }
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected()) {
            showToast(getResources().getString(R.string.disConnected));
        } else if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else {
            IntentUtil.goToActivity(getActivity(), GoalSetActivity.class);
        }
    }

    public /* synthetic */ void lambda$initView$15$DeviceFragment(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            return;
        }
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected()) {
            showToast(getResources().getString(R.string.disConnected));
        } else if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else {
            IntentUtil.goToActivity(getActivity(), MusicControlActivity.class);
        }
    }

    public /* synthetic */ void lambda$initView$16$DeviceFragment(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            return;
        }
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected()) {
            showToast(getResources().getString(R.string.disConnected));
        } else if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else if (!PermissionUtil.checkSelfPermission(this.permissionsCamer)) {
            PermissionUtil.requestPermissions(this, 1, this.permissionsCamer);
        } else {
            BleSdkWrapper.enterCamare(null);
            IntentUtil.goToActivity(getActivity(), CameraActivity.class);
        }
    }

    public /* synthetic */ boolean lambda$initView$17$DeviceFragment(View view) {
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected()) {
            return false;
        }
        ToastUtil.showToast(getActivity(), this.bleDevice.mDeviceProduct, 5000);
        return false;
    }

    /* access modifiers changed from: private */
    public void updateUI() {
        if (BleSdkWrapper.isBind()) {
            this.bleDevice = SPHelper.getBindBLEDevice();
            this.tvUnbind.setText((int) R.string.unbind);
            if (this.bleDevice.mDeviceName.contains("CS20")) {
                this.ivDevice.setImageResource(R.mipmap.device_img_cs20);
            } else if (this.bleDevice.mDeviceName.contains("C31") || this.bleDevice.mDeviceName.contains("S10")) {
                this.ivDevice.setImageResource(R.mipmap.device_img_c31);
            } else if (this.bleDevice.mDeviceName.contains("S5") || this.bleDevice.mDeviceName.contains("Symphony") || this.bleDevice.mDeviceName.contains("Empress")) {
                this.ivDevice.setImageResource(R.mipmap.device_img_s5);
            } else {
                this.ivDevice.setImageResource(R.mipmap.device_img);
            }
            this.rlBind.setVisibility(View.VISIBLE);
            this.rlUnbid.setVisibility(View.GONE);
            this.tvMac.setText(this.bleDevice.mDeviceAddress);
            if (this.bleDevice.power < 0) {
                this.tvPower.setText(getResources().getText(R.string.charging));
            } else {
                TextView textView = this.tvPower;
                textView.setText(this.bleDevice.power + "%");
            }
            this.tvDeviceName.setText(this.bleDevice.mDeviceName);
            if (!this.config.isDisturbMode) {
                this.il_remind_phone.setValue(getActivity().getResources().getString(R.string.remind_state_close));
            } else {
                this.il_remind_phone.setValue(getActivity().getResources().getString(R.string.remind_state_open));
            }
            this.ilVersion.setValue(this.bleDevice.mDeviceVersion);
        } else {
            this.il_remind_phone.setValue(getActivity().getResources().getString(R.string.remind_state_close));
            this.tvUnbind.setText((int) R.string.bind);
            this.ivDevice.setImageResource(R.mipmap.device_unbind);
            this.rlBind.setVisibility(View.GONE);
            this.rlUnbid.setVisibility(View.VISIBLE);
        }
        this.config = SPHelper.getDeviceConfig();
        if (this.config.isMusic) {
            this.ilMusicControl.setValue(getResources().getString(R.string.remind_state_open));
        } else {
            this.ilMusicControl.setValue(getResources().getString(R.string.remind_state_close));
        }
        boolean z = this.config.isTestTime;
        boolean z2 = true;
        if (this.config.deviceState.upHander != 1) {
            z2 = false;
        }
        this.ilTimeTest.setOpen(z);
        this.ilUpHand.setOpen(z2);
    }

    /* access modifiers changed from: package-private */
    public void bindDevice() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ivDevice, 1000) && !BleSdkWrapper.isBind()) {
            if (!BleScanTool.getInstance().isBluetoothOpen()) {
                LogUtil.dAndSave("扫描设备..蓝牙未打开.", Constants.LOG_PATH);
                new CommonDialog.Builder(getActivity()).isVertical(false).setTitle((int) R.string.fresh_ble_close).setLeftButton((int) R.string.cancel, $$Lambda$DeviceFragment$aJU7JHliwPOJzCY_CljdvSfutE.INSTANCE).setMessage((int) R.string.open_ble_tips).setRightButton((int) R.string.setting, new DialogInterface.OnClickListener() {
                    /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$D3Ta1YXNaJ5MGCXRi00SfqGi0A */

                    public final void onClick(DialogInterface dialogInterface, int i) {
                        DeviceFragment.this.lambda$bindDevice$19$DeviceFragment(dialogInterface, i);
                    }
                }).create().show();
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("from", "MainActivity");
            IntentUtil.goToActivityForResult(getActivity(), ScanDeviceReadyActivity.class, bundle, 1);
        }
    }

    public /* synthetic */ void lambda$bindDevice$19$DeviceFragment(DialogInterface dialogInterface, int i) {
        if (BleScanTool.getInstance().openBluetooth()) {
            Bundle bundle = new Bundle();
            bundle.putString("from", "MainActivity");
            IntentUtil.goToActivityForResult(getActivity(), ScanDeviceReadyActivity.class, bundle, 1);
        }
    }

    /* access modifiers changed from: protected */
    public void onVisiable() {
        super.onVisiable();
        if (Build.VERSION.SDK_INT >= 23) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(1280);
        }
        if (!BleClient.getInstance().isSynching() && BleSdkWrapper.isBind() && !AppApplication.getInstance().isSysndata()) {
            BleSdkWrapper.getPower(new BleCallback() {
                /* class com.wade.fit.ui.device.fragment.DeviceFragment.C24822 */

                public void setSuccess() {
                }

                public void complete(int i, Object obj) {
                    DeviceFragment.this.updateUI();
                }
            });
        }
        updateUI();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (!PermissionUtil.checkSelfPermission(this.permissionsCamer)) {
            PermissionUtil.requestPermissions(this, 1, this.permissionsCamer);
            return;
        }
        BleSdkWrapper.enterCamare(null);
        IntentUtil.goToActivity(getActivity(), CameraActivity.class);
    }

    public void requestPermissionsSuccess(int i) {
        super.requestPermissionsSuccess(i);
        if (i == 1) {
            BleSdkWrapper.enterCamare(null);
            IntentUtil.goToActivity(getActivity(), CameraActivity.class);
        }
    }

    public void requestPermissionsFail(int i) {
        super.requestPermissionsFail(i);
        String[] strArr = this.permissionsCamer;
        if (strArr.length > 0) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), strArr[0])) {
                this.mDialog = DialogHelperNew.showRemindDialog(getActivity(), getResources().getString(R.string.permisson_camera_title), getResources().getString(R.string.permisson_camera_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                    /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$MIP7AuVPHQtsJ0htDA7HVNzptEc */

                    public final void onClick(View view) {
                        DeviceFragment.this.lambda$requestPermissionsFail$20$DeviceFragment(view);
                    }
                }, new View.OnClickListener() {
                    /* class com.wade.fit.ui.device.fragment.$$Lambda$DeviceFragment$MSgqfsQaWjTupcdNUr4Uvi1wKpI */

                    public final void onClick(View view) {
                        DeviceFragment.this.lambda$requestPermissionsFail$21$DeviceFragment(view);
                    }
                });
            }
        }
    }

    public /* synthetic */ void lambda$requestPermissionsFail$20$DeviceFragment(View view) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getActivity().getApplicationContext().getPackageName(), null));
        startActivityForResult(intent, 1);
    }

    public /* synthetic */ void lambda$requestPermissionsFail$21$DeviceFragment(View view) {
        this.mDialog.dismiss();
    }

    public void updateConnect() {
        updateUI();
    }
}
