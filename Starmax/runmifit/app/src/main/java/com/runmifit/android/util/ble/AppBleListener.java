package com.wade.fit.util.ble;

import android.bluetooth.BluetoothGatt;
import com.wade.fit.model.bean.BLEDevice;

public interface AppBleListener {
    void initComplete();

    void onBLEConnectTimeOut();

    void onBLEConnected(BluetoothGatt bluetoothGatt);

    void onBLEConnecting(String str);

    void onBLEDisConnected(String str);

    void onBlueToothError(int i);

    void onDataSendTimeOut(byte[] bArr);

    void onInDfuMode(BLEDevice bLEDevice);

    void onServiceDiscover(BluetoothGatt bluetoothGatt, int i);
}
