package com.wade.fit.base.refresh;

import android.support.v4.internal.view.SupportMenu;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.base.IBaseView;
import com.wade.fit.base.refresh.BaseListPersenter;
import com.wade.fit.base.refresh.BaseRecyclerAdapter;
import com.wade.fit.util.CnWinUtil;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.views.RecyclerRefreshLayout;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseRefreshActivity<Presenter extends BaseListPersenter, Model> extends BaseActivity implements BaseListView<Model>, IBaseView, BaseRecyclerAdapter.OnItemClickListener, RecyclerRefreshLayout.SuperRefreshLayoutListener, BaseRecyclerAdapter.OnCustomClickListener, BaseRecyclerAdapter.RetryOnClickListener {
    protected FrameLayout flBottom;
    protected FrameLayout flTop;
    protected BaseRecyclerAdapter<Model> mAdapter;
    protected Presenter mPresenter;
    protected RecyclerView mRecyclerView;
    protected RecyclerRefreshLayout mRefreshLayout;

    /* access modifiers changed from: protected */
    public abstract BaseRecyclerAdapter<Model> getAdapter();

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.fragment_base_recycler;
    }

    /* access modifiers changed from: protected */
    public void onCustomClick(Model model, int i) {
    }

    /* access modifiers changed from: protected */
    public void onItemClick(Model model, int i) {
    }

    /* access modifiers changed from: protected */
    public void onViewCreate() {
        super.onViewCreate();
        this.mPresenter = (BaseListPersenter) CnWinUtil.getT(this, 0);
        Presenter presenter = this.mPresenter;
        if (presenter != null) {
            presenter.attachView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.mRefreshLayout.setSuperRefreshLayoutListener(this);
        this.mAdapter = getAdapter();
        this.mRecyclerView.setLayoutManager(getLayoutManager());
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mAdapter.setOnItemClickListener(this);
        this.mAdapter.setCustomClickListener(this);
        this.mAdapter.setRetryOnClickListener(this);
        this.mRefreshLayout.setColorSchemeColors(SupportMenu.CATEGORY_MASK, -16711936, -16776961, -16711681);
        this.mRefreshLayout.post(new Runnable() {
            /* class com.wade.fit.base.refresh.$$Lambda$BaseRefreshActivity$_oZ28zZo4Psl96Nb3fMM_JTUksc */

            public final void run() {
                BaseRefreshActivity.this.lambda$initView$0$BaseRefreshActivity();
            }
        });
    }

    public /* synthetic */ void lambda$initView$0$BaseRefreshActivity() {
        this.mRefreshLayout.setRefreshing(true);
        Presenter presenter = this.mPresenter;
        if (presenter != null) {
            presenter.onRefreshing();
        }
    }

    /* access modifiers changed from: protected */
    public void addView(View view) {
        this.flTop.addView(view);
    }

    /* access modifiers changed from: protected */
    public void addBottomView(View view) {
        this.flBottom.addView(view);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onItemClick(int i, long j) {
        Model item = this.mAdapter.getItem(i);
        if (item != null) {
            onItemClick(item, i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.base.refresh.BaseRefreshActivity.onCustomClick(java.lang.Object, int):void
     arg types: [Model, int]
     candidates:
      com.wade.fit.base.refresh.BaseRefreshActivity.onCustomClick(android.view.View, int):void
      com.wade.fit.base.refresh.BaseRecyclerAdapter.OnCustomClickListener.onCustomClick(android.view.View, int):void
      com.wade.fit.base.refresh.BaseRefreshActivity.onCustomClick(java.lang.Object, int):void */
    public void onCustomClick(View view, int i) {
        Model item = this.mAdapter.getItem(i);
        if (item != null) {
            onCustomClick((Object) item, i);
        }
    }

    public void onRefreshing() {
        this.mRefreshLayout.post(new Runnable() {
            /* class com.wade.fit.base.refresh.$$Lambda$BaseRefreshActivity$B5VYuO2pHc8hRa9vzfrngaVNjTw */

            public final void run() {
                BaseRefreshActivity.this.lambda$onRefreshing$1$BaseRefreshActivity();
            }
        });
    }

    public /* synthetic */ void lambda$onRefreshing$1$BaseRefreshActivity() {
        this.mRefreshLayout.setRefreshing(true);
        Presenter presenter = this.mPresenter;
        if (presenter != null) {
            presenter.onRefreshing();
        }
    }

    public void onLoadMore() {
        this.mPresenter.onLoadMore();
        this.mRecyclerView.post(new Runnable() {
            /* class com.wade.fit.base.refresh.$$Lambda$BaseRefreshActivity$ZGEgK_RzVsD0uNAu1cZishCxiMM */

            public final void run() {
                BaseRefreshActivity.this.lambda$onLoadMore$2$BaseRefreshActivity();
            }
        });
    }

    public /* synthetic */ void lambda$onLoadMore$2$BaseRefreshActivity() {
        this.mAdapter.setState(8, true);
    }

    public void onRefreshSuccess(List<Model> list) {
        this.mAdapter.resetItem(list);
    }

    public void onLoadMoreSuccess(List<Model> list) {
        this.mAdapter.addAll(list);
    }

    public void showNoData(int i) {
        this.mAdapter.resetItem(new ArrayList());
        this.mAdapter.setState(1, true, i);
    }

    public void showNoMore() {
        this.mAdapter.setState(1, true);
    }

    public void showNetError(String str) {
        showToast("网络异常");
        this.mAdapter.setState(3, true);
    }

    public void onComplete() {
        this.mRefreshLayout.onComplete();
    }

    public void showMsg(String str) {
        Toast.makeText(this, str, 0).show();
    }

    public void showLoading() {
        DialogHelperNew.buildWaitDialog(this, true);
    }

    public void showLoadingFalse() {
        DialogHelperNew.buildWaitDialog(this, false);
    }

    public void hideLoading() {
        DialogHelperNew.dismissWait();
    }

    public void onDestroy() {
        super.onDestroy();
        Presenter presenter = this.mPresenter;
        if (presenter != null) {
            presenter.detachView();
        }
    }

    public void retryClick() {
        onRefreshing();
    }

    /* access modifiers changed from: protected */
    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    public void goBack() {
        finish();
    }

    public void setCanLoadmore(boolean z) {
        this.mRefreshLayout.setCanLoadMore(z);
    }
}
