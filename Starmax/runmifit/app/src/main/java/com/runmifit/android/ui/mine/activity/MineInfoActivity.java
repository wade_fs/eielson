package com.wade.fit.ui.mine.activity;

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.library.gallery.ImagePickerActivity;
import com.library.gallery.SelectOptions;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.persenter.mine.IMineView;
import com.wade.fit.persenter.mine.MinePresenter;
import com.wade.fit.util.DataVaildUtil;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ToastUtil;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.dialog.WheelViewDialog;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/* renamed from: com.wade.fit.ui.mine.activity.MineInfoActivity */
public class MineInfoActivity extends BaseMvpActivity<MinePresenter> implements IMineView {
    ImageView imgBackground;
    ImageView ivHeader;
    RelativeLayout rlBirthDay;
    RelativeLayout rlHeight;
    RelativeLayout rlSex;
    RelativeLayout rlWeight;
    TextView tvBirthday;
    TextView tvEmail;
    TextView tvHeight;
    TextView tvSex;
    TextView tvUserName;
    TextView tvUserName2;
    TextView tvWeight;
    UserBean userBean;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_mine_info;
    }

    public UserBean getCurrentUserBean() {
        return null;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.person_info));
        this.userBean = AppApplication.getInstance().getUserBean();
        setData(this.userBean);
        this.rightText.setText(getResources().getString(R.string.update_background));
        this.rightText.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$V48oMTS3rnYIPWPl0M2UKY5rB7Y */

            public final void onClick(View view) {
                MineInfoActivity.this.lambda$initView$1$MineInfoActivity(view);
            }
        });
        this.tvUserName.addTextChangedListener(new TextWatcher() {
            /* class com.wade.fit.ui.mine.activity.MineInfoActivity.C25791 */

            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                MineInfoActivity.this.userBean.setUserName(MineInfoActivity.this.tvUserName.getText().toString());
            }
        });
    }

    public /* synthetic */ void lambda$initView$1$MineInfoActivity(View view) {
        ImagePickerActivity.show(this, new SelectOptions.Builder().setHasCam(true).setSelectCount(1).setCallback(new SelectOptions.Callback() {
            /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$diuEtQEmR01fR7tCrKMOkSOPtaw */

            public final void doSelected(String[] strArr) {
                MineInfoActivity.this.lambda$null$0$MineInfoActivity(strArr);
            }
        }).build());
    }

    public /* synthetic */ void lambda$null$0$MineInfoActivity(String[] strArr) {
        if (TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            this.userBean.setCoverImage(strArr[0]);
            getImageLoader().loadImage(this, this.imgBackground, this.userBean.getCoverImage());
            return;
        }
        ((MinePresenter) this.mPresenter).uploadImage(getImage(strArr[0]), 2);
    }

    private void setData(UserBean userBean2) {
        int i;
        Resources resources;
        StringBuilder sb;
        int i2;
        Resources resources2;
        StringBuilder sb2;
        this.tvSex.setText(userBean2.getGender() == 0 ? R.string.person_sex_boy : R.string.person_sex_girl);
        this.tvBirthday.setText(userBean2.getBirthday());
        TextView textView = this.tvHeight;
        if (BleSdkWrapper.isDistUnitKm()) {
            sb = new StringBuilder();
            sb.append(userBean2.getHeight());
            sb.append(" ");
            resources = getResources();
            i = R.string.unit_cm_2;
        } else {
            sb = new StringBuilder();
            sb.append(userBean2.getHeightLb() / 12);
            sb.append(" ");
            sb.append(getResources().getString(R.string.unit_feet));
            sb.append(" ");
            sb.append(userBean2.getHeightLb() % 12);
            sb.append(" ");
            resources = getResources();
            i = R.string.unit_inch;
        }
        sb.append(resources.getString(i));
        textView.setText(sb.toString());
        TextView textView2 = this.tvWeight;
        if (BleSdkWrapper.isDistUnitKm()) {
            sb2 = new StringBuilder();
            sb2.append(userBean2.getWeight());
            sb2.append(" ");
            resources2 = getResources();
            i2 = R.string.unit_kg;
        } else {
            sb2 = new StringBuilder();
            sb2.append(userBean2.getWeightLb());
            sb2.append(" ");
            resources2 = getResources();
            i2 = R.string.unit_lbs;
        }
        sb2.append(resources2.getString(i2));
        textView2.setText(sb2.toString());
        if (TextUtils.isEmpty(AppApplication.getInstance().getUserBean().getUserName())) {
            this.tvUserName.setText(getResources().getString(R.string.nosetting));
            this.tvUserName2.setText(getResources().getString(R.string.nosetting));
        } else {
            this.tvUserName.setText(AppApplication.getInstance().getUserBean().getUserName());
            this.tvUserName2.setText(AppApplication.getInstance().getUserBean().getUserName());
        }
        this.tvEmail.setText(userBean2.getAccount());
        getImageLoader().loadPortrait(this, this.ivHeader, userBean2.getHeaderUrl());
        getImageLoader().loadImage(this, this.imgBackground, userBean2.getCoverImage());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivHeader /*2131296568*/:
                ImagePickerActivity.show(this, new SelectOptions.Builder().setHasCam(true).setSelectCount(1).setCallback(new SelectOptions.Callback() {
                    /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$RUWMYgIn6CcurLSIowQiDE6uzm0 */

                    public final void doSelected(String[] strArr) {
                        MineInfoActivity.this.lambda$onClick$2$MineInfoActivity(strArr);
                    }
                }).build());
                return;
            case R.id.rlBirthDay /*2131296796*/:
                DialogHelperNew.showWheelBirthDayDialog(this, new int[]{this.userBean.getYear(), this.userBean.getMonth(), this.userBean.getDay()}, new WheelViewDialog.OnSelectClick() {
                    /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$akFBkJgLU2lMxneP_Xv0qyChbEU */

                    public final void onSelect(int i, int i2, int i3) {
                        MineInfoActivity.this.lambda$onClick$4$MineInfoActivity(i, i2, i3);
                    }
                });
                return;
            case R.id.rlHeight /*2131296802*/:
                if (BleSdkWrapper.isDistUnitKm()) {
                    DialogHelperNew.showWheelHeightDialog(this, this.userBean.getHeight(), new WheelViewDialog.OnSelectClick() {
                        /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$ykoYzNLg25GQXfVp6Es40bt3dw */

                        public final void onSelect(int i, int i2, int i3) {
                            MineInfoActivity.this.lambda$onClick$5$MineInfoActivity(i, i2, i3);
                        }
                    });
                    return;
                } else {
                    DialogHelperNew.showWheelHeightMileDialog(this, this.userBean.getHeightLb(), new WheelViewDialog.OnSelectClick() {
                        /* class com.wade.fit.ui.mine.activity.MineInfoActivity.C25802 */

                        public void onSelect(int i, int i2, int i3) {
                            int i4 = i + 1;
                            int i5 = (i4 * 12) + i2;
                            int inch2cm = UnitUtil.inch2cm(i5);
                            MineInfoActivity.this.userBean.setHeightLb(i5);
                            MineInfoActivity.this.userBean.setHeight(inch2cm);
                            ((MinePresenter) MineInfoActivity.this.mPresenter).sendUserToBle(MineInfoActivity.this.userBean);
                            TextView textView = MineInfoActivity.this.tvHeight;
                            textView.setText(i4 + " " + MineInfoActivity.this.getResources().getString(R.string.unit_feet) + " " + i2 + " " + MineInfoActivity.this.getResources().getString(R.string.unit_inch));
                        }
                    });
                    return;
                }
            case R.id.rlSex /*2131296817*/:
                DialogHelperNew.showWheelSexDialog(this, this.userBean.getGender(), new WheelViewDialog.OnSelectClick() {
                    /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$NFtj7j1vTtVc9i436RcqG5XBicc */

                    public final void onSelect(int i, int i2, int i3) {
                        MineInfoActivity.this.lambda$onClick$3$MineInfoActivity(i, i2, i3);
                    }
                });
                return;
            case R.id.rlUserName /*2131296820*/:
                editUserName();
                return;
            case R.id.rlWeight /*2131296821*/:
                DialogHelperNew.showWheelWeightDialog(this, BleSdkWrapper.isDistUnitKm() ? this.userBean.getWeight() : this.userBean.getWeightLb(), new WheelViewDialog.OnSelectClick() {
                    /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$9LMqKCplEGNTWWu_eDE2t624ycU */

                    public final void onSelect(int i, int i2, int i3) {
                        MineInfoActivity.this.lambda$onClick$6$MineInfoActivity(i, i2, i3);
                    }
                });
                return;
            default:
                return;
        }
    }

    public /* synthetic */ void lambda$onClick$2$MineInfoActivity(String[] strArr) {
        if (TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            this.userBean.setHeaderUrl(strArr[0]);
            getImageLoader().loadPortrait(this, this.ivHeader, this.userBean.getHeaderUrl());
            return;
        }
        ((MinePresenter) this.mPresenter).uploadImage(getImage(strArr[0]), 1);
    }

    public /* synthetic */ void lambda$onClick$3$MineInfoActivity(int i, int i2, int i3) {
        this.userBean.setGender(i);
        LogUtil.d("" + AppApplication.getInstance().getUserBean().getGender());
        this.tvSex.setText(this.userBean.getGender() == 0 ? R.string.person_sex_boy : R.string.person_sex_girl);
        ((MinePresenter) this.mPresenter).sendUserToBle(this.userBean);
    }

    public /* synthetic */ void lambda$onClick$4$MineInfoActivity(int i, int i2, int i3) {
        this.userBean.setYear(i);
        this.userBean.setMonth(i2);
        this.userBean.setDay(i3);
        UserBean userBean2 = this.userBean;
        userBean2.setBirthday(i + "-" + i2 + "-" + i3);
        this.tvBirthday.setText(DateUtil.format(this.userBean.getYear(), this.userBean.getMonth(), this.userBean.getDay(), "-"));
        ((MinePresenter) this.mPresenter).sendUserToBle(this.userBean);
    }

    public /* synthetic */ void lambda$onClick$5$MineInfoActivity(int i, int i2, int i3) {
        Resources resources;
        StringBuilder sb;
        int i4;
        if (BleSdkWrapper.isDistUnitKm()) {
            this.userBean.setHeight(i);
            this.userBean.setHeightLb(UnitUtil.cm2inchs(i));
        } else {
            this.userBean.setHeight(UnitUtil.inch2cm(i));
            this.userBean.setHeightLb(i);
        }
        TextView textView = this.tvHeight;
        if (BleSdkWrapper.isDistUnitKm()) {
            sb = new StringBuilder();
            sb.append(this.userBean.getHeight());
            sb.append(" ");
            resources = getResources();
            i4 = R.string.unit_cm_2;
        } else {
            sb = new StringBuilder();
            sb.append(this.userBean.getHeightLb() / 12);
            sb.append(" ");
            sb.append(getResources().getString(R.string.unit_feet));
            sb.append(" ");
            sb.append(this.userBean.getHeightLb() % 12);
            sb.append(" ");
            resources = getResources();
            i4 = R.string.unit_inch;
        }
        sb.append(resources.getString(i4));
        textView.setText(sb.toString());
        ((MinePresenter) this.mPresenter).sendUserToBle(this.userBean);
    }

    public /* synthetic */ void lambda$onClick$6$MineInfoActivity(int i, int i2, int i3) {
        Resources resources;
        StringBuilder sb;
        int i4;
        if (BleSdkWrapper.isDistUnitKm()) {
            this.userBean.setWeight(i * 10);
            this.userBean.setWeightLb(UnitUtil.kg2lb((float) i));
        } else {
            this.userBean.setWeight(UnitUtil.lb2kg((float) i) * 10);
            this.userBean.setWeightLb(i);
        }
        TextView textView = this.tvWeight;
        if (BleSdkWrapper.isDistUnitKm()) {
            sb = new StringBuilder();
            sb.append(this.userBean.getWeight());
            sb.append(" ");
            resources = getResources();
            i4 = R.string.unit_kg;
        } else {
            sb = new StringBuilder();
            sb.append(this.userBean.getWeightLb());
            sb.append(" ");
            resources = getResources();
            i4 = R.string.unit_lbs;
        }
        sb.append(resources.getString(i4));
        textView.setText(sb.toString());
        ((MinePresenter) this.mPresenter).sendUserToBle(this.userBean);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.File getImage(java.lang.String r7) {
        /*
            r6 = this;
            android.graphics.BitmapFactory$Options r0 = new android.graphics.BitmapFactory$Options
            r0.<init>()
            r1 = 1
            r0.inJustDecodeBounds = r1
            android.graphics.BitmapFactory.decodeFile(r7, r0)
            r2 = 0
            r0.inJustDecodeBounds = r2
            int r2 = r0.outWidth
            int r3 = r0.outHeight
            if (r2 <= r3) goto L_0x0021
            float r4 = (float) r2
            r5 = 1144258560(0x44340000, float:720.0)
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 <= 0) goto L_0x0021
            int r2 = r0.outWidth
            float r2 = (float) r2
            float r2 = r2 / r5
        L_0x001f:
            int r2 = (int) r2
            goto L_0x0030
        L_0x0021:
            if (r2 >= r3) goto L_0x002f
            float r2 = (float) r3
            r3 = 1152647168(0x44b40000, float:1440.0)
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 <= 0) goto L_0x002f
            int r2 = r0.outHeight
            float r2 = (float) r2
            float r2 = r2 / r3
            goto L_0x001f
        L_0x002f:
            r2 = 1
        L_0x0030:
            if (r2 > 0) goto L_0x0033
            goto L_0x0034
        L_0x0033:
            r1 = r2
        L_0x0034:
            r0.inSampleSize = r1
            android.graphics.Bitmap r7 = android.graphics.BitmapFactory.decodeFile(r7, r0)
            java.io.File r7 = compressImage(r7)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.ui.mine.activity.MineInfoActivity.getImage(java.lang.String):java.io.File");
    }

    public static File compressImage(Bitmap bitmap) {
        String format = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(System.currentTimeMillis()));
        File file = new File(Environment.getExternalStorageDirectory(), format + ".png");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 100;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        while (byteArrayOutputStream.toByteArray().length / 1024 > 512) {
            byteArrayOutputStream.reset();
            i -= 10;
            bitmap.compress(Bitmap.CompressFormat.JPEG, i, byteArrayOutputStream);
            int length = byteArrayOutputStream.toByteArray().length;
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            try {
                fileOutputStream.write(byteArrayOutputStream.toByteArray());
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        recycleBitmap(bitmap);
        return file;
    }

    public static void recycleBitmap(Bitmap... bitmapArr) {
        if (bitmapArr != null) {
            for (Bitmap bitmap : bitmapArr) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
        }
    }

    private void editUserName() {
        Dialog dialog = new Dialog(this, R.style.center_dialog);
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.dialog_input_username, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.dialogTitle)).setText(getResources().getString(R.string.edit_username));
        EditText editText = (EditText) inflate.findViewById(R.id.etInput);
        editText.setHint(this.userBean.getUserName());
        inflate.findViewById(R.id.tvCanle).setOnClickListener(new View.OnClickListener(dialog) {
            /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$VlStSc8UvXHDskMRBBWrMkHafXQ */
            private final /* synthetic */ Dialog f$0;

            {
                this.f$0 = r1;
            }

            public final void onClick(View view) {
                this.f$0.dismiss();
            }
        });
        inflate.findViewById(R.id.tvSure).setOnClickListener(new View.OnClickListener(dialog, editText) {
            /* class com.wade.fit.ui.mine.activity.$$Lambda$MineInfoActivity$bmj8wimCXwH5Xnr8u6JgPOp2EVc */
            private final /* synthetic */ Dialog f$1;
            private final /* synthetic */ EditText f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                MineInfoActivity.this.lambda$editUserName$8$MineInfoActivity(this.f$1, this.f$2, view);
            }
        });
        dialog.setContentView(inflate);
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        double d = (double) getResources().getDisplayMetrics().widthPixels;
        Double.isNaN(d);
        attributes.width = (int) (d * 0.8d);
        window.setAttributes(attributes);
        dialog.show();
    }

    public /* synthetic */ void lambda$editUserName$8$MineInfoActivity(Dialog dialog, EditText editText, View view) {
        dialog.dismiss();
        if (TextUtils.isEmpty(editText.getText().toString())) {
            ToastUtil.showToast(getResources().getString(R.string.input_username));
        } else if (DataVaildUtil.vaildUserName(editText.getText().toString())) {
            ToastUtil.showToast(getResources().getString(R.string.input_error));
        } else {
            this.tvUserName.setText(editText.getText().toString());
            this.tvUserName2.setText(editText.getText().toString());
            this.userBean.setUserName(editText.getText().toString());
            ((MinePresenter) this.mPresenter).sendUserToBle(this.userBean);
        }
    }

    public void saveUserBean(int i) {
        DialogHelperNew.dismissWait();
        AppApplication.getInstance().setUserBean(this.userBean);
        SPHelper.saveUserBean(this.userBean);
    }

    public void saveFaild() {
        this.userBean = AppApplication.getInstance().getUserBean();
        LogUtil.d("" + this.userBean.getGender());
        setData(this.userBean);
    }

    public void updateHeaderComplete(int i, String str) {
        if (i == 1) {
            this.userBean.setHeaderUrl(str);
            getImageLoader().loadPortrait(this, this.ivHeader, this.userBean.getHeaderUrl());
        } else if (i == 2) {
            this.userBean.setCoverImage(str);
            getImageLoader().loadImage(this, this.imgBackground, this.userBean.getCoverImage());
        }
        AppApplication.getInstance().setUserBean(this.userBean);
        SPHelper.saveUserBean(this.userBean);
    }
}
