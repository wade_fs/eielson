package com.wade.fit.model.net;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebSettings;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.common.net.HttpHeaders;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.net.api.FileApi;
import com.wade.fit.model.net.api.LoginApi;
import com.wade.fit.model.net.api.UserApi;
import com.wade.fit.util.AppUtil;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.log.LogUtil;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import p041io.reactivex.Observable;
import p041io.reactivex.android.schedulers.AndroidSchedulers;
import p041io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {
    public static <M> Observable<M> addObservable(Observable<M> observable) {
        return observable.subscribeOn(Schedulers.m8142io()).unsubscribeOn(Schedulers.m8142io()).observeOn(AndroidSchedulers.mainThread());
    }

    public static class StrTypeAdapter implements JsonSerializer<String>, JsonDeserializer<BaseBean> {
        public BaseBean deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            JsonObject asJsonObject = jsonElement.getAsJsonObject();
            int asInt = asJsonObject.get("code").getAsInt();
            if (asInt != 200) {
                return new BaseBean(asJsonObject.get("message").getAsString(), asInt, new Object());
            }
            if (asJsonObject.get("data") == null) {
                return new BaseBean(asJsonObject.get("message").getAsString(), asInt, new Object());
            }
            return new BaseBean(asJsonObject.get("message").getAsString(), asInt, jsonDeserializationContext.deserialize(asJsonObject.get("data"), ((ParameterizedType) type).getActualTypeArguments()[0]));
        }

        public JsonElement serialize(String str, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(str);
        }
    }

    private static Retrofit getRetrofit(String str) {
        Interceptor interceptor;
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor($$Lambda$ApiManager$z_TpMz9SywHqnrEll3PifuiutFQ.INSTANCE);
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        LogUtil.d("User-Agent:" + getUserAgent(AppApplication.getInstance()));
        if (!TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            interceptor = $$Lambda$ApiManager$1yjR0n2ISntVWwP8NPp5fmu4XNI.INSTANCE;
        } else {
            interceptor = $$Lambda$ApiManager$AsB0KQyYsn082lftGEIG7bnYU.INSTANCE;
        }
        return new Retrofit.Builder().baseUrl(str).addConverterFactory(GsonConverterFactory.create(new GsonBuilder().registerTypeAdapter(BaseBean.class, new StrTypeAdapter()).serializeNulls().setPrettyPrinting().disableHtmlEscaping().create())).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).client(new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).addInterceptor(interceptor).connectTimeout(10, TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).retryOnConnectionFailure(true).cookieJar(new CookieJar() {
            /* class com.wade.fit.model.net.ApiManager.C23891 */
            private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

            public void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
                this.cookieStore.put(httpUrl.host(), list);
            }

            public List<Cookie> loadForRequest(HttpUrl httpUrl) {
                List<Cookie> list = this.cookieStore.get(httpUrl.host());
                return list != null ? list : new ArrayList();
            }
        }).build()).build();
    }

    static /* synthetic */ Response lambda$getRetrofit$1(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder addHeader = request.newBuilder().addHeader("token", SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN)).addHeader("appType", "1").addHeader("packageName", AppApplication.getContext().getPackageName());
        Request.Builder addHeader2 = addHeader.addHeader("appVersion", AppUtil.getVersionCode(AppApplication.getContext()) + "").addHeader("phoneSystemVersion", String.valueOf(Build.VERSION.RELEASE));
        return chain.proceed(addHeader2.addHeader("phoneModel", Build.BRAND + " " + Build.MODEL).addHeader(HttpHeaders.CONTENT_TYPE, "application/json").removeHeader(HttpHeaders.USER_AGENT).addHeader(HttpHeaders.USER_AGENT, getUserAgent(AppApplication.getInstance())).method(request.method(), request.body()).build());
    }

    static /* synthetic */ Response lambda$getRetrofit$2(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder addHeader = request.newBuilder().addHeader("appType", "1").addHeader("packageName", AppApplication.getContext().getPackageName());
        Request.Builder addHeader2 = addHeader.addHeader("appVersion", AppUtil.getVersionCode(AppApplication.getContext()) + "").addHeader("phoneSystemVersion", String.valueOf(Build.VERSION.RELEASE));
        return chain.proceed(addHeader2.addHeader("phoneModel", Build.BRAND + " " + Build.MODEL).addHeader(HttpHeaders.CONTENT_TYPE, "application/json").removeHeader(HttpHeaders.USER_AGENT).addHeader(HttpHeaders.USER_AGENT, getUserAgent(AppApplication.getInstance())).method(request.method(), request.body()).build());
    }

    private static String getUserAgent(Context context) {
        String str;
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                str = WebSettings.getDefaultUserAgent(context);
            } catch (Exception unused) {
                str = System.getProperty("http.agent");
            }
        } else {
            str = System.getProperty("http.agent");
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                stringBuffer.append(String.format("\\u%04x", Integer.valueOf(charAt)));
            } else {
                stringBuffer.append(charAt);
            }
        }
        return stringBuffer.toString();
    }

    public static LoginApi getLoginApi() {
        return (LoginApi) getRetrofit(Constants.SERVER_URL).create(LoginApi.class);
    }

    public static UserApi getUserApi() {
        return (UserApi) getRetrofit(Constants.SERVER_URL).create(UserApi.class);
    }

    public static FileApi getFileApi() {
        return (FileApi) getRetrofit(Constants.SERVER_URL).create(FileApi.class);
    }
}
