package com.wade.fit.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ViewUtil;
import java.util.List;

public class HistroyRecordHrChartView extends View {
    private int MAX_VALUE = 225;
    private int MIN_VALUE = 0;
    private int bgColor;
    private float bottom = 30.0f;
    private float chartHeight;
    private float chartHeightSpan;
    private float chartWidth;
    private float chartWidthSpan;
    private int[] colors = {-12933088, -11030811, -68438, -12955, -760772, -760772};
    private DataMode dataMode;

    /* renamed from: h */
    private int f5240h;
    private int hrColor;
    private Paint hrPaint;
    private float hrSize;
    private boolean isDrawY = true;
    private boolean isHaveDraw;
    private float left = 30.0f;
    private Paint mPaint;
    private float right = 30.0f;
    private float[] scaleY = {(float) this.MIN_VALUE, 60.0f, 120.0f, 180.0f, (float) this.MAX_VALUE};
    private float tempX1 = 0.0f;
    private float tempY1 = 0.0f;
    private int textColor;
    private float textSize;
    private int timeColor;
    private float timeSize;
    private float top = 30.0f;

    /* renamed from: w */
    private int f5241w;

    public HistroyRecordHrChartView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SportTypeHrChartView);
        this.bgColor = obtainStyledAttributes.getColor(0, 0);
        this.textColor = obtainStyledAttributes.getColor(3, 0);
        this.timeColor = obtainStyledAttributes.getColor(5, 0);
        this.hrColor = obtainStyledAttributes.getColor(1, 0);
        this.textSize = obtainStyledAttributes.getDimension(4, 16.0f);
        this.timeSize = obtainStyledAttributes.getDimension(6, 16.0f);
        this.hrSize = obtainStyledAttributes.getDimension(2, 16.0f);
        obtainStyledAttributes.recycle();
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.hrPaint = new Paint();
        this.hrPaint.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5241w = i;
        this.f5240h = i2;
        this.chartWidth = (float) i;
        this.chartHeight = (float) i2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.onDraw(canvas);
        if (this.dataMode != null) {
            this.mPaint.setTextSize(this.textSize);
            this.left = 0.0f;
            this.top = 0.0f;
            this.right = (float) this.f5241w;
            this.bottom = (float) this.f5240h;
            float f = this.chartHeight / ((float) this.MAX_VALUE);
            this.mPaint.setColor(this.bgColor);
            float f2 = this.left;
            float f3 = this.top;
            canvas.drawRect(f2, f3, f2 + this.chartWidth, f3 + this.chartHeight, this.mPaint);
            this.mPaint.setTextAlign(Paint.Align.LEFT);
            this.mPaint.setColor(this.textColor);
            float f4 = this.top + (this.chartHeight - (f * 30.0f));
            float textRectHeight = ViewUtil.getTextRectHeight(this.mPaint, String.valueOf((int) this.scaleY[1]));
            if (this.scaleY[1] >= 30.0f) {
                canvas2.drawText("30", 0.0f, f4 - ((float) ScreenUtil.dp2px(2.0f)), this.mPaint);
                ViewUtil.drawLine(canvas, this.left, f4, (float) ScreenUtil.dp2px(5.0f), ((this.left * 3.0f) / 2.0f) + this.chartWidth, this.mPaint);
            }
            float f5 = this.top + (this.chartHeight - (this.scaleY[1] * f));
            float f6 = f4 - f5;
            float f7 = (textRectHeight * 4.0f) / 3.0f;
            if (f6 < f7) {
                f5 -= f7 - f6;
            }
            float f8 = f5;
            canvas2.drawText(String.valueOf((int) this.scaleY[1]), 0.0f, f8 - ((float) ScreenUtil.dp2px(2.0f)), this.mPaint);
            ViewUtil.drawLine(canvas, this.left, f8, (float) ScreenUtil.dp2px(5.0f), ((this.left * 3.0f) / 2.0f) + this.chartWidth, this.mPaint);
            float f9 = this.top + (this.chartHeight - (this.scaleY[2] * f));
            float f10 = f8 - f9;
            if (f10 < f7) {
                f9 -= f7 - f10;
            }
            float f11 = f9;
            canvas2.drawText(String.valueOf((int) this.scaleY[2]), 0.0f, f11, this.mPaint);
            ViewUtil.drawLine(canvas, this.left, f11, (float) ScreenUtil.dp2px(5.0f), ((this.left * 3.0f) / 2.0f) + this.chartWidth, this.mPaint);
            float f12 = this.top + (this.chartHeight - (f * this.scaleY[3]));
            float f13 = f11 - f12;
            if (f13 < f7) {
                f12 -= f7 - f13;
            }
            float f14 = f12;
            canvas2.drawText(String.valueOf((int) this.scaleY[3]), 0.0f, f14 - ((float) ScreenUtil.dp2px(2.0f)), this.mPaint);
            Canvas canvas3 = canvas;
            ViewUtil.drawLine(canvas3, this.left, f14, (float) ScreenUtil.dp2px(5.0f), this.chartWidth + ((this.left * 3.0f) / 2.0f), this.mPaint);
            if (this.isDrawY) {
                this.mPaint.setColor(this.textColor);
                float f15 = this.left;
                canvas.drawLine(f15, 0.0f, f15, this.top + this.chartHeight, this.mPaint);
            }
            this.mPaint.setTextSize(this.timeSize);
            this.mPaint.setColor(this.timeColor);
            this.mPaint.setTextAlign(Paint.Align.LEFT);
            String startTime = this.dataMode.getStartTime();
            float f16 = this.left;
            float textRectHeight2 = ((this.top + this.chartHeight) - ViewUtil.getTextRectHeight(this.mPaint, startTime)) + ((float) ScreenUtil.dp2px(5.0f));
            canvas2.drawText(startTime, f16, textRectHeight2, this.mPaint);
            float f17 = this.left + this.chartWidth;
            this.mPaint.setTextAlign(Paint.Align.RIGHT);
            canvas2.drawText(this.dataMode.getEndTime(), f17, textRectHeight2, this.mPaint);
            drawHrLine(canvas2, this.dataMode.getItems());
        }
    }

    private void drawHrLine(Canvas canvas, List<HealthHeartRateItem> list) {
        float f;
        float f2;
        List<HealthHeartRateItem> list2 = list;
        this.hrPaint.setStrokeWidth(7.0f);
        this.hrPaint.setTextAlign(Paint.Align.LEFT);
        if (list2 != null && list.size() > 0) {
            this.chartWidthSpan = this.chartWidth / ((float) list2.get(list.size() - 1).getOffsetMinute());
            float f3 = this.chartHeight;
            this.chartHeightSpan = f3 / ((float) this.MAX_VALUE);
            float f4 = this.left;
            float f5 = this.top + f3;
            for (int i = 0; i < list.size(); i++) {
                if (i != 0) {
                    float f6 = this.tempX1;
                    float f7 = this.tempY1;
                    if (f7 == 0.0f) {
                        int i2 = i - 1;
                        f = (float) list2.get(i2).getOffsetMinute();
                        f2 = (float) list2.get(i2).getHeartRaveValue();
                    } else {
                        float f8 = f7;
                        f = f6;
                        f2 = f8;
                    }
                    float offsetMinute = (float) list2.get(i).getOffsetMinute();
                    float heartRaveValue = (float) list2.get(i).getHeartRaveValue();
                    if (f2 != 0.0f && heartRaveValue != 0.0f) {
                        this.tempY1 = 0.0f;
                        this.tempX1 = 0.0f;
                        this.hrPaint.setColor(-114147);
                        float f9 = f;
                        float f10 = f2;
                        int i3 = 1;
                        while (true) {
                            float[] fArr = this.scaleY;
                            if (i3 >= fArr.length) {
                                break;
                            }
                            if (f10 > fArr[i3] || heartRaveValue < fArr[i3]) {
                                float[] fArr2 = this.scaleY;
                                if (f10 <= fArr2[i3] || heartRaveValue >= fArr2[i3]) {
                                    float[] fArr3 = this.scaleY;
                                    if (f10 <= fArr3[i3] && heartRaveValue <= fArr3[i3]) {
                                        int i4 = i3 - 1;
                                        if (f10 >= fArr3[i4] && heartRaveValue >= fArr3[i4]) {
                                            float f11 = this.chartWidthSpan;
                                            float f12 = this.chartHeightSpan;
                                            canvas.drawLine(f4 + (f9 * f11), f5 - ((f10 - fArr3[0]) * f12), f4 + (f11 * offsetMinute), f5 - ((heartRaveValue - fArr3[0]) * f12), this.hrPaint);
                                        }
                                    }
                                } else {
                                    float f13 = fArr2[i3];
                                    float f14 = (((f10 - f13) * (offsetMinute - f9)) / (f10 - heartRaveValue)) + f9;
                                    float f15 = this.chartWidthSpan;
                                    float f16 = f4 + (offsetMinute * f15);
                                    float f17 = heartRaveValue - fArr2[0];
                                    float f18 = this.chartHeightSpan;
                                    canvas.drawLine(f16, f5 - (f17 * f18), f4 + (f15 * f14), f5 - ((f13 - fArr2[0]) * f18), this.hrPaint);
                                    heartRaveValue = f13;
                                    offsetMinute = f14;
                                }
                            } else {
                                float f19 = fArr[i3];
                                float f20 = (((f19 - f10) * (offsetMinute - f9)) / (heartRaveValue - f10)) + f9;
                                float f21 = this.chartWidthSpan;
                                float f22 = f4 + (f9 * f21);
                                float f23 = f10 - fArr[0];
                                float f24 = this.chartHeightSpan;
                                canvas.drawLine(f22, f5 - (f23 * f24), f4 + (f21 * f20), f5 - ((f19 - fArr[0]) * f24), this.hrPaint);
                                f10 = f19;
                                f9 = f20;
                            }
                            i3++;
                        }
                    } else if (f2 != 0.0f) {
                        this.tempY1 = f2;
                        this.tempX1 = f;
                    } else if (heartRaveValue != 0.0f) {
                        this.tempY1 = heartRaveValue;
                        this.tempX1 = offsetMinute;
                    }
                }
            }
        }
    }

    public void initDatas(DataMode dataMode2) {
        this.dataMode = dataMode2;
        if (dataMode2.getInterval() != null) {
            this.scaleY[3] = (float) dataMode2.getInterval().getLimit_threshold();
            this.scaleY[2] = (float) dataMode2.getInterval().getAerobic_threshold();
            this.scaleY[1] = (float) dataMode2.getInterval().getBurn_fat_threshold();
        }
        postInvalidate();
    }

    public void initDatas(DataMode dataMode2, boolean z) {
        this.dataMode = dataMode2;
        this.isDrawY = z;
        if (dataMode2.getInterval() != null) {
            this.scaleY[1] = (float) dataMode2.getInterval().getBurn_fat_threshold();
            this.scaleY[2] = (float) dataMode2.getInterval().getAerobic_threshold();
            this.scaleY[3] = (float) dataMode2.getInterval().getLimit_threshold();
        }
    }

    public static class DataMode {
        private String endTime;
        private HealthHeartRate interval;
        private List<HealthHeartRateItem> items;
        private String startTime;

        public String getStartTime() {
            return this.startTime;
        }

        public void setStartTime(String str) {
            this.startTime = str;
        }

        public String getEndTime() {
            return this.endTime;
        }

        public void setEndTime(String str) {
            this.endTime = str;
        }

        public List<HealthHeartRateItem> getItems() {
            return this.items;
        }

        public void setItems(List<HealthHeartRateItem> list) {
            this.items = list;
        }

        public HealthHeartRate getInterval() {
            return this.interval;
        }

        public void setInterval(HealthHeartRate healthHeartRate) {
            this.interval = healthHeartRate;
        }

        public String toString() {
            return "DataMode{startTime='" + this.startTime + '\'' + ", endTime='" + this.endTime + '\'' + ", items=" + this.items + ", interval=" + this.interval + '}';
        }
    }
}
