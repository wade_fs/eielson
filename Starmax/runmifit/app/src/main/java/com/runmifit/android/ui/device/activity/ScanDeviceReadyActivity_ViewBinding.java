package com.wade.fit.ui.device.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.RecyclerRefreshLayout;

/* renamed from: com.wade.fit.ui.device.activity.ScanDeviceReadyActivity_ViewBinding */
public class ScanDeviceReadyActivity_ViewBinding implements Unbinder {
    private ScanDeviceReadyActivity target;
    private View view2131296336;

    public ScanDeviceReadyActivity_ViewBinding(ScanDeviceReadyActivity scanDeviceReadyActivity) {
        this(scanDeviceReadyActivity, scanDeviceReadyActivity.getWindow().getDecorView());
    }

    public ScanDeviceReadyActivity_ViewBinding(final ScanDeviceReadyActivity scanDeviceReadyActivity, View view) {
        this.target = scanDeviceReadyActivity;
        scanDeviceReadyActivity.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.mRecyclerView, "field 'mRecyclerView'", RecyclerView.class);
        scanDeviceReadyActivity.mRefreshLayout = (RecyclerRefreshLayout) Utils.findRequiredViewAsType(view, R.id.mRefreshLayout, "field 'mRefreshLayout'", RecyclerRefreshLayout.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.btnSkip, "field 'btnSkip' and method 'tomain'");
        scanDeviceReadyActivity.btnSkip = (Button) Utils.castView(findRequiredView, R.id.btnSkip, "field 'btnSkip'", Button.class);
        this.view2131296336 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.ScanDeviceReadyActivity_ViewBinding.C24801 */

            public void doClick(View view) {
                scanDeviceReadyActivity.tomain();
            }
        });
    }

    public void unbind() {
        ScanDeviceReadyActivity scanDeviceReadyActivity = this.target;
        if (scanDeviceReadyActivity != null) {
            this.target = null;
            scanDeviceReadyActivity.mRecyclerView = null;
            scanDeviceReadyActivity.mRefreshLayout = null;
            scanDeviceReadyActivity.btnSkip = null;
            this.view2131296336.setOnClickListener(null);
            this.view2131296336 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
