package com.wade.fit.views.map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.CircleOptions;
import com.baidu.mapapi.map.GroundOverlayOptions;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BaseMap;
import com.wade.fit.model.bean.LatLngBean;
import com.wade.fit.util.AsyncTaskUtil;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.callback.MapScreenShotCallback;
import com.wade.fit.util.image.BitmapUtil;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;
import java.util.List;
import org.apache.commons.math3.distribution.PoissonDistribution;

public class BaiduMap extends BaseMap<TextureMapView, LatLng> {
    com.baidu.mapapi.map.BaiduMap aMap;
    private BaiduMap.OnMapLoadedCallback mapLoadedCallback = new BaiduMap.OnMapLoadedCallback() {
        /* class com.wade.fit.views.map.BaiduMap.C27343 */

        public void onMapLoaded() {
            DebugLog.m6203d("onMapLoaded :" + BaiduMap.this.onMapLoaded);
            boolean unused = BaiduMap.this.onMapLoaded = true;
        }
    };
    private PolylineOptions options;

    public BaiduMap() {
        this.isBaidu = true;
    }

    public void setLatLngBeanList(List<LatLngBean> list) {
        getLatLngBeanList().clear();
        for (LatLngBean latLngBean : list) {
            getLatLngBeanList().add(latLngBean.clone());
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((TextureMapView) this.mapView).onCreate(((TextureMapView) this.mapView).getContext(), bundle);
    }

    public void onPause() {
        ((TextureMapView) this.mapView).onPause();
    }

    public void onResume() {
        ((TextureMapView) this.mapView).onResume();
    }

    public void onDestroy() {
        ((TextureMapView) this.mapView).onDestroy();
    }

    /* access modifiers changed from: protected */
    public void showOrHideMap(LatLngBean latLngBean) {
        if (this.isHideMapView) {
            GroundOverlayOptions groundOverlayOptions = new GroundOverlayOptions();
            groundOverlayOptions.position(fromLatLngBean(latLngBean));
            groundOverlayOptions.zIndex(50);
            groundOverlayOptions.visible(true);
            groundOverlayOptions.dimensions(ScreenUtil.getScreenWidth(AppApplication.getInstance()) * 2, ScreenUtil.getScreenHeight(AppApplication.getInstance()) * 2);
            this.aMap.addOverlay(groundOverlayOptions);
            return;
        }
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.zIndex(50);
        circleOptions.radius(PoissonDistribution.DEFAULT_MAX_ITERATIONS);
        circleOptions.center(fromLatLngBean(latLngBean));
        circleOptions.fillColor(Color.argb((int) FMParserConstants.CLOSE_BRACKET, 0, 0, 0));
        this.aMap.addOverlay(circleOptions);
    }

    public void initMapView(TextureMapView textureMapView) {
        super.initMapView((View) textureMapView);
        if (this.aMap == null) {
            this.mapView = textureMapView;
            this.aMap = textureMapView.getMap();
            DebugLog.m6203d("initMapView");
            this.aMap.setOnMapLoadedCallback(this.mapLoadedCallback);
            this.aMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
                /* class com.wade.fit.views.map.BaiduMap.C27311 */

                public void onMapStatusChange(MapStatus mapStatus) {
                }

                public void onMapStatusChangeStart(MapStatus mapStatus) {
                }

                public void onMapStatusChangeStart(MapStatus mapStatus, int i) {
                }

                public void onMapStatusChangeFinish(MapStatus mapStatus) {
                    float unused = BaiduMap.this.zoomLevel = mapStatus.zoom;
                    DebugLog.m6203d("zoomLevel:" + BaiduMap.this.zoomLevel);
                }
            });
        }
    }

    public void setMapType(boolean z) {
        com.baidu.mapapi.map.BaiduMap baiduMap = this.aMap;
        if (baiduMap != null) {
            baiduMap.setMapType(z ? 1 : 2);
        }
    }

    /* access modifiers changed from: protected */
    public LatLng fromLatLngBean(LatLngBean latLngBean) {
        return new LatLng(latLngBean.latitude, latLngBean.longitude);
    }

    /* access modifiers changed from: protected */
    public void drawPolyline() {
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.width(12);
        polylineOptions.color(-16711936);
        polylineOptions.points(this.latLngList);
        polylineOptions.zIndex(102);
        this.aMap.addOverlay(polylineOptions);
    }

    public void onMapScreenShot(final MapScreenShotCallback mapScreenShotCallback) {
        com.baidu.mapapi.map.BaiduMap baiduMap = this.aMap;
        if (baiduMap != null) {
            baiduMap.snapshot(new BaiduMap.SnapshotReadyCallback() {
                /* class com.wade.fit.views.map.BaiduMap.C27322 */

                public void onSnapshotReady(final Bitmap bitmap) {
                    BaiduMap.this.removeTimeOut();
                    new AsyncTaskUtil(new AsyncTaskUtil.AsyncTaskCallBackAdapter() {
                        /* class com.wade.fit.views.map.BaiduMap.C27322.C27331 */

                        public Object doInBackground(String... strArr) {
                            BitmapUtil.saveBitmap(bitmap, BaiduMap.this.getShotFilePath());
                            return null;
                        }

                        public void onPostExecute(Object obj) {
                            if (mapScreenShotCallback != null) {
                                mapScreenShotCallback.shotComplet(bitmap);
                            }
                        }
                    }).execute("");
                }
            });
        }
    }

    public void addMarker(LatLngBean latLngBean, int i) {
        if (this.aMap != null) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(new LatLng(latLngBean.latitude, latLngBean.longitude));
            markerOptions.draggable(true);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(i));
            markerOptions.anchor(0.5f, 0.5f);
            markerOptions.zIndex(200);
            this.aMap.addOverlay(markerOptions);
        }
    }

    public void setIsHideMapView(boolean z) {
        super.setIsHideMapView(z);
        this.aMap.getUiSettings().setAllGesturesEnabled(!z);
        drawAllAndShot(this.startTime, this.sporType);
    }

    public void ajustMapView() {
        if (this.aMap != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            LatLngBounds latLngBounds = null;
            for (LatLngBean latLngBean : getLatLngBeanList()) {
                latLngBounds = builder.include(fromLatLngBean(latLngBean)).build();
            }
            this.aMap.setMapStatus(MapStatusUpdateFactory.newLatLngBounds(latLngBounds, ScreenUtil.getScreenWidth(AppApplication.getInstance()) / 4, ScreenUtil.getScreenHeight(AppApplication.getInstance()) / 4));
        }
    }

    public void addPolyline(LatLngBean latLngBean) {
        if (this.aMap != null) {
            this.centerLatlng = latLngBean;
            if (this.options == null) {
                this.options = new PolylineOptions();
                this.options.width(12);
                this.options.zIndex(102);
            }
            this.options.color(-16711936);
            this.latLngList.add(fromLatLngBean(latLngBean));
            this.lastLatLng = latLngBean;
            if (this.latLngList.size() > 2) {
                this.options.points(this.latLngList);
                this.aMap.addOverlay(this.options);
            }
        }
    }

    public void addPolylineAndMove(LatLngBean latLngBean, boolean z) {
        LatLngBean clone = latLngBean.clone();
        LogUtil.d(clone.toString());
        super.addPolylineAndMove(clone, z);
    }

    /* access modifiers changed from: protected */
    public void preDrawAllAndShot() {
        this.latLngList.clear();
        this.aMap.clear();
    }

    public void addMileMark() {
        if (this.aMap != null && this.isMarker) {
            int i = 1;
            for (Integer num : this.mileMarkList) {
                LatLng fromLatLngBean = fromLatLngBean(getLatLngBeanList().get(num.intValue()));
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(fromLatLngBean);
                markerOptions.draggable(true);
                markerOptions.zIndex(200);
                Bitmap copy = BitmapFactory.decodeResource(AppApplication.getInstance().getResources(), R.mipmap.mark_bg).copy(Bitmap.Config.ARGB_8888, true);
                Canvas canvas = new Canvas(copy);
                Paint paint = new Paint();
                paint.setColor(-1);
                paint.setTextSize(40.0f);
                String valueOf = String.valueOf(i);
                Paint.FontMetrics fontMetrics = paint.getFontMetrics();
                canvas.drawText(valueOf, (float) ((int) ((((float) copy.getWidth()) - paint.measureText(valueOf)) / 2.0f)), (float) ((int) (((((float) copy.getHeight()) - fontMetrics.descent) - fontMetrics.ascent) / 2.0f)), paint);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(copy));
                markerOptions.anchor(0.5f, 0.5f);
                this.aMap.addOverlay(markerOptions);
                i++;
            }
        }
    }

    public void animateCamera(LatLngBean latLngBean) {
        com.baidu.mapapi.map.BaiduMap baiduMap = this.aMap;
        if (baiduMap != null) {
            baiduMap.animateMapStatus(MapStatusUpdateFactory.newLatLngZoom(fromLatLngBean(latLngBean), this.zoomLevel));
        }
    }
}
