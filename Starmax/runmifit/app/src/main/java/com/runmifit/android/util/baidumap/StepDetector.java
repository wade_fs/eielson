package com.wade.fit.util.baidumap;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class StepDetector implements SensorEventListener {
    final float InitialValue = 1.3f;
    float ThreadValue = 2.0f;
    int TimeInterval = 250;
    final int ValueNum = 4;
    int continueUpCount = 0;
    int continueUpFormerCount = 0;
    float gravityNew = 0.0f;
    float gravityOld = 0.0f;
    boolean isDirectionUp = false;
    boolean lastStatus = false;
    private StepCountListener mStepListeners;
    float[] oriValues = new float[3];
    float peakOfWave = 0.0f;
    int tempCount = 0;
    float[] tempValue = new float[4];
    long timeOfLastPeak = 0;
    long timeOfNow = 0;
    long timeOfThisPeak = 0;
    float valleyOfWave = 0.0f;

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        for (int i = 0; i < 3; i++) {
            this.oriValues[i] = sensorEvent.values[i];
        }
        float[] fArr = this.oriValues;
        this.gravityNew = (float) Math.sqrt((double) ((fArr[0] * fArr[0]) + (fArr[1] * fArr[1]) + (fArr[2] * fArr[2])));
        detectorNewStep(this.gravityNew);
    }

    public void initListener(StepCountListener stepCountListener) {
        this.mStepListeners = stepCountListener;
    }

    public void detectorNewStep(float f) {
        float f2 = this.gravityOld;
        if (f2 == 0.0f) {
            this.gravityOld = f;
        } else if (detectorPeak(f, f2)) {
            this.timeOfLastPeak = this.timeOfThisPeak;
            this.timeOfNow = System.currentTimeMillis();
            long j = this.timeOfNow;
            if (j - this.timeOfLastPeak >= ((long) this.TimeInterval) && this.peakOfWave - this.valleyOfWave >= this.ThreadValue) {
                this.timeOfThisPeak = j;
                this.mStepListeners.countStep();
            }
            long j2 = this.timeOfNow;
            if (j2 - this.timeOfLastPeak >= ((long) this.TimeInterval)) {
                float f3 = this.peakOfWave;
                float f4 = this.valleyOfWave;
                if (f3 - f4 >= 1.3f) {
                    this.timeOfThisPeak = j2;
                    this.ThreadValue = peakValleyThread(f3 - f4);
                }
            }
        }
        this.gravityOld = f;
    }

    public boolean detectorPeak(float f, float f2) {
        this.lastStatus = this.isDirectionUp;
        if (f >= f2) {
            this.isDirectionUp = true;
            this.continueUpCount++;
        } else {
            this.continueUpFormerCount = this.continueUpCount;
            this.continueUpCount = 0;
            this.isDirectionUp = false;
        }
        if (this.isDirectionUp || !this.lastStatus || (this.continueUpFormerCount < 2 && f2 < 20.0f)) {
            if (!this.lastStatus && this.isDirectionUp) {
                this.valleyOfWave = f2;
            }
            return false;
        }
        this.peakOfWave = f2;
        return true;
    }

    public float peakValleyThread(float f) {
        float f2 = this.ThreadValue;
        int i = this.tempCount;
        if (i < 4) {
            this.tempValue[i] = f;
            this.tempCount = i + 1;
        } else {
            f2 = averageValue(this.tempValue, 4);
            for (int i2 = 1; i2 < 4; i2++) {
                float[] fArr = this.tempValue;
                fArr[i2 - 1] = fArr[i2];
            }
            this.tempValue[3] = f;
        }
        return f2;
    }

    public float averageValue(float[] fArr, int i) {
        float f = 0.0f;
        for (int i2 = 0; i2 < i; i2++) {
            f += fArr[i2];
        }
        float f2 = f / 4.0f;
        if (f2 >= 8.0f) {
            return 4.3f;
        }
        if (f2 >= 7.0f && f2 < 8.0f) {
            return 3.3f;
        }
        if (f2 < 4.0f || f2 >= 7.0f) {
            return (f2 < 3.0f || f2 >= 4.0f) ? 1.3f : 2.0f;
        }
        return 2.3f;
    }
}
