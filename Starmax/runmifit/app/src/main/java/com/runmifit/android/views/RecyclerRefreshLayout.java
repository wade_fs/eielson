package com.wade.fit.views;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.wade.fit.R;

public class RecyclerRefreshLayout extends SwipeRefreshLayout implements SwipeRefreshLayout.OnRefreshListener {
    private SuperRefreshLayoutListener listener;
    /* access modifiers changed from: private */
    public boolean mCanLoadMore;
    private boolean mHasMore;
    private boolean mIsOnLoading;
    private int mLastY;
    private RecyclerView mRecycleView;
    private int mTouchSlop;
    private int mYDown;

    public interface SuperRefreshLayoutListener {
        void onLoadMore();

        void onRefreshing();
    }

    public RecyclerRefreshLayout(Context context) {
        this(context, null);
    }

    public RecyclerRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mIsOnLoading = false;
        this.mCanLoadMore = true;
        this.mHasMore = true;
        this.mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        setOnRefreshListener(this);
    }

    public void onRefresh() {
        SuperRefreshLayoutListener superRefreshLayoutListener = this.listener;
        if (superRefreshLayoutListener == null || this.mIsOnLoading) {
            setRefreshing(false);
        } else {
            superRefreshLayoutListener.onRefreshing();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.mRecycleView == null) {
            getRecycleView();
        }
    }

    private void getRecycleView() {
        if (getChildCount() > 0) {
            View childAt = getChildAt(0);
            if (!(childAt instanceof RecyclerView)) {
                childAt = findViewById(R.id.refresh_recyclerView);
            }
            if (childAt != null && (childAt instanceof RecyclerView)) {
                this.mRecycleView = (RecyclerView) childAt;
                this.mRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    /* class com.wade.fit.views.RecyclerRefreshLayout.C27131 */

                    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                    }

                    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                        if (RecyclerRefreshLayout.this.canLoad() && RecyclerRefreshLayout.this.mCanLoadMore) {
                            RecyclerRefreshLayout.this.loadData();
                        }
                    }
                });
            }
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            this.mYDown = (int) motionEvent.getRawY();
        } else if (action == 2) {
            this.mLastY = (int) motionEvent.getRawY();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    /* access modifiers changed from: private */
    public boolean canLoad() {
        return isScrollBottom() && !this.mIsOnLoading && isPullUp() && this.mHasMore;
    }

    /* access modifiers changed from: private */
    public void loadData() {
        if (this.listener != null) {
            setOnLoading(true);
            this.listener.onLoadMore();
        }
    }

    private boolean isPullUp() {
        return this.mYDown - this.mLastY >= this.mTouchSlop;
    }

    public void setOnLoading(boolean z) {
        if (!this.mIsOnLoading) {
            this.mYDown = 0;
            this.mLastY = 0;
        }
        this.mIsOnLoading = z;
    }

    private boolean isScrollBottom() {
        RecyclerView recyclerView = this.mRecycleView;
        if (recyclerView == null || recyclerView.getAdapter() == null || getLastVisiblePosition() != this.mRecycleView.getAdapter().getItemCount() - 1) {
            return false;
        }
        return true;
    }

    public void onComplete() {
        setOnLoading(false);
        setRefreshing(false);
        this.mHasMore = true;
    }

    public void setCanLoadMore(boolean z) {
        this.mCanLoadMore = z;
    }

    public int getLastVisiblePosition() {
        if (this.mRecycleView.getLayoutManager() instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) this.mRecycleView.getLayoutManager()).findLastVisibleItemPosition();
        }
        if (this.mRecycleView.getLayoutManager() instanceof GridLayoutManager) {
            return ((GridLayoutManager) this.mRecycleView.getLayoutManager()).findLastVisibleItemPosition();
        }
        if (!(this.mRecycleView.getLayoutManager() instanceof StaggeredGridLayoutManager)) {
            return this.mRecycleView.getLayoutManager().getItemCount() - 1;
        }
        StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) this.mRecycleView.getLayoutManager();
        return getMaxPosition(staggeredGridLayoutManager.findLastVisibleItemPositions(new int[staggeredGridLayoutManager.getSpanCount()]));
    }

    private int getMaxPosition(int[] iArr) {
        int i = Integer.MIN_VALUE;
        for (int i2 : iArr) {
            i = Math.max(i, i2);
        }
        return i;
    }

    public void setSuperRefreshLayoutListener(SuperRefreshLayoutListener superRefreshLayoutListener) {
        this.listener = superRefreshLayoutListener;
    }
}
