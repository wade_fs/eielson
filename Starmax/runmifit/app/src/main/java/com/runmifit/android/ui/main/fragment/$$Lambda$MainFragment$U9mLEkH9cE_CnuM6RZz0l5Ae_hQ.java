package com.wade.fit.ui.main.fragment;

import android.content.DialogInterface;
import com.wade.fit.util.ble.BleScanTool;

/* renamed from: com.wade.fit.ui.main.fragment.-$$Lambda$MainFragment$U9mLEkH9cE_CnuM6RZz0l5Ae_hQ  reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$MainFragment$U9mLEkH9cE_CnuM6RZz0l5Ae_hQ implements DialogInterface.OnClickListener {
    public static final /* synthetic */ $$Lambda$MainFragment$U9mLEkH9cE_CnuM6RZz0l5Ae_hQ INSTANCE = new $$Lambda$MainFragment$U9mLEkH9cE_CnuM6RZz0l5Ae_hQ();

    private /* synthetic */ $$Lambda$MainFragment$U9mLEkH9cE_CnuM6RZz0l5Ae_hQ() {
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        BleScanTool.getInstance().openBluetooth();
    }
}
