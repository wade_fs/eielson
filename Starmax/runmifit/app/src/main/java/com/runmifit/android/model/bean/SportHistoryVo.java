package com.wade.fit.model.bean;

import com.wade.fit.greendao.bean.HealthActivity;
import java.util.ArrayList;
import java.util.List;

public class SportHistoryVo {
    public List<HealthActivity> allDatas = new ArrayList();
    public List<HealthActivity> datas = new ArrayList();
    public String date;
    public List<String> dates = new ArrayList();
    public List<HealthMonthActivity> monthDatas = new ArrayList();
    public String totalTime;
}
