package com.wade.fit.util.ble;

public interface DeviceCallback {
    void addVol();

    void answerRingingCall();

    void camare();

    void endRingingCall();

    void enterCamare();

    void exitCamare();

    void exitMusic();

    void findPhone();

    void musicControl();

    void nextMusic();

    void preMusic();

    void sos();

    void subVol();
}
