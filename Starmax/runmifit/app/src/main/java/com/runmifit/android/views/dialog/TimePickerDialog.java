package com.wade.fit.views.dialog;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.util.TimeUtil;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.views.wheel.ArrayWheelAdapter;
import com.wade.fit.views.wheel.NumericWheelAdapter;
import com.wade.fit.views.wheel.WheelView;

public class TimePickerDialog extends BaseDialog {
    private String[] amOrPm;
    private OnClickCallback callback;
    private TextView cancel = ((TextView) this.dialog.findViewById(R.id.cancleTv));

    /* renamed from: ok */
    public TextView f5253ok = ((TextView) this.dialog.findViewById(R.id.setTv));
    public WheelView wheelPoint;
    public WheelView wheel_amOrpm;
    public WheelView wheel_h;
    public WheelView wheel_mi;

    public interface OnClickCallback {
        void onCancel();

        void onSure(String str, int i, int i2);
    }

    public TimePickerDialog(Context context) {
        super(context);
        this.dialog.setContentView((int) R.layout.dialog_time_picker);
        this.cancel.setOnClickListener(this);
        this.f5253ok.setOnClickListener(this);
        this.amOrPm = new String[]{AppApplication.getInstance().getResources().getString(R.string.am), AppApplication.getInstance().getResources().getString(R.string.pm)};
        this.wheel_amOrpm = (WheelView) this.dialog.findViewById(R.id.am_pm);
        this.wheel_h = (WheelView) this.dialog.findViewById(R.id.hour);
        String[] stringArray = context.getResources().getStringArray(R.array.amOrpm);
        this.wheelPoint = (WheelView) this.dialog.findViewById(R.id.point);
        this.wheelPoint.setAdapter(new ArrayWheelAdapter(new String[]{Config.TRACE_TODAY_VISIT_SPLIT}, 1));
        this.wheelPoint.setClickable(false);
        this.wheelPoint.setEnabled(false);
        this.wheelPoint.setCyclic(false);
        if (TimeUtil.is24Hour()) {
            this.wheel_h.setAdapter(new NumericWheelAdapter(0, 23, "%02d"));
            this.wheel_amOrpm.setVisibility(View.GONE);
        } else {
            this.wheel_amOrpm.setAdapter(new ArrayWheelAdapter(stringArray, 2));
            this.wheel_amOrpm.setCyclic(false);
            this.wheel_amOrpm.setVisibility(View.VISIBLE);
            this.wheel_h.setAdapter(new NumericWheelAdapter(1, 12, "%02d"));
        }
        this.wheel_h.setCyclic(true);
        this.wheel_mi = (WheelView) this.dialog.findViewById(R.id.min);
        this.wheel_mi.setAdapter(new NumericWheelAdapter(0, 59, "%02d"));
        this.wheel_mi.setCyclic(true);
        setDialogLocation(this.mContext, this.dialog);
    }

    public void setTime(int i, int i2) {
        this.wheel_amOrpm.setCurrentItem(TimeUtil.isAM(i) ^ true ? 1 : 0);
        WheelView wheelView = this.wheel_h;
        if (!TimeUtil.is24Hour()) {
            i = TimeUtil.format24To12(i) - 1;
        }
        wheelView.setCurrentItem(i);
        this.wheel_mi.setCurrentItem(i2);
    }

    public String getTime() {
        String str;
        int currentItem = this.wheel_h.getCurrentItem();
        int currentItem2 = this.wheel_mi.getCurrentItem();
        if (TimeUtil.is24Hour()) {
            str = TimeUtil.timeFormatter(currentItem, currentItem2, TimeUtil.is24Hour(), this.amOrPm);
        } else {
            currentItem = TimeUtil.format12To24(currentItem + 1, this.wheel_amOrpm.getCurrentItem() == 0);
            str = TimeUtil.timeFormatter(currentItem, currentItem2, TimeUtil.is24Hour(), this.amOrPm);
        }
        DebugLog.m6203d("timeMode=" + str + " Hour=" + currentItem + "  Min=" + currentItem2);
        return str;
    }

    public void onClick(View view) {
        String str;
        super.onClick(view);
        int id = view.getId();
        if (id == R.id.cancleTv) {
            OnClickCallback onClickCallback = this.callback;
            if (onClickCallback != null) {
                onClickCallback.onCancel();
            }
        } else if (id == R.id.setTv && this.callback != null) {
            int currentItem = this.wheel_h.getCurrentItem();
            int currentItem2 = this.wheel_mi.getCurrentItem();
            if (TimeUtil.is24Hour()) {
                str = TimeUtil.timeFormatter(currentItem, currentItem2, TimeUtil.is24Hour(), this.amOrPm);
            } else {
                currentItem = TimeUtil.format12To24(currentItem + 1, this.wheel_amOrpm.getCurrentItem() == 0);
                str = TimeUtil.timeFormatter(currentItem, currentItem2, TimeUtil.is24Hour(), this.amOrPm);
            }
            DebugLog.m6203d("timeMode=" + str + " Hour=" + currentItem + "  Min=" + currentItem2);
            this.callback.onSure(str, currentItem, currentItem2);
        }
    }

    public void setCallback(OnClickCallback onClickCallback) {
        this.callback = onClickCallback;
    }
}
