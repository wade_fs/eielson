package com.wade.fit.ui.sport.activity;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.baidu.mapapi.map.MapView;
import com.wade.fit.R;
import com.wade.fit.views.CountDownProgressBar;

/* renamed from: com.wade.fit.ui.sport.activity.SportActivity_ViewBinding */
public class SportActivity_ViewBinding implements Unbinder {
    private SportActivity target;
    private View view2131296526;
    private View view2131296741;
    private View view2131296750;
    private View view2131296869;

    public SportActivity_ViewBinding(SportActivity sportActivity) {
        this(sportActivity, sportActivity.getWindow().getDecorView());
    }

    public SportActivity_ViewBinding(final SportActivity sportActivity, View view) {
        this.target = sportActivity;
        sportActivity.mMapView = (MapView) Utils.findRequiredViewAsType(view, R.id.mMapView, "field 'mMapView'", MapView.class);
        sportActivity.rlMapView = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.rlMapView, "field 'rlMapView'", RelativeLayout.class);
        sportActivity.rlDataView = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.rlDataView, "field 'rlDataView'", RelativeLayout.class);
        sportActivity.rlAnimationView = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.rlAnimationView, "field 'rlAnimationView'", RelativeLayout.class);
        sportActivity.animationTextView = (TextView) Utils.findRequiredViewAsType(view, R.id.animationTextView, "field 'animationTextView'", TextView.class);
        sportActivity.tvDistance = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDistance, "field 'tvDistance'", TextView.class);
        sportActivity.tvTime = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTime, "field 'tvTime'", TextView.class);
        sportActivity.rlScreenTitle = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.rlScreenTitle, "field 'rlScreenTitle'", RelativeLayout.class);
        sportActivity.distance2 = (TextView) Utils.findRequiredViewAsType(view, R.id.distance2, "field 'distance2'", TextView.class);
        sportActivity.useTime = (TextView) Utils.findRequiredViewAsType(view, R.id.useTime, "field 'useTime'", TextView.class);
        sportActivity.speed = (TextView) Utils.findRequiredViewAsType(view, R.id.speed, "field 'speed'", TextView.class);
        sportActivity.tvCal = (TextView) Utils.findRequiredViewAsType(view, R.id.tvCal, "field 'tvCal'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.pausedBtn, "field 'pausedBtn' and method 'pauseLocation'");
        sportActivity.pausedBtn = (RelativeLayout) Utils.castView(findRequiredView, R.id.pausedBtn, "field 'pausedBtn'", RelativeLayout.class);
        this.view2131296741 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportActivity_ViewBinding.C26131 */

            public void doClick(View view) {
                sportActivity.pauseLocation();
            }
        });
        sportActivity.playLayout = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.playLayout, "field 'playLayout'", RelativeLayout.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.playBtn, "field 'playLayoutBtn' and method 'contiueLocation'");
        sportActivity.playLayoutBtn = (RelativeLayout) Utils.castView(findRequiredView2, R.id.playBtn, "field 'playLayoutBtn'", RelativeLayout.class);
        this.view2131296750 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportActivity_ViewBinding.C26142 */

            public void doClick(View view) {
                sportActivity.contiueLocation();
            }
        });
        sportActivity.endLayoutBtn = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.endBtn, "field 'endLayoutBtn'", RelativeLayout.class);
        sportActivity.endBtn = (CountDownProgressBar) Utils.findRequiredViewAsType(view, R.id.ivEnd, "field 'endBtn'", CountDownProgressBar.class);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.imgHideMapView, "method 'hideMapView'");
        this.view2131296526 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportActivity_ViewBinding.C26153 */

            public void doClick(View view) {
                sportActivity.hideMapView();
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.show_map, "method 'showMapView'");
        this.view2131296869 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportActivity_ViewBinding.C26164 */

            public void doClick(View view) {
                sportActivity.showMapView();
            }
        });
    }

    public void unbind() {
        SportActivity sportActivity = this.target;
        if (sportActivity != null) {
            this.target = null;
            sportActivity.mMapView = null;
            sportActivity.rlMapView = null;
            sportActivity.rlDataView = null;
            sportActivity.rlAnimationView = null;
            sportActivity.animationTextView = null;
            sportActivity.tvDistance = null;
            sportActivity.tvTime = null;
            sportActivity.rlScreenTitle = null;
            sportActivity.distance2 = null;
            sportActivity.useTime = null;
            sportActivity.speed = null;
            sportActivity.tvCal = null;
            sportActivity.pausedBtn = null;
            sportActivity.playLayout = null;
            sportActivity.playLayoutBtn = null;
            sportActivity.endLayoutBtn = null;
            sportActivity.endBtn = null;
            this.view2131296741.setOnClickListener(null);
            this.view2131296741 = null;
            this.view2131296750.setOnClickListener(null);
            this.view2131296750 = null;
            this.view2131296526.setOnClickListener(null);
            this.view2131296526 = null;
            this.view2131296869.setOnClickListener(null);
            this.view2131296869 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
