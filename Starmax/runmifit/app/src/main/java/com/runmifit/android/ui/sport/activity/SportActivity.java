package com.wade.fit.ui.sport.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.location.BDLocation;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthGpsItem;
import com.wade.fit.greendao.gen.HealthActivityDao;
import com.wade.fit.greendao.gen.HealthGpsItemDao;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.ui.sport.activity.SportActivity;
import com.wade.fit.service.LocationService;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.SportDataHelper;
import com.wade.fit.util.SpringScaleInterpolator;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.baidumap.MyOrientationListener;
import com.wade.fit.util.baidumap.StepService;
import com.wade.fit.util.baidumap.UpdateUiCallBack;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.CountDownProgressBar;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.wade.fit.ui.sport.activity.SportActivity */
public class SportActivity extends BaseActivity {
    TextView animationTextView;
    private String avgSpeed;
    private int calorie;
    ServiceConnection conn = new ServiceConnection() {
        /* class com.wade.fit.ui.sport.activity.SportActivity.C26104 */

        public void onServiceDisconnected(ComponentName componentName) {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ((StepService.StepBinder) iBinder).getService().registerCallback(new UpdateUiCallBack() {
                /* class com.wade.fit.ui.sport.activity.$$Lambda$SportActivity$4$26CwMRSpyDnO92U5lvMZQLyQqbM */

                public final void updateUi(int i) {
                    SportActivity.C26104.this.lambda$onServiceConnected$0$SportActivity$4(i);
                }
            });
        }

        public /* synthetic */ void lambda$onServiceConnected$0$SportActivity$4(int i) {
            SportActivity.this.saveHealth.setStep(i);
        }
    };
    TextView distance2;
    private double distanceTotal;
    CountDownProgressBar endBtn;
    RelativeLayout endLayoutBtn;
    Handler handler = new Handler();
    private boolean isBind = false;
    private boolean isDownCut = false;
    /* access modifiers changed from: private */
    public boolean isFrist = true;
    /* access modifiers changed from: private */
    public boolean isPause = false;
    private LatLng last;
    private long lastTime;
    private List<LatLng> latLngs = new ArrayList();
    private BitmapDescriptor loactionDB;
    private BDLocation location;
    /* access modifiers changed from: private */
    public long longTime;
    private BaiduMap mBaiduMap;
    private float mCurrentX;
    /* access modifiers changed from: private */
    public float mCurrentZoom;
    MapView mMapView;
    /* access modifiers changed from: private */
    public LocationService.MyBinder myBinder;
    private MyConnection myConnection;
    private MyOrientationListener myOrientationListener;
    RelativeLayout pausedBtn;
    RelativeLayout playLayout;
    RelativeLayout playLayoutBtn;
    private double pointLat;
    private double pointLon;
    private List<LatLng> points = new ArrayList();
    RelativeLayout rlAnimationView;
    RelativeLayout rlDataView;
    RelativeLayout rlMapView;
    RelativeLayout rlScreenTitle;
    /* access modifiers changed from: private */
    public HealthActivity saveHealth;
    private long screenOffTime;
    private long screenOnTime;
    TextView speed;
    Runnable timeRun = new Runnable() {
        /* class com.wade.fit.ui.sport.activity.SportActivity.C26115 */

        public void run() {
            SportActivity.access$708(SportActivity.this);
            boolean unused = SportActivity.this.isFrist = false;
            TextView textView = SportActivity.this.tvTime;
            textView.setText(String.format("%02d", Long.valueOf(SportActivity.this.longTime / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf((SportActivity.this.longTime % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf(SportActivity.this.longTime % 60)));
            TextView textView2 = SportActivity.this.useTime;
            textView2.setText(String.format("%02d", Long.valueOf(SportActivity.this.longTime / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf((SportActivity.this.longTime % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf(SportActivity.this.longTime % 60)));
            SportActivity sportActivity = SportActivity.this;
            sportActivity.postHandler(sportActivity.timeRun, 1000);
        }
    };
    TextView tvCal;
    TextView tvDistance;
    TextView tvTime;
    TextView useTime;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_sport;
    }

    public void onBackPressed() {
    }

    static /* synthetic */ long access$708(SportActivity sportActivity) {
        long j = sportActivity.longTime;
        sportActivity.longTime = 1 + j;
        return j;
    }

    /* renamed from: com.wade.fit.ui.sport.activity.SportActivity$MyConnection */
    public class MyConnection implements ServiceConnection {
        public void onServiceDisconnected(ComponentName componentName) {
        }

        public MyConnection() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            LocationService.MyBinder unused = SportActivity.this.myBinder = (LocationService.MyBinder) iBinder;
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        Typeface createFromAsset = Typeface.createFromAsset(getAssets(), "fonts/DINCondensedBold.ttf");
        this.distance2.setTypeface(createFromAsset);
        this.speed.setTypeface(createFromAsset);
        this.useTime.setTypeface(createFromAsset);
        this.tvCal.setTypeface(createFromAsset);
        this.tvTime.setTypeface(createFromAsset);
        this.tvDistance.setTypeface(createFromAsset);
        this.layoutTitle.setVisibility(View.GONE);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.rlScreenTitle.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = ScreenUtil.getStatusHeight(this);
        this.rlScreenTitle.setLayoutParams(layoutParams);
        initMapView();
        this.pointLat = getIntent().getExtras().getDouble("pointLat", 0.0d);
        this.pointLon = getIntent().getExtras().getDouble("pointLon", 0.0d);
        this.mCurrentZoom = getIntent().getExtras().getFloat("mCurrentZoom", 18.0f);
        this.saveHealth = (HealthActivity) getIntent().getExtras().getSerializable("saveHearth");
        this.lastTime = getTime(getIntent().getExtras().getString("locationTime"));
        this.loactionDB = BitmapDescriptorFactory.fromResource(R.mipmap.run_go);
        if (this.pointLat == 0.0d && this.pointLon == 0.0d) {
            double d = getIntent().getExtras().getDouble("tempPointLat", 0.0d);
            double d2 = getIntent().getExtras().getDouble("tempPointLon", 0.0d);
            locateAndZoom(new LatLng(d, d2));
            this.location = new BDLocation();
            this.location.setRadius(0.0f);
            this.location.setLatitude(d);
            this.location.setLongitude(d2);
        } else {
            this.last = new LatLng(this.pointLat, this.pointLon);
            this.points.add(this.last);
            locateAndZoom(this.last);
            this.location = new BDLocation();
            this.location.setRadius(0.0f);
            this.location.setLatitude(this.pointLat);
            this.location.setLongitude(this.pointLon);
        }
        this.endBtn.setColorArray(new int[]{Color.parseColor("#FF330B"), Color.parseColor("#FF330B")});
        this.endBtn.setOnTouchListener(new View.OnTouchListener() {
            /* class com.wade.fit.ui.sport.activity.$$Lambda$SportActivity$9fuzwo0YaHC2ht1sHe7wE2mJzg */

            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return SportActivity.this.lambda$initView$0$SportActivity(view, motionEvent);
            }
        });
        Intent intent = new Intent(this, LocationService.class);
        this.myConnection = new MyConnection();
        bindService(intent, this.myConnection, 1);
        this.rlAnimationView.setVisibility(View.VISIBLE);
        this.rlMapView.setVisibility(View.GONE);
        this.rlDataView.setVisibility(View.GONE);
        postHandler(new Runnable() {
            /* class com.wade.fit.ui.sport.activity.$$Lambda$SportActivity$VYrL3WyzWLU4OoC85EfCdf6sWc */

            public final void run() {
                SportActivity.this.lambda$initView$1$SportActivity();
            }
        }, 500);
    }

    public /* synthetic */ boolean lambda$initView$0$SportActivity(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            this.isDownCut = true;
            startCutDown();
        } else if (action == 1 && this.isDownCut) {
            this.endBtn.reset();
        }
        return true;
    }

    public /* synthetic */ void lambda$initView$1$SportActivity() {
        animationScale(this.animationTextView);
    }

    private void startCutDown() {
        this.endBtn.setDuration(1000, new CountDownProgressBar.OnFinishListener() {
            /* class com.wade.fit.ui.sport.activity.$$Lambda$SportActivity$SMDBIdtXJZboaZM7DUXh8XC5eQ8 */

            public final void onFinish() {
                SportActivity.this.lambda$startCutDown$2$SportActivity();
            }
        });
    }

    public /* synthetic */ void lambda$startCutDown$2$SportActivity() {
        this.isDownCut = false;
        this.myBinder.stopAlarm();
        this.handler.removeCallbacks(this.timeRun);
        if (this.distanceTotal <= 0.0d || this.longTime < 180) {
            DialogHelperNew.showSportLowTipDialog(this, new DialogHelperNew.IOnclickListener() {
                /* class com.wade.fit.ui.sport.activity.SportActivity.C26071 */

                public void leftButton() {
                    IntentUtil.goToActivityAndFinish(SportActivity.this, SportOutActivity.class);
                }

                public void rightButton() {
                    SportActivity.this.myBinder.goOnAlarm();
                    SportActivity sportActivity = SportActivity.this;
                    sportActivity.postHandler(sportActivity.timeRun, 1000);
                    SportActivity.this.btnEndAnimation();
                }
            });
        } else {
            saveData();
        }
    }

    private void initMapView() {
        this.mBaiduMap = this.mMapView.getMap();
        this.mBaiduMap.setMyLocationEnabled(true);
        this.mMapView.removeViewAt(1);
        this.mMapView.removeViewAt(2);
        this.mMapView.showZoomControls(false);
        this.mCurrentZoom = 18.0f;
        this.mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
            /* class com.wade.fit.ui.sport.activity.SportActivity.C26082 */

            public void onMapStatusChange(MapStatus mapStatus) {
            }

            public void onMapStatusChangeStart(MapStatus mapStatus) {
            }

            public void onMapStatusChangeStart(MapStatus mapStatus, int i) {
            }

            public void onMapStatusChangeFinish(MapStatus mapStatus) {
                float unused = SportActivity.this.mCurrentZoom = mapStatus.zoom;
            }
        });
        this.mBaiduMap.setMyLocationConfiguration(new MyLocationConfiguration(MyLocationConfiguration.LocationMode.FOLLOWING, true, null));
        this.myOrientationListener = new MyOrientationListener(this);
        this.myOrientationListener.setOnOrientationListener(new MyOrientationListener.OnOrientationListener() {
            /* class com.wade.fit.ui.sport.activity.$$Lambda$SportActivity$dv3NkLZgHdxUt2UKzVQbj3RPCAk */

            public final void onOrientationChanged(float f) {
                SportActivity.this.lambda$initMapView$3$SportActivity(f);
            }
        });
        this.myOrientationListener.start();
    }

    public /* synthetic */ void lambda$initMapView$3$SportActivity(float f) {
        this.mCurrentX = f;
        if (this.location != null) {
            this.mBaiduMap.setMyLocationData(new MyLocationData.Builder().direction(this.mCurrentX).accuracy(this.location.getRadius()).latitude(this.location.getLatitude()).longitude(this.location.getLongitude()).build());
            this.mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(MyLocationConfiguration.LocationMode.NORMAL, true, this.loactionDB));
        }
    }

    public static long getTime(String str) {
        try {
            return DateUtil.simpleDateFormat.parse(str).getTime();
        } catch (ParseException unused) {
            return 0;
        }
    }

    private void completeCalorie() {
        this.calorie = SportDataHelper.getSportGpsCarloy(this.saveHealth.getType(), (float) AppApplication.getInstance().getUserBean().getWeight(), this.distanceTotal / 1000.0d);
        this.tvCal.setText(String.valueOf(this.calorie));
    }

    private String completeAvgPace(double d, long j) {
        if (!BleSdkWrapper.isDistUnitKm()) {
            d = (double) ((int) UnitUtil.km2mile((float) d));
        }
        int i = 0;
        if (d != 0.0d) {
            double d2 = (double) (j * 1000);
            Double.isNaN(d2);
            i = (int) (d2 / d);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i / 60);
        stringBuffer.append("'");
        stringBuffer.append(i % 60);
        stringBuffer.append("\"");
        this.avgSpeed = stringBuffer.toString();
        return stringBuffer.toString();
    }

    private void locateAndZoom(LatLng latLng) {
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(latLng).zoom(this.mCurrentZoom);
        this.mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
    }

    private LatLng getMostAccuracyLocation(BDLocation bDLocation) {
        LatLng latLng = new LatLng(bDLocation.getLatitude(), bDLocation.getLongitude());
        if (DistanceUtil.getDistance(this.last, latLng) > 50.0d) {
            this.last = latLng;
            this.latLngs.clear();
            return null;
        }
        this.latLngs.add(latLng);
        this.last = latLng;
        if (this.latLngs.size() < 5) {
            return null;
        }
        this.latLngs.clear();
        return latLng;
    }

    /* access modifiers changed from: package-private */
    public void hideMapView() {
        this.rlMapView.setVisibility(View.GONE);
        this.rlDataView.setVisibility(View.VISIBLE);
    }

    /* access modifiers changed from: package-private */
    public void showMapView() {
        this.rlMapView.setVisibility(View.VISIBLE);
        this.rlDataView.setVisibility(View.GONE);
    }

    /* access modifiers changed from: private */
    public void animationScale(final TextView textView) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, "ScaleX", 1.0f, 0.8f, 1.5f, 1.0f);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(textView, "ScaleY", 1.0f, 0.8f, 1.5f, 1.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(ofFloat, ofFloat2);
        animatorSet.setDuration(1000L);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            /* class com.wade.fit.ui.sport.activity.SportActivity.C26093 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                if (textView.getText().equals("3")) {
                    SportActivity.this.postHandler(new Runnable(textView) {
                        /* class com.wade.fit.ui.sport.activity.$$Lambda$SportActivity$3$LAx_T1NXuiJMhwh3_Zru7NE3gZg */
                        private final /* synthetic */ TextView f$1;

                        {
                            this.f$1 = r2;
                        }

                        public final void run() {
                            SportActivity.C26093.this.lambda$onAnimationEnd$0$SportActivity$3(this.f$1);
                        }
                    }, 200);
                } else if (textView.getText().equals("2")) {
                    SportActivity.this.postHandler(new Runnable(textView) {
                        /* class com.wade.fit.ui.sport.activity.$$Lambda$SportActivity$3$gMZZGRjtLg90srRv2d4cdxNQ87s */
                        private final /* synthetic */ TextView f$1;

                        {
                            this.f$1 = r2;
                        }

                        public final void run() {
                            SportActivity.C26093.this.lambda$onAnimationEnd$1$SportActivity$3(this.f$1);
                        }
                    }, 200);
                } else if (textView.getText().equals("1")) {
                    SportActivity.this.postHandler(new Runnable(textView) {
                        /* class com.wade.fit.ui.sport.activity.$$Lambda$SportActivity$3$Vcdv82xEYTQ4E6sY3_B3Nms308 */
                        private final /* synthetic */ TextView f$1;

                        {
                            this.f$1 = r2;
                        }

                        public final void run() {
                            SportActivity.C26093.this.lambda$onAnimationEnd$2$SportActivity$3(this.f$1);
                        }
                    }, 200);
                } else {
                    SportActivity.this.rlAnimationView.setVisibility(View.GONE);
                    textView.setText("3");
                    SportActivity.this.rlDataView.setVisibility(View.VISIBLE);
                    LogUtil.d("开启定时");
                    boolean unused = SportActivity.this.isFrist = false;
                    SportActivity.this.setupService();
                    SportActivity sportActivity = SportActivity.this;
                    sportActivity.postHandler(sportActivity.timeRun, 1000);
                }
            }

            public /* synthetic */ void lambda$onAnimationEnd$0$SportActivity$3(TextView textView) {
                textView.setText("2");
                SportActivity.this.animationScale(textView);
            }

            public /* synthetic */ void lambda$onAnimationEnd$1$SportActivity$3(TextView textView) {
                textView.setText("1");
                SportActivity.this.animationScale(textView);
            }

            public /* synthetic */ void lambda$onAnimationEnd$2$SportActivity$3(TextView textView) {
                textView.setText("GO");
                SportActivity.this.animationScale(textView);
            }
        });
        animatorSet.start();
    }

    /* access modifiers changed from: private */
    public void setupService() {
        Intent intent = new Intent(this, StepService.class);
        this.isBind = bindService(intent, this.conn, 1);
        startService(intent);
    }

    /* access modifiers changed from: private */
    public void postHandler(Runnable runnable, long j) {
        this.handler.postDelayed(runnable, j);
    }

    /* access modifiers changed from: package-private */
    public void pauseLocation() {
        this.playLayout.setVisibility(View.VISIBLE);
        this.pausedBtn.setVisibility(View.GONE);
        this.myBinder.stopAlarm();
        this.handler.removeCallbacks(this.timeRun);
        btnStartAnimation();
        this.isPause = true;
    }

    /* access modifiers changed from: package-private */
    public void contiueLocation() {
        this.myBinder.goOnAlarm();
        postHandler(this.timeRun, 1000);
        btnEndAnimation();
    }

    private void btnStartAnimation() {
        new ObjectAnimator();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.playLayoutBtn, "translationX", 0.0f, -200.0f);
        new ObjectAnimator();
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.endLayoutBtn, "translationX", 0.0f, 200.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(ofFloat, ofFloat2);
        animatorSet.setDuration(200L);
        animatorSet.setInterpolator(new SpringScaleInterpolator(0.1f));
        animatorSet.start();
    }

    /* access modifiers changed from: private */
    public void btnEndAnimation() {
        new ObjectAnimator();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.playLayoutBtn, "translationX", -200.0f, 0.0f);
        new ObjectAnimator();
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.endLayoutBtn, "translationX", 200.0f, 0.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(ofFloat, ofFloat2);
        animatorSet.setDuration(200L);
        animatorSet.start();
        animatorSet.addListener(new AnimatorListenerAdapter() {
            /* class com.wade.fit.ui.sport.activity.SportActivity.C26126 */

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                SportActivity.this.playLayout.setVisibility(View.GONE);
                SportActivity.this.pausedBtn.setVisibility(View.VISIBLE);
                boolean unused = SportActivity.this.isPause = false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        LogUtil.d("ACTION_SCREEN_OFF");
        this.screenOffTime = System.currentTimeMillis();
        this.handler.removeCallbacks(this.timeRun);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.isFrist && !this.isPause) {
            this.handler.removeCallbacks(this.timeRun);
            LogUtil.d("ACTION_SCREEN_ON");
            this.screenOnTime = System.currentTimeMillis();
            this.longTime += (this.screenOnTime - this.screenOffTime) / 1000;
            this.tvTime.setText(String.format("%02d", Long.valueOf(this.longTime / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf((this.longTime % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf(this.longTime % 60)));
            this.useTime.setText(String.format("%02d", Long.valueOf(this.longTime / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf((this.longTime % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf(this.longTime % 60)));
            double d = this.distanceTotal;
            if (d == 0.0d) {
                this.speed.setText("-'-\"");
            } else {
                this.speed.setText(completeAvgPace(d, this.longTime));
            }
            postHandler(this.timeRun, 1000);
        }
    }

    private void saveData() {
        this.saveHealth.setDistance((int) this.distanceTotal);
        this.saveHealth.setAvgSpeed(this.avgSpeed);
        this.saveHealth.setCalories(this.calorie);
        this.saveHealth.setDurations((int) this.longTime);
        ArrayList arrayList = new ArrayList();
        for (LatLng latLng : this.points) {
            HealthGpsItem healthGpsItem = new HealthGpsItem();
            healthGpsItem.setLatitude(Double.valueOf(latLng.latitude));
            healthGpsItem.setLongitude(Double.valueOf(latLng.longitude));
            healthGpsItem.setDate(Long.valueOf(this.saveHealth.getDate()));
            arrayList.add(healthGpsItem);
        }
        HealthGpsItemDao healthGpsItemDao = AppApplication.getInstance().getDaoSession().getHealthGpsItemDao();
        HealthActivityDao healthActivityDao = AppApplication.getInstance().getDaoSession().getHealthActivityDao();
        healthGpsItemDao.saveInTx(arrayList);
        healthActivityDao.save(this.saveHealth);
        Bundle bundle = new Bundle();
        bundle.putInt("ACTIVITY_FROM", 2);
        bundle.putSerializable("SPORT_DATA", this.saveHealth);
        IntentUtil.goToActivityAndFinish(this, SportDetailActivityNew.class, bundle);
        EventBusHelper.post(1003);
    }

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
        super.handleMessage(baseMessage);
        int type = baseMessage.getType();
        if (type == 202) {
            this.location = (BDLocation) baseMessage.getData();
            LogUtil.d("location:," + this.location.getLatitude() + "," + this.location.getLongitude());
            LatLng latLng = new LatLng(this.location.getLatitude(), this.location.getLongitude());
            double distance = DistanceUtil.getDistance(this.last, latLng);
            long time = getTime(this.location.getTime());
            double d = (double) (time - this.lastTime);
            Double.isNaN(d);
            double d2 = (distance / d) / 1000.0d;
            if (this.last == null) {
                this.last = latLng;
                this.points.add(latLng);
            } else if (distance < 10.0d || d2 > 20.0d) {
                LogUtil.d("location:过滤点" + DistanceUtil.getDistance(this.last, latLng) + "/" + d2);
                LogUtil.d("时间点：" + time + "/" + this.lastTime);
            } else {
                this.distanceTotal += distance;
                this.points.add(latLng);
                this.last = latLng;
                this.lastTime = time;
                locateAndZoom(latLng);
                this.mMapView.getMap().clear();
                this.mBaiduMap.addOverlay(new PolylineOptions().width(8).color(-1426128896).points(this.points));
                this.tvDistance.setText(String.format("%.2f", Double.valueOf(this.distanceTotal / 1000.0d)));
                this.distance2.setText(String.format("%.2f", Double.valueOf(this.distanceTotal / 1000.0d)));
                double d3 = this.distanceTotal;
                if (d3 == 0.0d) {
                    this.speed.setText("-'-\"");
                } else {
                    this.speed.setText(completeAvgPace(d3, this.longTime));
                }
                completeCalorie();
            }
        } else if (type == 203) {
            this.longTime = ((Long) baseMessage.getData()).longValue();
            this.tvTime.setText(String.format("%02d", Long.valueOf(this.longTime / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf((this.longTime % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf(this.longTime % 60)));
            this.useTime.setText(String.format("%02d", Long.valueOf(this.longTime / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf((this.longTime % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Long.valueOf(this.longTime % 60)));
            double d4 = this.distanceTotal;
            if (d4 == 0.0d) {
                this.speed.setText("-'-\"");
            } else {
                this.speed.setText(completeAvgPace(d4, this.longTime));
            }
        } else if (type == 1004) {
            showToast(getResources().getString(R.string.state_network_error));
        } else if (type == 1005) {
            EventBusHelper.post(200);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mBaiduMap.setMyLocationEnabled(false);
        this.mMapView.getMap().clear();
        this.mMapView.onDestroy();
        this.mMapView = null;
        BitmapDescriptor bitmapDescriptor = this.loactionDB;
        if (bitmapDescriptor != null) {
            bitmapDescriptor.recycle();
        }
        stopService(new Intent(this, LocationService.class));
        unbindService(this.myConnection);
        this.myOrientationListener.stop();
        if (this.isBind) {
            unbindService(this.conn);
        }
    }
}
