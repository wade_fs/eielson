package com.wade.fit.ui.device.activity;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.device.activity.NoticeActivity_ViewBinding */
public class NoticeActivity_ViewBinding implements Unbinder {
    private NoticeActivity target;
    private View view2131297097;

    public NoticeActivity_ViewBinding(NoticeActivity noticeActivity) {
        this(noticeActivity, noticeActivity.getWindow().getDecorView());
    }

    public NoticeActivity_ViewBinding(final NoticeActivity noticeActivity, View view) {
        this.target = noticeActivity;
        noticeActivity.itNoNotice = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itNoNotice, "field 'itNoNotice'", ItemToggleLayout.class);
        noticeActivity.itMessage = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itMessage, "field 'itMessage'", ItemToggleLayout.class);
        noticeActivity.itCall = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itCall, "field 'itCall'", ItemToggleLayout.class);
        noticeActivity.itQQ = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itQQ, "field 'itQQ'", ItemToggleLayout.class);
        noticeActivity.itFacebook = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itFacebook, "field 'itFacebook'", ItemToggleLayout.class);
        noticeActivity.itWeChat = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itWeChat, "field 'itWeChat'", ItemToggleLayout.class);
        noticeActivity.itLinked = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itLinked, "field 'itLinked'", ItemToggleLayout.class);
        noticeActivity.itSkype = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itSkype, "field 'itSkype'", ItemToggleLayout.class);
        noticeActivity.itInstagram = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itInstagram, "field 'itInstagram'", ItemToggleLayout.class);
        noticeActivity.itTwitter = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itTwitter, "field 'itTwitter'", ItemToggleLayout.class);
        noticeActivity.itLine = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itLine, "field 'itLine'", ItemToggleLayout.class);
        noticeActivity.itWhatsApp = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itWhatsApp, "field 'itWhatsApp'", ItemToggleLayout.class);
        noticeActivity.itVK = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itVK, "field 'itVK'", ItemToggleLayout.class);
        noticeActivity.itMessenger = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itMessenger, "field 'itMessenger'", ItemToggleLayout.class);
        noticeActivity.itEmial = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itEmail, "field 'itEmial'", ItemToggleLayout.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.txtSetting, "method 'toSetNoti'");
        this.view2131297097 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.NoticeActivity_ViewBinding.C24731 */

            public void doClick(View view) {
                noticeActivity.toSetNoti();
            }
        });
    }

    public void unbind() {
        NoticeActivity noticeActivity = this.target;
        if (noticeActivity != null) {
            this.target = null;
            noticeActivity.itNoNotice = null;
            noticeActivity.itMessage = null;
            noticeActivity.itCall = null;
            noticeActivity.itQQ = null;
            noticeActivity.itFacebook = null;
            noticeActivity.itWeChat = null;
            noticeActivity.itLinked = null;
            noticeActivity.itSkype = null;
            noticeActivity.itInstagram = null;
            noticeActivity.itTwitter = null;
            noticeActivity.itLine = null;
            noticeActivity.itWhatsApp = null;
            noticeActivity.itVK = null;
            noticeActivity.itMessenger = null;
            noticeActivity.itEmial = null;
            this.view2131297097.setOnClickListener(null);
            this.view2131297097 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
