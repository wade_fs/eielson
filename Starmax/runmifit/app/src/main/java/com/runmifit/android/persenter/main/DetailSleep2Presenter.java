package com.wade.fit.persenter.main;

import android.text.TextUtils;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.gen.HealthSleepDao;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.model.bean.SleepDetailVO;
import com.wade.fit.persenter.sport.BaseTimePresenter;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.util.SharePreferenceUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DetailSleep2Presenter extends BaseTimePresenter {
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd");
    List<SleepDetailVO.WeekSleep> weekSleeps = new ArrayList();

    public SleepDetailVO getDetailCurrent(DetailTimeType detailTimeType, Date date) {
        currentDate(detailTimeType, date);
        getWeekSleep();
        return getDetail();
    }

    public SleepDetailVO getDetailCurrent(DetailTimeType detailTimeType) {
        currentDate(detailTimeType);
        getWeekSleep();
        return getDetail();
    }

    public SleepDetailVO getDetailNext(DetailTimeType detailTimeType) {
        nextDate(detailTimeType);
        getWeekSleep();
        return getDetail();
    }

    public SleepDetailVO getDetailPre(DetailTimeType detailTimeType) {
        preDate(detailTimeType);
        getWeekSleep();
        return getDetail();
    }

    public SleepDetailVO getSixMonth() {
        setSixMonth();
        getWeekSleep();
        return get();
    }

    public SleepDetailVO getYearMonth() {
        setYear();
        getWeekSleep();
        return get();
    }

    private void getWeekSleep() {
        List list = AppApplication.getInstance().getDaoSession().getHealthSleepDao().queryBuilder().orderDesc(HealthSleepDao.Properties.Date).build().list();
        int intValue = ((Integer) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.WEEK_START_DAY_INDEX, 0)).intValue();
        SleepDetailVO.WeekSleep weekSleep = new SleepDetailVO.WeekSleep();
        weekSleep.label = AppApplication.getInstance().getResources().getString(R.string.chart_type_week_str);
        weekSleep.avgTime = "0" + AppApplication.getInstance().getResources().getString(R.string.unit_hour) + 0 + AppApplication.getInstance().getResources().getString(R.string.unit_minute);
        this.weekSleeps.clear();
        this.weekSleeps.add(weekSleep);
        if (!list.isEmpty()) {
            Date weekEndDate = ProDbUtils.getWeekEndDate(0, intValue);
            Calendar instance = Calendar.getInstance();
            instance.setTime(weekEndDate);
            setCalendarZero(instance);
            Date date = new Date(((HealthSleep) list.get(list.size() - 1)).getDate());
            int size = list.size();
            int i = 1;
            int i2 = 0;
            int i3 = 0;
            while (instance.getTime().getTime() > date.getTime()) {
                HealthSleep healthSleep = null;
                for (int i4 = 0; i4 < size; i4++) {
                    int i5 = instance.get(1);
                    int i6 = instance.get(2) + 1;
                    int i7 = instance.get(5);
                    HealthSleep healthSleep2 = (HealthSleep) list.get(i4);
                    if (healthSleep2.getYear() == i5 && healthSleep2.getMonth() == i6 && healthSleep2.getDay() == i7) {
                        healthSleep = healthSleep2;
                    }
                }
                if (healthSleep != null) {
                    SleepDetailVO.DaySleep daySleep = new SleepDetailVO.DaySleep();
                    daySleep.day = healthSleep.getDay() + AppApplication.getInstance().getResources().getString(R.string.day);
                    daySleep.time = formattime(healthSleep);
                    daySleep.date = healthSleep.getDate();
                    i2 += healthSleep.getTotalSleepMinutes();
                    i3++;
                    List<SleepDetailVO.WeekSleep> list2 = this.weekSleeps;
                    SleepDetailVO.WeekSleep weekSleep2 = list2.get(list2.size() - 1);
                    weekSleep2.daySleeps.add(daySleep);
                    StringBuilder sb = new StringBuilder();
                    int i8 = i2 / i3;
                    sb.append(i8 / 60);
                    sb.append(AppApplication.getInstance().getResources().getString(R.string.unit_hour));
                    sb.append(i8 % 60);
                    sb.append(AppApplication.getInstance().getResources().getString(R.string.unit_minute));
                    weekSleep2.avgTime = sb.toString();
                }
                instance.add(5, -1);
                if (i % 7 == 0) {
                    SleepDetailVO.WeekSleep weekSleep3 = new SleepDetailVO.WeekSleep();
                    weekSleep3.label = getWeekStr2(instance);
                    this.weekSleeps.add(weekSleep3);
                    i2 = 0;
                    i3 = 0;
                }
                i++;
            }
            for (int i9 = 0; i9 < this.weekSleeps.size(); i9++) {
                SleepDetailVO.WeekSleep weekSleep4 = this.weekSleeps.get(i9);
                if (TextUtils.isEmpty(weekSleep4.avgTime)) {
                    this.weekSleeps.remove(weekSleep4);
                }
            }
        }
    }

    private String formattime(HealthSleep healthSleep) {
        return (healthSleep.getTotalSleepMinutes() / 60) + AppApplication.getInstance().getResources().getString(R.string.unit_hour) + (healthSleep.getTotalSleepMinutes() % 60) + AppApplication.getInstance().getResources().getString(R.string.unit_minute);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v0, resolved type: com.wade.fit.greendao.bean.HealthSleep} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.wade.fit.model.bean.SleepDetailVO get() {
        /*
            r21 = this;
            r0 = r21
            java.util.List r1 = r0.dates
            r1.clear()
            com.wade.fit.app.AppApplication r1 = com.wade.fit.app.AppApplication.getInstance()
            com.wade.fit.greendao.gen.DaoSession r1 = r1.getDaoSession()
            com.wade.fit.greendao.gen.HealthSleepDao r1 = r1.getHealthSleepDao()
            org.greenrobot.greendao.query.QueryBuilder r1 = r1.queryBuilder()
            org.greenrobot.greendao.Property r2 = com.wade.fit.greendao.gen.HealthSleepDao.Properties.Date
            java.util.Date r3 = r0.endDate
            long r3 = r3.getTime()
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            java.util.Date r4 = r0.startDate
            long r4 = r4.getTime()
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            org.greenrobot.greendao.query.WhereCondition r2 = r2.between(r3, r4)
            r3 = 0
            org.greenrobot.greendao.query.WhereCondition[] r4 = new org.greenrobot.greendao.query.WhereCondition[r3]
            org.greenrobot.greendao.query.QueryBuilder r1 = r1.where(r2, r4)
            r2 = 1
            org.greenrobot.greendao.Property[] r4 = new org.greenrobot.greendao.Property[r2]
            org.greenrobot.greendao.Property r5 = com.wade.fit.greendao.gen.HealthSleepDao.Properties.Date
            r4[r3] = r5
            org.greenrobot.greendao.query.QueryBuilder r1 = r1.orderDesc(r4)
            org.greenrobot.greendao.query.Query r1 = r1.build()
            java.util.List r1 = r1.list()
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            java.util.Date r5 = r0.endDate
            r4.setTime(r5)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            com.wade.fit.model.bean.DetailTimeType r6 = r0.timeType
            com.wade.fit.util.log.LogUtil.m5264d(r6)
            com.wade.fit.greendao.bean.HealthSleep r6 = new com.wade.fit.greendao.bean.HealthSleep
            r6.<init>()
            r7 = 0
            r8 = 0
        L_0x0065:
            java.util.Date r9 = r4.getTime()
            long r9 = r9.getTime()
            java.util.Date r11 = r0.startDate
            long r11 = r11.getTime()
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 > 0) goto L_0x03fc
            int r9 = r4.get(r2)
            r10 = 2
            int r11 = r4.get(r10)
            int r11 = r11 + r2
            r12 = 5
            int r13 = r4.get(r12)
            java.util.Iterator r14 = r1.iterator()
            r16 = 0
        L_0x008c:
            boolean r17 = r14.hasNext()
            if (r17 == 0) goto L_0x01e1
            java.lang.Object r17 = r14.next()
            r15 = r17
            com.wade.fit.greendao.bean.HealthSleep r15 = (com.wade.fit.greendao.bean.HealthSleep) r15
            int r12 = r15.getYear()
            if (r12 != r9) goto L_0x01cf
            int r12 = r15.getMonth()
            if (r12 != r11) goto L_0x01cf
            int r12 = r15.getDay()
            if (r12 != r13) goto L_0x01cf
            com.wade.fit.app.AppApplication r12 = com.wade.fit.app.AppApplication.getInstance()
            com.wade.fit.greendao.gen.DaoSession r12 = r12.getDaoSession()
            com.wade.fit.greendao.gen.HealthSleepItemDao r12 = r12.getHealthSleepItemDao()
            org.greenrobot.greendao.query.QueryBuilder r2 = r12.queryBuilder()
            org.greenrobot.greendao.Property r3 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Year
            r18 = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r9)
            org.greenrobot.greendao.query.WhereCondition r1 = r3.eq(r1)
            org.greenrobot.greendao.query.WhereCondition[] r3 = new org.greenrobot.greendao.query.WhereCondition[r10]
            org.greenrobot.greendao.Property r10 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Month
            r19 = r14
            java.lang.Integer r14 = java.lang.Integer.valueOf(r11)
            org.greenrobot.greendao.query.WhereCondition r10 = r10.eq(r14)
            r14 = 0
            r3[r14] = r10
            org.greenrobot.greendao.Property r10 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Day
            java.lang.Integer r14 = java.lang.Integer.valueOf(r13)
            org.greenrobot.greendao.query.WhereCondition r10 = r10.eq(r14)
            r14 = 1
            r3[r14] = r10
            org.greenrobot.greendao.query.QueryBuilder r1 = r2.where(r1, r3)
            org.greenrobot.greendao.Property[] r2 = new org.greenrobot.greendao.Property[r14]
            org.greenrobot.greendao.Property r3 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Date
            r10 = 0
            r2[r10] = r3
            org.greenrobot.greendao.query.QueryBuilder r1 = r1.orderDesc(r2)
            org.greenrobot.greendao.query.Query r1 = r1.build()
            java.util.List r1 = r1.list()
            if (r1 == 0) goto L_0x01ca
            java.lang.Object r2 = r4.clone()
            java.util.Calendar r2 = (java.util.Calendar) r2
            r3 = -1
            r10 = 5
            r2.add(r10, r3)
            r3 = 2
            int r14 = r2.get(r3)
            r16 = 1
            int r14 = r14 + 1
            int r2 = r2.get(r10)
            r1.size()
            org.greenrobot.greendao.query.QueryBuilder r10 = r12.queryBuilder()
            org.greenrobot.greendao.Property r12 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Year
            r20 = r5
            java.lang.Integer r5 = java.lang.Integer.valueOf(r9)
            org.greenrobot.greendao.query.WhereCondition r5 = r12.eq(r5)
            org.greenrobot.greendao.query.WhereCondition[] r12 = new org.greenrobot.greendao.query.WhereCondition[r3]
            org.greenrobot.greendao.Property r3 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Month
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            org.greenrobot.greendao.query.WhereCondition r3 = r3.eq(r14)
            r14 = 0
            r12[r14] = r3
            org.greenrobot.greendao.Property r3 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Day
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            org.greenrobot.greendao.query.WhereCondition r2 = r3.eq(r2)
            r3 = 1
            r12[r3] = r2
            org.greenrobot.greendao.query.QueryBuilder r2 = r10.where(r5, r12)
            org.greenrobot.greendao.Property[] r5 = new org.greenrobot.greendao.Property[r3]
            org.greenrobot.greendao.Property r3 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Date
            r5[r14] = r3
            org.greenrobot.greendao.query.QueryBuilder r2 = r2.orderDesc(r5)
            org.greenrobot.greendao.query.Query r2 = r2.build()
            java.util.List r2 = r2.list()
            r3 = 0
            r5 = 0
        L_0x015e:
            r10 = 85
            if (r3 >= r10) goto L_0x01c1
            com.wade.fit.greendao.bean.HealthSleepItem r10 = new com.wade.fit.greendao.bean.HealthSleepItem
            r10.<init>()
            r12 = 10
            r10.setOffsetMinute(r12)
            r12 = 18
            if (r3 >= r12) goto L_0x018b
            if (r2 == 0) goto L_0x0199
            int r14 = r2.size()
            if (r3 >= r14) goto L_0x0199
            int r14 = r2.size()
            if (r14 <= r12) goto L_0x0199
            int r10 = r2.size()
            int r10 = r10 - r12
            int r10 = r10 + r3
            java.lang.Object r10 = r2.get(r10)
            com.wade.fit.greendao.bean.HealthSleepItem r10 = (com.wade.fit.greendao.bean.HealthSleepItem) r10
            goto L_0x0199
        L_0x018b:
            int r12 = r1.size()
            if (r3 >= r12) goto L_0x0199
            int r10 = r3 + -18
            java.lang.Object r10 = r1.get(r10)
            com.wade.fit.greendao.bean.HealthSleepItem r10 = (com.wade.fit.greendao.bean.HealthSleepItem) r10
        L_0x0199:
            int r12 = r10.getSleepStatus()
            r14 = 1
            if (r12 == r14) goto L_0x01bc
            int r12 = r10.getSleepStatus()
            r14 = 3
            if (r12 == r14) goto L_0x01bc
            int r12 = r10.getSleepStatus()
            r14 = 2
            if (r12 == r14) goto L_0x01bc
            int r12 = r10.getSleepStatus()
            r14 = 5
            if (r12 == r14) goto L_0x01bc
            int r10 = r10.getSleepStatus()
            r12 = 4
            if (r10 != r12) goto L_0x01be
        L_0x01bc:
            int r5 = r5 + 10
        L_0x01be:
            int r3 = r3 + 1
            goto L_0x015e
        L_0x01c1:
            if (r5 != 0) goto L_0x01c6
            r16 = 0
            goto L_0x01d5
        L_0x01c6:
            r15.setTotalSleepMinutes(r5)
            goto L_0x01cc
        L_0x01ca:
            r20 = r5
        L_0x01cc:
            r16 = r15
            goto L_0x01d5
        L_0x01cf:
            r18 = r1
            r20 = r5
            r19 = r14
        L_0x01d5:
            r1 = r18
            r14 = r19
            r5 = r20
            r2 = 1
            r3 = 0
            r10 = 2
            r12 = 5
            goto L_0x008c
        L_0x01e1:
            r18 = r1
            r20 = r5
            if (r16 != 0) goto L_0x01ec
            com.wade.fit.greendao.bean.HealthSleep r16 = new com.wade.fit.greendao.bean.HealthSleep
            r16.<init>()
        L_0x01ec:
            r1 = r16
            int[] r2 = com.wade.fit.persenter.main.DetailSleep2Presenter.C23961.$SwitchMap$com$wade.fit$model$bean$DetailTimeType
            com.wade.fit.model.bean.DetailTimeType r3 = r0.timeType
            int r3 = r3.ordinal()
            r2 = r2[r3]
            r3 = 1
            if (r2 == r3) goto L_0x03d8
            r3 = 2
            if (r2 == r3) goto L_0x0320
            r3 = 3
            if (r2 == r3) goto L_0x0208
        L_0x0201:
            r2 = r20
            r1 = 5
            r3 = 1
        L_0x0205:
            r14 = 0
            goto L_0x03f2
        L_0x0208:
            int r2 = r1.getTotalSleepMinutes()
            if (r2 <= 0) goto L_0x0210
            int r8 = r8 + 1
        L_0x0210:
            int r2 = r6.getTotalSleepMinutes()
            int r3 = r1.getTotalSleepMinutes()
            int r2 = r2 + r3
            r6.setTotalSleepMinutes(r2)
            int r2 = r6.getDeepSleepMinutes()
            int r3 = r1.getDeepSleepMinutes()
            int r2 = r2 + r3
            r6.setDeepSleepMinutes(r2)
            int r2 = r6.getLightSleepMinutes()
            int r3 = r1.getLightSleepMinutes()
            int r2 = r2 + r3
            r6.setLightSleepMinutes(r2)
            int r2 = r6.getWakeMunutes()
            int r3 = r1.getWakeMunutes()
            int r2 = r2 + r3
            r6.setWakeMunutes(r2)
            int r2 = r6.getEyeMinutes()
            int r3 = r1.getEyeMinutes()
            int r2 = r2 + r3
            r6.setEyeMinutes(r2)
            int r2 = r6.getSleepMinutes()
            int r1 = r1.getSleepMinutes()
            int r2 = r2 + r1
            r6.setSleepMinutes(r2)
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            java.util.Date r2 = r0.startDate
            r1.setTime(r2)
            r2 = 11
            r3 = 0
            r1.set(r2, r3)
            r2 = 12
            r1.set(r2, r3)
            r2 = 13
            r1.set(r2, r3)
            r2 = 14
            r1.set(r2, r3)
            int r2 = r11 + -1
            boolean r2 = r0.isEndMonth(r9, r2, r13)
            if (r2 != 0) goto L_0x0292
            java.util.Date r2 = r4.getTime()
            long r2 = r2.getTime()
            java.util.Date r1 = r1.getTime()
            long r9 = r1.getTime()
            int r1 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r1 != 0) goto L_0x0201
        L_0x0292:
            java.lang.String r1 = r6.toString()
            com.wade.fit.util.log.LogUtil.d(r1)
            r0.setDateLabel(r7, r4, r11)
            com.wade.fit.greendao.bean.HealthSleep r1 = new com.wade.fit.greendao.bean.HealthSleep
            r1.<init>()
            java.util.Date r2 = r4.getTime()
            long r2 = r2.getTime()
            java.util.Date r5 = r0.startDate
            long r9 = r5.getTime()
            int r5 = (r2 > r9 ? 1 : (r2 == r9 ? 0 : -1))
            if (r5 != 0) goto L_0x02c2
            int r1 = r20.size()
            r2 = 1
            int r1 = r1 - r2
            r2 = r20
            java.lang.Object r1 = r2.get(r1)
            com.wade.fit.greendao.bean.HealthSleep r1 = (com.wade.fit.greendao.bean.HealthSleep) r1
            goto L_0x02c7
        L_0x02c2:
            r2 = r20
            r2.add(r1)
        L_0x02c7:
            java.text.SimpleDateFormat r3 = r0.dateFormat4
            java.util.Date r5 = r4.getTime()
            java.lang.String r3 = r3.format(r5)
            r1.setRemark(r3)
            if (r8 == 0) goto L_0x0306
            int r3 = r6.getTotalSleepMinutes()
            int r3 = r3 / r8
            r1.setTotalSleepMinutes(r3)
            int r3 = r6.getDeepSleepMinutes()
            int r3 = r3 / r8
            r1.setDeepSleepMinutes(r3)
            int r3 = r6.getLightSleepMinutes()
            int r3 = r3 / r8
            r1.setLightSleepMinutes(r3)
            int r3 = r6.getEyeMinutes()
            int r3 = r3 / r8
            r1.setEyeMinutes(r3)
            int r3 = r6.getWakeMunutes()
            int r3 = r3 / r8
            r1.setWakeMunutes(r3)
            int r3 = r6.getSleepMinutes()
            int r3 = r3 / r8
            r1.setSleepMinutes(r3)
        L_0x0306:
            r1 = 0
            r6.setTotalSleepMinutes(r1)
            r6.setDeepSleepMinutes(r1)
            r6.setLightSleepMinutes(r1)
            r6.setWakeMunutes(r1)
            r6.setEyeMinutes(r1)
            r6.setSleepMinutes(r1)
            int r7 = r7 + 1
            r1 = 5
            r3 = 1
            r8 = 0
            goto L_0x0205
        L_0x0320:
            r2 = r20
            int r3 = r1.getTotalSleepMinutes()
            if (r3 <= 0) goto L_0x032a
            int r8 = r8 + 1
        L_0x032a:
            r3 = r8
            int r5 = r6.getTotalSleepMinutes()
            int r8 = r1.getTotalSleepMinutes()
            int r5 = r5 + r8
            r6.setTotalSleepMinutes(r5)
            int r5 = r6.getDeepSleepMinutes()
            int r8 = r1.getDeepSleepMinutes()
            int r5 = r5 + r8
            r6.setDeepSleepMinutes(r5)
            int r5 = r6.getLightSleepMinutes()
            int r8 = r1.getLightSleepMinutes()
            int r5 = r5 + r8
            r6.setLightSleepMinutes(r5)
            int r5 = r6.getWakeMunutes()
            int r8 = r1.getWakeMunutes()
            int r5 = r5 + r8
            r6.setWakeMunutes(r5)
            int r5 = r6.getEyeMinutes()
            int r8 = r1.getEyeMinutes()
            int r5 = r5 + r8
            r6.setEyeMinutes(r5)
            int r5 = r6.getSleepMinutes()
            int r1 = r1.getSleepMinutes()
            int r5 = r5 + r1
            r6.setSleepMinutes(r5)
            r1 = 0
            r0.setDateLabel(r7, r4, r1)
            boolean r1 = r0.isWeek(r7, r4)
            if (r1 == 0) goto L_0x03d3
            com.wade.fit.greendao.bean.HealthSleep r1 = new com.wade.fit.greendao.bean.HealthSleep
            r1.<init>()
            if (r3 == 0) goto L_0x03b4
            int r5 = r6.getTotalSleepMinutes()
            int r5 = r5 / r3
            r1.setTotalSleepMinutes(r5)
            int r5 = r6.getDeepSleepMinutes()
            int r5 = r5 / r3
            r1.setDeepSleepMinutes(r5)
            int r5 = r6.getLightSleepMinutes()
            int r5 = r5 / r3
            r1.setLightSleepMinutes(r5)
            int r5 = r6.getEyeMinutes()
            int r5 = r5 / r3
            r1.setEyeMinutes(r5)
            int r5 = r6.getWakeMunutes()
            int r5 = r5 / r3
            r1.setWakeMunutes(r5)
            int r5 = r6.getSleepMinutes()
            int r5 = r5 / r3
            r1.setSleepMinutes(r5)
        L_0x03b4:
            java.lang.String r3 = r0.getWeekStr(r4)
            r1.setRemark(r3)
            r2.add(r1)
            r14 = 0
            r6.setTotalSleepMinutes(r14)
            r6.setDeepSleepMinutes(r14)
            r6.setLightSleepMinutes(r14)
            r6.setWakeMunutes(r14)
            r6.setEyeMinutes(r14)
            r6.setSleepMinutes(r14)
            r3 = 0
            goto L_0x03d4
        L_0x03d3:
            r14 = 0
        L_0x03d4:
            int r7 = r7 + 1
            r8 = r3
            goto L_0x03f0
        L_0x03d8:
            r2 = r20
            r14 = 0
            r0.setDateLabel(r7, r4, r14)
            java.text.SimpleDateFormat r3 = r0.dateFormat3
            java.util.Date r5 = r4.getTime()
            java.lang.String r3 = r3.format(r5)
            r1.setRemark(r3)
            r2.add(r1)
            int r7 = r7 + 1
        L_0x03f0:
            r1 = 5
            r3 = 1
        L_0x03f2:
            r4.add(r1, r3)
            r5 = r2
            r1 = r18
            r2 = 1
            r3 = 0
            goto L_0x0065
        L_0x03fc:
            r2 = r5
            r14 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "size:"
            r1.append(r3)
            int r3 = r2.size()
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            com.wade.fit.util.log.LogUtil.d(r1)
            java.util.List r1 = r0.dates
            java.lang.Object[] r1 = r1.toArray()
            java.lang.String r1 = java.util.Arrays.toString(r1)
            com.wade.fit.util.log.LogUtil.d(r1)
            com.wade.fit.model.bean.SleepDetailVO r1 = new com.wade.fit.model.bean.SleepDetailVO
            r1.<init>()
            java.util.List r3 = r0.dates
            r1.dates = r3
            com.wade.fit.views.MainVO r3 = r21.getByDate()
            r1.mainVO = r3
            com.wade.fit.greendao.bean.HealthSleep r3 = new com.wade.fit.greendao.bean.HealthSleep
            r3.<init>()
            r4 = 0
        L_0x0438:
            int r5 = r2.size()
            if (r14 >= r5) goto L_0x048b
            java.lang.Object r5 = r2.get(r14)
            com.wade.fit.greendao.bean.HealthSleep r5 = (com.wade.fit.greendao.bean.HealthSleep) r5
            int r6 = r5.getSleepMinutes()
            int r7 = r3.getSleepMinutes()
            int r6 = r6 + r7
            r3.setSleepMinutes(r6)
            int r6 = r5.getWakeMunutes()
            int r7 = r3.getWakeMunutes()
            int r6 = r6 + r7
            r3.setWakeMunutes(r6)
            int r6 = r5.getEyeMinutes()
            int r7 = r3.getEyeMinutes()
            int r6 = r6 + r7
            r3.setEyeMinutes(r6)
            int r6 = r5.getLightSleepMinutes()
            int r7 = r3.getLightSleepMinutes()
            int r6 = r6 + r7
            r3.setLightSleepMinutes(r6)
            int r6 = r5.getDeepSleepMinutes()
            int r7 = r3.getDeepSleepMinutes()
            int r6 = r6 + r7
            r3.setDeepSleepMinutes(r6)
            int r5 = r5.getTotalSleepMinutes()
            if (r5 <= 0) goto L_0x0488
            int r4 = r4 + 1
        L_0x0488:
            int r14 = r14 + 1
            goto L_0x0438
        L_0x048b:
            r1.sleepList = r2
            if (r4 <= 0) goto L_0x04b7
            int r2 = r3.getSleepMinutes()
            int r2 = r2 / r4
            r3.setSleepMinutes(r2)
            int r2 = r3.getWakeMunutes()
            int r2 = r2 / r4
            r3.setWakeMunutes(r2)
            int r2 = r3.getEyeMinutes()
            int r2 = r2 / r4
            r3.setEyeMinutes(r2)
            int r2 = r3.getLightSleepMinutes()
            int r2 = r2 / r4
            r3.setLightSleepMinutes(r2)
            int r2 = r3.getDeepSleepMinutes()
            int r2 = r2 / r4
            r3.setDeepSleepMinutes(r2)
        L_0x04b7:
            com.wade.fit.views.MainVO r2 = r1.mainVO
            r2.healthSleep = r3
            java.util.List<com.wade.fit.model.bean.SleepDetailVO$WeekSleep> r2 = r0.weekSleeps
            r1.weekSleeps = r2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.persenter.main.DetailSleep2Presenter.get():com.wade.fit.model.bean.SleepDetailVO");
    }

    /* renamed from: com.wade.fit.persenter.main.DetailSleep2Presenter$1 */
    static /* synthetic */ class C23961 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailTimeType = new int[DetailTimeType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.wade.fit.model.bean.DetailTimeType[] r0 = com.wade.fit.model.bean.DetailTimeType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.persenter.main.DetailSleep2Presenter.C23961.$SwitchMap$com$wade.fit$model$bean$DetailTimeType = r0
                int[] r0 = com.wade.fit.persenter.main.DetailSleep2Presenter.C23961.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.ONE_MONTH     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.persenter.main.DetailSleep2Presenter.C23961.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.SIX_MONTH     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.persenter.main.DetailSleep2Presenter.C23961.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.YEAR     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.wade.fit.persenter.main.DetailSleep2Presenter.C23961.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.DAY     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.persenter.main.DetailSleep2Presenter.C23961.<clinit>():void");
        }
    }

    public SleepDetailVO getOneMonth() {
        setOneMonth();
        return get();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v19, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v3, resolved type: com.wade.fit.greendao.bean.HealthSleepItem} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v25, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v5, resolved type: com.wade.fit.greendao.bean.HealthSleepItem} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01ae  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.wade.fit.model.bean.SleepDetailVO getDetail() {
        /*
            r16 = this;
            r0 = r16
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "index:"
            r1.append(r2)
            int r2 = r0.index
            r1.append(r2)
            java.lang.String r2 = ",date:"
            r1.append(r2)
            java.text.SimpleDateFormat r2 = r0.dateFormat
            java.util.Date r3 = r0.currentDate
            java.lang.String r2 = r2.format(r3)
            r1.append(r2)
            java.lang.String r2 = "detailTimeType:"
            r1.append(r2)
            com.wade.fit.model.bean.DetailTimeType r2 = r0.timeType
            java.lang.String r2 = r2.toString()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.wade.fit.util.log.LogUtil.d(r1)
            com.wade.fit.model.bean.SleepDetailVO r1 = new com.wade.fit.model.bean.SleepDetailVO
            r1.<init>()
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            com.wade.fit.app.AppApplication r3 = com.wade.fit.app.AppApplication.getInstance()
            com.wade.fit.greendao.gen.DaoSession r3 = r3.getDaoSession()
            com.wade.fit.greendao.gen.HealthSleepItemDao r3 = r3.getHealthSleepItemDao()
            org.greenrobot.greendao.query.QueryBuilder r4 = r3.queryBuilder()
            r5 = 1
            org.greenrobot.greendao.Property[] r6 = new org.greenrobot.greendao.Property[r5]
            org.greenrobot.greendao.Property r7 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Date
            r8 = 0
            r6[r8] = r7
            org.greenrobot.greendao.query.QueryBuilder r4 = r4.orderDesc(r6)
            org.greenrobot.greendao.query.Query r4 = r4.build()
            r4.list()
            r1.sleepList = r2
            int[] r4 = com.wade.fit.persenter.main.DetailSleep2Presenter.C23961.$SwitchMap$com$wade.fit$model$bean$DetailTimeType
            com.wade.fit.model.bean.DetailTimeType r6 = r0.timeType
            int r6 = r6.ordinal()
            r4 = r4[r6]
            if (r4 == r5) goto L_0x032f
            r6 = 2
            if (r4 == r6) goto L_0x032f
            r7 = 3
            if (r4 == r7) goto L_0x032f
            r9 = 4
            if (r4 == r9) goto L_0x0080
            r3 = 0
            r11 = 0
            r13 = 0
            r14 = 0
            goto L_0x02c7
        L_0x0080:
            org.greenrobot.greendao.query.QueryBuilder r4 = r3.queryBuilder()
            org.greenrobot.greendao.Property r10 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Year
            int r11 = r0.year
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            org.greenrobot.greendao.query.WhereCondition r10 = r10.eq(r11)
            org.greenrobot.greendao.query.WhereCondition[] r11 = new org.greenrobot.greendao.query.WhereCondition[r6]
            org.greenrobot.greendao.Property r12 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Month
            int r13 = r0.month
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            org.greenrobot.greendao.query.WhereCondition r12 = r12.eq(r13)
            r11[r8] = r12
            org.greenrobot.greendao.Property r12 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Day
            int r13 = r0.day
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            org.greenrobot.greendao.query.WhereCondition r12 = r12.eq(r13)
            r11[r5] = r12
            org.greenrobot.greendao.query.QueryBuilder r4 = r4.where(r10, r11)
            org.greenrobot.greendao.Property[] r10 = new org.greenrobot.greendao.Property[r5]
            org.greenrobot.greendao.Property r11 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Date
            r10[r8] = r11
            org.greenrobot.greendao.query.QueryBuilder r4 = r4.orderDesc(r10)
            org.greenrobot.greendao.query.Query r4 = r4.build()
            java.util.List r4 = r4.list()
            if (r4 == 0) goto L_0x0206
            java.util.Calendar r10 = r0.calendar
            java.lang.Object r10 = r10.clone()
            java.util.Calendar r10 = (java.util.Calendar) r10
            int r11 = r0.day
            int r11 = r11 - r5
            r12 = 5
            r10.set(r12, r11)
            int r11 = r10.get(r5)
            int r13 = r10.get(r6)
            int r13 = r13 + r5
            int r10 = r10.get(r12)
            r4.size()
            org.greenrobot.greendao.query.QueryBuilder r3 = r3.queryBuilder()
            org.greenrobot.greendao.Property r12 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Year
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            org.greenrobot.greendao.query.WhereCondition r11 = r12.eq(r11)
            org.greenrobot.greendao.query.WhereCondition[] r12 = new org.greenrobot.greendao.query.WhereCondition[r6]
            org.greenrobot.greendao.Property r14 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Month
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            org.greenrobot.greendao.query.WhereCondition r13 = r14.eq(r13)
            r12[r8] = r13
            org.greenrobot.greendao.Property r13 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Day
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            org.greenrobot.greendao.query.WhereCondition r10 = r13.eq(r10)
            r12[r5] = r10
            org.greenrobot.greendao.query.QueryBuilder r3 = r3.where(r11, r12)
            org.greenrobot.greendao.Property[] r10 = new org.greenrobot.greendao.Property[r5]
            org.greenrobot.greendao.Property r11 = com.wade.fit.greendao.gen.HealthSleepItemDao.Properties.Date
            r10[r8] = r11
            org.greenrobot.greendao.query.QueryBuilder r3 = r3.orderDesc(r10)
            org.greenrobot.greendao.query.Query r3 = r3.build()
            java.util.List r3 = r3.list()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
        L_0x012d:
            r15 = 85
            if (r10 >= r15) goto L_0x0204
            com.wade.fit.greendao.bean.HealthSleepItem r15 = new com.wade.fit.greendao.bean.HealthSleepItem
            r15.<init>()
            r8 = 10
            r15.setOffsetMinute(r8)
            r8 = 18
            if (r10 >= r8) goto L_0x015b
            if (r3 == 0) goto L_0x016a
            int r9 = r3.size()
            if (r10 >= r9) goto L_0x016a
            int r9 = r3.size()
            if (r9 <= r8) goto L_0x016a
            int r9 = r3.size()
            int r9 = r9 - r8
            int r9 = r9 + r10
            java.lang.Object r9 = r3.get(r9)
            r15 = r9
            com.wade.fit.greendao.bean.HealthSleepItem r15 = (com.wade.fit.greendao.bean.HealthSleepItem) r15
            goto L_0x016a
        L_0x015b:
            int r9 = r4.size()
            if (r10 >= r9) goto L_0x016a
            int r9 = r10 + -18
            java.lang.Object r9 = r4.get(r9)
            r15 = r9
            com.wade.fit.greendao.bean.HealthSleepItem r15 = (com.wade.fit.greendao.bean.HealthSleepItem) r15
        L_0x016a:
            int r9 = r15.getSleepStatus()
            if (r9 == r5) goto L_0x0183
            int r9 = r15.getSleepStatus()
            if (r9 == r7) goto L_0x0183
            int r9 = r15.getSleepStatus()
            if (r9 == r6) goto L_0x0183
            int r9 = r15.getSleepStatus()
            r8 = 4
            if (r9 != r8) goto L_0x01fc
        L_0x0183:
            int r8 = r15.getSleepStatus()
            if (r8 != r5) goto L_0x018d
            int r12 = r12 + 10
        L_0x018b:
            r9 = 4
            goto L_0x01a8
        L_0x018d:
            int r8 = r15.getSleepStatus()
            if (r8 != r7) goto L_0x0196
            int r13 = r13 + 10
            goto L_0x018b
        L_0x0196:
            int r8 = r15.getSleepStatus()
            if (r8 != r6) goto L_0x019f
            int r14 = r14 + 10
            goto L_0x018b
        L_0x019f:
            int r8 = r15.getSleepStatus()
            r9 = 4
            if (r8 != r9) goto L_0x01a8
            int r11 = r11 + 10
        L_0x01a8:
            java.lang.String r8 = ":"
            r7 = 18
            if (r10 > r7) goto L_0x01c9
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            int r9 = r10 * 10
            int r9 = r9 + 1260
            int r6 = r9 / 60
            r7.append(r6)
            r7.append(r8)
            int r9 = r9 % 60
            r7.append(r9)
            java.lang.String r6 = r7.toString()
            goto L_0x01e5
        L_0x01c9:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            int r7 = r10 + -18
            r9 = 10
            int r7 = r7 * 10
            int r9 = r7 / 60
            r6.append(r9)
            r6.append(r8)
            int r7 = r7 % 60
            r6.append(r7)
            java.lang.String r6 = r6.toString()
        L_0x01e5:
            r15.setIndex(r10)
            boolean r7 = com.wade.fit.util.CommonUtil.is24Hour()
            com.wade.fit.app.AppApplication r8 = com.wade.fit.app.AppApplication.getInstance()
            java.lang.String r6 = com.wade.fit.util.CommonUtil.timeFormatter(r6, r7, r8)
            r15.setSleeptime(r6)
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r6 = r1.sleepItemList
            r6.add(r15)
        L_0x01fc:
            int r10 = r10 + 1
            r6 = 2
            r7 = 3
            r8 = 0
            r9 = 4
            goto L_0x012d
        L_0x0204:
            r8 = r12
            goto L_0x020a
        L_0x0206:
            r8 = 0
            r11 = 0
            r13 = 0
            r14 = 0
        L_0x020a:
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r3 = r1.sleepItemList
            int r3 = r3.size()
            if (r3 <= 0) goto L_0x023d
            java.util.List<java.lang.String> r3 = r1.dates
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r4 = r1.sleepItemList
            r6 = 0
            java.lang.Object r4 = r4.get(r6)
            com.wade.fit.greendao.bean.HealthSleepItem r4 = (com.wade.fit.greendao.bean.HealthSleepItem) r4
            java.lang.String r4 = r4.getSleeptime()
            r3.add(r4)
            java.util.List<java.lang.String> r3 = r1.dates
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r4 = r1.sleepItemList
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r6 = r1.sleepItemList
            int r6 = r6.size()
            int r6 = r6 - r5
            java.lang.Object r4 = r4.get(r6)
            com.wade.fit.greendao.bean.HealthSleepItem r4 = (com.wade.fit.greendao.bean.HealthSleepItem) r4
            java.lang.String r4 = r4.getSleeptime()
            r3.add(r4)
            goto L_0x0260
        L_0x023d:
            java.util.List<java.lang.String> r3 = r1.dates
            java.lang.String r4 = "21:00"
            r3.add(r4)
            java.util.List<java.lang.String> r3 = r1.dates
            java.lang.String r4 = "24:00"
            r3.add(r4)
            java.util.List<java.lang.String> r3 = r1.dates
            java.lang.String r4 = "04:00"
            r3.add(r4)
            java.util.List<java.lang.String> r3 = r1.dates
            java.lang.String r4 = "08:00"
            r3.add(r4)
            java.util.List<java.lang.String> r3 = r1.dates
            java.lang.String r4 = "11:00"
            r3.add(r4)
        L_0x0260:
            int r3 = r8 + r13
            int r3 = r3 + r14
            r4 = 0
            int r3 = r3 + r4
            int r3 = r3 + r11
            com.wade.fit.app.AppApplication r4 = com.wade.fit.app.AppApplication.getInstance()
            com.wade.fit.greendao.gen.DaoSession r4 = r4.getDaoSession()
            com.wade.fit.greendao.gen.HealthSleepDao r4 = r4.getHealthSleepDao()
            org.greenrobot.greendao.query.QueryBuilder r4 = r4.queryBuilder()
            org.greenrobot.greendao.Property r6 = com.wade.fit.greendao.gen.HealthSleepDao.Properties.Year
            int r7 = r0.year
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            org.greenrobot.greendao.query.WhereCondition r6 = r6.eq(r7)
            r7 = 2
            org.greenrobot.greendao.query.WhereCondition[] r7 = new org.greenrobot.greendao.query.WhereCondition[r7]
            org.greenrobot.greendao.Property r9 = com.wade.fit.greendao.gen.HealthSleepDao.Properties.Month
            int r10 = r0.month
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            org.greenrobot.greendao.query.WhereCondition r9 = r9.eq(r10)
            r10 = 0
            r7[r10] = r9
            org.greenrobot.greendao.Property r9 = com.wade.fit.greendao.gen.HealthSleepDao.Properties.Day
            int r12 = r0.day
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            org.greenrobot.greendao.query.WhereCondition r9 = r9.eq(r12)
            r7[r5] = r9
            org.greenrobot.greendao.query.QueryBuilder r4 = r4.where(r6, r7)
            org.greenrobot.greendao.Property[] r5 = new org.greenrobot.greendao.Property[r5]
            org.greenrobot.greendao.Property r6 = com.wade.fit.greendao.gen.HealthSleepDao.Properties.Date
            r5[r10] = r6
            org.greenrobot.greendao.query.QueryBuilder r4 = r4.orderDesc(r5)
            org.greenrobot.greendao.query.Query r4 = r4.build()
            java.util.List r4 = r4.list()
            if (r4 == 0) goto L_0x02c7
            int r5 = r4.size()
            if (r5 <= 0) goto L_0x02c7
            java.lang.Object r4 = r4.get(r10)
            r2.add(r4)
        L_0x02c7:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "size:"
            r2.append(r4)
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r4 = r1.sleepItemList
            int r4 = r4.size()
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            com.wade.fit.util.log.LogUtil.d(r2)
            java.util.List<java.lang.String> r2 = r1.dates
            java.lang.Object[] r2 = r2.toArray()
            java.lang.String r2 = java.util.Arrays.toString(r2)
            com.wade.fit.util.log.LogUtil.d(r2)
            com.wade.fit.views.MainVO r2 = r0.mainVO
            com.wade.fit.greendao.bean.HealthSleep r2 = r2.healthSleep
            r2.setTotalSleepMinutes(r3)
            com.wade.fit.views.MainVO r2 = r0.mainVO
            com.wade.fit.greendao.bean.HealthSleep r2 = r2.healthSleep
            r2.setSleepstartedTimeM(r8)
            com.wade.fit.views.MainVO r2 = r0.mainVO
            com.wade.fit.greendao.bean.HealthSleep r2 = r2.healthSleep
            r2.setDeepSleepMinutes(r13)
            com.wade.fit.views.MainVO r2 = r0.mainVO
            com.wade.fit.greendao.bean.HealthSleep r2 = r2.healthSleep
            r2.setLightSleepMinutes(r14)
            com.wade.fit.views.MainVO r2 = r0.mainVO
            com.wade.fit.greendao.bean.HealthSleep r2 = r2.healthSleep
            r3 = 0
            r2.setEyeMinutes(r3)
            com.wade.fit.views.MainVO r2 = r0.mainVO
            com.wade.fit.greendao.bean.HealthSleep r2 = r2.healthSleep
            r2.setWakeMunutes(r11)
            com.wade.fit.views.MainVO r2 = r0.mainVO
            r1.mainVO = r2
            java.util.List<com.wade.fit.model.bean.SleepDetailVO$WeekSleep> r2 = r0.weekSleeps
            r1.weekSleeps = r2
            java.util.List<com.wade.fit.model.bean.SleepDetailVO$WeekSleep> r2 = r1.weekSleeps
            java.lang.Object[] r2 = r2.toArray()
            java.lang.String r2 = java.util.Arrays.toString(r2)
            com.wade.fit.util.log.LogUtil.d(r2)
            return r1
        L_0x032f:
            com.wade.fit.model.bean.SleepDetailVO r1 = r16.get()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.persenter.main.DetailSleep2Presenter.getDetail():com.wade.fit.model.bean.SleepDetailVO");
    }
}
