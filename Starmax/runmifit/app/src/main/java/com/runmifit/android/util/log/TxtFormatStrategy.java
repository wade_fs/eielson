package com.wade.fit.util.log;

import android.os.Environment;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.LogStrategy;
import com.wade.fit.util.log.DiskLogStrategy;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TxtFormatStrategy implements FormatStrategy {
    private static final String NEW_LINE = System.getProperty("line.separator");
    private static final String SEPARATOR = " ";
    private final Date date;
    private final SimpleDateFormat dateFormat;
    private final LogStrategy logStrategy;
    private final String tag;

    private String logLevel(int i) {
        switch (i) {
            case 2:
                return "VERBOSE";
            case 3:
                return "DEBUG";
            case 4:
                return "INFO";
            case 5:
                return "WARN";
            case 6:
                return "ERROR";
            case 7:
                return "ASSERT";
            default:
                return "UNKNOWN";
        }
    }

    private TxtFormatStrategy(Builder builder) {
        this.date = builder.date;
        this.dateFormat = builder.dateFormat;
        this.logStrategy = builder.logStrategy;
        this.tag = builder.tag;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public void log(int i, String str, String str2) {
        String formatTag = formatTag(str);
        this.date.setTime(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        sb.append(Long.toString(this.date.getTime()));
        sb.append(SEPARATOR);
        sb.append(this.dateFormat.format(this.date));
        sb.append(SEPARATOR);
        sb.append(logLevel(i));
        sb.append(SEPARATOR);
        sb.append(formatTag);
        sb.append(SEPARATOR);
        StringBuilder sb2 = new StringBuilder();
        sb2.append((CharSequence) sb);
        if (str2.contains(NEW_LINE)) {
            String str3 = NEW_LINE;
            str2 = str2.replaceAll(str3, NEW_LINE + sb.toString());
        }
        sb2.append(str2);
        sb2.append(NEW_LINE);
        this.logStrategy.log(i, formatTag, sb2.toString());
    }

    private String formatTag(String str) {
        if (TextUtils.isEmpty(str) || str.equals(this.tag)) {
            return this.tag;
        }
        return this.tag + "-" + str;
    }

    public static final class Builder {
        Date date;
        SimpleDateFormat dateFormat;
        LogStrategy logStrategy;
        String tag;

        private Builder() {
            this.tag = "PRETTY_LOGGER";
        }

        public Builder date(Date date2) {
            this.date = date2;
            return this;
        }

        public Builder dateFormat(SimpleDateFormat simpleDateFormat) {
            this.dateFormat = simpleDateFormat;
            return this;
        }

        public Builder logStrategy(LogStrategy logStrategy2) {
            this.logStrategy = logStrategy2;
            return this;
        }

        public Builder tag(String str) {
            this.tag = str;
            return this;
        }

        public TxtFormatStrategy build(String str, String str2) {
            if (this.date == null) {
                this.date = new Date();
            }
            if (this.dateFormat == null) {
                this.dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSS", Locale.UK);
            }
            if (this.logStrategy == null) {
                String str3 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separatorChar + "Android" + File.separatorChar + "data" + File.separatorChar + str + File.separatorChar + "log" + File.separatorChar;
                HandlerThread handlerThread = new HandlerThread("AndroidFileLogger." + str3);
                handlerThread.start();
                this.logStrategy = new DiskLogStrategy(new DiskLogStrategy.WriteHandler(handlerThread.getLooper(), str3, str2));
            }
            return new TxtFormatStrategy(this);
        }
    }
}
