package com.wade.fit.views.photo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

public class ClipZoomImageView extends ImageView implements ScaleGestureDetector.OnScaleGestureListener, View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener {
    public static float SCALE_MAX = 4.0f;
    /* access modifiers changed from: private */
    public static float SCALE_MID = 2.0f;
    private static final String TAG = ClipZoomImageView.class.getSimpleName();
    /* access modifiers changed from: private */
    public float initScale;
    /* access modifiers changed from: private */
    public boolean isAutoScale;
    private boolean isCanDrag;
    private int lastPointerCount;
    private GestureDetector mGestureDetector;
    private int mHorizontalPadding;
    private float mLastX;
    private float mLastY;
    private ScaleGestureDetector mScaleGestureDetector;
    /* access modifiers changed from: private */
    public final Matrix mScaleMatrix;
    private int mTouchSlop;
    private int mVerticalPadding;
    private final float[] matrixValues;
    private boolean once;

    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return true;
    }

    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
    }

    public ClipZoomImageView(Context context) {
        this(context, null);
    }

    public ClipZoomImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.initScale = 1.0f;
        this.once = true;
        this.matrixValues = new float[9];
        this.mScaleGestureDetector = null;
        this.mScaleMatrix = new Matrix();
        setScaleType(ImageView.ScaleType.MATRIX);
        this.mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            /* class com.wade.fit.views.photo.ClipZoomImageView.C27351 */

            public boolean onDoubleTap(MotionEvent motionEvent) {
                if (ClipZoomImageView.this.isAutoScale) {
                    return true;
                }
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                if (ClipZoomImageView.this.getScale() < ClipZoomImageView.SCALE_MID) {
                    ClipZoomImageView clipZoomImageView = ClipZoomImageView.this;
                    clipZoomImageView.postDelayed(new AutoScaleRunnable(ClipZoomImageView.SCALE_MID, x, y), 16);
                    boolean unused = ClipZoomImageView.this.isAutoScale = true;
                } else {
                    ClipZoomImageView clipZoomImageView2 = ClipZoomImageView.this;
                    clipZoomImageView2.postDelayed(new AutoScaleRunnable(clipZoomImageView2.initScale, x, y), 16);
                    boolean unused2 = ClipZoomImageView.this.isAutoScale = true;
                }
                return true;
            }
        });
        this.mScaleGestureDetector = new ScaleGestureDetector(context, this);
        setOnTouchListener(this);
    }

    private class AutoScaleRunnable implements Runnable {
        static final float BIGGER = 1.07f;
        static final float SMALLER = 0.93f;
        private float mTargetScale;
        private float tmpScale;

        /* renamed from: x */
        private float f5264x;

        /* renamed from: y */
        private float f5265y;

        public AutoScaleRunnable(float f, float f2, float f3) {
            this.mTargetScale = f;
            this.f5264x = f2;
            this.f5265y = f3;
            if (ClipZoomImageView.this.getScale() < this.mTargetScale) {
                this.tmpScale = BIGGER;
            } else {
                this.tmpScale = SMALLER;
            }
        }

        public void run() {
            Matrix access$300 = ClipZoomImageView.this.mScaleMatrix;
            float f = this.tmpScale;
            access$300.postScale(f, f, this.f5264x, this.f5265y);
            ClipZoomImageView.this.checkBorder();
            ClipZoomImageView clipZoomImageView = ClipZoomImageView.this;
            clipZoomImageView.setImageMatrix(clipZoomImageView.mScaleMatrix);
            float scale = ClipZoomImageView.this.getScale();
            if ((this.tmpScale <= 1.0f || scale >= this.mTargetScale) && (this.tmpScale >= 1.0f || this.mTargetScale >= scale)) {
                float f2 = this.mTargetScale / scale;
                ClipZoomImageView.this.mScaleMatrix.postScale(f2, f2, this.f5264x, this.f5265y);
                ClipZoomImageView.this.checkBorder();
                ClipZoomImageView clipZoomImageView2 = ClipZoomImageView.this;
                clipZoomImageView2.setImageMatrix(clipZoomImageView2.mScaleMatrix);
                boolean unused = ClipZoomImageView.this.isAutoScale = false;
                return;
            }
            ClipZoomImageView.this.postDelayed(this, 16);
        }
    }

    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        float scale = getScale();
        float scaleFactor = scaleGestureDetector.getScaleFactor();
        if (getDrawable() == null) {
            return true;
        }
        if ((scale < SCALE_MAX && scaleFactor > 1.0f) || (scale > this.initScale && scaleFactor < 1.0f)) {
            float f = this.initScale;
            if (scaleFactor * scale < f) {
                scaleFactor = f / scale;
            }
            float f2 = SCALE_MAX;
            if (scaleFactor * scale > f2) {
                scaleFactor = f2 / scale;
            }
            this.mScaleMatrix.postScale(scaleFactor, scaleFactor, scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
            checkBorder();
            setImageMatrix(this.mScaleMatrix);
        }
        return true;
    }

    private RectF getMatrixRectF() {
        Matrix matrix = this.mScaleMatrix;
        RectF rectF = new RectF();
        Drawable drawable = getDrawable();
        if (drawable != null) {
            rectF.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            matrix.mapRect(rectF);
        }
        return rectF;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r10 != 3) goto L_0x009d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r10, android.view.MotionEvent r11) {
        /*
            r9 = this;
            android.view.GestureDetector r10 = r9.mGestureDetector
            boolean r10 = r10.onTouchEvent(r11)
            r0 = 1
            if (r10 == 0) goto L_0x000a
            return r0
        L_0x000a:
            android.view.ScaleGestureDetector r10 = r9.mScaleGestureDetector
            r10.onTouchEvent(r11)
            int r10 = r11.getPointerCount()
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
        L_0x0018:
            if (r3 >= r10) goto L_0x0027
            float r6 = r11.getX(r3)
            float r4 = r4 + r6
            float r6 = r11.getY(r3)
            float r5 = r5 + r6
            int r3 = r3 + 1
            goto L_0x0018
        L_0x0027:
            float r3 = (float) r10
            float r4 = r4 / r3
            float r5 = r5 / r3
            int r3 = r9.lastPointerCount
            if (r10 == r3) goto L_0x0034
            r9.isCanDrag = r1
            r9.mLastX = r4
            r9.mLastY = r5
        L_0x0034:
            r9.lastPointerCount = r10
            int r10 = r11.getAction()
            if (r10 == r0) goto L_0x009b
            r11 = 2
            if (r10 == r11) goto L_0x0043
            r11 = 3
            if (r10 == r11) goto L_0x009b
            goto L_0x009d
        L_0x0043:
            float r10 = r9.mLastX
            float r10 = r4 - r10
            float r1 = r9.mLastY
            float r1 = r5 - r1
            boolean r3 = r9.isCanDrag
            if (r3 != 0) goto L_0x0055
            boolean r3 = r9.isCanDrag(r10, r1)
            r9.isCanDrag = r3
        L_0x0055:
            boolean r3 = r9.isCanDrag
            if (r3 == 0) goto L_0x0096
            android.graphics.drawable.Drawable r3 = r9.getDrawable()
            if (r3 == 0) goto L_0x0096
            android.graphics.RectF r3 = r9.getMatrixRectF()
            float r6 = r3.width()
            int r7 = r9.getWidth()
            int r8 = r9.mHorizontalPadding
            int r8 = r8 * 2
            int r7 = r7 - r8
            float r7 = (float) r7
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 > 0) goto L_0x0076
            r10 = 0
        L_0x0076:
            float r3 = r3.height()
            int r6 = r9.getHeight()
            int r7 = r9.mVerticalPadding
            int r7 = r7 * 2
            int r6 = r6 - r7
            float r11 = (float) r6
            int r11 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r11 > 0) goto L_0x0089
            r1 = 0
        L_0x0089:
            android.graphics.Matrix r11 = r9.mScaleMatrix
            r11.postTranslate(r10, r1)
            r9.checkBorder()
            android.graphics.Matrix r10 = r9.mScaleMatrix
            r9.setImageMatrix(r10)
        L_0x0096:
            r9.mLastX = r4
            r9.mLastY = r5
            goto L_0x009d
        L_0x009b:
            r9.lastPointerCount = r1
        L_0x009d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.photo.ClipZoomImageView.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    public final float getScale() {
        this.mScaleMatrix.getValues(this.matrixValues);
        return this.matrixValues[0];
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getViewTreeObserver().removeGlobalOnLayoutListener(this);
    }

    public void onGlobalLayout() {
        Drawable drawable;
        if (this.once && (drawable = getDrawable()) != null) {
            this.mVerticalPadding = (getHeight() - (getWidth() - (this.mHorizontalPadding * 2))) / 2;
            int width = getWidth();
            int height = getHeight();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            float width2 = (intrinsicWidth >= getWidth() - (this.mHorizontalPadding * 2) || intrinsicHeight <= getHeight() - (this.mVerticalPadding * 2)) ? 1.0f : ((((float) getWidth()) * 1.0f) - ((float) (this.mHorizontalPadding * 2))) / ((float) intrinsicWidth);
            if (intrinsicHeight < getHeight() - (this.mVerticalPadding * 2) && intrinsicWidth > getWidth() - (this.mHorizontalPadding * 2)) {
                width2 = ((((float) getHeight()) * 1.0f) - ((float) (this.mVerticalPadding * 2))) / ((float) intrinsicHeight);
            }
            if (intrinsicWidth < getWidth() - (this.mHorizontalPadding * 2) && intrinsicHeight < getHeight() - (this.mVerticalPadding * 2)) {
                width2 = Math.max(((((float) getWidth()) * 1.0f) - ((float) (this.mHorizontalPadding * 2))) / ((float) intrinsicWidth), ((((float) getHeight()) * 1.0f) - ((float) (this.mVerticalPadding * 2))) / ((float) intrinsicHeight));
            }
            this.initScale = width2;
            float f = this.initScale;
            SCALE_MID = 2.0f * f;
            SCALE_MAX = f * 4.0f;
            this.mScaleMatrix.postTranslate((float) ((width - intrinsicWidth) / 2), (float) ((height - intrinsicHeight) / 2));
            this.mScaleMatrix.postScale(width2, width2, (float) (getWidth() / 2), (float) (getHeight() / 2));
            setImageMatrix(this.mScaleMatrix);
            this.once = false;
        }
    }

    public Bitmap clip() {
        Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
        draw(new Canvas(createBitmap));
        return Bitmap.createBitmap(createBitmap, this.mHorizontalPadding, this.mVerticalPadding, getWidth() - (this.mHorizontalPadding * 2), getWidth() - (this.mHorizontalPadding * 2));
    }

    /* access modifiers changed from: private */
    public void checkBorder() {
        float f;
        RectF matrixRectF = getMatrixRectF();
        int width = getWidth();
        int height = getHeight();
        String str = TAG;
        Log.e(str, "rect.width() =  " + matrixRectF.width() + " , width - 2 * mHorizontalPadding =" + (width - (this.mHorizontalPadding * 2)));
        double width2 = (double) matrixRectF.width();
        Double.isNaN(width2);
        float f2 = 0.0f;
        if (width2 + 0.01d >= ((double) (width - (this.mHorizontalPadding * 2)))) {
            float f3 = matrixRectF.left > ((float) this.mHorizontalPadding) ? (-matrixRectF.left) + ((float) this.mHorizontalPadding) : 0.0f;
            float f4 = matrixRectF.right;
            int i = this.mHorizontalPadding;
            f = f4 < ((float) (width - i)) ? ((float) (width - i)) - matrixRectF.right : f3;
        } else {
            f = 0.0f;
        }
        double height2 = (double) matrixRectF.height();
        Double.isNaN(height2);
        if (height2 + 0.01d >= ((double) (height - (this.mVerticalPadding * 2)))) {
            if (matrixRectF.top > ((float) this.mVerticalPadding)) {
                f2 = (-matrixRectF.top) + ((float) this.mVerticalPadding);
            }
            float f5 = matrixRectF.bottom;
            int i2 = this.mVerticalPadding;
            if (f5 < ((float) (height - i2))) {
                f2 = ((float) (height - i2)) - matrixRectF.bottom;
            }
        }
        this.mScaleMatrix.postTranslate(f, f2);
    }

    private boolean isCanDrag(float f, float f2) {
        return Math.sqrt((double) ((f * f) + (f2 * f2))) >= ((double) this.mTouchSlop);
    }

    public void setHorizontalPadding(int i) {
        this.mHorizontalPadding = i;
    }
}
