package com.wade.fit.views.zoomview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.OverScroller;
import android.widget.Scroller;

public class ZoomImageView extends ImageView implements View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener {
    public static final float DEFAULT_MAX_SCALE = 3.0f;
    public static final float DEFAULT_MID_SCALE = 1.75f;
    public static final float DEFAULT_MIN_SCALE = 1.0f;
    private static final int EDGE_BOTH = 2;
    private static final int EDGE_LEFT = 0;
    private static final int EDGE_NONE = -1;
    private static final int EDGE_RIGHT = 1;
    /* access modifiers changed from: private */
    public boolean allowParentInterceptOnEdge;
    private final Matrix baseMatrix;
    private int bottom;
    /* access modifiers changed from: private */
    public FlingRunnable currentFlingRunnable;
    private final RectF displayRect;
    private final Matrix drawMatrix;
    private boolean isZoomEnabled;
    private int left;
    /* access modifiers changed from: private */
    public View.OnLongClickListener longClickListener;
    private final float[] matrixValues;
    /* access modifiers changed from: private */
    public float maxScale;
    /* access modifiers changed from: private */
    public float midScale;
    /* access modifiers changed from: private */
    public float minScale;
    /* access modifiers changed from: private */
    public MultiGestureDetector multiGestureDetector;
    /* access modifiers changed from: private */
    public OnPhotoTapListener photoTapListener;
    private int right;
    private ImageView.ScaleType scaleType;
    /* access modifiers changed from: private */
    public int scrollEdge;
    /* access modifiers changed from: private */
    public final Matrix suppMatrix;
    private int top;
    /* access modifiers changed from: private */
    public OnViewTapListener viewTapListener;

    public interface OnPhotoTapListener {
        void onPhotoTap(View view, float f, float f2);
    }

    public interface OnViewTapListener {
        void onViewTap(View view, float f, float f2);
    }

    private class MultiGestureDetector extends GestureDetector.SimpleOnGestureListener implements ScaleGestureDetector.OnScaleGestureListener {
        private final GestureDetector gestureDetector;
        private boolean isDragging;
        private float lastPointerCount;
        private float lastTouchX;
        private float lastTouchY;
        private final ScaleGestureDetector scaleGestureDetector;
        private final float scaledMinimumFlingVelocity;
        private final float scaledTouchSlop;
        private VelocityTracker velocityTracker;

        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }

        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector2) {
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector2) {
        }

        public MultiGestureDetector(Context context) {
            this.scaleGestureDetector = new ScaleGestureDetector(context, this);
            this.gestureDetector = new GestureDetector(context, this);
            this.gestureDetector.setOnDoubleTapListener(this);
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.scaledMinimumFlingVelocity = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.scaledTouchSlop = (float) viewConfiguration.getScaledTouchSlop();
        }

        public boolean isScaling() {
            return this.scaleGestureDetector.isInProgress();
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            if (this.gestureDetector.onTouchEvent(motionEvent)) {
                return true;
            }
            this.scaleGestureDetector.onTouchEvent(motionEvent);
            int pointerCount = motionEvent.getPointerCount();
            float f = 0.0f;
            float f2 = 0.0f;
            for (int i = 0; i < pointerCount; i++) {
                f += motionEvent.getX(i);
                f2 += motionEvent.getY(i);
            }
            float f3 = (float) pointerCount;
            float f4 = f / f3;
            float f5 = f2 / f3;
            if (f3 != this.lastPointerCount) {
                this.isDragging = false;
                VelocityTracker velocityTracker2 = this.velocityTracker;
                if (velocityTracker2 != null) {
                    velocityTracker2.clear();
                }
                this.lastTouchX = f4;
                this.lastTouchY = f5;
            }
            this.lastPointerCount = f3;
            int action = motionEvent.getAction();
            if (action == 0) {
                VelocityTracker velocityTracker3 = this.velocityTracker;
                if (velocityTracker3 == null) {
                    this.velocityTracker = VelocityTracker.obtain();
                } else {
                    velocityTracker3.clear();
                }
                this.velocityTracker.addMovement(motionEvent);
                this.lastTouchX = f4;
                this.lastTouchY = f5;
                this.isDragging = false;
            } else if (action != 1) {
                if (action == 2) {
                    float f6 = f4 - this.lastTouchX;
                    float f7 = f5 - this.lastTouchY;
                    if (!this.isDragging) {
                        this.isDragging = Math.sqrt((double) ((f6 * f6) + (f7 * f7))) >= ((double) this.scaledTouchSlop);
                    }
                    if (this.isDragging) {
                        if (ZoomImageView.this.getDrawable() != null) {
                            ZoomImageView.this.suppMatrix.postTranslate(f6, f7);
                            ZoomImageView.this.checkAndDisplayMatrix();
                            if (ZoomImageView.this.allowParentInterceptOnEdge && !ZoomImageView.this.multiGestureDetector.isScaling() && ((ZoomImageView.this.scrollEdge == 2 || ((ZoomImageView.this.scrollEdge == 0 && f6 >= 1.0f) || (ZoomImageView.this.scrollEdge == 1 && f6 <= -1.0f))) && ZoomImageView.this.getParent() != null)) {
                                ZoomImageView.this.getParent().requestDisallowInterceptTouchEvent(false);
                            }
                        }
                        this.lastTouchX = f4;
                        this.lastTouchY = f5;
                        VelocityTracker velocityTracker4 = this.velocityTracker;
                        if (velocityTracker4 != null) {
                            velocityTracker4.addMovement(motionEvent);
                        }
                    }
                } else if (action == 3) {
                    this.lastPointerCount = 0.0f;
                    VelocityTracker velocityTracker5 = this.velocityTracker;
                    if (velocityTracker5 != null) {
                        velocityTracker5.recycle();
                        this.velocityTracker = null;
                    }
                }
            } else if (this.isDragging) {
                this.lastTouchX = f4;
                this.lastTouchY = f5;
                VelocityTracker velocityTracker6 = this.velocityTracker;
                if (velocityTracker6 != null) {
                    velocityTracker6.addMovement(motionEvent);
                    this.velocityTracker.computeCurrentVelocity(1000);
                    float xVelocity = this.velocityTracker.getXVelocity();
                    float yVelocity = this.velocityTracker.getYVelocity();
                    if (Math.max(Math.abs(xVelocity), Math.abs(yVelocity)) >= this.scaledMinimumFlingVelocity && ZoomImageView.this.getDrawable() != null) {
                        ZoomImageView zoomImageView = ZoomImageView.this;
                        FlingRunnable unused = zoomImageView.currentFlingRunnable = new FlingRunnable(zoomImageView.getContext());
                        ZoomImageView.this.currentFlingRunnable.fling(ZoomImageView.this.getWidth(), ZoomImageView.this.getHeight(), (int) (-xVelocity), (int) (-yVelocity));
                        ZoomImageView zoomImageView2 = ZoomImageView.this;
                        zoomImageView2.post(zoomImageView2.currentFlingRunnable);
                    }
                }
            }
            return true;
        }

        public boolean onScale(ScaleGestureDetector scaleGestureDetector2) {
            float scale = ZoomImageView.this.getScale();
            float scaleFactor = scaleGestureDetector2.getScaleFactor();
            if (ZoomImageView.this.getDrawable() == null) {
                return true;
            }
            if (scale >= ZoomImageView.this.maxScale && scaleFactor > 1.0f) {
                return true;
            }
            if (((double) scale) <= 0.75d && scaleFactor < 1.0f) {
                return true;
            }
            ZoomImageView.this.suppMatrix.postScale(scaleFactor, scaleFactor, scaleGestureDetector2.getFocusX(), scaleGestureDetector2.getFocusY());
            ZoomImageView.this.checkAndDisplayMatrix();
            return true;
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            try {
                float scale = ZoomImageView.this.getScale();
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                if (scale < ZoomImageView.this.midScale) {
                    ZoomImageView.this.post(new AnimatedZoomRunnable(scale, ZoomImageView.this.midScale, x, y));
                    return true;
                } else if (scale < ZoomImageView.this.midScale || scale >= ZoomImageView.this.maxScale) {
                    ZoomImageView.this.post(new AnimatedZoomRunnable(scale, ZoomImageView.this.minScale, x, y));
                    return true;
                } else {
                    ZoomImageView.this.post(new AnimatedZoomRunnable(scale, ZoomImageView.this.maxScale, x, y));
                    return true;
                }
            } catch (Exception unused) {
                return true;
            }
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            RectF displayRect;
            if (!(ZoomImageView.this.photoTapListener == null || (displayRect = ZoomImageView.this.getDisplayRect()) == null)) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                if (displayRect.contains(x, y)) {
                    ZoomImageView.this.photoTapListener.onPhotoTap(ZoomImageView.this, (x - displayRect.left) / displayRect.width(), (y - displayRect.top) / displayRect.height());
                    return true;
                }
            }
            if (ZoomImageView.this.viewTapListener == null) {
                return false;
            }
            ZoomImageView.this.viewTapListener.onViewTap(ZoomImageView.this, motionEvent.getX(), motionEvent.getY());
            return false;
        }

        public void onLongPress(MotionEvent motionEvent) {
            if (ZoomImageView.this.longClickListener != null) {
                ZoomImageView.this.longClickListener.onLongClick(ZoomImageView.this);
            }
        }
    }

    private class ScrollerProxy {
        private boolean isOld;
        private Object scroller;

        public ScrollerProxy(Context context) {
            if (Build.VERSION.SDK_INT < 9) {
                this.isOld = true;
                this.scroller = new Scroller(context);
                return;
            }
            this.isOld = false;
            this.scroller = new OverScroller(context);
        }

        public boolean computeScrollOffset() {
            if (this.isOld) {
                return ((Scroller) this.scroller).computeScrollOffset();
            }
            return ((OverScroller) this.scroller).computeScrollOffset();
        }

        public void fling(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
            if (this.isOld) {
                ((Scroller) this.scroller).fling(i, i2, i3, i4, i5, i6, i7, i8);
            } else {
                ((OverScroller) this.scroller).fling(i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
            }
        }

        public void forceFinished(boolean z) {
            if (this.isOld) {
                ((Scroller) this.scroller).forceFinished(z);
            } else {
                ((OverScroller) this.scroller).forceFinished(z);
            }
        }

        public int getCurrX() {
            return this.isOld ? ((Scroller) this.scroller).getCurrX() : ((OverScroller) this.scroller).getCurrX();
        }

        public int getCurrY() {
            return this.isOld ? ((Scroller) this.scroller).getCurrY() : ((OverScroller) this.scroller).getCurrY();
        }
    }

    public ZoomImageView(Context context) {
        this(context, null);
    }

    public ZoomImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ZoomImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.minScale = 1.0f;
        this.midScale = 1.75f;
        this.maxScale = 3.0f;
        this.allowParentInterceptOnEdge = true;
        this.baseMatrix = new Matrix();
        this.drawMatrix = new Matrix();
        this.suppMatrix = new Matrix();
        this.displayRect = new RectF();
        this.matrixValues = new float[9];
        this.scrollEdge = 2;
        this.scaleType = ImageView.ScaleType.FIT_XY;
        super.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        setOnTouchListener(this);
        this.multiGestureDetector = new MultiGestureDetector(context);
        setIsZoomEnabled(true);
    }

    public final RectF getDisplayRect() {
        checkMatrixBounds();
        return getDisplayRect(getDisplayMatrix());
    }

    public float getMinScale() {
        return this.minScale;
    }

    public void setMinScale(float f) {
        checkZoomLevels(f, this.midScale, this.maxScale);
        this.minScale = f;
    }

    public float getMidScale() {
        return this.midScale;
    }

    public void setMidScale(float f) {
        checkZoomLevels(this.minScale, f, this.maxScale);
        this.midScale = f;
    }

    public float getMaxScale() {
        return this.maxScale;
    }

    public void setMaxScale(float f) {
        checkZoomLevels(this.minScale, this.midScale, f);
        this.maxScale = f;
    }

    public final float getScale() {
        this.suppMatrix.getValues(this.matrixValues);
        return this.matrixValues[0];
    }

    public final ImageView.ScaleType getScaleType() {
        return this.scaleType;
    }

    public final void setScaleType(ImageView.ScaleType scaleType2) {
        if (scaleType2 == ImageView.ScaleType.MATRIX) {
            throw new IllegalArgumentException(scaleType2.name() + " is not supported in ZoomImageView");
        } else if (scaleType2 != this.scaleType) {
            this.scaleType = scaleType2;
            update();
        }
    }

    public final boolean isZoomEnabled() {
        return this.isZoomEnabled;
    }

    public final void setIsZoomEnabled(boolean z) {
        this.isZoomEnabled = z;
        update();
    }

    public void setAllowParentInterceptOnEdge(boolean z) {
        this.allowParentInterceptOnEdge = z;
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        update();
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        update();
    }

    public void setImageResource(int i) {
        super.setImageResource(i);
        update();
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        update();
    }

    public final void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.longClickListener = onLongClickListener;
    }

    public final void setOnPhotoTapListener(OnPhotoTapListener onPhotoTapListener) {
        this.photoTapListener = onPhotoTapListener;
    }

    public final void setOnViewTapListener(OnViewTapListener onViewTapListener) {
        this.viewTapListener = onViewTapListener;
    }

    public final void onGlobalLayout() {
        if (this.isZoomEnabled) {
            int top2 = getTop();
            int right2 = getRight();
            int bottom2 = getBottom();
            int left2 = getLeft();
            if (top2 != this.top || bottom2 != this.bottom || left2 != this.left || right2 != this.right) {
                updateBaseMatrix(getDrawable());
                this.top = top2;
                this.right = right2;
                this.bottom = bottom2;
                this.left = left2;
            }
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        RectF displayRect2;
        boolean z = false;
        if (!this.isZoomEnabled) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            if (view.getParent() != null) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
            }
            FlingRunnable flingRunnable = this.currentFlingRunnable;
            if (flingRunnable != null) {
                flingRunnable.cancelFling();
                this.currentFlingRunnable = null;
            }
        } else if ((action == 1 || action == 3) && getScale() < this.minScale && (displayRect2 = getDisplayRect()) != null) {
            view.post(new AnimatedZoomRunnable(getScale(), this.minScale, displayRect2.centerX(), displayRect2.centerY()));
            z = true;
        }
        MultiGestureDetector multiGestureDetector2 = this.multiGestureDetector;
        if (multiGestureDetector2 == null || !multiGestureDetector2.onTouchEvent(motionEvent)) {
            return z;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getViewTreeObserver().removeGlobalOnLayoutListener(this);
    }

    /* access modifiers changed from: protected */
    public Matrix getDisplayMatrix() {
        this.drawMatrix.set(this.baseMatrix);
        this.drawMatrix.postConcat(this.suppMatrix);
        return this.drawMatrix;
    }

    private final void update() {
        if (this.isZoomEnabled) {
            super.setScaleType(ImageView.ScaleType.MATRIX);
            updateBaseMatrix(getDrawable());
            return;
        }
        resetMatrix();
    }

    /* access modifiers changed from: private */
    public void checkAndDisplayMatrix() {
        checkMatrixBounds();
        setImageMatrix(getDisplayMatrix());
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void checkMatrixBounds() {
        /*
            r10 = this;
            android.graphics.Matrix r0 = r10.getDisplayMatrix()
            android.graphics.RectF r0 = r10.getDisplayRect(r0)
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            float r1 = r0.height()
            float r2 = r0.width()
            int r3 = r10.getHeight()
            float r3 = (float) r3
            r4 = 1073741824(0x40000000, float:2.0)
            r5 = 2
            r6 = 1
            r7 = 0
            int r8 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r8 > 0) goto L_0x003d
            int[] r8 = com.wade.fit.views.zoomview.ZoomImageView.C27461.$SwitchMap$android$widget$ImageView$ScaleType
            android.widget.ImageView$ScaleType r9 = r10.scaleType
            int r9 = r9.ordinal()
            r8 = r8[r9]
            if (r8 == r6) goto L_0x003a
            if (r8 == r5) goto L_0x0036
            float r3 = r3 - r1
            float r3 = r3 / r4
            float r1 = r0.top
        L_0x0033:
            float r1 = r3 - r1
            goto L_0x0051
        L_0x0036:
            float r3 = r3 - r1
            float r1 = r0.top
            goto L_0x0033
        L_0x003a:
            float r1 = r0.top
            goto L_0x0045
        L_0x003d:
            float r1 = r0.top
            int r1 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r1 <= 0) goto L_0x0047
            float r1 = r0.top
        L_0x0045:
            float r1 = -r1
            goto L_0x0051
        L_0x0047:
            float r1 = r0.bottom
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0050
            float r1 = r0.bottom
            goto L_0x0033
        L_0x0050:
            r1 = 0
        L_0x0051:
            int r3 = r10.getWidth()
            float r3 = (float) r3
            int r8 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r8 > 0) goto L_0x007a
            int[] r7 = com.wade.fit.views.zoomview.ZoomImageView.C27461.$SwitchMap$android$widget$ImageView$ScaleType
            android.widget.ImageView$ScaleType r8 = r10.scaleType
            int r8 = r8.ordinal()
            r7 = r7[r8]
            if (r7 == r6) goto L_0x0073
            if (r7 == r5) goto L_0x006f
            float r3 = r3 - r2
            float r3 = r3 / r4
            float r0 = r0.left
        L_0x006c:
            float r3 = r3 - r0
            r7 = r3
            goto L_0x0077
        L_0x006f:
            float r3 = r3 - r2
            float r0 = r0.left
            goto L_0x006c
        L_0x0073:
            float r0 = r0.left
            float r0 = -r0
            r7 = r0
        L_0x0077:
            r10.scrollEdge = r5
            goto L_0x0097
        L_0x007a:
            float r2 = r0.left
            int r2 = (r2 > r7 ? 1 : (r2 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x0087
            r2 = 0
            r10.scrollEdge = r2
            float r0 = r0.left
            float r7 = -r0
            goto L_0x0097
        L_0x0087:
            float r2 = r0.right
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 >= 0) goto L_0x0094
            float r0 = r0.right
            float r7 = r3 - r0
            r10.scrollEdge = r6
            goto L_0x0097
        L_0x0094:
            r0 = -1
            r10.scrollEdge = r0
        L_0x0097:
            android.graphics.Matrix r0 = r10.suppMatrix
            r0.postTranslate(r7, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.zoomview.ZoomImageView.checkMatrixBounds():void");
    }

    /* renamed from: com.wade.fit.views.zoomview.ZoomImageView$1 */
    static /* synthetic */ class C27461 {
        static final /* synthetic */ int[] $SwitchMap$android$widget$ImageView$ScaleType = new int[ImageView.ScaleType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                android.widget.ImageView$ScaleType[] r0 = android.widget.ImageView.ScaleType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.views.zoomview.ZoomImageView.C27461.$SwitchMap$android$widget$ImageView$ScaleType = r0
                int[] r0 = com.wade.fit.views.zoomview.ZoomImageView.C27461.$SwitchMap$android$widget$ImageView$ScaleType     // Catch:{ NoSuchFieldError -> 0x0014 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_START     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.views.zoomview.ZoomImageView.C27461.$SwitchMap$android$widget$ImageView$ScaleType     // Catch:{ NoSuchFieldError -> 0x001f }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_END     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.views.zoomview.ZoomImageView.C27461.$SwitchMap$android$widget$ImageView$ScaleType     // Catch:{ NoSuchFieldError -> 0x002a }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_CENTER     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.wade.fit.views.zoomview.ZoomImageView.C27461.$SwitchMap$android$widget$ImageView$ScaleType     // Catch:{ NoSuchFieldError -> 0x0035 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_XY     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.zoomview.ZoomImageView.C27461.<clinit>():void");
        }
    }

    private RectF getDisplayRect(Matrix matrix) {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return null;
        }
        this.displayRect.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
        matrix.mapRect(this.displayRect);
        return this.displayRect;
    }

    private void resetMatrix() {
        this.suppMatrix.reset();
        setImageMatrix(getDisplayMatrix());
        checkMatrixBounds();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void updateBaseMatrix(Drawable drawable) {
        if (drawable != null) {
            float width = (float) getWidth();
            float height = (float) getHeight();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            this.baseMatrix.reset();
            float f = (float) intrinsicWidth;
            float f2 = width / f;
            float f3 = (float) intrinsicHeight;
            float f4 = height / f3;
            if (this.scaleType == ImageView.ScaleType.CENTER) {
                this.baseMatrix.postTranslate((width - f) / 2.0f, (height - f3) / 2.0f);
            } else if (this.scaleType == ImageView.ScaleType.CENTER_CROP) {
                float max = Math.max(f2, f4);
                this.baseMatrix.postScale(max, max);
                this.baseMatrix.postTranslate((width - (f * max)) / 2.0f, (height - (f3 * max)) / 2.0f);
            } else if (this.scaleType == ImageView.ScaleType.CENTER_INSIDE) {
                float min = Math.min(1.0f, Math.min(f2, f4));
                this.baseMatrix.postScale(min, min);
                this.baseMatrix.postTranslate((width - (f * min)) / 2.0f, (height - (f3 * min)) / 2.0f);
            } else {
                RectF rectF = new RectF(0.0f, 0.0f, f, f3);
                RectF rectF2 = new RectF(0.0f, 0.0f, width, height);
                int i = C27461.$SwitchMap$android$widget$ImageView$ScaleType[this.scaleType.ordinal()];
                if (i == 1) {
                    this.baseMatrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.START);
                } else if (i == 2) {
                    this.baseMatrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.END);
                } else if (i == 3) {
                    this.baseMatrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
                } else if (i == 4) {
                    this.baseMatrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.FILL);
                }
            }
            resetMatrix();
        }
    }

    /* access modifiers changed from: private */
    public void postOnAnimation(View view, Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimation(runnable);
        } else {
            view.postDelayed(runnable, 16);
        }
    }

    private void checkZoomLevels(float f, float f2, float f3) {
        if (f >= f2) {
            throw new IllegalArgumentException("MinZoom should be less than MidZoom");
        } else if (f2 >= f3) {
            throw new IllegalArgumentException("MidZoom should be less than MaxZoom");
        }
    }

    private class AnimatedZoomRunnable implements Runnable {
        static final float ANIMATION_SCALE_PER_ITERATION_IN = 1.07f;
        static final float ANIMATION_SCALE_PER_ITERATION_OUT = 0.93f;
        private final float deltaScale;
        private final float focalX;
        private final float focalY;
        private final float targetZoom;

        public AnimatedZoomRunnable(float f, float f2, float f3, float f4) {
            this.targetZoom = f2;
            this.focalX = f3;
            this.focalY = f4;
            if (f < f2) {
                this.deltaScale = ANIMATION_SCALE_PER_ITERATION_IN;
            } else {
                this.deltaScale = ANIMATION_SCALE_PER_ITERATION_OUT;
            }
        }

        public void run() {
            Matrix access$000 = ZoomImageView.this.suppMatrix;
            float f = this.deltaScale;
            access$000.postScale(f, f, this.focalX, this.focalY);
            ZoomImageView.this.checkAndDisplayMatrix();
            float scale = ZoomImageView.this.getScale();
            if ((this.deltaScale <= 1.0f || scale >= this.targetZoom) && (this.deltaScale >= 1.0f || this.targetZoom >= scale)) {
                float f2 = this.targetZoom / scale;
                ZoomImageView.this.suppMatrix.postScale(f2, f2, this.focalX, this.focalY);
                ZoomImageView.this.checkAndDisplayMatrix();
                return;
            }
            ZoomImageView zoomImageView = ZoomImageView.this;
            zoomImageView.postOnAnimation(zoomImageView, this);
        }
    }

    private class FlingRunnable implements Runnable {
        private int currentX;
        private int currentY;
        private final ScrollerProxy scroller;

        public FlingRunnable(Context context) {
            this.scroller = new ScrollerProxy(context);
        }

        public void cancelFling() {
            this.scroller.forceFinished(true);
        }

        public void fling(int i, int i2, int i3, int i4) {
            int i5;
            int i6;
            int i7;
            int i8;
            RectF displayRect = ZoomImageView.this.getDisplayRect();
            if (displayRect != null) {
                int round = Math.round(-displayRect.left);
                float f = (float) i;
                if (f < displayRect.width()) {
                    i5 = Math.round(displayRect.width() - f);
                    i6 = 0;
                } else {
                    i6 = round;
                    i5 = i6;
                }
                int round2 = Math.round(-displayRect.top);
                float f2 = (float) i2;
                if (f2 < displayRect.height()) {
                    i7 = Math.round(displayRect.height() - f2);
                    i8 = 0;
                } else {
                    i8 = round2;
                    i7 = i8;
                }
                this.currentX = round;
                this.currentY = round2;
                if (round != i5 || round2 != i7) {
                    this.scroller.fling(round, round2, i3, i4, i6, i5, i8, i7, 0, 0);
                }
            }
        }

        public void run() {
            if (this.scroller.computeScrollOffset()) {
                int currX = this.scroller.getCurrX();
                int currY = this.scroller.getCurrY();
                ZoomImageView.this.suppMatrix.postTranslate((float) (this.currentX - currX), (float) (this.currentY - currY));
                ZoomImageView zoomImageView = ZoomImageView.this;
                zoomImageView.setImageMatrix(zoomImageView.getDisplayMatrix());
                this.currentX = currX;
                this.currentY = currY;
                ZoomImageView zoomImageView2 = ZoomImageView.this;
                zoomImageView2.postOnAnimation(zoomImageView2, this);
            }
        }
    }
}
