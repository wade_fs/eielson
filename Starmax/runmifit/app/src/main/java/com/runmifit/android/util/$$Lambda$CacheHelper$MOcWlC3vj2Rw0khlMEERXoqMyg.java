package com.wade.fit.util;

import com.bumptech.glide.Glide;
import com.wade.fit.app.AppApplication;

/* renamed from: com.wade.fit.util.-$$Lambda$CacheHelper$MOcWlC3vj2R-w0khlMEERXoqMyg  reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$CacheHelper$MOcWlC3vj2Rw0khlMEERXoqMyg implements Runnable {
    public static final /* synthetic */ $$Lambda$CacheHelper$MOcWlC3vj2Rw0khlMEERXoqMyg INSTANCE = new $$Lambda$CacheHelper$MOcWlC3vj2Rw0khlMEERXoqMyg();

    private /* synthetic */ $$Lambda$CacheHelper$MOcWlC3vj2Rw0khlMEERXoqMyg() {
    }

    public final void run() {
        Glide.get(AppApplication.getInstance()).clearDiskCache();
    }
}
