package com.wade.fit.base.refresh;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.RecyclerRefreshLayout;

public class BaseRefreshActivity_ViewBinding implements Unbinder {
    private BaseRefreshActivity target;

    public BaseRefreshActivity_ViewBinding(BaseRefreshActivity baseRefreshActivity) {
        this(baseRefreshActivity, baseRefreshActivity.getWindow().getDecorView());
    }

    public BaseRefreshActivity_ViewBinding(BaseRefreshActivity baseRefreshActivity, View view) {
        this.target = baseRefreshActivity;
        baseRefreshActivity.flTop = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.fl_top, "field 'flTop'", FrameLayout.class);
        baseRefreshActivity.flBottom = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.fl_bottom, "field 'flBottom'", FrameLayout.class);
        baseRefreshActivity.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.refresh_recyclerView, "field 'mRecyclerView'", RecyclerView.class);
        baseRefreshActivity.mRefreshLayout = (RecyclerRefreshLayout) Utils.findRequiredViewAsType(view, R.id.refreshLayout, "field 'mRefreshLayout'", RecyclerRefreshLayout.class);
    }

    public void unbind() {
        BaseRefreshActivity baseRefreshActivity = this.target;
        if (baseRefreshActivity != null) {
            this.target = null;
            baseRefreshActivity.flTop = null;
            baseRefreshActivity.flBottom = null;
            baseRefreshActivity.mRecyclerView = null;
            baseRefreshActivity.mRefreshLayout = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
