package com.wade.fit.views.wheel;

public interface OnWheelChangedNewListener {
    void onChanged(NewWheelView newWheelView, int i, int i2);
}
