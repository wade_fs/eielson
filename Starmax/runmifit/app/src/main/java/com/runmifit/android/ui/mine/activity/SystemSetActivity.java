package com.wade.fit.ui.mine.activity;

import android.view.View;
import com.wade.fit.R;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.util.IntentUtil;

/* renamed from: com.wade.fit.ui.mine.activity.SystemSetActivity */
public class SystemSetActivity extends BaseActivity implements Constants {
    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_system_set;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.mine_sys_settings));
    }

    public void onClick(View view) {
        IntentUtil.goToActivity(this, UnitActivity.class);
    }

    public void toGoogleFit() {
        IntentUtil.goToActivity(this, GoogleFitActivity.class);
    }
}
