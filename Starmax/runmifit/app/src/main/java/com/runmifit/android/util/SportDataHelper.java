package com.wade.fit.util;

import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.model.bean.HeartRateInterval;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SportDataHelper implements Constants {
    public static int getResByType(int i) {
        switch (i) {
            case 0:
                return R.mipmap.sport_type_walk;
            case 1:
            default:
                return R.mipmap.sport_run;
            case 2:
                return R.mipmap.sport_swing;
            case 3:
                return R.mipmap.sport_bycle;
            case 4:
                return R.mipmap.sport_type_run_indoor;
            case 5:
                return R.mipmap.sport_train;
            case 6:
                return R.mipmap.sport_type_socker;
            case 7:
                return R.mipmap.sport_type_basketball;
            case 8:
                return R.mipmap.sport_type_badminton;
            case 9:
                return R.mipmap.sport_type_rope;
            case 10:
                return R.mipmap.sport_sit_ups;
            case 11:
                return R.mipmap.sport_push_ups;
            case 12:
                return R.mipmap.sport_cli;
        }
    }

    public static List<String> getSportType() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(AppApplication.getInstance().getResources().getStringArray(R.array.sportTypes)));
        return arrayList;
    }

    public static String getSelectSportType() {
        return getSportType().get(((Integer) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.CHECKED_SPORT_TYPE_INDEX, 0)).intValue());
    }

    public static int getSportGpsCarloy(int i, float f, double d) {
        double d2 = 0.8214d;
        if (i != 0) {
            if (i == 1) {
                d2 = 1.036d;
            } else if (i == 3) {
                d2 = 0.6142d;
            }
        }
        double d3 = (double) f;
        Double.isNaN(d3);
        return (int) (d3 * d * d2);
    }

    public static HeartRateInterval initHeartRateValue(int i) {
        HeartRateInterval heartRateInterval = new HeartRateInterval();
        heartRateInterval.userMaxHR = i;
        double d = (double) i;
        Double.isNaN(d);
        heartRateInterval.setLimintThreshold((int) (0.85d * d));
        Double.isNaN(d);
        heartRateInterval.setAerobicThreshold((int) (0.7d * d));
        Double.isNaN(d);
        heartRateInterval.setBurnFatThreshold((int) (d * 0.5d));
        return heartRateInterval;
    }

    public static HealthHeartRate initHealthHeartRate(int i) {
        HealthHeartRate healthHeartRate = new HealthHeartRate();
        double d = (double) i;
        Double.isNaN(d);
        healthHeartRate.setLimit_threshold((int) (0.85d * d));
        Double.isNaN(d);
        healthHeartRate.setAerobic_threshold((int) (0.7d * d));
        Double.isNaN(d);
        healthHeartRate.setBurn_fat_threshold((int) (d * 0.5d));
        return healthHeartRate;
    }

    public static String getStringBytyp(int i) {
        return CacheHelper.getSportName(i);
    }
}
