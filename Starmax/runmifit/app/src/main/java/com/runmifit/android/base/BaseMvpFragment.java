package com.wade.fit.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.util.CnWinUtil;
import com.wade.fit.util.DialogHelperNew;

public abstract class BaseMvpFragment<T extends BasePersenter> extends BaseFragment implements IBaseView {
    protected final String TAG = getClass().getSimpleName();
    protected FragmentActivity activity;
    protected Bundle mBundle;
    protected Context mContext;
    protected LayoutInflater mInflater;
    protected T mPresenter;
    protected View mRootView;
    Unbinder unbinder;

    /* access modifiers changed from: protected */
    public abstract int getLayoutId();

    /* access modifiers changed from: protected */
    public void initBundle(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void initView() {
    }

    public void showNetError(String str) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Log.e(this.TAG, "onCreate");
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        Log.e(this.TAG, "onPause");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.mPresenter = (BasePersenter) CnWinUtil.getT(this, 0);
        Log.e(this.TAG, "onCreateView");
        T t = this.mPresenter;
        if (t != null) {
            t.attachView(this);
        }
        this.mRootView = layoutInflater.inflate(getLayoutId(), viewGroup, false);
        this.mInflater = layoutInflater;
        this.unbinder = ButterKnife.bind(this, this.mRootView);
        initView();
        initData();
        this.mContext = getActivity();
        return this.mRootView;
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.e(this.TAG, "onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        T t = this.mPresenter;
        if (t != null) {
            t.detachView();
        }
        Unbinder unbinder2 = this.unbinder;
        if (unbinder2 != null) {
            unbinder2.unbind();
        }
        Log.e(this.TAG, "onDestroy");
    }

    public void showMsg(String str) {
        showToast(str);
    }

    public void showLoading() {
        DialogHelperNew.buildWaitDialog(getActivity(), true);
    }

    public void showLoadingFalse() {
        DialogHelperNew.buildWaitDialog(getActivity(), false);
    }

    public void hideLoading() {
        DialogHelperNew.dismissWait();
    }

    public void goBack() {
        getActivity().finish();
    }
}
