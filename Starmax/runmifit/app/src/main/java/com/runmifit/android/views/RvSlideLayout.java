package com.wade.fit.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import com.wade.fit.R;
import com.wade.fit.base.refresh.BaseDeleteAdapter;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.log.LogUtil;

public class RvSlideLayout extends HorizontalScrollView {
    private static final float radio = 0.2f;
    long downTime = 0;
    private boolean isOpen;
    private final int mMenuWidth;
    private final int mScreenWidth;
    private int mSpacing = 0;
    private boolean once = true;

    public RvSlideLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mSpacing = (int) context.obtainStyledAttributes(attributeSet, R.styleable.RvSlideLayout).getDimension(0, 0.0f);
        this.mScreenWidth = ScreenUtil.getScreenWidth(context);
        this.mMenuWidth = (int) (((float) this.mScreenWidth) * 0.2f);
        setOverScrollMode(2);
        setHorizontalScrollBarEnabled(false);
    }

    public void closeMenu() {
        smoothScrollTo(0, 0);
        this.isOpen = false;
    }

    public boolean isOpen() {
        return this.isOpen;
    }

    private BaseDeleteAdapter getAdapter() {
        View view = this;
        do {
            view = (View) view.getParent();
        } while (!(view instanceof RecyclerView));
        return (BaseDeleteAdapter) ((RecyclerView) view).getAdapter();
    }

    private void onOpenMenu() {
        getAdapter().holdOpenMenu(this);
        this.isOpen = true;
    }

    public void closeOpenMenu() {
        this.isOpen = false;
        getAdapter().closeOpenMenu();
    }

    public RvSlideLayout getScrollingMenu() {
        return getAdapter().getScrollingMenu();
    }

    public void setScrollingMenu(RvSlideLayout rvSlideLayout) {
        getAdapter().setScrollingMenu(rvSlideLayout);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.once) {
            LinearLayout linearLayout = (LinearLayout) getChildAt(0);
            linearLayout.getChildAt(0).getLayoutParams().width = this.mScreenWidth - this.mSpacing;
            linearLayout.getChildAt(1).getLayoutParams().width = this.mMenuWidth;
            this.once = false;
        }
        super.onMeasure(i, i2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            this.downTime = System.currentTimeMillis();
            closeOpenMenu();
            setScrollingMenu(this);
            LogUtil.d("down:" + motionEvent.getX());
        } else if (action == 1) {
            setScrollingMenu(null);
            int scrollX = getScrollX();
            LogUtil.d("up:" + motionEvent.getX());
            if (System.currentTimeMillis() - this.downTime > 100 || scrollX != 0) {
                if (scrollX != 0) {
                    int abs = Math.abs(scrollX);
                    int i = this.mMenuWidth;
                    if (abs > i / 2) {
                        smoothScrollTo(i, 0);
                        onOpenMenu();
                    } else {
                        closeMenu();
                    }
                }
                return true;
            }
            closeOpenMenu();
            performClick();
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }
}
