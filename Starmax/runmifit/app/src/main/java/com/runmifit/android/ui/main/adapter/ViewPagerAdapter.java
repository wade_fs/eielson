package com.wade.fit.ui.main.adapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

/* renamed from: com.wade.fit.ui.main.adapter.ViewPagerAdapter */
public class ViewPagerAdapter extends PagerAdapter {
    private int mChildCount = 0;
    private List<View> views;

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public ViewPagerAdapter(List<View> list) {
        this.views = list;
    }

    public void notifyDataSetChanged() {
        this.mChildCount = getCount();
        super.notifyDataSetChanged();
    }

    public int getCount() {
        List<View> list = this.views;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public int getItemPosition(Object obj) {
        int i = this.mChildCount;
        if (i <= 0) {
            return super.getItemPosition(obj);
        }
        this.mChildCount = i - 1;
        return -2;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        ((ViewPager) viewGroup).removeView(this.views.get(i));
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        ((ViewPager) viewGroup).addView(this.views.get(i), 0);
        return this.views.get(i);
    }
}
