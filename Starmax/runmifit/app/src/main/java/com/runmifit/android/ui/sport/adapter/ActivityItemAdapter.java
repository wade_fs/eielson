package com.wade.fit.ui.sport.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.base.BaseAdapter;
import com.wade.fit.base.BaseViewHolder;
import com.wade.fit.ui.sport.bean.SportActivityItem;
import java.util.List;

/* renamed from: com.wade.fit.ui.sport.adapter.ActivityItemAdapter */
public class ActivityItemAdapter extends BaseAdapter<SportActivityItem, ViewHolder> {

    /* renamed from: com.wade.fit.ui.sport.adapter.ActivityItemAdapter$ViewHolder_ViewBinding */
    public class ViewHolder_ViewBinding implements Unbinder {
        private ViewHolder target;

        public ViewHolder_ViewBinding(ViewHolder viewHolder, View view) {
            this.target = viewHolder;
            viewHolder.imgIcon = (ImageView) Utils.findRequiredViewAsType(view, R.id.imgIcon, "field 'imgIcon'", ImageView.class);
            viewHolder.txContent = (TextView) Utils.findRequiredViewAsType(view, R.id.txContent, "field 'txContent'", TextView.class);
            viewHolder.txDes = (TextView) Utils.findRequiredViewAsType(view, R.id.txDes, "field 'txDes'", TextView.class);
        }

        public void unbind() {
            ViewHolder viewHolder = this.target;
            if (viewHolder != null) {
                this.target = null;
                viewHolder.imgIcon = null;
                viewHolder.txContent = null;
                viewHolder.txDes = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public ActivityItemAdapter(Context context, List<SportActivityItem> list) {
        super(context, list);
    }

    /* access modifiers changed from: protected */
    public void onNormalBindViewHolder(ViewHolder viewHolder, SportActivityItem sportActivityItem, int i) {
        viewHolder.imgIcon.setImageResource(sportActivityItem.getImgResource());
        viewHolder.txContent.setText(sportActivityItem.getContent());
        viewHolder.txContent.setTypeface(Typeface.createFromAsset(this.mContext.getAssets(), "fonts/DINCondensedBold.ttf"));
        viewHolder.txDes.setText(sportActivityItem.getDes());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(this.inflater.inflate((int) R.layout.item_sport_activity, viewGroup, false));
    }

    /* renamed from: com.wade.fit.ui.sport.adapter.ActivityItemAdapter$ViewHolder */
    public class ViewHolder extends BaseViewHolder {
        ImageView imgIcon;
        TextView txContent;
        TextView txDes;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
