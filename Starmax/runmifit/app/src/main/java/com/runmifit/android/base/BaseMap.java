package com.wade.fit.base;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.view.View;
import com.baidu.mapapi.model.LatLng;
import com.wade.fit.R;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.LatLngBean;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.MapHelper;
import com.wade.fit.util.ObjectUtil;
import com.wade.fit.util.ThreadUtil;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.callback.MapScreenShotCallback;
import com.wade.fit.util.file.FileUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class BaseMap<T extends View, L> implements MapScreenShotCallback {
    protected static final int HID_MAP_INDEX = 50;
    protected static final int LINE_INDEX = 102;
    protected static final int LINE_WIDTH = 12;
    protected static int MAP_SHOT_TIME_OUT = 5000;
    protected static final int MARKET_INDEX = 200;
    protected static int UPDATE_INTERVAL = 6000;
    protected Activity activity;
    protected float[] bikeSpeed = {21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f, 30.0f};
    protected LatLngBean centerLatlng;
    protected int[] colorArray = {SupportMenu.CATEGORY_MASK, -43759, -30720, -17664, InputDeviceCompat.SOURCE_ANY, -4456704, -8913152, -16711936};
    protected List<Integer> colorList = new ArrayList();
    protected LatLngBean currentLatLng;
    protected int distanceUnit;
    protected boolean isBaidu;
    public boolean isHideMapView = false;
    protected boolean isMarker = true;
    protected LatLngBean lastLatLng = null;
    protected List<LatLngBean> latLngBeanList = new ArrayList();
    protected List<L> latLngList = new ArrayList();
    protected int mapLoadDelay = 1000;
    private Handler mapShotTimeOutHanlder = new Handler();
    protected T mapView;
    protected List<Integer> mileMarkList = new ArrayList();
    protected boolean onMapLoaded = false;
    protected float[] runSpeed = {3.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f};
    protected float[] speeds;
    protected int sporType;
    protected long startTime;
    protected float[] walkSpeed = {3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 7.5f, 8.0f};
    protected float zoomLevel = 15.0f;

    public abstract void addMarker(LatLngBean latLngBean, int i);

    public abstract void addMileMark();

    public abstract void addPolyline(LatLngBean latLngBean);

    public abstract void ajustMapView();

    /* access modifiers changed from: protected */
    public abstract void animateCamera(LatLngBean latLngBean);

    /* access modifiers changed from: protected */
    public abstract void drawPolyline();

    /* access modifiers changed from: protected */
    public abstract L fromLatLngBean(LatLngBean latLngBean);

    public void onCreate(Bundle bundle) {
    }

    public void onDestroy() {
    }

    public abstract void onMapScreenShot(MapScreenShotCallback mapScreenShotCallback);

    public void onPause() {
    }

    public void onResume() {
    }

    public void onSaveInstanceState(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public abstract void preDrawAllAndShot();

    public abstract void setMapType(boolean z);

    /* access modifiers changed from: protected */
    public abstract void showOrHideMap(LatLngBean latLngBean);

    public void setActivity(Activity activity2) {
        this.activity = activity2;
    }

    public long getStartTime() {
        return this.startTime;
    }

    public void setStartTime(long j) {
        this.startTime = j;
    }

    public T getMapView() {
        return this.mapView;
    }

    public double getDistance(LatLngBean latLngBean, LatLngBean latLngBean2) {
        new LatLng(latLngBean.latitude, latLngBean.longitude);
        new LatLng(latLngBean2.latitude, latLngBean2.longitude);
        return MapHelper.getDistance(latLngBean, latLngBean2);
    }

    public void initMapView(T t) {
        this.mapView = t;
    }

    public void addPolylineAndMove(LatLngBean latLngBean, boolean z) {
        addPolyline(latLngBean);
        this.latLngBeanList.add(latLngBean);
        if (this.latLngBeanList.size() == 1) {
            addMarker(latLngBean, R.mipmap.run_go);
        }
        animateCamera(latLngBean);
    }

    public final void drawAllAndShot(long j, int i) {
        if (this.mapView != null) {
            this.sporType = i;
            if (!ObjectUtil.isCollectionEmpty((Collection) getLatLngBeanList())) {
                preDrawAllAndShot();
                this.latLngList.clear();
                this.startTime = j;
                List<LatLngBean> latLngBeanList2 = getLatLngBeanList();
                addMarker(latLngBeanList2.get(0), R.mipmap.run_go);
                comleteColorAndMileMark();
                int size = latLngBeanList2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.latLngList.add(fromLatLngBean(latLngBeanList2.get(i2)));
                }
                drawPolyline();
                addMarker(latLngBeanList2.get(this.isBaidu ? latLngBeanList2.size() - 2 : latLngBeanList2.size() - 1), R.mipmap.run_go);
                ajustMapView();
                animateCamera(this.centerLatlng);
            }
        }
    }

    private void comleteColorAndMileMark() {
        double d;
        this.mileMarkList.clear();
        this.colorList.clear();
        int size = this.latLngBeanList.size() - 2;
        int i = 0;
        double d2 = 0.0d;
        double d3 = 0.0d;
        double d4 = 0.0d;
        double d5 = 0.0d;
        double d6 = 0.0d;
        while (i < size) {
            LatLngBean latLngBean = this.latLngBeanList.get(i);
            int i2 = i + 1;
            LatLngBean latLngBean2 = this.latLngBeanList.get(i2);
            double d7 = d5;
            double abs = Math.abs(getDistance(latLngBean, latLngBean2));
            int i3 = size;
            int i4 = i2;
            if (this.distanceUnit == 1) {
                abs = (double) UnitUtil.getKm2mile((float) abs);
            }
            this.colorList.add(Integer.valueOf(getColorList(latLngBean2, latLngBean)));
            d6 += abs;
            if (d6 > 1000.0d) {
                d6 -= 1000.0d;
                this.mileMarkList.add(Integer.valueOf(i));
            }
            d2 = Math.max(d2, latLngBean.latitude);
            d4 = Math.max(d4, latLngBean.longitude);
            if (d3 == 0.0d) {
                d3 = latLngBean.latitude;
                d = latLngBean.longitude;
            } else {
                d = d7;
            }
            d3 = Math.min(d3, latLngBean.latitude);
            d5 = Math.min(d, latLngBean.longitude);
            size = i3;
            i = i4;
        }
        this.centerLatlng = new LatLngBean((d2 + d3) / 2.0d, (d4 + d5) / 2.0d);
    }

    /* access modifiers changed from: protected */
    public List<LatLngBean> getLatLngBeanList() {
        return this.latLngBeanList;
    }

    public void setLatLngBeanList(List<LatLngBean> list) {
        this.latLngBeanList = list;
    }

    public void setIsHideMapView(boolean z) {
        this.isHideMapView = z;
    }

    /* access modifiers changed from: protected */
    public boolean shouldMapScreenShot(long j) {
        return j >= 0 && !FileUtil.fileExists(getShotFilePath());
    }

    /* access modifiers changed from: protected */
    public String getShotFilePath() {
        return Constants.PIC_PATH + "/" + this.startTime + ".png";
    }

    public void setIsMarker(boolean z) {
        this.isMarker = z;
        drawAllAndShot(this.startTime, this.sporType);
    }

    private void setSpeedRange() {
        if (this.speeds == null) {
            this.speeds = this.walkSpeed;
        }
    }

    /* access modifiers changed from: protected */
    public int getColorList(LatLngBean latLngBean, LatLngBean latLngBean2) {
        double abs = Math.abs(MapHelper.getDistance(latLngBean, latLngBean2));
        long abs2 = Math.abs(DateUtil.getLongFromDateStr(latLngBean.currentTimeMillis) - DateUtil.getLongFromDateStr(latLngBean2.currentTimeMillis));
        if (abs2 == 0) {
            abs2 = 1;
        }
        double d = (double) abs2;
        Double.isNaN(d);
        double d2 = (abs / d) * 3.6d;
        setSpeedRange();
        int i = 0;
        while (true) {
            float[] fArr = this.speeds;
            if (i >= fArr.length) {
                int[] iArr = this.colorArray;
                return iArr[iArr.length - 1];
            } else if (d2 < ((double) fArr[i])) {
                return this.colorArray[i];
            } else {
                i++;
            }
        }
    }

    public void shotComplet(Bitmap bitmap) {
        addMileMark();
        showOrHideMap(this.centerLatlng);
    }

    public void endAndShot(final MapScreenShotCallback mapScreenShotCallback) {
        addMarker(getLatLngBeanList().get(getLatLngBeanList().size() - 1), R.mipmap.run_end);
        ajustMapView();
        ThreadUtil.delayTask(new Runnable() {
            /* class com.wade.fit.base.BaseMap.C23841 */

            public void run() {
                BaseMap.this.onMapScreenShot(mapScreenShotCallback);
            }
        }, 1000);
        removeTimeOut();
        this.mapShotTimeOutHanlder.postDelayed(new Runnable() {
            /* class com.wade.fit.base.BaseMap.C23852 */

            public void run() {
                MapScreenShotCallback mapScreenShotCallback = mapScreenShotCallback;
                if (mapScreenShotCallback != null) {
                    mapScreenShotCallback.shotComplet(null);
                }
            }
        }, (long) MAP_SHOT_TIME_OUT);
    }

    /* access modifiers changed from: protected */
    public final void removeTimeOut() {
        this.mapShotTimeOutHanlder.removeCallbacksAndMessages(null);
    }
}
