package com.wade.fit.util.ble;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Handler;
import com.wade.fit.app.BleContant;
import com.wade.fit.util.BaseCmdUtil;
import com.wade.fit.util.ThreadUtil;
import com.wade.fit.util.log.LogUtil;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class SendDataTask implements BleContant {
    private static final int TIME_OUT = 5000;
    private Request cacheRequst;
    private LinkedList<Request> cacheTaskQueue = new LinkedList<>();
    private BaseDataHandler dataHandler;
    BluetoothGatt gatt;
    private Handler handler = new Handler();
    boolean isDelaySend;
    boolean isOrder;
    boolean isWaitComplete;
    private boolean mOperationInProgress;
    private LinkedList<Request> mTaskQueue = new LinkedList<>();
    private BluetoothGattCharacteristic mWriteNormalGattCharacteristic;
    private LinkedList<Request> sendQueue = new LinkedList<>();
    private Runnable sendTimeOut = new Runnable() {
        /* class com.wade.fit.util.ble.SendDataTask.C26871 */

        public void run() {
        }
    };

    public void setDataHandler(BaseDataHandler baseDataHandler) {
        this.dataHandler = baseDataHandler;
    }

    public void setmOperationInProgress(boolean z) {
        this.mOperationInProgress = z;
    }

    public void setGatt(BluetoothGatt bluetoothGatt) {
        this.gatt = bluetoothGatt;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void enqueue(int r7, byte[] r8, com.wade.fit.util.ble.BleCallback r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            com.wade.fit.util.ble.BleManager r0 = com.wade.fit.util.ble.BleManager.getInstance()     // Catch:{ all -> 0x003f }
            boolean r0 = r0.isDeviceConnected()     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x0038
            android.bluetooth.BluetoothGatt r0 = r6.gatt     // Catch:{ all -> 0x003f }
            if (r0 != 0) goto L_0x0015
            java.lang.String r7 = "mBluetoothGatt is null!"
            com.wade.fit.util.ble.BleClient.showMessage(r7)     // Catch:{ all -> 0x003f }
            goto L_0x003d
        L_0x0015:
            android.bluetooth.BluetoothGattCharacteristic r0 = r6.mWriteNormalGattCharacteristic     // Catch:{ all -> 0x003f }
            if (r0 != 0) goto L_0x002c
            android.bluetooth.BluetoothGatt r0 = r6.gatt     // Catch:{ all -> 0x003f }
            android.bluetooth.BluetoothGattCharacteristic r0 = com.wade.fit.util.ble.BleGattAttributes.getNormalWriteCharacteristic(r0)     // Catch:{ all -> 0x003f }
            r6.mWriteNormalGattCharacteristic = r0     // Catch:{ all -> 0x003f }
            android.bluetooth.BluetoothGattCharacteristic r0 = r6.mWriteNormalGattCharacteristic     // Catch:{ all -> 0x003f }
            if (r0 != 0) goto L_0x002c
            java.lang.String r7 = "mWriteNormalGattCharacteristic is nul"
            com.wade.fit.util.ble.BleClient.showMessage(r7)     // Catch:{ all -> 0x003f }
            monitor-exit(r6)
            return
        L_0x002c:
            android.bluetooth.BluetoothGatt r2 = r6.gatt     // Catch:{ all -> 0x003f }
            android.bluetooth.BluetoothGattCharacteristic r3 = r6.mWriteNormalGattCharacteristic     // Catch:{ all -> 0x003f }
            r0 = r6
            r1 = r7
            r4 = r8
            r5 = r9
            r0.enqueue(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x003f }
            goto L_0x003d
        L_0x0038:
            java.lang.String r7 = "设备未连接...."
            com.wade.fit.util.log.LogUtil.m5268e(r7)     // Catch:{ all -> 0x003f }
        L_0x003d:
            monitor-exit(r6)
            return
        L_0x003f:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.ble.SendDataTask.enqueue(int, byte[], com.wade.fit.util.ble.BleCallback):void");
    }

    private synchronized void enqueue(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback) {
        enqueue(0, bluetoothGatt, bluetoothGattCharacteristic, bArr, bleCallback);
    }

    private synchronized void enqueue(int i, BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback) {
        if (bluetoothGattCharacteristic != null) {
            try {
                if ((bluetoothGattCharacteristic.getProperties() | 8) > 0) {
                    BleClient.showMessage("enqueue [" + ByteDataConvertUtil.bytesToHexString(bArr) + "]");
                    enqueue(i, Request.newWriteRequest(bluetoothGattCharacteristic, bArr, bleCallback, i));
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        if (bluetoothGattCharacteristic != null) {
            BleClient.showMessage("发送数据失败 , c.getProperties():" + bluetoothGattCharacteristic.getProperties());
        } else {
            BleClient.showMessage("发送数据失败 , c is null");
        }
    }

    public synchronized boolean enqueue(int i, Request request) {
        if (request == null) {
            return false;
        }
        BleClient.showMessage("enqueue，flag:" + i + ",request.flag:" + request.flag + ",isWaitComplete:" + this.isWaitComplete);
        int i2 = request.flag;
        this.isDelaySend = false;
        if (i2 == 0) {
            BleClient.showMessage("正常命令，isOrder:" + this.isOrder);
            if (this.isOrder) {
                BleClient.showMessage("连续命令还未发送完成，放入缓存 size:" + this.cacheTaskQueue.size());
                this.cacheTaskQueue.add(request);
            } else {
                this.mTaskQueue.add(request);
                BleClient.showMessage("添加了1条消息,消息队列里面有<" + this.mTaskQueue.size() + ">条消息等待发送 mOperationInProgress:" + this.mOperationInProgress);
            }
        } else if (i2 == 1) {
            this.isDelaySend = true;
            BleClient.showMessage("发现连续命令，开始");
            this.isOrder = true;
            this.mTaskQueue.add(request);
            request.isNwxtDelaySend = true;
        } else if (i2 == 2) {
            BleClient.showMessage("发现连续命令，继续");
            this.mTaskQueue.add(request);
            this.isDelaySend = true;
            request.isNwxtDelaySend = true;
        } else if (i2 == 3) {
            BleClient.showMessage("发现连续命令，结束");
            this.isOrder = false;
            this.isDelaySend = false;
            this.mTaskQueue.add(request);
            cacheToActiveTastQueue();
        } else if (i2 == 4) {
            this.isWaitComplete = true;
            this.mTaskQueue.add(request);
        } else if (i2 == 200) {
            this.mTaskQueue.add(request);
        }
        nextRequest();
        return true;
    }

    public void nextRequest() {
        Request request;
        BleClient.showMessage("...mOperationInProgress:" + this.mOperationInProgress + ",size:" + this.mTaskQueue.size());
        if (!this.mOperationInProgress && this.mTaskQueue.size() != 0) {
            try {
                request = this.mTaskQueue.poll();
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                request = null;
            }
            if (request != null) {
                if (request.flag == 0 || request.flag == 3 || request.flag == 200) {
                    this.sendQueue.add(request);
                }
                this.mOperationInProgress = true;
                boolean sendData = sendData(request);
                BleClient.showMessage("result:" + sendData + ",sendQueue.size:" + this.sendQueue.size());
                if (!sendData) {
                    this.mOperationInProgress = false;
                    if (request.callback != null) {
                        request.callback.complete(0, null);
                    }
                    nextRequest();
                }
            }
        }
    }

    private boolean sendData(Request request) {
        if (this.gatt == null) {
            LogUtil.d("sendData....mBluetoothGatt is null");
            clearTaskQueue();
            return false;
        }
        BluetoothGattCharacteristic bluetoothGattCharacteristic = request.characteristic;
        if (bluetoothGattCharacteristic == null) {
            LogUtil.d("sendData....characteristic is null");
            clearTaskQueue();
            return false;
        }
        bluetoothGattCharacteristic.setValue(request.value);
        bluetoothGattCharacteristic.setWriteType(bluetoothGattCharacteristic.getWriteType());
        LogUtil.d("发送数据 writeCharacteristic....");
        try {
            return this.gatt.writeCharacteristic(bluetoothGattCharacteristic);
        } catch (Exception e) {
            e.printStackTrace();
            BleClient.showMessage("writeCharacteristic 发送异常....断线重连");
            return false;
        }
    }

    public void clearTaskQueue() {
        this.mTaskQueue.clear();
        this.sendQueue.clear();
        this.cacheTaskQueue.clear();
        this.mOperationInProgress = false;
    }

    public void cacheToActiveTastQueue() {
        if (!this.cacheTaskQueue.isEmpty()) {
            while (true) {
                Request poll = this.cacheTaskQueue.poll();
                if (poll != null) {
                    this.mTaskQueue.add(poll);
                } else {
                    return;
                }
            }
        }
    }

    public void handleTaskQueue() {
        if (this.mTaskQueue.size() > 10) {
            BleClient.showMessage("发送队列数量超限，强制移除旧命令，保留最后2个命令");
            for (int i = 0; i < 8; i++) {
                this.mTaskQueue.pollFirst();
            }
        }
    }

    public void sendBleSuccess() {
        BleClient.showMessage("发送消息成功 ,isDelaySend:" + this.isDelaySend + ", 还有[" + this.mTaskQueue.size() + "]条消息未发送");
        handleTaskQueue();
        this.mOperationInProgress = false;
        if (this.isDelaySend) {
            ThreadUtil.delayTask(new Runnable() {
                /* class com.wade.fit.util.ble.SendDataTask.C26882 */

                public void run() {
                    SendDataTask.this.nextRequest();
                }
            }, 500);
        } else {
            nextRequest();
        }
    }

    public void reciverBleData(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        HandlerBleDataResult handlerBleDataResult;
        byte[] bArr = new byte[20];
        BaseCmdUtil.copy(bluetoothGattCharacteristic.getValue(), bArr);
        BleClient.showMessage("接收到手环数据===" + ByteDataConvertUtil.bytesToHexString(bArr));
        Object handler2 = this.dataHandler.handler(bArr);
        Request poll = this.sendQueue.poll();
        LogUtil.m5264d(handler2 == null ? "result is null" : handler2);
        LogUtil.m5264d(poll == null ? "request is null" : poll);
        if (poll != null) {
            LogUtil.d("flag:" + poll.flag);
            if (poll.flag == 200) {
                this.cacheRequst = poll;
            } else if (poll.flag == 3) {
                if (handler2 == null) {
                    handlerBleDataResult = new HandlerBleDataResult();
                } else {
                    handlerBleDataResult = handler2 instanceof HandlerBleDataResult ? (HandlerBleDataResult) handler2 : null;
                }
                if (handlerBleDataResult != null) {
                    handlerBleDataResult.isComplete = true;
                    callback(poll, handlerBleDataResult);
                    return;
                }
            }
        }
        if (!(handler2 instanceof HandlerBleDataResult)) {
            callback(poll, handler2);
        } else if (((HandlerBleDataResult) handler2).isComplete) {
            callback(this.cacheRequst, handler2);
            this.cacheRequst = null;
        } else {
            callback(this.cacheRequst, handler2);
        }
    }

    private void callback(final Request request, final Object obj) {
        if (request != null && request.callback != null) {
            ThreadUtil.runOnMainThread(new Runnable() {
                /* class com.wade.fit.util.ble.SendDataTask.C26893 */

                public void run() {
                    request.callback.complete(1, obj);
                }
            });
        }
    }
}
