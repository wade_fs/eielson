package com.wade.fit.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.util.Set;

public class CommonPreferences {
    protected SharedPreferences mSharePre;

    public void init(Context context, String str) {
        Log.i("CommonPreferences", "context  = :" + context + "   spName : " + str);
        this.mSharePre = context.getSharedPreferences(str, 0);
    }

    public boolean remove(String str) {
        return this.mSharePre.edit().remove(str).commit();
    }

    /* access modifiers changed from: protected */
    public float getValue(String str, float f) {
        return this.mSharePre.getFloat(str, f);
    }

    /* access modifiers changed from: protected */
    public int getValue(String str, int i) {
        return this.mSharePre.getInt(str, i);
    }

    /* access modifiers changed from: protected */
    public long getValue(String str, long j) {
        return this.mSharePre.getLong(str, j);
    }

    /* access modifiers changed from: protected */
    public String getValue(String str, String str2) {
        return this.mSharePre.getString(str, str2);
    }

    /* access modifiers changed from: protected */
    public Set<String> getValue(String str, Set<String> set) {
        return this.mSharePre.getStringSet(str, set);
    }

    /* access modifiers changed from: protected */
    public boolean getValue(String str, boolean z) {
        return this.mSharePre.getBoolean(str, z);
    }

    /* access modifiers changed from: protected */
    public void setValue(String str, float f) {
        this.mSharePre.edit().putFloat(str, f).commit();
    }

    /* access modifiers changed from: protected */
    public void setValue(String str, int i) {
        this.mSharePre.edit().putInt(str, i).commit();
    }

    /* access modifiers changed from: protected */
    public void setValue(String str, long j) {
        this.mSharePre.edit().putLong(str, j).commit();
    }

    /* access modifiers changed from: protected */
    public void setValue(String str, String str2) {
        this.mSharePre.edit().putString(str, str2).commit();
    }

    /* access modifiers changed from: protected */
    public void setValue(String str, Set<String> set) {
        this.mSharePre.edit().putStringSet(str, set).commit();
    }

    /* access modifiers changed from: protected */
    public void setValue(String str, boolean z) {
        this.mSharePre.edit().putBoolean(str, z).commit();
    }
}
