package com.wade.fit.task;

import com.wade.fit.util.ThreadUtil;
import com.wade.fit.util.log.LogUtil;
import java.util.concurrent.atomic.AtomicInteger;

public class RunTask<T> implements Runnable, RunnableComplete {
    private T data;
    public AtomicInteger mCount = new AtomicInteger(0);

    public void onPreExecute(Object obj) {
    }

    public RunTask() {
    }

    public RunTask(T t) {
        this.data = t;
    }

    public void setData(T t) {
        this.data = t;
    }

    public /* synthetic */ void lambda$run$0$RunTask() {
        onPreExecute(this.data);
    }

    public void run() {
        ThreadUtil.runOnMainThread(new Runnable() {
            /* class com.wade.fit.task.$$Lambda$RunTask$tiLa7msxWgNsVUl0dCoqwKBlWyg */

            public final void run() {
                RunTask.this.lambda$run$0$RunTask();
            }
        });
    }

    public final boolean isExeRunFinish() {
        return this.mCount.compareAndSet(1, 1);
    }

    public final void runFinish() {
        LogUtil.d("runFinish ....");
        this.mCount.incrementAndGet();
    }
}
