package com.wade.fit.util.file;

import com.wade.fit.util.ConvertUtil;
import com.wade.fit.util.EncryptUtil;
import com.wade.fit.util.StringUtils;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileUtil {
    private FileUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x005b A[SYNTHETIC, Splitter:B:35:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0065 A[SYNTHETIC, Splitter:B:40:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0071 A[SYNTHETIC, Splitter:B:46:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x007b A[SYNTHETIC, Splitter:B:51:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:59:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void fileCopy(java.io.File r13, java.io.File r14) {
        /*
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
            r1.<init>(r13)     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
            java.nio.channels.FileChannel r13 = r1.getChannel()     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x004b, all -> 0x0045 }
            r1.<init>(r14)     // Catch:{ Exception -> 0x004b, all -> 0x0045 }
            java.nio.channels.FileChannel r14 = r1.getChannel()     // Catch:{ Exception -> 0x004b, all -> 0x0045 }
            r0 = 67076096(0x3ff8000, float:1.501694E-36)
            long r8 = r13.size()     // Catch:{ Exception -> 0x0040, all -> 0x003b }
            r1 = 0
            r10 = r1
        L_0x001d:
            int r1 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
            if (r1 >= 0) goto L_0x002b
            long r5 = (long) r0     // Catch:{ Exception -> 0x0040, all -> 0x003b }
            r2 = r13
            r3 = r10
            r7 = r14
            long r1 = r2.transferTo(r3, r5, r7)     // Catch:{ Exception -> 0x0040, all -> 0x003b }
            long r10 = r10 + r1
            goto L_0x001d
        L_0x002b:
            if (r13 == 0) goto L_0x0035
            r13.close()     // Catch:{ IOException -> 0x0031 }
            goto L_0x0035
        L_0x0031:
            r13 = move-exception
            r13.printStackTrace()
        L_0x0035:
            if (r14 == 0) goto L_0x006d
            r14.close()     // Catch:{ IOException -> 0x0069 }
            goto L_0x006d
        L_0x003b:
            r0 = move-exception
            r12 = r0
            r0 = r13
            r13 = r12
            goto L_0x006f
        L_0x0040:
            r0 = move-exception
            r12 = r0
            r0 = r13
            r13 = r12
            goto L_0x0056
        L_0x0045:
            r14 = move-exception
            r12 = r0
            r0 = r13
            r13 = r14
            r14 = r12
            goto L_0x006f
        L_0x004b:
            r14 = move-exception
            r12 = r0
            r0 = r13
            r13 = r14
            r14 = r12
            goto L_0x0056
        L_0x0051:
            r13 = move-exception
            r14 = r0
            goto L_0x006f
        L_0x0054:
            r13 = move-exception
            r14 = r0
        L_0x0056:
            r13.printStackTrace()     // Catch:{ all -> 0x006e }
            if (r0 == 0) goto L_0x0063
            r0.close()     // Catch:{ IOException -> 0x005f }
            goto L_0x0063
        L_0x005f:
            r13 = move-exception
            r13.printStackTrace()
        L_0x0063:
            if (r14 == 0) goto L_0x006d
            r14.close()     // Catch:{ IOException -> 0x0069 }
            goto L_0x006d
        L_0x0069:
            r13 = move-exception
            r13.printStackTrace()
        L_0x006d:
            return
        L_0x006e:
            r13 = move-exception
        L_0x006f:
            if (r0 == 0) goto L_0x0079
            r0.close()     // Catch:{ IOException -> 0x0075 }
            goto L_0x0079
        L_0x0075:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0079:
            if (r14 == 0) goto L_0x0083
            r14.close()     // Catch:{ IOException -> 0x007f }
            goto L_0x0083
        L_0x007f:
            r14 = move-exception
            r14.printStackTrace()
        L_0x0083:
            goto L_0x0085
        L_0x0084:
            throw r13
        L_0x0085:
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.file.FileUtil.fileCopy(java.io.File, java.io.File):void");
    }

    public static boolean fileExists(String str) {
        return new File(str).exists();
    }

    public static File getFileByPath(String str) {
        if (StringUtils.isSpace(str)) {
            return null;
        }
        return new File(str);
    }

    public static boolean isFileExists(String str) {
        return isFileExists(getFileByPath(str));
    }

    public static boolean isFileExists(File file) {
        return file != null && file.exists();
    }

    public static boolean isDir(String str) {
        return isDir(getFileByPath(str));
    }

    public static boolean isDir(File file) {
        return isFileExists(file) && file.isDirectory();
    }

    public static boolean isFile(String str) {
        return isFile(getFileByPath(str));
    }

    public static boolean isFile(File file) {
        return isFileExists(file) && file.isFile();
    }

    public static boolean createOrExistsDir(String str) {
        return createOrExistsDir(getFileByPath(str));
    }

    public static boolean createOrExistsDir(File file) {
        return file != null && (!file.exists() ? file.mkdirs() : file.isDirectory());
    }

    public static boolean createOrExistsFile(String str) {
        return createOrExistsFile(getFileByPath(str));
    }

    public static boolean createOrExistsFile(File file) {
        if (file == null) {
            return false;
        }
        if (file.exists()) {
            return file.isFile();
        }
        if (!createOrExistsDir(file.getParentFile())) {
            return false;
        }
        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean createFileByDeleteOldFile(String str) {
        return createFileByDeleteOldFile(getFileByPath(str));
    }

    public static boolean createFileByDeleteOldFile(File file) {
        if (file == null) {
            return false;
        }
        if ((file.exists() && file.isFile() && !file.delete()) || !createOrExistsDir(file.getParentFile())) {
            return false;
        }
        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean copyOrMoveDir(String str, String str2, boolean z) {
        return copyOrMoveDir(getFileByPath(str), getFileByPath(str2), z);
    }

    private static boolean copyOrMoveDir(File file, File file2, boolean z) {
        if (file == null || file2 == null) {
            return false;
        }
        String str = file2.getPath() + File.separator;
        if (str.contains(file.getPath() + File.separator) || !file.exists() || !file.isDirectory() || !createOrExistsDir(file2)) {
            return false;
        }
        File[] listFiles = file.listFiles();
        for (File file3 : listFiles) {
            File file4 = new File(str + file3.getName());
            if (file3.isFile()) {
                if (!copyOrMoveFile(file3, file4, z)) {
                    return false;
                }
            } else if (file3.isDirectory() && !copyOrMoveDir(file3, file4, z)) {
                return false;
            }
        }
        if (!z || deleteDir(file)) {
            return true;
        }
        return false;
    }

    private static boolean copyOrMoveFile(String str, String str2, boolean z) {
        return copyOrMoveFile(getFileByPath(str), getFileByPath(str2), z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.file.FileUtil.write(java.io.File, java.io.InputStream, boolean):boolean
     arg types: [java.io.File, java.io.FileInputStream, int]
     candidates:
      com.wade.fit.util.file.FileUtil.write(java.lang.String, java.io.InputStream, boolean):boolean
      com.wade.fit.util.file.FileUtil.write(java.lang.String, java.lang.String, boolean):boolean
      com.wade.fit.util.file.FileUtil.write(java.io.File, java.io.InputStream, boolean):boolean */
    private static boolean copyOrMoveFile(File file, File file2, boolean z) {
        if (file != null && file2 != null && file.exists() && file.isFile()) {
            if ((file2.exists() && file2.isFile()) || !createOrExistsDir(file2.getParentFile())) {
                return false;
            }
            try {
                if (!write(file2, (InputStream) new FileInputStream(file), false)) {
                    return false;
                }
                if (!z || deleteFile(file)) {
                    return true;
                }
                return false;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static boolean copyDir(String str, String str2) {
        return copyDir(getFileByPath(str), getFileByPath(str2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.file.FileUtil.copyOrMoveDir(java.io.File, java.io.File, boolean):boolean
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.wade.fit.util.file.FileUtil.copyOrMoveDir(java.lang.String, java.lang.String, boolean):boolean
      com.wade.fit.util.file.FileUtil.copyOrMoveDir(java.io.File, java.io.File, boolean):boolean */
    public static boolean copyDir(File file, File file2) {
        return copyOrMoveDir(file, file2, false);
    }

    public static boolean copyFile(String str, String str2) {
        return copyFile(getFileByPath(str), getFileByPath(str2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.file.FileUtil.copyOrMoveFile(java.io.File, java.io.File, boolean):boolean
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.wade.fit.util.file.FileUtil.copyOrMoveFile(java.lang.String, java.lang.String, boolean):boolean
      com.wade.fit.util.file.FileUtil.copyOrMoveFile(java.io.File, java.io.File, boolean):boolean */
    public static boolean copyFile(File file, File file2) {
        return copyOrMoveFile(file, file2, false);
    }

    public static boolean moveDir(String str, String str2) {
        return moveDir(getFileByPath(str), getFileByPath(str2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.file.FileUtil.copyOrMoveDir(java.io.File, java.io.File, boolean):boolean
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.wade.fit.util.file.FileUtil.copyOrMoveDir(java.lang.String, java.lang.String, boolean):boolean
      com.wade.fit.util.file.FileUtil.copyOrMoveDir(java.io.File, java.io.File, boolean):boolean */
    public static boolean moveDir(File file, File file2) {
        return copyOrMoveDir(file, file2, true);
    }

    public static boolean moveFile(String str, String str2) {
        return moveFile(getFileByPath(str), getFileByPath(str2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.file.FileUtil.copyOrMoveFile(java.io.File, java.io.File, boolean):boolean
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.wade.fit.util.file.FileUtil.copyOrMoveFile(java.lang.String, java.lang.String, boolean):boolean
      com.wade.fit.util.file.FileUtil.copyOrMoveFile(java.io.File, java.io.File, boolean):boolean */
    public static boolean moveFile(File file, File file2) {
        return copyOrMoveFile(file, file2, true);
    }

    public static boolean deleteDir(String str) {
        return deleteDir(getFileByPath(str));
    }

    public static boolean deleteDir(File file) {
        if (file == null) {
            return false;
        }
        if (!file.exists()) {
            return true;
        }
        if (!file.isDirectory()) {
            return false;
        }
        File[] listFiles = file.listFiles();
        for (File file2 : listFiles) {
            if (file2.isFile()) {
                if (!deleteFile(file2)) {
                    return false;
                }
            } else if (file2.isDirectory() && !deleteDir(file2)) {
                return false;
            }
        }
        return file.delete();
    }

    public static boolean deleteFile(String str) {
        return deleteFile(getFileByPath(str));
    }

    public static boolean deleteFile(File file) {
        return file != null && (!file.exists() || (file.isFile() && file.delete()));
    }

    public static List<File> listFilesInDir(String str, boolean z) {
        return listFilesInDir(getFileByPath(str), z);
    }

    public static List<File> listFilesInDir(File file, boolean z) {
        if (z) {
            return listFilesInDir(file);
        }
        if (file == null || !isDir(file)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Collections.addAll(arrayList, file.listFiles());
        return arrayList;
    }

    public static List<File> listFilesInDir(String str) {
        return listFilesInDir(getFileByPath(str));
    }

    public static List<File> listFilesInDir(File file) {
        if (file == null || !isDir(file)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        for (File file2 : listFiles) {
            arrayList.add(file2);
            if (file2.isDirectory()) {
                arrayList.addAll(listFilesInDir(file2));
            }
        }
        return arrayList;
    }

    public static List<File> listFilesInDirWithFilter(String str, String str2, boolean z) {
        return listFilesInDirWithFilter(getFileByPath(str), str2, z);
    }

    public static List<File> listFilesInDirWithFilter(File file, String str, boolean z) {
        if (z) {
            return listFilesInDirWithFilter(file, str);
        }
        if (file == null || !isDir(file)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        for (File file2 : listFiles) {
            if (file2.getName().toUpperCase().endsWith(str.toUpperCase())) {
                arrayList.add(file2);
            }
        }
        return arrayList;
    }

    public static List<File> listFilesInDirWithFilter(String str, String str2) {
        return listFilesInDirWithFilter(getFileByPath(str), str2);
    }

    public static List<File> listFilesInDirWithFilter(File file, String str) {
        if (file == null || !isDir(file)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        for (File file2 : listFiles) {
            if (file2.getName().toUpperCase().endsWith(str.toUpperCase())) {
                arrayList.add(file2);
            }
            if (file2.isDirectory()) {
                arrayList.addAll(listFilesInDirWithFilter(file2, str));
            }
        }
        return arrayList;
    }

    public static List<File> listFilesInDirWithFilter(String str, FilenameFilter filenameFilter, boolean z) {
        return listFilesInDirWithFilter(getFileByPath(str), filenameFilter, z);
    }

    public static List<File> listFilesInDirWithFilter(File file, FilenameFilter filenameFilter, boolean z) {
        if (z) {
            return listFilesInDirWithFilter(file, filenameFilter);
        }
        if (file == null || !isDir(file)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        for (File file2 : listFiles) {
            if (filenameFilter.accept(file2.getParentFile(), file2.getName())) {
                arrayList.add(file2);
            }
        }
        return arrayList;
    }

    public static List<File> listFilesInDirWithFilter(String str, FilenameFilter filenameFilter) {
        return listFilesInDirWithFilter(getFileByPath(str), filenameFilter);
    }

    public static List<File> listFilesInDirWithFilter(File file, FilenameFilter filenameFilter) {
        if (file == null || !isDir(file)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        for (File file2 : listFiles) {
            if (filenameFilter.accept(file2.getParentFile(), file2.getName())) {
                arrayList.add(file2);
            }
            if (file2.isDirectory()) {
                arrayList.addAll(listFilesInDirWithFilter(file2, filenameFilter));
            }
        }
        return arrayList;
    }

    public static List<File> searchFileInDir(String str, String str2) {
        return searchFileInDir(getFileByPath(str), str2);
    }

    public static List<File> searchFileInDir(File file, String str) {
        if (file == null || !isDir(file)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        for (File file2 : listFiles) {
            if (file2.getName().toUpperCase().equals(str.toUpperCase())) {
                arrayList.add(file2);
            }
            if (file2.isDirectory()) {
                arrayList.addAll(listFilesInDirWithFilter(file2, str));
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.file.FileUtil.write(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.wade.fit.util.file.FileUtil.write(java.io.File, java.io.InputStream, boolean):boolean
      com.wade.fit.util.file.FileUtil.write(java.lang.String, java.io.InputStream, boolean):boolean
      com.wade.fit.util.file.FileUtil.write(java.lang.String, java.lang.String, boolean):boolean */
    public static boolean write(String str, String str2) {
        return write(str, str2, true);
    }

    public static boolean write(String str, String str2, boolean z) {
        BufferedWriter bufferedWriter = null;
        try {
            if (!isFileExists(str)) {
                closeIO(null);
                return false;
            }
            BufferedWriter bufferedWriter2 = new BufferedWriter(new FileWriter(str, z));
            try {
                bufferedWriter2.write(str2);
                bufferedWriter2.newLine();
                bufferedWriter2.flush();
                closeIO(bufferedWriter2);
                return true;
            } catch (Exception unused) {
                bufferedWriter = bufferedWriter2;
                closeIO(bufferedWriter);
                return false;
            } catch (Throwable th) {
                th = th;
                bufferedWriter = bufferedWriter2;
                closeIO(bufferedWriter);
                throw th;
            }
        } catch (Exception unused2) {
            closeIO(bufferedWriter);
            return false;
        } catch (Throwable th2) {
            th = th2;
            closeIO(bufferedWriter);
            throw th;
        }
    }

    public static boolean write(String str, InputStream inputStream, boolean z) {
        return write(getFileByPath(str), inputStream, z);
    }

    public static boolean write(File file, InputStream inputStream, boolean z) {
        if (file == null || inputStream == null || !createOrExistsFile(file)) {
            return false;
        }
        BufferedOutputStream bufferedOutputStream = null;
        try {
            BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(new FileOutputStream(file, z));
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr, 0, 1024);
                    if (read != -1) {
                        bufferedOutputStream2.write(bArr, 0, read);
                    } else {
                        closeIO(inputStream, bufferedOutputStream2);
                        return true;
                    }
                }
            } catch (IOException e) {
                e = e;
                bufferedOutputStream = bufferedOutputStream2;
                try {
                    e.printStackTrace();
                    closeIO(inputStream, bufferedOutputStream);
                    return false;
                } catch (Throwable th) {
                    th = th;
                    closeIO(inputStream, bufferedOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedOutputStream = bufferedOutputStream2;
                closeIO(inputStream, bufferedOutputStream);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            e.printStackTrace();
            closeIO(inputStream, bufferedOutputStream);
            return false;
        }
    }

    public static String read(String str) {
        BufferedReader bufferedReader;
        StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader2 = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(str));
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        stringBuffer.append(readLine);
                    } else {
                        closeIO(bufferedReader);
                        return stringBuffer.toString();
                    }
                } catch (Exception e) {
                    e = e;
                    try {
                        e.printStackTrace();
                        closeIO(bufferedReader);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        bufferedReader2 = bufferedReader;
                        closeIO(bufferedReader2);
                        throw th;
                    }
                }
            }
        } catch (Exception e2) {
            e = e2;
            bufferedReader = null;
            e.printStackTrace();
            closeIO(bufferedReader);
            return null;
        } catch (Throwable th2) {
            th = th2;
            closeIO(bufferedReader2);
            throw th;
        }
    }

    public static String read(String str, String str2) {
        return read(getFileByPath(str), str2);
    }

    public static String read(File file, String str) {
        BufferedReader bufferedReader;
        BufferedReader bufferedReader2 = null;
        if (file == null) {
            return null;
        }
        try {
            StringBuilder sb = new StringBuilder();
            if (StringUtils.isSpace(str)) {
                bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            } else {
                bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), str));
            }
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        sb.append(readLine);
                        sb.append("\r\n");
                    } else {
                        String sb2 = sb.delete(sb.length() - 2, sb.length()).toString();
                        closeIO(bufferedReader);
                        return sb2;
                    }
                } catch (IOException e) {
                    e = e;
                    try {
                        e.printStackTrace();
                        closeIO(bufferedReader);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        bufferedReader2 = bufferedReader;
                        closeIO(bufferedReader2);
                        throw th;
                    }
                }
            }
        } catch (IOException e2) {
            e = e2;
            bufferedReader = null;
            e.printStackTrace();
            closeIO(bufferedReader);
            return null;
        } catch (Throwable th2) {
            th = th2;
            closeIO(bufferedReader2);
            throw th;
        }
    }

    public static List<String> readFile2List(String str, String str2) {
        return readFile2List(getFileByPath(str), str2);
    }

    public static List<String> readFile2List(File file, String str) {
        return readFile2List(file, 0, Integer.MAX_VALUE, str);
    }

    public static List<String> readFile2List(String str, int i, int i2, String str2) {
        return readFile2List(getFileByPath(str), i, i2, str2);
    }

    public static List<String> readFile2List(File file, int i, int i2, String str) {
        BufferedReader bufferedReader;
        BufferedReader bufferedReader2 = null;
        if (file == null || i > i2) {
            return null;
        }
        try {
            ArrayList arrayList = new ArrayList();
            if (StringUtils.isSpace(str)) {
                bufferedReader = new BufferedReader(new FileReader(file));
            } else {
                bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), str));
            }
            int i3 = 1;
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    } else if (i3 > i2) {
                        break;
                    } else {
                        if (i <= i3 && i3 <= i2) {
                            arrayList.add(readLine);
                        }
                        i3++;
                    }
                } catch (IOException e) {
                    e = e;
                    try {
                        e.printStackTrace();
                        closeIO(bufferedReader);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        bufferedReader2 = bufferedReader;
                        closeIO(bufferedReader2);
                        throw th;
                    }
                }
            }
            closeIO(bufferedReader);
            return arrayList;
        } catch (IOException e2) {
            e = e2;
            bufferedReader = null;
            e.printStackTrace();
            closeIO(bufferedReader);
            return null;
        } catch (Throwable th2) {
            th = th2;
            closeIO(bufferedReader2);
            throw th;
        }
    }

    public static byte[] readFile2Bytes(String str) {
        return readFile2Bytes(getFileByPath(str));
    }

    public static byte[] readFile2Bytes(File file) {
        if (file == null) {
            return null;
        }
        try {
            return ConvertUtil.inputStream2Bytes(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getFileCharsetSimple(String str) {
        return getFileCharsetSimple(getFileByPath(str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004c A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getFileCharsetSimple(java.io.File r5) {
        /*
            r0 = 1
            r1 = 0
            r2 = 0
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0028 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0028 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0028 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0028 }
            int r5 = r3.read()     // Catch:{ IOException -> 0x0023, all -> 0x0020 }
            int r5 = r5 << 8
            int r2 = r3.read()     // Catch:{ IOException -> 0x0023, all -> 0x0020 }
            int r5 = r5 + r2
            java.io.Closeable[] r0 = new java.io.Closeable[r0]
            r0[r1] = r3
            closeIO(r0)
            goto L_0x0034
        L_0x0020:
            r5 = move-exception
            r2 = r3
            goto L_0x004f
        L_0x0023:
            r5 = move-exception
            r2 = r3
            goto L_0x0029
        L_0x0026:
            r5 = move-exception
            goto L_0x004f
        L_0x0028:
            r5 = move-exception
        L_0x0029:
            r5.printStackTrace()     // Catch:{ all -> 0x0026 }
            java.io.Closeable[] r5 = new java.io.Closeable[r0]
            r5[r1] = r2
            closeIO(r5)
            r5 = 0
        L_0x0034:
            r0 = 61371(0xefbb, float:8.5999E-41)
            if (r5 == r0) goto L_0x004c
            r0 = 65279(0xfeff, float:9.1475E-41)
            if (r5 == r0) goto L_0x0049
            r0 = 65534(0xfffe, float:9.1833E-41)
            if (r5 == r0) goto L_0x0046
            java.lang.String r5 = "GBK"
            return r5
        L_0x0046:
            java.lang.String r5 = "Unicode"
            return r5
        L_0x0049:
            java.lang.String r5 = "UTF-16BE"
            return r5
        L_0x004c:
            java.lang.String r5 = "UTF-8"
            return r5
        L_0x004f:
            java.io.Closeable[] r0 = new java.io.Closeable[r0]
            r0[r1] = r2
            closeIO(r0)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.file.FileUtil.getFileCharsetSimple(java.io.File):java.lang.String");
    }

    public static int getFileLines(String str) {
        return getFileLines(getFileByPath(str));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0028, code lost:
        r9 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0029, code lost:
        r2 = r3;
        r4 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0036, code lost:
        r9 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0037, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0039, code lost:
        r9 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003a, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0039 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:4:0x000f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int getFileLines(java.io.File r9) {
        /*
            r0 = 0
            r1 = 1
            r2 = 0
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0041 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0041 }
            r4.<init>(r9)     // Catch:{ IOException -> 0x0041 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0041 }
            r9 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r9]     // Catch:{ IOException -> 0x003c, all -> 0x0039 }
            r4 = 1
        L_0x0012:
            int r5 = r3.read(r2, r0, r9)     // Catch:{ IOException -> 0x0036, all -> 0x0039 }
            r6 = -1
            if (r5 == r6) goto L_0x002e
            r6 = r4
            r4 = 0
        L_0x001b:
            if (r4 >= r5) goto L_0x002c
            byte r7 = r2[r4]     // Catch:{ IOException -> 0x0028, all -> 0x0039 }
            r8 = 10
            if (r7 != r8) goto L_0x0025
            int r6 = r6 + 1
        L_0x0025:
            int r4 = r4 + 1
            goto L_0x001b
        L_0x0028:
            r9 = move-exception
            r2 = r3
            r4 = r6
            goto L_0x0043
        L_0x002c:
            r4 = r6
            goto L_0x0012
        L_0x002e:
            java.io.Closeable[] r9 = new java.io.Closeable[r1]
            r9[r0] = r3
            closeIO(r9)
            goto L_0x004d
        L_0x0036:
            r9 = move-exception
            r2 = r3
            goto L_0x0043
        L_0x0039:
            r9 = move-exception
            r2 = r3
            goto L_0x004e
        L_0x003c:
            r9 = move-exception
            r2 = r3
            goto L_0x0042
        L_0x003f:
            r9 = move-exception
            goto L_0x004e
        L_0x0041:
            r9 = move-exception
        L_0x0042:
            r4 = 1
        L_0x0043:
            r9.printStackTrace()     // Catch:{ all -> 0x003f }
            java.io.Closeable[] r9 = new java.io.Closeable[r1]
            r9[r0] = r2
            closeIO(r9)
        L_0x004d:
            return r4
        L_0x004e:
            java.io.Closeable[] r1 = new java.io.Closeable[r1]
            r1[r0] = r2
            closeIO(r1)
            goto L_0x0057
        L_0x0056:
            throw r9
        L_0x0057:
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.file.FileUtil.getFileLines(java.io.File):int");
    }

    public static String getFileSize(String str) {
        return getFileSize(getFileByPath(str));
    }

    public static String getFileSize(File file) {
        if (!isFileExists(file)) {
            return "";
        }
        return ConvertUtil.byte2FitSize(file.length());
    }

    public static String getFileMD5(String str) {
        return getFileMD5(getFileByPath(str));
    }

    public static String getFileMD5(File file) {
        return EncryptUtil.encryptMD5File2String(file);
    }

    public static void closeIO(Closeable... closeableArr) {
        if (closeableArr != null) {
            try {
                for (Closeable closeable : closeableArr) {
                    if (closeable != null) {
                        closeable.close();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getDirName(File file) {
        if (file == null) {
            return null;
        }
        return getDirName(file.getPath());
    }

    public static String getDirName(String str) {
        if (StringUtils.isSpace(str)) {
            return str;
        }
        int lastIndexOf = str.lastIndexOf(File.separator);
        if (lastIndexOf == -1) {
            return "";
        }
        return str.substring(0, lastIndexOf + 1);
    }

    public static String getFileName(File file) {
        if (file == null) {
            return null;
        }
        return getFileName(file.getPath());
    }

    public static String getFileName(String str) {
        int lastIndexOf;
        return (!StringUtils.isSpace(str) && (lastIndexOf = str.lastIndexOf(File.separator)) != -1) ? str.substring(lastIndexOf + 1) : str;
    }

    public static String getFileNameNoExtension(File file) {
        if (file == null) {
            return null;
        }
        return getFileNameNoExtension(file.getPath());
    }

    public static String getFileNameNoExtension(String str) {
        if (StringUtils.isSpace(str)) {
            return str;
        }
        int lastIndexOf = str.lastIndexOf(46);
        int lastIndexOf2 = str.lastIndexOf(File.separator);
        if (lastIndexOf2 == -1) {
            return lastIndexOf == -1 ? str : str.substring(0, lastIndexOf);
        }
        if (lastIndexOf == -1 || lastIndexOf2 > lastIndexOf) {
            return str.substring(lastIndexOf2 + 1);
        }
        return str.substring(lastIndexOf2 + 1, lastIndexOf);
    }

    public static String getFileExtension(File file) {
        if (file == null) {
            return null;
        }
        return getFileExtension(file.getPath());
    }

    public static String getFileExtension(String str) {
        if (StringUtils.isSpace(str)) {
            return str;
        }
        int lastIndexOf = str.lastIndexOf(46);
        return (lastIndexOf == -1 || str.lastIndexOf(File.separator) >= lastIndexOf) ? "" : str.substring(lastIndexOf + 1);
    }
}
