package com.wade.fit.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.common.util.CrashUtils;

public class IntentUtil {
    public static void goToActivity(Context context, Class cls) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        context.startActivity(intent);
    }

    public static void goToActivity(Context context, Class cls, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void goToActivityForResult(Context context, Class cls, int i) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        ((Activity) context).startActivityForResult(intent, i);
    }

    public static void goToActivityForResult(Context context, Class cls, Bundle bundle, int i) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        intent.putExtras(bundle);
        ((Activity) context).startActivityForResult(intent, i);
    }

    public static void goToActivityAndFinish(Context context, Class cls) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    public static void goToActivityAndFinish(Context context, Class cls, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        intent.putExtras(bundle);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    public static void goToActivityAndFinishTop(Context context, Class cls) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        intent.setFlags(67108864);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    public static void goToActivityAndFinishTop(Context context, Class cls, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(context, cls);
        intent.putExtras(bundle);
        intent.setFlags(67108864);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    public static void goToActivityAndFinishSingleTop(Context context, Class cls, Bundle bundle) {
        Intent intent = new Intent(context, cls);
        intent.putExtras(bundle);
        intent.setFlags(CrashUtils.ErrorDialogData.DYNAMITE_CRASH);
        Activity activity = (Activity) context;
        activity.startActivity(intent);
        activity.finish();
    }

    public static void goToActivityAndFinishSingleTop(Context context, Class cls) {
        Intent intent = new Intent(context, cls);
        intent.setFlags(CrashUtils.ErrorDialogData.DYNAMITE_CRASH);
        Activity activity = (Activity) context;
        activity.startActivity(intent);
        activity.finish();
    }
}
