package com.wade.fit.views.camera;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.wade.fit.util.ScreenUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private static final double MAX_ASPECT_DISTORTION = 0.15d;
    private static final int MIN_PREVIEW_PIXELS = 153600;
    private Context context;
    private boolean isPreview;
    private Camera mCamera;
    private int mCameraId;
    private SurfaceHolder mHolder;
    private final SizeMap mPictureSizes = new SizeMap();
    private final List<Size> mPictureSizes2 = new ArrayList();
    private final SizeMap mPreviewSizes = new SizeMap();
    private final List<Size> mPreviewSizes2 = new ArrayList();

    private boolean isLandscape(int i) {
        return i == 90 || i == 270;
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }

    public CameraPreview(Context context2, Camera camera, int i) {
        super(context2);
        this.context = context2;
        this.mCamera = camera;
        this.mHolder = getHolder();
        this.mHolder.addCallback(this);
        this.mCameraId = i;
        this.mHolder.setType(3);
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            Camera.Parameters parameters = this.mCamera.getParameters();
            Camera.Size findBestPreviewResolution = findBestPreviewResolution();
            Camera.Size findBestPictureResolution = findBestPictureResolution();
            parameters.setPreviewSize(findBestPreviewResolution.width, findBestPreviewResolution.height);
            parameters.setPictureSize(findBestPictureResolution.width, findBestPictureResolution.height);
            parameters.setPictureFormat(256);
            parameters.setRotation(90);
            this.mCamera.setParameters(parameters);
            this.mCamera.setPreviewDisplay(surfaceHolder);
            this.mCamera.startPreview();
            this.isPreview = true;
        } catch (IOException e) {
            Log.e("CameraPreview", "相机预览错误: " + e.getMessage());
        }
    }

    public Camera.Size findBestPreviewResolution() {
        Camera.Parameters parameters = this.mCamera.getParameters();
        Camera.Size previewSize = parameters.getPreviewSize();
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        if (supportedPreviewSizes == null) {
            return previewSize;
        }
        ArrayList<Camera.Size> arrayList = new ArrayList<>(supportedPreviewSizes);
        Collections.sort(arrayList, new Comparator<Camera.Size>() {
            /* class com.wade.fit.views.camera.CameraPreview.C27251 */

            public int compare(Camera.Size size, Camera.Size size2) {
                int i = size.height * size.width;
                int i2 = size2.height * size2.width;
                if (i2 < i) {
                    return -1;
                }
                return i2 > i ? 1 : 0;
            }
        });
        StringBuilder sb = new StringBuilder();
        for (Camera.Size size : arrayList) {
            sb.append(size.width);
            sb.append('x');
            sb.append(size.height);
            sb.append(' ');
        }
        double screenWidth = (double) ScreenUtil.getScreenWidth(this.context);
        double screenHeight = (double) ScreenUtil.getScreenHeight(this.context);
        Double.isNaN(screenWidth);
        Double.isNaN(screenHeight);
        double d = screenWidth / screenHeight;
        Iterator it = arrayList.iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                return !arrayList.isEmpty() ? (Camera.Size) arrayList.get(0) : previewSize;
            }
            Camera.Size size2 = (Camera.Size) it.next();
            int i = size2.width;
            int i2 = size2.height;
            if (i * i2 < MIN_PREVIEW_PIXELS) {
                it.remove();
            } else {
                if (i > i2) {
                    z = true;
                }
                int i3 = z ? i2 : i;
                if (!z) {
                    i = i2;
                }
                double d2 = (double) i3;
                double d3 = (double) i;
                Double.isNaN(d2);
                Double.isNaN(d3);
                if (Math.abs((d2 / d3) - d) > MAX_ASPECT_DISTORTION) {
                    it.remove();
                } else if (i3 == ScreenUtil.getScreenWidth(this.context) && i == ScreenUtil.getScreenHeight(this.context)) {
                    return size2;
                }
            }
        }
    }

    private Camera.Size findBestPictureResolution() {
        Camera.Parameters parameters = this.mCamera.getParameters();
        List<Camera.Size> supportedPictureSizes = parameters.getSupportedPictureSizes();
        StringBuilder sb = new StringBuilder();
        for (Camera.Size size : supportedPictureSizes) {
            sb.append(size.width);
            sb.append('x');
            sb.append(size.height);
            sb.append(" ");
        }
        Camera.Size pictureSize = parameters.getPictureSize();
        ArrayList arrayList = new ArrayList(supportedPictureSizes);
        Collections.sort(arrayList, new Comparator<Camera.Size>() {
            /* class com.wade.fit.views.camera.CameraPreview.C27262 */

            public int compare(Camera.Size size, Camera.Size size2) {
                int i = size.height * size.width;
                int i2 = size2.height * size2.width;
                if (i2 < i) {
                    return -1;
                }
                return i2 > i ? 1 : 0;
            }
        });
        double screenWidth = (double) ScreenUtil.getScreenWidth(this.context);
        double screenHeight = (double) ScreenUtil.getScreenHeight(this.context);
        Double.isNaN(screenWidth);
        Double.isNaN(screenHeight);
        double d = screenWidth / screenHeight;
        Iterator it = arrayList.iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            Camera.Size size2 = (Camera.Size) it.next();
            int i = size2.width;
            int i2 = size2.height;
            if (i > i2) {
                z = true;
            }
            int i3 = z ? i2 : i;
            if (z) {
                i2 = i;
            }
            double d2 = (double) i3;
            double d3 = (double) i2;
            Double.isNaN(d2);
            Double.isNaN(d3);
            if (Math.abs((d2 / d3) - d) > MAX_ASPECT_DISTORTION) {
                it.remove();
            }
        }
        return !arrayList.isEmpty() ? (Camera.Size) arrayList.get(0) : pictureSize;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        if (surfaceHolder.getSurface() != null) {
            this.mCamera.stopPreview();
            try {
                this.mCamera.setPreviewDisplay(this.mHolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.mCamera.startPreview();
        }
    }
}
