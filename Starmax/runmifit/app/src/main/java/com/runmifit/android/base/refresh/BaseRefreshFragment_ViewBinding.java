package com.wade.fit.base.refresh;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.RecyclerRefreshLayout;

public class BaseRefreshFragment_ViewBinding implements Unbinder {
    private BaseRefreshFragment target;

    public BaseRefreshFragment_ViewBinding(BaseRefreshFragment baseRefreshFragment, View view) {
        this.target = baseRefreshFragment;
        baseRefreshFragment.flTop = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.fl_top, "field 'flTop'", FrameLayout.class);
        baseRefreshFragment.flBottom = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.fl_bottom, "field 'flBottom'", FrameLayout.class);
        baseRefreshFragment.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.refresh_recyclerView, "field 'mRecyclerView'", RecyclerView.class);
        baseRefreshFragment.mRefreshLayout = (RecyclerRefreshLayout) Utils.findRequiredViewAsType(view, R.id.refreshLayout, "field 'mRefreshLayout'", RecyclerRefreshLayout.class);
    }

    public void unbind() {
        BaseRefreshFragment baseRefreshFragment = this.target;
        if (baseRefreshFragment != null) {
            this.target = null;
            baseRefreshFragment.flTop = null;
            baseRefreshFragment.flBottom = null;
            baseRefreshFragment.mRecyclerView = null;
            baseRefreshFragment.mRefreshLayout = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
