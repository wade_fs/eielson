package com.wade.fit.util;

import android.text.TextUtils;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.util.ble.BleSdkWrapper;

public class MineInfoHelper {
    public static String compluteWeight(int i) {
        return "";
    }

    public static String compluteHeight(int i) {
        Math.min(i, 250);
        int max = Math.max(0, i);
        if (!BleSdkWrapper.isDistUnitKm()) {
            int[] cm2inch = UnitFormat.cm2inch(i);
            return cm2inch[0] + "'" + cm2inch[1] + "\"";
        }
        String string = AppApplication.getInstance().getResources().getString(R.string.unit_cm);
        return max + string;
    }

    public static void saveUserInfo(UserBean userBean) {
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.USER_INFO_KEY, GsonUtil.toJson(userBean));
    }

    public static void saveSex(int i) {
        UserBean userBean = AppApplication.getInstance().getUserBean();
        userBean.setGender(i);
        saveUserInfo(userBean);
    }

    public static UserBean getCurrentUserBean() {
        return AppApplication.getInstance().getUserBean();
    }

    public static void saveYear(int i) {
        UserBean userBean = AppApplication.getInstance().getUserBean();
        userBean.setYear(i);
        saveUserInfo(userBean);
    }

    public static void saveHeight(int i) {
        UserBean userBean = AppApplication.getInstance().getUserBean();
        userBean.setHeight(i);
        saveUserInfo(userBean);
    }

    public static void resolverBirthday(String str, UserBean userBean) {
        if (!TextUtils.isEmpty(str)) {
            String[] split = str.split("-");
            userBean.setYear(NumUtil.parseInt(split[0]).intValue());
            if (split.length > 1) {
                userBean.setMonth(NumUtil.parseInt(split[1]).intValue());
            }
            if (split.length > 2) {
                userBean.setDay(NumUtil.parseInt(split[2]).intValue());
            }
        }
    }

    public static void resolverHeight(UserBean userBean) {
        userBean.setHeightLb(UnitUtil.cm2inchs(userBean.getHeight()));
    }

    public static void resolverWeight(UserBean userBean) {
        userBean.setWeightLb(Math.round((float) UnitUtil.kg2lb((float) userBean.getWeight())));
        userBean.setWeightSt(Math.round((float) UnitUtil.kg2st(userBean.getWeight())));
    }
}
