package com.wade.fit.util.log;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

public class LogMonitor {
    private static final long TIME_BLOCK = 1000;
    private static Runnable mLogRunnable = new Runnable() {
        /* class com.wade.fit.util.log.LogMonitor.C27021 */

        public void run() {
            StringBuilder sb = new StringBuilder();
            StackTraceElement[] stackTrace = Looper.getMainLooper().getThread().getStackTrace();
            for (StackTraceElement stackTraceElement : stackTrace) {
                sb.append(stackTraceElement.toString() + "\n");
            }
            Log.e("TAG", sb.toString());
        }
    };
    private static LogMonitor sInstance = new LogMonitor();
    private Handler mIoHandler;
    private HandlerThread mLogThread = new HandlerThread("log");

    public boolean isMonitor() {
        return true;
    }

    private LogMonitor() {
        this.mLogThread.start();
        this.mIoHandler = new Handler(this.mLogThread.getLooper());
    }

    public static LogMonitor getInstance() {
        return sInstance;
    }

    public void startMonitor() {
        this.mIoHandler.postDelayed(mLogRunnable, TIME_BLOCK);
    }

    public void removeMonitor() {
        this.mIoHandler.removeCallbacks(mLogRunnable);
    }
}
