package com.wade.fit.ui.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.base.BaseAdapter;
import com.wade.fit.base.BaseViewHolder;
import com.wade.fit.ui.main.adapter.MainMenuAdapter;
import com.wade.fit.ui.main.bean.MainMenuBean;
import com.wade.fit.util.ButtonUtils;
import java.util.List;

/* renamed from: com.wade.fit.ui.main.adapter.MainMenuAdapter */
public class MainMenuAdapter extends BaseAdapter<MainMenuBean, ViewHolder> {

    /* renamed from: com.wade.fit.ui.main.adapter.MainMenuAdapter$ViewHolder_ViewBinding */
    public class ViewHolder_ViewBinding implements Unbinder {
        private ViewHolder target;

        public ViewHolder_ViewBinding(ViewHolder viewHolder, View view) {
            this.target = viewHolder;
            viewHolder.tvData0 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvData0, "field 'tvData0'", TextView.class);
            viewHolder.ivMenu = (ImageView) Utils.findRequiredViewAsType(view, R.id.ivMenu, "field 'ivMenu'", ImageView.class);
            viewHolder.tvMenu = (TextView) Utils.findRequiredViewAsType(view, R.id.tvMenu, "field 'tvMenu'", TextView.class);
            viewHolder.tvData1 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvData1, "field 'tvData1'", TextView.class);
            viewHolder.tvUnit1 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvUnit1, "field 'tvUnit1'", TextView.class);
            viewHolder.tvData2 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvData2, "field 'tvData2'", TextView.class);
            viewHolder.tvUnit2 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvUnit2, "field 'tvUnit2'", TextView.class);
            viewHolder.lyMeau = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.lyMeau, "field 'lyMeau'", LinearLayout.class);
        }

        public void unbind() {
            ViewHolder viewHolder = this.target;
            if (viewHolder != null) {
                this.target = null;
                viewHolder.tvData0 = null;
                viewHolder.ivMenu = null;
                viewHolder.tvMenu = null;
                viewHolder.tvData1 = null;
                viewHolder.tvUnit1 = null;
                viewHolder.tvData2 = null;
                viewHolder.tvUnit2 = null;
                viewHolder.lyMeau = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public MainMenuAdapter(Context context, List<MainMenuBean> list) {
        super(context, list);
    }

    /* access modifiers changed from: protected */
    public void onNormalBindViewHolder(ViewHolder viewHolder, MainMenuBean mainMenuBean, int i) {
        viewHolder.tvMenu.setText(mainMenuBean.label);
        viewHolder.ivMenu.setImageResource(mainMenuBean.res);
        int i2 = mainMenuBean.type;
        if (i2 == 0) {
            viewHolder.lyMeau.setVisibility(View.VISIBLE);
            viewHolder.tvData0.setVisibility(View.VISIBLE);
            viewHolder.tvData0.setText(mainMenuBean.sportType);
            viewHolder.tvData1.setText(mainMenuBean.data1);
            viewHolder.tvData2.setText(mainMenuBean.data2);
            viewHolder.tvUnit1.setText(mainMenuBean.unit1);
            viewHolder.tvUnit2.setText(mainMenuBean.unit2);
        } else if (i2 == 1) {
            viewHolder.lyMeau.setVisibility(View.VISIBLE);
            viewHolder.tvData0.setVisibility(View.GONE);
            viewHolder.tvData1.setText(mainMenuBean.data1);
            viewHolder.tvData2.setText(mainMenuBean.data2);
            viewHolder.tvUnit1.setText(mainMenuBean.unit1);
            viewHolder.tvUnit2.setText(mainMenuBean.unit2);
        } else if (i2 == 2) {
            viewHolder.lyMeau.setVisibility(View.VISIBLE);
            viewHolder.tvData0.setVisibility(View.GONE);
            viewHolder.tvData1.setText(mainMenuBean.data1);
            viewHolder.tvUnit1.setText(mainMenuBean.unit1);
            viewHolder.tvData2.setText("");
            viewHolder.tvUnit2.setText("");
        } else if (i2 == 3 || i2 == 4 || i2 == 9) {
            viewHolder.lyMeau.setVisibility(View.INVISIBLE);
            viewHolder.tvData0.setVisibility(View.GONE);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener(viewHolder, i) {
            /* class com.wade.fit.ui.main.adapter.$$Lambda$MainMenuAdapter$S376K961AHciUwD7S1HMTxwVW6A */
            private final /* synthetic */ MainMenuAdapter.ViewHolder f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                MainMenuAdapter.this.lambda$onNormalBindViewHolder$0$MainMenuAdapter(this.f$1, this.f$2, view);
            }
        });
    }

    public /* synthetic */ void lambda$onNormalBindViewHolder$0$MainMenuAdapter(ViewHolder viewHolder, int i, View view) {
        if (!ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            this.mOnItemClickListener.onItemClick(viewHolder.itemView, i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(this.inflater.inflate((int) R.layout.item_main_fragment, viewGroup, false));
    }

    /* renamed from: com.wade.fit.ui.main.adapter.MainMenuAdapter$ViewHolder */
    public class ViewHolder extends BaseViewHolder {
        ImageView ivMenu;
        LinearLayout lyMeau;
        TextView tvData0;
        TextView tvData1;
        TextView tvData2;
        TextView tvMenu;
        TextView tvUnit1;
        TextView tvUnit2;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
