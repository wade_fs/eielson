package com.wade.fit.persenter.mine;

import com.wade.fit.base.IBaseView;

public interface AccountContract {

    public interface Presenter {
        void clearUserData();

        void loginOut();
    }

    public interface View extends IBaseView {
        void clearFaild(int i);

        void clearSuccess();

        void loginOutFaild(int i);

        void loginOutSuccess();
    }
}
