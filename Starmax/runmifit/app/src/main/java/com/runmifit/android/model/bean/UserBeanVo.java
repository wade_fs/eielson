package com.wade.fit.model.bean;

import android.graphics.Bitmap;

public class UserBeanVo {
    public Bitmap headerBitmap;
    public UserBean userBean;
}
