package com.wade.fit.util;

import android.text.TextUtils;
import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    private static Calendar calendar;
    public static SimpleDateFormat formatYMD = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat formatYMDHM = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static SimpleDateFormat formatYMDHM12 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MM-dd");

    public static String format(String str) {
        return new SimpleDateFormat(str).format(new Date());
    }

    public static String format(SimpleDateFormat simpleDateFormat3, Date date) {
        return simpleDateFormat3.format(date);
    }

    public static String getDay() {
        calendar = Calendar.getInstance();
        StringBuffer stringBuffer = new StringBuffer();
        int i = calendar.get(1);
        if (i < 10) {
            stringBuffer.append("0" + i);
        } else {
            stringBuffer.append(String.valueOf(i));
        }
        stringBuffer.append("-");
        int i2 = calendar.get(2) + 1;
        if (i2 < 10) {
            stringBuffer.append("0" + i2);
        } else {
            stringBuffer.append(String.valueOf(i2));
        }
        stringBuffer.append("-");
        int i3 = calendar.get(5);
        if (i3 < 10) {
            stringBuffer.append("0" + i3);
        } else {
            stringBuffer.append(String.valueOf(i3));
        }
        return new String(stringBuffer);
    }

    public static String format(int i, int i2, int i3) {
        return i + "-" + String.format("%02d", Integer.valueOf(i2)) + "-" + String.format("%02d", Integer.valueOf(i3));
    }

    public static String formatDateLongToStr(long j) {
        String str = j + "";
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("yyyyMMddHHmmss").parse(str));
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    public static String minFormatStr(int i) {
        return secondFormatStr(i * 60);
    }

    public static String secondFormatStr(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = i / 60;
        int i3 = i2 / 60;
        stringBuffer.append(String.format("%02d", Integer.valueOf(i3)));
        stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
        stringBuffer.append(String.format("%02d", Integer.valueOf(i2 - (i3 * 60))));
        stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
        stringBuffer.append(String.format("%02d", Integer.valueOf(i % 60)));
        return stringBuffer.toString();
    }

    public static Date getStartdayThisWeek(int i, int i2) {
        Calendar instance = Calendar.getInstance();
        int i3 = instance.get(7) - (i2 == 0 ? -1 : (i2 != 1 && i2 == 2) ? 1 : 0);
        int i4 = (-i3) + 1;
        System.out.println("getStartdayThisWeek day_of_week:" + i3 + ",offDay:" + i4);
        if (i4 > 0) {
            i4 -= 7;
        }
        if (i4 <= -7) {
            i4 = 0;
        }
        System.out.println("getStartdayThisWeek 往前推....:" + i3 + ",offDay....:" + i4);
        instance.add(5, i4 + (i * 7));
        return instance.getTime();
    }

    public static int dayOfMonth(int i, int i2) {
        int[] iArr = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0) {
            iArr[1] = iArr[1] + 1;
        }
        return iArr[i2];
    }

    public static long getLongFromDateStr(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return System.currentTimeMillis();
            }
            return simpleDateFormat.parse(str).getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static Date getDateByHMS(int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance();
        instance.set(11, i);
        instance.set(12, i2);
        instance.set(13, i3);
        instance.set(14, 0);
        return instance.getTime();
    }

    public static String computeTimePace(String str) {
        if (str.contains(FileUtil.HIDDEN_PREFIX)) {
            String[] split = str.split("\\.");
            return String.format("%02d", Integer.valueOf(Integer.parseInt(split[0]))) + "'" + String.format("%02d", Integer.valueOf(Integer.parseInt(split[1]))) + "''";
        } else if (str.contains(",")) {
            String[] split2 = str.split(",");
            return String.format("%02d", Integer.valueOf(Integer.parseInt(split2[0]))) + "'" + String.format("%02d", Integer.valueOf(Integer.parseInt(split2[1]))) + "''";
        } else {
            return String.format("%02d", 0) + "'" + String.format("%02d", 0) + "''";
        }
    }

    public static String computeTimeMS(int i) {
        return String.format("%02d", Integer.valueOf(i / 60)) + "'" + String.format("%02d", Integer.valueOf(i % 60)) + "''";
    }

    public static String computeTimeHMS(long j) {
        int i = (int) ((j / 60) / 60);
        StringBuffer stringBuffer = new StringBuffer();
        long j2 = j - ((long) ((i * 60) * 60));
        stringBuffer.append(String.format("%02d", Integer.valueOf(i)));
        stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
        int i2 = (int) (j2 / 60);
        int i3 = (int) (j2 - ((long) (i2 * 60)));
        stringBuffer.append(String.format("%02d", Integer.valueOf(i2)));
        stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
        stringBuffer.append(String.format("%02d", Integer.valueOf(i3)));
        return stringBuffer.toString();
    }

    public static String format(int i, int i2, int i3, String str) {
        return i + str + String.format("%02d", Integer.valueOf(i2)) + str + String.format("%02d", Integer.valueOf(i3));
    }

    public static int[] todayYearMonthDay() {
        calendar = Calendar.getInstance();
        return new int[]{calendar.get(1), calendar.get(2) + 1, calendar.get(5)};
    }

    public static int getTodayHour() {
        return Calendar.getInstance().get(11);
    }

    public static int getTodayMin() {
        return Calendar.getInstance().get(12);
    }

    public static int[] yearMonthDay(String str) {
        String[] split = str.split("-");
        int[] iArr = new int[3];
        int i = 0;
        while (i < split.length && i < iArr.length) {
            iArr[i] = Integer.parseInt(split[i]);
            i++;
        }
        return iArr;
    }

    public static boolean isInDate(Date date, String str, String str2) {
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(date);
        int parseInt = Integer.parseInt(format.substring(11, 13) + format.substring(14, 16) + format.substring(17, 19));
        int parseInt2 = Integer.parseInt(str.substring(0, 2) + str.substring(3, 5) + str.substring(6, 8));
        int parseInt3 = Integer.parseInt(str2.substring(0, 2) + str2.substring(3, 5) + str2.substring(6, 8));
        if (parseInt < parseInt2 || parseInt > parseInt3) {
            return false;
        }
        return true;
    }

    public static String getSpecifiedDayBefore(String str) {
        Date date;
        Calendar instance = Calendar.getInstance();
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            date = null;
        }
        instance.setTime(date);
        instance.set(5, instance.get(5) - 1);
        return new SimpleDateFormat("yyyy-MM-dd").format(instance.getTime());
    }

    public static Date getSpecifiedDayBeforeDate(String str) {
        Date date;
        Calendar instance = Calendar.getInstance();
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            date = null;
        }
        instance.setTime(date);
        instance.set(5, instance.get(5) - 1);
        return instance.getTime();
    }

    public static String getSpecifiedDayAfter(String str) {
        Date date;
        Calendar instance = Calendar.getInstance();
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            date = null;
        }
        instance.setTime(date);
        instance.set(5, instance.get(5) + 1);
        return new SimpleDateFormat("yyyy-MM-dd").format(instance.getTime());
    }

    public static Date getSpecifiedDayAfterDate(String str) {
        Date date;
        Calendar instance = Calendar.getInstance();
        try {
            date = new SimpleDateFormat("yy-MM-dd").parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            date = null;
        }
        instance.setTime(date);
        instance.set(5, instance.get(5) + 1);
        return instance.getTime();
    }

    public static boolean isToday(String str) throws ParseException {
        return str.equals(formatYMD.format(new Date()));
    }
}
