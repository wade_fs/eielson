package com.wade.fit.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.util.ScreenUtil;

public class CommonDialog extends Dialog {
    public CommonDialog(Context context) {
        super(context);
    }

    public CommonDialog(Context context, int i) {
        super(context, i);
    }

    public static class Builder {
        private boolean cancelable;
        private View contentView;
        private Context context;
        private boolean isVertical = false;
        private int leftTextColor = -1;
        private String message;
        private int messageTextColor = -1;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener negativeButtonOnClickListener;
        private String negativeButtonText;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener positiveButtonOnClickListener;
        private String positiveButtonText;
        private int rightTextColor = -1;
        private String title;
        private int titleTextColor = -1;
        private int type = -1;

        public Builder(Context context2) {
            this.context = context2;
        }

        public Builder setTitle(int i) {
            this.title = this.context.getString(i);
            return this;
        }

        public Builder setTitle(String str) {
            this.title = str;
            return this;
        }

        public Builder setMessage(int i) {
            this.message = this.context.getString(i);
            return this;
        }

        public Builder setMessage(String str) {
            this.message = str;
            return this;
        }

        public Builder setLeftTextColor(int i) {
            this.leftTextColor = this.context.getResources().getColor(i);
            return this;
        }

        public Builder setRightTextColor(int i) {
            this.rightTextColor = this.context.getResources().getColor(i);
            return this;
        }

        public Builder setTitleTextColor(int i) {
            this.titleTextColor = this.context.getResources().getColor(i);
            return this;
        }

        public Builder setMessageTextColor(int i) {
            this.messageTextColor = this.context.getResources().getColor(i);
            return this;
        }

        public Builder isVertical(boolean z) {
            this.isVertical = z;
            return this;
        }

        public Builder setCancelable(boolean z) {
            this.cancelable = z;
            return this;
        }

        public Builder setRightButton(String str, DialogInterface.OnClickListener onClickListener) {
            this.positiveButtonText = str;
            this.positiveButtonOnClickListener = onClickListener;
            return this;
        }

        public Builder setRightButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.positiveButtonText = this.context.getString(i);
            this.positiveButtonOnClickListener = onClickListener;
            return this;
        }

        public Builder setRightButton(DialogInterface.OnClickListener onClickListener) {
            this.positiveButtonOnClickListener = onClickListener;
            return this;
        }

        public Builder setLeftButton(String str, DialogInterface.OnClickListener onClickListener) {
            this.negativeButtonText = str;
            this.negativeButtonOnClickListener = onClickListener;
            return this;
        }

        public Builder setLeftButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.negativeButtonText = this.context.getString(i);
            this.negativeButtonOnClickListener = onClickListener;
            return this;
        }

        public Builder setLeftButton(int i) {
            this.negativeButtonText = this.context.getString(i);
            return this;
        }

        public Builder setView(View view) {
            this.contentView = view;
            return this;
        }

        public Builder setType(int i) {
            this.type = i;
            return this;
        }

        public CommonDialog create() {
            View view;
            LayoutInflater from = LayoutInflater.from(this.context);
            final CommonDialog commonDialog = new CommonDialog(this.context, R.style.dialog);
            if (this.type == -1) {
                view = from.inflate(this.isVertical ? R.layout.common_dialog_vertical_layout : R.layout.common_dialog_layout, (ViewGroup) null);
            } else {
                view = from.inflate((int) R.layout.common_dialog_vertical_layout2, (ViewGroup) null);
            }
            commonDialog.addContentView(view, new ViewGroup.LayoutParams(-1, -2));
            commonDialog.setCancelable(this.cancelable);
            commonDialog.setCanceledOnTouchOutside(true);
            if (this.type != -1) {
                commonDialog.getWindow().getAttributes().width = (int) (((float) ScreenUtil.getScreenWidth(this.context)) * 0.9f);
                commonDialog.getWindow().setGravity(80);
            } else if (!this.isVertical) {
                commonDialog.getWindow().getAttributes().width = (int) (((float) ScreenUtil.getScreenWidth(this.context)) * 0.75f);
            }
            if (!TextUtils.isEmpty(this.title)) {
                ((TextView) view.findViewById(R.id.title)).setText(this.title);
                if (this.titleTextColor != -1) {
                    ((TextView) view.findViewById(R.id.title)).setTextColor(this.titleTextColor);
                }
            } else {
                view.findViewById(R.id.title).setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(this.message)) {
                ((TextView) view.findViewById(R.id.message)).setText(this.message);
                if (this.messageTextColor != -1) {
                    ((TextView) view.findViewById(R.id.message)).setTextColor(this.messageTextColor);
                }
            } else if (this.contentView != null) {
                ((LinearLayout) view.findViewById(R.id.content)).removeAllViews();
                ((LinearLayout) view.findViewById(R.id.content)).addView(this.contentView, new LinearLayout.LayoutParams(-1, -1));
            }
            if (!TextUtils.isEmpty(this.positiveButtonText)) {
                ((Button) view.findViewById(R.id.positiveButton)).setText(this.positiveButtonText);
                view.findViewById(R.id.positiveButton).setOnClickListener(new View.OnClickListener() {
                    /* class com.wade.fit.views.dialog.CommonDialog.Builder.C27271 */

                    public void onClick(View view) {
                        commonDialog.dismiss();
                        if (Builder.this.positiveButtonOnClickListener != null) {
                            Builder.this.positiveButtonOnClickListener.onClick(commonDialog, -1);
                        }
                    }
                });
                if (this.rightTextColor != -1) {
                    ((Button) view.findViewById(R.id.positiveButton)).setTextColor(this.rightTextColor);
                }
            } else {
                view.findViewById(R.id.positiveButton).setVisibility(View.GONE);
                view.findViewById(R.id.bottom_line).setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(this.negativeButtonText)) {
                ((Button) view.findViewById(R.id.negativeButton)).setText(this.negativeButtonText);
                view.findViewById(R.id.negativeButton).setOnClickListener(new View.OnClickListener() {
                    /* class com.wade.fit.views.dialog.CommonDialog.Builder.C27282 */

                    public void onClick(View view) {
                        commonDialog.dismiss();
                        if (Builder.this.negativeButtonOnClickListener != null) {
                            Builder.this.negativeButtonOnClickListener.onClick(commonDialog, -2);
                        }
                    }
                });
                if (this.leftTextColor != -1) {
                    ((Button) view.findViewById(R.id.negativeButton)).setTextColor(this.leftTextColor);
                }
            } else {
                view.findViewById(R.id.negativeButton).setVisibility(View.GONE);
                view.findViewById(R.id.bottom_line).setVisibility(View.GONE);
            }
            commonDialog.setContentView(view);
            return commonDialog;
        }
    }
}
