package com.wade.fit.ui.device.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.base.BaseAdapter;
import com.wade.fit.base.BaseViewHolder;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.ui.device.adapter.ScanDeviceAdapter;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.adapter.ScanDeviceAdapter */
public class ScanDeviceAdapter extends BaseAdapter<BLEDevice, ViewHolder> {
    private int connPosition = -1;

    /* renamed from: com.wade.fit.ui.device.adapter.ScanDeviceAdapter$ViewHolder_ViewBinding */
    public class ViewHolder_ViewBinding implements Unbinder {
        private ViewHolder target;

        public ViewHolder_ViewBinding(ViewHolder viewHolder, View view) {
            this.target = viewHolder;
            viewHolder.tvDeviceName = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDeviceName, "field 'tvDeviceName'", TextView.class);
            viewHolder.tvMac = (TextView) Utils.findRequiredViewAsType(view, R.id.tvMac, "field 'tvMac'", TextView.class);
            viewHolder.layoutItem = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.layoutItem, "field 'layoutItem'", RelativeLayout.class);
            viewHolder.tvState = (ImageView) Utils.findRequiredViewAsType(view, R.id.tvState, "field 'tvState'", ImageView.class);
            viewHolder.tvConnect = (ImageView) Utils.findRequiredViewAsType(view, R.id.tvConnect, "field 'tvConnect'", ImageView.class);
        }

        public void unbind() {
            ViewHolder viewHolder = this.target;
            if (viewHolder != null) {
                this.target = null;
                viewHolder.tvDeviceName = null;
                viewHolder.tvMac = null;
                viewHolder.layoutItem = null;
                viewHolder.tvState = null;
                viewHolder.tvConnect = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public ScanDeviceAdapter(Context context, List<BLEDevice> list) {
        super(context, list);
    }

    public void setData(List<BLEDevice> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onNormalBindViewHolder(ViewHolder viewHolder, BLEDevice bLEDevice, int i) {
        if (i == this.connPosition) {
            viewHolder.tvState.setVisibility(View.GONE);
            viewHolder.tvConnect.setVisibility(View.VISIBLE);
            viewHolder.tvConnect.startAnimation(AnimationUtils.loadAnimation(this.mContext, R.anim.progress_drawable));
        } else {
            viewHolder.tvConnect.setVisibility(View.GONE);
            viewHolder.tvState.setVisibility(View.VISIBLE);
            if (Math.abs(bLEDevice.mRssi) <= 70) {
                viewHolder.tvState.setImageResource(R.mipmap.device_rssi_1);
            } else if (Math.abs(bLEDevice.mRssi) <= 90) {
                viewHolder.tvState.setImageResource(R.mipmap.device_rssi_2);
            } else {
                viewHolder.tvState.setImageResource(R.mipmap.device_rssi_3);
            }
        }
        viewHolder.tvDeviceName.setText(bLEDevice.mDeviceName);
        viewHolder.tvMac.setText(bLEDevice.mDeviceAddress);
        viewHolder.layoutItem.setOnClickListener(new View.OnClickListener(viewHolder, i) {
            /* class com.wade.fit.ui.device.adapter.$$Lambda$ScanDeviceAdapter$rmxzmk9mU60V_RoLgcyoIpfsOBs */
            private final /* synthetic */ ScanDeviceAdapter.ViewHolder f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                ScanDeviceAdapter.this.lambda$onNormalBindViewHolder$0$ScanDeviceAdapter(this.f$1, this.f$2, view);
            }
        });
    }

    public /* synthetic */ void lambda$onNormalBindViewHolder$0$ScanDeviceAdapter(ViewHolder viewHolder, int i, View view) {
        this.mOnItemClickListener.onItemClick(viewHolder.layoutItem, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(this.inflater.inflate((int) R.layout.item_scan_device, viewGroup, false));
    }

    public void connecting(int i) {
        this.connPosition = i;
        notifyDataSetChanged();
    }

    /* renamed from: com.wade.fit.ui.device.adapter.ScanDeviceAdapter$ViewHolder */
    public class ViewHolder extends BaseViewHolder {
        RelativeLayout layoutItem;
        ImageView tvConnect;
        TextView tvDeviceName;
        TextView tvMac;
        ImageView tvState;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
