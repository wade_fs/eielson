package com.wade.fit.persenter.main;

import android.text.TextUtils;
import com.wade.fit.app.AppApplication;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.greendao.gen.HealthHeartRateDao;
import com.wade.fit.greendao.gen.HealthHeartRateItemDao;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.model.bean.HeartRateDetailVO;
import com.wade.fit.persenter.sport.BaseTimePresenter;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.greenrobot.greendao.query.WhereCondition;

public class DetailHrPresenter extends BaseTimePresenter {
    public HeartRateDetailVO getDetailCurrent(DetailTimeType detailTimeType, Date date) {
        currentDate(detailTimeType, date);
        return getDetail();
    }

    public HeartRateDetailVO getDetailCurrent(DetailTimeType detailTimeType) {
        currentDate(detailTimeType);
        return getDetail();
    }

    public HeartRateDetailVO getDetailNext(DetailTimeType detailTimeType) {
        nextDate(detailTimeType);
        return getDetail();
    }

    public HeartRateDetailVO getDetailPre(DetailTimeType detailTimeType) {
        preDate(detailTimeType);
        return getDetail();
    }

    public HeartRateDetailVO getSixMonth() {
        setSixMonth();
        return get();
    }

    public HeartRateDetailVO getYearMonth() {
        setYear();
        return get();
    }

    private HeartRateDetailVO get() {
        this.dates.clear();
        List<HealthHeartRate> list = AppApplication.getInstance().getDaoSession().getHealthHeartRateDao().queryBuilder().where(HealthHeartRateDao.Properties.Date.between(Long.valueOf(this.endDate.getTime()), Long.valueOf(this.startDate.getTime())), new WhereCondition[0]).orderDesc(HealthHeartRateDao.Properties.Date).build().list();
        for (HealthHeartRate healthHeartRate : list) {
            LogUtil.d("size" + healthHeartRate.getDate() + "/" + healthHeartRate.getAvgHr());
        }
        LogUtil.d("size1" + list.size());
        LogUtil.d("size1" + this.startDate.getTime());
        LogUtil.d("size1" + this.endDate.getTime());
        Calendar instance = Calendar.getInstance();
        instance.setTime(this.endDate);
        ArrayList arrayList = new ArrayList();
        LogUtil.m5264d(this.timeType);
        HealthHeartRate healthHeartRate2 = new HealthHeartRate();
        int i = 0;
        int i2 = 0;
        while (instance.getTime().getTime() <= this.startDate.getTime()) {
            int i3 = instance.get(1);
            int i4 = instance.get(2) + 1;
            int i5 = instance.get(5);
            HealthHeartRate healthHeartRate3 = null;
            for (HealthHeartRate healthHeartRate4 : list) {
                if (healthHeartRate4.getYear() == i3 && healthHeartRate4.getMonth() == i4 && healthHeartRate4.getDay() == i5) {
                    healthHeartRate3 = healthHeartRate4;
                }
            }
            if (healthHeartRate3 == null) {
                healthHeartRate3 = new HealthHeartRate();
            }
            HealthHeartRate healthHeartRate5 = healthHeartRate3;
            int i6 = C23941.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
            if (i6 == 1) {
                setDateLabel(i, instance, 0);
                healthHeartRate5.setRemark(this.dateFormat3.format(instance.getTime()));
                arrayList.add(healthHeartRate5);
                i++;
            } else if (i6 == 2) {
                if (healthHeartRate5.getAvgHr() > 0) {
                    i2++;
                }
                healthHeartRate2.setAvgHr(healthHeartRate2.getAvgHr() + healthHeartRate5.getAvgHr());
                setDateLabel(i, instance, 0);
                if (isWeek(i, instance)) {
                    HealthHeartRate healthHeartRate6 = new HealthHeartRate();
                    if (i2 != 0) {
                        healthHeartRate6.setAvgHr(healthHeartRate2.getAvgHr() / i2);
                        healthHeartRate6.setAvgDayHr(healthHeartRate2.getAvgHr() / i2);
                    }
                    healthHeartRate6.setRemark(getWeekStr(instance));
                    arrayList.add(healthHeartRate6);
                    healthHeartRate2.setAvgHr(0);
                    i2 = 0;
                }
                i++;
            } else if (i6 == 3) {
                if (healthHeartRate5.getAvgHr() > 0) {
                    i2++;
                }
                healthHeartRate2.setAvgHr(healthHeartRate2.getAvgHr() + healthHeartRate5.getAvgHr());
                if (TextUtils.isEmpty(healthHeartRate2.getRemark())) {
                    healthHeartRate2.setRemark(this.dateFormat4.format(instance.getTime()));
                }
                Calendar instance2 = Calendar.getInstance();
                instance2.setTime(this.startDate);
                instance2.set(11, 0);
                instance2.set(12, 0);
                instance2.set(13, 0);
                instance2.set(14, 0);
                if (isEndMonth(i3, i4 - 1, i5) || instance.getTime().getTime() == instance2.getTime().getTime()) {
                    LogUtil.d(healthHeartRate2.toString());
                    setDateLabel(i, instance, i4);
                    HealthHeartRate healthHeartRate7 = new HealthHeartRate();
                    healthHeartRate7.setRemark(healthHeartRate2.getRemark());
                    if (i2 != 0) {
                        healthHeartRate7.setAvgHr(healthHeartRate2.getAvgHr() / i2);
                        healthHeartRate7.setAvgDayHr(healthHeartRate2.getAvgHr() / i2);
                    }
                    arrayList.add(healthHeartRate7);
                    healthHeartRate2.setAvgHr(0);
                    healthHeartRate2.setRemark(null);
                    i++;
                    i2 = 0;
                }
            }
            instance.add(5, 1);
        }
        LogUtil.d(Arrays.toString(this.dates.toArray()));
        LogUtil.d("size:" + arrayList.size());
        HeartRateDetailVO heartRateDetailVO = new HeartRateDetailVO();
        heartRateDetailVO.healthSportList = arrayList;
        heartRateDetailVO.dates = this.dates;
        heartRateDetailVO.mainVO = getByDate();
        ArrayList arrayList2 = new ArrayList();
        HealthHeartRate healthHeartRate8 = new HealthHeartRate();
        int i7 = 0;
        int i8 = 0;
        for (int i9 = 0; i9 < arrayList.size(); i9++) {
            LogUtil.d("size:" + ((HealthHeartRate) arrayList.get(i9)).toString());
            HealthHeartRate healthHeartRate9 = (HealthHeartRate) arrayList.get(i9);
            HealthHeartRateItem healthHeartRateItem = new HealthHeartRateItem();
            healthHeartRateItem.setOffsetMinute(1440 / arrayList.size());
            healthHeartRateItem.setHeartRaveValue(healthHeartRate9.getAvgHr());
            healthHeartRateItem.setRemark(healthHeartRate9.getRemark());
            arrayList2.add(healthHeartRateItem);
            i8 += healthHeartRate9.getAvgHr();
            if (i9 == 0) {
                healthHeartRate8.setSilentHeart(healthHeartRateItem.getHeartRaveValue());
            }
            if (healthHeartRateItem.getHeartRaveValue() > 0) {
                i7++;
            }
            heartRateDetailVO.maxValue = Math.max(heartRateDetailVO.maxValue, healthHeartRateItem.getHeartRaveValue());
            if (healthHeartRateItem.getHeartRaveValue() > 0) {
                heartRateDetailVO.minValue = heartRateDetailVO.minValue == 0 ? healthHeartRateItem.getHeartRaveValue() : Math.min(heartRateDetailVO.minValue, healthHeartRateItem.getHeartRaveValue());
            }
        }
        if (i7 > 0) {
            heartRateDetailVO.avgValue = i8 / i7;
        }
        LogUtil.d("size:" + arrayList2.size());
        heartRateDetailVO.mainVO.healthHeartRate = healthHeartRate8;
        heartRateDetailVO.items = arrayList2;
        return heartRateDetailVO;
    }

    /* renamed from: com.wade.fit.persenter.main.DetailHrPresenter$1 */
    static /* synthetic */ class C23941 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailTimeType = new int[DetailTimeType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.wade.fit.model.bean.DetailTimeType[] r0 = com.wade.fit.model.bean.DetailTimeType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.persenter.main.DetailHrPresenter.C23941.$SwitchMap$com$wade.fit$model$bean$DetailTimeType = r0
                int[] r0 = com.wade.fit.persenter.main.DetailHrPresenter.C23941.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.ONE_MONTH     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.persenter.main.DetailHrPresenter.C23941.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.SIX_MONTH     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.persenter.main.DetailHrPresenter.C23941.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.YEAR     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.wade.fit.persenter.main.DetailHrPresenter.C23941.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.DAY     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.persenter.main.DetailHrPresenter.C23941.<clinit>():void");
        }
    }

    public HeartRateDetailVO getOneMonth() {
        setOneMonth();
        return get();
    }

    public HeartRateDetailVO getDetail() {
        LogUtil.d("index:" + this.index + ",date:" + this.dateFormat.format(this.currentDate) + "detailTimeType:" + this.timeType.toString());
        HeartRateDetailVO heartRateDetailVO = new HeartRateDetailVO();
        ArrayList arrayList = new ArrayList();
        heartRateDetailVO.healthSportList = arrayList;
        int i = C23941.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
        if (i == 1 || i == 2 || i == 3) {
            return get();
        }
        if (i == 4) {
            List list = AppApplication.getInstance().getDaoSession().getHealthHeartRateItemDao().queryBuilder().where(HealthHeartRateItemDao.Properties.Year.eq(Integer.valueOf(this.year)), HealthHeartRateItemDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthHeartRateItemDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthHeartRateItemDao.Properties.Date).build().list();
            if (list != null) {
                for (int i2 = 0; i2 < list.size(); i2++) {
                    HealthHeartRateItem healthHeartRateItem = (HealthHeartRateItem) list.get(i2);
                    healthHeartRateItem.setRemark(getDayRemark(i2, healthHeartRateItem.getOffsetMinute()));
                }
                heartRateDetailVO.items.addAll(list);
            }
            LogUtil.d("size:" + heartRateDetailVO.items.size());
            heartRateDetailVO.dates.add("00:00");
            heartRateDetailVO.dates.add("06:00");
            heartRateDetailVO.dates.add("12:00");
            heartRateDetailVO.dates.add("18:00");
            heartRateDetailVO.dates.add("23:59");
            HealthHeartRate healthHeartRate = (HealthHeartRate) AppApplication.getInstance().getDaoSession().getHealthHeartRateDao().queryBuilder().where(HealthHeartRateDao.Properties.Year.eq(Integer.valueOf(this.year)), HealthHeartRateDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthHeartRateDao.Properties.Day.eq(Integer.valueOf(this.day))).build().unique();
            if (healthHeartRate != null) {
                arrayList.add(healthHeartRate);
            }
        }
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < heartRateDetailVO.items.size(); i5++) {
            HealthHeartRateItem healthHeartRateItem2 = heartRateDetailVO.items.get(i5);
            HealthHeartRateItem healthHeartRateItem3 = new HealthHeartRateItem();
            healthHeartRateItem3.setHeartRaveValue(healthHeartRateItem2.getHeartRaveValue());
            healthHeartRateItem3.setRemark(healthHeartRateItem2.getRemark());
            if (healthHeartRateItem3.getHeartRaveValue() > 0) {
                i3++;
            }
            i4 += healthHeartRateItem2.getHeartRaveValue();
            heartRateDetailVO.maxValue = Math.max(heartRateDetailVO.maxValue, healthHeartRateItem3.getHeartRaveValue());
            if (healthHeartRateItem3.getHeartRaveValue() > 0) {
                heartRateDetailVO.minValue = heartRateDetailVO.minValue == 0 ? healthHeartRateItem3.getHeartRaveValue() : Math.min(heartRateDetailVO.minValue, healthHeartRateItem3.getHeartRaveValue());
            }
        }
        if (i3 > 0) {
            heartRateDetailVO.avgValue = i4 / i3;
        }
        LogUtil.d("size:" + heartRateDetailVO.items.size());
        LogUtil.d(Arrays.toString(heartRateDetailVO.items.toArray()));
        LogUtil.d(Arrays.toString(heartRateDetailVO.dates.toArray()));
        heartRateDetailVO.mainVO = this.mainVO;
        return heartRateDetailVO;
    }
}
