package com.wade.fit.persenter.mine;

import android.text.TextUtils;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.ImageResult;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.http.FileHttp;
import com.wade.fit.model.net.http.UserHttp;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleCallback;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.tamic.novate.util.FileUtil;
import com.tamic.novate.util.Utils;
import com.tencent.open.SocialConstants;
import java.io.File;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import p041io.reactivex.disposables.Disposable;

public class MinePresenter extends GetUserPresenter<IMineView> {
    public void sendUserToBle(final UserBean userBean) {
        if (!TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            UserHttp.setUserInfo(RequestBody.create(MediaType.parse(Utils.MULTIPART_JSON_DATA), "{\"name\":\"" + userBean.getUserName() + "\",\"sex\":\"" + userBean.getGender() + "\",\"birthday\":\"" + userBean.getYear() + "-" + String.format("%02d", Integer.valueOf(userBean.getMonth())) + "-" + String.format("%02d", Integer.valueOf(userBean.getDay())) + "\",\"height\":\"" + userBean.getHeight() + "\",\"weight\":\"" + (userBean.getWeight() * 10) + "\",\"stepSize\":\"" + userBean.getStepDistance() + "\",\"appleHealth\":\"" + false + "\",\"unit\":\"" + userBean.getUnit() + "\"}"), new ApiCallback<BaseBean<String>>() {
                /* class com.wade.fit.persenter.mine.MinePresenter.C24251 */

                public void onSuccess(BaseBean<String> baseBean) {
                    if (BleSdkWrapper.isConnected() && BleSdkWrapper.isBind()) {
                        BleSdkWrapper.setUserInfo(userBean, new BleCallback() {
                            /* class com.wade.fit.persenter.mine.MinePresenter.C24251.C24261 */

                            public void setSuccess() {
                            }

                            public void complete(int i, Object obj) {
                                ((IMineView) MinePresenter.this.mView).saveUserBean(1);
                            }
                        });
                    }
                }

                public void onFailure(int i, String str) {
                    ((IMineView) MinePresenter.this.mView).saveFaild();
                    ((IMineView) MinePresenter.this.mView).hideLoading();
                }

                public void onFinish() {
                    MinePresenter.this.dispose();
                }

                public void onSubscribe(Disposable disposable) {
                    MinePresenter.this.addDisposable(disposable);
                }
            });
        } else if (BleSdkWrapper.isConnected() && BleSdkWrapper.isBind()) {
            BleSdkWrapper.setUserInfo(userBean, new BleCallback() {
                /* class com.wade.fit.persenter.mine.MinePresenter.C24272 */

                public void setSuccess() {
                }

                public void complete(int i, Object obj) {
                    ((IMineView) MinePresenter.this.mView).saveUserBean(1);
                }
            });
        }
    }

    public void uploadImage(File file, final int i) {
        if (!TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            ((IMineView) this.mView).showLoadingFalse();
            MultipartBody.Builder type = new MultipartBody.Builder().setType(MultipartBody.FORM);
            FileHttp.uploadImage(type.addFormDataPart(SocialConstants.PARAM_TYPE, i + "").addFormDataPart("image", file.getName(), RequestBody.create(MediaType.parse(FileUtil.MIME_TYPE_IMAGE), file)).build(), new ApiCallback<BaseBean<ImageResult>>() {
                /* class com.wade.fit.persenter.mine.MinePresenter.C24283 */

                public void onFinish() {
                }

                public void onSuccess(BaseBean<ImageResult> baseBean) {
                    ImageResult data = baseBean.getData();
                    ((IMineView) MinePresenter.this.mView).hideLoading();
                    int i = i;
                    if (i == 1) {
                        ((IMineView) MinePresenter.this.mView).updateHeaderComplete(i, data.getAvatar());
                    } else if (i == 2) {
                        ((IMineView) MinePresenter.this.mView).updateHeaderComplete(i, data.getCoverImage());
                    }
                }

                public void onFailure(int i, String str) {
                    ((IMineView) MinePresenter.this.mView).saveFaild();
                    ((IMineView) MinePresenter.this.mView).hideLoading();
                }

                public void onSubscribe(Disposable disposable) {
                    MinePresenter.this.addDisposable(disposable);
                }
            });
        }
    }
}
