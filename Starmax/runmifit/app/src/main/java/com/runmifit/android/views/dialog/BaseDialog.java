package com.wade.fit.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import com.wade.fit.R;

public class BaseDialog implements View.OnClickListener {
    protected Dialog dialog = new Dialog(this.mContext, R.style.MyDialog);
    protected Context mContext;

    protected BaseDialog(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public void setDialogLocation(Context context, Dialog dialog2) {
        WindowManager.LayoutParams attributes = dialog2.getWindow().getAttributes();
        attributes.x = 0;
        attributes.y = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
        attributes.width = -1;
        attributes.height = -2;
        dialog2.onWindowAttributesChanged(attributes);
        dialog2.setCanceledOnTouchOutside(true);
    }

    public void show() {
        Dialog dialog2 = this.dialog;
        if (dialog2 != null && !dialog2.isShowing()) {
            this.dialog.show();
        }
    }

    public void dismiss() {
        Dialog dialog2 = this.dialog;
        if (dialog2 != null && dialog2.isShowing()) {
            this.dialog.dismiss();
        }
    }

    public void onClick(View view) {
        dismiss();
    }
}
