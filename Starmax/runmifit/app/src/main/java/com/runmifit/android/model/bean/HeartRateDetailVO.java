package com.wade.fit.model.bean;

import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.views.MainVO;
import java.util.ArrayList;
import java.util.List;

public class HeartRateDetailVO {
    public List<HealthHeartRate> allDatas = new ArrayList();
    public int avgValue;
    public List<HealthHeartRate> datas;
    public String date;
    public List<String> dates = new ArrayList();
    public List<HealthHeartRate> healthSportList = new ArrayList();
    public List<HealthHeartRateItem> items = new ArrayList();
    public MainVO mainVO;
    public int maxValue;
    public int minValue = 0;
}
