package com.wade.fit.greendao.bean;

public class HealthSleepItem {
    private long date;
    private int day;
    private int index;
    private int month;
    private int offsetMinute;
    private String remark;
    private int sleepStatus;
    private String sleeptime;
    private String userId;
    private int year;

    public HealthSleepItem(int i, int i2, int i3, int i4, String str, int i5, int i6, long j, String str2, String str3) {
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.offsetMinute = i4;
        this.sleeptime = str;
        this.index = i5;
        this.sleepStatus = i6;
        this.date = j;
        this.userId = str2;
        this.remark = str3;
    }

    public HealthSleepItem() {
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getOffsetMinute() {
        return this.offsetMinute;
    }

    public void setOffsetMinute(int i) {
        this.offsetMinute = i;
    }

    public String getSleeptime() {
        return this.sleeptime;
    }

    public void setSleeptime(String str) {
        this.sleeptime = str;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int i) {
        this.index = i;
    }

    public int getSleepStatus() {
        return this.sleepStatus;
    }

    public void setSleepStatus(int i) {
        this.sleepStatus = i;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String str) {
        this.remark = str;
    }
}
