package com.wade.fit.util.ble;

import com.baidu.mobstat.Config;
import com.google.common.primitives.UnsignedBytes;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.DeviceState;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.log.LogUtil;
import com.tamic.novate.util.FileUtil;
import java.util.Arrays;

public class BaseDataHandler implements Constants {
    public static final int ACCEPT_HISTORY_CONTINUME = 2;
    public static final int ACCEPT_HISTORY_END = 3;
    public static final int ACCEPT_HISTORY_START = 1;
    private static final int FLAG_GET_NOTICE = 137;
    private static final int FLAG_GET_SLEEP = 136;
    private static final int FLAG_GET_TARGE = 135;
    private static final int FLAG_SEND_MESSAGE = 138;
    private static final int FLAG_SET_ALARM = 133;
    private static final int FLAG_SET_LONGSIT = 134;
    private static final int FLAG_SET_TIME = 132;
    public static final int FLAT_ALARM = 5;
    private static final int FLAT_DEVICE_CONTROL = 144;
    public static final int FLAT_HISOTY_STEP = 1;
    public static final int FLAT_HISTORY_ACTIVITY = 3;
    public static final int FLAT_HISTORY_ECG = 85;
    public static final int FLAT_HISTORY_HEART_RATE = 2;
    public static final int FLAT_NORMAL_DATA = 0;
    private static final int FLAT_SET_USERINFO = 131;
    private static final int FLAT_START_GET_ACTIVITY = 163;
    private static final int FLAT_START_GET_CURRENT_STEP = 160;
    private static final int FLAT_START_GET_DEVICE = 129;
    private static final int FLAT_START_GET_DEVICE_STATE = 130;
    private static final int FLAT_START_GET_ECG = 162;
    private static final int FLAT_START_GET_HEART_RATE = 161;
    private static final int FLAT_START_GET_HISTORY_STEP = 2;
    private static final int FLAT_START_GET_POWER = 167;
    private static int HISTORY_HEART_RATE_MAX_INDEX = 14;
    private static final int HISTORY_STEP_MAX_INDEX = 14;
    private static final int RESPONE_START_CMD = 129;
    private static BaseDataHandler dataHandler = new BaseDataHandler();
    private ActivityDataHandler activityDataHandler = new ActivityDataHandler();
    private AlarmDataHandler alarmDataHandler = new AlarmDataHandler();
    DeviceControllerDataHandler controllerDataHandler = new DeviceControllerDataHandler();
    EcgDataHandler ecgDataHandler = new EcgDataHandler();
    public int flag = 0;
    private HealthDataHandler healthDataHandler = new HealthDataHandler();
    HealthHrDataHandler healthHrDataHandler = new HealthHrDataHandler();
    private LongSitDataHandler longsitDataHandler = new LongSitDataHandler();
    private NoticeDataHandler noticeDataHandler = new NoticeDataHandler();
    private SleepDataHandler sleepDataHandler = new SleepDataHandler();
    private TargeDataHandler targeDataHandler = new TargeDataHandler();

    public static BaseDataHandler getInstance() {
        return dataHandler;
    }

    public void init() {
        this.flag = 0;
    }

    public void addDeviceCallback(DeviceCallback deviceCallback) {
        this.controllerDataHandler.addDeviceCallback(deviceCallback);
    }

    public void removeDeviceCallback(DeviceCallback deviceCallback) {
        this.controllerDataHandler.removeDeviceCallback(deviceCallback);
    }

    public Object handler(byte[] bArr) {
        LogUtil.d(ByteDataConvertUtil.bytesToHexString(bArr));
        if (this.controllerDataHandler.isDeviceControl(bArr)) {
            return this.controllerDataHandler.handlerData(Arrays.copyOf(bArr, 12));
        }
        byte b = bArr[0] & UnsignedBytes.MAX_VALUE;
        if (b == 85) {
            LogUtil.d("接收心电数据");
            this.ecgDataHandler.receiverHistory(bArr);
        } else if (b != 144) {
            if (b != 167) {
                switch (b) {
                    case 129:
                        handlerDeviceInfo(bArr);
                        break;
                    case 130:
                        if (bArr[1] != 16) {
                            LogUtil.d("设置设备状态");
                            break;
                        } else {
                            handlerDeviceState(bArr);
                            break;
                        }
                    case 131:
                        LogUtil.d("设置个人信息");
                        handlerUserInfo(bArr);
                        break;
                    case 132:
                        LogUtil.d("设置时间");
                        break;
                    case 133:
                        if (bArr[1] == 1) {
                            LogUtil.d("设置闹钟");
                            break;
                        } else {
                            return handlerAlarm(bArr);
                        }
                    case 134:
                        if (bArr[1] == 5) {
                            LogUtil.d("设置久坐");
                            handlerLongSit(bArr);
                            break;
                        }
                        break;
                    case 135:
                        if (bArr[1] != 1) {
                            LogUtil.d("设置目标信息");
                            handlerTarge(bArr);
                            break;
                        }
                        break;
                    case 136:
                        if (bArr[1] != 1) {
                            handlerSleep(bArr);
                            break;
                        } else {
                            LogUtil.d("设置自动睡眠信息");
                            break;
                        }
                    case 137:
                        LogUtil.d("设置通知");
                        if (bArr[1] == 4) {
                            handlerNotice(bArr);
                            break;
                        }
                        break;
                    case 138:
                        LogUtil.d("发送消息");
                        break;
                    default:
                        switch (b) {
                            case FLAT_START_GET_CURRENT_STEP /*160*/:
                                return handlerCurrentData(bArr);
                            case 161:
                                if (bArr[2] != 0 || bArr[3] != 7) {
                                    if (bArr[2] == 0 && bArr[3] == 8) {
                                        LogUtil.d("设置心率自动检测开关");
                                        handlerHartOpen(bArr);
                                        break;
                                    } else {
                                        return handlerHeartRate(bArr);
                                    }
                                } else {
                                    LogUtil.d("设置心率区间");
                                    handlerHartRong(bArr);
                                    break;
                                }
                                break;
                            case 162:
                                LogUtil.d("心电数据");
                                this.ecgDataHandler.handler(bArr);
                                break;
                            case FLAT_START_GET_ACTIVITY /*163*/:
                                Object handlerActivity = this.activityDataHandler.handlerActivity(bArr);
                                HandlerBleDataResult handlerBleDataResult = (HandlerBleDataResult) handlerActivity;
                                LogUtil.d("是否完成:" + handlerBleDataResult.isComplete);
                                if (!handlerBleDataResult.isComplete) {
                                    this.flag = 3;
                                    return handlerActivity;
                                }
                                this.flag = 0;
                                return handlerActivity;
                            default:
                                return hanlderHistory(bArr);
                        }
                }
            } else {
                handlerPower(bArr);
            }
        }
        return null;
    }

    private Object hanlderHistory(byte[] bArr) {
        int i = this.flag;
        if (i == 1) {
            LogUtil.d("接收步数历史数据");
            return this.healthDataHandler.receiverHistory(bArr);
        } else if (i == 2) {
            LogUtil.d("接收心率历史数据");
            return this.healthHrDataHandler.receiverHistory(bArr);
        } else if (i == 3) {
            LogUtil.d("接收活动历史数据");
            return this.activityDataHandler.receiverHistory(bArr, this);
        } else if (i != 5) {
            LogUtil.d("接收其他的数据");
            return nextRequest();
        } else {
            LogUtil.d("接收闹钟数据");
            return this.alarmDataHandler.receiverData(bArr);
        }
    }

    private Object nextRequest() {
        HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
        handlerBleDataResult.isComplete = true;
        handlerBleDataResult.hasNext = false;
        this.flag = 0;
        return handlerBleDataResult;
    }

    private Object handlerHeartRate(byte[] bArr) {
        LogUtil.d("*************解析A1 心率数据信息****************");
        byte b = bArr[1];
        if (b == 5) {
            this.flag = 0;
            return this.healthHrDataHandler.handlerCurrent(bArr);
        } else if (b != 2) {
            LogUtil.d("解析心率历史数据.....");
            this.healthHrDataHandler.init(bArr);
            this.flag = 2;
            HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
            handlerBleDataResult.isComplete = false;
            handlerBleDataResult.hasNext = false;
            return handlerBleDataResult;
        } else if (bArr[3] == 3 || bArr[3] == 4) {
            LogUtil.d("设置心率检测成功....");
            return null;
        } else {
            LogUtil.d("无历史心率数据.....");
            HandlerBleDataResult handlerBleDataResult2 = new HandlerBleDataResult();
            handlerBleDataResult2.isComplete = true;
            handlerBleDataResult2.hasNext = false;
            this.flag = 0;
            return handlerBleDataResult2;
        }
    }

    private Object handlerActivity() {
        return new HealthActivity();
    }

    private Object handlerCurrentData(byte[] bArr) {
        LogUtil.d("*************解析A0数据信息****************");
        byte b = bArr[1];
        if (b == 13) {
            LogUtil.d("获取当前步数");
            this.flag = 0;
            return this.healthDataHandler.handlerCurrent(bArr);
        } else if (b == 2) {
            LogUtil.d("无历史数据.....");
            HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
            handlerBleDataResult.isComplete = true;
            handlerBleDataResult.hasNext = false;
            this.flag = 0;
            return handlerBleDataResult;
        } else if (b != 38) {
            return null;
        } else {
            LogUtil.d("*************解析A0步数历史数据信息****************");
            this.healthDataHandler.init(bArr);
            this.flag = 1;
            HandlerBleDataResult handlerBleDataResult2 = new HandlerBleDataResult();
            handlerBleDataResult2.isComplete = false;
            handlerBleDataResult2.hasNext = true;
            return handlerBleDataResult2;
        }
    }

    private Object handlerDeviceState(byte[] bArr) {
        boolean z = true;
        if (bArr[1] == 1) {
            LogUtil.d("设置设备状态成功...");
            return null;
        }
        DeviceState deviceState = SPHelper.getDeviceState();
        byte b = bArr[3];
        byte b2 = bArr[4];
        byte b3 = bArr[5];
        byte b4 = bArr[9];
        byte b5 = bArr[10];
        byte b6 = bArr[11];
        deviceState.screenLight = b;
        deviceState.screenTime = b2;
        deviceState.theme = b3;
        deviceState.upHander = b4;
        SPHelper.getDeviceConfig().isMusic = b5 == 1;
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        if (b6 != 1) {
            z = false;
        }
        deviceConfig.isDisturbMode = z;
        SPHelper.saveDeviceConfig(SPHelper.getDeviceConfig());
        SPHelper.saveDeviceState(deviceState);
        LogUtil.d("获取设备状态成功..." + deviceState);
        LogUtil.dAndSave("获取设备状态:" + deviceState.toString(), Constants.BUG_PATH);
        return deviceState;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(int, int):int}
     arg types: [byte, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int} */
    private Object handlerPower(byte[] bArr) {
        int i;
        LogUtil.d("*************解析电量信息****************");
        byte[] copyOfRange = Arrays.copyOfRange(bArr, 3, bArr.length);
        if (copyOfRange[0] == 255) {
            i = -1;
        } else {
            i = Math.min((int) copyOfRange[0], 100);
        }
        BLEDevice bindBLEDevice = SPHelper.getBindBLEDevice();
        if (bindBLEDevice == null) {
            bindBLEDevice = new BLEDevice();
            bindBLEDevice.power = i;
        } else {
            bindBLEDevice.power = i;
        }
        LogUtil.m5266d("handlerPower：" + bindBLEDevice.toString(), BUG_PATH);
        SPHelper.saveBLEDevice(bindBLEDevice);
        LogUtil.d("power:" + i);
        return Integer.valueOf(i);
    }

    private Object handlerAlarm(byte[] bArr) {
        LogUtil.d("*************解析闹钟信息****************");
        this.alarmDataHandler.initData(bArr);
        this.flag = 5;
        HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
        handlerBleDataResult.isComplete = false;
        handlerBleDataResult.hasNext = true;
        return handlerBleDataResult;
    }

    private void handlerLongSit(byte[] bArr) {
        LogUtil.d("*************解析久坐信息****************");
        this.longsitDataHandler.handlerLongSitData(bArr);
    }

    private void handlerUserInfo(byte[] bArr) {
        LogUtil.d("*************解析个人信息****************");
    }

    private void handlerTarge(byte[] bArr) {
        LogUtil.d("*************解析目标信息****************");
        this.targeDataHandler.handlerTargeData(bArr);
    }

    private void handlerSleep(byte[] bArr) {
        LogUtil.d("*************解析自动睡眠信息****************");
        this.sleepDataHandler.handlerSleepData(bArr);
    }

    private void handlerNotice(byte[] bArr) {
        LogUtil.d("*************解析通知****************");
        this.noticeDataHandler.handlerNoticeData(bArr);
    }

    private void handlerHartRong(byte[] bArr) {
        this.healthDataHandler.setHeartRong(bArr);
    }

    private void handlerHartOpen(byte[] bArr) {
        this.healthDataHandler.handlerHartOpen(bArr);
    }

    private Object handlerDeviceInfo(byte[] bArr) {
        LogUtil.d("*************解析设备信息****************");
        String str = new String(Arrays.copyOfRange(bArr, 3, 11));
        LogUtil.d("deviceBand:" + str);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, 11, 13);
        StringBuilder sb = new StringBuilder();
        sb.append((int) (copyOfRange[0] & UnsignedBytes.MAX_VALUE));
        sb.append(FileUtil.HIDDEN_PREFIX);
        sb.append((int) (copyOfRange[1] & UnsignedBytes.MAX_VALUE));
        StringBuilder sb2 = new StringBuilder();
        for (byte b : Arrays.copyOfRange(bArr, 13, 19)) {
            sb2.append(Integer.toHexString(b & UnsignedBytes.MAX_VALUE).toUpperCase());
            sb2.append(Config.TRACE_TODAY_VISIT_SPLIT);
        }
        sb2.deleteCharAt(sb2.length() - 1);
        BLEDevice bindBLEDevice = SPHelper.getBindBLEDevice();
        if (bindBLEDevice == null) {
            bindBLEDevice = new BLEDevice();
            bindBLEDevice.mDeviceAddress = sb2.toString();
        }
        bindBLEDevice.mDeviceProduct = str;
        bindBLEDevice.mDeviceVersion = sb.toString();
        SPHelper.saveBLEDevice(bindBLEDevice);
        LogUtil.dAndSave(bindBLEDevice.toString(), BUG_PATH);
        return bindBLEDevice;
    }
}
