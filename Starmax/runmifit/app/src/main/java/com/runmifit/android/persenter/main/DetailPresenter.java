package com.wade.fit.persenter.main;

import com.wade.fit.app.AppApplication;
import com.wade.fit.greendao.bean.HealthSport;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.greendao.gen.HealthSportDao;
import com.wade.fit.greendao.gen.HealthSportItemDao;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.model.bean.StepDetailVO;
import com.wade.fit.persenter.sport.BaseTimePresenter;
import com.wade.fit.util.StringUtils;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DetailPresenter extends BaseTimePresenter<IDetailStep> {
    public StepDetailVO getDetailCurrent(DetailTimeType detailTimeType, Date date) {
        currentDate(detailTimeType, date);
        return getDetail();
    }

    public StepDetailVO getDetailCurrent(DetailTimeType detailTimeType) {
        currentDate(detailTimeType);
        return getDetail();
    }

    public StepDetailVO getDetailNext(DetailTimeType detailTimeType) {
        nextDate(detailTimeType);
        return getDetail();
    }

    public StepDetailVO getDetailPre(DetailTimeType detailTimeType) {
        preDate(detailTimeType);
        return getDetail();
    }

    public StepDetailVO getSixMonth() {
        setSixMonth();
        return get();
    }

    public StepDetailVO getYearMonth() {
        setYear();
        return get();
    }

    private void addTo(HealthSport healthSport, HealthSport healthSport2) {
        healthSport.setTotalStepCount(healthSport.getTotalStepCount() + healthSport2.getTotalStepCount());
        healthSport.setTotalCalory(healthSport.getTotalCalory() + healthSport2.getTotalCalory());
        healthSport.setTotalDistance(healthSport.getTotalDistance() + healthSport2.getTotalDistance());
        healthSport.setTotalActiveTime(healthSport.getTotalActiveTime() + healthSport2.getTotalActiveTime());
    }

    private void clear(HealthSport healthSport) {
        healthSport.setTotalStepCount(0);
        healthSport.setTotalCalory(0);
        healthSport.setTotalDistance(0);
        healthSport.setTotalActiveTime(0);
        healthSport.setRemark(null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v11, resolved type: com.wade.fit.greendao.bean.HealthSport} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.wade.fit.model.bean.StepDetailVO get() {
        /*
            r18 = this;
            r0 = r18
            java.util.List r1 = r0.dates
            r1.clear()
            com.wade.fit.app.AppApplication r1 = com.wade.fit.app.AppApplication.getInstance()
            com.wade.fit.greendao.gen.DaoSession r1 = r1.getDaoSession()
            com.wade.fit.greendao.gen.HealthSportDao r1 = r1.getHealthSportDao()
            org.greenrobot.greendao.query.QueryBuilder r1 = r1.queryBuilder()
            org.greenrobot.greendao.Property r2 = com.wade.fit.greendao.gen.HealthSportDao.Properties.Date
            java.util.Date r3 = r0.endDate
            long r3 = r3.getTime()
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            java.util.Date r4 = r0.startDate
            long r4 = r4.getTime()
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            org.greenrobot.greendao.query.WhereCondition r2 = r2.between(r3, r4)
            r3 = 0
            org.greenrobot.greendao.query.WhereCondition[] r4 = new org.greenrobot.greendao.query.WhereCondition[r3]
            org.greenrobot.greendao.query.QueryBuilder r1 = r1.where(r2, r4)
            r2 = 1
            org.greenrobot.greendao.Property[] r4 = new org.greenrobot.greendao.Property[r2]
            org.greenrobot.greendao.Property r5 = com.wade.fit.greendao.gen.HealthSportDao.Properties.Date
            r4[r3] = r5
            org.greenrobot.greendao.query.QueryBuilder r1 = r1.orderDesc(r4)
            org.greenrobot.greendao.query.Query r1 = r1.build()
            java.util.List r1 = r1.list()
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            java.util.Date r5 = r0.endDate
            r4.setTime(r5)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            com.wade.fit.greendao.bean.HealthSport r6 = new com.wade.fit.greendao.bean.HealthSport
            r6.<init>()
            com.wade.fit.greendao.bean.HealthSport r7 = new com.wade.fit.greendao.bean.HealthSport
            r7.<init>()
            r8 = 0
            r9 = 0
        L_0x0065:
            java.util.Date r10 = r4.getTime()
            long r10 = r10.getTime()
            java.util.Date r12 = r0.startDate
            long r12 = r12.getTime()
            int r14 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r14 > 0) goto L_0x026b
            r10 = 0
            int r11 = r4.get(r2)
            r12 = 2
            int r13 = r4.get(r12)
            int r13 = r13 + r2
            r14 = 5
            int r15 = r4.get(r14)
            java.util.Iterator r16 = r1.iterator()
        L_0x008b:
            boolean r17 = r16.hasNext()
            if (r17 == 0) goto L_0x00cb
            java.lang.Object r17 = r16.next()
            r14 = r17
            com.wade.fit.greendao.bean.HealthSport r14 = (com.wade.fit.greendao.bean.HealthSport) r14
            int r3 = r14.getYear()
            if (r3 != r11) goto L_0x00c8
            int r3 = r14.getMonth()
            if (r3 != r13) goto L_0x00c8
            int r3 = r14.getDay()
            if (r3 != r15) goto L_0x00c8
            int r3 = r14.getTotalStepCount()
            r14.setTotalDayStepCount(r3)
            int r3 = r14.getTotalCalory()
            r14.setTotalDayCalory(r3)
            int r3 = r14.getTotalDistance()
            r14.setTotalDayDistance(r3)
            int r3 = r14.getTotalActiveTime()
            r14.setTotalDayActiveTime(r3)
            r10 = r14
        L_0x00c8:
            r3 = 0
            r14 = 5
            goto L_0x008b
        L_0x00cb:
            if (r10 != 0) goto L_0x00d2
            com.wade.fit.greendao.bean.HealthSport r10 = new com.wade.fit.greendao.bean.HealthSport
            r10.<init>()
        L_0x00d2:
            r0.addTo(r7, r10)
            int[] r3 = com.wade.fit.persenter.main.DetailPresenter.C23951.$SwitchMap$com$wade.fit$model$bean$DetailTimeType
            com.wade.fit.model.bean.DetailTimeType r14 = r0.timeType
            int r14 = r14.ordinal()
            r3 = r3[r14]
            if (r3 == r2) goto L_0x024c
            if (r3 == r12) goto L_0x01d3
            r12 = 3
            if (r3 == r12) goto L_0x00eb
            r14 = r1
            r1 = 5
        L_0x00e8:
            r3 = 0
            goto L_0x0265
        L_0x00eb:
            java.lang.String r3 = r6.getRemark()
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 == 0) goto L_0x0102
            java.text.SimpleDateFormat r3 = r0.dateFormat4
            java.util.Date r12 = r4.getTime()
            java.lang.String r3 = r3.format(r12)
            r6.setRemark(r3)
        L_0x0102:
            int r3 = r10.getTotalStepCount()
            if (r3 <= 0) goto L_0x010a
            int r9 = r9 + 1
        L_0x010a:
            r0.addTo(r6, r10)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.util.Date r10 = r4.getTime()
            r14 = r1
            long r1 = r10.getTime()
            r3.append(r1)
            java.lang.String r1 = ","
            r3.append(r1)
            java.util.Date r1 = r0.startDate
            long r1 = r1.getTime()
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            com.wade.fit.util.log.LogUtil.d(r1)
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            java.util.Date r2 = r0.startDate
            r1.setTime(r2)
            r2 = 11
            r3 = 0
            r1.set(r2, r3)
            r2 = 12
            r1.set(r2, r3)
            r2 = 13
            r1.set(r2, r3)
            r2 = 14
            r1.set(r2, r3)
            int r2 = r13 + -1
            boolean r2 = r0.isEndMonth(r11, r2, r15)
            if (r2 != 0) goto L_0x0167
            java.util.Date r2 = r4.getTime()
            java.util.Date r1 = r1.getTime()
            boolean r1 = r0.isDateEquals(r2, r1)
            if (r1 == 0) goto L_0x0248
        L_0x0167:
            java.lang.String r1 = r6.toString()
            com.wade.fit.util.log.LogUtil.d(r1)
            r0.setDateLabel(r8, r4, r13)
            com.wade.fit.greendao.bean.HealthSport r1 = new com.wade.fit.greendao.bean.HealthSport
            r1.<init>()
            if (r9 == 0) goto L_0x01b7
            r1.setItemCount(r9)
            int r2 = r6.getTotalStepCount()
            r1.setTotalStepCount(r2)
            int r2 = r6.getTotalStepCount()
            int r2 = r2 / r9
            r1.setTotalDayStepCount(r2)
            int r2 = r6.getTotalCalory()
            r1.setTotalCalory(r2)
            int r2 = r6.getTotalCalory()
            int r2 = r2 / r9
            r1.setTotalDayCalory(r2)
            int r2 = r6.getTotalDistance()
            r1.setTotalDistance(r2)
            int r2 = r6.getTotalDistance()
            int r2 = r2 / r9
            r1.setTotalDayDistance(r2)
            int r2 = r6.getTotalActiveTime()
            r1.setTotalActiveTime(r2)
            int r2 = r6.getTotalActiveTime()
            int r2 = r2 / r9
            r1.setTotalDayActiveTime(r2)
        L_0x01b7:
            java.lang.String r2 = r6.getRemark()
            r1.setRemark(r2)
            java.lang.String r2 = r1.getRemark()
            com.wade.fit.util.log.LogUtil.d(r2)
            r5.add(r1)
            r0.clear(r6)
            int r8 = r8 + 1
            r1 = 5
            r2 = 1
            r3 = 0
            r9 = 0
            goto L_0x0265
        L_0x01d3:
            r14 = r1
            r0.addTo(r6, r10)
            int r1 = r10.getTotalStepCount()
            if (r1 <= 0) goto L_0x01df
            int r9 = r9 + 1
        L_0x01df:
            r3 = r9
            r1 = 0
            r0.setDateLabel(r8, r4, r1)
            boolean r1 = r0.isWeek(r8, r4)
            if (r1 == 0) goto L_0x0245
            com.wade.fit.greendao.bean.HealthSport r1 = new com.wade.fit.greendao.bean.HealthSport
            r1.<init>()
            if (r3 == 0) goto L_0x0230
            r1.setItemCount(r3)
            int r2 = r6.getTotalStepCount()
            r1.setTotalStepCount(r2)
            int r2 = r6.getTotalStepCount()
            int r2 = r2 / r3
            r1.setTotalDayStepCount(r2)
            int r2 = r6.getTotalCalory()
            r1.setTotalCalory(r2)
            int r2 = r6.getTotalCalory()
            int r2 = r2 / r3
            r1.setTotalDayCalory(r2)
            int r2 = r6.getTotalDistance()
            r1.setTotalDistance(r2)
            int r2 = r6.getTotalDistance()
            int r2 = r2 / r3
            r1.setTotalDayDistance(r2)
            int r2 = r6.getTotalActiveTime()
            r1.setTotalActiveTime(r2)
            int r2 = r6.getTotalActiveTime()
            int r2 = r2 / r3
            r1.setTotalDayActiveTime(r2)
        L_0x0230:
            java.lang.String r2 = r0.getWeekStr(r4)
            r1.setRemark(r2)
            java.lang.String r2 = r1.getRemark()
            com.wade.fit.util.log.LogUtil.d(r2)
            r5.add(r1)
            r0.clear(r6)
            r3 = 0
        L_0x0245:
            int r8 = r8 + 1
            r9 = r3
        L_0x0248:
            r1 = 5
            r2 = 1
            goto L_0x00e8
        L_0x024c:
            r14 = r1
            r3 = 0
            r0.setDateLabel(r8, r4, r3)
            java.text.SimpleDateFormat r1 = r0.dateFormat3
            java.util.Date r2 = r4.getTime()
            java.lang.String r1 = r1.format(r2)
            r10.setRemark(r1)
            r5.add(r10)
            int r8 = r8 + 1
            r1 = 5
            r2 = 1
        L_0x0265:
            r4.add(r1, r2)
            r1 = r14
            goto L_0x0065
        L_0x026b:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "size:"
            r1.append(r2)
            int r2 = r5.size()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.wade.fit.util.log.LogUtil.d(r1)
            java.util.List r1 = r0.dates
            java.lang.Object[] r1 = r1.toArray()
            java.lang.String r1 = java.util.Arrays.toString(r1)
            com.wade.fit.util.log.LogUtil.d(r1)
            com.wade.fit.model.bean.StepDetailVO r1 = new com.wade.fit.model.bean.StepDetailVO
            r1.<init>()
            r1.healthSportList = r5
            java.util.List r2 = r0.dates
            r1.dates = r2
            com.wade.fit.views.MainVO r2 = r18.getByDate()
            r1.mainVO = r2
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
        L_0x02a6:
            int r4 = r5.size()
            if (r3 >= r4) goto L_0x0303
            java.lang.Object r4 = r5.get(r3)
            com.wade.fit.greendao.bean.HealthSport r4 = (com.wade.fit.greendao.bean.HealthSport) r4
            com.wade.fit.greendao.bean.HealthSportItem r6 = new com.wade.fit.greendao.bean.HealthSportItem
            r6.<init>()
            int r8 = r4.getItemCount()
            r6.setItemCount(r8)
            int r8 = r4.getTotalStepCount()
            r6.setStepCount(r8)
            int r8 = r4.getTotalCalory()
            r6.setCalory(r8)
            int r8 = r4.getTotalDistance()
            r6.setDistance(r8)
            int r8 = r4.getTotalActiveTime()
            r6.setActiveTime(r8)
            java.lang.String r8 = r4.getRemark()
            r6.setRemark(r8)
            int r8 = r4.getTotalDayActiveTime()
            r6.setAvgActiveTime(r8)
            int r8 = r4.getTotalDayStepCount()
            r6.setAvgStep(r8)
            int r8 = r4.getTotalDayCalory()
            r6.setAvgCalory(r8)
            int r4 = r4.getTotalDayDistance()
            r6.setAvgDistance(r4)
            r2.add(r6)
            int r3 = r3 + 1
            goto L_0x02a6
        L_0x0303:
            com.wade.fit.views.MainVO r3 = r1.mainVO
            r3.healthSport = r7
            r1.items = r2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.persenter.main.DetailPresenter.get():com.wade.fit.model.bean.StepDetailVO");
    }

    /* renamed from: com.wade.fit.persenter.main.DetailPresenter$1 */
    static /* synthetic */ class C23951 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailTimeType = new int[DetailTimeType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.wade.fit.model.bean.DetailTimeType[] r0 = com.wade.fit.model.bean.DetailTimeType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.persenter.main.DetailPresenter.C23951.$SwitchMap$com$wade.fit$model$bean$DetailTimeType = r0
                int[] r0 = com.wade.fit.persenter.main.DetailPresenter.C23951.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.ONE_MONTH     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.persenter.main.DetailPresenter.C23951.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.SIX_MONTH     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.persenter.main.DetailPresenter.C23951.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.YEAR     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.wade.fit.persenter.main.DetailPresenter.C23951.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.DAY     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.persenter.main.DetailPresenter.C23951.<clinit>():void");
        }
    }

    public StepDetailVO getOneMonth() {
        setOneMonth();
        return get();
    }

    public StepDetailVO getDetail() {
        LogUtil.d("index:" + this.index + ",date:" + this.dateFormat.format(this.currentDate) + "detailTimeType:" + this.timeType.toString());
        StepDetailVO stepDetailVO = new StepDetailVO();
        ArrayList arrayList = new ArrayList();
        stepDetailVO.healthSportList = arrayList;
        int i = C23951.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
        if (i == 1 || i == 2 || i == 3) {
            return get();
        }
        if (i == 4) {
            List list = AppApplication.getInstance().getDaoSession().getHealthSportItemDao().queryBuilder().where(HealthSportItemDao.Properties.Year.eq(Integer.valueOf(this.year)), HealthSportItemDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthSportItemDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthSportItemDao.Properties.Date).build().list();
            HealthSportItem healthSportItem = new HealthSportItem();
            if (list.size() < 144) {
                for (int i2 = 0; i2 < 144 - list.size(); i2++) {
                    list.add(new HealthSportItem());
                }
            }
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            int i7 = 0;
            while (i3 < list.size()) {
                HealthSportItem healthSportItem2 = (HealthSportItem) list.get(i3);
                healthSportItem.setStepCount(healthSportItem.getStepCount() + healthSportItem2.getStepCount());
                healthSportItem.setCalory(healthSportItem.getCalory() + healthSportItem2.getCalory());
                healthSportItem.setDistance(healthSportItem.getDistance() + healthSportItem2.getDistance());
                i4 += healthSportItem2.getStepCount();
                i5 += healthSportItem2.getCalory();
                i6 += healthSportItem2.getDistance();
                if (healthSportItem2.getStepCount() > 0) {
                    healthSportItem.setActiveTime(healthSportItem.getActiveTime() + 10);
                }
                i3++;
                if (i3 % 6 == 0) {
                    HealthSportItem healthSportItem3 = new HealthSportItem();
                    healthSportItem3.setDistance(i6);
                    healthSportItem3.setCalory(i5);
                    healthSportItem3.setStepCount(i4);
                    StringBuilder sb = new StringBuilder();
                    sb.append(StringUtils.format("%02d:00", Integer.valueOf(i7)));
                    sb.append("-");
                    i7++;
                    sb.append(StringUtils.format("%02d:00", Integer.valueOf(i7)));
                    healthSportItem3.setRemark(sb.toString());
                    stepDetailVO.items.add(healthSportItem3);
                    i4 = 0;
                    i5 = 0;
                    i6 = 0;
                }
            }
            stepDetailVO.dates.add("00:00");
            stepDetailVO.dates.add("06:00");
            stepDetailVO.dates.add("12:00");
            stepDetailVO.dates.add("18:00");
            stepDetailVO.dates.add("23:59");
            this.mainVO.healthSport.setTotalStepCount(healthSportItem.getStepCount());
            this.mainVO.healthSport.setTotalCalory(healthSportItem.getCalory());
            this.mainVO.healthSport.setTotalDistance(healthSportItem.getDistance());
            this.mainVO.healthSport.setTotalActiveTime(healthSportItem.getActiveTime());
            List list2 = AppApplication.getInstance().getDaoSession().getHealthSportDao().queryBuilder().where(HealthSportDao.Properties.Year.eq(Integer.valueOf(this.year)), HealthSportDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthSportDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthSportDao.Properties.Date).build().list();
            if (list2 != null && list2.size() > 0) {
                arrayList.add(list2.get(0));
            }
        }
        LogUtil.d("size:" + stepDetailVO.items.size());
        LogUtil.d(Arrays.toString(stepDetailVO.items.toArray()));
        LogUtil.d(Arrays.toString(stepDetailVO.dates.toArray()));
        stepDetailVO.mainVO = this.mainVO;
        return stepDetailVO;
    }
}
