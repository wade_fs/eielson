package com.wade.fit.ui.mine.activity;

import android.os.Build;
import android.widget.Button;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.DeviceState;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.views.dialog.WheelViewDialog;

/* renamed from: com.wade.fit.ui.mine.activity.PersonUnitActivity */
public class PersonUnitActivity extends BaseActivity {
    Button btnSave;
    private DeviceState deviceState;
    TextView tvBrithday;
    TextView tvFt;
    TextView tvKm;
    UserBean userBean;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_person_unit;
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.titleBack.setVisibility(View.GONE);
        this.deviceState = SPHelper.getDeviceState();
        this.layoutTitle.setBackgroundColor(getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(9216);
        }
        this.bgView.setVisibility(View.GONE);
        this.userBean = AppApplication.getInstance().getUserBean();
        TextView textView = this.tvBrithday;
        textView.setText(this.userBean.getYear() + "-" + this.userBean.getMonth() + "-" + this.userBean.getDay());
    }

    /* access modifiers changed from: package-private */
    public void toNext() {
        AppApplication.getInstance().setUserBean(this.userBean);
        SPHelper.saveUserBean(this.userBean);
        IntentUtil.goToActivity(this, PersonInfoActivity.class);
    }

    /* access modifiers changed from: package-private */
    public void setUserUnitKm() {
        this.userBean.setUnit(0);
        this.deviceState.unit = 0;
        this.tvKm.setBackgroundResource(R.drawable.unit_select_bg);
        this.tvKm.setTextColor(getResources().getColor(R.color.white));
        this.tvFt.setBackgroundResource(R.drawable.unit_nolmrl_bg);
        this.tvFt.setTextColor(getResources().getColor(R.color.unit_select_color));
    }

    /* access modifiers changed from: package-private */
    public void setUserUnitFt() {
        this.userBean.setUnit(1);
        this.deviceState.unit = 1;
        this.tvFt.setBackgroundResource(R.drawable.unit_select_bg);
        this.tvFt.setTextColor(getResources().getColor(R.color.white));
        this.tvKm.setBackgroundResource(R.drawable.unit_nolmrl_bg);
        this.tvKm.setTextColor(getResources().getColor(R.color.unit_select_color));
    }

    /* access modifiers changed from: package-private */
    public void selectBrithday() {
        DialogHelperNew.showWheelBirthDayDialog(this, new int[]{this.userBean.getYear(), this.userBean.getMonth(), this.userBean.getDay()}, new WheelViewDialog.OnSelectClick() {
            /* class com.wade.fit.ui.mine.activity.$$Lambda$PersonUnitActivity$lnsp92VFLyvhzGpIWI5IyhfWSFo */

            public final void onSelect(int i, int i2, int i3) {
                PersonUnitActivity.this.lambda$selectBrithday$0$PersonUnitActivity(i, i2, i3);
            }
        });
    }

    public /* synthetic */ void lambda$selectBrithday$0$PersonUnitActivity(int i, int i2, int i3) {
        this.userBean.setYear(i);
        this.userBean.setMonth(i2);
        this.userBean.setDay(i3);
        UserBean userBean2 = this.userBean;
        userBean2.setBirthday(i + "-" + i2 + "-" + i3);
        TextView textView = this.tvBrithday;
        textView.setText(this.userBean.getYear() + getResources().getString(R.string.year) + this.userBean.getMonth() + getResources().getString(R.string.month) + this.userBean.getDay() + getResources().getString(R.string.day));
    }
}
