package com.wade.fit.service;

import android.app.Activity;
import no.nordicsemi.android.dfu.DfuBaseService;

public class DFUService extends DfuBaseService {
    /* access modifiers changed from: protected */
    public Class<? extends Activity> getNotificationTarget() {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean isDebug() {
        return true;
    }
}
