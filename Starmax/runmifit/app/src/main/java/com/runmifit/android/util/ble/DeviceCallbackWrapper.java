package com.wade.fit.util.ble;

public class DeviceCallbackWrapper implements DeviceCallback {
    public void addVol() {
    }

    public void answerRingingCall() {
    }

    public void camare() {
    }

    public void endRingingCall() {
    }

    public void enterCamare() {
    }

    public void exitCamare() {
    }

    public void exitMusic() {
    }

    public void findPhone() {
    }

    public void musicControl() {
    }

    public void nextMusic() {
    }

    public void preMusic() {
    }

    public void sos() {
    }

    public void subVol() {
    }
}
