package com.wade.fit.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.baidu.mobstat.Config;
import com.wade.fit.R;

public class CountDownProgressBar extends View {
    private float alphaAngle;
    private ValueAnimator animator;
    private int centerTextColor;
    private int centerTextSize;
    private Paint circlePaint;
    private int circleWidth;
    private int[] colorArray;
    private int currentValue;
    private int duration;
    private int firstColor;
    private boolean isShowGradient;
    private OnFinishListener listener;
    private int maxValue;
    private int secondColor;
    private Paint textPaint;

    public interface OnFinishListener {
        void onFinish();
    }

    public CountDownProgressBar(Context context) {
        this(context, null);
    }

    public CountDownProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CountDownProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.maxValue = 200;
        this.centerTextColor = -16776961;
        this.isShowGradient = false;
        this.colorArray = new int[]{Color.parseColor("#2773FF"), Color.parseColor("#27C0D2"), Color.parseColor("#40C66E")};
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CountDownProgressBar, i, 0);
        int indexCount = obtainStyledAttributes.getIndexCount();
        for (int i2 = 0; i2 < indexCount; i2++) {
            int index = obtainStyledAttributes.getIndex(i2);
            switch (index) {
                case 1:
                    this.centerTextColor = obtainStyledAttributes.getColor(index, -16776961);
                    break;
                case 2:
                    this.centerTextSize = obtainStyledAttributes.getDimensionPixelSize(index, (int) dip2px(40.0f));
                    break;
                case 3:
                    this.circleWidth = obtainStyledAttributes.getDimensionPixelSize(index, (int) dip2px(6.0f));
                    break;
                case 4:
                    this.firstColor = obtainStyledAttributes.getColor(index, -3355444);
                    break;
                case 5:
                    this.isShowGradient = obtainStyledAttributes.getBoolean(index, false);
                    break;
                case 6:
                    this.secondColor = obtainStyledAttributes.getColor(index, -16776961);
                    break;
            }
        }
        obtainStyledAttributes.recycle();
        this.circlePaint = new Paint();
        this.circlePaint.setAntiAlias(true);
        this.circlePaint.setDither(true);
        this.circlePaint.setStrokeWidth((float) this.circleWidth);
        this.textPaint = new Paint();
        this.textPaint.setAntiAlias(true);
        this.textPaint.setDither(true);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = getResources().getDisplayMetrics().widthPixels;
        int i4 = getResources().getDisplayMetrics().heightPixels;
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        int min = Math.min(i3, size);
        int min2 = Math.min(i4, size2);
        setMeasuredDimension(Math.min(min, min2), Math.min(min, min2));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int width = getWidth() / 2;
        drawCircle(canvas, width, width - (this.circleWidth / 2));
        drawText(canvas, width);
    }

    private void drawCircle(Canvas canvas, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        this.circlePaint.setShader(null);
        this.circlePaint.setColor(this.firstColor);
        this.circlePaint.setStyle(Paint.Style.STROKE);
        float f = (float) i3;
        canvas.drawCircle(f, f, (float) i4, this.circlePaint);
        float f2 = (float) (i3 - i4);
        float f3 = (float) (i3 + i4);
        RectF rectF = new RectF(f2, f2, f3, f3);
        if (this.isShowGradient) {
            int i5 = this.circleWidth;
            this.circlePaint.setShader(new LinearGradient((float) i5, (float) i5, (float) (getMeasuredWidth() - this.circleWidth), (float) (getMeasuredHeight() - this.circleWidth), this.colorArray, (float[]) null, Shader.TileMode.MIRROR));
        }
        this.circlePaint.setColor(this.secondColor);
        this.circlePaint.setStrokeCap(Paint.Cap.ROUND);
        this.alphaAngle = ((((float) this.currentValue) * 360.0f) / ((float) this.maxValue)) * 1.0f;
        canvas.drawArc(rectF, -90.0f, this.alphaAngle, false, this.circlePaint);
    }

    public void reset() {
        this.currentValue = 0;
        ValueAnimator valueAnimator = this.animator;
        if (valueAnimator != null) {
            valueAnimator.start();
            this.animator.cancel();
        }
        this.listener = null;
    }

    private void drawText(Canvas canvas, int i) {
        String str;
        Object obj;
        Object obj2;
        int i2 = this.maxValue;
        int i3 = this.currentValue;
        int i4 = ((i2 - i3) * (this.duration / 1000)) / i2;
        if (i2 == i3) {
            this.textPaint.setTextSize((float) this.centerTextSize);
            str = "完成";
        } else {
            StringBuilder sb = new StringBuilder();
            int i5 = i4 / 60;
            if (i5 < 10) {
                obj = "0" + i5;
            } else {
                obj = Integer.valueOf(i5);
            }
            sb.append(obj);
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            int i6 = i4 % 60;
            if (i6 < 10) {
                obj2 = "0" + i6;
            } else {
                obj2 = Integer.valueOf(i6);
            }
            sb.append(obj2);
            str = sb.toString();
            Paint paint = this.textPaint;
            int i7 = this.centerTextSize;
            paint.setTextSize((float) (i7 + (i7 / 3)));
        }
        this.textPaint.setTextAlign(Paint.Align.CENTER);
        this.textPaint.setColor(this.centerTextColor);
        this.textPaint.setStrokeWidth(0.0f);
        this.textPaint.getTextBounds(str, 0, str.length(), new Rect());
        Paint.FontMetricsInt fontMetricsInt = this.textPaint.getFontMetricsInt();
        canvas.drawText(str, (float) i, (float) ((((fontMetricsInt.bottom - fontMetricsInt.top) / 2) + i) - fontMetricsInt.bottom), this.textPaint);
    }

    public void setCircleWidth(int i) {
        this.circleWidth = (int) TypedValue.applyDimension(1, (float) i, getResources().getDisplayMetrics());
        this.circlePaint.setStrokeWidth((float) this.circleWidth);
        invalidate();
    }

    public void setFirstColor(int i) {
        this.firstColor = i;
        this.circlePaint.setColor(this.firstColor);
        invalidate();
    }

    public void setSecondColor(int i) {
        this.secondColor = i;
        this.circlePaint.setColor(this.secondColor);
        invalidate();
    }

    public void setColorArray(int[] iArr) {
        this.colorArray = iArr;
        invalidate();
    }

    public void setDuration(int i, OnFinishListener onFinishListener) {
        this.listener = onFinishListener;
        this.duration = i + 1000;
        ValueAnimator valueAnimator = this.animator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        } else {
            this.animator = ValueAnimator.ofInt(0, this.maxValue);
            this.animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                /* class com.wade.fit.views.$$Lambda$CountDownProgressBar$BiCcfX_hEyvC4bBiYKm5yl_fmPo */

                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    CountDownProgressBar.this.lambda$setDuration$0$CountDownProgressBar(valueAnimator);
                }
            });
            this.animator.setInterpolator(new LinearInterpolator());
        }
        this.animator.setDuration((long) i);
        this.animator.start();
    }

    public /* synthetic */ void lambda$setDuration$0$CountDownProgressBar(ValueAnimator valueAnimator) {
        OnFinishListener onFinishListener;
        this.currentValue = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        invalidate();
        if (this.maxValue == this.currentValue && (onFinishListener = this.listener) != null) {
            this.currentValue = 0;
            onFinishListener.onFinish();
        }
    }

    public void setOnFinishListener(OnFinishListener onFinishListener) {
        this.listener = onFinishListener;
    }

    public static int px2dip(int i) {
        return (int) ((((float) i) / Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    public static float dip2px(float f) {
        return (f * Resources.getSystem().getDisplayMetrics().density) + 0.5f;
    }
}
