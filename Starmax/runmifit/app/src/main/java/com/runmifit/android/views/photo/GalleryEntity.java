package com.wade.fit.views.photo;

public class GalleryEntity {
    private int count;
    private int gallery_id;
    private String gallery_name;

    /* renamed from: id */
    private int f5266id;
    private String path;

    public GalleryEntity() {
    }

    public GalleryEntity(int i, String str, int i2, String str2, int i3) {
        this.f5266id = i;
        this.path = str;
        this.gallery_id = i2;
        this.gallery_name = str2;
        this.count = i3;
    }

    public int getId() {
        return this.f5266id;
    }

    public void setId(int i) {
        this.f5266id = i;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String str) {
        this.path = str;
    }

    public int getGallery_id() {
        return this.gallery_id;
    }

    public void setGallery_id(int i) {
        this.gallery_id = i;
    }

    public String getGallery_name() {
        return this.gallery_name;
    }

    public void setGallery_name(String str) {
        this.gallery_name = str;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int i) {
        this.count = i;
    }

    public String toString() {
        return "GalleryEntity [id=" + this.f5266id + ", path=" + this.path + ", gallery_id=" + this.gallery_id + ", gallery_name=" + this.gallery_name + ", count=" + this.count + "]";
    }
}
