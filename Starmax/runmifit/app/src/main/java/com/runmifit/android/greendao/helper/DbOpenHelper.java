package com.wade.fit.greendao.helper;

import android.content.Context;
import com.wade.fit.greendao.gen.DaoMaster;
import com.wade.fit.greendao.gen.ECGItemInfoDao;
import com.wade.fit.greendao.gen.EcgRecordInfoDao;
import com.wade.fit.greendao.gen.HealthActivityDao;
import com.wade.fit.greendao.gen.HealthGpsItemDao;
import com.wade.fit.greendao.gen.HealthHeartRateDao;
import com.wade.fit.greendao.gen.HealthHeartRateItemDao;
import com.wade.fit.greendao.gen.HealthSleepDao;
import com.wade.fit.greendao.gen.HealthSleepItemDao;
import com.wade.fit.greendao.gen.HealthSportDao;
import com.wade.fit.greendao.gen.HealthSportItemDao;
import com.wade.fit.greendao.helper.MigrationHelper;
import org.greenrobot.greendao.database.Database;

public class DbOpenHelper extends DaoMaster.DevOpenHelper {
    public DbOpenHelper(Context context, String str) {
        super(context, str);
    }

    public void onUpgrade(Database database, int i, int i2) {
        if (i < i2) {
            MigrationHelper.migrate(database, new MigrationHelper.ReCreateAllTableListener() {
                /* class com.wade.fit.greendao.helper.DbOpenHelper.C15271 */

                public void onCreateAllTables(Database database, boolean z) {
                    DaoMaster.createAllTables(database, z);
                }

                public void onDropAllTables(Database database, boolean z) {
                    DaoMaster.dropAllTables(database, z);
                }
            }, ECGItemInfoDao.class, EcgRecordInfoDao.class, HealthSleepItemDao.class, HealthSportItemDao.class, HealthSleepDao.class, HealthSportDao.class, HealthActivityDao.class, HealthGpsItemDao.class, HealthHeartRateItemDao.class, HealthHeartRateDao.class);
        }
    }
}
