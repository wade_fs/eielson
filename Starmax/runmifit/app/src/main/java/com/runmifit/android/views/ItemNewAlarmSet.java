package com.wade.fit.views;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.util.TimeUtil;

public class ItemNewAlarmSet extends RelativeLayout {
    private TextView mAlarmSetTitleTv = ((TextView) findViewById(R.id.alarm_set_title_tv));
    private TextView mAlarmSetTypeTv = ((TextView) findViewById(R.id.alarm_set_type_tv));
    private ValueStateTextView mValueState = ((ValueStateTextView) findViewById(R.id.value_state));

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.wade.fit.views.ItemNewAlarmSet, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ItemNewAlarmSet(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        LayoutInflater.from(context).inflate((int) R.layout.item_new_alarm_set, (ViewGroup) this, true);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ItemLableValue);
        String string = obtainStyledAttributes.getString(8);
        obtainStyledAttributes.recycle();
        this.mAlarmSetTitleTv.setText(string);
    }

    public void setValueState(boolean z) {
        this.mValueState.setOpen(z);
    }

    public void setAlartRemind(String str) {
        if (str != null) {
            this.mAlarmSetTypeTv.setText(str);
        } else {
            this.mAlarmSetTypeTv.setText("");
        }
    }

    public void setAlartRepeat(boolean[] zArr, String[] strArr) {
        String str = "";
        if (zArr != null && zArr.length > 0 && strArr != null && strArr.length > 0) {
            String str2 = str;
            for (int i = 0; i < zArr.length; i++) {
                if (zArr[i]) {
                    if (str2.equals(str)) {
                        str2 = strArr[i];
                    } else {
                        str2 = str2 + "," + strArr[i];
                    }
                }
            }
            str = str2;
        }
        this.mAlarmSetTypeTv.setText(str);
    }

    public void setAlartTime(int i, int i2, Activity activity) {
        this.mAlarmSetTypeTv.setText(TimeUtil.timeFormatter(i, i2, TimeUtil.is24Hour(activity), new String[]{AppApplication.getInstance().getResources().getString(R.string.am), AppApplication.getInstance().getResources().getString(R.string.pm)}));
    }

    public void setAlartTime(int i, int i2, String str) {
        TextView textView = this.mAlarmSetTypeTv;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2str(i + ""));
        sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
        sb.append(str2str(i2 + ""));
        textView.setText(sb.toString());
    }

    private String str2str(String str) {
        if (str.length() > 1) {
            return str;
        }
        return "0" + str;
    }
}
