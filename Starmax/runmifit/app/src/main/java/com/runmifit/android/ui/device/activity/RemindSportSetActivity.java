package com.wade.fit.ui.device.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.LongSit;
import com.wade.fit.model.bean.LongSitBean;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.ble.ByteDataConvertUtil;
import com.wade.fit.views.ItemToggleLayout;
import com.wade.fit.views.dialog.TimePickerDialog;
import com.wade.fit.views.dialog.WheelViewDialog;

/* renamed from: com.wade.fit.ui.device.activity.RemindSportSetActivity */
public class RemindSportSetActivity extends BaseActivity {
    private final int REQUEST_WEEKDAY_CODE = 0;
    /* access modifiers changed from: private */
    public int endHour;
    /* access modifiers changed from: private */
    public int endMin;
    TimePickerDialog endTimePickerDialog;
    /* access modifiers changed from: private */
    public LongSit longSit;
    LinearLayout remindSportSetLl;
    ItemToggleLayout remindSportSwitch;
    RelativeLayout rlRemindDelay;
    RelativeLayout rlRemindTime;
    /* access modifiers changed from: private */
    public int startHour;
    /* access modifiers changed from: private */
    public int startMin;
    TimePickerDialog startTimePickerDialog;
    TextView tvRemindDelay;
    TextView tvRemindEndTime;
    TextView tvRemindStartTime;
    TextView tvRemindTime;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_remind_sport_set;
    }

    public void initView() {
        initDatePicker();
        this.titleName.setText(getResources().getString(R.string.remind_sport));
        this.rightText.setText(getResources().getString(R.string.save));
        this.rightText.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$RemindSportSetActivity$vImIMq9zP9xAb_0qATigDSwymD0 */

            public final void onClick(View view) {
                RemindSportSetActivity.this.lambda$initView$0$RemindSportSetActivity(view);
            }
        });
        this.longSit = SPHelper.getLongSit();
        if (this.longSit == null) {
            this.longSit = new LongSit();
            this.longSit.setStartHour(9);
            this.longSit.setEndHour(18);
            this.longSit.setWeeks(new boolean[]{true, true, true, true, true, true, true});
        }
        updateRemindSwitch();
        setWeeksString(this.longSit.getWeeks());
        this.startHour = this.longSit.getStartHour();
        this.startMin = this.longSit.getStartMinute();
        this.endHour = this.longSit.getEndHour();
        this.endMin = this.longSit.getEndMinute();
        this.remindSportSwitch.setOpen(this.longSit.isOnOff());
        TextView textView = this.tvRemindDelay;
        textView.setText(this.longSit.getInterval() + " " + getResources().getString(R.string.unit_minute));
        this.startTimePickerDialog.setTime(this.longSit.getStartHour(), this.longSit.getStartMinute());
        this.tvRemindStartTime.setText(this.startTimePickerDialog.getTime());
        this.endTimePickerDialog.setTime(this.longSit.getEndHour(), this.longSit.getEndMinute());
        this.tvRemindEndTime.setText(this.endTimePickerDialog.getTime());
        initEvent();
    }

    public /* synthetic */ void lambda$initView$0$RemindSportSetActivity(View view) {
        onSave();
    }

    private void setWeeksString(boolean[] zArr) {
        StringBuffer stringBuffer = new StringBuffer();
        String[] stringArray = getResources().getStringArray(R.array.weekDay);
        int i = 0;
        for (int i2 = 0; i2 < zArr.length; i2++) {
            if (i2 == 6) {
                if (zArr[i2]) {
                    i++;
                    stringBuffer.append(stringArray[0]);
                    stringBuffer.append(",");
                }
            } else if (zArr[i2]) {
                i++;
                stringBuffer.append(stringArray[i2 + 1]);
                stringBuffer.append(",");
            }
        }
        if (i <= 0) {
            this.tvRemindTime.setText(getResources().getString(R.string.remind_time_none));
        } else if (i == 7) {
            this.tvRemindTime.setText(getResources().getString(R.string.alarm_every_day));
        } else {
            this.tvRemindTime.setText(stringBuffer.toString().substring(0, stringBuffer.length() - 1));
        }
    }

    private void updateRemindSwitch() {
        if (this.longSit.isOnOff()) {
            this.remindSportSetLl.setVisibility(View.VISIBLE);
        } else {
            this.remindSportSetLl.setVisibility(View.GONE);
        }
    }

    private void initEvent() {
        this.remindSportSwitch.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$RemindSportSetActivity$Ab4TDS058HQQfKsadimA7MSBDs */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                RemindSportSetActivity.this.lambda$initEvent$1$RemindSportSetActivity(itemToggleLayout, z);
            }
        });
    }

    public /* synthetic */ void lambda$initEvent$1$RemindSportSetActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        this.longSit.setOnOff(z);
        updateRemindSwitch();
    }

    private void onSave() {
        this.longSit.setStartHour(this.startHour);
        this.longSit.setStartMinute(this.startMin);
        this.longSit.setEndMinute(this.endMin);
        this.longSit.setEndHour(this.endHour);
        LongSitBean longSitBean = new LongSitBean();
        longSitBean.startTime = this.startHour;
        longSitBean.endTime = this.endHour;
        longSitBean.period = this.longSit.getInterval();
        longSitBean.state = this.remindSportSwitch.isOpen() ? 1 : 0;
        byte[] bArr = new byte[(this.longSit.getWeeks().length + 1)];
        for (int i = 0; i < this.longSit.getWeeks().length; i++) {
            bArr[i] = this.longSit.getWeeks()[(this.longSit.getWeeks().length + -1) - i] ? (byte) 1 : 0;
        }
        longSitBean.repeat = ByteDataConvertUtil.Bit8Array2Int(bArr);
        BleSdkWrapper.setLongSit(longSitBean, null);
        SPHelper.saveLongSit(this.longSit);
        finish();
    }

    /* access modifiers changed from: package-private */
    public void selecStatrTime() {
        this.startTimePickerDialog.setTime(this.startHour, this.startMin);
        this.startTimePickerDialog.show();
    }

    /* access modifiers changed from: package-private */
    public void selecEndTime() {
        this.endTimePickerDialog.setTime(this.endHour, this.endMin);
        this.endTimePickerDialog.show();
    }

    /* access modifiers changed from: package-private */
    public void SelectRemindDelay() {
        DialogHelperNew.showWheelRemindDelayDialog(this, this.longSit.getInterval(), new WheelViewDialog.OnSelectClick() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$RemindSportSetActivity$xEAS6yYTchUOLvJC_2H9k6tnpRY */

            public final void onSelect(int i, int i2, int i3) {
                RemindSportSetActivity.this.lambda$SelectRemindDelay$2$RemindSportSetActivity(i, i2, i3);
            }
        });
    }

    public /* synthetic */ void lambda$SelectRemindDelay$2$RemindSportSetActivity(int i, int i2, int i3) {
        int i4 = i * 5;
        if ((((this.endHour - this.startHour) * 60) + this.endMin) - this.startMin < i4) {
            showToast(getResources().getString(R.string.remind_tips2));
            return;
        }
        this.longSit.setInterval(i4);
        TextView textView = this.tvRemindDelay;
        textView.setText(this.longSit.getInterval() + " " + getResources().getString(R.string.unit_minute));
    }

    /* access modifiers changed from: package-private */
    public void SelectRemindTime() {
        Bundle bundle = new Bundle();
        bundle.putBooleanArray("isSelect", this.longSit.getWeeks());
        IntentUtil.goToActivityForResult(this, SelectWeekDayActivity.class, bundle, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == 200 && i == 0) {
            this.longSit.setWeeks(intent.getExtras().getBooleanArray("isSelect"));
            setWeeksString(this.longSit.getWeeks());
        }
    }

    private void initDatePicker() {
        this.startTimePickerDialog = new TimePickerDialog(this);
        this.startTimePickerDialog.setCallback(new TimePickerDialog.OnClickCallback() {
            /* class com.wade.fit.ui.device.activity.RemindSportSetActivity.C24741 */

            public void onCancel() {
                RemindSportSetActivity.this.startTimePickerDialog.dismiss();
            }

            public void onSure(String str, int i, int i2) {
                if (i > RemindSportSetActivity.this.endHour || (i == RemindSportSetActivity.this.endHour && i2 >= RemindSportSetActivity.this.endMin)) {
                    RemindSportSetActivity remindSportSetActivity = RemindSportSetActivity.this;
                    remindSportSetActivity.showToast(remindSportSetActivity.getResources().getString(R.string.remind_tips1));
                } else if ((((RemindSportSetActivity.this.endHour - i) * 60) + RemindSportSetActivity.this.endMin) - i2 < RemindSportSetActivity.this.longSit.getInterval()) {
                    RemindSportSetActivity remindSportSetActivity2 = RemindSportSetActivity.this;
                    remindSportSetActivity2.showToast(remindSportSetActivity2.getResources().getString(R.string.remind_tips2));
                } else {
                    RemindSportSetActivity.this.startTimePickerDialog.dismiss();
                    RemindSportSetActivity.this.tvRemindStartTime.setText(str);
                    int unused = RemindSportSetActivity.this.startHour = i;
                    int unused2 = RemindSportSetActivity.this.startMin = i2;
                }
            }
        });
        this.endTimePickerDialog = new TimePickerDialog(this);
        this.endTimePickerDialog.setCallback(new TimePickerDialog.OnClickCallback() {
            /* class com.wade.fit.ui.device.activity.RemindSportSetActivity.C24752 */

            public void onCancel() {
                RemindSportSetActivity.this.endTimePickerDialog.dismiss();
            }

            public void onSure(String str, int i, int i2) {
                if (i < RemindSportSetActivity.this.startHour || (i == RemindSportSetActivity.this.startHour && i2 <= RemindSportSetActivity.this.startMin)) {
                    RemindSportSetActivity remindSportSetActivity = RemindSportSetActivity.this;
                    remindSportSetActivity.showToast(remindSportSetActivity.getResources().getString(R.string.remind_tips1));
                } else if ((((i - RemindSportSetActivity.this.startHour) * 60) + i2) - RemindSportSetActivity.this.startMin < RemindSportSetActivity.this.longSit.getInterval()) {
                    RemindSportSetActivity remindSportSetActivity2 = RemindSportSetActivity.this;
                    remindSportSetActivity2.showToast(remindSportSetActivity2.getResources().getString(R.string.remind_tips2));
                } else {
                    RemindSportSetActivity.this.endTimePickerDialog.dismiss();
                    RemindSportSetActivity.this.tvRemindEndTime.setText(str);
                    int unused = RemindSportSetActivity.this.endHour = i;
                    int unused2 = RemindSportSetActivity.this.endMin = i2;
                }
            }
        });
    }
}
