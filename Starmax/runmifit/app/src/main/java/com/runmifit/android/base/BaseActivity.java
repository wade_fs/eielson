package com.wade.fit.base;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.baidu.mobstat.StatService;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.ui.main.activity.WelcomeActivity;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.ImageLoadUtil;
import com.wade.fit.util.PermissionUtil;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.StatusBarUtil;
import com.wade.fit.util.ToastUtil;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class BaseActivity extends AppCompatActivity implements PermissionUtil.RequsetResult {
    protected final String TAG = getClass().getSimpleName();
    protected RelativeLayout bar_bg;
    protected View bgView;
    private Bundle bundle;
    protected ImageLoadUtil imageLoadUtil;
    protected RelativeLayout layoutTitle;
    protected FrameLayout mFlContent;
    protected View mRoodView;
    private PermissionUtil permissionUtil;
    protected ImageView rightImg;
    protected TextView rightText;
    protected ImageView titleBack;
    protected RelativeLayout titleBg;
    protected TextView titleName;

    /* access modifiers changed from: protected */
    public abstract int getContentView();

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
    }

    /* access modifiers changed from: protected */
    public void initView() {
    }

    public void requestPermissionsFail(int i) {
    }

    public void requestPermissionsSuccess(int i) {
    }

    public void onCreate(Bundle bundle2) {
        if (Build.VERSION.SDK_INT == 26 && isTranslucentOrFloating()) {
            fixOrientation();
        }
        super.onCreate(bundle2);
        this.bundle = bundle2;
        initActivity();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.bundle != null) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            intent.setFlags(268468224);
            startActivity(intent);
        }
    }

    private boolean fixOrientation() {
        try {
            Field declaredField = Activity.class.getDeclaredField("mActivityInfo");
            declaredField.setAccessible(true);
            ((ActivityInfo) declaredField.get(this)).screenOrientation = -1;
            declaredField.setAccessible(false);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setRequestedOrientation(int i) {
        if (Build.VERSION.SDK_INT != 26 || !isTranslucentOrFloating()) {
            super.setRequestedOrientation(i);
        }
    }

    private boolean isTranslucentOrFloating() {
        boolean z = false;
        try {
            TypedArray obtainStyledAttributes = obtainStyledAttributes((int[]) Class.forName("com.android.internal.R$styleable").getField("Window").get(null));
            Method method = ActivityInfo.class.getMethod("isTranslucentOrFloating", TypedArray.class);
            method.setAccessible(true);
            boolean booleanValue = ((Boolean) method.invoke(null, obtainStyledAttributes)).booleanValue();
            try {
                method.setAccessible(false);
                return booleanValue;
            } catch (Exception e) {
                boolean z2 = booleanValue;
                e = e;
                z = z2;
                e.printStackTrace();
                return z;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return z;
        }
    }

    public void initActivity() {
        setContentView((int) R.layout.layout_base_title);
        this.titleBack = (ImageView) findViewById(R.id.base_title_back);
        this.titleName = (TextView) findViewById(R.id.base_title_name);
        this.rightText = (TextView) findViewById(R.id.base_right_text);
        this.titleBg = (RelativeLayout) findViewById(R.id.base_title_bg);
        this.mFlContent = (FrameLayout) findViewById(R.id.base_content);
        this.layoutTitle = (RelativeLayout) findViewById(R.id.layoutTitle);
        this.bgView = findViewById(R.id.bgView);
        this.titleBack.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.base.$$Lambda$BaseActivity$CC3KoVqfmLbq_mFzcxEQuO7XQuQ */

            public final void onClick(View view) {
                BaseActivity.this.lambda$initActivity$0$BaseActivity(view);
            }
        });
        this.bar_bg = (RelativeLayout) findViewById(R.id.bar_bg);
        this.rightImg = (ImageView) findViewById(R.id.rightImg);
        onViewCreate();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.bar_bg.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = ScreenUtil.getStatusHeight(this);
        this.bar_bg.setLayoutParams(layoutParams);
        StatusBarUtil.setTranslucentForImageView(this, 0, this.titleBg);
        initView();
        AppApplication.addActivity(this);
        this.permissionUtil = new PermissionUtil();
        this.permissionUtil.setRequsetResult(this);
        EventBusHelper.register(this);
        this.layoutTitle.setBackgroundColor(getResources().getColor(R.color.white));
        this.titleBack.setImageResource(R.mipmap.back_black);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(9216);
        }
    }

    public /* synthetic */ void lambda$initActivity$0$BaseActivity(View view) {
        onBackPressed();
    }

    public boolean checkSelfPermission(String... strArr) {
        return PermissionUtil.checkSelfPermission(strArr);
    }

    public void requestPermissions(int i, String... strArr) {
        PermissionUtil.requestPermissions(this, i, strArr);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public final void handleMessageInner(BaseMessage baseMessage) {
        handleMessage(baseMessage);
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        this.permissionUtil.onRequestPermissionsResult(i, strArr, iArr);
    }

    /* access modifiers changed from: protected */
    public void onViewCreate() {
        this.mRoodView = LayoutInflater.from(this).inflate(getContentView(), (ViewGroup) null);
        ButterKnife.bind(this, this.mRoodView);
        this.mFlContent.addView(this.mRoodView);
    }

    /* access modifiers changed from: protected */
    public void showToast(String str) {
        runOnUiThread(new Runnable(str) {
            /* class com.wade.fit.base.$$Lambda$BaseActivity$EsdVS9rLWsz3WduBAeICQDf9NEY */
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void run() {
                ToastUtil.showToast(this.f$0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AppApplication.removeActivity(this);
        EventBusHelper.unregister(this);
        super.onDestroy();
    }

    public ImageLoadUtil getImageLoader() {
        if (this.imageLoadUtil == null) {
            this.imageLoadUtil = new ImageLoadUtil();
        }
        return this.imageLoadUtil;
    }

    /* access modifiers changed from: protected */
    public void setBaiduStat(String str, String str2) {
        StatService.onEvent(this, str, str2);
    }
}
