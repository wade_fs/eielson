package com.wade.fit.ui.sport.activity;

import android.content.DialogInterface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthGpsItem;
import com.wade.fit.greendao.gen.HealthActivityDao;
import com.wade.fit.greendao.gen.HealthGpsItemDao;
import com.wade.fit.ui.sport.adapter.ActivityItemAdapter;
import com.wade.fit.ui.sport.bean.SportActivityItem;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.NumUtil;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.SportDataHelper;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.views.dialog.CommonDialog;
import java.util.ArrayList;
import java.util.List;
import org.greenrobot.greendao.query.WhereCondition;

/* renamed from: com.wade.fit.ui.sport.activity.SportDetailActivityNew */
public class SportDetailActivityNew extends BaseActivity {
    public static final int ACTIVITY_FROM_RECORD = 1;
    public static final int ACTIVITY_FROM_SPORT = 2;
    private static final String DATA_KEY = "SPORT_DATA";
    private int ACTIVITY_FROM;
    RelativeLayout barBg;
    private HealthGpsItemDao healthGpsItemDao;
    private List<LatLng> latLngs = new ArrayList();
    private BaiduMap mBaiduMap;
    MapView mMapView;
    RecyclerView mRecyclerView;
    private HealthActivity mSportData;
    private List<HealthGpsItem> mTrajectories;
    private LatLng target;
    TextView tvDate;
    TextView txtTitle;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_sport_detail_new;
    }

    public void initView() {
        SportActivityItem sportActivityItem;
        SportActivityItem sportActivityItem2;
        super.initView();
        this.layoutTitle.setVisibility(View.GONE);
        this.healthGpsItemDao = AppApplication.getInstance().getDaoSession().getHealthGpsItemDao();
        this.mSportData = (HealthActivity) getIntent().getExtras().getSerializable(DATA_KEY);
        this.ACTIVITY_FROM = getIntent().getExtras().getInt("ACTIVITY_FROM");
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.barBg.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = ScreenUtil.getStatusHeight(this);
        this.barBg.setLayoutParams(layoutParams);
        this.txtTitle.setText(SportDataHelper.getStringBytyp(this.mSportData.getType()));
        if (DateFormat.is24HourFormat(this)) {
            this.tvDate.setText(DateUtil.formatYMDHM.format(Long.valueOf(this.mSportData.getDate())));
        } else {
            this.tvDate.setText(DateUtil.formatYMDHM12.format(Long.valueOf(this.mSportData.getDate())));
        }
        if (this.mSportData.getDataFrom() == 1) {
            this.mTrajectories = this.healthGpsItemDao.queryBuilder().where(HealthGpsItemDao.Properties.Date.eq(Long.valueOf(this.mSportData.getDate())), new WhereCondition[0]).orderDesc(HealthGpsItemDao.Properties.Date).build().list();
            List<HealthGpsItem> list = this.mTrajectories;
            if (list == null || list.size() < 2) {
                this.mMapView.setVisibility(View.GONE);
            } else {
                showMapView();
            }
        } else if (this.mSportData.getDataFrom() == 2) {
            this.mMapView.setVisibility(View.GONE);
        }
        this.mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        ArrayList arrayList = new ArrayList();
        SportActivityItem sportActivityItem3 = new SportActivityItem(R.mipmap.sport_time, String.format("%02d", Integer.valueOf(this.mSportData.getDurations() / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf((this.mSportData.getDurations() % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf(this.mSportData.getDurations() % 60)), getResources().getString(R.string.record_time_sport));
        if (!BleSdkWrapper.isDistUnitKm()) {
            String format2Point = NumUtil.format2Point((double) UnitUtil.getKm2mile(((float) this.mSportData.getDistance()) / 1000.0f));
            sportActivityItem = new SportActivityItem(R.mipmap.sport_distance, format2Point, getResources().getString(R.string.distance_str) + "(" + getResources().getString(R.string.unit_mi) + ")");
        } else {
            String string = getResources().getString(R.string.mileage_str, NumUtil.format2Point((double) (((float) this.mSportData.getDistance()) / 1000.0f)));
            sportActivityItem = new SportActivityItem(R.mipmap.sport_distance, string, getResources().getString(R.string.distance_str) + "(" + getResources().getString(R.string.unit_kilometer) + ")");
        }
        SportActivityItem sportActivityItem4 = new SportActivityItem(R.mipmap.sport_cal, this.mSportData.getCalories() + "", getResources().getString(R.string.calories_unit));
        SportActivityItem sportActivityItem5 = new SportActivityItem(R.mipmap.sport_step, this.mSportData.getStep() + "", getResources().getString(R.string.detail_steps));
        if (this.mSportData.getAvgSpeed().equals("0'0\"") || this.mSportData.getAvgSpeed().equals("0")) {
            sportActivityItem2 = new SportActivityItem(R.mipmap.sport_speed, "-'-\"", getResources().getString(R.string.history_record_pace));
        } else {
            sportActivityItem2 = new SportActivityItem(R.mipmap.sport_speed, this.mSportData.getAvgSpeed(), getResources().getString(R.string.history_record_pace));
        }
        switch (this.mSportData.getType()) {
            case 2:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                arrayList.add(sportActivityItem3);
                arrayList.add(sportActivityItem4);
                break;
            case 3:
                if (this.mSportData.getDataFrom() != 1) {
                    arrayList.add(sportActivityItem3);
                    arrayList.add(sportActivityItem4);
                    break;
                } else {
                    arrayList.add(sportActivityItem3);
                    arrayList.add(sportActivityItem);
                    arrayList.add(sportActivityItem4);
                    arrayList.add(sportActivityItem2);
                    break;
                }
            case 4:
            default:
                arrayList.add(sportActivityItem3);
                arrayList.add(sportActivityItem);
                arrayList.add(sportActivityItem4);
                arrayList.add(sportActivityItem5);
                arrayList.add(sportActivityItem2);
                break;
        }
        this.mRecyclerView.setAdapter(new ActivityItemAdapter(this, arrayList));
    }

    private void showMapView() {
        this.mMapView.setVisibility(View.VISIBLE);
        this.mBaiduMap = this.mMapView.getMap();
        this.mBaiduMap.setMyLocationEnabled(true);
        coordinateConvert();
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(this.target).zoom(18.0f);
        this.mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
        ((Polyline) this.mBaiduMap.addOverlay(new PolylineOptions().width(13).color(-1426128896).points(this.latLngs))).setZIndex(3);
        drawOption();
    }

    private void coordinateConvert() {
        double d = 0.0d;
        double d2 = 0.0d;
        for (int i = 0; i < this.mTrajectories.size(); i++) {
            LatLng latLng = new LatLng(this.mTrajectories.get(i).getLatitude().doubleValue(), this.mTrajectories.get(i).getLongitude().doubleValue());
            d += latLng.latitude;
            d2 += latLng.longitude;
            this.latLngs.add(latLng);
        }
        double size = (double) this.mTrajectories.size();
        Double.isNaN(size);
        double d3 = d / size;
        double size2 = (double) this.mTrajectories.size();
        Double.isNaN(size2);
        this.target = new LatLng(d3, d2 / size2);
    }

    private void drawOption() {
        BitmapDescriptor fromResource = BitmapDescriptorFactory.fromResource(R.mipmap.ic_me_history_startpoint);
        BitmapDescriptor fromResource2 = BitmapDescriptorFactory.fromResource(R.mipmap.ic_me_history_finishpoint);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(this.latLngs.get(0));
        markerOptions.icon(fromResource);
        markerOptions.zIndex(4);
        Marker marker = (Marker) this.mBaiduMap.addOverlay(markerOptions);
        MarkerOptions markerOptions2 = new MarkerOptions();
        List<LatLng> list = this.latLngs;
        Marker marker2 = (Marker) this.mBaiduMap.addOverlay(markerOptions2.position(list.get(list.size() - 1)).icon(fromResource2).zIndex(4));
    }

    /* access modifiers changed from: package-private */
    public void backTo() {
        int i = this.ACTIVITY_FROM;
        if (i == 1) {
            finish();
        } else if (i == 2) {
            IntentUtil.goToActivityAndFinish(this, SportOutActivity.class);
        }
    }

    /* access modifiers changed from: package-private */
    public void showDialog() {
        new CommonDialog.Builder(this).setRightButton((int) R.string.delete_sport_activity, new DialogInterface.OnClickListener() {
            /* class com.wade.fit.ui.sport.activity.$$Lambda$SportDetailActivityNew$bWaPNZ7RqeFJMnAv92uRikGp6PM */

            public final void onClick(DialogInterface dialogInterface, int i) {
                SportDetailActivityNew.this.lambda$showDialog$0$SportDetailActivityNew(dialogInterface, i);
            }
        }).setLeftButton(R.string.cancel).setType(1).setRightTextColor(R.color.red).create().show();
    }

    public /* synthetic */ void lambda$showDialog$0$SportDetailActivityNew(DialogInterface dialogInterface, int i) {
        AppApplication.getInstance().getDaoSession().getHealthActivityDao().queryBuilder().where(HealthActivityDao.Properties.Date.eq(Long.valueOf(this.mSportData.getDate())), new WhereCondition[0]).buildDelete().executeDeleteWithoutDetachingEntities();
        int i2 = this.ACTIVITY_FROM;
        if (i2 == 1) {
            setResult(200);
            finish();
        } else if (i2 == 2) {
            IntentUtil.goToActivityAndFinish(this, SportOutActivity.class);
        }
    }
}
