package com.wade.fit.util;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.Time;
import com.baidu.mobstat.Config;
import com.wade.fit.app.AppApplication;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeUtil {
    public static int format12To24(int i, boolean z) {
        if (z) {
            if (i == 12) {
                return 0;
            }
            return i;
        } else if (i == 12) {
            return 12;
        } else {
            return 12 + i;
        }
    }

    public static int getDayMonthOfCurrentYear() {
        return 12;
    }

    public static int getDayOfCurrentWeek() {
        return 7;
    }

    public static int getMonthOfYearMonthDay(int i, int i2, int i3, int i4) {
        if (i > i3) {
            return ((i - i3) * 12) + (i2 - i4);
        }
        if (i2 > i4) {
            return i2 - i4;
        }
        return 0;
    }

    public static int getYearOfYearMonthDay(int i, int i2) {
        if (i > i2) {
            return i - i2;
        }
        return 0;
    }

    public static boolean isAM(int i) {
        return i < 12;
    }

    public static String getSaturdayPreviousWeek(SimpleDateFormat simpleDateFormat, int i) {
        return getStartdayThisWeek(simpleDateFormat, i);
    }

    public static String getFridayThisWeek(SimpleDateFormat simpleDateFormat, int i) {
        return getEnddayThisWeek(simpleDateFormat, i);
    }

    public static String getEnddayThisWeek(SimpleDateFormat simpleDateFormat, int i) {
        Calendar instance = Calendar.getInstance();
        SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1);
        int i2 = instance.get(7) + 1;
        int i3 = 7 - i2;
        if (i3 < 0) {
            i3 = 6;
        }
        if (i3 >= 7) {
            i3 = 0;
        }
        PrintStream printStream = System.out;
        printStream.println("getEnddayThisWeek 往后推:" + i2 + ",offDay:" + i3);
        instance.add(5, i3 + (i * 7));
        return simpleDateFormat.format(instance.getTime());
    }

    public static String getStartdayThisWeek(SimpleDateFormat simpleDateFormat, int i) {
        Calendar instance = Calendar.getInstance();
        int weekStartIndex = SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1);
        int i2 = instance.get(7) - (weekStartIndex == 0 ? -1 : (weekStartIndex != 1 && weekStartIndex == 2) ? 1 : 0);
        int i3 = (-i2) + 1;
        System.out.println("getStartdayThisWeek day_of_week:" + i2 + ",offDay:" + i3);
        if (i3 > 0) {
            i3 -= 7;
        }
        if (i3 <= -7) {
            i3 = 0;
        }
        System.out.println("getStartdayThisWeek 往前推....:" + i2 + ",offDay....:" + i3);
        instance.add(5, i3 + (i * 7));
        return simpleDateFormat.format(instance.getTime());
    }

    public static Date getStartdayCurrentWeek(Date date, int i) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int weekStartIndex = SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1);
        int i2 = instance.get(7) - (weekStartIndex == 0 ? -1 : (weekStartIndex != 1 && weekStartIndex == 2) ? 1 : 0);
        int i3 = (-i2) + 1;
        System.out.println("getStartdayThisWeek day_of_week:" + i2 + ",offDay:" + i3);
        if (i3 > 0) {
            i3 -= 7;
        }
        if (i3 <= -7) {
            i3 = 0;
        }
        System.out.println("getStartdayThisWeek 往前推....:" + i2 + ",offDay....:" + i3);
        instance.add(5, i3 + (i * 7));
        return instance.getTime();
    }

    public static Date getEnddayCurrentWeek(Date date, int i) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i2 = 1;
        int weekStartIndex = SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1);
        if (weekStartIndex == 0) {
            i2 = -1;
        } else if (weekStartIndex == 1 || weekStartIndex != 2) {
            i2 = 0;
        }
        int i3 = instance.get(7) - i2;
        int i4 = 7 - i3;
        if (i4 < 0) {
            i4 = 6;
        }
        if (i4 >= 7) {
            i4 = 0;
        }
        PrintStream printStream = System.out;
        printStream.println("getEnddayThisWeek 往后推:" + i3 + ",offDay:" + i4);
        instance.add(5, i4 + (i * 7));
        return instance.getTime();
    }

    public static String getSundayPreviousWeek(SimpleDateFormat simpleDateFormat, int i) {
        return getStartdayThisWeek(simpleDateFormat, i);
    }

    public static String formatTime(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public static String formatTime(int i, String str) {
        return new SimpleDateFormat(str).format(Integer.valueOf(i));
    }

    public static String m2HM(int i) {
        return String.format("%02d:%02d", Integer.valueOf(i / 60), Integer.valueOf(i % 60));
    }

    public static String getMondayThisWeek(SimpleDateFormat simpleDateFormat, int i) {
        return getStartdayThisWeek(simpleDateFormat, i);
    }

    public static String getSundayThisWeek(SimpleDateFormat simpleDateFormat, int i) {
        return getEnddayThisWeek(simpleDateFormat, i);
    }

    public static String getSaturdayThisWeek(SimpleDateFormat simpleDateFormat, int i) {
        return getEnddayThisWeek(simpleDateFormat, i);
    }

    public static String getDayHistoryWeek(SimpleDateFormat simpleDateFormat, int i) {
        int weekStartIndex = SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1);
        if (weekStartIndex == 0) {
            return getSaturdayPreviousWeek(simpleDateFormat, i) + "-" + getFridayThisWeek(simpleDateFormat, i);
        } else if (weekStartIndex == 1) {
            return getSundayPreviousWeek(simpleDateFormat, i) + "-" + getSaturdayThisWeek(simpleDateFormat, i);
        } else if (weekStartIndex != 2) {
            return "";
        } else {
            return getMondayThisWeek(simpleDateFormat, i) + "-" + getSundayThisWeek(simpleDateFormat, i);
        }
    }

    public static int getCurrentMonth() {
        return Calendar.getInstance().get(2);
    }

    public static String getDayHistoryMonth(SimpleDateFormat simpleDateFormat, int i) {
        return getFirstDayOfMonth(simpleDateFormat, i) + "-" + getLastDayOfMonth(simpleDateFormat, i);
    }

    public static String getFirstDayOfMonth(SimpleDateFormat simpleDateFormat, int i) {
        Calendar instance = Calendar.getInstance();
        instance.set(2, i);
        instance.set(5, instance.getMinimum(5));
        return simpleDateFormat.format(instance.getTime());
    }

    public static String getLastDayOfMonth(SimpleDateFormat simpleDateFormat, int i) {
        Calendar instance = Calendar.getInstance();
        instance.set(2, i);
        instance.set(5, instance.getActualMaximum(5));
        return simpleDateFormat.format(instance.getTime());
    }

    public static int[] getCurrentYearMonthDay() {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(1);
        int i2 = instance.get(2);
        return new int[]{i, i2 + 1, instance.get(5)};
    }

    public static int getDaysByYearMonth(int i, int i2) {
        Calendar instance = Calendar.getInstance();
        instance.set(1, i);
        instance.set(2, i2 - 1);
        instance.set(5, 1);
        instance.roll(5, -1);
        return instance.get(5);
    }

    public static int getCurrentYear() {
        return Calendar.getInstance().get(1);
    }

    public static boolean is24Hour(Context context) {
        return is24Hour();
    }

    public static boolean is24Hour() {
        "24".equals(Settings.System.getString(AppApplication.getContext().getContentResolver(), "time_12_24"));
        return true;
    }

    public static String timeStamp2Date(long j) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(Long.valueOf(j).longValue()));
    }

    public static String timeStamp2Date(String str) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(Long.valueOf(str).longValue()));
    }

    public static String timeStamp2Date(long j, String str) {
        return new SimpleDateFormat(str).format(new Date(Long.valueOf(j).longValue()));
    }

    public static String timeStamp2Date(String str, String str2) {
        return (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) ? "" : new SimpleDateFormat(str2).format(new Date(Long.valueOf(str).longValue()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.TimeUtil.timeFormatter(int, boolean, java.lang.String[], boolean):java.lang.String
     arg types: [int, boolean, java.lang.String[], int]
     candidates:
      com.wade.fit.util.TimeUtil.timeFormatter(int, int, boolean, java.lang.String[]):java.lang.String
      com.wade.fit.util.TimeUtil.timeFormatter(int, boolean, java.lang.String[], boolean):java.lang.String */
    public static String timeStamp2Date(long j, String str, boolean z, String[] strArr) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str);
        if (z) {
            return simpleDateFormat.format(new Date(Long.valueOf(j).longValue()));
        }
        String format = simpleDateFormat.format(new Date(Long.valueOf(j).longValue()));
        if (format.contains(" ")) {
            String[] split = format.split(" ");
            String[] split2 = split[1].split(Config.TRACE_TODAY_VISIT_SPLIT);
            int intValue = (NumUtil.isEmptyInt(split2[0]).intValue() * 60) + NumUtil.isEmptyInt(split2[1]).intValue();
            return split[0] + " " + timeFormatter(intValue, z, strArr, true);
        }
        String[] split3 = format.split(Config.TRACE_TODAY_VISIT_SPLIT);
        return timeFormatter((NumUtil.isEmptyInt(split3[0]).intValue() * 60) + NumUtil.isEmptyInt(split3[1]).intValue(), z, strArr, true);
    }

    public static Date getDateFromString(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return null;
        }
        try {
            return new SimpleDateFormat(str2).parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Calendar getCalendarFromString(String str, String str2) {
        Date dateFromString = getDateFromString(str, str2);
        if (dateFromString == null) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(dateFromString);
        return instance;
    }

    public static String date2TimeStamp(String str, String str2) {
        try {
            return String.valueOf(new SimpleDateFormat(str2).parse(str).getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int getDayOfMonth() {
        return Calendar.getInstance().get(5);
    }

    public static Time getTime() {
        Time time = new Time(Time.getCurrentTimezone());
        time.setToNow();
        return time;
    }

    public static int getDayOfWeek(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return instance.get(7);
    }

    public static String getYearMonthDay(int i, int i2, int i3) {
        return "" + i + String.format("%02d", Integer.valueOf(i2)) + String.format("%02d", Integer.valueOf(i3));
    }

    public static int getToDayTotalMinutes(long j) {
        String[] split = timeStamp2Date(j, "HH/mm").split("/");
        return (Integer.parseInt(split[0]) * 60) + Integer.parseInt(split[1]);
    }

    public static boolean is24HourFormat(Context context) {
        return DateFormat.is24HourFormat(context);
    }

    public static long getStartDateToStamp(int i, int i2, int i3) {
        return dateToStamp(i, i2, i3, 0, 0, 0);
    }

    public static Date getStartDate(int i, int i2, int i3) {
        return getDate(i, i2, i3, 0, 0, 0);
    }

    public static long getEndDateToStamp(int i, int i2, int i3) {
        return dateToStamp(i, i2, i3, 23, 59, 59);
    }

    public static Date getEndDate(int i, int i2, int i3) {
        return getDate(i, i2, i3, 23, 59, 59);
    }

    public static long dateToStamp(int i, int i2, int i3, int i4, int i5, int i6) {
        Date date = getDate(i, i2, i3, i4, i5, i6);
        if (date != null) {
            return date.getTime();
        }
        return 0;
    }

    public static int getMonth() {
        return GregorianCalendar.getInstance().get(2) + 1;
    }

    public static int getYear() {
        return GregorianCalendar.getInstance().get(1);
    }

    public static int getDay() {
        return GregorianCalendar.getInstance().get(5);
    }

    public static int getHour() {
        return GregorianCalendar.getInstance().get(11);
    }

    public static int getMinute() {
        return GregorianCalendar.getInstance().get(12);
    }

    public static int getSecond() {
        return GregorianCalendar.getInstance().get(13);
    }

    public static Date getDate() {
        return getDate(getYear(), getMonth(), getDay());
    }

    public static Date getDate(int i, int i2, int i3) {
        return getDate(i, i2, i3, 0, 0, 0);
    }

    public static Date getDate(int i, int i2, int i3, int i4, int i5, int i6) {
        return new GregorianCalendar(i, i2 - 1, i3, i4, i5, i6).getTime();
    }

    public static boolean isSameDay(long j, long j2) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date(j));
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(new Date(j2));
        if (instance.get(1) == instance2.get(1) && instance.get(2) == instance2.get(2) && instance.get(5) == instance2.get(5)) {
            return true;
        }
        return false;
    }

    public static int getWeekOfYearMonthDay(int i, int i2, int i3) {
        Calendar instance = Calendar.getInstance();
        instance.set(i, i2 - 1, i3);
        instance.setFirstDayOfWeek(1);
        return instance.get(3);
    }

    public static String timeFormatter(int i, boolean z, String[] strArr, boolean z2) {
        int i2;
        int i3;
        String str = "";
        if (i >= 0 && i < 1440) {
            int i4 = getHourAndMin(i, z)[0];
            int i5 = i % 60;
            if (z) {
                Object[] objArr = new Object[2];
                if (i4 == 24) {
                    i4 = 0;
                }
                objArr[0] = Integer.valueOf(i4);
                objArr[1] = Integer.valueOf(i5);
                return String.format("%1$02d:%2$02d", objArr);
            }
            if (z2) {
                if (strArr != null) {
                    str = i < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i < 720 ? "am" : "pm";
                }
            }
            if (str.equals("下午") || str.equals("上午")) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                Object[] objArr2 = new Object[2];
                if (i4 == 24) {
                    i4 = 0;
                }
                objArr2[0] = Integer.valueOf(i4);
                objArr2[1] = Integer.valueOf(i5);
                sb.append(String.format("%1$02d:%2$02d", objArr2));
                return sb.toString();
            }
            StringBuilder sb2 = new StringBuilder();
            Object[] objArr3 = new Object[2];
            if (i4 == 24) {
                i4 = 0;
            }
            objArr3[0] = Integer.valueOf(i4);
            objArr3[1] = Integer.valueOf(i5);
            sb2.append(String.format("%1$02d:%2$02d", objArr3));
            sb2.append(str);
            return sb2.toString();
        } else if (i < 1440) {
            return "00:00";
        } else {
            int i6 = i - 1440;
            if (i6 > 0) {
                i3 = getHourAndMin(i6, z)[0];
                i2 = i6 % 60;
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (z) {
                Object[] objArr4 = new Object[2];
                if (i3 == 24) {
                    i3 = 0;
                }
                objArr4[0] = Integer.valueOf(i3);
                objArr4[1] = Integer.valueOf(i2);
                return String.format("%1$02d:%2$02d", objArr4);
            }
            if (z2) {
                if (strArr != null) {
                    str = i6 < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i6 < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb3 = new StringBuilder();
            Object[] objArr5 = new Object[2];
            if (i3 == 24) {
                i3 = 0;
            }
            objArr5[0] = Integer.valueOf(i3);
            objArr5[1] = Integer.valueOf(i2);
            sb3.append(String.format("%1$02d:%2$02d", objArr5));
            sb3.append(str);
            return sb3.toString();
        }
    }

    public static String timeFormatter(int i, boolean z, String[] strArr) {
        int i2;
        int i3;
        String str;
        String str2;
        if (i >= 0 && i < 1440) {
            int i4 = getHourAndMin(i, z)[0];
            int i5 = i % 60;
            if (z) {
                Object[] objArr = new Object[2];
                if (i4 == 24) {
                    i4 = 0;
                }
                objArr[0] = Integer.valueOf(i4);
                objArr[1] = Integer.valueOf(i5);
                return String.format("%1$02d:%2$02d", objArr);
            }
            if (strArr != null) {
                str2 = i < 720 ? strArr[0] : strArr[1];
            } else {
                str2 = i < 720 ? "am" : "pm";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            Object[] objArr2 = new Object[2];
            if (i4 == 24) {
                i4 = 0;
            }
            objArr2[0] = Integer.valueOf(i4);
            objArr2[1] = Integer.valueOf(i5);
            sb.append(String.format("%1$02d:%2$02d", objArr2));
            return sb.toString();
        } else if (i < 1440) {
            return "00:00";
        } else {
            int i6 = i - 1440;
            if (i6 > 0) {
                i3 = getHourAndMin(i6, z)[0];
                i2 = i6 % 60;
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (z) {
                Object[] objArr3 = new Object[2];
                if (i3 == 24) {
                    i3 = 0;
                }
                objArr3[0] = Integer.valueOf(i3);
                objArr3[1] = Integer.valueOf(i2);
                return String.format("%1$02d:%2$02d", objArr3);
            }
            if (strArr != null) {
                str = i6 < 720 ? strArr[0] : strArr[1];
            } else {
                str = i6 < 720 ? "am" : "pm";
            }
            StringBuilder sb2 = new StringBuilder();
            Object[] objArr4 = new Object[2];
            if (i3 == 24) {
                i3 = 0;
            }
            objArr4[0] = Integer.valueOf(i3);
            objArr4[1] = Integer.valueOf(i2);
            sb2.append(String.format("%1$02d:%2$02d", objArr4));
            sb2.append(str);
            return sb2.toString();
        }
    }

    public static String timeFormatter(int i, int i2, boolean z, String[] strArr, boolean z2) {
        int i3;
        int i4;
        int i5 = i;
        boolean z3 = z;
        String str = "";
        if (i5 >= 0 && i5 < 1440) {
            int i6 = getHourAndMin(i, z3)[0];
            int i7 = i5 % 60;
            if (z3) {
                Object[] objArr = new Object[3];
                if (i6 == 24) {
                    i6 = 0;
                }
                objArr[0] = Integer.valueOf(i6);
                objArr[1] = Integer.valueOf(i7);
                objArr[2] = Integer.valueOf(i2);
                return String.format("%1$02d:%2$02d:%3$02d", objArr);
            }
            if (z2) {
                if (strArr != null) {
                    str = i5 < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i5 < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            Object[] objArr2 = new Object[3];
            if (i6 == 24) {
                i6 = 0;
            }
            objArr2[0] = Integer.valueOf(i6);
            objArr2[1] = Integer.valueOf(i7);
            objArr2[2] = Integer.valueOf(i2);
            sb.append(String.format("%1$02d:%2$02d:%3$02d", objArr2));
            return sb.toString();
        } else if (i5 < 1440) {
            return "00:00";
        } else {
            int i8 = i5 - 1440;
            if (i8 > 0) {
                i4 = getHourAndMin(i8, z3)[0];
                i3 = i8 % 60;
            } else {
                i4 = 0;
                i3 = 0;
            }
            if (z3) {
                Object[] objArr3 = new Object[3];
                if (i4 == 24) {
                    i4 = 0;
                }
                objArr3[0] = Integer.valueOf(i4);
                objArr3[1] = Integer.valueOf(i3);
                objArr3[2] = Integer.valueOf(i2);
                return String.format("%1$02d:%2$02d:%3$02d", objArr3);
            }
            if (z2) {
                if (strArr != null) {
                    str = i8 < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i8 < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            Object[] objArr4 = new Object[3];
            if (i4 == 24) {
                i4 = 0;
            }
            objArr4[0] = Integer.valueOf(i4);
            objArr4[1] = Integer.valueOf(i3);
            objArr4[2] = Integer.valueOf(i2);
            sb2.append(String.format("%1$02d:%2$02d:%3$02d", objArr4));
            return sb2.toString();
        }
    }

    public static String timeFormatter(int i, boolean z, String[] strArr, boolean z2, boolean z3) {
        int i2;
        int i3;
        int i4 = i;
        boolean z4 = z;
        String str = "";
        if (i4 >= 0 && i4 < 1440) {
            int i5 = getHourAndMin(i, z)[0];
            int i6 = i4 % 60;
            if (!z3 && i6 != 0) {
                i5++;
            }
            if (z4) {
                Object[] objArr = new Object[2];
                if (i5 == 24) {
                    i5 = 0;
                }
                objArr[0] = Integer.valueOf(i5);
                objArr[1] = 0;
                return String.format("%1$02d:%2$02d", objArr);
            }
            if (z2) {
                if (strArr != null) {
                    str = i4 < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i4 < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            Object[] objArr2 = new Object[2];
            if (i5 == 24) {
                i5 = 0;
            }
            objArr2[0] = Integer.valueOf(i5);
            objArr2[1] = 0;
            sb.append(String.format("%1$02d:%2$02d", objArr2));
            return sb.toString();
        } else if (i4 < 1440) {
            return "00:00";
        } else {
            int i7 = i4 - 1440;
            if (i7 > 0) {
                i3 = getHourAndMin(i7, z)[0];
                i2 = i7 % 60;
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (!z3 && i2 != 0) {
                i3++;
            }
            if (z4) {
                Object[] objArr3 = new Object[2];
                if (i3 == 24) {
                    i3 = 0;
                }
                objArr3[0] = Integer.valueOf(i3);
                objArr3[1] = 0;
                return String.format("%1$02d:%2$02d", objArr3);
            }
            if (z2) {
                if (strArr != null) {
                    str = i7 < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i7 < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            Object[] objArr4 = new Object[2];
            if (i3 == 24) {
                i3 = 0;
            }
            objArr4[0] = Integer.valueOf(i3);
            objArr4[1] = 0;
            sb2.append(String.format("%1$02d:%2$02d", objArr4));
            return sb2.toString();
        }
    }

    public static String timeFormatter(int i, int i2, boolean z, String[] strArr) {
        if (z) {
            Object[] objArr = new Object[2];
            if (i == 24) {
                i = 0;
            }
            objArr[0] = Integer.valueOf(i);
            objArr[1] = Integer.valueOf(i2);
            return String.format("%1$02d:%2$02d", objArr);
        }
        String str = i < 12 ? strArr[0] : strArr[1];
        if (i > 12) {
            i -= 12;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        Object[] objArr2 = new Object[2];
        if (i == 0) {
            i = 12;
        }
        objArr2[0] = Integer.valueOf(i);
        objArr2[1] = Integer.valueOf(i2);
        sb.append(String.format("%1$02d:%2$02d", objArr2));
        return sb.toString();
    }

    public static String timeFormatterForTimeAxis(int i, boolean z, String[] strArr, boolean z2) {
        int i2;
        int i3;
        String str = "";
        if (i >= 0 && i < 1440) {
            int i4 = getHourAndMin(i, z)[0];
            int i5 = i % 60;
            if (z) {
                Object[] objArr = new Object[2];
                if (i4 == 24) {
                    i4 = 0;
                }
                objArr[0] = Integer.valueOf(i4);
                objArr[1] = Integer.valueOf(i5);
                return String.format("%1$02d:%2$02d", objArr);
            }
            if (z2) {
                if (strArr != null) {
                    str = i < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            Object[] objArr2 = new Object[2];
            if (i4 == 24) {
                i4 = 0;
            }
            objArr2[0] = Integer.valueOf(i4);
            objArr2[1] = Integer.valueOf(i5);
            sb.append(String.format("%1$02d:%2$02d", objArr2));
            return sb.toString();
        } else if (i < 1440) {
            return "00:00";
        } else {
            int i6 = i - 1440;
            if (i6 > 0) {
                i3 = getHourAndMin(i6, z)[0];
                i2 = i6 % 60;
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (z) {
                Object[] objArr3 = new Object[2];
                if (i3 == 24) {
                    i3 = 0;
                }
                objArr3[0] = Integer.valueOf(i3);
                objArr3[1] = Integer.valueOf(i2);
                return String.format("%1$02d:%2$02d", objArr3);
            }
            if (z2) {
                if (strArr != null) {
                    str = i6 < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i6 < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            Object[] objArr4 = new Object[2];
            if (i3 == 24) {
                i3 = 0;
            }
            objArr4[0] = Integer.valueOf(i3);
            objArr4[1] = Integer.valueOf(i2);
            sb2.append(String.format("%1$02d:%2$02d", objArr4));
            return sb2.toString();
        }
    }

    public static int[] getHourAndMin(int i, boolean z) {
        int i2 = i / 60;
        if (!z) {
            if (i2 % 12 == 0) {
                i2 = 12;
            } else if (i2 > 12) {
                i2 -= 12;
            }
        }
        return new int[]{i2, i % 60};
    }

    public static String getDayHistoryYear(int i) {
        return i + "/01-" + i + "/12";
    }

    public static int format24To12(int i) {
        int i2 = i % 12;
        if (i == 12) {
            if (i2 == 0) {
                return 12;
            }
            return i2;
        } else if (i2 == 0) {
            return 0;
        } else {
            return i2;
        }
    }

    public static Calendar getStartDateThisWeek(int i) {
        Calendar instance = Calendar.getInstance();
        int weekStartIndex = SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1);
        int i2 = instance.get(7) - (weekStartIndex == 0 ? -1 : (weekStartIndex != 1 && weekStartIndex == 2) ? 1 : 0);
        int i3 = (-i2) + 1;
        System.out.println("getStartdayThisWeek day_of_week:" + i2 + ",offDay:" + i3);
        if (i3 > 0) {
            i3 -= 7;
        }
        if (i3 <= -7) {
            i3 = 0;
        }
        System.out.println("getStartdayThisWeek 往前推....:" + i2 + ",offDay....:" + i3);
        instance.add(5, i3 + (i * 7));
        return instance;
    }
}
