package com.wade.fit.base;

import android.content.Context;
import com.wade.fit.model.bean.LocationMessage;

public interface IBaseLocation {
    void init(Context context);

    void onDestory();

    void onLocationChanged(LocationMessage locationMessage);

    void startLocation(boolean z);

    void stopLocation();
}
