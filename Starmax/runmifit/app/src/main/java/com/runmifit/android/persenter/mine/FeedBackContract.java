package com.wade.fit.persenter.mine;

import com.wade.fit.base.IBaseView;

public interface FeedBackContract {

    public interface Presenter {
        void uploadFeedbackInfo(String str, String str2);
    }

    public interface View extends IBaseView {
        void requestFaild();

        void requestSuccess();
    }
}
