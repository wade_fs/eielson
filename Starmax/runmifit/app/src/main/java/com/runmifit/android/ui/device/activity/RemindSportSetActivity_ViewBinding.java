package com.wade.fit.ui.device.activity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.device.activity.RemindSportSetActivity_ViewBinding */
public class RemindSportSetActivity_ViewBinding implements Unbinder {
    private RemindSportSetActivity target;
    private View view2131296811;
    private View view2131296812;
    private View view2131296813;
    private View view2131296814;

    public RemindSportSetActivity_ViewBinding(RemindSportSetActivity remindSportSetActivity) {
        this(remindSportSetActivity, remindSportSetActivity.getWindow().getDecorView());
    }

    public RemindSportSetActivity_ViewBinding(final RemindSportSetActivity remindSportSetActivity, View view) {
        this.target = remindSportSetActivity;
        remindSportSetActivity.remindSportSwitch = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.remind_sport_switch, "field 'remindSportSwitch'", ItemToggleLayout.class);
        remindSportSetActivity.remindSportSetLl = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.remind_sport_set_ll, "field 'remindSportSetLl'", LinearLayout.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.rlRemindDelay, "field 'rlRemindDelay' and method 'SelectRemindDelay'");
        remindSportSetActivity.rlRemindDelay = (RelativeLayout) Utils.castView(findRequiredView, R.id.rlRemindDelay, "field 'rlRemindDelay'", RelativeLayout.class);
        this.view2131296811 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.RemindSportSetActivity_ViewBinding.C24761 */

            public void doClick(View view) {
                remindSportSetActivity.SelectRemindDelay();
            }
        });
        remindSportSetActivity.tvRemindDelay = (TextView) Utils.findRequiredViewAsType(view, R.id.tvRemindDelay, "field 'tvRemindDelay'", TextView.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.rlRemindTime, "field 'rlRemindTime' and method 'SelectRemindTime'");
        remindSportSetActivity.rlRemindTime = (RelativeLayout) Utils.castView(findRequiredView2, R.id.rlRemindTime, "field 'rlRemindTime'", RelativeLayout.class);
        this.view2131296814 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.RemindSportSetActivity_ViewBinding.C24772 */

            public void doClick(View view) {
                remindSportSetActivity.SelectRemindTime();
            }
        });
        remindSportSetActivity.tvRemindTime = (TextView) Utils.findRequiredViewAsType(view, R.id.tvRemindTime, "field 'tvRemindTime'", TextView.class);
        remindSportSetActivity.tvRemindStartTime = (TextView) Utils.findRequiredViewAsType(view, R.id.tvRemindStartTime, "field 'tvRemindStartTime'", TextView.class);
        remindSportSetActivity.tvRemindEndTime = (TextView) Utils.findRequiredViewAsType(view, R.id.tvRemindEndTime, "field 'tvRemindEndTime'", TextView.class);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.rlRemindStartTime, "method 'selecStatrTime'");
        this.view2131296813 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.RemindSportSetActivity_ViewBinding.C24783 */

            public void doClick(View view) {
                remindSportSetActivity.selecStatrTime();
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.rlRemindEndTime, "method 'selecEndTime'");
        this.view2131296812 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.RemindSportSetActivity_ViewBinding.C24794 */

            public void doClick(View view) {
                remindSportSetActivity.selecEndTime();
            }
        });
    }

    public void unbind() {
        RemindSportSetActivity remindSportSetActivity = this.target;
        if (remindSportSetActivity != null) {
            this.target = null;
            remindSportSetActivity.remindSportSwitch = null;
            remindSportSetActivity.remindSportSetLl = null;
            remindSportSetActivity.rlRemindDelay = null;
            remindSportSetActivity.tvRemindDelay = null;
            remindSportSetActivity.rlRemindTime = null;
            remindSportSetActivity.tvRemindTime = null;
            remindSportSetActivity.tvRemindStartTime = null;
            remindSportSetActivity.tvRemindEndTime = null;
            this.view2131296811.setOnClickListener(null);
            this.view2131296811 = null;
            this.view2131296814.setOnClickListener(null);
            this.view2131296814 = null;
            this.view2131296813.setOnClickListener(null);
            this.view2131296813 = null;
            this.view2131296812.setOnClickListener(null);
            this.view2131296812 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
