package com.wade.fit.util;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class GridSpanDecoration extends RecyclerView.ItemDecoration {
    private int headItemCount;
    private boolean includeEdge;
    private final int margin;
    private final int padding;

    public GridSpanDecoration(int i) {
        this.padding = i;
        this.margin = 0;
    }

    public GridSpanDecoration(int i, int i2, int i3, boolean z) {
        this.padding = i;
        this.headItemCount = i3;
        this.includeEdge = z;
        this.margin = i2;
    }

    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        super.getItemOffsets(rect, view, recyclerView, state);
        int childLayoutPosition = recyclerView.getChildLayoutPosition(view);
        int i = this.headItemCount;
        int i2 = childLayoutPosition - i;
        if (i == 0 || i2 >= 0) {
            int spanCount = ((GridLayoutManager) recyclerView.getLayoutManager()).getSpanCount();
            GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) view.getLayoutParams();
            int spanIndex = layoutParams.getSpanIndex();
            int spanSize = layoutParams.getSpanSize();
            if (spanIndex != 0) {
                rect.left = this.padding / 2;
            } else if (this.includeEdge) {
                rect.left = this.margin;
            }
            if (spanIndex + spanSize != spanCount) {
                rect.right = this.padding / 2;
            } else if (this.includeEdge) {
                rect.right = this.margin;
            }
            int i3 = this.padding;
            rect.top = i3 / 2;
            rect.bottom = i3 / 2;
            if (isLayoutRTL(recyclerView)) {
                int i4 = rect.left;
                rect.left = rect.right;
                rect.right = i4;
            }
        }
    }

    private static boolean isLayoutRTL(RecyclerView recyclerView) {
        return recyclerView.getLayoutDirection() == 1;
    }
}
