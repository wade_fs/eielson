package com.wade.fit.util.image;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import com.wade.fit.util.ConvertUtil;
import com.wade.fit.util.file.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class BitmapUtil {
    private BitmapUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static Bitmap readBitmapFromFile(String str, int i, int i2) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        int i3 = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        float f = (float) options.outWidth;
        float f2 = (float) options.outHeight;
        float f3 = (float) i2;
        if (f2 > f3 || f > ((float) i)) {
            if (f > f2) {
                i3 = Math.round(f2 / f3);
            } else {
                i3 = Math.round(f / ((float) i));
            }
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = i3;
        return BitmapFactory.decodeFile(str, options);
    }

    public static Bitmap readBitmapFromByteArray(byte[] bArr, int i, int i2) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        int i3 = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        float f = (float) options.outWidth;
        float f2 = (float) options.outHeight;
        float f3 = (float) i2;
        if (f2 > f3 || f > ((float) i)) {
            if (f > f2) {
                i3 = Math.round(f2 / f3);
            } else {
                i3 = Math.round(f / ((float) i));
            }
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = i3;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        return BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
    }

    public static int getBitmapSize(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 19) {
            return (bitmap.getAllocationByteCount() / 1024) / 1024;
        }
        if (Build.VERSION.SDK_INT >= 12) {
            return (bitmap.getByteCount() / 1024) / 1024;
        }
        return ((bitmap.getRowBytes() * bitmap.getHeight()) / 1024) / 1024;
    }

    public static Bitmap getMapAndViewScreenShot(Bitmap bitmap, ViewGroup viewGroup, View view, boolean z, View... viewArr) {
        int width = viewGroup.getWidth();
        int height = viewGroup.getHeight();
        if (z) {
            height = 0;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                height += viewGroup.getChildAt(i).getHeight();
            }
        }
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, (float) view.getLeft(), (float) view.getTop(), (Paint) null);
        for (View view2 : viewArr) {
            if (view2.getVisibility() == 0) {
                view2.setDrawingCacheEnabled(true);
                canvas.drawBitmap(view2.getDrawingCache(), (float) view2.getLeft(), (float) view2.getTop(), (Paint) null);
            }
        }
        return createBitmap;
    }

    public static byte[] bitmap2Bytes(Bitmap bitmap, Bitmap.CompressFormat compressFormat) {
        return ConvertUtil.bitmap2Bytes(bitmap, compressFormat);
    }

    public static Bitmap bytes2Bitmap(byte[] bArr) {
        return ConvertUtil.bytes2Bitmap(bArr);
    }

    public static Bitmap drawable2Bitmap(Drawable drawable) {
        return ConvertUtil.drawable2Bitmap(drawable);
    }

    public static void saveBitmap(Bitmap bitmap, String str) {
        System.currentTimeMillis();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(str));
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public static Drawable bitmap2Drawable(Resources resources, Bitmap bitmap) {
        return ConvertUtil.bitmap2Drawable(resources, bitmap);
    }

    public static byte[] drawable2Bytes(Drawable drawable, Bitmap.CompressFormat compressFormat) {
        return ConvertUtil.drawable2Bytes(drawable, compressFormat);
    }

    public static Drawable bytes2Drawable(Resources resources, byte[] bArr) {
        return ConvertUtil.bytes2Drawable(resources, bArr);
    }

    public static Bitmap view2Bitmap(View view) {
        return ConvertUtil.view2Bitmap(view);
    }

    public static void save(Bitmap bitmap, String str) {
        save(bitmap, str, 100);
    }

    public static void save(Bitmap bitmap, File file) {
        save(bitmap, file, 100);
    }

    public static void save(Bitmap bitmap, String str, int i) {
        save(bitmap, str, (Bitmap.CompressFormat) null, i);
    }

    public static void save(Bitmap bitmap, File file, int i) {
        save(bitmap, file, (Bitmap.CompressFormat) null, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.image.BitmapUtil.save(android.graphics.Bitmap, java.io.File, android.graphics.Bitmap$CompressFormat, int, boolean):boolean
     arg types: [android.graphics.Bitmap, java.io.File, android.graphics.Bitmap$CompressFormat, int, int]
     candidates:
      com.wade.fit.util.image.BitmapUtil.save(android.graphics.Bitmap, java.lang.String, android.graphics.Bitmap$CompressFormat, int, boolean):boolean
      com.wade.fit.util.image.BitmapUtil.save(android.graphics.Bitmap, java.io.File, android.graphics.Bitmap$CompressFormat, int, boolean):boolean */
    public static boolean save(Bitmap bitmap, String str, Bitmap.CompressFormat compressFormat, int i) {
        return save(bitmap, FileUtil.getFileByPath(str), compressFormat, i, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.image.BitmapUtil.save(android.graphics.Bitmap, java.io.File, android.graphics.Bitmap$CompressFormat, int, boolean):boolean
     arg types: [android.graphics.Bitmap, java.io.File, android.graphics.Bitmap$CompressFormat, int, int]
     candidates:
      com.wade.fit.util.image.BitmapUtil.save(android.graphics.Bitmap, java.lang.String, android.graphics.Bitmap$CompressFormat, int, boolean):boolean
      com.wade.fit.util.image.BitmapUtil.save(android.graphics.Bitmap, java.io.File, android.graphics.Bitmap$CompressFormat, int, boolean):boolean */
    public static boolean save(Bitmap bitmap, File file, Bitmap.CompressFormat compressFormat, int i) {
        return save(bitmap, file, compressFormat, i, false);
    }

    public static boolean save(Bitmap bitmap, String str, Bitmap.CompressFormat compressFormat, int i, boolean z) {
        return save(bitmap, FileUtil.getFileByPath(str), compressFormat, i, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        r5 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002f, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        r5 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003a, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0039 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x001e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean save(android.graphics.Bitmap r5, java.io.File r6, android.graphics.Bitmap.CompressFormat r7, int r8, boolean r9) {
        /*
            boolean r0 = isEmptyBitmap(r5)
            r1 = 0
            if (r0 != 0) goto L_0x0056
            boolean r0 = com.wade.fit.util.file.FileUtil.createOrExistsFile(r6)
            if (r0 != 0) goto L_0x000e
            goto L_0x0056
        L_0x000e:
            if (r7 != 0) goto L_0x0012
            android.graphics.Bitmap$CompressFormat r7 = android.graphics.Bitmap.CompressFormat.PNG
        L_0x0012:
            r0 = 0
            r2 = 1
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0041 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0041 }
            r4.<init>(r6)     // Catch:{ IOException -> 0x0041 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0041 }
            boolean r6 = r5.compress(r7, r8, r3)     // Catch:{ IOException -> 0x003c, all -> 0x0039 }
            if (r9 == 0) goto L_0x0031
            boolean r7 = r5.isRecycled()     // Catch:{ IOException -> 0x002e, all -> 0x0039 }
            if (r7 != 0) goto L_0x0031
            r5.recycle()     // Catch:{ IOException -> 0x002e, all -> 0x0039 }
            goto L_0x0031
        L_0x002e:
            r5 = move-exception
            r0 = r3
            goto L_0x0043
        L_0x0031:
            java.io.Closeable[] r5 = new java.io.Closeable[r2]
            r5[r1] = r3
            com.wade.fit.util.file.FileUtil.closeIO(r5)
            goto L_0x004d
        L_0x0039:
            r5 = move-exception
            r0 = r3
            goto L_0x004e
        L_0x003c:
            r5 = move-exception
            r0 = r3
            goto L_0x0042
        L_0x003f:
            r5 = move-exception
            goto L_0x004e
        L_0x0041:
            r5 = move-exception
        L_0x0042:
            r6 = 0
        L_0x0043:
            r5.printStackTrace()     // Catch:{ all -> 0x003f }
            java.io.Closeable[] r5 = new java.io.Closeable[r2]
            r5[r1] = r0
            com.wade.fit.util.file.FileUtil.closeIO(r5)
        L_0x004d:
            return r6
        L_0x004e:
            java.io.Closeable[] r6 = new java.io.Closeable[r2]
            r6[r1] = r0
            com.wade.fit.util.file.FileUtil.closeIO(r6)
            throw r5
        L_0x0056:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.image.BitmapUtil.save(android.graphics.Bitmap, java.io.File, android.graphics.Bitmap$CompressFormat, int, boolean):boolean");
    }

    public static boolean isImage(File file) {
        return file != null && isImage(file.getPath());
    }

    public static boolean isImage(String str) {
        String upperCase = str.toUpperCase();
        return upperCase.endsWith(".PNG") || upperCase.endsWith(".JPG") || upperCase.endsWith(".JPEG") || upperCase.endsWith(".BMP") || upperCase.endsWith(".GIF");
    }

    public static String getImageType(String str) {
        return getImageType(FileUtil.getFileByPath(str));
    }

    public static String getImageType(File file) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2 = null;
        if (file == null) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                String imageType = getImageType(fileInputStream);
                FileUtil.closeIO(fileInputStream);
                return imageType;
            } catch (IOException e) {
                e = e;
                try {
                    e.printStackTrace();
                    FileUtil.closeIO(fileInputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    fileInputStream2 = fileInputStream;
                    FileUtil.closeIO(fileInputStream2);
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            fileInputStream = null;
            e.printStackTrace();
            FileUtil.closeIO(fileInputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            FileUtil.closeIO(fileInputStream2);
            throw th;
        }
    }

    public static String getImageType(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        try {
            byte[] bArr = new byte[8];
            if (inputStream.read(bArr, 0, 8) != -1) {
                return getImageType(bArr);
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getImageType(byte[] bArr) {
        if (isJPEG(bArr)) {
            return "JPEG";
        }
        if (isGIF(bArr)) {
            return "GIF";
        }
        if (isPNG(bArr)) {
            return "PNG";
        }
        if (isBMP(bArr)) {
            return "BMP";
        }
        return null;
    }

    private static boolean isJPEG(byte[] bArr) {
        return bArr.length >= 2 && bArr[0] == -1 && bArr[1] == -40;
    }

    private static boolean isGIF(byte[] bArr) {
        return bArr.length >= 6 && bArr[0] == 71 && bArr[1] == 73 && bArr[2] == 70 && bArr[3] == 56 && (bArr[4] == 55 || bArr[4] == 57) && bArr[5] == 97;
    }

    private static boolean isPNG(byte[] bArr) {
        return bArr.length >= 8 && bArr[0] == -119 && bArr[1] == 80 && bArr[2] == 78 && bArr[3] == 71 && bArr[4] == 13 && bArr[5] == 10 && bArr[6] == 26 && bArr[7] == 10;
    }

    private static boolean isBMP(byte[] bArr) {
        return bArr.length >= 2 && bArr[0] == 66 && bArr[1] == 77;
    }

    public static boolean isEmptyBitmap(Bitmap bitmap) {
        return bitmap == null || bitmap.getWidth() == 0 || bitmap.getHeight() == 0;
    }
}
