package com.wade.fit.util.ble;

import android.util.Log;

public class DFUServiceParser {
    private static DFUServiceParser mParserInstance;
    private final String TAG = "DFUServiceParser";
    private boolean isValidDFUSensor = false;
    private int packetLength = 0;

    private DFUServiceParser() {
    }

    public static synchronized DFUServiceParser getDFUParser() {
        DFUServiceParser dFUServiceParser;
        synchronized (DFUServiceParser.class) {
            if (mParserInstance == null) {
                mParserInstance = new DFUServiceParser();
            }
            dFUServiceParser = mParserInstance;
        }
        return dFUServiceParser;
    }

    public boolean isValidDFUSensor() {
        return this.isValidDFUSensor;
    }

    public void decodeDFUAdvData(byte[] bArr) {
        byte b;
        int i;
        int i2 = 0;
        this.isValidDFUSensor = false;
        if (bArr != null) {
            this.packetLength = bArr.length;
            while (i2 < this.packetLength && (b = bArr[i2]) != 0) {
                int i3 = i2 + 1;
                if (bArr[i3] == 6) {
                    i = b - 1;
                    decodeService128BitUUID(bArr, i3 + 1, i);
                } else {
                    i = b - 1;
                }
                i2 = i3 + i + 1;
            }
            return;
        }
        Log.d("DFUServiceParser", "data is null!");
    }

    private void decodeService128BitUUID(byte[] bArr, int i, int i2) {
        Log.d("DFUServiceParser", "StartPosition: " + i + " Data length: " + i2);
        StringBuilder sb = new StringBuilder();
        int i3 = i + i2;
        sb.append(Byte.toString(bArr[i3 + -3]));
        sb.append(Byte.toString(bArr[i3 + -4]));
        if (sb.toString().equals("2148")) {
            this.isValidDFUSensor = true;
        }
    }
}
