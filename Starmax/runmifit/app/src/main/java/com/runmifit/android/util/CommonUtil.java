package com.wade.fit.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextPaint;
import android.text.TextUtils;
import android.widget.TextView;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class CommonUtil {
    private static DecimalFormat decimalFormat = new DecimalFormat("###,###,###,##0.00");

    /* renamed from: df */
    private static DecimalFormat f5223df = new DecimalFormat("#,###");

    public static boolean isAM(int i) {
        return i < 12;
    }

    public static boolean is24Hour() {
        "24".equals(Settings.System.getString(AppApplication.getInstance().getContentResolver(), "time_12_24"));
        return true;
    }

    public static int format24To12(int i) {
        int i2 = i % 12;
        if (i == 12) {
            if (i2 == 0) {
                return 12;
            }
            return i2;
        } else if (i2 == 0) {
            return 0;
        } else {
            return i2;
        }
    }

    public static String timeFormatter(int i, boolean z, String[] strArr, boolean z2) {
        int i2;
        int i3;
        String str = "";
        if (i >= 0 && i < 1440) {
            int i4 = getHourAndMin(i, z)[0];
            int i5 = i % 60;
            if (z) {
                Object[] objArr = new Object[2];
                if (i4 == 24) {
                    i4 = 0;
                }
                objArr[0] = Integer.valueOf(i4);
                objArr[1] = Integer.valueOf(i5);
                return String.format("%1$02d:%2$02d", objArr);
            }
            if (z2) {
                if (strArr != null) {
                    str = i <= 720 ? strArr[0] : strArr[1];
                } else {
                    str = i <= 720 ? "am" : "pm";
                }
            }
            StringBuilder sb = new StringBuilder();
            Object[] objArr2 = new Object[2];
            if (i4 == 24) {
                i4 = 0;
            }
            objArr2[0] = Integer.valueOf(i4);
            objArr2[1] = Integer.valueOf(i5);
            sb.append(String.format("%1$02d:%2$02d", objArr2));
            sb.append(str);
            return sb.toString();
        } else if (i < 1440) {
            return "00:00";
        } else {
            int i6 = i - 1440;
            if (i6 > 0) {
                i3 = getHourAndMin(i6, z)[0];
                i2 = i6 % 60;
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (z) {
                Object[] objArr3 = new Object[2];
                if (i3 == 24) {
                    i3 = 0;
                }
                objArr3[0] = Integer.valueOf(i3);
                objArr3[1] = Integer.valueOf(i2);
                return String.format("%1$02d:%2$02d", objArr3);
            }
            if (z2) {
                if (strArr != null) {
                    str = i6 <= 720 ? strArr[0] : strArr[1];
                } else {
                    str = i6 <= 720 ? "am" : "pm";
                }
            }
            StringBuilder sb2 = new StringBuilder();
            Object[] objArr4 = new Object[2];
            if (i3 == 24) {
                i3 = 0;
            }
            objArr4[0] = Integer.valueOf(i3);
            objArr4[1] = Integer.valueOf(i2);
            sb2.append(String.format("%1$02d:%2$02d", objArr4));
            sb2.append(str);
            return sb2.toString();
        }
    }

    public static String timeFormatter(int i, boolean z, String[] strArr, boolean z2, boolean z3) {
        int i2;
        int i3;
        int i4 = i;
        boolean z4 = z;
        String str = "";
        if (i4 >= 0 && i4 < 1440) {
            int i5 = getHourAndMin(i, z)[0];
            int i6 = i4 % 60;
            if (!z3 && i6 != 0) {
                i5++;
            }
            if (z4) {
                Object[] objArr = new Object[2];
                if (i5 == 24) {
                    i5 = 0;
                }
                objArr[0] = Integer.valueOf(i5);
                objArr[1] = 0;
                return String.format("%1$02d:%2$02d", objArr);
            }
            if (z2) {
                if (strArr != null) {
                    str = i4 < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i4 < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            Object[] objArr2 = new Object[2];
            if (i5 == 24) {
                i5 = 0;
            }
            objArr2[0] = Integer.valueOf(i5);
            objArr2[1] = 0;
            sb.append(String.format("%1$02d:%2$02d", objArr2));
            return sb.toString();
        } else if (i4 < 1440) {
            return "00:00";
        } else {
            int i7 = i4 - 1440;
            if (i7 > 0) {
                i3 = getHourAndMin(i7, z)[0];
                i2 = i7 % 60;
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (!z3 && i2 != 0) {
                i3++;
            }
            if (z4) {
                Object[] objArr3 = new Object[2];
                if (i3 == 24) {
                    i3 = 0;
                }
                objArr3[0] = Integer.valueOf(i3);
                objArr3[1] = 0;
                return String.format("%1$02d:%2$02d", objArr3);
            }
            if (z2) {
                if (strArr != null) {
                    str = i7 < 720 ? strArr[0] : strArr[1];
                } else {
                    str = i7 < 720 ? "am" : "pm";
                }
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            Object[] objArr4 = new Object[2];
            if (i3 == 24) {
                i3 = 0;
            }
            objArr4[0] = Integer.valueOf(i3);
            objArr4[1] = 0;
            sb2.append(String.format("%1$02d:%2$02d", objArr4));
            return sb2.toString();
        }
    }

    public static int[] getHourAndMin(int i, boolean z) {
        int i2 = i / 60;
        if (!z) {
            if (i2 % 12 == 0) {
                i2 = 12;
            } else if (i2 > 12) {
                i2 -= 12;
            }
        }
        return new int[]{i2, i % 60};
    }

    public static String timeFormat(int i, int i2, int i3, Context context, boolean z, boolean z2) {
        int i4 = (i * 60) + i2;
        if (i4 < i3) {
            i4 += 1440;
        }
        return timeFormatter(i4 - i3, z2, context.getResources().getStringArray(R.array.amOrpm), z);
    }

    public static String timeFormatter(int i, int i2, boolean z, Activity activity) {
        if (z) {
            Object[] objArr = new Object[2];
            if (i == 24) {
                i = 0;
            }
            objArr[0] = Integer.valueOf(i);
            objArr[1] = Integer.valueOf(i2);
            return String.format("%1$02d:%2$02d", objArr);
        }
        String string = activity.getResources().getString(i < 12 ? R.string.am : R.string.pm);
        if (i > 12) {
            i -= 12;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(string);
        Object[] objArr2 = new Object[2];
        if (i == 0) {
            i = 12;
        }
        objArr2[0] = Integer.valueOf(i);
        objArr2[1] = Integer.valueOf(i2);
        sb.append(String.format("%1$02d:%2$02d", objArr2));
        return sb.toString();
    }

    public static String timeFormatter(String str, boolean z, Context context) {
        int parseInt = Integer.parseInt(str.split(Config.TRACE_TODAY_VISIT_SPLIT)[0]);
        int parseInt2 = Integer.parseInt(str.split(Config.TRACE_TODAY_VISIT_SPLIT)[1]);
        if (z) {
            Object[] objArr = new Object[2];
            if (parseInt == 24) {
                parseInt = 0;
            }
            objArr[0] = Integer.valueOf(parseInt);
            objArr[1] = Integer.valueOf(parseInt2);
            return String.format("%1$02d:%2$02d", objArr);
        }
        int i = 12;
        String string = context.getResources().getString(parseInt < 12 ? R.string.am : R.string.pm);
        if (parseInt > 12) {
            parseInt -= 12;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(string);
        Object[] objArr2 = new Object[2];
        if (parseInt != 0) {
            i = parseInt;
        }
        objArr2[0] = Integer.valueOf(i);
        objArr2[1] = Integer.valueOf(parseInt2);
        sb.append(String.format("%1$02d:%2$02d", objArr2));
        return sb.toString();
    }

    public static boolean isOPen(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        return locationManager.isProviderEnabled("gps") || locationManager.isProviderEnabled("network");
    }

    public static void openGPS(Context context) {
        Intent intent = new Intent();
        intent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
        intent.addCategory("android.intent.category.ALTERNATIVE");
        intent.setData(Uri.parse("custom:3"));
        try {
            PendingIntent.getBroadcast(context, 0, intent, 0).send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

    public static String[] getWeeksByWeekStartDay(Context context, int i) {
        String[] stringArray = context.getResources().getStringArray(R.array.weekDay);
        String[] strArr = new String[stringArray.length];
        int i2 = 0;
        if (i == 0) {
            while (i2 < stringArray.length) {
                if (i2 == 0) {
                    strArr[i2] = stringArray[stringArray.length - 1];
                } else {
                    strArr[i2] = stringArray[i2 - 1];
                }
                i2++;
            }
        } else if (i == 1) {
            while (i2 < stringArray.length) {
                strArr[i2] = stringArray[i2];
                i2++;
            }
        } else if (i == 2) {
            for (int i3 = 0; i3 < stringArray.length; i3++) {
                if (i3 == stringArray.length - 1) {
                    strArr[i3] = stringArray[0];
                } else {
                    strArr[i3] = stringArray[i3 + 1];
                }
            }
        }
        return strArr;
    }

    public static boolean[] alarmToShowAlarm(boolean[] zArr, int i) {
        boolean[] zArr2 = new boolean[7];
        if (i == 0) {
            zArr2[0] = zArr[2];
            zArr2[1] = zArr[3];
            zArr2[2] = zArr[4];
            zArr2[3] = zArr[5];
            zArr2[4] = zArr[6];
            zArr2[5] = zArr[0];
            zArr2[6] = zArr[1];
            return zArr2;
        } else if (i != 1) {
            return Arrays.copyOf(zArr, zArr.length);
        } else {
            zArr2[0] = zArr[1];
            zArr2[1] = zArr[2];
            zArr2[2] = zArr[3];
            zArr2[3] = zArr[4];
            zArr2[4] = zArr[5];
            zArr2[5] = zArr[6];
            zArr2[6] = zArr[0];
            return zArr2;
        }
    }

    public static boolean[] alarmToShowAlarm2(boolean[] zArr, int i) {
        boolean[] zArr2 = new boolean[7];
        if (i == 0) {
            zArr2[0] = zArr[5];
            zArr2[1] = zArr[6];
            zArr2[2] = zArr[0];
            zArr2[3] = zArr[1];
            zArr2[4] = zArr[2];
            zArr2[5] = zArr[3];
            zArr2[6] = zArr[4];
            return zArr2;
        } else if (i != 1) {
            return Arrays.copyOf(zArr, zArr.length);
        } else {
            zArr2[0] = zArr[6];
            zArr2[1] = zArr[0];
            zArr2[2] = zArr[1];
            zArr2[3] = zArr[2];
            zArr2[4] = zArr[3];
            zArr2[5] = zArr[4];
            zArr2[6] = zArr[5];
            return zArr2;
        }
    }

    public static boolean hasOrbit(int i) {
        for (int i2 : new int[]{1, 2, 3, 4}) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public static String noHeartRate(String str) {
        if (TextUtils.isEmpty(str) || str.equals("0")) {
            return "--";
        }
        return str + "";
    }

    public static String noBloodPressure(int i, int i2) {
        if (i == 0 || i2 == 0) {
            return "--/--";
        }
        return i + "/" + i2;
    }

    public static String noPace(int i) {
        if (i == 0) {
            return "--";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(i / 60);
        stringBuffer.append("'");
        stringBuffer.append(i % 60);
        stringBuffer.append("\"");
        return stringBuffer.toString();
    }

    public static void adjustTvTextSize(TextView textView, int i, String str) {
        int paddingLeft = ((i - textView.getPaddingLeft()) - textView.getPaddingRight()) - 10;
        if (paddingLeft > 0) {
            TextPaint textPaint = new TextPaint(textView.getPaint());
            float textSize = textPaint.getTextSize();
            while (textPaint.measureText(str) > ((float) paddingLeft)) {
                textSize -= 1.0f;
                textPaint.setTextSize(textSize);
            }
            textView.setTextSize(0, textSize);
        }
    }

    static {
        Locale.setDefault(Locale.CHINA);
    }

    public static String formatThree(int i) {
        return f5223df.format((long) i);
    }

    public static String formatThree(float f) {
        return f5223df.format((double) f);
    }

    public static String formatNumber(int i) {
        return i > 10000 ? "10,000+" : f5223df.format((long) i);
    }

    public static String formatDistance(float f) {
        return decimalFormat.format((double) f);
    }

    public static SimpleDateFormat getFormat(String str) {
        return new SimpleDateFormat(str);
    }

    private static int calculationDaysOfMonth(int i, int i2) {
        switch (i2) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 2:
                return (i % 100 != 0 ? i % 4 != 0 : i % 400 != 0) ? 28 : 29;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 0;
        }
    }

    public static List<String> getSportTimeList() {
        String str;
        ArrayList arrayList = new ArrayList();
        String string = AppApplication.getInstance().getResources().getString(R.string.unit_minute);
        String string2 = AppApplication.getInstance().getResources().getString(R.string.unit_hour);
        arrayList.add("5" + string);
        for (int i = 10; i <= 480; i += 10) {
            int i2 = i / 60;
            int i3 = i % 60;
            String str2 = "";
            if (i2 <= 0) {
                str = str2;
            } else {
                str = i2 + string2;
            }
            if (i3 > 0) {
                str2 = i3 + string;
            }
            arrayList.add(str + str2);
        }
        return arrayList;
    }

    public static List<Float> getTimeList() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Float.valueOf(5.0f));
        for (int i = 10; i <= 6000; i += 10) {
            arrayList.add(Float.valueOf(((float) i) * 1.0f));
        }
        return arrayList;
    }

    public static List<String> getSportDistanceList() {
        ArrayList arrayList = new ArrayList();
        String unitStr = UnitUtil.getUnitStr();
        arrayList.add("0.5" + unitStr);
        for (int i = 1; i <= 100; i++) {
            arrayList.add(i + unitStr);
        }
        return arrayList;
    }

    public static List<Float> getDistanceList() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Float.valueOf(0.5f));
        for (int i = 1; i <= 100; i++) {
            arrayList.add(Float.valueOf(((float) i) * 1.0f));
        }
        return arrayList;
    }

    public static List<String> getPromptCalorieList() {
        ArrayList arrayList = new ArrayList();
        String string = AppApplication.getInstance().getResources().getString(R.string.unit_calorie);
        for (int i = 300; i <= 9000; i += 300) {
            arrayList.add(i + string);
        }
        return arrayList;
    }

    public static List<Float> gettCalorieList() {
        ArrayList arrayList = new ArrayList();
        for (int i = 300; i <= 9000; i += 300) {
            arrayList.add(Float.valueOf(((float) i) * 1.0f));
        }
        return arrayList;
    }
}
