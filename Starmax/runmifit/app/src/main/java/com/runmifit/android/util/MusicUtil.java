package com.wade.fit.util;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Handler;
import com.wade.fit.app.AppApplication;
import com.wade.fit.model.bean.MusicPlayData;
import com.wade.fit.service.MusicContrlService;
import com.wade.fit.util.ble.BaseAppBleListener;
import com.wade.fit.util.ble.BleClient;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.views.CustomToggleButton;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MusicUtil {
    public static final String TYPE_AUDIO = "audio/*";
    private Activity activity;
    private BaseAppBleListener baseAppBleListener = new BaseAppBleListener() {
        /* class com.wade.fit.util.MusicUtil.C26462 */

        public void onBLEConnected(BluetoothGatt bluetoothGatt) {
            super.onBLEConnected(bluetoothGatt);
            DebugLog.m6203d("蓝牙监听重连操作");
            MusicUtil.this.openMusicService();
        }

        public void onBLEDisConnected(String str) {
            super.onBLEDisConnected(str);
            DebugLog.m6203d("蓝牙监听断开连接");
        }
    };
    /* access modifiers changed from: private */
    public CustomToggleButton customToggleButton;
    private Handler handler = new Handler();
    private BleClient protocolUtils = BleClient.getInstance();

    public void addListener() {
    }

    /* access modifiers changed from: protected */
    public boolean isDeviceConnected() {
        return false;
    }

    public void removeListener() {
    }

    public void setNoticeMusicPlay(boolean z) {
    }

    public MusicUtil(Activity activity2, CustomToggleButton customToggleButton2) {
        this.activity = activity2;
        this.customToggleButton = customToggleButton2;
    }

    public void openMusicService() {
        CustomToggleButton customToggleButton2 = this.customToggleButton;
        if (customToggleButton2 != null) {
            customToggleButton2.setSwitchState(true);
        }
        Activity activity2 = this.activity;
        activity2.startService(new Intent(activity2, MusicContrlService.class));
    }

    public void setNotConnectedNoticeSwitchState(final boolean z) {
        if (this.customToggleButton != null) {
            this.handler.postDelayed(new Runnable() {
                /* class com.wade.fit.util.MusicUtil.C26451 */

                public void run() {
                    MusicUtil.this.customToggleButton.setSwitchState(!z);
                }
            }, 500);
        }
    }

    public static ArrayList<MusicPlayData> getMusicPlayLists() {
        return getMusicPlayerLists();
    }

    public static ArrayList<MusicPlayData> getMusicPlayLists(boolean z) {
        PackageManager packageManager = AppApplication.getInstance().getPackageManager();
        ArrayList<MusicPlayData> arrayList = new ArrayList<>();
        List<ResolveInfo> shareApps = getShareApps(AppApplication.getInstance());
        for (int i = 0; i < shareApps.size(); i++) {
            ResolveInfo resolveInfo = shareApps.get(i);
            MusicPlayData musicPlayData = new MusicPlayData();
            if (z) {
                musicPlayData.setPlayIcon(resolveInfo.loadIcon(packageManager));
            }
            musicPlayData.setPlayName(resolveInfo.loadLabel(packageManager).toString());
            musicPlayData.setPackageName(resolveInfo.activityInfo.packageName);
            if (!arrayList.contains(musicPlayData)) {
                arrayList.add(musicPlayData);
            }
        }
        if (queryFilterAppInfo() != null) {
            arrayList.add(queryFilterAppInfo());
        }
        return arrayList;
    }

    public static ArrayList<MusicPlayData> getMusicPlayerLists() {
        PackageManager packageManager = AppApplication.getInstance().getApplicationContext().getPackageManager();
        ArrayList<MusicPlayData> arrayList = new ArrayList<>();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setFlags(8388608);
        intent.setDataAndType(Uri.fromFile(new File("")), "audio/*");
        for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 65536)) {
            MusicPlayData musicPlayData = new MusicPlayData();
            musicPlayData.setPlayIcon(resolveInfo.loadIcon(packageManager));
            musicPlayData.setPlayName(resolveInfo.loadLabel(packageManager).toString());
            musicPlayData.setPackageName(resolveInfo.activityInfo.packageName);
            arrayList.add(musicPlayData);
        }
        return arrayList;
    }

    public static String getSelectedMusic() {
        boolean z;
        ArrayList<MusicPlayData> musicPlayerLists = getMusicPlayerLists();
        String str = SPHelper.getDeviceConfig().musicPackageName;
        DebugLog.m6203d("getMusicPlayPackageName:" + str);
        String str2 = "";
        if (musicPlayerLists != null && !musicPlayerLists.isEmpty()) {
            Iterator<MusicPlayData> it = musicPlayerLists.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                MusicPlayData next = it.next();
                if (next.getPackageName().equals(str)) {
                    z = true;
                    str2 = next.getPlayName();
                    break;
                }
            }
            if (!z) {
                str2 = musicPlayerLists.get(0).playName;
            }
        }
        DebugLog.m6203d("musicName:" + str2);
        return str2;
    }

    private static MusicPlayData queryFilterAppInfo() {
        PackageManager packageManager = AppApplication.getInstance().getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, 0)) {
            if (resolveInfo.activityInfo.packageName.equals("com.android.music")) {
                MusicPlayData musicPlayData = new MusicPlayData();
                musicPlayData.setPlayIcon(resolveInfo.activityInfo.loadIcon(packageManager));
                musicPlayData.setPlayName(resolveInfo.activityInfo.loadLabel(packageManager).toString());
                musicPlayData.setPackageName(resolveInfo.activityInfo.packageName);
                return musicPlayData;
            }
        }
        return null;
    }

    public static List<ResolveInfo> getShareApps(Context context) {
        new ArrayList();
        return context.getPackageManager().queryBroadcastReceivers(new Intent("android.intent.action.MEDIA_BUTTON"), 96);
    }
}
