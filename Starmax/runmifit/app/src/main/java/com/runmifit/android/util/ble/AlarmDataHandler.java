package com.wade.fit.util.ble;

import com.wade.fit.model.bean.Alarm;
import com.wade.fit.util.SPHelper;
import java.util.ArrayList;

public class AlarmDataHandler {
    private byte[] dataByte = new byte[40];
    private int dataIndex;

    public void initData(byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            this.dataByte[i] = bArr[i];
        }
        this.dataIndex = bArr.length;
    }

    public HandlerBleDataResult receiverData(byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            this.dataByte[this.dataIndex + i] = bArr[i];
        }
        return handlerAlarmData(this.dataByte);
    }

    public HandlerBleDataResult handlerAlarmData(byte[] bArr) {
        ArrayList arrayList = new ArrayList();
        int Byte2Int = ByteDataConvertUtil.Byte2Int(bArr[1]) / 4;
        for (int i = 0; i < Byte2Int; i++) {
            int i2 = i * 4;
            int i3 = i2 + 3;
            if (ByteDataConvertUtil.Byte2Int(bArr[i3]) != 255 || ByteDataConvertUtil.Byte2Int(bArr[i2 + 4]) != 255 || ByteDataConvertUtil.Byte2Int(bArr[i2 + 5]) != 255 || ByteDataConvertUtil.Byte2Int(bArr[i2 + 6]) != 255) {
                Alarm alarm = new Alarm();
                alarm.setAlarmType(ByteDataConvertUtil.Byte2Int(bArr[i2 + 6]));
                alarm.setAlarmHour(ByteDataConvertUtil.Byte2Int(bArr[i2 + 4]));
                alarm.setAlarmMinute(ByteDataConvertUtil.Byte2Int(bArr[i2 + 5]));
                if (bArr[i3] == 1) {
                    alarm.setOn_off(true);
                } else if (bArr[i3] == 12) {
                    alarm.setOn_off(true);
                    alarm.setWeekRepeat(new boolean[]{true, true, true, true, true, true, true});
                } else {
                    boolean[] zArr = new boolean[7];
                    byte[] Int2Bit8 = ByteDataConvertUtil.Int2Bit8(ByteDataConvertUtil.Byte2Int(bArr[i3]));
                    if (Int2Bit8[0] == 1) {
                        alarm.setOn_off(true);
                    } else {
                        alarm.setOn_off(false);
                    }
                    for (int i4 = 1; i4 < Int2Bit8.length; i4++) {
                        if (Int2Bit8[i4] == 0) {
                            zArr[i4 - 1] = false;
                        } else {
                            zArr[i4 - 1] = true;
                        }
                    }
                    alarm.setWeekRepeat(zArr);
                }
                arrayList.add(alarm);
            }
        }
        SPHelper.saveAlarms(arrayList);
        HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
        handlerBleDataResult.isComplete = true;
        handlerBleDataResult.hasNext = false;
        return handlerBleDataResult;
    }
}
