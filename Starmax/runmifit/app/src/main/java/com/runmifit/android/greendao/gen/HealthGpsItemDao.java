package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthGpsItem;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class HealthGpsItemDao extends AbstractDao<HealthGpsItem, Void> {
    public static final String TABLENAME = "HEALTH_GPS_ITEM";

    public static class Properties {
        public static final Property Date = new Property(5, Long.class, "date", false, "DATE");
        public static final Property IsUploaded = new Property(0, Boolean.TYPE, "isUploaded", false, "IS_UPLOADED");
        public static final Property Latitude = new Property(4, Double.class, "latitude", false, "LATITUDE");
        public static final Property Longitude = new Property(3, Double.class, "longitude", false, "LONGITUDE");
        public static final Property MacAddress = new Property(1, String.class, "macAddress", false, "MAC_ADDRESS");
        public static final Property Remark = new Property(7, String.class, "remark", false, "REMARK");
        public static final Property StartDate = new Property(2, String.class, "startDate", false, "START_DATE");
        public static final Property UserId = new Property(6, String.class, Constants.userId, false, "USER_ID");
    }

    public Void getKey(HealthGpsItem healthGpsItem) {
        return null;
    }

    public boolean hasKey(HealthGpsItem healthGpsItem) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(HealthGpsItem healthGpsItem, long j) {
        return null;
    }

    public HealthGpsItemDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public HealthGpsItemDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"HEALTH_GPS_ITEM\" (\"IS_UPLOADED\" INTEGER NOT NULL ,\"MAC_ADDRESS\" TEXT,\"START_DATE\" TEXT,\"LONGITUDE\" REAL,\"LATITUDE\" REAL,\"DATE\" INTEGER,\"USER_ID\" TEXT,\"REMARK\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"HEALTH_GPS_ITEM\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, HealthGpsItem healthGpsItem) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, healthGpsItem.getIsUploaded() ? 1 : 0);
        String macAddress = healthGpsItem.getMacAddress();
        if (macAddress != null) {
            databaseStatement.bindString(2, macAddress);
        }
        String startDate = healthGpsItem.getStartDate();
        if (startDate != null) {
            databaseStatement.bindString(3, startDate);
        }
        Double longitude = healthGpsItem.getLongitude();
        if (longitude != null) {
            databaseStatement.bindDouble(4, longitude.doubleValue());
        }
        Double latitude = healthGpsItem.getLatitude();
        if (latitude != null) {
            databaseStatement.bindDouble(5, latitude.doubleValue());
        }
        Long date = healthGpsItem.getDate();
        if (date != null) {
            databaseStatement.bindLong(6, date.longValue());
        }
        String userId = healthGpsItem.getUserId();
        if (userId != null) {
            databaseStatement.bindString(7, userId);
        }
        String remark = healthGpsItem.getRemark();
        if (remark != null) {
            databaseStatement.bindString(8, remark);
        }
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, HealthGpsItem healthGpsItem) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, healthGpsItem.getIsUploaded() ? 1 : 0);
        String macAddress = healthGpsItem.getMacAddress();
        if (macAddress != null) {
            sQLiteStatement.bindString(2, macAddress);
        }
        String startDate = healthGpsItem.getStartDate();
        if (startDate != null) {
            sQLiteStatement.bindString(3, startDate);
        }
        Double longitude = healthGpsItem.getLongitude();
        if (longitude != null) {
            sQLiteStatement.bindDouble(4, longitude.doubleValue());
        }
        Double latitude = healthGpsItem.getLatitude();
        if (latitude != null) {
            sQLiteStatement.bindDouble(5, latitude.doubleValue());
        }
        Long date = healthGpsItem.getDate();
        if (date != null) {
            sQLiteStatement.bindLong(6, date.longValue());
        }
        String userId = healthGpsItem.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(7, userId);
        }
        String remark = healthGpsItem.getRemark();
        if (remark != null) {
            sQLiteStatement.bindString(8, remark);
        }
    }

    public HealthGpsItem readEntity(Cursor cursor, int i) {
        boolean z = cursor.getShort(i + 0) != 0;
        int i2 = i + 1;
        String string = cursor.isNull(i2) ? null : cursor.getString(i2);
        int i3 = i + 2;
        String string2 = cursor.isNull(i3) ? null : cursor.getString(i3);
        int i4 = i + 3;
        Double valueOf = cursor.isNull(i4) ? null : Double.valueOf(cursor.getDouble(i4));
        int i5 = i + 4;
        Double valueOf2 = cursor.isNull(i5) ? null : Double.valueOf(cursor.getDouble(i5));
        int i6 = i + 5;
        Long valueOf3 = cursor.isNull(i6) ? null : Long.valueOf(cursor.getLong(i6));
        int i7 = i + 6;
        int i8 = i + 7;
        return new HealthGpsItem(z, string, string2, valueOf, valueOf2, valueOf3, cursor.isNull(i7) ? null : cursor.getString(i7), cursor.isNull(i8) ? null : cursor.getString(i8));
    }

    public void readEntity(Cursor cursor, HealthGpsItem healthGpsItem, int i) {
        healthGpsItem.setIsUploaded(cursor.getShort(i + 0) != 0);
        int i2 = i + 1;
        String str = null;
        healthGpsItem.setMacAddress(cursor.isNull(i2) ? null : cursor.getString(i2));
        int i3 = i + 2;
        healthGpsItem.setStartDate(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 3;
        healthGpsItem.setLongitude(cursor.isNull(i4) ? null : Double.valueOf(cursor.getDouble(i4)));
        int i5 = i + 4;
        healthGpsItem.setLatitude(cursor.isNull(i5) ? null : Double.valueOf(cursor.getDouble(i5)));
        int i6 = i + 5;
        healthGpsItem.setDate(cursor.isNull(i6) ? null : Long.valueOf(cursor.getLong(i6)));
        int i7 = i + 6;
        healthGpsItem.setUserId(cursor.isNull(i7) ? null : cursor.getString(i7));
        int i8 = i + 7;
        if (!cursor.isNull(i8)) {
            str = cursor.getString(i8);
        }
        healthGpsItem.setRemark(str);
    }
}
