package com.wade.fit.ui.sport.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.baidu.mapapi.map.MapView;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.sport.activity.SportDetailActivityNew_ViewBinding */
public class SportDetailActivityNew_ViewBinding implements Unbinder {
    private SportDetailActivityNew target;
    private View view2131296558;
    private View view2131296571;

    public SportDetailActivityNew_ViewBinding(SportDetailActivityNew sportDetailActivityNew) {
        this(sportDetailActivityNew, sportDetailActivityNew.getWindow().getDecorView());
    }

    public SportDetailActivityNew_ViewBinding(final SportDetailActivityNew sportDetailActivityNew, View view) {
        this.target = sportDetailActivityNew;
        sportDetailActivityNew.barBg = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.bar_bg, "field 'barBg'", RelativeLayout.class);
        sportDetailActivityNew.txtTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.title, "field 'txtTitle'", TextView.class);
        sportDetailActivityNew.tvDate = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDate, "field 'tvDate'", TextView.class);
        sportDetailActivityNew.mMapView = (MapView) Utils.findRequiredViewAsType(view, R.id.mMapView, "field 'mMapView'", MapView.class);
        sportDetailActivityNew.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.mRecyclerView, "field 'mRecyclerView'", RecyclerView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.ivBack, "method 'backTo'");
        this.view2131296558 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportDetailActivityNew_ViewBinding.C26171 */

            public void doClick(View view) {
                sportDetailActivityNew.backTo();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ivMore, "method 'showDialog'");
        this.view2131296571 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportDetailActivityNew_ViewBinding.C26182 */

            public void doClick(View view) {
                sportDetailActivityNew.showDialog();
            }
        });
    }

    public void unbind() {
        SportDetailActivityNew sportDetailActivityNew = this.target;
        if (sportDetailActivityNew != null) {
            this.target = null;
            sportDetailActivityNew.barBg = null;
            sportDetailActivityNew.txtTitle = null;
            sportDetailActivityNew.tvDate = null;
            sportDetailActivityNew.mMapView = null;
            sportDetailActivityNew.mRecyclerView = null;
            this.view2131296558.setOnClickListener(null);
            this.view2131296558 = null;
            this.view2131296571.setOnClickListener(null);
            this.view2131296571 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
