package com.wade.fit.model.bean;

import java.io.Serializable;

public enum DetailType implements Serializable {
    STEP,
    CAL,
    DISTANCE
}
