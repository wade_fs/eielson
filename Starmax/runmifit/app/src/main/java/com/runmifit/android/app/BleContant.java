package com.wade.fit.app;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public interface BleContant {
    public static final int ALARM_TYPE_FOOD = 3;
    public static final int ALARM_TYPE_GET_UP = 6;
    public static final int ALARM_TYPE_HAVA_A_DATA = 7;
    public static final int ALARM_TYPE_JOIN = 8;
    public static final int ALARM_TYPE_MEET = 9;
    public static final int ALARM_TYPE_NONE = 0;
    public static final int ALARM_TYPE_PICK = 2;
    public static final int ALARM_TYPE_SLEEP = 5;
    public static final int ALARM_TYPE_SPORT = 4;
    public static final int ALARM_TYPE_WATER = 1;
    public static final int CLEAR_ALL_DATA = 1;
    public static final int CLEAR_SET = 2;
    public static final int ECG_STATE_GOOD = 2;
    public static final int ECG_STATE_NORMAL = 1;
    public static final int ECG_STATE_WEAK = 0;
    public static final int FLAG_CMD_CONTIUNE = 2;
    public static final int FLAG_CMD_END = 3;
    public static final int FLAG_CMD_GET_MORE = 200;
    public static final int FLAG_CMD_NORMAL = 0;
    public static final int FLAG_CMD_START = 1;
    public static final int FLAG_CMD_SYN = 5;
    public static final int FLAG_CMD_WAIT_COMPLETE = 4;
    public static final int MES_TYPE_GMAIL = 20;
    public static final int MES_TYPE_KAKAO_TALK = 19;
    public static final int MES_TYPE_LINE = 8;
    public static final int MES_TYPE_OUTLOOK = 21;
    public static final int MES_TYPE_SNAPCHAT = 22;
    public static final int MES_TYPE_TELEGRAM = 15;
    public static final int MES_TYPE_VIBER = 14;
    public static final int MES_TYPE_VK = 13;
    public static final int MSG_TYPE_CALENDAR = 12;
    public static final int MSG_TYPE_CALL = 0;
    public static final int MSG_TYPE_EMAIL = 9;
    public static final int MSG_TYPE_FACEBOOK = 4;
    public static final int MSG_TYPE_INSTAGRAM = 10;
    public static final int MSG_TYPE_LINKEDIN = 11;
    public static final int MSG_TYPE_MESSENGER = 12;
    public static final int MSG_TYPE_MSG = 1;
    public static final int MSG_TYPE_QQ = 3;
    public static final int MSG_TYPE_SKEY = 5;
    public static final int MSG_TYPE_TWITTER = 6;
    public static final int MSG_TYPE_UNKNOW = 255;
    public static final int MSG_TYPE_WEIBO = 15;
    public static final int MSG_TYPE_WHATSAPP = 7;
    public static final int MSG_TYPE_WX = 2;
    public static final int RESET_SET = 3;
    public static final int SLEEP_AWAKE = 4;
    public static final int SLEEP_DEEP = 3;
    public static final int SLEEP_EYE = 5;
    public static final int SLEEP_LIGHT = 2;
    public static final int SLEEP_START = 1;
    public static final int SPORT_TYPE_BADMINTON = 8;
    public static final int SPORT_TYPE_BASKETBALL = 7;
    public static final int SPORT_TYPE_CLIMBING = 12;
    public static final int SPORT_TYPE_CYCLING = 3;
    public static final int SPORT_TYPE_PUSH_UP = 11;
    public static final int SPORT_TYPE_ROPE = 9;
    public static final int SPORT_TYPE_RUN = 1;
    public static final int SPORT_TYPE_RUN_IN_ROOM = 4;
    public static final int SPORT_TYPE_SIT_UP = 10;
    public static final int SPORT_TYPE_SOCKER = 6;
    public static final int SPORT_TYPE_SWIM = 2;
    public static final int SPORT_TYPE_TRAIN = 5;
    public static final int SPORT_TYPE_WALK = 0;
    public static final int STATE_OFF = 0;
    public static final int STATE_ON = 1;
    public static final int SYNCH_CONFIG_FAILD = 108;
    public static final int SYNCH_CONFIG_ING = 106;
    public static final int SYNCH_CONFIG_NONE = 105;
    public static final int SYNCH_CONFIG_SUCCESS = 107;
    public static final int SYNCH_STATE_CONFIG_CONTIUNE = 106;
    public static final int SYNCH_STATE_CONFIG_END = 107;
    public static final int SYNCH_STATE_CONFIG_START = 105;
    public static final int SYNCH_STATE_FAILD = 104;
    public static final int SYNCH_STATE_NONE = 100;
    public static final int SYNCH_STATE_START = 102;
    public static final int SYNCH_STATE_SUCCESS = 103;
    public static final int SYNCH_STATE_SYNING = 101;
    public static final int TYPE_CLEAR_SPORT_DATA = 0;
    public static final int TYPE_REBOOT_DEVICE = 2;
    public static final int TYPE_RECOVER_SET = 1;
    public static final int UNIT_BRITISH = 1;
    public static final int UNIT_METRIC = 0;

    @Retention(RetentionPolicy.SOURCE)
    public @interface ClearType {
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface MessageType {
    }
}
