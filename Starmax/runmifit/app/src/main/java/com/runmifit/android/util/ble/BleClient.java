package com.wade.fit.util.ble;

import android.content.Context;
import android.os.Handler;
import com.baidu.mobstat.Config;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.BleContant;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleScanTool;
import com.wade.fit.util.log.LogService;
import com.wade.fit.util.log.LogUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class BleClient implements BleContant {
    private static final int CONN_FAILD = 2;
    static final int CONN_ING = 1;
    private static final int CONN_NONE = -1;
    static final int CONN_SUCCESS = 3;
    private static final int SYNCH_TIME_OUT = 60000;
    private static BleClient bleClient = new BleClient();
    public static boolean isConnectionSyn = false;
    private BaseAppBleListener bleListener;
    Calendar calendar;
    /* access modifiers changed from: private */
    public List<BleCallback> callbacks = new ArrayList();
    private Runnable connTimeOut = new Runnable() {
        /* class com.wade.fit.util.ble.BleClient.C15412 */

        public void run() {
            LogUtil.d("同步超时了...");
            BleClient.this.handSynFaild();
        }
    };
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINESE);
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public int heartHistory = 0;
    /* access modifiers changed from: private */
    public int state = 100;
    private int syncProgress;
    /* access modifiers changed from: private */
    public Runnable timeOutTask = new Runnable() {
        /* class com.wade.fit.util.ble.BleClient.C15489 */

        public void run() {
            if (BleClient.this.state == 101) {
                LogUtil.dAndSave("timeOutTask超时了....", Constants.SYCN_PATH);
                BleClient.this.handSynFaild();
            }
        }
    };
    private String today;

    public BleClient() {
        EventBusHelper.register(this);
        isConnectionSyn = ((Boolean) SharePreferenceUtils.get(AppApplication.getInstance(), "FIRST_SYNC", true)).booleanValue();
        this.bleListener = new BaseAppBleListener() {
            /* class com.wade.fit.util.ble.BleClient.C15401 */

            public void initComplete() {
                LogUtil.dAndSave("连接成功是否同步数据...." + BleClient.isConnectionSyn, Constants.SYCN_PATH);
                EventBusHelper.postSticky(303);
                BaseDataHandler.getInstance().init();
                if (BleClient.isConnectionSyn) {
                    BleClient.this.handler.removeCallbacks(BleClient.this.timeOutTask);
                    BleClient.this.synch(null);
                }
            }
        };
        BleManager.getInstance().setBleListener(this.bleListener);
    }

    public static BleClient getInstance() {
        return bleClient;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleMessage(BaseMessage baseMessage) {
        int type = baseMessage.getType();
        if (type == 300) {
            BleManager.getInstance().reConnect();
        } else if (type == 301 && isSynching()) {
            handSynFaild();
        }
    }

    public void synch(BleCallback bleCallback) {
        if (bleCallback != null && !this.callbacks.contains(bleCallback)) {
            this.callbacks.add(bleCallback);
        }
        if (!BleSdkWrapper.isBind()) {
            LogUtil.dAndSave("没有绑定....不同步", Constants.SYCN_PATH);
        } else if (!BleHelper.isBluetoothOpen()) {
            LogUtil.dAndSave("蓝牙未打开....不同步", Constants.SYCN_PATH);
        } else if (!BleSdkWrapper.isInitCon()) {
            LogUtil.dAndSave("蓝牙未初始化连接....去连接或者初始化连接", Constants.SYCN_PATH);
            isConnectionSyn = true;
            BleManager.getInstance().reConnect();
            this.handler.postDelayed(this.connTimeOut, 30000);
        } else if (isSynching()) {
            LogUtil.dAndSave("已经在同步数据了.....", Constants.SYCN_PATH);
        } else {
            realSyn();
        }
    }

    public boolean isSynching() {
        return this.state == 101;
    }

    private void initSyn() {
        this.state = 101;
        this.syncProgress = 0;
    }

    private void realSyn() {
        initSyn();
        for (BleCallback bleCallback : this.callbacks) {
            bleCallback.complete(102, null);
        }
        boolean isFirstSyn = isFirstSyn();
        LogUtil.dAndSave("是否同步配置信息..........." + isFirstSyn, Constants.SYCN_PATH);
        if (isFirstSyn) {
            startConfigData();
        } else {
            startSyncHealthData();
        }
        this.handler.removeCallbacksAndMessages(null);
        this.handler.postDelayed(this.timeOutTask, 60000);
    }

    private void startConfigData() {
        new DeviceConfigHelper().synchDeviceConfig(new BleCallback() {
            /* class com.wade.fit.util.ble.BleClient.C15423 */

            public void setSuccess() {
                for (int i = 0; i < BleClient.this.callbacks.size(); i++) {
                    ((BleCallback) BleClient.this.callbacks.get(i)).setSuccess();
                }
            }

            public void complete(int i, Object obj) {
                if (i == 107 || i == 108) {
                    BleClient.this.startSyncHealthData();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void startSyncHealthData() {
        LogUtil.dAndSave("开始同步健康数据...........", Constants.SYCN_PATH);
        this.calendar = Calendar.getInstance();
        LogUtil.m5266d("1***********开始获取当天的步数数据了", Constants.SYCN_PATH);
        BleSdkWrapper.getCurrentStep(new BleCallback() {
            /* class com.wade.fit.util.ble.BleClient.C15434 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                for (int i2 = 0; i2 < BleClient.this.callbacks.size(); i2++) {
                    ((BleCallback) BleClient.this.callbacks.get(i2)).setSuccess();
                }
                LogUtil.m5266d("1***********获取当天的步数数据了", Constants.SYCN_PATH);
                BleClient.this.syncStepHistory(true);
            }
        });
    }

    /* access modifiers changed from: private */
    public void syncStepHistory(boolean z) {
        if (z) {
            this.calendar = Calendar.getInstance();
        }
        final int i = this.calendar.get(1);
        final int i2 = this.calendar.get(2) + 1;
        final int i3 = this.calendar.get(5);
        BleSdkWrapper.getStepOrSleepHistory(1, i, i2, i3, new BleCallback() {
            /* class com.wade.fit.util.ble.BleClient.C15445 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                if (obj instanceof HandlerBleDataResult) {
                    HandlerBleDataResult handlerBleDataResult = (HandlerBleDataResult) obj;
                    LogUtil.d("" + handlerBleDataResult.toString());
                    if (handlerBleDataResult.isComplete) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("2***********获取步数历史的数据了,year:");
                        sb.append(i);
                        sb.append(",month:");
                        sb.append(i2);
                        sb.append(",day");
                        sb.append(i3);
                        sb.append(",hasNext:");
                        sb.append(handlerBleDataResult.hasNext);
                        sb.append(",isSynchHistory():");
                        BleClient bleClient = BleClient.this;
                        sb.append(bleClient.isSynchHistory(bleClient.calendar.getTime()));
                        LogUtil.m5266d(sb.toString(), Constants.SYCN_PATH);
                        for (int i2 = 0; i2 < BleClient.this.callbacks.size(); i2++) {
                            ((BleCallback) BleClient.this.callbacks.get(i2)).setSuccess();
                        }
                        if (handlerBleDataResult.hasNext) {
                            BleClient bleClient2 = BleClient.this;
                            if (!bleClient2.isSynchHistory(bleClient2.calendar.getTime())) {
                                BleClient.this.calendar.add(5, -1);
                                BleClient.this.syncStepHistory(false);
                                return;
                            }
                        }
                        BleClient.this.synchHeartRate();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void syncHeartHistory(boolean z) {
        if (z) {
            this.calendar = Calendar.getInstance();
        }
        this.heartHistory++;
        final int i = this.calendar.get(1);
        final int i2 = this.calendar.get(2) + 1;
        final int i3 = this.calendar.get(5);
        LogUtil.m5266d("4***********开始心率历史的数据,year:" + i + ",month:" + i2 + ",day" + i3 + ",heartHistory:" + this.heartHistory + ",isSynchHistory():" + isSynchHistory(this.calendar.getTime()), Constants.SYCN_PATH);
        BleSdkWrapper.getHistoryHeartRateData(1, i, i2, i3, new BleCallback() {
            /* class com.wade.fit.util.ble.BleClient.C15456 */

            public void setSuccess() {
                for (int i = 0; i < BleClient.this.callbacks.size(); i++) {
                    ((BleCallback) BleClient.this.callbacks.get(i)).setSuccess();
                }
            }

            public void complete(int i, Object obj) {
                if (obj instanceof HandlerBleDataResult) {
                    HandlerBleDataResult handlerBleDataResult = (HandlerBleDataResult) obj;
                    if (handlerBleDataResult.isComplete) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("4***********获取心率历史的数据了,year:");
                        sb.append(i);
                        sb.append(",month:");
                        sb.append(i2);
                        sb.append(",day");
                        sb.append(i3);
                        sb.append(",hasNext:");
                        sb.append(handlerBleDataResult.hasNext);
                        sb.append(",heartHistory:");
                        sb.append(BleClient.this.heartHistory);
                        sb.append(",isSynchHistory():");
                        BleClient bleClient = BleClient.this;
                        sb.append(bleClient.isSynchHistory(bleClient.calendar.getTime()));
                        LogUtil.m5266d(sb.toString(), Constants.SYCN_PATH);
                        ((Boolean) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.IS_SYN_HISTORY_KEY, true)).booleanValue();
                        for (int i2 = 0; i2 < BleClient.this.callbacks.size(); i2++) {
                            ((BleCallback) BleClient.this.callbacks.get(i2)).setSuccess();
                        }
                        if (handlerBleDataResult.hasNext) {
                            BleClient bleClient2 = BleClient.this;
                            if (!bleClient2.isSynchHistory(bleClient2.calendar.getTime())) {
                                BleClient.this.calendar.add(5, -1);
                                BleClient.this.syncHeartHistory(false);
                                return;
                            }
                        }
                        BleClient.this.synchHeartRateHistory();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isSynchHistory(Date date) {
        String str = (String) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.SYNCH_HISTORY_DATA_KEY, "");
        if (((String) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.SYNCH_TODAY_DATA_KEY, "")).equals(this.format.format(new Date()))) {
            return false;
        }
        return str.equals(this.format.format(date));
    }

    /* access modifiers changed from: private */
    public void synchHeartRateHistory() {
        synchActiviy();
    }

    /* access modifiers changed from: private */
    public void synchHeartRate() {
        LogUtil.m5266d("3***********开始获取当天的心率数据了", Constants.SYCN_PATH);
        this.heartHistory = 0;
        BleSdkWrapper.getHeartRate(new BleCallback() {
            /* class com.wade.fit.util.ble.BleClient.C15467 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                LogUtil.m5266d("3***********获取当天的心率数据了", Constants.SYCN_PATH);
                for (int i2 = 0; i2 < BleClient.this.callbacks.size(); i2++) {
                    ((BleCallback) BleClient.this.callbacks.get(i2)).setSuccess();
                }
                BleClient.this.syncHeartHistory(true);
            }
        });
    }

    /* access modifiers changed from: private */
    public void synchActiviy() {
        LogUtil.m5266d("5***********获取当天的活动数据了**********", Constants.SYCN_PATH);
        BleSdkWrapper.getActivity(new BleCallback() {
            /* class com.wade.fit.util.ble.BleClient.C15478 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                if (obj != null && (obj instanceof HandlerBleDataResult)) {
                    HandlerBleDataResult handlerBleDataResult = (HandlerBleDataResult) obj;
                    if (!handlerBleDataResult.isComplete) {
                        return;
                    }
                    if (handlerBleDataResult.hasNext) {
                        BleClient.this.synchActiviy();
                        return;
                    }
                    for (int i2 = 0; i2 < BleClient.this.callbacks.size(); i2++) {
                        ((BleCallback) BleClient.this.callbacks.get(i2)).setSuccess();
                    }
                    BleClient.this.handSynSuccess();
                }
            }
        });
    }

    private boolean isFirstSyn() {
        return ((Boolean) SharePreferenceUtils.get(AppApplication.getInstance(), "FIRST_SYNC", false)).booleanValue();
    }

    /* access modifiers changed from: private */
    public void handSynFaild() {
        LogUtil.dAndSave("同步失败 isBluetoothOpen:" + BleScanTool.getInstance().isBluetoothOpen(), Constants.SYCN_PATH);
        this.state = 104;
        isConnectionSyn = false;
        this.handler.removeCallbacksAndMessages(null);
    }

    /* access modifiers changed from: private */
    public void handSynSuccess() {
        this.state = 103;
        isConnectionSyn = false;
        LogUtil.dAndSave("handSynSuccess", Constants.SYCN_PATH);
        this.handler.removeCallbacksAndMessages(null);
        SharePreferenceUtils.put(AppApplication.getInstance(), "FIRST_SYNC", false);
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.IS_SYN_HISTORY_KEY, false);
        Calendar instance = Calendar.getInstance();
        instance.add(5, -1);
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.SYNCH_TODAY_DATA_KEY, this.format.format(new Date()));
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.SYNCH_HISTORY_DATA_KEY, this.format.format(instance.getTime()));
        EventBusHelper.post(101);
        for (int i = 0; i < this.callbacks.size(); i++) {
            this.callbacks.get(i).complete(103, null);
        }
    }

    public Context getContext() {
        return AppApplication.getInstance();
    }

    public static void showMessage(Object obj) {
        LogUtil.m5267d2(obj.toString());
        LogService.m6213p(Constants.BLE_PATH, "dAndSave", obj.toString());
    }

    public static void showMessage(Object obj, BleContant.MessageType messageType) {
        LogUtil.m5267d2("" + messageType + Config.TRACE_TODAY_VISIT_SPLIT + obj.toString());
        LogService.m6213p(Constants.BLE_PATH, "dAndSave", obj.toString());
    }

    public static void scanDevices(boolean z) {
        BleScanTool.getInstance().scanLeDevice(z, 8000);
    }

    public static void setScanDeviceListener(BleScanTool.ScanDeviceListener scanDeviceListener) {
        BleScanTool.getInstance().addScanDeviceListener(scanDeviceListener);
    }

    public static void scanDevices() {
        BleScanTool.getInstance().scanLeDevice(true, 8000);
    }
}
