package com.wade.fit.ui.sport.bean;

/* renamed from: com.wade.fit.ui.sport.bean.SportActivityItem */
public class SportActivityItem {
    private String content;
    private String des;
    private int imgResource;

    public SportActivityItem(int i, String str, String str2) {
        this.imgResource = i;
        this.content = str;
        this.des = str2;
    }

    public int getImgResource() {
        return this.imgResource;
    }

    public void setImgResource(int i) {
        this.imgResource = i;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public String getDes() {
        return this.des;
    }

    public void setDes(String str) {
        this.des = str;
    }
}
