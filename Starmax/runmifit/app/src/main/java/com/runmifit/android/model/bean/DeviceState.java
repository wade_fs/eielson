package com.wade.fit.model.bean;

public class DeviceState {
    public int language = 1;
    public int screenLight = 100;
    public int screenTime = 5;
    public int theme = 1;
    public int timeFormat = 0;
    public int unit = 0;
    public int upHander = 0;

    public String toString() {
        return "DeviceState{screenLight=" + this.screenLight + ", screenTime=" + this.screenTime + ", theme=" + this.theme + ", language=" + this.language + ", unit=" + this.unit + ", timeFormat=" + this.timeFormat + ", upHander=" + this.upHander + '}';
    }
}
