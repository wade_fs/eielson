package com.wade.fit.persenter.sport;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.SportRunningBean;

public interface ISportRunView extends IBaseView {
    void bleConnect();

    void bleDisconnect();

    void connecetTimeOut();

    void continueRun(boolean z);

    void getRunData(SportRunningBean sportRunningBean);

    void pauseRun(boolean z);

    void startRun(int i);

    void stopRun(boolean z);
}
