package com.wade.fit.persenter.mine;

import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.http.UserHttp;
import com.wade.fit.persenter.mine.PersonContract;
import com.tamic.novate.util.Utils;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import p041io.reactivex.disposables.Disposable;

public class PersonPresenter extends BasePersenter<PersonContract.View> implements PersonContract.Presenter {
    public void setUserInfo(UserBean userBean) {
        ((PersonContract.View) this.mView).showLoadingFalse();
        UserHttp.setUserInfo(RequestBody.create(MediaType.parse(Utils.MULTIPART_JSON_DATA), "{\"name\":\"" + userBean.getUserName() + "\",\"sex\":\"" + userBean.getGender() + "\",\"birthday\":\"" + userBean.getYear() + "-" + String.format("%02d", Integer.valueOf(userBean.getMonth())) + "-" + String.format("%02d", Integer.valueOf(userBean.getDay())) + "\",\"height\":\"" + userBean.getHeight() + "\",\"weight\":\"" + (userBean.getWeight() * 10) + "\",\"stepSize\":\"" + userBean.getStepDistance() + "\",\"appleHealth\":\"" + false + "\",\"unit\":\"" + userBean.getUnit() + "\"}"), new ApiCallback<BaseBean<String>>() {
            /* class com.wade.fit.persenter.mine.PersonPresenter.C24291 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<String> baseBean) {
                ((PersonContract.View) PersonPresenter.this.mView).requestSuccess();
            }

            public void onFailure(int i, String str) {
                ((PersonContract.View) PersonPresenter.this.mView).hideLoading();
            }

            public void onSubscribe(Disposable disposable) {
                PersonPresenter.this.addDisposable(disposable);
            }
        });
    }
}
