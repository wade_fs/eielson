package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.WebViewFaqActivity_ViewBinding */
public class WebViewFaqActivity_ViewBinding implements Unbinder {
    private WebViewFaqActivity target;

    public WebViewFaqActivity_ViewBinding(WebViewFaqActivity webViewFaqActivity) {
        this(webViewFaqActivity, webViewFaqActivity.getWindow().getDecorView());
    }

    public WebViewFaqActivity_ViewBinding(WebViewFaqActivity webViewFaqActivity, View view) {
        this.target = webViewFaqActivity;
        webViewFaqActivity.mWebView = (WebView) Utils.findRequiredViewAsType(view, R.id.mWebView, "field 'mWebView'", WebView.class);
    }

    public void unbind() {
        WebViewFaqActivity webViewFaqActivity = this.target;
        if (webViewFaqActivity != null) {
            this.target = null;
            webViewFaqActivity.mWebView = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
