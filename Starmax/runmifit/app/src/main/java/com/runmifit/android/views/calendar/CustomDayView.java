package com.wade.fit.views.calendar;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ldf.calendar.Utils;
import com.ldf.calendar.component.State;
import com.ldf.calendar.interf.IDayRenderer;
import com.ldf.calendar.model.CalendarDate;
import com.ldf.calendar.view.DayView;
import com.wade.fit.R;

public class CustomDayView extends DayView {
    private TextView dateTv = ((TextView) findViewById(R.id.date));
    private ImageView marker = ((ImageView) findViewById(R.id.maker));
    private View selectedBackground = findViewById(R.id.selected_background);
    private final CalendarDate today = new CalendarDate();
    private View todayBackground = findViewById(R.id.today_background);

    public CustomDayView(Context context, int i) {
        super(context, i);
    }

    public void refreshContent() {
        renderToday(this.day.getDate());
        renderSelect(this.day.getState());
        renderMarker(this.day.getDate(), this.day.getState());
        super.refreshContent();
    }

    private void renderMarker(CalendarDate calendarDate, State state) {
        if (!Utils.loadMarkData().containsKey(calendarDate.toString())) {
            this.marker.setVisibility(View.GONE);
        } else if (state == State.SELECT || calendarDate.toString().equals(this.today.toString())) {
            this.marker.setVisibility(View.GONE);
        } else {
            this.marker.setVisibility(View.VISIBLE);
            if (Utils.loadMarkData().get(calendarDate.toString()).equals("0")) {
                this.marker.setEnabled(true);
            } else {
                this.marker.setEnabled(false);
            }
        }
    }

    private void renderSelect(State state) {
        if (state == State.SELECT) {
            this.selectedBackground.setVisibility(View.VISIBLE);
            this.dateTv.setTextColor(-1);
            this.dateTv.setBackgroundColor(getResources().getColor(R.color.main_bg_color));
        } else if (state == State.NEXT_MONTH || state == State.PAST_MONTH) {
            this.selectedBackground.setVisibility(View.GONE);
            this.dateTv.setTextColor(Color.parseColor("#d5d5d5"));
            this.dateTv.setBackgroundColor(-1);
        } else {
            this.selectedBackground.setVisibility(View.GONE);
            this.dateTv.setTextColor(Color.parseColor("#111111"));
            this.dateTv.setBackgroundColor(-1);
        }
    }

    private void renderToday(CalendarDate calendarDate) {
        if (calendarDate != null) {
            if (calendarDate.equals(this.today)) {
                this.todayBackground.setVisibility(View.VISIBLE);
            } else {
                this.todayBackground.setVisibility(View.GONE);
            }
            TextView textView = this.dateTv;
            textView.setText(calendarDate.day + "");
        }
    }

    public IDayRenderer copy() {
        return new CustomDayView(this.context, this.layoutResource);
    }
}
