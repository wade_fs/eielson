package com.wade.fit.util.ble;

import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.SleepTimeBean;
import com.wade.fit.util.SPHelper;

public class SleepDataHandler {
    public void handlerSleepData(byte[] bArr) {
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        SleepTimeBean sleepTimeBean = new SleepTimeBean();
        sleepTimeBean.state = ByteDataConvertUtil.Byte2Int(bArr[18]);
        deviceConfig.sleepTimeBean = sleepTimeBean;
        if (ByteDataConvertUtil.Byte2Int(bArr[18]) == 1) {
            sleepTimeBean.state = 1;
        } else {
            sleepTimeBean.state = 0;
        }
        SPHelper.saveDeviceConfig(deviceConfig);
    }
}
