package com.wade.fit.app;

import android.content.Context;
import android.os.StrictMode;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.DiskLogAdapter;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;
import com.wade.fit.util.CrashUtil;
import com.wade.fit.util.log.BlockDetectByPrinter;
import com.wade.fit.util.log.TxtFormatStrategy;
import com.tamic.novate.util.FileUtil;

public class LibContext {
    static LibContext libContext = new LibContext();
    private Context context;

    private void initFacebook() {
    }

    public static LibContext getInstance() {
        return libContext;
    }

    public static Context getAppContext() {
        return libContext.context;
    }

    public void init(Context context2) {
        this.context = context2.getApplicationContext();
        BlockDetectByPrinter.start();
        initFacebook();
        init7_0_Camera();
        initLogger();
    }

    public String getLogPath() {
        String packageName = this.context.getPackageName();
        return packageName.substring(packageName.indexOf(FileUtil.HIDDEN_PREFIX) + 1, packageName.lastIndexOf(FileUtil.HIDDEN_PREFIX));
    }

    private void initLogger() {
        Logger.addLogAdapter(new AndroidLogAdapter(PrettyFormatStrategy.newBuilder().tag(getLogPath()).build()));
        Logger.addLogAdapter(new DiskLogAdapter(TxtFormatStrategy.newBuilder().tag(getLogPath()).build(this.context.getPackageName(), getLogPath())));
    }

    public void initCrashHandler(String str) {
        CrashUtil.getInstance().init(this.context);
        CrashUtil.getInstance().setCrashDir(str);
    }

    private void init7_0_Camera() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
    }
}
