package com.wade.fit.base;

import android.os.Bundle;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.views.calendar.CalendarDialog;
import java.util.Date;

public abstract class BaseCalendarActivity<P extends BasePersenter> extends BaseMvpActivity<P> {
    protected CalendarDialog clendarDialog;
    protected boolean isToday = true;
    protected OnSelectDateListener onSelectDateListener = new OnSelectDateListener() {
        /* class com.wade.fit.base.BaseCalendarActivity.C23831 */

        public void onSelectOtherMonth(int i) {
        }

        public void onSelectDate(CalendarDate calendarDate) {
            BaseCalendarActivity baseCalendarActivity = BaseCalendarActivity.this;
            baseCalendarActivity.selecetCalendarDate = calendarDate;
            baseCalendarActivity.selectedDate = ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day);
            if (ProDbUtils.isOneDay(BaseCalendarActivity.this.selectedDate, ProDbUtils.getTodayDate())) {
                BaseCalendarActivity.this.isToday = true;
            } else {
                BaseCalendarActivity.this.isToday = false;
            }
            BaseCalendarActivity baseCalendarActivity2 = BaseCalendarActivity.this;
            baseCalendarActivity2.updateBySelected(baseCalendarActivity2.selectedDate, BaseCalendarActivity.this.selecetCalendarDate);
        }
    };
    protected CalendarDate selecetCalendarDate;
    protected Date selectedDate;

    /* access modifiers changed from: protected */
    public void updateBySelected(Date date, CalendarDate calendarDate) {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.clendarDialog = new CalendarDialog(this);
        this.clendarDialog.mOnSelectDateListener = this.onSelectDateListener;
    }
}
