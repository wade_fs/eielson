package com.wade.fit.model.bean;

import java.io.Serializable;

public class AppExchangeDataStartPara extends SportType implements Serializable {
    public static int FORCE_START_INVALID = 0;
    public static int FORCE_START_VALID = 1;
    public static int TARGET_TYPE_CALORIES = 3;
    public static int TARGET_TYPE_DISTANCE = 2;
    public static int TARGET_TYPE_DURATIONS = 4;
    public static int TARGET_TYPE_REPEAT_COUNT = 1;
    public static int TARGET_TYPE_UNKNOW = 0;
    private static final long serialVersionUID = 1;
    public int day;
    public int force_start;
    public int hour;
    public int minute;
    public int second;
    public int sportType;
    public int target_type;
    public int target_value;

    public String toString() {
        return "AppExchangeDataStartPara{day=" + this.day + ", hour=" + this.hour + ", minute=" + this.minute + ", second=" + this.second + ", sportType=" + this.sportType + ", target_type=" + this.target_type + ", target_value=" + this.target_value + ", force_start=" + this.force_start + '}';
    }
}
