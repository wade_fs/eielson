package com.wade.fit.ui.device.activity;

import android.content.DialogInterface;
import com.wade.fit.util.ble.BleScanTool;

/* renamed from: com.wade.fit.ui.device.activity.-$$Lambda$ScanDeviceReadyActivity$1icJ1mewZO56Ge5tVUwqbu6i9MA  reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ScanDeviceReadyActivity$1icJ1mewZO56Ge5tVUwqbu6i9MA implements DialogInterface.OnClickListener {
    public static final /* synthetic */ $$Lambda$ScanDeviceReadyActivity$1icJ1mewZO56Ge5tVUwqbu6i9MA INSTANCE = new $$Lambda$ScanDeviceReadyActivity$1icJ1mewZO56Ge5tVUwqbu6i9MA();

    private /* synthetic */ $$Lambda$ScanDeviceReadyActivity$1icJ1mewZO56Ge5tVUwqbu6i9MA() {
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        BleScanTool.getInstance().openBluetooth();
    }
}
