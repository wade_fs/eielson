package com.wade.fit.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ViewUtil;

public class BurningRectView extends View {
    private int MAX_VALUE;

    /* renamed from: h */
    private int f5227h;
    String label;
    private Paint mPaint = new Paint();
    private int normalColor;
    private float normalSize;
    private Paint precentPaint;
    private int rectColor;
    private float rectProportion;
    private String text;
    private float textLeftPadding;
    private String unit;
    private int unitColor;
    private float unitSize;
    private int value;
    private int valueColor;
    private float valueSize;

    /* renamed from: w */
    private int f5228w;

    public BurningRectView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.BurningRectView);
        this.rectColor = obtainStyledAttributes.getColor(2, 0);
        this.normalColor = obtainStyledAttributes.getColor(0, 0);
        this.valueColor = obtainStyledAttributes.getColor(9, 0);
        this.unitColor = obtainStyledAttributes.getColor(7, 0);
        this.textLeftPadding = obtainStyledAttributes.getDimension(5, 20.0f);
        this.rectProportion = obtainStyledAttributes.getDimension(3, 0.7f);
        this.normalSize = obtainStyledAttributes.getDimension(1, 16.0f);
        this.valueSize = obtainStyledAttributes.getDimension(10, 16.0f);
        this.unitSize = obtainStyledAttributes.getDimension(8, 16.0f);
        this.text = obtainStyledAttributes.getString(4);
        this.unit = obtainStyledAttributes.getString(6);
        obtainStyledAttributes.recycle();
        this.mPaint.setAntiAlias(true);
        this.precentPaint = new Paint();
        this.precentPaint.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5228w = i;
        this.f5227h = i2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        String str;
        super.onDraw(canvas);
        this.mPaint.setColor(this.rectColor);
        ViewUtil.drawRect(canvas, 0.0f, 0.0f, (float) this.f5227h, this.value, this.MAX_VALUE, (float) this.f5228w, this.rectProportion, 0.0f, this.mPaint);
        float calculateRectW = this.textLeftPadding + ViewUtil.calculateRectW(this.value, this.MAX_VALUE, (float) this.f5228w, this.rectProportion);
        this.mPaint.setTextAlign(Paint.Align.LEFT);
        this.mPaint.setColor(this.normalColor);
        this.mPaint.setTextSize(this.normalSize);
        Paint.FontMetricsInt fontMetricsInt = this.mPaint.getFontMetricsInt();
        float height = (float) (((getHeight() / 2) - fontMetricsInt.descent) + ((fontMetricsInt.bottom - fontMetricsInt.top) / 2));
        canvas.drawText(this.text, calculateRectW, height, this.mPaint);
        this.precentPaint.setTextSize(this.normalSize);
        this.precentPaint.setColor(this.normalColor);
        canvas.drawText(Math.round((((float) this.value) * 100.0f) / ((float) this.MAX_VALUE)) + "%", (float) ScreenUtil.dip2px(5.0f), height, this.precentPaint);
        this.mPaint.setTextAlign(Paint.Align.LEFT);
        this.mPaint.setColor(this.valueColor);
        this.mPaint.setTextSize(this.valueSize);
        int i = this.f5227h;
        ViewUtil.getTextRectHeight(this.mPaint, String.valueOf(this.label));
        float textRectWidth = ViewUtil.getTextRectWidth(this.mPaint, String.valueOf(this.label));
        canvas.drawText(String.valueOf(this.label), calculateRectW, height, this.mPaint);
        if (this.value > 60) {
            str = (this.value / 60) + AppApplication.getInstance().getResources().getString(R.string.unit_hour) + (this.value % 60) + AppApplication.getInstance().getResources().getString(R.string.unit_minute);
        } else {
            str = (this.value % 60) + AppApplication.getInstance().getResources().getString(R.string.unit_minute);
        }
        this.mPaint.setColor(this.unitColor);
        this.mPaint.setTextSize(this.unitSize);
        canvas.drawText(str, calculateRectW + textRectWidth + this.textLeftPadding, height, this.mPaint);
    }

    public void setData(int i, int i2, String str) {
        this.value = i;
        this.MAX_VALUE = i2;
        this.label = str;
        postInvalidate();
    }
}
