package com.wade.fit.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.util.ViewUtil;

public class CustomGPSView extends View {
    private Bitmap badBg;
    private float baseLine;
    private float bottom;
    private float commonH = 12.0f;
    private Bitmap goodBg;
    private String gps;
    private Paint gpsPaint;
    private int gpsSignalBgColor;
    private int gpsSignalColor;
    private float gpsSignalMaxHeight = 16.0f;
    private String gpsSingalText;
    private float gpsSingalWidth;
    private int gpsTextColor;
    private float gpsTextSize;

    /* renamed from: h */
    private int f5229h;
    private float left;
    private Bitmap noBg;
    private float right;
    private int signalValue = 1;
    private float strongH = 16.0f;
    private float top;

    /* renamed from: w */
    private int f5230w;
    private float weakH = 8.0f;

    public CustomGPSView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CustomGPSView);
        this.gps = obtainStyledAttributes.getString(7);
        this.gpsTextSize = obtainStyledAttributes.getDimension(8, 16.0f);
        this.gpsTextColor = obtainStyledAttributes.getColor(9, 0);
        this.gpsSignalColor = obtainStyledAttributes.getColor(2, 0);
        this.gpsSignalBgColor = obtainStyledAttributes.getColor(1, 0);
        this.gpsSingalWidth = obtainStyledAttributes.getDimension(6, 4.0f);
        this.gpsSingalText = obtainStyledAttributes.getString(5);
        this.goodBg = drawable2Bitmap(obtainStyledAttributes.getDrawable(3));
        this.badBg = drawable2Bitmap(obtainStyledAttributes.getDrawable(0));
        obtainStyledAttributes.recycle();
        this.gpsPaint = new Paint();
        this.gpsPaint.setAntiAlias(true);
        this.gpsPaint.setTextSize(this.gpsTextSize);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        Paint paint = new Paint();
        paint.setTextSize(this.gpsTextSize);
        setMeasuredDimension(Math.round(paint.measureText(this.gps) + ((float) this.goodBg.getWidth())), i2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5230w = i;
        this.f5229h = i2;
        this.baseLine = ((float) i2) - calculationSurplusHeight(this.gpsSignalMaxHeight);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.gpsPaint.setTextAlign(Paint.Align.CENTER);
        this.gpsPaint.setColor(this.gpsTextColor);
        float textRectHeight = ViewUtil.getTextRectHeight(this.gpsPaint, this.gps);
        canvas.drawText(this.gps, (float) (this.f5230w / 3), ((float) this.f5229h) - calculationSurplusHeight(textRectHeight), this.gpsPaint);
        float f = ((float) (this.f5230w * 3)) / 5.0f;
        this.gpsPaint.setTextAlign(Paint.Align.RIGHT);
        int i = this.signalValue;
        if (i != 1) {
            if (i == 2) {
                canvas.drawBitmap(this.badBg, f, ((float) (this.f5229h - this.goodBg.getHeight())) / 2.0f, this.gpsPaint);
            } else if (i == 0) {
                Bitmap bitmap = this.goodBg;
                canvas.drawBitmap(bitmap, f, ((float) (this.f5229h - bitmap.getHeight())) / 2.0f, this.gpsPaint);
            }
        }
    }

    private void drawGPSSignalStrength(Canvas canvas, int i, int i2, int i3) {
        float f = (float) ((this.f5230w * 3) / 4);
        this.gpsPaint.setTextAlign(Paint.Align.CENTER);
        this.gpsPaint.setColor(i2);
        float f2 = this.gpsSingalWidth;
        this.left = f - (f2 / 2.0f);
        float f3 = this.baseLine;
        this.top = f3 - this.commonH;
        this.right = (f2 / 2.0f) + f;
        this.bottom = f3;
        Canvas canvas2 = canvas;
        canvas2.drawRect(this.left, this.top, this.right, this.bottom, this.gpsPaint);
        this.gpsPaint.setTextAlign(Paint.Align.RIGHT);
        this.gpsPaint.setColor(i);
        float f4 = this.gpsSingalWidth;
        this.left = (f - (f4 / 2.0f)) - (f4 * 2.0f);
        float f5 = this.baseLine;
        this.top = f5 - this.weakH;
        this.right = (f - (f4 / 2.0f)) - f4;
        this.bottom = f5;
        canvas2.drawRect(this.left, this.top, this.right, this.bottom, this.gpsPaint);
        this.gpsPaint.setTextAlign(Paint.Align.LEFT);
        this.gpsPaint.setColor(i3);
        float f6 = this.gpsSingalWidth;
        this.left = (f6 / 2.0f) + f + f6;
        float f7 = this.baseLine;
        this.top = f7 - this.strongH;
        this.right = f + (f6 / 2.0f) + (f6 * 2.0f);
        this.bottom = f7;
        canvas.drawRect(this.left, this.top, this.right, this.bottom, this.gpsPaint);
    }

    private float calculationSurplusHeight(float f) {
        return (((float) this.f5229h) - f) / 2.0f;
    }

    public void setGPSSignalStrength(int i) {
        this.signalValue = i;
        invalidate();
    }

    public int getGPSSignalStrength() {
        return this.signalValue;
    }

    private Bitmap drawable2Bitmap(Drawable drawable) {
        return ((BitmapDrawable) drawable).getBitmap();
    }
}
