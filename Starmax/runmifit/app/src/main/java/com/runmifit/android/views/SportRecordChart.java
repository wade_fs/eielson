package com.wade.fit.views;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.model.bean.BarChartProperties;
import com.wade.fit.model.bean.DetailType;
import com.wade.fit.util.NumUtil;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ViewUtil;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.DebugLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SportRecordChart extends View {
    private final int ANIM2PROGRESS_MAX = 30;
    private float MAX_VALUE;
    private float MIDDLE_VALUE;
    private float MIN_VALUE;
    List<Map<String, Float>> Xvalue_distance = new ArrayList();
    List<Map<String, Float>> Xvalue_kll = new ArrayList();
    List<Map<String, Float>> Xvalue_step = new ArrayList();
    private ObjectAnimator anim1;
    private int anim1Progress;
    private ObjectAnimator anim2;
    private int anim2Progress;
    private ObjectAnimator anim3;
    private boolean animing1 = true;
    private boolean animing2;
    private boolean animing3;
    private int barColor;
    private int barNum = 96;
    private Paint barPaint;
    private float barWid;
    private float barWidth;
    private int bitmapH;
    private int bitmapW;
    private float bottomH;
    private float centerH;
    private Paint dataPaint;
    List<String> dateDistanceList = new ArrayList();
    List<String> dateKllList = new ArrayList();
    List<String> dateStepList = new ArrayList();
    DetailType detailType;
    List<String> distanceValueList = new ArrayList();
    float downX = 0.0f;
    float downY = 0.0f;
    private int exelColor = -1;
    Paint exelPaint = new Paint(1);
    private Bitmap finishAfterBitmap;
    private Bitmap finishBeforeBitmap;
    private List<HealthSportItem> healthSportItems = new ArrayList();
    private boolean initDraw;
    private boolean isDouble;
    private boolean isDrawTAB;
    private boolean isToday;
    boolean is_down = false;
    List<String> kllValueList = new ArrayList();
    private float[] lineHeight = new float[3];
    private LinearGradient linearGradient = null;
    private int mHeight;
    private int mWidth;
    private int middleIndex;
    float paddingTop;
    private NinePatchDrawable popup;
    private NinePatchDrawable popupCenter;
    private NinePatchDrawable popupLeft;
    private NinePatchDrawable popupRight;
    private float radTopPadding;
    private RectF selectRect = new RectF();
    private Bitmap sportBitmap;
    private float sportToOneLineH;
    List<String> stepValueList = new ArrayList();
    private Paint tagPaint;
    private Paint tagTextPaint;
    private int tempSteps = 0;
    private int textColor;
    private Paint textPaint;
    private float textSize;
    private Drawable topDrawable;
    private float topH;
    private Bitmap touchBg;
    Paint touchBgPaint;
    private Paint touchPaint;
    private Paint touchPaint2;
    private float touchX;
    private float touchY;
    private Paint unitPaint;
    private List<String> xLables = new ArrayList();
    private float xOffSet;
    private float xWidth;
    private float yAxisLength;
    private float[] yHeight = new float[4];
    private int yLineColor = -1447447;
    private float yScale;
    private int[] yVelocitys;
    private float yZero;

    public SportRecordChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SportBarChart);
        this.textSize = obtainStyledAttributes.getDimension(0, 28.0f);
        this.textColor = obtainStyledAttributes.getColor(1, -1);
        this.barColor = obtainStyledAttributes.getColor(2, -1092544);
        this.topDrawable = obtainStyledAttributes.getDrawable(3);
        obtainStyledAttributes.recycle();
        initPaint();
        this.sportBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.sport_bar_chart_top);
        this.popupLeft = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup_left);
        this.popupRight = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup_right);
        this.popupCenter = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup);
        this.touchBg = BitmapFactory.decodeResource(getResources(), R.drawable.detail_touch_bg);
        this.finishBeforeBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.finish_before);
        this.finishAfterBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.finish_after);
        this.radTopPadding = (float) (this.finishBeforeBitmap.getHeight() > this.finishBeforeBitmap.getWidth() ? this.finishBeforeBitmap.getHeight() : this.finishBeforeBitmap.getWidth());
    }

    private void initPaint() {
        this.barPaint = new Paint(1);
        this.barPaint.setColor(this.barColor);
        this.touchPaint = new Paint(1);
        this.touchBgPaint = new Paint(1);
        this.touchPaint2 = new Paint(1);
        this.textPaint = new Paint(1);
        this.textPaint.setColor(this.textColor);
        this.textPaint.setTextAlign(Paint.Align.CENTER);
        this.textPaint.setTextSize(this.textSize);
        this.dataPaint = new Paint(1);
        this.dataPaint.setColor(this.textColor);
        this.dataPaint.setTextAlign(Paint.Align.RIGHT);
        this.dataPaint.setTextSize(this.textSize * 1.8f);
        this.unitPaint = new Paint(1);
        this.unitPaint.setColor(this.textColor);
        this.unitPaint.setTextSize(this.textSize);
        this.unitPaint.setTextAlign(Paint.Align.LEFT);
        this.tagPaint = new Paint();
        this.tagPaint.setColor(-7829368);
        this.tagPaint.setAntiAlias(true);
        this.tagTextPaint = new Paint();
        this.tagTextPaint.setColor(-1);
        this.tagTextPaint.setTextSize((float) ScreenUtil.dip2px(8.0f));
        this.tagTextPaint.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.mWidth = i;
        this.mHeight = i2;
        calculate();
        initYscale();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action == 1) {
                this.is_down = false;
                invalidate();
            } else if (action != 2) {
                if (action == 3) {
                    this.touchX = -1.0f;
                    this.touchY = -1.0f;
                }
            }
            return true;
        }
        this.downX = motionEvent.getX();
        this.downY = motionEvent.getY();
        this.is_down = true;
        invalidate();
        this.touchX = motionEvent.getX();
        this.touchY = motionEvent.getX();
        if (Math.abs(this.touchX - this.downX) > ((float) ScreenUtil.dip2px(15.0f))) {
            this.is_down = false;
            invalidate();
        }
        return true;
    }

    private void calculate() {
        this.bitmapH = this.sportBitmap.getHeight();
        this.bitmapW = this.sportBitmap.getWidth();
        this.topH = (float) this.bitmapH;
        this.bottomH = ViewUtil.getTextHeight(this.dataPaint);
        this.paddingTop = (float) ScreenUtil.dip2px(0.0f);
        int i = this.mHeight;
        float f = this.topH;
        this.centerH = ((((float) i) - f) - this.bottomH) - this.paddingTop;
        this.sportToOneLineH = (float) (i / 30);
        float[] fArr = this.yHeight;
        float f2 = this.sportToOneLineH;
        fArr[0] = f2;
        fArr[1] = f2 + f;
        fArr[2] = f + this.centerH;
        fArr[3] = (float) (i - ScreenUtil.dp2px(3.0f, (Activity) getContext()));
        float[] fArr2 = this.lineHeight;
        float f3 = this.topH;
        float f4 = this.sportToOneLineH;
        fArr2[0] = f3 + f4 + f4;
        float f5 = fArr2[0];
        float f6 = this.centerH;
        fArr2[1] = (f5 + (f6 / 2.0f)) - f4;
        fArr2[2] = (f3 + (f6 - f4)) - f4;
        this.barPaint.setTextSize(this.textSize);
        this.xOffSet = ViewUtil.getTextRectWidth(this.barPaint, "100");
        this.barWid = (float) ScreenUtil.dip2px(10.0f);
        DebugLog.m6203d("mWidth= " + this.mWidth + ",xOffSet= " + this.xOffSet + ",(mWidth - 2 * xOffSet)= " + (((float) this.mWidth) - (this.xOffSet * 2.0f)) + ",barWidth= " + this.barWidth + ",barWid= " + this.barWid);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.exelPaint.setColor(this.exelColor);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.exelPaint);
        drawY(canvas);
        drawXLine(canvas);
        drawX(canvas);
        boolean z = this.is_down;
        drawTouch(canvas);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:72:0x05d9, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void drawDataTag(android.graphics.Canvas r17) {
        /*
            r16 = this;
            r1 = r16
            r0 = r17
            monitor-enter(r16)
            com.wade.fit.model.bean.DetailType r2 = r1.detailType     // Catch:{ all -> 0x05da }
            com.wade.fit.model.bean.DetailType r3 = com.wade.fit.model.bean.DetailType.STEP     // Catch:{ all -> 0x05da }
            r4 = 1104674816(0x41d80000, float:27.0)
            r5 = 1099431936(0x41880000, float:17.0)
            r6 = 1073741824(0x40000000, float:2.0)
            r7 = 0
            r8 = 1084227584(0x40a00000, float:5.0)
            r9 = 1097859072(0x41700000, float:15.0)
            r10 = 2
            if (r2 != r3) goto L_0x01fe
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r2 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            if (r2 == 0) goto L_0x01fe
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r2 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            int r2 = r2.size()     // Catch:{ all -> 0x05da }
            if (r2 <= 0) goto L_0x01fe
            r2 = 0
        L_0x0024:
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r3 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            int r3 = r3.size()     // Catch:{ all -> 0x05da }
            if (r2 >= r3) goto L_0x01fe
            float r3 = r1.downX     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            int r3 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r3 <= 0) goto L_0x01fa
            float r3 = r1.downX     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x2"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            int r3 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r3 >= 0) goto L_0x01fa
            java.util.List<java.lang.String> r3 = r1.stepValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x05da }
            int r3 = r3.length()     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r11 = r1.dateStepList     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x05da }
            int r11 = r11.length()     // Catch:{ all -> 0x05da }
            if (r3 <= r11) goto L_0x0109
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x05da }
            r3.<init>()     // Catch:{ all -> 0x05da }
            android.graphics.Paint r11 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r12 = r1.stepValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r12 = r12.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r13 = r1.stepValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r13 = r13.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x05da }
            int r13 = r13.length()     // Catch:{ all -> 0x05da }
            r11.getTextBounds(r12, r7, r13, r3)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r7 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r7 = (java.util.Map) r7     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x2"
            java.lang.Object r7 = r7.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r7 = (java.lang.Float) r7     // Catch:{ all -> 0x05da }
            float r7 = r7.floatValue()     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 - r11
            float r7 = r7 / r6
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r6 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r6 = (java.util.Map) r6     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x1"
            java.lang.Object r6 = r6.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r6 = (java.lang.Float) r6     // Catch:{ all -> 0x05da }
            float r6 = r6.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 + r6
            int r6 = r3.left     // Catch:{ all -> 0x05da }
            int r11 = r3.right     // Catch:{ all -> 0x05da }
            int r6 = r6 + r11
            int r6 = r6 / r10
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            int r11 = r3.width()     // Catch:{ all -> 0x05da }
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r11 = r11 + r12
            int r11 = r11 / r10
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            float r11 = r7 - r11
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r12 = (float) r12     // Catch:{ all -> 0x05da }
            int r13 = r3.width()     // Catch:{ all -> 0x05da }
            int r14 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r13 = r13 + r14
            int r13 = r13 / r10
            float r13 = (float) r13     // Catch:{ all -> 0x05da }
            float r13 = r13 + r7
            int r14 = r3.height()     // Catch:{ all -> 0x05da }
            int r15 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
        L_0x0104:
            int r14 = r14 + r15
            float r14 = (float) r14     // Catch:{ all -> 0x05da }
            float r14 = r14 + r12
            goto L_0x0195
        L_0x0109:
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x05da }
            r3.<init>()     // Catch:{ all -> 0x05da }
            android.graphics.Paint r11 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r12 = r1.dateStepList     // Catch:{ all -> 0x05da }
            java.lang.Object r12 = r12.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r13 = r1.dateStepList     // Catch:{ all -> 0x05da }
            java.lang.Object r13 = r13.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x05da }
            int r13 = r13.length()     // Catch:{ all -> 0x05da }
            r11.getTextBounds(r12, r7, r13, r3)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r7 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r7 = (java.util.Map) r7     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x2"
            java.lang.Object r7 = r7.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r7 = (java.lang.Float) r7     // Catch:{ all -> 0x05da }
            float r7 = r7.floatValue()     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 - r11
            float r7 = r7 / r6
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r6 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r6 = (java.util.Map) r6     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x1"
            java.lang.Object r6 = r6.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r6 = (java.lang.Float) r6     // Catch:{ all -> 0x05da }
            float r6 = r6.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 + r6
            int r6 = r3.left     // Catch:{ all -> 0x05da }
            int r11 = r3.right     // Catch:{ all -> 0x05da }
            int r6 = r6 + r11
            int r6 = r6 / r10
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            int r11 = r3.width()     // Catch:{ all -> 0x05da }
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r11 = r11 + r12
            int r11 = r11 / r10
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            float r11 = r7 - r11
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r12 = (float) r12     // Catch:{ all -> 0x05da }
            int r13 = r3.width()     // Catch:{ all -> 0x05da }
            int r14 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r13 = r13 + r14
            int r13 = r13 / r10
            float r13 = (float) r13     // Catch:{ all -> 0x05da }
            float r13 = r13 + r7
            int r14 = r3.height()     // Catch:{ all -> 0x05da }
            int r15 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            goto L_0x0104
        L_0x0195:
            android.graphics.RectF r15 = new android.graphics.RectF     // Catch:{ all -> 0x05da }
            r15.<init>(r11, r12, r13, r14)     // Catch:{ all -> 0x05da }
            int r11 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            int r8 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r8 = (float) r8     // Catch:{ all -> 0x05da }
            android.graphics.Paint r12 = r1.tagPaint     // Catch:{ all -> 0x05da }
            r0.drawRoundRect(r15, r11, r8, r12)     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r8 = r1.stepValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r8 = r8.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x05da }
            float r7 = r7 - r6
            int r6 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            android.graphics.Paint r9 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            r0.drawText(r8, r7, r6, r9)     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r6 = r1.dateStepList     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x05da }
            int r5 = com.wade.fit.util.ScreenUtil.dip2px(r5)     // Catch:{ all -> 0x05da }
            int r3 = r3.height()     // Catch:{ all -> 0x05da }
            int r5 = r5 + r3
            float r3 = (float) r5     // Catch:{ all -> 0x05da }
            android.graphics.Paint r5 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            r0.drawText(r6, r7, r3, r5)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r3 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r2 = r3.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r2 = (java.util.Map) r2     // Catch:{ all -> 0x05da }
            java.lang.String r3 = "x1"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x05da }
            java.lang.Float r2 = (java.lang.Float) r2     // Catch:{ all -> 0x05da }
            float r5 = r2.floatValue()     // Catch:{ all -> 0x05da }
            int r2 = com.wade.fit.util.ScreenUtil.dip2px(r4)     // Catch:{ all -> 0x05da }
            float r4 = (float) r2     // Catch:{ all -> 0x05da }
            float[] r2 = r1.lineHeight     // Catch:{ all -> 0x05da }
            r6 = r2[r10]     // Catch:{ all -> 0x05da }
            android.graphics.Paint r7 = r1.textPaint     // Catch:{ all -> 0x05da }
            r2 = r17
            r3 = r5
            r2.drawLine(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x05da }
            monitor-exit(r16)
            return
        L_0x01fa:
            int r2 = r2 + 1
            goto L_0x0024
        L_0x01fe:
            com.wade.fit.model.bean.DetailType r2 = r1.detailType     // Catch:{ all -> 0x05da }
            com.wade.fit.model.bean.DetailType r3 = com.wade.fit.model.bean.DetailType.CAL     // Catch:{ all -> 0x05da }
            if (r2 != r3) goto L_0x03eb
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r2 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            if (r2 == 0) goto L_0x03eb
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r2 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            int r2 = r2.size()     // Catch:{ all -> 0x05da }
            if (r2 <= 0) goto L_0x03eb
            r2 = 0
        L_0x0211:
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r3 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            int r3 = r3.size()     // Catch:{ all -> 0x05da }
            if (r2 >= r3) goto L_0x03eb
            float r3 = r1.downX     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            int r3 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r3 <= 0) goto L_0x03e7
            float r3 = r1.downX     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x2"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            int r3 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r3 >= 0) goto L_0x03e7
            java.util.List<java.lang.String> r3 = r1.kllValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x05da }
            int r3 = r3.length()     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r11 = r1.dateKllList     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x05da }
            int r11 = r11.length()     // Catch:{ all -> 0x05da }
            if (r3 <= r11) goto L_0x02f6
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x05da }
            r3.<init>()     // Catch:{ all -> 0x05da }
            android.graphics.Paint r11 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r12 = r1.kllValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r12 = r12.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r13 = r1.kllValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r13 = r13.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x05da }
            int r13 = r13.length()     // Catch:{ all -> 0x05da }
            r11.getTextBounds(r12, r7, r13, r3)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r7 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r7 = (java.util.Map) r7     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x2"
            java.lang.Object r7 = r7.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r7 = (java.lang.Float) r7     // Catch:{ all -> 0x05da }
            float r7 = r7.floatValue()     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_step     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 - r11
            float r7 = r7 / r6
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r6 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r6 = (java.util.Map) r6     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x1"
            java.lang.Object r6 = r6.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r6 = (java.lang.Float) r6     // Catch:{ all -> 0x05da }
            float r6 = r6.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 + r6
            int r6 = r3.left     // Catch:{ all -> 0x05da }
            int r11 = r3.right     // Catch:{ all -> 0x05da }
            int r6 = r6 + r11
            int r6 = r6 / r10
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            int r11 = r3.width()     // Catch:{ all -> 0x05da }
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r11 = r11 + r12
            int r11 = r11 / r10
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            float r11 = r7 - r11
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r12 = (float) r12     // Catch:{ all -> 0x05da }
            int r13 = r3.width()     // Catch:{ all -> 0x05da }
            int r14 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r13 = r13 + r14
            int r13 = r13 / r10
            float r13 = (float) r13     // Catch:{ all -> 0x05da }
            float r13 = r13 + r7
            int r14 = r3.height()     // Catch:{ all -> 0x05da }
            int r15 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
        L_0x02f1:
            int r14 = r14 + r15
            float r14 = (float) r14     // Catch:{ all -> 0x05da }
            float r14 = r14 + r12
            goto L_0x0382
        L_0x02f6:
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x05da }
            r3.<init>()     // Catch:{ all -> 0x05da }
            android.graphics.Paint r11 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r12 = r1.dateKllList     // Catch:{ all -> 0x05da }
            java.lang.Object r12 = r12.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r13 = r1.dateKllList     // Catch:{ all -> 0x05da }
            java.lang.Object r13 = r13.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x05da }
            int r13 = r13.length()     // Catch:{ all -> 0x05da }
            r11.getTextBounds(r12, r7, r13, r3)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r7 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r7 = (java.util.Map) r7     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x2"
            java.lang.Object r7 = r7.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r7 = (java.lang.Float) r7     // Catch:{ all -> 0x05da }
            float r7 = r7.floatValue()     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 - r11
            float r7 = r7 / r6
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r6 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r6 = (java.util.Map) r6     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x1"
            java.lang.Object r6 = r6.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r6 = (java.lang.Float) r6     // Catch:{ all -> 0x05da }
            float r6 = r6.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 + r6
            int r6 = r3.left     // Catch:{ all -> 0x05da }
            int r11 = r3.right     // Catch:{ all -> 0x05da }
            int r6 = r6 + r11
            int r6 = r6 / r10
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            int r11 = r3.width()     // Catch:{ all -> 0x05da }
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r11 = r11 + r12
            int r11 = r11 / r10
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            float r11 = r7 - r11
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r12 = (float) r12     // Catch:{ all -> 0x05da }
            int r13 = r3.width()     // Catch:{ all -> 0x05da }
            int r14 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r13 = r13 + r14
            int r13 = r13 / r10
            float r13 = (float) r13     // Catch:{ all -> 0x05da }
            float r13 = r13 + r7
            int r14 = r3.height()     // Catch:{ all -> 0x05da }
            int r15 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            goto L_0x02f1
        L_0x0382:
            android.graphics.RectF r15 = new android.graphics.RectF     // Catch:{ all -> 0x05da }
            r15.<init>(r11, r12, r13, r14)     // Catch:{ all -> 0x05da }
            int r11 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            int r8 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r8 = (float) r8     // Catch:{ all -> 0x05da }
            android.graphics.Paint r12 = r1.tagPaint     // Catch:{ all -> 0x05da }
            r0.drawRoundRect(r15, r11, r8, r12)     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r8 = r1.kllValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r8 = r8.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x05da }
            float r7 = r7 - r6
            int r6 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            android.graphics.Paint r9 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            r0.drawText(r8, r7, r6, r9)     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r6 = r1.dateKllList     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x05da }
            int r5 = com.wade.fit.util.ScreenUtil.dip2px(r5)     // Catch:{ all -> 0x05da }
            int r3 = r3.height()     // Catch:{ all -> 0x05da }
            int r5 = r5 + r3
            float r3 = (float) r5     // Catch:{ all -> 0x05da }
            android.graphics.Paint r5 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            r0.drawText(r6, r7, r3, r5)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r3 = r1.Xvalue_kll     // Catch:{ all -> 0x05da }
            java.lang.Object r2 = r3.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r2 = (java.util.Map) r2     // Catch:{ all -> 0x05da }
            java.lang.String r3 = "x1"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x05da }
            java.lang.Float r2 = (java.lang.Float) r2     // Catch:{ all -> 0x05da }
            float r5 = r2.floatValue()     // Catch:{ all -> 0x05da }
            int r2 = com.wade.fit.util.ScreenUtil.dip2px(r4)     // Catch:{ all -> 0x05da }
            float r4 = (float) r2     // Catch:{ all -> 0x05da }
            float[] r2 = r1.lineHeight     // Catch:{ all -> 0x05da }
            r6 = r2[r10]     // Catch:{ all -> 0x05da }
            android.graphics.Paint r7 = r1.textPaint     // Catch:{ all -> 0x05da }
            r2 = r17
            r3 = r5
            r2.drawLine(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x05da }
            monitor-exit(r16)
            return
        L_0x03e7:
            int r2 = r2 + 1
            goto L_0x0211
        L_0x03eb:
            com.wade.fit.model.bean.DetailType r2 = r1.detailType     // Catch:{ all -> 0x05da }
            com.wade.fit.model.bean.DetailType r3 = com.wade.fit.model.bean.DetailType.DISTANCE     // Catch:{ all -> 0x05da }
            if (r2 != r3) goto L_0x05d8
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r2 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            if (r2 == 0) goto L_0x05d8
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r2 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            int r2 = r2.size()     // Catch:{ all -> 0x05da }
            if (r2 <= 0) goto L_0x05d8
            r2 = 0
        L_0x03fe:
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r3 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            int r3 = r3.size()     // Catch:{ all -> 0x05da }
            if (r2 >= r3) goto L_0x05d8
            float r3 = r1.downX     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            int r3 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r3 <= 0) goto L_0x05d4
            float r3 = r1.downX     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x2"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            int r3 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r3 >= 0) goto L_0x05d4
            java.util.List<java.lang.String> r3 = r1.distanceValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x05da }
            int r3 = r3.length()     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r11 = r1.dateDistanceList     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x05da }
            int r11 = r11.length()     // Catch:{ all -> 0x05da }
            if (r3 <= r11) goto L_0x04e3
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x05da }
            r3.<init>()     // Catch:{ all -> 0x05da }
            android.graphics.Paint r11 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r12 = r1.distanceValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r12 = r12.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r13 = r1.distanceValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r13 = r13.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x05da }
            int r13 = r13.length()     // Catch:{ all -> 0x05da }
            r11.getTextBounds(r12, r7, r13, r3)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r7 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r7 = (java.util.Map) r7     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x2"
            java.lang.Object r7 = r7.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r7 = (java.lang.Float) r7     // Catch:{ all -> 0x05da }
            float r7 = r7.floatValue()     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 - r11
            float r7 = r7 / r6
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r6 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r6 = (java.util.Map) r6     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x1"
            java.lang.Object r6 = r6.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r6 = (java.lang.Float) r6     // Catch:{ all -> 0x05da }
            float r6 = r6.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 + r6
            int r6 = r3.left     // Catch:{ all -> 0x05da }
            int r11 = r3.right     // Catch:{ all -> 0x05da }
            int r6 = r6 + r11
            int r6 = r6 / r10
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            int r11 = r3.width()     // Catch:{ all -> 0x05da }
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r11 = r11 + r12
            int r11 = r11 / r10
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            float r11 = r7 - r11
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r12 = (float) r12     // Catch:{ all -> 0x05da }
            int r13 = r3.width()     // Catch:{ all -> 0x05da }
            int r14 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r13 = r13 + r14
            int r13 = r13 / r10
            float r13 = (float) r13     // Catch:{ all -> 0x05da }
            float r13 = r13 + r7
            int r14 = r3.height()     // Catch:{ all -> 0x05da }
            int r15 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
        L_0x04de:
            int r14 = r14 + r15
            float r14 = (float) r14     // Catch:{ all -> 0x05da }
            float r14 = r14 + r12
            goto L_0x056f
        L_0x04e3:
            android.graphics.Rect r3 = new android.graphics.Rect     // Catch:{ all -> 0x05da }
            r3.<init>()     // Catch:{ all -> 0x05da }
            android.graphics.Paint r11 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r12 = r1.dateDistanceList     // Catch:{ all -> 0x05da }
            java.lang.Object r12 = r12.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r13 = r1.dateDistanceList     // Catch:{ all -> 0x05da }
            java.lang.Object r13 = r13.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x05da }
            int r13 = r13.length()     // Catch:{ all -> 0x05da }
            r11.getTextBounds(r12, r7, r13, r3)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r7 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r7 = (java.util.Map) r7     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x2"
            java.lang.Object r7 = r7.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r7 = (java.lang.Float) r7     // Catch:{ all -> 0x05da }
            float r7 = r7.floatValue()     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r11 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r11 = (java.util.Map) r11     // Catch:{ all -> 0x05da }
            java.lang.String r12 = "x1"
            java.lang.Object r11 = r11.get(r12)     // Catch:{ all -> 0x05da }
            java.lang.Float r11 = (java.lang.Float) r11     // Catch:{ all -> 0x05da }
            float r11 = r11.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 - r11
            float r7 = r7 / r6
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r6 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r6 = (java.util.Map) r6     // Catch:{ all -> 0x05da }
            java.lang.String r11 = "x1"
            java.lang.Object r6 = r6.get(r11)     // Catch:{ all -> 0x05da }
            java.lang.Float r6 = (java.lang.Float) r6     // Catch:{ all -> 0x05da }
            float r6 = r6.floatValue()     // Catch:{ all -> 0x05da }
            float r7 = r7 + r6
            int r6 = r3.left     // Catch:{ all -> 0x05da }
            int r11 = r3.right     // Catch:{ all -> 0x05da }
            int r6 = r6 + r11
            int r6 = r6 / r10
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            int r11 = r3.width()     // Catch:{ all -> 0x05da }
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r11 = r11 + r12
            int r11 = r11 / r10
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            float r11 = r7 - r11
            int r12 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r12 = (float) r12     // Catch:{ all -> 0x05da }
            int r13 = r3.width()     // Catch:{ all -> 0x05da }
            int r14 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            int r13 = r13 + r14
            int r13 = r13 / r10
            float r13 = (float) r13     // Catch:{ all -> 0x05da }
            float r13 = r13 + r7
            int r14 = r3.height()     // Catch:{ all -> 0x05da }
            int r15 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            goto L_0x04de
        L_0x056f:
            android.graphics.RectF r15 = new android.graphics.RectF     // Catch:{ all -> 0x05da }
            r15.<init>(r11, r12, r13, r14)     // Catch:{ all -> 0x05da }
            int r11 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r11 = (float) r11     // Catch:{ all -> 0x05da }
            int r8 = com.wade.fit.util.ScreenUtil.dip2px(r8)     // Catch:{ all -> 0x05da }
            float r8 = (float) r8     // Catch:{ all -> 0x05da }
            android.graphics.Paint r12 = r1.tagPaint     // Catch:{ all -> 0x05da }
            r0.drawRoundRect(r15, r11, r8, r12)     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r8 = r1.distanceValueList     // Catch:{ all -> 0x05da }
            java.lang.Object r8 = r8.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x05da }
            float r7 = r7 - r6
            int r6 = com.wade.fit.util.ScreenUtil.dip2px(r9)     // Catch:{ all -> 0x05da }
            float r6 = (float) r6     // Catch:{ all -> 0x05da }
            android.graphics.Paint r9 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            r0.drawText(r8, r7, r6, r9)     // Catch:{ all -> 0x05da }
            java.util.List<java.lang.String> r6 = r1.dateDistanceList     // Catch:{ all -> 0x05da }
            java.lang.Object r6 = r6.get(r2)     // Catch:{ all -> 0x05da }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x05da }
            int r5 = com.wade.fit.util.ScreenUtil.dip2px(r5)     // Catch:{ all -> 0x05da }
            int r3 = r3.height()     // Catch:{ all -> 0x05da }
            int r5 = r5 + r3
            float r3 = (float) r5     // Catch:{ all -> 0x05da }
            android.graphics.Paint r5 = r1.tagTextPaint     // Catch:{ all -> 0x05da }
            r0.drawText(r6, r7, r3, r5)     // Catch:{ all -> 0x05da }
            java.util.List<java.util.Map<java.lang.String, java.lang.Float>> r3 = r1.Xvalue_distance     // Catch:{ all -> 0x05da }
            java.lang.Object r2 = r3.get(r2)     // Catch:{ all -> 0x05da }
            java.util.Map r2 = (java.util.Map) r2     // Catch:{ all -> 0x05da }
            java.lang.String r3 = "x1"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x05da }
            java.lang.Float r2 = (java.lang.Float) r2     // Catch:{ all -> 0x05da }
            float r5 = r2.floatValue()     // Catch:{ all -> 0x05da }
            int r2 = com.wade.fit.util.ScreenUtil.dip2px(r4)     // Catch:{ all -> 0x05da }
            float r4 = (float) r2     // Catch:{ all -> 0x05da }
            float[] r2 = r1.lineHeight     // Catch:{ all -> 0x05da }
            r6 = r2[r10]     // Catch:{ all -> 0x05da }
            android.graphics.Paint r7 = r1.textPaint     // Catch:{ all -> 0x05da }
            r2 = r17
            r3 = r5
            r2.drawLine(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x05da }
            monitor-exit(r16)
            return
        L_0x05d4:
            int r2 = r2 + 1
            goto L_0x03fe
        L_0x05d8:
            monitor-exit(r16)
            return
        L_0x05da:
            r0 = move-exception
            monitor-exit(r16)
            goto L_0x05de
        L_0x05dd:
            throw r0
        L_0x05de:
            goto L_0x05dd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.SportRecordChart.drawDataTag(android.graphics.Canvas):void");
    }

    private void drawTouch(Canvas canvas) {
        if (this.touchX == -1.0f || this.touchY == -1.0f) {
        }
    }

    private void drawNoData(Canvas canvas) {
        this.barPaint.setTextSize(this.textSize * 1.5f);
        this.barPaint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(getResources().getString(R.string.state_no_data), (float) (this.mWidth / 2), (float) (this.mHeight / 2), this.barPaint);
    }

    private void drawXLine(Canvas canvas) {
        this.barPaint.setStrokeWidth(1.0f);
        this.barPaint.setColor(this.yLineColor);
        float f = this.xOffSet;
        float[] fArr = this.lineHeight;
        float f2 = fArr[2];
        float f3 = this.yScale;
        float f4 = this.MAX_VALUE;
        float f5 = f2 - (f3 * f4);
        float f6 = this.paddingTop;
        canvas.drawLine(f * 1.5f, f5 + f6, ((float) this.mWidth) - (f * 1.5f), (fArr[2] - (f3 * f4)) + f6, this.barPaint);
        float f7 = this.xOffSet;
        float[] fArr2 = this.lineHeight;
        float f8 = fArr2[2];
        float f9 = this.yScale;
        float f10 = this.MIDDLE_VALUE;
        float f11 = f8 - (f9 * f10);
        float f12 = this.paddingTop;
        canvas.drawLine(f7 * 1.5f, f11 + f12, ((float) this.mWidth) - (f7 * 1.5f), (fArr2[2] - (f9 * f10)) + f12, this.barPaint);
        float f13 = this.xOffSet;
        float[] fArr3 = this.lineHeight;
        float f14 = fArr3[2];
        float f15 = this.yScale;
        float f16 = this.MIN_VALUE;
        float f17 = f14 - (f15 * f16);
        float f18 = this.paddingTop;
        canvas.drawLine(f13 * 1.5f, f17 + f18, ((float) this.mWidth) - (f13 * 1.5f), (fArr3[2] - (f15 * f16)) + f18, this.barPaint);
    }

    private void drawX(Canvas canvas) {
        int i;
        float f = this.xOffSet;
        float f2 = 1.5f;
        float[] fArr = this.lineHeight;
        float f3 = fArr[2];
        float f4 = this.paddingTop;
        canvas.drawLine(f * 1.5f, f3 + f4, ((float) this.mWidth) - (f * 1.5f), fArr[2] + f4, this.barPaint);
        this.xWidth = ((float) this.mWidth) - (this.xOffSet * 3.0f);
        int size = this.xLables.size();
        int i2 = size - 1;
        float f5 = this.xWidth / ((float) i2);
        float textHeight = ViewUtil.getTextHeight(this.barPaint);
        this.barPaint.setColor(getResources().getColor(R.color.item_text_color));
        for (int i3 = 0; i3 < size; i3++) {
            float f6 = (((float) i3) * f5) + (this.xOffSet * 1.5f);
            if (i3 == 0) {
                this.barPaint.setTextAlign(Paint.Align.LEFT);
            } else if (i3 == i2) {
                this.barPaint.setTextAlign(Paint.Align.RIGHT);
            } else {
                this.barPaint.setTextAlign(Paint.Align.CENTER);
            }
            canvas.drawText(this.xLables.get(i3), f6, this.lineHeight[2] + textHeight + this.paddingTop, this.barPaint);
        }
        this.barPaint.setStrokeWidth(this.barWid);
        this.barPaint.setColor(getResources().getColor(R.color.main_bg_color));
        float dip2px = (this.xWidth - ((float) ScreenUtil.dip2px(10.0f))) / ((float) (this.healthSportItems.size() - 1));
        float f7 = this.lineHeight[2] + this.paddingTop;
        this.Xvalue_step.clear();
        this.Xvalue_distance.clear();
        this.Xvalue_kll.clear();
        int i4 = 0;
        while (i4 < this.healthSportItems.size()) {
            float f8 = (((float) i4) * dip2px) + this.barWid + (this.xOffSet * f2);
            int i5 = C27161.$SwitchMap$com$wade.fit$model$bean$DetailType[this.detailType.ordinal()];
            if (i5 == 1) {
                i = this.healthSportItems.get(i4).getStepCount();
                String str = i + "步";
                String remark = this.healthSportItems.get(i4).getRemark();
                HashMap hashMap = new HashMap();
                hashMap.put("x1", Float.valueOf(f8));
                hashMap.put("x2", Float.valueOf(this.barWid + f8));
                if (i > 0) {
                    this.Xvalue_step.add(hashMap);
                    this.stepValueList.add(str);
                    this.dateStepList.add(remark);
                }
            } else if (i5 == 2) {
                i = this.healthSportItems.get(i4).getCalory();
                String str2 = i + "千卡";
                String remark2 = this.healthSportItems.get(i4).getRemark();
                HashMap hashMap2 = new HashMap();
                hashMap2.put("x1", Float.valueOf(f8));
                hashMap2.put("x2", Float.valueOf(this.barWid + f8));
                if (i > 0) {
                    this.Xvalue_kll.add(hashMap2);
                    this.kllValueList.add(str2);
                    this.dateKllList.add(remark2);
                }
            } else if (i5 != 3) {
                i = 0;
            } else {
                int distance = this.healthSportItems.get(i4).getDistance();
                String str3 = NumUtil.float2String(((float) distance) / 1000.0f, 2) + "公里";
                String remark3 = this.healthSportItems.get(i4).getRemark();
                HashMap hashMap3 = new HashMap();
                hashMap3.put("x1", Float.valueOf(f8));
                hashMap3.put("x2", Float.valueOf(this.barWid + f8));
                if (distance > 0) {
                    this.Xvalue_distance.add(hashMap3);
                    this.distanceValueList.add(str3);
                    this.dateDistanceList.add(remark3);
                }
                i = distance;
            }
            float f9 = f7 - (this.yScale * ((float) i));
            this.barPaint.setColor(this.barColor);
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(this.barWid);
            this.linearGradient = new LinearGradient(f8, f7, f8, f9, new int[]{-1, this.barColor}, (float[]) null, Shader.TileMode.REPEAT);
            paint.setShader(this.linearGradient);
            canvas.drawLine(f8, f7, f8, f9, paint);
            i4++;
            f2 = 1.5f;
        }
    }

    /* renamed from: com.wade.fit.views.SportRecordChart$1 */
    static /* synthetic */ class C27161 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailType = new int[DetailType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.wade.fit.model.bean.DetailType[] r0 = com.wade.fit.model.bean.DetailType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.views.SportRecordChart.C27161.$SwitchMap$com$wade.fit$model$bean$DetailType = r0
                int[] r0 = com.wade.fit.views.SportRecordChart.C27161.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailType r1 = com.wade.fit.model.bean.DetailType.STEP     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.views.SportRecordChart.C27161.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailType r1 = com.wade.fit.model.bean.DetailType.CAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.views.SportRecordChart.C27161.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.DetailType r1 = com.wade.fit.model.bean.DetailType.DISTANCE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.SportRecordChart.C27161.<clinit>():void");
        }
    }

    private void drawY(Canvas canvas) {
        this.barPaint.setStrokeWidth(1.0f);
        this.barPaint.setTextSize(this.textSize * 1.0f);
        this.barPaint.setTextAlign(Paint.Align.CENTER);
        this.barPaint.setColor(getResources().getColor(R.color.normal_font_color));
        float ascent = (this.barPaint.ascent() + this.barPaint.descent()) / 2.0f;
        if (this.detailType != DetailType.DISTANCE) {
            canvas.drawText(((int) this.MAX_VALUE) + "", this.xOffSet * 0.75f, (this.lineHeight[2] - (this.yScale * this.MAX_VALUE)) - ascent, this.barPaint);
            canvas.drawText(((int) this.MIDDLE_VALUE) + "", this.xOffSet * 0.75f, (this.lineHeight[2] - (this.yScale * this.MIDDLE_VALUE)) - ascent, this.barPaint);
            canvas.drawText(((int) this.MIN_VALUE) + "", this.xOffSet * 0.75f, (this.lineHeight[2] - (this.yScale * this.MIN_VALUE)) - ascent, this.barPaint);
            canvas.drawText("0", this.xOffSet * 0.75f, this.lineHeight[2] - ascent, this.barPaint);
            return;
        }
        canvas.drawText(NumUtil.float2String(this.MAX_VALUE / 1000.0f, 2) + "", this.xOffSet * 0.75f, (this.lineHeight[2] - (this.yScale * this.MAX_VALUE)) - ascent, this.barPaint);
        canvas.drawText(NumUtil.float2String(this.MIDDLE_VALUE / 1000.0f, 2) + "", this.xOffSet * 0.75f, (this.lineHeight[2] - (this.yScale * this.MIDDLE_VALUE)) - ascent, this.barPaint);
        canvas.drawText(NumUtil.float2String(this.MIN_VALUE / 1000.0f, 2) + "", this.xOffSet * 0.75f, (this.lineHeight[2] - (this.yScale * this.MIN_VALUE)) - ascent, this.barPaint);
        canvas.drawText("0", this.xOffSet * 0.75f, this.lineHeight[2] - ascent, this.barPaint);
    }

    public void setData(List<HealthSportItem> list, List<String> list2, DetailType detailType2, BarChartProperties barChartProperties) {
        this.barColor = -1 != barChartProperties.barColor ? barChartProperties.barColor : this.barColor;
        this.yLineColor = -1 != barChartProperties.yLineColor ? barChartProperties.yLineColor : this.yLineColor;
        initPaint();
        initDatas(list, list2, detailType2);
    }

    public void initDatas(List<HealthSportItem> list, List<String> list2, DetailType detailType2) {
        this.detailType = detailType2;
        this.isToday = this.isToday;
        this.healthSportItems = list;
        this.MAX_VALUE = 0.0f;
        boolean z = true;
        if (list == null || list.size() <= 0) {
            this.MAX_VALUE = 0.0f;
        } else {
            for (int i = 0; i < list.size(); i++) {
                HealthSportItem healthSportItem = list.get(i);
                int i2 = C27161.$SwitchMap$com$wade.fit$model$bean$DetailType[detailType2.ordinal()];
                if (i2 != 1) {
                    if (i2 != 2) {
                        if (i2 == 3 && ((float) healthSportItem.getDistance()) > this.MAX_VALUE) {
                            if (BleSdkWrapper.isDistUnitKm()) {
                                this.MAX_VALUE = (float) NumUtil.formatPoint((double) healthSportItem.getDistance(), 2);
                            } else {
                                this.MAX_VALUE = (float) NumUtil.formatPoint((double) UnitUtil.getKm2mile((float) healthSportItem.getDistance()), 2);
                            }
                        }
                    } else if (((float) healthSportItem.getCalory()) > this.MAX_VALUE) {
                        this.MAX_VALUE = (float) healthSportItem.getCalory();
                    }
                } else if (((float) healthSportItem.getStepCount()) > this.MAX_VALUE) {
                    this.MAX_VALUE = (float) healthSportItem.getStepCount();
                }
            }
        }
        float f = this.MAX_VALUE;
        if (f != 0.0f) {
            this.isDrawTAB = true;
            this.MIDDLE_VALUE = (2.0f * f) / 3.0f;
            this.MIN_VALUE = f / 3.0f;
        } else {
            this.isDrawTAB = true;
            this.MAX_VALUE = 3.0f;
            this.MIDDLE_VALUE = 2.0f;
            this.MIN_VALUE = 1.0f;
        }
        this.barNum = list.size();
        this.xLables = list2;
        if (this.barNum % 2 != 0) {
            z = false;
        }
        this.isDouble = z;
        this.middleIndex = (this.barNum / 2) - (this.isDouble ? 1 : 0);
        initYscale();
        this.initDraw = false;
        invalidate();
    }

    private void initYscale() {
        float f = this.MAX_VALUE;
        if (f != 0.0f) {
            float[] fArr = this.lineHeight;
            this.yScale = (fArr[2] - fArr[0]) / f;
        }
    }
}
