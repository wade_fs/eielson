package com.wade.fit.base.refresh;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.internal.view.SupportMenu;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.wade.fit.R;
import com.wade.fit.base.BaseFragment;
import com.wade.fit.base.IBaseView;
import com.wade.fit.base.refresh.BaseListPersenter;
import com.wade.fit.base.refresh.BaseRecyclerAdapter;
import com.wade.fit.util.CnWinUtil;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.views.RecyclerRefreshLayout;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseRefreshFragment<Presenter extends BaseListPersenter, Model> extends BaseFragment implements BaseListView<Model>, IBaseView, BaseRecyclerAdapter.OnItemClickListener, RecyclerRefreshLayout.SuperRefreshLayoutListener {
    FrameLayout flBottom;
    FrameLayout flTop;
    protected BaseRecyclerAdapter<Model> mAdapter;
    protected Presenter mPresenter;
    protected RecyclerView mRecyclerView;
    protected RecyclerRefreshLayout mRefreshLayout;

    /* access modifiers changed from: protected */
    public abstract BaseRecyclerAdapter<Model> getAdapter();

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.fragment_base_recycler;
    }

    public void goBack() {
    }

    /* access modifiers changed from: protected */
    public void onItemClick(Model model, int i) {
    }

    public void onCreate(Bundle bundle) {
        this.mPresenter = (BaseListPersenter) CnWinUtil.getT(this, 0);
        Presenter presenter = this.mPresenter;
        if (presenter != null) {
            presenter.attachView(this);
        }
        super.onCreate(bundle);
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.mRefreshLayout.setSuperRefreshLayoutListener(this);
        this.mAdapter = getAdapter();
        this.mRecyclerView.setLayoutManager(getLayoutManager());
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mAdapter.setOnItemClickListener(this);
        this.mRefreshLayout.setColorSchemeColors(SupportMenu.CATEGORY_MASK, -16711936, -16711681);
    }

    /* access modifiers changed from: protected */
    public void addView(View view) {
        this.flTop.addView(view);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.mRefreshLayout.post(new Runnable() {
            /* class com.wade.fit.base.refresh.$$Lambda$BaseRefreshFragment$9FaaiZCiVKMbsmn9L3O2Y56Sa_8 */

            public final void run() {
                BaseRefreshFragment.this.lambda$initData$0$BaseRefreshFragment();
            }
        });
    }

    public /* synthetic */ void lambda$initData$0$BaseRefreshFragment() {
        this.mRefreshLayout.setRefreshing(true);
        Presenter presenter = this.mPresenter;
        if (presenter != null) {
            presenter.onRefreshing();
        }
    }

    public void onItemClick(int i, long j) {
        Model item = this.mAdapter.getItem(i);
        if (item != null) {
            onItemClick(item, i);
        }
    }

    public void onRefreshing() {
        if (this.mPresenter != null) {
            this.mAdapter.setState(5, true);
            this.mPresenter.onRefreshing();
        }
    }

    public void onLoadMore() {
        this.mPresenter.onLoadMore();
        this.mRecyclerView.post(new Runnable() {
            /* class com.wade.fit.base.refresh.$$Lambda$BaseRefreshFragment$TcwIjj0D_YGa66kH1YXVWBAywlo */

            public final void run() {
                BaseRefreshFragment.this.lambda$onLoadMore$1$BaseRefreshFragment();
            }
        });
    }

    public /* synthetic */ void lambda$onLoadMore$1$BaseRefreshFragment() {
        this.mAdapter.setState(8, true);
    }

    public void onRefreshSuccess(List<Model> list) {
        this.mAdapter.resetItem(list);
    }

    public void onLoadMoreSuccess(List<Model> list) {
        this.mAdapter.addAll(list);
    }

    public void showNoMore() {
        this.mAdapter.setState(1, true);
    }

    public void showNetError(String str) {
        this.mAdapter.setState(3, true);
    }

    public void onComplete() {
        this.mRefreshLayout.onComplete();
    }

    public void showMsg(String str) {
        Toast.makeText(this.activity, str, 0).show();
    }

    public void onDestroy() {
        super.onDestroy();
        Presenter presenter = this.mPresenter;
        if (presenter != null) {
            presenter.detachView();
        }
    }

    /* access modifiers changed from: protected */
    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this.mContext);
    }

    public void showLoading() {
        DialogHelperNew.buildWaitDialog(getActivity(), true);
    }

    public void showLoadingFalse() {
        DialogHelperNew.buildWaitDialog(getActivity(), false);
    }

    public void hideLoading() {
        DialogHelperNew.dismissWait();
    }

    public void showNoData(int i) {
        this.mAdapter.resetItem(new ArrayList());
        this.mAdapter.setState(1, true, i);
    }

    public void setCanLoadmore(boolean z) {
        this.mRefreshLayout.setCanLoadMore(z);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }
}
