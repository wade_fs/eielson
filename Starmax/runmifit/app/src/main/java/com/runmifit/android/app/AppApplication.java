package com.wade.fit.app;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.NotificationCompat;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.util.CrashUtils;
import com.mob.MobSDK;
import com.wade.fit.greendao.gen.DaoMaster;
import com.wade.fit.greendao.gen.DaoSession;
import com.wade.fit.greendao.helper.DbOpenHelper;
import com.wade.fit.model.bean.DeviceState;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.ui.main.activity.LoginActivity;
import com.wade.fit.ui.main.activity.WelcomeActivity;
import com.wade.fit.receiver.BleReceiver;
import com.wade.fit.receiver.NetworkConnectChangedReceiver;
import com.wade.fit.service.IntelligentNotificationService;
import com.wade.fit.util.AppSharedPreferencesUtils;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.PermissionUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.TimeUtil;
import com.wade.fit.util.ToastUtil;
import com.wade.fit.util.ble.BleCallback;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.file.FileUtil;
import com.wade.fit.util.log.LogUtil;
import com.tencent.bugly.Bugly;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class AppApplication extends MultiDexApplication {
    public static List<Activity> activities = new ArrayList();
    private static AppApplication app = null;
    public static boolean isCamer = false;
    public static boolean isHaveEcg = false;
    public static boolean isTestLocation = true;

    /* renamed from: db */
    private SQLiteDatabase f4543db;
    private boolean isSysndata = false;
    private BroadcastReceiver languageReceiver = new BroadcastReceiver() {
        /* class com.wade.fit.app.AppApplication.C15262 */

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void onReceive(Context context, Intent intent) {
            Locale locale;
            char c;
            if (BleSdkWrapper.isBind() && BleSdkWrapper.isConnected() && "android.intent.action.LOCALE_CHANGED".equals(intent.getAction())) {
                DeviceState deviceState = SPHelper.getDeviceState();
                if (Build.VERSION.SDK_INT >= 24) {
                    locale = AppApplication.this.getResources().getConfiguration().getLocales().get(0);
                } else {
                    locale = AppApplication.this.getResources().getConfiguration().locale;
                }
                String language = locale.getLanguage();
                switch (language.hashCode()) {
                    case 3139:
                        if (language.equals("be")) {
                            c = 2;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3166:
                        if (language.equals("ca")) {
                            c = 5;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3201:
                        if (language.equals("de")) {
                            c = 8;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3246:
                        if (language.equals("es")) {
                            c = 6;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3276:
                        if (language.equals("fr")) {
                            c = 4;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3371:
                        if (language.equals("it")) {
                            c = 11;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3383:
                        if (language.equals("ja")) {
                            c = 9;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3580:
                        if (language.equals(Config.PROCESS_LABEL)) {
                            c = 10;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3588:
                        if (language.equals(Config.PLATFORM_TYPE)) {
                            c = 7;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3645:
                        if (language.equals("ro")) {
                            c = 12;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3651:
                        if (language.equals("ru")) {
                            c = 1;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3734:
                        if (language.equals("uk")) {
                            c = 3;
                            break;
                        }
                        c = 65535;
                        break;
                    case 3886:
                        if (language.equals("zh")) {
                            c = 0;
                            break;
                        }
                        c = 65535;
                        break;
                    default:
                        c = 65535;
                        break;
                }
                switch (c) {
                    case 0:
                        deviceState.language = 1;
                        break;
                    case 1:
                    case 2:
                        deviceState.language = 2;
                        break;
                    case 3:
                        deviceState.language = 3;
                        break;
                    case 4:
                        deviceState.language = 4;
                        break;
                    case 5:
                    case 6:
                        deviceState.language = 5;
                        break;
                    case 7:
                        deviceState.language = 6;
                        break;
                    case 8:
                        deviceState.language = 7;
                        break;
                    case 9:
                        deviceState.language = 8;
                        break;
                    case 10:
                        deviceState.language = 9;
                        break;
                    case 11:
                        deviceState.language = 10;
                        break;
                    case 12:
                        deviceState.language = 11;
                        break;
                    default:
                        deviceState.language = 0;
                        break;
                }
                SPHelper.saveDeviceState(deviceState);
                BleSdkWrapper.setDeviceState(deviceState, null);
                AppApplication.finishAll();
                Process.killProcess(Process.myPid());
            }
        }
    };
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;
    private DbOpenHelper mHelper;
    private BroadcastReceiver systemReceiver = new BroadcastReceiver() {
        /* class com.wade.fit.app.AppApplication.C15241 */

        public void onReceive(Context context, Intent intent) {
            if (BleSdkWrapper.isBind() && BleSdkWrapper.isConnected()) {
                String action = intent.getAction();
                if ("android.intent.action.TIMEZONE_CHANGED".equals(action) || "android.intent.action.TIME_SET".equals(action)) {
                    DeviceState deviceState = SPHelper.getDeviceState();
                    deviceState.timeFormat = TimeUtil.is24HourFormat(context) ^ true ? 1 : 0;
                    SPHelper.saveDeviceState(deviceState);
                    LogUtil.d("24:" + TimeUtil.is24HourFormat(context));
                    BleSdkWrapper.setDeviceState(new BleCallback() {
                        /* class com.wade.fit.app.AppApplication.C15241.C15251 */

                        public void setSuccess() {
                        }

                        public void complete(int i, Object obj) {
                            BleSdkWrapper.setTime(null);
                        }
                    });
                    EventBusHelper.post(1006);
                } else if ("android.intent.action.DATE_CHANGED".equals(action)) {
                    EventBusHelper.post(1006);
                }
            }
        }
    };
    private UserBean userBean;

    private void initFacebook() {
    }

    static {
        System.loadLibrary("hello");
    }

    public void onCreate() {
        super.onCreate();
        app = this;
        intLib();
        initCrashHandler();
        BleSdkWrapper.init(this);
        initBaidu();
        AppSharedPreferencesUtils.getInstance().init(this);
        registerBleReceiver();
        initShareSDK();
        registerSystemReceiver();
        registerLanguageReceiver();
        registerNetorkReceiver();
        initBugly();
        deleteExistFile();
    }

    public void setDatabase() {
        this.mHelper = new DbOpenHelper(this, "zhihuiju-" + this.userBean.getUserId() + "-db");
        this.f4543db = this.mHelper.getWritableDatabase();
        this.mDaoMaster = new DaoMaster(this.f4543db);
        this.mDaoSession = this.mDaoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return this.mDaoSession;
    }

    private void startNotifyService() {
        if (Build.VERSION.SDK_INT >= 26) {
            startForegroundService(new Intent(this, IntelligentNotificationService.class));
        } else {
            startService(new Intent(this, IntelligentNotificationService.class));
        }
    }

    private void initBugly() {
        Bugly.init(getApplicationContext(), "d3da7031aa", false);
    }

    private void initBaidu() {
        SDKInitializer.initialize(this);
    }

    private void intLib() {
        LibContext.getInstance().init(this);
    }

    private void registerBleReceiver() {
        BleReceiver bleReceiver = new BleReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        registerReceiver(bleReceiver, intentFilter);
    }

    private void initCrashHandler() {
        LibContext.getInstance().initCrashHandler(Constants.CRASH_PATH);
    }

    public void deleteExistFile() {
        if (((Boolean) SharePreferenceUtils.get(this, Constants.IS_FIRST_RUN, true)).booleanValue() && PermissionUtil.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE")) {
            FileUtil.deleteDir(Constants.APP_ROOT_PATH);
            SharePreferenceUtils.put(this, Constants.IS_FIRST_RUN, false);
        }
    }

    private void registerSystemReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.TIME_SET");
        intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        intentFilter.addAction("android.intent.action.DATE_CHANGED");
        registerReceiver(this.systemReceiver, intentFilter);
    }

    private void registerNetorkReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(new NetworkConnectChangedReceiver(), intentFilter);
    }

    private void registerLanguageReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.LOCALE_CHANGED");
        registerReceiver(this.languageReceiver, intentFilter);
    }

    public static void toLogin(String str) {
        if (str != null) {
            ToastUtil.showToast(str);
        }
        SharePreferenceUtils.putString(Constants.USER_PASSWORD, "");
        SharePreferenceUtils.putString(Constants.USER_TOKEN, "");
        SharePreferenceUtils.putString(Constants.IS_TO_LOGIN, "1");
        SPHelper.saveUserBean(null);
        Bundle bundle = new Bundle();
        bundle.putString("from", "WelcomeActivity");
        Intent intent = new Intent();
        intent.setClass(getInstance(), LoginActivity.class);
        intent.setFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
        intent.putExtras(bundle);
        getInstance().startActivity(intent);
        for (Activity activity : activities) {
            if (!(activity instanceof LoginActivity) && !activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    public void restartApp() {
        ((AlarmManager) getSystemService(NotificationCompat.CATEGORY_ALARM)).set(1, System.currentTimeMillis(), PendingIntent.getActivity(getApplicationContext(), 0, new Intent(this, WelcomeActivity.class), CrashUtils.ErrorDialogData.BINDER_CRASH));
        finishAll();
        Process.killProcess(Process.myPid());
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public static Context getContext() {
        return app.getApplicationContext();
    }

    public static AppApplication getInstance() {
        return app;
    }

    public static void addActivity(Activity activity) {
        activities.add(activity);
    }

    public static List<Activity> getActivities() {
        return activities;
    }

    public static void setActivities(List<Activity> list) {
        activities = list;
    }

    public static void removeActivity(Activity activity) {
        if (activities.contains(activity)) {
            activity.finish();
            activities.remove(activity);
        }
    }

    public static void finishActivity(Class<?> cls) {
        Iterator<Activity> it = activities.iterator();
        while (it.hasNext()) {
            Activity next = it.next();
            if (next.getClass() == cls) {
                it.remove();
                next.finish();
            }
        }
    }

    public boolean isSysndata() {
        return this.isSysndata;
    }

    public void setSysndata(boolean z) {
        this.isSysndata = z;
    }

    public static void finishAll() {
        for (Activity activity : activities) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    private void initShareSDK() {
        MobSDK.init(this);
    }

    public UserBean getUserBean() {
        return this.userBean;
    }

    public void setUserBean(UserBean userBean2) {
        this.userBean = userBean2;
    }
}
