package com.wade.fit.views.photo;

import android.content.Context;
import com.wade.fit.app.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Images {
    public static List<String> mCameraImgs;
    public static List<String> mGalleryImgs;
    public static List<String> mImgs;

    public static List<String> getImages() {
        if (mCameraImgs != null) {
            mCameraImgs = null;
        }
        File file = new File(Constants.imageDir);
        if (!file.exists()) {
            file.mkdir();
        }
        mCameraImgs = GalleryUtil.getAllImagePathsByFolder(Constants.imageDir);
        List<String> list = mCameraImgs;
        if (list != null && list.size() > 0) {
            Collections.sort(mCameraImgs, Collections.reverseOrder());
        }
        return mCameraImgs;
    }

    public static List<String> getCameraImages(Context context) {
        ArrayList<String> allImagePathsByFolder;
        List<String> list = mCameraImgs;
        if (list != null) {
            list.clear();
            mCameraImgs = null;
        }
        mCameraImgs = new ArrayList();
        for (GalleryEntity galleryEntity : GalleryUtil.queryGallery(context)) {
            if (galleryEntity.getPath().contains("Camera") && (allImagePathsByFolder = GalleryUtil.getAllImagePathsByFolder(GalleryUtil.getFolderByFileName(galleryEntity.getPath()))) != null && allImagePathsByFolder.size() > 0) {
                mCameraImgs.addAll(allImagePathsByFolder);
            }
        }
        List<String> list2 = mCameraImgs;
        if (list2 != null && list2.size() > 0) {
            Collections.sort(mCameraImgs, Collections.reverseOrder());
        }
        return mCameraImgs;
    }

    public static List<String> getGalleryImages(Context context) {
        List<String> list = mGalleryImgs;
        if (list != null) {
            list.clear();
            mGalleryImgs = null;
        }
        mGalleryImgs = new ArrayList();
        for (GalleryEntity galleryEntity : GalleryUtil.queryGallery(context)) {
            ArrayList<String> allImagePathsByFolder = GalleryUtil.getAllImagePathsByFolder(GalleryUtil.getFolderByFileName(galleryEntity.getPath()));
            if (allImagePathsByFolder != null && allImagePathsByFolder.size() > 0) {
                mGalleryImgs.addAll(allImagePathsByFolder);
            }
        }
        List<String> list2 = mGalleryImgs;
        if (list2 != null && list2.size() > 0) {
            Collections.sort(mGalleryImgs, Collections.reverseOrder());
        }
        return mGalleryImgs;
    }
}
