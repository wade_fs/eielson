package com.wade.fit.persenter.sport;

import android.content.Context;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.base.IBaseLocation;
import com.wade.fit.model.bean.LatLngBean;
import com.wade.fit.model.bean.LocationMessage;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.MapHelper;
import com.wade.fit.util.PermissionUtil;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class BaseLocationPresenter extends BasePersenter implements IBaseLocation, Constants {
    private static final int LOCATION_MAX_TIME = 3;
    public static boolean isTest = AppApplication.isTestLocation;
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected int UPDATE_INTERVAL = 0;
    protected LatLngBean currentLatlng = new LatLngBean();
    protected LatLngBean lastLatlng;
    double lat = 22.68586d;
    double lng = 113.980724d;
    private int locationTime;
    protected boolean onceLocation = false;
    protected int totalDistance = 0;

    private void setPeriod() {
        this.UPDATE_INTERVAL = 5000;
    }

    public void startLocation(boolean z) {
        LogUtil.d("startLocation  isOnce:" + z + "," + toString());
        this.onceLocation = z;
    }

    /* access modifiers changed from: protected */
    public boolean checkLocationPremission() {
        return PermissionUtil.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
    }

    public void stopLocation() {
        LogUtil.d("stopLocation ," + toString());
    }

    public void init(Context context) {
        setPeriod();
        this.totalDistance = 0;
        this.lat = 22.68586d;
        this.lng = 113.980724d;
    }

    public void onDestory() {
        LogUtil.d("onDestory ," + toString());
    }

    public final void onLocationChanged(LocationMessage locationMessage) {
        if (locationMessage != null) {
            handlerOnceLocation(locationMessage);
            LatLngBean latLngBean = (LatLngBean) locationMessage.getData();
            LatLngBean latLngBean2 = this.lastLatlng;
            if (latLngBean2 == null) {
                this.lastLatlng = latLngBean;
            } else {
                double distance = MapHelper.getDistance(latLngBean, latLngBean2);
                if (distance < 1.0d || distance > 1000.0d) {
                    LogUtil.d("轨迹点过滤----distance:" + distance);
                    locationMessage.isValid = false;
                    return;
                }
                if (locationMessage.accurac > 300.0f) {
                    LogUtil.d("轨迹点过滤----Accuracy:" + locationMessage.accurac);
                    locationMessage.isValid = false;
                }
                this.lastLatlng = latLngBean;
                double d = (double) this.totalDistance;
                Double.isNaN(d);
                this.totalDistance = (int) (d + distance);
            }
            latLngBean.setCurrentTimeMillis(simpleDateFormat.format(new Date()));
            locationMessage.totalDistance = this.totalDistance;
            EventBusHelper.post(locationMessage);
        } else if (this.onceLocation) {
            this.locationTime++;
        }
    }

    private void handlerOnceLocation(LocationMessage locationMessage) {
        if (this.onceLocation) {
            saveLocation(locationMessage);
            stopLocation();
            onDestory();
        }
    }

    private void saveLocation(LocationMessage locationMessage) {
        String str = ((LatLngBean) locationMessage.getData()).getLongitude() + "," + ((LatLngBean) locationMessage.getData()).getLatitude();
        LogUtil.d("pointKey:" + str);
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.POINT_KEY, str);
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.LOCATION_DETAIL, locationMessage.toString());
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.COUNTRY_KEY, locationMessage.country);
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.CITY_KEY, locationMessage.city);
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.CURRENT_CITY_KEY, locationMessage.city);
        ((LatLngBean) locationMessage.getData()).getLongitude();
        ((LatLngBean) locationMessage.getData()).getLatitude();
        DebugLog.m6203d("国家编号:" + "");
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.COUNTRY_CODE_KEY, "");
    }
}
