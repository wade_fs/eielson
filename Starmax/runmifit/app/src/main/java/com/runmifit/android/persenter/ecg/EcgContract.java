package com.wade.fit.persenter.ecg;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.ECGRecordShowList;
import java.util.List;

public interface EcgContract {

    public interface Presenter {
        void getRecordInfos();
    }

    public interface View extends IBaseView {
        void getSuccess(List<ECGRecordShowList> list);
    }
}
