package com.wade.fit.ui.device.activity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.image.BitmapUtil;
import com.wade.fit.views.photo.Images;
import com.wade.fit.views.zoomview.ZoomImageView;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.activity.ImageDetailsOtherActivity */
public class ImageDetailsOtherActivity extends BaseActivity implements ViewPager.OnPageChangeListener {
    public TextView pageText;
    public TextView tv_cancel;
    public ViewPager viewPager;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_image_other_details;
    }

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.choise_photo));
        int intExtra = getIntent().getIntExtra("image_position", 0);
        this.viewPager.setFitsSystemWindows(true);
        Images.getImages();
        this.viewPager.setAdapter(new SamplePagerAdapter());
        this.viewPager.setCurrentItem(intExtra);
        this.viewPager.setOnPageChangeListener(this);
        this.viewPager.setEnabled(false);
        TextView textView = this.pageText;
        textView.setText((intExtra + 1) + "/" + Images.mCameraImgs.size());
    }

    public void onViewClicked() {
        finish();
    }

    /* renamed from: com.wade.fit.ui.device.activity.ImageDetailsOtherActivity$SamplePagerAdapter */
    private class SamplePagerAdapter extends PagerAdapter {
        private Handler backgroundHandler;

        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        public SamplePagerAdapter() {
            HandlerThread handlerThread = new HandlerThread("backgroundThread");
            handlerThread.start();
            this.backgroundHandler = new Handler(handlerThread.getLooper());
        }

        public int getCount() {
            if (Images.mCameraImgs != null) {
                return Images.mCameraImgs.size();
            }
            return 0;
        }

        public View instantiateItem(ViewGroup viewGroup, int i) {
            final ZoomImageView zoomImageView = new ZoomImageView(viewGroup.getContext());
            final String str = Images.mCameraImgs.get(i);
            this.backgroundHandler.post(new Runnable() {
                /* class com.wade.fit.ui.device.activity.ImageDetailsOtherActivity.SamplePagerAdapter.C24681 */

                public void run() {
                    final Bitmap readBitmapFromFile = BitmapUtil.readBitmapFromFile(str, ScreenUtil.getScreenWidth(ImageDetailsOtherActivity.this), ScreenUtil.getScreenHeight(ImageDetailsOtherActivity.this));
                    zoomImageView.post(new Runnable() {
                        /* class com.wade.fit.ui.device.activity.ImageDetailsOtherActivity.SamplePagerAdapter.C24681.C24691 */

                        public void run() {
                            zoomImageView.setImageBitmap(readBitmapFromFile);
                        }
                    });
                }
            });
            viewGroup.addView(zoomImageView, -2, -2);
            return zoomImageView;
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            viewGroup.removeView((View) obj);
            try {
                ImageView imageView = (ImageView) obj;
                Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                imageView.setImageBitmap(null);
                bitmap.recycle();
            } catch (Exception unused) {
            }
        }
    }

    public void onPageSelected(int i) {
        TextView textView = this.pageText;
        textView.setText((i + 1) + "/" + Images.mCameraImgs.size());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        List<String> images = Images.getImages();
        if (images == null || images.size() <= 0) {
            showToast(getResources().getString(R.string.no_image));
            finish();
        }
    }
}
