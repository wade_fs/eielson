package com.wade.fit.views.camera;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import com.wade.fit.app.Constants;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtil {
    public static String saveBitmap(Context context, Bitmap bitmap) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            File file = new File(Constants.imageDir);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        String str = Constants.imageDir + File.separator + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date(System.currentTimeMillis())) + ".jpg";
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bufferedOutputStream);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            context.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + str)));
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String saveBitmap2Gallery(Context context, Bitmap bitmap) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            File file = new File(Constants.imageDir);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        String format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date(System.currentTimeMillis()));
        String str = Constants.imageDir + File.separator + format + ".jpg";
        String str2 = null;
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, bufferedOutputStream);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            try {
                String insertImage = MediaStore.Images.Media.insertImage(context.getContentResolver(), str, format + ".jpg", (String) null);
                if (insertImage != null) {
                    str2 = getLatestImagePath(context, Uri.parse(insertImage));
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            context.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + str)));
            return str2;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String getLatestImagePath(Context context, Uri uri) {
        Cursor query = context.getContentResolver().query(uri, new String[]{"_data"}, null, null, null);
        int columnIndexOrThrow = query.getColumnIndexOrThrow("_data");
        query.moveToFirst();
        String string = query.getString(columnIndexOrThrow);
        if (query != null && !query.isClosed()) {
            query.close();
        }
        return string;
    }
}
