package com.wade.fit.views;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.bean.HealthSleepItem;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.TimeUtil;
import com.wade.fit.util.ViewUtil;
import java.util.List;

public class SleepBarChart extends View {
    private float bottomHeight;
    private Paint bottomPaint;
    private float centerHeight;
    private Paint centerPaint;
    private float centerWithd;
    private int[] colors = {-1116674, -9969156, -16286494};
    private float endHeight;

    /* renamed from: h */
    private int f5242h;
    private int screenWidth;
    private HealthSleep sleepData;
    private List<HealthSleepItem> sleepItems;
    private Paint sleepPaint;
    private int[] sleepTimes;
    private float startHeight;
    private int textColor = -1;
    private float timeTextSize;
    private float titleSpan;
    private float tittleTextSize;
    private Bitmap topBitmap;
    private Drawable topDrawable;
    private float topHeight;
    private Paint topPaint;
    private float topWidth;

    /* renamed from: w */
    private int f5243w;
    private float xBarHeight;
    private float xLabelTextSize;
    private float xLable = 0.0f;
    private float xOffSet;
    private int xOffet = 20;
    private String[] xtimes;
    private float yLable = 0.0f;
    private float yOffSet;

    public SleepBarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SleepBarChart);
        Resources resources = getResources();
        this.textColor = obtainStyledAttributes.getColor(0, resources.getColor(R.color.theme_text_color_lable));
        this.tittleTextSize = obtainStyledAttributes.getDimension(5, 20.0f);
        this.xLabelTextSize = obtainStyledAttributes.getDimension(5, 20.0f);
        this.timeTextSize = obtainStyledAttributes.getDimension(4, 20.0f);
        obtainStyledAttributes.recycle();
        this.sleepTimes = new int[2];
        initPaint();
        this.topDrawable = resources.getDrawable(R.mipmap.home_sleep_3_5s);
        this.topBitmap = BitmapFactory.decodeResource(resources, R.mipmap.home_sleep_3_5s);
        this.screenWidth = ScreenUtil.getScreenWidth(context);
    }

    private void initPaint() {
        this.topPaint = new Paint(1);
        this.centerPaint = new Paint(1);
        this.sleepPaint = new Paint(1);
        this.bottomPaint = new Paint(1);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5243w = i;
        this.f5242h = i2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        List<HealthSleepItem> list;
        super.onDraw(canvas);
        this.titleSpan = (float) (this.f5242h / 15);
        drawTitle(canvas);
        if (!(this.sleepData == null || (list = this.sleepItems) == null || list.size() <= 0)) {
            drawTime(canvas);
        }
        drawCenterData(canvas);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.TimeUtil.timeFormatter(int, boolean, java.lang.String[], boolean):java.lang.String
     arg types: [int, boolean, java.lang.String[], int]
     candidates:
      com.wade.fit.util.TimeUtil.timeFormatter(int, int, boolean, java.lang.String[]):java.lang.String
      com.wade.fit.util.TimeUtil.timeFormatter(int, boolean, java.lang.String[], boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.TimeUtil.timeFormatter(int, boolean, java.lang.String[], boolean, boolean):java.lang.String
     arg types: [int, boolean, java.lang.String[], int, int]
     candidates:
      com.wade.fit.util.TimeUtil.timeFormatter(int, int, boolean, java.lang.String[], boolean):java.lang.String
      com.wade.fit.util.TimeUtil.timeFormatter(int, boolean, java.lang.String[], boolean, boolean):java.lang.String */
    private void drawCenterData(Canvas canvas) {
        List<HealthSleepItem> list;
        String[] strArr;
        int i = this.f5242h;
        float f = this.topHeight;
        float f2 = this.bottomHeight;
        this.centerHeight = (((float) i) - f) - f2;
        this.yOffSet = this.centerHeight / 20.0f;
        int i2 = this.f5243w;
        this.xOffSet = (float) (i2 / 30);
        this.centerWithd = ((float) i2) - (this.xOffSet * 2.0f);
        this.startHeight = f + this.yOffSet;
        this.endHeight = (((float) i) - f2) - ((float) this.xOffet);
        this.centerPaint.setColor(-1);
        this.centerPaint.setStrokeWidth(1.0f);
        this.xBarHeight = (float) ScreenUtil.dp2px(10.0f, (Activity) getContext());
        String[] stringArray = getResources().getStringArray(R.array.amOrpm);
        if (this.sleepData == null || (list = this.sleepItems) == null || list.size() <= 0) {
            this.centerPaint.setColor(this.textColor);
            this.centerPaint.setTextSize(this.xLabelTextSize * 1.5f);
            this.centerPaint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText(getResources().getString(R.string.state_no_data), (float) (this.f5243w / 2), (float) (this.f5242h / 2), this.centerPaint);
        } else {
            this.xtimes = new String[6];
            int totalSleepMinutes = this.sleepData.getTotalSleepMinutes() / (this.xtimes.length - 1);
            int i3 = 0;
            int i4 = getStartAndEndTime(this.sleepData)[0];
            int i5 = getStartAndEndTime(this.sleepData)[1];
            String str = "";
            String str2 = str;
            int i6 = 0;
            while (true) {
                strArr = this.xtimes;
                if (i6 >= strArr.length) {
                    break;
                }
                if (i6 == 0) {
                    str = TimeUtil.timeFormatter(i4, TimeUtil.is24Hour((Activity) getContext()), stringArray, true);
                    this.xtimes[i6] = TimeUtil.timeFormatter(i4, TimeUtil.is24Hour((Activity) getContext()), stringArray, true, true);
                } else if (i6 == strArr.length - 1) {
                    str2 = TimeUtil.timeFormatter(i5, TimeUtil.is24Hour((Activity) getContext()), stringArray, true);
                    this.xtimes[i6] = TimeUtil.timeFormatter(i5, TimeUtil.is24Hour((Activity) getContext()), stringArray, true, false);
                } else {
                    strArr[i6] = TimeUtil.timeFormatter((totalSleepMinutes * i6) + i4, TimeUtil.is24Hour((Activity) getContext()), stringArray, false);
                }
                i6++;
            }
            float length = this.centerWithd / ((float) (strArr.length - 1));
            this.centerPaint.setTextSize(this.xLabelTextSize * 0.8f);
            String[] strArr2 = this.xtimes;
            if (strArr2 != null && strArr2.length > 0) {
                while (true) {
                    String[] strArr3 = this.xtimes;
                    if (i3 >= strArr3.length) {
                        break;
                    }
                    if (i3 == 0) {
                        this.centerPaint.setTextAlign(Paint.Align.LEFT);
                    } else if (i3 == strArr3.length - 1) {
                        this.centerPaint.setTextAlign(Paint.Align.RIGHT);
                    } else {
                        this.centerPaint.setTextAlign(Paint.Align.CENTER);
                    }
                    canvas.drawText(this.xtimes[i3], (((float) i3) * length) + this.xOffSet, this.endHeight, this.centerPaint);
                    i3++;
                }
            }
            this.centerPaint.setTextAlign(Paint.Align.LEFT);
            canvas.drawText(str, this.xOffSet * 2.0f, this.startHeight, this.centerPaint);
            this.centerPaint.setTextAlign(Paint.Align.RIGHT);
            canvas.drawText(str2, ((float) this.f5243w) - (this.xOffSet * 2.0f), this.startHeight, this.centerPaint);
            drawSleepBar(canvas);
        }
        float f3 = this.xOffSet;
        float f4 = this.endHeight;
        float f5 = this.xBarHeight;
        Canvas canvas2 = canvas;
        canvas2.drawLine(f3, f4 - f5, ((float) this.f5243w) - f3, f4 - f5, this.centerPaint);
        float f6 = this.xOffSet;
        canvas2.drawLine(f6 * 2.0f, this.endHeight - this.xBarHeight, f6 * 2.0f, this.startHeight, this.centerPaint);
        int i7 = this.f5243w;
        float f7 = this.xOffSet;
        canvas2.drawLine(((float) i7) - (f7 * 2.0f), this.endHeight - this.xBarHeight, ((float) i7) - (f7 * 2.0f), this.startHeight, this.centerPaint);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c7 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void drawSleepBar(android.graphics.Canvas r15) {
        /*
            r14 = this;
            float r0 = r14.xOffSet
            r1 = 1073741824(0x40000000, float:2.0)
            float r2 = r0 * r1
            int r3 = r14.f5243w
            float r3 = (float) r3
            float r0 = r0 * r1
            float r3 = r3 - r0
            float r0 = r14.startHeight
            float r4 = r14.yOffSet
            r5 = 1077936128(0x40400000, float:3.0)
            float r4 = r4 * r5
            float r0 = r0 + r4
            float r4 = r14.endHeight
            float r5 = r14.xBarHeight
            float r4 = r4 - r5
            float r3 = r3 - r2
            float r5 = r4 - r0
            com.wade.fit.greendao.bean.HealthSleep r6 = r14.sleepData
            int r6 = r6.getTotalSleepMinutes()
            float r6 = (float) r6
            float r3 = r3 / r6
            android.graphics.Paint r6 = r14.sleepPaint
            android.graphics.Paint$Align r7 = android.graphics.Paint.Align.RIGHT
            r6.setTextAlign(r7)
            r12 = 0
            r6 = r2
            r2 = 0
        L_0x002f:
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r7 = r14.sleepItems
            int r7 = r7.size()
            if (r2 >= r7) goto L_0x00cc
            r7 = 0
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r8 = r14.sleepItems
            java.lang.Object r8 = r8.get(r2)
            com.wade.fit.greendao.bean.HealthSleepItem r8 = (com.wade.fit.greendao.bean.HealthSleepItem) r8
            int r9 = r8.getSleepStatus()
            r10 = 4
            r11 = 1101004800(0x41a00000, float:20.0)
            if (r9 != r10) goto L_0x005a
            android.graphics.Paint r7 = r14.sleepPaint
            int[] r9 = r14.colors
            r9 = r9[r12]
            r7.setColor(r9)
            r7 = 1092616192(0x41200000, float:10.0)
        L_0x0054:
            float r7 = r7 * r5
            float r7 = r7 / r11
            float r7 = r7 + r0
            r10 = r7
            goto L_0x0082
        L_0x005a:
            int r9 = r8.getSleepStatus()
            r10 = 2
            if (r9 != r10) goto L_0x006e
            android.graphics.Paint r7 = r14.sleepPaint
            int[] r9 = r14.colors
            r10 = 1
            r9 = r9[r10]
            r7.setColor(r9)
            r7 = 1086324736(0x40c00000, float:6.0)
            goto L_0x0054
        L_0x006e:
            int r9 = r8.getSleepStatus()
            r13 = 3
            if (r9 != r13) goto L_0x0081
            android.graphics.Paint r7 = r14.sleepPaint
            int[] r9 = r14.colors
            r9 = r9[r10]
            r7.setColor(r9)
            r7 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0054
        L_0x0081:
            r10 = 0
        L_0x0082:
            android.graphics.Paint r7 = r14.sleepPaint
            int r9 = r8.getOffsetMinute()
            float r9 = (float) r9
            float r9 = r9 * r3
            r7.setStrokeWidth(r9)
            if (r2 != 0) goto L_0x0095
            int r7 = r8.getOffsetMinute()
            goto L_0x00ac
        L_0x0095:
            java.util.List<com.wade.fit.greendao.bean.HealthSleepItem> r7 = r14.sleepItems
            int r9 = r2 + -1
            java.lang.Object r7 = r7.get(r9)
            com.wade.fit.greendao.bean.HealthSleepItem r7 = (com.wade.fit.greendao.bean.HealthSleepItem) r7
            int r7 = r7.getOffsetMinute()
            float r7 = (float) r7
            float r7 = r7 * r3
            float r7 = r7 / r1
            float r6 = r6 + r7
            int r7 = r8.getOffsetMinute()
        L_0x00ac:
            float r7 = (float) r7
            float r7 = r7 * r3
            float r7 = r7 / r1
            float r6 = r6 + r7
            r13 = r6
            int r6 = r14.f5243w
            float r6 = (float) r6
            float r7 = r14.xOffSet
            float r7 = r7 * r1
            float r6 = r6 - r7
            int r6 = (r13 > r6 ? 1 : (r13 == r6 ? 0 : -1))
            if (r6 > 0) goto L_0x00c7
            android.graphics.Paint r11 = r14.sleepPaint
            r6 = r15
            r7 = r13
            r8 = r4
            r9 = r13
            r6.drawLine(r7, r8, r9, r10, r11)
        L_0x00c7:
            int r2 = r2 + 1
            r6 = r13
            goto L_0x002f
        L_0x00cc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.SleepBarChart.drawSleepBar(android.graphics.Canvas):void");
    }

    private void drawTime(Canvas canvas) {
        float f;
        float f2;
        this.bottomPaint.setColor(-1);
        float f3 = this.timeTextSize * 2.0f;
        this.bottomPaint.setTextSize(f3);
        int i = this.screenWidth;
        if (i == 720) {
            f = this.timeTextSize;
            f2 = 1.6f;
        } else {
            if (i < 720) {
                f = this.timeTextSize;
                f2 = 1.2f;
            }
            int[] iArr = this.sleepTimes;
            int i2 = iArr[0];
            int i3 = iArr[1];
            String str = ((Object) getTotalSleepTimeSpanText(i2, i3)) + "";
            this.bottomPaint.setTextAlign(Paint.Align.CENTER);
            this.bottomPaint.setColor(-1);
            this.bottomPaint.setTextSize(f3 * 1.5f);
            float ascent = (this.bottomPaint.ascent() + this.bottomPaint.descent()) / 2.0f;
            canvas.drawText(str, (float) (this.f5243w / 2), ((float) this.f5242h) + ascent, this.bottomPaint);
            this.bottomHeight = ViewUtil.getTextRectHeight(this.bottomPaint, str) - ascent;
        }
        f3 = f * f2;
        int[] iArr2 = this.sleepTimes;
        int i22 = iArr2[0];
        int i32 = iArr2[1];
        String str2 = ((Object) getTotalSleepTimeSpanText(i22, i32)) + "";
        this.bottomPaint.setTextAlign(Paint.Align.CENTER);
        this.bottomPaint.setColor(-1);
        this.bottomPaint.setTextSize(f3 * 1.5f);
        float ascent2 = (this.bottomPaint.ascent() + this.bottomPaint.descent()) / 2.0f;
        canvas.drawText(str2, (float) (this.f5243w / 2), ((float) this.f5242h) + ascent2, this.bottomPaint);
        this.bottomHeight = ViewUtil.getTextRectHeight(this.bottomPaint, str2) - ascent2;
    }

    private void drawTitle(Canvas canvas) {
        List<HealthSleepItem> list;
        if (!(this.sleepData == null || (list = this.sleepItems) == null || list.size() <= 0)) {
            this.topPaint.setTextAlign(Paint.Align.CENTER);
            this.topWidth = (float) this.topBitmap.getWidth();
            canvas.drawBitmap(this.topBitmap, ((float) (this.f5243w / 2)) - (this.topWidth / 2.0f), this.titleSpan, this.topPaint);
        }
        this.topHeight = ((float) this.topBitmap.getHeight()) + this.titleSpan;
    }

    public void setDatas(HealthSleep healthSleep, List<HealthSleepItem> list) {
        if (list != null && list.size() > 0) {
            for (HealthSleepItem healthSleepItem : list) {
            }
        }
        this.sleepData = healthSleep;
        this.sleepItems = list;
        if (healthSleep != null) {
            this.sleepTimes[0] = healthSleep.getTotalSleepMinutes() / 60;
            this.sleepTimes[1] = healthSleep.getTotalSleepMinutes() % 60;
        } else {
            int[] iArr = this.sleepTimes;
            iArr[0] = 0;
            iArr[1] = 0;
        }
        invalidate();
    }

    private SpannableString getTotalSleepTimeSpanText(int i, int i2) {
        String string = getResources().getString(R.string.unit_hour);
        String string2 = getResources().getString(R.string.unit_minute);
        String str = i + " " + string + " " + i2 + " " + string2;
        SpannableString spannableString = new SpannableString(str);
        int indexOf = str.indexOf(string2);
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(i2);
        spannableString.setSpan(new RelativeSizeSpan(1.3333334f), 0, (i + "").length() + 0, 33);
        spannableString.setSpan(new RelativeSizeSpan(1.3333334f), indexOf - sb.toString().length(), indexOf, 33);
        return spannableString;
    }

    private int[] getStartAndEndTime(HealthSleep healthSleep) {
        int sleepEndedTimeH = (healthSleep.getSleepEndedTimeH() * 60) + healthSleep.getSleepEndedTimeM();
        int totalSleepMinutes = healthSleep.getTotalSleepMinutes();
        if (sleepEndedTimeH < totalSleepMinutes) {
            sleepEndedTimeH += 1440;
        }
        return new int[]{sleepEndedTimeH - totalSleepMinutes, sleepEndedTimeH};
    }
}
