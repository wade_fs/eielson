package com.wade.fit.util;

public class ButtonUtils {
    private static long DIFF = 1000;
    private static int lastButtonId = -1;
    private static long lastClickTime;

    public static boolean isFastDoubleClick() {
        return isFastDoubleClick(-1, DIFF);
    }

    public static boolean isFastDoubleClick(int i) {
        return isFastDoubleClick(i, DIFF);
    }

    public static boolean isFastDoubleClick(int i, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = lastClickTime;
        long j3 = currentTimeMillis - j2;
        if (lastButtonId == i && j2 > 0 && j3 < j && j3 > 0) {
            return true;
        }
        lastClickTime = currentTimeMillis;
        lastButtonId = i;
        return false;
    }
}
