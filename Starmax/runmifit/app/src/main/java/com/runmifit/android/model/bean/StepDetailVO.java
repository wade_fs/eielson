package com.wade.fit.model.bean;

import com.wade.fit.greendao.bean.HealthSport;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.views.MainVO;
import java.util.ArrayList;
import java.util.List;

public class StepDetailVO {
    public List<String> dates = new ArrayList();
    public List<HealthSport> healthSportList = new ArrayList();
    public List<HealthSportItem> items = new ArrayList();
    public MainVO mainVO;
}
