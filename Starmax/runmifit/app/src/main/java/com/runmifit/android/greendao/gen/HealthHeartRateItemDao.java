package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class HealthHeartRateItemDao extends AbstractDao<HealthHeartRateItem, Void> {
    public static final String TABLENAME = "HEALTH_HEART_RATE_ITEM";

    public static class Properties {
        public static final Property Date = new Property(10, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(4, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");

        /* renamed from: Fz */
        public static final Property f5218Fz = new Property(7, Integer.TYPE, "fz", false, "FZ");
        public static final Property HeartRaveValue = new Property(9, Integer.TYPE, "HeartRaveValue", false, "HEART_RAVE_VALUE");
        public static final Property IsUploaded = new Property(0, Boolean.TYPE, "isUploaded", false, "IS_UPLOADED");
        public static final Property MacAddress = new Property(1, String.class, "macAddress", false, "MAC_ADDRESS");
        public static final Property Month = new Property(3, Integer.TYPE, "month", false, "MONTH");
        public static final Property OffsetMinute = new Property(5, Integer.TYPE, "offsetMinute", false, "OFFSET_MINUTE");
        public static final Property Oxygen = new Property(8, Integer.TYPE, "oxygen", false, "OXYGEN");
        public static final Property Remark = new Property(12, String.class, "remark", false, "REMARK");

        /* renamed from: Ss */
        public static final Property f5219Ss = new Property(6, Integer.TYPE, "ss", false, "SS");
        public static final Property UserId = new Property(11, String.class, Constants.userId, false, "USER_ID");
        public static final Property Year = new Property(2, Integer.TYPE, "year", false, "YEAR");
    }

    public Void getKey(HealthHeartRateItem healthHeartRateItem) {
        return null;
    }

    public boolean hasKey(HealthHeartRateItem healthHeartRateItem) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(HealthHeartRateItem healthHeartRateItem, long j) {
        return null;
    }

    public HealthHeartRateItemDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public HealthHeartRateItemDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"HEALTH_HEART_RATE_ITEM\" (\"IS_UPLOADED\" INTEGER NOT NULL ,\"MAC_ADDRESS\" TEXT,\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"OFFSET_MINUTE\" INTEGER NOT NULL ,\"SS\" INTEGER NOT NULL ,\"FZ\" INTEGER NOT NULL ,\"OXYGEN\" INTEGER NOT NULL ,\"HEART_RAVE_VALUE\" INTEGER NOT NULL ,\"DATE\" INTEGER NOT NULL ,\"USER_ID\" TEXT,\"REMARK\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"HEALTH_HEART_RATE_ITEM\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, HealthHeartRateItem healthHeartRateItem) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, healthHeartRateItem.getIsUploaded() ? 1 : 0);
        String macAddress = healthHeartRateItem.getMacAddress();
        if (macAddress != null) {
            databaseStatement.bindString(2, macAddress);
        }
        databaseStatement.bindLong(3, (long) healthHeartRateItem.getYear());
        databaseStatement.bindLong(4, (long) healthHeartRateItem.getMonth());
        databaseStatement.bindLong(5, (long) healthHeartRateItem.getDay());
        databaseStatement.bindLong(6, (long) healthHeartRateItem.getOffsetMinute());
        databaseStatement.bindLong(7, (long) healthHeartRateItem.getSs());
        databaseStatement.bindLong(8, (long) healthHeartRateItem.getFz());
        databaseStatement.bindLong(9, (long) healthHeartRateItem.getOxygen());
        databaseStatement.bindLong(10, (long) healthHeartRateItem.getHeartRaveValue());
        databaseStatement.bindLong(11, healthHeartRateItem.getDate());
        String userId = healthHeartRateItem.getUserId();
        if (userId != null) {
            databaseStatement.bindString(12, userId);
        }
        String remark = healthHeartRateItem.getRemark();
        if (remark != null) {
            databaseStatement.bindString(13, remark);
        }
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, HealthHeartRateItem healthHeartRateItem) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, healthHeartRateItem.getIsUploaded() ? 1 : 0);
        String macAddress = healthHeartRateItem.getMacAddress();
        if (macAddress != null) {
            sQLiteStatement.bindString(2, macAddress);
        }
        sQLiteStatement.bindLong(3, (long) healthHeartRateItem.getYear());
        sQLiteStatement.bindLong(4, (long) healthHeartRateItem.getMonth());
        sQLiteStatement.bindLong(5, (long) healthHeartRateItem.getDay());
        sQLiteStatement.bindLong(6, (long) healthHeartRateItem.getOffsetMinute());
        sQLiteStatement.bindLong(7, (long) healthHeartRateItem.getSs());
        sQLiteStatement.bindLong(8, (long) healthHeartRateItem.getFz());
        sQLiteStatement.bindLong(9, (long) healthHeartRateItem.getOxygen());
        sQLiteStatement.bindLong(10, (long) healthHeartRateItem.getHeartRaveValue());
        sQLiteStatement.bindLong(11, healthHeartRateItem.getDate());
        String userId = healthHeartRateItem.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(12, userId);
        }
        String remark = healthHeartRateItem.getRemark();
        if (remark != null) {
            sQLiteStatement.bindString(13, remark);
        }
    }

    public HealthHeartRateItem readEntity(Cursor cursor, int i) {
        Cursor cursor2 = cursor;
        int i2 = i + 1;
        int i3 = i + 11;
        int i4 = i + 12;
        return new HealthHeartRateItem(cursor2.getShort(i + 0) != 0, cursor2.isNull(i2) ? null : cursor2.getString(i2), cursor2.getInt(i + 2), cursor2.getInt(i + 3), cursor2.getInt(i + 4), cursor2.getInt(i + 5), cursor2.getInt(i + 6), cursor2.getInt(i + 7), cursor2.getInt(i + 8), cursor2.getInt(i + 9), cursor2.getLong(i + 10), cursor2.isNull(i3) ? null : cursor2.getString(i3), cursor2.isNull(i4) ? null : cursor2.getString(i4));
    }

    public void readEntity(Cursor cursor, HealthHeartRateItem healthHeartRateItem, int i) {
        healthHeartRateItem.setIsUploaded(cursor.getShort(i + 0) != 0);
        int i2 = i + 1;
        String str = null;
        healthHeartRateItem.setMacAddress(cursor.isNull(i2) ? null : cursor.getString(i2));
        healthHeartRateItem.setYear(cursor.getInt(i + 2));
        healthHeartRateItem.setMonth(cursor.getInt(i + 3));
        healthHeartRateItem.setDay(cursor.getInt(i + 4));
        healthHeartRateItem.setOffsetMinute(cursor.getInt(i + 5));
        healthHeartRateItem.setSs(cursor.getInt(i + 6));
        healthHeartRateItem.setFz(cursor.getInt(i + 7));
        healthHeartRateItem.setOxygen(cursor.getInt(i + 8));
        healthHeartRateItem.setHeartRaveValue(cursor.getInt(i + 9));
        healthHeartRateItem.setDate(cursor.getLong(i + 10));
        int i3 = i + 11;
        healthHeartRateItem.setUserId(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 12;
        if (!cursor.isNull(i4)) {
            str = cursor.getString(i4);
        }
        healthHeartRateItem.setRemark(str);
    }
}
