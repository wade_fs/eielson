package com.wade.fit.persenter.ecg;

import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.greendao.gen.ECGItemInfoDao;
import com.wade.fit.persenter.ecg.EcgDetailContract;
import org.greenrobot.greendao.query.WhereCondition;

public class EcgDetailPresenter extends BasePersenter<EcgDetailContract.View> implements EcgDetailContract.Presenter {
    public void getEcgDetail(String str) {
        ((EcgDetailContract.View) this.mView).getSuccess(AppApplication.getInstance().getDaoSession().getECGItemInfoDao().queryBuilder().where(ECGItemInfoDao.Properties.Date.eq(str), new WhereCondition[0]).orderDesc(ECGItemInfoDao.Properties.Date).build().list());
    }
}
