package com.wade.fit.ui.device.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.base.refresh.BaseDeleteAdapter;
import com.wade.fit.model.bean.Alarm;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.ui.device.adapter.AlarmListAdapter;
import com.wade.fit.persenter.device.AlarmContract;
import com.wade.fit.persenter.device.AlarmPresenter;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleCallback;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.ble.HandlerBleDataResult;
import com.wade.fit.util.log.DebugLog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.activity.AlarmListActivity */
public class AlarmListActivity extends BaseMvpActivity<AlarmPresenter> implements AlarmContract.View, BaseDeleteAdapter.OnCustomClickListener, BaseDeleteAdapter.OnItemClickListener {
    private int editIndex = 0;
    /* access modifiers changed from: private */
    public AlarmListAdapter mAdapter;
    RecyclerView mRecyclerView;
    /* access modifiers changed from: private */
    public List<Alarm> totalAlarms = new ArrayList();

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_alarm_list;
    }

    public void saveFaild() {
    }

    public void saveSuccess() {
    }

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
        super.handleMessage(baseMessage);
        if (baseMessage.getType() == 304) {
            DialogHelperNew.dismissWait();
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.titleName.setText(getResources().getString(R.string.alarm_title));
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.rightText.setText(getResources().getString(R.string.alarm_list_right_add));
        this.rightText.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AlarmListActivity$zm4uVfgXtNQfTHsjjnPKj5Fv3g */

            public final void onClick(View view) {
                AlarmListActivity.this.lambda$initView$0$AlarmListActivity(view);
            }
        });
        BleSdkWrapper.getAlarmList(new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AlarmListActivity.C24481 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                if ((obj instanceof HandlerBleDataResult) && ((HandlerBleDataResult) obj).isComplete) {
                    List unused = AlarmListActivity.this.totalAlarms = SPHelper.getAlarms();
                    AlarmListActivity alarmListActivity = AlarmListActivity.this;
                    AlarmListAdapter unused2 = alarmListActivity.mAdapter = new AlarmListAdapter(alarmListActivity, alarmListActivity.totalAlarms);
                    AlarmListActivity.this.mRecyclerView.setAdapter(AlarmListActivity.this.mAdapter);
                    AlarmListActivity.this.mAdapter.setOnItemClickListener(AlarmListActivity.this);
                    AlarmListActivity.this.mAdapter.setCustomClickListener(AlarmListActivity.this);
                }
            }
        }, false);
        SharePreferenceUtils.putInt(this, SharePreferenceUtils.SUPPORT_ALARM_NUM, 8);
        this.totalAlarms = SPHelper.getAlarms();
        this.mAdapter = new AlarmListAdapter(this, this.totalAlarms);
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mAdapter.setOnItemClickListener(this);
        this.mAdapter.setCustomClickListener(this);
    }

    public /* synthetic */ void lambda$initView$0$AlarmListActivity(View view) {
        List<Alarm> list = this.totalAlarms;
        if (list == null || list.size() < 8) {
            IntentUtil.goToActivityForResult(this, AlarmAddActivity.class, 1);
        } else {
            showToast(getResources().getString(R.string.alarm_max_tips));
        }
    }

    private boolean[] alarmToShowAlarm(boolean[] zArr) {
        boolean[] zArr2 = new boolean[7];
        if (SharePreferenceUtils.getInt(this, SharePreferenceUtils.WEEK_START_INDEX, 1) == 0) {
            zArr2[0] = zArr[5];
            zArr2[1] = zArr[6];
            zArr2[2] = zArr[0];
            zArr2[3] = zArr[1];
            zArr2[4] = zArr[2];
            zArr2[5] = zArr[3];
            zArr2[6] = zArr[4];
            return zArr2;
        } else if (SharePreferenceUtils.getInt(this, SharePreferenceUtils.WEEK_START_INDEX, 1) != 1) {
            return Arrays.copyOf(zArr, zArr.length);
        } else {
            zArr2[0] = zArr[6];
            zArr2[1] = zArr[0];
            zArr2[2] = zArr[1];
            zArr2[3] = zArr[2];
            zArr2[4] = zArr[3];
            zArr2[5] = zArr[4];
            zArr2[6] = zArr[5];
            return zArr2;
        }
    }

    public void onCustomClick(View view, int i) {
        int id = view.getId();
        if (id == R.id.alarm_switch) {
            this.totalAlarms.get(i).setOn_off(!this.totalAlarms.get(i).getOn_off());
            this.mAdapter.notifyItemChanged(i);
            ((AlarmPresenter) this.mPresenter).saveAlarmToDevice(this.totalAlarms);
        } else if (id == R.id.layout_delete) {
            this.totalAlarms.remove(i);
            ((AlarmPresenter) this.mPresenter).saveAlarmToDevice(this.totalAlarms);
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void onItemClick(View view, int i) {
        this.editIndex = i;
        Bundle bundle = new Bundle();
        bundle.putSerializable(NotificationCompat.CATEGORY_ALARM, this.totalAlarms.get(i));
        IntentUtil.goToActivityForResult(this, AlarmAddActivity.class, bundle, 3);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == 200) {
            if (i == 1) {
                Alarm alarm = (Alarm) intent.getSerializableExtra(NotificationCompat.CATEGORY_ALARM);
                DebugLog.m6203d("闹钟设置的回调=" + alarm.toString());
                intent.getBooleanArrayExtra("showWeeks");
                if (this.totalAlarms == null) {
                    this.totalAlarms = new ArrayList();
                }
                this.totalAlarms.add(alarm);
                this.mAdapter.setList(this.totalAlarms);
                ((AlarmPresenter) this.mPresenter).saveAlarmToDevice(this.totalAlarms);
            } else if (i == 3) {
                Alarm alarm2 = (Alarm) intent.getSerializableExtra(NotificationCompat.CATEGORY_ALARM);
                DebugLog.m6203d("闹钟设置的回调=" + alarm2.toString());
                intent.getBooleanArrayExtra("showWeeks");
                this.totalAlarms.add(this.editIndex, alarm2);
                this.totalAlarms.remove(this.editIndex + 1);
                this.mAdapter.setList(this.totalAlarms);
                ((AlarmPresenter) this.mPresenter).saveAlarmToDevice(this.totalAlarms);
            }
        }
    }
}
