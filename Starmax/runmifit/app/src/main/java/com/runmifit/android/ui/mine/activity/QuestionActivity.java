package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.ui.mine.bean.QuestionBean;
import com.wade.fit.ui.sport.adapter.LuAdapter;
import com.wade.fit.ui.sport.adapter.ViewHolder;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.wade.fit.ui.mine.activity.QuestionActivity */
public class QuestionActivity extends BaseActivity {
    LuAdapter<QuestionBean> adapter;
    List<QuestionBean> datas = new ArrayList();
    ListView listView;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_question;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.sys_normal_probleml));
        String[] stringArray = getResources().getStringArray(R.array.question_titles);
        String[] stringArray2 = getResources().getStringArray(R.array.question_contents);
        for (int i = 0; i < stringArray.length; i++) {
            QuestionBean questionBean = new QuestionBean();
            questionBean.title = stringArray[i];
            questionBean.content = stringArray2[i];
            this.datas.add(questionBean);
        }
        this.adapter = new LuAdapter<QuestionBean>(this, this.datas, R.layout.item_question) {
            /* class com.wade.fit.ui.mine.activity.QuestionActivity.C25961 */

            public void convert(ViewHolder viewHolder, int i) {
                QuestionBean questionBean = (QuestionBean) this.datas.get(i);
                viewHolder.setText(R.id.tvQuestionContent, questionBean.content);
                viewHolder.setText(R.id.tvQuestionTitle, questionBean.title);
                if (!questionBean.isOpen) {
                    viewHolder.getView(R.id.tvQuestionContent).setVisibility(View.GONE);
                    ((ImageView) viewHolder.getView(R.id.ivShowContent)).setImageResource(R.mipmap.list_close);
                    return;
                }
                ((ImageView) viewHolder.getView(R.id.ivShowContent)).setImageResource(R.mipmap.list_open);
                viewHolder.getView(R.id.tvQuestionContent).setVisibility(View.VISIBLE);
            }
        };
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class com.wade.fit.ui.mine.activity.$$Lambda$QuestionActivity$kE4ZcXK5iOrFdrOIGyzvsHuuDfI */

            public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
                QuestionActivity.this.lambda$initView$0$QuestionActivity(adapterView, view, i, j);
            }
        });
    }

    public /* synthetic */ void lambda$initView$0$QuestionActivity(AdapterView adapterView, View view, int i, long j) {
        this.datas.get(i).isOpen = !this.datas.get(i).isOpen;
        this.adapter.notifyDataSetChanged();
    }
}
