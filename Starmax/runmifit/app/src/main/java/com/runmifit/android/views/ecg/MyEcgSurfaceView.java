package com.wade.fit.views.ecg;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.ArrayDeque;
import java.util.Deque;

public class MyEcgSurfaceView extends SurfaceView implements SurfaceHolder.Callback, SurfaceHolder.Callback2, Runnable {
    private static float OFFSET_HEART_VALUE = 4096.0f;
    private static int offsetBS = 1;
    public Deque<Double> ECGdatadeque = new ArrayDeque();
    private int current = 0;
    private boolean isDrawing;
    float last_data = 0.0f;
    private Canvas mCanvas;
    private Paint mPaint01;
    private final int offset = 1;
    private SurfaceHolder surfaceHolder;

    public void surfaceChanged(SurfaceHolder surfaceHolder2, int i, int i2, int i3) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder2) {
    }

    public void surfaceRedrawNeeded(SurfaceHolder surfaceHolder2) {
    }

    public MyEcgSurfaceView(Context context) {
        super(context);
    }

    public MyEcgSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public MyEcgSurfaceView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void startThread() {
        if (!this.isDrawing) {
            this.isDrawing = true;
            new Thread(this).start();
        }
    }

    public void stopThread() {
        this.isDrawing = false;
        this.ECGdatadeque.clear();
        this.current = 0;
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder2) {
        this.isDrawing = false;
    }

    private void init() {
        this.surfaceHolder = getHolder();
        this.surfaceHolder.addCallback(this);
        setKeepScreenOn(true);
        setZOrderOnTop(true);
        getHolder().setFormat(-3);
        this.mPaint01 = new Paint();
        this.mPaint01.setColor(Color.parseColor("#FF0000"));
        this.mPaint01.setStyle(Paint.Style.STROKE);
        this.mPaint01.setStrokeWidth(2.0f);
        this.mPaint01.setAntiAlias(true);
    }

    public void run() {
        while (this.isDrawing) {
            long currentTimeMillis = System.currentTimeMillis();
            drawing();
            int currentTimeMillis2 = (int) (System.currentTimeMillis() - currentTimeMillis);
            while (currentTimeMillis2 < 30) {
                currentTimeMillis2 = (int) (System.currentTimeMillis() - currentTimeMillis);
                Thread.yield();
            }
        }
    }

    private void drawing() {
        if (!this.ECGdatadeque.isEmpty()) {
            try {
                if (this.current == 0) {
                    this.mCanvas = this.surfaceHolder.lockCanvas(new Rect(0, 0, getRight(), getHeight()));
                    this.mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
                    this.mCanvas.setDrawFilter(new PaintFlagsDrawFilter(0, 3));
                    this.surfaceHolder.unlockCanvasAndPost(this.mCanvas);
                }
                this.mCanvas = this.surfaceHolder.lockCanvas(new Rect(this.current, 0, this.current + this.ECGdatadeque.size() + 1, getHeight()));
                if (this.mCanvas == null) {
                    Canvas canvas = this.mCanvas;
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    if (!this.isDrawing) {
                        this.current = 0;
                        return;
                    }
                    return;
                }
                this.mCanvas.setDrawFilter(new PaintFlagsDrawFilter(0, 3));
                this.mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
                int i = 0;
                while (true) {
                    if (i >= this.ECGdatadeque.size()) {
                        break;
                    }
                    Double poll = this.ECGdatadeque.poll();
                    if (this.last_data == 0.0f) {
                        this.last_data = poll.floatValue();
                    }
                    this.mCanvas.drawLine((float) this.current, ((float) (getHeight() / 2)) + ((this.last_data * ((float) getHeight())) / OFFSET_HEART_VALUE), (float) (this.current + 1), ((float) (getHeight() / 2)) + ((poll.floatValue() * ((float) getHeight())) / OFFSET_HEART_VALUE), this.mPaint01);
                    this.last_data = poll.floatValue();
                    this.current++;
                    if (this.current > getWidth()) {
                        this.current = 0;
                        break;
                    }
                    i++;
                }
                Canvas canvas2 = this.mCanvas;
                if (canvas2 != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas2);
                }
                if (this.isDrawing) {
                    return;
                }
                this.current = 0;
            } catch (Exception e) {
                e.printStackTrace();
                Canvas canvas3 = this.mCanvas;
                if (canvas3 != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas3);
                }
                if (this.isDrawing) {
                }
            } catch (Throwable th) {
                Canvas canvas4 = this.mCanvas;
                if (canvas4 != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas4);
                }
                if (!this.isDrawing) {
                    this.current = 0;
                }
                throw th;
            }
        } else {
            try {
                Thread.sleep(3);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    public int getWHeight() {
        return getHeight();
    }

    public void setData(double d) {
        this.ECGdatadeque.add(Double.valueOf(d));
    }
}
