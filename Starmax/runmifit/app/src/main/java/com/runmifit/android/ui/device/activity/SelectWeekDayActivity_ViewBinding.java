package com.wade.fit.ui.device.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.device.activity.SelectWeekDayActivity_ViewBinding */
public class SelectWeekDayActivity_ViewBinding implements Unbinder {
    private SelectWeekDayActivity target;

    public SelectWeekDayActivity_ViewBinding(SelectWeekDayActivity selectWeekDayActivity) {
        this(selectWeekDayActivity, selectWeekDayActivity.getWindow().getDecorView());
    }

    public SelectWeekDayActivity_ViewBinding(SelectWeekDayActivity selectWeekDayActivity, View view) {
        this.target = selectWeekDayActivity;
        selectWeekDayActivity.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.mRecyclerView, "field 'mRecyclerView'", RecyclerView.class);
    }

    public void unbind() {
        SelectWeekDayActivity selectWeekDayActivity = this.target;
        if (selectWeekDayActivity != null) {
            this.target = null;
            selectWeekDayActivity.mRecyclerView = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
