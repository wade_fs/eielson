package com.wade.fit.notification;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import com.wade.fit.app.Constants;
import com.wade.fit.service.IntelligentNotificationService;
import com.wade.fit.util.AppUtil;
import com.wade.fit.util.log.LogUtil;

public class NotificationServiceManager {
    private NotificationServiceMonitor notificationServiceMonitor = new NotificationServiceMonitor();

    public void bindIntelligentNotificationService(Context context) {
        boolean isServiceRunning = AppUtil.isServiceRunning(context, IntelligentNotificationService.class.getName());
        LogUtil.dAndSave(" IntelligentNotificationService服务 是否运行 " + isServiceRunning, Constants.NOTIFICATION_PATH);
        if (!isServiceRunning) {
            toggleNotificationListenerService(context);
        }
    }

    private void toggleNotificationListenerService(Context context) {
        PackageManager packageManager = context.getPackageManager();
        LogUtil.dAndSave("toggleNotificationListenerService IntelligentNotificationService服务", Constants.NOTIFICATION_PATH);
        packageManager.setComponentEnabledSetting(new ComponentName(context, IntelligentNotificationService.class), 2, 1);
        packageManager.setComponentEnabledSetting(new ComponentName(context, IntelligentNotificationService.class), 1, 1);
        this.notificationServiceMonitor.startMonitor();
    }
}
