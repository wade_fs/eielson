package com.wade.fit.ui.main.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.SleepBarChart;
import com.wade.fit.views.SleepBarDetailChart;

/* renamed from: com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding */
public class DetailSleepActivity_ViewBinding implements Unbinder {
    private DetailSleepActivity target;
    private View view2131296572;
    private View view2131296575;
    private View view2131296576;
    private View view2131296769;
    private View view2131296770;
    private View view2131296771;
    private View view2131296772;
    private View view2131296995;
    private View view2131297033;
    private View view2131297080;
    private View view2131297083;

    public DetailSleepActivity_ViewBinding(DetailSleepActivity detailSleepActivity) {
        this(detailSleepActivity, detailSleepActivity.getWindow().getDecorView());
    }

    public DetailSleepActivity_ViewBinding(final DetailSleepActivity detailSleepActivity, View view) {
        this.target = detailSleepActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.tvWeek, "field 'tvWeek' and method 'onClick'");
        detailSleepActivity.tvWeek = (TextView) Utils.castView(findRequiredView, R.id.tvWeek, "field 'tvWeek'", TextView.class);
        this.view2131297080 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25131 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.tvMonth, "field 'tvMonth' and method 'onClick'");
        detailSleepActivity.tvMonth = (TextView) Utils.castView(findRequiredView2, R.id.tvMonth, "field 'tvMonth'", TextView.class);
        this.view2131297033 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25162 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.tvYear, "field 'tvYear' and method 'onClick'");
        detailSleepActivity.tvYear = (TextView) Utils.castView(findRequiredView3, R.id.tvYear, "field 'tvYear'", TextView.class);
        this.view2131297083 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25173 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.ivPreDate, "field 'ivPreDate' and method 'changePreDay'");
        detailSleepActivity.ivPreDate = (ImageView) Utils.castView(findRequiredView4, R.id.ivPreDate, "field 'ivPreDate'", ImageView.class);
        this.view2131296575 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25184 */

            public void doClick(View view) {
                detailSleepActivity.changePreDay();
            }
        });
        View findRequiredView5 = Utils.findRequiredView(view, R.id.tvDate, "field 'tvDate' and method 'onClick'");
        detailSleepActivity.tvDate = (TextView) Utils.castView(findRequiredView5, R.id.tvDate, "field 'tvDate'", TextView.class);
        this.view2131296995 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25195 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
        detailSleepActivity.tvStartSleepH = (TextView) Utils.findRequiredViewAsType(view, R.id.tvStartSleepH, "field 'tvStartSleepH'", TextView.class);
        detailSleepActivity.tvStartSleepM = (TextView) Utils.findRequiredViewAsType(view, R.id.tvStartSleepM, "field 'tvStartSleepM'", TextView.class);
        detailSleepActivity.tvDeepSleepH = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDeepSleepH, "field 'tvDeepSleepH'", TextView.class);
        detailSleepActivity.tvDeepSleepM = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDeepSleepM, "field 'tvDeepSleepM'", TextView.class);
        detailSleepActivity.tvLightSleepH = (TextView) Utils.findRequiredViewAsType(view, R.id.tvLightSleepH, "field 'tvLightSleepH'", TextView.class);
        detailSleepActivity.tvLightSleepM = (TextView) Utils.findRequiredViewAsType(view, R.id.tvLightSleepM, "field 'tvLightSleepM'", TextView.class);
        detailSleepActivity.tvWakeSleepH = (TextView) Utils.findRequiredViewAsType(view, R.id.tvWakeSleepH, "field 'tvWakeSleepH'", TextView.class);
        detailSleepActivity.tvWakeSleepM = (TextView) Utils.findRequiredViewAsType(view, R.id.tvWakeSleepM, "field 'tvWakeSleepM'", TextView.class);
        View findRequiredView6 = Utils.findRequiredView(view, R.id.ivNextDate, "field 'ivNextDate' and method 'changeNextDay'");
        detailSleepActivity.ivNextDate = (ImageView) Utils.castView(findRequiredView6, R.id.ivNextDate, "field 'ivNextDate'", ImageView.class);
        this.view2131296572 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25206 */

            public void doClick(View view) {
                detailSleepActivity.changeNextDay();
            }
        });
        detailSleepActivity.sleep_BarChart = (SleepBarChart) Utils.findRequiredViewAsType(view, R.id.sleep_BarChart, "field 'sleep_BarChart'", SleepBarChart.class);
        detailSleepActivity.sleepdetailbar = (SleepBarDetailChart) Utils.findRequiredViewAsType(view, R.id.sleepdetailbar, "field 'sleepdetailbar'", SleepBarDetailChart.class);
        detailSleepActivity.tvTotal = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTotal, "field 'tvTotal'", TextView.class);
        View findRequiredView7 = Utils.findRequiredView(view, R.id.rbDay, "method 'onClick'");
        this.view2131296769 = findRequiredView7;
        findRequiredView7.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25217 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
        View findRequiredView8 = Utils.findRequiredView(view, R.id.rbOneMonth, "method 'onClick'");
        this.view2131296770 = findRequiredView8;
        findRequiredView8.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25228 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
        View findRequiredView9 = Utils.findRequiredView(view, R.id.rbSixMonth, "method 'onClick'");
        this.view2131296771 = findRequiredView9;
        findRequiredView9.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C25239 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
        View findRequiredView10 = Utils.findRequiredView(view, R.id.rbYear, "method 'onClick'");
        this.view2131296772 = findRequiredView10;
        findRequiredView10.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C251410 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
        View findRequiredView11 = Utils.findRequiredView(view, R.id.ivSelectedDate, "method 'onClick'");
        this.view2131296576 = findRequiredView11;
        findRequiredView11.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailSleepActivity_ViewBinding.C251511 */

            public void doClick(View view) {
                detailSleepActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        DetailSleepActivity detailSleepActivity = this.target;
        if (detailSleepActivity != null) {
            this.target = null;
            detailSleepActivity.tvWeek = null;
            detailSleepActivity.tvMonth = null;
            detailSleepActivity.tvYear = null;
            detailSleepActivity.ivPreDate = null;
            detailSleepActivity.tvDate = null;
            detailSleepActivity.tvStartSleepH = null;
            detailSleepActivity.tvStartSleepM = null;
            detailSleepActivity.tvDeepSleepH = null;
            detailSleepActivity.tvDeepSleepM = null;
            detailSleepActivity.tvLightSleepH = null;
            detailSleepActivity.tvLightSleepM = null;
            detailSleepActivity.tvWakeSleepH = null;
            detailSleepActivity.tvWakeSleepM = null;
            detailSleepActivity.ivNextDate = null;
            detailSleepActivity.sleep_BarChart = null;
            detailSleepActivity.sleepdetailbar = null;
            detailSleepActivity.tvTotal = null;
            this.view2131297080.setOnClickListener(null);
            this.view2131297080 = null;
            this.view2131297033.setOnClickListener(null);
            this.view2131297033 = null;
            this.view2131297083.setOnClickListener(null);
            this.view2131297083 = null;
            this.view2131296575.setOnClickListener(null);
            this.view2131296575 = null;
            this.view2131296995.setOnClickListener(null);
            this.view2131296995 = null;
            this.view2131296572.setOnClickListener(null);
            this.view2131296572 = null;
            this.view2131296769.setOnClickListener(null);
            this.view2131296769 = null;
            this.view2131296770.setOnClickListener(null);
            this.view2131296770 = null;
            this.view2131296771.setOnClickListener(null);
            this.view2131296771 = null;
            this.view2131296772.setOnClickListener(null);
            this.view2131296772 = null;
            this.view2131296576.setOnClickListener(null);
            this.view2131296576 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
