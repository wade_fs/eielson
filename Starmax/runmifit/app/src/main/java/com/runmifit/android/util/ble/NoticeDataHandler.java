package com.wade.fit.util.ble;

import com.wade.fit.model.bean.AppNotice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.log.LogUtil;

public class NoticeDataHandler {
    public void handlerNoticeData(byte[] bArr) {
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        AppNotice appNotice = new AppNotice();
        byte[] Int2Bit8 = ByteDataConvertUtil.Int2Bit8(ByteDataConvertUtil.Byte2Int(bArr[3]));
        byte[] Int2Bit82 = ByteDataConvertUtil.Int2Bit8(ByteDataConvertUtil.Byte2Int(bArr[4]));
        for (int i = 0; i < Int2Bit8.length; i++) {
            LogUtil.d("nitoce:" + ((int) Int2Bit8[i]) + "/" + ((int) Int2Bit82[i]));
        }
        if (Int2Bit8[0] == 1) {
            appNotice.twitter = true;
        } else {
            appNotice.twitter = false;
        }
        if (Int2Bit8[1] == 1) {
            appNotice.facebook = true;
        } else {
            appNotice.facebook = false;
        }
        if (Int2Bit8[2] == 1) {
            appNotice.email = true;
        } else {
            appNotice.email = false;
        }
        if (Int2Bit8[3] == 1) {
            deviceConfig.isMessage = true;
        } else {
            deviceConfig.isMessage = false;
        }
        if (Int2Bit8[4] == 1) {
            deviceConfig.isCall = true;
        } else {
            deviceConfig.isCall = false;
        }
        if (Int2Bit8[5] == 1) {
            appNotice.whatsApp = true;
        } else {
            appNotice.whatsApp = false;
        }
        if (Int2Bit8[6] == 1) {
            appNotice.line = true;
        } else {
            appNotice.line = false;
        }
        if (Int2Bit8[7] == 1) {
            appNotice.skype = true;
        } else {
            appNotice.skype = false;
        }
        if (Int2Bit82[0] == 1) {
            appNotice.f5221qq = true;
        } else {
            appNotice.f5221qq = false;
        }
        if (Int2Bit82[1] == 1) {
            appNotice.wechat = true;
        } else {
            appNotice.wechat = false;
        }
        if (Int2Bit82[2] == 1) {
            appNotice.instagram = true;
        } else {
            appNotice.instagram = false;
        }
        if (Int2Bit82[3] == 1) {
            appNotice.linked = true;
        } else {
            appNotice.linked = false;
        }
        if (Int2Bit82[4] == 1) {
            appNotice.messager = true;
        } else {
            appNotice.messager = false;
        }
        if (Int2Bit82[5] == 1) {
            appNotice.f5222vk = true;
        } else {
            appNotice.f5222vk = false;
        }
        deviceConfig.notice = appNotice;
        SPHelper.saveDeviceConfig(deviceConfig);
    }
}
