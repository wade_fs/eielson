package com.wade.fit.views;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.support.v4.app.NotificationCompat;
import android.support.v4.internal.view.SupportMenu;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.wade.fit.R;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.log.LogUtil;

public class SportPieView extends RelativeLayout {
    private static final float PIE_RADIUS_SCALE = 0.4347826f;
    private static final float SPACE_SCALE = 0.006521739f;
    private int[] SWEEP_COLOR;
    ObjectAnimator anim;
    int animCount = 0;
    private int bgColor;
    private int bigScreenHeight = 780;
    private int defaultHeight = 750;
    private Bitmap finishAfterBitmap;
    private Bitmap finishBeforeBitmap;
    private long firstTime = 0;
    private TextView goalView;
    private SweepGradient gradient;

    /* renamed from: h */
    private int f5248h;
    private float height;
    private ImageView imageview;
    private int largeScreenHeight = 950;
    private float minSize;
    private float outRadius;
    private Paint outsidePaint;
    private Paint paint;
    private TextView percentView;
    private int pieColor;
    private float pieRadius;
    private int progress;
    private float radTopPadding;
    private float radius;
    private RectF rectF;
    private Paint redPaint;
    private Paint ringPain2;
    private Paint ringPaint;
    private float ringWidth;
    private boolean save = true;
    private int saveH;
    private float space;
    private int sportGoal = 0;
    private int sportStep = 0;
    private TextView stepView;
    private float textSize;
    private int twokScreenHeight = 1000;

    /* renamed from: w */
    private int f5249w;

    private void setShader(int i) {
    }

    public SportPieView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        LayoutInflater.from(context).inflate((int) R.layout.layout_sport_pie_view, this);
        setWillNotDraw(false);
        this.imageview = (ImageView) findViewById(R.id.stateImg);
        this.stepView = (TextView) findViewById(R.id.data);
        this.goalView = (TextView) findViewById(R.id.goal);
        this.percentView = (TextView) findViewById(R.id.percent);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SportPieView);
        this.pieColor = obtainStyledAttributes.getColor(2, 0);
        this.bgColor = obtainStyledAttributes.getColor(0, 1040187391);
        obtainStyledAttributes.recycle();
        this.finishBeforeBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.finish_before);
        this.finishAfterBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.finish_after);
        this.radTopPadding = (float) (this.finishBeforeBitmap.getHeight() > this.finishBeforeBitmap.getWidth() ? this.finishBeforeBitmap.getHeight() : this.finishBeforeBitmap.getWidth());
        this.firstTime = System.currentTimeMillis();
        this.SWEEP_COLOR = new int[]{getResources().getColor(R.color.sport_pie_view_1), getResources().getColor(R.color.sport_pie_view_2), getResources().getColor(R.color.sport_pie_view_2)};
        initPaint();
    }

    private void initPaint() {
        this.paint = new Paint(1);
        this.paint.setStyle(Paint.Style.STROKE);
        this.rectF = new RectF();
        this.outsidePaint = new Paint(1);
        this.outsidePaint.setStyle(Paint.Style.STROKE);
        this.ringPaint = new Paint(1);
        this.ringPaint.setStyle(Paint.Style.STROKE);
        this.ringPaint.setStrokeCap(Paint.Cap.ROUND);
        this.ringPain2 = new Paint(1);
        this.ringPain2.setStyle(Paint.Style.STROKE);
        this.ringPain2.setStrokeCap(Paint.Cap.BUTT);
        this.ringPain2.setColor((int) SupportMenu.CATEGORY_MASK);
        this.redPaint = new Paint(1);
        this.redPaint.setStyle(Paint.Style.STROKE);
        this.redPaint.setColor(this.SWEEP_COLOR[2]);
        this.redPaint.setStrokeCap(Paint.Cap.ROUND);
        setWillNotDraw(false);
    }

    public void setSportGoal(int i) {
        if (i != 0 || this.sportGoal != 0) {
            this.sportGoal = i;
            this.goalView.setText(getResources().getString(R.string.sport_goal, Integer.valueOf(i)));
            updateProgress(false);
        }
    }

    public void setSportSteps(int i) {
        setSportSteps(i, true);
    }

    public void setSportSteps(int i, boolean z) {
        this.sportStep = i;
        setSteps2View(this.sportStep);
        updateProgress(z);
    }

    private int getCurrentPresent() {
        int i = this.sportGoal;
        return (int) (i == 0 ? 0.0d : Math.floor((double) ((((float) this.sportStep) * 100.0f) / ((float) i))));
    }

    private void updateProgress(boolean z) {
        this.progress = getCurrentPresent();
        this.percentView.setText(getResources().getString(R.string.sport_percent, Integer.valueOf(this.progress)));
        if (z) {
            int i = this.sportStep;
        } else {
            invalidate();
        }
    }

    private void setSteps2View(int i) {
        this.stepView.setText(getSpanText(i));
        invalidate();
    }

    private SpannableString getSpanText(int i) {
        String str = i + "";
        SpannableString spannableString = new SpannableString(str);
        int indexOf = str.indexOf(i + "");
        int length = (i + "").length() + indexOf;
        if (ScreenUtil.isOver6Inch((Activity) getContext())) {
            if (i >= 10000) {
                spannableString.setSpan(new RelativeSizeSpan(1.6f), indexOf, length, 33);
            } else {
                spannableString.setSpan(new RelativeSizeSpan(2.2f), indexOf, length, 33);
            }
        } else if (i >= 10000) {
            spannableString.setSpan(new RelativeSizeSpan(0.9f), indexOf, length, 33);
        } else {
            spannableString.setSpan(new RelativeSizeSpan(1.3f), indexOf, length, 33);
        }
        return spannableString;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5249w = i;
        this.f5248h = i2;
        if (this.save) {
            this.saveH = i2;
            this.save = false;
        }
        setShader(this.progress);
        this.minSize = i < i2 ? (float) i : (float) i2;
        float f = this.minSize;
        this.height = f;
        if (f > 1000.0f) {
            this.minSize = 1000.0f;
        }
        float f2 = this.minSize;
        float f3 = this.radTopPadding;
        this.outRadius = (f2 / 2.0f) - (f3 / 2.0f);
        this.pieRadius = ((f2 / 2.0f) - f3) - (f3 / 2.0f);
        this.radius = this.pieRadius - ((float) ScreenUtil.dip2px(10.0f));
        this.ringWidth = this.radTopPadding;
        this.ringPaint.setColor(-1);
        this.ringPaint.setStrokeWidth((float) ScreenUtil.dip2px(4.0f));
        this.ringPain2.setStrokeWidth((float) ScreenUtil.dip2px(4.0f));
        this.ringPain2.setColor(this.SWEEP_COLOR[0]);
        this.ringPain2.setPathEffect(new DashPathEffect(new float[]{5.0f, 5.0f}, 0.0f));
        this.redPaint.setStrokeWidth(this.ringWidth);
        LogUtil.d("outRadius:" + this.outRadius + ",pieRadius:" + this.pieRadius + ",radius:" + this.radius);
        RectF rectF2 = this.rectF;
        float f4 = this.radius;
        rectF2.set((((float) i) / 2.0f) - f4, (((float) i2) / 2.0f) - f4, ((float) (i / 2)) + f4, ((float) (i2 / 2)) + f4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setLayerType(1, null);
        this.outsidePaint.setColor(this.bgColor);
        this.outsidePaint.setStyle(Paint.Style.STROKE);
        this.outsidePaint.setStrokeWidth(this.outRadius - this.pieRadius);
        this.outsidePaint.setShader(new LinearGradient(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), getResources().getColor(R.color.main_grap_color1), getResources().getColor(R.color.main_grap_color2), Shader.TileMode.MIRROR));
        this.outsidePaint.setColor(getResources().getColor(R.color.white));
        float f = this.outRadius;
        canvas.drawCircle((float) (this.f5249w / 2), (float) (this.f5248h / 2), f - ((f - this.pieRadius) / 2.0f), this.outsidePaint);
        this.paint.setColor(0);
        canvas.drawCircle((float) (this.f5249w / 2), (float) (this.f5248h / 2), this.radius, this.ringPain2);
        canvas.drawArc(this.rectF, -90.0f, ((((float) this.progress) / 100.0f) * 360.0f) - 1.0f, false, this.ringPaint);
    }

    public void setRadius(Canvas canvas) {
        if (this.progress >= 100) {
            this.progress = 100;
            this.redPaint.setColor(0);
            return;
        }
        this.redPaint.setColor(this.SWEEP_COLOR[2]);
    }

    public void setProgress(int i) {
        this.progress = i;
    }

    public void startAnim() {
        if (this.sportStep != 0) {
            this.animCount++;
            this.progress = getCurrentPresent();
            int i = this.progress;
            if (i != 0) {
                int max = Math.max(0, Math.min(i * 20, (int) ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED));
                int i2 = this.progress;
                ObjectAnimator objectAnimator = this.anim;
                if (objectAnimator == null || !objectAnimator.isStarted()) {
                    this.anim = ObjectAnimator.ofInt(this, NotificationCompat.CATEGORY_PROGRESS, 0, this.progress).setDuration((long) max);
                    this.anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(i2) {
                        /* class com.wade.fit.views.$$Lambda$SportPieView$_X6dd9jaTE6QLzVCvrUMoTpnUgc */
                        private final /* synthetic */ int f$1;

                        {
                            this.f$1 = r2;
                        }

                        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                            SportPieView.this.lambda$startAnim$0$SportPieView(this.f$1, valueAnimator);
                        }
                    });
                    this.anim.start();
                }
            }
        }
    }

    public /* synthetic */ void lambda$startAnim$0$SportPieView(int i, ValueAnimator valueAnimator) {
        float intValue = ((float) ((Integer) valueAnimator.getAnimatedValue()).intValue()) / (((float) i) * 1.0f);
        int abs = (int) (((float) this.sportStep) * Math.abs(intValue));
        if (abs >= 0) {
            this.stepView.setText(getSpanText(abs));
        }
        if (intValue == 1.0f) {
            this.stepView.setText(getSpanText(Math.abs(this.sportStep)));
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        if (i == 8) {
            this.animCount = 0;
        }
    }

    private int getProgress() {
        return this.progress;
    }
}
