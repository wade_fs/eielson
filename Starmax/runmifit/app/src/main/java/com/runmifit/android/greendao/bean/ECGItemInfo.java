package com.wade.fit.greendao.bean;

public class ECGItemInfo {
    private long date;
    private int day;

    /* renamed from: hr */
    private float f5214hr;
    private int month;
    private int speed;
    private int state = 1;
    private String userId;
    private int year;
    private int zengYi;

    public ECGItemInfo(int i, int i2, int i3, long j, String str, int i4, int i5, float f, int i6) {
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.date = j;
        this.userId = str;
        this.zengYi = i4;
        this.speed = i5;
        this.f5214hr = f;
        this.state = i6;
    }

    public ECGItemInfo() {
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public int getZengYi() {
        return this.zengYi;
    }

    public void setZengYi(int i) {
        this.zengYi = i;
    }

    public int getSpeed() {
        return this.speed;
    }

    public void setSpeed(int i) {
        this.speed = i;
    }

    public float getHr() {
        return this.f5214hr;
    }

    public void setHr(float f) {
        this.f5214hr = f;
    }

    public int getState() {
        return this.state;
    }

    public void setState(int i) {
        this.state = i;
    }
}
