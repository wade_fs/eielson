package com.wade.fit.service;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.service.notification.NotificationListenerService;
import android.view.KeyEvent;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ble.BaseDataHandler;
import com.wade.fit.util.ble.BleClient;
import com.wade.fit.util.ble.DeviceCallbackWrapper;
import com.wade.fit.util.log.DebugLog;

public class MusicContrlService extends NotificationListenerService {
    /* access modifiers changed from: private */
    public AudioManager audioManager;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public String mMusicPlayPackageName;
    /* access modifiers changed from: private */
    public PackageManager packageManager;
    private BleClient protocolUtils = BleClient.getInstance();

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.audioManager = (AudioManager) getSystemService("audio");
        this.packageManager = getPackageManager();
        BaseDataHandler.getInstance().addDeviceCallback(new DeviceCallbackWrapper() {
            /* class com.wade.fit.service.MusicContrlService.C24401 */

            public void musicControl() {
                super.musicControl();
                DebugLog.m6203d("手环控制App开始音乐成功");
                String unused = MusicContrlService.this.mMusicPlayPackageName = SPHelper.getDeviceConfig().musicPackageName;
                if (!MusicContrlService.this.mMusicPlayPackageName.equals("")) {
                    Intent launchIntentForPackage = MusicContrlService.this.packageManager.getLaunchIntentForPackage(MusicContrlService.this.mMusicPlayPackageName);
                    if (launchIntentForPackage != null) {
                        MusicContrlService.this.startActivity(launchIntentForPackage);
                    }
                    if (MusicContrlService.this.audioManager.isMusicActive()) {
                        MusicContrlService.this.sendMusicKeyEvent(FMParserConstants.CLOSE_PAREN);
                    } else {
                        MusicContrlService.this.sendMusicKeyEvent(FMParserConstants.OPEN_PAREN);
                    }
                }
            }

            public void exitMusic() {
                super.exitMusic();
                String unused = MusicContrlService.this.mMusicPlayPackageName = SPHelper.getDeviceConfig().musicPackageName;
                if (MusicContrlService.this.audioManager.isMusicActive()) {
                    MusicContrlService.this.sendMusicKeyEvent(FMParserConstants.CLOSE_PAREN);
                }
            }

            public void preMusic() {
                Intent launchIntentForPackage;
                super.preMusic();
                String unused = MusicContrlService.this.mMusicPlayPackageName = SPHelper.getDeviceConfig().musicPackageName;
                if (!MusicContrlService.this.mMusicPlayPackageName.equals("") && (launchIntentForPackage = MusicContrlService.this.packageManager.getLaunchIntentForPackage(MusicContrlService.this.mMusicPlayPackageName)) != null) {
                    MusicContrlService.this.startActivity(launchIntentForPackage);
                }
                if (!MusicContrlService.this.audioManager.isMusicActive()) {
                    MusicContrlService.this.sendMusicKeyEvent(FMParserConstants.OPEN_PAREN);
                }
                MusicContrlService.this.sendMusicKeyEvent(88);
                DebugLog.m6203d("android.os.Build.MANUFACTURER:" + Build.MANUFACTURER);
                if (Build.MANUFACTURER.equalsIgnoreCase("Meizu")) {
                    DebugLog.m6203d("魅族手机再发一次");
                    MusicContrlService.this.handler.postDelayed(new Runnable() {
                        /* class com.wade.fit.service.MusicContrlService.C24401.C24411 */

                        public void run() {
                            MusicContrlService.this.sendMusicKeyEvent(88);
                            if (!MusicContrlService.this.audioManager.isMusicActive()) {
                                MusicContrlService.this.sendMusicKeyEvent(FMParserConstants.OPEN_PAREN);
                            }
                        }
                    }, 100);
                }
            }

            public void nextMusic() {
                Intent launchIntentForPackage;
                super.nextMusic();
                DebugLog.m6203d("下一曲成功");
                String unused = MusicContrlService.this.mMusicPlayPackageName = SPHelper.getDeviceConfig().musicPackageName;
                if (!MusicContrlService.this.mMusicPlayPackageName.equals("") && (launchIntentForPackage = MusicContrlService.this.packageManager.getLaunchIntentForPackage(MusicContrlService.this.mMusicPlayPackageName)) != null) {
                    MusicContrlService.this.startActivity(launchIntentForPackage);
                }
                if (!MusicContrlService.this.audioManager.isMusicActive()) {
                    MusicContrlService.this.sendMusicKeyEvent(FMParserConstants.OPEN_PAREN);
                }
                MusicContrlService.this.sendMusicKeyEvent(87);
            }

            public void addVol() {
                super.addVol();
                DebugLog.m6203d("音量加成功");
                MusicContrlService.this.sendMusicKeyEvent(24);
            }

            public void subVol() {
                super.subVol();
                MusicContrlService.this.sendMusicKeyEvent(25);
            }
        });
    }

    /* access modifiers changed from: private */
    public void sendMusicKeyEvent(int i) {
        long uptimeMillis = SystemClock.uptimeMillis() - 1;
        KeyEvent keyEvent = new KeyEvent(uptimeMillis, uptimeMillis, 0, i, 0);
        dispatchMediaKeyToAudioService(keyEvent, i);
        dispatchMediaKeyToAudioService(KeyEvent.changeAction(keyEvent, 1), i);
        DebugLog.m6203d("----AudioManager 发送键值成功----");
    }

    private void dispatchMediaKeyToAudioService(KeyEvent keyEvent, int i) {
        if (this.audioManager == null) {
            this.audioManager = (AudioManager) getSystemService("audio");
        }
        AudioManager audioManager2 = this.audioManager;
        if (audioManager2 != null) {
            if (i == 24) {
                audioManager2.adjustStreamVolume(3, 1, 1);
            } else if (i == 25) {
                audioManager2.adjustStreamVolume(3, -1, 1);
            } else {
                try {
                    audioManager2.dispatchMediaKeyEvent(keyEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
