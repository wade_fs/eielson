package com.wade.fit.ui.main.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.mob.tools.utils.UIHandler;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.Alarm;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.ui.mine.activity.PersonUnitActivity;
import com.wade.fit.persenter.main.LoginContract;
import com.wade.fit.persenter.main.LoginPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DataVaildUtil;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.LogUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import p004cn.sharesdk.facebook.Facebook;
import p004cn.sharesdk.framework.Platform;
import p004cn.sharesdk.framework.PlatformActionListener;
import p004cn.sharesdk.framework.ShareSDK;
import p004cn.sharesdk.wechat.friends.Wechat;

/* renamed from: com.wade.fit.ui.main.activity.LoginActivity */
public class LoginActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.View, Handler.Callback {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final int MSG_ACTION_FACEBOOK_COMPLETE = 2;
    private static final int MSG_ACTION_WECHAT_COMPLETE = 1;
    Button btnLogin;
    EditText etPwd;
    EditText etUsername;
    ImageView imgClean;
    ImageView imgStatus;
    private boolean isFront = false;
    private int loginType;
    TextView tvSkip;
    private String userName;
    private String userPwd;

    static /* synthetic */ void lambda$showRemindDialog$5(View view) {
    }

    static /* synthetic */ void lambda$showRemindSkipDialog$1(View view) {
    }

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_login;
    }

    public void registerFaild(int i) {
    }

    public void registerSuccess() {
    }

    public void sendCodeFaild(int i) {
    }

    public void sendCodeSuccess() {
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(9216);
        }
        this.bgView.setVisibility(View.GONE);
        this.layoutTitle.setBackgroundColor(getResources().getColor(R.color.white));
        this.titleBack.setImageResource(R.mipmap.img_close);
        if (((Bundle) Objects.requireNonNull(getIntent().getExtras())).getString("from").equals("WelcomeActivity")) {
            this.titleBack.setVisibility(View.GONE);
            this.tvSkip.setVisibility(View.VISIBLE);
        } else {
            this.titleBack.setVisibility(View.VISIBLE);
            this.tvSkip.setVisibility(View.GONE);
        }
        this.etUsername.addTextChangedListener(new TextWatcher() {
            /* class com.wade.fit.ui.main.activity.LoginActivity.C15331 */

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(LoginActivity.this.etUsername.getText().toString())) {
                    LoginActivity.this.imgClean.setVisibility(View.GONE);
                } else {
                    LoginActivity.this.imgClean.setVisibility(View.VISIBLE);
                }
                if (TextUtils.isEmpty(LoginActivity.this.etUsername.getText().toString()) || TextUtils.isEmpty(LoginActivity.this.etPwd.getText().toString()) || LoginActivity.this.etPwd.getText().toString().length() < 6) {
                    LoginActivity.this.btnLogin.setClickable(false);
                    LoginActivity.this.btnLogin.setBackgroundResource(R.drawable.btn_submit_normle);
                    return;
                }
                LoginActivity.this.btnLogin.setClickable(true);
                LoginActivity.this.btnLogin.setBackgroundResource(R.drawable.btn_submit_bg);
            }
        });
        this.etPwd.addTextChangedListener(new TextWatcher() {
            /* class com.wade.fit.ui.main.activity.LoginActivity.C15342 */

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(LoginActivity.this.etUsername.getText().toString()) || TextUtils.isEmpty(LoginActivity.this.etPwd.getText().toString()) || LoginActivity.this.etPwd.getText().toString().length() < 6) {
                    LoginActivity.this.btnLogin.setClickable(false);
                    LoginActivity.this.btnLogin.setBackgroundResource(R.drawable.btn_submit_normle);
                    return;
                }
                LoginActivity.this.btnLogin.setClickable(true);
                LoginActivity.this.btnLogin.setBackgroundResource(R.drawable.btn_submit_bg);
            }
        });
        if (SharePreferenceUtils.getInt(this, Constants.LOGIN_TYPE, -1) == 1) {
            this.etUsername.setText(SharePreferenceUtils.getString(this, Constants.USER_NAME, ""));
        }
    }

    /* access modifiers changed from: package-private */
    public void updatePwdStatus() {
        if (this.imgStatus.getTag().equals("open")) {
            this.imgStatus.setTag("close");
            this.imgStatus.setImageResource(R.mipmap.pwd_open);
            this.etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else if (this.imgStatus.getTag().equals("close")) {
            this.imgStatus.setTag("open");
            this.imgStatus.setImageResource(R.mipmap.pwd_close);
            this.etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        EditText editText = this.etPwd;
        editText.setSelection(editText.getText().length());
    }

    /* access modifiers changed from: package-private */
    public void cleanUserName() {
        this.etUsername.setText("");
    }

    /* access modifiers changed from: package-private */
    public void sendLogin() {
        if (!ButtonUtils.isFastDoubleClick(R.id.btnlogin, 1000)) {
            this.userName = this.etUsername.getText().toString();
            this.userPwd = this.etPwd.getText().toString();
            if (DataVaildUtil.vaildEmailAndPwd(this, this.userName, this.userPwd)) {
                showLoading();
                this.loginType = 1;
                ((LoginPresenter) this.mPresenter).requestLogin(this.userName, this.userPwd);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void toRegister() {
        if (!ButtonUtils.isFastDoubleClick(R.id.tvRegister, 1000)) {
            Bundle bundle = new Bundle();
            bundle.putString("email", this.etUsername.getText().toString().isEmpty() ? "" : this.etUsername.getText().toString());
            IntentUtil.goToActivity(this, RegisterActivity.class, bundle);
        }
    }

    /* access modifiers changed from: package-private */
    public void toUpdatePassword() {
        if (!ButtonUtils.isFastDoubleClick(R.id.tvRegister, 1000)) {
            Bundle bundle = new Bundle();
            bundle.putString("title", getResources().getString(R.string.reset_your_pwd));
            IntentUtil.goToActivity(this, UpdatePasswordActivity.class, bundle);
        }
    }

    /* access modifiers changed from: package-private */
    public void toMain() {
        if (!ButtonUtils.isFastDoubleClick(R.id.tvSkip, 1000)) {
            showRemindSkipDialog();
        }
    }

    private void showRemindSkipDialog() {
        DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.tips), getResources().getString(R.string.skip_tips), getResources().getString(R.string.skip_ad), new View.OnClickListener() {
            /* class com.wade.fit.ui.main.activity.$$Lambda$LoginActivity$S2a5wqQ9Jpz7lPkDi8PCufnOFQ */

            public final void onClick(View view) {
                LoginActivity.this.lambda$showRemindSkipDialog$0$LoginActivity(view);
            }
        }, $$Lambda$LoginActivity$5U7epJUUld6qPIp3pKpbQ2VT7M.INSTANCE);
    }

    public /* synthetic */ void lambda$showRemindSkipDialog$0$LoginActivity(View view) {
        UserBean currentUserBean = SPHelper.getCurrentUserBean();
        SharePreferenceUtils.putString(Constants.USER_TOKEN, "");
        SharePreferenceUtils.putString(Constants.IS_TO_LOGIN, "2");
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.LAST_SYNCH_TIME, 0L);
        if (BleSdkWrapper.isConnected()) {
            AppApplication.getInstance().setSysndata(true);
        }
        AppApplication.getInstance().setUserBean(currentUserBean);
        AppApplication.getInstance().setDatabase();
        IntentUtil.goToActivityAndFinish(this, MainActivity.class);
    }

    public void onBackPressed() {
        UserBean currentUserBean = SPHelper.getCurrentUserBean();
        SharePreferenceUtils.putString(Constants.USER_TOKEN, "");
        SharePreferenceUtils.putString(Constants.IS_TO_LOGIN, "2");
        AppApplication.getInstance().setUserBean(currentUserBean);
        AppApplication.getInstance().setDatabase();
        super.onBackPressed();
    }

    /* access modifiers changed from: package-private */
    public void loginByWechat() {
        if (!ButtonUtils.isFastDoubleClick(R.id.tvLoginWechat, 1000)) {
            showWebViewDialog(1);
        }
    }

    private boolean isWeixinAvilible() {
        List<PackageInfo> installedPackages = getPackageManager().getInstalledPackages(0);
        if (installedPackages != null) {
            for (int i = 0; i < installedPackages.size(); i++) {
                if (installedPackages.get(i).packageName.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void loginByFacebook() {
        if (!ButtonUtils.isFastDoubleClick(R.id.tvLoginFacebook, 1000)) {
            showWebViewDialog(2);
        }
    }

    private void loginWechar() {
        if (isWeixinAvilible()) {
            Platform platform = ShareSDK.getPlatform(Wechat.NAME);
            ShareSDK.removeCookieOnAuthorize(true);
            platform.removeAccount(true);
            LogUtil.d("platform.isAuthValid() --------------------" + platform.isAuthValid());
            platform.setPlatformActionListener(new PlatformActionListener() {
                /* class com.wade.fit.ui.main.activity.LoginActivity.C15353 */

                public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                    LogUtil.d("onComplete --------------------" + hashMap);
                    Message message = new Message();
                    message.what = 1;
                    message.obj = platform;
                    UIHandler.sendMessage(message, LoginActivity.this);
                }

                public void onError(Platform platform, int i, Throwable th) {
                    LoginActivity.this.hideLoading();
                    LogUtil.d("onError  --------------------onError" + th.getMessage());
                }

                public void onCancel(Platform platform, int i) {
                    LoginActivity.this.hideLoading();
                    LogUtil.d("onCancel --------------------onCancel");
                }
            });
            platform.showUser(null);
            return;
        }
        showToast(getResources().getString(R.string.share_error_wechat));
    }

    private void loginFaceBook() {
        Platform platform = ShareSDK.getPlatform(Facebook.NAME);
        ShareSDK.removeCookieOnAuthorize(true);
        platform.removeAccount(true);
        if (platform.isClientValid()) {
            LogUtil.d("platform.isAuthValid() --------------------" + platform.isAuthValid());
            platform.setPlatformActionListener(new PlatformActionListener() {
                /* class com.wade.fit.ui.main.activity.LoginActivity.C15364 */

                public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                    LogUtil.d("onComplete --------------------" + hashMap);
                    Message message = new Message();
                    message.what = 2;
                    message.obj = platform;
                    UIHandler.sendMessage(message, LoginActivity.this);
                }

                public void onError(Platform platform, int i, Throwable th) {
                    LogUtil.d("onError  --------------------onError" + th.getMessage());
                }

                public void onCancel(Platform platform, int i) {
                    LoginActivity.this.hideLoading();
                    LogUtil.d("onCancel --------------------onCancel");
                }
            });
            platform.showUser(null);
            return;
        }
        showToast(getResources().getString(R.string.share_error_facebook));
    }

    private void showWebViewDialog(int i) {
        Locale locale;
        String str;
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.activity_web_view, (ViewGroup) null);
        final Dialog dialog = new Dialog(this, R.style.theme_dialog_aphe);
        WebView webView = (WebView) inflate.findViewById(R.id.wv_privacy_policy);
        inflate.findViewById(R.id.btnAgree).setOnClickListener(new View.OnClickListener(dialog, i) {
            /* class com.wade.fit.ui.main.activity.$$Lambda$LoginActivity$KVUEnq7C8d4OnwltAfTGpMo4D6w */
            private final /* synthetic */ Dialog f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                LoginActivity.this.lambda$showWebViewDialog$2$LoginActivity(this.f$1, this.f$2, view);
            }
        });
        inflate.findViewById(R.id.btnCancal).setOnClickListener(new View.OnClickListener(dialog) {
            /* class com.wade.fit.ui.main.activity.$$Lambda$LoginActivity$tjyqxIjbZQytZ_xpk3A2Je5c28U */
            private final /* synthetic */ Dialog f$0;

            {
                this.f$0 = r1;
            }

            public final void onClick(View view) {
                this.f$0.dismiss();
            }
        });
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            /* class com.wade.fit.ui.main.activity.LoginActivity.C15375 */

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                return true;
            }

            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                AlertDialog.Builder builder = new AlertDialog.Builder(webView.getContext());
                builder.setMessage("SSL认证失败，是否继续访问？");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener(sslErrorHandler) {
                    /* class com.wade.fit.ui.main.activity.$$Lambda$LoginActivity$5$PAcW3aIfCZlyFy2qB03hRCTO8 */
                    private final /* synthetic */ SslErrorHandler f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void onClick(DialogInterface dialogInterface, int i) {
                        this.f$0.proceed();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener(sslErrorHandler) {
                    /* class com.wade.fit.ui.main.activity.$$Lambda$LoginActivity$5$Kvrb2U1UTLa1Wcjjo1xEaqFEs */
                    private final /* synthetic */ SslErrorHandler f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void onClick(DialogInterface dialogInterface, int i) {
                        this.f$0.cancel();
                    }
                });
                builder.create().show();
            }

            public void onPageFinished(WebView webView, String str) {
                dialog.show();
            }
        });
        if (Build.VERSION.SDK_INT >= 24) {
            locale = getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = getResources().getConfiguration().locale;
        }
        String language = locale.getLanguage();
        if (language.contains("zh")) {
            str = locale.getCountry().equals("TW") ? "file:///android_asset/privacypolicy_tw.html" : "file:///android_asset/privacypolicy.html";
        } else if (language.contains("fr")) {
            str = "file:///android_asset/privacypolicy_fr.html";
        } else if (language.contains("es")) {
            str = "file:///android_asset/privacypolicy_es.html";
        } else if (language.contains("de")) {
            str = "file:///android_asset/privacypolicy_de.html";
        } else if (language.contains("ja")) {
            str = "file:///android_asset/privacypolicy_ja.html";
        } else {
            str = language.contains("it") ? "file:///android_asset/privacypolicy_it.html" : "file:///android_asset/privacypolicyEh.html";
        }
        webView.loadUrl(str);
        dialog.setContentView(inflate);
        dialog.setCancelable(true);
        dialog.show();
    }

    public /* synthetic */ void lambda$showWebViewDialog$2$LoginActivity(Dialog dialog, int i, View view) {
        dialog.dismiss();
        if (i == 1) {
            loginWechar();
        } else if (i == 2) {
            loginFaceBook();
        }
    }

    public void LoginSuccess(int i, BaseBean baseBean) {
        hideLoading();
        UserBean userBean = (UserBean) baseBean.getData();
        SharePreferenceUtils.putInt(this, Constants.LOGIN_TYPE, this.loginType);
        SharePreferenceUtils.putString(Constants.USER_NAME, this.userName);
        SharePreferenceUtils.putString(Constants.USER_PASSWORD, this.userPwd);
        SharePreferenceUtils.putString(Constants.USER_TOKEN, userBean.getToken());
        if (!userBean.isFirstLogin()) {
            if (userBean.getBirthday() == null || TextUtils.isEmpty(userBean.getBirthday())) {
                userBean.setYear(DateUtil.todayYearMonthDay()[0] - 20);
                userBean.setMonth(1);
                userBean.setDay(1);
                userBean.setBirthday(userBean.getYear() + "-" + userBean.getMonth() + "-" + userBean.getDay());
            } else {
                String[] split = userBean.getBirthday().split("-");
                userBean.setYear(Integer.parseInt(split[0]));
                userBean.setMonth(Integer.parseInt(split[1]));
                userBean.setDay(Integer.parseInt(split[2]));
            }
            userBean.setWeightLb(UnitUtil.kg2lb((float) userBean.getWeight()));
            userBean.setHeightLb(UnitUtil.cm2inchs(userBean.getHeight()));
            SharePreferenceUtils.put(AppApplication.getInstance(), Constants.LAST_SYNCH_TIME, 0L);
            if (BleSdkWrapper.isConnected()) {
                AppApplication.getInstance().setSysndata(true);
            }
            AppApplication.getInstance().setUserBean(userBean);
            SPHelper.saveUserBean(userBean);
            AppApplication.getInstance().setDatabase();
            IntentUtil.goToActivityAndFinish(this, MainActivity.class);
            return;
        }
        userBean.setYear(DateUtil.todayYearMonthDay()[0] - 20);
        userBean.setMonth(1);
        userBean.setDay(1);
        userBean.setBirthday(userBean.getYear() + "-" + userBean.getMonth() + "-" + userBean.getDay());
        userBean.setUnit(0);
        userBean.setHeight(Alarm.STATUS_NOT_DISPLAY);
        userBean.setHeightLb(67);
        userBean.setWeight(Constants.DEFAULT_WEIGHT_KG);
        userBean.setWeightLb(132);
        userBean.setWeightSt(10);
        userBean.setStepDistance(75);
        AppApplication.getInstance().setUserBean(userBean);
        SPHelper.saveUserBean(userBean);
        AppApplication.getInstance().setDatabase();
        IntentUtil.goToActivityAndFinish(this, PersonUnitActivity.class);
    }

    public void loginFaild(int i) {
        if (i == 6 || i == 4) {
            showRemindDialog();
        }
    }

    private void showRemindDialog() {
        DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.click_register), getResources().getString(R.string.click_register_tips), getResources().getString(R.string.user_register), new View.OnClickListener() {
            /* class com.wade.fit.ui.main.activity.$$Lambda$LoginActivity$VFwoIKh2vUU9pQdqoMOr0icpmcY */

            public final void onClick(View view) {
                LoginActivity.this.lambda$showRemindDialog$4$LoginActivity(view);
            }
        }, $$Lambda$LoginActivity$0Um8NuyNiZ0pE_ni8VCfot8AldM.INSTANCE);
    }

    public /* synthetic */ void lambda$showRemindDialog$4$LoginActivity(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("email", this.etUsername.getText().toString());
        IntentUtil.goToActivity(this, RegisterActivity.class, bundle);
    }

    public boolean handleMessage(Message message) {
        Platform platform = (Platform) message.obj;
        int i = message.what;
        if (i == 1) {
            this.userName = platform.getDb().getUserId();
            this.userPwd = platform.getDb().getToken();
            this.loginType = 2;
            if (this.isFront) {
                showLoadingFalse();
            }
            ((LoginPresenter) this.mPresenter).requestLoginByWechat(this.userName, this.userPwd);
            return false;
        } else if (i != 2) {
            return false;
        } else {
            this.userName = platform.getDb().getUserId();
            this.userPwd = platform.getDb().getToken();
            this.loginType = 3;
            if (this.isFront) {
                showLoadingFalse();
            }
            ((LoginPresenter) this.mPresenter).loginByFacebook(this.userName, this.userPwd);
            return false;
        }
    }

    public void onResume() {
        super.onResume();
        this.isFront = true;
    }

    public void onPause() {
        super.onPause();
        this.isFront = false;
    }
}
