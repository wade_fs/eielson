package com.wade.fit.util.file;

import android.hardware.Camera;
import java.util.Comparator;
import java.util.List;

public class CamParaUtil {
    private static CamParaUtil myCamPara;

    private CamParaUtil() {
    }

    public static CamParaUtil getInstance() {
        CamParaUtil camParaUtil = myCamPara;
        if (camParaUtil != null) {
            return camParaUtil;
        }
        myCamPara = new CamParaUtil();
        return myCamPara;
    }

    public class CameraSizeComparator implements Comparator<Camera.Size> {
        public CameraSizeComparator() {
        }

        public int compare(Camera.Size size, Camera.Size size2) {
            if (size.width == size2.width) {
                return 0;
            }
            return size.width > size2.width ? 1 : -1;
        }
    }

    public void printSupportPictureSize(Camera.Parameters parameters) {
        List<Camera.Size> supportedPictureSizes = parameters.getSupportedPictureSizes();
        for (int i = 0; i < supportedPictureSizes.size(); i++) {
            Camera.Size size = supportedPictureSizes.get(i);
        }
    }

    public void printSupportFocusMode(Camera.Parameters parameters) {
        for (String str : parameters.getSupportedFocusModes()) {
        }
    }
}
