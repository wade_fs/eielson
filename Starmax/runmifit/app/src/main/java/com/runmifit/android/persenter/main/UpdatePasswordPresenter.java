package com.wade.fit.persenter.main;

import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.http.LoginHttp;
import com.wade.fit.persenter.main.UpdatePasswordContract;
import com.wade.fit.util.Md5Util;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import p041io.reactivex.disposables.Disposable;

public class UpdatePasswordPresenter extends BasePersenter<UpdatePasswordContract.View> implements UpdatePasswordContract.Presenter {
    public void sendCode(String str) {
        ((UpdatePasswordContract.View) this.mView).showLoadingFalse();
        LoginHttp.sendEmailVerifyCode(str, new ApiCallback<BaseBean<String>>() {
            /* class com.wade.fit.persenter.main.UpdatePasswordPresenter.C24201 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<String> baseBean) {
                ((UpdatePasswordContract.View) UpdatePasswordPresenter.this.mView).hideLoading();
                ((UpdatePasswordContract.View) UpdatePasswordPresenter.this.mView).sendCodeSuccess();
            }

            public void onFailure(int i, String str) {
                ((UpdatePasswordContract.View) UpdatePasswordPresenter.this.mView).hideLoading();
                ((UpdatePasswordContract.View) UpdatePasswordPresenter.this.mView).sendCodeFaild(i);
            }

            public void onSubscribe(Disposable disposable) {
                UpdatePasswordPresenter.this.addDisposable(disposable);
            }
        });
    }

    public void updatePassword(String str, String str2, String str3) {
        ((UpdatePasswordContract.View) this.mView).showLoadingFalse();
        LoginHttp.updatePassword(RequestBody.create(MediaType.parse("application/json"), "{\"email\":\"" + str + "\",\"password\":\"" + Md5Util.stringToMD5(str2) + "\",\"verifyCode\":\"" + str3 + "\"}"), new ApiCallback<BaseBean<String>>() {
            /* class com.wade.fit.persenter.main.UpdatePasswordPresenter.C24212 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<String> baseBean) {
                ((UpdatePasswordContract.View) UpdatePasswordPresenter.this.mView).hideLoading();
                ((UpdatePasswordContract.View) UpdatePasswordPresenter.this.mView).updateSuccess();
            }

            public void onFailure(int i, String str) {
                ((UpdatePasswordContract.View) UpdatePasswordPresenter.this.mView).hideLoading();
                ((UpdatePasswordContract.View) UpdatePasswordPresenter.this.mView).updateFaild(i);
            }

            public void onSubscribe(Disposable disposable) {
                UpdatePasswordPresenter.this.addDisposable(disposable);
            }
        });
    }
}
