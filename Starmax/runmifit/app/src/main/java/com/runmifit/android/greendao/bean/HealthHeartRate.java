package com.wade.fit.greendao.bean;

public class HealthHeartRate {
    private int UserMaxHr;
    private int aerobic_mins;
    private int aerobic_threshold;
    private int avgDayHr;
    private int avgHr;
    private int burn_fat_mins;
    private int burn_fat_threshold;
    private long date;
    private int day;
    private boolean isUploaded;
    private int limit_mins;
    private int limit_threshold;
    private String macAddress;
    private int maxHr;
    private int minHr;
    private int month;
    private String remark;
    private int silentHeart;
    private int startTime;
    private String userId;
    private int year;

    public HealthHeartRate() {
    }

    public HealthHeartRate(boolean z, String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15, int i16, long j, String str2, String str3) {
        this.isUploaded = z;
        this.macAddress = str;
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.startTime = i4;
        this.silentHeart = i5;
        this.burn_fat_threshold = i6;
        this.aerobic_threshold = i7;
        this.limit_threshold = i8;
        this.burn_fat_mins = i9;
        this.aerobic_mins = i10;
        this.limit_mins = i11;
        this.UserMaxHr = i12;
        this.avgHr = i13;
        this.avgDayHr = i14;
        this.maxHr = i15;
        this.minHr = i16;
        this.date = j;
        this.userId = str2;
        this.remark = str3;
    }

    public int getAvgDayHr() {
        return this.avgDayHr;
    }

    public void setAvgDayHr(int i) {
        this.avgDayHr = i;
    }

    public boolean getIsUploaded() {
        return this.isUploaded;
    }

    public void setIsUploaded(boolean z) {
        this.isUploaded = z;
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getStartTime() {
        return this.startTime;
    }

    public void setStartTime(int i) {
        this.startTime = i;
    }

    public int getSilentHeart() {
        return this.silentHeart;
    }

    public void setSilentHeart(int i) {
        this.silentHeart = i;
    }

    public int getUserMaxHr() {
        return this.UserMaxHr;
    }

    public void setUserMaxHr(int i) {
        this.UserMaxHr = i;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String str) {
        this.remark = str;
    }

    public int getBurn_fat_threshold() {
        return this.burn_fat_threshold;
    }

    public void setBurn_fat_threshold(int i) {
        this.burn_fat_threshold = i;
    }

    public int getAerobic_threshold() {
        return this.aerobic_threshold;
    }

    public void setAerobic_threshold(int i) {
        this.aerobic_threshold = i;
    }

    public int getLimit_threshold() {
        return this.limit_threshold;
    }

    public void setLimit_threshold(int i) {
        this.limit_threshold = i;
    }

    public int getBurn_fat_mins() {
        return this.burn_fat_mins;
    }

    public void setBurn_fat_mins(int i) {
        this.burn_fat_mins = i;
    }

    public int getAerobic_mins() {
        return this.aerobic_mins;
    }

    public void setAerobic_mins(int i) {
        this.aerobic_mins = i;
    }

    public int getLimit_mins() {
        return this.limit_mins;
    }

    public void setLimit_mins(int i) {
        this.limit_mins = i;
    }

    public String toString() {
        return "HealthHeartRate{, silentHeart=" + this.silentHeart + ", avgHr=" + this.avgHr + ", date=" + this.date + '}';
    }

    public int getAvgHr() {
        return this.avgHr;
    }

    public void setAvgHr(int i) {
        this.avgHr = i;
    }

    public int getMaxHr() {
        return this.maxHr;
    }

    public void setMaxHr(int i) {
        this.maxHr = i;
    }

    public int getMinHr() {
        return this.minHr;
    }

    public void setMinHr(int i) {
        this.minHr = i;
    }
}
