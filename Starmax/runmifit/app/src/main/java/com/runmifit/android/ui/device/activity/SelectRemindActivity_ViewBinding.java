package com.wade.fit.ui.device.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.device.activity.SelectRemindActivity_ViewBinding */
public class SelectRemindActivity_ViewBinding implements Unbinder {
    private SelectRemindActivity target;

    public SelectRemindActivity_ViewBinding(SelectRemindActivity selectRemindActivity) {
        this(selectRemindActivity, selectRemindActivity.getWindow().getDecorView());
    }

    public SelectRemindActivity_ViewBinding(SelectRemindActivity selectRemindActivity, View view) {
        this.target = selectRemindActivity;
        selectRemindActivity.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.mRecyclerView, "field 'mRecyclerView'", RecyclerView.class);
    }

    public void unbind() {
        SelectRemindActivity selectRemindActivity = this.target;
        if (selectRemindActivity != null) {
            this.target = null;
            selectRemindActivity.mRecyclerView = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
