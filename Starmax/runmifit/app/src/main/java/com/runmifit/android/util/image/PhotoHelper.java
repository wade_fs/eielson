package com.wade.fit.util.image;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.view.View;
import com.wade.fit.app.Constant;
import com.wade.fit.util.AsyncTaskUtil;
import com.wade.fit.util.DialogHelperNew;
import com.tamic.novate.util.FileUtil;
import java.io.File;

public class PhotoHelper {
    public static File photoFile = new File(Constant.PIC_PATH + photoPath);
    public static String photoPath = "/avatar.png";
    public static String photoTemp = "/temp.png";
    public static File tempPhoto = new File(Constant.PIC_PATH + photoTemp);
    Activity activity;
    Dialog dialog;
    View.OnClickListener selectPhotoClick = new View.OnClickListener() {
        /* class com.wade.fit.util.image.PhotoHelper.C26941 */

        public void onClick(View view) {
            Intent intent = new Intent("android.intent.action.PICK", (Uri) null);
            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, FileUtil.MIME_TYPE_IMAGE);
            PhotoHelper.this.activity.startActivityForResult(intent, 2);
            PhotoHelper.this.dialog.dismiss();
        }
    };
    View.OnClickListener takePhotoClick = new View.OnClickListener() {
        /* class com.wade.fit.util.image.PhotoHelper.C26952 */

        public void onClick(View view) {
            if (!(ContextCompat.checkSelfPermission(PhotoHelper.this.activity, "android.permission.CAMERA") != 0)) {
                PhotoHelper.this.dialog.dismiss();
                PhotoHelper.this.jumpCaptureActivity();
            } else if (Build.VERSION.SDK_INT >= 23) {
                PhotoHelper.this.activity.requestPermissions(new String[]{"android.permission.CAMERA"}, 100);
            }
        }
    };

    public PhotoHelper(Activity activity2) {
        this.activity = activity2;
    }

    public void showPhotosDaiglog(Activity activity2) {
        if (this.dialog == null) {
            this.dialog = showPhotosDaiglog(activity2, this.selectPhotoClick, this.takePhotoClick);
        }
        this.dialog.show();
    }

    private void startPhotoCrop(Uri uri) {
        Intent intent = new Intent(this.activity, ImageFactoryActivity.class);
        intent.setData(uri);
        this.activity.startActivityForResult(intent, 4);
    }

    public Bitmap onActivityResult(int i, int i2, Intent intent) {
        if (i != 1) {
            if (i != 2) {
                if (i == 4) {
                    Bitmap decodeFile = BitmapFactory.decodeFile(Constant.PIC_PATH + photoPath);
                    File file = tempPhoto;
                    if (file.exists()) {
                        file.delete();
                    }
                    return decodeFile;
                }
            } else if (intent == null) {
                return null;
            } else {
                photoTemp = intent.getData().toString().substring(intent.getData().toString().lastIndexOf("/") + 1);
                startPhotoCrop(intent.getData());
            }
        } else if (tempPhoto.exists()) {
            startPhotoCrop(Uri.fromFile(tempPhoto));
        }
        return null;
    }

    private void saveBitmapInBackground(final Bitmap bitmap) {
        new AsyncTaskUtil(new AsyncTaskUtil.AsyncTaskCallBackAdapter() {
            /* class com.wade.fit.util.image.PhotoHelper.C26963 */

            public Object doInBackground(String... strArr) {
                File file = new File(Constant.PIC_PATH + PhotoHelper.photoPath);
                BitmapUtil.save(bitmap, file.getAbsolutePath());
                if (!file.exists()) {
                    return null;
                }
                File file2 = PhotoHelper.tempPhoto;
                if (!file2.exists()) {
                    return null;
                }
                file2.delete();
                return null;
            }
        }).execute("");
    }

    public void jumpCaptureActivity() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (Build.VERSION.SDK_INT < 24) {
            intent.putExtra("output", Uri.fromFile(tempPhoto));
            this.activity.startActivityForResult(intent, 1);
            return;
        }
        String str = Constant.PIC_PATH;
        tempPhoto = new File(str, System.currentTimeMillis() + "_temp.png.");
        ContentValues contentValues = new ContentValues(1);
        contentValues.put("_data", tempPhoto.getAbsolutePath());
        intent.putExtra("output", this.activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues));
        this.activity.startActivityForResult(intent, 1);
    }

    public static Dialog showPhotosDaiglog(Activity activity2, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        return DialogHelperNew.showPhotosDaiglog(activity2, onClickListener, onClickListener2);
    }

    static class DismissListener implements View.OnClickListener {
        Dialog dialog;

        public DismissListener(Dialog dialog2) {
            this.dialog = dialog2;
        }

        public void onClick(View view) {
            Dialog dialog2 = this.dialog;
            if (dialog2 != null) {
                dialog2.dismiss();
            }
        }
    }
}
