package com.wade.fit.util.baidumap;

public class StepCount implements StepCountListener {
    private int count = 0;
    private int mCount = 0;
    private StepValuePassListener mStepValuePassListener;
    private StepDetector stepDetector = new StepDetector();
    private long timeOfLastPeak = 0;
    private long timeOfThisPeak = 0;

    public StepCount() {
        this.stepDetector.initListener(this);
    }

    public StepDetector getStepDetector() {
        return this.stepDetector;
    }

    public void countStep() {
        this.timeOfLastPeak = this.timeOfThisPeak;
        this.timeOfThisPeak = System.currentTimeMillis();
        if (this.timeOfThisPeak - this.timeOfLastPeak <= 3000) {
            int i = this.count;
            if (i < 9) {
                this.count = i + 1;
            } else if (i == 9) {
                this.count = i + 1;
                this.mCount += this.count;
                notifyListener();
            } else {
                this.mCount++;
                notifyListener();
            }
        } else {
            this.count = 1;
        }
    }

    public void initListener(StepValuePassListener stepValuePassListener) {
        this.mStepValuePassListener = stepValuePassListener;
    }

    public void notifyListener() {
        StepValuePassListener stepValuePassListener = this.mStepValuePassListener;
        if (stepValuePassListener != null) {
            stepValuePassListener.stepChanged(this.mCount);
        }
    }

    public void setSteps(int i) {
        this.mCount = i;
        this.count = 0;
        this.timeOfLastPeak = 0;
        this.timeOfThisPeak = 0;
        notifyListener();
    }
}
