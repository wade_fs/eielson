package com.wade.fit.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.PermissionUtil;
import com.wade.fit.util.ToastUtil;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class BaseFragment extends Fragment implements PermissionUtil.RequsetResult {
    protected final String TAG = getClass().getSimpleName();
    protected FragmentActivity activity;
    protected boolean isHidden = false;
    protected Bundle mBundle;
    protected Context mContext;
    protected LayoutInflater mInflater;
    protected View mRootView;
    private PermissionUtil permissionUtil;
    Unbinder unbinder;

    /* access modifiers changed from: protected */
    public abstract int getLayoutId();

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
    }

    /* access modifiers changed from: protected */
    public void initBundle(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void initView() {
    }

    /* access modifiers changed from: protected */
    public void onVisiable() {
    }

    public void oninVisiable() {
    }

    public void requestPermissionsFail(int i) {
    }

    public void requestPermissionsSuccess(int i) {
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.activity = getActivity();
    }

    public void onHiddenChanged(boolean z) {
        super.onHiddenChanged(z);
        this.isHidden = z;
        if (!z) {
            onVisiable();
        } else {
            oninVisiable();
        }
    }

    public void onStop() {
        super.onStop();
        oninVisiable();
    }

    public void onResume() {
        super.onResume();
        if (!this.isHidden) {
            onVisiable();
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mContext = null;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mBundle = getArguments();
        initBundle(this.mBundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.mRootView = layoutInflater.inflate(getLayoutId(), viewGroup, false);
        this.mInflater = layoutInflater;
        this.unbinder = ButterKnife.bind(this, this.mRootView);
        EventBusHelper.register(this);
        this.permissionUtil = new PermissionUtil();
        this.permissionUtil.setRequsetResult(this);
        initView();
        initData();
        return this.mRootView;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public final void handleMessageInner(BaseMessage baseMessage) {
        handleMessage(baseMessage);
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
        Unbinder unbinder2 = this.unbinder;
        if (unbinder2 != null) {
            unbinder2.unbind();
        }
        EventBusHelper.unregister(this);
    }

    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void showToast(String str) {
        getActivity().runOnUiThread(new Runnable(str) {
            /* class com.wade.fit.base.$$Lambda$BaseFragment$A4ASMMEWgMvrxGP8K6_qJibmceA */
            private final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            public final void run() {
                ToastUtil.showToast(this.f$0);
            }
        });
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        this.permissionUtil.onRequestPermissionsResult(i, strArr, iArr);
    }
}
