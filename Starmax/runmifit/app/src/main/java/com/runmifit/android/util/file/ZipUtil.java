package com.wade.fit.util.file;

import com.wade.fit.app.Constant;
import com.wade.fit.util.StringUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ZipUtil implements Constant {
    private ZipUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static boolean zipFiles(Collection<File> collection, String str) throws IOException {
        return zipFiles(collection, str, (String) null);
    }

    public static boolean zipFiles(Collection<File> collection, String str, String str2) throws IOException {
        return zipFiles(collection, FileUtil.getFileByPath(str), str2);
    }

    public static boolean zipFiles(Collection<File> collection, File file) throws IOException {
        return zipFiles(collection, file, (String) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean zipFiles(java.util.Collection<java.io.File> r5, java.io.File r6, java.lang.String r7) throws java.io.IOException {
        /*
            r0 = 0
            if (r5 == 0) goto L_0x0051
            if (r6 != 0) goto L_0x0006
            goto L_0x0051
        L_0x0006:
            r1 = 0
            r2 = 1
            java.util.zip.ZipOutputStream r3 = new java.util.zip.ZipOutputStream     // Catch:{ all -> 0x0042 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ all -> 0x0042 }
            r4.<init>(r6)     // Catch:{ all -> 0x0042 }
            r3.<init>(r4)     // Catch:{ all -> 0x0042 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ all -> 0x0040 }
        L_0x0016:
            boolean r6 = r5.hasNext()     // Catch:{ all -> 0x0040 }
            if (r6 == 0) goto L_0x0035
            java.lang.Object r6 = r5.next()     // Catch:{ all -> 0x0040 }
            java.io.File r6 = (java.io.File) r6     // Catch:{ all -> 0x0040 }
            java.lang.String r1 = ""
            boolean r6 = zipFile(r6, r1, r3, r7)     // Catch:{ all -> 0x0040 }
            if (r6 != 0) goto L_0x0016
            r3.finish()
            java.io.Closeable[] r5 = new java.io.Closeable[r2]
            r5[r0] = r3
            com.wade.fit.util.file.FileUtil.closeIO(r5)
            return r0
        L_0x0035:
            r3.finish()
            java.io.Closeable[] r5 = new java.io.Closeable[r2]
            r5[r0] = r3
            com.wade.fit.util.file.FileUtil.closeIO(r5)
            return r2
        L_0x0040:
            r5 = move-exception
            goto L_0x0044
        L_0x0042:
            r5 = move-exception
            r3 = r1
        L_0x0044:
            if (r3 == 0) goto L_0x0050
            r3.finish()
            java.io.Closeable[] r6 = new java.io.Closeable[r2]
            r6[r0] = r3
            com.wade.fit.util.file.FileUtil.closeIO(r6)
        L_0x0050:
            throw r5
        L_0x0051:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.file.ZipUtil.zipFiles(java.util.Collection, java.io.File, java.lang.String):boolean");
    }

    public static boolean zipFile(String str, String str2) throws IOException {
        return zipFile(str, str2, (String) null);
    }

    public static boolean zipFile(String str, String str2, String str3) throws IOException {
        return zipFile(FileUtil.getFileByPath(str), FileUtil.getFileByPath(str2), str3);
    }

    public static boolean zipFile(File file, File file2) throws IOException {
        return zipFile(file, file2, (String) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean zipFile(java.io.File r5, java.io.File r6, java.lang.String r7) throws java.io.IOException {
        /*
            r0 = 0
            if (r5 == 0) goto L_0x0034
            if (r6 != 0) goto L_0x0006
            goto L_0x0034
        L_0x0006:
            r1 = 0
            r2 = 1
            java.util.zip.ZipOutputStream r3 = new java.util.zip.ZipOutputStream     // Catch:{ all -> 0x0026 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ all -> 0x0026 }
            r4.<init>(r6)     // Catch:{ all -> 0x0026 }
            r3.<init>(r4)     // Catch:{ all -> 0x0026 }
            java.lang.String r6 = ""
            boolean r5 = zipFile(r5, r6, r3, r7)     // Catch:{ all -> 0x0023 }
            r3.finish()
            java.io.Closeable[] r6 = new java.io.Closeable[r2]
            r6[r0] = r3
            com.wade.fit.util.file.FileUtil.closeIO(r6)
            return r5
        L_0x0023:
            r5 = move-exception
            r1 = r3
            goto L_0x0027
        L_0x0026:
            r5 = move-exception
        L_0x0027:
            if (r1 == 0) goto L_0x0033
            r1.finish()
            java.io.Closeable[] r6 = new java.io.Closeable[r2]
            r6[r0] = r1
            com.wade.fit.util.file.FileUtil.closeIO(r6)
        L_0x0033:
            throw r5
        L_0x0034:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.file.ZipUtil.zipFile(java.io.File, java.io.File, java.lang.String):boolean");
    }

    private static boolean zipFile(File file, String str, ZipOutputStream zipOutputStream, String str2) throws IOException {
        BufferedInputStream bufferedInputStream;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(StringUtils.isSpace(str) ? "" : File.separator);
        sb.append(file.getName());
        String sb2 = sb.toString();
        if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles.length <= 0) {
                ZipEntry zipEntry = new ZipEntry(sb2 + '/');
                if (!StringUtils.isEmpty(str2)) {
                    zipEntry.setComment(str2);
                }
                zipOutputStream.putNextEntry(zipEntry);
                zipOutputStream.closeEntry();
            } else {
                for (File file2 : listFiles) {
                    if (!zipFile(file2, sb2, zipOutputStream, str2)) {
                        return false;
                    }
                }
            }
        } else {
            try {
                bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                try {
                    ZipEntry zipEntry2 = new ZipEntry(sb2);
                    if (!StringUtils.isEmpty(str2)) {
                        zipEntry2.setComment(str2);
                    }
                    zipOutputStream.putNextEntry(zipEntry2);
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = bufferedInputStream.read(bArr, 0, 1024);
                        if (read == -1) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, read);
                    }
                    zipOutputStream.closeEntry();
                    FileUtil.closeIO(bufferedInputStream);
                } catch (Throwable th) {
                    th = th;
                    FileUtil.closeIO(bufferedInputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedInputStream = null;
                FileUtil.closeIO(bufferedInputStream);
                throw th;
            }
        }
        return true;
    }

    public static boolean unzipFiles(Collection<File> collection, String str) throws IOException {
        return unzipFiles(collection, FileUtil.getFileByPath(str));
    }

    public static boolean unzipFiles(Collection<File> collection, File file) throws IOException {
        if (collection == null || file == null) {
            return false;
        }
        for (File file2 : collection) {
            if (!unzipFile(file2, file)) {
                return false;
            }
        }
        return true;
    }

    public static boolean unzipFile(String str, String str2) throws IOException {
        return unzipFile(FileUtil.getFileByPath(str), FileUtil.getFileByPath(str2));
    }

    public static boolean unzipFile(File file, File file2) throws IOException {
        return unzipFileByKeyword(file, file2, null) != null;
    }

    public static List<File> unzipFileByKeyword(String str, String str2, String str3) throws IOException {
        return unzipFileByKeyword(FileUtil.getFileByPath(str), FileUtil.getFileByPath(str2), str3);
    }

    public static List<File> unzipFileByKeyword(File file, File file2, String str) throws IOException {
        BufferedInputStream bufferedInputStream;
        BufferedOutputStream bufferedOutputStream = null;
        if (file == null || file2 == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        ZipFile zipFile = new ZipFile(file);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            String name = zipEntry.getName();
            if (StringUtils.isEmpty(str) || FileUtil.getFileName(name).toLowerCase().contains(str.toLowerCase())) {
                File file3 = new File(file2 + File.separator + name);
                arrayList.add(file3);
                if (zipEntry.isDirectory()) {
                    if (!FileUtil.createOrExistsDir(file3)) {
                        return null;
                    }
                } else if (!FileUtil.createOrExistsFile(file3)) {
                    return null;
                } else {
                    try {
                        bufferedInputStream = new BufferedInputStream(zipFile.getInputStream(zipEntry));
                        try {
                            BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(new FileOutputStream(file3));
                            try {
                                byte[] bArr = new byte[1024];
                                while (true) {
                                    int read = bufferedInputStream.read(bArr);
                                    if (read == -1) {
                                        break;
                                    }
                                    bufferedOutputStream2.write(bArr, 0, read);
                                }
                                FileUtil.closeIO(bufferedInputStream, bufferedOutputStream2);
                            } catch (Throwable th) {
                                th = th;
                                bufferedOutputStream = bufferedOutputStream2;
                                FileUtil.closeIO(bufferedInputStream, bufferedOutputStream);
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            FileUtil.closeIO(bufferedInputStream, bufferedOutputStream);
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        bufferedInputStream = null;
                        FileUtil.closeIO(bufferedInputStream, bufferedOutputStream);
                        throw th;
                    }
                }
            }
        }
        return arrayList;
    }

    public static List<String> getFilesPath(String str) throws IOException {
        return getFilesPath(FileUtil.getFileByPath(str));
    }

    public static List<String> getFilesPath(File file) throws IOException {
        if (file == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Enumeration<?> entries = getEntries(file);
        while (entries.hasMoreElements()) {
            arrayList.add(((ZipEntry) entries.nextElement()).getName());
        }
        return arrayList;
    }

    public static List<String> getComments(String str) throws IOException {
        return getComments(FileUtil.getFileByPath(str));
    }

    public static List<String> getComments(File file) throws IOException {
        if (file == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Enumeration<?> entries = getEntries(file);
        while (entries.hasMoreElements()) {
            arrayList.add(((ZipEntry) entries.nextElement()).getComment());
        }
        return arrayList;
    }

    public static Enumeration<?> getEntries(String str) throws IOException {
        return getEntries(FileUtil.getFileByPath(str));
    }

    public static Enumeration<?> getEntries(File file) throws IOException {
        if (file == null) {
            return null;
        }
        return new ZipFile(file).entries();
    }
}
