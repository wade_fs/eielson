package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthSleepItem;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class HealthSleepItemDao extends AbstractDao<HealthSleepItem, Void> {
    public static final String TABLENAME = "HEALTH_SLEEP_ITEM";

    public static class Properties {
        public static final Property Date = new Property(7, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(2, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");
        public static final Property Index = new Property(5, Integer.TYPE, Config.FEED_LIST_ITEM_INDEX, false, "INDEX");
        public static final Property Month = new Property(1, Integer.TYPE, "month", false, "MONTH");
        public static final Property OffsetMinute = new Property(3, Integer.TYPE, "offsetMinute", false, "OFFSET_MINUTE");
        public static final Property Remark = new Property(9, String.class, "remark", false, "REMARK");
        public static final Property SleepStatus = new Property(6, Integer.TYPE, "sleepStatus", false, "SLEEP_STATUS");
        public static final Property Sleeptime = new Property(4, String.class, "sleeptime", false, "SLEEPTIME");
        public static final Property UserId = new Property(8, String.class, Constants.userId, false, "USER_ID");
        public static final Property Year = new Property(0, Integer.TYPE, "year", false, "YEAR");
    }

    public Void getKey(HealthSleepItem healthSleepItem) {
        return null;
    }

    public boolean hasKey(HealthSleepItem healthSleepItem) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(HealthSleepItem healthSleepItem, long j) {
        return null;
    }

    public HealthSleepItemDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public HealthSleepItemDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"HEALTH_SLEEP_ITEM\" (\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"OFFSET_MINUTE\" INTEGER NOT NULL ,\"SLEEPTIME\" TEXT,\"INDEX\" INTEGER NOT NULL ,\"SLEEP_STATUS\" INTEGER NOT NULL ,\"DATE\" INTEGER NOT NULL ,\"USER_ID\" TEXT,\"REMARK\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"HEALTH_SLEEP_ITEM\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, HealthSleepItem healthSleepItem) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, (long) healthSleepItem.getYear());
        databaseStatement.bindLong(2, (long) healthSleepItem.getMonth());
        databaseStatement.bindLong(3, (long) healthSleepItem.getDay());
        databaseStatement.bindLong(4, (long) healthSleepItem.getOffsetMinute());
        String sleeptime = healthSleepItem.getSleeptime();
        if (sleeptime != null) {
            databaseStatement.bindString(5, sleeptime);
        }
        databaseStatement.bindLong(6, (long) healthSleepItem.getIndex());
        databaseStatement.bindLong(7, (long) healthSleepItem.getSleepStatus());
        databaseStatement.bindLong(8, healthSleepItem.getDate());
        String userId = healthSleepItem.getUserId();
        if (userId != null) {
            databaseStatement.bindString(9, userId);
        }
        String remark = healthSleepItem.getRemark();
        if (remark != null) {
            databaseStatement.bindString(10, remark);
        }
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, HealthSleepItem healthSleepItem) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, (long) healthSleepItem.getYear());
        sQLiteStatement.bindLong(2, (long) healthSleepItem.getMonth());
        sQLiteStatement.bindLong(3, (long) healthSleepItem.getDay());
        sQLiteStatement.bindLong(4, (long) healthSleepItem.getOffsetMinute());
        String sleeptime = healthSleepItem.getSleeptime();
        if (sleeptime != null) {
            sQLiteStatement.bindString(5, sleeptime);
        }
        sQLiteStatement.bindLong(6, (long) healthSleepItem.getIndex());
        sQLiteStatement.bindLong(7, (long) healthSleepItem.getSleepStatus());
        sQLiteStatement.bindLong(8, healthSleepItem.getDate());
        String userId = healthSleepItem.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(9, userId);
        }
        String remark = healthSleepItem.getRemark();
        if (remark != null) {
            sQLiteStatement.bindString(10, remark);
        }
    }

    public HealthSleepItem readEntity(Cursor cursor, int i) {
        int i2 = cursor.getInt(i + 0);
        int i3 = cursor.getInt(i + 1);
        int i4 = cursor.getInt(i + 2);
        int i5 = cursor.getInt(i + 3);
        int i6 = i + 4;
        String string = cursor.isNull(i6) ? null : cursor.getString(i6);
        int i7 = cursor.getInt(i + 5);
        int i8 = cursor.getInt(i + 6);
        long j = cursor.getLong(i + 7);
        int i9 = i + 8;
        int i10 = i + 9;
        return new HealthSleepItem(i2, i3, i4, i5, string, i7, i8, j, cursor.isNull(i9) ? null : cursor.getString(i9), cursor.isNull(i10) ? null : cursor.getString(i10));
    }

    public void readEntity(Cursor cursor, HealthSleepItem healthSleepItem, int i) {
        healthSleepItem.setYear(cursor.getInt(i + 0));
        healthSleepItem.setMonth(cursor.getInt(i + 1));
        healthSleepItem.setDay(cursor.getInt(i + 2));
        healthSleepItem.setOffsetMinute(cursor.getInt(i + 3));
        int i2 = i + 4;
        String str = null;
        healthSleepItem.setSleeptime(cursor.isNull(i2) ? null : cursor.getString(i2));
        healthSleepItem.setIndex(cursor.getInt(i + 5));
        healthSleepItem.setSleepStatus(cursor.getInt(i + 6));
        healthSleepItem.setDate(cursor.getLong(i + 7));
        int i3 = i + 8;
        healthSleepItem.setUserId(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 9;
        if (!cursor.isNull(i4)) {
            str = cursor.getString(i4);
        }
        healthSleepItem.setRemark(str);
    }
}
