package com.wade.fit.base;

import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.model.bean.LatLngBean;
import com.wade.fit.model.bean.SportRunningBean;
import com.wade.fit.persenter.sport.DeviceSportRunPresenter;
import com.wade.fit.persenter.sport.ISportRunView;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.StringUtils;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ble.BleSdkWrapper;
import java.text.DecimalFormat;

public class BaseSportPresenter extends BasePersenter<ISportRunView> implements DeviceSportRunPresenter.ISportRunCallBack, DeviceSportRunPresenter.ISportConnectCallBack, DeviceSportRunPresenter.ISportStartCallBack {
    public static final int SPORT_START_FAILD = 2;
    public static final int SPORT_START_FAILD_LOW_POWER = 1;
    public static final int SPORT_START_SUCCESS = 0;
    DecimalFormat decimalFormat = new DecimalFormat("0.00");
    protected DeviceSportRunPresenter deviceSportRunPresenter = DeviceSportRunPresenter.getDefault();
    public boolean isRunning = false;
    private boolean isSaveData;
    private boolean isTarget = false;
    private float kmDistance;
    private float mileDistance;
    HealthActivity proHealthActivity = new HealthActivity();
    SportRunningBean sportRunningBean = new SportRunningBean();
    private int sportType;
    private int targetType;
    private float targetValue;

    private void initData() {
    }

    private void setTargetPrecent() {
    }

    public void close() {
    }

    public void setLocation(boolean z) {
    }

    public void startMuilSport(int i) {
    }

    public int getDuration() {
        return this.proHealthActivity.getDurations();
    }

    public void detachView() {
        super.detachView();
    }

    public void attachView(ISportRunView iSportRunView) {
        super.attachView((IBaseView) iSportRunView);
        this.deviceSportRunPresenter.setSportRunCallBack(this);
        initData();
    }

    public long getStartTime() {
        return this.proHealthActivity.getDate();
    }

    public void startRun(int i, int i2, int i3, boolean z) {
        this.sportType = i;
        this.deviceSportRunPresenter.startRun(i, i2, i3, z, this);
        if (BleSdkWrapper.isConnected()) {
            BleSdkWrapper.setHeartTest(true, null);
        }
    }

    public void startSportByApp() {
        this.deviceSportRunPresenter.sportStart();
    }

    public void forceStartRun() {
        this.deviceSportRunPresenter.forceStartRun();
    }

    public void continueOnPauseRun() {
        this.deviceSportRunPresenter.continueOnPauseRun();
    }

    public void startTime() {
        this.deviceSportRunPresenter.setSendCmd(false);
        this.deviceSportRunPresenter.startUpdateTimer(false);
    }

    public void pauseRun() {
        this.deviceSportRunPresenter.pauserRun();
    }

    public boolean isSaveData() {
        return this.isSaveData;
    }

    public void stopRun(boolean z) {
        this.isSaveData = z;
        this.deviceSportRunPresenter.stopRun(z);
    }

    private void stopRunNow() {
        this.isRunning = false;
        ((ISportRunView) this.mView).stopRun(true);
    }

    public void sportPause(boolean z) {
        if (z) {
            this.isRunning = false;
        }
        ((ISportRunView) this.mView).pauseRun(z);
    }

    public void sportResume(boolean z) {
        if (z) {
            this.isRunning = true;
        }
        ((ISportRunView) this.mView).continueRun(z);
    }

    public void sportStop(boolean z) {
        if (z) {
            stopRunNow();
        } else {
            ((ISportRunView) this.mView).stopRun(false);
        }
    }

    public void sportRunning(HealthActivity healthActivity, int i, LatLngBean latLngBean, int i2) {
        String str;
        this.proHealthActivity = healthActivity;
        this.sportRunningBean.duration = DateUtil.computeTimeHMS((long) this.proHealthActivity.getDurations());
        this.kmDistance = ((float) healthActivity.getDistance()) / 1000.0f;
        this.mileDistance = UnitUtil.getKm2mile(this.kmDistance);
        this.sportRunningBean.calorie = String.valueOf(healthActivity.getCalories());
        if (BleSdkWrapper.isDistUnitKm()) {
            this.sportRunningBean.distance = this.decimalFormat.format((double) this.kmDistance);
        } else {
            this.sportRunningBean.distance = this.decimalFormat.format((double) this.mileDistance);
        }
        this.sportRunningBean.heartRate = String.valueOf(i);
        this.sportRunningBean.signalValue = i2;
        if (healthActivity.getDistance() >= 10) {
            str = StringUtils.format("%.2f", Float.valueOf((((float) healthActivity.getDurations()) / 60.0f) / this.kmDistance));
            if (UnitUtil.getMode() == 1) {
                str = StringUtils.format("%.2f", Float.valueOf((((float) healthActivity.getDurations()) / 60.0f) / this.mileDistance));
            }
        } else {
            str = StringUtils.format("%.2f", Float.valueOf(0.0f));
        }
        String computeTimePace = DateUtil.computeTimePace(str);
        SportRunningBean sportRunningBean2 = this.sportRunningBean;
        sportRunningBean2.pace = computeTimePace;
        sportRunningBean2.latLngBean = latLngBean;
        sportRunningBean2.healthActivity = healthActivity;
        setTargetPrecent();
        ((ISportRunView) this.mView).getRunData(this.sportRunningBean);
    }

    public void connecetTimeOut() {
        ((ISportRunView) this.mView).connecetTimeOut();
    }

    public void bleDisconnect() {
        ((ISportRunView) this.mView).bleDisconnect();
    }

    public void bleConnect() {
        ((ISportRunView) this.mView).bleConnect();
    }

    public boolean isCompleteRun() {
        return this.deviceSportRunPresenter.isCompleteRun();
    }

    public void sportStartSuccess() {
        this.isRunning = true;
        ((ISportRunView) this.mView).startRun(0);
    }

    public void sportStartFaildByLowPower() {
        ((ISportRunView) this.mView).startRun(1);
    }

    public void sportStartFaild() {
        ((ISportRunView) this.mView).startRun(2);
    }
}
