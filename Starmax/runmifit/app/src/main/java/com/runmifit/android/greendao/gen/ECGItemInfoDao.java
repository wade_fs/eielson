package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.ECGItemInfo;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class ECGItemInfoDao extends AbstractDao<ECGItemInfo, Void> {
    public static final String TABLENAME = "ECGITEM_INFO";

    public static class Properties {
        public static final Property Date = new Property(3, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(2, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");

        /* renamed from: Hr */
        public static final Property f5217Hr = new Property(7, Float.TYPE, "hr", false, "HR");
        public static final Property Month = new Property(1, Integer.TYPE, "month", false, "MONTH");
        public static final Property Speed = new Property(6, Integer.TYPE, "speed", false, "SPEED");
        public static final Property State = new Property(8, Integer.TYPE, "state", false, "STATE");
        public static final Property UserId = new Property(4, String.class, Constants.userId, false, "USER_ID");
        public static final Property Year = new Property(0, Integer.TYPE, "year", false, "YEAR");
        public static final Property ZengYi = new Property(5, Integer.TYPE, "zengYi", false, "ZENG_YI");
    }

    public Void getKey(ECGItemInfo eCGItemInfo) {
        return null;
    }

    public boolean hasKey(ECGItemInfo eCGItemInfo) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(ECGItemInfo eCGItemInfo, long j) {
        return null;
    }

    public ECGItemInfoDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public ECGItemInfoDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"ECGITEM_INFO\" (\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"DATE\" INTEGER NOT NULL ,\"USER_ID\" TEXT,\"ZENG_YI\" INTEGER NOT NULL ,\"SPEED\" INTEGER NOT NULL ,\"HR\" REAL NOT NULL ,\"STATE\" INTEGER NOT NULL );");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"ECGITEM_INFO\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, ECGItemInfo eCGItemInfo) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, (long) eCGItemInfo.getYear());
        databaseStatement.bindLong(2, (long) eCGItemInfo.getMonth());
        databaseStatement.bindLong(3, (long) eCGItemInfo.getDay());
        databaseStatement.bindLong(4, eCGItemInfo.getDate());
        String userId = eCGItemInfo.getUserId();
        if (userId != null) {
            databaseStatement.bindString(5, userId);
        }
        databaseStatement.bindLong(6, (long) eCGItemInfo.getZengYi());
        databaseStatement.bindLong(7, (long) eCGItemInfo.getSpeed());
        databaseStatement.bindDouble(8, (double) eCGItemInfo.getHr());
        databaseStatement.bindLong(9, (long) eCGItemInfo.getState());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, ECGItemInfo eCGItemInfo) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, (long) eCGItemInfo.getYear());
        sQLiteStatement.bindLong(2, (long) eCGItemInfo.getMonth());
        sQLiteStatement.bindLong(3, (long) eCGItemInfo.getDay());
        sQLiteStatement.bindLong(4, eCGItemInfo.getDate());
        String userId = eCGItemInfo.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(5, userId);
        }
        sQLiteStatement.bindLong(6, (long) eCGItemInfo.getZengYi());
        sQLiteStatement.bindLong(7, (long) eCGItemInfo.getSpeed());
        sQLiteStatement.bindDouble(8, (double) eCGItemInfo.getHr());
        sQLiteStatement.bindLong(9, (long) eCGItemInfo.getState());
    }

    public ECGItemInfo readEntity(Cursor cursor, int i) {
        int i2 = i + 4;
        return new ECGItemInfo(cursor.getInt(i + 0), cursor.getInt(i + 1), cursor.getInt(i + 2), cursor.getLong(i + 3), cursor.isNull(i2) ? null : cursor.getString(i2), cursor.getInt(i + 5), cursor.getInt(i + 6), cursor.getFloat(i + 7), cursor.getInt(i + 8));
    }

    public void readEntity(Cursor cursor, ECGItemInfo eCGItemInfo, int i) {
        eCGItemInfo.setYear(cursor.getInt(i + 0));
        eCGItemInfo.setMonth(cursor.getInt(i + 1));
        eCGItemInfo.setDay(cursor.getInt(i + 2));
        eCGItemInfo.setDate(cursor.getLong(i + 3));
        int i2 = i + 4;
        eCGItemInfo.setUserId(cursor.isNull(i2) ? null : cursor.getString(i2));
        eCGItemInfo.setZengYi(cursor.getInt(i + 5));
        eCGItemInfo.setSpeed(cursor.getInt(i + 6));
        eCGItemInfo.setHr(cursor.getFloat(i + 7));
        eCGItemInfo.setState(cursor.getInt(i + 8));
    }
}
