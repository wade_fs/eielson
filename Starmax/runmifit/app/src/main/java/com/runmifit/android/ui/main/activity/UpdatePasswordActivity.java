package com.wade.fit.ui.main.activity;

import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.persenter.main.UpdatePasswordContract;
import com.wade.fit.persenter.main.UpdatePasswordPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DataVaildUtil;
import com.wade.fit.util.ToastUtil;

/* renamed from: com.wade.fit.ui.main.activity.UpdatePasswordActivity */
public class UpdatePasswordActivity extends BaseMvpActivity<UpdatePasswordPresenter> implements UpdatePasswordContract.View {
    Button btnUpdate;
    EditText etPwd;
    EditText etSmsCode;
    EditText etUsername;
    Handler handler = new Handler();
    ImageView imgClean;
    ImageView imgStatus;
    /* access modifiers changed from: private */
    public boolean isRunTime = false;
    /* access modifiers changed from: private */
    public int longTime;
    private long screenOffTime;
    private long screenOnTime;
    Runnable timeRun = new Runnable() {
        /* class com.wade.fit.ui.main.activity.UpdatePasswordActivity.C25451 */

        public void run() {
            UpdatePasswordActivity.access$008(UpdatePasswordActivity.this);
            if (UpdatePasswordActivity.this.longTime < 60) {
                boolean unused = UpdatePasswordActivity.this.isRunTime = true;
                TextView textView = UpdatePasswordActivity.this.tvSmsCode;
                textView.setText(UpdatePasswordActivity.this.getResources().getString(R.string.time_after_again) + "(" + (60 - UpdatePasswordActivity.this.longTime) + ")");
                UpdatePasswordActivity.this.tvSmsCode.setBackgroundResource(R.drawable.btn_unclick_bg);
                UpdatePasswordActivity updatePasswordActivity = UpdatePasswordActivity.this;
                updatePasswordActivity.postHandler(updatePasswordActivity.timeRun, 1000);
                return;
            }
            int unused2 = UpdatePasswordActivity.this.longTime = 0;
            boolean unused3 = UpdatePasswordActivity.this.isRunTime = false;
            UpdatePasswordActivity.this.tvSmsCode.setClickable(true);
            UpdatePasswordActivity.this.tvSmsCode.setBackgroundResource(R.drawable.btn_submit_bg);
            UpdatePasswordActivity.this.tvSmsCode.setText(UpdatePasswordActivity.this.getResources().getString(R.string.send_code));
            UpdatePasswordActivity.this.handler.removeCallbacks(this);
        }
    };
    TextView tvSmsCode;
    TextView tvTitle;
    private String userName;
    private String userPwd;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_update_pwd;
    }

    public void updateFaild(int i) {
    }

    static /* synthetic */ int access$008(UpdatePasswordActivity updatePasswordActivity) {
        int i = updatePasswordActivity.longTime;
        updatePasswordActivity.longTime = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.tvTitle.setText(getIntent().getExtras().getString("title"));
        this.layoutTitle.setBackgroundColor(getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(9216);
        }
        this.bgView.setVisibility(View.GONE);
        this.titleBack.setImageResource(R.mipmap.back_black);
        this.btnUpdate.setClickable(false);
    }

    /* access modifiers changed from: package-private */
    public void textChanged() {
        if (TextUtils.isEmpty(this.etUsername.getText().toString())) {
            this.imgClean.setVisibility(View.GONE);
        } else {
            this.imgClean.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(this.etUsername.getText().toString()) || TextUtils.isEmpty(this.etPwd.getText().toString()) || this.etPwd.getText().toString().length() < 6 || TextUtils.isEmpty(this.etSmsCode.getText().toString())) {
            this.btnUpdate.setClickable(false);
            this.btnUpdate.setBackgroundResource(R.drawable.btn_submit_normle);
            return;
        }
        this.btnUpdate.setClickable(true);
        this.btnUpdate.setBackgroundResource(R.drawable.btn_submit_bg);
    }

    /* access modifiers changed from: package-private */
    public void updatePwdStatus() {
        if (this.imgStatus.getTag().equals("open")) {
            this.imgStatus.setTag("close");
            this.imgStatus.setImageResource(R.mipmap.pwd_open);
            this.etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else if (this.imgStatus.getTag().equals("close")) {
            this.imgStatus.setTag("open");
            this.imgStatus.setImageResource(R.mipmap.pwd_close);
            this.etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        EditText editText = this.etPwd;
        editText.setSelection(editText.getText().length());
    }

    /* access modifiers changed from: package-private */
    public void cleanUserName() {
        this.etUsername.setText("");
    }

    /* access modifiers changed from: private */
    public void postHandler(Runnable runnable, long j) {
        this.handler.postDelayed(runnable, j);
    }

    /* access modifiers changed from: package-private */
    public void updatePassword() {
        if (!ButtonUtils.isFastDoubleClick(R.id.btnRegiest, 1000)) {
            this.userName = this.etUsername.getText().toString();
            this.userPwd = this.etPwd.getText().toString();
            if (!DataVaildUtil.vaildEmailAndPwd(this, this.userName, this.userPwd)) {
                return;
            }
            if (!TextUtils.isEmpty(this.etSmsCode.getText().toString())) {
                ((UpdatePasswordPresenter) this.mPresenter).updatePassword(this.userName, this.userPwd, this.etSmsCode.getText().toString());
            } else {
                ToastUtil.showToast(getResources().getString(R.string.input_validate_code));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void sendCode() {
        if (!ButtonUtils.isFastDoubleClick(R.id.tvSmsCode, 1000)) {
            String obj = this.etUsername.getText().toString();
            if (DataVaildUtil.validEmail(this, obj)) {
                this.tvSmsCode.setClickable(false);
                ((UpdatePasswordPresenter) this.mPresenter).sendCode(obj);
            }
        }
    }

    public void sendCodeSuccess() {
        postHandler(this.timeRun, 1000);
    }

    public void sendCodeFaild(int i) {
        this.tvSmsCode.setClickable(true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.screenOffTime = System.currentTimeMillis();
        this.handler.removeCallbacks(this.timeRun);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.isRunTime) {
            this.handler.removeCallbacks(this.timeRun);
            this.screenOnTime = System.currentTimeMillis();
            this.longTime = (int) (((long) this.longTime) + ((this.screenOnTime - this.screenOffTime) / 1000));
            postHandler(this.timeRun, 1000);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.handler.removeCallbacks(this.timeRun);
    }

    public void updateSuccess() {
        ToastUtil.showToast(getResources().getString(R.string.update_success));
        finish();
    }
}
