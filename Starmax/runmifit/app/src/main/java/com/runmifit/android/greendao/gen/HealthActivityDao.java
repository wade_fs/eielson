package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.google.android.gms.fitness.data.Field;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthActivity;
import com.tencent.open.SocialConstants;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class HealthActivityDao extends AbstractDao<HealthActivity, Void> {
    public static final String TABLENAME = "HEALTH_ACTIVITY";

    public static class Properties {
        public static final Property Aerobic_mins = new Property(18, Integer.TYPE, "aerobic_mins", false, "AEROBIC_MINS");
        public static final Property AvgSpeed = new Property(14, String.class, "avgSpeed", false, "AVG_SPEED");
        public static final Property Avg_hr_value = new Property(15, Integer.TYPE, "avg_hr_value", false, "AVG_HR_VALUE");
        public static final Property Burn_fat_mins = new Property(17, Integer.TYPE, "burn_fat_mins", false, "BURN_FAT_MINS");
        public static final Property Calories = new Property(12, Integer.TYPE, Field.NUTRIENT_CALORIES, false, "CALORIES");
        public static final Property DataFrom = new Property(22, Integer.TYPE, "dataFrom", false, "DATA_FROM");
        public static final Property Date = new Property(24, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(4, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");
        public static final Property Distance = new Property(13, Integer.TYPE, "distance", false, "DISTANCE");
        public static final Property Durations = new Property(11, Integer.TYPE, "durations", false, "DURATIONS");
        public static final Property Hour = new Property(5, Integer.TYPE, "hour", false, "HOUR");
        public static final Property Hr_data_interval_minute = new Property(8, Integer.TYPE, "hr_data_interval_minute", false, "HR_DATA_INTERVAL_MINUTE");
        public static final Property Hr_data_vlaue_json = new Property(23, String.class, "hr_data_vlaue_json", false, "HR_DATA_VLAUE_JSON");
        public static final Property IsUploaded = new Property(0, Boolean.TYPE, "isUploaded", false, "IS_UPLOADED");
        public static final Property Limit_mins = new Property(19, Integer.TYPE, "limit_mins", false, "LIMIT_MINS");
        public static final Property MacAddress = new Property(1, String.class, "macAddress", false, "MAC_ADDRESS");
        public static final Property Max_hr_value = new Property(16, Integer.TYPE, "max_hr_value", false, "MAX_HR_VALUE");
        public static final Property Minute = new Property(6, Integer.TYPE, "minute", false, "MINUTE");
        public static final Property Month = new Property(3, Integer.TYPE, "month", false, "MONTH");
        public static final Property Remark = new Property(21, String.class, "remark", false, "REMARK");
        public static final Property Second = new Property(7, Integer.TYPE, "second", false, "SECOND");
        public static final Property Step = new Property(10, Integer.TYPE, "step", false, "STEP");
        public static final Property Type = new Property(9, Integer.TYPE, SocialConstants.PARAM_TYPE, false, "TYPE");
        public static final Property UserId = new Property(20, String.class, Constants.userId, false, "USER_ID");
        public static final Property Year = new Property(2, Integer.TYPE, "year", false, "YEAR");
    }

    public Void getKey(HealthActivity healthActivity) {
        return null;
    }

    public boolean hasKey(HealthActivity healthActivity) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(HealthActivity healthActivity, long j) {
        return null;
    }

    public HealthActivityDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public HealthActivityDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"HEALTH_ACTIVITY\" (\"IS_UPLOADED\" INTEGER NOT NULL ,\"MAC_ADDRESS\" TEXT,\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"HOUR\" INTEGER NOT NULL ,\"MINUTE\" INTEGER NOT NULL ,\"SECOND\" INTEGER NOT NULL ,\"HR_DATA_INTERVAL_MINUTE\" INTEGER NOT NULL ,\"TYPE\" INTEGER NOT NULL ,\"STEP\" INTEGER NOT NULL ,\"DURATIONS\" INTEGER NOT NULL ,\"CALORIES\" INTEGER NOT NULL ,\"DISTANCE\" INTEGER NOT NULL ,\"AVG_SPEED\" TEXT,\"AVG_HR_VALUE\" INTEGER NOT NULL ,\"MAX_HR_VALUE\" INTEGER NOT NULL ,\"BURN_FAT_MINS\" INTEGER NOT NULL ,\"AEROBIC_MINS\" INTEGER NOT NULL ,\"LIMIT_MINS\" INTEGER NOT NULL ,\"USER_ID\" TEXT,\"REMARK\" TEXT,\"DATA_FROM\" INTEGER NOT NULL ,\"HR_DATA_VLAUE_JSON\" TEXT,\"DATE\" INTEGER NOT NULL );");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"HEALTH_ACTIVITY\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, HealthActivity healthActivity) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, healthActivity.getIsUploaded() ? 1 : 0);
        String macAddress = healthActivity.getMacAddress();
        if (macAddress != null) {
            databaseStatement.bindString(2, macAddress);
        }
        databaseStatement.bindLong(3, (long) healthActivity.getYear());
        databaseStatement.bindLong(4, (long) healthActivity.getMonth());
        databaseStatement.bindLong(5, (long) healthActivity.getDay());
        databaseStatement.bindLong(6, (long) healthActivity.getHour());
        databaseStatement.bindLong(7, (long) healthActivity.getMinute());
        databaseStatement.bindLong(8, (long) healthActivity.getSecond());
        databaseStatement.bindLong(9, (long) healthActivity.getHr_data_interval_minute());
        databaseStatement.bindLong(10, (long) healthActivity.getType());
        databaseStatement.bindLong(11, (long) healthActivity.getStep());
        databaseStatement.bindLong(12, (long) healthActivity.getDurations());
        databaseStatement.bindLong(13, (long) healthActivity.getCalories());
        databaseStatement.bindLong(14, (long) healthActivity.getDistance());
        String avgSpeed = healthActivity.getAvgSpeed();
        if (avgSpeed != null) {
            databaseStatement.bindString(15, avgSpeed);
        }
        databaseStatement.bindLong(16, (long) healthActivity.getAvg_hr_value());
        databaseStatement.bindLong(17, (long) healthActivity.getMax_hr_value());
        databaseStatement.bindLong(18, (long) healthActivity.getBurn_fat_mins());
        databaseStatement.bindLong(19, (long) healthActivity.getAerobic_mins());
        databaseStatement.bindLong(20, (long) healthActivity.getLimit_mins());
        String userId = healthActivity.getUserId();
        if (userId != null) {
            databaseStatement.bindString(21, userId);
        }
        String remark = healthActivity.getRemark();
        if (remark != null) {
            databaseStatement.bindString(22, remark);
        }
        databaseStatement.bindLong(23, (long) healthActivity.getDataFrom());
        String hr_data_vlaue_json = healthActivity.getHr_data_vlaue_json();
        if (hr_data_vlaue_json != null) {
            databaseStatement.bindString(24, hr_data_vlaue_json);
        }
        databaseStatement.bindLong(25, healthActivity.getDate());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, HealthActivity healthActivity) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, healthActivity.getIsUploaded() ? 1 : 0);
        String macAddress = healthActivity.getMacAddress();
        if (macAddress != null) {
            sQLiteStatement.bindString(2, macAddress);
        }
        sQLiteStatement.bindLong(3, (long) healthActivity.getYear());
        sQLiteStatement.bindLong(4, (long) healthActivity.getMonth());
        sQLiteStatement.bindLong(5, (long) healthActivity.getDay());
        sQLiteStatement.bindLong(6, (long) healthActivity.getHour());
        sQLiteStatement.bindLong(7, (long) healthActivity.getMinute());
        sQLiteStatement.bindLong(8, (long) healthActivity.getSecond());
        sQLiteStatement.bindLong(9, (long) healthActivity.getHr_data_interval_minute());
        sQLiteStatement.bindLong(10, (long) healthActivity.getType());
        sQLiteStatement.bindLong(11, (long) healthActivity.getStep());
        sQLiteStatement.bindLong(12, (long) healthActivity.getDurations());
        sQLiteStatement.bindLong(13, (long) healthActivity.getCalories());
        sQLiteStatement.bindLong(14, (long) healthActivity.getDistance());
        String avgSpeed = healthActivity.getAvgSpeed();
        if (avgSpeed != null) {
            sQLiteStatement.bindString(15, avgSpeed);
        }
        sQLiteStatement.bindLong(16, (long) healthActivity.getAvg_hr_value());
        sQLiteStatement.bindLong(17, (long) healthActivity.getMax_hr_value());
        sQLiteStatement.bindLong(18, (long) healthActivity.getBurn_fat_mins());
        sQLiteStatement.bindLong(19, (long) healthActivity.getAerobic_mins());
        sQLiteStatement.bindLong(20, (long) healthActivity.getLimit_mins());
        String userId = healthActivity.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(21, userId);
        }
        String remark = healthActivity.getRemark();
        if (remark != null) {
            sQLiteStatement.bindString(22, remark);
        }
        sQLiteStatement.bindLong(23, (long) healthActivity.getDataFrom());
        String hr_data_vlaue_json = healthActivity.getHr_data_vlaue_json();
        if (hr_data_vlaue_json != null) {
            sQLiteStatement.bindString(24, hr_data_vlaue_json);
        }
        sQLiteStatement.bindLong(25, healthActivity.getDate());
    }

    public HealthActivity readEntity(Cursor cursor, int i) {
        Cursor cursor2 = cursor;
        boolean z = cursor2.getShort(i + 0) != 0;
        int i2 = i + 1;
        String string = cursor2.isNull(i2) ? null : cursor2.getString(i2);
        int i3 = cursor2.getInt(i + 2);
        int i4 = cursor2.getInt(i + 3);
        int i5 = cursor2.getInt(i + 4);
        int i6 = cursor2.getInt(i + 5);
        int i7 = cursor2.getInt(i + 6);
        int i8 = cursor2.getInt(i + 7);
        int i9 = cursor2.getInt(i + 8);
        int i10 = cursor2.getInt(i + 9);
        int i11 = cursor2.getInt(i + 10);
        int i12 = cursor2.getInt(i + 11);
        int i13 = cursor2.getInt(i + 12);
        int i14 = cursor2.getInt(i + 13);
        int i15 = i + 14;
        String string2 = cursor2.isNull(i15) ? null : cursor2.getString(i15);
        int i16 = cursor2.getInt(i + 15);
        int i17 = cursor2.getInt(i + 16);
        int i18 = cursor2.getInt(i + 17);
        int i19 = cursor2.getInt(i + 18);
        int i20 = cursor2.getInt(i + 19);
        int i21 = i + 20;
        String string3 = cursor2.isNull(i21) ? null : cursor2.getString(i21);
        int i22 = i + 21;
        String string4 = cursor2.isNull(i22) ? null : cursor2.getString(i22);
        int i23 = i + 23;
        return new HealthActivity(z, string, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, string2, i16, i17, i18, i19, i20, string3, string4, cursor2.getInt(i + 22), cursor2.isNull(i23) ? null : cursor2.getString(i23), cursor2.getLong(i + 24));
    }

    public void readEntity(Cursor cursor, HealthActivity healthActivity, int i) {
        healthActivity.setIsUploaded(cursor.getShort(i + 0) != 0);
        int i2 = i + 1;
        String str = null;
        healthActivity.setMacAddress(cursor.isNull(i2) ? null : cursor.getString(i2));
        healthActivity.setYear(cursor.getInt(i + 2));
        healthActivity.setMonth(cursor.getInt(i + 3));
        healthActivity.setDay(cursor.getInt(i + 4));
        healthActivity.setHour(cursor.getInt(i + 5));
        healthActivity.setMinute(cursor.getInt(i + 6));
        healthActivity.setSecond(cursor.getInt(i + 7));
        healthActivity.setHr_data_interval_minute(cursor.getInt(i + 8));
        healthActivity.setType(cursor.getInt(i + 9));
        healthActivity.setStep(cursor.getInt(i + 10));
        healthActivity.setDurations(cursor.getInt(i + 11));
        healthActivity.setCalories(cursor.getInt(i + 12));
        healthActivity.setDistance(cursor.getInt(i + 13));
        int i3 = i + 14;
        healthActivity.setAvgSpeed(cursor.isNull(i3) ? null : cursor.getString(i3));
        healthActivity.setAvg_hr_value(cursor.getInt(i + 15));
        healthActivity.setMax_hr_value(cursor.getInt(i + 16));
        healthActivity.setBurn_fat_mins(cursor.getInt(i + 17));
        healthActivity.setAerobic_mins(cursor.getInt(i + 18));
        healthActivity.setLimit_mins(cursor.getInt(i + 19));
        int i4 = i + 20;
        healthActivity.setUserId(cursor.isNull(i4) ? null : cursor.getString(i4));
        int i5 = i + 21;
        healthActivity.setRemark(cursor.isNull(i5) ? null : cursor.getString(i5));
        healthActivity.setDataFrom(cursor.getInt(i + 22));
        int i6 = i + 23;
        if (!cursor.isNull(i6)) {
            str = cursor.getString(i6);
        }
        healthActivity.setHr_data_vlaue_json(str);
        healthActivity.setDate(cursor.getLong(i + 24));
    }
}
