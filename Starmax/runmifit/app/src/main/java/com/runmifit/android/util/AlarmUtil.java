package com.wade.fit.util;

import android.app.Activity;
import com.wade.fit.R;
import com.wade.fit.model.bean.SupportFunctionInfo;
import com.wade.fit.util.log.DebugLog;

public class AlarmUtil {
    public static int setAlarmIcon(int i) {
        switch (i) {
            case 0:
                return R.mipmap.device_alarm_wakeup;
            case 1:
                return R.mipmap.device_alarm_sleep;
            case 2:
                return R.mipmap.device_alarm_sport;
            case 3:
                return R.mipmap.device_alarm_pill;
            case 4:
                return R.mipmap.device_alarm_dating;
            case 5:
                return R.mipmap.device_alarm_together;
            case 6:
                return R.mipmap.device_alarm_meeting;
            case 7:
                return R.mipmap.device_alarm_customize;
            default:
                return R.mipmap.alarm_icon;
        }
    }

    public static String[] getAlarmText(Activity activity) {
        String[] alarmTexts = getAlarmTexts(activity);
        int[] alarmSupportTypes = getAlarmSupportTypes();
        DebugLog.m6203d("alarmSupportTypes长度：" + alarmSupportTypes.length);
        String[] strArr = new String[alarmSupportTypes.length];
        for (int i = 0; i < alarmSupportTypes.length; i++) {
            strArr[i] = alarmTexts[alarmSupportTypes[i]];
        }
        return strArr;
    }

    public static int setAlaryTypeIdByAlarmText(Activity activity, String[] strArr, String str) {
        int i = -1;
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (str.equals(strArr[i2])) {
                i = i2;
            }
        }
        return i;
    }

    public static int setAlarmIcon(Activity activity, String str) {
        String[] alarmTexts = getAlarmTexts(activity);
        if (str.equals(alarmTexts[0])) {
            return R.mipmap.device_alarm_wakeup;
        }
        if (str.equals(alarmTexts[0])) {
            return R.mipmap.device_alarm_sleep;
        }
        if (str.equals(alarmTexts[0])) {
            return R.mipmap.device_alarm_sport;
        }
        if (str.equals(alarmTexts[0])) {
            return R.mipmap.device_alarm_pill;
        }
        if (str.equals(alarmTexts[0])) {
            return R.mipmap.device_alarm_dating;
        }
        if (str.equals(alarmTexts[0])) {
            return R.mipmap.device_alarm_together;
        }
        if (str.equals(alarmTexts[0])) {
            return R.mipmap.device_alarm_meeting;
        }
        return str.equals(alarmTexts[0]) ? R.mipmap.device_alarm_customize : R.mipmap.alarm_icon;
    }

    public static int[] getAlarmSupportTypes() {
        SupportFunctionInfo supportFunctionInfo = new SupportFunctionInfo();
        boolean z = supportFunctionInfo.alarmWakeUp;
        boolean z2 = supportFunctionInfo.alarmSleep;
        boolean z3 = supportFunctionInfo.alarmSport;
        boolean z4 = supportFunctionInfo.alarmMedicine;
        boolean z5 = supportFunctionInfo.alarmDating;
        boolean z6 = supportFunctionInfo.alarmParty;
        boolean z7 = supportFunctionInfo.alarmMetting;
        boolean z8 = supportFunctionInfo.alarmCustom;
        Boolean[] boolArr = {true, true, true, true, true, true, true, true};
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < boolArr.length; i++) {
            DebugLog.m6203d("闹钟类型，是否支持：" + boolArr[i]);
            if (boolArr[i].booleanValue()) {
                sb.append(i + ",");
            }
        }
        String substring = sb.toString().substring(0, sb.toString().length() - 1);
        DebugLog.m6203d("支持的类型=" + substring);
        String[] split = substring.split(",");
        int[] iArr = new int[split.length];
        for (int i2 = 0; i2 < split.length; i2++) {
            iArr[i2] = NumUtil.isEmptyInt(split[i2]).intValue();
        }
        return iArr;
    }

    public static String[] getAlarmTexts(Activity activity) {
        return activity.getResources().getStringArray(R.array.alarmType);
    }
}
