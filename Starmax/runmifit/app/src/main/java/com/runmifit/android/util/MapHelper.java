package com.wade.fit.util;

import com.wade.fit.model.bean.LatLngBean;

public class MapHelper {
    public static int getMapSource() {
        return 0;
    }

    public static double getDistance(LatLngBean latLngBean, LatLngBean latLngBean2) {
        double d = latLngBean2.longitude * 0.017453292519943295d;
        double d2 = latLngBean.latitude * 0.017453292519943295d;
        double d3 = latLngBean2.latitude * 0.017453292519943295d;
        return Math.acos((Math.sin(d2) * Math.sin(d3)) + (Math.cos(d2) * Math.cos(d3) * Math.cos(d - (latLngBean.longitude * 0.017453292519943295d)))) * 6371.0d * 1000.0d;
    }

    public static void gaoDeToBaidu(LatLngBean latLngBean) {
        double longitude = latLngBean.getLongitude();
        double latitude = latLngBean.getLatitude();
        double sqrt = Math.sqrt((longitude * longitude) + (latitude * latitude)) + (Math.sin(latitude * 52.35987755982988d) * 2.0E-5d);
        double atan2 = Math.atan2(latitude, longitude) + (Math.cos(longitude * 52.35987755982988d) * 3.0E-6d);
        latLngBean.longitude = (Math.cos(atan2) * sqrt) + 0.0065d;
        latLngBean.latitude = (sqrt * Math.sin(atan2)) + 0.006d;
    }
}
