package com.wade.fit.util;

import android.content.Context;
import android.widget.ImageView;
import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.wade.fit.R;

public class ImageLoadUtil {
    private int error = R.mipmap.img_default;
    private int error_pro = R.mipmap.img_portrait_default;
    private int placeholder = R.mipmap.img_default;
    private BitmapTransformation transforms;

    public void loadImage(Context context, ImageView imageView, String str) {
        setTransformation(null);
        load(context, imageView, str);
    }

    public void loadRectImage(Context context, ImageView imageView, String str) {
        setTransformation(new GlideRoundTransform(context));
        load(context, imageView, str);
    }

    public void loadPortrait(Context context, ImageView imageView, String str) {
        setTransformation(new GlideCircleTransform(context));
        loadPor(context, imageView, str);
    }

    public ImageLoadUtil setPlaceholder(int i) {
        this.placeholder = i;
        return this;
    }

    public ImageLoadUtil setError(int i) {
        this.error = i;
        return this;
    }

    public ImageLoadUtil setTransformation(BitmapTransformation bitmapTransformation) {
        this.transforms = bitmapTransformation;
        return this;
    }

    private void loadPor(Context context, ImageView imageView, String str) {
        if (context != null) {
            DrawableRequestBuilder<String> diskCacheStrategy = Glide.with(context).load(str).placeholder(this.error_pro).error(this.error_pro).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL);
            if (this.transforms != null) {
                diskCacheStrategy.transform(new CenterCrop(context), this.transforms);
            } else {
                diskCacheStrategy.centerCrop();
            }
            diskCacheStrategy.into(imageView);
        }
    }

    private void load(Context context, ImageView imageView, String str) {
        if (context != null) {
            DrawableRequestBuilder<String> diskCacheStrategy = Glide.with(context).load(str).placeholder(this.placeholder).error(this.error).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL);
            if (this.transforms != null) {
                diskCacheStrategy.transform(new CenterCrop(context), this.transforms);
            } else {
                diskCacheStrategy.centerCrop();
            }
            diskCacheStrategy.into(imageView);
        }
    }
}
