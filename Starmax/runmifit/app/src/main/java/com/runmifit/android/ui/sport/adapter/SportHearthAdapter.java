package com.wade.fit.ui.sport.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.base.BaseAdapter;
import com.wade.fit.base.BaseViewHolder;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.ui.sport.adapter.SportHearthAdapter;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.NumUtil;
import com.wade.fit.util.SportDataHelper;
import com.wade.fit.util.TimeUtil;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.LogUtil;
import java.util.List;

/* renamed from: com.wade.fit.ui.sport.adapter.SportHearthAdapter */
public class SportHearthAdapter extends BaseAdapter<HealthActivity, ViewHolder> {
    private String[] amOrPm;

    /* renamed from: com.wade.fit.ui.sport.adapter.SportHearthAdapter$ViewHolder_ViewBinding */
    public class ViewHolder_ViewBinding implements Unbinder {
        private ViewHolder target;

        public ViewHolder_ViewBinding(ViewHolder viewHolder, View view) {
            this.target = viewHolder;
            viewHolder.ivSportType = (ImageView) Utils.findRequiredViewAsType(view, R.id.ivSportType, "field 'ivSportType'", ImageView.class);
            viewHolder.tvSportType = (TextView) Utils.findRequiredViewAsType(view, R.id.tvSportType, "field 'tvSportType'", TextView.class);
            viewHolder.tvTime = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTime, "field 'tvTime'", TextView.class);
            viewHolder.tvDate = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDate, "field 'tvDate'", TextView.class);
            viewHolder.tvDistance = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDistance, "field 'tvDistance'", TextView.class);
            viewHolder.tvDistanceUnit = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDistanceUnit, "field 'tvDistanceUnit'", TextView.class);
            viewHolder.tvDuration = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDuration, "field 'tvDuration'", TextView.class);
            viewHolder.tvAvHeartRate = (TextView) Utils.findRequiredViewAsType(view, R.id.tvAvHeartRate, "field 'tvAvHeartRate'", TextView.class);
            viewHolder.tvCal = (TextView) Utils.findRequiredViewAsType(view, R.id.tvCal, "field 'tvCal'", TextView.class);
            viewHolder.yearMonthLayout = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.year_month_layout, "field 'yearMonthLayout'", RelativeLayout.class);
        }

        public void unbind() {
            ViewHolder viewHolder = this.target;
            if (viewHolder != null) {
                this.target = null;
                viewHolder.ivSportType = null;
                viewHolder.tvSportType = null;
                viewHolder.tvTime = null;
                viewHolder.tvDate = null;
                viewHolder.tvDistance = null;
                viewHolder.tvDistanceUnit = null;
                viewHolder.tvDuration = null;
                viewHolder.tvAvHeartRate = null;
                viewHolder.tvCal = null;
                viewHolder.yearMonthLayout = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public SportHearthAdapter(Context context, List<HealthActivity> list) {
        super(context, list);
        this.amOrPm = context.getResources().getStringArray(R.array.amOrpm);
    }

    /* access modifiers changed from: protected */
    public void onNormalBindViewHolder(ViewHolder viewHolder, HealthActivity healthActivity, int i) {
        viewHolder.ivSportType.setImageResource(SportDataHelper.getResByType(healthActivity.getType()));
        viewHolder.tvSportType.setText(SportDataHelper.getStringBytyp(healthActivity.getType()));
        viewHolder.tvTime.setText(TimeUtil.timeStamp2Date(healthActivity.getDate(), "HH:mm", TimeUtil.is24Hour(null), this.amOrPm));
        viewHolder.tvDate.setText(DateUtil.simpleDateFormat2.format(Long.valueOf(healthActivity.getDate())));
        if (!BleSdkWrapper.isDistUnitKm()) {
            viewHolder.tvDistance.setText(NumUtil.format2Point((double) UnitUtil.getKm2mile(((float) healthActivity.getDistance()) / 1000.0f)));
            viewHolder.tvDistanceUnit.setText(this.mContext.getResources().getString(R.string.unit_mi));
        } else {
            viewHolder.tvDistance.setText(this.mContext.getResources().getString(R.string.mileage_str, NumUtil.format2Point((double) (((float) healthActivity.getDistance()) / 1000.0f))));
            viewHolder.tvDistanceUnit.setText(this.mContext.getResources().getString(R.string.unit_kilometer));
        }
        viewHolder.tvAvHeartRate.setText(String.valueOf(healthActivity.getAvg_hr_value()));
        viewHolder.tvCal.setText(healthActivity.getAvgSpeed());
        LogUtil.d("tvDuration:" + healthActivity.getDurations());
        TextView textView = viewHolder.tvDuration;
        textView.setText(String.format("%02d", Integer.valueOf(healthActivity.getDurations() / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf((healthActivity.getDurations() % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf(healthActivity.getDurations() % 60)));
        if (i > 0) {
            HealthActivity healthActivity2 = (HealthActivity) this.mList.get(i - 1);
            String remark = healthActivity.getRemark();
            if (remark == null) {
                viewHolder.yearMonthLayout.setVisibility(View.GONE);
            } else if (remark.equals(healthActivity2.getRemark())) {
                viewHolder.yearMonthLayout.setVisibility(View.GONE);
            } else {
                viewHolder.yearMonthLayout.setVisibility(View.VISIBLE);
            }
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener(viewHolder, i) {
            /* class com.wade.fit.ui.sport.adapter.$$Lambda$SportHearthAdapter$DFTS9ICxY2YpV_bcx_hqYLMUYHg */
            private final /* synthetic */ SportHearthAdapter.ViewHolder f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                SportHearthAdapter.this.lambda$onNormalBindViewHolder$0$SportHearthAdapter(this.f$1, this.f$2, view);
            }
        });
    }

    public /* synthetic */ void lambda$onNormalBindViewHolder$0$SportHearthAdapter(ViewHolder viewHolder, int i, View view) {
        this.mOnItemClickListener.onItemClick(viewHolder.itemView, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(this.inflater.inflate((int) R.layout.item_sport_health, viewGroup, false));
    }

    /* renamed from: com.wade.fit.ui.sport.adapter.SportHearthAdapter$ViewHolder */
    public class ViewHolder extends BaseViewHolder {
        ImageView ivSportType;
        TextView tvAvHeartRate;
        TextView tvCal;
        TextView tvDate;
        TextView tvDistance;
        TextView tvDistanceUnit;
        TextView tvDuration;
        TextView tvSportType;
        TextView tvTime;
        RelativeLayout yearMonthLayout;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
