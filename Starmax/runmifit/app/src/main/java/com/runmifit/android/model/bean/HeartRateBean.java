package com.wade.fit.model.bean;

import java.io.Serializable;
import java.util.Date;

public class HeartRateBean implements Serializable {
    public int OxygenValue;
    public Date date;
    public int fzValue;
    public int heartRateValue;
    public int ssValue;
}
