package com.wade.fit.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.util.CrashUtils;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.LibContext;
import com.wade.fit.ui.main.activity.WelcomeActivity;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CrashUtil implements Thread.UncaughtExceptionHandler {
    private static CrashUtil INSTANCE = new CrashUtil();
    public static final String TAG = "CrashUtil";
    private String dir;
    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    private Map<String, String> infos = new HashMap();
    private Context mContext;
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    public String path;

    private CrashUtil() {
    }

    public static CrashUtil getInstance() {
        return INSTANCE;
    }

    public void init(Context context) {
        this.mContext = context;
        this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void setCrashDir(String str) {
        this.dir = str;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        restartApp();
    }

    private void restartApp() {
        ((AlarmManager) AppApplication.getInstance().getSystemService(NotificationCompat.CATEGORY_ALARM)).set(1, System.currentTimeMillis() + 1000, PendingIntent.getActivity(AppApplication.getInstance(), 0, new Intent(AppApplication.getInstance(), WelcomeActivity.class), CrashUtils.ErrorDialogData.BINDER_CRASH));
        AppApplication.finishAll();
        Process.killProcess(Process.myPid());
    }

    private boolean handleException(Throwable th) {
        if (th == null) {
            return false;
        }
        collectDeviceInfo(this.mContext);
        saveCrashInfo2File(th);
        return true;
    }

    public void collectDeviceInfo(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 1);
            if (packageInfo != null) {
                this.infos.put("versionName", packageInfo.versionName == null ? "null" : packageInfo.versionName);
                this.infos.put("versionCode", packageInfo.versionCode + "");
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "an error occured when collect package info", e);
        }
        Field[] declaredFields = Build.class.getDeclaredFields();
        for (Field field : declaredFields) {
            try {
                field.setAccessible(true);
                this.infos.put(field.getName(), field.get(null).toString());
                Log.d(TAG, field.getName() + " : " + field.get(null));
            } catch (Exception e2) {
                Log.e(TAG, "an error occured when collect crash info", e2);
            }
        }
    }

    private String saveCrashInfo2File(Throwable th) {
        if (ContextCompat.checkSelfPermission(LibContext.getAppContext(), "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (Map.Entry entry : this.infos.entrySet()) {
            stringBuffer.append(((String) entry.getKey()) + "=" + ((String) entry.getValue()) + "\n");
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        printWriter.close();
        stringBuffer.append(stringWriter.toString());
        try {
            String str = "crash_log_" + this.formatter.format(new Date()) + ".txt";
            if (Environment.getExternalStorageState().equals("mounted")) {
                if (TextUtils.isEmpty(this.dir)) {
                    this.dir = Environment.getExternalStorageDirectory().getAbsolutePath();
                } else {
                    deleteOldFile(this.dir);
                }
                File file = new File(this.dir);
                if (!file.exists()) {
                    file.mkdirs();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(new File(this.dir, str));
                fileOutputStream.write(stringBuffer.toString().getBytes());
                fileOutputStream.close();
            }
            return str;
        } catch (Exception e) {
            Log.e(TAG, "an error occured while writing url...", e);
            return null;
        }
    }

    public static int daysOfTwo(Date date) {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(6);
        instance.setTime(date);
        return i - instance.get(6);
    }

    public static int daysOfTwo(long j) {
        return daysOfTwo(new Date(j));
    }

    private static void deleteOldFile(String str) {
        File[] listFiles;
        File file = new File(str);
        if (file.exists() && (listFiles = file.listFiles()) != null && file.length() > 10) {
            for (File file2 : listFiles) {
                if (daysOfTwo(file2.lastModified()) > 10) {
                    file2.delete();
                }
            }
        }
    }
}
