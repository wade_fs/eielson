package com.wade.fit.base.refresh;

import com.wade.fit.base.IBaseView;
import java.util.List;

public interface BaseListView<M> extends IBaseView {
    void onComplete();

    void onLoadMoreSuccess(List<M> list);

    void onRefreshSuccess(List<M> list);

    void setCanLoadmore(boolean z);

    void showNoData(int i);

    void showNoMore();
}
