package com.wade.fit.views.camera;

import android.support.v4.util.ArrayMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SizeMap {
    private final ArrayMap<AspectRatio, SortedSet<Size>> mRatios = new ArrayMap<>();

    public boolean add(Size size) {
        for (AspectRatio aspectRatio : this.mRatios.keySet()) {
            if (aspectRatio.matches(size)) {
                SortedSet sortedSet = this.mRatios.get(aspectRatio);
                if (sortedSet.contains(size)) {
                    return false;
                }
                sortedSet.add(size);
                return true;
            }
        }
        TreeSet treeSet = new TreeSet();
        treeSet.add(size);
        this.mRatios.put(AspectRatio.m6223of(size.getWidth(), size.getHeight()), treeSet);
        return true;
    }

    public void remove(AspectRatio aspectRatio) {
        this.mRatios.remove(aspectRatio);
    }

    /* access modifiers changed from: package-private */
    public Set<AspectRatio> ratios() {
        return this.mRatios.keySet();
    }

    /* access modifiers changed from: package-private */
    public SortedSet<Size> sizes(AspectRatio aspectRatio) {
        return this.mRatios.get(aspectRatio);
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.mRatios.clear();
    }

    /* access modifiers changed from: package-private */
    public boolean isEmpty() {
        return this.mRatios.isEmpty();
    }
}
