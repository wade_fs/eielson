package com.wade.fit.greendao.gen;

import com.wade.fit.greendao.bean.ECGItemInfo;
import com.wade.fit.greendao.bean.EcgRecordInfo;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthGpsItem;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.bean.HealthSleepItem;
import com.wade.fit.greendao.bean.HealthSport;
import com.wade.fit.greendao.bean.HealthSportItem;
import java.util.Map;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

public class DaoSession extends AbstractDaoSession {
    private final ECGItemInfoDao eCGItemInfoDao = new ECGItemInfoDao(this.eCGItemInfoDaoConfig, this);
    private final DaoConfig eCGItemInfoDaoConfig;
    private final EcgRecordInfoDao ecgRecordInfoDao = new EcgRecordInfoDao(this.ecgRecordInfoDaoConfig, this);
    private final DaoConfig ecgRecordInfoDaoConfig;
    private final HealthActivityDao healthActivityDao = new HealthActivityDao(this.healthActivityDaoConfig, this);
    private final DaoConfig healthActivityDaoConfig;
    private final HealthGpsItemDao healthGpsItemDao = new HealthGpsItemDao(this.healthGpsItemDaoConfig, this);
    private final DaoConfig healthGpsItemDaoConfig;
    private final HealthHeartRateDao healthHeartRateDao = new HealthHeartRateDao(this.healthHeartRateDaoConfig, this);
    private final DaoConfig healthHeartRateDaoConfig;
    private final HealthHeartRateItemDao healthHeartRateItemDao = new HealthHeartRateItemDao(this.healthHeartRateItemDaoConfig, this);
    private final DaoConfig healthHeartRateItemDaoConfig;
    private final HealthSleepDao healthSleepDao = new HealthSleepDao(this.healthSleepDaoConfig, this);
    private final DaoConfig healthSleepDaoConfig;
    private final HealthSleepItemDao healthSleepItemDao = new HealthSleepItemDao(this.healthSleepItemDaoConfig, this);
    private final DaoConfig healthSleepItemDaoConfig;
    private final HealthSportDao healthSportDao = new HealthSportDao(this.healthSportDaoConfig, this);
    private final DaoConfig healthSportDaoConfig;
    private final HealthSportItemDao healthSportItemDao = new HealthSportItemDao(this.healthSportItemDaoConfig, this);
    private final DaoConfig healthSportItemDaoConfig;

    public DaoSession(Database database, IdentityScopeType identityScopeType, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig> map) {
        super(database);
        this.eCGItemInfoDaoConfig = map.get(ECGItemInfoDao.class).clone();
        this.eCGItemInfoDaoConfig.initIdentityScope(identityScopeType);
        this.ecgRecordInfoDaoConfig = map.get(EcgRecordInfoDao.class).clone();
        this.ecgRecordInfoDaoConfig.initIdentityScope(identityScopeType);
        this.healthActivityDaoConfig = map.get(HealthActivityDao.class).clone();
        this.healthActivityDaoConfig.initIdentityScope(identityScopeType);
        this.healthGpsItemDaoConfig = map.get(HealthGpsItemDao.class).clone();
        this.healthGpsItemDaoConfig.initIdentityScope(identityScopeType);
        this.healthHeartRateDaoConfig = map.get(HealthHeartRateDao.class).clone();
        this.healthHeartRateDaoConfig.initIdentityScope(identityScopeType);
        this.healthHeartRateItemDaoConfig = map.get(HealthHeartRateItemDao.class).clone();
        this.healthHeartRateItemDaoConfig.initIdentityScope(identityScopeType);
        this.healthSleepDaoConfig = map.get(HealthSleepDao.class).clone();
        this.healthSleepDaoConfig.initIdentityScope(identityScopeType);
        this.healthSleepItemDaoConfig = map.get(HealthSleepItemDao.class).clone();
        this.healthSleepItemDaoConfig.initIdentityScope(identityScopeType);
        this.healthSportDaoConfig = map.get(HealthSportDao.class).clone();
        this.healthSportDaoConfig.initIdentityScope(identityScopeType);
        this.healthSportItemDaoConfig = map.get(HealthSportItemDao.class).clone();
        this.healthSportItemDaoConfig.initIdentityScope(identityScopeType);
        registerDao(ECGItemInfo.class, this.eCGItemInfoDao);
        registerDao(EcgRecordInfo.class, this.ecgRecordInfoDao);
        registerDao(HealthActivity.class, this.healthActivityDao);
        registerDao(HealthGpsItem.class, this.healthGpsItemDao);
        registerDao(HealthHeartRate.class, this.healthHeartRateDao);
        registerDao(HealthHeartRateItem.class, this.healthHeartRateItemDao);
        registerDao(HealthSleep.class, this.healthSleepDao);
        registerDao(HealthSleepItem.class, this.healthSleepItemDao);
        registerDao(HealthSport.class, this.healthSportDao);
        registerDao(HealthSportItem.class, this.healthSportItemDao);
    }

    public void clear() {
        this.eCGItemInfoDaoConfig.clearIdentityScope();
        this.ecgRecordInfoDaoConfig.clearIdentityScope();
        this.healthActivityDaoConfig.clearIdentityScope();
        this.healthGpsItemDaoConfig.clearIdentityScope();
        this.healthHeartRateDaoConfig.clearIdentityScope();
        this.healthHeartRateItemDaoConfig.clearIdentityScope();
        this.healthSleepDaoConfig.clearIdentityScope();
        this.healthSleepItemDaoConfig.clearIdentityScope();
        this.healthSportDaoConfig.clearIdentityScope();
        this.healthSportItemDaoConfig.clearIdentityScope();
    }

    public ECGItemInfoDao getECGItemInfoDao() {
        return this.eCGItemInfoDao;
    }

    public EcgRecordInfoDao getEcgRecordInfoDao() {
        return this.ecgRecordInfoDao;
    }

    public HealthActivityDao getHealthActivityDao() {
        return this.healthActivityDao;
    }

    public HealthGpsItemDao getHealthGpsItemDao() {
        return this.healthGpsItemDao;
    }

    public HealthHeartRateDao getHealthHeartRateDao() {
        return this.healthHeartRateDao;
    }

    public HealthHeartRateItemDao getHealthHeartRateItemDao() {
        return this.healthHeartRateItemDao;
    }

    public HealthSleepDao getHealthSleepDao() {
        return this.healthSleepDao;
    }

    public HealthSleepItemDao getHealthSleepItemDao() {
        return this.healthSleepItemDao;
    }

    public HealthSportDao getHealthSportDao() {
        return this.healthSportDao;
    }

    public HealthSportItemDao getHealthSportItemDao() {
        return this.healthSportItemDao;
    }
}
