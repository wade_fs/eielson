package com.wade.fit.ui.device.activity;

import android.app.Dialog;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.newland.springdialog.AnimSpring;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ble.BaseDataHandler;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.ble.DeviceCallbackWrapper;
import com.wade.fit.views.camera.CameraPreview;
import com.wade.fit.views.camera.OverCameraView;
import java.io.IOException;

/* renamed from: com.wade.fit.ui.device.activity.CameraActivity */
public class CameraActivity extends BaseActivity implements View.OnClickListener {
    public static final String KEY_IMAGE_PATH = "imagePath";
    private static int PERMISSIONS_REQUEST_OPEN_ALBUM = 1001;
    private Camera.AutoFocusCallback autoFocusCallback = new Camera.AutoFocusCallback() {
        /* class com.wade.fit.ui.device.activity.CameraActivity.C24601 */

        public void onAutoFocus(boolean z, Camera camera) {
            if (CameraActivity.this.mOverCameraView != null) {
                boolean unused = CameraActivity.this.isFoucing = false;
                CameraActivity.this.mOverCameraView.setFoucuing(false);
                CameraActivity.this.mOverCameraView.disDrawTouchFocusRect();
                CameraActivity.this.mHandler.removeCallbacks(CameraActivity.this.mRunnable);
            }
        }
    };
    private ImageView changeBack;
    DeviceCallbackWrapper deviceCallback = new DeviceCallbackWrapper() {
        /* class com.wade.fit.ui.device.activity.CameraActivity.C24612 */

        public void camare() {
            super.camare();
            CameraActivity.this.takePhoto();
        }

        public void exitCamare() {
            super.exitCamare();
            CameraActivity.this.finish();
        }
    };
    private byte[] imageData;
    private boolean isFlashing;
    /* access modifiers changed from: private */
    public boolean isFoucing;
    private boolean isTakePhoto;
    private Camera mCamera;
    private int mCameraId = 0;
    private Button mCancleButton;
    private Dialog mDialog;
    private ImageView mFlashButton;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    private SurfaceHolder mHolder;
    /* access modifiers changed from: private */
    public OverCameraView mOverCameraView;
    private ImageView mPhotoButton;
    private RelativeLayout mPhotoLayout;
    private FrameLayout mPreviewLayout;
    /* access modifiers changed from: private */
    public Runnable mRunnable;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_camre_layout;
    }

    public void requestPermissionsSuccess(int i) {
    }

    private void fitComprehensiveScreen() {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(2);
            getWindow().addFlags(512);
            getWindow().addFlags(256);
            getWindow().setNavigationBarColor(0);
            getWindow().setStatusBarColor(0);
        }
    }

    private void setOnclickListener() {
        this.mCancleButton.setOnClickListener(this);
        this.mFlashButton.setOnClickListener(this);
        this.changeBack.setOnClickListener(this);
        this.mPhotoButton.setOnClickListener(this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && !this.isFoucing) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            this.isFoucing = true;
            Camera camera = this.mCamera;
            if (camera != null && !this.isTakePhoto) {
                this.mOverCameraView.setTouchFoucusRect(camera, this.autoFocusCallback, x, y);
            }
            this.mRunnable = new Runnable() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$CameraActivity$Zr3VAzhb0MfIN_vT7Utaqbimvd8 */

                public final void run() {
                    CameraActivity.this.lambda$onTouchEvent$0$CameraActivity();
                }
            };
            this.mHandler.postDelayed(this.mRunnable, 3000);
        }
        return super.onTouchEvent(motionEvent);
    }

    public /* synthetic */ void lambda$onTouchEvent$0$CameraActivity() {
        this.isFoucing = false;
        this.mOverCameraView.setFoucuing(false);
        this.mOverCameraView.disDrawTouchFocusRect();
    }

    /* access modifiers changed from: private */
    public void takePhoto() {
        this.isTakePhoto = true;
        this.mCamera.takePicture(null, null, null, new Camera.PictureCallback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$CameraActivity$Morn5D_y6Lg3KfLEob4ZVOXdc */

            public final void onPictureTaken(byte[] bArr, Camera camera) {
                CameraActivity.this.lambda$takePhoto$1$CameraActivity(bArr, camera);
            }
        });
    }

    public /* synthetic */ void lambda$takePhoto$1$CameraActivity(byte[] bArr, Camera camera) {
        this.imageData = bArr;
        this.mCamera.stopPreview();
        savePhoto();
    }

    private void switchFlash() {
        this.isFlashing = !this.isFlashing;
        this.mFlashButton.setImageResource(this.isFlashing ? R.mipmap.flash_open : R.mipmap.flash_close);
        AnimSpring.getInstance(this.mFlashButton).startRotateAnim(120.0f, 360.0f);
        try {
            Camera.Parameters parameters = this.mCamera.getParameters();
            parameters.setFlashMode(this.isFlashing ? "torch" : "off");
            this.mCamera.setParameters(parameters);
        } catch (Exception unused) {
            Toast.makeText(this, "该设备不支持闪光灯", 0);
        }
    }

    private void cancleSavePhoto() {
        this.mPhotoLayout.setVisibility(View.VISIBLE);
        this.mCamera.startPreview();
        this.imageData = null;
        this.isTakePhoto = false;
    }

    public static String parseResult(Intent intent) {
        return intent.getStringExtra(KEY_IMAGE_PATH);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.cancle_button) {
            finish();
        } else if (id == R.id.take_photo_button) {
            if (!this.isTakePhoto) {
                takePhoto();
            }
        } else if (id == R.id.flash_button) {
            switchFlash();
        } else if (id == R.id.change_webcam) {
            switchCamera();
        }
    }

    public void switchCamera() {
        if (this.mCameraId == 0) {
            this.mCameraId = 1;
        } else {
            this.mCameraId = 0;
        }
        Camera camera = this.mCamera;
        if (camera != null) {
            try {
                camera.stopPreview();
                this.mCamera.release();
                this.mCamera = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.mCamera = openCamera(this.mCameraId);
        Camera camera2 = this.mCamera;
        if (camera2 != null) {
            try {
                camera2.setPreviewDisplay(this.mHolder);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.mCamera.setDisplayOrientation(90);
            this.mCamera.startPreview();
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.layoutTitle.setVisibility(View.GONE);
        this.mCancleButton = (Button) findViewById(R.id.cancle_button);
        this.mPreviewLayout = (FrameLayout) findViewById(R.id.camera_preview_layout);
        this.mPhotoLayout = (RelativeLayout) findViewById(R.id.ll_photo_layout);
        this.mPhotoButton = (ImageView) findViewById(R.id.take_photo_button);
        this.mFlashButton = (ImageView) findViewById(R.id.flash_button);
        this.changeBack = (ImageView) findViewById(R.id.change_webcam);
        setOnclickListener();
        openCamera();
        requestPermissions(PERMISSIONS_REQUEST_OPEN_ALBUM, "android.permission.WRITE_EXTERNAL_STORAGE");
    }

    private void openCamera() {
        if (this.mCamera == null) {
            this.mCamera = openCamera(this.mCameraId);
        }
        this.mCamera.setDisplayOrientation(90);
        CameraPreview cameraPreview = new CameraPreview(this, this.mCamera, this.mCameraId);
        this.mHolder = cameraPreview.getHolder();
        Camera.Size findBestPreviewResolution = cameraPreview.findBestPreviewResolution();
        cameraPreview.setLayoutParams(new ViewGroup.LayoutParams(ScreenUtil.getScreenWidth(this), (ScreenUtil.getScreenWidth(this) * findBestPreviewResolution.width) / findBestPreviewResolution.height));
        this.mPreviewLayout.addView(cameraPreview);
        if (this.mCameraId == 0) {
            this.mOverCameraView = new OverCameraView(this);
            this.mPreviewLayout.addView(this.mOverCameraView);
            this.mOverCameraView.setTouchFoucusRect(this.mCamera, this.autoFocusCallback, (float) (ScreenUtil.getScreenWidth(this) / 2), (float) (ScreenUtil.getScreenHeight(this) / 2));
        }
        BaseDataHandler.getInstance().addDeviceCallback(this.deviceCallback);
    }

    private Camera openCamera(int i) {
        int numberOfCameras = Camera.getNumberOfCameras();
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int i2 = -1;
        int i3 = -1;
        for (int i4 = 0; i4 < numberOfCameras; i4++) {
            Camera.getCameraInfo(i4, cameraInfo);
            if (cameraInfo.facing == 1) {
                i2 = i4;
            } else if (cameraInfo.facing == 0) {
                i3 = i4;
            }
        }
        if (i == 1 && i2 != -1) {
            return Camera.open(i2);
        }
        if (i != 0 || i3 == -1) {
            return null;
        }
        return Camera.open(i3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00ab A[SYNTHETIC, Splitter:B:22:0x00ab] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d4 A[SYNTHETIC, Splitter:B:30:0x00d4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void savePhoto() {
        /*
            r6 = this;
            java.lang.String r0 = "file://"
            java.lang.String r1 = "android.intent.action.MEDIA_SCANNER_SCAN_FILE"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r3 = r3.getPath()
            r2.append(r3)
            java.lang.String r3 = java.io.File.separator
            r2.append(r3)
            java.lang.String r3 = "DCIM"
            r2.append(r3)
            java.lang.String r3 = java.io.File.separator
            r2.append(r3)
            java.lang.String r3 = "Camera"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.io.File r3 = new java.io.File
            r3.<init>(r2)
            boolean r2 = r3.exists()
            if (r2 != 0) goto L_0x003a
            r3.mkdirs()
        L_0x003a:
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.lang.String r4 = "yyyyMMdd_HHmmss"
            r2.<init>(r4)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r3 = r3.getAbsolutePath()
            r4.append(r3)
            java.lang.String r3 = java.io.File.separator
            r4.append(r3)
            java.lang.String r3 = "IMG_"
            r4.append(r3)
            java.util.Date r3 = new java.util.Date
            r3.<init>()
            java.lang.String r2 = r2.format(r3)
            r4.append(r2)
            java.lang.String r2 = ".jpg"
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            java.io.File r3 = new java.io.File
            r3.<init>(r2)
            r4 = 0
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00a5 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x00a5 }
            byte[] r3 = r6.imageData     // Catch:{ Exception -> 0x00a0, all -> 0x009d }
            r5.write(r3)     // Catch:{ Exception -> 0x00a0, all -> 0x009d }
            r5.close()     // Catch:{ IOException -> 0x0080 }
            goto L_0x0084
        L_0x0080:
            r3 = move-exception
            r3.printStackTrace()
        L_0x0084:
            android.content.Intent r3 = new android.content.Intent
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r0)
            r4.append(r2)
            java.lang.String r0 = r4.toString()
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r3.<init>(r1, r0)
            goto L_0x00cb
        L_0x009d:
            r3 = move-exception
            r4 = r5
            goto L_0x00d2
        L_0x00a0:
            r3 = move-exception
            r4 = r5
            goto L_0x00a6
        L_0x00a3:
            r3 = move-exception
            goto L_0x00d2
        L_0x00a5:
            r3 = move-exception
        L_0x00a6:
            r3.printStackTrace()     // Catch:{ all -> 0x00a3 }
            if (r4 == 0) goto L_0x00b3
            r4.close()     // Catch:{ IOException -> 0x00af }
            goto L_0x00b3
        L_0x00af:
            r3 = move-exception
            r3.printStackTrace()
        L_0x00b3:
            android.content.Intent r3 = new android.content.Intent
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r0)
            r4.append(r2)
            java.lang.String r0 = r4.toString()
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r3.<init>(r1, r0)
        L_0x00cb:
            r6.sendBroadcast(r3)
            r6.cancleSavePhoto()
            return
        L_0x00d2:
            if (r4 == 0) goto L_0x00dc
            r4.close()     // Catch:{ IOException -> 0x00d8 }
            goto L_0x00dc
        L_0x00d8:
            r4 = move-exception
            r4.printStackTrace()
        L_0x00dc:
            android.content.Intent r4 = new android.content.Intent
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r0)
            r5.append(r2)
            java.lang.String r0 = r5.toString()
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r4.<init>(r1, r0)
            r6.sendBroadcast(r4)
            r6.cancleSavePhoto()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.ui.device.activity.CameraActivity.savePhoto():void");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        AppApplication.isCamer = false;
        BleSdkWrapper.exitCamare(null);
        BaseDataHandler.getInstance().removeDeviceCallback(this.deviceCallback);
        Camera camera = this.mCamera;
        if (camera != null) {
            try {
                camera.setPreviewCallback(null);
                this.mCamera.stopPreview();
                this.mCamera.release();
                this.mCamera = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }

    public void requestPermissionsFail(int i) {
        if (i != PERMISSIONS_REQUEST_OPEN_ALBUM) {
            return;
        }
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            this.mDialog = DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_camera_title), getResources().getString(R.string.permisson_camera_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$CameraActivity$I1kt_a5bBZiYag8hCvRCfQdZOf4 */

                public final void onClick(View view) {
                    CameraActivity.this.lambda$requestPermissionsFail$2$CameraActivity(view);
                }
            }, new View.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$CameraActivity$L7_rTfk4dSo7i27r5Me0I_lXlU */

                public final void onClick(View view) {
                    CameraActivity.this.lambda$requestPermissionsFail$3$CameraActivity(view);
                }
            });
        } else {
            finish();
        }
    }

    public /* synthetic */ void lambda$requestPermissionsFail$2$CameraActivity(View view) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getApplicationContext().getPackageName(), null));
        startActivityForResult(intent, PERMISSIONS_REQUEST_OPEN_ALBUM);
    }

    public /* synthetic */ void lambda$requestPermissionsFail$3$CameraActivity(View view) {
        this.mDialog.dismiss();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        int i3 = PERMISSIONS_REQUEST_OPEN_ALBUM;
        if (i == i3) {
            requestPermissions(i3, "android.permission.WRITE_EXTERNAL_STORAGE");
        }
    }
}
