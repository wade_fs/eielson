package com.wade.fit.util;

import android.view.animation.Interpolator;

public class SpringScaleInterpolator implements Interpolator {
    private float factor;

    public SpringScaleInterpolator(float f) {
        this.factor = f;
    }

    public float getInterpolation(float f) {
        double pow = Math.pow(2.0d, (double) (-8.0f * f));
        float f2 = this.factor;
        double d = (double) (f - (f2 / 4.0f));
        Double.isNaN(d);
        double d2 = (double) f2;
        Double.isNaN(d2);
        return ((float) (pow * Math.sin((d * 6.283185307179586d) / d2))) + 1.0f;
    }
}
