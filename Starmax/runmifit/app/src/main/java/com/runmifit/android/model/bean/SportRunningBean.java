package com.wade.fit.model.bean;

import com.wade.fit.greendao.bean.HealthActivity;

public class SportRunningBean {
    public String calorie = "0";
    public String distance = "0";
    public String duration = "0";
    public HealthActivity healthActivity;
    public String heartRate = "0";
    public LatLngBean latLngBean;
    public String pace = "0'00\"";
    public float percent;
    public String precentText;
    public int signalValue;

    public String toString() {
        return "SportRunningBean{duration='" + this.duration + '\'' + ", heartRate='" + this.heartRate + '\'' + ", calorie='" + this.calorie + '\'' + ", distance='" + this.distance + '\'' + ", pace='" + this.pace + '\'' + ", percent=" + this.percent + ", precentText='" + this.precentText + '\'' + '}';
    }
}
