package com.wade.fit.ui.mine.activity;

import android.content.Intent;
import com.wade.fit.R;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.util.AppUtil;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.PermissionUtil;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.util.thirddataplatform.GoogleFitPresenter;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.mine.activity.GoogleFitActivity */
public class GoogleFitActivity extends BaseActivity {
    private static final int REQUEST_PERMISSION_BODY_SENSORS = 100;
    ItemToggleLayout ilGoogleFit;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_google_fit;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        GoogleFitPresenter.getInstance().setActivity(this);
        this.titleName.setText(getResources().getString(R.string.google_fit));
        if (SharePreferenceUtils.getInt(this, Constants.GOOGLE_FIT_KEY, 1) == 1) {
            this.ilGoogleFit.setOpen(true);
        } else {
            this.ilGoogleFit.setOpen(false);
        }
        this.ilGoogleFit.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.mine.activity.$$Lambda$GoogleFitActivity$vGngg8fXnfluvXbjwfKYzTYiGWg */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                GoogleFitActivity.this.lambda$initView$0$GoogleFitActivity(itemToggleLayout, z);
            }
        });
    }

    public /* synthetic */ void lambda$initView$0$GoogleFitActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        if (!z) {
            SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 0);
        } else if (!AppUtil.isInstallAppByPackageName(this, "com.google.android.apps.fitness")) {
            this.ilGoogleFit.setOpen(false);
            showToast(getResources().getString(R.string.google_install));
        } else if (PermissionUtil.checkSelfPermission("android.permission.BODY_SENSORS")) {
            SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 1);
            GoogleFitPresenter.getInstance().connectGoogle();
            EventBusHelper.post(104);
        } else {
            requestPermissions(100, "android.permission.BODY_SENSORS");
        }
    }

    public void requestPermissionsSuccess(int i) {
        super.requestPermissionsSuccess(i);
        if (i == 100) {
            SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 1);
            GoogleFitPresenter.getInstance().connectGoogle();
            EventBusHelper.post(104);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        LogUtil.d("登录结果：" + i2);
        if (i2 == -1) {
            SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 1);
            GoogleFitPresenter.getInstance().handleSignInResult(i, intent);
            return;
        }
        SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 0);
        this.ilGoogleFit.setOpen(false);
        showToast(getResources().getString(R.string.bind_param_setting_fail));
    }

    public void requestPermissionsFail(int i) {
        super.requestPermissionsFail(i);
        if (i == 100) {
            this.ilGoogleFit.setOpen(false);
            this.ilGoogleFit.cancelProgressBar();
        }
    }
}
