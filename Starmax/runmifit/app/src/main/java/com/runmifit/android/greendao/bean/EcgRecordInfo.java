package com.wade.fit.greendao.bean;

import java.io.Serializable;

public class EcgRecordInfo implements Serializable {
    public static final long serialVersionUID = 111;
    private int avgHeart;
    private long date;
    private int day;
    private String desc;
    private int month;
    private int state;
    private int year;

    public EcgRecordInfo(long j, int i, int i2, int i3, int i4, int i5, String str) {
        this.date = j;
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.avgHeart = i4;
        this.state = i5;
        this.desc = str;
    }

    public EcgRecordInfo() {
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getAvgHeart() {
        return this.avgHeart;
    }

    public void setAvgHeart(int i) {
        this.avgHeart = i;
    }

    public int getState() {
        return this.state;
    }

    public void setState(int i) {
        this.state = i;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String str) {
        this.desc = str;
    }
}
