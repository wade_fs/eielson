package com.wade.fit.model.bean;

import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.bean.HealthSleepItem;
import com.wade.fit.views.MainVO;
import java.util.ArrayList;
import java.util.List;

public class SleepDetailVO {
    public List<HealthSleep> allList = new ArrayList();
    public String date;
    public List<String> dates = new ArrayList();
    public MainVO mainVO;
    public List<HealthSleepItem> sleepItemList = new ArrayList();
    public List<HealthSleep> sleepList = new ArrayList();
    public List<WeekSleep> weekSleeps = new ArrayList();

    public static class WeekSleep {
        public String avgTime;
        public List<DaySleep> daySleeps = new ArrayList();
        public boolean isShow;
        public String label;

        public String toString() {
            return "WeekSleep{label='" + this.label + '\'' + ", avgTime='" + this.avgTime + '\'' + ", isShow=" + this.isShow + ", daySleeps=" + this.daySleeps + '}';
        }
    }

    public static class DaySleep {
        public long date;
        public String day;
        public String time;
        public String totalTime;

        public String toString() {
            return "DaySleep{day='" + this.day + '\'' + ", time='" + this.time + '\'' + ", totalTime='" + this.totalTime + '\'' + '}';
        }
    }
}
