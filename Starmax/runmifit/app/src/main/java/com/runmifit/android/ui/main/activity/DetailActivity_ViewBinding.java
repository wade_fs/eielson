package com.wade.fit.ui.main.activity;

import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.DetailSportChart;

/* renamed from: com.wade.fit.ui.main.activity.DetailActivity_ViewBinding */
public class DetailActivity_ViewBinding implements Unbinder {
    private DetailActivity target;
    private View view2131296558;
    private View view2131296572;
    private View view2131296575;
    private View view2131296576;
    private View view2131296769;
    private View view2131296770;
    private View view2131296771;
    private View view2131296772;
    private View view2131297033;
    private View view2131297080;
    private View view2131297083;

    public DetailActivity_ViewBinding(DetailActivity detailActivity) {
        this(detailActivity, detailActivity.getWindow().getDecorView());
    }

    public DetailActivity_ViewBinding(final DetailActivity detailActivity, View view) {
        this.target = detailActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.ivPreDate, "field 'ivPreDate' and method 'changePreDay'");
        detailActivity.ivPreDate = (ImageView) Utils.castView(findRequiredView, R.id.ivPreDate, "field 'ivPreDate'", ImageView.class);
        this.view2131296575 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24881 */

            public void doClick(View view) {
                detailActivity.changePreDay();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ivNextDate, "field 'ivNextDate' and method 'changeNextDay'");
        detailActivity.ivNextDate = (ImageView) Utils.castView(findRequiredView2, R.id.ivNextDate, "field 'ivNextDate'", ImageView.class);
        this.view2131296572 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24912 */

            public void doClick(View view) {
                detailActivity.changeNextDay();
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.ivBack, "field 'ivBack' and method 'onClick'");
        detailActivity.ivBack = findRequiredView3;
        this.view2131296558 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24923 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
        detailActivity.tvDate = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDate, "field 'tvDate'", TextView.class);
        detailActivity.tvDay = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDay, "field 'tvDay'", TextView.class);
        View findRequiredView4 = Utils.findRequiredView(view, R.id.tvWeek, "field 'tvWeek' and method 'onClick'");
        detailActivity.tvWeek = (TextView) Utils.castView(findRequiredView4, R.id.tvWeek, "field 'tvWeek'", TextView.class);
        this.view2131297080 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24934 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
        View findRequiredView5 = Utils.findRequiredView(view, R.id.tvMonth, "field 'tvMonth' and method 'onClick'");
        detailActivity.tvMonth = (TextView) Utils.castView(findRequiredView5, R.id.tvMonth, "field 'tvMonth'", TextView.class);
        this.view2131297033 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24945 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
        View findRequiredView6 = Utils.findRequiredView(view, R.id.tvYear, "field 'tvYear' and method 'onClick'");
        detailActivity.tvYear = (TextView) Utils.castView(findRequiredView6, R.id.tvYear, "field 'tvYear'", TextView.class);
        this.view2131297083 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24956 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
        detailActivity.tvTotalTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTotalTitle, "field 'tvTotalTitle'", TextView.class);
        detailActivity.tvTotalData = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTotalData, "field 'tvTotalData'", TextView.class);
        detailActivity.tvData2Title = (TextView) Utils.findRequiredViewAsType(view, R.id.tvData2Title, "field 'tvData2Title'", TextView.class);
        detailActivity.tvData1 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvData1, "field 'tvData1'", TextView.class);
        detailActivity.tvUnit1 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvUnit1, "field 'tvUnit1'", TextView.class);
        detailActivity.tvData2 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvData2, "field 'tvData2'", TextView.class);
        detailActivity.tvUnit2 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvUnit2, "field 'tvUnit2'", TextView.class);
        detailActivity.detailSportChart = (DetailSportChart) Utils.findRequiredViewAsType(view, R.id.detailSportChart, "field 'detailSportChart'", DetailSportChart.class);
        detailActivity.tabLayout = (TabLayout) Utils.findRequiredViewAsType(view, R.id.tabLayout, "field 'tabLayout'", TabLayout.class);
        detailActivity.barBg = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.barBg, "field 'barBg'", RelativeLayout.class);
        View findRequiredView7 = Utils.findRequiredView(view, R.id.rbDay, "method 'onClick'");
        this.view2131296769 = findRequiredView7;
        findRequiredView7.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24967 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
        View findRequiredView8 = Utils.findRequiredView(view, R.id.rbOneMonth, "method 'onClick'");
        this.view2131296770 = findRequiredView8;
        findRequiredView8.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24978 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
        View findRequiredView9 = Utils.findRequiredView(view, R.id.rbSixMonth, "method 'onClick'");
        this.view2131296771 = findRequiredView9;
        findRequiredView9.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C24989 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
        View findRequiredView10 = Utils.findRequiredView(view, R.id.rbYear, "method 'onClick'");
        this.view2131296772 = findRequiredView10;
        findRequiredView10.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C248910 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
        View findRequiredView11 = Utils.findRequiredView(view, R.id.ivSelectedDate, "method 'onClick'");
        this.view2131296576 = findRequiredView11;
        findRequiredView11.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity_ViewBinding.C249011 */

            public void doClick(View view) {
                detailActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        DetailActivity detailActivity = this.target;
        if (detailActivity != null) {
            this.target = null;
            detailActivity.ivPreDate = null;
            detailActivity.ivNextDate = null;
            detailActivity.ivBack = null;
            detailActivity.tvDate = null;
            detailActivity.tvDay = null;
            detailActivity.tvWeek = null;
            detailActivity.tvMonth = null;
            detailActivity.tvYear = null;
            detailActivity.tvTotalTitle = null;
            detailActivity.tvTotalData = null;
            detailActivity.tvData2Title = null;
            detailActivity.tvData1 = null;
            detailActivity.tvUnit1 = null;
            detailActivity.tvData2 = null;
            detailActivity.tvUnit2 = null;
            detailActivity.detailSportChart = null;
            detailActivity.tabLayout = null;
            detailActivity.barBg = null;
            this.view2131296575.setOnClickListener(null);
            this.view2131296575 = null;
            this.view2131296572.setOnClickListener(null);
            this.view2131296572 = null;
            this.view2131296558.setOnClickListener(null);
            this.view2131296558 = null;
            this.view2131297080.setOnClickListener(null);
            this.view2131297080 = null;
            this.view2131297033.setOnClickListener(null);
            this.view2131297033 = null;
            this.view2131297083.setOnClickListener(null);
            this.view2131297083 = null;
            this.view2131296769.setOnClickListener(null);
            this.view2131296769 = null;
            this.view2131296770.setOnClickListener(null);
            this.view2131296770 = null;
            this.view2131296771.setOnClickListener(null);
            this.view2131296771 = null;
            this.view2131296772.setOnClickListener(null);
            this.view2131296772 = null;
            this.view2131296576.setOnClickListener(null);
            this.view2131296576 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
