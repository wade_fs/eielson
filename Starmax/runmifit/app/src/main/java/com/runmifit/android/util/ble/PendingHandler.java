package com.wade.fit.util.ble;

import android.os.Handler;
import android.os.Looper;

public class PendingHandler extends Handler {
    private boolean mPending = false;

    public PendingHandler(Looper looper) {
        super(looper);
    }

    public synchronized boolean postT(final Runnable runnable) {
        boolean z;
        z = false;
        if (!this.mPending && (z = post(new Runnable() {
            /* class com.wade.fit.util.ble.PendingHandler.C26851 */

            public void run() {
                runnable.run();
                PendingHandler.this.pending(false);
            }
        }))) {
            this.mPending = true;
        }
        return z;
    }

    public synchronized boolean postF(final Runnable runnable) {
        boolean post;
        removeCallbacksAndMessages(null);
        post = post(new Runnable() {
            /* class com.wade.fit.util.ble.PendingHandler.C26862 */

            public void run() {
                runnable.run();
                PendingHandler.this.pending(false);
            }
        });
        if (post) {
            this.mPending = true;
        }
        return post;
    }

    public synchronized boolean pending() {
        return this.mPending;
    }

    /* access modifiers changed from: private */
    public synchronized void pending(boolean z) {
        this.mPending = z;
    }
}
