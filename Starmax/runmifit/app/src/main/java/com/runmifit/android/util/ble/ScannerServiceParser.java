package com.wade.fit.util.ble;

import android.util.Log;
import com.google.common.primitives.UnsignedBytes;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

public class ScannerServiceParser {
    private static final int COMPLETE_LOCAL_NAME = 9;
    private static final int FLAGS_BIT = 1;
    private static final byte LE_GENERAL_DISCOVERABLE_MODE = 2;
    private static final byte LE_LIMITED_DISCOVERABLE_MODE = 1;
    private static final int SERVICES_COMPLETE_LIST_128_BIT = 7;
    private static final int SERVICES_COMPLETE_LIST_16_BIT = 3;
    private static final int SERVICES_COMPLETE_LIST_32_BIT = 5;
    private static final int SERVICES_MORE_AVAILABLE_128_BIT = 6;
    private static final int SERVICES_MORE_AVAILABLE_16_BIT = 2;
    private static final int SERVICES_MORE_AVAILABLE_32_BIT = 4;
    private static final int SHORTENED_LOCAL_NAME = 8;
    private static final String TAG = "ScannerServiceParser";

    public static boolean decodeDeviceAdvData(byte[] bArr, UUID uuid) {
        String uuid2 = uuid != null ? uuid.toString() : null;
        if (bArr == null) {
            return false;
        }
        boolean z = uuid2 == null;
        int length = bArr.length;
        boolean z2 = z;
        int i = 0;
        boolean z3 = false;
        while (i < length) {
            byte b = bArr[i];
            if (b != 0) {
                int i2 = i + 1;
                byte b2 = bArr[i2];
                if (uuid2 != null) {
                    if (b2 == 2 || b2 == 3) {
                        for (int i3 = i2 + 1; i3 < (i2 + b) - 1; i3 += 2) {
                            z2 = z2 || decodeService16BitUUID(uuid2, bArr, i3, 2);
                        }
                    } else if (b2 == 4 || b2 == 5) {
                        for (int i4 = i2 + 1; i4 < (i2 + b) - 1; i4 += 4) {
                            z2 = z2 || decodeService32BitUUID(uuid2, bArr, i4, 4);
                        }
                    } else if (b2 == 6 || b2 == 7) {
                        for (int i5 = i2 + 1; i5 < (i2 + b) - 1; i5 += 16) {
                            z2 = z2 || decodeService128BitUUID(uuid2, bArr, i5, 16);
                        }
                    }
                }
                if (b2 == 1) {
                    z3 = (bArr[i2 + 1] & 3) > 0;
                }
                i = i2 + (b - 1) + 1;
            } else if (!z3 || !z2) {
                return false;
            } else {
                return true;
            }
        }
        if (!z3 || !z2) {
            return false;
        }
        return true;
    }

    public static String decodeDeviceName(byte[] bArr) {
        int length = bArr.length;
        int i = 0;
        while (i < length) {
            byte b = bArr[i];
            if (b == 0) {
                break;
            }
            int i2 = i + 1;
            byte b2 = bArr[i2];
            if (b2 == 9 || b2 == 8) {
                return decodeLocalName(bArr, i2 + 1, b - 1);
            }
            i = i2 + (b - 1) + 1;
        }
        return null;
    }

    public static byte[] decodeManufacturer(byte[] bArr) {
        byte[] bArr2 = new byte[62];
        int length = bArr.length;
        int i = 0;
        while (i < length) {
            byte b = bArr[i];
            if (b == 0) {
                break;
            }
            int i2 = i + 1;
            if (bArr[i2] == -1) {
                int i3 = b - 1;
                ByteDataConvertUtil.BinnCat(bArr, bArr2, i2 + 1, i3);
                byte[] bArr3 = new byte[i3];
                ByteDataConvertUtil.BinnCat(bArr2, bArr3, 0, i3);
                return bArr3;
            }
            i = i2 + (b - 1) + 1;
        }
        return null;
    }

    public static String decodeLocalName(byte[] bArr, int i, int i2) {
        try {
            return new String(bArr, i, i2, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.d(TAG, "Unable to convert the complete local name to UTF-8", e);
            return null;
        } catch (IndexOutOfBoundsException e2) {
            Log.d(TAG, "Error when reading complete local name", e2);
            return null;
        }
    }

    private static boolean decodeService16BitUUID(String str, byte[] bArr, int i, int i2) {
        String format = String.format("%04x", Integer.valueOf(decodeUuid16(bArr, i)));
        String substring = str.substring(4, 8);
        Log.v(TAG, substring + "--16--" + format);
        return format.equalsIgnoreCase(substring);
    }

    private static boolean decodeService32BitUUID(String str, byte[] bArr, int i, int i2) {
        String format = String.format("%04x", Integer.valueOf(decodeUuid16(bArr, (i + i2) - 4)));
        String substring = str.substring(4, 8);
        Log.v(TAG, substring + "--32--" + format);
        return format.equalsIgnoreCase(substring);
    }

    private static boolean decodeService128BitUUID(String str, byte[] bArr, int i, int i2) {
        return String.format("%04x", Integer.valueOf(decodeUuid16(bArr, (i + i2) - 4))).equalsIgnoreCase(str.substring(4, 8));
    }

    private static int decodeUuid16(byte[] bArr, int i) {
        return ((bArr[i + 1] & UnsignedBytes.MAX_VALUE) << 8) | ((bArr[i] & UnsignedBytes.MAX_VALUE) << 0);
    }
}
