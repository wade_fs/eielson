package com.wade.fit.ui.device.activity;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.device.activity.AppNoticeActivity_ViewBinding */
public class AppNoticeActivity_ViewBinding implements Unbinder {
    private AppNoticeActivity target;

    public AppNoticeActivity_ViewBinding(AppNoticeActivity appNoticeActivity) {
        this(appNoticeActivity, appNoticeActivity.getWindow().getDecorView());
    }

    public AppNoticeActivity_ViewBinding(AppNoticeActivity appNoticeActivity, View view) {
        this.target = appNoticeActivity;
        appNoticeActivity.itQQ = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itQQ, "field 'itQQ'", ItemToggleLayout.class);
        appNoticeActivity.itFacebook = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itFacebook, "field 'itFacebook'", ItemToggleLayout.class);
        appNoticeActivity.itWeChat = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itWeChat, "field 'itWeChat'", ItemToggleLayout.class);
        appNoticeActivity.itLinked = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itLinked, "field 'itLinked'", ItemToggleLayout.class);
        appNoticeActivity.itSkype = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itSkype, "field 'itSkype'", ItemToggleLayout.class);
        appNoticeActivity.itInstagram = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itInstagram, "field 'itInstagram'", ItemToggleLayout.class);
        appNoticeActivity.itTwitter = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itTwitter, "field 'itTwitter'", ItemToggleLayout.class);
        appNoticeActivity.itLine = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itLine, "field 'itLine'", ItemToggleLayout.class);
        appNoticeActivity.itWhatsApp = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itWhatsApp, "field 'itWhatsApp'", ItemToggleLayout.class);
        appNoticeActivity.itVK = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itVK, "field 'itVK'", ItemToggleLayout.class);
        appNoticeActivity.itMessenger = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.itMessenger, "field 'itMessenger'", ItemToggleLayout.class);
    }

    public void unbind() {
        AppNoticeActivity appNoticeActivity = this.target;
        if (appNoticeActivity != null) {
            this.target = null;
            appNoticeActivity.itQQ = null;
            appNoticeActivity.itFacebook = null;
            appNoticeActivity.itWeChat = null;
            appNoticeActivity.itLinked = null;
            appNoticeActivity.itSkype = null;
            appNoticeActivity.itInstagram = null;
            appNoticeActivity.itTwitter = null;
            appNoticeActivity.itLine = null;
            appNoticeActivity.itWhatsApp = null;
            appNoticeActivity.itVK = null;
            appNoticeActivity.itMessenger = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
