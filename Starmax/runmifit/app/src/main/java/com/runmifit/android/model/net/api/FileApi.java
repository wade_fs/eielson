package com.wade.fit.model.net.api;

import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.ImageResult;
import okhttp3.RequestBody;
import p041io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface FileApi {
    @POST("user/uploadImage")
    Observable<BaseBean<ImageResult>> uploadFile(@Body RequestBody requestBody);
}
