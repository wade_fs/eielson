package com.wade.fit.base;

import com.wade.fit.base.IBaseView;
import p041io.reactivex.disposables.CompositeDisposable;
import p041io.reactivex.disposables.Disposable;

public abstract class BasePersenter<V extends IBaseView> {
    private CompositeDisposable mCompositeDisposable;
    /* access modifiers changed from: protected */
    public V mView;

    public void attachView(V v) {
        this.mView = v;
    }

    public void detachView() {
        this.mView = null;
        dispose();
    }

    /* access modifiers changed from: protected */
    public void addDisposable(Disposable disposable) {
        CompositeDisposable compositeDisposable = this.mCompositeDisposable;
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            this.mCompositeDisposable = new CompositeDisposable();
        }
        this.mCompositeDisposable.add(disposable);
    }

    public void dispose() {
        CompositeDisposable compositeDisposable = this.mCompositeDisposable;
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
