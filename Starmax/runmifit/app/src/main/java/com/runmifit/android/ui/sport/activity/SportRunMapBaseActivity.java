package com.wade.fit.ui.sport.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.wade.fit.base.BaseMap;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.base.BaseSportPresenter;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.model.bean.LatLngBean;
import com.wade.fit.persenter.sport.ISportRunView;
import com.wade.fit.service.LocationService;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.map.MapFactory;

/* renamed from: com.wade.fit.ui.sport.activity.SportRunMapBaseActivity */
public abstract class SportRunMapBaseActivity<P extends BaseSportPresenter> extends BaseMvpActivity<P> implements ISportRunView {
    protected boolean isLocation = true;
    protected BaseMap mapModel;
    protected int mapSource = 0;
    protected View mapView;

    public void initView() {
        super.initView();
        LogUtil.d("initViews...");
        this.mapView = MapFactory.getMapView(this);
    }

    public void onCreate(Bundle bundle) {
        this.mapModel = MapFactory.getMap();
        this.mapModel.setActivity(this);
        super.onCreate(bundle);
        this.mapModel.initMapView(this.mapView);
        this.mapModel.onCreate(bundle);
        LogUtil.d("onCreate isLocation:" + this.isLocation);
        if (this.isLocation) {
            startService(new Intent(this, LocationService.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mapModel.onResume();
    }

    public void handleMessage(BaseMessage baseMessage) {
        LatLngBean latLngBean;
        super.handleMessage(baseMessage);
        if (baseMessage.getType() == 202 && (latLngBean = (LatLngBean) baseMessage.getData()) != null) {
            this.mapModel.addPolylineAndMove(latLngBean, false);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mapModel.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mapModel.onDestroy();
        LogUtil.d("onDestroy isLocation:" + this.isLocation);
        if (this.isLocation) {
            stopService(new Intent(this, LocationService.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.mapModel.onSaveInstanceState(bundle);
    }
}
