package com.wade.fit.views;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.wade.fit.R;
import java.util.ArrayList;
import java.util.List;

public class ViewPagerIndicator extends LinearLayout implements ViewPager.OnAdapterChangeListener, ViewPager.OnPageChangeListener {
    boolean isRegister;
    /* access modifiers changed from: private */
    public int mCount;
    private DataSetObserver mDataSetObserver;
    private int mDotPadding;
    private List<View> mIndicators;
    /* access modifiers changed from: private */
    public PagerAdapter mPageAdapter;
    private int mSize;
    private ViewPager mViewPager;

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public ViewPagerIndicator(Context context) {
        this(context, null);
    }

    public ViewPagerIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, -1);
    }

    public ViewPagerIndicator(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mSize = 10;
        this.mDotPadding = 10;
        this.mDataSetObserver = new DataSetObserver() {
            /* class com.wade.fit.views.ViewPagerIndicator.C27181 */

            public void onChanged() {
                super.onChanged();
                if (ViewPagerIndicator.this.mPageAdapter != null && ViewPagerIndicator.this.mCount != ViewPagerIndicator.this.mPageAdapter.getCount()) {
                    ViewPagerIndicator.this.setIndicators();
                }
            }
        };
        this.isRegister = false;
        init();
    }

    private void init() {
        this.mIndicators = new ArrayList();
        setGravity(17);
    }

    public void setViewPager(ViewPager viewPager) {
        if (viewPager != null) {
            this.mViewPager = viewPager;
            this.mPageAdapter = viewPager.getAdapter();
            this.mViewPager.removeOnAdapterChangeListener(this);
            this.mViewPager.addOnAdapterChangeListener(this);
            this.mViewPager.addOnPageChangeListener(this);
            if (this.mPageAdapter != null) {
                setIndicators();
            }
        }
    }

    public void onAdapterChanged(ViewPager viewPager, PagerAdapter pagerAdapter, PagerAdapter pagerAdapter2) {
        this.mPageAdapter = pagerAdapter2;
        setIndicators();
    }

    /* access modifiers changed from: private */
    public void setIndicators() {
        this.mCount = this.mPageAdapter.getCount();
        if (this.mCount > 0) {
            removeAllViews();
            this.mIndicators.clear();
            for (int i = 0; i < this.mCount; i++) {
                addIndicator(i);
            }
        }
    }

    private void addIndicator(int i) {
        ImageView imageView = new ImageView(getContext());
        int i2 = this.mSize;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(i2, i2);
        int i3 = this.mDotPadding;
        layoutParams.leftMargin = i3 / 2;
        layoutParams.rightMargin = i3 / 2;
        imageView.setLayoutParams(layoutParams);
        imageView.setBackground(getResources().getDrawable(R.drawable.dot_selector));
        imageView.setSelected(i == 0);
        this.mIndicators.add(imageView);
        addView(imageView);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewPager viewPager = this.mViewPager;
        if (viewPager != null && viewPager.getAdapter() != null && !this.isRegister) {
            this.isRegister = true;
            this.mPageAdapter.registerDataSetObserver(this.mDataSetObserver);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        ViewPager viewPager = this.mViewPager;
        if (!(viewPager == null || viewPager.getAdapter() == null || !this.isRegister)) {
            this.isRegister = false;
            this.mViewPager.getAdapter().unregisterDataSetObserver(this.mDataSetObserver);
        }
        super.onDetachedFromWindow();
    }

    public void onPageSelected(int i) {
        int i2 = 0;
        while (i2 < this.mCount) {
            this.mIndicators.get(i2).setSelected(i2 == i);
            i2++;
        }
    }
}
