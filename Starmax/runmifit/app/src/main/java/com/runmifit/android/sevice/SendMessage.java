package com.wade.fit.service;

public class SendMessage {
    private String mText;
    private String mTitle;
    private int mType;

    public int getmType() {
        return this.mType;
    }

    public void setmType(int i) {
        this.mType = i;
    }

    public String getmTitle() {
        return this.mTitle;
    }

    public void setmTitle(String str) {
        this.mTitle = str;
    }

    public String getmText() {
        return this.mText;
    }

    public void setmText(String str) {
        this.mText = str;
    }
}
