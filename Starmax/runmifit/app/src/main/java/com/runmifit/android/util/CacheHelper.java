package com.wade.fit.util;

import android.util.SparseArray;
import com.bumptech.glide.Glide;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.util.image.PhotoHelper;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CacheHelper implements Constants {
    private static CacheHelper cacheHelper = new CacheHelper();
    private static boolean isTestShowWeight = false;
    private static SparseArray<String> sportType = new SparseArray<>();
    public static final String touristUserId = "123456";
    private List<Date> dateList = new ArrayList();
    private boolean isAutoLogin;
    private boolean isJumpUnit;
    private boolean isLogin;
    private boolean isLoginShowBackUp = false;
    public boolean isMonekyTest = false;
    public boolean isNeedRefreshTimeLineData;
    private String userId;

    public void initData() {
    }

    static {
        sportType.put(4, AppApplication.getInstance().getResources().getString(R.string.sport_type_run_in_door));
        sportType.put(5, AppApplication.getInstance().getResources().getString(R.string.sport_type_train));
        sportType.put(9, AppApplication.getInstance().getResources().getString(R.string.sport_type2_rope_skipping));
        sportType.put(2, AppApplication.getInstance().getResources().getString(R.string.sport_type0_swim));
        sportType.put(7, AppApplication.getInstance().getResources().getString(R.string.sport_type2_basketball));
        sportType.put(8, AppApplication.getInstance().getResources().getString(R.string.sport_type0_badminton));
        sportType.put(0, AppApplication.getInstance().getResources().getString(R.string.sport_type0_walk));
        sportType.put(1, AppApplication.getInstance().getResources().getString(R.string.sport_type0_run));
        sportType.put(3, AppApplication.getInstance().getResources().getString(R.string.sport_type0_by_bike));
        sportType.put(6, AppApplication.getInstance().getResources().getString(R.string.sport_type2_footballl));
        sportType.put(10, AppApplication.getInstance().getResources().getString(R.string.sport_type1_sit_up));
        sportType.put(11, AppApplication.getInstance().getResources().getString(R.string.sport_type1_push_up));
        sportType.put(12, AppApplication.getInstance().getResources().getString(R.string.sport_type0_mountain_climbing));
    }

    public static String getSportName(int i) {
        sportType.clear();
        sportType.put(4, AppApplication.getInstance().getResources().getString(R.string.sport_type_run_in_door));
        sportType.put(5, AppApplication.getInstance().getResources().getString(R.string.sport_type_train));
        sportType.put(9, AppApplication.getInstance().getResources().getString(R.string.sport_type2_rope_skipping));
        sportType.put(2, AppApplication.getInstance().getResources().getString(R.string.sport_type0_swim));
        sportType.put(7, AppApplication.getInstance().getResources().getString(R.string.sport_type2_basketball));
        sportType.put(8, AppApplication.getInstance().getResources().getString(R.string.sport_type0_badminton));
        sportType.put(0, AppApplication.getInstance().getResources().getString(R.string.sport_type0_walk));
        sportType.put(1, AppApplication.getInstance().getResources().getString(R.string.sport_type0_run));
        sportType.put(3, AppApplication.getInstance().getResources().getString(R.string.sport_type0_by_bike));
        sportType.put(6, AppApplication.getInstance().getResources().getString(R.string.sport_type2_footballl));
        sportType.put(10, AppApplication.getInstance().getResources().getString(R.string.sport_type1_sit_up));
        sportType.put(11, AppApplication.getInstance().getResources().getString(R.string.sport_type1_push_up));
        sportType.put(12, AppApplication.getInstance().getResources().getString(R.string.sport_type0_mountain_climbing));
        String str = sportType.get(i);
        return str == null ? "" : str;
    }

    public void setNeedRefreshTimeLineData(boolean z) {
        this.isNeedRefreshTimeLineData = z;
    }

    public boolean isAutoLogin() {
        return this.isAutoLogin;
    }

    public void setAutoLogin(boolean z) {
        this.isAutoLogin = z;
    }

    public void setLoginShowBackUp(boolean z) {
        this.isLoginShowBackUp = z;
    }

    public boolean isLoginShowBackUp() {
        return this.isLoginShowBackUp;
    }

    public List<Date> getDateList() {
        return this.dateList;
    }

    private CacheHelper() {
        DateUtil.todayYearMonthDay();
        initData();
    }

    public void setLogin(boolean z) {
        this.isLogin = z;
    }

    public void loginOut() {
        setLogin(false);
        this.userId = null;
        SharePreferenceUtils.put(AppApplication.getContext(), Constants.IS_AUTO_LOGIN, false);
        SharePreferenceUtils.put(AppApplication.getContext(), Constants.IS_LOGIN_STATE, false);
        SharePreferenceUtils.put(AppApplication.getContext(), Constants.IS_TOURIST_STATE, false);
        new File(PIC_PATH + PhotoHelper.photoPath).delete();
        Glide.get(AppApplication.getContext()).clearMemory();
        new Thread($$Lambda$CacheHelper$MOcWlC3vj2Rw0khlMEERXoqMyg.INSTANCE).start();
        AppApplication.finishAll();
    }

    public boolean isLogin() {
        return ((Boolean) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.IS_LOGIN_STATE, false)).booleanValue();
    }

    public boolean isTourist() {
        return ((Boolean) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.IS_TOURIST_STATE, false)).booleanValue();
    }

    public String getUserId() {
        String str = this.userId;
        if (str != null) {
            return str;
        }
        if (!this.isLogin) {
            return null;
        }
        AppApplication.getInstance().getUserBean();
        return this.userId;
    }

    public static CacheHelper getInstance() {
        return cacheHelper;
    }

    public boolean isJumpUnit() {
        return this.isJumpUnit;
    }

    public void setJumpUnit(boolean z) {
        this.isJumpUnit = z;
    }
}
