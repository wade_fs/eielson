package com.wade.fit.wxapi;

import android.os.Bundle;
import com.orhanobut.logger.Logger;
import com.tencent.p055mm.opensdk.modelbase.BaseReq;
import com.tencent.p055mm.opensdk.modelbase.BaseResp;
import com.tencent.p055mm.opensdk.openapi.IWXAPIEventHandler;
import p004cn.sharesdk.wechat.utils.WechatHandlerActivity;

public class WXEntryActivity extends WechatHandlerActivity implements IWXAPIEventHandler {
    public void onReq(BaseReq baseReq) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public void onResp(BaseResp baseResp) {
        Logger.m6177d("错误码 : " + baseResp.errCode + "");
        int i = baseResp.errCode;
        if (i == -4) {
            finish();
        } else if (i != -2) {
            if (i == 0) {
                finish();
                return;
            }
            return;
        }
        finish();
    }
}
