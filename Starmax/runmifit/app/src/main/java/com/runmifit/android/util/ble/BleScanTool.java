package com.wade.fit.util.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.wade.fit.model.bean.BLEDevice;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class BleScanTool {
    public static final UUID RX_SERVICE_UUID = UUID.fromString("00000af0-0000-1000-8000-00805f9b34fb");
    public static final UUID RX_UPDATE_UUID = UUID.fromString("00001530-1212-efde-1523-785feabcd123");
    private static BleScanTool instance = null;
    private boolean filterByService;
    private boolean isScaning = false;
    public ConcurrentHashMap<String, BLEDevice> mBleDeviceMap = new ConcurrentHashMap<>();
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;
    private Context mContext;
    private DFUServiceParser mDFUServiceParser = DFUServiceParser.getDFUParser();
    private CopyOnWriteArrayList<String> mFilterNameList = new CopyOnWriteArrayList<>();
    private Handler mHandler = new Handler();
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        /* class com.wade.fit.util.ble.$$Lambda$BleScanTool$tBFsZ6Q5eOVenxxiQtyEvWeHWAo */

        public final void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
            BleScanTool.this.lambda$new$1$BleScanTool(bluetoothDevice, i, bArr);
        }
    };
    /* access modifiers changed from: private */
    public CopyOnWriteArrayList<ScanDeviceListener> mScanDeviceListenerList = new CopyOnWriteArrayList<>();
    private UUID requiredUUID = UUID.fromString("00000af0-0000-1000-8000-00805f9b34fb");

    public interface ScanDeviceListener {
        void onFind(BLEDevice bLEDevice);

        void onFinish();
    }

    public void addFilterNameForNum(String str) {
    }

    public /* synthetic */ void lambda$new$1$BleScanTool(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
        try {
            BLEDevice bLEDevice = new BLEDevice();
            String name = bluetoothDevice.getName();
            String address = bluetoothDevice.getAddress();
            if (!this.mBleDeviceMap.containsKey(address) && !TextUtils.isEmpty(name)) {
                bLEDevice.mDeviceName = name;
                bLEDevice.mDeviceAddress = address;
                bLEDevice.mRssi = i;
                this.mBleDeviceMap.put(address, bLEDevice);
                this.mHandler.post(new Runnable(bLEDevice) {
                    /* class com.wade.fit.util.ble.$$Lambda$BleScanTool$gE29sqCNt9xapFUxB37hNdct_Jg */
                    private final /* synthetic */ BLEDevice f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void run() {
                        BleScanTool.this.lambda$null$0$BleScanTool(this.f$1);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public /* synthetic */ void lambda$null$0$BleScanTool(BLEDevice bLEDevice) {
        Iterator<ScanDeviceListener> it = this.mScanDeviceListenerList.iterator();
        while (it.hasNext()) {
            it.next().onFind(bLEDevice);
        }
    }

    public void init(Context context) {
        this.mContext = context;
        reset();
    }

    public void reset() {
        Context context = this.mContext;
        if (context != null) {
            this.mBluetoothManager = (BluetoothManager) context.getSystemService("bluetooth");
            BluetoothManager bluetoothManager = this.mBluetoothManager;
            if (bluetoothManager != null) {
                this.mBluetoothAdapter = bluetoothManager.getAdapter();
            }
        }
    }

    public boolean isSupportBLE() {
        return this.mContext.getPackageManager().hasSystemFeature("android.hardware.bluetooth_le");
    }

    public boolean isBluetoothOpen() {
        BluetoothAdapter bluetoothAdapter = this.mBluetoothAdapter;
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled();
    }

    public boolean openBluetooth() {
        BluetoothAdapter bluetoothAdapter = this.mBluetoothAdapter;
        return bluetoothAdapter != null && bluetoothAdapter.enable();
    }

    public BluetoothAdapter getBluetoothAdapter() {
        return this.mBluetoothAdapter;
    }

    public BluetoothManager getBluetoothManager() {
        return this.mBluetoothManager;
    }

    public boolean isScanning() {
        return this.isScaning;
    }

    public void addFilterName(String str) {
        if (str != null && !this.mFilterNameList.contains(str)) {
            this.mFilterNameList.add(str);
        }
    }

    public void removeFilterName(String str) {
        if (str != null && !this.mFilterNameList.isEmpty()) {
            this.mFilterNameList.remove(str);
        }
    }

    public void clearFilterName() {
        Iterator<String> it = this.mFilterNameList.iterator();
        while (it.hasNext()) {
            this.mFilterNameList.remove(it.next());
        }
    }

    public void addScanDeviceListener(ScanDeviceListener scanDeviceListener) {
        if (scanDeviceListener != null && !this.mScanDeviceListenerList.contains(scanDeviceListener)) {
            this.mScanDeviceListenerList.add(scanDeviceListener);
        }
    }

    public void removeScanDeviceListener(ScanDeviceListener scanDeviceListener) {
        if (scanDeviceListener != null && !this.mScanDeviceListenerList.isEmpty()) {
            this.mScanDeviceListenerList.remove(scanDeviceListener);
        }
    }

    public void clearScanDeviceListener() {
        Iterator<ScanDeviceListener> it = this.mScanDeviceListenerList.iterator();
        while (it.hasNext()) {
            this.mScanDeviceListenerList.remove(it.next());
        }
    }

    public void scanLeDevice(boolean z, long j) {
        BluetoothAdapter bluetoothAdapter;
        if (!isSupportBLE() || !isBluetoothOpen() || (bluetoothAdapter = this.mBluetoothAdapter) == null || !bluetoothAdapter.isEnabled()) {
            return;
        }
        if (!z) {
            this.mHandler.removeCallbacksAndMessages(null);
            stopScan();
        } else if (!this.isScaning) {
            this.mHandler.removeCallbacksAndMessages(null);
            this.isScaning = true;
            this.mBleDeviceMap.clear();
            if (!this.mBluetoothAdapter.startLeScan(new UUID[]{BleGattAttributes.SERVICE_UUID}, this.mLeScanCallback)) {
                stopScan();
            } else {
                this.mHandler.postDelayed(new Runnable() {
                    /* class com.wade.fit.util.ble.$$Lambda$BleScanTool$0RZwwSGkakhOWKBXEGByr3QAi_E */

                    public final void run() {
                        BleScanTool.this.stopScan();
                    }
                }, j);
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:13|14|15) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:(2:20|21)|(2:24|25)|26|27|36|28|17) */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r0 = getDeclaredMethod(r2, "stopScan", java.lang.Integer.TYPE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        r6 = true;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0033 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0065 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean releaseAllScanClient() {
        /*
            r10 = this;
            java.lang.String r0 = "stopScan"
            r1 = 0
            android.bluetooth.BluetoothAdapter r2 = android.bluetooth.BluetoothAdapter.getDefaultAdapter()     // Catch:{ Exception -> 0x0087 }
            java.lang.Object r2 = getIBluetoothManager(r2)     // Catch:{ Exception -> 0x0087 }
            if (r2 != 0) goto L_0x000e
            return r1
        L_0x000e:
            java.lang.Object r2 = getIBluetoothGatt(r2)     // Catch:{ Exception -> 0x0087 }
            if (r2 != 0) goto L_0x0015
            return r1
        L_0x0015:
            java.lang.String r3 = "unregisterClient"
            r4 = 1
            java.lang.Class[] r5 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0087 }
            java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0087 }
            r5[r1] = r6     // Catch:{ Exception -> 0x0087 }
            java.lang.reflect.Method r3 = getDeclaredMethod(r2, r3, r5)     // Catch:{ Exception -> 0x0087 }
            r5 = 2
            java.lang.Class[] r6 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x0033 }
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0033 }
            r6[r1] = r7     // Catch:{ Exception -> 0x0033 }
            java.lang.Class r7 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x0033 }
            r6[r4] = r7     // Catch:{ Exception -> 0x0033 }
            java.lang.reflect.Method r0 = getDeclaredMethod(r2, r0, r6)     // Catch:{ Exception -> 0x0033 }
            r6 = 0
            goto L_0x003e
        L_0x0033:
            java.lang.Class[] r6 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0087 }
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0087 }
            r6[r1] = r7     // Catch:{ Exception -> 0x0087 }
            java.lang.reflect.Method r0 = getDeclaredMethod(r2, r0, r6)     // Catch:{ Exception -> 0x0087 }
            r6 = 1
        L_0x003e:
            r7 = 0
        L_0x003f:
            r8 = 40
            if (r7 > r8) goto L_0x0073
            if (r6 != 0) goto L_0x0058
            java.lang.Object[] r8 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0057 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0057 }
            r8[r1] = r9     // Catch:{ Exception -> 0x0057 }
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0057 }
            r8[r4] = r9     // Catch:{ Exception -> 0x0057 }
            r0.invoke(r2, r8)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0058
        L_0x0057:
        L_0x0058:
            if (r6 != r4) goto L_0x0065
            java.lang.Object[] r8 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0065 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0065 }
            r8[r1] = r9     // Catch:{ Exception -> 0x0065 }
            r0.invoke(r2, r8)     // Catch:{ Exception -> 0x0065 }
        L_0x0065:
            java.lang.Object[] r8 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0070 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0070 }
            r8[r1] = r9     // Catch:{ Exception -> 0x0070 }
            r3.invoke(r2, r8)     // Catch:{ Exception -> 0x0070 }
        L_0x0070:
            int r7 = r7 + 1
            goto L_0x003f
        L_0x0073:
            r0.setAccessible(r1)     // Catch:{ Exception -> 0x0087 }
            r3.setAccessible(r1)     // Catch:{ Exception -> 0x0087 }
            java.lang.String r0 = "unregAll"
            java.lang.Class[] r3 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0087 }
            java.lang.reflect.Method r0 = getDeclaredMethod(r2, r0, r3)     // Catch:{ Exception -> 0x0087 }
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0087 }
            r0.invoke(r2, r3)     // Catch:{ Exception -> 0x0087 }
            return r4
        L_0x0087:
            r0 = move-exception
            r0.printStackTrace()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.ble.BleScanTool.releaseAllScanClient():boolean");
    }

    public static Object getIBluetoothGatt(Object obj) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return getDeclaredMethod(obj, "getBluetoothGatt", new Class[0]).invoke(obj, new Object[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.ble.BleScanTool.getDeclaredMethod(java.lang.Class<?>, java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method
     arg types: [java.lang.Class, java.lang.String, java.lang.Class[]]
     candidates:
      com.wade.fit.util.ble.BleScanTool.getDeclaredMethod(java.lang.Object, java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method
      com.wade.fit.util.ble.BleScanTool.getDeclaredMethod(java.lang.Class<?>, java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method */
    public static Object getIBluetoothManager(BluetoothAdapter bluetoothAdapter) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return getDeclaredMethod((Class<?>) BluetoothAdapter.class, "getBluetoothManager", (Class<?>[]) new Class[0]).invoke(bluetoothAdapter, new Object[0]);
    }

    public static Field getDeclaredField(Class<?> cls, String str) throws NoSuchFieldException {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        return declaredField;
    }

    public static Method getDeclaredMethod(Class<?> cls, String str, Class<?>... clsArr) throws NoSuchMethodException {
        Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }

    public static Method getDeclaredMethod(Object obj, String str, Class<?>... clsArr) throws NoSuchMethodException {
        Method declaredMethod = obj.getClass().getDeclaredMethod(str, clsArr);
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }

    /* access modifiers changed from: private */
    public void stopScan() {
        if (this.isScaning) {
            this.mBluetoothAdapter.stopLeScan(this.mLeScanCallback);
            this.isScaning = false;
            this.mHandler.post(new Runnable() {
                /* class com.wade.fit.util.ble.BleScanTool.C26641 */

                public void run() {
                    Iterator it = BleScanTool.this.mScanDeviceListenerList.iterator();
                    while (it.hasNext()) {
                        ((ScanDeviceListener) it.next()).onFinish();
                    }
                }
            });
        }
    }

    public void scanLeDeviceByService(boolean z, UUID uuid, long j) {
        this.requiredUUID = uuid;
        this.filterByService = z;
        scanLeDevice(z, j);
    }

    public static synchronized BleScanTool getInstance() {
        BleScanTool bleScanTool;
        synchronized (BleScanTool.class) {
            if (instance == null) {
                instance = new BleScanTool();
            }
            bleScanTool = instance;
        }
        return bleScanTool;
    }

    private BleScanTool() {
    }
}
