package com.wade.fit.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.log.DebugLog;

public class DetailBaseView extends View {
    private static int TOUCH_SLOP;
    private boolean isHandlerTouch;
    private IMoveListener moveListener;
    float touchX1 = 0.0f;
    private VelocityTracker vTracker = null;

    public interface IMoveListener {
        void getNext();

        void getPre();
    }

    public IMoveListener getMoveListener() {
        return this.moveListener;
    }

    public void setOnMoveListener(IMoveListener iMoveListener) {
        this.moveListener = iMoveListener;
    }

    public DetailBaseView(Context context) {
        super(context);
        init();
    }

    public DetailBaseView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public DetailBaseView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        TOUCH_SLOP = ScreenUtil.dp2px(8.0f);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        IMoveListener iMoveListener;
        IMoveListener iMoveListener2;
        int action = motionEvent.getAction();
        if (action == 0) {
            this.isHandlerTouch = true;
            this.touchX1 = (float) ((int) motionEvent.getX());
            VelocityTracker velocityTracker = this.vTracker;
            if (velocityTracker == null) {
                this.vTracker = VelocityTracker.obtain();
            } else {
                velocityTracker.clear();
            }
            this.vTracker.addMovement(motionEvent);
            this.vTracker.computeCurrentVelocity(1000);
        } else if (action == 2) {
            int x = (int) motionEvent.getX();
            this.vTracker.addMovement(motionEvent);
            this.vTracker.computeCurrentVelocity(1000);
            float xVelocity = this.vTracker.getXVelocity();
            if (!this.isHandlerTouch) {
                return false;
            }
            if (xVelocity > 200.0f && (iMoveListener2 = this.moveListener) != null) {
                iMoveListener2.getPre();
            }
            if (xVelocity < -200.0f && (iMoveListener = this.moveListener) != null) {
                iMoveListener.getNext();
            }
            this.isHandlerTouch = false;
            DebugLog.m6203d("touchX2-touchX1=" + (((float) x) - this.touchX1));
        }
        invalidate();
        return true;
    }
}
