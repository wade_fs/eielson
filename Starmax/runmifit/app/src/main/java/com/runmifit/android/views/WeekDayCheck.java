package com.wade.fit.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.wade.fit.R;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.WeekUtil;
import com.wade.fit.util.log.DebugLog;
import java.util.ArrayList;

public class WeekDayCheck extends LinearLayout {
    /* access modifiers changed from: private */
    public OnWeekCheckedChange onChange;
    /* access modifiers changed from: private */
    public boolean[] repetitions;
    /* access modifiers changed from: private */
    public boolean[] tempRepetitions = new boolean[7];
    private View.OnClickListener toggle = new View.OnClickListener() {
        /* class com.wade.fit.views.WeekDayCheck.C27191 */

        public void onClick(View view) {
            ValueStateTextView valueStateTextView = (ValueStateTextView) view;
            valueStateTextView.setOpen(!valueStateTextView.isOpen());
            WeekDayCheck.this.repetitions[((Integer) view.getTag()).intValue()] = valueStateTextView.isOpen();
            if (((ValueStateTextView) WeekDayCheck.this.values.get(0)).getText().toString().equals(WeekDayCheck.this.getResources().getString(R.string.alarm_set_time_week7))) {
                DebugLog.m6209i("闹钟信息:起始星期天");
                boolean[] unused = WeekDayCheck.this.tempRepetitions = new boolean[7];
                WeekDayCheck.this.tempRepetitions[0] = WeekDayCheck.this.repetitions[1];
                WeekDayCheck.this.tempRepetitions[1] = WeekDayCheck.this.repetitions[2];
                WeekDayCheck.this.tempRepetitions[2] = WeekDayCheck.this.repetitions[3];
                WeekDayCheck.this.tempRepetitions[3] = WeekDayCheck.this.repetitions[4];
                WeekDayCheck.this.tempRepetitions[4] = WeekDayCheck.this.repetitions[5];
                WeekDayCheck.this.tempRepetitions[5] = WeekDayCheck.this.repetitions[6];
                WeekDayCheck.this.tempRepetitions[6] = WeekDayCheck.this.repetitions[0];
                if (WeekDayCheck.this.onChange != null) {
                    WeekDayCheck.this.onChange.onChange(WeekDayCheck.this.tempRepetitions);
                }
            } else if (((ValueStateTextView) WeekDayCheck.this.values.get(0)).getText().toString().equals(WeekDayCheck.this.getResources().getString(R.string.alarm_set_time_week6))) {
                DebugLog.m6209i("闹钟信息:起始星期六");
                boolean[] unused2 = WeekDayCheck.this.tempRepetitions = new boolean[7];
                WeekDayCheck.this.tempRepetitions[0] = WeekDayCheck.this.repetitions[2];
                WeekDayCheck.this.tempRepetitions[1] = WeekDayCheck.this.repetitions[3];
                WeekDayCheck.this.tempRepetitions[2] = WeekDayCheck.this.repetitions[4];
                WeekDayCheck.this.tempRepetitions[3] = WeekDayCheck.this.repetitions[5];
                WeekDayCheck.this.tempRepetitions[4] = WeekDayCheck.this.repetitions[6];
                WeekDayCheck.this.tempRepetitions[5] = WeekDayCheck.this.repetitions[0];
                WeekDayCheck.this.tempRepetitions[6] = WeekDayCheck.this.repetitions[1];
                DebugLog.m6209i("闹钟信息1:" + WeekDayCheck.this.tempRepetitions[0] + "   " + WeekDayCheck.this.tempRepetitions[1] + "   " + WeekDayCheck.this.tempRepetitions[2] + "   " + WeekDayCheck.this.tempRepetitions[3] + "   " + WeekDayCheck.this.tempRepetitions[4] + "   " + WeekDayCheck.this.tempRepetitions[5] + "   " + WeekDayCheck.this.tempRepetitions[6] + "   ");
                DebugLog.m6209i("闹钟信息2:" + WeekDayCheck.this.repetitions[0] + "   " + WeekDayCheck.this.repetitions[1] + "   " + WeekDayCheck.this.repetitions[2] + "   " + WeekDayCheck.this.repetitions[3] + "   " + WeekDayCheck.this.repetitions[4] + "   " + WeekDayCheck.this.repetitions[5] + "   " + WeekDayCheck.this.repetitions[6] + "   ");
                if (WeekDayCheck.this.onChange != null) {
                    WeekDayCheck.this.onChange.onChange(WeekDayCheck.this.tempRepetitions);
                }
            } else if (WeekDayCheck.this.onChange != null) {
                WeekDayCheck.this.onChange.onChange(WeekDayCheck.this.repetitions);
            }
        }
    };
    /* access modifiers changed from: private */
    public ArrayList<ValueStateTextView> values = new ArrayList<>();
    private ValueStateTextView weekDay1;
    private ValueStateTextView weekDay2;
    private ValueStateTextView weekDay3;
    private ValueStateTextView weekDay4;
    private ValueStateTextView weekDay5;
    private ValueStateTextView weekDay6;
    private ValueStateTextView weekDay7;

    public interface OnWeekCheckedChange {
        void onChange(boolean[] zArr);
    }

    public WeekDayCheck(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        LayoutInflater.from(context).inflate((int) R.layout.weekday_check, this);
        this.weekDay1 = (ValueStateTextView) findViewById(R.id.week_day1);
        this.weekDay2 = (ValueStateTextView) findViewById(R.id.week_day2);
        this.weekDay3 = (ValueStateTextView) findViewById(R.id.week_day3);
        this.weekDay4 = (ValueStateTextView) findViewById(R.id.week_day4);
        this.weekDay5 = (ValueStateTextView) findViewById(R.id.week_day5);
        this.weekDay6 = (ValueStateTextView) findViewById(R.id.week_day6);
        this.weekDay7 = (ValueStateTextView) findViewById(R.id.week_day7);
        this.values.add(this.weekDay1);
        this.values.add(this.weekDay2);
        this.values.add(this.weekDay3);
        this.values.add(this.weekDay4);
        this.values.add(this.weekDay5);
        this.values.add(this.weekDay6);
        this.values.add(this.weekDay7);
    }

    public void initAndSetDefault(boolean[] zArr, boolean z) {
        this.repetitions = zArr;
        setWeekDayCheck(z);
        int i = 0;
        if (this.values.get(0).getText().toString().equals(getResources().getString(R.string.alarm_set_time_week7))) {
            DebugLog.m6209i("闹钟信息:起始星期天");
            while (i < getChildCount()) {
                if (i == 0) {
                    ValueStateTextView valueStateTextView = (ValueStateTextView) getChildAt(i);
                    valueStateTextView.setOpen(zArr[getChildCount() - 1]);
                    valueStateTextView.setOnClickListener(this.toggle);
                    valueStateTextView.setTag(Integer.valueOf(getChildCount() - 1));
                } else {
                    ValueStateTextView valueStateTextView2 = (ValueStateTextView) getChildAt(i);
                    int i2 = i - 1;
                    valueStateTextView2.setOpen(zArr[i2]);
                    valueStateTextView2.setOnClickListener(this.toggle);
                    valueStateTextView2.setTag(Integer.valueOf(i2));
                }
                i++;
            }
        } else if (this.values.get(0).getText().toString().equals(getResources().getString(R.string.alarm_set_time_week6))) {
            DebugLog.m6209i("闹钟信息:起始星期六");
            while (i < getChildCount()) {
                if (i == 0) {
                    ValueStateTextView valueStateTextView3 = (ValueStateTextView) getChildAt(i);
                    valueStateTextView3.setOpen(zArr[getChildCount() - 2]);
                    valueStateTextView3.setOnClickListener(this.toggle);
                    valueStateTextView3.setTag(Integer.valueOf(getChildCount() - 2));
                } else if (i == 1) {
                    ValueStateTextView valueStateTextView4 = (ValueStateTextView) getChildAt(i);
                    valueStateTextView4.setOpen(zArr[getChildCount() - 1]);
                    valueStateTextView4.setOnClickListener(this.toggle);
                    valueStateTextView4.setTag(Integer.valueOf(getChildCount() - 1));
                } else {
                    ValueStateTextView valueStateTextView5 = (ValueStateTextView) getChildAt(i);
                    int i3 = i - 2;
                    valueStateTextView5.setOpen(zArr[i3]);
                    valueStateTextView5.setOnClickListener(this.toggle);
                    valueStateTextView5.setTag(Integer.valueOf(i3));
                }
                i++;
            }
        } else {
            DebugLog.m6209i("闹钟信息:起始星期一");
            while (i < getChildCount()) {
                ValueStateTextView valueStateTextView6 = (ValueStateTextView) getChildAt(i);
                valueStateTextView6.setOpen(zArr[i]);
                valueStateTextView6.setOnClickListener(this.toggle);
                valueStateTextView6.setTag(Integer.valueOf(i));
                i++;
            }
        }
    }

    public void initAndSetDefault(boolean[] zArr) {
        initAndSetDefault(zArr, true);
    }

    public boolean[] getRepetition() {
        return this.repetitions;
    }

    public void setOnChangeLinstener(OnWeekCheckedChange onWeekCheckedChange) {
        this.onChange = onWeekCheckedChange;
    }

    public void setWeekDayCheck() {
        String[] weeksByWeekStartDay = WeekUtil.getWeeksByWeekStartDay(getContext(), SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1));
        for (int i = 0; i < weeksByWeekStartDay.length; i++) {
            this.values.get(i).setText(weeksByWeekStartDay[i]);
        }
    }

    public void setWeekDayCheck(boolean z) {
        String[] strArr;
        if (z) {
            strArr = WeekUtil.getWeeksByWeekStartDay(getContext(), SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1));
        } else {
            strArr = WeekUtil.getWeeksByWeekStartDay(getContext(), 1);
        }
        for (int i = 0; i < strArr.length; i++) {
            this.values.get(i).setText(strArr[i]);
        }
    }
}
