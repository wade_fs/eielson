package com.wade.fit.views.pullscrollview.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import com.wade.fit.R;
import com.wade.fit.views.pullscrollview.PullToRefreshBase;
import com.tencent.bugly.beta.tinker.TinkerReport;

public class FlipLoadingLayout extends LoadingLayout {
    static final int FLIP_ANIMATION_DURATION = 150;
    private final Animation mResetRotateAnimation;
    private final Animation mRotateAnimation;

    /* access modifiers changed from: protected */
    public int getDefaultDrawableResId() {
        return R.mipmap.default_ptr_flip;
    }

    /* access modifiers changed from: protected */
    public void onPullImpl(float f) {
    }

    public FlipLoadingLayout(Context context, PullToRefreshBase.Mode mode, PullToRefreshBase.Orientation orientation, TypedArray typedArray) {
        super(context, mode, orientation, typedArray);
        float f = (float) (mode == PullToRefreshBase.Mode.PULL_FROM_START ? -180 : TinkerReport.KEY_APPLIED_VERSION_CHECK);
        this.mRotateAnimation = new RotateAnimation(0.0f, f, 1, 0.5f, 1, 0.5f);
        this.mRotateAnimation.setInterpolator(ANIMATION_INTERPOLATOR);
        this.mRotateAnimation.setDuration(150);
        this.mRotateAnimation.setFillAfter(true);
        this.mResetRotateAnimation = new RotateAnimation(f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.mResetRotateAnimation.setInterpolator(ANIMATION_INTERPOLATOR);
        this.mResetRotateAnimation.setDuration(150);
        this.mResetRotateAnimation.setFillAfter(true);
    }

    /* access modifiers changed from: protected */
    public void onLoadingDrawableSet(Drawable drawable) {
        if (drawable != null) {
            int intrinsicHeight = drawable.getIntrinsicHeight();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            ViewGroup.LayoutParams layoutParams = this.mHeaderImage.getLayoutParams();
            int max = Math.max(intrinsicHeight, intrinsicWidth);
            layoutParams.height = max;
            layoutParams.width = max;
            this.mHeaderImage.requestLayout();
            this.mHeaderImage.setScaleType(ImageView.ScaleType.MATRIX);
            Matrix matrix = new Matrix();
            matrix.postTranslate(((float) (layoutParams.width - intrinsicWidth)) / 2.0f, ((float) (layoutParams.height - intrinsicHeight)) / 2.0f);
            matrix.postRotate(getDrawableRotationAngle(), ((float) layoutParams.width) / 2.0f, ((float) layoutParams.height) / 2.0f);
            this.mHeaderImage.setImageMatrix(matrix);
        }
    }

    /* access modifiers changed from: protected */
    public void pullToRefreshImpl() {
        if (this.mRotateAnimation == this.mHeaderImage.getAnimation()) {
            this.mHeaderImage.startAnimation(this.mResetRotateAnimation);
        }
    }

    /* access modifiers changed from: protected */
    public void refreshingImpl() {
        this.mHeaderImage.clearAnimation();
        this.mHeaderImage.setVisibility(View.INVISIBLE);
        this.mHeaderProgress.setVisibility(View.VISIBLE);
    }

    /* access modifiers changed from: protected */
    public void releaseToRefreshImpl() {
        this.mHeaderImage.startAnimation(this.mRotateAnimation);
    }

    /* access modifiers changed from: protected */
    public void resetImpl() {
        this.mHeaderImage.clearAnimation();
        this.mHeaderProgress.setVisibility(View.GONE);
        this.mHeaderImage.setVisibility(View.VISIBLE);
    }

    /* renamed from: com.wade.fit.views.pullscrollview.internal.FlipLoadingLayout$1 */
    static /* synthetic */ class C27411 {

        /* renamed from: $SwitchMap$com$wade.fit$views$pullscrollview$PullToRefreshBase$Mode */
        static final /* synthetic */ int[] f5272x3fa7fba6 = new int[PullToRefreshBase.Mode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Mode[] r0 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Mode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.views.pullscrollview.internal.FlipLoadingLayout.C27411.f5272x3fa7fba6 = r0
                int[] r0 = com.wade.fit.views.pullscrollview.internal.FlipLoadingLayout.C27411.f5272x3fa7fba6     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Mode r1 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Mode.PULL_FROM_END     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.views.pullscrollview.internal.FlipLoadingLayout.C27411.f5272x3fa7fba6     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.views.pullscrollview.PullToRefreshBase$Mode r1 = com.wade.fit.views.pullscrollview.PullToRefreshBase.Mode.PULL_FROM_START     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.pullscrollview.internal.FlipLoadingLayout.C27411.<clinit>():void");
        }
    }

    private float getDrawableRotationAngle() {
        int i = C27411.f5272x3fa7fba6[this.mMode.ordinal()];
        return i != 1 ? (i == 2 && this.mScrollDirection == PullToRefreshBase.Orientation.HORIZONTAL) ? 270.0f : 0.0f : this.mScrollDirection == PullToRefreshBase.Orientation.HORIZONTAL ? 90.0f : 180.0f;
    }
}
