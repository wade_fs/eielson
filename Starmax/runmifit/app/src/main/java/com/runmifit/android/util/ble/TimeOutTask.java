package com.wade.fit.util.ble;

import android.os.Handler;
import android.os.Looper;

public class TimeOutTask {
    private static TimeOutTask timeOutTask = new TimeOutTask();
    private Handler handler = new Handler(Looper.myLooper());

    public static TimeOutTask getInstance() {
        return timeOutTask;
    }

    public void postTimeOut(Runnable runnable, int i) {
        this.handler.postDelayed(runnable, (long) i);
    }

    public void remove(Runnable runnable) {
        this.handler.removeCallbacks(runnable);
    }
}
