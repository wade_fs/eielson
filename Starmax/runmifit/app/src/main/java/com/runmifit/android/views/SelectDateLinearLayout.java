package com.wade.fit.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.wade.fit.R;

public class SelectDateLinearLayout extends LinearLayout implements View.OnTouchListener {
    View ivSelectedDate;
    View rbDay;
    View rbOneMonth;
    View rbSixMonth;
    View rbYear;

    public SelectDateLinearLayout(Context context) {
        super(context);
    }

    public SelectDateLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SelectDateLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.rbDay = findViewById(R.id.rbDay);
        this.rbOneMonth = findViewById(R.id.rbOneMonth);
        this.rbSixMonth = findViewById(R.id.rbSixMonth);
        this.rbYear = findViewById(R.id.rbYear);
        this.ivSelectedDate = findViewById(R.id.ivSelectedDate);
        this.rbDay.setOnTouchListener(this);
        this.rbOneMonth.setOnTouchListener(this);
        this.rbSixMonth.setOnTouchListener(this);
        this.rbYear.setOnTouchListener(this);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            switch (view.getId()) {
                case R.id.rbDay /*2131296769*/:
                    this.ivSelectedDate.setVisibility(View.VISIBLE);
                    break;
                case R.id.rbOneMonth /*2131296770*/:
                    this.ivSelectedDate.setVisibility(View.GONE);
                    break;
                case R.id.rbSixMonth /*2131296771*/:
                    this.ivSelectedDate.setVisibility(View.GONE);
                    break;
                case R.id.rbYear /*2131296772*/:
                    this.ivSelectedDate.setVisibility(View.GONE);
                    break;
            }
        }
        return false;
    }
}
