package com.wade.fit.views;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.NotificationCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.wade.fit.R;
import com.wade.fit.app.Constants;
import java.util.Timer;
import java.util.TimerTask;

public class UnlockView extends View {
    private ObjectAnimator cancelAnimator;
    private long duration;
    /* access modifiers changed from: private */
    public UnLockListener lister;
    private Paint mPaint = new Paint();
    private int mRingColor;
    private float mStrokeWidth;
    private int mXCenter;
    private int mYCenter;
    /* access modifiers changed from: private */
    public int progress;
    private ObjectAnimator progressAnimator;
    private Timer timer;
    private Bitmap unLockBitmap;
    private Drawable unLockDrawable;

    public interface UnLockListener {
        void lockSuccess();
    }

    public UnlockView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.UnlockView);
        this.unLockDrawable = obtainStyledAttributes.getDrawable(2);
        this.mRingColor = obtainStyledAttributes.getColor(0, 0);
        this.mStrokeWidth = obtainStyledAttributes.getDimension(1, 10.0f);
        obtainStyledAttributes.recycle();
        this.mPaint.setAntiAlias(true);
        this.unLockBitmap = ((BitmapDrawable) this.unLockDrawable).getBitmap();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measureWidth = measureWidth(i);
        int measureHeight = measureHeight(i2);
        if (measureWidth > measureHeight) {
            measureWidth = measureHeight;
        }
        setMeasuredDimension(measureWidth, measureWidth);
    }

    private int measureWidth(int i) {
        return View.MeasureSpec.getMode(i) == 1073741824 ? View.MeasureSpec.getSize(i) : this.unLockBitmap.getWidth();
    }

    private int measureHeight(int i) {
        return View.MeasureSpec.getMode(i) == 1073741824 ? View.MeasureSpec.getSize(i) : this.unLockBitmap.getHeight();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.mXCenter = getWidth() / 2;
        this.mYCenter = getHeight() / 2;
        if (this.unLockBitmap != null) {
            Matrix matrix = new Matrix();
            float width = (float) this.unLockBitmap.getWidth();
            float height = (float) this.unLockBitmap.getHeight();
            float height2 = ((float) getWidth()) / width > ((float) getHeight()) / height ? ((float) getHeight()) / height : ((float) getWidth()) / width;
            matrix.postScale(height2, height2);
            Bitmap bitmap = this.unLockBitmap;
            this.unLockBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), this.unLockBitmap.getHeight(), matrix, true);
            canvas.drawBitmap(this.unLockBitmap, (float) (this.mXCenter - (getWidth() / 2)), (float) (this.mYCenter - (getHeight() / 2)), this.mPaint);
        }
        this.mPaint.setColor(this.mRingColor);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeWidth(this.mStrokeWidth);
        RectF rectF = new RectF();
        this.mPaint.setColor(getResources().getColor(R.color.white));
        float f = this.mStrokeWidth;
        rectF.left = f;
        rectF.top = f;
        rectF.right = ((float) getWidth()) - this.mStrokeWidth;
        rectF.bottom = ((float) getHeight()) - this.mStrokeWidth;
        this.mPaint.setColor(this.mRingColor);
        if (this.progress >= 0) {
            RectF rectF2 = new RectF();
            float f2 = this.mStrokeWidth;
            rectF2.left = f2;
            rectF2.top = f2;
            rectF2.right = ((float) getWidth()) - this.mStrokeWidth;
            rectF2.bottom = ((float) getHeight()) - this.mStrokeWidth;
            canvas.drawArc(rectF2, -90.0f, (float) this.progress, false, this.mPaint);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        int action = motionEvent.getAction();
        if (action == 0) {
            createTimer();
            if (this.progress <= 360) {
                this.timer.schedule(new TimerTask() {
                    /* class com.wade.fit.views.UnlockView.C27171 */

                    public void run() {
                        UnlockView unlockView = UnlockView.this;
                        int unused = unlockView.progress = unlockView.progress + 1;
                        UnlockView.this.postInvalidate();
                        if (UnlockView.this.lister != null && UnlockView.this.progress == 360) {
                            int unused2 = UnlockView.this.progress = 360;
                            UnlockView.this.lister.lockSuccess();
                            UnlockView.this.closeTimer();
                        }
                    }
                }, 0, 2);
            }
        } else if (action == 1) {
            closeTimer();
            int i = this.progress;
            if (i != 360) {
                if (i < 120) {
                    this.duration = (long) (((120 - i) * 5) + Constants.DEFAULT_WEIGHT_KG);
                    this.progressAnimator = ObjectAnimator.ofInt(this, NotificationCompat.CATEGORY_PROGRESS, i, 120, 0).setDuration(this.duration);
                    this.progressAnimator.setInterpolator(new LinearInterpolator());
                    this.progressAnimator.start();
                } else {
                    this.duration = (long) (i * 5);
                    this.cancelAnimator = ObjectAnimator.ofInt(this, NotificationCompat.CATEGORY_PROGRESS, i, 0).setDuration(this.duration);
                    this.cancelAnimator.start();
                }
            }
        }
        return true;
    }

    public void setProgress(int i) {
        this.progress = i;
        invalidate();
    }

    public void setUnLockListener(UnLockListener unLockListener) {
        this.lister = unLockListener;
    }

    private void createTimer() {
        if (this.timer == null) {
            this.timer = new Timer();
        }
    }

    /* access modifiers changed from: private */
    public void closeTimer() {
        Timer timer2 = this.timer;
        if (timer2 != null) {
            timer2.cancel();
            this.timer = null;
        }
    }
}
