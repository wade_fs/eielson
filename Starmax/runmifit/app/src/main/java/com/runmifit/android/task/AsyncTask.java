package com.wade.fit.task;

import com.wade.fit.util.log.LogUtil;
import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AsyncTask {
    private static final int CORE_POOL_SIZE = Math.max(2, Math.min(CPU_COUNT - 1, 4));
    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int KEEP_ALIVE_SECONDS = 30;
    private static final int MAXIMUM_POOL_SIZE = ((CPU_COUNT * 2) + 1);
    public static final Executor SERIAL_EXECUTOR = new SerialExecutor();
    private static final int TASK_TIME_OUT = 0;
    public static final Executor THREAD_POOL_EXECUTOR;
    private static volatile Executor sDefaultExecutor = SERIAL_EXECUTOR;
    private static final BlockingQueue<Runnable> sPoolWorkQueue = new LinkedBlockingQueue(128);
    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        /* class com.wade.fit.task.AsyncTask.C24421 */
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.mCount.getAndIncrement());
        }
    };

    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, 30, TimeUnit.SECONDS, sPoolWorkQueue, sThreadFactory);
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        THREAD_POOL_EXECUTOR = threadPoolExecutor;
    }

    private static class SerialExecutor implements Executor {
        Runnable mActive;
        final ArrayDeque<Runnable> mTasks;

        private SerialExecutor() {
            this.mTasks = new ArrayDeque<>();
        }

        public synchronized void execute(final Runnable runnable) {
            this.mTasks.offer(new Runnable() {
                /* class com.wade.fit.task.AsyncTask.SerialExecutor.C24431 */

                public void run() {
                    try {
                        runnable.run();
                        LogUtil.d(Thread.currentThread().getName() + ",........." + SerialExecutor.this.mTasks.size());
                        if (!SerialExecutor.this.mTasks.isEmpty()) {
                            Runnable runnable = runnable;
                            if (runnable instanceof RunTask) {
                                RunTask runTask = (RunTask) runnable;
                                LogUtil.d(Thread.currentThread().getName() + ",........." + runTask.mCount.get());
                                while (!runTask.isExeRunFinish()) {
                                    try {
                                        Thread.sleep(10);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            SerialExecutor.this.scheduleNext();
                            return;
                        }
                    } catch (Throwable th) {
                        LogUtil.d(Thread.currentThread().getName() + ",........." + SerialExecutor.this.mTasks.size());
                        if (!SerialExecutor.this.mTasks.isEmpty()) {
                            Runnable runnable2 = runnable;
                            if (runnable2 instanceof RunTask) {
                                RunTask runTask2 = (RunTask) runnable2;
                                LogUtil.d(Thread.currentThread().getName() + ",........." + runTask2.mCount.get());
                                while (!runTask2.isExeRunFinish()) {
                                    try {
                                        Thread.sleep(10);
                                    } catch (InterruptedException e2) {
                                        e2.printStackTrace();
                                    }
                                }
                            }
                            SerialExecutor.this.scheduleNext();
                            throw th;
                        }
                    }
                    SerialExecutor.this.scheduleNext();
                }
            });
            LogUtil.d("add Runnable....mTasks:" + this.mTasks.size());
            if (this.mActive == null) {
                scheduleNext();
            }
        }

        /* access modifiers changed from: protected */
        public synchronized void scheduleNext() {
            Runnable poll = this.mTasks.poll();
            this.mActive = poll;
            if (poll != null) {
                LogUtil.d("execute Runnable....");
                AsyncTask.THREAD_POOL_EXECUTOR.execute(this.mActive);
            }
        }
    }

    public <Params> AsyncTask execute(RunTask<Params> runTask, Params params) {
        runTask.setData(params);
        sDefaultExecutor.execute(runTask);
        return this;
    }
}
