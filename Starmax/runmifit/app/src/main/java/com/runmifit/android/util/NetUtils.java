package com.wade.fit.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.wade.fit.app.AppApplication;

public class NetUtils {
    private NetUtils() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected() || activeNetworkInfo.getState() != NetworkInfo.State.CONNECTED) {
            return false;
        }
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0061, code lost:
        if (r2.equalsIgnoreCase("CDMA2000") == false) goto L_0x006b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getNetworkType() {
        /*
            com.wade.fit.app.AppApplication r0 = com.wade.fit.app.AppApplication.getInstance()
            java.lang.String r1 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()
            java.lang.String r1 = "cocos2d-x"
            if (r0 == 0) goto L_0x0089
            boolean r2 = r0.isConnected()
            if (r2 == 0) goto L_0x0089
            int r2 = r0.getType()
            r3 = 1
            if (r2 != r3) goto L_0x0024
            java.lang.String r0 = "WIFI"
            goto L_0x008b
        L_0x0024:
            int r2 = r0.getType()
            if (r2 != 0) goto L_0x0089
            java.lang.String r2 = r0.getSubtypeName()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Network getSubtypeName : "
            r3.append(r4)
            r3.append(r2)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r1, r3)
            int r0 = r0.getSubtype()
            java.lang.String r3 = "3G"
            switch(r0) {
                case 1: goto L_0x0069;
                case 2: goto L_0x0069;
                case 3: goto L_0x0067;
                case 4: goto L_0x0069;
                case 5: goto L_0x0067;
                case 6: goto L_0x0067;
                case 7: goto L_0x0069;
                case 8: goto L_0x0067;
                case 9: goto L_0x0067;
                case 10: goto L_0x0067;
                case 11: goto L_0x0069;
                case 12: goto L_0x0067;
                case 13: goto L_0x0064;
                case 14: goto L_0x0067;
                case 15: goto L_0x0067;
                default: goto L_0x004b;
            }
        L_0x004b:
            java.lang.String r4 = "TD-SCDMA"
            boolean r4 = r2.equalsIgnoreCase(r4)
            if (r4 != 0) goto L_0x0067
            java.lang.String r4 = "WCDMA"
            boolean r4 = r2.equalsIgnoreCase(r4)
            if (r4 != 0) goto L_0x0067
            java.lang.String r4 = "CDMA2000"
            boolean r4 = r2.equalsIgnoreCase(r4)
            if (r4 == 0) goto L_0x006b
            goto L_0x0067
        L_0x0064:
            java.lang.String r2 = "4G"
            goto L_0x006b
        L_0x0067:
            r2 = r3
            goto L_0x006b
        L_0x0069:
            java.lang.String r2 = "2G"
        L_0x006b:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Network getSubtype : "
            r3.append(r4)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r0 = r0.toString()
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            android.util.Log.e(r1, r0)
            r0 = r2
            goto L_0x008b
        L_0x0089:
            java.lang.String r0 = ""
        L_0x008b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Network Type : "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r1, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.NetUtils.getNetworkType():java.lang.String");
    }

    public static boolean isConnected() {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) AppApplication.getInstance().getSystemService("connectivity");
        return connectivityManager != null && (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
    }

    public static boolean isWifi(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService("connectivity");
        if (connectivityManager != null && connectivityManager.getActiveNetworkInfo().getType() == 1) {
            return true;
        }
        return false;
    }

    public static void openSetting(Activity activity) {
        Intent intent = new Intent("/");
        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.WirelessSettings"));
        intent.setAction("android.intent.action.VIEW");
        activity.startActivityForResult(intent, 0);
    }
}
