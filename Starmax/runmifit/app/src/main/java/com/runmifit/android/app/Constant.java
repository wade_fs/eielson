package com.wade.fit.app;

import android.os.Environment;

public interface Constant {
    public static final String APP_ROOT_PATH = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/zhihuiju");
    public static final String CACHE_PATH = (APP_ROOT_PATH + "/cache");
    public static final String CRASH_PATH = (LOG_PATH + "/crash/");
    public static final int DAY = 86400000;
    public static final int DETAIL_TYPE_DAY = 0;
    public static final int DETAIL_TYPE_ONE_MONTH = 1;
    public static final int DETAIL_TYPE_SIX_MONTH = 2;
    public static final int DETAIL_TYPE_YEAR = 3;
    public static final String FILE_PATH = (APP_ROOT_PATH + "/file/");
    public static final String INFO_PATH = (APP_ROOT_PATH + "/info");

    /* renamed from: KB */
    public static final int f5210KB = 1024;
    public static final String LOG_PATH = (APP_ROOT_PATH + "/log");
    public static final int PERMISSIONS_REQUEST_CODE_PHOTO_CAMERA = 100;
    public static final String PIC_PATH = (APP_ROOT_PATH + "/pic");
    public static final int REQUEST_CODE_PHOTO_CAMERA = 1;
    public static final int REQUEST_CODE_PHOTO_CROP = 4;
    public static final int REQUEST_CODE_PHOTO_PICK = 2;
    public static final int REQUEST_CODE_PHOTO_RESOULT = 3;
    public static final String SERVER_PATH = (APP_ROOT_PATH + "/server");
    public static final String UPDATE_PATH = (LOG_PATH + "/deivce_update/");

    public enum MemoryUnit {
        BYTE,
        KB,
        MB,
        GB
    }
}
