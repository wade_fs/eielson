package com.wade.fit.ui.main.activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.main.activity.UpdatePasswordActivity_ViewBinding */
public class UpdatePasswordActivity_ViewBinding implements Unbinder {
    private UpdatePasswordActivity target;
    private View view2131296338;
    private View view2131296436;
    private TextWatcher view2131296436TextWatcher;
    private View view2131296439;
    private TextWatcher view2131296439TextWatcher;
    private View view2131296440;
    private TextWatcher view2131296440TextWatcher;
    private View view2131296525;
    private View view2131296533;
    private View view2131297051;

    public UpdatePasswordActivity_ViewBinding(UpdatePasswordActivity updatePasswordActivity) {
        this(updatePasswordActivity, updatePasswordActivity.getWindow().getDecorView());
    }

    public UpdatePasswordActivity_ViewBinding(final UpdatePasswordActivity updatePasswordActivity, View view) {
        this.target = updatePasswordActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.et_username, "field 'etUsername' and method 'textChanged'");
        updatePasswordActivity.etUsername = (EditText) Utils.castView(findRequiredView, R.id.et_username, "field 'etUsername'", EditText.class);
        this.view2131296440 = findRequiredView;
        this.view2131296440TextWatcher = new TextWatcher() {
            /* class com.wade.fit.ui.main.activity.UpdatePasswordActivity_ViewBinding.C25461 */

            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                updatePasswordActivity.textChanged();
            }
        };
        ((TextView) findRequiredView).addTextChangedListener(this.view2131296440TextWatcher);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.et_pwd, "field 'etPwd' and method 'textChanged'");
        updatePasswordActivity.etPwd = (EditText) Utils.castView(findRequiredView2, R.id.et_pwd, "field 'etPwd'", EditText.class);
        this.view2131296439 = findRequiredView2;
        this.view2131296439TextWatcher = new TextWatcher() {
            /* class com.wade.fit.ui.main.activity.UpdatePasswordActivity_ViewBinding.C25472 */

            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                updatePasswordActivity.textChanged();
            }
        };
        ((TextView) findRequiredView2).addTextChangedListener(this.view2131296439TextWatcher);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.etSmsCode, "field 'etSmsCode' and method 'textChanged'");
        updatePasswordActivity.etSmsCode = (EditText) Utils.castView(findRequiredView3, R.id.etSmsCode, "field 'etSmsCode'", EditText.class);
        this.view2131296436 = findRequiredView3;
        this.view2131296436TextWatcher = new TextWatcher() {
            /* class com.wade.fit.ui.main.activity.UpdatePasswordActivity_ViewBinding.C25483 */

            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                updatePasswordActivity.textChanged();
            }
        };
        ((TextView) findRequiredView3).addTextChangedListener(this.view2131296436TextWatcher);
        View findRequiredView4 = Utils.findRequiredView(view, R.id.btnUpdate, "field 'btnUpdate' and method 'updatePassword'");
        updatePasswordActivity.btnUpdate = (Button) Utils.castView(findRequiredView4, R.id.btnUpdate, "field 'btnUpdate'", Button.class);
        this.view2131296338 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.UpdatePasswordActivity_ViewBinding.C25494 */

            public void doClick(View view) {
                updatePasswordActivity.updatePassword();
            }
        });
        View findRequiredView5 = Utils.findRequiredView(view, R.id.tvSmsCode, "field 'tvSmsCode' and method 'sendCode'");
        updatePasswordActivity.tvSmsCode = (TextView) Utils.castView(findRequiredView5, R.id.tvSmsCode, "field 'tvSmsCode'", TextView.class);
        this.view2131297051 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.UpdatePasswordActivity_ViewBinding.C25505 */

            public void doClick(View view) {
                updatePasswordActivity.sendCode();
            }
        });
        updatePasswordActivity.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTitle, "field 'tvTitle'", TextView.class);
        View findRequiredView6 = Utils.findRequiredView(view, R.id.imgStatus, "field 'imgStatus' and method 'updatePwdStatus'");
        updatePasswordActivity.imgStatus = (ImageView) Utils.castView(findRequiredView6, R.id.imgStatus, "field 'imgStatus'", ImageView.class);
        this.view2131296533 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.UpdatePasswordActivity_ViewBinding.C25516 */

            public void doClick(View view) {
                updatePasswordActivity.updatePwdStatus();
            }
        });
        View findRequiredView7 = Utils.findRequiredView(view, R.id.imgClean, "field 'imgClean' and method 'cleanUserName'");
        updatePasswordActivity.imgClean = (ImageView) Utils.castView(findRequiredView7, R.id.imgClean, "field 'imgClean'", ImageView.class);
        this.view2131296525 = findRequiredView7;
        findRequiredView7.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.UpdatePasswordActivity_ViewBinding.C25527 */

            public void doClick(View view) {
                updatePasswordActivity.cleanUserName();
            }
        });
    }

    public void unbind() {
        UpdatePasswordActivity updatePasswordActivity = this.target;
        if (updatePasswordActivity != null) {
            this.target = null;
            updatePasswordActivity.etUsername = null;
            updatePasswordActivity.etPwd = null;
            updatePasswordActivity.etSmsCode = null;
            updatePasswordActivity.btnUpdate = null;
            updatePasswordActivity.tvSmsCode = null;
            updatePasswordActivity.tvTitle = null;
            updatePasswordActivity.imgStatus = null;
            updatePasswordActivity.imgClean = null;
            ((TextView) this.view2131296440).removeTextChangedListener(this.view2131296440TextWatcher);
            this.view2131296440TextWatcher = null;
            this.view2131296440 = null;
            ((TextView) this.view2131296439).removeTextChangedListener(this.view2131296439TextWatcher);
            this.view2131296439TextWatcher = null;
            this.view2131296439 = null;
            ((TextView) this.view2131296436).removeTextChangedListener(this.view2131296436TextWatcher);
            this.view2131296436TextWatcher = null;
            this.view2131296436 = null;
            this.view2131296338.setOnClickListener(null);
            this.view2131296338 = null;
            this.view2131297051.setOnClickListener(null);
            this.view2131297051 = null;
            this.view2131296533.setOnClickListener(null);
            this.view2131296533 = null;
            this.view2131296525.setOnClickListener(null);
            this.view2131296525 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
