package com.wade.fit.util.thirddataplatform;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.auth.api.signin.internal.zzi;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.greendao.gen.HealthHeartRateItemDao;
import com.wade.fit.greendao.gen.HealthSportItemDao;
import com.wade.fit.util.AsyncTaskUtil;
import com.wade.fit.util.NetUtils;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.log.LogUtil;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.greenrobot.greendao.query.WhereCondition;

public class GoogleFitPresenter extends GoogleFitDataHelper implements Constants {
    public static final int GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 10;
    public static final int GOOGLE_SIGN_IN = 3;
    public static final String TAG = "GoogleFitPresenter";
    public static GoogleFitPresenter modle;
    GoogleSignInAccount account;
    /* access modifiers changed from: private */
    public Activity activity;
    private boolean authInProgress;
    DataSet calorieDataSet;
    DataSet distanceDataSet;
    private FitnessOptions fitnessOptions;
    private boolean isSubscrib = false;
    private final int oneLoadCount = 3;
    DataSet rateDataSet;
    DataSet stepDataSet;
    private final long timeDis = 1800000;
    DataSet weightDataSet;

    public Activity getActivity() {
        return this.activity;
    }

    public void setActivity(Activity activity2) {
        this.activity = activity2;
    }

    private GoogleFitPresenter(Activity activity2) {
        this.activity = activity2;
        this.fitnessOptions = FitnessOptions.builder().addDataType(DataType.TYPE_STEP_COUNT_DELTA, 1).addDataType(DataType.TYPE_DISTANCE_DELTA, 1).addDataType(DataType.TYPE_WEIGHT, 1).addDataType(DataType.TYPE_HEART_RATE_BPM, 1).addDataType(DataType.TYPE_CALORIES_EXPENDED, 1).build();
    }

    public static GoogleFitPresenter getInstance(Activity activity2) {
        if (modle == null) {
            modle = new GoogleFitPresenter(activity2);
        }
        return modle;
    }

    public static GoogleFitPresenter getInstance() {
        return modle;
    }

    public void handleSignInResult(int i, Intent intent) {
        if (i == 3) {
            try {
                GoogleSignInResult signInResultFromIntent = zzi.getSignInResultFromIntent(intent);
                if (signInResultFromIntent.isSuccess()) {
                    this.account = GoogleSignIn.getSignedInAccountFromIntent(intent).getResult(ApiException.class);
                    sunbscrerib();
                    return;
                }
                LogUtil.d("登录失败：" + signInResultFromIntent.getStatus());
            } catch (ApiException unused) {
            }
        }
    }

    public void connectGoogle() {
        if (SharePreferenceUtils.getInt(AppApplication.getInstance(), Constants.GOOGLE_FIT_KEY, 0) != 1) {
            LogUtil.d("googleFit.....开关未开");
        } else if (NetUtils.isConnected(AppApplication.getInstance())) {
            try {
                this.account = GoogleSignIn.getLastSignedInAccount(this.activity);
                if (this.account == null) {
                    LogUtil.d("account is null,go to signIn");
                    signIn();
                    return;
                }
                LogUtil.d("account is exist,go to sunbscrerib");
                sunbscrerib();
            } catch (Exception e) {
                e.printStackTrace();
                signIn();
            }
        }
    }

    private void signIn() {
        this.activity.startActivityForResult(GoogleSignIn.getClient(this.activity, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()).getSignInIntent(), 3);
    }

    private void sunbscrerib() {
        try {
            this.account = GoogleSignIn.getLastSignedInAccount(this.activity);
            if (this.account == null) {
                LogUtil.d("account  is null");
            } else if (!GoogleSignIn.hasPermissions(this.account, this.fitnessOptions)) {
                GoogleSignIn.requestPermissions(this.activity, 10, this.account, this.fitnessOptions);
            } else {
                LogUtil.d("是否订阅了.....isSubscrib:" + this.isSubscrib);
                if (!this.isSubscrib) {
                    subscriptionData();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void subscriptionData() {
        subscribe(DataType.TYPE_STEP_COUNT_DELTA);
        subscribe(DataType.TYPE_DISTANCE_DELTA);
        subscribe(DataType.TYPE_HEIGHT);
        subscribe(DataType.TYPE_WEIGHT);
        subscribe(DataType.TYPE_HEART_RATE_BPM);
        subscribe(DataType.TYPE_CALORIES_EXPENDED);
    }

    private void subscribe(DataType dataType) {
        try {
            if (GoogleSignIn.getLastSignedInAccount(this.activity) == null) {
                LogUtil.d("account  is null");
                return;
            }
            Activity activity2 = this.activity;
            Fitness.getRecordingClient(activity2, GoogleSignIn.getLastSignedInAccount(activity2)).subscribe(dataType).addOnSuccessListener(new OnSuccessListener(dataType) {
                /* class com.wade.fit.util.thirddataplatform.$$Lambda$GoogleFitPresenter$zr_UOUnBwMLieOha6hWEdp617k */
                private final /* synthetic */ DataType f$1;

                {
                    this.f$1 = r2;
                }

                public final void onSuccess(Object obj) {
                    GoogleFitPresenter.this.lambda$subscribe$0$GoogleFitPresenter(this.f$1, (Void) obj);
                }
            }).addOnFailureListener(new OnFailureListener() {
                /* class com.wade.fit.util.thirddataplatform.$$Lambda$GoogleFitPresenter$LqPUBiEZhlYHsNA0iEIF2_DroA */

                public final void onFailure(Exception exc) {
                    LogUtil.d("Successfully onFailure! " + DataType.this + "," + exc.toString());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public /* synthetic */ void lambda$subscribe$0$GoogleFitPresenter(DataType dataType, Void voidR) {
        LogUtil.d("Successfully subscribed! " + dataType);
        this.isSubscrib = true;
        DataType dataType2 = DataType.TYPE_CALORIES_EXPENDED;
    }

    public void saveData() {
        int i;
        if (((Integer) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.GOOGLE_FIT_KEY, 0)).intValue() != 1) {
            LogUtil.d("googleFit.....开关未开");
        } else if (NetUtils.isConnected(AppApplication.getInstance())) {
            sunbscrerib();
            Calendar instance = Calendar.getInstance();
            instance.setTime(new Date());
            this.currentUploadTime = instance.getTimeInMillis();
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            instance.set(14, 0);
            long timeInMillis = instance.getTimeInMillis();
            this.lastUploadTime = SharePreferenceUtils.getLong(AppApplication.getInstance(), GoogleFitDataHelper.LAST_SYS_TIME, 0L);
            if (this.lastUploadTime < timeInMillis) {
                this.lastUploadTime = timeInMillis;
            }
            long j = 1800000;
            int i2 = ((int) ((this.currentUploadTime - this.lastUploadTime) / 1800000)) + 1;
            LogUtil.d("上传次数" + i2);
            List list = AppApplication.getInstance().getDaoSession().getHealthSportItemDao().queryBuilder().where(HealthSportItemDao.Properties.Date.between(Long.valueOf(this.lastUploadTime), Long.valueOf(this.currentUploadTime)), new WhereCondition[0]).orderAsc(HealthSportItemDao.Properties.Date).build().list();
            List list2 = AppApplication.getInstance().getDaoSession().getHealthHeartRateItemDao().queryBuilder().where(HealthHeartRateItemDao.Properties.Date.between(Long.valueOf(this.lastUploadTime), Long.valueOf(this.currentUploadTime)), new WhereCondition[0]).orderAsc(HealthHeartRateItemDao.Properties.Date).build().list();
            if (list == null || list.size() <= 0) {
                LogUtil.d("无数据不上传");
            } else {
                int i3 = 0;
                while (i3 < i2) {
                    long j2 = this.lastUploadTime + (((long) i3) * j);
                    if (i3 == i2 - 1) {
                        long j3 = this.currentUploadTime;
                        int i4 = 0;
                        int i5 = 0;
                        int i6 = 0;
                        for (int i7 = i3 * 3; i7 < list.size(); i7++) {
                            i4 += ((HealthSportItem) list.get(i7)).getStepCount();
                            i5 += ((HealthSportItem) list.get(i7)).getDistance();
                            i6 += ((HealthSportItem) list.get(i7)).getCalory();
                        }
                        uploadStep(i4, (float) i5, (float) i6, j2, j3);
                    } else {
                        int i8 = i3 + 1;
                        long j4 = (((long) i8) * j) + this.lastUploadTime;
                        int i9 = 0;
                        int i10 = 0;
                        int i11 = 0;
                        for (int i12 = i3 * 3; i12 < i8 * 3; i12++) {
                            i9 += ((HealthSportItem) list.get(i12)).getStepCount();
                            i10 += ((HealthSportItem) list.get(i12)).getDistance();
                            i11 += ((HealthSportItem) list.get(i12)).getCalory();
                        }
                        uploadStep(i9, (float) i10, (float) i11, j2, j4);
                    }
                    i3++;
                    j = 1800000;
                }
            }
            if (list2 == null || list2.size() <= 0) {
                LogUtil.d("无心率数据不上传");
                return;
            }
            int i13 = 0;
            while (i13 < list2.size()) {
                long j5 = this.lastUploadTime + ((long) (i13 * 900000));
                int i14 = i13 + 1;
                long j6 = this.lastUploadTime + ((long) (900000 * i14));
                if (((HealthHeartRateItem) list2.get(i13)).getHeartRaveValue() != 0) {
                    i = i14;
                    this.rateDataSet = createDataForRequest(DataType.TYPE_HEART_RATE_BPM, Field.FIELD_BPM, Float.valueOf((float) ((HealthHeartRateItem) list2.get(i13)).getHeartRaveValue()), this.lastUploadTime, this.currentUploadTime);
                    LogUtil.d("上传心率数据：" + ((HealthHeartRateItem) list2.get(i13)).getHeartRaveValue() + this.simpleDateFormat.format(new Date(j5)) + "," + this.simpleDateFormat.format(new Date(j6)) + this.simpleDateFormat.format(new Date(((HealthHeartRateItem) list2.get(i13)).getDate())));
                    new AsyncTaskUtil(new AsyncTaskUtil.IAsyncTaskCallBack() {
                        /* class com.wade.fit.util.thirddataplatform.GoogleFitPresenter.C27051 */

                        public void onPostExecute(Object obj) {
                        }

                        public Object doInBackground(String... strArr) {
                            Task<Void> insertData = Fitness.getHistoryClient(GoogleFitPresenter.this.activity, GoogleSignIn.getLastSignedInAccount(GoogleFitPresenter.this.activity)).insertData(GoogleFitPresenter.this.rateDataSet);
                            while (!insertData.isComplete()) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (insertData.isSuccessful()) {
                                LogUtil.d("上传心率数据成功,");
                                GoogleFitPresenter.this.uploadSuccess();
                                return null;
                            }
                            LogUtil.d("上传数据失败 step:");
                            return null;
                        }
                    }).execute("");
                } else {
                    i = i14;
                }
                i13 = i;
            }
        }
    }

    private void uploadStep(int i, float f, float f2, long j, long j2) {
        if (i != 0) {
            long j3 = j;
            long j4 = j2;
            this.stepDataSet = createDataForRequest(DataType.TYPE_STEP_COUNT_DELTA, Field.FIELD_STEPS, Integer.valueOf(i), j3, j4);
            this.distanceDataSet = createDataForRequest(DataType.TYPE_DISTANCE_DELTA, Field.FIELD_DISTANCE, Float.valueOf(f), j3, j4);
            this.calorieDataSet = createDataForRequest(DataType.TYPE_CALORIES_EXPENDED, Field.FIELD_CALORIES, Float.valueOf(f2), j3, j4);
            LogUtil.d("上传数据：stepAllCount:" + i + "，allDistance:" + f + ",allCalory:" + f2 + ",time:" + this.simpleDateFormat.format(new Date(j)) + "," + this.simpleDateFormat.format(new Date(j2)));
            new AsyncTaskUtil(new AsyncTaskUtil.IAsyncTaskCallBack() {
                /* class com.wade.fit.util.thirddataplatform.GoogleFitPresenter.C27062 */

                public void onPostExecute(Object obj) {
                }

                public Object doInBackground(String... strArr) {
                    Task<Void> insertData = Fitness.getHistoryClient(GoogleFitPresenter.this.activity, GoogleSignIn.getLastSignedInAccount(GoogleFitPresenter.this.activity)).insertData(GoogleFitPresenter.this.stepDataSet);
                    Task<Void> insertData2 = Fitness.getHistoryClient(GoogleFitPresenter.this.activity, GoogleSignIn.getLastSignedInAccount(GoogleFitPresenter.this.activity)).insertData(GoogleFitPresenter.this.distanceDataSet);
                    Task<Void> insertData3 = Fitness.getHistoryClient(GoogleFitPresenter.this.activity, GoogleSignIn.getLastSignedInAccount(GoogleFitPresenter.this.activity)).insertData(GoogleFitPresenter.this.calorieDataSet);
                    while (true) {
                        if (insertData.isComplete() && insertData2.isComplete() && insertData3.isComplete()) {
                            break;
                        }
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (!insertData.isSuccessful() || !insertData2.isSuccessful() || !insertData3.isSuccessful()) {
                        LogUtil.d("上传数据失败 step:");
                        return null;
                    }
                    LogUtil.d("上传数据成功,");
                    GoogleFitPresenter.this.uploadSuccess();
                    return null;
                }
            }).execute("");
            return;
        }
        LogUtil.d("无数据不上传");
    }

    private DataSet createDataForRequest(DataType dataType, Field field, Object obj, long j, long j2) {
        DataSet create = DataSet.create(new DataSource.Builder().setAppPackageName(this.activity).setDataType(dataType).setStreamName("VeryFitPro - step count").setType(0).build());
        DataPoint timeInterval = create.createDataPoint().setTimeInterval(j, j2, TimeUnit.MILLISECONDS);
        if (dataType == DataType.TYPE_CALORIES_EXPENDED || dataType == DataType.TYPE_HEART_RATE_BPM) {
            timeInterval.getValue(field).setFloat(((Float) obj).floatValue());
        } else if (obj instanceof Integer) {
            timeInterval.setIntValues(((Integer) obj).intValue());
        } else {
            timeInterval = timeInterval.setFloatValues(((Float) obj).floatValue());
        }
        create.add(timeInterval);
        return create;
    }

    public void close() {
        this.activity = null;
    }
}
