package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.MineInfoActivity_ViewBinding */
public class MineInfoActivity_ViewBinding implements Unbinder {
    private MineInfoActivity target;
    private View view2131296568;
    private View view2131296796;
    private View view2131296802;
    private View view2131296817;
    private View view2131296820;
    private View view2131296821;

    public MineInfoActivity_ViewBinding(MineInfoActivity mineInfoActivity) {
        this(mineInfoActivity, mineInfoActivity.getWindow().getDecorView());
    }

    public MineInfoActivity_ViewBinding(final MineInfoActivity mineInfoActivity, View view) {
        this.target = mineInfoActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.rlSex, "field 'rlSex' and method 'onClick'");
        mineInfoActivity.rlSex = (RelativeLayout) Utils.castView(findRequiredView, R.id.rlSex, "field 'rlSex'", RelativeLayout.class);
        this.view2131296817 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.MineInfoActivity_ViewBinding.C25811 */

            public void doClick(View view) {
                mineInfoActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.rlBirthDay, "field 'rlBirthDay' and method 'onClick'");
        mineInfoActivity.rlBirthDay = (RelativeLayout) Utils.castView(findRequiredView2, R.id.rlBirthDay, "field 'rlBirthDay'", RelativeLayout.class);
        this.view2131296796 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.MineInfoActivity_ViewBinding.C25822 */

            public void doClick(View view) {
                mineInfoActivity.onClick(view);
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.rlHeight, "field 'rlHeight' and method 'onClick'");
        mineInfoActivity.rlHeight = (RelativeLayout) Utils.castView(findRequiredView3, R.id.rlHeight, "field 'rlHeight'", RelativeLayout.class);
        this.view2131296802 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.MineInfoActivity_ViewBinding.C25833 */

            public void doClick(View view) {
                mineInfoActivity.onClick(view);
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.rlWeight, "field 'rlWeight' and method 'onClick'");
        mineInfoActivity.rlWeight = (RelativeLayout) Utils.castView(findRequiredView4, R.id.rlWeight, "field 'rlWeight'", RelativeLayout.class);
        this.view2131296821 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.MineInfoActivity_ViewBinding.C25844 */

            public void doClick(View view) {
                mineInfoActivity.onClick(view);
            }
        });
        mineInfoActivity.tvWeight = (TextView) Utils.findRequiredViewAsType(view, R.id.tvWeight, "field 'tvWeight'", TextView.class);
        mineInfoActivity.tvHeight = (TextView) Utils.findRequiredViewAsType(view, R.id.tvHeight, "field 'tvHeight'", TextView.class);
        mineInfoActivity.tvBirthday = (TextView) Utils.findRequiredViewAsType(view, R.id.tvBirthday, "field 'tvBirthday'", TextView.class);
        mineInfoActivity.tvSex = (TextView) Utils.findRequiredViewAsType(view, R.id.tvSex, "field 'tvSex'", TextView.class);
        mineInfoActivity.tvUserName = (TextView) Utils.findRequiredViewAsType(view, R.id.tvUserName, "field 'tvUserName'", TextView.class);
        mineInfoActivity.tvUserName2 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvUserName2, "field 'tvUserName2'", TextView.class);
        mineInfoActivity.tvEmail = (TextView) Utils.findRequiredViewAsType(view, R.id.tvEmail, "field 'tvEmail'", TextView.class);
        View findRequiredView5 = Utils.findRequiredView(view, R.id.ivHeader, "field 'ivHeader' and method 'onClick'");
        mineInfoActivity.ivHeader = (ImageView) Utils.castView(findRequiredView5, R.id.ivHeader, "field 'ivHeader'", ImageView.class);
        this.view2131296568 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.MineInfoActivity_ViewBinding.C25855 */

            public void doClick(View view) {
                mineInfoActivity.onClick(view);
            }
        });
        mineInfoActivity.imgBackground = (ImageView) Utils.findRequiredViewAsType(view, R.id.imgBackground, "field 'imgBackground'", ImageView.class);
        View findRequiredView6 = Utils.findRequiredView(view, R.id.rlUserName, "method 'onClick'");
        this.view2131296820 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.MineInfoActivity_ViewBinding.C25866 */

            public void doClick(View view) {
                mineInfoActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        MineInfoActivity mineInfoActivity = this.target;
        if (mineInfoActivity != null) {
            this.target = null;
            mineInfoActivity.rlSex = null;
            mineInfoActivity.rlBirthDay = null;
            mineInfoActivity.rlHeight = null;
            mineInfoActivity.rlWeight = null;
            mineInfoActivity.tvWeight = null;
            mineInfoActivity.tvHeight = null;
            mineInfoActivity.tvBirthday = null;
            mineInfoActivity.tvSex = null;
            mineInfoActivity.tvUserName = null;
            mineInfoActivity.tvUserName2 = null;
            mineInfoActivity.tvEmail = null;
            mineInfoActivity.ivHeader = null;
            mineInfoActivity.imgBackground = null;
            this.view2131296817.setOnClickListener(null);
            this.view2131296817 = null;
            this.view2131296796.setOnClickListener(null);
            this.view2131296796 = null;
            this.view2131296802.setOnClickListener(null);
            this.view2131296802 = null;
            this.view2131296821.setOnClickListener(null);
            this.view2131296821 = null;
            this.view2131296568.setOnClickListener(null);
            this.view2131296568 = null;
            this.view2131296820.setOnClickListener(null);
            this.view2131296820 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
