package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemLableValue;

/* renamed from: com.wade.fit.ui.mine.activity.AccoutActivity_ViewBinding */
public class AccoutActivity_ViewBinding implements Unbinder {
    private AccoutActivity target;
    private View view2131296332;
    private View view2131296496;
    private View view2131296509;

    public AccoutActivity_ViewBinding(AccoutActivity accoutActivity) {
        this(accoutActivity, accoutActivity.getWindow().getDecorView());
    }

    public AccoutActivity_ViewBinding(final AccoutActivity accoutActivity, View view) {
        this.target = accoutActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.ilResetPwd, "field 'ilResetPwd' and method 'toRestPassword'");
        accoutActivity.ilResetPwd = (ItemLableValue) Utils.castView(findRequiredView, R.id.ilResetPwd, "field 'ilResetPwd'", ItemLableValue.class);
        this.view2131296509 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.AccoutActivity_ViewBinding.C25731 */

            public void doClick(View view) {
                accoutActivity.toRestPassword();
            }
        });
        accoutActivity.ilLogOut = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilLogOut, "field 'ilLogOut'", ItemLableValue.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.btnLogOut, "field 'btnLogOut' and method 'loginClick'");
        accoutActivity.btnLogOut = (Button) Utils.castView(findRequiredView2, R.id.btnLogOut, "field 'btnLogOut'", Button.class);
        this.view2131296332 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.AccoutActivity_ViewBinding.C25742 */

            public void doClick(View view) {
                accoutActivity.loginClick();
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.ilClearData, "field 'ilClearData' and method 'onClick'");
        accoutActivity.ilClearData = (ItemLableValue) Utils.castView(findRequiredView3, R.id.ilClearData, "field 'ilClearData'", ItemLableValue.class);
        this.view2131296496 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.AccoutActivity_ViewBinding.C25753 */

            public void doClick(View view) {
                accoutActivity.onClick(view);
            }
        });
        accoutActivity.lineView = Utils.findRequiredView(view, R.id.lineView, "field 'lineView'");
    }

    public void unbind() {
        AccoutActivity accoutActivity = this.target;
        if (accoutActivity != null) {
            this.target = null;
            accoutActivity.ilResetPwd = null;
            accoutActivity.ilLogOut = null;
            accoutActivity.btnLogOut = null;
            accoutActivity.ilClearData = null;
            accoutActivity.lineView = null;
            this.view2131296509.setOnClickListener(null);
            this.view2131296509 = null;
            this.view2131296332.setOnClickListener(null);
            this.view2131296332 = null;
            this.view2131296496.setOnClickListener(null);
            this.view2131296496 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
