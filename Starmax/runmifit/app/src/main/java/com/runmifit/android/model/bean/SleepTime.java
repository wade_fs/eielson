package com.wade.fit.model.bean;

public class SleepTime {
    public int endH = 9;
    public int endM;
    public int startH = 23;
    public int startM;
    public int state;

    public String toString() {
        return "SleepTime{state=" + this.state + ", startH=" + this.startH + ", startM=" + this.startM + ", endH=" + this.endH + ", endM=" + this.endM + '}';
    }
}
