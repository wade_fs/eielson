package com.wade.fit.model.net;

import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.model.BaseBean;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.ToastUtil;
import java.net.SocketTimeoutException;
import p041io.reactivex.Observer;
import retrofit2.HttpException;

public abstract class ApiCallback<M extends BaseBean> implements Observer<M> {
    public abstract void onFailure(int i, String str);

    public abstract void onFinish();

    public abstract void onSuccess(M m);

    public void onError(Throwable th) {
        th.printStackTrace();
        if (th instanceof HttpException) {
            int code = ((HttpException) th).code();
            String string = AppApplication.getInstance().getString(R.string.state_network_error);
            onFailure(code, string);
            ToastUtil.showToast(string);
        } else if (th instanceof SocketTimeoutException) {
            onFailure(1001, "连接超时");
            ToastUtil.showToast(AppApplication.getInstance().getString(R.string.ble_connect_timeout));
        } else {
            onFailure(1002, "网络异常");
            ToastUtil.showToast(AppApplication.getInstance().getString(R.string.state_network_error));
        }
        onFinish();
    }

    public void onNext(M m) {
        int code = m.getCode();
        if (code == 200) {
            onSuccess(m);
        } else {
            String str = null;
            if (code == 1) {
                str = AppApplication.getInstance().getString(R.string.error_code_1);
            } else if (code == 2) {
                str = AppApplication.getInstance().getString(R.string.error_code_2);
            } else if (code == 3) {
                str = AppApplication.getInstance().getString(R.string.error_code_3);
            } else if (code == 4) {
                str = AppApplication.getInstance().getString(R.string.error_code_4);
            } else if (code == 5) {
                str = AppApplication.getInstance().getString(R.string.error_code_5);
            } else if (code == 6) {
                str = AppApplication.getInstance().getString(R.string.error_code_6);
            } else if (code == 7) {
                str = AppApplication.getInstance().getString(R.string.error_code_7);
            } else if (code == 8) {
                str = AppApplication.getInstance().getString(R.string.error_code_8);
            } else if (code == 9) {
                str = AppApplication.getInstance().getString(R.string.error_code_9);
            } else if (code == 10) {
                str = AppApplication.getInstance().getString(R.string.error_code_10);
            } else if (code == 11) {
                str = AppApplication.getInstance().getString(R.string.error_code_11);
            } else if (code == 401 || code == 402) {
                String string = AppApplication.getInstance().getString(R.string.error_code_401);
                DialogHelperNew.dismissWait();
                AppApplication.toLogin(string);
                return;
            } else if (code == 500) {
                str = AppApplication.getInstance().getString(R.string.error_code_500);
            }
            if (str != null) {
                ToastUtil.showToast(str);
            } else {
                str = m.getMessage();
            }
            onFailure(m.getCode(), str);
        }
        onFinish();
    }

    public void onComplete() {
        onFinish();
    }
}
