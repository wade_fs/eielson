package com.wade.fit.ui.device.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.base.BaseAdapter;
import com.wade.fit.base.BaseViewHolder;
import com.wade.fit.ui.device.adapter.SelectWeekDayAdapter;
import com.wade.fit.ui.device.bean.WeekDayBean;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.adapter.SelectWeekDayAdapter */
public class SelectWeekDayAdapter extends BaseAdapter<WeekDayBean, ViewHolder> {

    /* renamed from: com.wade.fit.ui.device.adapter.SelectWeekDayAdapter$ViewHolder_ViewBinding */
    public class ViewHolder_ViewBinding implements Unbinder {
        private ViewHolder target;

        public ViewHolder_ViewBinding(ViewHolder viewHolder, View view) {
            this.target = viewHolder;
            viewHolder.weekDayName = (TextView) Utils.findRequiredViewAsType(view, R.id.weekDayName, "field 'weekDayName'", TextView.class);
            viewHolder.isSelect = (ImageView) Utils.findRequiredViewAsType(view, R.id.isSelect, "field 'isSelect'", ImageView.class);
        }

        public void unbind() {
            ViewHolder viewHolder = this.target;
            if (viewHolder != null) {
                this.target = null;
                viewHolder.weekDayName = null;
                viewHolder.isSelect = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public SelectWeekDayAdapter(Context context, List<WeekDayBean> list) {
        super(context, list);
    }

    /* access modifiers changed from: protected */
    public void onNormalBindViewHolder(ViewHolder viewHolder, WeekDayBean weekDayBean, int i) {
        viewHolder.weekDayName.setText(weekDayBean.getWeekDayName());
        viewHolder.isSelect.setVisibility(weekDayBean.isSelect() ? 0 : 8);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener(viewHolder, i) {
            /* class com.wade.fit.ui.device.adapter.$$Lambda$SelectWeekDayAdapter$YynA2ZR4GoV2S4jyBjnNri344Z4 */
            private final /* synthetic */ SelectWeekDayAdapter.ViewHolder f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                SelectWeekDayAdapter.this.lambda$onNormalBindViewHolder$0$SelectWeekDayAdapter(this.f$1, this.f$2, view);
            }
        });
    }

    public /* synthetic */ void lambda$onNormalBindViewHolder$0$SelectWeekDayAdapter(ViewHolder viewHolder, int i, View view) {
        viewHolder.isSelect.setVisibility(viewHolder.isSelect.getVisibility() == 0 ? 8 : 0);
        this.mOnItemClickListener.onItemClick(viewHolder.itemView, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(this.inflater.inflate((int) R.layout.item_select_weekday, viewGroup, false));
    }

    /* renamed from: com.wade.fit.ui.device.adapter.SelectWeekDayAdapter$ViewHolder */
    public class ViewHolder extends BaseViewHolder {
        ImageView isSelect;
        TextView weekDayName;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
