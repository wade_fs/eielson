package com.wade.fit.base;

import com.wade.fit.base.BasePersenter;
import com.wade.fit.util.CnWinUtil;
import com.wade.fit.util.DialogHelperNew;

public abstract class BaseMvpActivity<T extends BasePersenter> extends BaseActivity implements IBaseView {
    protected T mPresenter;

    public void showNetError(String str) {
    }

    /* access modifiers changed from: protected */
    public void onViewCreate() {
        super.onViewCreate();
        this.mPresenter = (BasePersenter) CnWinUtil.getT(this, 0);
        T t = this.mPresenter;
        if (t != null) {
            t.attachView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        T t = this.mPresenter;
        if (t != null) {
            t.detachView();
        }
        super.onDestroy();
    }

    public void showMsg(String str) {
        showToast(str);
    }

    public void showLoading() {
        DialogHelperNew.buildWaitDialog(this, true);
    }

    public void showLoadingFalse() {
        DialogHelperNew.buildWaitDialog(this, false);
    }

    public void hideLoading() {
        DialogHelperNew.dismissWait();
    }

    public void goBack() {
        finish();
    }
}
