package com.wade.fit.ui.device.activity;

import android.text.TextUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.HeartRateInterval;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ble.BleCallbackWrapper;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.views.CustomHeartPhotoView;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.device.activity.HeartRateRegionActivity */
public class HeartRateRegionActivity extends BaseActivity {
    CustomHeartPhotoView customHeartPhotoView;
    ItemToggleLayout ilHeatWarning;
    HeartRateInterval proHealthHeartRate;
    RelativeLayout rlHeartRant;
    TextView tvMaxHr;
    TextView tvMixHr;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_heart_rate_region;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.heartrate_region_title));
        this.proHealthHeartRate = SPHelper.getInterval();
        this.customHeartPhotoView.setHeartRateValue(this.proHealthHeartRate.maxHr, this.proHealthHeartRate.minHr, AppApplication.getInstance().getUserBean().getYear());
        TextView textView = this.tvMaxHr;
        textView.setText(this.proHealthHeartRate.maxHr + getResources().getString(R.string.heart_unit));
        TextView textView2 = this.tvMixHr;
        textView2.setText(this.proHealthHeartRate.minHr + getResources().getString(R.string.heart_unit));
        this.ilHeatWarning.setOpen(this.proHealthHeartRate.isCustomHr);
        if (this.proHealthHeartRate.isCustomHr) {
            this.rlHeartRant.setVisibility(View.VISIBLE);
        } else {
            this.rlHeartRant.setVisibility(View.GONE);
        }
        this.ilHeatWarning.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$HeartRateRegionActivity$QCkXiQR3WegJE0vSOZIBKO1fRI8 */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                HeartRateRegionActivity.this.lambda$initView$0$HeartRateRegionActivity(itemToggleLayout, z);
            }
        });
    }

    public /* synthetic */ void lambda$initView$0$HeartRateRegionActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        this.proHealthHeartRate.isCustomHr = z;
        if (z) {
            this.rlHeartRant.setVisibility(View.VISIBLE);
        } else {
            this.rlHeartRant.setVisibility(View.GONE);
        }
        saveData();
    }

    /* access modifiers changed from: package-private */
    public void setHeartRateMax() {
        int i = this.proHealthHeartRate.maxHr;
        String string = getResources().getString(R.string.input_hr_max);
        DialogHelperNew.getInputDialog(this, i, string, getResources().getString(R.string.input_hr_max_range) + "220-80", new DialogHelperNew.SetInputCallback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$HeartRateRegionActivity$vHJeQ1v8FW2LTMFfvBm0VEBVok8 */

            public final void sure(int i) {
                HeartRateRegionActivity.this.lambda$setHeartRateMax$1$HeartRateRegionActivity(i);
            }
        });
    }

    public /* synthetic */ void lambda$setHeartRateMax$1$HeartRateRegionActivity(int i) {
        if (TextUtils.isEmpty(i + "")) {
            return;
        }
        if (i < 80 || i > 220) {
            showToast(getResources().getString(R.string.value_max_tip));
            return;
        }
        this.proHealthHeartRate.maxHr = i;
        TextView textView = this.tvMaxHr;
        textView.setText(i + getResources().getString(R.string.heart_unit));
        saveData();
    }

    /* access modifiers changed from: package-private */
    public void setHeartRateMin() {
        int i = this.proHealthHeartRate.minHr;
        String string = getResources().getString(R.string.input_hr_max);
        DialogHelperNew.getInputDialog(this, i, string, getResources().getString(R.string.input_hr_min_range) + "70-40", new DialogHelperNew.SetInputCallback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$HeartRateRegionActivity$bmNblT6q_CQWKOAt7zZCOSAPdw */

            public final void sure(int i) {
                HeartRateRegionActivity.this.lambda$setHeartRateMin$2$HeartRateRegionActivity(i);
            }
        });
    }

    public /* synthetic */ void lambda$setHeartRateMin$2$HeartRateRegionActivity(int i) {
        if (i < 40 || i > 70) {
            showToast(getResources().getString(R.string.value_min_tip));
            return;
        }
        this.proHealthHeartRate.minHr = i;
        TextView textView = this.tvMixHr;
        textView.setText(i + getResources().getString(R.string.heart_unit));
        saveData();
    }

    private void saveData() {
        DialogHelperNew.buildWaitDialog(this, true);
        boolean z = this.proHealthHeartRate.isCustomHr;
        BleSdkWrapper.setHeartRange(z ? 1 : 0, this.proHealthHeartRate.maxHr, this.proHealthHeartRate.minHr, new BleCallbackWrapper() {
            /* class com.wade.fit.ui.device.activity.HeartRateRegionActivity.C24651 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                super.complete(i, obj);
                SPHelper.saveInterval(HeartRateRegionActivity.this.proHealthHeartRate);
                DialogHelperNew.dismissWait();
            }
        });
    }
}
