package com.wade.fit.ui.main.bean;

import java.io.Serializable;

/* renamed from: com.wade.fit.ui.main.bean.MainMenuBean */
public class MainMenuBean implements Serializable {
    public String data1;
    public String data2;
    public boolean isFun = true;
    public boolean isShow;
    public String label;
    public String name;
    public int res;
    public String sportType;
    public int type;
    public String unit1;
    public String unit2;

    public String toString() {
        return "MainMenuBean{label='" + this.label + '\'' + ", type=" + this.type + ", isShow=" + this.isShow + ", name='" + this.name + '\'' + ", data1='" + this.data1 + '\'' + ", unit1='" + this.unit1 + '\'' + ", data2='" + this.data2 + '\'' + ", unit2='" + this.unit2 + '\'' + ", isFun=" + this.isFun + ", res=" + this.res + '}';
    }
}
