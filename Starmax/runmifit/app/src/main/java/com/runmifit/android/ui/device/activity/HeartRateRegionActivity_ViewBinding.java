package com.wade.fit.ui.device.activity;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.CustomHeartPhotoView;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.device.activity.HeartRateRegionActivity_ViewBinding */
public class HeartRateRegionActivity_ViewBinding implements Unbinder {
    private HeartRateRegionActivity target;
    private View view2131296809;
    private View view2131296810;

    public HeartRateRegionActivity_ViewBinding(HeartRateRegionActivity heartRateRegionActivity) {
        this(heartRateRegionActivity, heartRateRegionActivity.getWindow().getDecorView());
    }

    public HeartRateRegionActivity_ViewBinding(final HeartRateRegionActivity heartRateRegionActivity, View view) {
        this.target = heartRateRegionActivity;
        heartRateRegionActivity.customHeartPhotoView = (CustomHeartPhotoView) Utils.findRequiredViewAsType(view, R.id.custom_heart_photo_view, "field 'customHeartPhotoView'", CustomHeartPhotoView.class);
        heartRateRegionActivity.ilHeatWarning = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.ilHeatWarning, "field 'ilHeatWarning'", ItemToggleLayout.class);
        heartRateRegionActivity.rlHeartRant = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.rlHeartRant, "field 'rlHeartRant'", RelativeLayout.class);
        heartRateRegionActivity.tvMaxHr = (TextView) Utils.findRequiredViewAsType(view, R.id.tvMaxHr, "field 'tvMaxHr'", TextView.class);
        heartRateRegionActivity.tvMixHr = (TextView) Utils.findRequiredViewAsType(view, R.id.tvMixHr, "field 'tvMixHr'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.rlMax, "method 'setHeartRateMax'");
        this.view2131296809 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.HeartRateRegionActivity_ViewBinding.C24661 */

            public void doClick(View view) {
                heartRateRegionActivity.setHeartRateMax();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.rlMin, "method 'setHeartRateMin'");
        this.view2131296810 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.HeartRateRegionActivity_ViewBinding.C24672 */

            public void doClick(View view) {
                heartRateRegionActivity.setHeartRateMin();
            }
        });
    }

    public void unbind() {
        HeartRateRegionActivity heartRateRegionActivity = this.target;
        if (heartRateRegionActivity != null) {
            this.target = null;
            heartRateRegionActivity.customHeartPhotoView = null;
            heartRateRegionActivity.ilHeatWarning = null;
            heartRateRegionActivity.rlHeartRant = null;
            heartRateRegionActivity.tvMaxHr = null;
            heartRateRegionActivity.tvMixHr = null;
            this.view2131296809.setOnClickListener(null);
            this.view2131296809 = null;
            this.view2131296810.setOnClickListener(null);
            this.view2131296810 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
