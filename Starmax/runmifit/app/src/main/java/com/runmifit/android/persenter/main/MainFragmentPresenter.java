package com.wade.fit.persenter.main;

import android.os.Handler;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.bean.HealthSleepItem;
import com.wade.fit.greendao.bean.HealthSport;
import com.wade.fit.greendao.gen.HealthActivityDao;
import com.wade.fit.greendao.gen.HealthHeartRateDao;
import com.wade.fit.greendao.gen.HealthSleepItemDao;
import com.wade.fit.greendao.gen.HealthSportDao;
import com.wade.fit.persenter.main.MainFragmentContract;
import com.wade.fit.util.CacheHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ToastUtil;
import com.wade.fit.util.ble.BleCallback;
import com.wade.fit.util.ble.BleCallbackWrapper;
import com.wade.fit.util.ble.BleClient;
import com.wade.fit.util.ble.BleScanTool;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.ble.HandlerBleDataResult;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.MainVO;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainFragmentPresenter extends BasePersenter<MainFragmentContract.View> implements MainFragmentContract.Presenter {
    private static final int SYNCH_TIME_OUT = 60000;
    /* access modifiers changed from: private */
    public Calendar calendar;
    int delayMillis = 10000;
    int delayMillisSyn = 600000;
    private int distanDay = 0;
    BleCallback getCurrentStepCallback = new BleCallback() {
        /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24112 */

        public void setSuccess() {
        }

        public void complete(int i, Object obj) {
            if (BleClient.getInstance().isSynching() || !BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected()) {
                MainFragmentPresenter.this.synchLoaing = false;
                return;
            }
            if (obj instanceof HealthSport) {
                HealthSport healthSport = (HealthSport) obj;
                if (MainFragmentPresenter.this.mView != null) {
                    ((MainFragmentContract.View) MainFragmentPresenter.this.mView).updateSportVaule(healthSport);
                }
            }
            BleSdkWrapper.getHeartRate(new BleCallback() {
                /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24112.C24121 */

                public void setSuccess() {
                }

                public void complete(int i, Object obj) {
                    if (obj instanceof HealthHeartRate) {
                        MainFragmentPresenter.this.synchLoaing = false;
                        HealthHeartRate healthHeartRate = (HealthHeartRate) obj;
                        if (MainFragmentPresenter.this.mView != null) {
                            ((MainFragmentContract.View) MainFragmentPresenter.this.mView).updateHeartVaule(i, healthHeartRate.getSilentHeart());
                            ((MainFragmentContract.View) MainFragmentPresenter.this.mView).updateSych();
                        }
                    }
                }
            });
        }
    };
    Handler handler = new Handler();
    /* access modifiers changed from: private */
    public int mIndex;
    /* access modifiers changed from: private */
    public int synStatus = 100;
    public boolean synchLoaing = false;
    Runnable task = new Runnable() {
        /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24021 */

        public void run() {
            MainFragmentPresenter mainFragmentPresenter = MainFragmentPresenter.this;
            mainFragmentPresenter.synchLoaing = true;
            mainFragmentPresenter.handler.postDelayed(MainFragmentPresenter.this.task, (long) MainFragmentPresenter.this.delayMillis);
            if (!BleClient.getInstance().isSynching() && BleSdkWrapper.isBind() && BleSdkWrapper.isConnected()) {
                LogUtil.d("实时刷新中..........");
                BleSdkWrapper.getCurrentStep(MainFragmentPresenter.this.getCurrentStepCallback);
            }
        }
    };
    private Runnable timeOutTask = new Runnable() {
        /* class com.wade.fit.persenter.main.$$Lambda$MainFragmentPresenter$LbOodsjEomvL857s92SCgDx46U */

        public final void run() {
            MainFragmentPresenter.this.lambda$new$0$MainFragmentPresenter();
        }
    };

    static /* synthetic */ int access$1708(MainFragmentPresenter mainFragmentPresenter) {
        int i = mainFragmentPresenter.mIndex;
        mainFragmentPresenter.mIndex = i + 1;
        return i;
    }

    public void getHistoryData(int i, int i2, int i3, int i4) {
        int i5;
        MainVO mainVO = new MainVO();
        if (AppApplication.getInstance().getDaoSession() != null) {
            mainVO.healthSport = (HealthSport) AppApplication.getInstance().getDaoSession().getHealthSportDao().queryBuilder().where(HealthSportDao.Properties.Year.eq(Integer.valueOf(i2)), HealthSportDao.Properties.Month.eq(Integer.valueOf(i3)), HealthSportDao.Properties.Day.eq(Integer.valueOf(i4))).orderDesc(HealthSportDao.Properties.Date).build().unique();
            int geeSleepTime = geeSleepTime(i2, i3, i4);
            mainVO.healthSleep = new HealthSleep();
            mainVO.sleepHour = (geeSleepTime / 60) + "";
            mainVO.sleepMin = (geeSleepTime % 60) + "";
            HealthHeartRate healthHeartRate = (HealthHeartRate) AppApplication.getInstance().getDaoSession().getHealthHeartRateDao().queryBuilder().where(HealthHeartRateDao.Properties.Year.eq(Integer.valueOf(i2)), HealthHeartRateDao.Properties.Month.eq(Integer.valueOf(i3)), HealthHeartRateDao.Properties.Day.eq(Integer.valueOf(i4))).orderDesc(HealthHeartRateDao.Properties.Date).build().unique();
            if (healthHeartRate != null) {
                mainVO.healthHeartRate = healthHeartRate;
                mainVO.heartRate = healthHeartRate.getSilentHeart() + "";
            } else {
                mainVO.healthHeartRate = null;
            }
            List list = AppApplication.getInstance().getDaoSession().getHealthActivityDao().queryBuilder().where(HealthActivityDao.Properties.Year.eq(Integer.valueOf(i2)), HealthActivityDao.Properties.Month.eq(Integer.valueOf(i3)), HealthActivityDao.Properties.Day.eq(Integer.valueOf(i4))).orderDesc(HealthActivityDao.Properties.Date).build().list();
            if (list == null || list.size() <= 0) {
                i5 = 0;
            } else {
                i5 = ((HealthActivity) list.get(0)).getDurations();
                mainVO.sportType = CacheHelper.getSportName(((HealthActivity) list.get(0)).getType());
            }
            mainVO.sportTotalTime = i5;
            mainVO.sportHour = (i5 / 3600) + "";
            mainVO.sportMin = ((i5 % 3600) / 60) + "";
            ((MainFragmentContract.View) this.mView).requestSuccess(i, mainVO);
        }
    }

    public /* synthetic */ void lambda$new$0$MainFragmentPresenter() {
        if (this.synStatus == 101) {
            LogUtil.dAndSave("timeOutTask超时了....", Constants.SYCN_PATH);
            ((MainFragmentContract.View) this.mView).requestSuccess(5, null);
            ((MainFragmentContract.View) this.mView).requestSuccess(2, null);
            startTime();
        }
    }

    public void synchDate() {
        stopTime();
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected() || !BleScanTool.getInstance().isBluetoothOpen()) {
            ToastUtil.showToast(AppApplication.getInstance().getResources().getString(R.string.disConnected));
            return;
        }
        this.synStatus = 106;
        ((MainFragmentContract.View) this.mView).updateProgress(30);
        getDeviceInfo();
    }

    private void getDeviceInfo() {
        LogUtil.dAndSave("同步配置信息---getDeviceInfo>", Constants.SYCN_PATH);
        BleSdkWrapper.getDeviceInfo(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24133 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.getDeviceState();
            }
        });
    }

    /* access modifiers changed from: private */
    public void setDeviceState() {
        LogUtil.dAndSave("同步配置信息---setDeviceState>", Constants.SYCN_PATH);
        BleSdkWrapper.setDeviceState(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24144 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.setDeviceDate();
            }
        });
    }

    /* access modifiers changed from: private */
    public void setDeviceDate() {
        LogUtil.dAndSave("同步配置信息---setDeviceDate>", Constants.SYCN_PATH);
        BleSdkWrapper.setDeviceData(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24155 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.setUserInfo();
            }
        });
    }

    /* access modifiers changed from: private */
    public void setUserInfo() {
        BleSdkWrapper.setUserInfo(AppApplication.getInstance().getUserBean(), new BleCallbackWrapper() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24166 */

            public void setSuccess() {
                MainFragmentPresenter.this.getLongSit();
            }

            public void setFaild() {
                super.setFaild();
            }
        });
    }

    /* access modifiers changed from: private */
    public void getDeviceState() {
        LogUtil.dAndSave("同步配置信息---getDeviceState>", Constants.SYCN_PATH);
        BleSdkWrapper.getDeviceState(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24177 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.setDeviceState();
            }
        });
    }

    /* access modifiers changed from: private */
    public void getLongSit() {
        LogUtil.dAndSave("同步配置信息---getLongSit>", Constants.SYCN_PATH);
        BleSdkWrapper.getLongSit(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24188 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.getTarget();
            }
        }, false);
    }

    /* access modifiers changed from: private */
    public void getTarget() {
        LogUtil.dAndSave("同步配置信息---getTarget>", Constants.SYCN_PATH);
        BleSdkWrapper.getTarget(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C24199 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.getHeartRong();
            }
        }, false);
    }

    /* access modifiers changed from: private */
    public void getHeartRong() {
        LogUtil.dAndSave("同步配置信息---getHartRong>", Constants.SYCN_PATH);
        BleSdkWrapper.getHartRong(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C240310 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.getHeartOpen();
            }
        }, false);
    }

    /* access modifiers changed from: private */
    public void getHeartOpen() {
        LogUtil.dAndSave("同步配置信息---getHeartOpen>", Constants.SYCN_PATH);
        BleSdkWrapper.getHeartOpen(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C240411 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.getPower();
            }
        }, false);
    }

    /* access modifiers changed from: private */
    public void getPower() {
        LogUtil.dAndSave("同步配置信息---getPower>", Constants.SYCN_PATH);
        BleSdkWrapper.getPower(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C240512 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.getNotice();
            }
        });
    }

    /* access modifiers changed from: private */
    public void getNotice() {
        LogUtil.dAndSave("同步配置信息---getNotice>", Constants.SYCN_PATH);
        BleSdkWrapper.getNotice(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C240613 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                MainFragmentPresenter.this.getAlarmTask();
            }
        }, false);
    }

    /* access modifiers changed from: private */
    public void getAlarmTask() {
        LogUtil.dAndSave("同步配置信息---getAlarm>", Constants.SYCN_PATH);
        BleSdkWrapper.getAlarmList(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C240714 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                if ((obj instanceof HandlerBleDataResult) && ((HandlerBleDataResult) obj).isComplete) {
                    SharePreferenceUtils.put(AppApplication.getInstance(), "FIRST_SYNC", false);
                    int unused = MainFragmentPresenter.this.getDistanceDay();
                    int unused2 = MainFragmentPresenter.this.mIndex = 1;
                    MainFragmentPresenter.this.syncStepHistory(true);
                }
            }
        }, false);
    }

    public void startTime() {
        if (BleSdkWrapper.isBind() && BleSdkWrapper.isConnected() && this.synStatus == 100) {
            ((MainFragmentContract.View) this.mView).requestFaild(2);
            this.handler.removeCallbacks(this.task);
            LogUtil.d("开始实时刷新..........");
            this.handler.postDelayed(this.task, 0);
        }
    }

    public void stopTime() {
        if (BleSdkWrapper.isBind() && BleSdkWrapper.isConnected()) {
            LogUtil.d("停止实时刷新.........." + toString());
            this.handler.removeCallbacks(this.task);
        }
    }

    public int geeSleepTime(int i, int i2, int i3) {
        HealthSleepItemDao healthSleepItemDao = AppApplication.getInstance().getDaoSession().getHealthSleepItemDao();
        List list = healthSleepItemDao.queryBuilder().where(HealthSleepItemDao.Properties.Year.eq(Integer.valueOf(i)), HealthSleepItemDao.Properties.Month.eq(Integer.valueOf(i2)), HealthSleepItemDao.Properties.Day.eq(Integer.valueOf(i3))).orderDesc(HealthSleepItemDao.Properties.Date).build().list();
        if (list == null) {
            return 0;
        }
        Calendar instance = Calendar.getInstance();
        instance.set(i, i2 - 1, i3);
        instance.add(5, -1);
        int i4 = instance.get(1);
        int i5 = instance.get(5);
        list.size();
        List list2 = healthSleepItemDao.queryBuilder().where(HealthSleepItemDao.Properties.Year.eq(Integer.valueOf(i4)), HealthSleepItemDao.Properties.Month.eq(Integer.valueOf(instance.get(2) + 1)), HealthSleepItemDao.Properties.Day.eq(Integer.valueOf(i5))).orderDesc(HealthSleepItemDao.Properties.Date).build().list();
        new ArrayList();
        int i6 = 0;
        for (int i7 = 0; i7 < 85; i7++) {
            HealthSleepItem healthSleepItem = new HealthSleepItem();
            healthSleepItem.setOffsetMinute(10);
            if (i7 < 18) {
                if (list2 != null && i7 < list2.size() && list2.size() > 18) {
                    healthSleepItem = (HealthSleepItem) list2.get((list2.size() - 18) + i7);
                }
            } else if (i7 < list.size()) {
                healthSleepItem = (HealthSleepItem) list.get(i7 - 18);
            }
            if (healthSleepItem.getSleepStatus() == 1 || healthSleepItem.getSleepStatus() == 3 || healthSleepItem.getSleepStatus() == 2 || healthSleepItem.getSleepStatus() == 5 || healthSleepItem.getSleepStatus() == 4) {
                i6 += 10;
            }
        }
        return i6;
    }

    public void refreshingData() {
        stopTime();
        AppApplication.getInstance().setSysndata(true);
        if (!BleSdkWrapper.isBind() || !BleSdkWrapper.isConnected() || !BleScanTool.getInstance().isBluetoothOpen()) {
            ToastUtil.showToast(AppApplication.getInstance().getResources().getString(R.string.disConnected));
            return;
        }
        this.synStatus = 106;
        getDistanceDay();
        this.mIndex = 1;
        syncStepHistory(true);
    }

    /* access modifiers changed from: private */
    public int getDistanceDay() {
        long longValue = ((Long) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.LAST_SYNCH_TIME, 0L)).longValue();
        LogUtil.d("distanDay:" + longValue);
        Calendar instance = Calendar.getInstance();
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        int timeInMillis = (int) ((instance.getTimeInMillis() - longValue) / 86400000);
        if (timeInMillis <= 0) {
            return 2;
        }
        if (timeInMillis > 6) {
            return 7;
        }
        return timeInMillis + 1;
    }

    /* access modifiers changed from: private */
    public void syncStepHistory(boolean z) {
        ((MainFragmentContract.View) this.mView).updateProgress(55);
        if (z) {
            this.calendar = Calendar.getInstance();
        }
        BleSdkWrapper.getStepOrSleepHistory(1, this.calendar.get(1), this.calendar.get(2) + 1, this.calendar.get(5), new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C240815 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                if (obj instanceof HandlerBleDataResult) {
                    HandlerBleDataResult handlerBleDataResult = (HandlerBleDataResult) obj;
                    LogUtil.d("" + handlerBleDataResult.toString());
                    if (!handlerBleDataResult.isComplete) {
                        return;
                    }
                    if (!handlerBleDataResult.hasNext) {
                        MainFragmentPresenter.this.syncHeartHistory(true);
                    } else if (((Boolean) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.IS_SYN_HISTORY_KEY, true)).booleanValue()) {
                        MainFragmentPresenter.this.calendar.add(5, -1);
                        MainFragmentPresenter.this.syncStepHistory(false);
                    } else if (MainFragmentPresenter.this.mIndex == MainFragmentPresenter.this.getDistanceDay()) {
                        int unused = MainFragmentPresenter.this.mIndex = 1;
                        MainFragmentPresenter.this.syncHeartHistory(true);
                    } else {
                        MainFragmentPresenter.this.calendar.add(5, -1);
                        MainFragmentPresenter.access$1708(MainFragmentPresenter.this);
                        MainFragmentPresenter.this.syncStepHistory(false);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void syncHeartHistory(boolean z) {
        ((MainFragmentContract.View) this.mView).updateProgress(70);
        if (z) {
            this.calendar = Calendar.getInstance();
        }
        BleSdkWrapper.getHistoryHeartRateData(1, this.calendar.get(1), this.calendar.get(2) + 1, this.calendar.get(5), new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C240916 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                if (obj instanceof HandlerBleDataResult) {
                    HandlerBleDataResult handlerBleDataResult = (HandlerBleDataResult) obj;
                    if (!handlerBleDataResult.isComplete) {
                        return;
                    }
                    if (!handlerBleDataResult.hasNext) {
                        MainFragmentPresenter.this.synchActiviy();
                    } else if (((Boolean) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.IS_SYN_HISTORY_KEY, true)).booleanValue()) {
                        MainFragmentPresenter.this.calendar.add(5, -1);
                        MainFragmentPresenter.this.syncHeartHistory(false);
                    } else {
                        if (MainFragmentPresenter.this.mIndex == MainFragmentPresenter.this.getDistanceDay()) {
                            MainFragmentPresenter.this.synchActiviy();
                        } else {
                            MainFragmentPresenter.this.calendar.add(5, -1);
                            MainFragmentPresenter.access$1708(MainFragmentPresenter.this);
                            MainFragmentPresenter.this.syncHeartHistory(false);
                        }
                        MainFragmentPresenter.this.synchActiviy();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void synchActiviy() {
        ((MainFragmentContract.View) this.mView).updateProgress(98);
        BleSdkWrapper.getActivity(new BleCallback() {
            /* class com.wade.fit.persenter.main.MainFragmentPresenter.C241017 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                if (obj != null && (obj instanceof HandlerBleDataResult)) {
                    HandlerBleDataResult handlerBleDataResult = (HandlerBleDataResult) obj;
                    if (!handlerBleDataResult.isComplete) {
                        return;
                    }
                    if (handlerBleDataResult.hasNext) {
                        MainFragmentPresenter.this.synchActiviy();
                        return;
                    }
                    ((MainFragmentContract.View) MainFragmentPresenter.this.mView).updateProgress(100);
                    SharePreferenceUtils.put(AppApplication.getInstance(), Constants.LAST_SYNCH_TIME, Long.valueOf(System.currentTimeMillis()));
                    SharePreferenceUtils.put(AppApplication.getInstance(), Constants.IS_SYN_HISTORY_KEY, false);
                    AppApplication.getInstance().setSysndata(false);
                    int unused = MainFragmentPresenter.this.synStatus = 100;
                    ((MainFragmentContract.View) MainFragmentPresenter.this.mView).requestSuccess(5, null);
                    ((MainFragmentContract.View) MainFragmentPresenter.this.mView).requestSuccess(2, null);
                    ((MainFragmentContract.View) MainFragmentPresenter.this.mView).requestSuccess(6, null);
                }
            }
        });
    }
}
