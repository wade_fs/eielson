package com.wade.fit.base.refresh;

import com.wade.fit.base.BasePersenter;
import com.wade.fit.base.IBaseView;

public abstract class BaseListPersenter<V extends IBaseView> extends BasePersenter<V> {
    /* access modifiers changed from: protected */
    public abstract void onLoadMore();

    /* access modifiers changed from: protected */
    public abstract void onRefreshing();
}
