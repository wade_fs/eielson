package com.wade.fit.persenter.sport;

import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.base.IBaseView;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.bean.HealthSport;
import com.wade.fit.greendao.gen.HealthActivityDao;
import com.wade.fit.greendao.gen.HealthHeartRateDao;
import com.wade.fit.greendao.gen.HealthSleepDao;
import com.wade.fit.greendao.gen.HealthSportDao;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.util.CacheHelper;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.StringUtils;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.MainVO;
import com.tencent.bugly.beta.tinker.TinkerReport;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class BaseTimePresenter<V extends IBaseView> extends BasePersenter<V> {
    public Calendar calendar = Calendar.getInstance();
    public Date currentDate = new Date();
    protected SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected SimpleDateFormat dateFormat2 = new SimpleDateFormat("MM-dd");
    protected SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyy-MM-dd");
    protected SimpleDateFormat dateFormat4 = new SimpleDateFormat("yyyy-MM");
    protected List<String> dates = new ArrayList();
    protected int day;
    public Date endDate;
    public int index = 0;
    protected MainVO mainVO;
    protected int month;
    public Date startDate;
    protected DetailTimeType timeType = DetailTimeType.DAY;
    protected int year;

    public void setTimeType(DetailTimeType detailTimeType) {
        this.timeType = detailTimeType;
    }

    private void setHMS() {
        this.calendar.set(11, 0);
        this.calendar.set(12, 0);
        this.calendar.set(13, 0);
        this.calendar.set(14, 0);
    }

    public String getWeekStr(Calendar calendar2) {
        Calendar calendar3 = (Calendar) calendar2.clone();
        calendar3.add(5, -6);
        return this.dateFormat3.format(calendar3.getTime()) + "-" + this.dateFormat3.format(calendar2.getTime());
    }

    public String getWeekStr2(Calendar calendar2) {
        Calendar calendar3 = (Calendar) calendar2.clone();
        LogUtil.d("1111:" + this.dateFormat3.format(calendar3.getTime()));
        calendar3.add(5, -6);
        LogUtil.d("2222:" + this.dateFormat3.format(calendar3.getTime()));
        return this.dateFormat2.format(calendar3.getTime()) + "-" + this.dateFormat2.format(calendar2.getTime());
    }

    /* access modifiers changed from: protected */
    public String getDayRemark(int i) {
        StringBuilder sb = new StringBuilder();
        int i2 = i * 15;
        sb.append(StringUtils.format("%02d:%02d", Integer.valueOf(i2 / 60), Integer.valueOf(i2 % 60)));
        sb.append("-");
        int i3 = (i + 1) * 15;
        sb.append(StringUtils.format("%02d:%02d", Integer.valueOf(i3 / 60), Integer.valueOf(i3 % 60)));
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String getDayRemark(int i, int i2) {
        StringBuilder sb = new StringBuilder();
        int i3 = i * i2;
        sb.append(StringUtils.format("%02d:%02d", Integer.valueOf(i3 / 60), Integer.valueOf(i3 % 60)));
        sb.append("-");
        int i4 = (i + 1) * i2;
        sb.append(StringUtils.format("%02d:%02d", Integer.valueOf(i4 / 60), Integer.valueOf(i4 % 60)));
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public boolean isDateEquals(Date date, Date date2) {
        return date != null && date2 != null && date.getYear() == date2.getYear() && date.getMonth() == date2.getMonth() && date.getDate() == date2.getDate();
    }

    /* access modifiers changed from: protected */
    public boolean isEndMonth(int i, int i2, int i3) {
        return i3 == DateUtil.dayOfMonth(i, i2);
    }

    public void setOneMonth() {
        this.timeType = DetailTimeType.ONE_MONTH;
        this.calendar = Calendar.getInstance();
        this.dates.clear();
        setCalendarZero(this.calendar);
        this.calendar.set(11, 23);
        this.calendar.set(12, 59);
        this.calendar.set(13, 59);
        this.startDate = this.calendar.getTime();
        this.calendar.set(11, 0);
        this.calendar.set(12, 0);
        this.calendar.set(13, 0);
        this.calendar.add(2, -1);
        this.endDate = this.calendar.getTime();
        this.year = this.calendar.get(1);
        this.month = this.calendar.get(2) + 1;
        this.day = this.calendar.get(5);
        LogUtil.d("start date:" + this.dateFormat.format(this.startDate));
        LogUtil.d("end date:" + this.dateFormat.format(this.endDate));
    }

    public void setSixMonth() {
        this.timeType = DetailTimeType.SIX_MONTH;
        this.calendar = Calendar.getInstance();
        this.dates.clear();
        int intValue = ((Integer) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.WEEK_START_DAY_INDEX, 0)).intValue();
        this.calendar.setTime(ProDbUtils.getWeekEndDate(0, intValue));
        setCalendarZero(this.calendar);
        LogUtil.d("startDate:" + this.dateFormat.format(this.calendar.getTime()));
        this.calendar.set(11, 23);
        this.calendar.set(12, 59);
        this.calendar.set(13, 59);
        this.startDate = this.calendar.getTime();
        this.calendar.setTime(ProDbUtils.getWeekStartDate(23, intValue));
        setCalendarZero(this.calendar);
        this.endDate = this.calendar.getTime();
        this.year = this.calendar.get(1);
        this.month = this.calendar.get(2) + 1;
        this.day = this.calendar.get(5);
        LogUtil.d("start date:" + this.dateFormat.format(this.startDate));
        LogUtil.d("endDate:" + this.dateFormat.format(this.endDate));
    }

    public void setOneWeek() {
        this.timeType = DetailTimeType.WEEK;
        this.calendar = Calendar.getInstance();
        this.calendar.set(11, 23);
        this.calendar.set(12, 59);
        this.calendar.set(13, 59);
        this.startDate = this.calendar.getTime();
        this.calendar.add(5, -7);
        setCalendarZero(this.calendar);
        this.endDate = this.calendar.getTime();
        this.dates.clear();
        this.year = this.calendar.get(1);
        this.month = this.calendar.get(2) + 1;
        this.day = this.calendar.get(5);
        LogUtil.d("start date:" + this.dateFormat.format(this.startDate));
        LogUtil.d("end date:" + this.dateFormat.format(this.endDate));
    }

    public void setYear() {
        this.timeType = DetailTimeType.YEAR;
        this.calendar = Calendar.getInstance();
        this.dates.clear();
        this.calendar.set(11, 23);
        this.calendar.set(12, 59);
        this.calendar.set(13, 59);
        this.startDate = this.calendar.getTime();
        this.calendar.add(2, -11);
        setCalendarZero(this.calendar);
        this.endDate = this.calendar.getTime();
        this.year = this.calendar.get(1);
        this.month = this.calendar.get(2) + 1;
        this.day = this.calendar.get(5);
        LogUtil.d("start date:" + this.dateFormat.format(this.startDate));
        LogUtil.d("end date:" + this.dateFormat.format(this.endDate));
    }

    /* renamed from: com.wade.fit.persenter.sport.BaseTimePresenter$1 */
    static /* synthetic */ class C24301 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailTimeType = new int[DetailTimeType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.wade.fit.model.bean.DetailTimeType[] r0 = com.wade.fit.model.bean.DetailTimeType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.persenter.sport.BaseTimePresenter.C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType = r0
                int[] r0 = com.wade.fit.persenter.sport.BaseTimePresenter.C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.DAY     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.persenter.sport.BaseTimePresenter.C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.ONE_MONTH     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.persenter.sport.BaseTimePresenter.C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.SIX_MONTH     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.wade.fit.persenter.sport.BaseTimePresenter.C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.YEAR     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.persenter.sport.BaseTimePresenter.C24301.<clinit>():void");
        }
    }

    public MainVO preDate(DetailTimeType detailTimeType) {
        setTimeType(detailTimeType);
        int i = C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
        if (i == 1) {
            this.calendar.add(5, -1);
        } else if (i == 2) {
            this.startDate = this.calendar.getTime();
            this.calendar.add(5, -30);
            setCalendarZero(this.calendar);
            this.endDate = this.calendar.getTime();
        } else if (i == 3) {
            this.startDate = this.calendar.getTime();
            this.calendar.add(5, -180);
            setCalendarZero(this.calendar);
            this.endDate = this.calendar.getTime();
        } else if (i == 4) {
            this.startDate = this.calendar.getTime();
            this.calendar.add(2, -11);
            setCalendarZero(this.calendar);
            this.endDate = this.calendar.getTime();
        }
        this.index--;
        this.currentDate = this.calendar.getTime();
        return getByDate();
    }

    public MainVO currentDate(DetailTimeType detailTimeType, Date date) {
        setTimeType(detailTimeType);
        this.calendar = Calendar.getInstance();
        this.calendar.setTime(date);
        this.currentDate = this.calendar.getTime();
        this.index = 0;
        return getByDate();
    }

    public MainVO currentDate(DetailTimeType detailTimeType) {
        setTimeType(detailTimeType);
        this.calendar = Calendar.getInstance();
        this.currentDate = this.calendar.getTime();
        this.index = 0;
        return getByDate();
    }

    public MainVO nextDate(DetailTimeType detailTimeType) {
        setTimeType(detailTimeType);
        Calendar calendar2 = (Calendar) this.calendar.clone();
        calendar2.add(5, 1);
        if (calendar2.getTime().getTime() > new Date().getTime()) {
            return getByDate();
        }
        int i = C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
        if (i == 1) {
            this.calendar.add(5, 1);
        } else if (i == 2) {
            setCalendarZero(this.calendar);
            this.endDate = this.calendar.getTime();
            this.calendar.add(5, 30);
            this.startDate = this.calendar.getTime();
        } else if (i == 3) {
            setCalendarZero(this.calendar);
            this.endDate = this.calendar.getTime();
            this.calendar.add(5, TinkerReport.KEY_APPLIED_VERSION_CHECK);
            this.startDate = this.calendar.getTime();
        } else if (i == 4) {
            setCalendarZero(this.calendar);
            this.endDate = this.calendar.getTime();
            this.calendar.add(2, -11);
            this.startDate = this.calendar.getTime();
        }
        this.currentDate = this.calendar.getTime();
        this.index++;
        return getByDate();
    }

    /* access modifiers changed from: protected */
    public MainVO getByDate() {
        this.mainVO = new MainVO();
        this.year = this.calendar.get(1);
        this.month = this.calendar.get(2) + 1;
        this.day = this.calendar.get(5);
        int i = 0;
        HealthSport healthSport = (HealthSport) AppApplication.getInstance().getDaoSession().getHealthSportDao().queryBuilder().where(HealthSportDao.Properties.Date.eq(Integer.valueOf(this.year)), HealthSportDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthSportDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthSportDao.Properties.Date).build().unique();
        if (healthSport != null) {
            this.mainVO.healthSport = healthSport;
        } else {
            HealthSport healthSport2 = new HealthSport();
            healthSport2.setDate(ProDbUtils.getDate(this.year, this.month, this.day).getTime());
            this.mainVO.healthSport = healthSport2;
        }
        HealthSleep healthSleep = (HealthSleep) AppApplication.getInstance().getDaoSession().getHealthSleepDao().queryBuilder().where(HealthSleepDao.Properties.Date.eq(Integer.valueOf(this.year)), HealthSleepDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthSleepDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthSleepDao.Properties.Date).build().unique();
        if (healthSleep != null) {
            MainVO mainVO2 = this.mainVO;
            mainVO2.healthSleep = healthSleep;
            mainVO2.sleepHour = (healthSleep.getTotalSleepMinutes() / 60) + "";
            MainVO mainVO3 = this.mainVO;
            mainVO3.sleepMin = (healthSleep.getTotalSleepMinutes() % 60) + "";
        } else {
            HealthSleep healthSleep2 = new HealthSleep();
            healthSleep2.setDate(ProDbUtils.getDate(this.year, this.month, this.day).getTime());
            this.mainVO.healthSleep = healthSleep2;
        }
        HealthHeartRate healthHeartRate = (HealthHeartRate) AppApplication.getInstance().getDaoSession().getHealthHeartRateDao().queryBuilder().where(HealthHeartRateDao.Properties.Date.eq(Integer.valueOf(this.year)), HealthHeartRateDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthHeartRateDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthHeartRateDao.Properties.Date).build().unique();
        if (healthHeartRate != null) {
            MainVO mainVO4 = this.mainVO;
            mainVO4.healthHeartRate = healthHeartRate;
            mainVO4.heartRate = healthHeartRate.getSilentHeart() + "";
        } else {
            HealthHeartRate healthHeartRate2 = new HealthHeartRate();
            healthHeartRate2.setDate(ProDbUtils.getDate(this.year, this.month, this.day).getTime());
            this.mainVO.healthHeartRate = healthHeartRate2;
        }
        HealthActivity healthActivity = (HealthActivity) AppApplication.getInstance().getDaoSession().getHealthActivityDao().queryBuilder().where(HealthActivityDao.Properties.Date.eq(Integer.valueOf(this.year)), HealthActivityDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthActivityDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthActivityDao.Properties.Date).build().unique();
        if (healthActivity != null) {
            i = healthActivity.getDurations() / 60;
            this.mainVO.sportType = CacheHelper.getSportName(healthActivity.getType());
        }
        MainVO mainVO5 = this.mainVO;
        mainVO5.sportTotalTime = i;
        mainVO5.sportHour = (i / 60) + "";
        MainVO mainVO6 = this.mainVO;
        mainVO6.sportMin = (i % 60) + "";
        if (C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()] != 1) {
            MainVO mainVO7 = this.mainVO;
            mainVO7.date = this.dateFormat3.format(this.endDate) + "～" + this.dateFormat3.format(this.startDate);
        } else {
            this.mainVO.date = this.dateFormat3.format(this.calendar.getTime());
        }
        return this.mainVO;
    }

    /* access modifiers changed from: protected */
    public void setCalendarZero(Calendar calendar2) {
        calendar2.set(11, 0);
        calendar2.set(12, 0);
        calendar2.set(13, 0);
        calendar2.set(14, 0);
    }

    /* access modifiers changed from: protected */
    public void setCalendarZero(Date date) {
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
    }

    /* access modifiers changed from: protected */
    public void setDateLabel(int i, Calendar calendar2, int i2) {
        int i3 = C24301.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
        if (i3 != 2) {
            if (i3 != 3) {
                if (i3 == 4) {
                    this.dates.add(StringUtils.format("%02d", Integer.valueOf(i2)));
                }
            } else if (i % 28 == 0 || calendar2.getTime().getTime() == this.startDate.getTime()) {
                this.dates.add(this.dateFormat2.format(calendar2.getTime()));
            }
        } else if (i % 6 == 0) {
            this.dates.add(this.dateFormat2.format(calendar2.getTime()));
        }
    }

    /* access modifiers changed from: protected */
    public boolean isWeek(int i, Calendar calendar2) {
        return (i + 1) % 7 == 0 || calendar2.getTime().getTime() == this.startDate.getTime();
    }
}
