package com.wade.fit.persenter.mine;

import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.http.UserHttp;
import com.wade.fit.persenter.mine.FeedBackContract;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import p041io.reactivex.disposables.Disposable;

public class FeedBackPresenter extends BasePersenter<FeedBackContract.View> implements FeedBackContract.Presenter {
    public void uploadFeedbackInfo(String str, String str2) {
        ((FeedBackContract.View) this.mView).showLoadingFalse();
        UserHttp.uploadFeedbackInfo(RequestBody.create(MediaType.parse("application/json"), "{\"feedbackContent\":\"" + str + "\",\"contactWay\":\"" + str2 + "\"}"), new ApiCallback<BaseBean<String>>() {
            /* class com.wade.fit.persenter.mine.FeedBackPresenter.C24241 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<String> baseBean) {
                ((FeedBackContract.View) FeedBackPresenter.this.mView).hideLoading();
                ((FeedBackContract.View) FeedBackPresenter.this.mView).requestSuccess();
            }

            public void onFailure(int i, String str) {
                ((FeedBackContract.View) FeedBackPresenter.this.mView).hideLoading();
                ((FeedBackContract.View) FeedBackPresenter.this.mView).requestFaild();
            }

            public void onSubscribe(Disposable disposable) {
                FeedBackPresenter.this.addDisposable(disposable);
            }
        });
    }
}
