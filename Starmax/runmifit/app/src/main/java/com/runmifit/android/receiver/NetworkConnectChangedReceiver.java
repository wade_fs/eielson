package com.wade.fit.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.NetUtils;

public class NetworkConnectChangedReceiver extends BroadcastReceiver {
    private static final String TAG = "NetworkChangedReceiver";

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != "android.net.conn.CONNECTIVITY_CHANGE") {
            return;
        }
        if (!NetUtils.isConnected()) {
            EventBusHelper.post(1004);
        } else {
            EventBusHelper.post(1005);
        }
    }
}
