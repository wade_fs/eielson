package com.wade.fit.ui.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.wade.fit.R;
import com.wade.fit.base.BaseCalendarActivity;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.model.bean.SleepDetailVO;
import com.wade.fit.persenter.main.DetailSleep2Presenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.views.SleepBarChart;
import com.wade.fit.views.SleepBarDetailChart;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/* renamed from: com.wade.fit.ui.main.activity.DetailSleepActivity */
public class DetailSleepActivity extends BaseCalendarActivity<DetailSleep2Presenter> {
    Activity activity;
    private Calendar calendar = Calendar.getInstance();
    ImageView ivNextDate;
    ImageView ivPreDate;
    private List<String> lableList = new ArrayList();
    /* access modifiers changed from: private */
    public Date mSearchDate;
    int showType;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    SleepDetailVO sleepDetailVO;
    SleepBarChart sleep_BarChart;
    SleepBarDetailChart sleepdetailbar;
    private List<HealthSleep> sportDatas = new ArrayList();
    DetailTimeType timeType;
    TextView tvDate;
    TextView tvDeepSleepH;
    TextView tvDeepSleepM;
    TextView tvLightSleepH;
    TextView tvLightSleepM;
    TextView tvMonth;
    TextView tvStartSleepH;
    TextView tvStartSleepM;
    TextView tvTotal;
    TextView tvWakeSleepH;
    TextView tvWakeSleepM;
    TextView tvWeek;
    TextView tvYear;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_detail_sleep;
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id != R.id.ivSelectedDate && id != R.id.tvDate) {
            switch (id) {
                case R.id.rbDay /*2131296769*/:
                    updateWeekView();
                    this.showType = 0;
                    this.timeType = DetailTimeType.DAY;
                    this.ivPreDate.setVisibility(View.VISIBLE);
                    this.ivNextDate.setVisibility(View.VISIBLE);
                    updateData(((DetailSleep2Presenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate), 0);
                    return;
                case R.id.rbOneMonth /*2131296770*/:
                    this.showType = 1;
                    this.timeType = DetailTimeType.ONE_MONTH;
                    this.ivPreDate.setVisibility(View.GONE);
                    this.ivNextDate.setVisibility(View.GONE);
                    updateData(((DetailSleep2Presenter) this.mPresenter).getOneMonth(), 1);
                    return;
                case R.id.rbSixMonth /*2131296771*/:
                    this.showType = 2;
                    this.ivPreDate.setVisibility(View.GONE);
                    this.ivNextDate.setVisibility(View.GONE);
                    this.timeType = DetailTimeType.SIX_MONTH;
                    updateData(((DetailSleep2Presenter) this.mPresenter).getSixMonth(), 2);
                    return;
                case R.id.rbYear /*2131296772*/:
                    updateYearView();
                    this.ivPreDate.setVisibility(View.GONE);
                    this.ivNextDate.setVisibility(View.GONE);
                    this.timeType = DetailTimeType.YEAR;
                    this.showType = 3;
                    updateData(((DetailSleep2Presenter) this.mPresenter).getYearMonth(), 3);
                    return;
                default:
                    return;
            }
        } else if (this.timeType == DetailTimeType.DAY) {
            this.clendarDialog.mOnSelectDateListener = new OnSelectDateListener() {
                /* class com.wade.fit.ui.main.activity.DetailSleepActivity.C25121 */

                public void onSelectOtherMonth(int i) {
                }

                public void onSelectDate(CalendarDate calendarDate) {
                    Date unused = DetailSleepActivity.this.mSearchDate = ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day);
                    DetailSleepActivity detailSleepActivity = DetailSleepActivity.this;
                    detailSleepActivity.updateData(((DetailSleep2Presenter) detailSleepActivity.mPresenter).getDetailCurrent(DetailTimeType.DAY, ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day)), 0);
                }
            };
            this.clendarDialog.showDialog();
        }
    }

    /* access modifiers changed from: private */
    public void updateData(SleepDetailVO sleepDetailVO2, int i) {
        if (this.timeType == DetailTimeType.DAY) {
            changeDateUpdateUI(this.mSearchDate);
        } else {
            this.tvDate.setText(sleepDetailVO2.mainVO.date);
        }
        this.sleep_BarChart.setVisibility(View.GONE);
        HealthSleep healthSleep = sleepDetailVO2.mainVO.healthSleep;
        if (healthSleep == null) {
            healthSleep = new HealthSleep();
        }
        this.sleep_BarChart.setDatas(healthSleep, sleepDetailVO2.sleepItemList);
        this.tvStartSleepH.setText(String.valueOf(healthSleep.getSleepstartedTimeM() / 60));
        this.tvStartSleepM.setText(String.valueOf(healthSleep.getSleepstartedTimeM() % 60));
        this.tvDeepSleepH.setText(String.valueOf(healthSleep.getDeepSleepMinutes() / 60));
        this.tvDeepSleepM.setText(String.valueOf(healthSleep.getDeepSleepMinutes() % 60));
        this.tvLightSleepH.setText(String.valueOf(healthSleep.getLightSleepMinutes() / 60));
        this.tvLightSleepM.setText(String.valueOf(healthSleep.getLightSleepMinutes() % 60));
        this.tvWakeSleepH.setText(String.valueOf(healthSleep.getWakeMunutes() / 60));
        this.tvWakeSleepM.setText(String.valueOf(healthSleep.getWakeMunutes() % 60));
        int i2 = 0;
        this.sleepdetailbar.setVisibility(View.VISIBLE);
        if (i == 0) {
            this.sleepdetailbar.setDatas(sleepDetailVO2.sleepItemList, "00:23", "23:34", sleepDetailVO2.dates);
        } else {
            this.sleepdetailbar.setData(i, sleepDetailVO2.sleepList, sleepDetailVO2.dates);
        }
        if (this.timeType == DetailTimeType.DAY) {
            TextView textView = this.tvTotal;
            textView.setText(getResources().getString(R.string.detail_totalSleep) + " " + (healthSleep.getTotalSleepMinutes() / 60) + " " + getResources().getString(R.string.unit_hour) + " " + (healthSleep.getTotalSleepMinutes() % 60) + " " + getResources().getString(R.string.unit_minute));
            return;
        }
        int i3 = 0;
        for (HealthSleep healthSleep2 : sleepDetailVO2.sleepList) {
            if (healthSleep2.getTotalSleepMinutes() > 0) {
                i3 += healthSleep2.getTotalSleepMinutes();
                i2++;
            }
        }
        if (i2 == 0) {
            i2 = 1;
        }
        int i4 = i3 / i2;
        TextView textView2 = this.tvTotal;
        textView2.setText(getResources().getString(R.string.avg_sleep) + " " + (i4 / 60) + " " + getResources().getString(R.string.unit_hour) + " " + (i4 % 60) + " " + getResources().getString(R.string.unit_minute));
    }

    private void updateWeekView() {
        Date startdayThisWeek = DateUtil.getStartdayThisWeek(0, 1);
        Calendar instance = Calendar.getInstance();
        instance.setTime(startdayThisWeek);
        this.lableList.clear();
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                List<String> list = this.lableList;
                list.add(instance.get(5) + "");
            } else {
                instance.add(5, 1);
                List<String> list2 = this.lableList;
                list2.add(instance.get(5) + "");
            }
        }
        String format = this.simpleDateFormat.format(startdayThisWeek);
        String format2 = this.simpleDateFormat.format(instance.getTime());
        TextView textView = this.tvDate;
        textView.setText(format + "～" + format2);
        this.tvDate.setTag(format2);
    }

    private void updateMonthView() {
        int actualMaximum = this.calendar.getActualMaximum(5);
        this.lableList.clear();
        for (int actualMinimum = this.calendar.getActualMinimum(5); actualMinimum <= actualMaximum; actualMinimum++) {
            List<String> list = this.lableList;
            list.add(actualMinimum + "");
        }
        TextView textView = this.tvDate;
        textView.setText(String.format("%02d", Integer.valueOf(this.calendar.getActualMinimum(5))) + "-" + String.format("%02d", Integer.valueOf(this.calendar.get(2) + 1)) + "-" + this.calendar.get(1) + "～" + String.format("%02d", Integer.valueOf(this.calendar.getActualMaximum(5))) + "-" + String.format("%02d", Integer.valueOf(this.calendar.get(2) + 1)) + "-" + this.calendar.get(1));
        TextView textView2 = this.tvDate;
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%02d", Integer.valueOf(this.calendar.getActualMaximum(5))));
        sb.append("/");
        sb.append(String.format("%02d", Integer.valueOf(this.calendar.get(2) + 1)));
        sb.append("/");
        sb.append(this.calendar.get(1));
        textView2.setTag(sb.toString());
    }

    private void updateYearView() {
        this.lableList.clear();
        for (int i = 1; i <= 12; i++) {
            List<String> list = this.lableList;
            list.add(i + "");
        }
        TextView textView = this.tvDate;
        textView.setText(this.calendar.get(1) + getResources().getString(R.string.year));
        this.tvDate.setTag(this.simpleDateFormat.format(this.calendar.getTime()));
    }

    public static void startActivity(Activity activity2, Date date) {
        Intent intent = new Intent(activity2, DetailSleepActivity.class);
        intent.putExtra("DETAIL_DATE_KEY", date);
        activity2.startActivity(intent);
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.detail_sleep));
        this.activity = this;
        this.showType = 0;
        this.timeType = DetailTimeType.DAY;
        this.mSearchDate = (Date) getIntent().getSerializableExtra("DETAIL_DATE_KEY");
        this.sleepDetailVO = ((DetailSleep2Presenter) this.mPresenter).getDetailCurrent(this.timeType, this.mSearchDate);
        updateData(this.sleepDetailVO, this.showType);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: package-private */
    public void changeNextDay() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ivNextDate, 500)) {
            this.mSearchDate = DateUtil.getSpecifiedDayAfterDate(DateUtil.formatYMD.format(this.mSearchDate));
            updateData(((DetailSleep2Presenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate), 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void changePreDay() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ivPreDate, 500)) {
            this.mSearchDate = DateUtil.getSpecifiedDayBeforeDate(DateUtil.formatYMD.format(this.mSearchDate));
            updateData(((DetailSleep2Presenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate), 0);
        }
    }

    private void changeDateUpdateUI(Date date) {
        if (DateUtil.formatYMD.format(date).equals(DateUtil.formatYMD.format(Calendar.getInstance().getTime()))) {
            this.tvDate.setText(getResources().getString(R.string.today));
            this.ivNextDate.setVisibility(View.INVISIBLE);
            return;
        }
        this.tvDate.setText(DateUtil.formatYMD.format(date));
        this.ivNextDate.setVisibility(View.VISIBLE);
    }
}
