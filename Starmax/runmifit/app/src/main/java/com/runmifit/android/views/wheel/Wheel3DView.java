package com.wade.fit.views.wheel;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;

public class Wheel3DView extends WheelView {
    private Camera mCamera;
    private Matrix mMatrix;

    public Wheel3DView(Context context) {
        this(context, null);
    }

    public Wheel3DView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mCamera = new Camera();
        this.mMatrix = new Matrix();
    }

    public int getPrefWidth() {
        int prefWidth = super.getPrefWidth();
        double visibleItems = (double) (this.itemHeight * getVisibleItems() * 2);
        Double.isNaN(visibleItems);
        double sin = Math.sin(0.06544984694978735d);
        double d = (double) ((int) (visibleItems / 3.141592653589793d));
        Double.isNaN(d);
        return prefWidth + ((int) (sin * d));
    }

    public int getPrefHeight() {
        int paddingTop = getPaddingTop() + getPaddingBottom();
        double visibleItems = (double) (this.itemHeight * getVisibleItems() * 2);
        Double.isNaN(visibleItems);
        return ((int) (visibleItems / 3.141592653589793d)) + paddingTop;
    }

    /* access modifiers changed from: protected */
    public void drawItem(Canvas canvas, int i, int i2) {
        Canvas canvas2 = canvas;
        int i3 = i;
        CharSequence charSequence = getCharSequence(i3);
        if (charSequence != null) {
            double d = (double) this.itemHeight;
            double visibleItems = (double) getVisibleItems();
            Double.isNaN(visibleItems);
            Double.isNaN(d);
            double d2 = (double) ((int) (d * (visibleItems - 0.3d)));
            Double.isNaN(d2);
            int itemIndex = ((i3 - this.mScroller.getItemIndex()) * this.itemHeight) - i2;
            double d3 = (double) ((int) (d2 / 3.141592653589793d));
            Double.isNaN(d3);
            if (((double) Math.abs(itemIndex)) <= (3.141592653589793d * d3) / 2.0d) {
                double d4 = (double) itemIndex;
                Double.isNaN(d4);
                Double.isNaN(d3);
                double d5 = d4 / d3;
                float degrees = (float) Math.toDegrees(-d5);
                double sin = Math.sin(d5);
                Double.isNaN(d3);
                float f = (float) (sin * d3);
                Double.isNaN(d3);
                float cos = (float) ((1.0d - Math.cos(d5)) * d3);
                float textSize = ((float) getTextSize()) * 0.05f;
                int cos2 = (int) (Math.cos(d5) * 255.0d);
                int paddingLeft = getPaddingLeft();
                int width = getWidth() - getPaddingRight();
                int paddingTop = getPaddingTop();
                int height = getHeight() - getPaddingBottom();
                if (Math.abs(itemIndex) <= 0) {
                    this.mPaint.setColor(getSelectedColor());
                    canvas.save();
                    canvas2.translate(textSize, 0.0f);
                    canvas2.clipRect(paddingLeft, this.upperLimit, width, this.lowerLimit);
                    drawText(canvas, charSequence, 0.0f, f, cos, degrees);
                    canvas.restore();
                } else if (itemIndex <= 0 || itemIndex >= this.itemHeight) {
                    int i4 = height;
                    float f2 = cos;
                    int i5 = width;
                    if (itemIndex >= 0 || itemIndex <= (-this.itemHeight)) {
                        this.mPaint.setColor(getUnselectedColor());
                        this.mPaint.setAlpha(cos2);
                        canvas.save();
                        canvas2.clipRect(paddingLeft, paddingTop, i5, i4);
                        drawText(canvas, charSequence, 0.0f, f, f2, degrees);
                        canvas.restore();
                        return;
                    }
                    this.mPaint.setColor(getSelectedColor());
                    canvas.save();
                    canvas2.translate(textSize, 0.0f);
                    canvas2.clipRect(paddingLeft, this.upperLimit, i5, this.lowerLimit);
                    CharSequence charSequence2 = charSequence;
                    float f3 = f;
                    int i6 = paddingTop;
                    float f4 = f2;
                    int i7 = i6;
                    float f5 = degrees;
                    drawText(canvas, charSequence2, 0.0f, f3, f4, f5);
                    canvas.restore();
                    this.mPaint.setColor(getUnselectedColor());
                    this.mPaint.setAlpha(cos2);
                    canvas.save();
                    canvas2.clipRect(paddingLeft, i7, i5, this.upperLimit);
                    drawText(canvas, charSequence2, 0.0f, f3, f4, f5);
                    canvas.restore();
                } else {
                    this.mPaint.setColor(getSelectedColor());
                    canvas.save();
                    canvas2.translate(textSize, 0.0f);
                    canvas2.clipRect(paddingLeft, this.upperLimit, width, this.lowerLimit);
                    CharSequence charSequence3 = charSequence;
                    int i8 = height;
                    float f6 = f;
                    float f7 = cos;
                    float f8 = cos;
                    int i9 = width;
                    float f9 = degrees;
                    drawText(canvas, charSequence3, 0.0f, f6, f7, f9);
                    canvas.restore();
                    this.mPaint.setColor(getUnselectedColor());
                    this.mPaint.setAlpha(cos2);
                    canvas.save();
                    canvas2.clipRect(paddingLeft, this.lowerLimit, i9, i8);
                    drawText(canvas, charSequence3, 0.0f, f6, f8, f9);
                    canvas.restore();
                }
            }
        }
    }

    private void drawText(Canvas canvas, CharSequence charSequence, float f, float f2, float f3, float f4) {
        this.mCamera.save();
        this.mCamera.translate(f, 0.0f, f3);
        this.mCamera.rotateX(f4);
        this.mCamera.getMatrix(this.mMatrix);
        this.mCamera.restore();
        float f5 = (float) this.centerX;
        float f6 = ((float) this.centerY) + f2;
        this.mMatrix.preTranslate(-f5, -f6);
        this.mMatrix.postTranslate(f5, f6);
        canvas.concat(this.mMatrix);
        canvas.drawText(charSequence, 0, charSequence.length(), f5, f6 - ((float) this.baseline), this.mPaint);
    }
}
