package com.wade.fit.model.bean;

public enum DetailTimeType {
    DAY,
    WEEK,
    ONE_MONTH,
    SIX_MONTH,
    YEAR
}
