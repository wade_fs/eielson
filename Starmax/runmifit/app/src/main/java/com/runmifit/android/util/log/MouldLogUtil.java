package com.wade.fit.util.log;

import android.util.Log;

public class MouldLogUtil {
    /* renamed from: d */
    public static void m6214d(String str, String str2) {
        Log.d(str, str2);
    }

    /* renamed from: p */
    public static void m6216p(String str, String str2, String str3) {
        Log.i(str2, str3);
        LogService.m6213p(str, str2, str3);
    }

    /* renamed from: e */
    public static void m6215e(String str, String str2, String str3) {
        Log.e(str2, str3);
        LogService.m6212e(str, str2, str3);
    }
}
