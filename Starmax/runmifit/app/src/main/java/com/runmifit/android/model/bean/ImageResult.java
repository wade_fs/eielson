package com.wade.fit.model.bean;

public class ImageResult {
    private String avatar;
    private String coverImage;
    private String mid;

    public String getMid() {
        return this.mid;
    }

    public void setMid(String str) {
        this.mid = str;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String str) {
        this.avatar = str;
    }

    public String getCoverImage() {
        return this.coverImage;
    }

    public void setCoverImage(String str) {
        this.coverImage = str;
    }
}
