package com.wade.fit.util.ble;

import com.google.common.base.Ascii;
import com.google.common.primitives.UnsignedBytes;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.gen.HealthActivityDao;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.greenrobot.greendao.query.WhereCondition;

public class ActivityDataHandler {
    private List<byte[]> activityDatas = new ArrayList();
    private List<byte[]> bytes = new ArrayList();
    int dataIndex;
    byte[] end = {-93, 2, 0, 3, 0, -54};
    byte[] endOne = {-93, 2, 0, 2, 0, 116};
    private int index;
    private boolean isItem = false;
    private List<byte[]> itemDatas = new ArrayList();
    private List<List<byte[]>> listArrayList = new ArrayList();
    private int receiveIndex;
    byte[] startFlag = {-93, Ascii.SUB, 0, 0};

    public Object handlerActivity(byte[] bArr) {
        HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
        byte b = bArr[0];
        byte[] bArr2 = this.end;
        if (b == bArr2[0] && bArr[1] == bArr2[1] && bArr[2] == bArr2[2] && bArr[3] == bArr2[3]) {
            LogUtil.m5266d("没有活动数据了..", Constants.SYCN_PATH);
            handlerBleDataResult.isComplete = true;
            handlerBleDataResult.hasNext = false;
            return handlerBleDataResult;
        }
        byte b2 = bArr[0];
        byte[] bArr3 = this.endOne;
        if (b2 == bArr3[0] && bArr[1] == bArr3[1] && bArr[2] == bArr3[2] && bArr[3] == bArr3[3] && bArr[4] == bArr3[4]) {
            LogUtil.m5266d("本次活动接收完毕", Constants.SYCN_PATH);
            handlerBleDataResult.isComplete = true;
            handlerBleDataResult.hasNext = true;
            handlerData();
        } else {
            byte b3 = bArr[0];
            byte[] bArr4 = this.startFlag;
            if (b3 == bArr4[0] && bArr[1] == bArr4[1] && bArr[2] == bArr4[2] && bArr[3] == bArr4[3]) {
                LogUtil.dAndSave(Arrays.toString(bArr), Constants.LOG_PATH);
                this.listArrayList.clear();
                this.bytes.clear();
                this.itemDatas.clear();
                this.index = 1;
                this.receiveIndex = 1;
            }
            this.dataIndex = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 1, 3)) / 20;
            handlerBleDataResult.isComplete = false;
            handlerBleDataResult.hasNext = true;
            this.itemDatas.add(bArr);
        }
        return handlerBleDataResult;
    }

    public HandlerBleDataResult receiverHistory(byte[] bArr, BaseDataHandler baseDataHandler) {
        HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
        baseDataHandler.flag = 3;
        this.itemDatas.add(bArr);
        this.receiveIndex++;
        LogUtil.d("接收" + this.receiveIndex + "条数据");
        return handlerBleDataResult;
    }

    public void handlerData() {
        byte[] bArr = new byte[(this.itemDatas.size() * 20)];
        for (int i = 0; i < this.itemDatas.size(); i++) {
            byte[] bArr2 = this.itemDatas.get(i);
            for (int i2 = 0; i2 < bArr2.length; i2++) {
                bArr[(i * 20) + i2] = bArr2[i2];
            }
        }
        byte b = bArr[3];
        byte b2 = bArr[4];
        int bytesToInt = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 5, 7));
        byte b3 = bArr[7];
        byte b4 = bArr[8];
        byte b5 = bArr[9];
        byte b6 = bArr[10];
        int bytesToInt2 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 11, 13));
        int bytesToInt3 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 13, 17));
        int Byte2Int = ByteDataConvertUtil.Byte2Int(bArr[17]);
        int bytesToInt4 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 18, 20));
        int bytesToInt5 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 20, 24));
        int bytesToInt6 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 24, 28));
        byte b7 = bArr[28];
        byte[] bArr3 = bArr;
        LogUtil.m5266d("*******************开始解析活动数据******************** ", Constants.SYCN_PATH);
        LogUtil.d("opType:" + ((int) b));
        LogUtil.d("sportType:" + ((int) b2));
        LogUtil.d("year:" + bytesToInt + ",month:" + ((int) b3) + ",day:" + ((int) b4) + ",hour:" + ((int) b5) + ",min:" + ((int) b6));
        StringBuilder sb = new StringBuilder();
        sb.append("duration:");
        sb.append(bytesToInt2);
        LogUtil.d(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("step:");
        sb2.append(bytesToInt3);
        LogUtil.d(sb2.toString());
        LogUtil.d("avgSpeed:" + bytesToInt4);
        LogUtil.d("avgHeartRate:" + Byte2Int);
        LogUtil.d("cal:" + bytesToInt5);
        LogUtil.d("dis:" + bytesToInt6);
        LogUtil.d("intevalTime:" + ((int) b7));
        HealthActivity healthActivity = new HealthActivity();
        healthActivity.setDurations(bytesToInt2);
        healthActivity.setStep(bytesToInt3);
        healthActivity.setCalories(bytesToInt5);
        healthActivity.setAvg_hr_value(Byte2Int);
        healthActivity.setHour(b5);
        healthActivity.setMinute(b6);
        healthActivity.setSecond(0);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(bytesToInt4 / 60);
        stringBuffer.append("'");
        stringBuffer.append(bytesToInt4 % 60);
        stringBuffer.append("\"");
        healthActivity.setAvgSpeed(stringBuffer.toString());
        Calendar instance = Calendar.getInstance();
        instance.set(1, bytesToInt);
        instance.set(2, b3 - 1);
        instance.set(5, b4);
        instance.set(11, b5);
        instance.set(12, b6);
        instance.set(13, 0);
        instance.set(14, 0);
        healthActivity.setDataFrom(2);
        healthActivity.setYear(bytesToInt);
        healthActivity.setMonth(b3);
        healthActivity.setDay(b4);
        healthActivity.setDate(instance.getTimeInMillis());
        healthActivity.setType(b2);
        healthActivity.setDistance(bytesToInt6);
        healthActivity.setAerobic_mins(b7);
        for (int i3 = 0; i3 < this.itemDatas.size(); i3++) {
        }
        int i4 = 44;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (true) {
            int i8 = i4 + 2;
            byte[] bArr4 = bArr3;
            if (i8 < bArr4.length) {
                byte b8 = bArr4[i4] & UnsignedBytes.MAX_VALUE;
                byte b9 = bArr4[i4 + 1] & UnsignedBytes.MAX_VALUE;
                i5 += b8;
                i6 += b9;
                LogUtil.m5266d("index:" + i7 + ",heart:" + ((int) b8) + ",step1:" + ((int) b9), Constants.SYCN_PATH);
                i7++;
                i4 = i8;
                bArr3 = bArr4;
            } else {
                LogUtil.d("sum:" + i5 + ",sum1:" + i6 + ",k:" + i7);
                StringBuilder sb3 = new StringBuilder();
                sb3.append(".........获取活动数据");
                sb3.append(healthActivity.toString());
                LogUtil.m5266d(sb3.toString(), Constants.SYCN_PATH);
                HealthActivityDao healthActivityDao = AppApplication.getInstance().getDaoSession().getHealthActivityDao();
                healthActivityDao.queryBuilder().where(HealthActivityDao.Properties.Date.eq(Long.valueOf(healthActivity.getDate())), new WhereCondition[0]).buildDelete().executeDeleteWithoutDetachingEntities();
                healthActivityDao.insertOrReplace(healthActivity);
                LogUtil.m5266d("*******************解析活动数据完毕******************** ", Constants.SYCN_PATH);
                return;
            }
        }
    }
}
