package com.wade.fit.model.net.api;

import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.UserBean;
import okhttp3.RequestBody;
import p041io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface LoginApi {
    @POST("user/clearUserData")
    Observable<BaseBean<String>> clearUserData();

    @POST("login/email")
    Observable<BaseBean<UserBean>> loginByEmail(@Body RequestBody requestBody);

    @POST("login/facebook")
    Observable<BaseBean<UserBean>> loginByFacebook(@Body RequestBody requestBody);

    @POST("login/weixin")
    Observable<BaseBean<UserBean>> loginByWechat(@Body RequestBody requestBody);

    @POST("user/clearUserAccount")
    Observable<BaseBean<String>> loginOut();

    @POST("register/email")
    Observable<BaseBean<UserBean>> registerAccount(@Body RequestBody requestBody);

    @POST("register/sendEmailVerifyCode")
    Observable<BaseBean> sendCode(@Query("email") String str);

    @FormUrlEncoded
    @POST("user/sendEmailVerifyCode")
    Observable<BaseBean<String>> sendEmailVerifyCode(@Field("email") String str);

    @POST("user/editPasswordByEmail")
    Observable<BaseBean<String>> updatePassword(@Body RequestBody requestBody);
}
