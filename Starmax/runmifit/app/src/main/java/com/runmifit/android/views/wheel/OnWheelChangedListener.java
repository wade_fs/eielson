package com.wade.fit.views.wheel;

public interface OnWheelChangedListener {
    void onChanged(WheelView wheelView, int i, int i2);
}
