package com.wade.fit.persenter.main;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.StepDetailVO;

public interface IDetailStep extends IBaseView {
    void getStepDetailVO(StepDetailVO stepDetailVO);
}
