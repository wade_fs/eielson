package com.wade.fit.greendao.bean;

public class HealthSport {
    private long date;
    private int day;
    private boolean isUploaded;
    private int itemCount;
    private String macAddress;
    private int month;
    private String remark;
    private int startTime;
    private int timeSpace;
    private int totalActiveTime;
    private int totalCalory;
    private int totalDayActiveTime;
    private int totalDayCalory;
    private int totalDayDistance;
    private int totalDayStepCount;
    private int totalDistance;
    private int totalStepCount;
    private String userId;
    private int year;

    public int getItemCount() {
        return this.itemCount;
    }

    public void setItemCount(int i) {
        this.itemCount = i;
    }

    public HealthSport() {
    }

    public HealthSport(boolean z, String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, long j, String str2, String str3, int i14) {
        this.isUploaded = z;
        this.macAddress = str;
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.totalStepCount = i4;
        this.totalCalory = i5;
        this.totalDistance = i6;
        this.totalActiveTime = i7;
        this.totalDayStepCount = i8;
        this.totalDayCalory = i9;
        this.totalDayDistance = i10;
        this.totalDayActiveTime = i11;
        this.startTime = i12;
        this.timeSpace = i13;
        this.date = j;
        this.userId = str2;
        this.remark = str3;
        this.itemCount = i14;
    }

    public int getTotalDayStepCount() {
        return this.totalDayStepCount;
    }

    public void setTotalDayStepCount(int i) {
        this.totalDayStepCount = i;
    }

    public int getTotalDayCalory() {
        return this.totalDayCalory;
    }

    public void setTotalDayCalory(int i) {
        this.totalDayCalory = i;
    }

    public int getTotalDayDistance() {
        return this.totalDayDistance;
    }

    public void setTotalDayDistance(int i) {
        this.totalDayDistance = i;
    }

    public int getTotalDayActiveTime() {
        return this.totalDayActiveTime;
    }

    public void setTotalDayActiveTime(int i) {
        this.totalDayActiveTime = i;
    }

    public boolean getIsUploaded() {
        return this.isUploaded;
    }

    public void setIsUploaded(boolean z) {
        this.isUploaded = z;
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getTotalStepCount() {
        return this.totalStepCount;
    }

    public void setTotalStepCount(int i) {
        this.totalStepCount = i;
    }

    public int getTotalCalory() {
        return this.totalCalory;
    }

    public void setTotalCalory(int i) {
        this.totalCalory = i;
    }

    public int getTotalDistance() {
        return this.totalDistance;
    }

    public void setTotalDistance(int i) {
        this.totalDistance = i;
    }

    public int getTotalActiveTime() {
        return this.totalActiveTime;
    }

    public void setTotalActiveTime(int i) {
        this.totalActiveTime = i;
    }

    public int getStartTime() {
        return this.startTime;
    }

    public void setStartTime(int i) {
        this.startTime = i;
    }

    public int getTimeSpace() {
        return this.timeSpace;
    }

    public void setTimeSpace(int i) {
        this.timeSpace = i;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String str) {
        this.remark = str;
    }

    public String toString() {
        return "HealthSport{, totalStepCount=" + this.totalStepCount + ", totalCalory=" + this.totalCalory + ", totalDistance=" + this.totalDistance + ", timeSpace=" + this.timeSpace + '}';
    }
}
