package com.wade.fit.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.views.CustomToggleButton;

public class ItemToggleLayout extends ItemLableValue {
    public ImageView ivLeft;
    private TextView lableView;
    private boolean oldStatus;
    private OnToggleListener onToggleListener;
    private ProgressBar progressBar;
    private CustomToggleButton toggleBtn;

    public interface OnToggleListener {
        void onToggle(ItemToggleLayout itemToggleLayout, boolean z);
    }

    public ItemToggleLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.wade.fit.views.ItemToggleLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void init(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(context).inflate((int) R.layout.layout_item_toggle, (ViewGroup) this, true);
        this.lableView = (TextView) findViewById(R.id.lable);
        this.toggleBtn = (CustomToggleButton) findViewById(R.id.toggle);
        this.progressBar = (ProgressBar) findViewById(R.id.progress_circle);
        this.ivLeft = (ImageView) findViewById(R.id.left_drawable);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ItemLableValue);
        String string = obtainStyledAttributes.getString(8);
        setHasBottomLine(obtainStyledAttributes.getBoolean(4, true));
        if (isHasBottomLine()) {
            this.bottomLineColor = obtainStyledAttributes.getColor(1, getResources().getColor(R.color.driver_color));
            initDraw();
        }
        Drawable drawable = obtainStyledAttributes.getDrawable(9);
        obtainStyledAttributes.recycle();
        this.ivLeft.setImageDrawable(drawable);
        this.lableView.setText(string);
        this.toggleBtn.setOnSwitchListener(new CustomToggleButton.OnSwitchListener() {
            /* class com.wade.fit.views.$$Lambda$ItemToggleLayout$WmTfq1odyMGvoqBYkEIaMv2aUsw */

            public final void onSwitched(boolean z) {
                ItemToggleLayout.this.lambda$init$0$ItemToggleLayout(z);
            }
        });
        this.progressBar.setVisibility(View.GONE);
    }

    public /* synthetic */ void lambda$init$0$ItemToggleLayout(boolean z) {
        OnToggleListener onToggleListener2 = this.onToggleListener;
        if (onToggleListener2 != null) {
            onToggleListener2.onToggle(this, z);
        }
    }

    public void setImageBack(int i) {
        this.toggleBtn.setImageResource(i, R.mipmap.toggle_off, R.mipmap.toggle_thumb);
    }

    public void showProgressBar() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    public void cancelProgressBar() {
        this.progressBar.setVisibility(View.GONE);
    }

    public boolean isOpen() {
        return this.toggleBtn.getSwitchState();
    }

    public void initStauts(boolean z) {
        this.oldStatus = z;
        this.toggleBtn.setSwitchState(z);
    }

    public boolean isChange() {
        return this.oldStatus != this.toggleBtn.getSwitchState();
    }

    public void setOpen(boolean z) {
        DebugLog.m6203d("setOpen:" + z);
        this.toggleBtn.setSwitchState(z);
    }

    public void setToggleButtonCallback(CustomToggleButton.Callback callback) {
        this.toggleBtn.setCallback(callback);
    }

    public void setOnToggleListener(OnToggleListener onToggleListener2) {
        this.onToggleListener = onToggleListener2;
    }

    public void setLableText(String str) {
        this.lableView.setText(str);
    }
}
