package com.wade.fit.util.baidumap;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.orhanobut.logger.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StepService extends Service implements SensorEventListener {
    private static String CURRENT_DATE = "";
    private static int duration = 30000;
    private static int stepSensorType = -1;
    private int CURRENT_STEP;
    private String TAG = "StepService";
    private boolean hasRecord = false;
    private int hasStepCount = 0;
    private BroadcastReceiver mBatInfoReceiver;
    private NotificationCompat.Builder mBuilder;
    private UpdateUiCallBack mCallback;
    private StepCount mStepCount;
    int notifyId_Step = 100;
    int notify_remind_id = 200;
    private int previousStepCount = 0;
    private SensorManager sensorManager;
    private StepBinder stepBinder = new StepBinder();
    /* access modifiers changed from: private */
    public TimeCount time;

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return 1;
    }

    public void onCreate() {
        super.onCreate();
        Log.d(this.TAG, "onCreate()");
        new Thread(new Runnable() {
            /* class com.wade.fit.util.baidumap.$$Lambda$StepService$xO7AIgQKxi2ByzcHJRvOfVZxig */

            public final void run() {
                StepService.this.lambda$onCreate$0$StepService();
            }
        }).start();
        startTimeCount();
    }

    private String getTodayDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis()));
    }

    /* access modifiers changed from: private */
    public void startTimeCount() {
        if (this.time == null) {
            this.time = new TimeCount((long) duration, 1000);
        }
        this.time.start();
    }

    private void updateNotification() {
        UpdateUiCallBack updateUiCallBack = this.mCallback;
        if (updateUiCallBack != null) {
            updateUiCallBack.updateUi(this.CURRENT_STEP);
        }
        Log.d(this.TAG, "updateNotification()");
    }

    public void registerCallback(UpdateUiCallBack updateUiCallBack) {
        this.mCallback = updateUiCallBack;
    }

    public PendingIntent getDefalutIntent(int i) {
        return PendingIntent.getActivity(this, 1, new Intent(), i);
    }

    public IBinder onBind(Intent intent) {
        return this.stepBinder;
    }

    public class StepBinder extends Binder {
        public StepBinder() {
        }

        public StepService getService() {
            return StepService.this;
        }
    }

    public int getStepCount() {
        return this.CURRENT_STEP;
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: startStepDetector */
    public void lambda$onCreate$0$StepService() {
        if (this.sensorManager != null) {
            this.sensorManager = null;
        }
        this.sensorManager = (SensorManager) getSystemService("sensor");
        if (Build.VERSION.SDK_INT >= 19) {
            addCountStepListener();
        } else {
            addBasePedometerListener();
        }
    }

    private void addCountStepListener() {
        Sensor defaultSensor = this.sensorManager.getDefaultSensor(19);
        Sensor defaultSensor2 = this.sensorManager.getDefaultSensor(18);
        if (defaultSensor != null) {
            stepSensorType = 19;
            Log.v(this.TAG, "Sensor.TYPE_STEP_COUNTER");
            this.sensorManager.registerListener(this, defaultSensor, 3);
        } else if (defaultSensor2 != null) {
            stepSensorType = 18;
            Log.v(this.TAG, "Sensor.TYPE_STEP_DETECTOR");
            this.sensorManager.registerListener(this, defaultSensor2, 3);
        } else {
            Log.v(this.TAG, "Count sensor not available!");
            addBasePedometerListener();
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        int i = stepSensorType;
        if (i == 19) {
            int i2 = (int) sensorEvent.values[0];
            if (!this.hasRecord) {
                this.hasRecord = true;
                this.hasStepCount = i2;
            } else {
                int i3 = i2 - this.hasStepCount;
                this.CURRENT_STEP += i3 - this.previousStepCount;
                this.previousStepCount = i3;
            }
            Logger.m6177d("tempStep" + i2);
        } else if (i == 18 && ((double) sensorEvent.values[0]) == 1.0d) {
            this.CURRENT_STEP++;
        }
        updateNotification();
    }

    private void addBasePedometerListener() {
        this.mStepCount = new StepCount();
        this.mStepCount.setSteps(this.CURRENT_STEP);
        boolean registerListener = this.sensorManager.registerListener(this.mStepCount.getStepDetector(), this.sensorManager.getDefaultSensor(1), 2);
        this.mStepCount.initListener(new StepValuePassListener() {
            /* class com.wade.fit.util.baidumap.$$Lambda$StepService$EjPdPkv7cxyZJcF6clwE9L7ijs */

            public final void stepChanged(int i) {
                StepService.this.lambda$addBasePedometerListener$1$StepService(i);
            }
        });
        if (registerListener) {
            Log.v(this.TAG, "加速度传感器可以使用");
        } else {
            Log.v(this.TAG, "加速度传感器无法使用");
        }
    }

    public /* synthetic */ void lambda$addBasePedometerListener$1$StepService(int i) {
        this.CURRENT_STEP = i;
        updateNotification();
    }

    class TimeCount extends CountDownTimer {
        public void onTick(long j) {
        }

        public TimeCount(long j, long j2) {
            super(j, j2);
        }

        public void onFinish() {
            StepService.this.time.cancel();
            StepService.this.startTimeCount();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
        Logger.m6177d("stepService关闭");
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
