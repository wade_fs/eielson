package com.wade.fit.ui.main.activity;

import android.view.View;
import android.widget.FrameLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.NotifyRadioButton;

/* renamed from: com.wade.fit.ui.main.activity.MainActivity_ViewBinding */
public class MainActivity_ViewBinding implements Unbinder {
    private MainActivity target;
    private View view2131296773;
    private View view2131296774;
    private View view2131296775;
    private View view2131296776;

    public MainActivity_ViewBinding(MainActivity mainActivity) {
        this(mainActivity, mainActivity.getWindow().getDecorView());
    }

    public MainActivity_ViewBinding(final MainActivity mainActivity, View view) {
        this.target = mainActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.rb_tab_mainpage, "field 'rbTabMainpage' and method 'onClick'");
        mainActivity.rbTabMainpage = (NotifyRadioButton) Utils.castView(findRequiredView, R.id.rb_tab_mainpage, "field 'rbTabMainpage'", NotifyRadioButton.class);
        this.view2131296775 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.MainActivity_ViewBinding.C25321 */

            public void doClick(View view) {
                mainActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.rb_tab_ecg, "field 'rbTabEcg' and method 'onClick'");
        mainActivity.rbTabEcg = (NotifyRadioButton) Utils.castView(findRequiredView2, R.id.rb_tab_ecg, "field 'rbTabEcg'", NotifyRadioButton.class);
        this.view2131296774 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.MainActivity_ViewBinding.C25332 */

            public void doClick(View view) {
                mainActivity.onClick(view);
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.rb_tab_device, "field 'rbTabDevice' and method 'onClick'");
        mainActivity.rbTabDevice = (NotifyRadioButton) Utils.castView(findRequiredView3, R.id.rb_tab_device, "field 'rbTabDevice'", NotifyRadioButton.class);
        this.view2131296773 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.MainActivity_ViewBinding.C25343 */

            public void doClick(View view) {
                mainActivity.onClick(view);
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.rb_tab_user, "field 'rbTabUser' and method 'onClick'");
        mainActivity.rbTabUser = (NotifyRadioButton) Utils.castView(findRequiredView4, R.id.rb_tab_user, "field 'rbTabUser'", NotifyRadioButton.class);
        this.view2131296776 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.MainActivity_ViewBinding.C25354 */

            public void doClick(View view) {
                mainActivity.onClick(view);
            }
        });
        mainActivity.mActivityFrame = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.activity_frame, "field 'mActivityFrame'", FrameLayout.class);
    }

    public void unbind() {
        MainActivity mainActivity = this.target;
        if (mainActivity != null) {
            this.target = null;
            mainActivity.rbTabMainpage = null;
            mainActivity.rbTabEcg = null;
            mainActivity.rbTabDevice = null;
            mainActivity.rbTabUser = null;
            mainActivity.mActivityFrame = null;
            this.view2131296775.setOnClickListener(null);
            this.view2131296775 = null;
            this.view2131296774.setOnClickListener(null);
            this.view2131296774 = null;
            this.view2131296773.setOnClickListener(null);
            this.view2131296773 = null;
            this.view2131296776.setOnClickListener(null);
            this.view2131296776 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
