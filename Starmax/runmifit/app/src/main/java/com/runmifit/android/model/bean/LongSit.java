package com.wade.fit.model.bean;

import java.io.Serializable;
import java.util.Arrays;

public class LongSit implements Serializable {
    private static final long serialVersionUID = 1;
    private int endHour;
    private int endMinute;
    private int interval;
    private boolean onOff;
    private int repetitions;
    private int startHour;
    private int startMinute;
    private boolean[] weeks = new boolean[7];

    public int getStartHour() {
        return this.startHour;
    }

    public void setStartHour(int i) {
        this.startHour = i;
    }

    public int getStartMinute() {
        return this.startMinute;
    }

    public void setStartMinute(int i) {
        this.startMinute = i;
    }

    public int getEndHour() {
        return this.endHour;
    }

    public void setEndHour(int i) {
        this.endHour = i;
    }

    public int getEndMinute() {
        return this.endMinute;
    }

    public void setEndMinute(int i) {
        this.endMinute = i;
    }

    public int getInterval() {
        return this.interval;
    }

    public void setInterval(int i) {
        this.interval = i;
    }

    public boolean isOnOff() {
        return this.onOff;
    }

    public void setOnOff(boolean z) {
        this.repetitions = toByte(this.weeks, z);
        this.onOff = z;
    }

    public boolean[] getWeeks() {
        return this.weeks;
    }

    public void setWeeks(boolean[] zArr) {
        this.weeks = zArr;
        this.repetitions = toByte(zArr, this.onOff);
    }

    private int toByte(boolean[] zArr, boolean z) {
        int i = 0;
        for (int i2 = 0; i2 < 7; i2++) {
            if (zArr[i2]) {
                i |= 1 << (i2 + 1);
            }
        }
        return z ? i | 1 : i;
    }

    public String toString() {
        return "LongSit{startHour=" + this.startHour + ", startMinute=" + this.startMinute + ", endHour=" + this.endHour + ", endMinute=" + this.endMinute + ", interval=" + this.interval + ", repetitions=" + this.repetitions + ", onOff=" + this.onOff + ", weeks=" + Arrays.toString(this.weeks) + '}';
    }
}
