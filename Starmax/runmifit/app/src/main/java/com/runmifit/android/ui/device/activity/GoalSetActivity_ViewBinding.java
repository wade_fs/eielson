package com.wade.fit.ui.device.activity;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemLableValue;

/* renamed from: com.wade.fit.ui.device.activity.GoalSetActivity_ViewBinding */
public class GoalSetActivity_ViewBinding implements Unbinder {
    private GoalSetActivity target;
    private View view2131296495;
    private View view2131296497;
    private View view2131296511;

    public GoalSetActivity_ViewBinding(GoalSetActivity goalSetActivity) {
        this(goalSetActivity, goalSetActivity.getWindow().getDecorView());
    }

    public GoalSetActivity_ViewBinding(final GoalSetActivity goalSetActivity, View view) {
        this.target = goalSetActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.ilStep, "field 'ilStep' and method 'onClick'");
        goalSetActivity.ilStep = (ItemLableValue) Utils.castView(findRequiredView, R.id.ilStep, "field 'ilStep'", ItemLableValue.class);
        this.view2131296511 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.GoalSetActivity_ViewBinding.C24621 */

            public void doClick(View view) {
                goalSetActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ilDistance, "field 'ilDistance' and method 'onClick'");
        goalSetActivity.ilDistance = (ItemLableValue) Utils.castView(findRequiredView2, R.id.ilDistance, "field 'ilDistance'", ItemLableValue.class);
        this.view2131296497 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.GoalSetActivity_ViewBinding.C24632 */

            public void doClick(View view) {
                goalSetActivity.onClick(view);
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.ilCal, "field 'ilCal' and method 'onClick'");
        goalSetActivity.ilCal = (ItemLableValue) Utils.castView(findRequiredView3, R.id.ilCal, "field 'ilCal'", ItemLableValue.class);
        this.view2131296495 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.GoalSetActivity_ViewBinding.C24643 */

            public void doClick(View view) {
                goalSetActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        GoalSetActivity goalSetActivity = this.target;
        if (goalSetActivity != null) {
            this.target = null;
            goalSetActivity.ilStep = null;
            goalSetActivity.ilDistance = null;
            goalSetActivity.ilCal = null;
            this.view2131296511.setOnClickListener(null);
            this.view2131296511 = null;
            this.view2131296497.setOnClickListener(null);
            this.view2131296497 = null;
            this.view2131296495.setOnClickListener(null);
            this.view2131296495 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
