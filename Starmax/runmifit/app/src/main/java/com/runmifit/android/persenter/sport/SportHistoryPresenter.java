package com.wade.fit.persenter.sport;

import com.wade.fit.app.AppApplication;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.greendao.gen.HealthActivityDao;
import com.wade.fit.greendao.gen.HealthHeartRateDao;
import com.wade.fit.greendao.gen.HealthHeartRateItemDao;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.model.bean.HealthMonthActivity;
import com.wade.fit.model.bean.HeartRateDetailVO;
import com.wade.fit.model.bean.SportHistoryVo;
import com.wade.fit.util.StringUtils;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.greenrobot.greendao.query.WhereCondition;

public class SportHistoryPresenter extends BaseTimePresenter<ISportHistoryView> {
    List<HealthActivity> activityList = new ArrayList();

    public void getByType() {
        ArrayList arrayList;
        int year;
        int month;
        SportHistoryVo sportHistoryVo = new SportHistoryVo();
        this.activityList = AppApplication.getInstance().getDaoSession().getHealthActivityDao().queryBuilder().orderDesc(HealthActivityDao.Properties.Date).build().list();
        if (this.activityList.isEmpty()) {
            sportHistoryVo.monthDatas = null;
            ((ISportHistoryView) this.mView).getByType(sportHistoryVo);
            return;
        }
        for (int i = 0; i < this.activityList.size(); i++) {
            for (int size = this.activityList.size() - 1; size > i; size--) {
                if (this.activityList.get(i) == this.activityList.get(size)) {
                    this.activityList.remove(size);
                }
            }
        }
        sportHistoryVo.allDatas = this.activityList;
        ArrayList arrayList2 = new ArrayList();
        if (this.activityList.size() == 1) {
            ArrayList arrayList3 = new ArrayList();
            int year2 = this.activityList.get(0).getYear();
            int month2 = this.activityList.get(0).getMonth();
            HealthMonthActivity healthMonthActivity = new HealthMonthActivity();
            healthMonthActivity.setStMonth(year2 + "-" + month2);
            arrayList3.add(this.activityList.get(0));
            healthMonthActivity.setHealthActivities(arrayList3);
            arrayList2.add(healthMonthActivity);
        } else {
            ArrayList arrayList4 = null;
            HealthMonthActivity healthMonthActivity2 = null;
            int i2 = 0;
            int i3 = 0;
            for (int i4 = 0; i4 < this.activityList.size(); i4++) {
                if (i4 == 0) {
                    arrayList = new ArrayList();
                    year = this.activityList.get(i4).getYear();
                    month = this.activityList.get(i4).getMonth();
                    healthMonthActivity2 = new HealthMonthActivity();
                    healthMonthActivity2.setStMonth(year + "-" + month);
                    arrayList.add(this.activityList.get(i4));
                } else if (i4 == this.activityList.size() - 1) {
                    if (this.activityList.get(i4).getYear() == i2 && this.activityList.get(i4).getMonth() == i3) {
                        arrayList4.add(this.activityList.get(i4));
                        healthMonthActivity2.setHealthActivities(arrayList4);
                        arrayList2.add(healthMonthActivity2);
                    } else {
                        healthMonthActivity2.setHealthActivities(arrayList4);
                        arrayList2.add(healthMonthActivity2);
                        arrayList = new ArrayList();
                        year = this.activityList.get(i4).getYear();
                        month = this.activityList.get(i4).getMonth();
                        healthMonthActivity2 = new HealthMonthActivity();
                        healthMonthActivity2.setStMonth(year + "-" + month);
                        arrayList.add(this.activityList.get(i4));
                        healthMonthActivity2.setHealthActivities(arrayList);
                        arrayList2.add(healthMonthActivity2);
                    }
                } else if (this.activityList.get(i4).getYear() == i2 && this.activityList.get(i4).getMonth() == i3) {
                    arrayList4.add(this.activityList.get(i4));
                } else {
                    healthMonthActivity2.setHealthActivities(arrayList4);
                    arrayList2.add(healthMonthActivity2);
                    arrayList = new ArrayList();
                    year = this.activityList.get(i4).getYear();
                    month = this.activityList.get(i4).getMonth();
                    healthMonthActivity2 = new HealthMonthActivity();
                    healthMonthActivity2.setStMonth(year + "-" + month);
                    arrayList.add(this.activityList.get(i4));
                }
                int i5 = month;
                arrayList4 = arrayList;
                i2 = year;
                i3 = i5;
            }
        }
        sportHistoryVo.monthDatas = arrayList2;
        ((ISportHistoryView) this.mView).getByType(sportHistoryVo);
    }

    public SportHistoryVo getDetail() {
        HealthActivityDao healthActivityDao = AppApplication.getInstance().getDaoSession().getHealthActivityDao();
        LogUtil.d("index:" + this.index + ",date:" + this.dateFormat.format(this.currentDate) + "detailTimeType:" + this.timeType.toString());
        SportHistoryVo sportHistoryVo = new SportHistoryVo();
        int i = C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
        if (i == 1) {
            List<HealthActivity> list = healthActivityDao.queryBuilder().where(HealthActivityDao.Properties.Year.eq(Integer.valueOf(this.year)), HealthActivityDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthActivityDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthActivityDao.Properties.Date).build().list();
            ArrayList arrayList = new ArrayList(25);
            for (int i2 = 0; i2 <= 24; i2++) {
                HealthActivity healthActivity = new HealthActivity();
                healthActivity.setRemark("");
                for (HealthActivity healthActivity2 : list) {
                    if (healthActivity2.getHour() == i2) {
                        healthActivity.setCalories(healthActivity2.getCalories() + healthActivity.getCalories());
                        healthActivity.setDistance(healthActivity2.getDistance() + healthActivity.getDistance());
                        healthActivity.setStep(healthActivity2.getStep() + healthActivity.getStep());
                        healthActivity.setDurations(healthActivity2.getDurations() + healthActivity.getDurations());
                        healthActivity.setYear(this.year);
                        healthActivity.setMonth(this.month);
                        healthActivity.setDay(this.day);
                        healthActivity.setHour(i2);
                    }
                }
                healthActivity.setDistance(healthActivity.getDistance());
                healthActivity.setDurations(healthActivity.getDurations() / 60);
                arrayList.add(healthActivity);
            }
            sportHistoryVo.datas = arrayList;
            sportHistoryVo.dates.add("00:00");
            sportHistoryVo.dates.add("06:00");
            sportHistoryVo.dates.add("12:00");
            sportHistoryVo.dates.add("18:00");
            sportHistoryVo.dates.add("23:59");
        } else if (i == 2 || i == 3 || i == 4) {
            return get();
        }
        new HealthActivity();
        for (int i3 = 0; i3 < 24; i3++) {
            for (int i4 = 0; i4 < sportHistoryVo.datas.size(); i4++) {
                HealthHeartRateItem healthHeartRateItem = new HealthHeartRateItem();
                healthHeartRateItem.setRemark(sportHistoryVo.datas.get(i4).getRemark());
                int heartRaveValue = healthHeartRateItem.getHeartRaveValue();
            }
        }
        LogUtil.d(Arrays.toString(sportHistoryVo.dates.toArray()));
        return sportHistoryVo;
    }

    /* renamed from: com.wade.fit.persenter.sport.SportHistoryPresenter$1 */
    static /* synthetic */ class C24311 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailTimeType = new int[DetailTimeType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.wade.fit.model.bean.DetailTimeType[] r0 = com.wade.fit.model.bean.DetailTimeType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.persenter.sport.SportHistoryPresenter.C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType = r0
                int[] r0 = com.wade.fit.persenter.sport.SportHistoryPresenter.C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.DAY     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.persenter.sport.SportHistoryPresenter.C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.ONE_MONTH     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.persenter.sport.SportHistoryPresenter.C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.SIX_MONTH     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.wade.fit.persenter.sport.SportHistoryPresenter.C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.YEAR     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.persenter.sport.SportHistoryPresenter.C24311.<clinit>():void");
        }
    }

    public SportHistoryVo getDetailCurrent(DetailTimeType detailTimeType, Date date) {
        currentDate(detailTimeType, date);
        return getDetail();
    }

    public SportHistoryVo getDetailCurrent(DetailTimeType detailTimeType) {
        currentDate(detailTimeType);
        return getDetail();
    }

    public SportHistoryVo getDetailNext(DetailTimeType detailTimeType) {
        nextDate(detailTimeType);
        return getDetail();
    }

    public SportHistoryVo getDetailPre(DetailTimeType detailTimeType) {
        preDate(detailTimeType);
        return getDetail();
    }

    public SportHistoryVo getSixMonth() {
        setSixMonth();
        return get();
    }

    public int getTotalDurationByWeek() {
        setOneWeek();
        return getDurationByWeek();
    }

    public SportHistoryVo getOneMonth() {
        setOneMonth();
        return get();
    }

    public SportHistoryVo getYearMonth() {
        setYear();
        return get();
    }

    private void completeAvgFormList(List<HealthActivity> list, HealthActivity healthActivity) {
        for (HealthActivity healthActivity2 : list) {
            healthActivity.setCalories(healthActivity2.getCalories() + healthActivity.getCalories());
            healthActivity.setDistance(healthActivity2.getDistance() + healthActivity.getDistance());
            healthActivity.setStep(healthActivity2.getStep() + healthActivity.getStep());
            healthActivity.setDurations(healthActivity2.getDurations() + healthActivity.getDurations());
        }
    }

    private void completAvg(HealthActivity healthActivity, HealthActivity healthActivity2, int i) {
        if (i >= 1) {
            healthActivity.setCalories(healthActivity2.getCalories() / i);
            healthActivity.setDistance(healthActivity2.getDistance() / i);
            healthActivity.setStep(healthActivity2.getStep() / i);
            healthActivity.setDurations(healthActivity2.getDurations() / i);
        }
    }

    private int getDurationByWeek() {
        int i = 0;
        for (HealthActivity healthActivity : AppApplication.getInstance().getDaoSession().getHealthActivityDao().queryBuilder().where(HealthActivityDao.Properties.Date.between(Long.valueOf(this.endDate.getTime()), Long.valueOf(this.startDate.getTime())), new WhereCondition[0]).orderDesc(HealthActivityDao.Properties.Date).build().list()) {
            i += healthActivity.getDurations();
        }
        return i;
    }

    private SportHistoryVo get() {
        int i = 1;
        List<HealthActivity> list = AppApplication.getInstance().getDaoSession().getHealthActivityDao().queryBuilder().where(HealthActivityDao.Properties.Date.between(Long.valueOf(this.endDate.getTime()), Long.valueOf(this.startDate.getTime())), new WhereCondition[0]).orderDesc(HealthActivityDao.Properties.Date).build().list();
        LogUtil.d("size:" + list.size());
        Calendar instance = Calendar.getInstance();
        instance.setTime(this.endDate);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        LogUtil.m5264d(this.timeType);
        HealthActivity healthActivity = new HealthActivity();
        HealthActivity healthActivity2 = new HealthActivity();
        int i2 = 1;
        int i3 = 0;
        while (instance.getTime().getTime() <= this.startDate.getTime()) {
            ArrayList arrayList3 = new ArrayList();
            HealthActivity healthActivity3 = new HealthActivity();
            int i4 = instance.get(i);
            int i5 = instance.get(2) + 1;
            int i6 = instance.get(5);
            for (HealthActivity healthActivity4 : list) {
                List list2 = list;
                if (healthActivity4.getYear() == i4 && healthActivity4.getMonth() == i5 && healthActivity4.getDay() == i6) {
                    arrayList3.add(healthActivity4);
                }
                list = list2;
            }
            List list3 = list;
            completeAvgFormList(arrayList3, healthActivity3);
            int i7 = C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
            if (i7 == 2) {
                if (i2 % 6 == 0) {
                    arrayList2.add(this.dateFormat2.format(instance.getTime()));
                }
                healthActivity3.setRemark(this.dateFormat3.format(instance.getTime()));
                healthActivity3.setDistance(healthActivity3.getDistance());
                healthActivity3.setDurations(healthActivity3.getDurations() / 60);
                arrayList.add(healthActivity3);
                i2++;
            } else if (i7 == 3) {
                int durations = healthActivity3.getDurations();
                if (i2 % 30 == 0) {
                    arrayList2.add(this.dateFormat2.format(instance.getTime()));
                }
                healthActivity2.setCalories(healthActivity2.getCalories() + healthActivity3.getCalories());
                healthActivity2.setDistance(healthActivity2.getDistance() + healthActivity3.getDistance());
                healthActivity2.setStep(healthActivity2.getStep() + healthActivity3.getStep());
                healthActivity2.setDurations(healthActivity2.getDurations() + healthActivity3.getDurations());
                Date time = instance.getTime();
                if (i2 % 7 == 0 || time.getTime() == this.startDate.getTime()) {
                    HealthActivity healthActivity5 = new HealthActivity();
                    completAvg(healthActivity5, healthActivity2, 1);
                    Calendar calendar = (Calendar) instance.clone();
                    calendar.add(5, -6);
                    healthActivity5.setRemark(this.dateFormat3.format(calendar.getTime()) + "-" + this.dateFormat3.format(instance.getTime()));
                    healthActivity5.setDistance(healthActivity5.getDistance());
                    healthActivity5.setDurations(healthActivity5.getDurations() / 60);
                    arrayList.add(healthActivity5);
                    healthActivity2.setCalories(0);
                    healthActivity2.setDistance(0);
                    healthActivity2.setStep(0);
                    healthActivity2.setDurations(0);
                }
                i2++;
            } else if (i7 == 4) {
                int durations2 = healthActivity3.getDurations();
                healthActivity2.setCalories(healthActivity2.getCalories() + healthActivity3.getCalories());
                healthActivity2.setDistance(healthActivity2.getDistance() + healthActivity3.getDistance());
                healthActivity2.setStep(healthActivity2.getStep() + healthActivity3.getStep());
                healthActivity2.setDurations(healthActivity2.getDurations() + healthActivity3.getDurations());
                Calendar instance2 = Calendar.getInstance();
                instance2.setTime(this.startDate);
                instance2.set(11, 0);
                instance2.set(12, 0);
                instance2.set(13, 0);
                instance2.set(14, 0);
                if (i3 != i5 || instance.getTime().getTime() == instance2.getTime().getTime()) {
                    if (i3 != 0) {
                        LogUtil.d(healthActivity.toString());
                        arrayList2.add(StringUtils.format("%02d", Integer.valueOf(i3)));
                        HealthActivity healthActivity6 = new HealthActivity();
                        completAvg(healthActivity6, healthActivity2, 1);
                        healthActivity6.setRemark(this.dateFormat4.format(instance.getTime()));
                        healthActivity6.setDistance(healthActivity6.getDistance());
                        healthActivity6.setDurations(healthActivity6.getDurations() / 60);
                        arrayList.add(healthActivity6);
                        healthActivity2.setCalories(0);
                        healthActivity2.setDistance(0);
                        healthActivity2.setStep(0);
                        healthActivity2.setDurations(0);
                        i2++;
                    }
                    i3 = i5;
                }
            }
            instance.add(5, 1);
            list = list3;
            i = 1;
        }
        LogUtil.d("size:" + arrayList.size());
        SportHistoryVo sportHistoryVo = new SportHistoryVo();
        sportHistoryVo.datas = arrayList;
        sportHistoryVo.dates = arrayList2;
        return sportHistoryVo;
    }

    public HeartRateDetailVO getHeartDetailCurrent(DetailTimeType detailTimeType, Date date) {
        currentDate(detailTimeType, date);
        return getHeartDetail();
    }

    public HeartRateDetailVO getHeartDetailCurrent(DetailTimeType detailTimeType) {
        currentDate(detailTimeType);
        return getHeartDetail();
    }

    public HeartRateDetailVO getHeartDetailNext(DetailTimeType detailTimeType) {
        nextDate(detailTimeType);
        return getHeartDetail();
    }

    public HeartRateDetailVO getHeartDetailPre(DetailTimeType detailTimeType) {
        preDate(detailTimeType);
        return getHeartDetail();
    }

    public HeartRateDetailVO getHeartSixMonth() {
        setSixMonth();
        return getHeart();
    }

    public HeartRateDetailVO getHeartYearMonth() {
        setYear();
        return getHeart();
    }

    private HeartRateDetailVO getHeart() {
        int i = 1;
        List<HealthHeartRate> list = AppApplication.getInstance().getDaoSession().getHealthHeartRateDao().queryBuilder().where(HealthHeartRateDao.Properties.Date.between(Long.valueOf(this.endDate.getTime()), Long.valueOf(this.startDate.getTime())), new WhereCondition[0]).orderDesc(HealthHeartRateDao.Properties.Date).build().list();
        Calendar instance = Calendar.getInstance();
        instance.setTime(this.endDate);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        LogUtil.m5264d(this.timeType);
        HealthHeartRate healthHeartRate = new HealthHeartRate();
        int i2 = 1;
        int i3 = 0;
        int i4 = 0;
        while (instance.getTime().getTime() <= this.startDate.getTime()) {
            HealthHeartRate healthHeartRate2 = null;
            int i5 = 0;
            for (HealthHeartRate healthHeartRate3 : list) {
                int i6 = instance.get(i);
                int i7 = instance.get(2) + i;
                int i8 = instance.get(5);
                if (healthHeartRate3.getYear() == i6 && healthHeartRate3.getMonth() == i7 && healthHeartRate3.getDay() == i8) {
                    healthHeartRate2 = healthHeartRate3;
                }
                i5 = i7;
                i = 1;
            }
            if (healthHeartRate2 == null) {
                healthHeartRate2 = new HealthHeartRate();
            }
            int i9 = C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
            if (i9 == 2) {
                if (i2 % 6 == 0) {
                    arrayList2.add(this.dateFormat2.format(instance.getTime()));
                }
                healthHeartRate2.setRemark(this.dateFormat3.format(instance.getTime()));
                arrayList.add(healthHeartRate2);
                i2++;
            } else if (i9 == 3) {
                if (healthHeartRate2.getSilentHeart() > 0) {
                    i3++;
                }
                healthHeartRate.setSilentHeart(healthHeartRate.getSilentHeart() + healthHeartRate2.getSilentHeart());
                if (i2 % 30 == 0) {
                    arrayList2.add(this.dateFormat2.format(instance.getTime()));
                }
                if (i2 % 7 == 0 || instance.getTime().getTime() == this.startDate.getTime()) {
                    HealthHeartRate healthHeartRate4 = new HealthHeartRate();
                    if (i3 != 0) {
                        healthHeartRate4.setSilentHeart(healthHeartRate.getSilentHeart() / i3);
                    }
                    Calendar calendar = (Calendar) instance.clone();
                    calendar.add(5, -7);
                    healthHeartRate4.setRemark(this.dateFormat3.format(calendar.getTime()) + "-" + this.dateFormat3.format(instance.getTime()));
                    arrayList.add(healthHeartRate4);
                    healthHeartRate.setSilentHeart(0);
                    i3 = 0;
                    i2++;
                } else {
                    i2++;
                }
            } else if (i9 == 4) {
                if (healthHeartRate2.getSilentHeart() > 0) {
                    i3++;
                }
                healthHeartRate.setSilentHeart(healthHeartRate.getSilentHeart() + healthHeartRate2.getSilentHeart());
                if (i4 != i5 || instance.getTime().getTime() == this.startDate.getTime()) {
                    if (i4 != 0) {
                        LogUtil.d(healthHeartRate.toString());
                        if (i2 <= 12) {
                            arrayList2.add(String.valueOf(i2));
                        }
                        HealthHeartRate healthHeartRate5 = new HealthHeartRate();
                        healthHeartRate5.setRemark(this.dateFormat4.format(instance.getTime()));
                        if (i3 != 0) {
                            healthHeartRate5.setSilentHeart(healthHeartRate.getSilentHeart() / i3);
                        }
                        arrayList.add(healthHeartRate5);
                        healthHeartRate.setSilentHeart(0);
                        i2++;
                        i3 = 0;
                    }
                    i4 = i5;
                }
            }
            instance.add(5, 1);
            i = 1;
        }
        LogUtil.d("size:" + arrayList.size());
        LogUtil.d(Arrays.toString(arrayList2.toArray()));
        HeartRateDetailVO heartRateDetailVO = new HeartRateDetailVO();
        heartRateDetailVO.healthSportList = arrayList;
        heartRateDetailVO.dates = arrayList2;
        heartRateDetailVO.mainVO = getByDate();
        ArrayList arrayList3 = new ArrayList();
        HealthHeartRate healthHeartRate6 = new HealthHeartRate();
        int i10 = 0;
        int i11 = 0;
        for (int i12 = 0; i12 < arrayList.size(); i12++) {
            HealthHeartRate healthHeartRate7 = (HealthHeartRate) arrayList.get(i12);
            HealthHeartRateItem healthHeartRateItem = new HealthHeartRateItem();
            healthHeartRateItem.setOffsetMinute(1440 / arrayList.size());
            healthHeartRateItem.setHeartRaveValue(healthHeartRate7.getSilentHeart());
            healthHeartRateItem.setRemark(healthHeartRate7.getRemark());
            arrayList3.add(healthHeartRateItem);
            i11 += healthHeartRate7.getSilentHeart();
            if (i12 == 0) {
                healthHeartRate6.setSilentHeart(healthHeartRateItem.getHeartRaveValue());
            }
            if (healthHeartRateItem.getHeartRaveValue() > 0) {
                i10++;
            }
            heartRateDetailVO.maxValue = Math.max(heartRateDetailVO.maxValue, healthHeartRateItem.getHeartRaveValue());
            heartRateDetailVO.minValue = Math.min(heartRateDetailVO.minValue, healthHeartRateItem.getHeartRaveValue());
        }
        if (i10 > 0) {
            heartRateDetailVO.avgValue = i11 / i10;
        }
        heartRateDetailVO.mainVO.healthHeartRate = healthHeartRate6;
        heartRateDetailVO.items = arrayList3;
        return heartRateDetailVO;
    }

    public HeartRateDetailVO getHeartOneMonth() {
        setOneMonth();
        return getHeart();
    }

    public HeartRateDetailVO getHeartDetail() {
        LogUtil.d("index:" + this.index + ",date:" + this.dateFormat.format(this.currentDate) + "detailTimeType:" + this.timeType.toString());
        HeartRateDetailVO heartRateDetailVO = new HeartRateDetailVO();
        ArrayList arrayList = new ArrayList();
        heartRateDetailVO.healthSportList = arrayList;
        int i = C24311.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.timeType.ordinal()];
        if (i == 1) {
            List list = AppApplication.getInstance().getDaoSession().getHealthHeartRateItemDao().queryBuilder().where(HealthHeartRateItemDao.Properties.Year.eq(Integer.valueOf(this.year)), HealthHeartRateItemDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthHeartRateItemDao.Properties.Day.eq(Integer.valueOf(this.day))).orderDesc(HealthHeartRateItemDao.Properties.Date).build().list();
            if (list != null) {
                heartRateDetailVO.items.addAll(list);
            }
            heartRateDetailVO.dates.add("00:00");
            heartRateDetailVO.dates.add("06:00");
            heartRateDetailVO.dates.add("12:00");
            heartRateDetailVO.dates.add("18:00");
            heartRateDetailVO.dates.add("23:59");
            HealthHeartRate healthHeartRate = (HealthHeartRate) AppApplication.getInstance().getDaoSession().getHealthHeartRateDao().queryBuilder().where(HealthHeartRateDao.Properties.Year.eq(Integer.valueOf(this.year)), HealthHeartRateDao.Properties.Month.eq(Integer.valueOf(this.month)), HealthHeartRateDao.Properties.Day.eq(Integer.valueOf(this.day))).build().unique();
            if (healthHeartRate != null) {
                arrayList.add(healthHeartRate);
            }
        } else if (i == 2 || i == 3 || i == 4) {
            return getHeart();
        }
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < heartRateDetailVO.items.size(); i4++) {
            HealthHeartRateItem healthHeartRateItem = heartRateDetailVO.items.get(i4);
            HealthHeartRateItem healthHeartRateItem2 = new HealthHeartRateItem();
            healthHeartRateItem2.setHeartRaveValue(healthHeartRateItem.getHeartRaveValue());
            healthHeartRateItem2.setRemark(healthHeartRateItem.getRemark());
            if (healthHeartRateItem2.getHeartRaveValue() > 0) {
                i2++;
            }
            i3 += healthHeartRateItem.getHeartRaveValue();
            heartRateDetailVO.maxValue = Math.max(heartRateDetailVO.maxValue, healthHeartRateItem2.getHeartRaveValue());
            heartRateDetailVO.minValue = Math.min(heartRateDetailVO.minValue, healthHeartRateItem2.getHeartRaveValue());
        }
        if (i2 > 0) {
            heartRateDetailVO.avgValue = i3 / i2;
        }
        LogUtil.d("size:" + heartRateDetailVO.items.size());
        LogUtil.d(Arrays.toString(heartRateDetailVO.items.toArray()));
        LogUtil.d(Arrays.toString(heartRateDetailVO.dates.toArray()));
        heartRateDetailVO.mainVO = this.mainVO;
        return heartRateDetailVO;
    }
}
