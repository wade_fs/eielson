package com.wade.fit.model.net;

import android.util.Log;
import okhttp3.logging.HttpLoggingInterceptor;

/* renamed from: com.wade.fit.model.net.-$$Lambda$ApiManager$z_TpMz9SywHqnrEll3PifuiutFQ  reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ApiManager$z_TpMz9SywHqnrEll3PifuiutFQ implements HttpLoggingInterceptor.Logger {
    public static final /* synthetic */ $$Lambda$ApiManager$z_TpMz9SywHqnrEll3PifuiutFQ INSTANCE = new $$Lambda$ApiManager$z_TpMz9SywHqnrEll3PifuiutFQ();

    private /* synthetic */ $$Lambda$ApiManager$z_TpMz9SywHqnrEll3PifuiutFQ() {
    }

    public final void log(String str) {
        Log.d("TAG", "REQUEST：----" + str);
    }
}
