package com.wade.fit.ui.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.wade.fit.R;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseCalendarActivity;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.model.bean.DetailType;
import com.wade.fit.model.bean.StepDetailVO;
import com.wade.fit.persenter.main.DetailPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.NumUtil;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.views.DetailSportChart;
import java.util.Calendar;
import java.util.Date;

/* renamed from: com.wade.fit.ui.main.activity.DetailActivity */
public class DetailActivity extends BaseCalendarActivity<DetailPresenter> implements Constants {
    public static final String DETAILTYPE_KEY = "DETAILTYPE_KEY";
    public static final String DETAIL_DATE_KEY = "DETAIL_DATE_KEY";
    RelativeLayout barBg;
    DetailSportChart detailSportChart;
    DetailTimeType detailTimeType = DetailTimeType.DAY;
    DetailType detailType = DetailType.STEP;
    View ivBack;
    ImageView ivNextDate;
    ImageView ivPreDate;
    /* access modifiers changed from: private */
    public Date mSearchDate;
    TabLayout tabLayout;
    TextView tvData1;
    TextView tvData2;
    TextView tvData2Title;
    TextView tvDate;
    TextView tvDay;
    TextView tvMonth;
    TextView tvTotalData;
    TextView tvTotalTitle;
    TextView tvUnit1;
    TextView tvUnit2;
    TextView tvWeek;
    TextView tvYear;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_detail;
    }

    /* access modifiers changed from: protected */
    public void updateBySelected(Date date, CalendarDate calendarDate) {
        super.updateBySelected(date, calendarDate);
        updateByDate(((DetailPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day)));
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBack) {
            finish();
        } else if (id == R.id.ivSelectedDate || id == R.id.tvDate) {
            this.clendarDialog.mOnSelectDateListener = new OnSelectDateListener() {
                /* class com.wade.fit.ui.main.activity.DetailActivity.C24851 */

                public void onSelectOtherMonth(int i) {
                }

                public void onSelectDate(CalendarDate calendarDate) {
                    Date unused = DetailActivity.this.mSearchDate = ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day);
                    DetailActivity detailActivity = DetailActivity.this;
                    detailActivity.updateByDate(((DetailPresenter) detailActivity.mPresenter).getDetailCurrent(DetailTimeType.DAY, ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day)));
                }
            };
            this.clendarDialog.showDialog();
        } else {
            switch (id) {
                case R.id.rbDay /*2131296769*/:
                    this.detailSportChart.isAddAvgText(false);
                    this.detailTimeType = DetailTimeType.DAY;
                    this.ivNextDate.setVisibility(View.VISIBLE);
                    this.ivPreDate.setVisibility(View.VISIBLE);
                    updateByDate(((DetailPresenter) this.mPresenter).getDetailCurrent(this.detailTimeType, this.mSearchDate));
                    return;
                case R.id.rbOneMonth /*2131296770*/:
                    this.ivNextDate.setVisibility(View.GONE);
                    this.ivPreDate.setVisibility(View.GONE);
                    this.detailSportChart.isAddAvgText(false);
                    this.detailTimeType = DetailTimeType.ONE_MONTH;
                    updateByDate(((DetailPresenter) this.mPresenter).getOneMonth());
                    return;
                case R.id.rbSixMonth /*2131296771*/:
                    this.ivNextDate.setVisibility(View.GONE);
                    this.ivPreDate.setVisibility(View.GONE);
                    this.detailTimeType = DetailTimeType.SIX_MONTH;
                    this.detailSportChart.isAddAvgText(true);
                    updateByDate(((DetailPresenter) this.mPresenter).getSixMonth());
                    return;
                case R.id.rbYear /*2131296772*/:
                    this.ivNextDate.setVisibility(View.GONE);
                    this.ivPreDate.setVisibility(View.GONE);
                    this.detailTimeType = DetailTimeType.YEAR;
                    this.detailSportChart.isAddAvgText(true);
                    updateByDate(((DetailPresenter) this.mPresenter).getYearMonth());
                    return;
                default:
                    return;
            }
        }
    }

    private void setAvg(StepDetailVO stepDetailVO, int i) {
        float f;
        double formatPoint;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        for (HealthSportItem healthSportItem : stepDetailVO.items) {
            if (i == 0) {
                if (healthSportItem.getStepCount() > 0) {
                    if (this.detailTimeType == DetailTimeType.YEAR || this.detailTimeType == DetailTimeType.SIX_MONTH) {
                        i3 += healthSportItem.getItemCount();
                    } else {
                        i3++;
                    }
                }
                i4 = stepDetailVO.mainVO.healthSport.getTotalStepCount();
            } else if (i == 1) {
                if (healthSportItem.getCalory() > 0) {
                    if (this.detailTimeType == DetailTimeType.YEAR || this.detailTimeType == DetailTimeType.SIX_MONTH) {
                        i3 += healthSportItem.getItemCount();
                    } else {
                        i3++;
                    }
                }
                i4 = stepDetailVO.mainVO.healthSport.getTotalCalory();
            } else if (i == 2) {
                if (healthSportItem.getDistance() > 0) {
                    if (this.detailTimeType == DetailTimeType.YEAR || this.detailTimeType == DetailTimeType.SIX_MONTH) {
                        i3 += healthSportItem.getItemCount();
                    } else {
                        i3++;
                    }
                }
                i4 = stepDetailVO.mainVO.healthSport.getTotalDistance();
            }
        }
        if (i == 2) {
            f = 0.0f;
            if (BleSdkWrapper.isDistUnitKm()) {
                if (i3 != 0) {
                    formatPoint = NumUtil.formatPoint((double) (((float) (i4 / i3)) / 1000.0f), 2);
                }
            } else if (i3 != 0) {
                formatPoint = NumUtil.formatPoint((double) UnitUtil.km2mile(((float) (i4 / i3)) / 1000.0f), 2);
            }
            f = (float) formatPoint;
        } else {
            if (i3 != 0) {
                i2 = i4 / i3;
            }
            f = (float) i2;
        }
        this.tvData1.setVisibility(View.GONE);
        if (this.detailType.equals(DetailType.DISTANCE)) {
            TextView textView = this.tvData2;
            textView.setText(f + "");
        } else {
            TextView textView2 = this.tvData2;
            textView2.setText(((int) f) + "");
        }
        TextView textView3 = this.tvTotalData;
        textView3.setText(i4 + "");
    }

    /* access modifiers changed from: private */
    public void updateByDate(StepDetailVO stepDetailVO) {
        String str;
        StepDetailVO stepDetailVO2 = stepDetailVO;
        if (this.detailTimeType == DetailTimeType.DAY) {
            changeDateUpdateUI(this.mSearchDate);
        } else {
            this.tvDate.setText(stepDetailVO2.mainVO.date);
        }
        int i = C24873.$SwitchMap$com$wade.fit$model$bean$DetailType[this.detailType.ordinal()];
        if (i == 1) {
            TextView textView = this.tvTotalData;
            textView.setText(stepDetailVO2.mainVO.healthSport.getTotalStepCount() + "");
            TextView textView2 = this.tvTotalTitle;
            textView2.setText(getResources().getString(R.string.total_steps) + "(" + getResources().getString(R.string.unit_steps) + ")");
            int i2 = C24873.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.detailTimeType.ordinal()];
            if (i2 == 1) {
                this.tvUnit1.setVisibility(View.VISIBLE);
                this.tvUnit2.setVisibility(View.VISIBLE);
                this.tvData2Title.setText((int) R.string.activity_times);
                this.tvData1.setVisibility(View.VISIBLE);
                this.tvData1.setText(String.valueOf(stepDetailVO2.mainVO.healthSport.getTotalActiveTime() / 60));
                this.tvData2.setText(String.valueOf(stepDetailVO2.mainVO.healthSport.getTotalActiveTime() % 60));
            } else if (i2 == 2 || i2 == 3 || i2 == 4) {
                TextView textView3 = this.tvData2Title;
                textView3.setText(getResources().getString(R.string.avg_day) + "(" + getResources().getString(R.string.unit_steps) + ")");
                this.tvUnit1.setVisibility(View.GONE);
                this.tvUnit2.setVisibility(View.GONE);
                setAvg(stepDetailVO2, 0);
            }
        } else if (i == 2) {
            TextView textView4 = this.tvTotalTitle;
            textView4.setText(getResources().getString(R.string.total_speed) + "(" + getResources().getString(R.string.unit_calorie) + ")");
            this.tvData2Title.setText((int) R.string.activity_times);
            int i3 = C24873.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.detailTimeType.ordinal()];
            if (i3 == 1) {
                this.tvData1.setVisibility(View.VISIBLE);
                this.tvData1.setText(String.valueOf(stepDetailVO2.mainVO.healthSport.getTotalActiveTime() / 60));
                this.tvData2.setText(String.valueOf(stepDetailVO2.mainVO.healthSport.getTotalActiveTime() % 60));
                this.tvUnit1.setVisibility(View.VISIBLE);
                this.tvUnit2.setVisibility(View.VISIBLE);
                this.tvData2Title.setText((int) R.string.activity_times);
            } else if (i3 == 2 || i3 == 3 || i3 == 4) {
                this.tvUnit1.setVisibility(View.GONE);
                this.tvUnit2.setVisibility(View.GONE);
                TextView textView5 = this.tvData2Title;
                textView5.setText(getResources().getString(R.string.avg_day) + "(" + getResources().getString(R.string.unit_calorie) + ")");
                setAvg(stepDetailVO2, 1);
            }
            TextView textView6 = this.tvTotalData;
            textView6.setText(stepDetailVO2.mainVO.healthSport.getTotalCalory() + "");
        } else if (i == 3) {
            this.tvData2Title.setText((int) R.string.activity_times);
            int i4 = C24873.$SwitchMap$com$wade.fit$model$bean$DetailTimeType[this.detailTimeType.ordinal()];
            if (i4 == 1) {
                this.tvData1.setVisibility(View.VISIBLE);
                this.tvData1.setText(String.valueOf(stepDetailVO2.mainVO.healthSport.getTotalActiveTime() / 60));
                this.tvData2.setText(String.valueOf(stepDetailVO2.mainVO.healthSport.getTotalActiveTime() % 60));
                this.tvUnit1.setVisibility(View.VISIBLE);
                this.tvUnit2.setVisibility(View.VISIBLE);
                this.tvData2Title.setText((int) R.string.activity_times);
            } else if (i4 == 2 || i4 == 3 || i4 == 4) {
                if (!BleSdkWrapper.isDistUnitKm()) {
                    TextView textView7 = this.tvData2Title;
                    textView7.setText(getResources().getString(R.string.avg_day) + "(" + getResources().getString(R.string.unit_mi) + ")");
                } else {
                    TextView textView8 = this.tvData2Title;
                    textView8.setText(getResources().getString(R.string.avg_day) + "(" + getResources().getString(R.string.unit_kilometer) + ")");
                }
                this.tvUnit1.setVisibility(View.GONE);
                this.tvUnit2.setVisibility(View.GONE);
                setAvg(stepDetailVO2, 2);
            }
            float totalDistance = ((float) stepDetailVO2.mainVO.healthSport.getTotalDistance()) / 1000.0f;
            if (!BleSdkWrapper.isDistUnitKm()) {
                str = NumUtil.float2String(UnitUtil.km2mile(totalDistance), 2);
                TextView textView9 = this.tvTotalTitle;
                textView9.setText(getResources().getString(R.string.total_distance) + "(" + getResources().getString(R.string.unit_mi) + ")");
            } else {
                str = NumUtil.float2String(totalDistance, 2);
                TextView textView10 = this.tvTotalTitle;
                textView10.setText(getResources().getString(R.string.total_distance) + "(" + getResources().getString(R.string.unit_kilometer) + ")");
            }
            this.tvTotalData.setText(str);
        }
        this.detailSportChart.initDatas(stepDetailVO2.items, stepDetailVO2.dates, this.detailType);
    }

    /* renamed from: com.wade.fit.ui.main.activity.DetailActivity$3 */
    static /* synthetic */ class C24873 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailTimeType = new int[DetailTimeType.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailType = new int[DetailType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(14:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|(3:19|20|22)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0051 */
        static {
            /*
                com.wade.fit.model.bean.DetailType[] r0 = com.wade.fit.model.bean.DetailType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailType = r0
                r0 = 1
                int[] r1 = com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailType r2 = com.wade.fit.model.bean.DetailType.STEP     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailType r3 = com.wade.fit.model.bean.DetailType.CAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.DetailType r4 = com.wade.fit.model.bean.DetailType.DISTANCE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                com.wade.fit.model.bean.DetailTimeType[] r3 = com.wade.fit.model.bean.DetailTimeType.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailTimeType = r3
                int[] r3 = com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x003d }
                com.wade.fit.model.bean.DetailTimeType r4 = com.wade.fit.model.bean.DetailTimeType.DAY     // Catch:{ NoSuchFieldError -> 0x003d }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x003d }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x003d }
            L_0x003d:
                int[] r0 = com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0047 }
                com.wade.fit.model.bean.DetailTimeType r3 = com.wade.fit.model.bean.DetailTimeType.ONE_MONTH     // Catch:{ NoSuchFieldError -> 0x0047 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
                r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x0047 }
            L_0x0047:
                int[] r0 = com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0051 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.YEAR     // Catch:{ NoSuchFieldError -> 0x0051 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0051 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0051 }
            L_0x0051:
                int[] r0 = com.wade.fit.ui.main.activity.DetailActivity.C24873.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x005c }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.SIX_MONTH     // Catch:{ NoSuchFieldError -> 0x005c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
            L_0x005c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.ui.main.activity.DetailActivity.C24873.<clinit>():void");
        }
    }

    public static void startActivity(Activity activity, DetailType detailType2, Date date) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra("DETAILTYPE_KEY", detailType2);
        intent.putExtra("DETAIL_DATE_KEY", date);
        activity.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.TabLayout.addTab(android.support.design.widget.TabLayout$Tab, boolean):void
     arg types: [android.support.design.widget.TabLayout$Tab, int]
     candidates:
      android.support.design.widget.TabLayout.addTab(android.support.design.widget.TabLayout$Tab, int):void
      android.support.design.widget.TabLayout.addTab(android.support.design.widget.TabLayout$Tab, boolean):void */
    public void initView() {
        this.layoutTitle.setVisibility(View.GONE);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.barBg.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = ScreenUtil.getStatusHeight(this);
        this.barBg.setLayoutParams(layoutParams);
        this.detailType = (DetailType) getIntent().getSerializableExtra("DETAILTYPE_KEY");
        this.mSearchDate = (Date) getIntent().getSerializableExtra("DETAIL_DATE_KEY");
        int i = C24873.$SwitchMap$com$wade.fit$model$bean$DetailType[this.detailType.ordinal()];
        if (i == 1) {
            TabLayout tabLayout2 = this.tabLayout;
            tabLayout2.addTab(tabLayout2.newTab().setTag("1").setText((int) R.string.detail_steps), true);
            TabLayout tabLayout3 = this.tabLayout;
            tabLayout3.addTab(tabLayout3.newTab().setText((int) R.string.unit_calories));
            TabLayout tabLayout4 = this.tabLayout;
            tabLayout4.addTab(tabLayout4.newTab().setText((int) R.string.distance_str));
        } else if (i == 2) {
            TabLayout tabLayout5 = this.tabLayout;
            tabLayout5.addTab(tabLayout5.newTab().setText((int) R.string.detail_steps));
            TabLayout tabLayout6 = this.tabLayout;
            tabLayout6.addTab(tabLayout6.newTab().setText((int) R.string.unit_calories), true);
            TabLayout tabLayout7 = this.tabLayout;
            tabLayout7.addTab(tabLayout7.newTab().setText((int) R.string.distance_str));
        } else if (i == 3) {
            TabLayout tabLayout8 = this.tabLayout;
            tabLayout8.addTab(tabLayout8.newTab().setText((int) R.string.detail_steps));
            TabLayout tabLayout9 = this.tabLayout;
            tabLayout9.addTab(tabLayout9.newTab().setText((int) R.string.unit_calories));
            TabLayout tabLayout10 = this.tabLayout;
            tabLayout10.addTab(tabLayout10.newTab().setText((int) R.string.distance_str), true);
        }
        this.tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.small_font_color));
        this.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            /* class com.wade.fit.ui.main.activity.DetailActivity.C24862 */

            public void onTabReselected(TabLayout.Tab tab) {
            }

            public void onTabUnselected(TabLayout.Tab tab) {
            }

            public void onTabSelected(TabLayout.Tab tab) {
                int selectedTabPosition = DetailActivity.this.tabLayout.getSelectedTabPosition();
                if (selectedTabPosition == 0) {
                    DetailActivity.this.detailType = DetailType.STEP;
                } else if (selectedTabPosition == 1) {
                    DetailActivity.this.detailType = DetailType.CAL;
                } else if (selectedTabPosition == 2) {
                    DetailActivity.this.detailType = DetailType.DISTANCE;
                }
                DetailActivity detailActivity = DetailActivity.this;
                detailActivity.updateByDate(((DetailPresenter) detailActivity.mPresenter).getDetail());
            }
        });
        updateByDate(((DetailPresenter) this.mPresenter).getDetailCurrent(this.detailTimeType, this.mSearchDate));
    }

    /* access modifiers changed from: package-private */
    public void changeNextDay() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ivNextDate, 500)) {
            this.mSearchDate = DateUtil.getSpecifiedDayAfterDate(DateUtil.formatYMD.format(this.mSearchDate));
            updateByDate(((DetailPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
        }
    }

    /* access modifiers changed from: package-private */
    public void changePreDay() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ivPreDate, 500)) {
            this.mSearchDate = DateUtil.getSpecifiedDayBeforeDate(DateUtil.formatYMD.format(this.mSearchDate));
            updateByDate(((DetailPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
        }
    }

    private void changeDateUpdateUI(Date date) {
        if (DateUtil.formatYMD.format(date).equals(DateUtil.formatYMD.format(Calendar.getInstance().getTime()))) {
            this.ivNextDate.setVisibility(View.INVISIBLE);
            this.tvDate.setText(getResources().getString(R.string.today));
            return;
        }
        this.ivNextDate.setVisibility(View.VISIBLE);
        this.tvDate.setText(DateUtil.formatYMD.format(date));
    }
}
