package com.wade.fit.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.log.LogUtil;

public class CustomToggleButton extends View implements View.OnTouchListener, View.OnClickListener {
    private Callback callback;
    private float currentX;
    private float dis;
    private boolean isSlipping = false;
    private boolean isSwitchOn = false;
    public boolean isTouchEnable = true;
    private Rect off_Rect;
    private OnSwitchListener onSwitchListener;
    private Rect on_Rect;
    Paint paint = new Paint();
    private float previousX;
    private int rectPadding = 3;
    private Bitmap slip_Btn;
    private Bitmap switch_off_Bkg;
    private Bitmap switch_on_Bkg;

    public interface Callback {
        boolean handerEvent();
    }

    public interface OnSwitchListener {
        void onSwitched(boolean z);
    }

    public CustomToggleButton(Context context) {
        super(context);
        init();
    }

    public CustomToggleButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        setOnTouchListener(this);
        setImageResource(R.mipmap.toggle_on, R.mipmap.toggle_off, R.mipmap.toggle_thumb);
        setOnClickListener(this);
    }

    public void setImageResource(int i, int i2, int i3) {
        this.switch_on_Bkg = BitmapFactory.decodeResource(getResources(), i);
        this.switch_off_Bkg = BitmapFactory.decodeResource(getResources(), i2);
        this.slip_Btn = BitmapFactory.decodeResource(getResources(), i3);
        int width = this.switch_off_Bkg.getWidth() - this.slip_Btn.getWidth();
        int i4 = this.rectPadding;
        this.on_Rect = new Rect(width - i4, i4, this.switch_off_Bkg.getWidth() - this.rectPadding, this.slip_Btn.getHeight() - this.rectPadding);
        int i5 = this.rectPadding;
        this.off_Rect = new Rect(i5, i5, this.slip_Btn.getWidth() + this.rectPadding, this.slip_Btn.getHeight() - this.rectPadding);
        postInvalidate();
    }

    public boolean getSwitchState() {
        return this.isSwitchOn;
    }

    public void setSwitchState(boolean z) {
        this.isSwitchOn = z;
        postInvalidate();
    }

    /* access modifiers changed from: protected */
    public void updateSwitchState(boolean z) {
        this.isSwitchOn = z;
        postInvalidate();
    }

    public Callback getCallback() {
        return this.callback;
    }

    public void setCallback(Callback callback2) {
        this.callback = callback2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float f;
        super.onDraw(canvas);
        float f2 = 0.0f;
        if (this.isSlipping) {
            if (this.currentX > ((float) this.switch_on_Bkg.getWidth())) {
                f = (float) (this.switch_on_Bkg.getWidth() - this.slip_Btn.getWidth());
            } else {
                f = this.currentX - ((float) (this.slip_Btn.getWidth() / 2));
            }
            if (!this.isSlipping || this.currentX >= ((float) (this.switch_on_Bkg.getWidth() / 2))) {
                canvas.drawBitmap(this.switch_on_Bkg, 0.0f, (float) ((getMeasuredHeight() - this.switch_off_Bkg.getHeight()) / 2), this.paint);
            } else {
                canvas.drawBitmap(this.switch_off_Bkg, 0.0f, (float) ((getMeasuredHeight() - this.switch_off_Bkg.getHeight()) / 2), this.paint);
            }
        } else if (this.isSwitchOn) {
            f = (float) this.on_Rect.left;
            canvas.drawBitmap(this.switch_on_Bkg, 0.0f, (float) ((getMeasuredHeight() - this.switch_off_Bkg.getHeight()) / 2), this.paint);
        } else {
            f = (float) this.off_Rect.left;
            canvas.drawBitmap(this.switch_off_Bkg, 0.0f, (float) ((getMeasuredHeight() - this.switch_off_Bkg.getHeight()) / 2), this.paint);
        }
        if (f >= 0.0f) {
            f2 = f > ((float) (this.switch_on_Bkg.getWidth() - this.slip_Btn.getWidth())) ? (float) (this.switch_on_Bkg.getWidth() - this.slip_Btn.getWidth()) : f;
        }
        canvas.drawBitmap(this.slip_Btn, f2, (float) ((getMeasuredHeight() - this.slip_Btn.getHeight()) / 2), this.paint);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i2);
        if (View.MeasureSpec.getMode(i2) == 0) {
            size = this.switch_on_Bkg.getHeight() + getPaddingBottom() + getPaddingTop();
        }
        setMeasuredDimension(this.switch_on_Bkg.getWidth(), size);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        Callback callback2 = this.callback;
        return callback2 != null && callback2.handerEvent();
    }

    public void setOnSwitchListener(OnSwitchListener onSwitchListener2) {
        this.onSwitchListener = onSwitchListener2;
    }

    public void onClick(View view) {
        if (!ButtonUtils.isFastDoubleClick(super.getId())) {
            this.isSwitchOn = !this.isSwitchOn;
            LogUtil.d("isSwitchOn:" + this.isSwitchOn);
            OnSwitchListener onSwitchListener2 = this.onSwitchListener;
            if (onSwitchListener2 != null) {
                onSwitchListener2.onSwitched(this.isSwitchOn);
            }
            invalidate();
        }
    }
}
