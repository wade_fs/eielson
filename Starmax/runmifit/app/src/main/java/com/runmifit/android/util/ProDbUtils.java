package com.wade.fit.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.baidu.mobstat.Config;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ProDbUtils {
    public static Date getTodayDate() {
        int[] iArr = DateUtil.todayYearMonthDay();
        return getDate(iArr[0], iArr[1], iArr[2]);
    }

    public static boolean isOneDay(Date date, Date date2) {
        return date.getYear() == date2.getYear() && date.getMonth() == date2.getMonth() && date.getDate() == date2.getDate();
    }

    public static Date getDate(int i, int i2, int i3) {
        return getDate(i, i2, i3, 0, 0, 0);
    }

    public static String getTime(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return String.valueOf(instance.getTime().getTime());
    }

    public static Date getDate(int i, int i2, int i3, int i4, int i5, int i6) {
        return new GregorianCalendar(i, i2 - 1, i3, i4, i5, i6).getTime();
    }

    public static long dateToStamp(int i, int i2, int i3, int i4, int i5, int i6) {
        Date date = getDate(i, i2, i3, i4, i5, i6);
        if (date != null) {
            return date.getTime();
        }
        return 0;
    }

    public static long getStartDateToStamp(int i, int i2, int i3) {
        return dateToStamp(i, i2, i3, 0, 0, 0);
    }

    public static long getEndDateToStamp(int i, int i2, int i3) {
        return dateToStamp(i, i2, i3, 23, 59, 59);
    }

    public static Date getStartDate(int i, int i2, int i3) {
        return getDate(i, i2, i3, 0, 0, 0);
    }

    public static Date getEndDate(int i, int i2, int i3) {
        return getDate(i, i2, i3, 23, 59, 59);
    }

    public static Date getWeekStartDate(int i, int i2) {
        int i3;
        int abs = Math.abs(i);
        Calendar instance = Calendar.getInstance();
        int i4 = (instance.get(7) - 1) - i2;
        if (i4 <= 0) {
            if (i4 == 0) {
                i3 = 0;
                instance.add(5, i3 - (abs * 7));
                return getDayStartDate(instance.get(1), instance.get(2) + 1, instance.get(5));
            }
            i4 += 7;
        }
        i3 = -i4;
        instance.add(5, i3 - (abs * 7));
        return getDayStartDate(instance.get(1), instance.get(2) + 1, instance.get(5));
    }

    public static Date getWeekEndDate(int i, int i2) {
        int i3;
        int abs = Math.abs(i);
        Calendar instance = Calendar.getInstance();
        int i4 = (instance.get(7) - 1) - i2;
        if (i4 <= 0) {
            if (i4 == 0) {
                i3 = 0;
                instance.add(5, i3 - (abs * 7));
                instance.add(5, 6);
                return getDayEndDate(instance.get(1), instance.get(2) + 1, instance.get(5));
            }
            i4 += 7;
        }
        i3 = -i4;
        instance.add(5, i3 - (abs * 7));
        instance.add(5, 6);
        return getDayEndDate(instance.get(1), instance.get(2) + 1, instance.get(5));
    }

    public static Date getDayStartDate(int i, int i2, int i3) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(getDateStr(i, i2, i3, true));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getDayEndDate(int i, int i2, int i3) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(getDateStr(i, i2, i3, false));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDateStr(int i, int i2, int i3, boolean z) {
        if (z) {
            return String.format("%04d", Integer.valueOf(i)) + "-" + String.format("%02d", Integer.valueOf(i2)) + "-" + String.format("%02d", Integer.valueOf(i3)) + " " + String.format("%02d", 0) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", 0) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", 0);
        }
        return String.format("%04d", Integer.valueOf(i)) + "-" + String.format("%02d", Integer.valueOf(i2)) + "-" + String.format("%02d", Integer.valueOf(i3)) + " " + String.format("%02d", 23) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", 59) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", 59);
    }

    public static boolean isTableExist(SQLiteDatabase sQLiteDatabase, String str) {
        if (str == null) {
            return false;
        }
        try {
            Cursor rawQuery = sQLiteDatabase.rawQuery("select count(*) as c from Sqlite_master  where type ='table' and name ='" + str.trim() + "' ", null);
            if (!rawQuery.moveToNext() || rawQuery.getInt(0) <= 0) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
