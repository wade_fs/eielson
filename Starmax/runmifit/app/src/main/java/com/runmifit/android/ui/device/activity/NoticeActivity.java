package com.wade.fit.ui.device.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.AppNotice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.service.AssistService;
import com.wade.fit.service.IntelligentNotificationService;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.views.CustomToggleButton;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.device.activity.NoticeActivity */
public class NoticeActivity extends BaseActivity {
    private static final int REQUEST_CON_PERMISSION = 20;
    private static final int REQUEST_MSG_PERMISSION = 19;
    private static final int REQUEST_NOTICE_PERMISSION_CODE = 18;
    private static final int REQUEST_PHONE_PERMISSION = 17;
    private AppNotice appNotice;
    private DeviceConfig config;
    ItemToggleLayout itCall;
    ItemToggleLayout itEmial;
    ItemToggleLayout itFacebook;
    ItemToggleLayout itInstagram;
    ItemToggleLayout itLine;
    ItemToggleLayout itLinked;
    ItemToggleLayout itMessage;
    ItemToggleLayout itMessenger;
    ItemToggleLayout itNoNotice;
    ItemToggleLayout itQQ;
    ItemToggleLayout itSkype;
    ItemToggleLayout itTwitter;
    ItemToggleLayout itVK;
    ItemToggleLayout itWeChat;
    ItemToggleLayout itWhatsApp;
    private Dialog mDialog;
    private String[] permissionsCall = {"android.permission.READ_PHONE_STATE", "android.permission.CALL_PHONE", "android.permission.READ_CALL_LOG", "android.permission.ANSWER_PHONE_CALLS"};
    private String[] permissionsCon = {"android.permission.READ_CONTACTS"};
    private String[] permissionsMsg = {"android.permission.READ_SMS"};

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_notice;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.tips_1));
        this.config = SPHelper.getDeviceConfig();
        this.appNotice = this.config.notice;
        boolean z = this.config.isMessage;
        boolean z2 = this.config.isCall;
        this.itNoNotice.setOpen(this.config.isDisturbMode);
        this.itNoNotice.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$pvV9vuO_vi08LS1HoUPe8VpsskA */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$0$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itMessage.setOpen(z);
        this.itMessage.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$mF47uTdErgCAM85v3_pTmzgQX_E */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$1$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itCall.setOpen(z2);
        this.itCall.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$wh3srUupMaQ7FwCV2TK62OpFvi8 */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$2$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itQQ.setOpen(this.appNotice.f5221qq);
        this.itFacebook.setOpen(this.appNotice.facebook);
        this.itInstagram.setOpen(this.appNotice.instagram);
        this.itLine.setOpen(this.appNotice.line);
        this.itLinked.setOpen(this.appNotice.linked);
        this.itSkype.setOpen(this.appNotice.skype);
        this.itTwitter.setOpen(this.appNotice.twitter);
        this.itVK.setOpen(this.appNotice.f5222vk);
        this.itWeChat.setOpen(this.appNotice.wechat);
        this.itWhatsApp.setOpen(this.appNotice.whatsApp);
        this.itMessenger.setOpen(this.appNotice.messager);
        this.itEmial.setOpen(this.appNotice.email);
        this.itWhatsApp.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$nQwkSte0UFXZt153QTrL1_Dob10 */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$3$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itInstagram.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$lF0zI6ouSV8qWB3mBoiaVIX9M */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$4$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itQQ.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$lN_gw6evPNJwVeTEH5OxWQEaTLU */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$5$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itFacebook.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$5DuaNpMqO6tgqtxyJkvBPxVodrg */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$6$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itLine.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$7BsIzcm9Eyqysl4ypKB0uLn_3GI */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$7$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itLinked.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$gcx_WyQTPI9rg3KQXATeibqrB1c */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$8$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itSkype.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$285a8je22eTpx3IDNZBK_R4VYw */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$9$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itTwitter.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$TjCDGWMYUj6X7sWNprDg5oThDyg */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$10$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itVK.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$BOZ2GPq2EJ2C9zdE2zULcbedOI */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$11$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itWeChat.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$0S7pBKbB0hKMXIVvCFscIgqgxaA */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$12$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itMessenger.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$KjkYVEq_crtusWfaCpnKgJuA2MA */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$13$NoticeActivity(itemToggleLayout, z);
            }
        });
        this.itEmial.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$V7o0GM5V_3eYUyh9O3oUsCGSY */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                NoticeActivity.this.lambda$initView$14$NoticeActivity(itemToggleLayout, z);
            }
        });
        if (this.config.isDisturbMode) {
            this.itMessage.setImageBack(R.mipmap.toggle_on);
            this.itCall.setImageBack(R.mipmap.toggle_on);
            this.itQQ.setImageBack(R.mipmap.toggle_on);
            this.itFacebook.setImageBack(R.mipmap.toggle_on);
            this.itInstagram.setImageBack(R.mipmap.toggle_on);
            this.itLine.setImageBack(R.mipmap.toggle_on);
            this.itLinked.setImageBack(R.mipmap.toggle_on);
            this.itSkype.setImageBack(R.mipmap.toggle_on);
            this.itTwitter.setImageBack(R.mipmap.toggle_on);
            this.itVK.setImageBack(R.mipmap.toggle_on);
            this.itWeChat.setImageBack(R.mipmap.toggle_on);
            this.itWhatsApp.setImageBack(R.mipmap.toggle_on);
            this.itMessenger.setImageBack(R.mipmap.toggle_on);
            this.itEmial.setImageBack(R.mipmap.toggle_on);
        } else {
            this.itMessage.setImageBack(R.mipmap.toggle_on_n);
            this.itCall.setImageBack(R.mipmap.toggle_on_n);
            this.itQQ.setImageBack(R.mipmap.toggle_on_n);
            this.itFacebook.setImageBack(R.mipmap.toggle_on_n);
            this.itInstagram.setImageBack(R.mipmap.toggle_on_n);
            this.itLine.setImageBack(R.mipmap.toggle_on_n);
            this.itLinked.setImageBack(R.mipmap.toggle_on_n);
            this.itSkype.setImageBack(R.mipmap.toggle_on_n);
            this.itTwitter.setImageBack(R.mipmap.toggle_on_n);
            this.itVK.setImageBack(R.mipmap.toggle_on_n);
            this.itWeChat.setImageBack(R.mipmap.toggle_on_n);
            this.itWhatsApp.setImageBack(R.mipmap.toggle_on_n);
            this.itMessenger.setImageBack(R.mipmap.toggle_on_n);
            this.itEmial.setImageBack(R.mipmap.toggle_on_n);
        }
        this.itMessage.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$mqjyd32VAKNzu7AySYj_4FT4iSc */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$15$NoticeActivity();
            }
        });
        this.itCall.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$UeuM2ap4JVZ4Xntm9Ab0hwf69Gs */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$16$NoticeActivity();
            }
        });
        this.itQQ.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$8krbhPVHRXMXowTFS_wVIkDSFRw */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$17$NoticeActivity();
            }
        });
        this.itFacebook.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$S66zFGcScYSQwztSED2Rpok6Oxs */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$18$NoticeActivity();
            }
        });
        this.itInstagram.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$GeyXTCcJzN_CLcU7etpwyoEPaQ */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$19$NoticeActivity();
            }
        });
        this.itLine.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$RU4xnJd3kkqfTDiCOM5RQ5Gio88 */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$20$NoticeActivity();
            }
        });
        this.itLinked.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$h54yLlsLioUfWG0z_K_W1F0sCdA */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$21$NoticeActivity();
            }
        });
        this.itSkype.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$TFeWpphZi89oTn24BF_fHJFzAY */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$22$NoticeActivity();
            }
        });
        this.itTwitter.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$GXP4lUEXzX5tIizRk7zcSpsVvY */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$23$NoticeActivity();
            }
        });
        this.itVK.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$X0_VMd2qa98IYKE2jOQelIexnA4 */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$24$NoticeActivity();
            }
        });
        this.itWeChat.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$FVyJEooiU6Yb_eqyFjsAfduYthw */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$25$NoticeActivity();
            }
        });
        this.itWhatsApp.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$0JNjqTeQ39dQI2hmr6BfU6fOYU4 */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$26$NoticeActivity();
            }
        });
        this.itMessenger.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$DaMWieKtNyoAsrq6S4KwnZ9KPSc */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$27$NoticeActivity();
            }
        });
        this.itEmial.setToggleButtonCallback(new CustomToggleButton.Callback() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$_aVwduy5JCvYrHpSzP3X2WIdHAA */

            public final boolean handerEvent() {
                return NoticeActivity.this.lambda$initView$28$NoticeActivity();
            }
        });
        if (!checkSelfPermission(this.permissionsMsg)) {
            requestPermissions(19, this.permissionsMsg);
        } else if (!checkSelfPermission(this.permissionsCall)) {
            requestPermissions(17, this.permissionsCall);
        } else if (!checkSelfPermission(this.permissionsCon)) {
            requestPermissions(20, this.permissionsCon);
        } else {
            requestNotice();
        }
    }

    public /* synthetic */ void lambda$initView$0$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        DeviceConfig deviceConfig = this.config;
        deviceConfig.isDisturbMode = z;
        SPHelper.saveDeviceConfig(deviceConfig);
        BleSdkWrapper.setDeviceState(null);
        if (z) {
            this.itMessage.setImageBack(R.mipmap.toggle_on);
            this.itCall.setImageBack(R.mipmap.toggle_on);
            this.itQQ.setImageBack(R.mipmap.toggle_on);
            this.itFacebook.setImageBack(R.mipmap.toggle_on);
            this.itInstagram.setImageBack(R.mipmap.toggle_on);
            this.itLine.setImageBack(R.mipmap.toggle_on);
            this.itLinked.setImageBack(R.mipmap.toggle_on);
            this.itSkype.setImageBack(R.mipmap.toggle_on);
            this.itTwitter.setImageBack(R.mipmap.toggle_on);
            this.itVK.setImageBack(R.mipmap.toggle_on);
            this.itWeChat.setImageBack(R.mipmap.toggle_on);
            this.itWhatsApp.setImageBack(R.mipmap.toggle_on);
            this.itMessenger.setImageBack(R.mipmap.toggle_on);
            this.itEmial.setImageBack(R.mipmap.toggle_on);
            return;
        }
        this.itMessage.setImageBack(R.mipmap.toggle_on_n);
        this.itCall.setImageBack(R.mipmap.toggle_on_n);
        this.itQQ.setImageBack(R.mipmap.toggle_on_n);
        this.itFacebook.setImageBack(R.mipmap.toggle_on_n);
        this.itInstagram.setImageBack(R.mipmap.toggle_on_n);
        this.itLine.setImageBack(R.mipmap.toggle_on_n);
        this.itLinked.setImageBack(R.mipmap.toggle_on_n);
        this.itSkype.setImageBack(R.mipmap.toggle_on_n);
        this.itTwitter.setImageBack(R.mipmap.toggle_on_n);
        this.itVK.setImageBack(R.mipmap.toggle_on_n);
        this.itWeChat.setImageBack(R.mipmap.toggle_on_n);
        this.itWhatsApp.setImageBack(R.mipmap.toggle_on_n);
        this.itMessenger.setImageBack(R.mipmap.toggle_on_n);
        this.itEmial.setImageBack(R.mipmap.toggle_on_n);
    }

    public /* synthetic */ void lambda$initView$1$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        DeviceConfig deviceConfig = this.config;
        deviceConfig.isMessage = z;
        SPHelper.saveDeviceConfig(deviceConfig);
        BleSdkWrapper.setNotice(this.config.notice, null);
    }

    public /* synthetic */ void lambda$initView$2$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        DeviceConfig deviceConfig = this.config;
        deviceConfig.isCall = z;
        SPHelper.saveDeviceConfig(deviceConfig);
        BleSdkWrapper.setNotice(this.config.notice, null);
    }

    public /* synthetic */ void lambda$initView$3$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.whatsApp = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$4$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.instagram = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$5$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.f5221qq = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$6$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.facebook = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$7$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.line = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$8$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.linked = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$9$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.skype = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$10$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.twitter = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$11$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.f5222vk = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$12$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.wechat = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$13$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.messager = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ void lambda$initView$14$NoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.email = z;
        SPHelper.saveAppNotice(appNotice2);
        BleSdkWrapper.setNotice(this.appNotice, null);
    }

    public /* synthetic */ boolean lambda$initView$15$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$16$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$17$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$18$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$19$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$20$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$21$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$22$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$23$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$24$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$25$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$26$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$27$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public /* synthetic */ boolean lambda$initView$28$NoticeActivity() {
        return !this.config.isDisturbMode;
    }

    public void requestPermissionsSuccess(int i) {
        if (i != 17) {
            if (i != 19) {
                if (i == 20) {
                    requestNotice();
                }
            } else if (!checkSelfPermission(this.permissionsCall)) {
                requestPermissions(17, this.permissionsCall);
            } else if (!checkSelfPermission(this.permissionsCon)) {
                requestPermissions(20, this.permissionsCon);
            } else {
                requestNotice();
            }
        } else if (!checkSelfPermission(this.permissionsCon)) {
            requestPermissions(20, this.permissionsCon);
        } else {
            requestNotice();
        }
    }

    private void requestMsg() {
        String[] strArr = this.permissionsMsg;
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, strArr[i])) {
                this.mDialog = DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_sms_title), getResources().getString(R.string.permisson_sms_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                    /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$8AQcjdSYZth42_LzL7i15gIQ7o */

                    public final void onClick(View view) {
                        NoticeActivity.this.lambda$requestMsg$29$NoticeActivity(view);
                    }
                }, new View.OnClickListener() {
                    /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$Ql9auvdXtw5vnGfVmhj9UBjWWiY */

                    public final void onClick(View view) {
                        NoticeActivity.this.lambda$requestMsg$30$NoticeActivity(view);
                    }
                });
                return;
            } else {
                finish();
                i++;
            }
        }
    }

    public /* synthetic */ void lambda$requestMsg$29$NoticeActivity(View view) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getApplicationContext().getPackageName(), null));
        startActivityForResult(intent, 19);
    }

    public /* synthetic */ void lambda$requestMsg$30$NoticeActivity(View view) {
        this.mDialog.dismiss();
        finish();
    }

    private void requestNotice() {
        if (!isNotificationEnabled()) {
            this.mDialog = DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_notication_title), getResources().getString(R.string.permisson_notication_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$pPP6Qv7pLDS6012Xq3sfxQkhP2w */

                public final void onClick(View view) {
                    NoticeActivity.this.lambda$requestNotice$31$NoticeActivity(view);
                }
            }, new View.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$AsiBmUk8tLZT5RTvx_cnrovF_6Q */

                public final void onClick(View view) {
                    NoticeActivity.this.lambda$requestNotice$32$NoticeActivity(view);
                }
            });
        } else {
            startService(new Intent(this, AssistService.class));
        }
    }

    public /* synthetic */ void lambda$requestNotice$31$NoticeActivity(View view) {
        startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), 18);
        this.mDialog.dismiss();
    }

    public /* synthetic */ void lambda$requestNotice$32$NoticeActivity(View view) {
        this.mDialog.dismiss();
        finish();
    }

    /* access modifiers changed from: package-private */
    public void toSetNoti() {
        if (!ButtonUtils.isFastDoubleClick(R.id.txtSetting, 1000)) {
            startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), 18);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 18) {
            requestNotice();
        } else if (i == 19) {
            if (!checkSelfPermission(this.permissionsMsg)) {
                requestPermissions(19, this.permissionsMsg);
            } else if (!checkSelfPermission(this.permissionsCall)) {
                requestPermissions(17, this.permissionsCall);
            } else if (!checkSelfPermission(this.permissionsCon)) {
                requestPermissions(20, this.permissionsCon);
            } else {
                requestNotice();
            }
        } else if (i == 17) {
            if (!checkSelfPermission(this.permissionsCall)) {
                requestPermissions(17, this.permissionsCall);
            } else if (!checkSelfPermission(this.permissionsCon)) {
                requestPermissions(20, this.permissionsCon);
            } else {
                requestNotice();
            }
        } else if (i != 20) {
        } else {
            if (!checkSelfPermission(this.permissionsCon)) {
                requestPermissions(20, this.permissionsCon);
            } else {
                requestNotice();
            }
        }
    }

    public void requestPermissionsFail(int i) {
        if (i == 19) {
            requestMsg();
            return;
        }
        int i2 = 0;
        if (i == 17) {
            String[] strArr = this.permissionsCall;
            int length = strArr.length;
            while (i2 < length) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, strArr[i2])) {
                    this.mDialog = DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_phone_title), getResources().getString(R.string.permisson_phone_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                        /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$vcen3Q7k4DhNpUv0CycS8qHu0Fw */

                        public final void onClick(View view) {
                            NoticeActivity.this.lambda$requestPermissionsFail$33$NoticeActivity(view);
                        }
                    }, new View.OnClickListener() {
                        /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$7nfuYcnLoCWrpKOdeFJdZfw2VM */

                        public final void onClick(View view) {
                            NoticeActivity.this.lambda$requestPermissionsFail$34$NoticeActivity(view);
                        }
                    });
                    return;
                } else {
                    finish();
                    i2++;
                }
            }
        } else if (i == 20) {
            String[] strArr2 = this.permissionsCon;
            int length2 = strArr2.length;
            while (i2 < length2) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, strArr2[i2])) {
                    this.mDialog = DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_phone_title), getResources().getString(R.string.permisson_phone_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                        /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$UwIJuInEXsJu9aQRqiQmG9xzzHI */

                        public final void onClick(View view) {
                            NoticeActivity.this.lambda$requestPermissionsFail$35$NoticeActivity(view);
                        }
                    }, new View.OnClickListener() {
                        /* class com.wade.fit.ui.device.activity.$$Lambda$NoticeActivity$yrT2_GgzlgabbKj_j4LQIEOPlM */

                        public final void onClick(View view) {
                            NoticeActivity.this.lambda$requestPermissionsFail$36$NoticeActivity(view);
                        }
                    });
                    return;
                } else {
                    finish();
                    i2++;
                }
            }
        }
    }

    public /* synthetic */ void lambda$requestPermissionsFail$33$NoticeActivity(View view) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getApplicationContext().getPackageName(), null));
        startActivityForResult(intent, 17);
    }

    public /* synthetic */ void lambda$requestPermissionsFail$34$NoticeActivity(View view) {
        this.mDialog.dismiss();
        finish();
    }

    public /* synthetic */ void lambda$requestPermissionsFail$35$NoticeActivity(View view) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getApplicationContext().getPackageName(), null));
        startActivityForResult(intent, 20);
    }

    public /* synthetic */ void lambda$requestPermissionsFail$36$NoticeActivity(View view) {
        this.mDialog.dismiss();
        finish();
    }

    private boolean isNotificationEnabled() {
        String string = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        DebugLog.m6203d("enabledListeners:" + string);
        if (!TextUtils.isEmpty(string)) {
            return string.contains(IntelligentNotificationService.class.getName());
        }
        return false;
    }
}
