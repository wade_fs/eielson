package com.wade.fit.ui.mine.activity;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.mine.activity.GoogleFitActivity_ViewBinding */
public class GoogleFitActivity_ViewBinding implements Unbinder {
    private GoogleFitActivity target;

    public GoogleFitActivity_ViewBinding(GoogleFitActivity googleFitActivity) {
        this(googleFitActivity, googleFitActivity.getWindow().getDecorView());
    }

    public GoogleFitActivity_ViewBinding(GoogleFitActivity googleFitActivity, View view) {
        this.target = googleFitActivity;
        googleFitActivity.ilGoogleFit = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.ilGoogleFit, "field 'ilGoogleFit'", ItemToggleLayout.class);
    }

    public void unbind() {
        GoogleFitActivity googleFitActivity = this.target;
        if (googleFitActivity != null) {
            this.target = null;
            googleFitActivity.ilGoogleFit = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
