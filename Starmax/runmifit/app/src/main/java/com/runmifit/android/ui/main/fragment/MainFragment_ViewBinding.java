package com.wade.fit.ui.main.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.SportPieView;
import com.wade.fit.views.pullscrollview.PullToRefreshScrollView;

/* renamed from: com.wade.fit.ui.main.fragment.MainFragment_ViewBinding */
public class MainFragment_ViewBinding implements Unbinder {
    private MainFragment target;
    private View view2131296572;
    private View view2131296575;
    private View view2131296576;
    private View view2131296596;
    private View view2131296597;
    private View view2131296598;
    private View view2131296599;
    private View view2131296600;
    private View view2131296601;
    private View view2131296635;
    private View view2131296638;
    private View view2131296897;
    private View view2131297063;

    public MainFragment_ViewBinding(final MainFragment mainFragment, View view) {
        this.target = mainFragment;
        mainFragment.ivConState = (ImageView) Utils.findRequiredViewAsType(view, R.id.ivConState, "field 'ivConState'", ImageView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.ivSelectedDate, "field 'ivSelectedDate' and method 'changeDate'");
        mainFragment.ivSelectedDate = (ImageView) Utils.castView(findRequiredView, R.id.ivSelectedDate, "field 'ivSelectedDate'", ImageView.class);
        this.view2131296576 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25561 */

            public void doClick(View view) {
                mainFragment.changeDate();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.tvSynProgress, "field 'tvSynProgress' and method 'changeDate'");
        mainFragment.tvSynProgress = (TextView) Utils.castView(findRequiredView2, R.id.tvSynProgress, "field 'tvSynProgress'", TextView.class);
        this.view2131297063 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25612 */

            public void doClick(View view) {
                mainFragment.changeDate();
            }
        });
        mainFragment.barBgTitle = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.bar_bg_title, "field 'barBgTitle'", RelativeLayout.class);
        mainFragment.tvCal = (TextView) Utils.findRequiredViewAsType(view, R.id.tvCal, "field 'tvCal'", TextView.class);
        mainFragment.tvDistance = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDistance, "field 'tvDistance'", TextView.class);
        mainFragment.tvDistanceUnit = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDistanceUnit, "field 'tvDistanceUnit'", TextView.class);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.ivPreDate, "field 'ivPreDate' and method 'changePaseDay'");
        mainFragment.ivPreDate = (ImageView) Utils.castView(findRequiredView3, R.id.ivPreDate, "field 'ivPreDate'", ImageView.class);
        this.view2131296575 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25623 */

            public void doClick(View view) {
                mainFragment.changePaseDay();
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.ivNextDate, "field 'ivNextDate' and method 'changeNextDay'");
        mainFragment.ivNextDate = (ImageView) Utils.castView(findRequiredView4, R.id.ivNextDate, "field 'ivNextDate'", ImageView.class);
        this.view2131296572 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25634 */

            public void doClick(View view) {
                mainFragment.changeNextDay();
            }
        });
        View findRequiredView5 = Utils.findRequiredView(view, R.id.sport_centerDataPie, "field 'sport_centerDataPie' and method 'toDetail'");
        mainFragment.sport_centerDataPie = (SportPieView) Utils.castView(findRequiredView5, R.id.sport_centerDataPie, "field 'sport_centerDataPie'", SportPieView.class);
        this.view2131296897 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25645 */

            public void doClick(View view) {
                mainFragment.toDetail(view);
            }
        });
        mainFragment.mPullRefreshScrollView = (PullToRefreshScrollView) Utils.findRequiredViewAsType(view, R.id.mPullRefreshScrollView, "field 'mPullRefreshScrollView'", PullToRefreshScrollView.class);
        mainFragment.tvRecordType = (TextView) Utils.findRequiredViewAsType(view, R.id.tvRecordType, "field 'tvRecordType'", TextView.class);
        mainFragment.tvRecordVaule1 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvRecordVaule1, "field 'tvRecordVaule1'", TextView.class);
        mainFragment.tvRecordVaule2 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvRecordVaule2, "field 'tvRecordVaule2'", TextView.class);
        mainFragment.tvHeartVaule = (TextView) Utils.findRequiredViewAsType(view, R.id.tvHeartVaule, "field 'tvHeartVaule'", TextView.class);
        mainFragment.tvSleepVaule1 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvSleepVaule1, "field 'tvSleepVaule1'", TextView.class);
        mainFragment.tvSleepVaule2 = (TextView) Utils.findRequiredViewAsType(view, R.id.tvSleepVaule2, "field 'tvSleepVaule2'", TextView.class);
        View findRequiredView6 = Utils.findRequiredView(view, R.id.llCal, "method 'toDetail'");
        this.view2131296635 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25656 */

            public void doClick(View view) {
                mainFragment.toDetail(view);
            }
        });
        View findRequiredView7 = Utils.findRequiredView(view, R.id.llDistance, "method 'toDetail'");
        this.view2131296638 = findRequiredView7;
        findRequiredView7.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25667 */

            public void doClick(View view) {
                mainFragment.toDetail(view);
            }
        });
        View findRequiredView8 = Utils.findRequiredView(view, R.id.layout1, "method 'layoutClick'");
        this.view2131296596 = findRequiredView8;
        findRequiredView8.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25678 */

            public void doClick(View view) {
                mainFragment.layoutClick(view);
            }
        });
        View findRequiredView9 = Utils.findRequiredView(view, R.id.layout2, "method 'layoutClick'");
        this.view2131296597 = findRequiredView9;
        findRequiredView9.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C25689 */

            public void doClick(View view) {
                mainFragment.layoutClick(view);
            }
        });
        View findRequiredView10 = Utils.findRequiredView(view, R.id.layout3, "method 'layoutClick'");
        this.view2131296598 = findRequiredView10;
        findRequiredView10.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C255710 */

            public void doClick(View view) {
                mainFragment.layoutClick(view);
            }
        });
        View findRequiredView11 = Utils.findRequiredView(view, R.id.layout4, "method 'layoutClick'");
        this.view2131296599 = findRequiredView11;
        findRequiredView11.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C255811 */

            public void doClick(View view) {
                mainFragment.layoutClick(view);
            }
        });
        View findRequiredView12 = Utils.findRequiredView(view, R.id.layout5, "method 'layoutClick'");
        this.view2131296600 = findRequiredView12;
        findRequiredView12.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C255912 */

            public void doClick(View view) {
                mainFragment.layoutClick(view);
            }
        });
        View findRequiredView13 = Utils.findRequiredView(view, R.id.layout6, "method 'layoutClick'");
        this.view2131296601 = findRequiredView13;
        findRequiredView13.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.fragment.MainFragment_ViewBinding.C256013 */

            public void doClick(View view) {
                mainFragment.layoutClick(view);
            }
        });
    }

    public void unbind() {
        MainFragment mainFragment = this.target;
        if (mainFragment != null) {
            this.target = null;
            mainFragment.ivConState = null;
            mainFragment.ivSelectedDate = null;
            mainFragment.tvSynProgress = null;
            mainFragment.barBgTitle = null;
            mainFragment.tvCal = null;
            mainFragment.tvDistance = null;
            mainFragment.tvDistanceUnit = null;
            mainFragment.ivPreDate = null;
            mainFragment.ivNextDate = null;
            mainFragment.sport_centerDataPie = null;
            mainFragment.mPullRefreshScrollView = null;
            mainFragment.tvRecordType = null;
            mainFragment.tvRecordVaule1 = null;
            mainFragment.tvRecordVaule2 = null;
            mainFragment.tvHeartVaule = null;
            mainFragment.tvSleepVaule1 = null;
            mainFragment.tvSleepVaule2 = null;
            this.view2131296576.setOnClickListener(null);
            this.view2131296576 = null;
            this.view2131297063.setOnClickListener(null);
            this.view2131297063 = null;
            this.view2131296575.setOnClickListener(null);
            this.view2131296575 = null;
            this.view2131296572.setOnClickListener(null);
            this.view2131296572 = null;
            this.view2131296897.setOnClickListener(null);
            this.view2131296897 = null;
            this.view2131296635.setOnClickListener(null);
            this.view2131296635 = null;
            this.view2131296638.setOnClickListener(null);
            this.view2131296638 = null;
            this.view2131296596.setOnClickListener(null);
            this.view2131296596 = null;
            this.view2131296597.setOnClickListener(null);
            this.view2131296597 = null;
            this.view2131296598.setOnClickListener(null);
            this.view2131296598 = null;
            this.view2131296599.setOnClickListener(null);
            this.view2131296599 = null;
            this.view2131296600.setOnClickListener(null);
            this.view2131296600 = null;
            this.view2131296601.setOnClickListener(null);
            this.view2131296601 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
