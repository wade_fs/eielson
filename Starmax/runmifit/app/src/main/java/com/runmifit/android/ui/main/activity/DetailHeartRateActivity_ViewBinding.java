package com.wade.fit.ui.main.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.HeartRateChart;

/* renamed from: com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding */
public class DetailHeartRateActivity_ViewBinding implements Unbinder {
    private DetailHeartRateActivity target;
    private View view2131296572;
    private View view2131296575;
    private View view2131296576;
    private View view2131296769;
    private View view2131296770;
    private View view2131296771;
    private View view2131296772;
    private View view2131296995;
    private View view2131297003;
    private View view2131297033;
    private View view2131297080;
    private View view2131297083;

    public DetailHeartRateActivity_ViewBinding(DetailHeartRateActivity detailHeartRateActivity) {
        this(detailHeartRateActivity, detailHeartRateActivity.getWindow().getDecorView());
    }

    public DetailHeartRateActivity_ViewBinding(final DetailHeartRateActivity detailHeartRateActivity, View view) {
        this.target = detailHeartRateActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.ivPreDate, "field 'ivPreDate' and method 'changePreDay'");
        detailHeartRateActivity.ivPreDate = (ImageView) Utils.castView(findRequiredView, R.id.ivPreDate, "field 'ivPreDate'", ImageView.class);
        this.view2131296575 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25001 */

            public void doClick(View view) {
                detailHeartRateActivity.changePreDay();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ivNextDate, "field 'ivNextDate' and method 'changeNextDay'");
        detailHeartRateActivity.ivNextDate = (ImageView) Utils.castView(findRequiredView2, R.id.ivNextDate, "field 'ivNextDate'", ImageView.class);
        this.view2131296572 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25042 */

            public void doClick(View view) {
                detailHeartRateActivity.changeNextDay();
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.tvDate, "field 'tvDate' and method 'onClick'");
        detailHeartRateActivity.tvDate = (TextView) Utils.castView(findRequiredView3, R.id.tvDate, "field 'tvDate'", TextView.class);
        this.view2131296995 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25053 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.tvDay, "field 'tvDay' and method 'onClick'");
        detailHeartRateActivity.tvDay = (TextView) Utils.castView(findRequiredView4, R.id.tvDay, "field 'tvDay'", TextView.class);
        this.view2131297003 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25064 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        View findRequiredView5 = Utils.findRequiredView(view, R.id.tvWeek, "field 'tvWeek' and method 'onClick'");
        detailHeartRateActivity.tvWeek = (TextView) Utils.castView(findRequiredView5, R.id.tvWeek, "field 'tvWeek'", TextView.class);
        this.view2131297080 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25075 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        View findRequiredView6 = Utils.findRequiredView(view, R.id.tvMonth, "field 'tvMonth' and method 'onClick'");
        detailHeartRateActivity.tvMonth = (TextView) Utils.castView(findRequiredView6, R.id.tvMonth, "field 'tvMonth'", TextView.class);
        this.view2131297033 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25086 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        View findRequiredView7 = Utils.findRequiredView(view, R.id.tvYear, "field 'tvYear' and method 'onClick'");
        detailHeartRateActivity.tvYear = (TextView) Utils.castView(findRequiredView7, R.id.tvYear, "field 'tvYear'", TextView.class);
        this.view2131297083 = findRequiredView7;
        findRequiredView7.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25097 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        detailHeartRateActivity.tvHrValue = (TextView) Utils.findRequiredViewAsType(view, R.id.tvHrValue, "field 'tvHrValue'", TextView.class);
        detailHeartRateActivity.tvAvHrValue = (TextView) Utils.findRequiredViewAsType(view, R.id.tvAvHrValue, "field 'tvAvHrValue'", TextView.class);
        detailHeartRateActivity.tvMaxHrValue = (TextView) Utils.findRequiredViewAsType(view, R.id.tvMaxHrValue, "field 'tvMaxHrValue'", TextView.class);
        detailHeartRateActivity.heartRateChart = (HeartRateChart) Utils.findRequiredViewAsType(view, R.id.heartRateChart, "field 'heartRateChart'", HeartRateChart.class);
        View findRequiredView8 = Utils.findRequiredView(view, R.id.rbYear, "method 'onClick'");
        this.view2131296772 = findRequiredView8;
        findRequiredView8.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25108 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        View findRequiredView9 = Utils.findRequiredView(view, R.id.rbDay, "method 'onClick'");
        this.view2131296769 = findRequiredView9;
        findRequiredView9.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C25119 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        View findRequiredView10 = Utils.findRequiredView(view, R.id.rbOneMonth, "method 'onClick'");
        this.view2131296770 = findRequiredView10;
        findRequiredView10.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C250110 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        View findRequiredView11 = Utils.findRequiredView(view, R.id.rbSixMonth, "method 'onClick'");
        this.view2131296771 = findRequiredView11;
        findRequiredView11.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C250211 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
        View findRequiredView12 = Utils.findRequiredView(view, R.id.ivSelectedDate, "method 'onClick'");
        this.view2131296576 = findRequiredView12;
        findRequiredView12.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity_ViewBinding.C250312 */

            public void doClick(View view) {
                detailHeartRateActivity.onClick(view);
            }
        });
    }

    public void unbind() {
        DetailHeartRateActivity detailHeartRateActivity = this.target;
        if (detailHeartRateActivity != null) {
            this.target = null;
            detailHeartRateActivity.ivPreDate = null;
            detailHeartRateActivity.ivNextDate = null;
            detailHeartRateActivity.tvDate = null;
            detailHeartRateActivity.tvDay = null;
            detailHeartRateActivity.tvWeek = null;
            detailHeartRateActivity.tvMonth = null;
            detailHeartRateActivity.tvYear = null;
            detailHeartRateActivity.tvHrValue = null;
            detailHeartRateActivity.tvAvHrValue = null;
            detailHeartRateActivity.tvMaxHrValue = null;
            detailHeartRateActivity.heartRateChart = null;
            this.view2131296575.setOnClickListener(null);
            this.view2131296575 = null;
            this.view2131296572.setOnClickListener(null);
            this.view2131296572 = null;
            this.view2131296995.setOnClickListener(null);
            this.view2131296995 = null;
            this.view2131297003.setOnClickListener(null);
            this.view2131297003 = null;
            this.view2131297080.setOnClickListener(null);
            this.view2131297080 = null;
            this.view2131297033.setOnClickListener(null);
            this.view2131297033 = null;
            this.view2131297083.setOnClickListener(null);
            this.view2131297083 = null;
            this.view2131296772.setOnClickListener(null);
            this.view2131296772 = null;
            this.view2131296769.setOnClickListener(null);
            this.view2131296769 = null;
            this.view2131296770.setOnClickListener(null);
            this.view2131296770 = null;
            this.view2131296771.setOnClickListener(null);
            this.view2131296771 = null;
            this.view2131296576.setOnClickListener(null);
            this.view2131296576 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
