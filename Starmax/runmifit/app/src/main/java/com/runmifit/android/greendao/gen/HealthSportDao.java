package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthSport;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class HealthSportDao extends AbstractDao<HealthSport, Void> {
    public static final String TABLENAME = "HEALTH_SPORT";

    public static class Properties {
        public static final Property Date = new Property(15, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(4, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");
        public static final Property IsUploaded = new Property(0, Boolean.TYPE, "isUploaded", false, "IS_UPLOADED");
        public static final Property ItemCount = new Property(18, Integer.TYPE, "itemCount", false, "ITEM_COUNT");
        public static final Property MacAddress = new Property(1, String.class, "macAddress", false, "MAC_ADDRESS");
        public static final Property Month = new Property(3, Integer.TYPE, "month", false, "MONTH");
        public static final Property Remark = new Property(17, String.class, "remark", false, "REMARK");
        public static final Property StartTime = new Property(13, Integer.TYPE, "startTime", false, "START_TIME");
        public static final Property TimeSpace = new Property(14, Integer.TYPE, "timeSpace", false, "TIME_SPACE");
        public static final Property TotalActiveTime = new Property(8, Integer.TYPE, "totalActiveTime", false, "TOTAL_ACTIVE_TIME");
        public static final Property TotalCalory = new Property(6, Integer.TYPE, "totalCalory", false, "TOTAL_CALORY");
        public static final Property TotalDayActiveTime = new Property(12, Integer.TYPE, "totalDayActiveTime", false, "TOTAL_DAY_ACTIVE_TIME");
        public static final Property TotalDayCalory = new Property(10, Integer.TYPE, "totalDayCalory", false, "TOTAL_DAY_CALORY");
        public static final Property TotalDayDistance = new Property(11, Integer.TYPE, "totalDayDistance", false, "TOTAL_DAY_DISTANCE");
        public static final Property TotalDayStepCount = new Property(9, Integer.TYPE, "totalDayStepCount", false, "TOTAL_DAY_STEP_COUNT");
        public static final Property TotalDistance = new Property(7, Integer.TYPE, "totalDistance", false, "TOTAL_DISTANCE");
        public static final Property TotalStepCount = new Property(5, Integer.TYPE, "totalStepCount", false, "TOTAL_STEP_COUNT");
        public static final Property UserId = new Property(16, String.class, Constants.userId, false, "USER_ID");
        public static final Property Year = new Property(2, Integer.TYPE, "year", false, "YEAR");
    }

    public Void getKey(HealthSport healthSport) {
        return null;
    }

    public boolean hasKey(HealthSport healthSport) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(HealthSport healthSport, long j) {
        return null;
    }

    public HealthSportDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public HealthSportDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"HEALTH_SPORT\" (\"IS_UPLOADED\" INTEGER NOT NULL ,\"MAC_ADDRESS\" TEXT,\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"TOTAL_STEP_COUNT\" INTEGER NOT NULL ,\"TOTAL_CALORY\" INTEGER NOT NULL ,\"TOTAL_DISTANCE\" INTEGER NOT NULL ,\"TOTAL_ACTIVE_TIME\" INTEGER NOT NULL ,\"TOTAL_DAY_STEP_COUNT\" INTEGER NOT NULL ,\"TOTAL_DAY_CALORY\" INTEGER NOT NULL ,\"TOTAL_DAY_DISTANCE\" INTEGER NOT NULL ,\"TOTAL_DAY_ACTIVE_TIME\" INTEGER NOT NULL ,\"START_TIME\" INTEGER NOT NULL ,\"TIME_SPACE\" INTEGER NOT NULL ,\"DATE\" INTEGER NOT NULL ,\"USER_ID\" TEXT,\"REMARK\" TEXT,\"ITEM_COUNT\" INTEGER NOT NULL );");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"HEALTH_SPORT\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, HealthSport healthSport) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, healthSport.getIsUploaded() ? 1 : 0);
        String macAddress = healthSport.getMacAddress();
        if (macAddress != null) {
            databaseStatement.bindString(2, macAddress);
        }
        databaseStatement.bindLong(3, (long) healthSport.getYear());
        databaseStatement.bindLong(4, (long) healthSport.getMonth());
        databaseStatement.bindLong(5, (long) healthSport.getDay());
        databaseStatement.bindLong(6, (long) healthSport.getTotalStepCount());
        databaseStatement.bindLong(7, (long) healthSport.getTotalCalory());
        databaseStatement.bindLong(8, (long) healthSport.getTotalDistance());
        databaseStatement.bindLong(9, (long) healthSport.getTotalActiveTime());
        databaseStatement.bindLong(10, (long) healthSport.getTotalDayStepCount());
        databaseStatement.bindLong(11, (long) healthSport.getTotalDayCalory());
        databaseStatement.bindLong(12, (long) healthSport.getTotalDayDistance());
        databaseStatement.bindLong(13, (long) healthSport.getTotalDayActiveTime());
        databaseStatement.bindLong(14, (long) healthSport.getStartTime());
        databaseStatement.bindLong(15, (long) healthSport.getTimeSpace());
        databaseStatement.bindLong(16, healthSport.getDate());
        String userId = healthSport.getUserId();
        if (userId != null) {
            databaseStatement.bindString(17, userId);
        }
        String remark = healthSport.getRemark();
        if (remark != null) {
            databaseStatement.bindString(18, remark);
        }
        databaseStatement.bindLong(19, (long) healthSport.getItemCount());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, HealthSport healthSport) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, healthSport.getIsUploaded() ? 1 : 0);
        String macAddress = healthSport.getMacAddress();
        if (macAddress != null) {
            sQLiteStatement.bindString(2, macAddress);
        }
        sQLiteStatement.bindLong(3, (long) healthSport.getYear());
        sQLiteStatement.bindLong(4, (long) healthSport.getMonth());
        sQLiteStatement.bindLong(5, (long) healthSport.getDay());
        sQLiteStatement.bindLong(6, (long) healthSport.getTotalStepCount());
        sQLiteStatement.bindLong(7, (long) healthSport.getTotalCalory());
        sQLiteStatement.bindLong(8, (long) healthSport.getTotalDistance());
        sQLiteStatement.bindLong(9, (long) healthSport.getTotalActiveTime());
        sQLiteStatement.bindLong(10, (long) healthSport.getTotalDayStepCount());
        sQLiteStatement.bindLong(11, (long) healthSport.getTotalDayCalory());
        sQLiteStatement.bindLong(12, (long) healthSport.getTotalDayDistance());
        sQLiteStatement.bindLong(13, (long) healthSport.getTotalDayActiveTime());
        sQLiteStatement.bindLong(14, (long) healthSport.getStartTime());
        sQLiteStatement.bindLong(15, (long) healthSport.getTimeSpace());
        sQLiteStatement.bindLong(16, healthSport.getDate());
        String userId = healthSport.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(17, userId);
        }
        String remark = healthSport.getRemark();
        if (remark != null) {
            sQLiteStatement.bindString(18, remark);
        }
        sQLiteStatement.bindLong(19, (long) healthSport.getItemCount());
    }

    public HealthSport readEntity(Cursor cursor, int i) {
        Cursor cursor2 = cursor;
        boolean z = cursor2.getShort(i + 0) != 0;
        int i2 = i + 1;
        String string = cursor2.isNull(i2) ? null : cursor2.getString(i2);
        int i3 = cursor2.getInt(i + 2);
        int i4 = cursor2.getInt(i + 3);
        int i5 = cursor2.getInt(i + 4);
        int i6 = cursor2.getInt(i + 5);
        int i7 = cursor2.getInt(i + 6);
        int i8 = cursor2.getInt(i + 7);
        int i9 = cursor2.getInt(i + 8);
        int i10 = cursor2.getInt(i + 9);
        int i11 = cursor2.getInt(i + 10);
        int i12 = cursor2.getInt(i + 11);
        int i13 = cursor2.getInt(i + 12);
        int i14 = cursor2.getInt(i + 13);
        int i15 = cursor2.getInt(i + 14);
        long j = cursor2.getLong(i + 15);
        int i16 = i + 16;
        String string2 = cursor2.isNull(i16) ? null : cursor2.getString(i16);
        int i17 = i + 17;
        return new HealthSport(z, string, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, j, string2, cursor2.isNull(i17) ? null : cursor2.getString(i17), cursor2.getInt(i + 18));
    }

    public void readEntity(Cursor cursor, HealthSport healthSport, int i) {
        healthSport.setIsUploaded(cursor.getShort(i + 0) != 0);
        int i2 = i + 1;
        String str = null;
        healthSport.setMacAddress(cursor.isNull(i2) ? null : cursor.getString(i2));
        healthSport.setYear(cursor.getInt(i + 2));
        healthSport.setMonth(cursor.getInt(i + 3));
        healthSport.setDay(cursor.getInt(i + 4));
        healthSport.setTotalStepCount(cursor.getInt(i + 5));
        healthSport.setTotalCalory(cursor.getInt(i + 6));
        healthSport.setTotalDistance(cursor.getInt(i + 7));
        healthSport.setTotalActiveTime(cursor.getInt(i + 8));
        healthSport.setTotalDayStepCount(cursor.getInt(i + 9));
        healthSport.setTotalDayCalory(cursor.getInt(i + 10));
        healthSport.setTotalDayDistance(cursor.getInt(i + 11));
        healthSport.setTotalDayActiveTime(cursor.getInt(i + 12));
        healthSport.setStartTime(cursor.getInt(i + 13));
        healthSport.setTimeSpace(cursor.getInt(i + 14));
        healthSport.setDate(cursor.getLong(i + 15));
        int i3 = i + 16;
        healthSport.setUserId(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 17;
        if (!cursor.isNull(i4)) {
            str = cursor.getString(i4);
        }
        healthSport.setRemark(str);
        healthSport.setItemCount(cursor.getInt(i + 18));
    }
}
