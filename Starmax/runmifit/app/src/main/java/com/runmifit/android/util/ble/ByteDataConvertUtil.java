package com.wade.fit.util.ble;

import com.baidu.mobstat.Config;
import com.google.common.primitives.UnsignedBytes;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;

public class ByteDataConvertUtil {
    private static String CHAR_ENCODE = "UTF-8";

    public static int Byte2Int(byte b) {
        return b >= 0 ? b : b + 256;
    }

    public static byte Int2Byte(int i) {
        return (byte) (i & 255);
    }

    public static int bytesToInt(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (int length = bArr.length - 1; length >= 0; length--) {
            String hexString = Integer.toHexString(bArr[length] & UnsignedBytes.MAX_VALUE);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
        }
        return Integer.valueOf(sb.toString(), 16).intValue();
    }

    public static String bytesToHexString2(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & UnsignedBytes.MAX_VALUE);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
        }
        return sb.toString();
    }

    public static void BinnCat(byte[] bArr, byte b, int i, int i2) {
        int i3 = i2 + i;
        int i4 = 0;
        while (i < i3) {
            byte b2 = bArr[i4];
            i++;
            i4++;
        }
    }

    public static void BinnCat(byte[] bArr, byte[] bArr2, int i, int i2) {
        int i3 = 0;
        while (i3 < i2) {
            bArr2[i3] = bArr[i];
            i3++;
            i++;
        }
    }

    public static byte[] LongToBin(long j, int i) {
        byte[] bArr = new byte[i];
        int i2 = i - 1;
        int i3 = 0;
        while (i2 >= 0) {
            bArr[i3] = (byte) ((int) (j >> (i2 * 8)));
            i2--;
            i3++;
        }
        return bArr;
    }

    public static void LongToBin(long j, byte[] bArr, int i, int i2) {
        int i3 = i2 - 1;
        while (i3 >= 0) {
            bArr[i] = (byte) ((int) (j >> (i3 * 8)));
            i3--;
            i++;
        }
    }

    public static byte[] IntToBin(int i, int i2) {
        byte[] bArr = new byte[i2];
        int i3 = i2 - 1;
        int i4 = 0;
        while (i3 >= 0) {
            bArr[i4] = (byte) (i >> (i3 * 8));
            i3--;
            i4++;
        }
        return bArr;
    }

    public static byte[] IntToBinReverseArray(int i, int i2) {
        byte[] IntToBin = IntToBin(i, i2);
        byte[] bArr = new byte[IntToBin.length];
        for (int i3 = 0; i3 < bArr.length; i3++) {
            bArr[i3] = IntToBin[(IntToBin.length - 1) - i3];
        }
        return bArr;
    }

    public static byte[] IntToBin(int i, byte[] bArr, int i2, int i3) {
        int i4 = i3 - 1;
        while (i4 >= 0) {
            bArr[i2] = (byte) (i >> (i4 * 8));
            i4--;
            i2++;
        }
        return bArr;
    }

    public static long BinToLong(byte[] bArr, int i, int i2) {
        int i3 = i2 - 1;
        long j = 0;
        while (i3 >= 0) {
            j = (j << 8) | ((long) (bArr[i] & UnsignedBytes.MAX_VALUE));
            i3--;
            i++;
        }
        return j;
    }

    public static int BinToInt(byte[] bArr, int i, int i2) {
        int i3 = i2 - 1;
        byte b = 0;
        while (i3 >= 0) {
            b = (b << 8) | (bArr[i] & UnsignedBytes.MAX_VALUE);
            i3--;
            i++;
        }
        return b;
    }

    public static byte[] getMacBytes(String str) {
        byte[] bArr = new byte[6];
        String[] split = str.split(Config.TRACE_TODAY_VISIT_SPLIT);
        for (int i = 0; i < split.length; i++) {
            bArr[i] = (byte) Integer.parseInt(split[i], 16);
        }
        return bArr;
    }

    public static String getStrBytes(byte[] bArr, int i, int i2) {
        if (bArr.length < i + i2) {
            return null;
        }
        String str = "";
        for (int i3 = 0; i3 < i2; i3++) {
            str = str + String.format("%02X", Byte.valueOf(bArr[i + i3]));
        }
        return str;
    }

    public static String bytesToHexString(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & UnsignedBytes.MAX_VALUE);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
            sb.append(" ");
        }
        return sb.toString();
    }

    public static String[] bytesToHexStrings(byte[] bArr) {
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        String[] strArr = new String[bArr.length];
        for (int i = 0; i < bArr.length; i++) {
            String hexString = Integer.toHexString(bArr[i] & UnsignedBytes.MAX_VALUE);
            if (hexString.length() == 1) {
                hexString = 0 + hexString;
            }
            strArr[i] = hexString;
        }
        return strArr;
    }

    public static <T> T invertArray(T t) {
        int length = Array.getLength(t);
        T newInstance = Array.newInstance(t.getClass().getComponentType(), length);
        System.arraycopy(t, 0, newInstance, 0, length);
        for (int i = 0; i < length / 2; i++) {
            Object obj = Array.get(newInstance, i);
            int i2 = (length - i) - 1;
            Array.set(newInstance, i, Array.get(newInstance, i2));
            Array.set(newInstance, i2, obj);
        }
        return newInstance;
    }

    public static int toRevInt(byte[] bArr, int i, int i2) {
        int i3 = 0;
        int i4 = (i + i2) - 1;
        byte b = 0;
        while (i3 < i2) {
            b = (b << 8) | (bArr[i4] & UnsignedBytes.MAX_VALUE);
            i3++;
            i4--;
        }
        return b;
    }

    public static byte[] Int2Bit8(int i) {
        byte b = (byte) i;
        byte[] bArr = new byte[8];
        for (int i2 = 0; i2 <= 7; i2++) {
            bArr[i2] = (byte) (b & 1);
            b = (byte) (b >> 1);
        }
        return bArr;
    }

    public static int Bit8Array2Int(byte[] bArr) {
        int length = bArr.length - 1;
        int i = 0;
        for (int i2 = length; i2 >= 0; i2--) {
            i += bArr[i2] << (length - i2);
        }
        return i;
    }

    public static byte i2b(int i) {
        return (byte) Integer.toHexString(i).charAt(0);
    }

    public static byte[] byteMerger(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length, bArr2.length);
        return bArr3;
    }

    public static byte[] StringToByte(String str, String str2) {
        if (str != null) {
            try {
                if (!str.trim().equals("")) {
                    return str.getBytes(str2);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        }
        return new byte[0];
    }

    public static String bytetoString(byte[] bArr) {
        try {
            return new String(bArr, CHAR_ENCODE);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static byte[] getJsonByte(String str) {
        return StringToByte(str, CHAR_ENCODE);
    }

    public static byte[] getBytesByFilePath(String str) {
        try {
            File file = new File(str);
            FileInputStream fileInputStream = new FileInputStream(file);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream((int) file.length());
            byte[] bArr = new byte[1000];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    fileInputStream.close();
                    byteArrayOutputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
