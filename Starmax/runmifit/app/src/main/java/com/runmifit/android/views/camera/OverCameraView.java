package com.wade.fit.views.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.WindowManager;
import freemarker.log.Logger;
import java.util.ArrayList;

public class OverCameraView extends AppCompatImageView {
    private Context context;
    private boolean isFoucuing;
    private Paint touchFocusPaint;
    private Rect touchFocusRect;

    public OverCameraView(Context context2) {
        this(context2, null, 0);
    }

    public OverCameraView(Context context2, AttributeSet attributeSet) {
        this(context2, attributeSet, 0);
    }

    public OverCameraView(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        init(context2);
    }

    private void init(Context context2) {
        this.context = context2;
        this.touchFocusPaint = new Paint();
        this.touchFocusPaint.setColor(-16711936);
        this.touchFocusPaint.setStyle(Paint.Style.STROKE);
        this.touchFocusPaint.setStrokeWidth(3.0f);
    }

    public boolean isFoucuing() {
        return this.isFoucuing;
    }

    public void setFoucuing(boolean z) {
        this.isFoucuing = z;
    }

    public void setTouchFoucusRect(Camera camera, Camera.AutoFocusCallback autoFocusCallback, float f, float f2) {
        this.touchFocusRect = new Rect((int) (f - 100.0f), (int) (f2 - 100.0f), (int) (f + 100.0f), (int) (f2 + 100.0f));
        int i = 1000;
        int windowWidth = ((this.touchFocusRect.left * 2000) / getWindowWidth(this.context)) - 1000;
        int windowHeight = ((this.touchFocusRect.top * 2000) / getWindowHeight(this.context)) - 1000;
        int windowWidth2 = ((this.touchFocusRect.right * 2000) / getWindowWidth(this.context)) - 1000;
        int windowHeight2 = ((this.touchFocusRect.bottom * 2000) / getWindowHeight(this.context)) - 1000;
        if (windowWidth < -1000) {
            windowWidth = NotificationManagerCompat.IMPORTANCE_UNSPECIFIED;
        }
        if (windowHeight < -1000) {
            windowHeight = NotificationManagerCompat.IMPORTANCE_UNSPECIFIED;
        }
        if (windowWidth2 > 1000) {
            windowWidth2 = 1000;
        }
        if (windowHeight2 <= 1000) {
            i = windowHeight2;
        }
        doTouchFocus(camera, autoFocusCallback, new Rect(windowWidth, windowHeight, windowWidth2, i));
        postInvalidate();
    }

    public void doTouchFocus(Camera camera, Camera.AutoFocusCallback autoFocusCallback, Rect rect) {
        if (camera != null && !this.isFoucuing) {
            try {
                ArrayList arrayList = new ArrayList();
                arrayList.add(new Camera.Area(rect, 1000));
                Camera.Parameters parameters = camera.getParameters();
                parameters.setFocusAreas(arrayList);
                parameters.setMeteringAreas(arrayList);
                parameters.setFocusMode(Logger.LIBRARY_NAME_AUTO);
                camera.cancelAutoFocus();
                camera.setParameters(parameters);
                camera.autoFocus(autoFocusCallback);
                this.isFoucuing = true;
            } catch (Exception e) {
                Log.e("设置相机参数异常", e.getMessage());
            }
        }
    }

    public void disDrawTouchFocusRect() {
        this.touchFocusRect = null;
        postInvalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        drawTouchFocusRect(canvas);
        super.onDraw(canvas);
    }

    public static int getWindowHeight(Context context2) {
        return ((WindowManager) context2.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
    }

    public static int getWindowWidth(Context context2) {
        return ((WindowManager) context2.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();
    }

    private void drawTouchFocusRect(Canvas canvas) {
        Rect rect = this.touchFocusRect;
        if (rect != null) {
            Canvas canvas2 = canvas;
            canvas2.drawRect((float) (rect.left - 2), (float) this.touchFocusRect.bottom, (float) (this.touchFocusRect.left + 20), (float) (this.touchFocusRect.bottom + 2), this.touchFocusPaint);
            canvas2.drawRect((float) (this.touchFocusRect.left - 2), (float) (this.touchFocusRect.bottom - 20), (float) this.touchFocusRect.left, (float) this.touchFocusRect.bottom, this.touchFocusPaint);
            canvas2.drawRect((float) (this.touchFocusRect.left - 2), (float) (this.touchFocusRect.top - 2), (float) (this.touchFocusRect.left + 20), (float) this.touchFocusRect.top, this.touchFocusPaint);
            canvas2.drawRect((float) (this.touchFocusRect.left - 2), (float) this.touchFocusRect.top, (float) this.touchFocusRect.left, (float) (this.touchFocusRect.top + 20), this.touchFocusPaint);
            canvas2.drawRect((float) (this.touchFocusRect.right - 20), (float) (this.touchFocusRect.top - 2), (float) (this.touchFocusRect.right + 2), (float) this.touchFocusRect.top, this.touchFocusPaint);
            canvas2.drawRect((float) this.touchFocusRect.right, (float) this.touchFocusRect.top, (float) (this.touchFocusRect.right + 2), (float) (this.touchFocusRect.top + 20), this.touchFocusPaint);
            canvas2.drawRect((float) (this.touchFocusRect.right - 20), (float) this.touchFocusRect.bottom, (float) (this.touchFocusRect.right + 2), (float) (this.touchFocusRect.bottom + 2), this.touchFocusPaint);
            canvas2.drawRect((float) this.touchFocusRect.right, (float) (this.touchFocusRect.bottom - 20), (float) (this.touchFocusRect.right + 2), (float) this.touchFocusRect.bottom, this.touchFocusPaint);
        }
    }
}
