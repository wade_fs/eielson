package com.wade.fit.util;

import com.google.android.gms.common.util.AndroidUtilsLight;
import com.google.common.base.Ascii;
import com.google.common.primitives.UnsignedBytes;
import com.wade.fit.util.file.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class EncryptUtil {
    private static final String AES_Algorithm = "AES";
    public static String AES_Transformation = "AES/ECB/NoPadding";
    private static final String DES_Algorithm = "DES";
    public static String DES_Transformation = "DES/ECB/NoPadding";
    private static final String TripleDES_Algorithm = "DESede";
    public static String TripleDES_Transformation = "DESede/ECB/NoPadding";

    private EncryptUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static String stringToSHA(String str) {
        try {
            byte[] bytes = str.getBytes();
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bytes);
            return toHexString(instance.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String toHexString(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        int length = bArr.length;
        char[] cArr2 = new char[(length * 2)];
        for (int i = 0; i < length; i++) {
            byte b = bArr[i];
            int i2 = i * 2;
            cArr2[i2] = cArr[(b >>> 4) & 15];
            cArr2[i2 + 1] = cArr[b & Ascii.f4512SI];
        }
        return new String(cArr2);
    }

    public static String encryptMD2ToString(String str) {
        return encryptMD2ToString(str.getBytes());
    }

    public static String encryptMD2ToString(byte[] bArr) {
        return bytes2HexString(encryptMD2(bArr));
    }

    public static byte[] encryptMD2(byte[] bArr) {
        return encryptAlgorithm(bArr, "MD2");
    }

    public static String encryptMD5ToString(String str) {
        return encryptMD5ToString(str.getBytes());
    }

    public static String encryptMD5ToString(String str, String str2) {
        return bytes2HexString(encryptMD5((str + str2).getBytes()));
    }

    public static String encryptMD5ToString(byte[] bArr) {
        return bytes2HexString(encryptMD5(bArr));
    }

    public static String encryptMD5ToString(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length, bArr2.length);
        return bytes2HexString(encryptMD5(bArr3));
    }

    public static String bytes2HexString(byte[] bArr) {
        StringBuilder sb = new StringBuilder("");
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & UnsignedBytes.MAX_VALUE);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
        }
        return sb.toString();
    }

    public static byte[] encryptMD5(byte[] bArr) {
        return encryptAlgorithm(bArr, "MD5");
    }

    public static String encryptMD5File2String(String str) {
        return encryptMD5File2String(new File(str));
    }

    public static byte[] encryptMD5File(String str) {
        return encryptMD5File(new File(str));
    }

    public static String encryptMD5File2String(File file) {
        return encryptMD5File(file) != null ? bytes2HexString(encryptMD5File(file)) : "";
    }

    public static byte[] encryptMD5File(File file) {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                MappedByteBuffer map = fileInputStream.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(map);
                byte[] digest = instance.digest();
                FileUtil.closeIO(fileInputStream);
                return digest;
            } catch (IOException | NoSuchAlgorithmException e) {
                e = e;
                try {
                    e.printStackTrace();
                    FileUtil.closeIO(fileInputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    FileUtil.closeIO(fileInputStream);
                    throw th;
                }
            }
        } catch (IOException | NoSuchAlgorithmException e2) {
            e = e2;
            fileInputStream = null;
            e.printStackTrace();
            FileUtil.closeIO(fileInputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            FileUtil.closeIO(fileInputStream);
            throw th;
        }
    }

    public static String encryptSHA1ToString(String str) {
        return encryptSHA1ToString(str.getBytes());
    }

    public static String encryptSHA1ToString(byte[] bArr) {
        return bytes2HexString(encryptSHA1(bArr));
    }

    public static byte[] encryptSHA1(byte[] bArr) {
        return encryptAlgorithm(bArr, "SHA-1");
    }

    public static String encryptSHA224ToString(String str) {
        return encryptSHA224ToString(str.getBytes());
    }

    public static String encryptSHA224ToString(byte[] bArr) {
        return bytes2HexString(encryptSHA224(bArr));
    }

    public static byte[] encryptSHA224(byte[] bArr) {
        return encryptAlgorithm(bArr, "SHA-224");
    }

    public static String encryptSHA256ToString(String str) {
        return encryptSHA256ToString(str.getBytes());
    }

    public static String encryptSHA256ToString(byte[] bArr) {
        return bytes2HexString(encryptSHA256(bArr));
    }

    public static byte[] encryptSHA256(byte[] bArr) {
        return encryptAlgorithm(bArr, "SHA-256");
    }

    public static String encryptSHA384ToString(String str) {
        return encryptSHA384ToString(str.getBytes());
    }

    public static String encryptSHA384ToString(byte[] bArr) {
        return bytes2HexString(encryptSHA384(bArr));
    }

    public static byte[] encryptSHA384(byte[] bArr) {
        return encryptAlgorithm(bArr, "SHA-384");
    }

    public static String encryptSHA512ToString(String str) {
        return encryptSHA512ToString(str.getBytes());
    }

    public static String encryptSHA512ToString(byte[] bArr) {
        return bytes2HexString(encryptSHA512(bArr));
    }

    public static byte[] encryptSHA512(byte[] bArr) {
        return encryptAlgorithm(bArr, AndroidUtilsLight.DIGEST_ALGORITHM_SHA512);
    }

    private static byte[] encryptAlgorithm(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str);
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public static byte[] DESTemplet(byte[] bArr, byte[] bArr2, String str, String str2, boolean z) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, str);
            Cipher instance = Cipher.getInstance(str2);
            instance.init(z ? 1 : 2, secretKeySpec, new SecureRandom());
            return instance.doFinal(bArr);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static byte[] encryptDES2Base64(byte[] bArr, byte[] bArr2) {
        return EncodeUtil.base64Encode(encryptDES(bArr, bArr2));
    }

    public static String encryptDES2HexString(byte[] bArr, byte[] bArr2) {
        return bytes2HexString(encryptDES(bArr, bArr2));
    }

    public static byte[] encryptDES(byte[] bArr, byte[] bArr2) {
        return DESTemplet(bArr, bArr2, DES_Algorithm, DES_Transformation, true);
    }

    public static byte[] decryptBase64DES(byte[] bArr, byte[] bArr2) {
        return decryptDES(EncodeUtil.base64Decode(bArr), bArr2);
    }

    public static byte[] decryptHexStringDES(String str, byte[] bArr) {
        return decryptDES(hexString2Bytes(str), bArr);
    }

    public static byte[] hexString2Bytes(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        String upperCase = str.toUpperCase();
        int length = upperCase.length() / 2;
        char[] charArray = upperCase.toCharArray();
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            int i2 = i * 2;
            bArr[i] = (byte) (charToByte(charArray[i2 + 1]) | (charToByte(charArray[i2]) << 4));
        }
        return bArr;
    }

    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public static byte[] decryptDES(byte[] bArr, byte[] bArr2) {
        return DESTemplet(bArr, bArr2, DES_Algorithm, DES_Transformation, false);
    }

    public static byte[] encrypt3DES2Base64(byte[] bArr, byte[] bArr2) {
        return EncodeUtil.base64Encode(encrypt3DES(bArr, bArr2));
    }

    public static String encrypt3DES2HexString(byte[] bArr, byte[] bArr2) {
        return bytes2HexString(encrypt3DES(bArr, bArr2));
    }

    public static byte[] encrypt3DES(byte[] bArr, byte[] bArr2) {
        return DESTemplet(bArr, bArr2, TripleDES_Algorithm, TripleDES_Transformation, true);
    }

    public static byte[] decryptBase64_3DES(byte[] bArr, byte[] bArr2) {
        return decrypt3DES(EncodeUtil.base64Decode(bArr), bArr2);
    }

    public static byte[] decryptHexString3DES(String str, byte[] bArr) {
        return decrypt3DES(hexString2Bytes(str), bArr);
    }

    public static byte[] decrypt3DES(byte[] bArr, byte[] bArr2) {
        return DESTemplet(bArr, bArr2, TripleDES_Algorithm, TripleDES_Transformation, false);
    }

    public static byte[] encryptAES2Base64(byte[] bArr, byte[] bArr2) {
        return EncodeUtil.base64Encode(encryptAES(bArr, bArr2));
    }

    public static String encryptAES2HexString(byte[] bArr, byte[] bArr2) {
        return bytes2HexString(encryptAES(bArr, bArr2));
    }

    public static byte[] encryptAES(byte[] bArr, byte[] bArr2) {
        return DESTemplet(bArr, bArr2, AES_Algorithm, AES_Transformation, true);
    }

    public static byte[] decryptBase64AES(byte[] bArr, byte[] bArr2) {
        return decryptAES(EncodeUtil.base64Decode(bArr), bArr2);
    }

    public static byte[] decryptHexStringAES(String str, byte[] bArr) {
        return decryptAES(hexString2Bytes(str), bArr);
    }

    public static byte[] decryptAES(byte[] bArr, byte[] bArr2) {
        return DESTemplet(bArr, bArr2, AES_Algorithm, AES_Transformation, false);
    }
}
