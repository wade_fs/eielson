package com.wade.fit.model.bean;

import com.wade.fit.greendao.bean.HealthActivity;
import java.io.Serializable;
import java.util.List;

public class HealthMonthActivity implements Serializable {
    private List<HealthActivity> healthActivities;
    private String stMonth;

    public String getStMonth() {
        return this.stMonth;
    }

    public void setStMonth(String str) {
        this.stMonth = str;
    }

    public List<HealthActivity> getHealthActivities() {
        return this.healthActivities;
    }

    public void setHealthActivities(List<HealthActivity> list) {
        this.healthActivities = list;
    }
}
