package com.wade.fit.util;

public class UIAutoTest {
    public static final int REGISTER_FAILD = 101;
    public static final int REGISTER_ING = 100;
    public static final int REGISTER_SUCCESS = 101;
    public static final int SEND_LOG_FAILD = 301;
    public static final int SEND_LOG_ING = 300;
    public static final int SEND_LOG_SUCCESS = 301;
    public static final int SHARE_FAILD = 202;
    public static final int SHARE_ING = 200;
    public static final int SHARE_SUCCESS = 201;
    public static final int TEST_BLOOD_PRESSURE_ADJUSTING = 403;
    public static final int TEST_BLOOD_PRESSURE_ADJUST_FAILD = 402;
    public static final int TEST_BLOOD_PRESSURE_ADJUST_SUCCESS = 404;
    public static final int TEST_BLOOD_PRESSURE_FAILD = 402;
    public static final int TEST_BLOOD_PRESSURE_ING = 400;
    public static final int TEST_BLOOD_PRESSURE_SUCCESS = 401;
    public static int UI_CODE = 0;
    public static boolean isBackUpComplete = false;
    public static boolean isBackUpCompleteSuccess = false;
    public static boolean isLoginComplete = false;
    public static boolean isLoginSuccess = false;
}
