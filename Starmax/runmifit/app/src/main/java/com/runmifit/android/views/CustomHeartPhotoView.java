package com.wade.fit.views;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.model.bean.HeartRateInterval;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.log.DebugLog;
import java.util.Calendar;

public class CustomHeartPhotoView extends View {
    private int MAX_HEART_RATE = 225;
    private int MIN_HEART_RATE = 30;
    private int bgColor;
    private boolean centerY;

    /* renamed from: h */
    private int f5231h;
    private float heartTotalHeight = ((float) ((this.MAX_HEART_RATE - this.MIN_HEART_RATE) + 30));
    private int leftPadding = 20;
    int max;

    /* renamed from: p */
    Paint f5232p;
    private Bitmap photoBitmap;
    private Paint photoPaint;
    private int rightPadding = 50;
    private String title1;
    private String title2;
    private String title3;
    private Paint titlePaint;
    private int value1;
    private int value2;
    private int value3;
    private Bitmap valueBitmap;
    private Paint valuePaint;

    /* renamed from: w */
    private int f5233w;

    public CustomHeartPhotoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CustomHeartPhotoView);
        int color = obtainStyledAttributes.getColor(4, getResources().getColor(R.color.white));
        int color2 = obtainStyledAttributes.getColor(2, getResources().getColor(R.color.details_title_color));
        float dimension = obtainStyledAttributes.getDimension(5, 10.0f);
        float dimension2 = obtainStyledAttributes.getDimension(3, 15.0f);
        this.bgColor = obtainStyledAttributes.getColor(0, getResources().getColor(R.color.white));
        this.centerY = obtainStyledAttributes.getBoolean(1, false);
        obtainStyledAttributes.recycle();
        this.valuePaint = new Paint(1);
        this.valuePaint.setTextAlign(Paint.Align.CENTER);
        setTextSize(dimension, this.valuePaint);
        this.valuePaint.setColor(color);
        this.photoPaint = new Paint(1);
        this.titlePaint = new Paint(1);
        this.titlePaint.setTextAlign(Paint.Align.CENTER);
        setTextSize(dimension2, this.titlePaint);
        this.titlePaint.setColor(color2);
        this.f5232p = new TextPaint(1);
        this.f5232p.setColor(getResources().getColor(R.color.hr_range_color));
        this.f5232p.setTextSize((float) ScreenUtil.dip2px(12.0f));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5233w = i;
        this.f5231h = i2;
        invalidate();
    }

    private void calculateY(int i) {
        boolean z = this.centerY;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(this.bgColor);
        this.photoBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.heart_rate_region);
        int width = this.photoBitmap.getWidth();
        int height = this.photoBitmap.getHeight();
        float f = (float) ((this.f5231h - height) / 2);
        canvas.drawBitmap(this.photoBitmap, (float) ((this.f5233w - width) / 2), f, this.photoPaint);
        this.valueBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.com_region);
        int width2 = this.valueBitmap.getWidth();
        int height2 = this.valueBitmap.getHeight();
        float f2 = (float) height;
        float f3 = this.heartTotalHeight;
        float f4 = (f2 - ((f2 / f3) * ((float) this.value1))) + f;
        float f5 = (f2 - ((f2 / f3) * ((float) this.value2))) + f;
        float f6 = (f2 - ((f2 / f3) * ((float) this.value3))) + f;
        this.title1 = getResources().getString(R.string.top_heart_title);
        this.title2 = getResources().getString(R.string.heart_title);
        this.title3 = getResources().getString(R.string.fat_heart_title);
        this.titlePaint.setTextAlign(Paint.Align.LEFT);
        double d = (double) this.max;
        Double.isNaN(d);
        float f7 = (f2 - ((f2 / this.heartTotalHeight) * ((float) ((int) (d * 0.85d))))) + f;
        canvas.drawText(this.title1, (float) (((this.f5233w + width) / 2) + (this.leftPadding * 4)), f7, this.titlePaint);
        double d2 = (double) this.max;
        Double.isNaN(d2);
        canvas.drawText(String.valueOf((int) (d2 * 0.85d)), (float) (((this.f5233w + width) / 2) + this.leftPadding), f7 + ((float) ScreenUtil.getTextHeight(this.titlePaint, this.title1)), this.f5232p);
        double d3 = (double) this.max;
        Double.isNaN(d3);
        float f8 = (f2 - ((f2 / this.heartTotalHeight) * ((float) ((int) (d3 * 0.7d))))) + f;
        canvas.drawText(this.title2, (float) (((this.f5233w + width) / 2) + (this.leftPadding * 4)), f8, this.titlePaint);
        this.f5232p.setColor(getResources().getColor(R.color.hr_range_color_2));
        double d4 = (double) this.max;
        Double.isNaN(d4);
        canvas.drawText(String.valueOf((int) (d4 * 0.7d)), (float) (((this.f5233w + width) / 2) + this.leftPadding), f8 + ((float) ScreenUtil.getTextHeight(this.titlePaint, this.title2)), this.f5232p);
        double d5 = (double) this.max;
        Double.isNaN(d5);
        float f9 = f + (f2 - ((f2 / this.heartTotalHeight) * ((float) ((int) (d5 * 0.5d)))));
        canvas.drawText(this.title3, (float) (((this.f5233w + width) / 2) + (this.leftPadding * 4)), f9, this.titlePaint);
        this.f5232p.setColor(getResources().getColor(R.color.hr_range_color_3));
        double d6 = (double) this.max;
        Double.isNaN(d6);
        canvas.drawText(String.valueOf((int) (d6 * 0.5d)), (float) (((this.f5233w + width) / 2) + this.leftPadding), f9 + ((float) ScreenUtil.getTextHeight(this.titlePaint, this.title3)), this.f5232p);
        float f10 = f5 - f4;
        float f11 = f6 - f5;
        float f12 = (float) height2;
        if (f11 < f12) {
            f5 -= f12 - f11;
            DebugLog.m6208e("②中间图片和下面出现重叠 + 中间图片和上面图片出现重叠");
        } else if (f10 < f12) {
            DebugLog.m6208e("①中间图片和上面图片出现重叠");
        }
        int i = width2 / 2;
        float f13 = (float) (height2 / 2);
        canvas.drawBitmap(this.valueBitmap, (float) (((this.f5233w * 5) / 12) - i), f5 - f13, this.valuePaint);
        int i2 = width2 / 10;
        float f14 = (float) (height2 / 10);
        canvas.drawText(this.value2 + "", (float) (((this.f5233w * 5) / 12) - i2), f5 + f14, this.valuePaint);
        canvas.drawBitmap(this.valueBitmap, (float) (((this.f5233w * 5) / 12) - i), f6 - f13, this.valuePaint);
        canvas.drawText(this.value3 + "", (float) (((this.f5233w * 5) / 12) - i2), f6 + f14, this.valuePaint);
    }

    public void setHeartRateValue(HeartRateInterval heartRateInterval) {
        int max2 = Math.max(heartRateInterval.getBurnFatThreshold(), Math.max(heartRateInterval.getLimintThreshold(), heartRateInterval.getAerobicThreshold()));
        int min = Math.min(heartRateInterval.getBurnFatThreshold(), Math.min(heartRateInterval.getLimintThreshold(), heartRateInterval.getAerobicThreshold()));
        this.value1 = max2;
        this.value2 = heartRateInterval.getAerobicThreshold();
        this.value3 = min;
        invalidate();
    }

    public void setHeartRateValue(int i, int i2, int i3) {
        this.value2 = i;
        this.value3 = i2;
        this.max = 220 - (Calendar.getInstance().get(1) - i3);
        invalidate();
    }

    public void setTextSize(float f, Paint paint) {
        Resources resources;
        Context context = getContext();
        if (context == null) {
            resources = Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        paint.setTextSize((float) ((int) TypedValue.applyDimension(2, f, resources.getDisplayMetrics())));
    }
}
