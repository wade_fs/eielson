package com.wade.fit.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.google.gson.Gson;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ViewUtil;
import com.wade.fit.util.log.LogUtil;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

public class SportDetailChart extends DetailBaseView {
    private int barColor;
    private int barNum = 96;
    private Paint barPaint;
    private int[] colors = {R.color.awake_sleep_color, R.color.light_sleep_color, R.color.deep_sleep_color};
    private Paint dataPaint;
    private float dataSpace;
    private float dataWidth;
    int dateType;
    private Gson gson = new Gson();

    /* renamed from: h */
    private int f5246h;
    List<HealthActivity> healthSleepList = new ArrayList();
    private List<String> lables = new ArrayList();
    private float leftX = 180.0f;
    private float[] lineHeight = new float[this.splitNum];
    int maxTime = 720;
    float scaleY;
    int selectedIndex = -1;
    private int splitLineColor;
    private int splitNum = 6;
    private int textColor;
    private float textSize = 28.0f;
    private Paint timePaint;
    private Paint topDataPaint;
    private int totalCount;
    private int type;

    /* renamed from: w */
    private int f5247w;
    private float xOffSet;
    private float xWidth;
    private String[] xtimes;
    private Paint yLinePaint;

    public SportDetailChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SportBarChart);
        this.textSize = obtainStyledAttributes.getDimension(0, 28.0f);
        this.textColor = obtainStyledAttributes.getColor(1, ContextCompat.getColor(context, R.color.color_666666to999999));
        this.barColor = obtainStyledAttributes.getColor(2, -1092544);
        this.splitLineColor = obtainStyledAttributes.getColor(8, -11514545);
        obtainStyledAttributes.recycle();
        initPaint();
    }

    private void initPaint() {
        this.barPaint = new Paint(1);
        this.barPaint.setColor(this.barColor);
        this.dataPaint = new Paint(1);
        this.yLinePaint = new Paint();
        this.yLinePaint.setAntiAlias(true);
        this.yLinePaint.setStyle(Paint.Style.STROKE);
        this.yLinePaint.setStrokeJoin(Paint.Join.ROUND);
        this.yLinePaint.setStrokeCap(Paint.Cap.ROUND);
        this.yLinePaint.setDither(true);
        this.yLinePaint.setStrokeWidth(TypedValue.applyDimension(1, 0.5f, getResources().getDisplayMetrics()));
        this.yLinePaint.setColor(this.splitLineColor);
        this.yLinePaint.setPathEffect(new DashPathEffect(new float[]{3.0f, 5.0f}, 0.0f));
        this.timePaint = new Paint(1);
        this.timePaint.setColor(this.textColor);
        this.timePaint.setTextSize(this.textSize);
        this.topDataPaint = new Paint(1);
        this.topDataPaint.setColor(getResources().getColor(R.color.color_fffffftofa114f));
        this.topDataPaint.setTextAlign(Paint.Align.CENTER);
        this.topDataPaint.setTextSize(this.textSize);
        setLayerType(1, null);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5247w = i;
        this.f5246h = i2;
        calculate();
    }

    private void calculate() {
        int i = 0;
        while (true) {
            int i2 = this.splitNum;
            if (i >= i2) {
                break;
            }
            this.lineHeight[i] = (float) ((this.f5246h / i2) * i);
            i++;
        }
        this.barPaint.setTextSize(this.textSize);
        this.xOffSet = ViewUtil.getTextRectWidth(this.barPaint, Constants.VIA_REPORT_TYPE_SHARE_TO_QQ);
        int i3 = this.totalCount;
        if (i3 != 0) {
            this.dataSpace = (((float) this.f5247w) - (this.xOffSet * 2.8f)) / ((float) i3);
            this.dataWidth = this.dataSpace * 0.5f;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(getResources().getColor(R.color.item_color));
        drawXLine(canvas);
        drawX(canvas);
    }

    private void drawXLine(Canvas canvas) {
        for (int i = 0; i < this.splitNum; i++) {
            Path path = new Path();
            if (i != 0) {
                path.moveTo(this.xOffSet * 1.4f, this.lineHeight[i]);
                path.lineTo(((float) this.f5247w) - (this.xOffSet * 1.4f), this.lineHeight[i]);
                if (i == this.splitNum - 1) {
                    this.yLinePaint.setPathEffect(null);
                } else {
                    this.yLinePaint.setPathEffect(new DashPathEffect(new float[]{3.0f, 5.0f}, 0.0f));
                }
                canvas.drawPath(path, this.yLinePaint);
            }
        }
    }

    private void drawX(Canvas canvas) {
        List<HealthActivity> list = this.healthSleepList;
        if (list != null && !list.isEmpty() && !this.lables.isEmpty()) {
            this.dataPaint.setTextAlign(Paint.Align.CENTER);
            drawWeekMonthYear(canvas);
        }
    }

    /* access modifiers changed from: package-private */
    public void drawWeekMonthYear(Canvas canvas) {
        int i;
        Canvas canvas2 = canvas;
        if (this.healthSleepList != null && this.lables.size() == this.healthSleepList.size()) {
            for (int i2 = 0; i2 < this.healthSleepList.size(); i2++) {
                HealthActivity healthActivity = this.healthSleepList.get(i2);
                float f = this.dataWidth + (this.xOffSet * 1.4f) + (this.dataSpace * ((float) i2));
                int i3 = this.dateType;
                if (i3 == 0) {
                    i = healthActivity.getDistance();
                } else if (i3 == 1) {
                    i = healthActivity.getCalories();
                } else {
                    i = i3 == 2 ? healthActivity.getDurations() : 0;
                }
                float[] fArr = this.lineHeight;
                int i4 = this.splitNum;
                float f2 = fArr[i4 - 1];
                float f3 = f2 - (((((float) i) / ((float) this.maxTime)) * (f2 - ((float) (this.f5246h / i4)))) * 1.0f);
                float f4 = this.dataWidth;
                int saveLayer = canvas.saveLayer(f - (f4 / 2.0f), f3, f + (f4 / 2.0f), fArr[i4 - 1], null, 31);
                float f5 = this.dataWidth;
                RectF rectF = new RectF(f - (f5 / 2.0f), f3, (f5 / 2.0f) + f, this.lineHeight[this.splitNum - 1] + f5);
                this.dataPaint.setColor(getResources().getColor(this.colors[2]));
                this.dataPaint.setShader(new LinearGradient(rectF.left, rectF.top, rectF.bottom, rectF.right, AppApplication.getInstance().getResources().getColor(R.color.sport_detail_color_light), AppApplication.getInstance().getResources().getColor(R.color.sport_detail_color_deep), Shader.TileMode.REPEAT));
                canvas2.drawRect(rectF, this.dataPaint);
                canvas2.restoreToCount(saveLayer);
                this.timePaint.setTextAlign(Paint.Align.CENTER);
                if (this.type != 2) {
                    canvas2.drawText(this.lables.get(i2), f, this.lineHeight[this.splitNum - 1] + ViewUtil.getTextHeight(this.topDataPaint), this.timePaint);
                } else if (Integer.parseInt(this.lables.get(i2)) % 2 != 0) {
                    canvas2.drawText(this.lables.get(i2), f, this.lineHeight[this.splitNum - 1] + ViewUtil.getTextHeight(this.topDataPaint), this.timePaint);
                }
            }
        }
    }

    private void drawTouchValue(Canvas canvas, float f, int i, float f2) {
        Path path = new Path();
        path.moveTo(f2 - ((float) ScreenUtil.dip2px(4.0f)), f - ((float) ScreenUtil.dip2px(10.0f)));
        path.lineTo(((float) ScreenUtil.dip2px(4.0f)) + f2, f - ((float) ScreenUtil.dip2px(10.0f)));
        path.lineTo(f2, f - ((float) ScreenUtil.dip2px(4.0f)));
        path.close();
        canvas.drawPath(path, this.dataPaint);
        canvas.drawText((i / 60) + getResources().getString(R.string.unit_hour) + (i % 60) + getResources().getString(R.string.unit_minute), f2, f - ((float) ScreenUtil.dip2px(15.0f)), this.topDataPaint);
    }

    private void drawDayTouchValue(Canvas canvas, float f, float f2, String str, String str2) {
        Path path = new Path();
        path.moveTo(f2 - ((float) ScreenUtil.dip2px(4.0f)), f - ((float) ScreenUtil.dip2px(10.0f)));
        path.lineTo(((float) ScreenUtil.dip2px(4.0f)) + f2, f - ((float) ScreenUtil.dip2px(10.0f)));
        path.lineTo(f2, f - ((float) ScreenUtil.dip2px(4.0f)));
        path.close();
        canvas.drawPath(path, this.dataPaint);
        LogUtil.d("startX--" + f2);
        float f3 = this.leftX;
        if (f2 < f3) {
            canvas.drawText(str2, f3, f - ((float) ScreenUtil.dip2px(15.0f)), this.topDataPaint);
            canvas.drawText(str, this.leftX, f - ((float) ScreenUtil.dip2px(30.0f)), this.topDataPaint);
            return;
        }
        int i = this.f5247w;
        if (f2 > ((float) i) - f3) {
            canvas.drawText(str2, ((float) i) - f3, f - ((float) ScreenUtil.dip2px(15.0f)), this.topDataPaint);
            canvas.drawText(str, ((float) this.f5247w) - this.leftX, f - ((float) ScreenUtil.dip2px(30.0f)), this.topDataPaint);
            return;
        }
        canvas.drawText(str2, f2, f - ((float) ScreenUtil.dip2px(15.0f)), this.topDataPaint);
        canvas.drawText(str, f2, f - ((float) ScreenUtil.dip2px(30.0f)), this.topDataPaint);
    }

    public void setData(int i, List<HealthActivity> list, List<String> list2, int i2) {
        int i3;
        this.lables = list2;
        this.type = i;
        this.healthSleepList.clear();
        this.healthSleepList.addAll(list);
        this.totalCount = list.size();
        this.dateType = i2;
        int i4 = 0;
        for (int i5 = 0; i5 < list.size(); i5++) {
            HealthActivity healthActivity = list.get(i5);
            if (i2 == 0) {
                i3 = healthActivity.getDistance();
            } else if (i2 == 1) {
                i3 = healthActivity.getCalories();
            } else {
                i3 = i2 == 2 ? healthActivity.getDurations() : 0;
            }
            if (i4 < i3) {
                i4 = i3;
            }
        }
        this.maxTime = i4;
        calculate();
    }

    private void animateY(int i) {
        if (Build.VERSION.SDK_INT >= 11) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            ofFloat.setDuration((long) i);
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                /* class com.wade.fit.views.SportDetailChart.C27151 */

                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    SportDetailChart.this.scaleY = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                    SportDetailChart.this.invalidate();
                }
            });
            ofFloat.start();
        }
    }
}
