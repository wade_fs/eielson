package com.wade.fit.views.wheel;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.wade.fit.R;

public class WheelView extends View {
    private WheelAdapter adapter;
    int baseline;
    int centerX;
    int centerY;
    int itemHeight;
    int itemWidth;
    int lowerLimit;
    private boolean mCyclic;
    private Drawable mDividerBottom;
    private Drawable mDividerTop;
    private int mLineSpace;
    Paint mPaint;
    WheelScroller mScroller;
    private int mSelectedColor;
    private int mTextSize;
    private int mUnselectedColor;
    private int mVisibleItems;
    private boolean showLine;
    int upperLimit;

    public WheelView(Context context) {
        this(context, null);
    }

    public WheelView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mVisibleItems = 5;
        this.mLineSpace = 20;
        this.mTextSize = 24;
        this.adapter = null;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.WheelView);
        boolean z = obtainStyledAttributes.getBoolean(2, false);
        int i = obtainStyledAttributes.getInt(9, this.mVisibleItems);
        int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(7, this.mLineSpace);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(0, this.mTextSize);
        int color = obtainStyledAttributes.getColor(8, 0);
        int color2 = obtainStyledAttributes.getColor(6, 0);
        this.showLine = obtainStyledAttributes.getBoolean(4, true);
        if (this.showLine) {
            this.mDividerTop = obtainStyledAttributes.getDrawable(3);
            this.mDividerBottom = obtainStyledAttributes.getDrawable(3);
            if (this.mDividerTop == null) {
                this.mDividerTop = getResources().getDrawable(R.drawable.wheelview_divider);
                this.mDividerBottom = getResources().getDrawable(R.drawable.wheelview_divider);
            }
        }
        obtainStyledAttributes.recycle();
        this.mPaint = new TextPaint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setTextAlign(Paint.Align.CENTER);
        this.mScroller = new WheelScroller(context, this);
        setCyclic(z);
        setVisibleItems(i);
        setLineSpace(dimensionPixelOffset);
        setTextSize(dimensionPixelSize);
        setSelectedColor(color);
        setUnselectedColor(color2);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size2 = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 && mode2 == 1073741824) {
            setMeasuredDimension(size, size2);
        } else if (mode == 1073741824) {
            setMeasuredDimension(size, getPrefHeight());
        } else if (mode2 == 1073741824) {
            setMeasuredDimension(getPrefWidth(), size2);
        } else {
            setMeasuredDimension(getPrefWidth(), getPrefHeight());
        }
        this.centerX = ((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()) / 2;
        this.centerY = ((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom()) / 2;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (this.adapter != null) {
            int i5 = this.centerY;
            int i6 = this.itemHeight;
            this.upperLimit = i5 - (i6 / 2);
            this.lowerLimit = i5 + (i6 / 2);
            Drawable drawable = this.mDividerTop;
            if (drawable != null) {
                this.mDividerTop.setBounds(getPaddingLeft(), this.upperLimit, getWidth() - getPaddingRight(), this.upperLimit + drawable.getIntrinsicHeight());
            }
            Drawable drawable2 = this.mDividerBottom;
            if (drawable2 != null) {
                this.mDividerBottom.setBounds(getPaddingLeft(), this.lowerLimit - drawable2.getIntrinsicHeight(), getWidth() - getPaddingRight(), this.lowerLimit);
            }
        }
    }

    public int getPrefWidth() {
        return ((int) (((float) this.itemWidth) + (((float) this.mTextSize) * 0.5f))) + getPaddingLeft() + getPaddingRight();
    }

    public int getPrefHeight() {
        return (this.itemHeight * this.mVisibleItems) + getPaddingTop() + getPaddingBottom();
    }

    /* access modifiers changed from: package-private */
    public void measureItemSize() {
        if (this.adapter != null) {
            int i = 0;
            for (int i2 = 0; i2 < this.adapter.getItemsCount(); i2++) {
                i = Math.max(i, Math.round(Layout.getDesiredWidth(this.adapter.getItem(i2), (TextPaint) this.mPaint)));
            }
            this.itemWidth = i;
        }
        int round = Math.round((float) (this.mPaint.getFontMetricsInt(null) + (this.mLineSpace * 2)));
        if (this.itemHeight != round) {
            this.itemHeight = round;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c A[LOOP:0: B:10:0x002a->B:11:0x002c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r5) {
        /*
            r4 = this;
            com.wade.fit.views.wheel.WheelAdapter r0 = r4.adapter
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            com.wade.fit.views.wheel.WheelScroller r0 = r4.mScroller
            int r0 = r0.getItemIndex()
            com.wade.fit.views.wheel.WheelScroller r1 = r4.mScroller
            int r1 = r1.getItemOffset()
            int r2 = r4.mVisibleItems
            int r2 = r2 + 1
            int r2 = r2 / 2
            if (r1 >= 0) goto L_0x001f
            int r3 = r0 - r2
            int r3 = r3 + -1
        L_0x001d:
            int r0 = r0 + r2
            goto L_0x002a
        L_0x001f:
            if (r1 <= 0) goto L_0x0027
            int r3 = r0 - r2
            int r0 = r0 + r2
            int r0 = r0 + 1
            goto L_0x002a
        L_0x0027:
            int r3 = r0 - r2
            goto L_0x001d
        L_0x002a:
            if (r3 >= r0) goto L_0x0032
            r4.drawItem(r5, r3, r1)
            int r3 = r3 + 1
            goto L_0x002a
        L_0x0032:
            android.graphics.drawable.Drawable r0 = r4.mDividerTop
            if (r0 == 0) goto L_0x0039
            r0.draw(r5)
        L_0x0039:
            android.graphics.drawable.Drawable r0 = r4.mDividerBottom
            if (r0 == 0) goto L_0x0040
            r0.draw(r5)
        L_0x0040:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.wheel.WheelView.onDraw(android.graphics.Canvas):void");
    }

    /* access modifiers changed from: protected */
    public void drawItem(Canvas canvas, int i, int i2) {
        CharSequence charSequence = getCharSequence(i);
        if (charSequence != null) {
            int itemIndex = ((i - this.mScroller.getItemIndex()) * this.itemHeight) - i2;
            int paddingLeft = getPaddingLeft();
            int width = getWidth() - getPaddingRight();
            int paddingTop = getPaddingTop();
            int height = getHeight() - getPaddingBottom();
            if (Math.abs(itemIndex) <= 0) {
                this.mPaint.setColor(this.mSelectedColor);
                canvas.save();
                canvas.clipRect(paddingLeft, this.upperLimit, width, this.lowerLimit);
                canvas.drawText(charSequence, 0, charSequence.length(), (float) this.centerX, (float) ((this.centerY + itemIndex) - this.baseline), this.mPaint);
                canvas.restore();
            } else if (itemIndex > 0 && itemIndex < this.itemHeight) {
                this.mPaint.setColor(this.mSelectedColor);
                canvas.save();
                canvas.clipRect(paddingLeft, this.upperLimit, width, this.lowerLimit);
                canvas.drawText(charSequence, 0, charSequence.length(), (float) this.centerX, (float) ((this.centerY + itemIndex) - this.baseline), this.mPaint);
                canvas.restore();
                this.mPaint.setColor(this.mUnselectedColor);
                canvas.save();
                canvas.clipRect(paddingLeft, this.lowerLimit, width, height);
                canvas.drawText(charSequence, 0, charSequence.length(), (float) this.centerX, (float) ((this.centerY + itemIndex) - this.baseline), this.mPaint);
                canvas.restore();
            } else if (itemIndex >= 0 || itemIndex <= (-this.itemHeight)) {
                this.mPaint.setColor(this.mUnselectedColor);
                canvas.save();
                canvas.clipRect(paddingLeft, paddingTop, width, height);
                canvas.drawText(charSequence, 0, charSequence.length(), (float) this.centerX, (float) ((this.centerY + itemIndex) - this.baseline), this.mPaint);
                canvas.restore();
            } else {
                this.mPaint.setColor(this.mSelectedColor);
                canvas.save();
                canvas.clipRect(paddingLeft, this.upperLimit, width, this.lowerLimit);
                canvas.drawText(charSequence, 0, charSequence.length(), (float) this.centerX, (float) ((this.centerY + itemIndex) - this.baseline), this.mPaint);
                canvas.restore();
                this.mPaint.setColor(this.mUnselectedColor);
                canvas.save();
                canvas.clipRect(paddingLeft, paddingTop, width, this.upperLimit);
                canvas.drawText(charSequence, 0, charSequence.length(), (float) this.centerX, (float) ((this.centerY + itemIndex) - this.baseline), this.mPaint);
                canvas.restore();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public CharSequence getCharSequence(int i) {
        WheelAdapter wheelAdapter = this.adapter;
        if (wheelAdapter == null) {
            return "";
        }
        int itemsCount = wheelAdapter.getItemsCount();
        if (itemsCount == 0) {
            return null;
        }
        if (isCyclic()) {
            int i2 = i % itemsCount;
            if (i2 < 0) {
                i2 += itemsCount;
            }
            return this.adapter.getItem(i2);
        } else if (i < 0 || i >= itemsCount) {
            return null;
        } else {
            return this.adapter.getItem(i);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.mScroller.onTouchEvent(motionEvent);
    }

    public void computeScroll() {
        this.mScroller.computeScroll();
    }

    public boolean isCyclic() {
        return this.mCyclic;
    }

    public void setCyclic(boolean z) {
        this.mCyclic = z;
    }

    public int getVisibleItems() {
        return this.mVisibleItems;
    }

    public void setVisibleItems(int i) {
        this.mVisibleItems = Math.abs(((i / 2) * 2) + 1);
        this.mScroller.reset();
        requestLayout();
        invalidate();
    }

    public int getLineSpace() {
        return this.mLineSpace;
    }

    public void setLineSpace(int i) {
        this.mLineSpace = i;
        this.mScroller.reset();
        measureItemSize();
        requestLayout();
        invalidate();
    }

    public int getTextSize() {
        return this.mTextSize;
    }

    public void setTextSize(int i) {
        this.mTextSize = i;
        this.mPaint.setTextSize((float) i);
        Paint.FontMetrics fontMetrics = this.mPaint.getFontMetrics();
        this.baseline = (int) ((fontMetrics.top + fontMetrics.bottom) / 2.0f);
        this.mScroller.reset();
        measureItemSize();
        requestLayout();
        invalidate();
    }

    public int getSelectedColor() {
        return this.mSelectedColor;
    }

    public void setSelectedColor(int i) {
        this.mSelectedColor = i;
        invalidate();
    }

    public int getUnselectedColor() {
        return this.mUnselectedColor;
    }

    public void setUnselectedColor(int i) {
        this.mUnselectedColor = i;
        invalidate();
    }

    public int getItemSize() {
        WheelAdapter wheelAdapter = this.adapter;
        if (wheelAdapter == null) {
            return 0;
        }
        return wheelAdapter.getItemsCount();
    }

    public int getCurrentItem() {
        return this.mScroller.getCurrentIndex();
    }

    public void setCurrentItem(int i) {
        setCurrentItem(i, false);
    }

    public void setCurrentItem(int i, boolean z) {
        WheelAdapter wheelAdapter = this.adapter;
        if (wheelAdapter != null && wheelAdapter.getItemsCount() != 0) {
            this.mScroller.setCurrentIndex(i, z);
        }
    }

    public WheelAdapter getAdapter() {
        return this.adapter;
    }

    public void setAdapter(WheelAdapter wheelAdapter) {
        this.adapter = wheelAdapter;
        this.mScroller.reset();
        measureItemSize();
        requestLayout();
        invalidate();
    }

    public void addChangingListener(OnWheelChangedListener onWheelChangedListener) {
        this.mScroller.onWheelChangedListener = onWheelChangedListener;
    }
}
