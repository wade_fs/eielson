package com.wade.fit.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.wade.fit.app.AppApplication;

public abstract class SharePreferenceUtils {
    public static final String DEVICE_SPORT_SET = "DEVICE_SPORT_SET";
    public static String DISTURB_TIME_DISATACE = "Disturb_TIME_DISATACE";
    public static final String FIRST_SYNC = "FIRST_SYNC";
    public static String HEART_TIME_DISTANCE = "Heart_TIME_DISATACE";
    public static String SLEEP_TIME_DISTANCE = "Sleep_TIME_DISATACE";
    public static final String SUPPORT_ALARM_NUM = "SUPPORT_ALARM_NUM";
    public static final String WEEK_START_INDEX = "WEEK_START_INDEX";
    public static String WRIST_TIME_DISTANCE = "Wrist_TIME_DISATACE";
    private static SharedPreferences.Editor mEditor = null;
    private static String mName = "veryfit2.2_multi_new_app";

    public static void setSharePreferenceName(String str) {
        mName = str;
    }

    public static void putString(Context context, String str, String str2) {
        put(context, str, str2);
    }

    public static void putString(String str, String str2) {
        put(AppApplication.getInstance().getApplicationContext(), str, str2);
    }

    public static String getString(Context context, String str) {
        return (String) get(context, str, "");
    }

    public static String getString(Context context, String str, String str2) {
        return (String) get(context, str, str2);
    }

    public static void putInt(Context context, String str, int i) {
        put(context, str, Integer.valueOf(i));
    }

    public static int getInt(Context context, String str) {
        return ((Integer) get(context, str, 0)).intValue();
    }

    public static int getInt(String str) {
        return ((Integer) get(AppApplication.getInstance().getApplicationContext(), str, 0)).intValue();
    }

    public static int getInt(Context context, String str, int i) {
        return ((Integer) get(context, str, Integer.valueOf(i))).intValue();
    }

    public static void putBool(Context context, String str, Boolean bool) {
        put(context, str, bool);
    }

    public static void putBool(String str, Boolean bool) {
        put(AppApplication.getInstance().getApplicationContext(), str, bool);
    }

    public static boolean getBool(Context context, String str) {
        return ((Boolean) get(context, str, false)).booleanValue();
    }

    public static boolean getBool(Context context, String str, Boolean bool) {
        return ((Boolean) get(context, str, bool)).booleanValue();
    }

    public static void putFloat(Context context, String str, Float f) {
        put(context, str, f);
    }

    public static float getFloat(Context context, String str) {
        return ((Float) get(context, str, Float.valueOf(0.0f))).floatValue();
    }

    public static float getFloat(Context context, String str, Float f) {
        return ((Float) get(context, str, f)).floatValue();
    }

    public static void putLong(Context context, String str, Long l) {
        put(context, str, l);
    }

    public static long getLong(Context context, String str) {
        return ((Long) get(context, str, 0L)).longValue();
    }

    public static long getLong(Context context, String str, Long l) {
        return ((Long) get(context, str, l)).longValue();
    }

    public static int getWeekStartIndex(String str, int i) {
        return ((Integer) get(AppApplication.getInstance().getApplicationContext(), str, Integer.valueOf(i))).intValue();
    }

    public static Object getNoticeOff(Context context, String str, Object obj) {
        return get(context, str, obj);
    }

    public static void setNoticeOff(Context context, String str, Object obj) {
        put(context, str, obj);
    }

    public static boolean getToggleSwitchState(Context context, String str, boolean z) {
        return ((Boolean) get(context, str, Boolean.valueOf(z))).booleanValue();
    }

    public static boolean getFirstSportSet(Context context, String str, boolean z) {
        return ((Boolean) get(context, str, Boolean.valueOf(z))).booleanValue();
    }

    public static void remove(Context context, String str) {
        if (mEditor == null) {
            setEditor(context);
        }
        mEditor.remove(str);
        mEditor.commit();
    }

    public static void clear(Context context) {
        if (mEditor == null) {
            setEditor(context);
        }
        mEditor.clear();
        mEditor.commit();
    }

    public static boolean contains(Context context, String str) {
        return context.getSharedPreferences(mName, 0).contains(str);
    }

    private static void setEditor(Context context) {
        mEditor = context.getSharedPreferences(mName, 0).edit();
    }

    public static Object get(Context context, String str, Object obj) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(mName, 0);
        if (obj instanceof String) {
            return sharedPreferences.getString(str, (String) obj);
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(sharedPreferences.getInt(str, ((Integer) obj).intValue()));
        }
        if (obj instanceof Boolean) {
            return Boolean.valueOf(sharedPreferences.getBoolean(str, ((Boolean) obj).booleanValue()));
        }
        if (obj instanceof Float) {
            return Float.valueOf(sharedPreferences.getFloat(str, ((Float) obj).floatValue()));
        }
        if (obj instanceof Long) {
            return Long.valueOf(sharedPreferences.getLong(str, ((Long) obj).longValue()));
        }
        return sharedPreferences.getString(str, null);
    }

    public static void put(Context context, String str, Object obj) {
        if (mEditor == null) {
            setEditor(context);
        }
        if (obj instanceof String) {
            mEditor.putString(str, (String) obj);
        } else if (obj instanceof Integer) {
            mEditor.putInt(str, ((Integer) obj).intValue());
        } else if (obj instanceof Boolean) {
            mEditor.putBoolean(str, ((Boolean) obj).booleanValue());
        } else if (obj instanceof Float) {
            mEditor.putFloat(str, ((Float) obj).floatValue());
        } else if (obj instanceof Long) {
            mEditor.putLong(str, ((Long) obj).longValue());
        } else {
            mEditor.putString(str, obj.toString());
        }
        mEditor.commit();
    }
}
