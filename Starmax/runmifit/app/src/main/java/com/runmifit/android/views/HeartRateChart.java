package com.wade.fit.views;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import com.wade.fit.R;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ViewUtil;
import com.wade.fit.util.log.LogUtil;
import com.tencent.bugly.beta.tinker.TinkerReport;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

public class HeartRateChart extends View implements View.OnTouchListener {
    private static final String TAG = "Chart";
    private int MAX_VALUE;
    private int MIN_VALUE;
    private int aerobicThreshold;
    private Paint alphaPaint;
    float animateValue;
    private int barColor;
    private int barNum;
    private Paint barPaint;
    private float barWid;
    private float barWidth;
    private float bottomY;
    private int burnFatThreshold;
    private float chartWidth;
    private float chartWidthSpan;
    private TouchOrClickListener clickListener;

    /* renamed from: dY */
    private int f5236dY;
    private List<HealthHeartRateItem> datas;
    private int defaultDuration;
    private boolean fillAreaHasAnim;
    private Paint fillPaint;

    /* renamed from: h */
    private int f5237h;
    private boolean hasNoDataTip;
    private boolean hasTime;
    private boolean hasYvalue;
    boolean isCubic;
    private boolean isFillArea;
    private boolean isInit;
    private boolean isMeasure;
    private boolean isMove;
    private boolean isOneDay;
    /* access modifiers changed from: private */
    public boolean isOver;
    private TextPaint labelXPaint;
    private TextPaint labelYPaint;
    private float labelYWidth;
    private int limintThreshold;
    private float[] lineHeight;
    private Paint linePaint;
    /* access modifiers changed from: private */
    public float mAnimatorValue;
    private PathMeasure mPathMeasure;
    private ValueAnimator.AnimatorUpdateListener mUpdateListener;
    private Paint maxYSeparatePaint;
    private String noDataValue;

    /* renamed from: p */
    Paint f5238p;
    private Path path;
    private Path path2;
    private boolean playAnim;
    private float pointHeight;
    private Paint pointPaint;
    private Point[] points;
    RectF rect;
    private int[] scaleY;
    private int selPosition;
    private RectF selectRect;
    private int splitLineColor;
    private int splitNum;
    private int textColor;
    private float textSize;
    DetailTimeType timeType;
    private Paint touchPaint;
    private Paint touchPaint2;
    float touchX;
    private int touchX1;
    private int touchX2;
    float touchY;
    private ValueAnimator valueAnimator;

    /* renamed from: w */
    private int f5239w;
    private float xOffSet;
    private Paint xSeparatePaint;
    private List<String> xtimes;
    private int yValueColor;

    public interface TouchOrClickListener {
        void doClick();
    }

    public HeartRateChart(Context context) {
        this(context, null);
    }

    public HeartRateChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public HeartRateChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.splitNum = 6;
        this.lineHeight = new float[this.splitNum];
        this.barNum = 96;
        this.MIN_VALUE = 50;
        this.MAX_VALUE = 200;
        this.f5236dY = 50;
        int i2 = this.MIN_VALUE;
        int i3 = this.f5236dY;
        this.scaleY = new int[]{(i3 * 2) + i2, i3 + i2, i2};
        this.linePaint = new Paint();
        this.fillPaint = new Paint();
        this.xSeparatePaint = new Paint();
        this.maxYSeparatePaint = new Paint();
        this.pointPaint = new Paint();
        this.alphaPaint = new Paint();
        this.labelXPaint = new TextPaint();
        this.labelYPaint = new TextPaint();
        this.path = null;
        this.path2 = null;
        this.datas = new ArrayList();
        this.playAnim = true;
        this.defaultDuration = TinkerReport.KEY_APPLIED_PACKAGE_CHECK_SIGNATURE;
        this.touchX = -1.0f;
        this.touchY = -1.0f;
        this.selPosition = -1;
        this.isOver = false;
        this.isFillArea = true;
        this.fillAreaHasAnim = true;
        this.isMeasure = false;
        this.isInit = false;
        this.isOneDay = true;
        this.rect = new RectF();
        this.selectRect = new RectF();
        this.isCubic = true;
        this.f5238p = new Paint();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SportBarChart);
        this.textSize = obtainStyledAttributes.getDimension(0, 28.0f);
        this.textColor = obtainStyledAttributes.getColor(1, -6710887);
        this.barColor = obtainStyledAttributes.getColor(2, -1092544);
        this.splitLineColor = obtainStyledAttributes.getColor(8, -10471118);
        this.yValueColor = obtainStyledAttributes.getColor(9, -10471118);
        this.hasNoDataTip = obtainStyledAttributes.getBoolean(6, false);
        this.noDataValue = obtainStyledAttributes.getString(7);
        this.hasTime = obtainStyledAttributes.getBoolean(4, true);
        this.hasYvalue = obtainStyledAttributes.getBoolean(5, true);
        obtainStyledAttributes.recycle();
        init();
        if (Build.VERSION.SDK_INT < 20) {
            setLayerType(1, null);
        }
    }

    private void init() {
        setOnTouchListener(this);
        this.barPaint = new Paint(1);
        this.barPaint.setColor(this.barColor);
        this.linePaint.setAntiAlias(true);
        this.linePaint.setStyle(Paint.Style.STROKE);
        this.linePaint.setStrokeJoin(Paint.Join.ROUND);
        this.linePaint.setStrokeCap(Paint.Cap.ROUND);
        this.linePaint.setDither(true);
        this.linePaint.setShader(null);
        this.linePaint.setStrokeWidth(TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics()));
        this.linePaint.setColor(getResources().getColor(R.color.heartRateChart_lineColor));
        this.fillPaint.setAntiAlias(true);
        this.fillPaint.setStyle(Paint.Style.FILL);
        this.fillPaint.setStrokeJoin(Paint.Join.ROUND);
        this.fillPaint.setStrokeCap(Paint.Cap.ROUND);
        this.fillPaint.setDither(true);
        this.fillPaint.setStrokeWidth(TypedValue.applyDimension(1, 3.0f, getResources().getDisplayMetrics()));
        this.fillPaint.setColor(getResources().getColor(R.color.heartRateChart_fillColor));
        this.pointPaint.setAntiAlias(true);
        this.pointPaint.setStyle(Paint.Style.FILL);
        this.pointPaint.setStrokeJoin(Paint.Join.ROUND);
        this.pointPaint.setStrokeCap(Paint.Cap.ROUND);
        this.pointPaint.setDither(true);
        this.pointPaint.setStrokeWidth(TypedValue.applyDimension(1, 3.0f, getResources().getDisplayMetrics()));
        this.pointPaint.setColor(getResources().getColor(R.color.heartRateChart_pointColor));
        this.alphaPaint = new Paint();
        this.alphaPaint.setAntiAlias(true);
        this.alphaPaint.setStyle(Paint.Style.FILL);
        this.alphaPaint.setStrokeWidth(TypedValue.applyDimension(1, 3.0f, getResources().getDisplayMetrics()));
        this.alphaPaint.setColor(getResources().getColor(R.color.heartRateChart_alphaColor));
        this.alphaPaint.setDither(true);
        this.alphaPaint.setStrokeCap(Paint.Cap.ROUND);
        this.xSeparatePaint.setAntiAlias(true);
        this.xSeparatePaint.setStyle(Paint.Style.STROKE);
        this.xSeparatePaint.setStrokeJoin(Paint.Join.ROUND);
        this.xSeparatePaint.setStrokeCap(Paint.Cap.ROUND);
        this.xSeparatePaint.setDither(true);
        this.xSeparatePaint.setStrokeWidth(TypedValue.applyDimension(1, 0.5f, getResources().getDisplayMetrics()));
        this.xSeparatePaint.setColor(this.splitLineColor);
        DashPathEffect dashPathEffect = new DashPathEffect(new float[]{3.0f, 5.0f}, 0.0f);
        this.xSeparatePaint.setPathEffect(dashPathEffect);
        this.maxYSeparatePaint.setAntiAlias(true);
        this.maxYSeparatePaint.setStyle(Paint.Style.STROKE);
        this.maxYSeparatePaint.setStrokeJoin(Paint.Join.ROUND);
        this.maxYSeparatePaint.setStrokeCap(Paint.Cap.ROUND);
        this.maxYSeparatePaint.setDither(true);
        this.maxYSeparatePaint.setStrokeWidth(TypedValue.applyDimension(1, 0.5f, getResources().getDisplayMetrics()));
        this.maxYSeparatePaint.setColor(getResources().getColor(R.color.heartRateChart_maxYColor));
        this.maxYSeparatePaint.setPathEffect(dashPathEffect);
        this.labelXPaint.setAntiAlias(true);
        this.labelXPaint.setColor(this.textColor);
        this.labelXPaint.setTextAlign(Paint.Align.CENTER);
        this.labelXPaint.setTextSize(this.textSize);
        this.labelYPaint.setAntiAlias(true);
        this.labelYPaint.setColor(this.yValueColor);
        this.labelYPaint.setTextAlign(Paint.Align.LEFT);
        this.labelYPaint.setTextSize((float) ScreenUtil.dp2px(8.0f));
        this.pointHeight = ViewUtil.getTextRectWidth(this.labelYPaint, "100");
        this.labelYWidth = ViewUtil.getTextRectWidth(this.labelYPaint, "100");
        this.touchPaint = new Paint(1);
        this.touchPaint2 = new Paint(1);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5239w = i;
        this.f5237h = i2;
        calculate();
    }

    private void calculate() {
        int i = 0;
        while (true) {
            int i2 = this.splitNum;
            if (i < i2) {
                this.lineHeight[i] = ((((float) this.f5237h) - this.pointHeight) / ((float) i2)) * ((float) i);
                i++;
            } else {
                this.barPaint.setTextSize(this.textSize);
                this.xOffSet = ViewUtil.getTextRectWidth(this.barPaint, Constants.VIA_REPORT_TYPE_SHARE_TO_QQ);
                this.chartWidth = (((float) this.f5239w) - (this.xOffSet * 2.8f)) - this.labelYWidth;
                this.barWidth = this.chartWidth / ((float) this.barNum);
                this.barWid = this.barWidth * 0.5f;
                this.isMeasure = true;
                initView();
                return;
            }
        }
    }

    private void reset() {
        this.datas.clear();
        this.isMove = false;
        this.isOver = false;
        this.selPosition = -1;
        this.fillPaint.setShader(null);
        this.isInit = false;
    }

    private void updateScaleYValue() {
        if (this.MAX_VALUE <= 0) {
            this.MAX_VALUE = 200;
        }
        int i = this.MIN_VALUE;
        int i2 = this.f5236dY;
        this.burnFatThreshold = i + i2;
        this.aerobicThreshold = (i2 * 2) + i;
        this.limintThreshold = (i2 * 3) + i;
        this.scaleY = new int[]{this.limintThreshold, this.aerobicThreshold, this.burnFatThreshold, i};
    }

    private void filterData(List<HealthHeartRateItem> list) {
        if (list != null && list.size() > 0) {
            int i = 0;
            for (int i2 = 0; i2 < list.size(); i2++) {
                HealthHeartRateItem healthHeartRateItem = list.get(i2);
                if (i2 == 0) {
                    i = healthHeartRateItem.getOffsetMinute();
                } else {
                    i += healthHeartRateItem.getOffsetMinute();
                }
                healthHeartRateItem.setOffsetMinute(i);
                this.datas.add(healthHeartRateItem);
            }
        }
        ArrayList arrayList = new ArrayList();
        for (int i3 = 0; i3 < this.datas.size(); i3++) {
            if (this.datas.get(i3).getHeartRaveValue() > this.MIN_VALUE && this.datas.get(i3).getHeartRaveValue() < this.MAX_VALUE) {
                arrayList.add(this.datas.get(i3));
            }
        }
        this.datas.clear();
        this.datas.addAll(arrayList);
        LogUtil.d("date:" + this.datas.size());
    }

    public void setDatas(List<HealthHeartRateItem> list, HealthHeartRate healthHeartRate, boolean z, List<String> list2) {
        this.isOneDay = true;
        this.xtimes = list2;
        this.MAX_VALUE = 200;
        this.MIN_VALUE = 25;
        this.f5236dY = 25;
        int i = this.MIN_VALUE;
        int i2 = this.f5236dY;
        this.burnFatThreshold = i + i2;
        this.aerobicThreshold = (i2 * 2) + i;
        this.limintThreshold = (i2 * 3) + i;
        this.scaleY = new int[]{this.limintThreshold, this.aerobicThreshold, this.burnFatThreshold, i};
        reset();
        filterData(list);
        initView();
    }

    public void setType(DetailTimeType detailTimeType) {
        this.timeType = detailTimeType;
    }

    public void setDatas(List<HealthHeartRateItem> list, int i, int i2, int i3, int i4) {
        this.isOneDay = false;
        this.MAX_VALUE = i;
        reset();
        updateScaleYValue();
        filterData(list);
        initView();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private void initView() {
        if (this.isMeasure && !this.isInit) {
            this.isInit = true;
            if (this.isOneDay) {
                this.chartWidthSpan = this.chartWidth / 1440.0f;
            } else if (this.datas.size() > 0) {
                float f = this.chartWidth;
                List<HealthHeartRateItem> list = this.datas;
                this.chartWidthSpan = f / ((float) list.get(list.size() - 1).getOffsetMinute());
            } else {
                this.chartWidthSpan = this.chartWidth / 1440.0f;
            }
            LogUtil.d("date:" + this.datas.size());
            this.points = new Point[this.datas.size()];
            this.bottomY = (this.lineHeight[this.splitNum - 1] + this.pointHeight) - this.barWid;
            int i = 0;
            for (int i2 = 0; i2 < this.datas.size(); i2++) {
                this.points[i2] = new Point((int) ((((float) this.datas.get(i2).getOffsetMinute()) * this.chartWidthSpan) + (this.xOffSet * 1.4f) + this.labelYWidth), (int) (this.bottomY - ((float) getYValue(this.datas.get(i2).getHeartRaveValue()))));
                if (i < this.datas.get(i2).getHeartRaveValue()) {
                    i = this.datas.get(i2).getHeartRaveValue();
                }
            }
            initPath();
            initAnimator();
            if (this.playAnim) {
                this.valueAnimator.start();
            }
            this.fillPaint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) getHeight(), this.fillPaint.getColor(), 16777215 & this.fillPaint.getColor(), Shader.TileMode.MIRROR));
            invalidate();
        }
    }

    private int getYValue(int i) {
        float f;
        float f2;
        float f3;
        int i2 = this.burnFatThreshold;
        if (i < i2) {
            float[] fArr = this.lineHeight;
            f = ((fArr[1] / ((float) i2)) * ((float) (i - this.MIN_VALUE))) + fArr[1];
        } else {
            int i3 = this.aerobicThreshold;
            if (i < i3) {
                float[] fArr2 = this.lineHeight;
                f2 = (fArr2[1] / ((float) (i3 - i2))) * ((float) (i - i2));
                f3 = fArr2[2];
            } else {
                int i4 = this.limintThreshold;
                if (i < i4) {
                    float[] fArr3 = this.lineHeight;
                    f2 = (fArr3[1] / ((float) (i4 - i3))) * ((float) (i - i3));
                    f3 = fArr3[3];
                } else {
                    float[] fArr4 = this.lineHeight;
                    f2 = (fArr4[1] / ((float) (this.MAX_VALUE - i4))) * ((float) (i - i4));
                    f3 = fArr4[4];
                }
            }
            f = f2 + f3;
        }
        return (int) f;
    }

    private void initPath() {
        this.path = new Path();
        this.path2 = new Path();
        if (this.points.length > 0) {
            Point point = new Point();
            Point[] pointArr = this.points;
            int i = pointArr[pointArr.length - 1].x;
            Point[] pointArr2 = this.points;
            point.set(i, pointArr2[pointArr2.length - 1].y);
            this.path.moveTo((float) point.x, (float) point.y);
            this.path2.moveTo((float) point.x, (float) point.y);
            int length = this.points.length - 2;
            while (length >= 0) {
                Point point2 = new Point();
                point2.set(this.points[length].x, this.points[length].y);
                if (this.isCubic) {
                    int i2 = (point.x + point2.x) / 2;
                    Point point3 = new Point(i2, point.y);
                    Point point4 = new Point(i2, point2.y);
                    this.path.cubicTo((float) point3.x, (float) point3.y, (float) point4.x, (float) point4.y, (float) point2.x, (float) point2.y);
                    this.path2.cubicTo((float) point3.x, (float) point3.y, (float) point4.x, (float) point4.y, (float) point2.x, (float) point2.y);
                } else {
                    this.path.lineTo((float) point2.x, (float) point2.y);
                    this.path2.lineTo((float) point2.x, (float) point2.y);
                }
                length--;
                point = point2;
            }
        }
        this.mPathMeasure = new PathMeasure(this.path, false);
    }

    private void initAnimator() {
        this.mUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
            /* class com.wade.fit.views.HeartRateChart.C27081 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = HeartRateChart.this.mAnimatorValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                HeartRateChart.this.invalidate();
            }
        };
        this.valueAnimator = ValueAnimator.ofFloat(1.0f, 0.0f).setDuration((long) this.defaultDuration);
        this.valueAnimator.addUpdateListener(this.mUpdateListener);
        this.valueAnimator.addListener(new Animator.AnimatorListener() {
            /* class com.wade.fit.views.HeartRateChart.C27092 */

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                boolean unused = HeartRateChart.this.isOver = true;
                HeartRateChart.this.animateY();
                HeartRateChart.this.invalidate();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        List<HealthHeartRateItem> list = this.datas;
        if (list != null && list.size() != 0) {
            drawXLine(canvas);
            drawXValue(canvas);
            if (this.points.length == 1) {
                canvas.drawCircle((float) this.points[0].x, (float) this.points[0].y, TypedValue.applyDimension(1, 4.0f, getResources().getDisplayMetrics()) - 5.0f, this.pointPaint);
            } else {
                canvas.drawPath(this.path2, this.linePaint);
                this.path.lineTo((float) this.points[0].x, this.bottomY);
                this.path.lineTo((float) this.points[this.datas.size() - 1].x, this.bottomY);
                canvas.drawPath(this.path, this.fillPaint);
            }
            if (this.isOver || !this.playAnim) {
                drawTouch(canvas);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x02de  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void drawMaxHeight(android.graphics.Canvas r21) {
        /*
            r20 = this;
            r0 = r20
            r7 = r21
            java.util.List<com.wade.fit.greendao.bean.HealthHeartRateItem> r1 = r0.datas
            int r1 = r1.size()
            if (r1 != 0) goto L_0x000d
            return
        L_0x000d:
            int r1 = r0.selPosition
            if (r1 >= 0) goto L_0x0012
            return
        L_0x0012:
            android.graphics.Paint r1 = r0.f5238p
            r1.reset()
            android.graphics.Paint r1 = r0.f5238p
            android.content.res.Resources r2 = r20.getResources()
            r3 = 2131100053(0x7f060195, float:1.7812477E38)
            int r2 = r2.getColor(r3)
            r1.setColor(r2)
            android.graphics.Paint r1 = r0.f5238p
            r2 = 1
            r1.setAntiAlias(r2)
            android.graphics.Paint r1 = r0.f5238p
            r3 = 1090519040(0x41000000, float:8.0)
            int r3 = com.wade.fit.util.ScreenUtil.dip2px(r3)
            float r3 = (float) r3
            r1.setTextSize(r3)
            android.graphics.Paint r1 = r0.f5238p
            android.graphics.Paint$Align r3 = android.graphics.Paint.Align.CENTER
            r1.setTextAlign(r3)
            java.util.List<com.wade.fit.greendao.bean.HealthHeartRateItem> r1 = r0.datas
            int r3 = r0.selPosition
            java.lang.Object r1 = r1.get(r3)
            com.wade.fit.greendao.bean.HealthHeartRateItem r1 = (com.wade.fit.greendao.bean.HealthHeartRateItem) r1
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r4 = r1.getHeartRaveValue()
            r3.append(r4)
            java.lang.String r4 = ""
            r3.append(r4)
            r3.toString()
            android.graphics.Point r3 = new android.graphics.Point
            r3.<init>()
            int r5 = r1.getOffsetMinute()
            float r5 = r0.minutToX(r5)
            int r5 = (int) r5
            float r6 = r0.bottomY
            int r1 = r1.getHeartRaveValue()
            int r1 = r0.getYValue(r1)
            float r1 = (float) r1
            float r6 = r6 - r1
            int r1 = (int) r6
            r3.set(r5, r1)
            r1 = 1082130432(0x40800000, float:4.0)
            android.content.res.Resources r5 = r20.getResources()
            android.util.DisplayMetrics r5 = r5.getDisplayMetrics()
            float r1 = android.util.TypedValue.applyDimension(r2, r1, r5)
            int r5 = r3.x
            float r5 = (float) r5
            int r6 = r3.y
            float r6 = (float) r6
            r8 = 1084227584(0x40a00000, float:5.0)
            float r9 = r1 / r8
            float r6 = r6 + r9
            float r10 = r1 - r8
            android.graphics.Paint r11 = r0.pointPaint
            r7.drawCircle(r5, r6, r10, r11)
            android.graphics.Paint r5 = r0.alphaPaint
            android.content.res.Resources r6 = r20.getResources()
            r10 = 2131099872(0x7f0600e0, float:1.781211E38)
            int r6 = r6.getColor(r10)
            r5.setColor(r6)
            int r5 = r3.x
            float r5 = (float) r5
            int r6 = r3.y
            float r6 = (float) r6
            float r6 = r6 + r9
            android.graphics.Paint r10 = r0.alphaPaint
            r7.drawCircle(r5, r6, r1, r10)
            int r5 = r3.x
            float r5 = (float) r5
            int r6 = r3.y
            float r6 = (float) r6
            float r6 = r6 + r9
            float r9 = r0.animateValue
            float r1 = r1 * r9
            android.graphics.Paint r9 = r0.alphaPaint
            r7.drawCircle(r5, r6, r1, r9)
            android.graphics.Paint r1 = r0.f5238p
            com.wade.fit.util.ViewUtil.getTextHeight(r1)
            int r1 = r3.x
            float r9 = (float) r1
            int r1 = r0.selPosition
            if (r1 < 0) goto L_0x02f6
            android.graphics.Paint r1 = r0.barPaint
            android.content.res.Resources r5 = r20.getResources()
            r6 = 2131100157(0x7f0601fd, float:1.7812688E38)
            int r5 = r5.getColor(r6)
            r1.setColor(r5)
            android.graphics.Paint r1 = r0.touchPaint
            android.content.res.Resources r5 = r20.getResources()
            r6 = 2131165530(0x7f07015a, float:1.794528E38)
            float r5 = r5.getDimension(r6)
            r1.setTextSize(r5)
            android.graphics.Paint r1 = r0.touchPaint2
            android.content.res.Resources r5 = r20.getResources()
            r6 = 2131165585(0x7f070191, float:1.7945391E38)
            float r5 = r5.getDimension(r6)
            r1.setTextSize(r5)
            android.graphics.Paint r1 = r0.touchPaint
            android.content.res.Resources r5 = r20.getResources()
            r6 = 2131100179(0x7f060213, float:1.7812732E38)
            int r5 = r5.getColor(r6)
            r1.setColor(r5)
            android.graphics.Paint r1 = r0.touchPaint2
            android.content.res.Resources r5 = r20.getResources()
            int r5 = r5.getColor(r6)
            r1.setColor(r5)
            java.util.List<com.wade.fit.greendao.bean.HealthHeartRateItem> r1 = r0.datas
            int r5 = r0.selPosition
            java.lang.Object r1 = r1.get(r5)
            com.wade.fit.greendao.bean.HealthHeartRateItem r1 = (com.wade.fit.greendao.bean.HealthHeartRateItem) r1
            java.lang.String r10 = r1.getRemark()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.util.List<com.wade.fit.greendao.bean.HealthHeartRateItem> r5 = r0.datas
            int r6 = r0.selPosition
            java.lang.Object r5 = r5.get(r6)
            com.wade.fit.greendao.bean.HealthHeartRateItem r5 = (com.wade.fit.greendao.bean.HealthHeartRateItem) r5
            int r5 = r5.getHeartRaveValue()
            r1.append(r5)
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            int[] r4 = com.wade.fit.views.HeartRateChart.C27114.$SwitchMap$com$wade.fit$model$bean$DetailTimeType
            com.wade.fit.model.bean.DetailTimeType r5 = r0.timeType
            int r5 = r5.ordinal()
            r4 = r4[r5]
            if (r4 == r2) goto L_0x015c
            r2 = 2
            if (r4 == r2) goto L_0x015c
            goto L_0x018d
        L_0x015c:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.wade.fit.app.AppApplication r2 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r2 = r2.getResources()
            r4 = 2131624621(0x7f0e02ad, float:1.8876427E38)
            java.lang.String r2 = r2.getString(r4)
            r1.append(r2)
            java.lang.String r2 = " "
            r1.append(r2)
            java.util.List<com.wade.fit.greendao.bean.HealthHeartRateItem> r2 = r0.datas
            int r4 = r0.selPosition
            java.lang.Object r2 = r2.get(r4)
            com.wade.fit.greendao.bean.HealthHeartRateItem r2 = (com.wade.fit.greendao.bean.HealthHeartRateItem) r2
            int r2 = r2.getHeartRaveValue()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
        L_0x018d:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r1)
            java.lang.String r1 = " bpm"
            r2.append(r1)
            java.lang.String r11 = r2.toString()
            android.graphics.Paint r1 = r0.touchPaint
            float r1 = com.wade.fit.util.ViewUtil.getTextRectWidth(r1, r10)
            int r12 = (int) r1
            android.graphics.Paint r1 = r0.touchPaint
            float r1 = com.wade.fit.util.ViewUtil.getTextRectHeight(r1, r10)
            int r13 = (int) r1
            android.graphics.Paint r1 = r0.touchPaint2
            float r1 = com.wade.fit.util.ViewUtil.getTextRectWidth(r1, r11)
            int r14 = (int) r1
            android.graphics.Paint r1 = r0.touchPaint2
            float r1 = com.wade.fit.util.ViewUtil.getTextRectHeight(r1, r11)
            int r15 = (int) r1
            int r6 = java.lang.Math.max(r12, r14)
            int r5 = r13 + r15
            int r1 = r3.y
            float r4 = (float) r1
            r16 = 1073741824(0x40000000, float:2.0)
            int r1 = com.wade.fit.util.ScreenUtil.dip2px(r16)
            float r1 = (float) r1
            float r3 = r4 - r1
            int r1 = com.wade.fit.util.ScreenUtil.dip2px(r16)
            float r1 = (float) r1
            float r17 = r4 + r1
            android.graphics.Paint r1 = r0.touchPaint
            r2 = 1065353216(0x3f800000, float:1.0)
            int r2 = com.wade.fit.util.ScreenUtil.dip2px(r2)
            float r2 = (float) r2
            r1.setStrokeWidth(r2)
            android.graphics.Paint r2 = r0.touchPaint
            r1 = r21
            r18 = r2
            r2 = r9
            r19 = r4
            r4 = r9
            r8 = r5
            r5 = r17
            r17 = r11
            r11 = r6
            r6 = r18
            r1.drawLine(r2, r3, r4, r5, r6)
            r1 = 1092616192(0x41200000, float:10.0)
            int r1 = com.wade.fit.util.ScreenUtil.dip2px(r1)
            android.graphics.RectF r2 = r0.selectRect
            int r6 = r11 / 2
            float r3 = (float) r6
            float r3 = r9 - r3
            int r3 = (int) r3
            int r3 = r3 - r1
            float r3 = (float) r3
            r2.left = r3
            float r3 = (float) r13
            float r4 = r19 - r3
            float r5 = (float) r15
            float r4 = r4 - r5
            int r4 = (int) r4
            r6 = 1097859072(0x41700000, float:15.0)
            int r13 = com.wade.fit.util.ScreenUtil.dip2px(r6)
            int r4 = r4 - r13
            int r13 = r1 * 2
            int r4 = r4 - r13
            float r4 = (float) r4
            r2.top = r4
            android.graphics.RectF r2 = r0.selectRect
            float r4 = r2.top
            r15 = 0
            float r4 = java.lang.Math.max(r15, r4)
            r2.top = r4
            android.graphics.RectF r2 = r0.selectRect
            float r4 = r2.left
            float r11 = (float) r11
            float r4 = r4 + r11
            float r13 = (float) r13
            float r4 = r4 + r13
            r2.right = r4
            android.graphics.RectF r2 = r0.selectRect
            float r4 = r2.top
            float r8 = (float) r8
            float r4 = r4 + r8
            int r6 = com.wade.fit.util.ScreenUtil.dip2px(r6)
            float r6 = (float) r6
            float r4 = r4 + r6
            float r6 = (float) r1
            float r4 = r4 + r6
            r2.bottom = r4
            int r2 = r12 / 2
            float r2 = (float) r2
            float r2 = r9 - r2
            int r4 = r14 / 2
            float r4 = (float) r4
            float r9 = r9 - r4
            android.graphics.RectF r4 = r0.selectRect
            float r4 = r4.right
            int r8 = r20.getWidth()
            float r8 = (float) r8
            int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r4 <= 0) goto L_0x028b
            java.lang.String r2 = "。。。。。。。。。。。右边超过屏幕了"
            com.wade.fit.util.log.LogUtil.d(r2)
            android.graphics.RectF r2 = r0.selectRect
            int r4 = r20.getWidth()
            int r4 = r4 - r1
            float r1 = (float) r4
            r2.right = r1
            android.graphics.RectF r1 = r0.selectRect
            float r2 = r1.right
            float r2 = r2 - r11
            float r2 = r2 - r13
            r1.left = r2
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            android.graphics.RectF r2 = r0.selectRect
            float r2 = r2.width()
            float r4 = (float) r12
            float r2 = r2 - r4
            float r2 = r2 / r16
            float r2 = r2 + r1
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            android.graphics.RectF r4 = r0.selectRect
            float r4 = r4.width()
        L_0x0284:
            float r8 = (float) r14
            float r4 = r4 - r8
            float r4 = r4 / r16
            float r9 = r1 + r4
            goto L_0x02c4
        L_0x028b:
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            int r1 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r1 > 0) goto L_0x02b7
            android.graphics.RectF r1 = r0.selectRect
            r1.left = r6
            float r2 = r1.left
            float r2 = r2 + r11
            float r2 = r2 + r13
            r1.right = r2
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            android.graphics.RectF r2 = r0.selectRect
            float r2 = r2.width()
            float r4 = (float) r12
            float r2 = r2 - r4
            float r2 = r2 / r16
            float r2 = r2 + r1
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            android.graphics.RectF r4 = r0.selectRect
            float r4 = r4.width()
            goto L_0x0284
        L_0x02b7:
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.top
            int r1 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r1 > 0) goto L_0x02c4
            java.lang.String r1 = "。。。。。。。。。。。顶部超过屏幕了"
            com.wade.fit.util.log.LogUtil.d(r1)
        L_0x02c4:
            android.graphics.RectF r1 = r0.selectRect
            r4 = 1084227584(0x40a00000, float:5.0)
            int r8 = com.wade.fit.util.ScreenUtil.dip2px(r4)
            float r8 = (float) r8
            int r11 = com.wade.fit.util.ScreenUtil.dip2px(r4)
            float r4 = (float) r11
            android.graphics.Paint r11 = r0.barPaint
            r7.drawRoundRect(r1, r8, r4, r11)
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.top
            float r1 = r1 + r6
            if (r10 == 0) goto L_0x02e5
            float r4 = r1 + r3
            android.graphics.Paint r6 = r0.touchPaint
            r7.drawText(r10, r2, r4, r6)
        L_0x02e5:
            float r1 = r1 + r3
            float r1 = r1 + r5
            r2 = 1084227584(0x40a00000, float:5.0)
            int r2 = com.wade.fit.util.ScreenUtil.dip2px(r2)
            float r2 = (float) r2
            float r1 = r1 + r2
            android.graphics.Paint r2 = r0.touchPaint2
            r3 = r17
            r7.drawText(r3, r9, r1, r2)
        L_0x02f6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.HeartRateChart.drawMaxHeight(android.graphics.Canvas):void");
    }

    /* renamed from: com.wade.fit.views.HeartRateChart$4 */
    static /* synthetic */ class C27114 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailTimeType = new int[DetailTimeType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.wade.fit.model.bean.DetailTimeType[] r0 = com.wade.fit.model.bean.DetailTimeType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.views.HeartRateChart.C27114.$SwitchMap$com$wade.fit$model$bean$DetailTimeType = r0
                int[] r0 = com.wade.fit.views.HeartRateChart.C27114.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.SIX_MONTH     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.views.HeartRateChart.C27114.$SwitchMap$com$wade.fit$model$bean$DetailTimeType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailTimeType r1 = com.wade.fit.model.bean.DetailTimeType.YEAR     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.HeartRateChart.C27114.<clinit>():void");
        }
    }

    /* access modifiers changed from: private */
    public void animateY() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, 1.5f, 0.0f);
        ofFloat.setDuration(1000L);
        ofFloat.setInterpolator(new DecelerateInterpolator());
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            /* class com.wade.fit.views.HeartRateChart.C27103 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                HeartRateChart.this.animateValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                HeartRateChart.this.invalidate();
            }
        });
        ofFloat.start();
    }

    private void drawXLine(Canvas canvas) {
        this.labelXPaint.setTextAlign(Paint.Align.LEFT);
        for (int i = 0; i < this.splitNum; i++) {
            Path path3 = new Path();
            path3.moveTo(this.xOffSet * 1.4f, this.lineHeight[i] + this.pointHeight);
            path3.lineTo(((float) this.f5239w) - (this.xOffSet * 1.4f), this.lineHeight[i] + this.pointHeight);
            canvas.drawPath(path3, this.xSeparatePaint);
        }
        if (this.hasYvalue && this.datas.size() > 0) {
            int i2 = 0;
            while (i2 < this.scaleY.length) {
                i2++;
                canvas.drawText(this.scaleY[i2] + "", this.xOffSet * 1.4f, (this.lineHeight[i2] + this.pointHeight) - 10.0f, this.labelYPaint);
            }
        }
        this.barPaint.setStrokeWidth(this.barWid);
        this.barPaint.setTextAlign(Paint.Align.CENTER);
        int i3 = (int) (this.labelYWidth / this.barWidth);
        for (int i4 = 0; i4 < this.barNum + i3; i4++) {
            float f = this.barWidth;
            float f2 = (float) i4;
            float f3 = this.xOffSet;
            float f4 = (f * f2) + (f3 * 1.4f);
            float f5 = this.bottomY;
            float f6 = this.barWid;
            float f7 = (f * f2) + f6 + (f3 * 1.4f);
            this.rect.set(f4, f5, f7, f5 + f6);
        }
    }

    private void drawXValue(Canvas canvas) {
        float size = this.chartWidth / ((float) (this.xtimes.size() - 1));
        float textHeight = ViewUtil.getTextHeight(this.barPaint);
        for (int i = 0; i < this.xtimes.size(); i++) {
            float f = (((float) i) * size) + (this.xOffSet * 1.4f) + this.labelYWidth;
            if (i == 0) {
                this.labelXPaint.setTextAlign(Paint.Align.LEFT);
            } else if (i == this.xtimes.size() - 1) {
                this.labelXPaint.setTextAlign(Paint.Align.RIGHT);
            } else {
                this.labelXPaint.setTextAlign(Paint.Align.CENTER);
            }
            canvas.drawText(this.xtimes.get(i), f, this.bottomY + this.barWid + textHeight, this.labelXPaint);
        }
    }

    private void drawTouch(Canvas canvas) {
        if (this.isMove) {
            int i = 0;
            if (this.datas.size() == 1) {
                this.touchX = minutToX(this.datas.get(0).getOffsetMinute());
                this.touchY = (float) this.datas.get(0).getHeartRaveValue();
            } else if (this.datas.size() > 1) {
                float f = this.touchX;
                if (f != -1.0f) {
                    int xToMinute = xToMinute(f);
                    if (xToMinute >= 0) {
                        if (this.datas.get(0).getOffsetMinute() < xToMinute) {
                            List<HealthHeartRateItem> list = this.datas;
                            if (list.get(list.size() - 1).getOffsetMinute() > xToMinute) {
                                while (true) {
                                    if (i >= this.datas.size() - 1) {
                                        i = -1;
                                        break;
                                    }
                                    if (this.datas.get(i).getOffsetMinute() <= xToMinute) {
                                        int i2 = i + 1;
                                        if (this.datas.get(i2).getOffsetMinute() >= xToMinute) {
                                            if (Math.abs(this.datas.get(i).getOffsetMinute() - xToMinute) < Math.abs(this.datas.get(i2).getOffsetMinute() - xToMinute)) {
                                                xToMinute = this.datas.get(i).getOffsetMinute();
                                                this.touchY = (float) this.datas.get(i).getHeartRaveValue();
                                            } else {
                                                xToMinute = this.datas.get(i2).getOffsetMinute();
                                                this.touchY = (float) this.datas.get(i2).getHeartRaveValue();
                                                i = i2;
                                            }
                                        }
                                    }
                                    i++;
                                }
                            } else {
                                List<HealthHeartRateItem> list2 = this.datas;
                                xToMinute = list2.get(list2.size() - 1).getOffsetMinute();
                                List<HealthHeartRateItem> list3 = this.datas;
                                this.touchY = (float) list3.get(list3.size() - 1).getHeartRaveValue();
                                i = this.datas.size() - 1;
                            }
                        } else {
                            xToMinute = this.datas.get(0).getOffsetMinute();
                            this.touchY = (float) this.datas.get(0).getHeartRaveValue();
                        }
                    } else {
                        xToMinute = this.datas.get(0).getOffsetMinute();
                        this.touchY = (float) this.datas.get(0).getHeartRaveValue();
                    }
                    this.touchX = minutToX(xToMinute);
                } else {
                    List<HealthHeartRateItem> list4 = this.datas;
                    this.touchX = minutToX(list4.get(list4.size() - 1).getOffsetMinute());
                    List<HealthHeartRateItem> list5 = this.datas;
                    this.touchY = (float) list5.get(list5.size() - 1).getHeartRaveValue();
                    i = this.datas.size() - 1;
                }
            } else {
                i = -1;
            }
            this.selPosition = i;
        }
        drawMaxHeight(canvas);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        TouchOrClickListener touchOrClickListener;
        int action = motionEvent.getAction();
        if (action == 0) {
            this.touchX = motionEvent.getX();
            this.touchX1 = (int) motionEvent.getX();
        } else if (action == 1) {
            this.touchX2 = (int) motionEvent.getX();
            if (Math.abs((float) (this.touchX2 - this.touchX1)) < 10.0f && (touchOrClickListener = this.clickListener) != null) {
                touchOrClickListener.doClick();
            }
            this.isMove = true;
        } else if (action == 2) {
            this.touchX = motionEvent.getX();
            this.isMove = true;
        }
        invalidate();
        return true;
    }

    public void doClick(TouchOrClickListener touchOrClickListener) {
        this.clickListener = touchOrClickListener;
    }

    private float minutToX(int i) {
        return (((float) i) * this.chartWidthSpan) + (this.xOffSet * 1.4f) + this.labelYWidth;
    }

    private int xToMinute(float f) {
        return (int) (((f - (this.xOffSet * 1.4f)) - this.labelYWidth) / this.chartWidthSpan);
    }
}
