package com.wade.fit.greendao.bean;

public class HealthGpsItem {
    private Long date;
    private boolean isUploaded;
    private Double latitude;
    private Double longitude;
    private String macAddress;
    private String remark;
    private String startDate;
    private String userId;

    public HealthGpsItem() {
    }

    public HealthGpsItem(boolean z, String str, String str2, Double d, Double d2, Long l, String str3, String str4) {
        this.isUploaded = z;
        this.macAddress = str;
        this.startDate = str2;
        this.longitude = d;
        this.latitude = d2;
        this.date = l;
        this.userId = str3;
        this.remark = str4;
    }

    public boolean getIsUploaded() {
        return this.isUploaded;
    }

    public void setIsUploaded(boolean z) {
        this.isUploaded = z;
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Double d) {
        this.longitude = d;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double d) {
        this.latitude = d;
    }

    public Long getDate() {
        return this.date;
    }

    public void setDate(Long l) {
        this.date = l;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(String str) {
        this.startDate = str;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String str) {
        this.remark = str;
    }
}
