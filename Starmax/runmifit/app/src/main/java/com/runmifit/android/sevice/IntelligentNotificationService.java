package com.wade.fit.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.support.v4.internal.view.SupportMenu;
import android.text.TextUtils;
import com.baidu.mobstat.Config;
import com.google.common.base.Ascii;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.AppNotice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.ui.main.activity.MainActivity;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ble.BleCallback;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayDeque;
import java.util.Deque;

public class IntelligentNotificationService extends NotificationListenerService {
    public static final byte FACEBOOK = 6;
    public static final byte INSTAGRAM = 10;
    public static final byte LINKEDIN = 11;
    public static final byte MESSENGER = 9;

    /* renamed from: QQ */
    public static final byte f4544QQ = 4;
    public static final byte TWITTER = 7;
    public static final byte WECHAT = 3;
    public static final byte WHATSAPP = 8;
    private String CHANNEL_ONE_ID = "com.hearth.notification";
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    int index = 0;
    /* access modifiers changed from: private */
    public boolean isSending = false;
    private byte[] mSmsContent = null;
    private String mText = null;
    private String mTitle = null;
    private byte mType;
    public Deque<SendMessage> sendMessages = new ArrayDeque();
    private int smsIndex = 0;
    private int smsModLen = 0;
    Runnable timeRun = new Runnable() {
        /* class com.wade.fit.service.IntelligentNotificationService.C15311 */

        public void run() {
            IntelligentNotificationService.this.index++;
            if (IntelligentNotificationService.this.index <= 5) {
                IntelligentNotificationService.this.handler.postDelayed(IntelligentNotificationService.this.timeRun, 1000);
                return;
            }
            IntelligentNotificationService intelligentNotificationService = IntelligentNotificationService.this;
            intelligentNotificationService.index = 0;
            intelligentNotificationService.handler.removeCallbacks(IntelligentNotificationService.this.timeRun);
            boolean unused = IntelligentNotificationService.this.isSending = false;
        }
    };

    public void onCreate() {
        super.onCreate();
        PendingIntent activity = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        Notification.Builder builder = new Notification.Builder(this);
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel(this.CHANNEL_ONE_ID, "otifition_one", 4);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(SupportMenu.CATEGORY_MASK);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(1);
            ((NotificationManager) getSystemService("notification")).createNotificationChannel(notificationChannel);
            builder.setChannelId(this.CHANNEL_ONE_ID);
        }
        builder.setContentTitle("Foreground Service");
        builder.setContentText("Foreground Service Started.");
        builder.setContentIntent(activity);
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon((int) R.mipmap.ic_launcher);
        startForeground(1, builder.build());
        LogUtil.dAndSave("IntelligentNotificationService: onCreate()", Constants.NOTIFICATION_PATH);
    }

    public IBinder onBind(Intent intent) {
        LogUtil.dAndSave("IntelligentNotificationService: onBind()", Constants.NOTIFICATION_PATH);
        return super.onBind(intent);
    }

    public void onRebind(Intent intent) {
        LogUtil.dAndSave("IntelligentNotificationService: onRebind()", Constants.NOTIFICATION_PATH);
        super.onRebind(intent);
    }

    public void onListenerConnected() {
        LogUtil.dAndSave("IntelligentNotificationService: onListenerConnected:", Constants.NOTIFICATION_PATH);
        super.onListenerConnected();
    }

    public void onListenerDisconnected() {
        LogUtil.dAndSave("IntelligentNotificationService: onListenerDisconnected:", Constants.NOTIFICATION_PATH);
        super.onListenerDisconnected();
    }

    public void onDestroy() {
        super.onDestroy();
        LogUtil.dAndSave("IntelligentNotificationService: onDestroy()", Constants.NOTIFICATION_PATH);
        stopForeground(true);
    }

    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        DebugLog.m6203d("onNotificationPosted ------->sbn:" + statusBarNotification);
        Notification notification = statusBarNotification.getNotification();
        AppNotice appNotice = SPHelper.getDeviceConfig().notice;
        LogUtil.dAndSave("IntelligentNotificationService: state:" + SPHelper.getDeviceConfig().isDisturbMode, Constants.NOTIFICATION_PATH);
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        if (deviceConfig != null && deviceConfig.isDisturbMode && notification != null) {
            String packageName = statusBarNotification.getPackageName();
            String str = (String) notification.tickerText;
            LogUtil.d("p:" + packageName + ",tickerText:" + str + "," + notification.toString());
            if ("com.tencent.mm".equals(packageName) && appNotice.wechat) {
                if (!TextUtils.isEmpty(str)) {
                    if (str.startsWith("语音通话中") || str.startsWith("語音通話中") || str.startsWith("Tap to continue as voice call in progress")) {
                        LogUtil.d("微信正在语音");
                        return;
                    }
                }
                sendText(notification, (byte) 2, "weixin");
            } else if (("com.tencent.mobileqq".equals(packageName) || com.tencent.connect.common.Constants.PACKAGE_QQ_SPEED.equals(packageName)) && appNotice.f5221qq) {
                sendText(notification, (byte) 3, "qq");
            } else if ("com.facebook.katana".equals(packageName) && appNotice.facebook) {
                sendText(notification, (byte) 4, "facebook");
            } else if ("com.twitter.android".equals(packageName) && appNotice.twitter) {
                sendText(notification, (byte) 6, "twitter");
            } else if ("com.whatsapp".equals(packageName) && appNotice.whatsApp) {
                sendText(notification, (byte) 7, "whatsApp");
            } else if ("com.linkedin.android".equals(packageName) && appNotice.linked) {
                sendText(notification, (byte) 11, "linked");
            } else if ("com.instagram.android".equals(packageName) && appNotice.instagram) {
                sendText(notification, (byte) 10, "instagram");
            } else if (("jp.naver.line.android".equals(packageName) || packageName.contains("line.android")) && appNotice.line) {
                sendText(notification, (byte) 8, "line");
            } else if ("com.vkontakte.android".equals(packageName) && appNotice.f5222vk) {
                sendText(notification, Ascii.f4503CR, "vkontakte");
            } else if (("com.skype.raider".equals(packageName) || "com.skype.rover".equals(packageName)) && appNotice.skype) {
                sendText(notification, (byte) 5, "skype");
            } else if ("com.facebook.orca".equals(packageName) && appNotice.messager) {
                sendText(notification, Ascii.f4505FF, "messager");
            }
        }
    }

    private void sendText(Notification notification, byte b, String str) {
        String str2;
        String str3;
        DebugLog.m6203d("type===>" + ((int) b));
        String str4 = null;
        try {
            String charSequence = TextUtils.isEmpty(notification.tickerText) ? null : notification.tickerText.toString();
            if (TextUtils.isEmpty(charSequence) || !charSequence.contains(Config.TRACE_TODAY_VISIT_SPLIT)) {
                str3 = charSequence;
            } else {
                String[] split = charSequence.split(Config.TRACE_TODAY_VISIT_SPLIT);
                String str5 = split[0];
                str3 = split[1];
                str4 = split[0].contains("]") ? split[0].substring(split[0].indexOf("]") + 1) : str5;
            }
            str2 = str4;
            str4 = str3;
        } catch (Exception unused) {
            str2 = null;
        }
        if (b == 7) {
            Bundle bundle = notification.extras;
            if (bundle != null) {
                for (String str6 : bundle.keySet()) {
                    try {
                        LogUtil.d("Bundle ContentKey=" + str6 + "," + bundle.get(str6));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                String string = notification.extras.getString(NotificationCompat.EXTRA_TEXT);
                String string2 = notification.extras.getString(NotificationCompat.EXTRA_TITLE);
                if (string != null && !string.contains("条新信息") && string2 != null && !string2.equals("WhatsApp")) {
                    str4 = string;
                    str2 = string2;
                } else {
                    return;
                }
            }
        } else if (b == 11 && str4 == null) {
            str4 = AppApplication.getInstance().getString(R.string.new_msg);
        }
        this.mTitle = str2;
        this.mText = str4;
        this.mType = b;
        if (!TextUtils.isEmpty(this.mText)) {
            LogUtil.d("发送的提醒的信息,mTitle:" + this.mTitle + ",mText:" + this.mText + ",mType:" + ((int) this.mType));
            if (this.mTitle == null) {
                this.mTitle = "";
            }
            if (this.mTitle.equalsIgnoreCase(com.tencent.connect.common.Constants.SOURCE_QQ)) {
                LogUtil.d("QQ内部消息，，，，不发送");
            } else if (!TextUtils.isEmpty(this.mText)) {
                SendMessage sendMessage = new SendMessage();
                sendMessage.setmType(this.mType);
                sendMessage.setmTitle(this.mTitle);
                sendMessage.setmText(this.mText);
                this.sendMessages.add(sendMessage);
                postMessage();
            }
        }
    }

    private void postMessage() {
        if (!this.isSending) {
            SendMessage poll = this.sendMessages.poll();
            setSmsEvt(poll.getmType(), poll.getmTitle(), poll.getmText());
            this.handler.postDelayed(this.timeRun, 0);
        }
    }

    public void setSmsEvt(int i, String str, String str2) {
        this.isSending = true;
        BleSdkWrapper.setMessage(i, str, str2, new BleCallback() {
            /* class com.wade.fit.service.IntelligentNotificationService.C15322 */

            public void setSuccess() {
            }

            public void complete(int i, Object obj) {
                LogUtil.d("resultCode:" + i);
                boolean unused = IntelligentNotificationService.this.isSending = false;
                IntelligentNotificationService intelligentNotificationService = IntelligentNotificationService.this;
                intelligentNotificationService.index = 0;
                intelligentNotificationService.handler.removeCallbacks(IntelligentNotificationService.this.timeRun);
                if (!IntelligentNotificationService.this.sendMessages.isEmpty()) {
                    SendMessage poll = IntelligentNotificationService.this.sendMessages.poll();
                    IntelligentNotificationService.this.setSmsEvt(poll.getmType(), poll.getmTitle(), poll.getmText());
                    IntelligentNotificationService.this.handler.postDelayed(IntelligentNotificationService.this.timeRun, 0);
                }
            }
        });
    }
}
