package com.wade.fit.model.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WeatherBean {
    public int currentTemp = (new Random().nextInt(10) + 20);
    public int state = 1;
    public int tempUnit = 0;
    public List<WeatherBean> weatherBeans = new ArrayList();
    public int weatherType = 1;

    public String toString() {
        return "WeatherBean{state=" + this.state + ", tempUnit=" + this.tempUnit + ", weatherType=" + this.weatherType + ", currentTemp=" + this.currentTemp + ", weatherBeans=" + this.weatherBeans + '}';
    }
}
