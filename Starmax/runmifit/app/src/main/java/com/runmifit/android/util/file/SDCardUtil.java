package com.wade.fit.util.file;

import android.os.Environment;
import android.os.StatFs;
import com.wade.fit.util.ConvertUtil;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class SDCardUtil {
    private SDCardUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static boolean isSDCardEnable() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static String getDataPath() {
        if (!isSDCardEnable()) {
            return "sdcard unable!";
        }
        return Environment.getDataDirectory().getPath() + File.separator;
    }

    public static String getSDCardPath() {
        if (!isSDCardEnable()) {
            return "sdcard unable!";
        }
        return Environment.getExternalStorageDirectory().getPath() + File.separator;
    }

    public static String getSDCardPathByCmd() {
        if (!isSDCardEnable()) {
            return "sdcard unable!";
        }
        BufferedReader bufferedReader = null;
        try {
            Process exec = Runtime.getRuntime().exec("cat /proc/mounts");
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(new BufferedInputStream(exec.getInputStream())));
            while (true) {
                try {
                    String readLine = bufferedReader2.readLine();
                    if (readLine == null) {
                        FileUtil.closeIO(bufferedReader2);
                        break;
                    }
                    if (readLine.contains("sdcard") && readLine.contains(".android_secure")) {
                        String[] split = readLine.split(" ");
                        if (split.length >= 5) {
                            String str = split[1].replace("/.android_secure", "") + File.separator;
                            FileUtil.closeIO(bufferedReader2);
                            return str;
                        }
                    }
                    if (exec.waitFor() != 0 && exec.exitValue() == 1) {
                        FileUtil.closeIO(bufferedReader2);
                        return " 命令执行失败";
                    }
                } catch (Exception e) {
                    e = e;
                    bufferedReader = bufferedReader2;
                    try {
                        e.printStackTrace();
                        FileUtil.closeIO(bufferedReader);
                        return Environment.getExternalStorageDirectory().getPath() + File.separator;
                    } catch (Throwable th) {
                        th = th;
                        bufferedReader2 = bufferedReader;
                        FileUtil.closeIO(bufferedReader2);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    FileUtil.closeIO(bufferedReader2);
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            FileUtil.closeIO(bufferedReader);
            return Environment.getExternalStorageDirectory().getPath() + File.separator;
        }
        return Environment.getExternalStorageDirectory().getPath() + File.separator;
    }

    public static String getFreeSpace() {
        if (!isSDCardEnable()) {
            return "sdcard unable!";
        }
        StatFs statFs = new StatFs(getSDCardPath());
        return ConvertUtil.byte2FitSize(statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong());
    }

    public static SDCardInfo getSDCardInfo() {
        SDCardInfo sDCardInfo = new SDCardInfo();
        if ("mounted".equals(Environment.getExternalStorageState())) {
            sDCardInfo.isExist = true;
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            sDCardInfo.totalBlocks = statFs.getBlockCountLong();
            sDCardInfo.blockByteSize = statFs.getBlockSizeLong();
            sDCardInfo.availableBlocks = statFs.getAvailableBlocksLong();
            sDCardInfo.availableBytes = statFs.getAvailableBytes();
            sDCardInfo.freeBlocks = statFs.getFreeBlocksLong();
            sDCardInfo.freeBytes = statFs.getFreeBytes();
            sDCardInfo.totalBytes = statFs.getTotalBytes();
        }
        return sDCardInfo;
    }

    private static class SDCardInfo {
        long availableBlocks;
        long availableBytes;
        long blockByteSize;
        long freeBlocks;
        long freeBytes;
        boolean isExist;
        long totalBlocks;
        long totalBytes;

        private SDCardInfo() {
        }

        public String toString() {
            return "SDCardInfo{isExist=" + this.isExist + ", totalBlocks=" + this.totalBlocks + ", freeBlocks=" + this.freeBlocks + ", availableBlocks=" + this.availableBlocks + ", blockByteSize=" + this.blockByteSize + ", totalBytes=" + this.totalBytes + ", freeBytes=" + this.freeBytes + ", availableBytes=" + this.availableBytes + '}';
        }
    }
}
