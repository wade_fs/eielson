package com.wade.fit.util.baidumap;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class MyOrientationListener implements SensorEventListener {
    private float lastX;
    private Context mContext;
    private OnOrientationListener mOnOrientationListener;
    private Sensor mSensor;
    private SensorManager mSensorManager;

    public interface OnOrientationListener {
        void onOrientationChanged(float f);
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public MyOrientationListener(Context context) {
        this.mContext = context;
    }

    public void start() {
        this.mSensorManager = (SensorManager) this.mContext.getSystemService("sensor");
        SensorManager sensorManager = this.mSensorManager;
        if (sensorManager != null) {
            this.mSensor = sensorManager.getDefaultSensor(3);
        }
        Sensor sensor = this.mSensor;
        if (sensor != null) {
            this.mSensorManager.registerListener(this, sensor, 2);
        }
    }

    public void stop() {
        this.mSensorManager.unregisterListener(this);
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        OnOrientationListener onOrientationListener;
        if (sensorEvent.sensor.getType() == 3) {
            float f = sensorEvent.values[0];
            if (((double) Math.abs(f - this.lastX)) > 1.0d && (onOrientationListener = this.mOnOrientationListener) != null) {
                onOrientationListener.onOrientationChanged(f);
            }
            this.lastX = f;
        }
    }

    public void setOnOrientationListener(OnOrientationListener onOrientationListener) {
        this.mOnOrientationListener = onOrientationListener;
    }
}
