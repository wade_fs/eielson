package com.wade.fit.model.net;

import okhttp3.Interceptor;
import okhttp3.Response;

/* renamed from: com.wade.fit.model.net.-$$Lambda$ApiManager$-AsB-0KQyYsn082lftGEIG7bnYU  reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ApiManager$AsB0KQyYsn082lftGEIG7bnYU implements Interceptor {
    public static final /* synthetic */ $$Lambda$ApiManager$AsB0KQyYsn082lftGEIG7bnYU INSTANCE = new $$Lambda$ApiManager$AsB0KQyYsn082lftGEIG7bnYU();

    private /* synthetic */ $$Lambda$ApiManager$AsB0KQyYsn082lftGEIG7bnYU() {
    }

    public final Response intercept(Interceptor.Chain chain) {
        return ApiManager.lambda$getRetrofit$2(chain);
    }
}
