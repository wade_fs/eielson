package com.wade.fit.ui.mine.activity;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.persenter.mine.FeedBackContract;
import com.wade.fit.persenter.mine.FeedBackPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DataVaildUtil;
import com.wade.fit.util.ToastUtil;

/* renamed from: com.wade.fit.ui.mine.activity.FeedbackActivity */
public class FeedbackActivity extends BaseMvpActivity<FeedBackPresenter> implements FeedBackContract.View {
    Button btnSave;
    EditText edContent;
    EditText edEmail;
    TextView tvContentLength;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_feedback;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.titleName.setText(getResources().getString(R.string.question_feebak));
        this.edContent.addTextChangedListener(new TextWatcher() {
            /* class com.wade.fit.ui.mine.activity.FeedbackActivity.C25761 */

            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (TextUtils.isEmpty(FeedbackActivity.this.edContent.getText().toString())) {
                    FeedbackActivity.this.tvContentLength.setText("0/500");
                    FeedbackActivity.this.btnSave.setClickable(false);
                    FeedbackActivity.this.btnSave.setBackgroundResource(R.drawable.btn_submit_normle);
                    return;
                }
                TextView textView = FeedbackActivity.this.tvContentLength;
                textView.setText(FeedbackActivity.this.edContent.getText().toString().length() + "/500");
                if (!TextUtils.isEmpty(FeedbackActivity.this.edEmail.getText().toString())) {
                    FeedbackActivity.this.btnSave.setClickable(true);
                    FeedbackActivity.this.btnSave.setBackgroundResource(R.drawable.btn_submit_bg);
                }
            }
        });
        this.edEmail.addTextChangedListener(new TextWatcher() {
            /* class com.wade.fit.ui.mine.activity.FeedbackActivity.C25772 */

            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (TextUtils.isEmpty(FeedbackActivity.this.edEmail.getText().toString()) || TextUtils.isEmpty(FeedbackActivity.this.edContent.getText().toString())) {
                    FeedbackActivity.this.btnSave.setClickable(false);
                    FeedbackActivity.this.btnSave.setBackgroundResource(R.drawable.btn_submit_normle);
                    return;
                }
                FeedbackActivity.this.btnSave.setClickable(true);
                FeedbackActivity.this.btnSave.setBackgroundResource(R.drawable.btn_submit_bg);
            }
        });
        this.btnSave.setClickable(false);
    }

    /* access modifiers changed from: package-private */
    public void toCommit() {
        if (ButtonUtils.isFastDoubleClick(R.id.btnSave, 1000)) {
            return;
        }
        if (TextUtils.isEmpty(this.edContent.getText().toString())) {
            ToastUtil.showToast(getResources().getString(R.string.feed_content_tip));
        } else if (DataVaildUtil.vaildUserName(this.edContent.getText().toString())) {
            ToastUtil.showToast(getResources().getString(R.string.input_error));
        } else if (DataVaildUtil.validEmail(this, this.edEmail.getText().toString())) {
            ((FeedBackPresenter) this.mPresenter).uploadFeedbackInfo(this.edContent.getText().toString(), this.edEmail.getText().toString());
        }
    }

    public void requestSuccess() {
        ToastUtil.showToast(getResources().getString(R.string.config_success));
        finish();
    }

    public void requestFaild() {
        ToastUtil.showToast(getResources().getString(R.string.config_faild));
    }
}
