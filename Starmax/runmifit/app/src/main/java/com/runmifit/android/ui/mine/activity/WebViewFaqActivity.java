package com.wade.fit.ui.mine.activity;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import java.util.Locale;

/* renamed from: com.wade.fit.ui.mine.activity.WebViewFaqActivity */
public class WebViewFaqActivity extends BaseActivity {
    private String faq_h5_cn = "https://u.eqxiu.com/s/cdfs0Mfp";
    private String faq_h5_de = "https://a.eqxiu.com/s/kNYLV4LT";
    private String faq_h5_eh = "https://h.eqxiu.com/s/wpZMLSA5";
    private String faq_h5_es = "https://a.eqxiu.com/s/mXsCi21F";
    private String faq_h5_fr = "https://q.eqxiu.com/s/NY7Txy6u";
    private String faq_h5_it = "https://b.eqxiu.com/s/xvsLUEn6";
    private String faq_h5_ja = "https://q.eqxiu.com/s/4ZXS9gXf";
    private String faq_h5_tw = "https://c.eqxiu.com/s/7442fAn4";
    WebView mWebView;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_web_ecg;
    }

    public void initView() {
        Locale locale;
        WebSettings settings = this.mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setLoadWithOverviewMode(true);
        this.mWebView.setWebViewClient(new WebViewClient() {
            /* class com.wade.fit.ui.mine.activity.WebViewFaqActivity.C26001 */

            public void onPageFinished(WebView webView, String str) {
            }

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            }

            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                return true;
            }
        });
        int i = getIntent().getExtras().getInt("loadType");
        if (i == 4) {
            this.titleName.setText(getResources().getString(R.string.sys_normal_probleml));
        }
        if (Build.VERSION.SDK_INT >= 24) {
            locale = getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = getResources().getConfiguration().locale;
        }
        String language = locale.getLanguage();
        if (language.contains("zh")) {
            if (locale.getCountry().equals("TW")) {
                if (i == 4) {
                    this.mWebView.loadUrl(this.faq_h5_tw);
                }
            } else if (i == 4) {
                this.mWebView.loadUrl(this.faq_h5_cn);
            }
        } else if (language.contains("fr")) {
            if (i == 4) {
                this.mWebView.loadUrl(this.faq_h5_fr);
            }
        } else if (language.contains("es")) {
            if (i == 4) {
                this.mWebView.loadUrl(this.faq_h5_es);
            }
        } else if (language.contains("de")) {
            if (i == 4) {
                this.mWebView.loadUrl(this.faq_h5_de);
            }
        } else if (language.contains("ja")) {
            if (i == 4) {
                this.mWebView.loadUrl(this.faq_h5_ja);
            }
        } else if (language.contains("it")) {
            if (i == 4) {
                this.mWebView.loadUrl(this.faq_h5_it);
            }
        } else if (i == 4) {
            this.mWebView.loadUrl(this.faq_h5_eh);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
