package com.wade.fit.model;

import android.text.TextUtils;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseBean<T> implements Serializable {
    private int code;
    private T data;
    private String message;

    public int getCode() {
        return this.code;
    }

    public void setCode(int i) {
        this.code = i;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String str) {
        this.message = str;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T t) {
        this.data = t;
    }

    public BaseBean(String str, int i, Object obj) {
        this.message = str;
        this.code = i;
        this.data = obj;
    }

    public static String parseObject(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            return new JSONObject(str).optString(str2, "");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
