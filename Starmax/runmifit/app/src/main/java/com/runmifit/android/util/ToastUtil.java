package com.wade.fit.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.wade.fit.R;
import com.wade.fit.app.LibContext;
import com.wade.fit.task.MainThread;

public class ToastUtil {
    static Toast customToast;
    private static long mLastTime;
    private static Toast mToast;

    public static void showToast(Context context, int i, int i2) {
        showToast(context, context.getResources().getString(i), i2);
    }

    public static void showCustomToast(Context context, String str) {
        ThreadUtil.runOnMainThread(new Runnable(context, str) {
            /* class com.wade.fit.util.$$Lambda$ToastUtil$sl5LrdK0IUCXojeocaJIECjz1O4 */
            private final /* synthetic */ Context f$0;
            private final /* synthetic */ String f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            public final void run() {
                ToastUtil.lambda$showCustomToast$0(this.f$0, this.f$1);
            }
        });
    }

    static /* synthetic */ void lambda$showCustomToast$0(Context context, String str) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.toast_layout, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.toastContent)).setText(str);
        Toast toast = mToast;
        if (toast != null) {
            toast.cancel();
        }
        Toast toast2 = customToast;
        if (toast2 != null) {
            toast2.cancel();
        }
        customToast = new Toast(context);
        customToast.setGravity(17, 0, 0);
        customToast.setView(inflate);
        customToast.setDuration(0);
        customToast.show();
    }

    public static void showToast(Context context, String str, int i) {
        MainThread.runOnMainThread(new Runnable(context, str, i) {
            /* class com.wade.fit.util.$$Lambda$ToastUtil$cDKXJ43Xapj83TmMLIdkW1nXpw4 */
            private final /* synthetic */ Context f$0;
            private final /* synthetic */ String f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                ToastUtil.lambda$showToast$1(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    static /* synthetic */ void lambda$showToast$1(Context context, String str, int i) {
        if (mToast == null) {
            mToast = Toast.makeText(context, str, i);
        }
        mToast.setText(str);
        mToast.setDuration(i);
        mToast.setGravity(17, 0, 0);
        if (System.currentTimeMillis() - mLastTime > 1000) {
            mToast.show();
            mLastTime = System.currentTimeMillis();
        }
    }

    public static void showToast(Context context, int i) {
        if (context != null) {
            showToast(context, context.getString(i), 0);
        }
    }

    public static void showToast(Context context, String str) {
        if (context != null) {
            showToast(context, str, 0);
        }
    }

    public static void showToast(String str) {
        showToast(LibContext.getAppContext(), str);
    }

    public static void showToast(int i) {
        showToast(LibContext.getAppContext(), i);
    }
}
