package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthSportItem;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class HealthSportItemDao extends AbstractDao<HealthSportItem, Void> {
    public static final String TABLENAME = "HEALTH_SPORT_ITEM";

    public static class Properties {
        public static final Property ActiveTime = new Property(6, Integer.TYPE, "activeTime", false, "ACTIVE_TIME");
        public static final Property AvgActiveTime = new Property(10, Integer.TYPE, "avgActiveTime", false, "AVG_ACTIVE_TIME");
        public static final Property AvgCalory = new Property(11, Integer.TYPE, "avgCalory", false, "AVG_CALORY");
        public static final Property AvgDistance = new Property(12, Integer.TYPE, "avgDistance", false, "AVG_DISTANCE");
        public static final Property AvgStep = new Property(9, Integer.TYPE, "avgStep", false, "AVG_STEP");
        public static final Property Calory = new Property(7, Integer.TYPE, "calory", false, "CALORY");
        public static final Property Date = new Property(13, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(4, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");
        public static final Property Distance = new Property(8, Integer.TYPE, "distance", false, "DISTANCE");
        public static final Property IsUploaded = new Property(0, Boolean.TYPE, "isUploaded", false, "IS_UPLOADED");
        public static final Property ItemCount = new Property(16, Integer.TYPE, "itemCount", false, "ITEM_COUNT");
        public static final Property MacAddress = new Property(1, String.class, "macAddress", false, "MAC_ADDRESS");
        public static final Property Month = new Property(3, Integer.TYPE, "month", false, "MONTH");
        public static final Property Remark = new Property(15, String.class, "remark", false, "REMARK");
        public static final Property StepCount = new Property(5, Integer.TYPE, "stepCount", false, "STEP_COUNT");
        public static final Property UserId = new Property(14, String.class, Constants.userId, false, "USER_ID");
        public static final Property Year = new Property(2, Integer.TYPE, "year", false, "YEAR");
    }

    public Void getKey(HealthSportItem healthSportItem) {
        return null;
    }

    public boolean hasKey(HealthSportItem healthSportItem) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(HealthSportItem healthSportItem, long j) {
        return null;
    }

    public HealthSportItemDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public HealthSportItemDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"HEALTH_SPORT_ITEM\" (\"IS_UPLOADED\" INTEGER NOT NULL ,\"MAC_ADDRESS\" TEXT,\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"STEP_COUNT\" INTEGER NOT NULL ,\"ACTIVE_TIME\" INTEGER NOT NULL ,\"CALORY\" INTEGER NOT NULL ,\"DISTANCE\" INTEGER NOT NULL ,\"AVG_STEP\" INTEGER NOT NULL ,\"AVG_ACTIVE_TIME\" INTEGER NOT NULL ,\"AVG_CALORY\" INTEGER NOT NULL ,\"AVG_DISTANCE\" INTEGER NOT NULL ,\"DATE\" INTEGER NOT NULL ,\"USER_ID\" TEXT,\"REMARK\" TEXT,\"ITEM_COUNT\" INTEGER NOT NULL );");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"HEALTH_SPORT_ITEM\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, HealthSportItem healthSportItem) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, healthSportItem.getIsUploaded() ? 1 : 0);
        String macAddress = healthSportItem.getMacAddress();
        if (macAddress != null) {
            databaseStatement.bindString(2, macAddress);
        }
        databaseStatement.bindLong(3, (long) healthSportItem.getYear());
        databaseStatement.bindLong(4, (long) healthSportItem.getMonth());
        databaseStatement.bindLong(5, (long) healthSportItem.getDay());
        databaseStatement.bindLong(6, (long) healthSportItem.getStepCount());
        databaseStatement.bindLong(7, (long) healthSportItem.getActiveTime());
        databaseStatement.bindLong(8, (long) healthSportItem.getCalory());
        databaseStatement.bindLong(9, (long) healthSportItem.getDistance());
        databaseStatement.bindLong(10, (long) healthSportItem.getAvgStep());
        databaseStatement.bindLong(11, (long) healthSportItem.getAvgActiveTime());
        databaseStatement.bindLong(12, (long) healthSportItem.getAvgCalory());
        databaseStatement.bindLong(13, (long) healthSportItem.getAvgDistance());
        databaseStatement.bindLong(14, healthSportItem.getDate());
        String userId = healthSportItem.getUserId();
        if (userId != null) {
            databaseStatement.bindString(15, userId);
        }
        String remark = healthSportItem.getRemark();
        if (remark != null) {
            databaseStatement.bindString(16, remark);
        }
        databaseStatement.bindLong(17, (long) healthSportItem.getItemCount());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, HealthSportItem healthSportItem) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, healthSportItem.getIsUploaded() ? 1 : 0);
        String macAddress = healthSportItem.getMacAddress();
        if (macAddress != null) {
            sQLiteStatement.bindString(2, macAddress);
        }
        sQLiteStatement.bindLong(3, (long) healthSportItem.getYear());
        sQLiteStatement.bindLong(4, (long) healthSportItem.getMonth());
        sQLiteStatement.bindLong(5, (long) healthSportItem.getDay());
        sQLiteStatement.bindLong(6, (long) healthSportItem.getStepCount());
        sQLiteStatement.bindLong(7, (long) healthSportItem.getActiveTime());
        sQLiteStatement.bindLong(8, (long) healthSportItem.getCalory());
        sQLiteStatement.bindLong(9, (long) healthSportItem.getDistance());
        sQLiteStatement.bindLong(10, (long) healthSportItem.getAvgStep());
        sQLiteStatement.bindLong(11, (long) healthSportItem.getAvgActiveTime());
        sQLiteStatement.bindLong(12, (long) healthSportItem.getAvgCalory());
        sQLiteStatement.bindLong(13, (long) healthSportItem.getAvgDistance());
        sQLiteStatement.bindLong(14, healthSportItem.getDate());
        String userId = healthSportItem.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(15, userId);
        }
        String remark = healthSportItem.getRemark();
        if (remark != null) {
            sQLiteStatement.bindString(16, remark);
        }
        sQLiteStatement.bindLong(17, (long) healthSportItem.getItemCount());
    }

    public HealthSportItem readEntity(Cursor cursor, int i) {
        Cursor cursor2 = cursor;
        boolean z = cursor2.getShort(i + 0) != 0;
        int i2 = i + 1;
        String string = cursor2.isNull(i2) ? null : cursor2.getString(i2);
        int i3 = cursor2.getInt(i + 2);
        int i4 = cursor2.getInt(i + 3);
        int i5 = cursor2.getInt(i + 4);
        int i6 = cursor2.getInt(i + 5);
        int i7 = cursor2.getInt(i + 6);
        int i8 = cursor2.getInt(i + 7);
        int i9 = cursor2.getInt(i + 8);
        int i10 = cursor2.getInt(i + 9);
        int i11 = cursor2.getInt(i + 10);
        int i12 = cursor2.getInt(i + 11);
        int i13 = cursor2.getInt(i + 12);
        long j = cursor2.getLong(i + 13);
        int i14 = i + 14;
        String string2 = cursor2.isNull(i14) ? null : cursor2.getString(i14);
        int i15 = i + 15;
        return new HealthSportItem(z, string, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, j, string2, cursor2.isNull(i15) ? null : cursor2.getString(i15), cursor2.getInt(i + 16));
    }

    public void readEntity(Cursor cursor, HealthSportItem healthSportItem, int i) {
        healthSportItem.setIsUploaded(cursor.getShort(i + 0) != 0);
        int i2 = i + 1;
        String str = null;
        healthSportItem.setMacAddress(cursor.isNull(i2) ? null : cursor.getString(i2));
        healthSportItem.setYear(cursor.getInt(i + 2));
        healthSportItem.setMonth(cursor.getInt(i + 3));
        healthSportItem.setDay(cursor.getInt(i + 4));
        healthSportItem.setStepCount(cursor.getInt(i + 5));
        healthSportItem.setActiveTime(cursor.getInt(i + 6));
        healthSportItem.setCalory(cursor.getInt(i + 7));
        healthSportItem.setDistance(cursor.getInt(i + 8));
        healthSportItem.setAvgStep(cursor.getInt(i + 9));
        healthSportItem.setAvgActiveTime(cursor.getInt(i + 10));
        healthSportItem.setAvgCalory(cursor.getInt(i + 11));
        healthSportItem.setAvgDistance(cursor.getInt(i + 12));
        healthSportItem.setDate(cursor.getLong(i + 13));
        int i3 = i + 14;
        healthSportItem.setUserId(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 15;
        if (!cursor.isNull(i4)) {
            str = cursor.getString(i4);
        }
        healthSportItem.setRemark(str);
        healthSportItem.setItemCount(cursor.getInt(i + 16));
    }
}
