package com.wade.fit.model.bean;

import java.util.Arrays;

public class AlarmBean {
    public int hour;
    public int min;
    public int type;
    public byte[] weeks = new byte[8];

    public String toString() {
        return "AlarmBean [state=, repeat=, hour=" + this.hour + ", min=" + this.min + ", type=" + this.type + ", weeks=" + Arrays.toString(this.weeks) + "]";
    }
}
