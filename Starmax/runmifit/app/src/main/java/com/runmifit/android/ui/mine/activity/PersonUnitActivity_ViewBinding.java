package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.PersonUnitActivity_ViewBinding */
public class PersonUnitActivity_ViewBinding implements Unbinder {
    private PersonUnitActivity target;
    private View view2131296335;
    private View view2131296985;
    private View view2131297015;
    private View view2131297021;

    public PersonUnitActivity_ViewBinding(PersonUnitActivity personUnitActivity) {
        this(personUnitActivity, personUnitActivity.getWindow().getDecorView());
    }

    public PersonUnitActivity_ViewBinding(final PersonUnitActivity personUnitActivity, View view) {
        this.target = personUnitActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.btnSave, "field 'btnSave' and method 'toNext'");
        personUnitActivity.btnSave = (Button) Utils.castView(findRequiredView, R.id.btnSave, "field 'btnSave'", Button.class);
        this.view2131296335 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.PersonUnitActivity_ViewBinding.C25921 */

            public void doClick(View view) {
                personUnitActivity.toNext();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.tvBrithday, "field 'tvBrithday' and method 'selectBrithday'");
        personUnitActivity.tvBrithday = (TextView) Utils.castView(findRequiredView2, R.id.tvBrithday, "field 'tvBrithday'", TextView.class);
        this.view2131296985 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.PersonUnitActivity_ViewBinding.C25932 */

            public void doClick(View view) {
                personUnitActivity.selectBrithday();
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.tvKm, "field 'tvKm' and method 'setUserUnitKm'");
        personUnitActivity.tvKm = (TextView) Utils.castView(findRequiredView3, R.id.tvKm, "field 'tvKm'", TextView.class);
        this.view2131297021 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.PersonUnitActivity_ViewBinding.C25943 */

            public void doClick(View view) {
                personUnitActivity.setUserUnitKm();
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.tvFt, "field 'tvFt' and method 'setUserUnitFt'");
        personUnitActivity.tvFt = (TextView) Utils.castView(findRequiredView4, R.id.tvFt, "field 'tvFt'", TextView.class);
        this.view2131297015 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.PersonUnitActivity_ViewBinding.C25954 */

            public void doClick(View view) {
                personUnitActivity.setUserUnitFt();
            }
        });
    }

    public void unbind() {
        PersonUnitActivity personUnitActivity = this.target;
        if (personUnitActivity != null) {
            this.target = null;
            personUnitActivity.btnSave = null;
            personUnitActivity.tvBrithday = null;
            personUnitActivity.tvKm = null;
            personUnitActivity.tvFt = null;
            this.view2131296335.setOnClickListener(null);
            this.view2131296335 = null;
            this.view2131296985.setOnClickListener(null);
            this.view2131296985 = null;
            this.view2131297021.setOnClickListener(null);
            this.view2131297021 = null;
            this.view2131297015.setOnClickListener(null);
            this.view2131297015 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
