package com.wade.fit.ui.device.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemLableValue;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.device.fragment.DeviceFragment_ViewBinding */
public class DeviceFragment_ViewBinding implements Unbinder {
    private DeviceFragment target;
    private View view2131296562;
    private View view2131297072;

    public DeviceFragment_ViewBinding(final DeviceFragment deviceFragment, View view) {
        this.target = deviceFragment;
        deviceFragment.tvMac = (TextView) Utils.findRequiredViewAsType(view, R.id.tvMac, "field 'tvMac'", TextView.class);
        deviceFragment.tvPower = (TextView) Utils.findRequiredViewAsType(view, R.id.tvPower, "field 'tvPower'", TextView.class);
        deviceFragment.tvDeviceName = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDeviceName, "field 'tvDeviceName'", TextView.class);
        deviceFragment.il_remind_phone = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.il_remind_phone, "field 'il_remind_phone'", ItemLableValue.class);
        deviceFragment.il_setting = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.il_setting, "field 'il_setting'", ItemLableValue.class);
        deviceFragment.ilGoalSet = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilGoalSet, "field 'ilGoalSet'", ItemLableValue.class);
        deviceFragment.ilTimeTest = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.ilTimeTest, "field 'ilTimeTest'", ItemToggleLayout.class);
        deviceFragment.ilUpHand = (ItemToggleLayout) Utils.findRequiredViewAsType(view, R.id.ilUpHand, "field 'ilUpHand'", ItemToggleLayout.class);
        deviceFragment.ilPhoto = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilPhoto, "field 'ilPhoto'", ItemLableValue.class);
        deviceFragment.ilFindDevice = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilFindDevice, "field 'ilFindDevice'", ItemLableValue.class);
        deviceFragment.ilMusicControl = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilMusicControl, "field 'ilMusicControl'", ItemLableValue.class);
        deviceFragment.ilReset = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilReset, "field 'ilReset'", ItemLableValue.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.tvUnbind, "field 'tvUnbind' and method 'bindClick'");
        deviceFragment.tvUnbind = (Button) Utils.castView(findRequiredView, R.id.tvUnbind, "field 'tvUnbind'", Button.class);
        this.view2131297072 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.fragment.DeviceFragment_ViewBinding.C24831 */

            public void doClick(View view) {
                deviceFragment.bindClick();
            }
        });
        deviceFragment.rlContent = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.rlContent, "field 'rlContent'", RelativeLayout.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ivDevice, "field 'ivDevice' and method 'bindDevice'");
        deviceFragment.ivDevice = (ImageView) Utils.castView(findRequiredView2, R.id.ivDevice, "field 'ivDevice'", ImageView.class);
        this.view2131296562 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.fragment.DeviceFragment_ViewBinding.C24842 */

            public void doClick(View view) {
                deviceFragment.bindDevice();
            }
        });
        deviceFragment.rlUnbid = Utils.findRequiredView(view, R.id.rlUnbid, "field 'rlUnbid'");
        deviceFragment.rlBind = Utils.findRequiredView(view, R.id.rlBind, "field 'rlBind'");
        deviceFragment.barBg = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.bar_bg, "field 'barBg'", RelativeLayout.class);
        deviceFragment.barBgTitle = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.bar_bg_title, "field 'barBgTitle'", RelativeLayout.class);
        deviceFragment.ilVersion = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.il_device_version, "field 'ilVersion'", ItemLableValue.class);
    }

    public void unbind() {
        DeviceFragment deviceFragment = this.target;
        if (deviceFragment != null) {
            this.target = null;
            deviceFragment.tvMac = null;
            deviceFragment.tvPower = null;
            deviceFragment.tvDeviceName = null;
            deviceFragment.il_remind_phone = null;
            deviceFragment.il_setting = null;
            deviceFragment.ilGoalSet = null;
            deviceFragment.ilTimeTest = null;
            deviceFragment.ilUpHand = null;
            deviceFragment.ilPhoto = null;
            deviceFragment.ilFindDevice = null;
            deviceFragment.ilMusicControl = null;
            deviceFragment.ilReset = null;
            deviceFragment.tvUnbind = null;
            deviceFragment.rlContent = null;
            deviceFragment.ivDevice = null;
            deviceFragment.rlUnbid = null;
            deviceFragment.rlBind = null;
            deviceFragment.barBg = null;
            deviceFragment.barBgTitle = null;
            deviceFragment.ilVersion = null;
            this.view2131297072.setOnClickListener(null);
            this.view2131297072 = null;
            this.view2131296562.setOnClickListener(null);
            this.view2131296562 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
