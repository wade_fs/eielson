package com.wade.fit.ui.mine.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemLableValue;

/* renamed from: com.wade.fit.ui.mine.fragment.MineFragment_ViewBinding */
public class MineFragment_ViewBinding implements Unbinder {
    private MineFragment target;
    private View view2131296498;
    private View view2131296507;
    private View view2131296512;
    private View view2131296516;
    private View view2131296568;
    private View view2131297075;

    public MineFragment_ViewBinding(final MineFragment mineFragment, View view) {
        this.target = mineFragment;
        View findRequiredView = Utils.findRequiredView(view, R.id.ilUserInfo, "field 'ilUserInfo' and method 'editUserInfo'");
        mineFragment.ilUserInfo = (ItemLableValue) Utils.castView(findRequiredView, R.id.ilUserInfo, "field 'ilUserInfo'", ItemLableValue.class);
        this.view2131296516 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.fragment.MineFragment_ViewBinding.C26011 */

            public void doClick(View view) {
                mineFragment.editUserInfo();
            }
        });
        mineFragment.ilNewUser = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilNewUser, "field 'ilNewUser'", ItemLableValue.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ilQuestion, "field 'ilQuestion' and method 'toFaq'");
        mineFragment.ilQuestion = (ItemLableValue) Utils.castView(findRequiredView2, R.id.ilQuestion, "field 'ilQuestion'", ItemLableValue.class);
        this.view2131296507 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.fragment.MineFragment_ViewBinding.C26022 */

            public void doClick(View view) {
                mineFragment.toFaq();
            }
        });
        mineFragment.ilAbout = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilAbout, "field 'ilAbout'", ItemLableValue.class);
        mineFragment.ilSafe = (ItemLableValue) Utils.findRequiredViewAsType(view, R.id.ilSafe, "field 'ilSafe'", ItemLableValue.class);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.ilFeedBack, "field 'ilFeedBack' and method 'toFeedBack'");
        mineFragment.ilFeedBack = (ItemLableValue) Utils.castView(findRequiredView3, R.id.ilFeedBack, "field 'ilFeedBack'", ItemLableValue.class);
        this.view2131296498 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.fragment.MineFragment_ViewBinding.C26033 */

            public void doClick(View view) {
                mineFragment.toFeedBack();
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.ivHeader, "field 'ivHeader' and method 'toLogin'");
        mineFragment.ivHeader = (ImageView) Utils.castView(findRequiredView4, R.id.ivHeader, "field 'ivHeader'", ImageView.class);
        this.view2131296568 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.fragment.MineFragment_ViewBinding.C26044 */

            public void doClick(View view) {
                mineFragment.toLogin();
            }
        });
        mineFragment.rlContent = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.rlContent, "field 'rlContent'", RelativeLayout.class);
        View findRequiredView5 = Utils.findRequiredView(view, R.id.tvUserName, "field 'tvUserName' and method 'toLogin'");
        mineFragment.tvUserName = (TextView) Utils.castView(findRequiredView5, R.id.tvUserName, "field 'tvUserName'", TextView.class);
        this.view2131297075 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.fragment.MineFragment_ViewBinding.C26055 */

            public void doClick(View view) {
                mineFragment.toLogin();
            }
        });
        mineFragment.imgBackground = (ImageView) Utils.findRequiredViewAsType(view, R.id.imgBackground, "field 'imgBackground'", ImageView.class);
        mineFragment.rlScreenTitle = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.rlScreenTitle, "field 'rlScreenTitle'", RelativeLayout.class);
        View findRequiredView6 = Utils.findRequiredView(view, R.id.ilSystem, "method 'editUnit'");
        this.view2131296512 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.fragment.MineFragment_ViewBinding.C26066 */

            public void doClick(View view) {
                mineFragment.editUnit();
            }
        });
    }

    public void unbind() {
        MineFragment mineFragment = this.target;
        if (mineFragment != null) {
            this.target = null;
            mineFragment.ilUserInfo = null;
            mineFragment.ilNewUser = null;
            mineFragment.ilQuestion = null;
            mineFragment.ilAbout = null;
            mineFragment.ilSafe = null;
            mineFragment.ilFeedBack = null;
            mineFragment.ivHeader = null;
            mineFragment.rlContent = null;
            mineFragment.tvUserName = null;
            mineFragment.imgBackground = null;
            mineFragment.rlScreenTitle = null;
            this.view2131296516.setOnClickListener(null);
            this.view2131296516 = null;
            this.view2131296507.setOnClickListener(null);
            this.view2131296507 = null;
            this.view2131296498.setOnClickListener(null);
            this.view2131296498 = null;
            this.view2131296568.setOnClickListener(null);
            this.view2131296568 = null;
            this.view2131297075.setOnClickListener(null);
            this.view2131297075 = null;
            this.view2131296512.setOnClickListener(null);
            this.view2131296512 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
