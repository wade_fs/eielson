package com.wade.fit.ui.main.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.baidu.mobstat.StatService;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.Alarm;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.ui.mine.activity.PersonUnitActivity;
import com.wade.fit.persenter.main.LoginContract;
import com.wade.fit.persenter.main.LoginPresenter;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ThreadUtil;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.Utils;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.util.thirddataplatform.GoogleFitPresenter;
import java.util.Locale;

/* renamed from: com.wade.fit.ui.main.activity.WelcomeActivity */
public class WelcomeActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.View {
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 200;
    GoogleFitPresenter googleFitPresenter;
    /* access modifiers changed from: private */
    public Handler mHandle = new Handler();
    Runnable timeRunnable = new Runnable() {
        /* class com.wade.fit.ui.main.activity.WelcomeActivity.C15392 */

        public void run() {
            WelcomeActivity.access$008(WelcomeActivity.this);
            if (WelcomeActivity.this.timerLca == 10) {
                int unused = WelcomeActivity.this.timerLca = 0;
                WelcomeActivity.this.jumpActivity();
                WelcomeActivity.this.mHandle.removeCallbacks(WelcomeActivity.this.timeRunnable);
                return;
            }
            WelcomeActivity.this.mHandle.postDelayed(this, 1000);
        }
    };
    /* access modifiers changed from: private */
    public int timerLca = 0;
    private String token;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_welcome;
    }

    public void registerFaild(int i) {
    }

    public void registerSuccess() {
    }

    public void sendCodeFaild(int i) {
    }

    public void sendCodeSuccess() {
    }

    static /* synthetic */ int access$008(WelcomeActivity welcomeActivity) {
        int i = welcomeActivity.timerLca;
        welcomeActivity.timerLca = i + 1;
        return i;
    }

    public void requestPermissionsFail(int i) {
        if (TextUtils.isEmpty(this.token)) {
            showWebViewDialog();
        } else {
            autoLogin();
        }
    }

    public void requestPermissionsSuccess(int i) {
        super.requestPermissionsSuccess(i);
        if (TextUtils.isEmpty(this.token)) {
            showWebViewDialog();
        } else {
            autoLogin();
        }
    }

    private void showWebViewDialog() {
        Locale locale;
        String str;
        if (SharePreferenceUtils.getBool(this, Constants.IS_PRIVACY, true)) {
            View inflate = LayoutInflater.from(this).inflate((int) R.layout.activity_web_view, (ViewGroup) null);
            final Dialog dialog = new Dialog(this, R.style.theme_dialog_aphe);
            WebView webView = (WebView) inflate.findViewById(R.id.wv_privacy_policy);
            inflate.findViewById(R.id.btnAgree).setOnClickListener(new View.OnClickListener(dialog) {
                /* class com.wade.fit.ui.main.activity.$$Lambda$WelcomeActivity$cSCHRWWSETxDM9qzvdIwtP6V54g */
                private final /* synthetic */ Dialog f$1;

                {
                    this.f$1 = r2;
                }

                public final void onClick(View view) {
                    WelcomeActivity.this.lambda$showWebViewDialog$0$WelcomeActivity(this.f$1, view);
                }
            });
            inflate.findViewById(R.id.btnCancal).setOnClickListener(new View.OnClickListener(dialog) {
                /* class com.wade.fit.ui.main.activity.$$Lambda$WelcomeActivity$U1UiphPHCwEu2Fd_XDgfXOyedus */
                private final /* synthetic */ Dialog f$1;

                {
                    this.f$1 = r2;
                }

                public final void onClick(View view) {
                    WelcomeActivity.this.lambda$showWebViewDialog$1$WelcomeActivity(this.f$1, view);
                }
            });
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setDomStorageEnabled(true);
            webView.setWebViewClient(new WebViewClient() {
                /* class com.wade.fit.ui.main.activity.WelcomeActivity.C15381 */

                public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                }

                public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    webView.loadUrl(str);
                    return true;
                }

                public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(webView.getContext());
                    builder.setMessage("SSL认证失败，是否继续访问？");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener(sslErrorHandler) {
                        /* class com.wade.fit.ui.main.activity.$$Lambda$WelcomeActivity$1$R00MpomjlZPpzad2m62mjxb9so */
                        private final /* synthetic */ SslErrorHandler f$0;

                        {
                            this.f$0 = r1;
                        }

                        public final void onClick(DialogInterface dialogInterface, int i) {
                            this.f$0.proceed();
                        }
                    });
                    builder.setNegativeButton("取消", new DialogInterface.OnClickListener(sslErrorHandler) {
                        /* class com.wade.fit.ui.main.activity.$$Lambda$WelcomeActivity$1$9Tr10UMcqbzLDAKR3A3ofWxN6Ys */
                        private final /* synthetic */ SslErrorHandler f$0;

                        {
                            this.f$0 = r1;
                        }

                        public final void onClick(DialogInterface dialogInterface, int i) {
                            this.f$0.cancel();
                        }
                    });
                    builder.create().show();
                }

                public void onPageFinished(WebView webView, String str) {
                    dialog.show();
                }
            });
            if (Build.VERSION.SDK_INT >= 24) {
                locale = getResources().getConfiguration().getLocales().get(0);
            } else {
                locale = getResources().getConfiguration().locale;
            }
            String language = locale.getLanguage();
            if (language.contains("zh")) {
                str = locale.getCountry().equals("TW") ? "file:///android_asset/privacypolicy_tw.html" : "file:///android_asset/privacypolicy.html";
            } else if (language.contains("fr")) {
                str = "file:///android_asset/privacypolicy_fr.html";
            } else if (language.contains("es")) {
                str = "file:///android_asset/privacypolicy_es.html";
            } else if (language.contains("de")) {
                str = "file:///android_asset/privacypolicy_de.html";
            } else if (language.contains("ja")) {
                str = "file:///android_asset/privacypolicy_ja.html";
            } else {
                str = language.contains("it") ? "file:///android_asset/privacypolicy_it.html" : "file:///android_asset/privacypolicyEh.html";
            }
            webView.loadUrl(str);
            dialog.setContentView(inflate);
            dialog.setCancelable(true);
            dialog.show();
            return;
        }
        jumpActivity();
    }

    public /* synthetic */ void lambda$showWebViewDialog$0$WelcomeActivity(Dialog dialog, View view) {
        SharePreferenceUtils.putBool(this, Constants.IS_PRIVACY, false);
        dialog.dismiss();
        jumpActivity();
    }

    public /* synthetic */ void lambda$showWebViewDialog$1$WelcomeActivity(Dialog dialog, View view) {
        SharePreferenceUtils.putBool(this, Constants.IS_PRIVACY, true);
        dialog.dismiss();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        LogUtil.d("登录结果：" + i2);
        if (i2 == -1) {
            SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 1);
            GoogleFitPresenter.getInstance().handleSignInResult(i, intent);
        } else {
            SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 0);
        }
        jumpActivity();
    }

    /* access modifiers changed from: private */
    public void jumpActivity() {
        ThreadUtil.delayTask(new Runnable() {
            /* class com.wade.fit.ui.main.activity.$$Lambda$WelcomeActivity$OD67vRYbPqrHBcljq7NPcF6Z_8 */

            public final void run() {
                WelcomeActivity.this.lambda$jumpActivity$2$WelcomeActivity();
            }
        }, 2000);
    }

    public /* synthetic */ void lambda$jumpActivity$2$WelcomeActivity() {
        AppApplication.getInstance().setUserBean(SPHelper.getCurrentUserBean());
        AppApplication.getInstance().setDatabase();
        IntentUtil.goToActivityAndFinish(this, MainActivity.class);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        if ((getIntent().getFlags() & 4194304) != 0) {
            finish();
            return;
        }
        this.googleFitPresenter = GoogleFitPresenter.getInstance(this);
        this.layoutTitle.setVisibility(View.GONE);
        StatService.start(this);
        this.token = SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, "");
        if (!checkSelfPermission(Utils.appPermissions())) {
            requestPermissions(200, Utils.appPermissions());
        } else {
            showWebViewDialog();
        }
    }

    private void autoLogin() {
        ThreadUtil.delayTask(new Runnable() {
            /* class com.wade.fit.ui.main.activity.$$Lambda$WelcomeActivity$a2DXltHA0t4fzMqPfkKuHSf13Zo */

            public final void run() {
                WelcomeActivity.this.lambda$autoLogin$3$WelcomeActivity();
            }
        }, 2000);
    }

    public /* synthetic */ void lambda$autoLogin$3$WelcomeActivity() {
        int i = SharePreferenceUtils.getInt(this, Constants.LOGIN_TYPE, 1);
        String string = SharePreferenceUtils.getString(this, Constants.USER_NAME);
        String string2 = SharePreferenceUtils.getString(this, Constants.USER_PASSWORD);
        if (i == 1) {
            ((LoginPresenter) this.mPresenter).requestLogin(string, string2);
        } else if (i == 2) {
            ((LoginPresenter) this.mPresenter).requestLoginByWechat(string, string2);
        } else if (i == 3) {
            ((LoginPresenter) this.mPresenter).loginByFacebook(string, string2);
        }
    }

    public void LoginSuccess(int i, BaseBean baseBean) {
        UserBean userBean = (UserBean) baseBean.getData();
        SharePreferenceUtils.putString(Constants.USER_TOKEN, userBean.getToken());
        if (!userBean.isFirstLogin()) {
            if (userBean.getBirthday() == null || TextUtils.isEmpty(userBean.getBirthday())) {
                userBean.setYear(DateUtil.todayYearMonthDay()[0] - 20);
                userBean.setMonth(1);
                userBean.setDay(1);
                userBean.setBirthday(userBean.getYear() + "-" + userBean.getMonth() + "-" + userBean.getDay());
            } else {
                String[] split = userBean.getBirthday().split("-");
                userBean.setYear(Integer.parseInt(split[0]));
                userBean.setMonth(Integer.parseInt(split[1]));
                userBean.setDay(Integer.parseInt(split[2]));
            }
            userBean.setHeightLb(UnitUtil.cm2inchs(userBean.getHeight()));
            userBean.setWeightLb(UnitUtil.kg2lb((float) userBean.getWeight()));
            SPHelper.saveUserBean(userBean);
            AppApplication.getInstance().setUserBean(userBean);
            AppApplication.getInstance().setDatabase();
            IntentUtil.goToActivityAndFinish(this, MainActivity.class);
            return;
        }
        userBean.setYear(DateUtil.todayYearMonthDay()[0] - 20);
        userBean.setMonth(1);
        userBean.setDay(1);
        userBean.setBirthday(userBean.getYear() + "-" + userBean.getMonth() + "-" + userBean.getDay());
        userBean.setUnit(0);
        userBean.setHeight(Alarm.STATUS_NOT_DISPLAY);
        userBean.setHeightLb(68);
        userBean.setWeight(Constants.DEFAULT_WEIGHT_KG);
        userBean.setWeightLb(132);
        userBean.setWeightSt(10);
        userBean.setStepDistance(75);
        AppApplication.getInstance().setUserBean(userBean);
        AppApplication.getInstance().setDatabase();
        SPHelper.saveUserBean(userBean);
        IntentUtil.goToActivityAndFinish(this, PersonUnitActivity.class);
    }

    public void loginFaild(int i) {
        AppApplication.getInstance().setUserBean(SPHelper.getCurrentUserBean());
        AppApplication.getInstance().setDatabase();
        IntentUtil.goToActivityAndFinish(this, MainActivity.class);
    }
}
