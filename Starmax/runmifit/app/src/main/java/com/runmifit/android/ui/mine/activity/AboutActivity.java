package com.wade.fit.ui.mine.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.util.AppUtil;
import com.wade.fit.util.ButtonUtils;
import java.util.Locale;

/* renamed from: com.wade.fit.ui.mine.activity.AboutActivity */
public class AboutActivity extends BaseActivity {
    TextView privacy;
    TextView tvAppUpdate;
    TextView tvVersion;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_about;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.about_title));
        TextView textView = this.tvVersion;
        textView.setText(getResources().getString(R.string.app_version_info) + Config.TRACE_TODAY_VISIT_SPLIT + AppUtil.getVersionName(this));
    }

    public void onClick(View view) {
        if (view.getId() == R.id.tvAppUpdate) {
            showToast(getResources().getString(R.string.current_new_device));
        }
    }

    public void onShow(View view) {
        if (!ButtonUtils.isFastDoubleClick(R.id.privacy)) {
            showWebViewDialog();
        }
    }

    private void showWebViewDialog() {
        Locale locale;
        String str;
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.activity_web_view, (ViewGroup) null);
        final Dialog dialog = new Dialog(this, R.style.theme_dialog_aphe);
        WebView webView = (WebView) inflate.findViewById(R.id.wv_privacy_policy);
        inflate.findViewById(R.id.btnAgree).setVisibility(View.GONE);
        inflate.findViewById(R.id.btnCancal).setVisibility(View.GONE);
        inflate.findViewById(R.id.tvTips).setVisibility(View.GONE);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.back);
        imageView.setVisibility(View.VISIBLE);
        imageView.setOnClickListener(new View.OnClickListener(dialog) {
            /* class com.wade.fit.ui.mine.activity.$$Lambda$AboutActivity$91rngidgGFCfGowZePXG0sUkEFw */
            private final /* synthetic */ Dialog f$0;

            {
                this.f$0 = r1;
            }

            public final void onClick(View view) {
                this.f$0.dismiss();
            }
        });
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            /* class com.wade.fit.ui.mine.activity.AboutActivity.C25691 */

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                return true;
            }

            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                AlertDialog.Builder builder = new AlertDialog.Builder(webView.getContext());
                builder.setMessage("SSL认证失败，是否继续访问？");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener(sslErrorHandler) {
                    /* class com.wade.fit.ui.mine.activity.$$Lambda$AboutActivity$1$YSuLTMrGS5jh_dqHK5h0qlb4NhU */
                    private final /* synthetic */ SslErrorHandler f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void onClick(DialogInterface dialogInterface, int i) {
                        this.f$0.proceed();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener(sslErrorHandler) {
                    /* class com.wade.fit.ui.mine.activity.$$Lambda$AboutActivity$1$e7RJSq4ct2eRYV053EkrvATiTCI */
                    private final /* synthetic */ SslErrorHandler f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void onClick(DialogInterface dialogInterface, int i) {
                        this.f$0.cancel();
                    }
                });
                builder.create().show();
            }

            public void onPageFinished(WebView webView, String str) {
                dialog.show();
            }
        });
        if (Build.VERSION.SDK_INT >= 24) {
            locale = getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = getResources().getConfiguration().locale;
        }
        String language = locale.getLanguage();
        if (language.contains("zh")) {
            str = locale.getCountry().equals("TW") ? "file:///android_asset/privacypolicy_tw.html" : "file:///android_asset/privacypolicy.html";
        } else if (language.contains("fr")) {
            str = "file:///android_asset/privacypolicy_fr.html";
        } else if (language.contains("es")) {
            str = "file:///android_asset/privacypolicy_es.html";
        } else if (language.contains("de")) {
            str = "file:///android_asset/privacypolicy_de.html";
        } else if (language.contains("ja")) {
            str = "file:///android_asset/privacypolicy_ja.html";
        } else {
            str = language.contains("it") ? "file:///android_asset/privacypolicy_it.html" : "file:///android_asset/privacypolicyEh.html";
        }
        webView.loadUrl(str);
        dialog.setContentView(inflate);
        dialog.setCancelable(true);
        dialog.show();
    }
}
