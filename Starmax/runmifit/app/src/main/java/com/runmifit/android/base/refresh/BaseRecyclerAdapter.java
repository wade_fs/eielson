package com.wade.fit.base.refresh;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wade.fit.R;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter<T> extends RecyclerView.Adapter {
    public static final int BOTH_HEADER_FOOTER = 3;
    public static final int NEITHER = 0;
    public static final int ONLY_FOOTER = 2;
    public static final int ONLY_HEADER = 1;
    public static final int STATE_HIDE = 5;
    public static final int STATE_INVALID_NETWORK = 3;
    public static final int STATE_LOADING = 8;
    public static final int STATE_LOAD_ERROR = 7;
    public static final int STATE_LOAD_MORE = 2;
    public static final int STATE_NO_MORE = 1;
    public static final int STATE_REFRESHING = 6;
    private static final int VIEW_TYPE_FOOTER = -2;
    private static final int VIEW_TYPE_HEADER = -1;
    public static final int VIEW_TYPE_NORMAL = 0;
    private final int BEHAVIOR_MODE;
    protected OnCustomClickListener customClickListener;
    protected Context mContext;
    protected View mHeaderView;
    protected LayoutInflater mInflater;
    public List<T> mItems = new ArrayList();
    protected int mSelectedPosition = 0;
    protected int mState;
    protected String mSystemTime;
    private OnClickListener onClickListener;
    protected OnItemClickListener onItemClickListener;
    /* access modifiers changed from: private */
    public OnItemLongClickListener onItemLongClickListener;
    private OnLoadingHeaderCallBack onLoadingHeaderCallBack;
    private OnLongClickListener onLongClickListener;
    private RetryOnClickListener retryOnClickListener;

    public interface OnCustomClickListener {
        void onCustomClick(View view, int i);
    }

    public interface OnItemClickListener {
        void onItemClick(int i, long j);
    }

    public interface OnItemLongClickListener {
        void onLongClick(int i, long j);
    }

    public interface OnLoadingHeaderCallBack {
        void onBindHeaderHolder(RecyclerView.ViewHolder viewHolder, int i);

        RecyclerView.ViewHolder onCreateHeaderHolder(ViewGroup viewGroup);
    }

    public interface RetryOnClickListener {
        void retryClick();
    }

    /* access modifiers changed from: protected */
    public abstract void onBindDefaultViewHolder(RecyclerView.ViewHolder viewHolder, T t, int i);

    /* access modifiers changed from: protected */
    public abstract RecyclerView.ViewHolder onCreateDefaultViewHolder(ViewGroup viewGroup, int i);

    public BaseRecyclerAdapter(Context context, int i) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.BEHAVIOR_MODE = i;
        this.mState = 5;
        initListener();
    }

    private void initListener() {
        this.onClickListener = new OnClickListener() {
            /* class com.wade.fit.base.refresh.BaseRecyclerAdapter.C23861 */

            public void onClick(int i, long j) {
                if (BaseRecyclerAdapter.this.onItemClickListener != null) {
                    BaseRecyclerAdapter.this.onItemClickListener.onItemClick(i, j);
                }
            }
        };
        this.onLongClickListener = new OnLongClickListener() {
            /* class com.wade.fit.base.refresh.BaseRecyclerAdapter.C23872 */

            public boolean onLongClick(int i, long j) {
                if (BaseRecyclerAdapter.this.onItemLongClickListener == null) {
                    return false;
                }
                BaseRecyclerAdapter.this.onItemLongClickListener.onLongClick(i, j);
                return true;
            }
        };
    }

    public void setSystemTime(String str) {
        this.mSystemTime = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == -2) {
            return new FooterViewHolder(this.mInflater.inflate((int) R.layout.recycler_footer_view, viewGroup, false));
        }
        if (i != -1) {
            RecyclerView.ViewHolder onCreateDefaultViewHolder = onCreateDefaultViewHolder(viewGroup, i);
            if (onCreateDefaultViewHolder != null) {
                onCreateDefaultViewHolder.itemView.setTag(onCreateDefaultViewHolder);
                onCreateDefaultViewHolder.itemView.setOnLongClickListener(this.onLongClickListener);
                onCreateDefaultViewHolder.itemView.setOnClickListener(this.onClickListener);
            }
            return onCreateDefaultViewHolder;
        }
        OnLoadingHeaderCallBack onLoadingHeaderCallBack2 = this.onLoadingHeaderCallBack;
        if (onLoadingHeaderCallBack2 != null) {
            return onLoadingHeaderCallBack2.onCreateHeaderHolder(viewGroup);
        }
        throw new IllegalArgumentException("you have to impl the interface when using this viewType");
    }

    public void setCustomClickListener(OnCustomClickListener onCustomClickListener) {
        this.customClickListener = onCustomClickListener;
    }

    public void setRetryOnClickListener(RetryOnClickListener retryOnClickListener2) {
        this.retryOnClickListener = retryOnClickListener2;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        int itemViewType = viewHolder.getItemViewType();
        if (itemViewType == -2) {
            FooterViewHolder footerViewHolder = (FooterViewHolder) viewHolder;
            footerViewHolder.itemView.setVisibility(View.VISIBLE);
            footerViewHolder.llNoDate.setVisibility(View.GONE);
            footerViewHolder.llNetError.setVisibility(View.GONE);
            switch (this.mState) {
                case 1:
                    footerViewHolder.tv_footer.setText(this.mContext.getResources().getString(R.string.state_not_more));
                    footerViewHolder.pb_footer.setVisibility(View.GONE);
                    if (this.mItems.size() == 0) {
                        footerViewHolder.llNoDate.setVisibility(View.VISIBLE);
                        footerViewHolder.noDataImg.setImageResource(R.mipmap.ic_no_data);
                        footerViewHolder.noDateText.setText(this.mContext.getResources().getString(R.string.state_no_data));
                        footerViewHolder.noDateTextLitter.setText("");
                        footerViewHolder.layout_foot.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                        return;
                    }
                    footerViewHolder.llNoDate.setVisibility(View.GONE);
                    footerViewHolder.layout_foot.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                    return;
                case 2:
                case 8:
                    footerViewHolder.tv_footer.setText(this.mContext.getResources().getString(R.string.state_loading));
                    footerViewHolder.pb_footer.setVisibility(View.VISIBLE);
                    return;
                case 3:
                    this.mItems.clear();
                    resetItem(this.mItems);
                    footerViewHolder.tv_footer.setText(this.mContext.getResources().getString(R.string.state_network_error));
                    footerViewHolder.pb_footer.setVisibility(View.GONE);
                    if (this.mItems.size() == 0) {
                        footerViewHolder.llNetError.setVisibility(View.VISIBLE);
                        return;
                    }
                    return;
                case 4:
                default:
                    return;
                case 5:
                    footerViewHolder.itemView.setVisibility(View.GONE);
                    return;
                case 6:
                    footerViewHolder.tv_footer.setText(this.mContext.getResources().getString(R.string.state_refreshing));
                    footerViewHolder.pb_footer.setVisibility(View.GONE);
                    return;
                case 7:
                    footerViewHolder.tv_footer.setText(this.mContext.getResources().getString(R.string.state_load_error));
                    footerViewHolder.pb_footer.setVisibility(View.GONE);
                    return;
            }
        } else if (itemViewType != -1) {
            onBindDefaultViewHolder(viewHolder, getItems().get(getIndex(i)), i);
        } else {
            OnLoadingHeaderCallBack onLoadingHeaderCallBack2 = this.onLoadingHeaderCallBack;
            if (onLoadingHeaderCallBack2 != null) {
                onLoadingHeaderCallBack2.onBindHeaderHolder(viewHolder, i);
            }
        }
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                /* class com.wade.fit.base.refresh.BaseRecyclerAdapter.C23883 */

                public int getSpanSize(int i) {
                    if (BaseRecyclerAdapter.this.getItemViewType(i) == -1 || BaseRecyclerAdapter.this.getItemViewType(i) == -2) {
                        return gridLayoutManager.getSpanCount();
                    }
                    return 1;
                }
            });
        }
    }

    public void onViewAttachedToWindow(RecyclerView.ViewHolder viewHolder) {
        ViewGroup.LayoutParams layoutParams = viewHolder.itemView.getLayoutParams();
        if (layoutParams != null && (layoutParams instanceof StaggeredGridLayoutManager.LayoutParams)) {
            StaggeredGridLayoutManager.LayoutParams layoutParams2 = (StaggeredGridLayoutManager.LayoutParams) layoutParams;
            int i = this.BEHAVIOR_MODE;
            boolean z = false;
            if (i == 1) {
                if (viewHolder.getLayoutPosition() == 0) {
                    z = true;
                }
                layoutParams2.setFullSpan(z);
            } else if (i == 2) {
                if (viewHolder.getLayoutPosition() == this.mItems.size() + 1) {
                    z = true;
                }
                layoutParams2.setFullSpan(z);
            } else if (i != 3) {
            } else {
                if (viewHolder.getLayoutPosition() == 0 || viewHolder.getLayoutPosition() == this.mItems.size() + 1) {
                    layoutParams2.setFullSpan(true);
                }
            }
        }
    }

    public int getItemViewType(int i) {
        int i2;
        if (i == 0 && ((i2 = this.BEHAVIOR_MODE) == 1 || i2 == 3)) {
            return -1;
        }
        if (i + 1 != getItemCount()) {
            return 0;
        }
        int i3 = this.BEHAVIOR_MODE;
        return (i3 == 2 || i3 == 3) ? -2 : 0;
    }

    public T getSelectedItem() {
        int i = this.mSelectedPosition;
        if (i < 0 || i >= this.mItems.size()) {
            return null;
        }
        return this.mItems.get(this.mSelectedPosition);
    }

    public void setSelectedPosition(int i) {
        int i2 = this.mSelectedPosition;
        if (i != i2) {
            updateItem(i2);
            this.mSelectedPosition = i;
            updateItem(this.mSelectedPosition);
        }
        this.mSelectedPosition = i;
        updateItem(i);
    }

    /* access modifiers changed from: protected */
    public int getIndex(int i) {
        int i2 = this.BEHAVIOR_MODE;
        return (i2 == 1 || i2 == 3) ? i - 1 : i;
    }

    public int getItemCount() {
        int i = this.BEHAVIOR_MODE;
        if (i == 2 || i == 1) {
            return this.mItems.size() + 1;
        }
        if (i == 3) {
            return this.mItems.size() + 2;
        }
        return this.mItems.size();
    }

    public int getCount() {
        return this.mItems.size();
    }

    public final View getHeaderView() {
        return this.mHeaderView;
    }

    public final void setHeaderView(View view) {
        this.mHeaderView = view;
    }

    public final List<T> getItems() {
        return this.mItems;
    }

    public void addAll(List<T> list) {
        if (list != null) {
            try {
                this.mItems.addAll(list);
                notifyItemRangeInserted(this.mItems.size(), list.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void addItem(T t) {
        if (t != null) {
            this.mItems.add(t);
            notifyItemChanged(this.mItems.size());
        }
    }

    public void addItem(int i, T t) {
        if (t != null) {
            this.mItems.add(getIndex(i), t);
            notifyItemInserted(i);
        }
    }

    public void replaceItem(int i, T t) {
        if (t != null) {
            this.mItems.set(getIndex(i), t);
            notifyItemChanged(i);
        }
    }

    public void updateItem(int i) {
        if (getItemCount() > i) {
            notifyItemChanged(i);
        }
    }

    public final void removeItem(T t) {
        if (this.mItems.contains(t)) {
            int indexOf = this.mItems.indexOf(t);
            this.mItems.remove(t);
            notifyItemRemoved(indexOf);
        }
    }

    public final void removeItem(int i) {
        if (getItemCount() > i) {
            this.mItems.remove(getIndex(i));
            notifyItemRemoved(i);
        }
    }

    public final T getItem(int i) {
        int index = getIndex(i);
        if (index < 0 || index >= this.mItems.size()) {
            return null;
        }
        return this.mItems.get(getIndex(i));
    }

    public final void resetItem(List<T> list) {
        if (list != null) {
            clear();
            addAll(list);
        }
    }

    public final void clear() {
        try {
            this.mItems.clear();
            setState(5, false);
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setState(int i, boolean z) {
        this.mState = i;
        if (i == 3) {
            notifyDataSetChanged();
        } else if (z) {
            updateItem(getItemCount() - 1);
        }
    }

    public void setState(int i, boolean z, int i2) {
        this.mState = i;
        if (i == 3) {
            notifyDataSetChanged();
        } else if (z) {
            updateItem(getItemCount() - 1);
        }
    }

    public int getState() {
        return this.mState;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener2) {
        this.onItemClickListener = onItemClickListener2;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener2) {
        this.onItemLongClickListener = onItemLongClickListener2;
    }

    /* access modifiers changed from: protected */
    public void setOnLoadingHeaderCallBack(OnLoadingHeaderCallBack onLoadingHeaderCallBack2) {
        this.onLoadingHeaderCallBack = onLoadingHeaderCallBack2;
    }

    public static abstract class OnClickListener implements View.OnClickListener {
        public abstract void onClick(int i, long j);

        public void onClick(View view) {
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            onClick(viewHolder.getAdapterPosition(), viewHolder.getItemId());
        }
    }

    public static abstract class OnLongClickListener implements View.OnLongClickListener {
        public abstract boolean onLongClick(int i, long j);

        public boolean onLongClick(View view) {
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            return onLongClick(viewHolder.getAdapterPosition(), viewHolder.getItemId());
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout_foot;
        LinearLayout llNetError;
        LinearLayout llNoDate;
        ImageView noDataImg;
        TextView noDateText;
        TextView noDateTextLitter;
        ProgressBar pb_footer;
        TextView tv_footer;

        FooterViewHolder(View view) {
            super(view);
            this.pb_footer = (ProgressBar) view.findViewById(R.id.pb_footer);
            this.tv_footer = (TextView) view.findViewById(R.id.tv_footer);
            this.llNoDate = (LinearLayout) view.findViewById(R.id.ll_load_no_date);
            this.noDataImg = (ImageView) view.findViewById(R.id.module_base_id_empty_img);
            this.noDateText = (TextView) view.findViewById(R.id.module_base_empty_text);
            this.noDateTextLitter = (TextView) view.findViewById(R.id.module_base_empty_text_litter);
            this.llNetError = (LinearLayout) view.findViewById(R.id.ll_net_error);
            this.layout_foot = (RelativeLayout) view.findViewById(R.id.layout_foot);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }
}
