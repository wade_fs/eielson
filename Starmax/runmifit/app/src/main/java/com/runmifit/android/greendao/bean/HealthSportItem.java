package com.wade.fit.greendao.bean;

public class HealthSportItem {
    private static final long serialVersionUID = 1;
    private int activeTime;
    private int avgActiveTime;
    private int avgCalory;
    private int avgDistance;
    private int avgStep;
    private int calory;
    private long date;
    private int day;
    private int distance;
    private boolean isUploaded;
    private int itemCount;
    private String macAddress;
    private int month;
    private String remark;
    private int stepCount;
    private String userId;
    private int year;

    public int getItemCount() {
        return this.itemCount;
    }

    public void setItemCount(int i) {
        this.itemCount = i;
    }

    public HealthSportItem clone() {
        try {
            return (HealthSportItem) super.clone();
        } catch (Exception unused) {
            return null;
        }
    }

    public HealthSportItem() {
    }

    public HealthSportItem(boolean z, String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, long j, String str2, String str3, int i12) {
        this.isUploaded = z;
        this.macAddress = str;
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.stepCount = i4;
        this.activeTime = i5;
        this.calory = i6;
        this.distance = i7;
        this.avgStep = i8;
        this.avgActiveTime = i9;
        this.avgCalory = i10;
        this.avgDistance = i11;
        this.date = j;
        this.userId = str2;
        this.remark = str3;
        this.itemCount = i12;
    }

    public int getAvgStep() {
        return this.avgStep;
    }

    public void setAvgStep(int i) {
        this.avgStep = i;
    }

    public int getAvgActiveTime() {
        return this.avgActiveTime;
    }

    public void setAvgActiveTime(int i) {
        this.avgActiveTime = i;
    }

    public int getAvgCalory() {
        return this.avgCalory;
    }

    public void setAvgCalory(int i) {
        this.avgCalory = i;
    }

    public int getAvgDistance() {
        return this.avgDistance;
    }

    public void setAvgDistance(int i) {
        this.avgDistance = i;
    }

    public boolean getIsUploaded() {
        return this.isUploaded;
    }

    public void setIsUploaded(boolean z) {
        this.isUploaded = z;
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getStepCount() {
        return this.stepCount;
    }

    public void setStepCount(int i) {
        this.stepCount = i;
    }

    public int getActiveTime() {
        return this.activeTime;
    }

    public void setActiveTime(int i) {
        this.activeTime = i;
    }

    public int getCalory() {
        return this.calory;
    }

    public void setCalory(int i) {
        this.calory = i;
    }

    public int getDistance() {
        return this.distance;
    }

    public void setDistance(int i) {
        this.distance = i;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public String toString() {
        return "HealthSportItem{isUploaded=" + this.isUploaded + ", year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", stepCount=" + this.stepCount + ", activeTime=" + this.activeTime + ", calory=" + this.calory + ", distance=" + this.distance + ", date=" + this.date + '}';
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String str) {
        this.remark = str;
    }
}
