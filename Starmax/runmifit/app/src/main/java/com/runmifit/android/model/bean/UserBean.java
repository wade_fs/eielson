package com.wade.fit.model.bean;

import com.baidu.mobstat.Config;
import com.google.gson.annotations.SerializedName;
import com.wade.fit.app.Constants;
import java.io.Serializable;
import java.util.Objects;

public class UserBean implements Serializable, Cloneable {
    @SerializedName("email")
    private String account;
    private int age = 20;
    private String birthday;
    private String coverImage;
    private int day = 1;
    private boolean firstLogin;
    @SerializedName("sex")
    private int gender = 0;
    @SerializedName("avatar")
    private String headerUrl;
    private int height = 175;
    private int heightLb = 108;
    private int month = 1;
    private String openId;
    private String pwd;
    private String sessionId;
    private int stepDistance = 75;
    private String token;
    private int unit = 0;
    @SerializedName("mid")
    private String userId;
    @SerializedName(Config.FEED_LIST_NAME)
    private String userName;
    private int weight = Constants.DEFAULT_WEIGHT_KG;
    private int weightLb = FMParserConstants.TERMINATING_WHITESPACE;
    private int weightSt;
    private int year;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        UserBean userBean = (UserBean) obj;
        if (this.year == userBean.year && this.month == userBean.month && this.day == userBean.day && this.gender == userBean.gender && this.height == userBean.height && this.weight == userBean.weight && Objects.equals(this.userName, userBean.userName)) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "UserBean{userName='" + this.userName + '\'' + ", year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", gender=" + this.gender + ", height=" + this.height + ", account=" + this.account + ", weight=" + this.weight + ", headerUrl=" + this.headerUrl + ", stepDistance=" + this.stepDistance + '}';
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getCoverImage() {
        return this.coverImage;
    }

    public void setCoverImage(String str) {
        this.coverImage = str;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String str) {
        this.birthday = str;
    }

    public boolean isFirstLogin() {
        return this.firstLogin;
    }

    public void setFirstLogin(boolean z) {
        this.firstLogin = z;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String str) {
        this.token = str;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String str) {
        this.userName = str;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getGender() {
        return this.gender;
    }

    public void setGender(int i) {
        this.gender = i;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int i) {
        this.age = i;
    }

    public int getUnit() {
        return this.unit;
    }

    public void setUnit(int i) {
        this.unit = i;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String str) {
        this.sessionId = str;
    }

    public int getHeightLb() {
        return this.heightLb;
    }

    public void setHeightLb(int i) {
        this.heightLb = i;
    }

    public int getWeight() {
        return this.weight / 10;
    }

    public void setWeight(int i) {
        this.weight = i;
    }

    public int getWeightLb() {
        return this.weightLb;
    }

    public void setWeightLb(int i) {
        this.weightLb = i;
    }

    public int getWeightSt() {
        return this.weightSt;
    }

    public void setWeightSt(int i) {
        this.weightSt = i;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String str) {
        this.account = str;
    }

    public String getPwd() {
        return this.pwd;
    }

    public void setPwd(String str) {
        this.pwd = str;
    }

    public String getOpenId() {
        return this.openId;
    }

    public void setOpenId(String str) {
        this.openId = str;
    }

    public String getHeaderUrl() {
        return this.headerUrl;
    }

    public void setHeaderUrl(String str) {
        this.headerUrl = str;
    }

    public int getStepDistance() {
        return this.stepDistance;
    }

    public void setStepDistance(int i) {
        this.stepDistance = i;
    }
}
