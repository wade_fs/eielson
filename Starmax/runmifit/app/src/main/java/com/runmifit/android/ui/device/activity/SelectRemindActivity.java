package com.wade.fit.ui.device.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.base.BaseAdapter;
import com.wade.fit.ui.device.adapter.SelectRemindAdapter;
import java.util.Arrays;

/* renamed from: com.wade.fit.ui.device.activity.SelectRemindActivity */
public class SelectRemindActivity extends BaseActivity implements BaseAdapter.OnItemClickListener {
    private SelectRemindAdapter mAdapter;
    RecyclerView mRecyclerView;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_select_weekday;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.titleName.setText(getResources().getString(R.string.alarm_set_remind));
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.mAdapter = new SelectRemindAdapter(this, Arrays.asList(getResources().getStringArray(R.array.alarmType)), getIntent().getExtras().getInt("iPosition"));
        this.mAdapter.setOnItemClickListener(this);
        this.mRecyclerView.setAdapter(this.mAdapter);
    }

    public void onItemClick(View view, int i) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putInt("iPosition", i);
        intent.putExtras(bundle);
        setResult(200, intent);
        finish();
    }
}
