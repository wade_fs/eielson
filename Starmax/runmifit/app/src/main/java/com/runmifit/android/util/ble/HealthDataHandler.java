package com.wade.fit.util.ble;

import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.bean.HealthSleepItem;
import com.wade.fit.greendao.bean.HealthSport;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.greendao.gen.HealthSleepDao;
import com.wade.fit.greendao.gen.HealthSleepItemDao;
import com.wade.fit.greendao.gen.HealthSportDao;
import com.wade.fit.greendao.gen.HealthSportItemDao;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.HeartRateInterval;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.StringUtils;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HealthDataHandler {
    int dataIndex = 0;
    List<byte[]> datas = new ArrayList();

    public void init(byte[] bArr) {
        this.datas.clear();
        int bytesToInt = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 1, 3));
        if (bytesToInt % 20 == 0) {
            this.dataIndex = bytesToInt / 20;
        } else {
            this.dataIndex = (bytesToInt / 20) + 1;
        }
        this.datas.add(bArr);
        LogUtil.d("数据长度:" + bytesToInt + ",dataIndex:" + this.dataIndex);
    }

    public HandlerBleDataResult receiverHistory(byte[] bArr) {
        this.datas.add(bArr);
        HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
        handlerBleDataResult.isComplete = this.datas.size() == this.dataIndex;
        if (handlerBleDataResult.isComplete) {
            handler(this.datas);
        }
        handlerBleDataResult.hasNext = true;
        return handlerBleDataResult;
    }

    public HealthSport handlerCurrent(byte[] bArr) {
        byte b = bArr[3];
        UserBean userBean = AppApplication.getInstance().getUserBean();
        int bytesToInt = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 4, 8));
        double stepDistance = (double) (userBean.getStepDistance() * userBean.getWeight() * bytesToInt);
        Double.isNaN(stepDistance);
        int i = (int) ((stepDistance * 0.78d) / 100000.0d);
        int stepDistance2 = (userBean.getStepDistance() * bytesToInt) / 100;
        int[] iArr = DateUtil.todayYearMonthDay();
        HealthSport healthSport = (HealthSport) AppApplication.getInstance().getDaoSession().getHealthSportDao().queryBuilder().where(HealthSportDao.Properties.Year.eq(Integer.valueOf(iArr[0])), HealthSportDao.Properties.Month.eq(Integer.valueOf(iArr[1])), HealthSportDao.Properties.Day.eq(Integer.valueOf(iArr[2]))).orderDesc(HealthSportDao.Properties.Date).build().unique();
        if (healthSport == null) {
            healthSport = new HealthSport();
        }
        healthSport.setTotalCalory(i);
        healthSport.setTotalStepCount(bytesToInt);
        healthSport.setTotalDistance(stepDistance2);
        healthSport.setDate(ProDbUtils.getDate(iArr[0], iArr[1], iArr[2]).getTime());
        healthSport.setYear(iArr[0]);
        healthSport.setMonth(iArr[1]);
        healthSport.setDay(iArr[2]);
        LogUtil.d("type:" + ((int) b) + "," + healthSport);
        return healthSport;
    }

    private Object handler(List<byte[]> list) {
        return handlerHistory(list);
    }

    private List<HealthSportItem> handlerHistory(List<byte[]> list) {
        ArrayList arrayList;
        List<byte[]> list2 = list;
        ArrayList arrayList2 = new ArrayList();
        try {
            byte[] bArr = list2.get(0);
            byte[] bArr2 = new byte[(list.size() * 20)];
            for (int i = 0; i < list.size(); i++) {
                byte[] bArr3 = list2.get(i);
                for (int i2 = 0; i2 < bArr3.length; i2++) {
                    bArr2[(i * 20) + i2] = bArr3[i2];
                }
            }
            int i3 = 1;
            int bytesToInt = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 1, 3));
            byte b = bArr[3];
            int bytesToInt2 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 4, 6));
            byte b2 = bArr[6];
            byte b3 = bArr[7];
            byte b4 = bArr[8];
            DateUtil.todayYearMonthDay();
            LogUtil.m5266d("length:" + bytesToInt, Constants.SYCN_PATH);
            LogUtil.m5266d("total:" + (bytesToInt / 20), Constants.SYCN_PATH);
            LogUtil.m5266d("****************步数睡眠详情数据,type:" + ((int) b) + ",year:" + bytesToInt2 + ",month:" + ((int) b2) + ",day:" + ((int) b3) + ",period:" + ((int) b4) + "*************", Constants.SYCN_PATH);
            Date date = ProDbUtils.getDate(bytesToInt2, b2, b3);
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            HealthSleep healthSleep = new HealthSleep();
            arrayList = arrayList2;
            try {
                healthSleep.setDate(date.getTime());
                healthSleep.setYear(bytesToInt2);
                healthSleep.setMonth(b2);
                healthSleep.setDay(b3);
                UserBean userBean = AppApplication.getInstance().getUserBean();
                int i4 = 0;
                int i5 = 1;
                int i6 = 0;
                boolean z = false;
                while (true) {
                    int i7 = i4 + 11;
                    if (i7 >= bArr2.length || i5 >= 145) {
                        ArrayList arrayList5 = arrayList3;
                        ArrayList arrayList6 = arrayList4;
                        int i8 = i6;
                        UserBean userBean2 = userBean;
                        HealthSport healthSport = new HealthSport();
                        healthSport.setDate(date.getTime());
                        healthSport.setYear(bytesToInt2);
                        healthSport.setMonth(b2);
                        healthSport.setDay(b3);
                    } else {
                        byte[] copyOfRange = Arrays.copyOfRange(bArr2, i4 + 9, i7);
                        byte[] bArr4 = new byte[2];
                        bArr4[0] = copyOfRange[i3];
                        bArr4[i3] = copyOfRange[0];
                        Object[] objArr = new Object[i3];
                        objArr[0] = Long.valueOf(Long.parseLong(Integer.toBinaryString(Integer.parseInt(ByteDataConvertUtil.bytesToHexString2(bArr4), 16))));
                        String format = StringUtils.format("%016d", objArr);
                        int parseInt = Integer.parseInt(format.substring(0, 4), 2);
                        byte[] bArr5 = bArr2;
                        int parseInt2 = Integer.parseInt(format.substring(4), 2);
                        HealthSportItem healthSportItem = new HealthSportItem();
                        healthSportItem.setYear(bytesToInt2);
                        healthSportItem.setMonth(b2);
                        healthSportItem.setDay(b3);
                        ArrayList arrayList7 = arrayList3;
                        ArrayList arrayList8 = arrayList4;
                        healthSportItem.setDate(date.getTime());
                        HealthSleepItem healthSleepItem = new HealthSleepItem();
                        UserBean userBean3 = userBean;
                        int i9 = i4;
                        healthSleepItem.setDate(date.getTime());
                        healthSleepItem.setYear(bytesToInt2);
                        healthSleepItem.setMonth(b2);
                        healthSleepItem.setDay(b3);
                        if (parseInt == 15) {
                            int parseInt3 = Integer.parseInt(format.substring(4, 8), 2);
                            healthSleepItem.setSleepStatus(parseInt3);
                            healthSleepItem.setOffsetMinute(10);
                            if (!z) {
                                int i10 = i5 * 10;
                                healthSleep.setSleepstartedTimeH(i10 / 60);
                                healthSleep.setSleepstartedTimeM(i10 % 60);
                                z = true;
                            }
                            if (parseInt3 == 1) {
                                healthSleep.setSleepMinutes(healthSleep.getSleepMinutes() + 10);
                                healthSleep.setTotalSleepMinutes(healthSleep.getTotalSleepMinutes() + 10);
                            } else if (parseInt3 == 2) {
                                healthSleep.setLightSleepMinutes(healthSleep.getLightSleepMinutes() + 10);
                                healthSleep.setTotalSleepMinutes(healthSleep.getTotalSleepMinutes() + 10);
                            } else if (parseInt3 == 3) {
                                healthSleep.setDeepSleepMinutes(healthSleep.getDeepSleepMinutes() + 10);
                                healthSleep.setTotalSleepMinutes(healthSleep.getTotalSleepMinutes() + 10);
                            } else if (parseInt3 == 4) {
                                healthSleep.setWakeMunutes(healthSleep.getWakeMunutes() + 10);
                                healthSleep.setTotalSleepMinutes(healthSleep.getTotalSleepMinutes() + 10);
                            } else if (parseInt3 == 5) {
                                healthSleep.setEyeMinutes(healthSleep.getEyeMinutes() + 10);
                                healthSleep.setTotalSleepMinutes(healthSleep.getTotalSleepMinutes() + 10);
                            }
                        } else {
                            i6 += parseInt2;
                            double stepDistance = (double) (userBean3.getStepDistance() * userBean3.getWeight() * parseInt2);
                            Double.isNaN(stepDistance);
                            int i11 = (int) ((stepDistance * 0.78d) / 100000.0d);
                            healthSportItem.setStepCount(parseInt2);
                            healthSportItem.setCalory(i11);
                            healthSportItem.setDistance((userBean3.getStepDistance() * parseInt2) / 100);
                        }
                        arrayList7.add(healthSportItem);
                        i5++;
                        ArrayList arrayList9 = arrayList8;
                        arrayList9.add(healthSleepItem);
                        userBean = userBean3;
                        arrayList3 = arrayList7;
                        bArr2 = bArr5;
                        i3 = 1;
                        arrayList4 = arrayList9;
                        i4 = i9 + 2;
                    }
                }
                ArrayList arrayList52 = arrayList3;
                ArrayList arrayList62 = arrayList4;
                int i82 = i6;
                UserBean userBean22 = userBean;
                HealthSport healthSport2 = new HealthSport();
                healthSport2.setDate(date.getTime());
                healthSport2.setYear(bytesToInt2);
                healthSport2.setMonth(b2);
                healthSport2.setDay(b3);
                double stepDistance2 = (double) (userBean22.getStepDistance() * userBean22.getWeight() * i82);
                Double.isNaN(stepDistance2);
                healthSport2.setTotalCalory((int) ((stepDistance2 * 0.78d) / 100000.0d));
                healthSport2.setTotalDistance((userBean22.getStepDistance() * i82) / 100);
                healthSport2.setTotalStepCount(i82);
                HealthSportDao healthSportDao = AppApplication.getInstance().getDaoSession().getHealthSportDao();
                healthSportDao.queryBuilder().where(HealthSportDao.Properties.Year.eq(Integer.valueOf(bytesToInt2)), HealthSportDao.Properties.Month.eq(Integer.valueOf(b2)), HealthSportDao.Properties.Day.eq(Integer.valueOf(b3))).buildDelete().executeDeleteWithoutDetachingEntities();
                healthSportDao.insert(healthSport2);
                HealthSportItemDao healthSportItemDao = AppApplication.getInstance().getDaoSession().getHealthSportItemDao();
                healthSportItemDao.queryBuilder().where(HealthSportItemDao.Properties.Year.eq(Integer.valueOf(bytesToInt2)), HealthSportItemDao.Properties.Month.eq(Integer.valueOf(b2)), HealthSportItemDao.Properties.Day.eq(Integer.valueOf(b3))).buildDelete().executeDeleteWithoutDetachingEntities();
                healthSportItemDao.insertInTx(arrayList52);
                LogUtil.m5266d("****************睡眠数据：" + healthSleep, Constants.SYCN_PATH);
                if (healthSleep.getTotalSleepMinutes() != 0) {
                    HealthSleepDao healthSleepDao = AppApplication.getInstance().getDaoSession().getHealthSleepDao();
                    healthSleepDao.queryBuilder().where(HealthSleepDao.Properties.Year.eq(Integer.valueOf(bytesToInt2)), HealthSleepDao.Properties.Month.eq(Integer.valueOf(b2)), HealthSleepDao.Properties.Day.eq(Integer.valueOf(b3))).buildDelete().executeDeleteWithoutDetachingEntities();
                    healthSleepDao.insert(healthSleep);
                    long longValue = ((Long) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.LAST_SYNCH_TIME, 0L)).longValue();
                    Calendar instance = Calendar.getInstance();
                    instance.set(11, 0);
                    instance.set(12, 0);
                    instance.set(13, 0);
                    int timeInMillis = (int) ((instance.getTimeInMillis() - longValue) / 86400000);
                    if (timeInMillis <= 0) {
                        timeInMillis = 1;
                    }
                    instance.add(6, -timeInMillis);
                    HealthSleepItemDao healthSleepItemDao = AppApplication.getInstance().getDaoSession().getHealthSleepItemDao();
                    if (timeInMillis == 7) {
                        healthSleepItemDao.queryBuilder().where(HealthSleepItemDao.Properties.Year.eq(Integer.valueOf(bytesToInt2)), HealthSleepItemDao.Properties.Month.eq(Integer.valueOf(b2)), HealthSleepItemDao.Properties.Day.eq(Integer.valueOf(b3))).buildDelete().executeDeleteWithoutDetachingEntities();
                        healthSleepItemDao.insertOrReplaceInTx(arrayList62);
                    } else if (bytesToInt2 == instance.get(1) && b2 == instance.get(2) && b3 == instance.get(5)) {
                        List list3 = healthSleepItemDao.queryBuilder().where(HealthSleepItemDao.Properties.Year.eq(Integer.valueOf(bytesToInt2)), HealthSleepItemDao.Properties.Month.eq(Integer.valueOf(b2)), HealthSleepItemDao.Properties.Day.eq(Integer.valueOf(b3))).orderDesc(HealthSleepItemDao.Properties.Date).build().list();
                        for (int size = list3.size() - 1; size > list3.size() - 19; size--) {
                            healthSleepItemDao.delete(list3.get(size));
                        }
                        ArrayList arrayList10 = new ArrayList();
                        for (int i12 = 0; i12 < 18; i12++) {
                            arrayList10.add(arrayList62.get((arrayList62.size() + i12) - 18));
                        }
                        healthSleepItemDao.insertOrReplaceInTx(arrayList10);
                    } else {
                        healthSleepItemDao.queryBuilder().where(HealthSleepItemDao.Properties.Year.eq(Integer.valueOf(bytesToInt2)), HealthSleepItemDao.Properties.Month.eq(Integer.valueOf(b2)), HealthSleepItemDao.Properties.Day.eq(Integer.valueOf(b3))).buildDelete().executeDeleteWithoutDetachingEntities();
                        healthSleepItemDao.insertOrReplaceInTx(arrayList62);
                    }
                }
                LogUtil.m5266d("totalStep :" + i82, Constants.SYCN_PATH);
                LogUtil.m5266d("****************步数睡眠解析完毕*************", Constants.SYCN_PATH);
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return arrayList;
            }
        } catch (Exception e2) {
            e = e2;
            arrayList = arrayList2;
            e.printStackTrace();
            return arrayList;
        }
        return arrayList;
    }

    public void setHeartRong(byte[] bArr) {
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        HeartRateInterval heartRateInterval = new HeartRateInterval();
        if (bArr[4] == 1) {
            heartRateInterval.isCustomHr = true;
        } else {
            heartRateInterval.isCustomHr = false;
        }
        heartRateInterval.maxHr = ByteDataConvertUtil.Byte2Int(bArr[5]);
        heartRateInterval.minHr = ByteDataConvertUtil.Byte2Int(bArr[6]);
        deviceConfig.interval = heartRateInterval;
        SPHelper.saveDeviceConfig(deviceConfig);
    }

    public void handlerHartOpen(byte[] bArr) {
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        if (bArr[4] == 1) {
            deviceConfig.isTestTime = true;
        } else {
            deviceConfig.isTestTime = false;
        }
        SPHelper.saveDeviceConfig(deviceConfig);
    }
}
