package com.wade.fit.persenter.mine;

import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.http.LoginHttp;
import com.wade.fit.persenter.mine.AccountContract;
import p041io.reactivex.disposables.Disposable;

public class AccountPresenter extends BasePersenter<AccountContract.View> implements AccountContract.Presenter {
    public void loginOut() {
        ((AccountContract.View) this.mView).showLoadingFalse();
        LoginHttp.loginOut(new ApiCallback<BaseBean<String>>() {
            /* class com.wade.fit.persenter.mine.AccountPresenter.C24221 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<String> baseBean) {
                ((AccountContract.View) AccountPresenter.this.mView).hideLoading();
                ((AccountContract.View) AccountPresenter.this.mView).loginOutSuccess();
            }

            public void onFailure(int i, String str) {
                ((AccountContract.View) AccountPresenter.this.mView).hideLoading();
                ((AccountContract.View) AccountPresenter.this.mView).loginOutFaild(i);
            }

            public void onSubscribe(Disposable disposable) {
                AccountPresenter.this.addDisposable(disposable);
            }
        });
    }

    public void clearUserData() {
        ((AccountContract.View) this.mView).showLoadingFalse();
        LoginHttp.clearUserData(new ApiCallback<BaseBean<String>>() {
            /* class com.wade.fit.persenter.mine.AccountPresenter.C24232 */

            public void onFinish() {
            }

            public void onSuccess(BaseBean<String> baseBean) {
                ((AccountContract.View) AccountPresenter.this.mView).clearSuccess();
            }

            public void onFailure(int i, String str) {
                ((AccountContract.View) AccountPresenter.this.mView).clearFaild(i);
            }

            public void onSubscribe(Disposable disposable) {
                AccountPresenter.this.addDisposable(disposable);
            }
        });
    }
}
