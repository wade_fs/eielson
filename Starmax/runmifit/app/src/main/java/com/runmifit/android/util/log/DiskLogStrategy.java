package com.wade.fit.util.log;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.orhanobut.logger.LogStrategy;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DiskLogStrategy implements LogStrategy {
    private final Handler handler;

    DiskLogStrategy(Handler handler2) {
        this.handler = handler2;
    }

    public void log(int i, String str, String str2) {
        Handler handler2 = this.handler;
        handler2.sendMessage(handler2.obtainMessage(i, str2));
    }

    static class WriteHandler extends Handler {
        private static final long SAVE_DAYS = 604800000;
        private String appName = "Logger";
        private SimpleDateFormat fileFormat = new SimpleDateFormat("yyyy-MM-dd_HH", Locale.ENGLISH);
        private final String folder;

        WriteHandler(Looper looper, String str, String str2) {
            super(looper);
            this.folder = str + new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(new Date());
            this.appName = str2;
            deleteLoggerFile(str);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
          ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
        public void handleMessage(Message message) {
            FileWriter fileWriter;
            String str = (String) message.obj;
            try {
                fileWriter = new FileWriter(getLogFile(this.folder, this.appName), true);
                try {
                    writeLog(fileWriter, str);
                    fileWriter.flush();
                    fileWriter.close();
                } catch (IOException unused) {
                }
            } catch (IOException unused2) {
                fileWriter = null;
                if (fileWriter != null) {
                    try {
                        fileWriter.flush();
                        fileWriter.close();
                    } catch (IOException unused3) {
                    }
                }
            }
        }

        private void writeLog(FileWriter fileWriter, String str) throws IOException {
            fileWriter.append((CharSequence) str);
        }

        private File getLogFile(String str, String str2) {
            File file = new File(str);
            if (!file.exists()) {
                file.mkdirs();
            }
            return new File(file, String.format("%s_%s.log", str2, this.fileFormat.format(new Date())));
        }

        private synchronized void deleteLoggerFile(String str) {
            File file = new File(str);
            if (file.exists()) {
                File[] listFiles = file.listFiles();
                for (File file2 : listFiles) {
                    if (System.currentTimeMillis() - file2.lastModified() > 604800000) {
                        deleteDirWhiteFile(file2);
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0039, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private synchronized void deleteDirWhiteFile(java.io.File r6) {
            /*
                r5 = this;
                monitor-enter(r5)
                if (r6 == 0) goto L_0x0038
                boolean r0 = r6.exists()     // Catch:{ all -> 0x0035 }
                if (r0 == 0) goto L_0x0038
                boolean r0 = r6.isDirectory()     // Catch:{ all -> 0x0035 }
                if (r0 != 0) goto L_0x0010
                goto L_0x0038
            L_0x0010:
                java.io.File[] r0 = r6.listFiles()     // Catch:{ all -> 0x0035 }
                int r1 = r0.length     // Catch:{ all -> 0x0035 }
                r2 = 0
            L_0x0016:
                if (r2 >= r1) goto L_0x0030
                r3 = r0[r2]     // Catch:{ all -> 0x0035 }
                boolean r4 = r3.isFile()     // Catch:{ all -> 0x0035 }
                if (r4 == 0) goto L_0x0024
                r3.delete()     // Catch:{ all -> 0x0035 }
                goto L_0x002d
            L_0x0024:
                boolean r4 = r3.isDirectory()     // Catch:{ all -> 0x0035 }
                if (r4 == 0) goto L_0x002d
                r5.deleteDirWhiteFile(r3)     // Catch:{ all -> 0x0035 }
            L_0x002d:
                int r2 = r2 + 1
                goto L_0x0016
            L_0x0030:
                r6.delete()     // Catch:{ all -> 0x0035 }
                monitor-exit(r5)
                return
            L_0x0035:
                r6 = move-exception
                monitor-exit(r5)
                throw r6
            L_0x0038:
                monitor-exit(r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.log.DiskLogStrategy.WriteHandler.deleteDirWhiteFile(java.io.File):void");
        }
    }
}
