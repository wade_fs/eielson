package com.wade.fit.util;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public class GsonUtil {
    public static String toJson(Object obj) {
        try {
            return new Gson().toJson(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> List<T> fromJson2List(String str) {
        Gson gson = new Gson();
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (List) gson.fromJson(str, new TypeToken<List<T>>() {
                /* class com.wade.fit.util.GsonUtil.C26421 */
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <K, V> Map<K, V> fromJson2Map(String str) {
        return (Map) fromJsonToCollect(str);
    }

    public static <K, V> Map json2Map(String str, Map<K, V> map) {
        return (Map) fromJsonToCollect(str);
    }

    public static <T> T fromJsonToCollect(String str) {
        Gson gson = new Gson();
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return gson.fromJson(str, new TypeToken<T>() {
                /* class com.wade.fit.util.GsonUtil.C26432 */
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.Class):T
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(com.google.gson.stream.JsonReader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T */
    public static <T> T fromJson(String str, Class<T> cls) {
        try {
            return new Gson().fromJson(str, (Class) cls);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T fromJson(String str, Class<T> cls, Class<?> cls2) {
        try {
            return new Gson().fromJson(str, type(cls, cls2));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static ParameterizedType type(final Class cls, final Type... typeArr) {
        return new ParameterizedType() {
            /* class com.wade.fit.util.GsonUtil.C26443 */

            public Type getOwnerType() {
                return null;
            }

            public Type getRawType() {
                return cls;
            }

            public Type[] getActualTypeArguments() {
                return typeArr;
            }
        };
    }
}
