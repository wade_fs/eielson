package com.wade.fit.util;

import android.os.Build;
import android.text.Html;
import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class EncodeUtil {
    private EncodeUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static String urlEncode(String str) {
        return urlEncode(str, "UTF-8");
    }

    public static String urlEncode(String str, String str2) {
        try {
            return URLEncoder.encode(str, str2);
        } catch (UnsupportedEncodingException unused) {
            return str;
        }
    }

    public static String urlDecode(String str) {
        return urlDecode(str, "UTF-8");
    }

    public static String urlDecode(String str, String str2) {
        try {
            return URLDecoder.decode(str, str2);
        } catch (UnsupportedEncodingException unused) {
            return str;
        }
    }

    public static byte[] base64Encode(String str) {
        return base64Encode(str.getBytes());
    }

    public static byte[] base64Encode(byte[] bArr) {
        return Base64.encode(bArr, 2);
    }

    public static String base64Encode2String(byte[] bArr) {
        return Base64.encodeToString(bArr, 2);
    }

    public static byte[] base64Decode(String str) {
        return Base64.decode(str, 2);
    }

    public static byte[] base64Decode(byte[] bArr) {
        return Base64.decode(bArr, 2);
    }

    public static byte[] base64UrlSafeEncode(String str) {
        return Base64.encode(str.getBytes(), 8);
    }

    public static String htmlEncode(String str) {
        int i;
        char charAt;
        if (Build.VERSION.SDK_INT >= 16) {
            return Html.escapeHtml(str);
        }
        StringBuilder sb = new StringBuilder();
        int i2 = 0;
        int length = str.length();
        while (i2 < length) {
            char charAt2 = str.charAt(i2);
            if (charAt2 == '<') {
                sb.append("&lt;");
            } else if (charAt2 == '>') {
                sb.append("&gt;");
            } else if (charAt2 == '&') {
                sb.append("&amp;");
            } else if (charAt2 < 55296 || charAt2 > 57343) {
                if (charAt2 > '~' || charAt2 < ' ') {
                    sb.append("&#");
                    sb.append((int) charAt2);
                    sb.append(";");
                } else if (charAt2 == ' ') {
                    while (true) {
                        int i3 = i2 + 1;
                        if (i3 >= length || str.charAt(i3) != ' ') {
                            sb.append(' ');
                        } else {
                            sb.append("&nbsp;");
                            i2 = i3;
                        }
                    }
                    sb.append(' ');
                } else {
                    sb.append(charAt2);
                }
            } else if (charAt2 < 56320 && (i = i2 + 1) < length && (charAt = str.charAt(i)) >= 56320 && charAt <= 57343) {
                sb.append("&#");
                sb.append(65536 | ((charAt2 - 55296) << 10) | (charAt - 56320));
                sb.append(";");
                i2 = i;
            }
            i2++;
        }
        return sb.toString();
    }

    public static String htmlDecode(String str) {
        return Html.fromHtml(str).toString();
    }
}
