package com.wade.fit.ui.sport.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.model.bean.HealthMonthActivity;
import com.wade.fit.util.SportDataHelper;
import com.wade.fit.util.ble.BleSdkWrapper;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/* renamed from: com.wade.fit.ui.sport.adapter.SportActivityAdapter */
public class SportActivityAdapter extends BaseExpandableListAdapter {
    private String[] amOrPm = this.context.getResources().getStringArray(R.array.amOrpm);
    private Context context;
    private List<HealthMonthActivity> showList;

    public long getChildId(int i, int i2) {
        return (long) i2;
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    public SportActivityAdapter(Context context2, List<HealthMonthActivity> list) {
        this.context = context2;
        this.showList = list;
    }

    public void setDate(List<HealthMonthActivity> list) {
        this.showList = list;
        notifyDataSetChanged();
    }

    public int getGroupCount() {
        return this.showList.size();
    }

    public int getChildrenCount(int i) {
        if (this.showList.get(i).getHealthActivities() == null) {
            return 0;
        }
        return this.showList.get(i).getHealthActivities().size();
    }

    public Object getGroup(int i) {
        return this.showList.get(i);
    }

    public Object getChild(int i, int i2) {
        if (this.showList.get(i).getHealthActivities() == null) {
            return null;
        }
        return this.showList.get(i).getHealthActivities().get(i2);
    }

    /* renamed from: com.wade.fit.ui.sport.adapter.SportActivityAdapter$ParentViewHolder */
    private class ParentViewHolder {
        ImageView expanIcon;
        TextView monthName;

        private ParentViewHolder() {
        }
    }

    /* renamed from: com.wade.fit.ui.sport.adapter.SportActivityAdapter$ChildViewHolder */
    private class ChildViewHolder {
        ImageView imBluthTag;
        ImageView ivSportType;
        TextView tvAvHeartRate;
        TextView tvCal;
        TextView tvDate;
        TextView tvDistance;
        TextView tvDistanceUnit;
        TextView tvDuration;
        TextView tvSportType;

        private ChildViewHolder() {
        }
    }

    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        ParentViewHolder parentViewHolder;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.expand_title, (ViewGroup) null);
            parentViewHolder = new ParentViewHolder();
            parentViewHolder.monthName = (TextView) view.findViewById(R.id.monthName);
            parentViewHolder.expanIcon = (ImageView) view.findViewById(R.id.expanIcon);
            view.setTag(parentViewHolder);
        } else {
            parentViewHolder = (ParentViewHolder) view.getTag();
        }
        if (z) {
            parentViewHolder.expanIcon.setImageResource(R.mipmap.arrow_down_gray);
        } else {
            parentViewHolder.expanIcon.setImageResource(R.mipmap.arrow_up_gray);
        }
        parentViewHolder.monthName.setText(this.showList.get(i).getStMonth());
        return view;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        ChildViewHolder childViewHolder;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate((int) R.layout.item_sport_health, (ViewGroup) null);
            childViewHolder = new ChildViewHolder();
            childViewHolder.ivSportType = (ImageView) view.findViewById(R.id.ivSportType);
            childViewHolder.tvSportType = (TextView) view.findViewById(R.id.tvSportType);
            childViewHolder.tvDate = (TextView) view.findViewById(R.id.tvDate);
            childViewHolder.tvDistance = (TextView) view.findViewById(R.id.tvDistance);
            childViewHolder.tvDistanceUnit = (TextView) view.findViewById(R.id.tvDistanceUnit);
            childViewHolder.tvDuration = (TextView) view.findViewById(R.id.tvDuration);
            childViewHolder.tvAvHeartRate = (TextView) view.findViewById(R.id.tvAvHeartRate);
            childViewHolder.tvCal = (TextView) view.findViewById(R.id.tvCal);
            childViewHolder.imBluthTag = (ImageView) view.findViewById(R.id.imBluthTag);
            view.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) view.getTag();
        }
        HealthActivity healthActivity = this.showList.get(i).getHealthActivities().get(i2);
        childViewHolder.ivSportType.setImageResource(SportDataHelper.getResByType(healthActivity.getType()));
        childViewHolder.tvSportType.setText(SportDataHelper.getStringBytyp(healthActivity.getType()));
        if (DateFormat.is24HourFormat(this.context)) {
            childViewHolder.tvDate.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Long.valueOf(healthActivity.getDate())));
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(healthActivity.getDate());
            if (instance.get(9) == 0) {
                TextView textView = childViewHolder.tvDate;
                textView.setText(simpleDateFormat.format(Long.valueOf(healthActivity.getDate())) + "  AM");
            } else {
                TextView textView2 = childViewHolder.tvDate;
                textView2.setText(simpleDateFormat.format(Long.valueOf(healthActivity.getDate())) + "  PM");
            }
        }
        if (healthActivity.getDataFrom() == 2) {
            childViewHolder.imBluthTag.setVisibility(View.VISIBLE);
        } else {
            childViewHolder.imBluthTag.setVisibility(View.GONE);
        }
        if (healthActivity.getType() == 0 || healthActivity.getType() == 1 || healthActivity.getType() == 4) {
            childViewHolder.tvDistance.setVisibility(View.VISIBLE);
            childViewHolder.tvDistanceUnit.setVisibility(View.VISIBLE);
        } else if (healthActivity.getType() != 3) {
            childViewHolder.tvDistance.setVisibility(View.GONE);
            childViewHolder.tvDistanceUnit.setVisibility(View.GONE);
        } else if (healthActivity.getDataFrom() == 2) {
            childViewHolder.tvDistance.setVisibility(View.GONE);
            childViewHolder.tvDistanceUnit.setVisibility(View.GONE);
        } else {
            childViewHolder.tvDistance.setVisibility(View.VISIBLE);
            childViewHolder.tvDistanceUnit.setVisibility(View.VISIBLE);
        }
        childViewHolder.tvDistance.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/DINCondensedBold.ttf"));
        if (!BleSdkWrapper.isDistUnitKm()) {
            childViewHolder.tvDistance.setText(String.format("%.2f", Float.valueOf(((float) healthActivity.getDistance()) / 1000.0f)));
            childViewHolder.tvDistanceUnit.setText(this.context.getResources().getString(R.string.unit_mi));
        } else {
            childViewHolder.tvDistance.setText(String.format("%.2f", Float.valueOf(((float) healthActivity.getDistance()) / 1000.0f)));
            childViewHolder.tvDistanceUnit.setText(this.context.getResources().getString(R.string.unit_kilometer));
        }
        TextView textView3 = childViewHolder.tvCal;
        textView3.setText(healthActivity.getCalories() + " " + this.context.getResources().getString(R.string.unit_calorie));
        TextView textView4 = childViewHolder.tvDuration;
        textView4.setText(String.format("%02d", Integer.valueOf(healthActivity.getDurations() / 3600)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf((healthActivity.getDurations() % 3600) / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf(healthActivity.getDurations() % 60)));
        return view;
    }
}
