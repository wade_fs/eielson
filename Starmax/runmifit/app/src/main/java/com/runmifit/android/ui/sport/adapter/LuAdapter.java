package com.wade.fit.ui.sport.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;

/* renamed from: com.wade.fit.ui.sport.adapter.LuAdapter */
public abstract class LuAdapter<T> extends BaseAdapter {
    protected Context context;
    protected List<T> datas;
    private final int mItemLayoutId;

    public abstract void convert(ViewHolder viewHolder, int i);

    public void convert(ViewHolder viewHolder, T t) {
    }

    public long getItemId(int i) {
        return 0;
    }

    public LuAdapter(Context context2, List<T> list, int i) {
        this.context = context2;
        this.datas = list;
        this.mItemLayoutId = i;
    }

    public void add(T t) {
        List<T> list = this.datas;
        if (list != null) {
            list.add(t);
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        List<T> list = this.datas;
        if (list == null) {
            return 0;
        }
        return list.size();
    }

    public T getItem(int i) {
        List<T> list = this.datas;
        if (list == null) {
            return null;
        }
        return list.get(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = getViewHolder(view, viewGroup, i);
        convert(viewHolder, getItem(i));
        convert(viewHolder, i);
        return viewHolder.getConvertView();
    }

    /* access modifiers changed from: protected */
    public ViewHolder getViewHolder(View view, ViewGroup viewGroup, int i) {
        return ViewHolder.get(this.context, view, viewGroup, this.mItemLayoutId, i);
    }

    public void setDatas(List<T> list) {
        if (this.datas != null) {
            this.datas = list;
            notifyDataSetChanged();
        }
    }
}
