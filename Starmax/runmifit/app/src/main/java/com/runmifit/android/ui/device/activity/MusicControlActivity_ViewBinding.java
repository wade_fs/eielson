package com.wade.fit.ui.device.activity;

import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.CustomToggleButton;

/* renamed from: com.wade.fit.ui.device.activity.MusicControlActivity_ViewBinding */
public class MusicControlActivity_ViewBinding implements Unbinder {
    private MusicControlActivity target;

    public MusicControlActivity_ViewBinding(MusicControlActivity musicControlActivity) {
        this(musicControlActivity, musicControlActivity.getWindow().getDecorView());
    }

    public MusicControlActivity_ViewBinding(MusicControlActivity musicControlActivity, View view) {
        this.target = musicControlActivity;
        musicControlActivity.mMusicControl = (CustomToggleButton) Utils.findRequiredViewAsType(view, R.id.music_play_switch_ctb, "field 'mMusicControl'", CustomToggleButton.class);
        musicControlActivity.mSelectPlayTv = (TextView) Utils.findRequiredViewAsType(view, R.id.select_play_tv, "field 'mSelectPlayTv'", TextView.class);
        musicControlActivity.mMusicPlayLv = (ListView) Utils.findRequiredViewAsType(view, R.id.music_play_lv, "field 'mMusicPlayLv'", ListView.class);
    }

    public void unbind() {
        MusicControlActivity musicControlActivity = this.target;
        if (musicControlActivity != null) {
            this.target = null;
            musicControlActivity.mMusicControl = null;
            musicControlActivity.mSelectPlayTv = null;
            musicControlActivity.mMusicPlayLv = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
