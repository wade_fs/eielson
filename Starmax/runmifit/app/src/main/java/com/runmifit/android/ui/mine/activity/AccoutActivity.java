package com.wade.fit.ui.mine.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.ui.main.activity.LoginActivity;
import com.wade.fit.ui.main.activity.UpdatePasswordActivity;
import com.wade.fit.persenter.mine.AccountContract;
import com.wade.fit.persenter.mine.AccountPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.views.ItemLableValue;
import com.wade.fit.views.dialog.CommonDialog;

/* renamed from: com.wade.fit.ui.mine.activity.AccoutActivity */
public class AccoutActivity extends BaseMvpActivity<AccountPresenter> implements AccountContract.View {
    Button btnLogOut;
    ItemLableValue ilClearData;
    ItemLableValue ilLogOut;
    ItemLableValue ilResetPwd;
    View lineView;

    public void clearFaild(int i) {
    }

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_accout;
    }

    public void loginOutFaild(int i) {
    }

    public void loginOutSuccess() {
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.accout_title));
        if (!TextUtils.isEmpty(SharePreferenceUtils.getString(this, Constants.USER_TOKEN, ""))) {
            this.btnLogOut.setText((int) R.string.mine_exit_login);
        } else {
            this.btnLogOut.setText((int) R.string.user_login);
        }
        if (SharePreferenceUtils.getInt(this, Constants.LOGIN_TYPE, 1) != 1 || TextUtils.isEmpty(SharePreferenceUtils.getString(this, Constants.USER_TOKEN, ""))) {
            this.ilResetPwd.setVisibility(View.GONE);
            this.lineView.setVisibility(View.GONE);
            return;
        }
        this.ilResetPwd.setVisibility(View.VISIBLE);
        this.lineView.setVisibility(View.VISIBLE);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.ilClearData && !ButtonUtils.isFastDoubleClick(R.id.ilClearData, 1000)) {
            new CommonDialog.Builder(this).setMessage((int) R.string.clear_data_tips).setLeftButton(R.string.cancel).setRightButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                /* class com.wade.fit.ui.mine.activity.$$Lambda$AccoutActivity$86n03I9vuBhJ7bMY8sxAOI12U8 */

                public final void onClick(DialogInterface dialogInterface, int i) {
                    AccoutActivity.this.lambda$onClick$0$AccoutActivity(dialogInterface, i);
                }
            }).create().show();
        }
    }

    public /* synthetic */ void lambda$onClick$0$AccoutActivity(DialogInterface dialogInterface, int i) {
        if (!TextUtils.isEmpty(SharePreferenceUtils.getString(this, Constants.USER_TOKEN, ""))) {
            ((AccountPresenter) this.mPresenter).clearUserData();
        } else {
            cleanLocalData();
        }
    }

    private void cleanLocalData() {
        AppApplication.getInstance().getDaoSession().getEcgRecordInfoDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getECGItemInfoDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getHealthActivityDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getHealthSportDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getHealthSportItemDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getHealthSleepItemDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getHealthSleepDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getHealthHeartRateDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getHealthHeartRateItemDao().deleteAll();
        AppApplication.getInstance().getDaoSession().getHealthGpsItemDao().deleteAll();
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.IS_REF_DATA, true);
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.IS_SYN_HISTORY_KEY, true);
        showToast(getResources().getString(R.string.clear_data_success));
    }

    /* access modifiers changed from: package-private */
    public void loginClick() {
        if (ButtonUtils.isFastDoubleClick(R.id.btnLogOut, 1000)) {
            return;
        }
        if (!TextUtils.isEmpty(SharePreferenceUtils.getString(this, Constants.USER_TOKEN, ""))) {
            new CommonDialog.Builder(this).setMessage((int) R.string.login_out_tips).setLeftButton(R.string.cancel).setRightButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                /* class com.wade.fit.ui.mine.activity.$$Lambda$AccoutActivity$JGmYJLdcK1vy2VdDUm0O8zmnb3w */

                public final void onClick(DialogInterface dialogInterface, int i) {
                    AccoutActivity.this.lambda$loginClick$1$AccoutActivity(dialogInterface, i);
                }
            }).create().show();
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("from", "MainActivity");
        IntentUtil.goToActivity(this, LoginActivity.class, bundle);
    }

    public /* synthetic */ void lambda$loginClick$1$AccoutActivity(DialogInterface dialogInterface, int i) {
        SharePreferenceUtils.putString(Constants.USER_PASSWORD, "");
        SharePreferenceUtils.putString(Constants.USER_TOKEN, "");
        SharePreferenceUtils.putString(Constants.IS_TO_LOGIN, "1");
        SPHelper.saveUserBean(null);
        Bundle bundle = new Bundle();
        bundle.putString("from", "WelcomeActivity");
        IntentUtil.goToActivityAndFinish(this, LoginActivity.class, bundle);
    }

    /* access modifiers changed from: package-private */
    public void toRestPassword() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ilResetPwd, 1000)) {
            Bundle bundle = new Bundle();
            bundle.putString("title", getResources().getString(R.string.reset_pwd));
            IntentUtil.goToActivity(this, UpdatePasswordActivity.class, bundle);
        }
    }

    public void clearSuccess() {
        cleanLocalData();
    }
}
