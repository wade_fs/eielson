package com.wade.fit.util.ble;

import com.wade.fit.R;
import com.wade.fit.util.ToastUtil;

public abstract class BleCallbackWrapper implements BleCallback {
    public void setFaild() {
    }

    public abstract void setSuccess();

    public void complete(int i, Object obj) {
        if (i == 1) {
            setSuccess();
            return;
        }
        if (-4 == i) {
            ToastUtil.showToast((int) R.string.disConnected);
        } else if (-3 == i) {
            ToastUtil.showToast((int) R.string.server_sync_data);
        }
        setFaild();
    }
}
