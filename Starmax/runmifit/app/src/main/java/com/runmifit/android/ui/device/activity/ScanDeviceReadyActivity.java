package com.wade.fit.ui.device.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.internal.view.SupportMenu;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseAdapter;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.ui.device.adapter.ScanDeviceAdapter;
import com.wade.fit.ui.main.activity.MainActivity;
import com.wade.fit.persenter.device.ScanDeviceContract;
import com.wade.fit.persenter.device.ScanDevicePresenter;
import com.wade.fit.util.CommonUtil;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleScanTool;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.RecyclerRefreshLayout;
import com.wade.fit.views.dialog.CommonDialog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.activity.ScanDeviceReadyActivity */
public class ScanDeviceReadyActivity extends BaseMvpActivity<ScanDevicePresenter> implements ScanDeviceContract.View, BaseAdapter.OnItemClickListener, RecyclerRefreshLayout.SuperRefreshLayoutListener {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    protected static final int ACCESS_FINE_LOCATION_REQUEST_CODE = 100;
    public static final int CMD_CONNECTING = 1;
    public static final int CMD_SCAN = 0;
    Button btnSkip;
    private String fromActivity = "";
    private boolean isConnecting = false;
    private ScanDeviceAdapter mAdapter;
    private Dialog mDialog;
    RecyclerView mRecyclerView;
    RecyclerRefreshLayout mRefreshLayout;
    private String[] permissionsLocation = {"android.permission.ACCESS_FINE_LOCATION"};
    private List<BLEDevice> showList = new ArrayList();

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_scan_device;
    }

    public void onLoadMore() {
    }

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
        super.handleMessage(baseMessage);
        int type = baseMessage.getType();
        if (type == 300) {
            getScanDevice();
        } else if (type == 304) {
            showToast(getResources().getString(R.string.ble_connect_timeout));
            hideLoading();
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.titleName.setText(getResources().getString(R.string.bind));
        this.fromActivity = getIntent().getExtras().getString("from");
        if (this.fromActivity.equals("PersonInfoActivity")) {
            this.titleBack.setVisibility(View.GONE);
            this.btnSkip.setVisibility(View.VISIBLE);
        } else if (this.fromActivity.equals("MainActivity")) {
            this.titleBack.setVisibility(View.VISIBLE);
            this.btnSkip.setVisibility(View.GONE);
        }
        this.mRefreshLayout.setSuperRefreshLayoutListener(this);
        this.mRefreshLayout.setColorSchemeColors(SupportMenu.CATEGORY_MASK, -16711936, -16711681);
        this.mRefreshLayout.setCanLoadMore(false);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (!CommonUtil.isOPen(this)) {
            DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_location_title), getResources().getString(R.string.permisson_location_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$ScanDeviceReadyActivity$tXu75zRthsOXcAqZ_iNxxF3qflY */

                public final void onClick(View view) {
                    ScanDeviceReadyActivity.this.lambda$initView$0$ScanDeviceReadyActivity(view);
                }
            }, new View.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$ScanDeviceReadyActivity$SSyPHc1_ta2pu7AWSxAmqp61xS0 */

                public final void onClick(View view) {
                    ScanDeviceReadyActivity.this.lambda$initView$1$ScanDeviceReadyActivity(view);
                }
            });
        } else {
            getScanDevice();
        }
    }

    public /* synthetic */ void lambda$initView$0$ScanDeviceReadyActivity(View view) {
        Intent intent = new Intent();
        intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
        startActivityForResult(intent, 1000);
    }

    public /* synthetic */ void lambda$initView$1$ScanDeviceReadyActivity(View view) {
        finish();
    }

    /* access modifiers changed from: package-private */
    public void tomain() {
        IntentUtil.goToActivityAndFinish(this, MainActivity.class);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1000) {
            if (!CommonUtil.isOPen(this)) {
                finish();
            } else {
                getScanDevice();
            }
        } else if (i != 100) {
        } else {
            if (!checkSelfPermission(this.permissionsLocation)) {
                requestPermissions(100, this.permissionsLocation);
            } else {
                ((ScanDevicePresenter) this.mPresenter).startScanBle(0);
            }
        }
    }

    private void getScanDevice() {
        this.mRefreshLayout.post(new Runnable() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$ScanDeviceReadyActivity$O1U0KUNJ0dJ1eL2IFMkwkGoEPo */

            public final void run() {
                ScanDeviceReadyActivity.this.lambda$getScanDevice$2$ScanDeviceReadyActivity();
            }
        });
    }

    public /* synthetic */ void lambda$getScanDevice$2$ScanDeviceReadyActivity() {
        this.mRefreshLayout.setRefreshing(true);
        this.showList.clear();
        ScanDeviceAdapter scanDeviceAdapter = this.mAdapter;
        if (scanDeviceAdapter != null) {
            scanDeviceAdapter.setData(this.showList);
        }
        if (checkSelfPermission(this.permissionsLocation)) {
            ((ScanDevicePresenter) this.mPresenter).startScanBle(0);
        } else {
            requestPermissions(100, this.permissionsLocation);
        }
    }

    public void requestPermissionsFail(int i) {
        super.requestPermissionsFail(i);
        if (i != 100) {
            return;
        }
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, this.permissionsLocation[0])) {
            this.mDialog = DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_location_title), getResources().getString(R.string.permisson_location_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$ScanDeviceReadyActivity$6KvmOfOxUWi67AOEU0V3KsWBTYA */

                public final void onClick(View view) {
                    ScanDeviceReadyActivity.this.lambda$requestPermissionsFail$3$ScanDeviceReadyActivity(view);
                }
            }, new View.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$ScanDeviceReadyActivity$ooPBtZgJI8gsRMeHVsgMTeYtPbU */

                public final void onClick(View view) {
                    ScanDeviceReadyActivity.this.lambda$requestPermissionsFail$4$ScanDeviceReadyActivity(view);
                }
            });
        } else {
            finish();
        }
    }

    public /* synthetic */ void lambda$requestPermissionsFail$3$ScanDeviceReadyActivity(View view) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getApplicationContext().getPackageName(), null));
        startActivityForResult(intent, 100);
    }

    public /* synthetic */ void lambda$requestPermissionsFail$4$ScanDeviceReadyActivity(View view) {
        this.mDialog.dismiss();
        finish();
    }

    public void requestPermissionsSuccess(int i) {
        if (i == 100) {
            ((ScanDevicePresenter) this.mPresenter).startScanBle(0);
        }
    }

    public void requestSuccess(int i, BLEDevice bLEDevice) {
        if (i == 0) {
            this.mRefreshLayout.onComplete();
            if (!this.showList.contains(bLEDevice)) {
                this.showList.add(bLEDevice);
                Collections.sort(this.showList);
                ScanDeviceAdapter scanDeviceAdapter = this.mAdapter;
                if (scanDeviceAdapter == null) {
                    this.mAdapter = new ScanDeviceAdapter(this, this.showList);
                    this.mRecyclerView.setAdapter(this.mAdapter);
                } else {
                    scanDeviceAdapter.setData(this.showList);
                }
                this.mAdapter.setOnItemClickListener(this);
            }
        } else if (i == 1) {
            this.isConnecting = false;
            SharePreferenceUtils.put(this, Constants.UPDATECONFIG, Constants.NOUPDATE);
            SharePreferenceUtils.put(AppApplication.getInstance(), Constants.IS_SYN_HISTORY_KEY, true);
            if (this.fromActivity.equals("PersonInfoActivity")) {
                IntentUtil.goToActivityAndFinish(this, MainActivity.class);
            } else if (this.fromActivity.equals("MainActivity")) {
                hideLoading();
                setResult(200);
                finish();
            }
        }
    }

    public void requestFaild() {
        ScanDeviceAdapter scanDeviceAdapter = this.mAdapter;
        if (scanDeviceAdapter != null) {
            this.isConnecting = false;
            scanDeviceAdapter.connecting(-1);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mRefreshLayout.onComplete();
        ((ScanDevicePresenter) this.mPresenter).stopScanBle();
        hideLoading();
    }

    public void onItemClick(View view, int i) {
        if (!BleScanTool.getInstance().isBluetoothOpen()) {
            showToast(getResources().getString(R.string.fresh_ble_close));
        } else if (!this.isConnecting) {
            SharePreferenceUtils.put(this, Constants.UPDATECONFIG, Constants.UPDATE);
            this.mAdapter.connecting(i);
            this.isConnecting = true;
            ((ScanDevicePresenter) this.mPresenter).connecting(1, this.showList.get(i));
        }
    }

    public void onRefreshing() {
        if (this.isConnecting) {
            showToast(getResources().getString(R.string.connect_device_str));
        } else {
            isOpenBle();
        }
    }

    private void isOpenBle() {
        if (!BleScanTool.getInstance().isBluetoothOpen()) {
            LogUtil.dAndSave("扫描设备..蓝牙未打开.", Constants.LOG_PATH);
            this.mRefreshLayout.onComplete();
            new CommonDialog.Builder(this).isVertical(false).setTitle((int) R.string.fresh_ble_close).setLeftButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$ScanDeviceReadyActivity$wyqDYnOJnWwryxlOZGn4G9a0Ak */

                public final void onClick(DialogInterface dialogInterface, int i) {
                    ScanDeviceReadyActivity.this.lambda$isOpenBle$5$ScanDeviceReadyActivity(dialogInterface, i);
                }
            }).setMessage((int) R.string.open_ble_tips).setRightButton((int) R.string.setting, $$Lambda$ScanDeviceReadyActivity$1icJ1mewZO56Ge5tVUwqbu6i9MA.INSTANCE).create().show();
            return;
        }
        getScanDevice();
    }

    public /* synthetic */ void lambda$isOpenBle$5$ScanDeviceReadyActivity(DialogInterface dialogInterface, int i) {
        finish();
    }

    public void onBackPressed() {
        if (this.isConnecting) {
            showToast(getResources().getString(R.string.connect_device_str));
        } else if (this.fromActivity.equals("PersonInfoActivity")) {
            IntentUtil.goToActivityAndFinish(this, MainActivity.class);
        } else if (this.fromActivity.equals("MainActivity")) {
            finish();
        }
    }
}
