package com.wade.fit.util;

import com.wade.fit.model.bean.BaseMessage;
import org.greenrobot.eventbus.EventBus;

public class EventBusHelper {
    public static void register(Object obj) {
        unregister(obj);
        EventBus.getDefault().register(obj);
    }

    public static void unregister(Object obj) {
        EventBus.getDefault().unregister(obj);
    }

    public static void post(int i) {
        post(new BaseMessage(i));
    }

    public static void postSticky(int i) {
        EventBus.getDefault().postSticky(new BaseMessage(i));
    }

    public static void post(BaseMessage baseMessage) {
        EventBus.getDefault().post(baseMessage);
    }
}
