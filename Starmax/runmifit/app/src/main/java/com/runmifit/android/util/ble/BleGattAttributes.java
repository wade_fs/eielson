package com.wade.fit.util.ble;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;
import java.util.UUID;

public class BleGattAttributes {
    public static final UUID CLIENT_CHARACTERISTIC_CONFIG_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID NOTIFY_UUID_ECG = UUID.fromString("0000ef01-0000-1000-8000-00805f9b34fb");
    public static final UUID NOTIFY_UUID_NORMAL = UUID.fromString("0000ff01-0000-1000-8000-00805f9b34fb");
    public static final UUID SERVICE_UUID = UUID.fromString("0000ff00-0000-1000-8000-00805f9b34fb");
    public static final UUID[] SERVICE_UUIDS = {SERVICE_UUID};
    public static final UUID SERVICE_UUID_ECG = UUID.fromString("0000ef00-0000-1000-8000-00805f9b34fb");
    public static final UUID WRITE_UUID_ECG = UUID.fromString("0000ef02-0000-1000-8000-00805f9b34fb");
    public static final UUID WRITE_UUID_NORMAL = UUID.fromString("0000ff02-0000-1000-8000-00805f9b34fb");

    public static BluetoothGattCharacteristic getNormalWriteCharacteristic(BluetoothGatt bluetoothGatt) {
        return getCharacteristic(bluetoothGatt, SERVICE_UUID, WRITE_UUID_NORMAL);
    }

    public static boolean enablePeerDeviceNotifyNormal(BluetoothGatt bluetoothGatt, boolean z) {
        DebugLog.m6203d("enablePeerDeviceNotifyNormal: " + z);
        return enablePeerDeviceNotifyMe(bluetoothGatt, SERVICE_UUID, NOTIFY_UUID_NORMAL, z);
    }

    public static boolean enablePeerDeviceNotifyEcg(BluetoothGatt bluetoothGatt, boolean z) {
        DebugLog.m6203d("enablePeerDeviceNotifyNormal: " + z);
        return enablePeerDeviceNotifyMe(bluetoothGatt, SERVICE_UUID_ECG, NOTIFY_UUID_ECG, z);
    }

    private static boolean enablePeerDeviceNotifyMe(BluetoothGatt bluetoothGatt, UUID uuid, UUID uuid2, boolean z) {
        BluetoothGattCharacteristic characteristic;
        if (!(bluetoothGatt == null || (characteristic = getCharacteristic(bluetoothGatt, uuid, uuid2)) == null || (characteristic.getProperties() | 16) <= 0)) {
            boolean characteristicNotification = bluetoothGatt.setCharacteristicNotification(characteristic, z);
            String str = "uuid:" + uuid2 + ",enable:" + z + ",setCharacteristicNotification:" + characteristicNotification;
            DebugLog.m6203d(str);
            if (characteristicNotification && uuid2.equals(characteristic.getUuid())) {
                BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_UUID);
                descriptor.setValue(z ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                boolean writeDescriptor = bluetoothGatt.writeDescriptor(descriptor);
                LogUtil.d(str + ",writeDescriptor:" + writeDescriptor);
                return writeDescriptor;
            }
        }
        return false;
    }

    private static BluetoothGattCharacteristic getCharacteristic(BluetoothGatt bluetoothGatt, UUID uuid, UUID uuid2) {
        LogUtil.d("serviceId:" + uuid + ",characteristicId:" + uuid2);
        if (bluetoothGatt == null) {
            DebugLog.m6203d("gatt is nullllll");
            return null;
        }
        BluetoothGattService service = bluetoothGatt.getService(uuid);
        if (service != null) {
            return service.getCharacteristic(uuid2);
        }
        DebugLog.m6203d("service is nullllll");
        return null;
    }
}
