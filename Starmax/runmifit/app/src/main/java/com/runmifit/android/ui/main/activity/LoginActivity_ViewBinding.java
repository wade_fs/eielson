package com.wade.fit.ui.main.activity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.main.activity.LoginActivity_ViewBinding */
public class LoginActivity_ViewBinding implements Unbinder {
    private LoginActivity target;
    private View view2131296341;
    private View view2131296525;
    private View view2131296533;
    private View view2131297025;
    private View view2131297026;
    private View view2131297041;
    private View view2131297048;
    private View view2131297090;

    public LoginActivity_ViewBinding(LoginActivity loginActivity) {
        this(loginActivity, loginActivity.getWindow().getDecorView());
    }

    public LoginActivity_ViewBinding(final LoginActivity loginActivity, View view) {
        this.target = loginActivity;
        loginActivity.etUsername = (EditText) Utils.findRequiredViewAsType(view, R.id.et_username, "field 'etUsername'", EditText.class);
        loginActivity.etPwd = (EditText) Utils.findRequiredViewAsType(view, R.id.et_pwd, "field 'etPwd'", EditText.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.btnlogin, "field 'btnLogin' and method 'sendLogin'");
        loginActivity.btnLogin = (Button) Utils.castView(findRequiredView, R.id.btnlogin, "field 'btnLogin'", Button.class);
        this.view2131296341 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.LoginActivity_ViewBinding.C25241 */

            public void doClick(View view) {
                loginActivity.sendLogin();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.tvSkip, "field 'tvSkip' and method 'toMain'");
        loginActivity.tvSkip = (TextView) Utils.castView(findRequiredView2, R.id.tvSkip, "field 'tvSkip'", TextView.class);
        this.view2131297048 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.LoginActivity_ViewBinding.C25252 */

            public void doClick(View view) {
                loginActivity.toMain();
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.imgStatus, "field 'imgStatus' and method 'updatePwdStatus'");
        loginActivity.imgStatus = (ImageView) Utils.castView(findRequiredView3, R.id.imgStatus, "field 'imgStatus'", ImageView.class);
        this.view2131296533 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.LoginActivity_ViewBinding.C25263 */

            public void doClick(View view) {
                loginActivity.updatePwdStatus();
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.imgClean, "field 'imgClean' and method 'cleanUserName'");
        loginActivity.imgClean = (ImageView) Utils.castView(findRequiredView4, R.id.imgClean, "field 'imgClean'", ImageView.class);
        this.view2131296525 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.LoginActivity_ViewBinding.C25274 */

            public void doClick(View view) {
                loginActivity.cleanUserName();
            }
        });
        View findRequiredView5 = Utils.findRequiredView(view, R.id.tvRegister, "method 'toRegister'");
        this.view2131297041 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.LoginActivity_ViewBinding.C25285 */

            public void doClick(View view) {
                loginActivity.toRegister();
            }
        });
        View findRequiredView6 = Utils.findRequiredView(view, R.id.tv_forget_pwd, "method 'toUpdatePassword'");
        this.view2131297090 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.LoginActivity_ViewBinding.C25296 */

            public void doClick(View view) {
                loginActivity.toUpdatePassword();
            }
        });
        View findRequiredView7 = Utils.findRequiredView(view, R.id.tvLoginWechat, "method 'loginByWechat'");
        this.view2131297026 = findRequiredView7;
        findRequiredView7.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.LoginActivity_ViewBinding.C25307 */

            public void doClick(View view) {
                loginActivity.loginByWechat();
            }
        });
        View findRequiredView8 = Utils.findRequiredView(view, R.id.tvLoginFacebook, "method 'loginByFacebook'");
        this.view2131297025 = findRequiredView8;
        findRequiredView8.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.main.activity.LoginActivity_ViewBinding.C25318 */

            public void doClick(View view) {
                loginActivity.loginByFacebook();
            }
        });
    }

    public void unbind() {
        LoginActivity loginActivity = this.target;
        if (loginActivity != null) {
            this.target = null;
            loginActivity.etUsername = null;
            loginActivity.etPwd = null;
            loginActivity.btnLogin = null;
            loginActivity.tvSkip = null;
            loginActivity.imgStatus = null;
            loginActivity.imgClean = null;
            this.view2131296341.setOnClickListener(null);
            this.view2131296341 = null;
            this.view2131297048.setOnClickListener(null);
            this.view2131297048 = null;
            this.view2131296533.setOnClickListener(null);
            this.view2131296533 = null;
            this.view2131296525.setOnClickListener(null);
            this.view2131296525 = null;
            this.view2131297041.setOnClickListener(null);
            this.view2131297041 = null;
            this.view2131297090.setOnClickListener(null);
            this.view2131297090 = null;
            this.view2131297026.setOnClickListener(null);
            this.view2131297026 = null;
            this.view2131297025.setOnClickListener(null);
            this.view2131297025 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
