package com.wade.fit.model.bean;

import com.wade.fit.greendao.bean.EcgRecordInfo;
import java.util.List;

public class ECGRecordShowList {
    private String dateMonth;
    private List<EcgRecordInfo> recordInfos;

    public String getDateMonth() {
        return this.dateMonth;
    }

    public void setDateMonth(String str) {
        this.dateMonth = str;
    }

    public List<EcgRecordInfo> getRecordInfos() {
        return this.recordInfos;
    }

    public void setRecordInfos(List<EcgRecordInfo> list) {
        this.recordInfos = list;
    }
}
