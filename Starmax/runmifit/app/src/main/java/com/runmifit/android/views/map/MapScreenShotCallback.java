package com.wade.fit.views.map;

import android.graphics.Bitmap;

public interface MapScreenShotCallback {
    void shotComplet(Bitmap bitmap);
}
