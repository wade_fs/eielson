package com.wade.fit.ui.sport.activity;

import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.CustomExpandableListView;
import com.wade.fit.views.SportRecordChart;

/* renamed from: com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding */
public class SportRecordActivityNew_ViewBinding implements Unbinder {
    private SportRecordActivityNew target;
    private View view2131296572;
    private View view2131296575;
    private View view2131296576;
    private View view2131296769;
    private View view2131296770;
    private View view2131296771;
    private View view2131296772;
    private View view2131296805;
    private View view2131296806;
    private View view2131296807;
    private View view2131296955;
    private View view2131296995;

    public SportRecordActivityNew_ViewBinding(SportRecordActivityNew sportRecordActivityNew) {
        this(sportRecordActivityNew, sportRecordActivityNew.getWindow().getDecorView());
    }

    public SportRecordActivityNew_ViewBinding(final SportRecordActivityNew sportRecordActivityNew, View view) {
        this.target = sportRecordActivityNew;
        sportRecordActivityNew.barBg = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.barBg, "field 'barBg'", RelativeLayout.class);
        sportRecordActivityNew.viewTime = Utils.findRequiredView(view, R.id.viewTime, "field 'viewTime'");
        sportRecordActivityNew.viewDistance = Utils.findRequiredView(view, R.id.viewDistance, "field 'viewDistance'");
        sportRecordActivityNew.viewCalor = Utils.findRequiredView(view, R.id.viewCalor, "field 'viewCalor'");
        sportRecordActivityNew.tvTime = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTime, "field 'tvTime'", TextView.class);
        sportRecordActivityNew.tvDistance = (TextView) Utils.findRequiredViewAsType(view, R.id.tvDistance, "field 'tvDistance'", TextView.class);
        sportRecordActivityNew.tvCalory = (TextView) Utils.findRequiredViewAsType(view, R.id.tvCalory, "field 'tvCalory'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.tvDate, "field 'tvDate' and method 'selectDay'");
        sportRecordActivityNew.tvDate = (TextView) Utils.castView(findRequiredView, R.id.tvDate, "field 'tvDate'", TextView.class);
        this.view2131296995 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26251 */

            public void doClick(View view) {
                sportRecordActivityNew.selectDay();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ivSelectedDate, "field 'ivmSearchDate' and method 'selectDay'");
        sportRecordActivityNew.ivmSearchDate = (ImageView) Utils.castView(findRequiredView2, R.id.ivSelectedDate, "field 'ivmSearchDate'", ImageView.class);
        this.view2131296576 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26292 */

            public void doClick(View view) {
                sportRecordActivityNew.selectDay();
            }
        });
        sportRecordActivityNew.mRecyclerView = (CustomExpandableListView) Utils.findRequiredViewAsType(view, R.id.mRecyclerView, "field 'mRecyclerView'", CustomExpandableListView.class);
        sportRecordActivityNew.sportChartTime = (SportRecordChart) Utils.findRequiredViewAsType(view, R.id.sportChart_time, "field 'sportChartTime'", SportRecordChart.class);
        sportRecordActivityNew.sportChartDistance = (SportRecordChart) Utils.findRequiredViewAsType(view, R.id.sportChart_distance, "field 'sportChartDistance'", SportRecordChart.class);
        sportRecordActivityNew.sportChartCalory = (SportRecordChart) Utils.findRequiredViewAsType(view, R.id.sportChart_calory, "field 'sportChartCalory'", SportRecordChart.class);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.ivPreDate, "field 'ivPreDate' and method 'changePreDay'");
        sportRecordActivityNew.ivPreDate = (ImageView) Utils.castView(findRequiredView3, R.id.ivPreDate, "field 'ivPreDate'", ImageView.class);
        this.view2131296575 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26303 */

            public void doClick(View view) {
                sportRecordActivityNew.changePreDay();
            }
        });
        View findRequiredView4 = Utils.findRequiredView(view, R.id.ivNextDate, "field 'ivNextDate' and method 'changeNextDay'");
        sportRecordActivityNew.ivNextDate = (ImageView) Utils.castView(findRequiredView4, R.id.ivNextDate, "field 'ivNextDate'", ImageView.class);
        this.view2131296572 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26314 */

            public void doClick(View view) {
                sportRecordActivityNew.changeNextDay();
            }
        });
        sportRecordActivityNew.mDateGroup = (RadioGroup) Utils.findRequiredViewAsType(view, R.id.rg_date, "field 'mDateGroup'", RadioGroup.class);
        View findRequiredView5 = Utils.findRequiredView(view, R.id.rbDay, "field 'mDayTab' and method 'showDayData'");
        sportRecordActivityNew.mDayTab = (RadioButton) Utils.castView(findRequiredView5, R.id.rbDay, "field 'mDayTab'", RadioButton.class);
        this.view2131296769 = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26325 */

            public void doClick(View view) {
                sportRecordActivityNew.showDayData();
            }
        });
        View findRequiredView6 = Utils.findRequiredView(view, R.id.rbOneMonth, "field 'mOneMonthTab' and method 'showOneMonthData'");
        sportRecordActivityNew.mOneMonthTab = (RadioButton) Utils.castView(findRequiredView6, R.id.rbOneMonth, "field 'mOneMonthTab'", RadioButton.class);
        this.view2131296770 = findRequiredView6;
        findRequiredView6.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26336 */

            public void doClick(View view) {
                sportRecordActivityNew.showOneMonthData();
            }
        });
        View findRequiredView7 = Utils.findRequiredView(view, R.id.rbSixMonth, "field 'mSixMonthTab' and method 'showSixMonthData'");
        sportRecordActivityNew.mSixMonthTab = (RadioButton) Utils.castView(findRequiredView7, R.id.rbSixMonth, "field 'mSixMonthTab'", RadioButton.class);
        this.view2131296771 = findRequiredView7;
        findRequiredView7.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26347 */

            public void doClick(View view) {
                sportRecordActivityNew.showSixMonthData();
            }
        });
        View findRequiredView8 = Utils.findRequiredView(view, R.id.rbYear, "field 'mYearTab' and method 'showYearData'");
        sportRecordActivityNew.mYearTab = (RadioButton) Utils.castView(findRequiredView8, R.id.rbYear, "field 'mYearTab'", RadioButton.class);
        this.view2131296772 = findRequiredView8;
        findRequiredView8.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26358 */

            public void doClick(View view) {
                sportRecordActivityNew.showYearData();
            }
        });
        sportRecordActivityNew.llNoData = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.nodata, "field 'llNoData'", LinearLayout.class);
        sportRecordActivityNew.nesData = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.nesData, "field 'nesData'", LinearLayout.class);
        sportRecordActivityNew.mNestedScrollView = (NestedScrollView) Utils.findRequiredViewAsType(view, R.id.mNestedScrollView, "field 'mNestedScrollView'", NestedScrollView.class);
        View findRequiredView9 = Utils.findRequiredView(view, R.id.title_back, "method 'finishToback'");
        this.view2131296955 = findRequiredView9;
        findRequiredView9.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C26369 */

            public void doClick(View view) {
                sportRecordActivityNew.finishToback();
            }
        });
        View findRequiredView10 = Utils.findRequiredView(view, R.id.rlLayoutTime, "method 'showTimeData'");
        this.view2131296807 = findRequiredView10;
        findRequiredView10.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C262610 */

            public void doClick(View view) {
                sportRecordActivityNew.showTimeData();
            }
        });
        View findRequiredView11 = Utils.findRequiredView(view, R.id.rlLayoutDistance, "method 'showDistanceData'");
        this.view2131296806 = findRequiredView11;
        findRequiredView11.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C262711 */

            public void doClick(View view) {
                sportRecordActivityNew.showDistanceData();
            }
        });
        View findRequiredView12 = Utils.findRequiredView(view, R.id.rlLayoutCalory, "method 'showCaloryData'");
        this.view2131296805 = findRequiredView12;
        findRequiredView12.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew_ViewBinding.C262812 */

            public void doClick(View view) {
                sportRecordActivityNew.showCaloryData();
            }
        });
    }

    public void unbind() {
        SportRecordActivityNew sportRecordActivityNew = this.target;
        if (sportRecordActivityNew != null) {
            this.target = null;
            sportRecordActivityNew.barBg = null;
            sportRecordActivityNew.viewTime = null;
            sportRecordActivityNew.viewDistance = null;
            sportRecordActivityNew.viewCalor = null;
            sportRecordActivityNew.tvTime = null;
            sportRecordActivityNew.tvDistance = null;
            sportRecordActivityNew.tvCalory = null;
            sportRecordActivityNew.tvDate = null;
            sportRecordActivityNew.ivmSearchDate = null;
            sportRecordActivityNew.mRecyclerView = null;
            sportRecordActivityNew.sportChartTime = null;
            sportRecordActivityNew.sportChartDistance = null;
            sportRecordActivityNew.sportChartCalory = null;
            sportRecordActivityNew.ivPreDate = null;
            sportRecordActivityNew.ivNextDate = null;
            sportRecordActivityNew.mDateGroup = null;
            sportRecordActivityNew.mDayTab = null;
            sportRecordActivityNew.mOneMonthTab = null;
            sportRecordActivityNew.mSixMonthTab = null;
            sportRecordActivityNew.mYearTab = null;
            sportRecordActivityNew.llNoData = null;
            sportRecordActivityNew.nesData = null;
            sportRecordActivityNew.mNestedScrollView = null;
            this.view2131296995.setOnClickListener(null);
            this.view2131296995 = null;
            this.view2131296576.setOnClickListener(null);
            this.view2131296576 = null;
            this.view2131296575.setOnClickListener(null);
            this.view2131296575 = null;
            this.view2131296572.setOnClickListener(null);
            this.view2131296572 = null;
            this.view2131296769.setOnClickListener(null);
            this.view2131296769 = null;
            this.view2131296770.setOnClickListener(null);
            this.view2131296770 = null;
            this.view2131296771.setOnClickListener(null);
            this.view2131296771 = null;
            this.view2131296772.setOnClickListener(null);
            this.view2131296772 = null;
            this.view2131296955.setOnClickListener(null);
            this.view2131296955 = null;
            this.view2131296807.setOnClickListener(null);
            this.view2131296807 = null;
            this.view2131296806.setOnClickListener(null);
            this.view2131296806 = null;
            this.view2131296805.setOnClickListener(null);
            this.view2131296805 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
