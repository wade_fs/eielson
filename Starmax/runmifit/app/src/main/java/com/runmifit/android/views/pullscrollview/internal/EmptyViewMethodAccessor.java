package com.wade.fit.views.pullscrollview.internal;

import android.view.View;

public interface EmptyViewMethodAccessor {
    void setEmptyView(View view);

    void setEmptyViewInternal(View view);
}
