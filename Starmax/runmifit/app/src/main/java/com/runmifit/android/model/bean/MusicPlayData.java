package com.wade.fit.model.bean;

import android.graphics.drawable.Drawable;
import java.io.Serializable;

public class MusicPlayData implements Serializable {
    private static final long serialVersionUID = 1;
    public String packageName;
    public Drawable playIcon;
    public String playName;

    public String getPlayName() {
        return this.playName;
    }

    public void setPlayName(String str) {
        this.playName = str;
    }

    public Drawable getPlayIcon() {
        return this.playIcon;
    }

    public void setPlayIcon(Drawable drawable) {
        this.playIcon = drawable;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String str) {
        this.packageName = str;
    }

    public boolean equals(Object obj) {
        if (obj instanceof MusicPlayData) {
            return getPackageName().equals(((MusicPlayData) obj).getPackageName());
        }
        return super.equals(obj);
    }
}
