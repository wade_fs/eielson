package com.wade.fit.util.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v4.util.LruCache;
import android.widget.ImageView;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.util.ScreenUtil;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SDCardImageLoader {
    private static SDCardImageLoader imageLoader;
    private ExecutorService executorService = Executors.newFixedThreadPool(3);
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public LruCache<String, Bitmap> imageCache;
    /* access modifiers changed from: private */
    public int screenH;
    /* access modifiers changed from: private */
    public int screenW;

    public interface ImageCallback {
        void imageLoaded(Bitmap bitmap);
    }

    public static SDCardImageLoader getInstance() {
        if (imageLoader == null) {
            imageLoader = new SDCardImageLoader(ScreenUtil.getScreenWidth(AppApplication.getInstance()), ScreenUtil.getScreenHeight(AppApplication.getInstance()));
        }
        return imageLoader;
    }

    private SDCardImageLoader(int i, int i2) {
        this.screenW = i;
        this.screenH = i2;
        this.imageCache = new LruCache<String, Bitmap>(((int) (Runtime.getRuntime().maxMemory() / 1024)) / 8) {
            /* class com.wade.fit.util.image.SDCardImageLoader.C26971 */

            /* access modifiers changed from: protected */
            public int sizeOf(String str, Bitmap bitmap) {
                return bitmap.getRowBytes() * bitmap.getHeight();
            }
        };
    }

    private Bitmap loadDrawable(final int i, final String str, final ImageCallback imageCallback) {
        if (this.imageCache.get(str) != null) {
            return this.imageCache.get(str);
        }
        this.executorService.submit(new Runnable() {
            /* class com.wade.fit.util.image.SDCardImageLoader.C26982 */

            public void run() {
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(str, options);
                    int i = options.outWidth;
                    int i2 = options.outHeight;
                    if (i == 0) {
                        return;
                    }
                    if (i2 != 0) {
                        options.inSampleSize = i;
                        if (i > i2) {
                            if (i > SDCardImageLoader.this.screenW) {
                                options.inSampleSize *= i / SDCardImageLoader.this.screenW;
                            }
                        } else if (i2 > SDCardImageLoader.this.screenH) {
                            options.inSampleSize *= i2 / SDCardImageLoader.this.screenH;
                        }
                        options.inJustDecodeBounds = false;
                        final Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
                        SDCardImageLoader.this.imageCache.put(str, decodeFile);
                        if (imageCallback != null) {
                            SDCardImageLoader.this.handler.post(new Runnable() {
                                /* class com.wade.fit.util.image.SDCardImageLoader.C26982.C26991 */

                                public void run() {
                                    imageCallback.imageLoaded(decodeFile);
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return null;
    }

    public void loadImage(int i, final String str, final ImageView imageView) {
        Bitmap loadDrawable = loadDrawable(i, str, new ImageCallback() {
            /* class com.wade.fit.util.image.SDCardImageLoader.C27003 */

            public void imageLoaded(Bitmap bitmap) {
                if (!imageView.getTag().equals(str)) {
                    return;
                }
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                } else {
                    imageView.setImageResource(R.mipmap.empty_photo);
                }
            }
        });
        if (loadDrawable == null) {
            imageView.setImageResource(R.mipmap.empty_photo);
        } else if (imageView.getTag().equals(str)) {
            imageView.setImageBitmap(loadDrawable);
        }
    }

    public static final int caculateInSampleSize(BitmapFactory.Options options, int i, int i2) {
        int i3 = options.outHeight;
        int i4 = options.outWidth;
        if (i == 0 || i2 == 0) {
            return 1;
        }
        if (i3 <= i2 && i4 <= i) {
            return 1;
        }
        int round = Math.round(((float) i3) / ((float) i2));
        int round2 = Math.round(((float) i4) / ((float) i));
        return round < round2 ? round : round2;
    }

    public static final Bitmap compressBitmap(String str, int i, int i2) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        options.inSampleSize = caculateInSampleSize(options, i, i2);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(str, options);
    }

    public Bitmap compressBitmap(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        boolean z = true;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        int i = options.outWidth;
        int i2 = options.outHeight;
        int screenHeight = i / ScreenUtil.getScreenHeight(AppApplication.getInstance());
        int screenHeight2 = i2 / ScreenUtil.getScreenHeight(AppApplication.getInstance());
        int i3 = (screenHeight > screenHeight2) & (screenHeight2 >= 1) ? screenHeight : 1;
        boolean z2 = screenHeight2 > screenHeight;
        if (screenHeight < 1) {
            z = false;
        }
        if (!z || !z2) {
            screenHeight2 = i3;
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = screenHeight2;
        return BitmapFactory.decodeFile(str, options);
    }

    public void addBitmapToMemoryCache(String str, Bitmap bitmap) {
        if (getBitmapFromMemoryCache(str) == null) {
            this.imageCache.put(str, bitmap);
        }
    }

    public void removeBitmapFromMemoryCache(String str) {
        if (getBitmapFromMemoryCache(str) != null) {
            this.imageCache.remove(str);
        }
    }

    public Bitmap getBitmapFromMemoryCache(String str) {
        return this.imageCache.get(str);
    }
}
