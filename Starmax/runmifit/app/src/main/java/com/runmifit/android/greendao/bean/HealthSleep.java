package com.wade.fit.greendao.bean;

public class HealthSleep {
    private int awakeCount;
    private long date;
    private int day;
    private int deepSleepCount;
    private int deepSleepMinutes;
    private int eyeMinutes;
    private boolean isUploaded;
    private int lightSleepCount;
    private int lightSleepMinutes;
    private String macAddress;
    private int month;
    private String remark;
    private int sleepEndedTimeH;
    private int sleepEndedTimeM;
    private int sleepMinutes;
    private int sleepstartedTimeH;
    private int sleepstartedTimeM;
    private int totalSleepMinutes;
    private String userId;
    private int wakeMunutes;
    private int year;

    public HealthSleep() {
    }

    public HealthSleep(boolean z, String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, int i14, int i15, int i16, long j, String str2, String str3) {
        this.isUploaded = z;
        this.macAddress = str;
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.sleepEndedTimeH = i4;
        this.sleepEndedTimeM = i5;
        this.sleepstartedTimeH = i6;
        this.sleepstartedTimeM = i7;
        this.totalSleepMinutes = i8;
        this.lightSleepCount = i9;
        this.deepSleepCount = i10;
        this.awakeCount = i11;
        this.lightSleepMinutes = i12;
        this.deepSleepMinutes = i13;
        this.wakeMunutes = i14;
        this.sleepMinutes = i15;
        this.eyeMinutes = i16;
        this.date = j;
        this.userId = str2;
        this.remark = str3;
    }

    public boolean getIsUploaded() {
        return this.isUploaded;
    }

    public void setIsUploaded(boolean z) {
        this.isUploaded = z;
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getSleepEndedTimeH() {
        return this.sleepEndedTimeH;
    }

    public void setSleepEndedTimeH(int i) {
        this.sleepEndedTimeH = i;
    }

    public int getSleepEndedTimeM() {
        return this.sleepEndedTimeM;
    }

    public void setSleepEndedTimeM(int i) {
        this.sleepEndedTimeM = i;
    }

    public int getTotalSleepMinutes() {
        return this.totalSleepMinutes;
    }

    public void setTotalSleepMinutes(int i) {
        this.totalSleepMinutes = i;
    }

    public int getLightSleepCount() {
        return this.lightSleepCount;
    }

    public void setLightSleepCount(int i) {
        this.lightSleepCount = i;
    }

    public int getDeepSleepCount() {
        return this.deepSleepCount;
    }

    public void setDeepSleepCount(int i) {
        this.deepSleepCount = i;
    }

    public int getAwakeCount() {
        return this.awakeCount;
    }

    public void setAwakeCount(int i) {
        this.awakeCount = i;
    }

    public int getLightSleepMinutes() {
        return this.lightSleepMinutes;
    }

    public void setLightSleepMinutes(int i) {
        this.lightSleepMinutes = i;
    }

    public int getDeepSleepMinutes() {
        return this.deepSleepMinutes;
    }

    public void setDeepSleepMinutes(int i) {
        this.deepSleepMinutes = i;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String str) {
        this.remark = str;
    }

    public int getWakeMunutes() {
        return this.wakeMunutes;
    }

    public void setWakeMunutes(int i) {
        this.wakeMunutes = i;
    }

    public int getSleepMinutes() {
        return this.sleepMinutes;
    }

    public void setSleepMinutes(int i) {
        this.sleepMinutes = i;
    }

    public int getEyeMinutes() {
        return this.eyeMinutes;
    }

    public void setEyeMinutes(int i) {
        this.eyeMinutes = i;
    }

    public int getSleepstartedTimeH() {
        return this.sleepstartedTimeH;
    }

    public void setSleepstartedTimeH(int i) {
        this.sleepstartedTimeH = i;
    }

    public int getSleepstartedTimeM() {
        return this.sleepstartedTimeM;
    }

    public void setSleepstartedTimeM(int i) {
        this.sleepstartedTimeM = i;
    }

    public String toString() {
        return "HealthSleep{, year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", sleepEndedTimeH=" + this.sleepEndedTimeH + ", sleepEndedTimeM=" + this.sleepEndedTimeM + ", sleepstartedTimeH=" + this.sleepstartedTimeH + ", sleepstartedTimeM=" + this.sleepstartedTimeM + ", totalSleepMinutes=" + this.totalSleepMinutes + ", lightSleepMinutes=" + this.lightSleepMinutes + ", deepSleepMinutes=" + this.deepSleepMinutes + ", wakeMunutes=" + this.wakeMunutes + ", sleepMinutes=" + this.sleepMinutes + ", eyeMinutes=" + this.eyeMinutes + ", userId='" + this.userId + '\'' + ", remark='" + this.remark + '\'' + '}';
    }
}
