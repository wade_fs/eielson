package com.wade.fit.model.bean;

public class Goal {
    public int calstate;
    public int distancestate;
    public int goalCal = 500;
    public int goalDistanceKm = 5;
    public int goalDistanceLb = 6;
    public int goalSleep = 8;
    public int goalStep = 10000;
    public int sleepstate;
    public int stepstate = 1;

    public String toString() {
        return "Goal{goalStep=" + this.goalStep + ", goalDistanceKm=" + this.goalDistanceKm + ", goalDistanceLb=" + this.goalDistanceLb + ", goalCal=" + this.goalCal + ", goalSleep=" + this.goalSleep + ", sleepstate=" + this.sleepstate + ", stepstate=" + this.stepstate + ", calstate=" + this.calstate + ", distancestate=" + this.distancestate + '}';
    }
}
