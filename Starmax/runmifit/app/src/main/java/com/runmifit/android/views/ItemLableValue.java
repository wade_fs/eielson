package com.wade.fit.views;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.log.DebugLog;

public class ItemLableValue extends RelativeLayout {
    protected int bottomLineColor;
    private Context context;
    private boolean hasBottomLine;
    private boolean hasTopLine;
    private boolean has_all_line;
    private String lable;
    private TextView lableView;
    private View.OnClickListener onClick;
    protected Paint paint;
    public View rightIv;
    /* access modifiers changed from: private */
    public String targetActivty;
    protected int topLineColor;
    private String value;
    public ValueStateTextView valueView;

    public ItemLableValue(Context context2, AttributeSet attributeSet) {
        this(context2, attributeSet, 0);
    }

    public ItemLableValue(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        this.onClick = new View.OnClickListener() {
            /* class com.wade.fit.views.ItemLableValue.C27121 */

            public void onClick(View view) {
                DebugLog.m6203d("onClick : targetActivty = " + ItemLableValue.this.targetActivty);
                if (ItemLableValue.this.targetActivty != null && ItemLableValue.this.valueView.isEnabled()) {
                    Intent intent = new Intent();
                    intent.setClassName(ItemLableValue.this.getContext(), ItemLableValue.this.targetActivty);
                    ItemLableValue.this.getContext().startActivity(intent);
                }
            }
        };
        init(context2, attributeSet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.wade.fit.views.ItemLableValue, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void init(Context context2, AttributeSet attributeSet) {
        this.context = context2;
        LayoutInflater.from(context2).inflate((int) R.layout.layout_item_lable_value, (ViewGroup) this, true);
        this.lableView = (TextView) findViewById(R.id.lable);
        this.valueView = (ValueStateTextView) findViewById(R.id.value);
        ImageView imageView = (ImageView) findViewById(R.id.left_drawable);
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet, R.styleable.ItemLableValue);
        this.lable = obtainStyledAttributes.getString(8);
        this.value = obtainStyledAttributes.getString(17);
        this.targetActivty = obtainStyledAttributes.getString(13);
        int color = obtainStyledAttributes.getColor(16, 0);
        int integer = obtainStyledAttributes.getInteger(10, 12);
        this.has_all_line = obtainStyledAttributes.getBoolean(3, false);
        setHasBottomLine(obtainStyledAttributes.getBoolean(4, true));
        setHasTopLine(obtainStyledAttributes.getBoolean(5, false));
        if (isHasBottomLine()) {
            this.bottomLineColor = obtainStyledAttributes.getColor(1, getResources().getColor(R.color.driver_color));
            initDraw();
        }
        if (isHasTopLine()) {
            this.topLineColor = obtainStyledAttributes.getColor(14, getResources().getColor(R.color.driver_color));
            initTopDraw();
        }
        Drawable drawable = obtainStyledAttributes.getDrawable(12);
        Drawable drawable2 = obtainStyledAttributes.getDrawable(9);
        if (drawable2 == null) {
            imageView.setVisibility(View.GONE);
        }
        imageView.setImageDrawable(drawable2);
        obtainStyledAttributes.recycle();
        if (drawable != null) {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            this.valueView.setCompoundDrawables(null, null, drawable, null);
        }
        if (color != 0) {
            this.valueView.setTextColor(color);
        }
        this.lableView.setText(this.lable);
        this.valueView.setText(this.value);
        this.valueView.setTextSize((float) integer);
        if (this.targetActivty != null) {
            setOnClickListener(this.onClick);
        }
    }

    /* access modifiers changed from: protected */
    public void initDraw() {
        setWillNotDraw(false);
        this.paint = new Paint(1);
        this.paint.setColor(this.bottomLineColor);
        this.paint.setStrokeWidth(2.0f);
    }

    /* access modifiers changed from: protected */
    public void initTopDraw() {
        setWillNotDraw(false);
        this.paint = new Paint(1);
        this.paint.setColor(this.topLineColor);
        this.paint.setStrokeWidth(2.0f);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isHasBottomLine()) {
            if (this.has_all_line) {
                canvas.drawLine(0.0f, (float) getMeasuredHeight(), (float) getMeasuredWidth(), (float) getMeasuredHeight(), this.paint);
            } else {
                canvas.drawLine((float) ScreenUtil.dp2px(35.0f), (float) getMeasuredHeight(), (float) getMeasuredWidth(), (float) getMeasuredHeight(), this.paint);
            }
        }
        if (!isHasTopLine()) {
            return;
        }
        if (this.has_all_line) {
            canvas.drawLine(0.0f, 0.0f, (float) getMeasuredWidth(), 0.0f, this.paint);
            return;
        }
        canvas.drawLine((float) ScreenUtil.dp2px(35.0f), 0.0f, (float) getMeasuredWidth(), 0.0f, this.paint);
    }

    public void setEnable(boolean z) {
        super.setEnabled(z);
        this.valueView.setEnabled(z);
    }

    public boolean isEnable() {
        return this.valueView.isEnabled();
    }

    public String getValue() {
        return this.valueView.getText().toString().trim();
    }

    public void setValue(String str) {
        this.valueView.setText(str);
    }

    public boolean isOpen() {
        return this.valueView.isOpen();
    }

    public void setOpen(boolean z) {
        this.valueView.setOpen(z);
        this.valueView.setText(z ? R.string.remind_state_open : R.string.remind_state_close);
    }

    public void setValueState(boolean z, int i) {
        setEnable(z);
        setOpen(z);
        this.valueView.setText(i);
    }

    public void setValueState(boolean z, String str) {
        setEnable(z);
        setOpen(z);
        this.valueView.setText(str);
    }

    public void setValueColor(int i) {
        this.valueView.setTextColor(i);
    }

    public boolean isHasBottomLine() {
        return this.hasBottomLine;
    }

    public void setHasBottomLine(boolean z) {
        this.hasBottomLine = z;
        invalidate();
    }

    public boolean isHasTopLine() {
        return this.hasTopLine;
    }

    public void setHasTopLine(boolean z) {
        this.hasTopLine = z;
        invalidate();
    }

    public View getTextView() {
        return this.lableView;
    }

    public void setLable(String str) {
        this.lableView.setText(str);
    }

    public ValueStateTextView getValueView() {
        return this.valueView;
    }

    public void setLableColor(int i) {
        this.lableView.setTextColor(i);
    }
}
