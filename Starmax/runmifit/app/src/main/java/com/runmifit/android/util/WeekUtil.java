package com.wade.fit.util;

import android.content.Context;
import com.wade.fit.R;
import com.wade.fit.model.bean.WeekMonthYearEnum;
import java.util.Calendar;

public class WeekUtil {
    public static String[] getWeeksByWeekStartDay(Context context, int i) {
        String[] stringArray = context.getResources().getStringArray(R.array.weekDay);
        String[] strArr = new String[stringArray.length];
        int i2 = 0;
        if (i == 0) {
            while (i2 < stringArray.length) {
                if (i2 == 0) {
                    strArr[i2] = stringArray[stringArray.length - 1];
                } else {
                    strArr[i2] = stringArray[i2 - 1];
                }
                i2++;
            }
        } else if (i == 1) {
            while (i2 < stringArray.length) {
                strArr[i2] = stringArray[i2];
                i2++;
            }
        } else if (i == 2) {
            for (int i3 = 0; i3 < stringArray.length; i3++) {
                if (i3 == stringArray.length - 1) {
                    strArr[i3] = stringArray[0];
                } else {
                    strArr[i3] = stringArray[i3 + 1];
                }
            }
        }
        return strArr;
    }

    public static int getDayOfWeek() {
        return Calendar.getInstance().get(7);
    }

    public static int getDayOfMonth() {
        return Calendar.getInstance().get(5);
    }

    public static int getMonthOfYear() {
        return Calendar.getInstance().get(2) + 1;
    }

    /* renamed from: com.wade.fit.util.WeekUtil$1 */
    static /* synthetic */ class C26511 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$WeekMonthYearEnum = new int[WeekMonthYearEnum.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.wade.fit.model.bean.WeekMonthYearEnum[] r0 = com.wade.fit.model.bean.WeekMonthYearEnum.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.util.WeekUtil.C26511.$SwitchMap$com$wade.fit$model$bean$WeekMonthYearEnum = r0
                int[] r0 = com.wade.fit.util.WeekUtil.C26511.$SwitchMap$com$wade.fit$model$bean$WeekMonthYearEnum     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.WeekMonthYearEnum r1 = com.wade.fit.model.bean.WeekMonthYearEnum.WEEK     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.util.WeekUtil.C26511.$SwitchMap$com$wade.fit$model$bean$WeekMonthYearEnum     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.WeekMonthYearEnum r1 = com.wade.fit.model.bean.WeekMonthYearEnum.MONTH     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.util.WeekUtil.C26511.$SwitchMap$com$wade.fit$model$bean$WeekMonthYearEnum     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.WeekMonthYearEnum r1 = com.wade.fit.model.bean.WeekMonthYearEnum.YEAR     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.WeekUtil.C26511.<clinit>():void");
        }
    }

    public static int getOffsetIndex(WeekMonthYearEnum weekMonthYearEnum) {
        int i;
        int i2 = C26511.$SwitchMap$com$wade.fit$model$bean$WeekMonthYearEnum[weekMonthYearEnum.ordinal()];
        if (i2 == 1) {
            int weekStartIndex = SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1);
            if (weekStartIndex == 0) {
                i = getDayOfWeek() - 1;
            } else if (weekStartIndex == 1) {
                i = getDayOfWeek() - 2;
            } else if (weekStartIndex != 6) {
                return -1;
            } else {
                i = getDayOfWeek();
            }
            return i;
        } else if (i2 == 2) {
            return getDayOfMonth();
        } else {
            if (i2 != 3) {
                return -1;
            }
            return getMonthOfYear() - 1;
        }
    }

    public static String getWeekStr(boolean[] zArr, String[] strArr) {
        String str = "";
        for (int i = 0; i < zArr.length; i++) {
            if (zArr[i]) {
                if (str.equals("")) {
                    str = strArr[i];
                } else {
                    str = str + "," + strArr[i];
                }
            }
        }
        return (str.equals("日,一,二,三,四,五,六") || str.equals("一,二,三,四,五,六,日") || str.equals("六,日,一,二,三,四,五")) ? "每天" : str;
    }

    public static boolean[] getSelectWeeksByWeekStartIndex() {
        boolean[] zArr = new boolean[7];
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = true;
        }
        return zArr;
    }

    public static boolean[] getSelectWeeksByWeekStartIndex(int i) {
        boolean[] zArr = new boolean[7];
        for (int i2 = 0; i2 < zArr.length; i2++) {
            if (i != 0) {
                if (i != 1) {
                    if (i == 2) {
                        if (i2 == zArr.length - 2 || i2 == zArr.length - 1) {
                            zArr[i2] = false;
                        } else {
                            zArr[i2] = true;
                        }
                    }
                } else if (i2 == 0 || i2 == zArr.length - 1) {
                    zArr[i2] = false;
                } else {
                    zArr[i2] = true;
                }
            } else if (i2 == 0 || i2 == 1) {
                zArr[i2] = false;
            } else {
                zArr[i2] = true;
            }
        }
        return zArr;
    }

    public static boolean[] getSelectWeeks(int i, int i2, boolean[] zArr) {
        if (zArr == null || zArr.length <= 0) {
            return null;
        }
        boolean[] zArr2 = new boolean[zArr.length];
        int i3 = i - i2;
        int i4 = 0;
        if (i3 > 0) {
            while (i4 < zArr2.length) {
                if (i4 < i3) {
                    zArr2[zArr.length - (i3 - i4)] = zArr[i4];
                } else {
                    zArr2[i4 - i3] = zArr[i4];
                }
                i4++;
            }
        } else if (i3 >= 0) {
            return zArr;
        } else {
            while (i4 < zArr2.length) {
                if (i4 < (-i3)) {
                    zArr2[i4] = zArr[zArr.length + i4 + i3];
                } else {
                    zArr2[i4] = zArr[i4 + i3];
                }
                i4++;
            }
        }
        return zArr2;
    }
}
