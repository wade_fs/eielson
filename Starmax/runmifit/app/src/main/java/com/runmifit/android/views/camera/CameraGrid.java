package com.wade.fit.views.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class CameraGrid extends View {
    private Paint mPaint;
    private boolean showGrid;
    private int topBannerWidth;

    public CameraGrid(Context context) {
        this(context, null);
    }

    public CameraGrid(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.topBannerWidth = 0;
        this.showGrid = true;
        init();
    }

    private void init() {
        this.mPaint = new Paint();
        this.mPaint.setColor(-1);
        this.mPaint.setAlpha(120);
        this.mPaint.setStrokeWidth(1.0f);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        if (width < height) {
            this.topBannerWidth = height - width;
        }
        if (this.showGrid) {
            float f = (float) (width / 3);
            float f2 = (float) height;
            canvas.drawLine(f, 0.0f, f, f2, this.mPaint);
            float f3 = (float) ((width * 2) / 3);
            canvas.drawLine(f3, 0.0f, f3, f2, this.mPaint);
            float f4 = (float) (height / 3);
            float f5 = (float) width;
            canvas.drawLine(0.0f, f4, f5, f4, this.mPaint);
            float f6 = (float) ((height * 2) / 3);
            canvas.drawLine(0.0f, f6, f5, f6, this.mPaint);
        }
    }

    public boolean isShowGrid() {
        return this.showGrid;
    }

    public void setShowGrid(boolean z) {
        this.showGrid = z;
    }

    public int getTopWidth() {
        return this.topBannerWidth;
    }
}
