package com.wade.fit.util.image;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.app.Constant;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.util.AsyncTaskUtil;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.views.DialogManager;
import com.wade.fit.views.photo.ClipImageLayout;
import com.tencent.bugly.beta.tinker.TinkerReport;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ImageFactoryActivity extends BaseActivity {
    private Bitmap bitmap;
    View.OnClickListener cropClick = new View.OnClickListener() {
        /* class com.wade.fit.util.image.ImageFactoryActivity.C26922 */

        public void onClick(View view) {
            ImageFactoryActivity imageFactoryActivity = ImageFactoryActivity.this;
            final ProgressDialog showWaitDialog = DialogManager.showWaitDialog(imageFactoryActivity, imageFactoryActivity.getString(R.string.requesting));
            new AsyncTaskUtil().setIAsyncTaskCallBack(new AsyncTaskUtil.IAsyncTaskCallBack() {
                /* class com.wade.fit.util.image.ImageFactoryActivity.C26922.C26931 */

                public Object doInBackground(String... strArr) {
                    Bitmap clip = ImageFactoryActivity.this.cropImageView.clip();
                    BitmapUtil.save(clip, new File(Constant.PIC_PATH + PhotoHelper.photoPath).getAbsolutePath(), Bitmap.CompressFormat.JPEG, 90);
                    return null;
                }

                public void onPostExecute(Object obj) {
                    showWaitDialog.dismiss();
                    ImageFactoryActivity.this.setResult(3, new Intent());
                    ImageFactoryActivity.this.finish();
                }
            }).execute("");
        }
    };
    /* access modifiers changed from: private */
    public ClipImageLayout cropImageView;
    private Handler handler = new Handler();

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_imagefactory;
    }

    public void initEvent() {
    }

    public void initView() {
        this.cropImageView = (ClipImageLayout) findViewById(R.id.cropImageView);
        this.titleName.setText(getResources().getString(R.string.crop));
        this.rightText.setText(getResources().getString(R.string.dialog_sure));
        this.rightText.setOnClickListener(this.cropClick);
        final Uri data = getIntent().getData();
        if (Build.MANUFACTURER.equalsIgnoreCase("xiaomi") || Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
            Bitmap bitmap2 = BitmapBuilder.getBitmap(data.getPath(), 400, 400);
            if (!data.getPath().endsWith(".png")) {
                Bitmap bitmap3 = null;
                try {
                    bitmap3 = getBitmapFormUri(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (bitmap3 != null) {
                    DebugLog.m6203d("ImageFactoryActivity  不是png图片+bitmap2不为空");
                    this.cropImageView.mZoomImageView.setImageBitmap(rotateBitmapByDegree(bitmap3, getBitmapDegree(data.getPath())));
                } else {
                    DebugLog.m6203d("ImageFactoryActivity  不是png图片+bitmap2为空");
                }
            } else if (Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
                if (bitmap2 != null) {
                    this.cropImageView.mZoomImageView.setImageBitmap(rotateBitmapByDegree(bitmap2, getBitmapDegree(data.getPath())));
                }
            } else if (bitmap2 != null) {
                this.cropImageView.mZoomImageView.setImageBitmap(rotateBitmapByDegree(bitmap2, getBitmapDegree(data.getPath())));
            }
        } else {
            try {
                Bitmap bitmapFormUri = getBitmapFormUri(data);
                if (bitmapFormUri != null) {
                    this.cropImageView.mZoomImageView.setImageBitmap(bitmapFormUri);
                } else {
                    Bitmap bitmap4 = BitmapBuilder.getBitmap(data.getPath(), ScreenUtil.getScreenWidth(this), ScreenUtil.getScreenHeight(this));
                    if (bitmap4 != null) {
                        this.cropImageView.mZoomImageView.setImageBitmap(bitmap4);
                    } else {
                        this.handler.postDelayed(new Runnable() {
                            /* class com.wade.fit.util.image.ImageFactoryActivity.C26911 */

                            public void run() {
                                ImageFactoryActivity.this.cropImageView.mZoomImageView.setImageBitmap(ImageFactoryActivity.this.getBitmapFormUri(data));
                            }
                        }, 2000);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        initEvent();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap rotateBitmapByDegree(Bitmap bitmap2, int i) {
        Bitmap bitmap3;
        Matrix matrix = new Matrix();
        matrix.postRotate((float) i);
        try {
            bitmap3 = Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.getWidth(), bitmap2.getHeight(), matrix, true);
        } catch (OutOfMemoryError unused) {
            bitmap3 = null;
        }
        if (bitmap3 == null) {
            bitmap3 = bitmap2;
        }
        if (bitmap2 != bitmap3) {
            bitmap2.recycle();
        }
        return bitmap3;
    }

    private int getBitmapDegree(String str) {
        try {
            int attributeInt = new ExifInterface(str).getAttributeInt("Orientation", 1);
            if (attributeInt == 3) {
                return TinkerReport.KEY_APPLIED_VERSION_CHECK;
            }
            if (attributeInt == 6) {
                return 90;
            }
            if (attributeInt != 8) {
                return 0;
            }
            return 270;
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Bitmap getBitmapFormUri(Uri uri) {
        try {
            InputStream openInputStream = getContentResolver().openInputStream(uri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inDither = true;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            BitmapFactory.decodeStream(openInputStream, null, options);
            openInputStream.close();
            int i = options.outWidth;
            int i2 = options.outHeight;
            DebugLog.m6209i("getBitmapFormUri()............22");
            if (i != -1) {
                if (i2 != -1) {
                    float screenHeight = (float) ScreenUtil.getScreenHeight(this);
                    float screenWidth = (float) ScreenUtil.getScreenWidth(this);
                    if (i >= i2) {
                        float f = (float) i;
                        if (f > screenWidth) {
                            int i3 = (int) (f / screenWidth);
                            BitmapFactory.Options options2 = new BitmapFactory.Options();
                            options2.inSampleSize = 4;
                            options2.inDither = true;
                            options2.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            InputStream openInputStream2 = getContentResolver().openInputStream(uri);
                            Bitmap decodeStream = BitmapFactory.decodeStream(openInputStream2, null, options2);
                            openInputStream2.close();
                            DebugLog.m6209i("getBitmapFormUri():" + decodeStream);
                            return compressImage(decodeStream);
                        }
                    }
                    if (i < i2) {
                        float f2 = (float) i2;
                        if (f2 > screenHeight) {
                            int i4 = (int) (f2 / screenHeight);
                            BitmapFactory.Options options22 = new BitmapFactory.Options();
                            options22.inSampleSize = 4;
                            options22.inDither = true;
                            options22.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            InputStream openInputStream22 = getContentResolver().openInputStream(uri);
                            Bitmap decodeStream2 = BitmapFactory.decodeStream(openInputStream22, null, options22);
                            openInputStream22.close();
                            DebugLog.m6209i("getBitmapFormUri():" + decodeStream2);
                            return compressImage(decodeStream2);
                        }
                    }
                    BitmapFactory.Options options222 = new BitmapFactory.Options();
                    options222.inSampleSize = 4;
                    options222.inDither = true;
                    options222.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    InputStream openInputStream222 = getContentResolver().openInputStream(uri);
                    Bitmap decodeStream22 = BitmapFactory.decodeStream(openInputStream222, null, options222);
                    openInputStream222.close();
                    DebugLog.m6209i("getBitmapFormUri():" + decodeStream22);
                    return compressImage(decodeStream22);
                }
            }
            DebugLog.m6209i("getBitmapFormUri()............nullll");
            return null;
        } catch (Exception e) {
            DebugLog.m6209i("Exception():" + this.bitmap);
            e.printStackTrace();
            return compressImage(this.bitmap);
        }
    }

    public Bitmap compressImage(Bitmap bitmap2) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        int i = 100;
        while (byteArrayOutputStream.toByteArray().length / 1024 > 100) {
            byteArrayOutputStream.reset();
            bitmap2.compress(Bitmap.CompressFormat.JPEG, i, byteArrayOutputStream);
            i -= 10;
        }
        return BitmapFactory.decodeStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()), null, null);
    }
}
