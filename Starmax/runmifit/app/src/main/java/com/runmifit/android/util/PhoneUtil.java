package com.wade.fit.util;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import com.android.internal.telephony.ITelephony;
import com.wade.fit.app.AppApplication;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;
import java.lang.reflect.Method;

public class PhoneUtil {
    public static String TAG = PhoneUtil.class.getSimpleName();

    public static void endCall(Context context) {
        TelecomManager telecomManager = Build.VERSION.SDK_INT >= 21 ? (TelecomManager) context.getSystemService("telecom") : null;
        if (telecomManager == null) {
            try {
                ITelephony.Stub.asInterface((IBinder) Class.forName("android.os.ServiceManager").getMethod("getService", String.class).invoke(null, "phone")).endCall();
            } catch (Exception unused) {
            }
        } else if (ActivityCompat.checkSelfPermission(context, "android.permission.ANSWER_PHONE_CALLS") == 0) {
            if (Build.VERSION.SDK_INT >= 28) {
                telecomManager.endCall();
                return;
            }
            try {
                ITelephony.Stub.asInterface((IBinder) Class.forName("android.os.ServiceManager").getMethod("getService", String.class).invoke(null, "phone")).endCall();
            } catch (Exception e) {
                LogUtil.d(e.toString());
            }
        }
    }

    private static Object getTelephonyObject(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            Method declaredMethod = telephonyManager.getClass().getDeclaredMethod("getITelephony", new Class[0]);
            declaredMethod.setAccessible(true);
            return declaredMethod.invoke(telephonyManager, new Object[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void answerRingingCallWithReflect(Context context) {
        try {
            Object telephonyObject = getTelephonyObject(context);
            if (telephonyObject != null) {
                Method method = telephonyObject.getClass().getMethod("answerRingingCall", new Class[0]);
                method.setAccessible(true);
                method.invoke(telephonyObject, new Object[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void answerRingingCallWithBroadcast(Context context) {
        boolean isWiredHeadsetOn = ((AudioManager) context.getSystemService("audio")).isWiredHeadsetOn();
        if (!isWiredHeadsetOn) {
            DebugLog.m6203d("伪造一个有线耳机插入------1=" + isWiredHeadsetOn);
            Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
            intent.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(0, 79));
            AppApplication.getInstance().sendOrderedBroadcast(intent, "android.permission.CALL_PRIVILEGED");
            Intent intent2 = new Intent("android.intent.action.MEDIA_BUTTON");
            intent2.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(1, 79));
            AppApplication.getInstance().sendOrderedBroadcast(intent2, "android.permission.CALL_PRIVILEGED");
            return;
        }
        DebugLog.m6203d("伪造一个有线耳机插入-----2=" + isWiredHeadsetOn);
        Intent intent3 = new Intent("android.intent.action.MEDIA_BUTTON");
        intent3.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(1, 79));
        context.sendOrderedBroadcast(intent3, null);
    }

    public static void answerRingingCall(Context context) {
        answerRingingCallWithReflect(context);
    }

    public static void callPhone(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + str));
                if (ActivityCompat.checkSelfPermission(context, "android.permission.CALL_PHONE") == 0) {
                    context.startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void dialPhone(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
