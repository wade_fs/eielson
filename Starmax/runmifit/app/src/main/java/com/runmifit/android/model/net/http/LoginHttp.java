package com.wade.fit.model.net.http;

import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.ApiManager;
import okhttp3.RequestBody;

public class LoginHttp {
    public static void loginByEmail(RequestBody requestBody, ApiCallback<BaseBean<UserBean>> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().loginByEmail(requestBody)).subscribe(apiCallback);
    }

    public static void loginByWechat(RequestBody requestBody, ApiCallback<BaseBean<UserBean>> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().loginByWechat(requestBody)).subscribe(apiCallback);
    }

    public static void loginByFacebook(RequestBody requestBody, ApiCallback<BaseBean<UserBean>> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().loginByFacebook(requestBody)).subscribe(apiCallback);
    }

    public static void sendCode(String str, ApiCallback<BaseBean> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().sendCode(str)).subscribe(apiCallback);
    }

    public static void registerAccount(RequestBody requestBody, ApiCallback<BaseBean<UserBean>> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().registerAccount(requestBody)).subscribe(apiCallback);
    }

    public static void sendEmailVerifyCode(String str, ApiCallback<BaseBean<String>> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().sendEmailVerifyCode(str)).subscribe(apiCallback);
    }

    public static void updatePassword(RequestBody requestBody, ApiCallback<BaseBean<String>> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().updatePassword(requestBody)).subscribe(apiCallback);
    }

    public static void loginOut(ApiCallback<BaseBean<String>> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().loginOut()).subscribe(apiCallback);
    }

    public static void clearUserData(ApiCallback<BaseBean<String>> apiCallback) {
        ApiManager.addObservable(ApiManager.getLoginApi().clearUserData()).subscribe(apiCallback);
    }
}
