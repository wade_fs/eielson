package com.wade.fit.util.ble;

import android.content.Context;
import android.os.Build;
import android.text.format.DateFormat;
import com.baidu.mobstat.Config;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.BleContant;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.AlarmBean;
import com.wade.fit.model.bean.AppExchangeDataStartPara;
import com.wade.fit.model.bean.AppNotice;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.DeviceState;
import com.wade.fit.model.bean.Goal;
import com.wade.fit.model.bean.LongSitBean;
import com.wade.fit.model.bean.SleepTime;
import com.wade.fit.model.bean.SleepTimeBean;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.model.bean.WeatherBean;
import com.wade.fit.util.ObjectUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleScanTool;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class BleSdkWrapper implements Constants, BleContant {
    public static final UUID RX_SERVICE_UUID = UUID.fromString("00000af0-0000-1000-8000-00805f9b34fb");
    private static Context mContext;

    public static void appSwitchDataEnd(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
    }

    public static void appSwitchDataStart(AppExchangeDataStartPara appExchangeDataStartPara) {
    }

    public static void appSwitchRestore(int i, int i2, int i3, int i4) {
    }

    public static void bind() {
    }

    public static void endMeasureBloodPressed() {
    }

    public static String getBindMac() {
        return null;
    }

    public static void getBloodPressureData() {
    }

    public static void queryBloodPressureAdjustResult() {
    }

    public static void setBloodPressureAdjustPara(int i, int i2) {
    }

    public static void startMeasureBloodPressure() {
    }

    public static boolean isDistUnitKm() {
        if (AppApplication.getInstance().getUserBean() == null || AppApplication.getInstance().getUserBean().getUnit() == 0) {
            return true;
        }
        return false;
    }

    public static int getDistanceUnit() {
        return SPHelper.getDeviceState().unit;
    }

    public static boolean isConnected() {
        if (!BleScanTool.getInstance().isBluetoothOpen()) {
            return false;
        }
        return BleManager.getInstance().isConnBluetoothSuccess();
    }

    public static void disConnect() {
        BleManager.getInstance().disconnectBluethoothConnection();
    }

    public static void init(Context context) {
        mContext = context;
        BleScanTool.getInstance().init(context);
        BleManager.getInstance().init(context);
    }

    public static void getDeviceInfo(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_DEVICE, bleCallback);
        }
    }

    public static void getDeviceInfo(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_DEVICE, bleCallback);
        }
    }

    public static void setHeartTest(boolean z, BleCallback bleCallback, boolean z2) {
        if (isCanSend(bleCallback, z2)) {
            BleManager.getInstance().enqueue(CmdHelper.setHeartTest(z ? 1 : 0, 30), bleCallback);
        }
    }

    public static void setEcgHeartTest(boolean z, BleCallback bleCallback, boolean z2) {
        if (isCanSend(bleCallback, z2)) {
            BleManager.getInstance().enqueue(CmdHelper.setEcg(z ? 1 : 2), bleCallback);
        }
    }

    public static void setHeartTest(boolean z, BleCallback bleCallback) {
        setHeartTest(z, bleCallback, true);
    }

    public static void getCurrentStep(BleCallback bleCallback) {
        BleManager.getInstance().enqueue(CmdHelper.CMD_GET_CURRENT_STEP, bleCallback);
    }

    public static void getActivity(BleCallback bleCallback) {
        BleManager.getInstance().enqueue(200, CmdHelper.CMD_GET_ACTIVITY, bleCallback);
    }

    public static void getHeartRate(BleCallback bleCallback) {
        BleManager.getInstance().enqueue(CmdHelper.getHeartRate(0), bleCallback);
    }

    public static void connect(BLEDevice bLEDevice) {
        BleManager.getInstance().connect(bLEDevice);
    }

    public static void setBleListener(AppBleListener appBleListener) {
        BleManager.getInstance().setBleListener(appBleListener);
    }

    public static void removeListener(AppBleListener appBleListener) {
        BleManager.getInstance().removeListener(appBleListener);
    }

    public static void stopScanDevices() {
        BleScanTool.getInstance().scanLeDevice(false, 1000);
    }

    public static void removeScanDeviceListener(BleScanTool.ScanDeviceListener scanDeviceListener) {
        BleScanTool.getInstance().removeScanDeviceListener(scanDeviceListener);
    }

    public static void startScanDevices(BleScanTool.ScanDeviceListener scanDeviceListener) {
        BleScanTool.getInstance().addScanDeviceListener(scanDeviceListener);
        BleScanTool.getInstance().scanLeDeviceByService(true, BleScanTool.RX_SERVICE_UUID, 8000);
    }

    private static boolean isExtendScan() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("xiaomi");
        arrayList.add("meizu");
        return arrayList.contains(getPhoneManufacturer());
    }

    public static String getPhoneManufacturer() {
        return Build.MANUFACTURER.toLowerCase();
    }

    public static void getUserInfo(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_USERINFO, bleCallback);
        }
    }

    public static void setUserInfo(UserBean userBean, BleCallback bleCallback) {
        setUserInfo(userBean, bleCallback, true);
    }

    public static void setUserInfo(UserBean userBean, BleCallback bleCallback, boolean z) {
        if (isCanSend(userBean, bleCallback, z)) {
            LogUtil.dAndSave("setUserInfo:" + userBean.toString(), SEND_BLD_DATA_PATH);
            BleManager.getInstance().enqueue(CmdHelper.getUserInfo(userBean.getGender(), userBean.getAge(), userBean.getHeight(), userBean.getWeight(), userBean.getStepDistance()), bleCallback);
        }
    }

    private static boolean isCanSend(Object obj, BleCallback bleCallback, boolean z) {
        if (obj != null) {
            return isCanSend(bleCallback, z);
        }
        if (bleCallback != null) {
            bleCallback.complete(-2, null);
        }
        BleClient.showMessage("数据无效...不发送数据");
        return false;
    }

    private static boolean isCanSend(Object obj, BleCallback bleCallback) {
        return isCanSend(obj, bleCallback, true);
    }

    private static boolean isCanSend(BleCallback bleCallback, boolean z) {
        if (!isConnected() && bleCallback != null) {
            bleCallback.complete(-4, null);
            BleClient.showMessage("未连接....不发送数据");
            return false;
        } else if (!z || !BleClient.getInstance().isSynching() || bleCallback == null) {
            return true;
        } else {
            bleCallback.complete(-3, null);
            BleClient.showMessage("同步数据中....不发送数据");
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.ble.BleSdkWrapper.isCanSend(com.wade.fit.util.ble.BleCallback, boolean):boolean
     arg types: [com.wade.fit.util.ble.BleCallback, int]
     candidates:
      com.wade.fit.util.ble.BleSdkWrapper.isCanSend(java.lang.Object, com.wade.fit.util.ble.BleCallback):boolean
      com.wade.fit.util.ble.BleSdkWrapper.isCanSend(com.wade.fit.util.ble.BleCallback, boolean):boolean */
    private static boolean isCanSend(BleCallback bleCallback) {
        return isCanSend(bleCallback, true);
    }

    public static void setTime(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            BleManager.getInstance().enqueue(CmdHelper.getTime(), bleCallback);
        }
    }

    public static void write(byte[] bArr, BleCallback bleCallback) {
        BleManager.getInstance().enqueue(bArr, bleCallback);
    }

    public static void write(int i, byte[] bArr, BleCallback bleCallback) {
        BleManager.getInstance().enqueue(i, bArr, bleCallback);
    }

    public static void getPower(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_CURRENT_POWER, bleCallback);
        }
    }

    public static void getPower(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_CURRENT_POWER, bleCallback);
        }
    }

    public static void getDeviceState(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_DEVICE_STATE, bleCallback);
        }
    }

    public static void getDeviceState(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_DEVICE_STATE, bleCallback);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static void setDeviceState(BleCallback bleCallback) {
        Locale locale;
        char c;
        DeviceState deviceState = SPHelper.getDeviceState();
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        deviceState.timeFormat = DateFormat.is24HourFormat(AppApplication.getInstance()) ^ true ? 1 : 0;
        if (Build.VERSION.SDK_INT >= 24) {
            locale = mContext.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = mContext.getResources().getConfiguration().locale;
        }
        String language = locale.getLanguage();
        switch (language.hashCode()) {
            case 3139:
                if (language.equals("be")) {
                    c = 3;
                    break;
                }
                c = 65535;
                break;
            case 3166:
                if (language.equals("ca")) {
                    c = 6;
                    break;
                }
                c = 65535;
                break;
            case 3201:
                if (language.equals("de")) {
                    c = 9;
                    break;
                }
                c = 65535;
                break;
            case 3241:
                if (language.equals("en")) {
                    c = 0;
                    break;
                }
                c = 65535;
                break;
            case 3246:
                if (language.equals("es")) {
                    c = 7;
                    break;
                }
                c = 65535;
                break;
            case 3276:
                if (language.equals("fr")) {
                    c = 5;
                    break;
                }
                c = 65535;
                break;
            case 3371:
                if (language.equals("it")) {
                    c = 12;
                    break;
                }
                c = 65535;
                break;
            case 3383:
                if (language.equals("ja")) {
                    c = 10;
                    break;
                }
                c = 65535;
                break;
            case 3580:
                if (language.equals(Config.PROCESS_LABEL)) {
                    c = 11;
                    break;
                }
                c = 65535;
                break;
            case 3588:
                if (language.equals(Config.PLATFORM_TYPE)) {
                    c = 8;
                    break;
                }
                c = 65535;
                break;
            case 3645:
                if (language.equals("ro")) {
                    c = 13;
                    break;
                }
                c = 65535;
                break;
            case 3651:
                if (language.equals("ru")) {
                    c = 2;
                    break;
                }
                c = 65535;
                break;
            case 3734:
                if (language.equals("uk")) {
                    c = 4;
                    break;
                }
                c = 65535;
                break;
            case 3886:
                if (language.equals("zh")) {
                    c = 1;
                    break;
                }
                c = 65535;
                break;
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                deviceState.language = 0;
                break;
            case 1:
                deviceState.language = 1;
                break;
            case 2:
            case 3:
                deviceState.language = 2;
                break;
            case 4:
                deviceState.language = 3;
                break;
            case 5:
                deviceState.language = 4;
                break;
            case 6:
            case 7:
                deviceState.language = 5;
                break;
            case 8:
                deviceState.language = 6;
                break;
            case 9:
                deviceState.language = 7;
                break;
            case 10:
                deviceState.language = 8;
                break;
            case 11:
                deviceState.language = 9;
                break;
            case 12:
                deviceState.language = 10;
                break;
            case 13:
                deviceState.language = 11;
                break;
        }
        int i = deviceState.screenLight;
        int i2 = deviceState.screenTime;
        int i3 = deviceState.theme;
        int i4 = deviceState.language;
        int i5 = deviceState.unit;
        int i6 = deviceState.timeFormat;
        int i7 = deviceState.upHander;
        boolean z = deviceConfig.isDisturbMode;
        LogUtil.d("语言设置：" + i4);
        SPHelper.saveDeviceState(deviceState);
        write(CmdHelper.setDeviceState(i, i2, i3, i4, i5, i6, i7, z ? 1 : 0), bleCallback);
    }

    public static void setDeviceData(BleCallback bleCallback) {
        BleManager.getInstance().enqueue(CmdHelper.setDeviceData(), bleCallback);
    }

    public static void getStepOrSleepHistory(int i, int i2, int i3, int i4, BleCallback bleCallback) {
        BleManager.getInstance().enqueue(200, CmdHelper.getHistoryData(i, i2, i3, i4), bleCallback);
    }

    public static void setLongSit(LongSitBean longSitBean, BleCallback bleCallback) {
        if (isCanSend(longSitBean, bleCallback)) {
            LogUtil.dAndSave("setLongSit:" + longSitBean.toString(), SEND_BLD_DATA_PATH);
            write(CmdHelper.setLongSit(longSitBean), bleCallback);
        }
    }

    public static void getLongSit(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_LONGSIT, bleCallback);
        }
    }

    public static void setLongSit(LongSitBean longSitBean, BleCallback bleCallback, boolean z) {
        if (isCanSend(longSitBean, bleCallback, z)) {
            LogUtil.dAndSave("setLongSit:" + longSitBean.toString(), SEND_BLD_DATA_PATH);
            write(CmdHelper.setLongSit(longSitBean), bleCallback);
        }
    }

    public static void setAlarm(List<AlarmBean> list, BleCallback bleCallback) {
        if (isCanSend(list, bleCallback)) {
            LogUtil.dAndSave("setAlarm:" + list.toString(), SEND_BLD_DATA_PATH);
            List<byte[]> alarm2 = CmdHelper.setAlarm2(list);
            if (alarm2.size() > 1) {
                for (int i = 0; i < alarm2.size(); i++) {
                    if (i == 0) {
                        write(1, alarm2.get(i), bleCallback);
                    } else if (i == alarm2.size() - 1) {
                        write(3, alarm2.get(i), bleCallback);
                    } else {
                        write(2, alarm2.get(i), bleCallback);
                    }
                }
                return;
            }
            write(3, alarm2.get(0), bleCallback);
        }
    }

    public static void getAlarmList(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(200, CmdHelper.CMD_GET_ALARM, bleCallback);
        }
    }

    public static void getNotice(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_NOTICE, bleCallback);
        }
    }

    public static void setNotice(AppNotice appNotice, BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.setNotice(appNotice), bleCallback);
        }
    }

    public static void clearHeartData(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.clearHeartData(), bleCallback);
        }
    }

    public static void setWeather(WeatherBean weatherBean, BleCallback bleCallback) {
        if (isCanSend(weatherBean, bleCallback)) {
            LogUtil.dAndSave("setWeather:" + weatherBean.toString(), SEND_BLD_DATA_PATH);
            write(CmdHelper.setWeather(weatherBean), bleCallback);
        }
    }

    public static void setSleepTime(List<SleepTime> list, int i, BleCallback bleCallback) {
        if (isCanSend(list, bleCallback)) {
            write(CmdHelper.setSleepTime(list, i), bleCallback);
        }
    }

    public static void setSleepTime(List<SleepTime> list, int i, BleCallback bleCallback, boolean z) {
        if (isCanSend(list, bleCallback, z)) {
            write(CmdHelper.setSleepTime(list, i), bleCallback);
        }
    }

    public static void setSleepTime(SleepTimeBean sleepTimeBean, BleCallback bleCallback) {
        if (isCanSend(sleepTimeBean, bleCallback)) {
            LogUtil.dAndSave("setSleepTime:" + sleepTimeBean.toString(), SEND_BLD_DATA_PATH);
            write(CmdHelper.setSleepTime(sleepTimeBean.sleepTimes, sleepTimeBean.state), bleCallback);
        }
    }

    public static void setSleepTime(SleepTimeBean sleepTimeBean, BleCallback bleCallback, boolean z) {
        if (isCanSend(sleepTimeBean, bleCallback, z)) {
            LogUtil.dAndSave("setSleepTime:" + sleepTimeBean.toString(), SEND_BLD_DATA_PATH);
            write(CmdHelper.setSleepTime(sleepTimeBean.sleepTimes, sleepTimeBean.state), bleCallback);
        }
    }

    public static void getSleepStatus(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_SLEEP, bleCallback);
        }
    }

    public static void setDeviceState(DeviceState deviceState, BleCallback bleCallback) {
        if (isCanSend(deviceState, bleCallback)) {
            LogUtil.dAndSave("setDeviceState:" + deviceState.toString(), SEND_BLD_DATA_PATH);
            LogUtil.dAndSave("发送设备状态:" + deviceState.toString(), Constants.BUG_PATH);
            write(CmdHelper.setDeviceState(deviceState.screenLight, deviceState.screenTime, deviceState.theme, deviceState.language, deviceState.unit, deviceState.timeFormat, deviceState.upHander, SPHelper.getDeviceConfig().isDisturbMode ? 1 : 0), bleCallback);
        }
    }

    public static void setHeartRange(int i, int i2, int i3, BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            LogUtil.dAndSave("setHeartRange:" + i + ",maxHr:" + i2 + ",minHr:" + i3, SEND_BLD_DATA_PATH);
            write(CmdHelper.setHeartRange(i, i2, i3), bleCallback);
        }
    }

    public static void setDeviceState(DeviceState deviceState, BleCallback bleCallback, boolean z) {
        if (isCanSend(deviceState, bleCallback, z)) {
            LogUtil.dAndSave("setDeviceState:" + deviceState.toString(), SEND_BLD_DATA_PATH);
            LogUtil.dAndSave("发送设备状态:" + deviceState.toString(), Constants.BUG_PATH);
            write(CmdHelper.setDeviceState(deviceState.screenLight, deviceState.screenTime, deviceState.theme, deviceState.language, deviceState.unit, deviceState.timeFormat, deviceState.upHander, SPHelper.getDeviceConfig().isDisturbMode ? 1 : 0), bleCallback);
        }
    }

    public static void getTarget(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_TARGE, bleCallback);
        }
    }

    public static void setTarget(Goal goal, BleCallback bleCallback) {
        if (isCanSend(goal, bleCallback)) {
            LogUtil.dAndSave("setTarget:" + goal.toString(), SEND_BLD_DATA_PATH);
            write(CmdHelper.setTarget(goal.sleepstate, goal.goalSleep, goal.stepstate, goal.goalStep, goal.calstate, goal.goalCal, goal.distancestate, goal.goalDistanceKm), bleCallback);
        }
    }

    public static void setTarget(Goal goal, BleCallback bleCallback, boolean z) {
        if (isCanSend(goal, bleCallback, z)) {
            LogUtil.dAndSave("setTarget:" + goal.toString(), SEND_BLD_DATA_PATH);
            write(CmdHelper.setTarget(goal.sleepstate, goal.goalSleep, goal.stepstate, goal.goalStep, goal.calstate, goal.goalCal, goal.distancestate, goal.goalDistanceKm), bleCallback);
        }
    }

    public static void setMessage(int i, String str, String str2, BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            LogUtil.dAndSave("setMessage:" + i + ",title:" + str + ",message:" + str2, SEND_BLD_DATA_PATH);
            write(1, CmdHelper.setMessageType(i), bleCallback);
            List<byte[]> message2 = CmdHelper.setMessage2(1, str);
            for (int i2 = 0; i2 < message2.size(); i2++) {
                write(2, message2.get(i2), bleCallback);
            }
            List<byte[]> message22 = CmdHelper.setMessage2(2, str2);
            for (int i3 = 0; i3 < message22.size(); i3++) {
                write(2, message22.get(i3), bleCallback);
            }
            write(3, CmdHelper.END_MESSAGE, bleCallback);
        }
    }

    public static void getHistoryData(int i, int i2, int i3, int i4, BleCallback bleCallback) {
        write(CmdHelper.getHistoryData(i, i2, i3, i4), bleCallback);
    }

    public static void getHistoryHeartRateData(int i, int i2, int i3, int i4, BleCallback bleCallback) {
        write(200, CmdHelper.getHistoryHeartRateData(i, i2, i3, i4), bleCallback);
    }

    public static void getHartRong(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_HART_RONG, bleCallback);
        }
    }

    public static void getHeartOpen(BleCallback bleCallback, boolean z) {
        if (isCanSend(bleCallback, z)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_GET_HART_OPEN, bleCallback);
        }
    }

    public static void enterUpdate(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.enterUpdate(), bleCallback);
        }
    }

    public static void enterCamare(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlDeviceCamare(1), bleCallback);
        }
    }

    public static void camare(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlDeviceCamare(2), bleCallback);
        }
    }

    public static void exitCamare(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlDeviceCamare(3), bleCallback);
        }
    }

    public static void musicControl(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlMusic(1), bleCallback);
        }
    }

    public static void preMusic(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlMusic(2), bleCallback);
        }
    }

    public static void nextMusic(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlMusic(3), bleCallback);
        }
    }

    public static void stopMusic(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlMusic(4), bleCallback);
        }
    }

    public static void addVol(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlMusic(5), bleCallback);
        }
    }

    public static void subVol(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlMusic(6), bleCallback);
        }
    }

    public static void findPhone(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.findDevice(), bleCallback);
        }
    }

    public static void callComing(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlDeviceCall(1), bleCallback);
        }
    }

    public static void callDisturb(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlDeviceCall(2), bleCallback);
        }
    }

    public static void sos(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.controlDeviceSos(1), bleCallback);
        }
    }

    public static boolean isInitCon() {
        return BleManager.getInstance().isInitCon;
    }

    public static void clearSportData(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.clearDeviceData(1), bleCallback);
        }
    }

    public static void recoverSet(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.clearDeviceData(2), bleCallback);
        }
    }

    public static void rebootDevice(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            write(CmdHelper.clearDeviceData(3), bleCallback);
        }
    }

    public static boolean isBind() {
        return !ObjectUtil.isEmpty(SPHelper.getDeviceConfig().bleDeviceList);
    }

    public static void unBind() {
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.IS_BIND, false);
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.SYNCH_HISTORY_DATA_KEY, "");
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        deviceConfig.bleDeviceList.clear();
        SPHelper.saveDeviceConfig(deviceConfig);
        disConnect();
    }

    public static void clearActivity(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            BleManager.getInstance().enqueue(CmdHelper.CMD_CLEAR_ACTIVITY, bleCallback);
        }
    }

    public static void bindDevice(BleCallback bleCallback) {
        if (isCanSend(bleCallback)) {
            BleManager.getInstance().enqueue(CmdHelper.getBind(), bleCallback);
        }
    }
}
