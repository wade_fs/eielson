package com.wade.fit.util.log;

import android.util.Log;
import com.baidu.mobstat.Config;
import org.apache.commons.math3.geometry.VectorFormat;
import org.json.JSONArray;
import org.json.JSONObject;

public class DebugLog {
    private static final int JSON_INDENT = 4;
    static String className;
    static int lineNumber;
    static String methodName;

    public static boolean isDebuggable() {
        return true;
    }

    private DebugLog() {
    }

    private static String createLog(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        long currentTimeMillis = System.currentTimeMillis();
        stringBuffer.append("[");
        stringBuffer.append("(");
        stringBuffer.append(className);
        stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
        stringBuffer.append(lineNumber);
        stringBuffer.append(")");
        stringBuffer.append("#");
        stringBuffer.append(methodName);
        stringBuffer.append("]");
        stringBuffer.append(printIfJson(str));
        long currentTimeMillis2 = System.currentTimeMillis();
        Log.d("DebugLog", (currentTimeMillis2 - currentTimeMillis) + "ms");
        return stringBuffer.toString();
    }

    public static String printIfJson(String str) {
        int i;
        StringBuffer stringBuffer = new StringBuffer();
        try {
            if (str.contains(VectorFormat.DEFAULT_PREFIX)) {
                i = str.indexOf(VectorFormat.DEFAULT_PREFIX);
            } else {
                i = str.contains("[") ? str.indexOf(VectorFormat.DEFAULT_PREFIX) : -1;
            }
            if (i != -1) {
                stringBuffer.append(str.substring(0, i));
                str = str.substring(i);
            }
            if (str.startsWith(VectorFormat.DEFAULT_PREFIX)) {
                str = new JSONObject(str).toString(4);
            } else if (str.startsWith("[")) {
                str = new JSONArray(str).toString(4);
            }
        } catch (Exception unused) {
        }
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    private static void getMethodNames(StackTraceElement[] stackTraceElementArr) {
        className = stackTraceElementArr[1].getFileName();
        methodName = stackTraceElementArr[1].getMethodName();
        lineNumber = stackTraceElementArr[1].getLineNumber();
    }

    private static void getMethodNames2(StackTraceElement[] stackTraceElementArr) {
        className = stackTraceElementArr[2].getFileName();
        methodName = stackTraceElementArr[2].getMethodName();
        lineNumber = stackTraceElementArr[2].getLineNumber();
    }

    /* renamed from: e */
    public static void m6208e(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.d(className, createLog(str));
        }
    }

    /* renamed from: i */
    public static void m6209i(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.d(className, createLog(str));
        }
    }

    /* renamed from: d */
    public static void m6203d(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.i(className, createLog(str));
        }
    }

    /* renamed from: d */
    public static void m6204d(String str, String str2) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            String str3 = className;
            Log.i(str3, createLog("tag:" + str + "," + str2));
        }
    }

    /* renamed from: d */
    public static void m6202d(Object obj) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.i(className, createLog(obj == null ? "message is null" : obj.toString()));
        }
    }

    /* renamed from: d2 */
    public static void m6206d2(String str) {
        if (isDebuggable()) {
            getMethodNames2(new Throwable().getStackTrace());
            Log.i(className, createLog(str));
        }
    }

    /* renamed from: d2 */
    public static void m6207d2(String str, String str2) {
        if (isDebuggable()) {
            getMethodNames2(new Throwable().getStackTrace());
            String str3 = className;
            Log.i(str3, createLog(str + "," + str2));
        }
    }

    /* renamed from: d2 */
    public static void m6205d2(Object obj) {
        if (isDebuggable()) {
            getMethodNames2(new Throwable().getStackTrace());
            Log.i(className, createLog(obj == null ? "warm!!!message is null" : obj.toString()));
        }
    }

    /* renamed from: v */
    public static void m6210v(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.v(className, createLog(str));
        }
    }

    /* renamed from: w */
    public static void m6211w(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.w(className, createLog(str));
        }
    }

    public static void wtf(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.wtf(className, createLog(str));
        }
    }
}
