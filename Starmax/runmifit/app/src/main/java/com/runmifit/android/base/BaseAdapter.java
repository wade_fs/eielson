package com.wade.fit.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<T, MVH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter {
    private static final int TYPE_NET_ERROR = 2;
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_NO_DATA = 1;
    private static int currentType;
    protected OnCustomClickListener customClickListener;
    protected LayoutInflater inflater;
    protected Context mContext;
    protected List<T> mList;
    protected OnItemClickListener mOnItemClickListener;
    protected OnRetryClickListener mRetryClickListener;

    public interface OnCustomClickListener {
        void onCustomClick(View view, int i);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int i);
    }

    public interface OnRetryClickListener {
        void onRetryClick(View view);
    }

    /* access modifiers changed from: protected */
    public abstract RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup);

    /* access modifiers changed from: protected */
    public abstract void onNormalBindViewHolder(MVH mvh, T t, int i);

    public class NetErrorViewHolder_ViewBinding implements Unbinder {
        private NetErrorViewHolder target;

        public NetErrorViewHolder_ViewBinding(NetErrorViewHolder netErrorViewHolder, View view) {
            this.target = netErrorViewHolder;
            netErrorViewHolder.llNetError = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.ll_net_error, "field 'llNetError'", LinearLayout.class);
        }

        public void unbind() {
            NetErrorViewHolder netErrorViewHolder = this.target;
            if (netErrorViewHolder != null) {
                this.target = null;
                netErrorViewHolder.llNetError = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public class NoDataViewHolder_ViewBinding implements Unbinder {
        private NoDataViewHolder target;

        public NoDataViewHolder_ViewBinding(NoDataViewHolder noDataViewHolder, View view) {
            this.target = noDataViewHolder;
            noDataViewHolder.moduleBaseIdEmptyImg = (ImageView) Utils.findRequiredViewAsType(view, R.id.module_base_id_empty_img, "field 'moduleBaseIdEmptyImg'", ImageView.class);
            noDataViewHolder.moduleBaseEmptyText = (TextView) Utils.findRequiredViewAsType(view, R.id.module_base_empty_text, "field 'moduleBaseEmptyText'", TextView.class);
            noDataViewHolder.llLoadNoDate = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.ll_load_no_date, "field 'llLoadNoDate'", LinearLayout.class);
        }

        public void unbind() {
            NoDataViewHolder noDataViewHolder = this.target;
            if (noDataViewHolder != null) {
                this.target = null;
                noDataViewHolder.moduleBaseIdEmptyImg = null;
                noDataViewHolder.moduleBaseEmptyText = null;
                noDataViewHolder.llLoadNoDate = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public BaseAdapter(Context context, List<T> list) {
        this.mList = list;
        if (list == null || list.size() <= 0) {
            showNoData();
        } else {
            showNormalData();
        }
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        if (i == 0) {
            return onCreateViewHolder(viewGroup);
        }
        if (i == 1) {
            viewHolder = new NoDataViewHolder(this.inflater.inflate((int) R.layout.refresh_load_no_date, viewGroup, false));
        } else if (i != 2) {
            RecyclerView.ViewHolder onCreateViewHolder = onCreateViewHolder(viewGroup, i);
            if (onCreateViewHolder != null) {
                onCreateViewHolder.itemView.setTag(onCreateViewHolder);
                onCreateViewHolder.itemView.setOnClickListener(new View.OnClickListener(i) {
                    /* class com.wade.fit.base.$$Lambda$BaseAdapter$TeyW4sL3uCfbD4hlnjzYJmUQWko */
                    private final /* synthetic */ int f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void onClick(View view) {
                        BaseAdapter.this.lambda$onCreateViewHolder$0$BaseAdapter(this.f$1, view);
                    }
                });
            }
            return null;
        } else {
            viewHolder = new NetErrorViewHolder(this.inflater.inflate((int) R.layout.refresh_load_net_error, viewGroup, false));
        }
        return viewHolder;
    }

    public /* synthetic */ void lambda$onCreateViewHolder$0$BaseAdapter(int i, View view) {
        this.mOnItemClickListener.onItemClick(view, i);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (!(viewHolder instanceof NoDataViewHolder) && !(viewHolder instanceof NetErrorViewHolder)) {
            onNormalBindViewHolder(viewHolder, this.mList.get(i), i);
        }
    }

    public int getItemCount() {
        if (currentType == 0) {
            return this.mList.size();
        }
        return 1;
    }

    public void setCustomClickListener(OnCustomClickListener onCustomClickListener) {
        this.customClickListener = onCustomClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setRetryClickListener(OnRetryClickListener onRetryClickListener) {
        this.mRetryClickListener = onRetryClickListener;
    }

    public int getItemViewType(int i) {
        return currentType;
    }

    public void addMoreItem(List<T> list) {
        this.mList.addAll(list);
        notifyDataSetChanged();
    }

    public void setList(List<T> list) {
        if (list == null) {
            currentType = 1;
            this.mList = new ArrayList();
        } else {
            currentType = 0;
            this.mList = list;
        }
        notifyDataSetChanged();
    }

    public List<T> getList() {
        return this.mList;
    }

    public void addItem(int i, T t) {
        this.mList.add(i, t);
        notifyItemInserted(i);
        notifyItemRangeChanged(i, this.mList.size() - i);
    }

    public T getItem(int i) {
        return this.mList.get(i);
    }

    public void showNormalData() {
        currentType = 0;
        notifyDataSetChanged();
    }

    public void showNoData() {
        currentType = 1;
        notifyDataSetChanged();
    }

    public void showNetError() {
        currentType = 2;
        notifyDataSetChanged();
    }

    public static class NoDataViewHolder extends BaseViewHolder {
        LinearLayout llLoadNoDate;
        TextView moduleBaseEmptyText;
        ImageView moduleBaseIdEmptyImg;

        public NoDataViewHolder(View view) {
            super(view);
            this.llLoadNoDate.setVisibility(View.VISIBLE);
        }
    }

    public static class NetErrorViewHolder extends BaseViewHolder {
        LinearLayout llNetError;

        public NetErrorViewHolder(View view) {
            super(view);
            this.llNetError.setVisibility(View.VISIBLE);
        }
    }
}
