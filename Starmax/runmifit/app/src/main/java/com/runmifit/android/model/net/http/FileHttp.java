package com.wade.fit.model.net.http;

import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.ImageResult;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.ApiManager;
import okhttp3.RequestBody;

public class FileHttp {
    public static void uploadImage(RequestBody requestBody, ApiCallback<BaseBean<ImageResult>> apiCallback) {
        ApiManager.addObservable(ApiManager.getFileApi().uploadFile(requestBody)).subscribe(apiCallback);
    }
}
