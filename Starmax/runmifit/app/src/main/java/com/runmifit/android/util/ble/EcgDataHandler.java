package com.wade.fit.util.ble;

import com.baidu.mobstat.Config;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.log.LogUtil;
import java.util.Arrays;

public class EcgDataHandler {
    public void receiverHistory(byte[] bArr) {
        handler(bArr);
    }

    public void handler(byte[] bArr) {
        LogUtil.dAndSave("handler:" + ByteDataConvertUtil.bytesToHexString(bArr), Constants.BLE_PATH_ECG);
        if (bArr[0] == -94 && bArr[2] == 0 && bArr[3] == 0 && bArr[4] == 0) {
            BaseDataHandler.getInstance().flag = 85;
            int bytesToInt = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 5, 7));
            byte b = bArr[7];
            byte b2 = bArr[8];
            byte b3 = bArr[9];
            byte b4 = bArr[10];
            byte b5 = bArr[11];
            int bytesToInt2 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 12, 14));
            int bytesToInt3 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 14, 16));
            LogUtil.d(bytesToInt + "-" + ((int) b) + "-" + ((int) b2) + " " + ((int) b3) + Config.TRACE_TODAY_VISIT_SPLIT + ((int) b4) + Config.TRACE_TODAY_VISIT_SPLIT + ((int) b5) + ",time:" + bytesToInt2 + ",hz:" + bytesToInt3);
            LogUtil.dAndSave("心电开始头:" + bytesToInt + "-" + ((int) b) + "-" + ((int) b2) + " " + ((int) b3) + Config.TRACE_TODAY_VISIT_SPLIT + ((int) b4) + Config.TRACE_TODAY_VISIT_SPLIT + ((int) b5) + ",time:" + bytesToInt2 + ",hz:" + bytesToInt3, Constants.BLE_PATH_ECG);
        } else if (bArr[0] == -94 && bArr[1] == 4 && bArr[2] == 0 && bArr[4] == 1) {
            BaseDataHandler.getInstance().flag = 0;
            EventBusHelper.post((int) Constants.MESSAGE_TYPE.EVENT_TYPE_ECG_MEASURNING_END);
        } else {
            EventBusHelper.post(new BaseMessage(Constants.MESSAGE_TYPE.EVENT_TYPE_ECG_MEASURNING_ING, Integer.valueOf(ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 14, 16)))));
        }
    }
}
