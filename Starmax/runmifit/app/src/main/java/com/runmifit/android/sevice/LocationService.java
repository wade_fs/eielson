package com.wade.fit.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.log.LogUtil;

public class LocationService extends Service {
    public static final String LOCATION_CLOCK = "LocationService.LOCATION_CLOCK";
    /* access modifiers changed from: private */
    public long TIME_INTERVAL = 5000;
    AlarmManager alarmManager;
    IntentFilter filter;
    private Intent intent;
    /* access modifiers changed from: private */
    public LocationClient mLocClient;
    private BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver() {
        /* class com.wade.fit.service.LocationService.C24391 */

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(LocationService.LOCATION_CLOCK)) {
                if (Build.VERSION.SDK_INT >= 23) {
                    LocationService.this.alarmManager.setExactAndAllowWhileIdle(2, SystemClock.elapsedRealtime() + LocationService.this.TIME_INTERVAL, LocationService.this.pendingIntent);
                } else if (Build.VERSION.SDK_INT >= 19) {
                    LocationService.this.alarmManager.setExact(2, SystemClock.elapsedRealtime() + LocationService.this.TIME_INTERVAL, LocationService.this.pendingIntent);
                }
                context.startService(new Intent(context, LocationService.class));
            }
        }
    };
    private MyLocationListenner myListener;
    PendingIntent pendingIntent;

    public IBinder onBind(Intent intent2) {
        return new MyBinder();
    }

    public void onCreate() {
        super.onCreate();
        LogUtil.d("onCreate");
        startAlarm();
        this.mLocClient = new LocationClient(this);
        this.myListener = new MyLocationListenner();
        this.mLocClient.registerLocationListener(this.myListener);
        LocationClientOption locationClientOption = new LocationClientOption();
        locationClientOption.setLocationMode(LocationClientOption.LocationMode.Device_Sensors);
        locationClientOption.setOpenGps(true);
        locationClientOption.setCoorType("bd09ll");
        locationClientOption.setScanSpan(1000);
        this.mLocClient.setLocOption(locationClientOption);
        this.mLocClient.start();
    }

    public class MyLocationListenner implements BDLocationListener {
        public MyLocationListenner() {
        }

        public void onReceiveLocation(BDLocation bDLocation) {
            if (bDLocation != null && bDLocation.getLocType() == 61) {
                LogUtil.d("location:" + bDLocation.getLocType() + "," + bDLocation.getLatitude() + "," + bDLocation.getLongitude());
                EventBusHelper.post(new BaseMessage(202, bDLocation));
            }
        }
    }

    public void startAlarm() {
        LogUtil.d("开启定时2");
        this.intent = new Intent(LOCATION_CLOCK);
        this.pendingIntent = PendingIntent.getBroadcast(this, 0, this.intent, 0);
        this.alarmManager = (AlarmManager) getSystemService(NotificationCompat.CATEGORY_ALARM);
        this.filter = new IntentFilter();
        this.filter.addAction(LOCATION_CLOCK);
        contiueAlarm();
        if (Build.VERSION.SDK_INT >= 23) {
            this.alarmManager.setExactAndAllowWhileIdle(2, SystemClock.elapsedRealtime(), this.pendingIntent);
        } else if (Build.VERSION.SDK_INT >= 19) {
            this.alarmManager.setExact(2, SystemClock.elapsedRealtime(), this.pendingIntent);
        } else {
            this.alarmManager.setRepeating(2, SystemClock.elapsedRealtime(), this.TIME_INTERVAL, this.pendingIntent);
        }
    }

    public void contiueAlarm() {
        BroadcastReceiver broadcastReceiver = this.myBroadcastReceiver;
        if (broadcastReceiver != null) {
            registerReceiver(broadcastReceiver, this.filter);
        }
    }

    public class MyBinder extends Binder {
        public void initAlarm() {
        }

        public MyBinder() {
        }

        public void stopAlarm() {
            LocationService.this.mLocClient.stop();
        }

        public void goOnAlarm() {
            LocationService.this.mLocClient.start();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        LogUtil.d("onDestroy");
        AlarmManager alarmManager2 = this.alarmManager;
        if (alarmManager2 != null) {
            alarmManager2.cancel(this.pendingIntent);
        }
        BroadcastReceiver broadcastReceiver = this.myBroadcastReceiver;
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
        LocationClient locationClient = this.mLocClient;
        if (locationClient != null) {
            locationClient.unRegisterLocationListener(this.myListener);
            this.mLocClient.stop();
        }
    }
}
