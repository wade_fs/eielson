package com.wade.fit.util;

import com.tamic.novate.util.FileUtil;
import java.math.BigDecimal;

public class NumUtil {
    public static String float2String(float f, int i) {
        String str = f + "";
        int isNumIndex = getIsNumIndex(str);
        String substring = str.substring(0, isNumIndex);
        String substring2 = str.substring(isNumIndex + 1, str.length());
        int length = substring2.length();
        if (i < 0) {
            i = 0;
        }
        int i2 = length - i;
        System.out.println("firstNum=" + substring + ",secondNum=" + substring2 + ",surplusLen=" + i2);
        if (i2 > 0) {
            substring2 = substring2.substring(0, i);
        } else if (i2 < 0) {
            for (int i3 = 0; i3 < (-i2); i3++) {
                substring2 = substring2 + "0";
            }
        }
        if (i <= 0) {
            return substring;
        }
        return substring + FileUtil.HIDDEN_PREFIX + substring2;
    }

    public static boolean strIsChangeInt(String str) {
        return str.matches("[0-9]+");
    }

    public static String[] getStringArray(String str) {
        StringBuilder sb = new StringBuilder("");
        int i = 0;
        while (true) {
            if (i >= str.length()) {
                i = -1;
                break;
            }
            int i2 = i + 1;
            String substring = str.substring(i, i2);
            if (!strIsChangeInt(substring)) {
                break;
            }
            sb.append(substring);
            i = i2;
        }
        return new String[]{sb.toString(), i + ""};
    }

    public static String format2Point(double d) {
        return String.valueOf(new BigDecimal(d).setScale(2, 4).doubleValue());
    }

    public static double formatPoint(double d, int i) {
        double pow = Math.pow(10.0d, (double) i);
        double d2 = (double) ((int) (d * pow));
        Double.isNaN(d2);
        return d2 / pow;
    }

    public static int getIsNumIndex(String str) {
        int i = 0;
        while (i < str.length()) {
            int i2 = i + 1;
            if (!strIsChangeInt(str.substring(i, i2))) {
                return i;
            }
            i = i2;
        }
        return -1;
    }

    public static float float2float(float f) {
        return new BigDecimal((double) f).setScale(2, 6).floatValue();
    }

    public static Integer isEmptyInt(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }
        return parseInt(str);
    }

    public static Integer parseInt(String str) {
        try {
            return Integer.valueOf(Integer.parseInt(str));
        } catch (Exception unused) {
            return 0;
        }
    }

    public static String isEmptyStr(String str) {
        return (str == null || str.equals("")) ? "" : str;
    }

    public static Float isEmptyFloat(String str) {
        return Float.valueOf(parseFloat(str));
    }

    public static float parseFloat(String str) {
        if (str != null && !str.equals("")) {
            if (str.contains(",")) {
                str = str.replaceAll(",", FileUtil.HIDDEN_PREFIX);
            }
            try {
                return Float.parseFloat(str);
            } catch (Exception unused) {
            }
        }
        return -1.0f;
    }

    public static float valueOf(String str) {
        return parseFloat(str);
    }

    public static int range(int i, int i2, int i3) {
        return Math.max(Math.min(i2, i), i3);
    }
}
