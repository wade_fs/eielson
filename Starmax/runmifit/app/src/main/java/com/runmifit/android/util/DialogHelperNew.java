package com.wade.fit.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.app.Constants;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.views.dialog.CommonDialog;
import com.wade.fit.views.dialog.WheelViewDialog;
import com.wade.fit.views.wheel.ArrayWheelAdapter;
import com.wade.fit.views.wheel.NewWheelView;
import com.wade.fit.views.wheel.NumericWheelAdapter;
import com.wade.fit.views.wheel.OnWheelChangedNewListener;
import com.wade.fit.views.wheel.WheelAdapter;
import com.tencent.bugly.beta.tinker.TinkerReport;
import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class DialogHelperNew {
    private static int END_YEAR = 2090;
    /* access modifiers changed from: private */
    public static int START_YEAR = 1920;
    /* access modifiers changed from: private */
    public static int mCurrentYear;
    private static Dialog waitDialog;

    public interface IOnclickListener {
        void leftButton();

        void rightButton();
    }

    public interface SetInputCallback {
        void sure(int i);
    }

    public static Dialog buildWaitDialog(Context context, boolean z) {
        if (waitDialog == null) {
            waitDialog = new Dialog(context, R.style.theme_dialog);
        }
        waitDialog.setContentView(LayoutInflater.from(context).inflate((int) R.layout.dialog_wait, (ViewGroup) null));
        waitDialog.setCancelable(z);
        waitDialog.show();
        return waitDialog;
    }

    public static void dismissWait() {
        Dialog dialog = waitDialog;
        if (dialog != null) {
            dialog.dismiss();
            waitDialog = null;
        }
    }

    public static Dialog showPhotosDaiglog(Activity activity, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        Dialog dialog = new Dialog(activity, R.style.MyDialog);
        dialog.setContentView((int) R.layout.dialog_take_photo);
        dialog.getWindow().setGravity(80);
        dialog.setCancelable(true);
        dialog.getWindow().getAttributes().width = (int) (((float) activity.getWindowManager().getDefaultDisplay().getWidth()) * 1.0f);
        File file = new File(Constants.PIC_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
        dialog.findViewById(R.id.photo_select_photos).setOnClickListener(onClickListener);
        dialog.findViewById(R.id.photo_take_photos).setOnClickListener(onClickListener2);
        dialog.findViewById(R.id.photo_cancel).setOnClickListener(new DismissListener(dialog));
        return dialog;
    }

    static class DismissListener implements View.OnClickListener {
        Dialog dialog;

        public DismissListener(Dialog dialog2) {
            this.dialog = dialog2;
        }

        public void onClick(View view) {
            Dialog dialog2 = this.dialog;
            if (dialog2 != null) {
                dialog2.dismiss();
            }
        }
    }

    public static Dialog getConfirmDialog(Context context, View view, boolean z) {
        Dialog dialog = new Dialog(context, R.style.center_dialog);
        dialog.setContentView(view);
        dialog.setCancelable(z);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        double d = (double) context.getResources().getDisplayMetrics().widthPixels;
        Double.isNaN(d);
        attributes.width = (int) (d * 0.8d);
        window.setAttributes(attributes);
        return dialog;
    }

    public static boolean checkActivityisDestroyed(Context context) {
        if (context != null && (context instanceof Activity) && !((Activity) context).isDestroyed()) {
            return true;
        }
        return false;
    }

    public static AlertDialog showBindDialog(Context context) {
        return showBindDialog(context, true);
    }

    public static AlertDialog showBindDialog(Context context, boolean z) {
        if (!checkActivityisDestroyed(context)) {
            return null;
        }
        AlertDialog create = new AlertDialog.Builder(context).create();
        create.setCanceledOnTouchOutside(false);
        create.setCancelable(false);
        create.show();
        create.getWindow().setContentView((int) R.layout.dialog_bind);
        create.findViewById(R.id.llContent).setOnClickListener(new View.OnClickListener(create) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$sUpHE2sB4G7EOseWNYAJ6Ymk_aA */
            private final /* synthetic */ AlertDialog f$0;

            {
                this.f$0 = r1;
            }

            public final void onClick(View view) {
                this.f$0.dismiss();
            }
        });
        if (z) {
            create.findViewById(R.id.ivBindBg).startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate_item_scan));
        }
        return create;
    }

    public static void getInputDialog(Context context, int i, String str, String str2, SetInputCallback setInputCallback) {
        Dialog dialog = new Dialog(context, R.style.center_dialog);
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.dialog_input, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.tvTips)).setText(str2);
        ((TextView) inflate.findViewById(R.id.dialogTitle)).setText(str);
        EditText editText = (EditText) inflate.findViewById(R.id.etInput);
        editText.setHint(i + "");
        inflate.findViewById(R.id.tvCanle).setOnClickListener(new View.OnClickListener(dialog) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$QX3td3Lh783eyY4HRZo0hv98Ies */
            private final /* synthetic */ Dialog f$0;

            {
                this.f$0 = r1;
            }

            public final void onClick(View view) {
                this.f$0.dismiss();
            }
        });
        inflate.findViewById(R.id.tvSure).setOnClickListener(new View.OnClickListener(dialog, editText, setInputCallback) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$AOapr1hiQC0Fejm_j6zCxAKURw */
            private final /* synthetic */ Dialog f$0;
            private final /* synthetic */ EditText f$1;
            private final /* synthetic */ DialogHelperNew.SetInputCallback f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                DialogHelperNew.lambda$getInputDialog$2(this.f$0, this.f$1, this.f$2, view);
            }
        });
        dialog.setContentView(inflate);
        dialog.setCancelable(true);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        double d = (double) context.getResources().getDisplayMetrics().widthPixels;
        Double.isNaN(d);
        attributes.width = (int) (d * 0.8d);
        window.setAttributes(attributes);
        dialog.show();
    }

    static /* synthetic */ void lambda$getInputDialog$2(Dialog dialog, EditText editText, SetInputCallback setInputCallback, View view) {
        dialog.dismiss();
        if (!TextUtils.isEmpty(editText.getText().toString())) {
            setInputCallback.sure(Integer.parseInt(editText.getText().toString()));
        }
    }

    public static void showWheelDialog(Context context, int i, int i2, int i3, WheelViewDialog.OnSelectClick onSelectClick) {
        showWheelDialog(context, i, i2, i3, 1, onSelectClick);
    }

    public static void showWheelDialog(Context context, int i, int i2, int i3, int i4, WheelViewDialog.OnSelectClick onSelectClick) {
        WheelViewDialog wheelViewDialog = new WheelViewDialog(context);
        NewWheelView wv_1 = wheelViewDialog.getWv_1();
        wv_1.setVisibility(View.VISIBLE);
        wv_1.setVisibleItems(3);
        wheelViewDialog.getWv_2().setVisibility(View.GONE);
        wheelViewDialog.getWv_3().setVisibility(View.GONE);
        if (i > i3) {
            i = i3;
        }
        if (i < i2) {
            i = i2;
        }
        wv_1.setAdapter(new NumericWheelAdapter(i2, i3, i4));
        wv_1.setCurrentItem(i - i2);
        wv_1.setAddZero(false);
        wheelViewDialog.findViewById(R.id.set).setOnClickListener(new View.OnClickListener(wv_1, i2, i4, wheelViewDialog) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$G4HRBi8RqB7j3rR8HCCAqkqsqZw */
            private final /* synthetic */ NewWheelView f$1;
            private final /* synthetic */ int f$2;
            private final /* synthetic */ int f$3;
            private final /* synthetic */ WheelViewDialog f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void onClick(View view) {
                DialogHelperNew.lambda$showWheelDialog$3(WheelViewDialog.OnSelectClick.this, this.f$1, this.f$2, this.f$3, this.f$4, view);
            }
        });
        wheelViewDialog.show();
    }

    static /* synthetic */ void lambda$showWheelDialog$3(WheelViewDialog.OnSelectClick onSelectClick, NewWheelView newWheelView, int i, int i2, WheelViewDialog wheelViewDialog, View view) {
        if (onSelectClick != null) {
            onSelectClick.onSelect((newWheelView.getCurrentItem() + i) * i2, 0, 0);
        }
        wheelViewDialog.dismiss();
    }

    public static void showWheelSportTargetDialog(Context context, int[] iArr, WheelViewDialog.OnSelectClick onSelectClick) {
        AppCompatDialog appCompatDialog = new AppCompatDialog(context, R.style.MyDialog);
        appCompatDialog.setContentView((int) R.layout.dialog_wheelviews_title);
        appCompatDialog.getWindow().setGravity(80);
        appCompatDialog.setCancelable(true);
        appCompatDialog.getWindow().getAttributes().width = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
        NewWheelView newWheelView = (NewWheelView) appCompatDialog.findViewById(R.id.wv_1);
        NewWheelView newWheelView2 = (NewWheelView) appCompatDialog.findViewById(R.id.wv_2);
        newWheelView.setVisibility(View.VISIBLE);
        newWheelView2.setVisibility(View.VISIBLE);
        ((NewWheelView) appCompatDialog.findViewById(R.id.wv_3)).setVisibility(View.GONE);
        initSportTargetPicker(context, iArr, newWheelView, newWheelView2);
        appCompatDialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$a1B0ZOdjD0JEpzmkFexWstUr2GA */

            public final void onClick(View view) {
                AppCompatDialog.this.dismiss();
            }
        });
        appCompatDialog.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener(newWheelView, newWheelView2, appCompatDialog) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$hxMVBzRWoUdn6UbEskSxWj0dZj4 */
            private final /* synthetic */ NewWheelView f$1;
            private final /* synthetic */ NewWheelView f$2;
            private final /* synthetic */ AppCompatDialog f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void onClick(View view) {
                DialogHelperNew.lambda$showWheelSportTargetDialog$5(WheelViewDialog.OnSelectClick.this, this.f$1, this.f$2, this.f$3, view);
            }
        });
        appCompatDialog.show();
    }

    static /* synthetic */ void lambda$showWheelSportTargetDialog$5(WheelViewDialog.OnSelectClick onSelectClick, NewWheelView newWheelView, NewWheelView newWheelView2, AppCompatDialog appCompatDialog, View view) {
        if (onSelectClick != null) {
            onSelectClick.onSelect(newWheelView.getCurrentItem(), newWheelView2.getCurrentItem(), 0);
        }
        appCompatDialog.dismiss();
    }

    private static void initSportTargetPicker(Context context, int[] iArr, NewWheelView newWheelView, NewWheelView newWheelView2) {
        newWheelView.setAdapter(new ArrayWheelAdapter(context.getResources().getStringArray(R.array.target_type), 25));
        newWheelView.setCyclic(true);
        newWheelView.setCurrentItem(iArr[0]);
        newWheelView.setVisibleItems(3);
        List<String> sportTimeList = CommonUtil.getSportTimeList();
        List<String> sportDistanceList = CommonUtil.getSportDistanceList();
        List<String> promptCalorieList = CommonUtil.getPromptCalorieList();
        newWheelView2.setAdapter(new ArrayWheelAdapter(iArr[0] == 0 ? sportTimeList.toArray() : 1 == iArr[0] ? sportDistanceList.toArray() : promptCalorieList.toArray(), 100));
        newWheelView2.setCyclic(true);
        newWheelView2.setCurrentItem(iArr[1]);
        newWheelView2.setVisibleItems(3);
        newWheelView.addChangingListener(new OnWheelChangedNewListener(sportTimeList, sportDistanceList, promptCalorieList, newWheelView2) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$guiLKNT2v3J7mKo0d2diAcgEL4 */
            private final /* synthetic */ List f$0;
            private final /* synthetic */ List f$1;
            private final /* synthetic */ List f$2;
            private final /* synthetic */ NewWheelView f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            public final void onChanged(NewWheelView newWheelView, int i, int i2) {
                DialogHelperNew.lambda$initSportTargetPicker$6(this.f$0, this.f$1, this.f$2, this.f$3, newWheelView, i, i2);
            }
        });
    }

    static /* synthetic */ void lambda$initSportTargetPicker$6(List list, List list2, List list3, NewWheelView newWheelView, NewWheelView newWheelView2, int i, int i2) {
        newWheelView.setAdapter(new ArrayWheelAdapter(i2 == 0 ? list.toArray() : 1 == i2 ? list2.toArray() : list3.toArray(), 100));
    }

    public static Dialog showWeakGPSTipDialog(Activity activity, IOnclickListener iOnclickListener) {
        CommonDialog create = new CommonDialog.Builder(activity).setCancelable(false).setMessage((int) R.string.gps__weak_tips).setTitle((int) R.string.gps__weak_title).setLeftTextColor(R.color.sport_dialog_btn).setRightTextColor(R.color.sport_dialog_btn).setLeftButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$yN4n2bVH0jRgpncUsQ2Swv3GPE */

            public final void onClick(DialogInterface dialogInterface, int i) {
                DialogHelperNew.lambda$showWeakGPSTipDialog$7(DialogHelperNew.IOnclickListener.this, dialogInterface, i);
            }
        }).setRightButton((int) R.string.continues, new DialogInterface.OnClickListener() {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$oKicvNFW3Z8PrzxRgCT5OtbPQ */

            public final void onClick(DialogInterface dialogInterface, int i) {
                DialogHelperNew.lambda$showWeakGPSTipDialog$8(DialogHelperNew.IOnclickListener.this, dialogInterface, i);
            }
        }).create();
        create.show();
        return create;
    }

    static /* synthetic */ void lambda$showWeakGPSTipDialog$7(IOnclickListener iOnclickListener, DialogInterface dialogInterface, int i) {
        if (iOnclickListener != null) {
            iOnclickListener.leftButton();
        }
    }

    static /* synthetic */ void lambda$showWeakGPSTipDialog$8(IOnclickListener iOnclickListener, DialogInterface dialogInterface, int i) {
        if (iOnclickListener != null) {
            iOnclickListener.rightButton();
        }
    }

    public static Dialog showSportLowTipDialog(Activity activity, IOnclickListener iOnclickListener) {
        CommonDialog create = new CommonDialog.Builder(activity).setCancelable(false).setMessage((int) R.string.not_save_data).setLeftTextColor(R.color.sport_dialog_btn).setRightTextColor(R.color.sport_dialog_btn).setLeftButton((int) R.string.remind_sport_end, new DialogInterface.OnClickListener() {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$vMFKY63Ey_yecAMObVUdqmhYqY */

            public final void onClick(DialogInterface dialogInterface, int i) {
                DialogHelperNew.lambda$showSportLowTipDialog$9(DialogHelperNew.IOnclickListener.this, dialogInterface, i);
            }
        }).setRightButton((int) R.string.remind_sport_again, new DialogInterface.OnClickListener() {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$RB9E3vvJqf5H_FpGRWcPP8e9Wkw */

            public final void onClick(DialogInterface dialogInterface, int i) {
                DialogHelperNew.lambda$showSportLowTipDialog$10(DialogHelperNew.IOnclickListener.this, dialogInterface, i);
            }
        }).create();
        create.show();
        return create;
    }

    static /* synthetic */ void lambda$showSportLowTipDialog$9(IOnclickListener iOnclickListener, DialogInterface dialogInterface, int i) {
        if (iOnclickListener != null) {
            iOnclickListener.leftButton();
        }
    }

    static /* synthetic */ void lambda$showSportLowTipDialog$10(IOnclickListener iOnclickListener, DialogInterface dialogInterface, int i) {
        if (iOnclickListener != null) {
            iOnclickListener.rightButton();
        }
    }

    public static Dialog showDeleteDataTipDialog(Activity activity, IOnclickListener iOnclickListener) {
        CommonDialog create = new CommonDialog.Builder(activity).setCancelable(false).setMessage((int) R.string.delete_all_tip).setLeftTextColor(R.color.sport_dialog_btn).setRightTextColor(R.color.sport_dialog_btn).setLeftButton((int) R.string.no, new DialogInterface.OnClickListener() {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$rGSTfCB_YjzPOPOwq6mrTCMVvnE */

            public final void onClick(DialogInterface dialogInterface, int i) {
                DialogHelperNew.lambda$showDeleteDataTipDialog$11(DialogHelperNew.IOnclickListener.this, dialogInterface, i);
            }
        }).setRightButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$i0n18cGC8jzXjQI48fxYBFsXWUw */

            public final void onClick(DialogInterface dialogInterface, int i) {
                DialogHelperNew.lambda$showDeleteDataTipDialog$12(DialogHelperNew.IOnclickListener.this, dialogInterface, i);
            }
        }).create();
        create.show();
        return create;
    }

    static /* synthetic */ void lambda$showDeleteDataTipDialog$11(IOnclickListener iOnclickListener, DialogInterface dialogInterface, int i) {
        if (iOnclickListener != null) {
            iOnclickListener.leftButton();
        }
    }

    static /* synthetic */ void lambda$showDeleteDataTipDialog$12(IOnclickListener iOnclickListener, DialogInterface dialogInterface, int i) {
        if (iOnclickListener != null) {
            iOnclickListener.rightButton();
        }
    }

    public static void showWheelSexDialog(Context context, int i, final WheelViewDialog.OnSelectClick onSelectClick) {
        final WheelViewDialog wheelViewDialog = new WheelViewDialog(context);
        final NewWheelView wv_1 = wheelViewDialog.getWv_1();
        wv_1.setAddZero(false);
        wv_1.setVisibility(View.VISIBLE);
        wv_1.setVisibleItems(3);
        wv_1.setCyclic(false);
        wheelViewDialog.getWv_2().setVisibility(View.GONE);
        wheelViewDialog.getWv_3().setVisibility(View.GONE);
        wv_1.setAdapter(new ArrayWheelAdapter(context.getResources().getStringArray(R.array.unit_sex), 25));
        wv_1.setCurrentItem(i);
        wheelViewDialog.findViewById(R.id.set).setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.util.DialogHelperNew.C26391 */

            public void onClick(View view) {
                WheelViewDialog.OnSelectClick onSelectClick = onSelectClick;
                if (onSelectClick != null) {
                    onSelectClick.onSelect(wv_1.getCurrentItem(), 0, 0);
                }
                wheelViewDialog.dismiss();
            }
        });
        wheelViewDialog.show();
    }

    public static void showWheelBirthDayDialog(Context context, int[] iArr, WheelViewDialog.OnSelectClick onSelectClick) {
        WheelViewDialog wheelViewDialog = new WheelViewDialog(context, R.style.MyDialog);
        wheelViewDialog.setContentView((int) R.layout.dialog_wheelviews);
        wheelViewDialog.getWindow().setGravity(80);
        wheelViewDialog.setCancelable(true);
        wheelViewDialog.getWindow().getAttributes().width = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
        NewWheelView newWheelView = (NewWheelView) wheelViewDialog.findViewById(R.id.wv_1);
        NewWheelView newWheelView2 = (NewWheelView) wheelViewDialog.findViewById(R.id.wv_2);
        NewWheelView newWheelView3 = (NewWheelView) wheelViewDialog.findViewById(R.id.wv_3);
        END_YEAR = Calendar.getInstance().get(1);
        START_YEAR = END_YEAR - 100;
        newWheelView.setVisibility(View.VISIBLE);
        newWheelView2.setVisibility(View.VISIBLE);
        newWheelView3.setVisibility(View.VISIBLE);
        initTimePicker(context, iArr, newWheelView, newWheelView2, newWheelView3);
        wheelViewDialog.findViewById(R.id.cancle).setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$BPkj86qlBeNAv8P_TrJrvdDci50 */

            public final void onClick(View view) {
                WheelViewDialog.this.dismiss();
            }
        });
        wheelViewDialog.findViewById(R.id.set).setOnClickListener(new View.OnClickListener(newWheelView, newWheelView2, newWheelView3, wheelViewDialog) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$tsF7FKvLB134ZSnuDnXaI_QdZY */
            private final /* synthetic */ NewWheelView f$1;
            private final /* synthetic */ NewWheelView f$2;
            private final /* synthetic */ NewWheelView f$3;
            private final /* synthetic */ WheelViewDialog f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            public final void onClick(View view) {
                DialogHelperNew.lambda$showWheelBirthDayDialog$14(WheelViewDialog.OnSelectClick.this, this.f$1, this.f$2, this.f$3, this.f$4, view);
            }
        });
        wheelViewDialog.show();
    }

    static /* synthetic */ void lambda$showWheelBirthDayDialog$14(WheelViewDialog.OnSelectClick onSelectClick, NewWheelView newWheelView, NewWheelView newWheelView2, NewWheelView newWheelView3, WheelViewDialog wheelViewDialog, View view) {
        if (onSelectClick != null) {
            onSelectClick.onSelect(newWheelView.getCurrentItem() + START_YEAR, newWheelView2.getCurrentItem() + 1, newWheelView3.getCurrentItem() + 1);
        }
        wheelViewDialog.dismiss();
    }

    private static void initTimePicker(Context context, int[] iArr, NewWheelView newWheelView, NewWheelView newWheelView2, NewWheelView newWheelView3) {
        NewWheelView newWheelView4 = newWheelView;
        NewWheelView newWheelView5 = newWheelView2;
        NewWheelView newWheelView6 = newWheelView3;
        Calendar instance = Calendar.getInstance();
        int i = instance.get(1);
        int i2 = instance.get(2);
        int i3 = instance.get(5);
        String[] strArr = {"1", "3", "5", "7", com.tencent.connect.common.Constants.VIA_SHARE_TYPE_PUBLISHVIDEO, com.tencent.connect.common.Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, com.tencent.connect.common.Constants.VIA_REPORT_TYPE_SET_AVATAR};
        String[] strArr2 = {"4", com.tencent.connect.common.Constants.VIA_SHARE_TYPE_INFO, com.tencent.connect.common.Constants.VIA_SHARE_TYPE_MINI_PROGRAM, com.tencent.connect.common.Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE};
        List asList = Arrays.asList(strArr);
        List asList2 = Arrays.asList(strArr2);
        newWheelView4.setAdapter(new NumericWheelAdapter(START_YEAR, END_YEAR));
        newWheelView4.setCyclic(false);
        newWheelView4.setCurrentItem(iArr[0] - START_YEAR);
        newWheelView4.setVisibleItems(3);
        String[] strArr3 = new String[12];
        int i4 = 0;
        while (i4 < strArr3.length) {
            int i5 = i4 + 1;
            strArr3[i4] = String.format("%02d", Integer.valueOf(i5));
            i4 = i5;
        }
        setYearsMonth(iArr[1] - 1, newWheelView5);
        newWheelView5.setCyclic(false);
        newWheelView5.setCurrentItem(iArr[1] - 1);
        newWheelView5.setVisibleItems(3);
        newWheelView6.setCyclic(false);
        newWheelView6.setVisibleItems(3);
        final String[] strArr4 = new String[31];
        int i6 = 0;
        while (i6 < strArr4.length) {
            int i7 = i6 + 1;
            strArr4[i6] = String.format("%02d", Integer.valueOf(i7));
            i6 = i7;
        }
        String[] strArr5 = new String[30];
        int i8 = 0;
        while (i8 < strArr5.length) {
            int i9 = i8 + 1;
            strArr5[i8] = String.format("%02d", Integer.valueOf(i9));
            i8 = i9;
        }
        String[] strArr6 = new String[29];
        int i10 = 0;
        while (i10 < strArr6.length) {
            int i11 = i10 + 1;
            strArr6[i10] = String.format("%02d", Integer.valueOf(i11));
            i10 = i11;
        }
        String[] strArr7 = new String[28];
        int i12 = 0;
        while (i12 < strArr7.length) {
            int i13 = i12 + 1;
            strArr7[i12] = String.format("%02d", Integer.valueOf(i13));
            i12 = i13;
        }
        int i14 = i2 + 1;
        if (asList.contains(String.valueOf(i14))) {
            newWheelView6.setAdapter(new ArrayWheelAdapter(strArr4, 25));
        } else if (asList2.contains(String.valueOf(i14))) {
            newWheelView6.setAdapter(new ArrayWheelAdapter(strArr5, 25));
        } else if ((i % 4 != 0 || i % 100 == 0) && i % 400 != 0) {
            newWheelView6.setAdapter(new ArrayWheelAdapter(strArr7, 25));
        } else {
            newWheelView6.setAdapter(new ArrayWheelAdapter(strArr6, 25));
        }
        newWheelView6.setCurrentItem(iArr[2] - 1);
        final NewWheelView newWheelView7 = newWheelView2;
        final String[] strArr8 = strArr6;
        final List list = asList;
        String[] strArr9 = strArr5;
        final NewWheelView newWheelView8 = newWheelView3;
        final List list2 = asList2;
        List list3 = asList2;
        final String[] strArr10 = strArr9;
        final String[] strArr11 = strArr7;
        C26402 r9 = new OnWheelChangedNewListener() {
            /* class com.wade.fit.util.DialogHelperNew.C26402 */

            public void onChanged(NewWheelView newWheelView, int i, int i2) {
                int access$000 = i2 + DialogHelperNew.START_YEAR;
                DialogHelperNew.setYearsMonth(access$000, newWheelView7);
                int unused = DialogHelperNew.mCurrentYear = access$000;
                if (list.contains(String.valueOf(newWheelView7.getCurrentItem() + 1))) {
                    newWheelView8.setAdapter(new ArrayWheelAdapter(strArr4, 25));
                } else if (list2.contains(String.valueOf(newWheelView7.getCurrentItem() + 1))) {
                    newWheelView8.setAdapter(new ArrayWheelAdapter(strArr10, 25));
                } else if ((access$000 % 4 != 0 || access$000 % 100 == 0) && access$000 % 400 != 0) {
                    newWheelView8.setAdapter(new ArrayWheelAdapter(strArr11, 25));
                } else {
                    newWheelView8.setAdapter(new ArrayWheelAdapter(strArr8, 25));
                }
            }
        };
        $$Lambda$DialogHelperNew$KIbXzJn5gACfMN6HqLE_4tN6ZGM r0 = new OnWheelChangedNewListener(i2, i, newWheelView3, i3, asList, list3, newWheelView) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$KIbXzJn5gACfMN6HqLE_4tN6ZGM */
            private final /* synthetic */ int f$0;
            private final /* synthetic */ int f$1;
            private final /* synthetic */ NewWheelView f$2;
            private final /* synthetic */ int f$3;
            private final /* synthetic */ List f$4;
            private final /* synthetic */ List f$5;
            private final /* synthetic */ NewWheelView f$6;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            public final void onChanged(NewWheelView newWheelView, int i, int i2) {
                DialogHelperNew.lambda$initTimePicker$15(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, newWheelView, i, i2);
            }
        };
        newWheelView4.addChangingListener(r9);
        newWheelView5.addChangingListener(r0);
    }

    static /* synthetic */ void lambda$initTimePicker$15(int i, int i2, NewWheelView newWheelView, int i3, List list, List list2, NewWheelView newWheelView2, NewWheelView newWheelView3, int i4, int i5) {
        int i6 = i5 + 1;
        if (i + 1 == i6 && i2 == mCurrentYear) {
            newWheelView.setAdapter(new NumericWheelAdapter(1, i3));
            newWheelView.setCurrentItem(0);
        } else if (list.contains(String.valueOf(i6))) {
            newWheelView.setAdapter(new NumericWheelAdapter(1, 31));
        } else if (list2.contains(String.valueOf(i6))) {
            newWheelView.setAdapter(new NumericWheelAdapter(1, 30));
        } else if (((newWheelView2.getCurrentItem() + START_YEAR) % 4 != 0 || (newWheelView2.getCurrentItem() + START_YEAR) % 100 == 0) && (newWheelView2.getCurrentItem() + START_YEAR) % 400 != 0) {
            newWheelView.setAdapter(new NumericWheelAdapter(1, 28));
        } else {
            newWheelView.setAdapter(new NumericWheelAdapter(1, 29));
        }
    }

    /* access modifiers changed from: private */
    public static void setYearsMonth(int i, NewWheelView newWheelView) {
        Calendar instance = Calendar.getInstance();
        int i2 = instance.get(1);
        newWheelView.setAdapter(new NumericWheelAdapter(1, i2 == i ? instance.get(2) + 1 : 12));
        if (i2 == i) {
            newWheelView.setCurrentItem(0);
        }
    }

    public static Dialog showWheelHeightDialog(Activity activity, int i, WheelViewDialog.OnSelectClick onSelectClick) {
        return showWheelViewDialog(activity, Math.max(40, Math.min((int) Constants.HEIGHT_MAX_METRIC, i)) - 40, new NumericWheelAdapter(40, Constants.HEIGHT_MAX_METRIC), onSelectClick, 40);
    }

    private static Dialog showWheelViewDialog(Activity activity, int i, WheelAdapter wheelAdapter, WheelViewDialog.OnSelectClick onSelectClick, int i2) {
        WheelViewDialog wheelViewDialog = new WheelViewDialog(activity, R.style.MyDialog);
        NewWheelView wv_1 = wheelViewDialog.getWv_1();
        wv_1.setAddZero(false);
        wv_1.setVisibility(View.VISIBLE);
        wv_1.setVisibleItems(3);
        wv_1.setCyclic(false);
        wv_1.setAdapter(wheelAdapter);
        wv_1.setCurrentItem(i);
        wheelViewDialog.setOnSelectClick(onSelectClick);
        wheelViewDialog.setStartOffset1(i2);
        wheelViewDialog.show();
        return wheelViewDialog;
    }

    public static void showWheelHeightMileDialog(Context context, int i, WheelViewDialog.OnSelectClick onSelectClick) {
        WheelViewDialog wheelViewDialog = new WheelViewDialog(context, R.style.MyDialog);
        NewWheelView newWheelView = (NewWheelView) wheelViewDialog.findViewById(R.id.wv_1);
        NewWheelView newWheelView2 = (NewWheelView) wheelViewDialog.findViewById(R.id.wv_2);
        newWheelView.setVisibility(View.VISIBLE);
        newWheelView.setVisibleItems(3);
        newWheelView.setCyclic(false);
        newWheelView2.setVisibility(View.VISIBLE);
        newWheelView2.setVisibleItems(3);
        newWheelView2.setCyclic(false);
        wheelViewDialog.setOnSelectClick(onSelectClick);
        String[] strArr = new String[8];
        int i2 = 0;
        while (i2 < strArr.length) {
            StringBuilder sb = new StringBuilder();
            int i3 = i2 + 1;
            sb.append(i3);
            sb.append("'");
            strArr[i2] = sb.toString();
            i2 = i3;
        }
        String[] strArr2 = new String[12];
        for (int i4 = 0; i4 < strArr2.length; i4++) {
            strArr2[i4] = i4 + "\"";
        }
        String[] strArr3 = new String[3];
        for (int i5 = 0; i5 < strArr3.length; i5++) {
            strArr3[i5] = i5 + "\"";
        }
        newWheelView.setAdapter(new ArrayWheelAdapter(strArr, 25));
        newWheelView2.setAdapter(new ArrayWheelAdapter(strArr2, 25));
        int[] inch2inch = UnitFormat.inch2inch(i);
        newWheelView.setCurrentItem(inch2inch[0] - 1);
        if (inch2inch[0] == 8) {
            newWheelView2.setAdapter(new ArrayWheelAdapter(strArr3, 25));
        }
        newWheelView2.setCurrentItem(inch2inch[1]);
        newWheelView.addChangingListener(new OnWheelChangedNewListener(strArr, strArr3, newWheelView, newWheelView2, strArr2, i) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$HGL1v7bPIuh3IqGtkQ76BpKz9zI */
            private final /* synthetic */ String[] f$0;
            private final /* synthetic */ String[] f$1;
            private final /* synthetic */ NewWheelView f$2;
            private final /* synthetic */ NewWheelView f$3;
            private final /* synthetic */ String[] f$4;
            private final /* synthetic */ int f$5;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            public final void onChanged(NewWheelView newWheelView, int i, int i2) {
                DialogHelperNew.lambda$showWheelHeightMileDialog$16(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, newWheelView, i, i2);
            }
        });
        wheelViewDialog.show();
    }

    static /* synthetic */ void lambda$showWheelHeightMileDialog$16(String[] strArr, String[] strArr2, NewWheelView newWheelView, NewWheelView newWheelView2, String[] strArr3, int i, NewWheelView newWheelView3, int i2, int i3) {
        if (((i3 + 1) + "'").equals(strArr[strArr.length - 1])) {
            for (int i4 = 0; i4 < strArr2.length; i4++) {
                strArr2[i4] = i4 + "\"";
            }
            newWheelView.setAdapter(new ArrayWheelAdapter(strArr, 25));
            newWheelView2.setAdapter(new ArrayWheelAdapter(strArr3, 25));
            newWheelView.setCurrentItem(i3);
            newWheelView2.setCurrentItem(0);
            return;
        }
        newWheelView.setAdapter(new ArrayWheelAdapter(strArr, 25));
        newWheelView2.setAdapter(new ArrayWheelAdapter(strArr3, 25));
        UnitFormat.cm2inch(i);
        newWheelView.setCurrentItem(i3);
        newWheelView2.setCurrentItem(0);
    }

    public static void showWheelWeightDialog(Context context, int i, final WheelViewDialog.OnSelectClick onSelectClick) {
        int i2;
        final int i3;
        final WheelViewDialog wheelViewDialog = new WheelViewDialog(context, R.style.MyDialog);
        final NewWheelView wv_1 = wheelViewDialog.getWv_1();
        wv_1.setVisibility(View.VISIBLE);
        wv_1.setVisibleItems(3);
        wheelViewDialog.getWv_2().setVisibility(View.GONE);
        wheelViewDialog.getWv_3().setVisibility(View.GONE);
        if (!BleSdkWrapper.isDistUnitKm()) {
            i3 = 55;
            i2 = TinkerReport.KEY_LOADED_INTERPRET_INTERPRET_COMMAND_ERROR;
        } else {
            i3 = 20;
            i2 = 200;
        }
        if (i > i2) {
            i = i2;
        }
        if (i < i3) {
            i = i3;
        }
        wv_1.setAdapter(new NumericWheelAdapter(i3, i2));
        wv_1.setCurrentItem(i - i3);
        wheelViewDialog.findViewById(R.id.set).setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.util.DialogHelperNew.C26413 */

            public void onClick(View view) {
                WheelViewDialog.OnSelectClick onSelectClick = onSelectClick;
                if (onSelectClick != null) {
                    onSelectClick.onSelect(wv_1.getCurrentItem() + i3, 0, 0);
                }
                wheelViewDialog.dismiss();
            }
        });
        wheelViewDialog.show();
    }

    public static Dialog showWheelRemindDelayDialog(Activity activity, int i, WheelViewDialog.OnSelectClick onSelectClick) {
        return showWheelViewDialog(activity, (Math.max(1, Math.min((int) Constants.REMIND_MAX_METRIC, i)) / 5) - 1, new NumericWheelAdapter(1, (int) Constants.REMIND_MAX_METRIC, 5), onSelectClick, 1);
    }

    public static Dialog showRemindDialog(Activity activity, String str, String str2, String str3, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
        Dialog dialog = new Dialog(activity, R.style.center_dialog);
        View inflate = LayoutInflater.from(activity).inflate((int) R.layout.dialog_remind, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.dialogTitle)).setText(str);
        ((TextView) inflate.findViewById(R.id.tvTips)).setText(str2);
        inflate.findViewById(R.id.tvCanle).setOnClickListener(new View.OnClickListener(dialog, onClickListener2, inflate) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$p0AHoeSPqJqnhqZmmS_U5real5g */
            private final /* synthetic */ Dialog f$0;
            private final /* synthetic */ View.OnClickListener f$1;
            private final /* synthetic */ View f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                DialogHelperNew.lambda$showRemindDialog$17(this.f$0, this.f$1, this.f$2, view);
            }
        });
        TextView textView = (TextView) inflate.findViewById(R.id.tvSure);
        textView.setText(str3);
        textView.setOnClickListener(new View.OnClickListener(dialog, onClickListener, inflate) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$UxYyt1R7JS3MpfQa9zElgN_U2QM */
            private final /* synthetic */ Dialog f$0;
            private final /* synthetic */ View.OnClickListener f$1;
            private final /* synthetic */ View f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                DialogHelperNew.lambda$showRemindDialog$18(this.f$0, this.f$1, this.f$2, view);
            }
        });
        dialog.setContentView(inflate);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        double d = (double) activity.getResources().getDisplayMetrics().widthPixels;
        Double.isNaN(d);
        attributes.width = (int) (d * 0.8d);
        window.setAttributes(attributes);
        dialog.show();
        return dialog;
    }

    static /* synthetic */ void lambda$showRemindDialog$17(Dialog dialog, View.OnClickListener onClickListener, View view, View view2) {
        dialog.dismiss();
        onClickListener.onClick(view);
    }

    static /* synthetic */ void lambda$showRemindDialog$18(Dialog dialog, View.OnClickListener onClickListener, View view, View view2) {
        dialog.dismiss();
        onClickListener.onClick(view);
    }

    public static Dialog showTipsDialog(Activity activity, String str, String str2, String str3, View.OnClickListener onClickListener) {
        Dialog dialog = new Dialog(activity, R.style.center_dialog);
        View inflate = LayoutInflater.from(activity).inflate((int) R.layout.dialog_tips, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.dialogTitle)).setText(str);
        ((TextView) inflate.findViewById(R.id.tvTips)).setText(str2);
        TextView textView = (TextView) inflate.findViewById(R.id.tvSure);
        textView.setText(str3);
        textView.setOnClickListener(new View.OnClickListener(dialog, onClickListener, inflate) {
            /* class com.wade.fit.util.$$Lambda$DialogHelperNew$yYmh4d5YQVvXWbS5_p71B_KSbM */
            private final /* synthetic */ Dialog f$0;
            private final /* synthetic */ View.OnClickListener f$1;
            private final /* synthetic */ View f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                DialogHelperNew.lambda$showTipsDialog$19(this.f$0, this.f$1, this.f$2, view);
            }
        });
        dialog.setContentView(inflate);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        double d = (double) activity.getResources().getDisplayMetrics().widthPixels;
        Double.isNaN(d);
        attributes.width = (int) (d * 0.8d);
        window.setAttributes(attributes);
        dialog.show();
        return dialog;
    }

    static /* synthetic */ void lambda$showTipsDialog$19(Dialog dialog, View.OnClickListener onClickListener, View view, View view2) {
        dialog.dismiss();
        onClickListener.onClick(view);
    }
}
