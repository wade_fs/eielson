package com.wade.fit.model.net.api;

import com.wade.fit.model.BaseBean;
import okhttp3.RequestBody;
import p041io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserApi {
    @POST("user/editUser")
    Observable<BaseBean<String>> setUserInfo(@Body RequestBody requestBody);

    @POST("user/uploadFeedbackInfo")
    Observable<BaseBean<String>> uploadFeedbackInfo(@Body RequestBody requestBody);
}
