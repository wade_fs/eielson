package com.wade.fit.persenter.mine;

import android.text.TextUtils;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.persenter.mine.IGetUserInfoView;

public class GetUserPresenter<V extends IGetUserInfoView> extends BasePersenter<V> {
    public UserBean getCurrentUser() {
        UserBean userBean = AppApplication.getInstance().getUserBean();
        if (TextUtils.isEmpty(userBean.getUserName())) {
            userBean.setUserName("user");
        }
        return userBean;
    }
}
