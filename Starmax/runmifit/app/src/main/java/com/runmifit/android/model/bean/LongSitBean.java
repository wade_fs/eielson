package com.wade.fit.model.bean;

public class LongSitBean {
    public int endTime = 18;
    public int period = 1;
    public int repeat = 15;
    public int startTime = 9;
    public int state = 1;
    public byte[] weeks;

    public String toString() {
        return "LongSitBean{state=" + this.state + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", repeat=" + this.repeat + ", period=" + this.period + '}';
    }
}
