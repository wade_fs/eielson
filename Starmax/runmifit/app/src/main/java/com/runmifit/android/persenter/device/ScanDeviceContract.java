package com.wade.fit.persenter.device;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.BLEDevice;

public interface ScanDeviceContract {

    public interface Presenter {
        void connecting(int i, BLEDevice bLEDevice);

        void startScanBle(int i);

        void stopScanBle();
    }

    public interface View extends IBaseView {
        void requestFaild();

        void requestSuccess(int i, BLEDevice bLEDevice);
    }
}
