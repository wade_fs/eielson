package com.wade.fit.persenter.ecg;

import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.greendao.bean.EcgRecordInfo;
import com.wade.fit.greendao.gen.EcgRecordInfoDao;
import com.wade.fit.model.bean.ECGRecordShowList;
import com.wade.fit.persenter.ecg.EcgContract;
import java.util.ArrayList;
import java.util.List;

public class EcgPresenter extends BasePersenter<EcgContract.View> implements EcgContract.Presenter {
    public void getRecordInfos() {
        List list = AppApplication.getInstance().getDaoSession().getEcgRecordInfoDao().queryBuilder().orderDesc(EcgRecordInfoDao.Properties.Date).build().list();
        if (list.isEmpty()) {
            ((EcgContract.View) this.mView).getSuccess(null);
            return;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (list.size() == 1) {
            arrayList2.clear();
            int year = ((EcgRecordInfo) list.get(0)).getYear();
            int month = ((EcgRecordInfo) list.get(0)).getMonth();
            ECGRecordShowList eCGRecordShowList = new ECGRecordShowList();
            eCGRecordShowList.setDateMonth(year + "-" + month);
            arrayList2.add(list.get(0));
            eCGRecordShowList.setRecordInfos(arrayList2);
            arrayList.add(eCGRecordShowList);
        } else {
            ECGRecordShowList eCGRecordShowList2 = null;
            int i = 0;
            int i2 = 0;
            for (int i3 = 0; i3 < list.size(); i3++) {
                if (i3 == 0) {
                    arrayList2.clear();
                    i = ((EcgRecordInfo) list.get(i3)).getYear();
                    i2 = ((EcgRecordInfo) list.get(i3)).getMonth();
                    eCGRecordShowList2 = new ECGRecordShowList();
                    eCGRecordShowList2.setDateMonth(i + "-" + i2);
                    arrayList2.add(list.get(i3));
                } else if (i3 == list.size() - 1) {
                    if (((EcgRecordInfo) list.get(i3)).getYear() == i && ((EcgRecordInfo) list.get(i3)).getMonth() == i2) {
                        arrayList2.add(list.get(i3));
                        eCGRecordShowList2.setRecordInfos(arrayList2);
                        arrayList.add(eCGRecordShowList2);
                    } else {
                        arrayList2.clear();
                        i = ((EcgRecordInfo) list.get(i3)).getYear();
                        i2 = ((EcgRecordInfo) list.get(i3)).getMonth();
                        eCGRecordShowList2 = new ECGRecordShowList();
                        eCGRecordShowList2.setDateMonth(i + "-" + i2);
                        arrayList2.add(list.get(i3));
                        eCGRecordShowList2.setRecordInfos(arrayList2);
                        arrayList.add(eCGRecordShowList2);
                    }
                } else if (((EcgRecordInfo) list.get(i3)).getYear() == i && ((EcgRecordInfo) list.get(i3)).getMonth() == i2) {
                    arrayList2.add(list.get(i3));
                } else {
                    eCGRecordShowList2.setRecordInfos(arrayList2);
                    arrayList.add(eCGRecordShowList2);
                    arrayList2.clear();
                    i = ((EcgRecordInfo) list.get(i3)).getYear();
                    i2 = ((EcgRecordInfo) list.get(i3)).getMonth();
                    eCGRecordShowList2 = new ECGRecordShowList();
                    eCGRecordShowList2.setDateMonth(i + "-" + i2);
                    arrayList2.add(list.get(i3));
                }
            }
        }
        ((EcgContract.View) this.mView).getSuccess(arrayList);
    }
}
