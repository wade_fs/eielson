package com.wade.fit.util;

import android.R;
import android.os.AsyncTask;

public class AsyncTaskUtil {
    /* access modifiers changed from: private */
    public IAsyncTaskCallBack iAsyncTaskCallBack;
    private MyTask myTask;

    public static class AsyncTaskCallBackAdapter implements IAsyncTaskCallBack {
        public Object doInBackground(String... strArr) {
            return null;
        }

        public void onPostExecute(Object obj) {
        }
    }

    public interface IAsyncTaskCallBack {
        Object doInBackground(String... strArr);

        void onPostExecute(Object obj);
    }

    public AsyncTaskUtil() {
    }

    public AsyncTaskUtil(IAsyncTaskCallBack iAsyncTaskCallBack2) {
        this.iAsyncTaskCallBack = iAsyncTaskCallBack2;
    }

    public AsyncTaskUtil setIAsyncTaskCallBack(IAsyncTaskCallBack iAsyncTaskCallBack2) {
        this.iAsyncTaskCallBack = iAsyncTaskCallBack2;
        return this;
    }

    public void execute(String... strArr) {
        this.myTask = new MyTask();
        this.myTask.execute(strArr);
    }

    public void cancel(boolean z) {
        MyTask myTask2 = this.myTask;
        if (myTask2 != null) {
            myTask2.cancel(z);
        }
    }

    class MyTask extends AsyncTask<String, R.integer, Object> {
        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        MyTask() {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(String... strArr) {
            return AsyncTaskUtil.this.iAsyncTaskCallBack.doInBackground(strArr);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object obj) {
            AsyncTaskUtil.this.iAsyncTaskCallBack.onPostExecute(obj);
        }
    }
}
