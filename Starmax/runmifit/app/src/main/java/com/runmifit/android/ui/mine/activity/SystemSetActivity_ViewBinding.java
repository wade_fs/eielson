package com.wade.fit.ui.mine.activity;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.SystemSetActivity_ViewBinding */
public class SystemSetActivity_ViewBinding implements Unbinder {
    private SystemSetActivity target;
    private View view2131296501;
    private View view2131296514;

    public SystemSetActivity_ViewBinding(SystemSetActivity systemSetActivity) {
        this(systemSetActivity, systemSetActivity.getWindow().getDecorView());
    }

    public SystemSetActivity_ViewBinding(final SystemSetActivity systemSetActivity, View view) {
        this.target = systemSetActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.ilUnit, "method 'onClick'");
        this.view2131296514 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.SystemSetActivity_ViewBinding.C25971 */

            public void doClick(View view) {
                systemSetActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.ilGoogleFit, "method 'toGoogleFit'");
        this.view2131296501 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.SystemSetActivity_ViewBinding.C25982 */

            public void doClick(View view) {
                systemSetActivity.toGoogleFit();
            }
        });
    }

    public void unbind() {
        if (this.target != null) {
            this.target = null;
            this.view2131296514.setOnClickListener(null);
            this.view2131296514 = null;
            this.view2131296501.setOnClickListener(null);
            this.view2131296501 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
