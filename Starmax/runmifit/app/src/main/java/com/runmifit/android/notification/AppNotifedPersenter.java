package com.wade.fit.notification;

import android.content.pm.PackageInfo;
import com.wade.fit.model.bean.AppNotice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.util.SPHelper;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AppNotifedPersenter implements Serializable {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static AppNotifedPersenter appMode = new AppNotifedPersenter();
    public Map<String, PackageInfo> packageInfoMap = new HashMap();

    private AppNotifedPersenter() {
    }

    public static AppNotifedPersenter getInstance() {
        return appMode;
    }

    public boolean isOpenNotifed() {
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        if (!deviceConfig.isDisturbMode) {
            return false;
        }
        AppNotice appNotice = deviceConfig.notice;
        if (appNotice.line || appNotice.f5222vk || appNotice.instagram || appNotice.linked || appNotice.whatsApp || appNotice.twitter || appNotice.facebook || appNotice.f5221qq || appNotice.wechat || appNotice.skype) {
            return true;
        }
        return false;
    }
}
