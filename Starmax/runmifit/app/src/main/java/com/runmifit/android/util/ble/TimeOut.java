package com.wade.fit.util.ble;

import android.os.Handler;

public class TimeOut {
    private Handler handler = new Handler();

    /* access modifiers changed from: protected */
    public void poseTimeOut(Runnable runnable, int i) {
        this.handler.removeCallbacksAndMessages(null);
        this.handler.postDelayed(runnable, (long) i);
    }

    /* access modifiers changed from: protected */
    public void removeTimeOutTask() {
        this.handler.removeCallbacksAndMessages(null);
    }
}
