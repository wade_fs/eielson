package com.wade.fit.views.calendar;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.ldf.calendar.component.CalendarAttr;
import com.ldf.calendar.component.CalendarViewAdapter;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.ldf.calendar.view.Calendar;
import com.ldf.calendar.view.MonthPager;
import com.wade.fit.R;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Date;

public class CalendarDialog extends Dialog implements View.OnClickListener {
    CalendarViewAdapter calendarAdapter;
    MonthPager calendar_view;
    View layout_left;
    View layout_right;
    public OnSelectDateListener mOnSelectDateListener;
    OnSelectDateListener onSelectDateListener;
    CalendarDate selecetCalendarDate;
    TextView tvDate;

    static /* synthetic */ void lambda$initCalendarView$0(CalendarAttr.CalendarType calendarType) {
    }

    public CalendarDialog(Context context) {
        super(context, R.style.dialog_style);
    }

    public void showDialog() {
        if (this.selecetCalendarDate == null) {
            int[] iArr = DateUtil.todayYearMonthDay();
            this.selecetCalendarDate = new CalendarDate();
            CalendarDate calendarDate = this.selecetCalendarDate;
            calendarDate.year = iArr[0];
            calendarDate.month = iArr[1];
            calendarDate.day = iArr[2];
        }
        showDialog(this.selecetCalendarDate);
    }

    public void showDialog(CalendarDate calendarDate) {
        setContentView((int) R.layout.activity_calendar);
        this.calendar_view = (MonthPager) findViewById(R.id.calendar_view);
        this.layout_left = findViewById(R.id.layout_left);
        this.layout_left.setOnClickListener(this);
        this.tvDate = (TextView) findViewById(R.id.tvDate);
        this.layout_right = findViewById(R.id.layout_right);
        this.layout_right.setOnClickListener(this);
        initCalendarView();
        this.calendarAdapter.notifyDataChanged(calendarDate);
        TextView textView = this.tvDate;
        textView.setText(calendarDate.getYear() + "-" + calendarDate.getMonth());
        show();
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.width = -1;
        attributes.height = -2;
        attributes.gravity = 48;
        attributes.dimAmount = 0.2f;
        window.setAttributes(attributes);
    }

    private void initCalendarView() {
        initListener();
        this.calendarAdapter = new CalendarViewAdapter(getContext(), this.onSelectDateListener, CalendarAttr.WeekArrayType.Sunday, new CustomDayView(getContext(), R.layout.custom_day));
        this.calendarAdapter.setOnCalendarTypeChangedListener($$Lambda$CalendarDialog$I0WEHGqyvrRHMEOGQQtYPQxoPPs.INSTANCE);
        initMonthPager();
    }

    private void initListener() {
        this.onSelectDateListener = new OnSelectDateListener() {
            /* class com.wade.fit.views.calendar.CalendarDialog.C27211 */

            public void onSelectDate(CalendarDate calendarDate) {
                int[] iArr = DateUtil.todayYearMonthDay();
                Date date = ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day);
                if (date.getTime() <= ProDbUtils.getDate(iArr[0], iArr[1], iArr[2]).getTime()) {
                    BaseMessage baseMessage = new BaseMessage(103);
                    baseMessage.setData(date);
                    EventBusHelper.post(baseMessage);
                    LogUtil.d(calendarDate.getYear() + "-" + calendarDate.getMonth() + "-" + calendarDate.day);
                    CalendarDialog.this.dismiss();
                    if (CalendarDialog.this.mOnSelectDateListener != null) {
                        CalendarDialog.this.mOnSelectDateListener.onSelectDate(calendarDate);
                    }
                }
            }

            public void onSelectOtherMonth(int i) {
                CalendarDialog.this.calendar_view.selectOtherMonth(i);
            }
        };
    }

    private void initMonthPager() {
        this.calendar_view.setAdapter(this.calendarAdapter);
        this.calendar_view.setCurrentItem(MonthPager.CURRENT_DAY_INDEX);
        this.calendar_view.setPageTransformer(false, new ViewPager.PageTransformer() {
            /* class com.wade.fit.views.calendar.CalendarDialog.C27222 */

            public void transformPage(View view, float f) {
                view.setAlpha((float) Math.sqrt((double) (1.0f - Math.abs(f))));
            }
        });
        this.calendar_view.addOnPageChangeListener(new MonthPager.OnPageChangeListener() {
            /* class com.wade.fit.views.calendar.CalendarDialog.C27233 */

            public void onPageScrollStateChanged(int i) {
            }

            public void onPageScrolled(int i, float f, int i2) {
            }

            public void onPageSelected(int i) {
                ArrayList<Calendar> pagers = CalendarDialog.this.calendarAdapter.getPagers();
                if (pagers.get(i % pagers.size()) != null) {
                    CalendarDate seedDate = pagers.get(i % pagers.size()).getSeedDate();
                    LogUtil.d(seedDate.getYear() + "年" + seedDate.getMonth() + "");
                    CalendarDialog calendarDialog = CalendarDialog.this;
                    calendarDialog.selecetCalendarDate = seedDate;
                    TextView textView = calendarDialog.tvDate;
                    textView.setText(seedDate.getYear() + "-" + seedDate.getMonth());
                }
            }
        });
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.layout_left) {
            this.calendar_view.selectOtherMonth(-1);
        } else if (id == R.id.layout_right) {
            this.calendar_view.selectOtherMonth(1);
        }
    }
}
