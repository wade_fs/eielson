package com.wade.fit.util.ble;

import android.bluetooth.BluetoothGattCharacteristic;
import java.util.Arrays;

public class Request {
    public BleCallback callback;
    public BluetoothGattCharacteristic characteristic;
    public int flag;
    public boolean isNwxtDelaySend;
    public boolean isTimeOut;
    public boolean isWaitComplete;
    public byte[] value;

    private enum Type {
        CREATE_BOND,
        WRITE,
        READ,
        WRITE_DESCRIPTOR,
        READ_DESCRIPTOR,
        ENABLE_NOTIFICATIONS,
        ENABLE_INDICATIONS,
        READ_BATTERY_LEVEL,
        ENABLE_BATTERY_LEVEL_NOTIFICATIONS,
        DISABLE_BATTERY_LEVEL_NOTIFICATIONS,
        ENABLE_SERVICE_CHANGED_INDICATIONS
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("flag:");
        sb.append(this.flag);
        sb.append(",value:");
        byte[] bArr = this.value;
        sb.append(bArr == null ? "is null" : ByteDataConvertUtil.bytesToHexString(bArr));
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return Arrays.equals(((Request) obj).value, this.value);
    }

    private Request(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr) {
        this.characteristic = bluetoothGattCharacteristic;
        this.value = bArr;
    }

    private Request(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback) {
        this(bluetoothGattCharacteristic, bArr);
        this.callback = bleCallback;
    }

    private Request(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback, int i) {
        this(bluetoothGattCharacteristic, bArr);
        this.callback = bleCallback;
        this.flag = i;
    }

    private Request(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback, boolean z) {
        this(bluetoothGattCharacteristic, bArr);
        this.callback = bleCallback;
        this.isWaitComplete = z;
    }

    public static Request newWriteRequest(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr) {
        return new Request(bluetoothGattCharacteristic, bArr);
    }

    public static Request newWriteRequest(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback) {
        return new Request(bluetoothGattCharacteristic, bArr, bleCallback);
    }

    public static Request newWriteRequest(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback, int i) {
        return new Request(bluetoothGattCharacteristic, bArr, bleCallback, i);
    }

    public static Request newWriteRequest(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback, boolean z) {
        return new Request(bluetoothGattCharacteristic, bArr, bleCallback, z);
    }
}
