package com.wade.fit.util.callback;

import android.graphics.Bitmap;

public interface MapScreenShotCallback {
    void shotComplet(Bitmap bitmap);
}
