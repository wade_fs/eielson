package com.wade.fit.persenter.device;

import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.bean.Goal;
import com.wade.fit.persenter.device.GoalSetContract;
import com.wade.fit.util.ble.BleCallbackWrapper;
import com.wade.fit.util.ble.BleSdkWrapper;

public class GoalSetPersenter extends BasePersenter<GoalSetContract.View> implements GoalSetContract.Presenter {
    public void saveGold(final int i, Goal goal) {
        BleSdkWrapper.setTarget(goal, new BleCallbackWrapper() {
            /* class com.wade.fit.persenter.device.GoalSetPersenter.C23911 */

            public void setSuccess() {
                ((GoalSetContract.View) GoalSetPersenter.this.mView).saveSuccess(i);
            }

            public void setFaild() {
                ((GoalSetContract.View) GoalSetPersenter.this.mView).saveFaild(i);
            }
        });
    }
}
