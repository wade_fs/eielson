package com.wade.fit.views.ecg;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class EcgGrid extends View {
    private int mGridColor = Color.parseColor("#ffe6e6");
    public Paint mPaint = new Paint();
    private int mSGridColor = Color.parseColor("#fff0ed");

    public EcgGrid(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setDither(true);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawRect(canvas);
    }

    private void drawRect(Canvas canvas) {
        float width = (((float) getWidth()) / 256.0f) * 25.0f;
        float height = (((float) getHeight()) / 256.0f) * 25.0f;
        float width2 = ((float) getWidth()) / width;
        int i = 0;
        int i2 = 0;
        while (true) {
            float f = (float) i2;
            if (f > width) {
                break;
            }
            this.mPaint.setColor(this.mSGridColor);
            this.mPaint.setStrokeWidth(1.0f);
            float f2 = f * width2;
            canvas.drawLine(((float) getLeft()) + f2, (float) getTop(), ((float) getLeft()) + f2, (float) getHeight(), this.mPaint);
            if (i2 % 5 == 0) {
                this.mPaint.setColor(this.mGridColor);
                this.mPaint.setStrokeWidth(1.5f);
                canvas.drawLine(((float) getLeft()) + f2, (float) getTop(), ((float) getLeft()) + f2, (float) getHeight(), this.mPaint);
            }
            i2++;
        }
        while (true) {
            float f3 = (float) i;
            if (f3 <= height) {
                this.mPaint.setColor(this.mSGridColor);
                this.mPaint.setStrokeWidth(1.0f);
                float f4 = f3 * width2;
                canvas.drawLine((float) getLeft(), ((float) getTop()) + f4, (float) getRight(), ((float) getTop()) + f4, this.mPaint);
                if (i % 5 == 0) {
                    this.mPaint.setColor(this.mGridColor);
                    this.mPaint.setStrokeWidth(1.5f);
                    canvas.drawLine((float) getLeft(), ((float) getTop()) + f4, (float) getRight(), ((float) getTop()) + f4, this.mPaint);
                }
                i++;
            } else {
                this.mPaint.setColor(this.mGridColor);
                this.mPaint.setStrokeWidth(1.5f);
                canvas.drawLine((float) getLeft(), 1.5f, (float) getRight(), 1.5f, this.mPaint);
                return;
            }
        }
    }
}
