package com.wade.fit.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.wade.fit.app.AppApplication;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleManager;
import com.wade.fit.util.log.LogUtil;

public class BleReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
            switch (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE)) {
                case 10:
                    LogUtil.m5266d("aaa", "STATE_OFF 手机蓝牙关闭");
                    BleManager.getInstance().cleanConnectionState();
                    EventBusHelper.postSticky(301);
                    return;
                case 11:
                    LogUtil.m5266d("aaa", "STATE_TURNING_ON 手机蓝牙正在开启");
                    return;
                case 12:
                    LogUtil.m5266d("aaa", "STATE_ON 手机蓝牙开启");
                    SharePreferenceUtils.putString(AppApplication.getInstance(), "scanDevice", "1");
                    EventBusHelper.postSticky(300);
                    return;
                case 13:
                    LogUtil.m5266d("aaa", "STATE_TURNING_OFF 手机蓝牙正在关闭");
                    return;
                default:
                    return;
            }
        }
    }
}
