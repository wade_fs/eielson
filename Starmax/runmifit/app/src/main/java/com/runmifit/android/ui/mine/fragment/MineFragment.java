package com.wade.fit.ui.mine.fragment;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.base.BaseFragment;
import com.wade.fit.ui.main.activity.LoginActivity;
import com.wade.fit.ui.mine.activity.FeedbackActivity;
import com.wade.fit.ui.mine.activity.MineInfoActivity;
import com.wade.fit.ui.mine.activity.SystemSetActivity;
import com.wade.fit.ui.mine.activity.WebViewFaqActivity;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.ItemLableValue;
import java.util.Objects;

/* renamed from: com.wade.fit.ui.mine.fragment.MineFragment */
public class MineFragment extends BaseFragment {
    ItemLableValue ilAbout;
    ItemLableValue ilFeedBack;
    ItemLableValue ilNewUser;
    ItemLableValue ilQuestion;
    ItemLableValue ilSafe;
    ItemLableValue ilUserInfo;
    ImageView imgBackground;
    ImageView ivHeader;
    RelativeLayout rlContent;
    RelativeLayout rlScreenTitle;
    TextView tvUserName;

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.fragment_mine;
    }

    /* access modifiers changed from: protected */
    public void onVisiable() {
        super.onVisiable();
        if (Build.VERSION.SDK_INT >= 23) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(9216);
        }
        if (TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            this.tvUserName.setText(getResources().getString(R.string.login_register));
        } else if (TextUtils.isEmpty(AppApplication.getInstance().getUserBean().getUserName())) {
            this.tvUserName.setText(getResources().getString(R.string.nosetting));
        } else {
            this.tvUserName.setText(AppApplication.getInstance().getUserBean().getUserName());
        }
        LogUtil.d("headerUrl:" + AppApplication.getInstance().getUserBean().getHeaderUrl());
        ((BaseActivity) getActivity()).getImageLoader().loadPortrait(getActivity(), this.ivHeader, AppApplication.getInstance().getUserBean().getHeaderUrl());
        ((BaseActivity) getActivity()).getImageLoader().loadImage(getActivity(), this.imgBackground, AppApplication.getInstance().getUserBean().getCoverImage());
    }

    /* access modifiers changed from: package-private */
    public void editUserInfo() {
        if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else {
            IntentUtil.goToActivity(getActivity(), MineInfoActivity.class);
        }
    }

    /* access modifiers changed from: package-private */
    public void toFaq() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ilQuestion)) {
            Bundle bundle = new Bundle();
            bundle.putInt("loadType", 4);
            IntentUtil.goToActivity(getActivity(), WebViewFaqActivity.class, bundle);
        }
    }

    /* access modifiers changed from: package-private */
    public void editUnit() {
        if (AppApplication.getInstance().isSysndata()) {
            showToast(getResources().getString(R.string.server_sync_data));
        } else {
            IntentUtil.goToActivity(getActivity(), SystemSetActivity.class);
        }
    }

    /* access modifiers changed from: package-private */
    public void toFeedBack() {
        if (!TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            IntentUtil.goToActivity(getActivity(), FeedbackActivity.class);
        } else {
            showToast(getResources().getString(R.string.to_login));
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.rlScreenTitle.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = ScreenUtil.getStatusHeight(getActivity());
        if (AppApplication.getInstance().getUserBean() != null) {
            ((BaseActivity) Objects.requireNonNull(getActivity())).getImageLoader().loadPortrait(getActivity(), this.ivHeader, AppApplication.getInstance().getUserBean().getHeaderUrl());
            ((BaseActivity) getActivity()).getImageLoader().loadImage(getActivity(), this.imgBackground, AppApplication.getInstance().getUserBean().getCoverImage());
        }
    }

    /* access modifiers changed from: package-private */
    public void toLogin() {
        if (TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            Bundle bundle = new Bundle();
            bundle.putString("from", "MainActivity");
            IntentUtil.goToActivity(getActivity(), LoginActivity.class, bundle);
        }
    }
}
