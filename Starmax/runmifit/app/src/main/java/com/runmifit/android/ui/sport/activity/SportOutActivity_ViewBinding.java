package com.wade.fit.ui.sport.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.baidu.mapapi.map.MapView;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.sport.activity.SportOutActivity_ViewBinding */
public class SportOutActivity_ViewBinding implements Unbinder {
    private SportOutActivity target;
    private View view2131296532;
    private View view2131296652;
    private View view2131296653;
    private View view2131296654;

    public SportOutActivity_ViewBinding(SportOutActivity sportOutActivity) {
        this(sportOutActivity, sportOutActivity.getWindow().getDecorView());
    }

    public SportOutActivity_ViewBinding(final SportOutActivity sportOutActivity, View view) {
        this.target = sportOutActivity;
        sportOutActivity.mMapView = (MapView) Utils.findRequiredViewAsType(view, R.id.mMapView, "field 'mMapView'", MapView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.llSportRun, "field 'llSportRun' and method 'setTypeRun'");
        sportOutActivity.llSportRun = (LinearLayout) Utils.castView(findRequiredView, R.id.llSportRun, "field 'llSportRun'", LinearLayout.class);
        this.view2131296653 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportOutActivity_ViewBinding.C26201 */

            public void doClick(View view) {
                sportOutActivity.setTypeRun();
            }
        });
        sportOutActivity.imgSportRun = (ImageView) Utils.findRequiredViewAsType(view, R.id.imgSportRun, "field 'imgSportRun'", ImageView.class);
        sportOutActivity.tvSportRun = (TextView) Utils.findRequiredViewAsType(view, R.id.tvSportRun, "field 'tvSportRun'", TextView.class);
        View findRequiredView2 = Utils.findRequiredView(view, R.id.llSportWalk, "field 'llSportWalk' and method 'setTypeWalk'");
        sportOutActivity.llSportWalk = (LinearLayout) Utils.castView(findRequiredView2, R.id.llSportWalk, "field 'llSportWalk'", LinearLayout.class);
        this.view2131296654 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportOutActivity_ViewBinding.C26212 */

            public void doClick(View view) {
                sportOutActivity.setTypeWalk();
            }
        });
        sportOutActivity.imgSportWalk = (ImageView) Utils.findRequiredViewAsType(view, R.id.imgSportWalk, "field 'imgSportWalk'", ImageView.class);
        sportOutActivity.tvSportWalk = (TextView) Utils.findRequiredViewAsType(view, R.id.tvSportWalk, "field 'tvSportWalk'", TextView.class);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.llSportBike, "field 'llSportBike' and method 'setTypeBike'");
        sportOutActivity.llSportBike = (LinearLayout) Utils.castView(findRequiredView3, R.id.llSportBike, "field 'llSportBike'", LinearLayout.class);
        this.view2131296652 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportOutActivity_ViewBinding.C26223 */

            public void doClick(View view) {
                sportOutActivity.setTypeBike();
            }
        });
        sportOutActivity.imgSportBike = (ImageView) Utils.findRequiredViewAsType(view, R.id.imgSportBike, "field 'imgSportBike'", ImageView.class);
        sportOutActivity.tvSportBike = (TextView) Utils.findRequiredViewAsType(view, R.id.tvSportBike, "field 'tvSportBike'", TextView.class);
        View findRequiredView4 = Utils.findRequiredView(view, R.id.imgStart, "method 'startSport'");
        this.view2131296532 = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.sport.activity.SportOutActivity_ViewBinding.C26234 */

            public void doClick(View view) {
                sportOutActivity.startSport();
            }
        });
    }

    public void unbind() {
        SportOutActivity sportOutActivity = this.target;
        if (sportOutActivity != null) {
            this.target = null;
            sportOutActivity.mMapView = null;
            sportOutActivity.llSportRun = null;
            sportOutActivity.imgSportRun = null;
            sportOutActivity.tvSportRun = null;
            sportOutActivity.llSportWalk = null;
            sportOutActivity.imgSportWalk = null;
            sportOutActivity.tvSportWalk = null;
            sportOutActivity.llSportBike = null;
            sportOutActivity.imgSportBike = null;
            sportOutActivity.tvSportBike = null;
            this.view2131296653.setOnClickListener(null);
            this.view2131296653 = null;
            this.view2131296654.setOnClickListener(null);
            this.view2131296654 = null;
            this.view2131296652.setOnClickListener(null);
            this.view2131296652 = null;
            this.view2131296532.setOnClickListener(null);
            this.view2131296532 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
