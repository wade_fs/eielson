package com.wade.fit.util.image;

import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import java.lang.reflect.Array;

public class StackBlurUtil {
    public static void stackBlur(int i, int i2, int i3, int[] iArr) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        int i7 = i4 - 1;
        int i8 = i5 - 1;
        int i9 = i4 * i5;
        int i10 = i6 + i6 + 1;
        int[] iArr2 = new int[i9];
        int[] iArr3 = new int[i9];
        int[] iArr4 = new int[i9];
        int[] iArr5 = new int[Math.max(i, i2)];
        int i11 = (i10 + 1) >> 1;
        int i12 = i11 * i11;
        int i13 = i12 * 256;
        int[] iArr6 = new int[i13];
        int i14 = 0;
        for (int i15 = 0; i15 < i13; i15++) {
            iArr6[i15] = i15 / i12;
        }
        int[][] iArr7 = (int[][]) Array.newInstance(int.class, i10, 3);
        int i16 = i6 + 1;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        while (i17 < i5) {
            int i20 = 0;
            int i21 = 0;
            int i22 = 0;
            int i23 = 0;
            int i24 = 0;
            int i25 = 0;
            int i26 = 0;
            int i27 = 0;
            int i28 = 0;
            for (int i29 = -i6; i29 <= i6; i29++) {
                int i30 = iArr[i18 + Math.min(i7, Math.max(i29, i14))];
                int[] iArr8 = iArr7[i29 + i6];
                iArr8[i14] = (i30 & 16711680) >> 16;
                iArr8[1] = (i30 & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                iArr8[2] = i30 & 255;
                int abs = i16 - Math.abs(i29);
                i20 += iArr8[i14] * abs;
                i21 += iArr8[1] * abs;
                i22 += iArr8[2] * abs;
                if (i29 > 0) {
                    i26 += iArr8[i14];
                    i27 += iArr8[1];
                    i28 += iArr8[2];
                } else {
                    i23 += iArr8[i14];
                    i24 += iArr8[1];
                    i25 += iArr8[2];
                }
            }
            int i31 = i6;
            int i32 = 0;
            while (i32 < i4) {
                iArr2[i18] = iArr6[i20];
                iArr3[i18] = iArr6[i21];
                iArr4[i18] = iArr6[i22];
                int i33 = i20 - i23;
                int i34 = i21 - i24;
                int i35 = i22 - i25;
                int[] iArr9 = iArr7[((i31 - i6) + i10) % i10];
                int i36 = i23 - iArr9[i14];
                int i37 = i24 - iArr9[1];
                int i38 = i25 - iArr9[2];
                if (i17 == 0) {
                    iArr5[i32] = Math.min(i32 + i6 + 1, i7);
                }
                int i39 = iArr[i19 + iArr5[i32]];
                iArr9[0] = (i39 & 16711680) >> 16;
                iArr9[1] = (i39 & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8;
                iArr9[2] = i39 & 255;
                int i40 = i26 + iArr9[0];
                int i41 = i27 + iArr9[1];
                int i42 = i28 + iArr9[2];
                i20 = i33 + i40;
                i21 = i34 + i41;
                i22 = i35 + i42;
                i31 = (i31 + 1) % i10;
                int[] iArr10 = iArr7[i31 % i10];
                i23 = i36 + iArr10[0];
                i24 = i37 + iArr10[1];
                i25 = i38 + iArr10[2];
                i26 = i40 - iArr10[0];
                i27 = i41 - iArr10[1];
                i28 = i42 - iArr10[2];
                i18++;
                i32++;
                i14 = 0;
            }
            i19 += i4;
            i17++;
            i5 = i2;
            i14 = 0;
        }
        int i43 = 0;
        while (i43 < i4) {
            int i44 = -i6;
            int i45 = i44 * i4;
            int i46 = 0;
            int i47 = 0;
            int i48 = 0;
            int i49 = 0;
            int i50 = 0;
            int i51 = 0;
            int i52 = 0;
            int i53 = 0;
            int i54 = 0;
            while (i44 <= i6) {
                int[] iArr11 = iArr5;
                int max = Math.max(0, i45) + i43;
                int[] iArr12 = iArr7[i44 + i6];
                iArr12[0] = iArr2[max];
                iArr12[1] = iArr3[max];
                iArr12[2] = iArr4[max];
                int abs2 = i16 - Math.abs(i44);
                i46 += iArr2[max] * abs2;
                i47 += iArr3[max] * abs2;
                i48 += iArr4[max] * abs2;
                if (i44 > 0) {
                    i52 += iArr12[0];
                    i53 += iArr12[1];
                    i54 += iArr12[2];
                } else {
                    i49 += iArr12[0];
                    i50 += iArr12[1];
                    i51 += iArr12[2];
                }
                if (i44 < i8) {
                    i45 += i4;
                }
                i44++;
                iArr5 = iArr11;
            }
            int[] iArr13 = iArr5;
            int i55 = i2;
            int i56 = i43;
            int i57 = i54;
            int i58 = 0;
            int i59 = i53;
            int i60 = i52;
            int i61 = i6;
            while (i58 < i55) {
                iArr[i56] = (iArr[i56] & ViewCompat.MEASURED_STATE_MASK) | (iArr6[i46] << 16) | (iArr6[i47] << 8) | iArr6[i48];
                int i62 = i46 - i49;
                int i63 = i47 - i50;
                int i64 = i48 - i51;
                int[] iArr14 = iArr7[((i61 - i6) + i10) % i10];
                int i65 = i49 - iArr14[0];
                int i66 = i50 - iArr14[1];
                int i67 = i51 - iArr14[2];
                if (i43 == 0) {
                    iArr13[i58] = Math.min(i58 + i16, i8) * i4;
                }
                int i68 = iArr13[i58] + i43;
                iArr14[0] = iArr2[i68];
                iArr14[1] = iArr3[i68];
                iArr14[2] = iArr4[i68];
                int i69 = i60 + iArr14[0];
                int i70 = i59 + iArr14[1];
                int i71 = i57 + iArr14[2];
                i46 = i62 + i69;
                i47 = i63 + i70;
                i48 = i64 + i71;
                i61 = (i61 + 1) % i10;
                int[] iArr15 = iArr7[i61];
                i49 = i65 + iArr15[0];
                i50 = i66 + iArr15[1];
                i51 = i67 + iArr15[2];
                i60 = i69 - iArr15[0];
                i59 = i70 - iArr15[1];
                i57 = i71 - iArr15[2];
                i56 += i4;
                i58++;
                i6 = i3;
            }
            i43++;
            i6 = i3;
            iArr5 = iArr13;
        }
    }
}
