package com.wade.fit.views.ecg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.wade.fit.R;

public class EcgGridAdapter extends BaseAdapter {
    private Context context;
    private String[] hrv_score;
    private String[] hrv_title;
    private LayoutInflater inflater = LayoutInflater.from(this.context);
    private int[] txt_color;

    public long getItemId(int i) {
        return (long) i;
    }

    public EcgGridAdapter(Context context2, String[] strArr, String[] strArr2, int[] iArr) {
        this.context = context2;
        this.hrv_title = strArr;
        this.hrv_score = strArr2;
        this.txt_color = iArr;
    }

    public int getCount() {
        return this.hrv_title.length;
    }

    public Object getItem(int i) {
        return this.hrv_title[i];
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        ViewHolder viewHolder;
        if (view == null) {
            viewHolder = new ViewHolder();
            view2 = this.inflater.inflate((int) R.layout.ecgreport_grid_item, (ViewGroup) null);
            viewHolder.textView1 = (TextView) view2.findViewById(R.id.tv_item);
            viewHolder.textView2 = (TextView) view2.findViewById(R.id.tv_item_des);
            view2.setTag(viewHolder);
        } else {
            view2 = view;
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.textView1.setText(this.hrv_title[i]);
        viewHolder.textView2.setText(this.hrv_score[i]);
        viewHolder.textView2.setTextColor(this.context.getResources().getColor(this.txt_color[i]));
        return view2;
    }

    private static class ViewHolder {
        TextView textView1;
        TextView textView2;

        private ViewHolder() {
        }
    }
}
