package com.wade.fit.model.bean;

import java.io.Serializable;

public class BLEDevice implements Serializable, Comparable<BLEDevice> {
    private static final long serialVersionUID = -5217710157640312976L;
    public boolean isBind;
    public String mDeviceAddress;
    public String mDeviceName;
    public String mDeviceProduct;
    public String mDeviceVersion;
    public int mRssi;
    public int power;

    public int compareTo(BLEDevice bLEDevice) {
        int i = this.mRssi;
        int i2 = bLEDevice.mRssi;
        if (i > i2) {
            return -1;
        }
        return i == i2 ? 0 : 1;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return this.mDeviceAddress.equals(((BLEDevice) obj).mDeviceAddress);
    }

    public String toString() {
        return "BLEDevice{mDeviceName='" + this.mDeviceName + '\'' + ", mDeviceAddress='" + this.mDeviceAddress + '\'' + ", mDeviceProduct='" + this.mDeviceProduct + '\'' + ", mDeviceVersion='" + this.mDeviceVersion + '\'' + ", mRssi=" + this.mRssi + '}';
    }
}
