package com.wade.fit.model.net.http;

import com.wade.fit.model.BaseBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.ApiManager;
import okhttp3.RequestBody;

public class UserHttp {
    public static void setUserInfo(RequestBody requestBody, ApiCallback<BaseBean<String>> apiCallback) {
        ApiManager.addObservable(ApiManager.getUserApi().setUserInfo(requestBody)).subscribe(apiCallback);
    }

    public static void uploadFeedbackInfo(RequestBody requestBody, ApiCallback<BaseBean<String>> apiCallback) {
        ApiManager.addObservable(ApiManager.getUserApi().uploadFeedbackInfo(requestBody)).subscribe(apiCallback);
    }
}
