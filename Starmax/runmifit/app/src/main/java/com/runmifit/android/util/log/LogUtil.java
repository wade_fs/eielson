package com.wade.fit.util.log;

import android.text.TextUtils;
import android.util.Log;
import com.baidu.mobstat.Config;
import com.wade.fit.app.Constant;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.math3.geometry.VectorFormat;
import org.json.JSONArray;
import org.json.JSONObject;

public class LogUtil {
    private static final String APP_ROOT_PATH = Constant.APP_ROOT_PATH;
    private static final String GOOGLEFIT_PATH = (LOG_PATH + "/googlefit/");
    private static final int JSON_INDENT = 4;
    public static final String LOG_PATH = (APP_ROOT_PATH + "/log");
    private static String className = null;
    private static int lineNumber = 0;
    private static SimpleDateFormat logfile = new SimpleDateFormat("yyyy-MM-dd");
    private static final String login_log = "login_log.txt";
    private static String methodName;
    private static SimpleDateFormat myLogSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /* renamed from: d */
    public static void m5266d(String str, String str2) {
        getMethodNames(new Throwable().getStackTrace(), 1);
        String createLog = createLog(str);
        LogService.m6213p(str2, "dAndSave", createLog);
        Log.i(className, createLog);
    }

    public static void dAndSave(String str, String str2) {
        getMethodNames(new Throwable().getStackTrace(), 1);
        String createLog = createLog(str);
        LogService.m6213p(str2, "dAndSave", createLog);
        Log.i(className, createLog);
    }

    /* renamed from: d2 */
    public static void m5267d2(String str) {
        getMethodNames(new Throwable().getStackTrace(), 2);
        Log.i(className, createLog(str));
    }

    /* renamed from: d */
    public static void d(String str) {
        getMethodNames(new Throwable().getStackTrace(), 1);
        Log.i(className, createLog(str));
    }

    /* renamed from: e */
    public static void m5268e(String str) {
        getMethodNames(new Throwable().getStackTrace(), 1);
        Log.e(className, createLog(str));
    }

    /* renamed from: w */
    public static void m5269w(String str) {
        getMethodNames(new Throwable().getStackTrace(), 1);
        Log.w(className, createLog(str));
    }

    /* renamed from: d */
    public static void m5264d(Object obj) {
        String obj2 = obj == null ? "message is null" : obj.toString();
        getMethodNames(new Throwable().getStackTrace(), 1);
        Log.i(className, createLog(obj2));
    }

    private static String createLog(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        stringBuffer.append("(");
        stringBuffer.append(className);
        stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
        stringBuffer.append(lineNumber);
        stringBuffer.append(")");
        stringBuffer.append("#");
        stringBuffer.append(methodName);
        stringBuffer.append("]");
        stringBuffer.append(printIfJson(str));
        return stringBuffer.toString();
    }

    public static String printIfJson(String str) {
        int i;
        StringBuffer stringBuffer = new StringBuffer();
        try {
            if (str.contains(VectorFormat.DEFAULT_PREFIX)) {
                i = str.indexOf(VectorFormat.DEFAULT_PREFIX);
            } else {
                i = str.contains("[") ? str.indexOf(VectorFormat.DEFAULT_PREFIX) : -1;
            }
            if (i != -1) {
                stringBuffer.append(str.substring(0, i));
                str = str.substring(i);
            }
            if (str.startsWith(VectorFormat.DEFAULT_PREFIX)) {
                str = new JSONObject(str).toString(4);
            } else if (str.startsWith("[")) {
                str = new JSONArray(str).toString(4);
            }
        } catch (Exception unused) {
        }
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    private static void getMethodNames(StackTraceElement[] stackTraceElementArr, int i) {
        className = stackTraceElementArr[i].getFileName();
        methodName = stackTraceElementArr[i].getMethodName();
        lineNumber = stackTraceElementArr[i].getLineNumber();
    }

    public static void writeGoogleFitLogInfotoFile(String str) {
        writeGoogleFitLogInfotoFile(null, str);
    }

    public static void writeGoogleFitLogInfotoFile(String str, String str2) {
        String str3 = GOOGLEFIT_PATH;
        writeMsgToFile(GOOGLEFIT_PATH, new File(str3, "googlefit_" + logfile.format(new Date()) + ".txt").getName(), str2, true, true);
    }

    private static void writeMsgToFile(String str, String str2, String str3, boolean z, boolean z2) {
        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            File file = new File(APP_ROOT_PATH);
            if (!TextUtils.isEmpty(str)) {
                file = new File(str);
            }
            if (!file.exists()) {
                file.mkdirs();
            }
            if (!TextUtils.isEmpty(str) && z2) {
                deleteOldFile(file.getAbsolutePath());
            }
            File file2 = new File(file, str2);
            BufferedWriter bufferedWriter = null;
            try {
                BufferedWriter bufferedWriter2 = new BufferedWriter(new FileWriter(file2, z));
                try {
                    bufferedWriter2.write("[" + myLogSdf.format(new Date()) + "]" + str3);
                    bufferedWriter2.newLine();
                    bufferedWriter2.flush();
                    closeWrite(bufferedWriter2);
                } catch (IOException e) {
                    e = e;
                    bufferedWriter = bufferedWriter2;
                    try {
                        e.printStackTrace();
                        closeWrite(bufferedWriter);
                    } catch (Throwable th) {
                        th = th;
                        bufferedWriter2 = bufferedWriter;
                        closeWrite(bufferedWriter2);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    closeWrite(bufferedWriter2);
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
                closeWrite(bufferedWriter);
            }
        }
    }

    private static void deleteOldFile(String str) {
        File[] listFiles;
        File file = new File(str);
        if (file.exists() && (listFiles = file.listFiles()) != null && file.length() > 10) {
            for (File file2 : listFiles) {
                if (daysOfTwo(file2.lastModified()) > 10) {
                    d("file1.getAbsolutePath():" + file2.getAbsolutePath());
                    d("file1.delete():" + file2.delete());
                }
            }
        }
    }

    public static void closeWrite(BufferedWriter bufferedWriter) {
        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static int daysOfTwo(long j) {
        return daysOfTwo(new Date(j));
    }

    public static int daysOfTwo(Date date) {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(6);
        instance.setTime(date);
        return i - instance.get(6);
    }

    public static void login_log(String str) {
        writeLogDug(str, login_log, true);
    }

    private static void writeLogDug(String str, String str2, boolean z) {
        writeMsgToFile(null, str2, str, z, false);
    }
}
