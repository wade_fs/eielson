package com.wade.fit.persenter.ecg;

import com.wade.fit.base.IBaseView;
import com.wade.fit.greendao.bean.ECGItemInfo;
import java.util.List;

public interface EcgDetailContract {

    public interface Presenter {
        void getEcgDetail(String str);
    }

    public interface View extends IBaseView {
        void getSuccess(List<ECGItemInfo> list);
    }
}
