package com.wade.fit.util.ble;

import com.google.common.base.Ascii;
import com.wade.fit.model.bean.AlarmBean;
import com.wade.fit.model.bean.AppNotice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.LongSitBean;
import com.wade.fit.model.bean.SleepTime;
import com.wade.fit.model.bean.WeatherBean;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class CmdHelper {
    public static final byte[] CMD_BIND_DEVICE = {Ascii.f4505FF, 4, 0, 0, 5, 6, 7};
    public static final byte[] CMD_CLEAR_ACTIVITY = {35, 1, 0, 0, -56};
    public static final byte[] CMD_GET_ACTIVITY = {35, 1, 0, 0, 114};
    public static final byte[] CMD_GET_ALARM = {5, 0, 0, 8};
    public static final byte[] CMD_GET_CURRENT_HISTORY_STEP = {32, 5, 0, 1, -30, 7, Ascii.f4505FF, 1, -62};
    public static final byte[] CMD_GET_CURRENT_POWER = {39, 0, 0, 116};
    public static final byte[] CMD_GET_CURRENT_STEP = {32, 1, 0, 0, 112};
    public static final byte[] CMD_GET_DEVICE = {1, 0, 0, -80};
    public static final byte[] CMD_GET_DEVICE_STATE = {2, 0, 0, 6};
    public static final byte[] CMD_GET_HART_OPEN = {33, 1, 0, 8, 118};
    public static final byte[] CMD_GET_HART_RONG = {33, 1, 0, 7, 32};
    public static final byte[] CMD_GET_LONGSIT = {6, 0, 0, 94};
    public static final byte[] CMD_GET_NOTICE = {9, 0, 0, 96};
    public static final byte[] CMD_GET_SLEEP = {8, 0, 0, 10};
    public static final byte[] CMD_GET_TARGE = {7, 0, 0, -76};
    public static final byte[] CMD_GET_USERINFO = {3, 0, 0, 92};
    public static final int DATA_LENTH = 2;
    public static byte[] END_MESSAGE = {10, 1, 0, 3, Ascii.f4513SO};

    public static byte[] getBind() {
        byte[] bArr = new byte[(CMD_BIND_DEVICE.length + 1)];
        int i = 0;
        while (true) {
            byte[] bArr2 = CMD_BIND_DEVICE;
            if (i < bArr2.length) {
                bArr[i] = bArr2[i];
                i++;
            } else {
                bArr[bArr.length - 1] = completeCheckCode(bArr);
                return bArr;
            }
        }
    }

    public static byte[] setHeartRange(int i, int i2, int i3) {
        byte[] bArr = new byte[8];
        bArr[0] = 33;
        bArr[1] = 4;
        bArr[2] = 0;
        bArr[3] = 3;
        bArr[4] = (byte) i;
        bArr[5] = (byte) i2;
        bArr[6] = (byte) i3;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setEcg(int i) {
        byte[] bArr;
        if (i == 0) {
            bArr = new byte[5];
            bArr[1] = 1;
        } else {
            byte[] bArr2 = new byte[7];
            bArr2[1] = 3;
            bArr2[4] = 60;
            bArr2[5] = 0;
            bArr = bArr2;
        }
        bArr[0] = 34;
        bArr[2] = 0;
        bArr[3] = (byte) i;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setHeartTest(int i, int i2) {
        byte[] bArr = new byte[8];
        bArr[0] = 33;
        bArr[1] = 4;
        bArr[2] = 0;
        bArr[3] = 4;
        bArr[4] = (byte) i;
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i2, 2);
        bArr[5] = IntToBinReverseArray[0];
        bArr[6] = IntToBinReverseArray[1];
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] clearHeartData() {
        byte[] bArr = new byte[5];
        bArr[0] = 33;
        bArr[1] = 1;
        bArr[2] = 0;
        bArr[3] = 2;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] clearStepData() {
        byte[] bArr = new byte[5];
        bArr[0] = 32;
        bArr[1] = 1;
        bArr[2] = 0;
        bArr[3] = 2;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] clearActivityData() {
        byte[] bArr = new byte[5];
        bArr[0] = 35;
        bArr[1] = 1;
        bArr[2] = 0;
        bArr[3] = -1;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] clearDeviceData(int i) {
        byte[] bArr = new byte[5];
        bArr[0] = 38;
        bArr[1] = 1;
        bArr[2] = 0;
        bArr[3] = (byte) i;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] getHistoryData(int i, int i2, int i3, int i4) {
        byte[] bArr = new byte[9];
        bArr[0] = 32;
        bArr[1] = 5;
        bArr[2] = 0;
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i2, 2);
        bArr[3] = (byte) i;
        bArr[4] = IntToBinReverseArray[0];
        bArr[5] = IntToBinReverseArray[1];
        bArr[6] = (byte) i3;
        bArr[7] = (byte) i4;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] getHistoryHeartRateData(int i, int i2, int i3, int i4) {
        byte[] bArr = new byte[9];
        bArr[0] = 33;
        bArr[1] = 5;
        bArr[2] = 0;
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i2, 2);
        bArr[3] = (byte) i;
        bArr[4] = IntToBinReverseArray[0];
        bArr[5] = IntToBinReverseArray[1];
        bArr[6] = (byte) i3;
        bArr[7] = (byte) i4;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] getHeartRate(int i) {
        byte[] bArr = new byte[5];
        bArr[0] = 33;
        bArr[1] = 1;
        bArr[2] = 0;
        bArr[3] = (byte) i;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] getUserInfo(int i, int i2, int i3, int i4, int i5) {
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i4 * 10, 2);
        byte[] IntToBinReverseArray2 = ByteDataConvertUtil.IntToBinReverseArray(i3, 2);
        byte[] bArr = {3, 7, 0, (byte) i, (byte) i2, IntToBinReverseArray2[0], IntToBinReverseArray2[1], IntToBinReverseArray[0], IntToBinReverseArray[1], (byte) i5};
        byte completeCheckCode = completeCheckCode(bArr);
        byte[] bArr2 = new byte[(bArr.length + 1)];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        bArr2[bArr2.length - 1] = completeCheckCode;
        return bArr2;
    }

    public static byte[] setDeviceState(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        byte[] bArr = {2, 16, 0, (byte) i, (byte) i2, (byte) i3, (byte) i4, (byte) i5, (byte) i6, (byte) i7, 0, (byte) i8, 0, 0, 0, 0, 0, 0, 0, 0};
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setLongSit(int i, int i2, int i3, int i4, int i5) {
        byte[] bArr = new byte[9];
        bArr[0] = 6;
        bArr[1] = 5;
        bArr[2] = 0;
        bArr[3] = (byte) i;
        bArr[4] = (byte) i2;
        bArr[5] = (byte) i3;
        bArr[6] = (byte) i4;
        bArr[7] = (byte) i5;
        bArr[8] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setNotice(AppNotice appNotice) {
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        byte[] bArr = new byte[8];
        bArr[0] = 9;
        bArr[1] = 4;
        bArr[2] = 0;
        byte[] bArr2 = new byte[8];
        if (appNotice.twitter) {
            bArr2[7] = 1;
        } else {
            bArr2[7] = 0;
        }
        if (appNotice.facebook) {
            bArr2[6] = 1;
        } else {
            bArr2[6] = 0;
        }
        if (appNotice.facebook) {
            bArr2[5] = 1;
        } else {
            bArr2[5] = 0;
        }
        if (deviceConfig.isMessage) {
            bArr2[4] = 1;
        } else {
            bArr2[4] = 0;
        }
        if (deviceConfig.isCall) {
            bArr2[3] = 1;
        } else {
            bArr2[3] = 0;
        }
        if (appNotice.whatsApp) {
            bArr2[2] = 1;
        } else {
            bArr2[2] = 0;
        }
        if (appNotice.line) {
            bArr2[1] = 1;
        } else {
            bArr2[1] = 0;
        }
        if (appNotice.skype) {
            bArr2[0] = 1;
        } else {
            bArr2[0] = 0;
        }
        byte[] bArr3 = new byte[8];
        if (appNotice.f5221qq) {
            bArr3[7] = 1;
        } else {
            bArr3[7] = 0;
        }
        if (appNotice.wechat) {
            bArr3[6] = 1;
        } else {
            bArr3[6] = 0;
        }
        if (appNotice.instagram) {
            bArr3[5] = 1;
        } else {
            bArr3[5] = 0;
        }
        if (appNotice.linked) {
            bArr3[4] = 1;
        } else {
            bArr3[4] = 0;
        }
        if (appNotice.messager) {
            bArr3[3] = 1;
        } else {
            bArr3[3] = 0;
        }
        if (appNotice.f5222vk) {
            bArr3[2] = 1;
        } else {
            bArr3[2] = 0;
        }
        bArr3[1] = 0;
        bArr3[0] = 0;
        bArr[3] = (byte) ByteDataConvertUtil.Bit8Array2Int(bArr2);
        bArr[4] = (byte) ByteDataConvertUtil.Bit8Array2Int(bArr3);
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setWeather(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        byte[] bArr = new byte[15];
        bArr[0] = 37;
        bArr[1] = 11;
        bArr[2] = 0;
        bArr[3] = (byte) i;
        bArr[4] = (byte) i2;
        bArr[5] = (byte) i3;
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i4, 2);
        for (int i9 = 0; i9 < IntToBinReverseArray.length; i9++) {
            bArr[i9 + 6] = IntToBinReverseArray[i9];
        }
        bArr[8] = (byte) i5;
        byte[] IntToBinReverseArray2 = ByteDataConvertUtil.IntToBinReverseArray(i6, 2);
        for (int i10 = 0; i10 < IntToBinReverseArray2.length; i10++) {
            bArr[i10 + 9] = IntToBinReverseArray2[i10];
        }
        bArr[11] = (byte) i7;
        byte[] IntToBinReverseArray3 = ByteDataConvertUtil.IntToBinReverseArray(i8, 2);
        for (int i11 = 0; i11 < IntToBinReverseArray3.length; i11++) {
            bArr[i11 + 12] = IntToBinReverseArray3[i11];
        }
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setWeather(WeatherBean weatherBean) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (weatherBean.weatherBeans != null) {
            if (weatherBean.weatherBeans.size() > 0) {
                i6 = weatherBean.weatherBeans.get(0).weatherType;
                i5 = weatherBean.weatherBeans.get(0).currentTemp;
            } else {
                i6 = 0;
                i5 = 0;
            }
            if (weatherBean.weatherBeans.size() > 1) {
                i4 = i6;
                i2 = weatherBean.weatherBeans.get(1).weatherType;
                i3 = i5;
                i = weatherBean.weatherBeans.get(1).currentTemp;
                return setWeather(weatherBean.state, weatherBean.tempUnit, weatherBean.weatherType, weatherBean.currentTemp, i4, i3, i2, i);
            }
            i4 = i6;
            i3 = i5;
        } else {
            i4 = 0;
            i3 = 0;
        }
        i2 = 0;
        i = 0;
        return setWeather(weatherBean.state, weatherBean.tempUnit, weatherBean.weatherType, weatherBean.currentTemp, i4, i3, i2, i);
    }

    public static byte[] setSleepTime(List<SleepTime> list, int i) {
        byte[] bArr = new byte[20];
        bArr[0] = 8;
        bArr[1] = 16;
        bArr[2] = 0;
        for (int i2 = 0; i2 < list.size(); i2++) {
            SleepTime sleepTime = list.get(i2);
            int i3 = i2 * 5;
            bArr[i3 + 3] = (byte) sleepTime.state;
            bArr[i3 + 4] = (byte) sleepTime.startH;
            bArr[i3 + 5] = (byte) sleepTime.startM;
            bArr[i3 + 6] = (byte) sleepTime.endH;
            bArr[i3 + 7] = (byte) sleepTime.endM;
        }
        bArr[bArr.length - 2] = (byte) i;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setTarget(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        byte[] bArr = new byte[18];
        bArr[0] = 7;
        bArr[1] = Ascii.f4513SO;
        bArr[2] = 0;
        bArr[3] = (byte) i;
        bArr[4] = (byte) i2;
        bArr[5] = (byte) i3;
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i4, 3);
        for (int i9 = 0; i9 < IntToBinReverseArray.length; i9++) {
            bArr[i9 + 6] = IntToBinReverseArray[i9];
        }
        bArr[9] = (byte) i5;
        byte[] IntToBinReverseArray2 = ByteDataConvertUtil.IntToBinReverseArray(i6, 3);
        for (int i10 = 0; i10 < IntToBinReverseArray2.length; i10++) {
            bArr[i10 + 10] = IntToBinReverseArray2[i10];
        }
        bArr[13] = (byte) i7;
        byte[] IntToBinReverseArray3 = ByteDataConvertUtil.IntToBinReverseArray(i8, 3);
        for (int i11 = 0; i11 < IntToBinReverseArray3.length; i11++) {
            bArr[i11 + 14] = IntToBinReverseArray3[i11];
        }
        bArr[17] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] getTime() {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(1);
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i, 2);
        int i2 = instance.get(2) + 1;
        int i3 = instance.get(5);
        int i4 = instance.get(11);
        int i5 = instance.get(12);
        int i6 = instance.get(13);
        LogUtil.d("year:" + i + ",month:" + i2 + ",day:" + i3 + ",hour:" + i4 + ",min:" + i5 + ",sencond:" + i6);
        byte[] bArr = {4, 8, 0, IntToBinReverseArray[0], IntToBinReverseArray[1], (byte) i2, (byte) i3, (byte) i4, (byte) i5, (byte) i6, 0};
        byte completeCheckCode = completeCheckCode(bArr);
        byte[] bArr2 = new byte[(bArr.length + 1)];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        bArr2[bArr2.length - 1] = completeCheckCode;
        return bArr2;
    }

    public static byte completeCheckCode(byte[] bArr) {
        byte b = 0;
        for (byte b2 : bArr) {
            b = (byte) (b + b2);
        }
        return (byte) ((b * 86) + 90);
    }

    public static byte[] completeCmd(byte[] bArr) {
        byte b = 0;
        for (int i = 0; i < bArr.length - 1; i++) {
            b = (byte) (b + bArr[i]);
        }
        bArr[bArr.length - 1] = (byte) ((b * 86) + 90);
        return bArr;
    }

    public static byte[] getCmd(byte b, byte[] bArr, byte[] bArr2) {
        LogUtil.d("命令:" + Integer.toHexString(b));
        LogUtil.d("长度:" + ByteDataConvertUtil.bytesToHexString(bArr));
        byte[] bArr3 = new byte[(bArr[0] + bArr[1] + 1 + 2 + 1)];
        bArr3[0] = b;
        int i = 0;
        while (i < bArr.length) {
            int i2 = i + 1;
            bArr3[i2] = bArr[i];
            i = i2;
        }
        if (bArr2 != null) {
            for (int i3 = 0; i3 < bArr2.length; i3++) {
                bArr3[i3 + 3] = bArr2[i3];
            }
        }
        bArr3[bArr3.length - 1] = completeCheckCode(Arrays.copyOf(bArr3, bArr3.length - 1));
        LogUtil.d("要发送的数据:" + ByteDataConvertUtil.bytesToHexString(bArr3));
        return bArr3;
    }

    public static byte[] getCmd(byte b, byte[] bArr) {
        return getCmd(b, bArr, null);
    }

    public static byte[] getCmd(byte b) {
        return getCmd(b, new byte[]{0, 0}, null);
    }

    public static byte[] enterUpdate() {
        return new byte[]{11, 0, 0, Ascii.f4505FF};
    }

    public static byte[] answerRingingCall() {
        return new byte[]{-112, 8, 0, 1, 0, 0, 0, 0, 0, 0, 0, -64};
    }

    public static byte[] endRingingCall() {
        return new byte[]{-112, 8, 0, 2, 0, 0, 0, 0, 0, 0, 0, Ascii.SYN};
    }

    public static byte[] sos() {
        return new byte[]{-112, 8, 0, 0, 1, 0, 0, 0, 0, 0, 0, -64};
    }

    public static byte[] enterCamare() {
        return new byte[]{-112, 8, 0, 0, 0, 1, 0, 0, 0, 0, 0, -64};
    }

    public static byte[] camare() {
        return new byte[]{-112, 8, 0, 0, 0, 2, 0, 0, 0, 0, 0, Ascii.SYN};
    }

    public static byte[] exitCamare() {
        return new byte[]{-112, 8, 0, 0, 0, 3, 0, 0, 0, 0, 0, 108};
    }

    public static byte[] musicControl() {
        return new byte[]{-112, 8, 0, 0, 0, 0, 1, 0, 0, 0, 0, -64};
    }

    public static byte[] preMusic() {
        return new byte[]{-112, 8, 0, 0, 0, 0, 2, 0, 0, 0, 0, Ascii.SYN};
    }

    public static byte[] nextMusic() {
        return new byte[]{-112, 8, 0, 0, 0, 0, 3, 0, 0, 0, 0, 108};
    }

    public static byte[] exitMusic() {
        return new byte[]{-112, 8, 0, 0, 0, 0, 4, 0, 0, 0, 0, -62};
    }

    public static byte[] voiceAdd() {
        return new byte[]{-112, 8, 0, 0, 0, 0, 5, 0, 0, 0, 0, -62};
    }

    public static byte[] voiceSub() {
        return new byte[]{-112, 8, 0, 0, 0, 0, 6, 0, 0, 0, 0, -62};
    }

    public static byte[] findPhone() {
        return new byte[]{-112, 8, 0, 0, 0, 0, 0, 1, 0, 0, 0, -64};
    }

    public static byte[] findDevice() {
        return new byte[]{16, 8, 0, 0, 0, 0, 0, 1, 0, 0, 0, -64};
    }

    public static byte[] controlDevicePhone(int i) {
        return controlDevice(0, i);
    }

    public static byte[] controlDeviceSos(int i) {
        return controlDevice(1, i);
    }

    public static byte[] controlDeviceCall(int i) {
        return controlDevice(0, i);
    }

    public static byte[] controlDeviceCamare(int i) {
        return controlDevice(2, i);
    }

    public static byte[] controlMusic(int i) {
        return controlDevice(3, i);
    }

    public static byte[] controlDeviceFindPhone(int i) {
        return controlDevice(4, i);
    }

    public static byte[] controlDeviceQuickResponse(int i) {
        return controlDevice(5, i);
    }

    public static byte[] controlDeviceCall2(int i) {
        return controlDevice(6, i);
    }

    public static byte[] controlDeviceGps(int i) {
        return controlDevice(7, i);
    }

    private static byte[] controlDevice(int i, int i2) {
        byte[] bArr = new byte[12];
        bArr[0] = 16;
        bArr[1] = 8;
        bArr[2] = 0;
        bArr[i + 3] = (byte) i2;
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setLongSit(LongSitBean longSitBean) {
        byte[] bArr = new byte[9];
        bArr[0] = 6;
        bArr[1] = 5;
        bArr[2] = 0;
        bArr[3] = (byte) longSitBean.state;
        bArr[4] = (byte) longSitBean.startTime;
        bArr[5] = (byte) longSitBean.endTime;
        bArr[6] = (byte) longSitBean.repeat;
        bArr[7] = (byte) (longSitBean.period / 5);
        bArr[8] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setDeviceData() {
        Calendar instance = Calendar.getInstance();
        byte[] bArr = new byte[12];
        bArr[0] = 4;
        bArr[1] = 8;
        bArr[2] = 0;
        bArr[3] = ByteDataConvertUtil.IntToBinReverseArray(instance.get(1), 2)[0];
        bArr[4] = ByteDataConvertUtil.IntToBinReverseArray(instance.get(1), 2)[1];
        bArr[5] = ByteDataConvertUtil.Int2Byte(instance.get(2) + 1);
        bArr[6] = ByteDataConvertUtil.Int2Byte(instance.get(5));
        bArr[7] = ByteDataConvertUtil.Int2Byte(instance.get(11));
        bArr[8] = ByteDataConvertUtil.Int2Byte(instance.get(12));
        bArr[9] = ByteDataConvertUtil.Int2Byte(instance.get(13));
        bArr[10] = 0;
        bArr[11] = completeCheckCode(bArr);
        return bArr;
    }

    public static byte[] setMessageType(int i) {
        byte[] bArr = {10, 2, 0, 0, (byte) i, 0};
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return bArr;
    }

    public static List<byte[]> setAlarm2(List<AlarmBean> list) {
        int size = (list.size() * 4) + 4;
        if (size < 36) {
            size = 36;
        }
        byte[] bArr = new byte[size];
        bArr[0] = 5;
        bArr[1] = 32;
        bArr[2] = 0;
        for (int i = 0; i < list.size(); i++) {
            AlarmBean alarmBean = list.get(i);
            byte[] bArr2 = new byte[alarmBean.weeks.length];
            for (int i2 = 0; i2 < alarmBean.weeks.length; i2++) {
                bArr2[i2] = alarmBean.weeks[(alarmBean.weeks.length - 1) - i2];
            }
            int i3 = i * 4;
            bArr[i3 + 3] = (byte) ByteDataConvertUtil.Bit8Array2Int(bArr2);
            bArr[i3 + 4] = (byte) alarmBean.hour;
            bArr[i3 + 5] = (byte) alarmBean.min;
            bArr[i3 + 6] = (byte) alarmBean.type;
        }
        for (int i4 = size - 2; i4 >= (list.size() * 4) + 3; i4--) {
            bArr[i4] = -1;
        }
        bArr[bArr.length - 1] = completeCheckCode(bArr);
        return spliteData(bArr);
    }

    public static byte[] setMessage(int i, String str) {
        int i2;
        byte[] bArr = null;
        try {
            bArr = str.getBytes("UTF-8");
            i2 = bArr.length;
        } catch (Exception e) {
            e.printStackTrace();
            i2 = 0;
        }
        if (bArr == null) {
            bArr = new byte[0];
        }
        byte[] bArr2 = new byte[(i2 + 5)];
        bArr2[0] = 10;
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i2 + 1, 2);
        bArr2[1] = IntToBinReverseArray[0];
        bArr2[2] = IntToBinReverseArray[1];
        bArr2[3] = (byte) i;
        for (int i3 = 0; i3 < bArr.length; i3++) {
            bArr2[i3 + 4] = bArr[i3];
        }
        bArr2[bArr2.length - 1] = completeCheckCode(bArr2);
        return bArr2;
    }

    public static List<byte[]> setMessage2(int i, String str) {
        int i2;
        byte[] bArr = null;
        try {
            bArr = str.getBytes("UTF-8");
            LogUtil.d(ByteDataConvertUtil.bytesToHexString(bArr));
            LogUtil.d(bArr.length + "");
            i2 = bArr.length;
        } catch (Exception e) {
            e.printStackTrace();
            i2 = 0;
        }
        if (bArr == null) {
            bArr = new byte[0];
        }
        if (i2 > 128) {
            i2 = 128;
        }
        byte[] bArr2 = new byte[(i2 + 5)];
        bArr2[0] = 10;
        byte[] IntToBinReverseArray = ByteDataConvertUtil.IntToBinReverseArray(i2 + 1, 2);
        bArr2[1] = IntToBinReverseArray[0];
        bArr2[2] = IntToBinReverseArray[1];
        bArr2[3] = (byte) i;
        for (int i3 = 0; i3 < i2; i3++) {
            bArr2[i3 + 4] = bArr[i3];
        }
        bArr2[bArr2.length - 1] = completeCheckCode(bArr2);
        return spliteData(bArr2);
    }

    public static List<byte[]> spliteData(byte[] bArr) {
        byte[] bArr2;
        ArrayList arrayList = new ArrayList();
        int length = bArr.length / 20;
        LogUtil.d(ByteDataConvertUtil.bytesToHexString(bArr) + ",model:" + length);
        if (bArr.length <= 20) {
            arrayList.add(bArr);
        } else {
            if (bArr.length % 20 > 0) {
                length++;
            }
            for (int i = 0; i < length; i++) {
                int i2 = i * 20;
                int i3 = i2 + 20;
                if (i3 >= bArr.length) {
                    bArr2 = Arrays.copyOfRange(bArr, i2, bArr.length);
                } else {
                    bArr2 = Arrays.copyOfRange(bArr, i2, i3);
                }
                LogUtil.d(ByteDataConvertUtil.bytesToHexString(bArr2));
                arrayList.add(bArr2);
            }
        }
        return arrayList;
    }
}
