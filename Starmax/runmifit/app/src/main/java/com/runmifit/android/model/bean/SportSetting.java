package com.wade.fit.model.bean;

import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.util.SharePreferenceUtils;
import java.io.Serializable;

public class SportSetting implements Serializable {
    public int mapSource = 0;
    public String tipValueUnit = "";
    public int tipsState = 0;
    public int tipsType = 0;
    public float tipsValue = -1.0f;

    public static SportSetting getInstance() {
        SportSetting sportSetting = (SportSetting) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.SPORT_SETTING, null);
        return sportSetting == null ? new SportSetting() : sportSetting;
    }

    public int getMapSource() {
        return this.mapSource;
    }

    public void setMapSource(int i) {
        this.mapSource = i;
    }

    public int getTipsState() {
        return this.tipsState;
    }

    public void setTipsState(int i) {
        this.tipsState = i;
    }

    public int getTipsType() {
        return this.tipsType;
    }

    public void setTipsType(int i) {
        this.tipsType = i;
    }

    public float getTipsValue() {
        return this.tipsValue;
    }

    public void setTipsValue(int i) {
        this.tipsValue = (float) i;
    }

    public String getUnit() {
        if (this.tipsType == 0) {
            return AppApplication.getInstance().getResources().getString(R.string.unit_minute);
        }
        return AppApplication.getInstance().getResources().getString(R.string.unit_kilometer);
    }

    public String toString() {
        return "SportSetting{mapSource=" + this.mapSource + ", tipsState=" + this.tipsState + ", tipsType=" + this.tipsType + ", tipsValue=" + this.tipsValue + '}';
    }
}
