package com.wade.fit.views;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ViewUtil;
import com.wade.fit.util.log.DebugLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HeartBarChart extends View {
    private float MAX_VALUE = 225.0f;
    private float MIN_VALUE = 0.0f;
    private int bgColor;
    private int bottom = 30;
    private int chartHeight;
    private float chartHeightSpan;
    private int chartWidth;
    private float chartWidthSpan;
    private TouchOrClickListener clickListener;
    private int[] colors = {-12933088, -11030811, -274872, -758772, -1298164, -293088};
    private int exelColor = 1006632959;
    private Paint exelPaint;

    /* renamed from: h */
    private int f5234h;
    private boolean haveRecordLeftTouchX = false;
    private List<HealthHeartRateItem> healthHeartRateItems;
    private Paint hrPaint;
    private int index = -1;
    private HealthHeartRate interval;
    private boolean isOnclick = true;
    private List<HealthHeartRateItem> items;
    private int left = 30;
    private float leftTouchX = 0.0f;
    Paint linePaint = new Paint();
    private List<HealthHeartRateItem> points = new ArrayList();
    private NinePatchDrawable popup;
    private NinePatchDrawable popupCenter;
    private NinePatchDrawable popupLeft;
    private NinePatchDrawable popupRight;
    private int right = 20;
    private float[] scaleY = {this.MIN_VALUE, 60.0f, 120.0f, 180.0f, this.MAX_VALUE};
    private Rect selectRect;
    private int startTime;
    private float surplusHeight;
    private List<HealthHeartRateItem> tempPoints = new ArrayList();
    float tempTouchX = -1.0f;
    private boolean tempTouchX1b = true;
    private float tempTouchX2 = 0.0f;
    float tempTouchY = -1.0f;
    private float tempX1 = 0.0f;
    float tempXx;
    private float tempY1 = 0.0f;
    private int textColor;
    private float textSize;
    private int top = 30;
    private Paint touchPaint;
    float touchX = -1.0f;
    private int touchX1 = 0;
    private int touchX2 = 0;
    float touchY = -1.0f;

    /* renamed from: w */
    private int f5235w;
    List<String> xLables;
    private Paint xPaint;
    private Paint yPaint;

    public interface TouchOrClickListener {
        void doClick();
    }

    public boolean isOnclick() {
        return this.isOnclick;
    }

    public void setIsOnclick(boolean z) {
        this.isOnclick = z;
    }

    public HeartBarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.HeartBarChart);
        this.textSize = obtainStyledAttributes.getDimension(2, context.getResources().getDimension(R.dimen.y25));
        this.textColor = obtainStyledAttributes.getColor(1, -1);
        obtainStyledAttributes.recycle();
        this.popupLeft = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup_left);
        this.popupRight = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup_right);
        this.popupCenter = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup);
        this.selectRect = new Rect();
        ScreenUtil.initScreen((Activity) context);
        initPaint();
    }

    private void initPaint() {
        this.exelPaint = new Paint(1);
        this.xPaint = new Paint(1);
        this.yPaint = new Paint(1);
        this.hrPaint = new Paint(1);
        this.touchPaint = new Paint(1);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5235w = i;
        this.f5234h = i2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.chartWidth = (this.f5235w - ScreenUtil.dp2px((float) this.right)) - ScreenUtil.dp2px((float) this.left);
        this.chartHeight = (this.f5234h - ScreenUtil.dp2px((float) this.bottom)) - ScreenUtil.dp2px((float) this.top);
        this.chartWidthSpan = ((float) this.chartWidth) / 1440.0f;
        this.chartHeightSpan = ((float) this.chartHeight) / this.MAX_VALUE;
        if (this.points.size() > 0) {
            this.surplusHeight = ((float) this.points.get(0).getOffsetMinute()) * this.chartWidthSpan;
        } else {
            this.surplusHeight = 0.0f;
        }
        drawExelBackground(canvas);
        drawX(canvas);
        drawValueAndDottedLine(canvas);
        drawY(canvas);
        drawHeartRateValue(canvas);
        drawTouch(canvas);
    }

    private void drawTouch(Canvas canvas) {
        int i;
        int i2;
        int offsetMinute;
        int i3;
        Canvas canvas2 = canvas;
        float f = ((float) this.chartHeight) / this.MAX_VALUE;
        if (this.points.size() == 1) {
            this.touchX = minutToX(this.points.get(0).getOffsetMinute(), this.chartWidth);
            this.touchY = (float) this.points.get(0).getHeartRaveValue();
            i = 0;
        } else if (this.points.size() > 1) {
            float f2 = this.touchX;
            if (f2 != -1.0f) {
                int xToMinute = xToMinute(f2, this.chartWidth);
                if (xToMinute < 0) {
                    offsetMinute = this.points.get(0).getOffsetMinute();
                    this.touchY = (float) this.points.get(0).getHeartRaveValue();
                } else if (this.points.get(0).getOffsetMinute() >= xToMinute) {
                    offsetMinute = this.points.get(0).getOffsetMinute();
                    this.touchY = (float) this.points.get(0).getHeartRaveValue();
                } else {
                    List<HealthHeartRateItem> list = this.points;
                    if (list.get(list.size() - 1).getOffsetMinute() <= xToMinute) {
                        List<HealthHeartRateItem> list2 = this.points;
                        xToMinute = list2.get(list2.size() - 1).getOffsetMinute();
                        List<HealthHeartRateItem> list3 = this.points;
                        this.touchY = (float) list3.get(list3.size() - 1).getHeartRaveValue();
                        i2 = this.points.size() - 1;
                    } else {
                        int i4 = 0;
                        while (true) {
                            if (i4 >= this.points.size() - 1) {
                                i3 = -1;
                                break;
                            }
                            if (this.points.get(i4).getOffsetMinute() <= xToMinute) {
                                i3 = i4 + 1;
                                if (this.points.get(i3).getOffsetMinute() >= xToMinute) {
                                    if (Math.abs(this.points.get(i4).getOffsetMinute() - xToMinute) < Math.abs(this.points.get(i3).getOffsetMinute() - xToMinute)) {
                                        xToMinute = this.points.get(i4).getOffsetMinute();
                                        this.touchY = (float) this.points.get(i4).getHeartRaveValue();
                                        i3 = i4;
                                    } else {
                                        xToMinute = this.points.get(i3).getOffsetMinute();
                                        this.touchY = (float) this.points.get(i3).getHeartRaveValue();
                                    }
                                    if (this.points.get(i4).getHeartRaveValue() > 0) {
                                        this.tempTouchY = this.touchY;
                                        this.tempXx = (float) xToMinute;
                                    }
                                }
                            }
                            i4++;
                        }
                        i2 = i3;
                    }
                    this.touchX = minutToX(xToMinute, this.chartWidth);
                    i = i2;
                }
                i2 = 0;
                this.touchX = minutToX(xToMinute, this.chartWidth);
                i = i2;
            } else {
                List<HealthHeartRateItem> list4 = this.points;
                this.touchX = minutToX(list4.get(list4.size() - 1).getOffsetMinute(), this.chartWidth);
                List<HealthHeartRateItem> list5 = this.points;
                this.touchY = (float) list5.get(list5.size() - 1).getHeartRaveValue();
                i = this.points.size() - 1;
            }
        } else {
            i = -1;
        }
        if (this.points.size() >= 1) {
            float f3 = this.touchX;
            if (f3 < this.leftTouchX) {
                if (this.points.get(i).getHeartRaveValue() > 0) {
                    float f4 = this.leftTouchX;
                    this.touchX = f4;
                    this.tempTouchX = f4;
                } else {
                    this.touchX = this.tempTouchX;
                }
            } else if (f3 > this.tempTouchX2) {
                this.points.get(i).getHeartRaveValue();
            }
            if (this.touchY <= 0.0f) {
                Iterator<HealthHeartRateItem> it = this.points.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    HealthHeartRateItem next = it.next();
                    if (next.getHeartRaveValue() > 0) {
                        this.touchY = (float) next.getHeartRaveValue();
                        break;
                    }
                }
            }
            this.touchPaint.setColor(-1);
            this.touchPaint.setStrokeWidth(1.0f);
            int height = getHeight() - ScreenUtil.dp2px((float) this.bottom);
            if (this.touchY > 0.0f) {
                float f5 = this.touchX;
                canvas.drawLine(f5, (float) height, f5, (float) (ScreenUtil.dp2px((float) this.top) / 2), this.touchPaint);
            }
            Paint paint = new Paint();
            paint.setColor(-1);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            paint.setTextSize(20.0f);
            String str = ((int) this.touchY) + getContext().getString(R.string.bmp);
            DebugLog.m6203d("onDraw...........position:" + i);
            HealthHeartRateItem healthHeartRateItem = this.points.get(i);
            String str2 = String.format("%02d", Integer.valueOf(healthHeartRateItem.getOffsetMinute() / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf(healthHeartRateItem.getOffsetMinute() % 60));
            if (healthHeartRateItem.getHeartRaveValue() <= 0) {
                Iterator<HealthHeartRateItem> it2 = this.points.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    HealthHeartRateItem next2 = it2.next();
                    if (next2.getHeartRaveValue() > 0) {
                        str2 = String.format("%02d", Integer.valueOf(next2.getOffsetMinute() / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf(next2.getOffsetMinute() % 60));
                        break;
                    }
                }
            } else {
                str2 = String.format("%02d", Integer.valueOf(healthHeartRateItem.getOffsetMinute() / 60)) + Config.TRACE_TODAY_VISIT_SPLIT + String.format("%02d", Integer.valueOf(healthHeartRateItem.getOffsetMinute() % 60));
            }
            float measureText = paint.measureText(str);
            float measureText2 = paint.measureText(str2);
            float dp2px = this.touchX + ((float) ScreenUtil.dp2px(5.0f));
            float dp2px2 = this.touchX + ((float) ScreenUtil.dp2px(5.0f));
            this.popup = this.popupLeft;
            if (this.touchX + ((float) ScreenUtil.dp2px(10.0f)) + measureText > ((float) getWidth())) {
                dp2px = (this.touchX - ((float) ScreenUtil.dp2px(5.0f))) - measureText;
                dp2px2 = (this.touchX - ((float) ScreenUtil.dp2px(5.0f))) - measureText2;
                this.popup = this.popupRight;
            }
            if (this.touchX + ((float) ScreenUtil.dp2px(10.0f)) + measureText2 > ((float) getWidth())) {
                dp2px = (this.touchX - ((float) ScreenUtil.dp2px(5.0f))) - measureText;
                dp2px2 = (this.touchX - ((float) ScreenUtil.dp2px(5.0f))) - measureText2;
                this.popup = this.popupRight;
            }
            float f6 = this.touchY;
            if (f6 > 0.0f) {
                if (this.popup == this.popupRight) {
                    float f7 = (float) height;
                    this.selectRect.bottom = (int) (((((f7 - ((f6 - this.scaleY[0]) * f)) + ((float) ScreenUtil.dp2px(11.0f))) + ((float) ScreenUtil.getTextHeight(paint, str))) + ((float) ScreenUtil.dp2px(2.0f))) - ((float) ScreenUtil.dp2px(17.0f)));
                    this.selectRect.top = (int) ((((f7 - ((this.touchY - this.scaleY[0]) * f)) - ((float) ScreenUtil.getTextHeight(paint, str))) - ((float) ScreenUtil.dp2px(2.0f))) - ((float) ScreenUtil.dp2px(17.0f)));
                    this.selectRect.left = ((int) dp2px) - ScreenUtil.dp2px(2.0f);
                    this.selectRect.right = (int) (measureText + dp2px + ((float) ScreenUtil.dp2px(2.0f)));
                } else {
                    float f8 = (float) height;
                    this.selectRect.bottom = (int) (((((f8 - ((f6 - this.scaleY[0]) * f)) + ((float) ScreenUtil.dp2px(11.0f))) + ((float) ScreenUtil.getTextHeight(paint, str))) + ((float) ScreenUtil.dp2px(2.0f))) - ((float) ScreenUtil.dp2px(15.0f)));
                    this.selectRect.top = (int) ((((f8 - ((this.touchY - this.scaleY[0]) * f)) - ((float) ScreenUtil.getTextHeight(paint, str))) - ((float) ScreenUtil.dp2px(2.0f))) - ((float) ScreenUtil.dp2px(15.0f)));
                    this.selectRect.left = ((int) dp2px2) - ScreenUtil.dp2px(2.0f);
                    this.selectRect.right = (int) (measureText + dp2px2 + ((float) ScreenUtil.dp2px(2.0f)));
                }
                this.popup.setBounds(this.selectRect);
                this.popup.draw(canvas2);
                float f9 = (float) height;
                canvas2.drawText(str2, dp2px2, ((f9 - ((this.touchY - this.scaleY[0]) * f)) + ((float) ScreenUtil.dp2px(11.0f))) - ((float) ScreenUtil.dp2px(17.0f)), paint);
                canvas2.drawCircle(this.touchX, f9 - ((this.touchY - this.scaleY[0]) * f), 5.0f, paint);
                canvas2.drawText(str, dp2px, (f9 - ((this.touchY - this.scaleY[0]) * f)) - ((float) ScreenUtil.dp2px(17.0f)), paint);
            }
        }
    }

    private void drawHeartRateValue(Canvas canvas) {
        this.hrPaint.setStrokeWidth(4.0f);
        List<HealthHeartRateItem> list = this.points;
        if (list != null && list.size() > 0) {
            float dp2px = (float) (ScreenUtil.dp2px((float) this.left) + ScreenUtil.dp2px(1.0f));
            float dp2px2 = (float) (this.f5234h - ScreenUtil.dp2px((float) this.bottom));
            for (int i = 0; i < this.points.size(); i++) {
                if (i != 0) {
                    float f = this.tempX1;
                    float f2 = this.tempY1;
                    float f3 = 0.0f;
                    if (f2 == 0.0f) {
                        int i2 = i - 1;
                        f = (float) this.points.get(i2).getOffsetMinute();
                        f2 = (float) this.points.get(i2).getHeartRaveValue();
                    }
                    float offsetMinute = (float) this.points.get(i).getOffsetMinute();
                    float heartRaveValue = (float) this.points.get(i).getHeartRaveValue();
                    if (f2 != 0.0f && heartRaveValue != 0.0f) {
                        this.tempY1 = 0.0f;
                        this.tempX1 = 0.0f;
                        float f4 = f;
                        int i3 = 1;
                        while (true) {
                            float[] fArr = this.scaleY;
                            if (i3 >= fArr.length) {
                                break;
                            }
                            if (f2 > fArr[i3] || heartRaveValue < fArr[i3]) {
                                float[] fArr2 = this.scaleY;
                                if (f2 <= fArr2[i3] || heartRaveValue >= fArr2[i3]) {
                                    float[] fArr3 = this.scaleY;
                                    if (f2 <= fArr3[i3] && heartRaveValue <= fArr3[i3]) {
                                        int i4 = i3 - 1;
                                        if (f2 >= fArr3[i4] && heartRaveValue >= fArr3[i4]) {
                                            this.hrPaint.setColor(this.colors[i3]);
                                            float f5 = this.chartWidthSpan;
                                            float f6 = dp2px + (f4 * f5);
                                            float[] fArr4 = this.scaleY;
                                            float f7 = this.chartHeightSpan;
                                            float f8 = dp2px2 - ((f2 - fArr4[0]) * f7);
                                            float f9 = (f5 * offsetMinute) + dp2px;
                                            float f10 = dp2px2 - ((heartRaveValue - fArr4[0]) * f7);
                                            if (heartRaveValue != 0.0f) {
                                                this.tempTouchX2 = f9;
                                            } else if (f2 != 0.0f) {
                                                this.tempTouchX2 = f6;
                                            }
                                            if (!this.haveRecordLeftTouchX) {
                                                if (this.points.get(i - 1).getHeartRaveValue() != 0) {
                                                    this.leftTouchX = f6;
                                                    this.haveRecordLeftTouchX = true;
                                                } else if (this.points.get(i).getHeartRaveValue() != 0) {
                                                    this.leftTouchX = f6;
                                                    this.haveRecordLeftTouchX = true;
                                                }
                                            }
                                            canvas.drawLine(f6, f8, f9, f10, this.hrPaint);
                                            i3++;
                                            f3 = 0.0f;
                                        }
                                    }
                                } else {
                                    float f11 = fArr2[i3];
                                    float f12 = (((f2 - f11) * (offsetMinute - f4)) / (f2 - heartRaveValue)) + f4;
                                    this.hrPaint.setColor(this.colors[i3]);
                                    float f13 = this.chartWidthSpan;
                                    float f14 = dp2px + (offsetMinute * f13);
                                    float[] fArr5 = this.scaleY;
                                    float f15 = this.chartHeightSpan;
                                    float f16 = dp2px2 - ((heartRaveValue - fArr5[0]) * f15);
                                    float f17 = (f13 * f12) + dp2px;
                                    float f18 = dp2px2 - ((f11 - fArr5[0]) * f15);
                                    if (heartRaveValue != 0.0f) {
                                        this.tempTouchX2 = f17;
                                    } else if (f2 != 0.0f) {
                                        this.tempTouchX2 = f14;
                                    }
                                    if (!this.haveRecordLeftTouchX) {
                                        if (this.points.get(i - 1).getHeartRaveValue() != 0) {
                                            this.leftTouchX = f14;
                                            this.haveRecordLeftTouchX = true;
                                        } else if (this.points.get(i).getHeartRaveValue() != 0) {
                                            this.leftTouchX = f14;
                                            this.haveRecordLeftTouchX = true;
                                        }
                                    }
                                    canvas.drawLine(f14, f16, f17, f18, this.hrPaint);
                                    heartRaveValue = f11;
                                    offsetMinute = f12;
                                }
                            } else {
                                float f19 = fArr[i3];
                                float f20 = (((f19 - f2) * (offsetMinute - f4)) / (heartRaveValue - f2)) + f4;
                                this.hrPaint.setColor(this.colors[i3]);
                                float f21 = this.chartWidthSpan;
                                float f22 = (f4 * f21) + dp2px;
                                float[] fArr6 = this.scaleY;
                                float f23 = this.chartHeightSpan;
                                float f24 = dp2px2 - ((f2 - fArr6[0]) * f23);
                                float f25 = (f21 * f20) + dp2px;
                                float f26 = dp2px2 - ((f19 - fArr6[0]) * f23);
                                if (heartRaveValue != f3) {
                                    this.tempTouchX2 = f25;
                                } else if (f2 != f3) {
                                    this.tempTouchX2 = f22;
                                }
                                if (!this.haveRecordLeftTouchX) {
                                    if (this.points.get(i - 1).getHeartRaveValue() != 0) {
                                        this.tempTouchX = f22;
                                        this.leftTouchX = f22;
                                        this.haveRecordLeftTouchX = true;
                                    } else if (this.points.get(i).getHeartRaveValue() != 0) {
                                        this.tempTouchX = f22;
                                        this.leftTouchX = f22;
                                        this.haveRecordLeftTouchX = true;
                                    }
                                }
                                canvas.drawLine(f22, f24, f25, f26, this.hrPaint);
                                f2 = f19;
                                f4 = f20;
                            }
                            i3++;
                            f3 = 0.0f;
                        }
                    } else if (f2 != 0.0f) {
                        this.tempY1 = f2;
                        this.tempX1 = f;
                        if (this.tempTouchX1b) {
                            this.index = i - 1;
                            this.tempTouchX1b = false;
                        }
                    } else if (heartRaveValue != 0.0f) {
                        this.tempY1 = heartRaveValue;
                        this.tempX1 = offsetMinute;
                        if (this.tempTouchX1b) {
                            this.index = i;
                            this.tempTouchX1b = false;
                        }
                    }
                }
            }
        }
    }

    private void drawY(Canvas canvas) {
        this.yPaint.setStrokeWidth(1.0f);
        this.yPaint.setColor(-1);
        canvas.drawLine((float) ScreenUtil.dp2px((float) this.left), (float) (ScreenUtil.dp2px((float) this.top) / 2), (float) ScreenUtil.dp2px((float) this.left), (float) (this.f5234h - ScreenUtil.dp2px((float) this.bottom)), this.yPaint);
    }

    private void drawX(Canvas canvas) {
        this.xPaint.setColor(-1);
        this.xPaint.setTextSize(this.textSize);
        int size = this.chartWidth / (this.xLables.size() - 1);
        for (int i = 0; i < this.xLables.size(); i++) {
            if (i == 0) {
                this.xPaint.setTextAlign(Paint.Align.LEFT);
            } else if (i == this.xLables.size() - 1) {
                this.xPaint.setTextAlign(Paint.Align.RIGHT);
            } else {
                this.xPaint.setTextAlign(Paint.Align.CENTER);
            }
            canvas.drawText(this.xLables.get(i) + "", (float) (ScreenUtil.dp2px((float) this.left) + (size * i)), (float) (this.f5234h - (ScreenUtil.dp2px((float) this.bottom) / 2)), this.xPaint);
        }
    }

    private void drawValueAndDottedLine(Canvas canvas) {
        this.xPaint.setColor(-1);
        this.xPaint.setStrokeWidth(1.0f);
        this.xPaint.setTextAlign(Paint.Align.CENTER);
        this.xPaint.setTextSize(this.textSize * 1.0f);
        this.linePaint.setColor(-1);
        this.linePaint.setStrokeWidth(1.0f);
        this.linePaint.setTextAlign(Paint.Align.CENTER);
        this.linePaint.setTextSize(this.textSize * 1.0f);
        this.linePaint.setStyle(Paint.Style.STROKE);
        this.linePaint.setPathEffect(new DashPathEffect(new float[]{5.0f, 5.0f, 5.0f, 5.0f}, 1.0f));
        float f = ((float) this.chartHeight) / this.MAX_VALUE;
        int ascent = ((int) (this.xPaint.ascent() + this.xPaint.descent())) / 2;
        int dip2px = ScreenUtil.dip2px((float) this.left);
        int dip2px2 = (this.f5235w - dip2px) - ScreenUtil.dip2px((float) this.right);
        int dp2px = ScreenUtil.dp2px(6.0f);
        int i = dip2px2 / dp2px;
        int i2 = 1;
        float f2 = 0.0f;
        while (true) {
            float[] fArr = this.scaleY;
            if (i2 < fArr.length - 1) {
                float dp2px2 = (((float) this.f5234h) - (fArr[i2] * f)) - ((float) ScreenUtil.dp2px((float) this.bottom));
                if (i2 > 1) {
                    Paint paint = this.xPaint;
                    int textRectHeight = (int) ((f2 - dp2px2) - ViewUtil.getTextRectHeight(paint, ((int) this.scaleY[i2]) + ""));
                    if (textRectHeight <= 0) {
                        dp2px2 += (float) (textRectHeight - ScreenUtil.dp2px(3.0f));
                    }
                }
                canvas.drawText(((int) this.scaleY[i2]) + "", (float) this.left, dp2px2 - ((float) ascent), this.xPaint);
                float f3 = (float) (dip2px + dp2px);
                canvas.drawLine((float) dip2px, dp2px2, f3, dp2px2, this.xPaint);
                float f4 = f3;
                int i3 = 1;
                while (true) {
                    if (i3 > i) {
                        break;
                    }
                    float f5 = f4 + ((float) (dp2px / 2));
                    f4 = ((float) dp2px) + f5;
                    float f6 = (float) (dip2px2 + dip2px);
                    if (f4 >= f6) {
                        canvas.drawLine(f5, dp2px2, f6, dp2px2, this.xPaint);
                        break;
                    }
                    canvas.drawLine(f5, dp2px2, f4, dp2px2, this.xPaint);
                    i3++;
                }
                i2++;
                f2 = dp2px2;
            } else {
                return;
            }
        }
    }

    private void drawExelBackground(Canvas canvas) {
        this.exelPaint.setColor(this.exelColor);
        canvas.drawRect((float) ScreenUtil.dp2px((float) this.left), (float) ScreenUtil.dp2px((float) this.top), (float) (this.f5235w - ScreenUtil.dp2px((float) this.right)), (float) (this.f5234h - ScreenUtil.dp2px((float) this.bottom)), this.exelPaint);
    }

    private float minutToX(int i, int i2) {
        return (float) (((i * i2) / 1440) + ScreenUtil.dp2px((float) this.left));
    }

    private int xToMinute(float f, int i) {
        return (int) ((((f - ((float) ScreenUtil.dp2px((float) this.left))) * 60.0f) * 24.0f) / ((float) i));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        this.touchX = motionEvent.getX();
        int action = motionEvent.getAction();
        if (action == 0) {
            this.touchX1 = (int) motionEvent.getX();
        } else if (action == 1) {
            this.touchX2 = (int) motionEvent.getX();
            int i = this.touchX2 - this.touchX1;
            if (i < 10 && i > -10 && isOnclick()) {
                this.clickListener.doClick();
            }
        }
        invalidate();
        return true;
    }

    public void doClick(TouchOrClickListener touchOrClickListener) {
        this.clickListener = touchOrClickListener;
    }
}
