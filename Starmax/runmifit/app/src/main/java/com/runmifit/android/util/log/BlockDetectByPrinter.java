package com.wade.fit.util.log;

import android.os.Looper;
import android.util.Printer;

public class BlockDetectByPrinter {
    public static void start() {
        Looper.getMainLooper().setMessageLogging(new Printer() {
            /* class com.wade.fit.util.log.BlockDetectByPrinter.C27011 */
            private static final String END = "<<<<< Finished";
            private static final String START = ">>>>> Dispatching";

            public void println(String str) {
                if (str.startsWith(START)) {
                    LogMonitor.getInstance().startMonitor();
                }
                if (str.startsWith(END)) {
                    LogMonitor.getInstance().removeMonitor();
                }
            }
        });
    }
}
