package com.wade.fit.ui.device.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.baidu.mobstat.Config;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.Alarm;
import com.wade.fit.util.AlarmUtil;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.TimeUtil;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.views.ItemNewAlarmSet;
import com.wade.fit.views.wheel.ArrayWheelAdapter;
import com.wade.fit.views.wheel.NumericWheelAdapter;
import com.wade.fit.views.wheel.OnWheelChangedListener;
import com.wade.fit.views.wheel.WheelView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;

/* renamed from: com.wade.fit.ui.device.activity.AlarmAddActivity */
public class AlarmAddActivity extends BaseActivity {
    private final int REQUEST_REMIND_CODE = 1;
    private final int REQUEST_WEEKDAY_CODE = 0;
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public Alarm alarm;
    WheelView alarmSetAmPm;
    WheelView alarmSetHour;
    WheelView alarmSetMin;
    private String[] alartTypes;
    private String[] alartTypesTwo;
    /* access modifiers changed from: private */
    public String[] amOrpm;
    /* access modifiers changed from: private */
    public boolean is24;
    private boolean isUpdate = true;
    WheelView pointWheelView;
    private Map<Integer, RadioButton> sparseArray = new Hashtable();
    private boolean[] tempAlarm = new boolean[7];
    ItemNewAlarmSet timeAlarmSet;
    LinearLayout timeAlarmSetContentLl;
    TextView tvAlarmRemind;
    TextView tvAlarmRepeat;
    private String[] weeks;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_new_alarm_set;
    }

    public void initView() {
        this.activity = this;
        this.alarm = (Alarm) getIntent().getSerializableExtra(NotificationCompat.CATEGORY_ALARM);
        this.bgView.setVisibility(View.GONE);
        if (this.alarm == null) {
            this.titleName.setText(getResources().getString(R.string.alarm_list_add));
            this.alarm = new Alarm();
            Calendar instance = Calendar.getInstance();
            this.alarm.setAlarmHour(instance.get(11));
            this.alarm.setAlarmMinute(instance.get(12));
            this.isUpdate = false;
        } else {
            this.titleName.setText(getResources().getString(R.string.alarm_list_edit));
            this.isUpdate = true;
        }
        this.alartTypes = AlarmUtil.getAlarmText(this);
        this.alartTypesTwo = AlarmUtil.getAlarmTexts(this);
        setWeek(this.alarm.getWeekRepeat());
        this.amOrpm = getResources().getStringArray(R.array.amOrpm);
        this.timeAlarmSet.setAlartTime(this.alarm.getAlarmHour(), this.alarm.getAlarmMinute(), this.activity);
        this.is24 = TimeUtil.is24Hour(this.activity);
        setAlarmTime();
        setWheelListener();
        initEvent();
        this.tvAlarmRemind.setText(this.alartTypesTwo[this.alarm.getAlarmType()]);
    }

    private void setWeek(boolean[] zArr) {
        StringBuffer stringBuffer = new StringBuffer();
        String[] stringArray = getResources().getStringArray(R.array.weekDay);
        int i = 0;
        for (int i2 = 0; i2 < zArr.length; i2++) {
            if (i2 == 6) {
                if (zArr[i2]) {
                    i++;
                    stringBuffer.append(stringArray[0] + ",");
                }
            } else if (zArr[i2]) {
                i++;
                stringBuffer.append(stringArray[i2 + 1] + ",");
            }
        }
        if (i <= 0) {
            this.tvAlarmRepeat.setText(getResources().getString(R.string.alarm_once));
        } else if (i == 7) {
            this.tvAlarmRepeat.setText(getResources().getString(R.string.alarm_every_day));
        } else {
            this.tvAlarmRepeat.setText(stringBuffer.toString().substring(0, stringBuffer.length() - 1));
        }
    }

    /* access modifiers changed from: package-private */
    public void selectRemind() {
        Bundle bundle = new Bundle();
        bundle.putInt("iPosition", this.alarm.getAlarmType());
        IntentUtil.goToActivityForResult(this, SelectRemindActivity.class, bundle, 1);
    }

    /* access modifiers changed from: package-private */
    public void selectRepeat() {
        Bundle bundle = new Bundle();
        bundle.putBooleanArray("isSelect", this.alarm.getWeekRepeat());
        IntentUtil.goToActivityForResult(this, SelectWeekDayActivity.class, bundle, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == 200) {
            if (i == 0) {
                this.alarm.setWeekRepeat(intent.getExtras().getBooleanArray("isSelect"));
                setWeek(this.alarm.getWeekRepeat());
            } else if (i == 1) {
                this.alarm.setAlarmType(intent.getExtras().getInt("iPosition"));
                this.tvAlarmRemind.setText(this.alartTypesTwo[this.alarm.getAlarmType()]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        changeTimeZ();
    }

    private void changeTimeZ() {
        if (this.is24 != TimeUtil.is24Hour(this.activity)) {
            this.is24 = TimeUtil.is24Hour(this.activity);
            this.alarmSetAmPm.addChangingListener(null);
            this.alarmSetHour.addChangingListener(null);
            this.alarmSetMin.addChangingListener(null);
            if (this.alarmSetAmPm.getCurrentItem() == 0) {
                if (this.alarm.getAlarmHour() == 12) {
                    this.alarm.setAlarmHour(0);
                } else {
                    Alarm alarm2 = this.alarm;
                    alarm2.setAlarmHour(alarm2.getAlarmHour());
                }
            } else if (this.alarm.getAlarmHour() == 24) {
                this.alarm.setAlarmHour(12);
            } else {
                Alarm alarm3 = this.alarm;
                alarm3.setAlarmHour(alarm3.getAlarmHour() < 12 ? this.alarm.getAlarmHour() + 12 : this.alarm.getAlarmHour());
            }
            setAlarmTime();
            setWheelListener();
            if (this.is24) {
                this.timeAlarmSet.setAlartTime(this.alarm.getAlarmHour(), this.alarm.getAlarmMinute(), this.activity);
            } else {
                this.timeAlarmSet.setAlartTime(this.alarm.getAlarmHour() % 12, this.alarm.getAlarmMinute(), this.amOrpm[this.alarmSetAmPm.getCurrentItem()]);
            }
        }
    }

    private void setAlarmTime() {
        if (this.is24) {
            this.alarmSetAmPm.setVisibility(View.GONE);
        } else {
            this.alarmSetAmPm.setVisibility(View.VISIBLE);
            this.alarmSetAmPm.setAdapter(new ArrayWheelAdapter(this.amOrpm, 2));
            this.alarmSetAmPm.setCurrentItem(TimeUtil.isAM(this.alarm.getAlarmHour()) ^ true ? 1 : 0);
            this.alarmSetAmPm.setCyclic(false);
        }
        if (this.is24) {
            this.alarmSetHour.setAdapter(new NumericWheelAdapter(0, 23, "%02d"));
        } else {
            this.alarmSetHour.setAdapter(new NumericWheelAdapter(1, 12, "%02d"));
        }
        this.alarmSetHour.setCurrentItem(this.is24 ? this.alarm.getAlarmHour() : TimeUtil.format24To12(this.alarm.getAlarmHour()) - 1);
        this.alarmSetHour.setCyclic(true);
        this.alarmSetMin.setAdapter(new NumericWheelAdapter(0, 59, "%02d"));
        this.alarmSetMin.setCurrentItem(this.alarm.getAlarmMinute());
        this.alarmSetMin.setCyclic(true);
        this.pointWheelView.setAdapter(new ArrayWheelAdapter(new String[]{Config.TRACE_TODAY_VISIT_SPLIT}, 1));
        this.pointWheelView.setClickable(false);
        this.pointWheelView.setEnabled(false);
    }

    private void initEvent() {
        this.rightText.setText(getResources().getString(R.string.save));
        this.rightText.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AlarmAddActivity$SMu7F2sL34dDDrCykkDDrsocHO4 */

            public final void onClick(View view) {
                AlarmAddActivity.this.lambda$initEvent$0$AlarmAddActivity(view);
            }
        });
    }

    public /* synthetic */ void lambda$initEvent$0$AlarmAddActivity(View view) {
        if (!ButtonUtils.isFastDoubleClick(view.getId())) {
            Intent intent = new Intent();
            if (this.alarmSetAmPm.getVisibility() == 0) {
                DebugLog.m6203d("12小时制：" + this.alarm.toString());
                if (this.alarmSetAmPm.getCurrentItem() == 0) {
                    if (this.alarm.getAlarmHour() == 12) {
                        this.alarm.setAlarmHour(0);
                    } else {
                        Alarm alarm2 = this.alarm;
                        alarm2.setAlarmHour(alarm2.getAlarmHour());
                    }
                } else if (this.alarm.getAlarmHour() == 24) {
                    this.alarm.setAlarmHour(12);
                } else {
                    Alarm alarm3 = this.alarm;
                    alarm3.setAlarmHour(alarm3.getAlarmHour() < 12 ? this.alarm.getAlarmHour() + 12 : this.alarm.getAlarmHour());
                }
            } else {
                DebugLog.m6203d("24小时制：" + this.alarm.toString());
            }
            intent.putExtra(NotificationCompat.CATEGORY_ALARM, this.alarm);
            intent.putExtra("showWeeks", this.tempAlarm);
            intent.putExtra("isUpdate", this.isUpdate);
            DebugLog.m6203d("alarm:" + this.alarm.toString());
            DebugLog.m6203d("tempAlarm:" + Arrays.toString(this.tempAlarm));
            setResult(200, intent);
            this.activity.finish();
        }
    }

    private void setWheelListener() {
        this.alarmSetAmPm.addChangingListener(new OnWheelChangedListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AlarmAddActivity$CvWaiKM_EfFhXJ5Els9YKnQ2DZo */

            public final void onChanged(WheelView wheelView, int i, int i2) {
                AlarmAddActivity.this.lambda$setWheelListener$1$AlarmAddActivity(wheelView, i, i2);
            }
        });
        this.alarmSetHour.addChangingListener(new OnWheelChangedListener() {
            /* class com.wade.fit.ui.device.activity.AlarmAddActivity.C24441 */

            public void onChanged(WheelView wheelView, int i, int i2) {
                DebugLog.m6203d("闹钟滑动小时的动作oldValue=" + i + ",newValue=" + i2);
                int currentItem = AlarmAddActivity.this.alarmSetMin.getCurrentItem();
                if (AlarmAddActivity.this.is24) {
                    AlarmAddActivity.this.timeAlarmSet.setAlartTime(i2, currentItem, AlarmAddActivity.this.activity);
                } else {
                    i2++;
                    AlarmAddActivity.this.timeAlarmSet.setAlartTime(i2, currentItem, AlarmAddActivity.this.amOrpm[AlarmAddActivity.this.alarmSetAmPm.getCurrentItem()]);
                }
                AlarmAddActivity.this.alarm.setAlarmHour(i2);
                AlarmAddActivity.this.alarm.setAlarmMinute(currentItem);
            }
        });
        this.alarmSetMin.addChangingListener(new OnWheelChangedListener() {
            /* class com.wade.fit.ui.device.activity.AlarmAddActivity.C24452 */

            public void onChanged(WheelView wheelView, int i, int i2) {
                DebugLog.m6203d("闹钟滑动分钟的动作oldValue=" + i + ",newValue=" + i2);
                int currentItem = AlarmAddActivity.this.alarmSetHour.getCurrentItem();
                if (AlarmAddActivity.this.is24) {
                    AlarmAddActivity.this.timeAlarmSet.setAlartTime(currentItem, i2, AlarmAddActivity.this.activity);
                } else {
                    currentItem++;
                    AlarmAddActivity.this.timeAlarmSet.setAlartTime(currentItem, i2, AlarmAddActivity.this.amOrpm[AlarmAddActivity.this.alarmSetAmPm.getCurrentItem()]);
                }
                AlarmAddActivity.this.alarm.setAlarmHour(currentItem);
                AlarmAddActivity.this.alarm.setAlarmMinute(i2);
            }
        });
    }

    public /* synthetic */ void lambda$setWheelListener$1$AlarmAddActivity(WheelView wheelView, int i, int i2) {
        DebugLog.m6203d("闹钟滑动上下午的动作oldValue=" + i + ",newValue=" + i2);
        int currentItem = this.alarmSetHour.getCurrentItem();
        int currentItem2 = this.alarmSetMin.getCurrentItem();
        if (this.is24) {
            this.timeAlarmSet.setAlartTime(currentItem, currentItem2, this.activity);
        } else {
            currentItem++;
            this.alarmSetAmPm.setCurrentItem(i2);
            this.timeAlarmSet.setAlartTime(currentItem, currentItem2, this.amOrpm[i2]);
        }
        this.alarm.setAlarmHour(currentItem);
        this.alarm.setAlarmMinute(currentItem2);
    }
}
