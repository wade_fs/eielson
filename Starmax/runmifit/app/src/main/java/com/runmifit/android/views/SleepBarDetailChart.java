package com.wade.fit.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import com.baidu.mobstat.Config;
import com.google.gson.Gson;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.bean.HealthSleepItem;
import com.wade.fit.util.CommonUtil;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ViewUtil;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

public class SleepBarDetailChart extends DetailBaseView {
    int SLEEP_AWAKE = 4;
    int SLEEP_DEEP = 3;
    int SLEEP_EYE = 5;
    int SLEEP_LIGHT = 2;
    int SLEEP_START = 1;
    private int barColor;
    private int barNum = 96;
    private Paint barPaint;
    private int[] colors = {R.color.sleep_start_color, R.color.sleep_light_color, R.color.sleep_deep_color, R.color.sleep_wake_color, R.color.sleep_eye_color};
    private Paint dataPaint;
    private float dataSpace;
    private float dataWidth;
    private String endTime;
    private Gson gson = new Gson();

    /* renamed from: h */
    private int f5244h;
    List<HealthSleep> healthSleepList = new ArrayList();
    private List<HealthSleepItem> item = new ArrayList();
    private List<String> lables = new ArrayList();
    private float leftX = 180.0f;
    private float[] lineHeight = new float[this.splitNum];
    int maxTime = 720;
    int paddingTop;
    Path path = new Path();
    Paint pathPaint = new Paint();
    RectF rect = new RectF();
    float scaleY;
    private Paint selectPaint;
    private RectF selectRect = new RectF();
    int selectedIndex = -1;
    private int splitLineColor;
    private int splitNum = 6;
    private String startTime;
    private int textColor;
    private float textSize;
    private Paint timePaint;
    private Paint topDataPaint;
    private int totalCount;
    int totleTime = 0;
    private float touchX;
    private int type;

    /* renamed from: w */
    private int f5245w;
    private float xOffSet;
    private float xWidth;
    private String[] xtimes;
    private Paint yLinePaint;

    public SleepBarDetailChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SportBarChart);
        this.textSize = obtainStyledAttributes.getDimension(0, 28.0f);
        this.textColor = obtainStyledAttributes.getColor(1, ContextCompat.getColor(context, R.color.color_666666to999999));
        this.barColor = obtainStyledAttributes.getColor(2, -1092544);
        this.splitLineColor = obtainStyledAttributes.getColor(8, -11514545);
        obtainStyledAttributes.recycle();
        initPaint();
    }

    private void initPaint() {
        this.barPaint = new Paint(1);
        this.barPaint.setColor(this.barColor);
        this.dataPaint = new Paint(1);
        this.pathPaint = new Paint(1);
        this.pathPaint.setColor(getResources().getColor(R.color.touch_bg_color));
        this.selectPaint = new Paint(1);
        this.selectPaint.setColor(getResources().getColor(R.color.touch_bg_color));
        this.selectPaint.setTextAlign(Paint.Align.CENTER);
        this.selectPaint.setTextSize(this.textSize);
        this.yLinePaint = new Paint();
        this.yLinePaint.setAntiAlias(true);
        this.yLinePaint.setStyle(Paint.Style.STROKE);
        this.yLinePaint.setStrokeJoin(Paint.Join.ROUND);
        this.yLinePaint.setStrokeCap(Paint.Cap.ROUND);
        this.yLinePaint.setDither(true);
        this.yLinePaint.setStrokeWidth(TypedValue.applyDimension(1, 0.5f, getResources().getDisplayMetrics()));
        this.yLinePaint.setColor(this.splitLineColor);
        this.yLinePaint.setPathEffect(new DashPathEffect(new float[]{3.0f, 5.0f}, 0.0f));
        this.timePaint = new Paint(1);
        this.timePaint.setColor(this.textColor);
        this.timePaint.setTextSize(this.textSize);
        this.topDataPaint = new Paint(1);
        this.topDataPaint.setColor(getResources().getColor(R.color.white));
        this.topDataPaint.setTextAlign(Paint.Align.CENTER);
        this.topDataPaint.setTextSize(this.textSize);
        setLayerType(1, null);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.f5245w = i;
        this.f5244h = i2;
        calculate();
    }

    private void calculate() {
        this.paddingTop = ScreenUtil.dip2px(20.0f);
        int i = 0;
        while (true) {
            int i2 = this.splitNum;
            if (i >= i2) {
                break;
            }
            this.lineHeight[i] = (float) (((this.f5244h - this.paddingTop) / i2) * i);
            i++;
        }
        this.barPaint.setTextSize(this.textSize);
        this.xOffSet = ViewUtil.getTextRectWidth(this.barPaint, Constants.VIA_REPORT_TYPE_SHARE_TO_QQ);
        int i3 = this.totalCount;
        if (i3 != 0) {
            this.dataSpace = (((float) this.f5245w) - (this.xOffSet * 2.8f)) / ((float) i3);
            if (this.type == 0) {
                this.dataWidth = this.dataSpace;
            } else {
                this.dataWidth = this.dataSpace * 0.5f;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(getResources().getColor(R.color.white));
        drawX(canvas);
    }

    private void drawX(Canvas canvas) {
        if (this.type == 0) {
            List<HealthSleepItem> list = this.item;
            if (list == null || list.isEmpty()) {
                return;
            }
        } else {
            List<HealthSleep> list2 = this.healthSleepList;
            if (list2 == null || list2.isEmpty()) {
                return;
            }
        }
        if (!this.lables.isEmpty()) {
            this.dataPaint.setTextAlign(Paint.Align.CENTER);
            if (this.type == 0) {
                drawDay(canvas);
            } else {
                drawWeekMonthYear(canvas);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void drawWeekMonthYear(Canvas canvas) {
        String str;
        float f = this.dataWidth + (this.xOffSet * 1.4f);
        float width = (((float) getWidth()) - (this.xOffSet * 2.8f)) / ((float) this.lables.size());
        int i = this.type;
        if (i == 1 || i == 2) {
            width = (((float) getWidth()) - (this.xOffSet * 2.8f)) / ((float) (this.lables.size() - 1));
        }
        char c = 0;
        for (int i2 = 0; i2 < this.lables.size(); i2++) {
            float f2 = (((float) i2) * width) + f;
            this.timePaint.setTextAlign(Paint.Align.LEFT);
            if (i2 == 0) {
                this.timePaint.setTextAlign(Paint.Align.LEFT);
            } else if (i2 == this.lables.size() - 1) {
                this.timePaint.setTextAlign(Paint.Align.RIGHT);
            } else {
                this.timePaint.setTextAlign(Paint.Align.CENTER);
            }
            canvas.drawText(this.lables.get(i2), f2, this.lineHeight[this.splitNum - 1] + ViewUtil.getTextHeight(this.barPaint) + ((float) ScreenUtil.dip2px(20.0f)), this.timePaint);
        }
        if (this.healthSleepList != null) {
            int i3 = 0;
            int i4 = -1;
            float f3 = 0.0f;
            float f4 = 0.0f;
            while (i3 < this.healthSleepList.size()) {
                HealthSleep healthSleep = this.healthSleepList.get(i3);
                float ceil = (float) Math.ceil((double) ((this.dataSpace * ((float) i3)) + f));
                int sleepMinutes = healthSleep.getSleepMinutes() + healthSleep.getLightSleepMinutes() + healthSleep.getEyeMinutes() + healthSleep.getWakeMunutes() + healthSleep.getDeepSleepMinutes();
                if (sleepMinutes != 0) {
                    float[] fArr = this.lineHeight;
                    int i5 = this.splitNum;
                    float f5 = fArr[i5 - 1];
                    int i6 = this.paddingTop;
                    float f6 = ((float) i6) + f5;
                    float f7 = (float) sleepMinutes;
                    float f8 = (f7 / ((float) this.maxTime)) * (f6 - ((float) ((this.f5244h - i6) / i5))) * 0.8f;
                    float deepSleepMinutes = (((float) healthSleep.getDeepSleepMinutes()) / f7) * f8;
                    float lightSleepMinutes = (((float) healthSleep.getLightSleepMinutes()) / f7) * f8;
                    float wakeMunutes = (((float) healthSleep.getWakeMunutes()) / f7) * f8;
                    float eyeMinutes = (((float) healthSleep.getEyeMinutes()) / f7) * f8;
                    float sleepMinutes2 = (((float) healthSleep.getSleepMinutes()) / f7) * f8;
                    this.dataPaint.setStrokeWidth(this.dataWidth);
                    this.dataPaint.setColor(getResources().getColor(this.colors[c]));
                    float f9 = f6 - (sleepMinutes2 * this.scaleY);
                    float f10 = ceil;
                    float f11 = f10;
                    canvas.drawLine(ceil, f6, f11, f9, this.dataPaint);
                    float f12 = f9 - (deepSleepMinutes * this.scaleY);
                    this.dataPaint.setColor(getResources().getColor(this.colors[2]));
                    float f13 = f10;
                    canvas.drawLine(f13, f9, f11, f12, this.dataPaint);
                    this.dataPaint.setColor(getResources().getColor(this.colors[1]));
                    float f14 = f12 - (lightSleepMinutes * this.scaleY);
                    canvas.drawLine(f13, f12, f11, f14, this.dataPaint);
                    float f15 = f14 - (eyeMinutes * this.scaleY);
                    this.dataPaint.setColor(getResources().getColor(this.colors[4]));
                    Canvas canvas2 = canvas;
                    float f16 = f10;
                    canvas2.drawLine(f16, f14, f11, f15, this.dataPaint);
                    this.dataPaint.setColor(getResources().getColor(this.colors[3]));
                    float f17 = f15 - (wakeMunutes * this.scaleY);
                    canvas2.drawLine(f16, f15, f11, f17, this.dataPaint);
                    this.timePaint.setTextAlign(Paint.Align.CENTER);
                    float f18 = this.touchX;
                    if (f18 >= f10 && f18 < f10 + this.dataWidth) {
                        i4 = i3;
                        f3 = f17;
                        f4 = f10;
                    }
                }
                i3++;
                c = 0;
            }
            if (i4 != -1) {
                float f19 = f4 - (this.dataWidth / 2.0f);
                this.dataPaint.setColor(getResources().getColor(this.colors[0]));
                int i7 = this.type;
                if (i7 == 2 || i7 == 3) {
                    str = ("" + AppApplication.getInstance().getResources().getString(R.string.time_heart_avg)) + " " + (this.healthSleepList.get(i4).getTotalSleepMinutes() / 60) + " " + AppApplication.getInstance().getResources().getString(R.string.hour_minute) + " " + (this.healthSleepList.get(i4).getTotalSleepMinutes() % 60) + " " + AppApplication.getInstance().getResources().getString(R.string.unit_minute);
                } else {
                    str = "" + (this.healthSleepList.get(i4).getTotalSleepMinutes() / 60) + " " + AppApplication.getInstance().getResources().getString(R.string.unit_hour) + " " + (this.healthSleepList.get(i4).getTotalSleepMinutes() % 60) + " " + AppApplication.getInstance().getResources().getString(R.string.unit_minute);
                }
                drawDayTouchValue(canvas, f3, f19 + (this.dataWidth / 2.0f), str, this.healthSleepList.get(i4).getRemark());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String setPaintColor(HealthSleepItem healthSleepItem) {
        int sleepStatus = healthSleepItem.getSleepStatus();
        if (sleepStatus == 1) {
            this.dataPaint.setColor(getResources().getColor(this.colors[0]));
            return getResources().getString(R.string.sleep_start);
        } else if (sleepStatus == 2) {
            this.dataPaint.setColor(getResources().getColor(this.colors[1]));
            return getResources().getString(R.string.light_sleep);
        } else if (sleepStatus == 3) {
            this.dataPaint.setColor(getResources().getColor(this.colors[2]));
            return getResources().getString(R.string.deep_sleep);
        } else if (sleepStatus == 4) {
            this.dataPaint.setColor(getResources().getColor(this.colors[3]));
            return getResources().getString(R.string.wake_sleep);
        } else if (sleepStatus != 5) {
            return "";
        } else {
            this.dataPaint.setColor(getResources().getColor(this.colors[4]));
            return getResources().getString(R.string.eye_sleep);
        }
    }

    /* access modifiers changed from: package-private */
    public void drawDay(Canvas canvas) {
        float f;
        float f2;
        String str;
        float f3;
        Canvas canvas2 = canvas;
        float f4 = this.dataWidth + (this.xOffSet * 1.4f);
        int dip2px = ScreenUtil.dip2px(10.0f);
        if (this.type == 0) {
            f = ((((float) getWidth()) - f4) - ((float) dip2px)) / ((float) (this.lables.size() - 1));
        } else {
            f = (((float) getWidth()) - f4) / 1440.0f;
        }
        int i = 0;
        while (true) {
            f2 = 20.0f;
            if (i >= this.lables.size()) {
                break;
            }
            if (this.type == 0) {
                f3 = ((float) i) * f;
            } else {
                f3 = ((float) this.item.get(i).getOffsetMinute()) * f * ((float) i);
            }
            float f5 = f3 + f4;
            if (i == 0) {
                this.timePaint.setTextAlign(Paint.Align.LEFT);
            } else if (i == this.lables.size() - 1) {
                this.timePaint.setTextAlign(Paint.Align.RIGHT);
            } else {
                this.timePaint.setTextAlign(Paint.Align.CENTER);
            }
            canvas2.drawText(this.lables.get(i), f5, this.lineHeight[this.splitNum - 1] + ViewUtil.getTextHeight(this.barPaint) + ((float) ScreenUtil.dip2px(20.0f)), this.timePaint);
            i++;
        }
        float f6 = this.dataWidth;
        this.item.get(0).getIndex();
        float f7 = f4;
        int i2 = 0;
        boolean z = false;
        float f8 = 0.0f;
        float f9 = 0.0f;
        while (i2 < this.item.size()) {
            HealthSleepItem healthSleepItem = this.item.get(i2);
            float dip2px2 = this.lineHeight[this.splitNum - 1] + ((float) ScreenUtil.dip2px(f2));
            float dip2px3 = dip2px2 - ((((float) ((this.f5244h - ScreenUtil.dip2px(f2)) / this.splitNum)) * 3.5f) * this.scaleY);
            this.dataPaint.setStrokeWidth(this.dataWidth);
            this.dataPaint.setTextAlign(Paint.Align.CENTER);
            setPaintColor(healthSleepItem);
            float f10 = this.dataWidth + f7;
            this.timePaint.setTextAlign(Paint.Align.LEFT);
            boolean z2 = z;
            float floor = (float) Math.floor((double) f7);
            if (healthSleepItem.getSleepStatus() != 0) {
                this.rect.set(floor, dip2px3, f7 + this.dataWidth, dip2px2);
                canvas2.drawRect(this.rect, this.dataPaint);
            }
            String[] split = "0:0".split(Config.TRACE_TODAY_VISIT_SPLIT);
            String str2 = split[0];
            String str3 = split[1];
            float f11 = this.touchX;
            if (f11 >= f10 && f11 < this.dataWidth + f10 && healthSleepItem.getSleepStatus() != 0 && this.selectedIndex < this.item.size() && this.selectedIndex >= 0) {
                this.selectedIndex = i2;
                f9 = f10 - this.dataWidth;
                f8 = dip2px3;
                z2 = true;
            }
            i2++;
            f7 = f10;
            z = z2;
            f2 = 20.0f;
        }
        if (z && this.selectedIndex >= 0) {
            StringBuffer stringBuffer = new StringBuffer();
            StringBuffer stringBuffer2 = new StringBuffer();
            HealthSleepItem healthSleepItem2 = this.item.get(this.selectedIndex);
            stringBuffer.append(setPaintColor(healthSleepItem2));
            healthSleepItem2.getOffsetMinute();
            int index = this.item.get(this.selectedIndex).getIndex();
            if (index <= 18) {
                StringBuilder sb = new StringBuilder();
                int i3 = (index * 10) + 1260;
                sb.append(i3 / 60);
                sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
                sb.append(i3 % 60);
                str = sb.toString();
                StringBuilder sb2 = new StringBuilder();
                int i4 = i3 + 10;
                sb2.append(i4 / 60);
                sb2.append(Config.TRACE_TODAY_VISIT_SPLIT);
                sb2.append(i4 % 60);
                this.endTime = sb2.toString();
            } else {
                StringBuilder sb3 = new StringBuilder();
                int i5 = (index - 18) * 10;
                sb3.append(i5 / 60);
                sb3.append(Config.TRACE_TODAY_VISIT_SPLIT);
                sb3.append(i5 % 60);
                str = sb3.toString();
                StringBuilder sb4 = new StringBuilder();
                int i6 = i5 + 10;
                sb4.append(i6 / 60);
                sb4.append(Config.TRACE_TODAY_VISIT_SPLIT);
                sb4.append(i6 % 60);
                this.endTime = sb4.toString();
            }
            stringBuffer2.append(CommonUtil.timeFormatter(str, CommonUtil.is24Hour(), getContext()));
            String stringBuffer3 = stringBuffer.toString();
            String stringBuffer4 = stringBuffer2.toString();
            drawDayTouchValue(canvas, f8, (this.dataWidth / 2.0f) + f9, stringBuffer3, stringBuffer4);
        }
    }

    private void drawDayTouchValue(Canvas canvas, float f, float f2, String str, String str2) {
        this.path.reset();
        this.path.moveTo(f2 - ((float) ScreenUtil.dip2px(4.0f)), f - ((float) ScreenUtil.dip2px(10.0f)));
        this.path.lineTo(((float) ScreenUtil.dip2px(4.0f)) + f2, f - ((float) ScreenUtil.dip2px(10.0f)));
        this.path.lineTo(f2, f - ((float) ScreenUtil.dip2px(4.0f)));
        this.path.close();
        canvas.drawPath(this.path, this.pathPaint);
        float max = Math.max(ViewUtil.getTextRectWidth(this.selectPaint, str), ViewUtil.getTextRectWidth(this.selectPaint, str2));
        int dip2px = ScreenUtil.dip2px(5.0f);
        float f3 = this.leftX;
        if (f2 < f3) {
            RectF rectF = this.selectRect;
            rectF.left = (f3 - (max / 2.0f)) - ((float) (dip2px * 3));
            rectF.bottom = f - ((float) ScreenUtil.dip2px(10.0f));
            RectF rectF2 = this.selectRect;
            rectF2.right = rectF2.left + max + ((float) (dip2px * 6));
            RectF rectF3 = this.selectRect;
            rectF3.top = (((rectF3.bottom - ViewUtil.getTextRectHeight(this.selectPaint, str2)) - ViewUtil.getTextRectHeight(this.selectPaint, str)) - ((float) ScreenUtil.dip2px(15.0f))) - ((float) dip2px);
            canvas.drawRoundRect(this.selectRect, (float) ScreenUtil.dip2px(5.0f), (float) ScreenUtil.dip2px(5.0f), this.selectPaint);
            canvas.drawText(str2, this.leftX, f - ((float) ScreenUtil.dip2px(30.0f)), this.topDataPaint);
            canvas.drawText(str, this.leftX, f - ((float) ScreenUtil.dip2px(15.0f)), this.topDataPaint);
            return;
        }
        int i = this.f5245w;
        if (f2 > ((float) i) - f3) {
            int i2 = this.type;
            if (i2 == 1 || i2 == 0) {
                RectF rectF4 = this.selectRect;
                rectF4.left = ((((float) this.f5245w) - this.leftX) - (max / 2.0f)) - ((float) dip2px);
                rectF4.right = rectF4.left + max + ((float) (dip2px * 3));
            } else {
                RectF rectF5 = this.selectRect;
                float f4 = (((float) i) - f3) - (max / 2.0f);
                float f5 = (float) (dip2px * 3);
                rectF5.left = f4 - f5;
                rectF5.right = rectF5.left + max + f5;
            }
            this.selectRect.bottom = f - ((float) ScreenUtil.dip2px(10.0f));
            RectF rectF6 = this.selectRect;
            float f6 = (float) dip2px;
            rectF6.top = (((rectF6.bottom - ViewUtil.getTextRectHeight(this.selectPaint, str2)) - ViewUtil.getTextRectHeight(this.selectPaint, str)) - ((float) ScreenUtil.dip2px(15.0f))) - f6;
            LogUtil.d("超过右边..." + this.selectRect);
            canvas.drawRoundRect(this.selectRect, (float) ScreenUtil.dip2px(5.0f), (float) ScreenUtil.dip2px(5.0f), this.selectPaint);
            int i3 = this.type;
            if (i3 == 1 || i3 == 0) {
                canvas.drawText(str2, (((float) this.f5245w) - this.leftX) + f6, f - ((float) ScreenUtil.dip2px(30.0f)), this.topDataPaint);
                canvas.drawText(str, (((float) this.f5245w) - this.leftX) + f6, f - ((float) ScreenUtil.dip2px(15.0f)), this.topDataPaint);
                return;
            }
            canvas.drawText(str2, (((float) this.f5245w) - this.leftX) - f6, f - ((float) ScreenUtil.dip2px(30.0f)), this.topDataPaint);
            canvas.drawText(str, (((float) this.f5245w) - this.leftX) - f6, f - ((float) ScreenUtil.dip2px(15.0f)), this.topDataPaint);
            return;
        }
        RectF rectF7 = this.selectRect;
        rectF7.left = (f2 - (max / 2.0f)) - ((float) (dip2px * 3));
        rectF7.bottom = f - ((float) ScreenUtil.dip2px(10.0f));
        RectF rectF8 = this.selectRect;
        rectF8.right = rectF8.left + max + ((float) (dip2px * 6));
        RectF rectF9 = this.selectRect;
        rectF9.top = (((rectF9.bottom - ViewUtil.getTextRectHeight(this.selectPaint, str2)) - ViewUtil.getTextRectHeight(this.selectPaint, str)) - ((float) ScreenUtil.dip2px(15.0f))) - ((float) dip2px);
        canvas.drawRoundRect(this.selectRect, (float) ScreenUtil.dip2px(5.0f), (float) ScreenUtil.dip2px(5.0f), this.selectPaint);
        canvas.drawText(str2, f2, f - ((float) ScreenUtil.dip2px(30.0f)), this.topDataPaint);
        canvas.drawText(str, f2, f - ((float) ScreenUtil.dip2px(15.0f)), this.topDataPaint);
    }

    public void setDatas(List<HealthSleepItem> list, String str, String str2, List<String> list2) {
        this.type = 0;
        this.totalCount = list.size();
        this.totleTime = 0;
        this.maxTime = 0;
        this.startTime = str;
        this.endTime = str2;
        this.item.clear();
        this.item = list;
        this.lables.clear();
        this.lables = list2;
        if (!(list == null || list.size() == 0)) {
            DebugLog.m6209i("设置数据      count:        item.size():" + this.item.size());
            int i = 0;
            for (int i2 = 0; i2 < list.size(); i2++) {
                if (i < list.get(i2).getOffsetMinute()) {
                    i = list.get(i2).getOffsetMinute();
                }
                this.totleTime += list.get(i2).getOffsetMinute();
            }
            this.maxTime = i;
        }
        calculate();
        animateY(500);
    }

    public void setData(int i, List<HealthSleep> list, List<String> list2) {
        this.lables = list2;
        this.type = i;
        this.healthSleepList.clear();
        this.healthSleepList.addAll(list);
        this.totalCount = list.size();
        int i2 = 0;
        for (int i3 = 0; i3 < list.size(); i3++) {
            HealthSleep healthSleep = list.get(i3);
            int deepSleepMinutes = healthSleep.getDeepSleepMinutes() + healthSleep.getLightSleepMinutes() + healthSleep.getEyeMinutes() + healthSleep.getWakeMunutes() + healthSleep.getSleepMinutes();
            if (i2 < deepSleepMinutes) {
                i2 = deepSleepMinutes;
            }
        }
        this.maxTime = i2;
        calculate();
        animateY(500);
    }

    private void animateY(int i) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.setDuration((long) i);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            /* class com.wade.fit.views.SleepBarDetailChart.C27141 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                SleepBarDetailChart.this.scaleY = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                SleepBarDetailChart.this.invalidate();
            }
        });
        ofFloat.start();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 2) {
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            int i = this.f5244h - 100;
            this.touchX = motionEvent.getX();
            int i2 = 0;
            while (true) {
                if (i2 >= this.totalCount) {
                    break;
                }
                float f = this.dataWidth;
                float f2 = this.xOffSet;
                float f3 = f + (f2 * 1.4f);
                float f4 = this.dataSpace;
                int i3 = i2 + 1;
                if (new Rect((int) ((f2 * 1.4f) + (f4 * ((float) i2))), 0, (int) (f3 + (((float) i3) * f4)), i).contains(x, y)) {
                    this.selectedIndex = i2;
                    break;
                }
                i2 = i3;
            }
        }
        invalidate();
        return super.onTouchEvent(motionEvent);
    }
}
