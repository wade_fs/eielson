package com.wade.fit.util.ble;

public class HandlerBleDataResult {
    public Object data;
    public boolean hasNext;
    public boolean isComplete;

    public String toString() {
        return "HandlerBleDataResult{data=" + this.data + "hasNext=" + this.hasNext + ", isComplete=" + this.isComplete + '}';
    }
}
