package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthHeartRate;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class HealthHeartRateDao extends AbstractDao<HealthHeartRate, Void> {
    public static final String TABLENAME = "HEALTH_HEART_RATE";

    public static class Properties {
        public static final Property Aerobic_mins = new Property(11, Integer.TYPE, "aerobic_mins", false, "AEROBIC_MINS");
        public static final Property Aerobic_threshold = new Property(8, Integer.TYPE, "aerobic_threshold", false, "AEROBIC_THRESHOLD");
        public static final Property AvgDayHr = new Property(15, Integer.TYPE, "avgDayHr", false, "AVG_DAY_HR");
        public static final Property AvgHr = new Property(14, Integer.TYPE, "avgHr", false, "AVG_HR");
        public static final Property Burn_fat_mins = new Property(10, Integer.TYPE, "burn_fat_mins", false, "BURN_FAT_MINS");
        public static final Property Burn_fat_threshold = new Property(7, Integer.TYPE, "burn_fat_threshold", false, "BURN_FAT_THRESHOLD");
        public static final Property Date = new Property(18, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(4, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");
        public static final Property IsUploaded = new Property(0, Boolean.TYPE, "isUploaded", false, "IS_UPLOADED");
        public static final Property Limit_mins = new Property(12, Integer.TYPE, "limit_mins", false, "LIMIT_MINS");
        public static final Property Limit_threshold = new Property(9, Integer.TYPE, "limit_threshold", false, "LIMIT_THRESHOLD");
        public static final Property MacAddress = new Property(1, String.class, "macAddress", false, "MAC_ADDRESS");
        public static final Property MaxHr = new Property(16, Integer.TYPE, "maxHr", false, "MAX_HR");
        public static final Property MinHr = new Property(17, Integer.TYPE, "minHr", false, "MIN_HR");
        public static final Property Month = new Property(3, Integer.TYPE, "month", false, "MONTH");
        public static final Property Remark = new Property(20, String.class, "remark", false, "REMARK");
        public static final Property SilentHeart = new Property(6, Integer.TYPE, "silentHeart", false, "SILENT_HEART");
        public static final Property StartTime = new Property(5, Integer.TYPE, "startTime", false, "START_TIME");
        public static final Property UserId = new Property(19, String.class, Constants.userId, false, "USER_ID");
        public static final Property UserMaxHr = new Property(13, Integer.TYPE, "UserMaxHr", false, "USER_MAX_HR");
        public static final Property Year = new Property(2, Integer.TYPE, "year", false, "YEAR");
    }

    public Void getKey(HealthHeartRate healthHeartRate) {
        return null;
    }

    public boolean hasKey(HealthHeartRate healthHeartRate) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(HealthHeartRate healthHeartRate, long j) {
        return null;
    }

    public HealthHeartRateDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public HealthHeartRateDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"HEALTH_HEART_RATE\" (\"IS_UPLOADED\" INTEGER NOT NULL ,\"MAC_ADDRESS\" TEXT,\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"START_TIME\" INTEGER NOT NULL ,\"SILENT_HEART\" INTEGER NOT NULL ,\"BURN_FAT_THRESHOLD\" INTEGER NOT NULL ,\"AEROBIC_THRESHOLD\" INTEGER NOT NULL ,\"LIMIT_THRESHOLD\" INTEGER NOT NULL ,\"BURN_FAT_MINS\" INTEGER NOT NULL ,\"AEROBIC_MINS\" INTEGER NOT NULL ,\"LIMIT_MINS\" INTEGER NOT NULL ,\"USER_MAX_HR\" INTEGER NOT NULL ,\"AVG_HR\" INTEGER NOT NULL ,\"AVG_DAY_HR\" INTEGER NOT NULL ,\"MAX_HR\" INTEGER NOT NULL ,\"MIN_HR\" INTEGER NOT NULL ,\"DATE\" INTEGER NOT NULL ,\"USER_ID\" TEXT,\"REMARK\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"HEALTH_HEART_RATE\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, HealthHeartRate healthHeartRate) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, healthHeartRate.getIsUploaded() ? 1 : 0);
        String macAddress = healthHeartRate.getMacAddress();
        if (macAddress != null) {
            databaseStatement.bindString(2, macAddress);
        }
        databaseStatement.bindLong(3, (long) healthHeartRate.getYear());
        databaseStatement.bindLong(4, (long) healthHeartRate.getMonth());
        databaseStatement.bindLong(5, (long) healthHeartRate.getDay());
        databaseStatement.bindLong(6, (long) healthHeartRate.getStartTime());
        databaseStatement.bindLong(7, (long) healthHeartRate.getSilentHeart());
        databaseStatement.bindLong(8, (long) healthHeartRate.getBurn_fat_threshold());
        databaseStatement.bindLong(9, (long) healthHeartRate.getAerobic_threshold());
        databaseStatement.bindLong(10, (long) healthHeartRate.getLimit_threshold());
        databaseStatement.bindLong(11, (long) healthHeartRate.getBurn_fat_mins());
        databaseStatement.bindLong(12, (long) healthHeartRate.getAerobic_mins());
        databaseStatement.bindLong(13, (long) healthHeartRate.getLimit_mins());
        databaseStatement.bindLong(14, (long) healthHeartRate.getUserMaxHr());
        databaseStatement.bindLong(15, (long) healthHeartRate.getAvgHr());
        databaseStatement.bindLong(16, (long) healthHeartRate.getAvgDayHr());
        databaseStatement.bindLong(17, (long) healthHeartRate.getMaxHr());
        databaseStatement.bindLong(18, (long) healthHeartRate.getMinHr());
        databaseStatement.bindLong(19, healthHeartRate.getDate());
        String userId = healthHeartRate.getUserId();
        if (userId != null) {
            databaseStatement.bindString(20, userId);
        }
        String remark = healthHeartRate.getRemark();
        if (remark != null) {
            databaseStatement.bindString(21, remark);
        }
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, HealthHeartRate healthHeartRate) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, healthHeartRate.getIsUploaded() ? 1 : 0);
        String macAddress = healthHeartRate.getMacAddress();
        if (macAddress != null) {
            sQLiteStatement.bindString(2, macAddress);
        }
        sQLiteStatement.bindLong(3, (long) healthHeartRate.getYear());
        sQLiteStatement.bindLong(4, (long) healthHeartRate.getMonth());
        sQLiteStatement.bindLong(5, (long) healthHeartRate.getDay());
        sQLiteStatement.bindLong(6, (long) healthHeartRate.getStartTime());
        sQLiteStatement.bindLong(7, (long) healthHeartRate.getSilentHeart());
        sQLiteStatement.bindLong(8, (long) healthHeartRate.getBurn_fat_threshold());
        sQLiteStatement.bindLong(9, (long) healthHeartRate.getAerobic_threshold());
        sQLiteStatement.bindLong(10, (long) healthHeartRate.getLimit_threshold());
        sQLiteStatement.bindLong(11, (long) healthHeartRate.getBurn_fat_mins());
        sQLiteStatement.bindLong(12, (long) healthHeartRate.getAerobic_mins());
        sQLiteStatement.bindLong(13, (long) healthHeartRate.getLimit_mins());
        sQLiteStatement.bindLong(14, (long) healthHeartRate.getUserMaxHr());
        sQLiteStatement.bindLong(15, (long) healthHeartRate.getAvgHr());
        sQLiteStatement.bindLong(16, (long) healthHeartRate.getAvgDayHr());
        sQLiteStatement.bindLong(17, (long) healthHeartRate.getMaxHr());
        sQLiteStatement.bindLong(18, (long) healthHeartRate.getMinHr());
        sQLiteStatement.bindLong(19, healthHeartRate.getDate());
        String userId = healthHeartRate.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(20, userId);
        }
        String remark = healthHeartRate.getRemark();
        if (remark != null) {
            sQLiteStatement.bindString(21, remark);
        }
    }

    public HealthHeartRate readEntity(Cursor cursor, int i) {
        Cursor cursor2 = cursor;
        boolean z = cursor2.getShort(i + 0) != 0;
        int i2 = i + 1;
        String string = cursor2.isNull(i2) ? null : cursor2.getString(i2);
        int i3 = cursor2.getInt(i + 2);
        int i4 = cursor2.getInt(i + 3);
        int i5 = cursor2.getInt(i + 4);
        int i6 = cursor2.getInt(i + 5);
        int i7 = cursor2.getInt(i + 6);
        int i8 = cursor2.getInt(i + 7);
        int i9 = cursor2.getInt(i + 8);
        int i10 = cursor2.getInt(i + 9);
        int i11 = cursor2.getInt(i + 10);
        int i12 = cursor2.getInt(i + 11);
        int i13 = cursor2.getInt(i + 12);
        int i14 = cursor2.getInt(i + 13);
        int i15 = cursor2.getInt(i + 14);
        int i16 = cursor2.getInt(i + 15);
        int i17 = cursor2.getInt(i + 16);
        int i18 = cursor2.getInt(i + 17);
        long j = cursor2.getLong(i + 18);
        int i19 = i + 19;
        String string2 = cursor2.isNull(i19) ? null : cursor2.getString(i19);
        int i20 = i + 20;
        return new HealthHeartRate(z, string, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18, j, string2, cursor2.isNull(i20) ? null : cursor2.getString(i20));
    }

    public void readEntity(Cursor cursor, HealthHeartRate healthHeartRate, int i) {
        healthHeartRate.setIsUploaded(cursor.getShort(i + 0) != 0);
        int i2 = i + 1;
        String str = null;
        healthHeartRate.setMacAddress(cursor.isNull(i2) ? null : cursor.getString(i2));
        healthHeartRate.setYear(cursor.getInt(i + 2));
        healthHeartRate.setMonth(cursor.getInt(i + 3));
        healthHeartRate.setDay(cursor.getInt(i + 4));
        healthHeartRate.setStartTime(cursor.getInt(i + 5));
        healthHeartRate.setSilentHeart(cursor.getInt(i + 6));
        healthHeartRate.setBurn_fat_threshold(cursor.getInt(i + 7));
        healthHeartRate.setAerobic_threshold(cursor.getInt(i + 8));
        healthHeartRate.setLimit_threshold(cursor.getInt(i + 9));
        healthHeartRate.setBurn_fat_mins(cursor.getInt(i + 10));
        healthHeartRate.setAerobic_mins(cursor.getInt(i + 11));
        healthHeartRate.setLimit_mins(cursor.getInt(i + 12));
        healthHeartRate.setUserMaxHr(cursor.getInt(i + 13));
        healthHeartRate.setAvgHr(cursor.getInt(i + 14));
        healthHeartRate.setAvgDayHr(cursor.getInt(i + 15));
        healthHeartRate.setMaxHr(cursor.getInt(i + 16));
        healthHeartRate.setMinHr(cursor.getInt(i + 17));
        healthHeartRate.setDate(cursor.getLong(i + 18));
        int i3 = i + 19;
        healthHeartRate.setUserId(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 20;
        if (!cursor.isNull(i4)) {
            str = cursor.getString(i4);
        }
        healthHeartRate.setRemark(str);
    }
}
