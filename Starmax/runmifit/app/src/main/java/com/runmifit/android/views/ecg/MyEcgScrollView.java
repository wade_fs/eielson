package com.wade.fit.views.ecg;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;
import com.tamic.novate.download.MimeType;
import java.util.ArrayList;
import java.util.List;

public class MyEcgScrollView extends HorizontalScrollView {
    private static float OFFSET_HEART_VALUE = 4096.0f;
    private static int offsetBS = 1;
    private static float startX;
    private static float x_changed;
    private int dataNum_per_grid = 60;
    private int data_num;
    public List<Float> data_source = new ArrayList();
    private float gap_grid;
    private float gap_x;
    private int grid_hori;
    private int grid_ver;
    private int height;
    private Paint mPaint01;
    protected OnScrollListener mScrollListener;
    private float multiple_for_rect_width;
    private final int offset = 1;
    private float offset_x_max;
    private float rect_gap_x;
    private int rect_high = 80;
    private float rect_width;
    private int width;
    private float x_change;
    private int xori;
    private float y_center;

    public interface OnScrollListener {
        void onScroll(int i);
    }

    public MyEcgScrollView(Context context) {
        super(context);
    }

    public MyEcgScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public MyEcgScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void init() {
        this.mPaint01 = new Paint();
        this.mPaint01.setColor(Color.parseColor("#FF0000"));
        this.mPaint01.setStyle(Paint.Style.STROKE);
        this.mPaint01.setStrokeWidth(2.0f);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (z) {
            this.xori = 0;
            this.gap_grid = 30.0f;
            this.width = getWidth();
            this.height = getHeight();
            int i5 = this.height;
            float f = this.gap_grid;
            this.grid_hori = i5 / ((int) f);
            this.grid_ver = this.width / ((int) f);
            this.y_center = (float) ((i5 - this.rect_high) / 2);
            this.gap_x = f / ((float) this.dataNum_per_grid);
            this.data_num = this.data_source.size();
            this.x_change = 0.0f;
            x_changed = 0.0f;
            int i6 = this.width;
            float f2 = this.gap_x;
            int i7 = this.data_num;
            this.offset_x_max = ((float) i6) - (((float) i7) * f2);
            this.rect_gap_x = ((float) i6) / ((float) i7);
            this.rect_width = (((float) i6) * ((float) i6)) / (f2 * ((float) i7));
            this.multiple_for_rect_width = ((float) i6) / this.rect_width;
            Log.e(MimeType.JSON, "本页面宽： " + this.width + "  高:" + this.height);
            Log.e(MimeType.JSON, "两点间横坐标间距:" + this.gap_x + "   矩形区域两点间横坐标间距：" + this.rect_gap_x);
        }
        super.onLayout(z, i, i2, i3, i4);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        drawing(canvas);
    }

    private void drawing(Canvas canvas) {
        Path path = new Path();
        x_changed += this.x_change;
        float f = x_changed;
        int i = this.xori;
        if (f > ((float) i)) {
            x_changed = (float) i;
        } else {
            float f2 = this.offset_x_max;
            if (f < f2) {
                x_changed = f2;
            }
        }
        int i2 = 1;
        int i3 = 1;
        while (true) {
            if (i3 >= this.data_source.size()) {
                break;
            }
            float f3 = ((float) this.xori) + (this.gap_x * ((float) i3)) + x_changed;
            if (f3 >= 0.0f) {
                path.moveTo(f3, ((float) (getHeight() / 2)) + ((this.data_source.get(i3).floatValue() * ((float) getHeight())) / OFFSET_HEART_VALUE));
                i2 = i3;
                break;
            }
            i3++;
        }
        while (i2 < this.data_source.size()) {
            float f4 = this.gap_x;
            float f5 = ((float) this.xori) + (((float) i2) * f4) + x_changed;
            if (f5 < ((float) this.width) + f4) {
                path.lineTo(f5, ((float) (getHeight() / 2)) + ((this.data_source.get(i2).floatValue() * ((float) getHeight())) / OFFSET_HEART_VALUE));
            }
            i2++;
        }
        canvas.drawPath(path, this.mPaint01);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            startX = motionEvent.getX();
            return true;
        } else if (action != 2) {
            return true;
        } else {
            this.x_change = motionEvent.getX() - startX;
            this.mScrollListener.onScroll((int) ((this.x_change * 10000.0f) / this.offset_x_max));
            invalidate();
            return true;
        }
    }

    public void setChange(int i) {
        this.x_change = (((float) i) * this.offset_x_max) / 10000.0f;
        invalidate();
    }

    public void setData(List<Float> list) {
        this.data_source = list;
        invalidate();
    }

    public void setmScrollListener(OnScrollListener onScrollListener) {
        this.mScrollListener = onScrollListener;
    }
}
