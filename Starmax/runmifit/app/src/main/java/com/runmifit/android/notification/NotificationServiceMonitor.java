package com.wade.fit.notification;

import android.os.Handler;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.service.IntelligentNotificationService;
import com.wade.fit.util.AppUtil;
import com.wade.fit.util.log.LogUtil;

public class NotificationServiceMonitor {
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    private Runnable monitorTask = new Runnable() {
        /* class com.wade.fit.notification.NotificationServiceMonitor.C23901 */

        public void run() {
            if (System.currentTimeMillis() - NotificationServiceMonitor.this.startTime > 30000) {
                NotificationServiceMonitor.this.closeMonitor();
            } else if (!AppUtil.isServiceRunning(AppApplication.getInstance(), IntelligentNotificationService.class.getName())) {
                LogUtil.dAndSave("IntelligentNotificationService未开启", Constants.NOTIFICATION_PATH);
                NotificationServiceMonitor.this.handler.postDelayed(this, 1000);
            } else {
                LogUtil.dAndSave("IntelligentNotificationService已开启", Constants.NOTIFICATION_PATH);
            }
        }
    };
    /* access modifiers changed from: private */
    public long startTime;

    public void startMonitor() {
        closeMonitor();
        this.startTime = System.currentTimeMillis();
        LogUtil.dAndSave("startMonitor 开始检测 IntelligentNotificationService服务", Constants.NOTIFICATION_PATH);
        this.handler.post(this.monitorTask);
    }

    /* access modifiers changed from: private */
    public void closeMonitor() {
        this.handler.removeCallbacks(this.monitorTask);
    }
}
