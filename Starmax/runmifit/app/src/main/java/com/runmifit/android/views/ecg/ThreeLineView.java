package com.wade.fit.views.ecg;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.wade.fit.R;

public class ThreeLineView extends View {
    private static final String TAG = ThreeLineView.class.getSimpleName();
    private float baseStartX;
    private int color_one;
    private int color_three;
    private int color_two;
    private int index_one;
    private int index_three;
    private int index_two;
    private int lineheight;
    protected float mHeightY;
    private boolean mIsFirstLoad;
    private int mMargin;
    private Paint mPaint;
    private int mPaintTimes;
    private int mTriangleHeight;
    private int mTriangleLength;
    protected float mWidthX;
    private float mbottom;
    private float moveOffset;
    private float mright;
    private float mtop;
    private Path path;
    private RectF rect;
    private RectF rectF;
    private int roundRectRadius;
    private Paint scorePaint;
    private int socre;
    protected float startX;

    /* renamed from: tB */
    float f5254tB;

    /* renamed from: tT */
    float f5255tT;
    private float textWidth;
    private int tipHeight;
    private int tipWidth;

    /* renamed from: tl */
    float f5256tl;

    /* renamed from: tr */
    float f5257tr;
    private Paint txtPaint;
    float txtX;
    float txtY;
    private int txt_color;
    private int txt_color_sorce;
    private String txt_one;
    private int txt_size_sorce;
    private int txt_sizeb;
    private String txt_three;
    private String txt_two;
    private String txt_up;
    private int txt_up_color;
    private int txt_up_size;

    /* renamed from: x1 */
    private float f5258x1;

    /* renamed from: x2 */
    private float f5259x2;

    /* renamed from: x3 */
    private float f5260x3;

    /* renamed from: y1 */
    private float f5261y1;

    /* renamed from: y2 */
    private float f5262y2;

    /* renamed from: y3 */
    private float f5263y3;

    public ThreeLineView(Context context) {
        this(context, null);
    }

    public ThreeLineView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ThreeLineView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mMargin = 0;
        this.socre = 0;
        this.mTriangleLength = dip2px(10.0f);
        this.mTriangleHeight = dip2px(5.0f);
        this.rectF = new RectF();
        this.mPaintTimes = 0;
        this.moveOffset = 0.0f;
        this.mIsFirstLoad = true;
        initAttributes(context, attributeSet);
        this.mPaint = new Paint();
        this.mPaint.setColor(this.color_one);
        this.mPaint.setStyle(Paint.Style.FILL);
        this.mPaint.setStrokeWidth(0.0f);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setTextAlign(Paint.Align.CENTER);
        this.mPaint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 0));
        this.rect = new RectF();
        this.path = new Path();
        this.roundRectRadius = dip2px(5.0f);
        this.txtPaint = new Paint();
        this.txtPaint.setStyle(Paint.Style.FILL);
        this.txtPaint.setStrokeWidth(1.0f);
        this.txtPaint.setAntiAlias(true);
        this.txtPaint.setTextAlign(Paint.Align.CENTER);
        this.txtPaint.setTypeface(Typeface.create(Typeface.SANS_SERIF, 0));
        this.scorePaint = new Paint(33);
    }

    private void initAttributes(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.LineView, 0, 0);
        try {
            this.color_one = obtainStyledAttributes.getColor(0, Color.parseColor("#ff871c"));
            this.color_two = obtainStyledAttributes.getColor(2, Color.parseColor("#ffcc1c"));
            this.color_three = obtainStyledAttributes.getColor(1, Color.parseColor("#a5e725"));
            this.lineheight = obtainStyledAttributes.getDimensionPixelSize(7, 8);
            this.index_one = obtainStyledAttributes.getInteger(4, 50);
            this.index_two = obtainStyledAttributes.getInteger(6, 80);
            this.index_three = obtainStyledAttributes.getInteger(5, 100);
            this.tipWidth = obtainStyledAttributes.getDimensionPixelSize(10, 45);
            this.tipHeight = obtainStyledAttributes.getDimensionPixelSize(9, 30);
            this.txt_color = obtainStyledAttributes.getColor(11, Color.parseColor("#666666"));
            this.txt_sizeb = obtainStyledAttributes.getDimensionPixelSize(15, 36);
            this.txt_size_sorce = obtainStyledAttributes.getDimensionPixelSize(14, 94);
            this.txt_color_sorce = obtainStyledAttributes.getColor(12, Color.parseColor("#666666"));
            this.txt_up_size = obtainStyledAttributes.getDimensionPixelSize(20, 34);
            this.txt_up_color = obtainStyledAttributes.getColor(19, Color.parseColor("#999999"));
            this.mMargin = obtainStyledAttributes.getDimensionPixelSize(8, 30);
            this.txt_one = obtainStyledAttributes.getString(13);
            this.txt_two = obtainStyledAttributes.getString(17);
            this.txt_three = obtainStyledAttributes.getString(16);
            this.txt_up = obtainStyledAttributes.getString(18);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        setMeasuredDimension(size, size2);
        int i3 = this.mMargin;
        this.mWidthX = (float) (size - i3);
        this.mHeightY = (float) size2;
        this.startX = (float) (i3 / 2);
        if (this.mIsFirstLoad) {
            this.baseStartX = this.startX;
        }
        this.mIsFirstLoad = false;
        float f = this.mHeightY;
        int i4 = this.lineheight;
        this.mtop = (f - ((float) i4)) / 2.0f;
        this.mright = this.mWidthX / 3.0f;
        this.mbottom = (f + ((float) i4)) / 2.0f;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mIsFirstLoad) {
            this.baseStartX = this.startX;
            this.mPaintTimes = 0;
        } else {
            int i = this.mPaintTimes;
            int i2 = this.index_one;
            if (i <= i2) {
                this.baseStartX = this.startX + (((float) i) * this.moveOffset);
            } else if (i <= i2 || i > this.index_two) {
                int i3 = this.mPaintTimes;
                int i4 = this.index_two;
                if (i3 > i4 && i3 <= this.index_three) {
                    this.baseStartX = this.startX + (this.mright * 2.0f) + (((float) (i3 - i4)) * this.moveOffset);
                }
            } else {
                this.baseStartX = this.startX + this.mright + (((float) (i - i2)) * this.moveOffset);
            }
        }
        this.mIsFirstLoad = false;
        this.mPaint.setColor(this.color_one);
        RectF rectF2 = this.rect;
        float f = this.startX;
        rectF2.set(f, this.mtop, this.mright + f, this.mbottom);
        canvas.drawRect(this.rect, this.mPaint);
        this.mPaint.setColor(this.color_two);
        RectF rectF3 = this.rect;
        float f2 = this.startX;
        float f3 = this.mright;
        rectF3.set(f2 + f3, this.mtop, f2 + (f3 * 2.0f), this.mbottom);
        canvas.drawRect(this.rect, this.mPaint);
        this.mPaint.setColor(this.color_three);
        RectF rectF4 = this.rect;
        float f4 = this.startX;
        rectF4.set((this.mright * 2.0f) + f4, this.mtop, f4 + this.mWidthX, this.mbottom);
        canvas.drawRect(this.rect, this.mPaint);
        this.txtPaint.setColor(this.txt_color);
        this.txtPaint.setTextSize((float) this.txt_sizeb);
        canvas.drawText(this.txt_one, this.startX + (this.mright / 2.0f), this.mbottom + ((float) (this.lineheight * 5)), this.txtPaint);
        String str = this.txt_two;
        float f5 = this.startX;
        float f6 = this.mright;
        canvas.drawText(str, f5 + f6 + (f6 / 2.0f), this.mbottom + ((float) (this.lineheight * 5)), this.txtPaint);
        String str2 = this.txt_three;
        float f7 = this.startX;
        float f8 = this.mright;
        canvas.drawText(str2, f7 + (f8 * 2.0f) + (f8 / 2.0f), this.mbottom + ((float) (this.lineheight * 5)), this.txtPaint);
        float f9 = this.baseStartX;
        int i5 = this.mTriangleLength;
        this.f5258x1 = ((float) (i5 / 2)) + f9;
        float f10 = this.mtop;
        int i6 = this.mTriangleHeight;
        this.f5261y1 = (f10 - 10.0f) - ((float) i6);
        this.f5259x2 = f9;
        this.f5262y2 = f10 - 10.0f;
        this.f5260x3 = f9 - ((float) (i5 / 2));
        this.f5263y3 = (f10 - 10.0f) - ((float) i6);
        float f11 = this.f5259x2;
        int i7 = this.tipWidth;
        this.f5256tl = f11 - ((float) (i7 / 2));
        this.f5257tr = f11 + ((float) (i7 / 2));
        float f12 = this.f5261y1;
        this.f5255tT = f12 - ((float) this.tipHeight);
        this.f5254tB = f12 + 1.0f;
        float f13 = this.f5256tl;
        this.txtX = f13 + ((this.f5257tr - f13) / 2.0f);
        float f14 = this.f5255tT;
        this.txtY = f14 + ((this.f5254tB - f14) / 2.0f);
        this.path.reset();
        this.path.moveTo(this.f5258x1, this.f5261y1);
        this.path.lineTo(this.f5259x2, this.f5262y2);
        this.path.lineTo(this.f5260x3, this.f5263y3);
        this.path.close();
        int i8 = this.mPaintTimes;
        int i9 = this.index_one;
        if (i8 <= i9) {
            this.mPaint.setColor(this.color_one);
            this.moveOffset = this.mright / ((float) this.index_one);
            canvas.drawPath(this.path, this.mPaint);
            this.rectF.set(this.f5256tl, this.f5255tT, this.f5257tr, this.f5254tB);
            RectF rectF5 = this.rectF;
            int i10 = this.roundRectRadius;
            canvas.drawRoundRect(rectF5, (float) i10, (float) i10, this.mPaint);
            this.txtPaint.setColor(this.txt_color_sorce);
            this.txtPaint.setTextSize((float) this.txt_size_sorce);
            Paint paint = this.txtPaint;
            this.textWidth = paint.measureText(this.mPaintTimes + "");
            canvas.drawText(this.mPaintTimes + "", this.f5259x2, this.f5254tB - ((float) (this.tipHeight / 4)), this.txtPaint);
            int i11 = this.mPaintTimes;
            if (i11 < this.socre) {
                this.baseStartX = this.startX + (((float) i11) * this.moveOffset);
            }
        } else if (i8 <= i9 || i8 > this.index_two) {
            int i12 = this.mPaintTimes;
            if (i12 > this.index_two && i12 <= this.index_three) {
                this.mPaint.setColor(this.color_three);
                this.moveOffset = this.mright / ((float) (this.index_three - this.index_two));
                canvas.drawPath(this.path, this.mPaint);
                this.rectF.set(this.f5256tl, this.f5255tT, this.f5257tr, this.f5254tB);
                RectF rectF6 = this.rectF;
                int i13 = this.roundRectRadius;
                canvas.drawRoundRect(rectF6, (float) i13, (float) i13, this.mPaint);
                this.txtPaint.setColor(this.txt_color_sorce);
                this.txtPaint.setTextSize((float) this.txt_size_sorce);
                Paint paint2 = this.txtPaint;
                this.textWidth = paint2.measureText(this.mPaintTimes + "");
                canvas.drawText(this.mPaintTimes + "", this.f5259x2, this.f5254tB - ((float) (this.tipHeight / 4)), this.txtPaint);
                int i14 = this.mPaintTimes;
                if (i14 < this.socre) {
                    this.baseStartX = this.startX + (this.mright * 2.0f) + (((float) (i14 - this.index_two)) * this.moveOffset);
                }
            }
        } else {
            this.mPaint.setColor(this.color_two);
            this.moveOffset = this.mright / ((float) (this.index_two - this.index_one));
            canvas.drawPath(this.path, this.mPaint);
            this.rectF.set(this.f5256tl, this.f5255tT, this.f5257tr, this.f5254tB);
            RectF rectF7 = this.rectF;
            int i15 = this.roundRectRadius;
            canvas.drawRoundRect(rectF7, (float) i15, (float) i15, this.mPaint);
            this.txtPaint.setColor(this.txt_color_sorce);
            this.txtPaint.setTextSize((float) this.txt_size_sorce);
            Paint paint3 = this.txtPaint;
            this.textWidth = paint3.measureText(this.mPaintTimes + "");
            canvas.drawText(this.mPaintTimes + "", this.f5259x2, this.f5254tB - ((float) (this.tipHeight / 4)), this.txtPaint);
            int i16 = this.mPaintTimes;
            if (i16 < this.socre) {
                this.baseStartX = this.startX + this.mright + (((float) (i16 - this.index_one)) * this.moveOffset);
            }
        }
        int i17 = this.mPaintTimes;
        if (i17 < this.socre) {
            this.mPaintTimes = i17 + 1;
            postInvalidate();
        }
    }

    public synchronized void setSorce(int i) {
        if (i > 0 && i <= 100) {
            this.mIsFirstLoad = true;
            this.socre = i;
            postInvalidate();
        }
    }

    public int dip2px(float f) {
        return (int) ((f * getContext().getResources().getDisplayMetrics().density) + 0.5f);
    }

    public int sp2px(float f) {
        return (int) ((f * getContext().getResources().getDisplayMetrics().density) + 0.5f);
    }
}
