package com.wade.fit.util;

import android.content.Context;
import com.wade.fit.util.log.DebugLog;
import com.tencent.bugly.beta.tinker.TinkerReport;
import java.util.Calendar;

public class AppSharedPreferencesUtils extends CommonPreferences {
    private static String AGPS_CHECK_CODE = "AGPS_CHECK_CODE";
    private static final String BIND_DEVICE_ADDR = "bind_device_addr";
    private static final String BIND_TEMP_DEVICE_ADDR = "BIND_TEMP_DEVICE_ADDR";
    private static final String BLE_CONNECT_STATE = "BLE_CONNECT_STATE";
    private static final String DEFAULT_WEEK_START_INDEX = "DEFAULT_WEEK_START_INDEX";
    private static String DEVICE_HEART_RATE_SWITCH = "DEVICE_HEART_RATE_SWITCH";
    private static String DEVICE_HEART_RATE_VALUE = "DEVICE_HEART_RATE_VALUE";
    private static final String DEVICE_NEED_SYNC_DATA = "DEVICE_NEED_SYNC_DATA";
    private static final String DEVICE_SPORT_SET = "DEVICE_SPORT_SET";
    private static final String DEVICE_SYNC_END_TIME = "device_sync_end_time";
    private static final String DEVICE_SYNC_END_TIME_MINS = "DEVICE_SYNC_END_TIME_MINS";
    private static final String DEVICE_UPDATE_PRESS = "DEVICE_UPDATE_PRESS";
    private static final String FIRST_FINISH = "FIRST_FINISH";
    private static final String FIRST_START_APP = "first_start_app";
    private static final String FIRST_SYNC = "FIRST_SYNC";
    private static final String FIRST_UPDATE_APP = "first_update_app";
    private static final String FIST_READ_FILE = "FIST_READ_FILE";
    private static final String FULL_SCREEN = "FULL_SCREEN";
    private static String Heart_TIME_DISTANCE = "Heart_TIME_DISATACE";
    private static final String IS_24_HOUR = "IS_24_HOUR";
    private static final String IS_FLASH_DATA = "IS_FLASH_DATA";
    private static String LAST_STEP_CAL = "LAST_STEP_CAL";
    private static String LAST_STEP_COUNT = "LAST_STEP_COUNT";
    private static String LAST_STEP_DISTANCE = "LAST_STEP_DISTANCE";
    private static String LAST_STEP_ITEM = "LAST_STEP_ITEM";
    private static String LAST_SYNC_TIME = "LAST_SYNC_TIME";
    private static final String LOG_DEVICE_SWITCH_STATE = "LOG_DEVICE_SWITCH_STATE";
    private static final String LOG_SWITCH_STATE = "LOG_SWITCH_STATE";
    private static final String LONG_REMIND_DIALOG_TAG = "LONG_REMIND_DIALOG_TAG";
    private static final String MAIN_ACTION_OPEN = "MAIN_ACTION_OPEN";
    private static String MAP_SOURCES = "MAP_SOURCES";
    private static final String MOVE_TASK_TOBACK = "MOVE_TASK_TOBACK";
    private static final String MO_NI_HEART_RATE_VALUE = "MO_NI_HEART_RATE_VALUE";
    private static final String MUSIC_PLAY_PACKAGE_NAME = "MUSIC_PLAY_PACKAGE_NAME";
    private static final String MUSIC_PLAY_TYPE = "MUSIC_PLAY_TYPE";
    private static String New_Lift_Wrist = "New_Lift_Wrist";
    private static final String PAGE_DATA_INDEX = "PAGE_DATA_INDEX";
    private static final String PHONE_VALUE = "PHONE_VALUE";
    public static final String REAL_STEP = "REAL_STEP";
    private static final String REBIND_DEVICE = "REBIND_DEVICE";
    private static final String RUN_CHANGE_TYPE = "RUN_CHANGE_TYPE";
    private static final String RUN_CMD_START_TIME = "RUN_CMD_START_TIME";
    private static final String SAVE_DEVICE_ID = "SAVE_DEVICE_ID";
    private static final String SAVE_DEVICE_NAME = "SAVE_DEVICE_NAME";
    private static final String SAVE_FREQUENCY_SETTING = "SAVE_FREQUENCY_SETTING";
    private static final String SAVE_GPS_SWITCH = "SAVE_GPS_SWITCH";
    private static final String SAVE_HEALTH_SLEEP_DATA = "SAVE_HEALTH_SLEEP_DATA";
    private static final String SAVE_HISTORY_RECORD_DETAILS_DATA = "SAVE_HISTORY_RECORD_DETAILS_DATA";
    private static final String SAVE_HISTORY_RECORD_HR_DATA = "SAVE_HISTORY_RECORD_HR_DATA";
    private static final String SAVE_OPEN_TIPS_MUSIC = "SAVE_OPEN_TIPS_MUSIC";
    private static final String SAVE_REST_HEART_RATE_DATA = "SAVE_REST_HEART_RATE_DATA";
    private static final String SAVE_SPORT_TYPE = "SAVE_SPORT_TYPE";
    private static final String SAVE_STOP_SPORT_TIME = "SAVE_STOP_SPORT_TIME";
    private static final String SAVE_SWITCH_DATA_APP_START = "SAVE_SWITCH_DATA_APP_START";
    private static final String SAVE_SWITCH_DATA_BLE_START = "SAVE_SWITCH_DATA_BLE_START";
    private static final String SAVE_TARGET_WEIGHT = "SAVE_TARGET_WEIGHT";
    private static final String SAVE_TARGET_WEIGHT_BRITISH = "SAVE_TARGET_WEIGHT_METRIC";
    private static final String SAVE_TARGET_WEIGHT_ST = "SAVE_TARGET_WEIGHT_ST";
    private static final String SAVE_TOGGLE_SWITCH_STATE = "SAVE_TOGGLE_SWITCH_STATE";
    private static final String SAVE_WEIGHT_UNIT = "SAVE_WEIGHT_UNIT";
    private static final String SCREEN_RAW_HEIGHT = "SCREEN_RAW_HEIGHT";
    private static final String SEND_MUSIC_CMD = "SEND_MUSIC_CMD";
    private static final String SET_BRITISH_RETURN_VALUE = "SET_BRITISH_RETURN_VALUE";
    private static final String SET_RETURN_TITLE = "SET_RETURN_TITLE";
    private static final String SET_RETURN_VALUE = "SET_RETURN_VALUE";
    private static final String SET_STRIDE = "SET_STRIDE";
    private static final String SET__RUN_STRIDE = "SET__RUN_STRIDE";
    private static final String SINGLE_RUN_CMD_START_TIME = "SINGLE_RUN_CMD_START_TIME";
    private static final String SLEEP_GOAL = "SLEEP_GOAL";
    private static final String SPORT_ANIMATION_SWITH = "SPORT_ANIMATION_SWITH";
    private static final String SPORT_GOAL = "SPORT_GOAL";
    private static final String SPORT_TOTAL_ACTIVETIME = "SPORT_TOTAL_ACTIVETIME";
    private static final String SPORT_TOTAL_DISTANCE = "SPORT_TOTAL_DISTANCE";
    private static final String SPORT_TOTAL_STEP = "SPORT_TOTAL_STEP";
    private static final String SP_NAME = "smart_android";
    private static final String START_TIMER = "START_TIMER";
    private static final String SUPPORT_ALARM_NUM = "SUPPORT_ALARM_NUM";
    private static String Sleep_TIME_DISTANCE = "Sleep_TIME_DISATACE";
    private static final String TARGET_SETTING_BRITISH_VALUE = "TARGET_SETTING_BRITISH_VALUE";
    private static final String TARGET_SETTING_TYPE = "TARGET_SETTING_TYPE";
    private static final String TARGET_SETTING_VALUE = "TARGET_SETTING_VALUE";
    private static String TIME_DISTANCE = "Disturb_TIME_DISATACE";
    private static String TIME_MODE = "TIME_MODE";
    private static final String TIME_ZONE = "TIME_ZONE";
    private static final String USER_BIRTHDAY = "USER_BIRTHDAY";
    private static final String USER_GENDER = "USER_GENDER";
    private static final String USER_HEIGHT = "USER_HEIGHT";
    private static final String USER_HEIGHT_BRITISH = "USER_HEIGHT_BRITISH";
    private static final String USER_WEIGHT = "USER_WEIGHT";
    private static final String USER_WEIGHT_British = "USER_WEIGHT_British";
    private static final String USER_WEIGHT_ST = "USER_WEIGHT_ST";
    private static String VOICE_PROMPT_DATA = "VOICE_PROMPT_DATA";
    private static String VOICE_PROMPT_TONE = "VOICE_PROMPT_TONE";
    private static String VOICE_PROMPT_TYPE = "VOICE_PROMPT_TYPE";
    private static String VOICE_PROMPT_VALUE = "VOICE_PROMPT_VALUE";
    private static final String WEEK_START_INDEX = "WEEK_START_INDEX";
    private static final String WEIGHT_DATA = "WEIGHT_DATA";
    private static String Wrist_TIME_DISTANCE = "Wrist_TIME_DISATACE";
    private static AppSharedPreferencesUtils instance;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean isFirstSync() {
        return getValue("FIRST_SYNC", true);
    }

    public void setFirstSync(boolean z) {
        setValue("FIRST_SYNC", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean isFirstStartApp() {
        return getValue(FIRST_START_APP, true);
    }

    public void setFirstStartApp(boolean z) {
        setValue(FIRST_START_APP, z);
    }

    private AppSharedPreferencesUtils() {
    }

    public static final AppSharedPreferencesUtils getInstance() {
        if (instance == null) {
            instance = new AppSharedPreferencesUtils();
        }
        return instance;
    }

    public void init(Context context) {
        super.init(context, SP_NAME);
    }

    public void setDeviceHeartRateValue(int i) {
        setValue(DEVICE_HEART_RATE_VALUE, i);
    }

    public int getDeviceHeartRateValue() {
        return getValue(DEVICE_HEART_RATE_VALUE, (int) TinkerReport.KEY_APPLIED_VERSION_CHECK);
    }

    public void setDeviceHeartRateSwitch(boolean z) {
        setValue(DEVICE_HEART_RATE_SWITCH, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getDeviceHeartRateSwitch() {
        return getValue(DEVICE_HEART_RATE_SWITCH, false);
    }

    public void setLastSyncTime(long j) {
        setValue(LAST_SYNC_TIME, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long */
    public long getLastSyncTime() {
        return getValue(LAST_SYNC_TIME, 0L);
    }

    public void setFirstUpdateApp(boolean z) {
        setValue(FIRST_UPDATE_APP, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getFirstupdateApp() {
        return getValue(FIRST_UPDATE_APP, false);
    }

    public void setBindDeviceAddr(String str) {
        DebugLog.m6209i("设置BindDeviceAddr：" + str);
        setValue(BIND_DEVICE_ADDR, str);
    }

    public String getBindDeviceAddr() {
        return getValue(BIND_DEVICE_ADDR, "");
    }

    public void setBindDeviceAddrTemp(String str) {
        setValue(BIND_TEMP_DEVICE_ADDR, str);
    }

    public String getBindDeviceAddrTemp() {
        return getValue(BIND_TEMP_DEVICE_ADDR, "");
    }

    public void setDeviceSyncEndTime(Calendar calendar) {
        setValue(DEVICE_SYNC_END_TIME_MINS, calendar.get(12) + (calendar.get(11) * 60));
        setValue(DEVICE_SYNC_END_TIME, String.format("%02d/%02d/%02d ", Integer.valueOf(calendar.get(1) % 1000), Integer.valueOf(calendar.get(2) + 1), Integer.valueOf(calendar.get(5))));
    }

    public Object[] getDeviceSyncEndTime() {
        return new Object[]{getValue((String) DEVICE_SYNC_END_TIME, ""), Integer.valueOf(getValue((String) DEVICE_SYNC_END_TIME_MINS, 0))};
    }

    public void setSyncData(boolean z) {
        setValue(DEVICE_NEED_SYNC_DATA, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean isSyncData() {
        return getValue(DEVICE_NEED_SYNC_DATA, false);
    }

    public void setMoNiHeartRateValue(String str) {
        setValue(MO_NI_HEART_RATE_VALUE, str);
    }

    public String getMoNiHeartRateValue() {
        return getValue(MO_NI_HEART_RATE_VALUE, "");
    }

    public void setAnimationSwith(boolean z) {
        setValue(SPORT_ANIMATION_SWITH, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getAnimationSwith() {
        return getValue(SPORT_ANIMATION_SWITH, true);
    }

    public void setBLEConnectState(boolean z) {
        setValue(BLE_CONNECT_STATE, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getBLEConnectState() {
        return getValue(BLE_CONNECT_STATE, false);
    }

    public int getDeviceId() {
        return getValue(SAVE_DEVICE_ID, 0);
    }

    public void setDeviceId(int i) {
        setValue(SAVE_DEVICE_ID, i);
    }

    public int getDeviceUpdateProgress() {
        return getValue(DEVICE_UPDATE_PRESS, 0);
    }

    public void setDeviceUpdateProgress(int i) {
        setValue(DEVICE_UPDATE_PRESS, i);
    }

    public int getSupportAlarmNum() {
        return getValue("SUPPORT_ALARM_NUM", 10);
    }

    public void setSupportAlarmNum(int i) {
        setValue("SUPPORT_ALARM_NUM", i);
    }

    public int getPhoneValue() {
        return getValue(PHONE_VALUE, 3);
    }

    public void setPhoneValue(int i) {
        setValue(PHONE_VALUE, i);
    }

    public int getUserGender() {
        return getValue(USER_GENDER, 0);
    }

    public void setUserGender(int i) {
        setValue(USER_GENDER, i);
    }

    public int getUserBirthday() {
        return getValue(USER_BIRTHDAY, TimeUtil.getCurrentYear() - 25);
    }

    public void setUserBirthday(int i) {
        setValue(USER_BIRTHDAY, i);
    }

    public int getUserHeight() {
        return getValue(USER_HEIGHT, 175);
    }

    public void setUserHeight(int i) {
        setValue(USER_HEIGHT, i);
    }

    public int getUserHeightBritish() {
        return getValue(USER_HEIGHT_BRITISH, 69);
    }

    public void setUserHeightBritish(int i) {
        setValue(USER_HEIGHT_BRITISH, i);
    }

    public int getUserWeight() {
        return getValue(USER_WEIGHT, 65);
    }

    public void setUserWeight(int i) {
        setValue(USER_WEIGHT, i);
    }

    public int getUserWeightBritish() {
        return getValue(USER_WEIGHT_British, 174);
    }

    public void setUserWeightBritish(int i) {
        setValue(USER_WEIGHT_British, i);
    }

    public int getUserWeightST() {
        return getValue(USER_WEIGHT_ST, 10);
    }

    public void setUserWeightST(int i) {
        setValue(USER_WEIGHT_ST, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getFirstSportSet() {
        return getValue("DEVICE_SPORT_SET", true);
    }

    public void setFirstSportSet(boolean z) {
        setValue("DEVICE_SPORT_SET", z);
    }

    public String getMusicPlayType() {
        return getValue(MUSIC_PLAY_TYPE, "");
    }

    public void setMusicPlayType(String str) {
        setValue(MUSIC_PLAY_TYPE, str);
    }

    public String getMusicPlayPackageName() {
        return getValue(MUSIC_PLAY_PACKAGE_NAME, "");
    }

    public void setMusicPlayPackageName(String str) {
        setValue(MUSIC_PLAY_PACKAGE_NAME, str);
    }

    public int getPageDataIndex() {
        return getValue(PAGE_DATA_INDEX, 0);
    }

    public void setPageDataIndex(int i) {
        setValue(PAGE_DATA_INDEX, i);
    }

    public void setActionOpen(boolean z) {
        setValue(MAIN_ACTION_OPEN, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getActionOpen() {
        return getValue(MAIN_ACTION_OPEN, false);
    }

    public void setSportGoal(int i) {
        setValue(SPORT_GOAL, i);
    }

    public int getSportGoal() {
        return getValue(SPORT_GOAL, 10000);
    }

    public void setSleepGoal(int i) {
        setValue(SLEEP_GOAL, i);
    }

    public int getSleepGoal() {
        return getValue(SLEEP_GOAL, 480);
    }

    public void setIs24Hour(boolean z) {
        setValue(IS_24_HOUR, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getIs24Hour() {
        return getValue(IS_24_HOUR, false);
    }

    public void setRealStep(int i) {
        setValue(REAL_STEP, i);
    }

    public int getRealStep() {
        return getValue(REAL_STEP, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getLogSwitchState() {
        return getValue(LOG_SWITCH_STATE, true);
    }

    public void setLogSwitchState(boolean z) {
        setValue(LOG_SWITCH_STATE, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getDeviceLogSwitchState() {
        return getValue(LOG_DEVICE_SWITCH_STATE, true);
    }

    public void setDeviceLogSwitchState(boolean z) {
        setValue(LOG_DEVICE_SWITCH_STATE, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getStartTimer() {
        return getValue(START_TIMER, false);
    }

    public void setStartTimer(boolean z) {
        setValue(START_TIMER, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getFullScreen() {
        return getValue(FULL_SCREEN, false);
    }

    public void setFullScreen(boolean z) {
        setValue(FULL_SCREEN, z);
    }

    public String getDeviceName() {
        return getValue(SAVE_DEVICE_NAME, "");
    }

    public void setDeviceName(String str) {
        setValue(SAVE_DEVICE_NAME, str);
    }

    public void setFirstFinish(boolean z) {
        setValue(FIRST_FINISH, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getFirstFinish() {
        return getValue(FIRST_FINISH, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float */
    public float getScreenRawHeight() {
        return getValue(SCREEN_RAW_HEIGHT, 0.0f);
    }

    public void setScreenRawHeight(float f) {
        setValue(SCREEN_RAW_HEIGHT, f);
    }

    public void setDialogTag(String str) {
        setValue(LONG_REMIND_DIALOG_TAG, str);
    }

    public String getDialogTag() {
        return getValue(LONG_REMIND_DIALOG_TAG, "");
    }

    public void setTotalCalory(int i) {
        setValue(SPORT_TOTAL_STEP, i);
    }

    public int getTotalCalory() {
        return getValue(SPORT_TOTAL_STEP, 0);
    }

    public void setTotalDistance(int i) {
        setValue(SPORT_TOTAL_DISTANCE, i);
    }

    public int getTotalDistance() {
        return getValue(SPORT_TOTAL_DISTANCE, 0);
    }

    public void setTotalActiveTime(int i) {
        setValue(SPORT_TOTAL_ACTIVETIME, i);
    }

    public int getTotalActiveTime() {
        return getValue(SPORT_TOTAL_ACTIVETIME, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getSendMusicCmd() {
        return getValue(SEND_MUSIC_CMD, false);
    }

    public void setSendMusicCmd(boolean z) {
        setValue(SEND_MUSIC_CMD, z);
    }

    public void setWeekStartIndex(int i) {
        setValue("WEEK_START_INDEX", i);
    }

    public int getWeekStartIndex() {
        return getValue("WEEK_START_INDEX", 1);
    }

    public void setDefaultWeekStartIndex(int i) {
        setValue(DEFAULT_WEEK_START_INDEX, i);
    }

    public int getDefaultWeekStartIndex() {
        return getValue(DEFAULT_WEEK_START_INDEX, 1);
    }

    public void setTargetSettingValue(String str) {
        setValue(TARGET_SETTING_VALUE, str);
    }

    public String getTargetSettingValue() {
        return getValue(TARGET_SETTING_VALUE, "");
    }

    public void setBritishTargetSettingValue(String str) {
        setValue(TARGET_SETTING_BRITISH_VALUE, str);
    }

    public String getBritishTargetSettingValue() {
        return getValue(TARGET_SETTING_BRITISH_VALUE, "");
    }

    public int getRunChangeType() {
        return getValue(RUN_CHANGE_TYPE, 1);
    }

    public void setRunChangeType(int i) {
        setValue(RUN_CHANGE_TYPE, i);
    }

    public int getSportType() {
        return getValue(SAVE_SPORT_TYPE, 0);
    }

    public void setSportType(int i) {
        setValue(SAVE_SPORT_TYPE, i);
    }

    public void setReturnValue(String str) {
        setValue(SET_RETURN_VALUE, str);
    }

    public String getReturnValue() {
        return getValue(SET_RETURN_VALUE, "");
    }

    public void setBritishReturnValue(String str) {
        setValue(SET_BRITISH_RETURN_VALUE, str);
    }

    public String getBritishReturnValue() {
        return getValue(SET_BRITISH_RETURN_VALUE, "");
    }

    public void setReturnTitle(int i) {
        setValue(SET_RETURN_TITLE, i);
    }

    public int getReturnTitle() {
        return getValue(SET_RETURN_TITLE, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getSportModelGpsSwitch() {
        return getValue(SAVE_GPS_SWITCH, false);
    }

    public void setSportModelGpsSwitch(boolean z) {
        setValue(SAVE_GPS_SWITCH, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float */
    public float getTargetWeight() {
        return getValue(SAVE_TARGET_WEIGHT, 60.0f);
    }

    public void setTargetWeight(float f) {
        setValue(SAVE_TARGET_WEIGHT, f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float */
    public float getTargetWeightBritish() {
        return getValue(SAVE_TARGET_WEIGHT_BRITISH, 132.0f);
    }

    public void setTargetWeightBritish(float f) {
        setValue(SAVE_TARGET_WEIGHT_BRITISH, f);
    }

    public float getTargetWeightSt() {
        return (float) getValue(SAVE_TARGET_WEIGHT_ST, 10);
    }

    public void setTargetWeightSt(int i) {
        setValue(SAVE_TARGET_WEIGHT_ST, i);
    }

    public String getRunCmdStartTime() {
        return getValue(RUN_CMD_START_TIME, "");
    }

    public void setRunCmdStartTime(String str) {
        setValue(RUN_CMD_START_TIME, str);
    }

    public String getSaveSwitchDataAppStart() {
        return getValue(SAVE_SWITCH_DATA_APP_START, "");
    }

    public void setSaveSwitchDataAppStart(String str) {
        setValue(SAVE_SWITCH_DATA_APP_START, str);
    }

    public String getSaveSwitchDataBleStart() {
        return getValue(SAVE_SWITCH_DATA_BLE_START, "");
    }

    public void setSaveSwitchDataBleStart(String str) {
        setValue(SAVE_SWITCH_DATA_BLE_START, str);
    }

    public String getWeightUnit() {
        return getValue(SAVE_WEIGHT_UNIT, "");
    }

    public void setWeightUnit(String str) {
        setValue(SAVE_WEIGHT_UNIT, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean isFirstReadFile() {
        return getValue(FIST_READ_FILE, true);
    }

    public void setFirstReadFile(boolean z) {
        setValue(FIST_READ_FILE, z);
    }

    public void setStopSportTime(long j) {
        setValue(SAVE_STOP_SPORT_TIME, j);
    }

    public long setStopSportTime() {
        return (long) getValue(SAVE_STOP_SPORT_TIME, 0);
    }

    public void setSingleRunCmdStartTime(long j) {
        setValue(SINGLE_RUN_CMD_START_TIME, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long */
    public long getSingleRunCmdStartTime() {
        return getValue(SINGLE_RUN_CMD_START_TIME, 0L);
    }

    public void setWeightData(String str) {
        setValue(WEIGHT_DATA, str);
    }

    public String getWeightData() {
        return getValue(WEIGHT_DATA, "");
    }

    public void setRebindDevice(boolean z) {
        setValue(REBIND_DEVICE, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getRebindDevice() {
        return getValue(REBIND_DEVICE, false);
    }

    public void setHistoryRecordDetailsDatas(String str) {
        setValue(SAVE_HISTORY_RECORD_DETAILS_DATA, str);
    }

    public String getHistoryRecordDetailsDatas() {
        return getValue(SAVE_HISTORY_RECORD_DETAILS_DATA, "");
    }

    public void setHistoryRecordHeartRateDatas(String str) {
        setValue(SAVE_HISTORY_RECORD_HR_DATA, str);
    }

    public String getHistoryRecordHeartRateDatas() {
        return getValue(SAVE_HISTORY_RECORD_HR_DATA, "");
    }

    public String getRestHeartRateData() {
        return getValue(SAVE_REST_HEART_RATE_DATA, "");
    }

    public void setRestHeartRateData(String str) {
        setValue(SAVE_REST_HEART_RATE_DATA, str);
    }

    public String getSaveHealthSleepData() {
        return getValue(SAVE_HEALTH_SLEEP_DATA, "");
    }

    public void setSaveHealthSleepData(String str) {
        setValue(SAVE_HEALTH_SLEEP_DATA, str);
    }

    public void setToggleSwitchState(boolean z) {
        setValue(SAVE_TOGGLE_SWITCH_STATE, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getToggleSwitchState() {
        return getValue(SAVE_TOGGLE_SWITCH_STATE, false);
    }

    public void setFrequencySetting(String str) {
        setValue(SAVE_FREQUENCY_SETTING, str);
    }

    public String getFrequencySetting() {
        return getValue(SAVE_FREQUENCY_SETTING, "");
    }

    public void setOpenTipsMusic(boolean z) {
        setValue(SAVE_OPEN_TIPS_MUSIC, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getOpenTipsMusic() {
        return getValue(SAVE_OPEN_TIPS_MUSIC, true);
    }

    public void setTimeZone(String str) {
        setValue(TIME_ZONE, str);
    }

    public String getTimeZone() {
        return getValue(TIME_ZONE, "");
    }

    public void setIsFlashData(boolean z) {
        setValue(IS_FLASH_DATA, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean isFlashData() {
        return getValue(IS_FLASH_DATA, true);
    }

    public void setMoveTaskToBack(boolean z) {
        setValue(MOVE_TASK_TOBACK, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getMoveTaskToBack() {
        return getValue(MOVE_TASK_TOBACK, false);
    }

    public void setStride(int i) {
        setValue(SET_STRIDE, i);
    }

    public int getStride() {
        try {
            return getValue(SET_STRIDE, 0);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void setRunStride(int i) {
        setValue(SET__RUN_STRIDE, i);
    }

    public int getRunStride() {
        try {
            return getValue(SET__RUN_STRIDE, 0);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void setLastStepCount(int i) {
        setValue(LAST_STEP_COUNT, i);
    }

    public int getLastStepCount() {
        return getValue(LAST_STEP_COUNT, 0);
    }

    public void setLastStepDistance(float f) {
        setValue(LAST_STEP_DISTANCE, f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float */
    public float getLastStepDistance() {
        return getValue(LAST_STEP_DISTANCE, 0.0f);
    }

    public void setLastStepCal(float f) {
        setValue(LAST_STEP_CAL, f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float */
    public float getLastStepCal() {
        return getValue(LAST_STEP_CAL, 0.0f);
    }

    public void setLastStepItem(int i) {
        setValue(LAST_STEP_ITEM, i);
    }

    public int getLastStepItem() {
        return getValue(LAST_STEP_ITEM, 0);
    }

    public void setTimemode(int i) {
        setValue(TIME_MODE, i);
    }

    public int getTimemode() {
        return getValue(TIME_MODE, 0);
    }

    public void setDisturbTime(String str) {
        setValue(TIME_DISTANCE, str);
    }

    public String getDisturbTime() {
        return getValue(TIME_DISTANCE, "");
    }

    public void setWristTime(String str) {
        setValue(Wrist_TIME_DISTANCE, str);
    }

    public String getWristTime() {
        return getValue(Wrist_TIME_DISTANCE, "");
    }

    public void setHeartTime(String str) {
        setValue(Heart_TIME_DISTANCE, str);
    }

    public String getHearttTime() {
        return getValue(Heart_TIME_DISTANCE, "");
    }

    public void setSleepTime(String str) {
        setValue(Sleep_TIME_DISTANCE, str);
    }

    public String getSleeptTime() {
        return getValue(Sleep_TIME_DISTANCE, "");
    }

    public void setNewWrist(boolean z) {
        setValue(New_Lift_Wrist, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean getNewWrist() {
        return getValue(New_Lift_Wrist, true);
    }

    public String getAgpsCheckCode() {
        return getValue(AGPS_CHECK_CODE, "");
    }

    public void setAgpsCheckCode(String str) {
        setValue(AGPS_CHECK_CODE, str);
    }

    public String getMapSources() {
        return getValue(MAP_SOURCES, "");
    }

    public void setMapSources(String str) {
        setValue(MAP_SOURCES, str);
    }

    public int getVoicePromptType() {
        return getValue(VOICE_PROMPT_TYPE, 1);
    }

    public void setVoicePromptType(int i) {
        setValue(VOICE_PROMPT_TYPE, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean */
    public boolean isPromptTone() {
        return getValue(VOICE_PROMPT_TONE, true);
    }

    public void setPromptTone(boolean z) {
        setValue(VOICE_PROMPT_TONE, z);
    }

    public String getVoicePromptValue() {
        return getValue(VOICE_PROMPT_VALUE, "1公里");
    }

    public void setVoicePromptValue(String str) {
        setValue(VOICE_PROMPT_VALUE, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float
     arg types: [java.lang.String, int]
     candidates:
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, int):int
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, long):long
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.lang.String):java.lang.String
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, java.util.Set<java.lang.String>):java.util.Set<java.lang.String>
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, boolean):boolean
      com.wade.fit.util.CommonPreferences.getValue(java.lang.String, float):float */
    public float getVoicePromptData() {
        return getValue(VOICE_PROMPT_VALUE, 1.0f);
    }

    public void setVoicePromptData(float f) {
        setValue(VOICE_PROMPT_VALUE, f);
    }
}
