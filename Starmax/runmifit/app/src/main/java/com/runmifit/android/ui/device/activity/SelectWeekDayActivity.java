package com.wade.fit.ui.device.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.base.BaseAdapter;
import com.wade.fit.ui.device.adapter.SelectWeekDayAdapter;
import com.wade.fit.ui.device.bean.WeekDayBean;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.activity.SelectWeekDayActivity */
public class SelectWeekDayActivity extends BaseActivity implements BaseAdapter.OnItemClickListener {
    private boolean[] isSelect;
    private List<WeekDayBean> list = new ArrayList();
    private SelectWeekDayAdapter mAdapter;
    RecyclerView mRecyclerView;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_select_weekday;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.titleName.setText("");
        this.rightText.setText(getResources().getString(R.string.dialog_sure));
        this.rightText.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$SelectWeekDayActivity$Z29JvrwLvrwmUCFLzNfhY18qz1U */

            public final void onClick(View view) {
                SelectWeekDayActivity.this.lambda$initView$0$SelectWeekDayActivity(view);
            }
        });
        this.isSelect = getIntent().getExtras().getBooleanArray("isSelect");
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        initDate();
        this.mAdapter = new SelectWeekDayAdapter(this, this.list);
        this.mAdapter.setOnItemClickListener(this);
        this.mRecyclerView.setAdapter(this.mAdapter);
    }

    public /* synthetic */ void lambda$initView$0$SelectWeekDayActivity(View view) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putBooleanArray("isSelect", this.isSelect);
        intent.putExtras(bundle);
        setResult(200, intent);
        finish();
    }

    private void initDate() {
        String[] stringArray = getResources().getStringArray(R.array.weekDay);
        WeekDayBean weekDayBean = new WeekDayBean(stringArray[0], this.isSelect[6]);
        WeekDayBean weekDayBean2 = new WeekDayBean(stringArray[1], this.isSelect[0]);
        WeekDayBean weekDayBean3 = new WeekDayBean(stringArray[2], this.isSelect[1]);
        WeekDayBean weekDayBean4 = new WeekDayBean(stringArray[3], this.isSelect[2]);
        WeekDayBean weekDayBean5 = new WeekDayBean(stringArray[4], this.isSelect[3]);
        WeekDayBean weekDayBean6 = new WeekDayBean(stringArray[5], this.isSelect[4]);
        WeekDayBean weekDayBean7 = new WeekDayBean(stringArray[6], this.isSelect[5]);
        this.list.clear();
        this.list.add(weekDayBean);
        this.list.add(weekDayBean2);
        this.list.add(weekDayBean3);
        this.list.add(weekDayBean4);
        this.list.add(weekDayBean5);
        this.list.add(weekDayBean6);
        this.list.add(weekDayBean7);
    }

    public void onItemClick(View view, int i) {
        switch (i) {
            case 0:
                boolean[] zArr = this.isSelect;
                zArr[6] = true ^ zArr[6];
                return;
            case 1:
                boolean[] zArr2 = this.isSelect;
                zArr2[0] = true ^ zArr2[0];
                return;
            case 2:
                boolean[] zArr3 = this.isSelect;
                zArr3[1] = !zArr3[1];
                return;
            case 3:
                boolean[] zArr4 = this.isSelect;
                zArr4[2] = true ^ zArr4[2];
                return;
            case 4:
                boolean[] zArr5 = this.isSelect;
                zArr5[3] = true ^ zArr5[3];
                return;
            case 5:
                boolean[] zArr6 = this.isSelect;
                zArr6[4] = true ^ zArr6[4];
                return;
            case 6:
                boolean[] zArr7 = this.isSelect;
                zArr7[5] = true ^ zArr7[5];
                return;
            default:
                return;
        }
    }
}
