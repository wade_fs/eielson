package com.wade.fit.model.bean;

import java.util.ArrayList;
import java.util.List;

public class DeviceConfig {
    public List<Alarm> alarms;
    public List<BLEDevice> bleDeviceList = new ArrayList();
    public BPValue bpValue = new BPValue();
    public String callNumber;
    public DeviceState deviceState = new DeviceState();
    public Goal goal = new Goal();
    public HeartRateInterval interval = new HeartRateInterval();
    public boolean isCall;
    public boolean isDisturbMode;
    public boolean isFindPhone;
    public boolean isMessage;
    public boolean isMusic;
    public boolean isTemp;
    public boolean isTestTime;
    public LongSitBean longSitBean = new LongSitBean();
    public String musicAppName;
    public String musicPackageName;
    public AppNotice notice = new AppNotice();
    public SleepTimeBean sleepTimeBean = new SleepTimeBean();

    public String toString() {
        return "DeviceConfig{alarms=" + this.alarms + ", notice=" + this.notice + ", isTestTime=" + this.isTestTime + ", isMessage=" + this.isMessage + ", isCall=" + this.isCall + ", isMusic=" + this.isMusic + ", musicPackageName='" + this.musicPackageName + '\'' + ", musicAppName='" + this.musicAppName + '\'' + ", goal=" + this.goal + ", bleDeviceList=" + this.bleDeviceList + ", deviceState=" + this.deviceState + ", longSitBean=" + this.longSitBean + ", sleepTimeBean=" + this.sleepTimeBean + '}';
    }
}
