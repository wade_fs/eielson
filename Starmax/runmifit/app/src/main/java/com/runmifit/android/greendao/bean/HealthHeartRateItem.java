package com.wade.fit.greendao.bean;

import java.io.Serializable;

public class HealthHeartRateItem implements Serializable, Cloneable {
    public static final long serialVersionUID = 1;
    private int HeartRaveValue;
    private long date;
    private int day;

    /* renamed from: fz */
    private int f5215fz;
    private boolean isUploaded;
    private String macAddress;
    private int month;
    private int offsetMinute;
    private int oxygen;
    private String remark;

    /* renamed from: ss */
    private int f5216ss;
    private String userId;
    private int year;

    public HealthHeartRateItem() {
    }

    public HealthHeartRateItem(boolean z, String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j, String str2, String str3) {
        this.isUploaded = z;
        this.macAddress = str;
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.offsetMinute = i4;
        this.f5216ss = i5;
        this.f5215fz = i6;
        this.oxygen = i7;
        this.HeartRaveValue = i8;
        this.date = j;
        this.userId = str2;
        this.remark = str3;
    }

    public boolean getIsUploaded() {
        return this.isUploaded;
    }

    public void setIsUploaded(boolean z) {
        this.isUploaded = z;
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    public int getOffsetMinute() {
        return this.offsetMinute;
    }

    public void setOffsetMinute(int i) {
        this.offsetMinute = i;
    }

    public int getHeartRaveValue() {
        return this.HeartRaveValue;
    }

    public void setHeartRaveValue(int i) {
        this.HeartRaveValue = i;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String str) {
        this.remark = str;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getSs() {
        return this.f5216ss;
    }

    public void setSs(int i) {
        this.f5216ss = i;
    }

    public int getFz() {
        return this.f5215fz;
    }

    public void setFz(int i) {
        this.f5215fz = i;
    }

    public int getOxygen() {
        return this.oxygen;
    }

    public void setOxygen(int i) {
        this.oxygen = i;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String toString() {
        return "HealthHeartRateItem{isUploaded=" + this.isUploaded + ", macAddress='" + this.macAddress + '\'' + ", year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", offsetMinute=" + this.offsetMinute + ", ss=" + this.f5216ss + ", fz=" + this.f5215fz + ", oxygen=" + this.oxygen + ", HeartRaveValue=" + this.HeartRaveValue + ", date=" + this.date + ", userId='" + this.userId + '\'' + ", remark='" + this.remark + '\'' + '}';
    }
}
