package com.wade.fit.ui.mine.activity;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.DeviceState;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.model.net.ApiCallback;
import com.wade.fit.model.net.http.UserHttp;
import com.wade.fit.persenter.mine.IMineView;
import com.wade.fit.persenter.mine.MinePresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.tamic.novate.util.Utils;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import p041io.reactivex.disposables.Disposable;

/* renamed from: com.wade.fit.ui.mine.activity.UnitActivity */
public class UnitActivity extends BaseMvpActivity<MinePresenter> implements View.OnClickListener, IMineView {
    private Activity activity;
    private DeviceState deviceState;
    View ivFt;
    View ivKm;
    private boolean tempIsCelsius = true;
    private UserBean userBean;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_unit;
    }

    public UserBean getCurrentUserBean() {
        return null;
    }

    public void saveFaild() {
    }

    public void saveUserBean(int i) {
    }

    public void updateHeaderComplete(int i, String str) {
    }

    public void initView() {
        findViewById(R.id.rlKm).setOnClickListener(this);
        findViewById(R.id.rlFt).setOnClickListener(this);
        this.activity = this;
        this.titleName.setText(getResources().getString(R.string.sys_unit_set));
        this.userBean = AppApplication.getInstance().getUserBean();
        this.deviceState = SPHelper.getDeviceState();
        if (this.userBean.getUnit() == 0) {
            this.ivKm.setVisibility(View.VISIBLE);
            this.ivFt.setVisibility(View.GONE);
            return;
        }
        this.ivKm.setVisibility(View.GONE);
        this.ivFt.setVisibility(View.VISIBLE);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id != R.id.rlFt) {
            if (id == R.id.rlKm && !ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
                setUnit(true);
                setBaiduStat("N3", "千米");
            }
        } else if (!ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            setUnit(false);
            setBaiduStat("N4", "英里");
        }
    }

    private void setUnit(boolean z) {
        if (z) {
            this.userBean.setUnit(0);
            this.deviceState.unit = 0;
        } else {
            this.userBean.setUnit(1);
            this.deviceState.unit = 1;
        }
        int i = 8;
        this.ivKm.setVisibility(z ? 0 : 8);
        View view = this.ivFt;
        if (!z) {
            i = 0;
        }
        view.setVisibility(i);
        AppApplication.getInstance().setUserBean(this.userBean);
        SPHelper.saveUserBean(this.userBean);
        SPHelper.saveDeviceState(this.deviceState);
        if (!TextUtils.isEmpty(SharePreferenceUtils.getString(AppApplication.getInstance(), Constants.USER_TOKEN, ""))) {
            UserHttp.setUserInfo(RequestBody.create(MediaType.parse(Utils.MULTIPART_JSON_DATA), "{\"name\":\"" + this.userBean.getUserName() + "\",\"sex\":\"" + this.userBean.getGender() + "\",\"birthday\":\"" + this.userBean.getBirthday() + "\",\"height\":\"" + this.userBean.getHeight() + "\",\"weight\":\"" + (this.userBean.getWeight() * 10) + "\",\"stepSize\":\"" + this.userBean.getStepDistance() + "\",\"appleHealth\":\"" + false + "\",\"unit\":\"" + this.userBean.getUnit() + "\"}"), new ApiCallback<BaseBean<String>>() {
                /* class com.wade.fit.ui.mine.activity.UnitActivity.C25991 */

                public void onFailure(int i, String str) {
                }

                public void onFinish() {
                }

                public void onSubscribe(Disposable disposable) {
                }

                public void onSuccess(BaseBean<String> baseBean) {
                }
            });
        }
        if (BleSdkWrapper.isConnected() && BleSdkWrapper.isBind()) {
            BleSdkWrapper.setDeviceState(this.deviceState, null);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
