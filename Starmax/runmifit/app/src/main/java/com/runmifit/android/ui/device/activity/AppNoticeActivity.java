package com.wade.fit.ui.device.activity;

import android.content.Intent;
import android.provider.Settings;
import android.text.TextUtils;
import com.wade.fit.R;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.AppNotice;
import com.wade.fit.service.IntelligentNotificationService;
import com.wade.fit.util.GsonUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleCallback;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.views.ItemToggleLayout;

/* renamed from: com.wade.fit.ui.device.activity.AppNoticeActivity */
public class AppNoticeActivity extends BaseActivity {
    private static final int REQUEST_NOTICE_PERMISSION_CODE = 18;
    AppNotice appNotice;
    ItemToggleLayout itFacebook;
    ItemToggleLayout itInstagram;
    ItemToggleLayout itLine;
    ItemToggleLayout itLinked;
    ItemToggleLayout itMessenger;
    ItemToggleLayout itQQ;
    ItemToggleLayout itSkype;
    ItemToggleLayout itTwitter;
    ItemToggleLayout itVK;
    ItemToggleLayout itWeChat;
    ItemToggleLayout itWhatsApp;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_app_notice;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.third_app_tips));
        this.appNotice = SPHelper.getDeviceConfig().notice;
        if (this.appNotice == null) {
            this.appNotice = new AppNotice();
        }
        startNotification();
        this.itQQ.setOpen(this.appNotice.f5221qq);
        this.itFacebook.setOpen(this.appNotice.facebook);
        this.itInstagram.setOpen(this.appNotice.instagram);
        this.itLine.setOpen(this.appNotice.line);
        this.itLinked.setOpen(this.appNotice.linked);
        this.itSkype.setOpen(this.appNotice.skype);
        this.itTwitter.setOpen(this.appNotice.twitter);
        this.itVK.setOpen(this.appNotice.f5222vk);
        this.itWeChat.setOpen(this.appNotice.wechat);
        this.itWhatsApp.setOpen(this.appNotice.whatsApp);
        this.itMessenger.setOpen(this.appNotice.messager);
        this.itWhatsApp.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$nxDWSYuCj6yAjr2TI2LbV6hw9g */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$0$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itInstagram.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$p_eGFjl_UAPWtFKvApYZpFhf08 */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$1$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itQQ.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$8F_GDBjEiPKFqP6zRT2bzr0oUHo */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$2$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itFacebook.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$9bblp4HPOLa87eYHnAzppZFDz4 */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$3$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itLine.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$NNQTFz7dzK3I55brPGE16CTOmg */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$4$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itLinked.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$wfs9CeBn4VYjJp8e2EhKdIiMARQ */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$5$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itSkype.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$apm8cSQwfJzp1uOBaQd2cGpvm8 */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$6$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itTwitter.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$uYq0qi8qwRQkT3QzUT3Ax5wS_g */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$7$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itVK.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$cDqh4mds1vimSMnyJGg76kjfTaw */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$8$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itWeChat.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$xS3YfQija2Rl7Z4hmG0xGIJav4Q */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$9$AppNoticeActivity(itemToggleLayout, z);
            }
        });
        this.itMessenger.setOnToggleListener(new ItemToggleLayout.OnToggleListener() {
            /* class com.wade.fit.ui.device.activity.$$Lambda$AppNoticeActivity$e61vWw9i8jfna4nXGIVtjNrIKco */

            public final void onToggle(ItemToggleLayout itemToggleLayout, boolean z) {
                AppNoticeActivity.this.lambda$initView$10$AppNoticeActivity(itemToggleLayout, z);
            }
        });
    }

    public /* synthetic */ void lambda$initView$0$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.whatsApp = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24491 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$1$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.instagram = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24522 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$2$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.f5221qq = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24533 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$3$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.facebook = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24544 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$4$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.line = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24555 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$5$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.linked = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24566 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$6$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.skype = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24577 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$7$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.twitter = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24588 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$8$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.f5222vk = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C24599 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$9$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.wechat = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C245010 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    public /* synthetic */ void lambda$initView$10$AppNoticeActivity(ItemToggleLayout itemToggleLayout, boolean z) {
        AppNotice appNotice2 = this.appNotice;
        appNotice2.messager = z;
        SharePreferenceUtils.put(this, Constants.APP_NOTICE, GsonUtil.toJson(appNotice2));
        SPHelper.saveAppNotice(this.appNotice);
        BleSdkWrapper.setNotice(this.appNotice, new BleCallback() {
            /* class com.wade.fit.ui.device.activity.AppNoticeActivity.C245111 */

            public void complete(int i, Object obj) {
            }

            public void setSuccess() {
            }
        });
    }

    private boolean isNotificationEnabled() {
        String string = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        DebugLog.m6203d("enabledListeners:" + string);
        if (!TextUtils.isEmpty(string)) {
            return string.contains(IntelligentNotificationService.class.getName());
        }
        return false;
    }

    private void startNotification() {
        if (!isNotificationEnabled()) {
            startActivityForResult(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"), 18);
        }
    }
}
