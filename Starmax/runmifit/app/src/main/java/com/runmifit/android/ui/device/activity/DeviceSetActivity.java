package com.wade.fit.ui.device.activity;

import android.view.View;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.Function;
import com.wade.fit.util.SPHelper;

/* renamed from: com.wade.fit.ui.device.activity.DeviceSetActivity */
public class DeviceSetActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_device_set;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.device_set));
        Function function = SPHelper.getFunction();
        int i = 0;
        findViewById(R.id.llAlarm).setVisibility(function.alarmClock > 0 ? 0 : 8);
        findViewById(R.id.llHrRange).setVisibility(function.hriaeWarning > 0 ? 0 : 8);
        View findViewById = findViewById(R.id.llLongSit);
        if (function.sReminder <= 0) {
            i = 8;
        }
        findViewById.setVisibility(i);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
