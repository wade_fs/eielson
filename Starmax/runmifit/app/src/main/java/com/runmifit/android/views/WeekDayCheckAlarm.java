package com.wade.fit.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.wade.fit.R;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.WeekUtil;
import com.wade.fit.util.log.DebugLog;
import java.util.ArrayList;
import java.util.Arrays;

public class WeekDayCheckAlarm extends LinearLayout {
    /* access modifiers changed from: private */
    public OnWeekCheckedChange onChange;
    /* access modifiers changed from: private */
    public boolean[] repetitions;
    private boolean[] tempRepetitions = new boolean[7];
    private View.OnClickListener toggle = new View.OnClickListener() {
        /* class com.wade.fit.views.WeekDayCheckAlarm.C27201 */

        public void onClick(View view) {
            ValueStateTextView valueStateTextView = (ValueStateTextView) view;
            valueStateTextView.setOpen(!valueStateTextView.isOpen());
            WeekDayCheckAlarm.this.repetitions[((Integer) view.getTag()).intValue()] = valueStateTextView.isOpen();
            DebugLog.m6209i("repetitions:" + Arrays.toString(WeekDayCheckAlarm.this.repetitions));
            if (WeekDayCheckAlarm.this.onChange != null) {
                WeekDayCheckAlarm.this.onChange.onChange(WeekDayCheckAlarm.this.repetitions);
            }
        }
    };
    private ArrayList<ValueStateTextView> values = new ArrayList<>();
    private ValueStateTextView weekDay1;
    private ValueStateTextView weekDay2;
    private ValueStateTextView weekDay3;
    private ValueStateTextView weekDay4;
    private ValueStateTextView weekDay5;
    private ValueStateTextView weekDay6;
    private ValueStateTextView weekDay7;

    public interface OnWeekCheckedChange {
        void onChange(boolean[] zArr);
    }

    public WeekDayCheckAlarm(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        LayoutInflater.from(context).inflate((int) R.layout.weekday_check, this);
        this.weekDay1 = (ValueStateTextView) findViewById(R.id.week_day1);
        this.weekDay2 = (ValueStateTextView) findViewById(R.id.week_day2);
        this.weekDay3 = (ValueStateTextView) findViewById(R.id.week_day3);
        this.weekDay4 = (ValueStateTextView) findViewById(R.id.week_day4);
        this.weekDay5 = (ValueStateTextView) findViewById(R.id.week_day5);
        this.weekDay6 = (ValueStateTextView) findViewById(R.id.week_day6);
        this.weekDay7 = (ValueStateTextView) findViewById(R.id.week_day7);
        this.values.add(this.weekDay1);
        this.values.add(this.weekDay2);
        this.values.add(this.weekDay3);
        this.values.add(this.weekDay4);
        this.values.add(this.weekDay5);
        this.values.add(this.weekDay6);
        this.values.add(this.weekDay7);
    }

    public void initAndSetDefault(boolean[] zArr) {
        this.repetitions = zArr;
        setWeekDayCheck();
        for (int i = 0; i < getChildCount(); i++) {
            ValueStateTextView valueStateTextView = (ValueStateTextView) getChildAt(i);
            valueStateTextView.setOpen(zArr[i]);
            valueStateTextView.setOnClickListener(this.toggle);
            valueStateTextView.setTag(Integer.valueOf(i));
        }
    }

    public boolean[] getRepetition() {
        return this.repetitions;
    }

    public void setOnChangeLinstener(OnWeekCheckedChange onWeekCheckedChange) {
        this.onChange = onWeekCheckedChange;
    }

    public void setWeekDayCheck() {
        String[] weeksByWeekStartDay = WeekUtil.getWeeksByWeekStartDay(getContext(), SharePreferenceUtils.getWeekStartIndex(SharePreferenceUtils.WEEK_START_INDEX, 1));
        for (int i = 0; i < weeksByWeekStartDay.length; i++) {
            this.values.get(i).setText(weeksByWeekStartDay[i]);
        }
    }
}
