package com.wade.fit.ui.main.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseMvpFragment;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthSport;
import com.wade.fit.greendao.gen.HealthActivityDao;
import com.wade.fit.greendao.gen.HealthSportDao;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.model.bean.DetailType;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.ui.device.activity.AlarmListActivity;
import com.wade.fit.ui.device.activity.RemindSportSetActivity;
import com.wade.fit.ui.main.activity.DetailActivity;
import com.wade.fit.ui.main.activity.DetailHeartRateActivity;
import com.wade.fit.ui.main.activity.DetailSleepActivity;
import com.wade.fit.ui.sport.activity.SportOutActivity;
import com.wade.fit.ui.sport.activity.SportRecordActivityNew;
import com.wade.fit.persenter.main.MainFragmentContract;
import com.wade.fit.persenter.main.MainFragmentPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.CacheHelper;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.NumUtil;
import com.wade.fit.util.ObjectUtil;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ble.BleScanTool;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.MainVO;
import com.wade.fit.views.SportPieView;
import com.wade.fit.views.calendar.CalendarDialog;
import com.wade.fit.views.dialog.CommonDialog;
import com.wade.fit.views.pullscrollview.PullToRefreshBase;
import com.wade.fit.views.pullscrollview.PullToRefreshScrollView;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/* renamed from: com.wade.fit.ui.main.fragment.MainFragment */
public class MainFragment extends BaseMvpFragment<MainFragmentPresenter> implements MainFragmentContract.View, Constants {
    public static final int CMD_ACTIVITY = 6;
    public static final int CMD_SETPDATA = 3;
    public static final int CMD_SLEEPDATA = 5;
    public static final int CMD_SYNCHDATA = 2;
    private final int CMD_SHOWDATE = 1;
    private final int CMD_TODAYDATE = 0;
    RelativeLayout barBgTitle;
    public Calendar calendar = Calendar.getInstance();
    protected CalendarDialog clendarDialog;
    private HealthSport currentSport;
    Handler handler = new Handler();
    /* access modifiers changed from: private */
    public boolean haseSynch = false;
    ImageView ivConState;
    ImageView ivNextDate;
    ImageView ivPreDate;
    ImageView ivSelectedDate;
    PullToRefreshScrollView mPullRefreshScrollView;
    /* access modifiers changed from: private */
    public int nowProgress;
    private OnSelectDateListener onSelectDateListener = new OnSelectDateListener() {
        /* class com.wade.fit.ui.main.fragment.MainFragment.C25531 */

        public void onSelectDate(CalendarDate calendarDate) {
            MainFragment mainFragment = MainFragment.this;
            mainFragment.selecetCalendarDate = calendarDate;
            int unused = mainFragment.selectYear = calendarDate.year;
            int unused2 = MainFragment.this.selectMonth = calendarDate.month;
            int unused3 = MainFragment.this.selectDay = calendarDate.day;
            MainFragment.this.changeDateUpdateUI();
        }

        public void onSelectOtherMonth(int i) {
            MainFragment mainFragment = MainFragment.this;
            mainFragment.showToast(mainFragment.getActivity().getResources().getString(R.string.out_date));
        }
    };
    /* access modifiers changed from: private */
    public int progressIndex = 0;
    Runnable progressRun = new Runnable() {
        /* class com.wade.fit.ui.main.fragment.MainFragment.C25553 */

        public void run() {
            if (MainFragment.this.progressIndex == 100) {
                int unused = MainFragment.this.timeoutT = 0;
                int unused2 = MainFragment.this.progressIndex = 0;
                MainFragment.this.handler.removeCallbacks(MainFragment.this.timeoutRun);
                MainFragment.this.handler.removeCallbacks(MainFragment.this.progressRun);
                boolean unused3 = MainFragment.this.haseSynch = false;
                AppApplication.getInstance().setSysndata(false);
                MainFragment.this.mPullRefreshScrollView.onRefreshComplete();
                return;
            }
            if (MainFragment.this.progressIndex < MainFragment.this.nowProgress) {
                MainFragment.access$608(MainFragment.this);
            }
            PullToRefreshScrollView pullToRefreshScrollView = MainFragment.this.mPullRefreshScrollView;
            pullToRefreshScrollView.setRefreshingLabel(MainFragment.this.getActivity().getResources().getString(R.string.server_sync_data) + " " + MainFragment.this.progressIndex + "%");
            if (MainFragment.this.progressIndex <= 30) {
                MainFragment.this.handler.removeCallbacks(MainFragment.this.progressRun);
                MainFragment.this.handler.postDelayed(MainFragment.this.progressRun, 60);
            } else if (MainFragment.this.progressIndex <= 55) {
                MainFragment.this.handler.removeCallbacks(MainFragment.this.progressRun);
                MainFragment.this.handler.postDelayed(MainFragment.this.progressRun, 50);
            } else {
                MainFragment.this.handler.removeCallbacks(MainFragment.this.progressRun);
                MainFragment.this.handler.postDelayed(MainFragment.this.progressRun, 30);
            }
        }
    };
    CalendarDate selecetCalendarDate = null;
    /* access modifiers changed from: private */
    public int selectDay;
    /* access modifiers changed from: private */
    public int selectMonth;
    /* access modifiers changed from: private */
    public int selectYear;
    private MainVO showMainVO;
    SportPieView sport_centerDataPie;
    Runnable timeoutRun = new Runnable() {
        /* class com.wade.fit.ui.main.fragment.MainFragment.C25542 */

        public void run() {
            MainFragment.access$508(MainFragment.this);
            if (MainFragment.this.timeoutT <= 15) {
                MainFragment.this.handler.postDelayed(MainFragment.this.timeoutRun, 1000);
            } else {
                MainFragment.this.timeout();
            }
        }
    };
    /* access modifiers changed from: private */
    public int timeoutT = 0;
    TextView tvCal;
    TextView tvDistance;
    TextView tvDistanceUnit;
    TextView tvHeartVaule;
    TextView tvRecordType;
    TextView tvRecordVaule1;
    TextView tvRecordVaule2;
    TextView tvSleepVaule1;
    TextView tvSleepVaule2;
    TextView tvSynProgress;
    UserBean userBean;

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    static /* synthetic */ int access$508(MainFragment mainFragment) {
        int i = mainFragment.timeoutT;
        mainFragment.timeoutT = i + 1;
        return i;
    }

    static /* synthetic */ int access$608(MainFragment mainFragment) {
        int i = mainFragment.progressIndex;
        mainFragment.progressIndex = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
        super.handleMessage(baseMessage);
        if (baseMessage.getType() == 1006) {
            this.calendar = Calendar.getInstance();
            this.selectYear = this.calendar.get(1);
            this.selectMonth = this.calendar.get(2) + 1;
            this.selectDay = this.calendar.get(5);
            changeDateUpdateUI();
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.barBgTitle.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = ScreenUtil.getStatusHeight(getActivity());
        this.barBgTitle.setLayoutParams(layoutParams);
        this.selectYear = this.calendar.get(1);
        this.selectMonth = this.calendar.get(2) + 1;
        this.selectDay = this.calendar.get(5);
        this.ivConState.setImageResource(BleSdkWrapper.isConnected() ? R.mipmap.link_state : R.mipmap.unlink_state);
        this.ivNextDate.setVisibility(View.GONE);
        ((MainFragmentPresenter) this.mPresenter).getHistoryData(0, this.selectYear, this.selectMonth, this.selectDay);
        this.mPullRefreshScrollView.setVisibility(View.VISIBLE);
        this.mPullRefreshScrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            /* class com.wade.fit.ui.main.fragment.$$Lambda$MainFragment$zomc8PCJDUE0v7fmmyCGk3W4KLA */

            public final void onRefresh(PullToRefreshBase pullToRefreshBase) {
                MainFragment.this.lambda$initView$0$MainFragment(pullToRefreshBase);
            }
        });
        if (!BleScanTool.getInstance().isBluetoothOpen()) {
            LogUtil.dAndSave("扫描设备..蓝牙未打开.", Constants.LOG_PATH);
            new CommonDialog.Builder(getActivity()).isVertical(false).setTitle((int) R.string.fresh_ble_close).setLeftButton((int) R.string.cancel, $$Lambda$MainFragment$Wkaf7FHPjL_CsOi3C_6tS6BLdFg.INSTANCE).setMessage((int) R.string.open_ble_tips).setRightButton((int) R.string.setting, $$Lambda$MainFragment$U9mLEkH9cE_CnuM6RZz0l5Ae_hQ.INSTANCE).create().show();
        }
    }

    public /* synthetic */ void lambda$initView$0$MainFragment(PullToRefreshBase pullToRefreshBase) {
        if (AppApplication.getInstance().isSysndata()) {
            return;
        }
        if (this.selectYear != this.calendar.get(1) || this.selectMonth != this.calendar.get(2) + 1 || this.selectDay != this.calendar.get(5)) {
            this.mPullRefreshScrollView.onRefreshComplete();
        } else if (BleSdkWrapper.isConnected()) {
            this.mPullRefreshScrollView.setRefreshing(true);
            this.handler.post(this.timeoutRun);
            this.handler.post(this.progressRun);
            AppApplication.getInstance().setSysndata(true);
            ((MainFragmentPresenter) this.mPresenter).refreshingData();
        } else {
            showToast(getActivity().getResources().getString(R.string.disConnected));
            this.mPullRefreshScrollView.onRefreshComplete();
        }
    }

    /* access modifiers changed from: protected */
    public void onVisiable() {
        super.onVisiable();
        ((MainFragmentPresenter) this.mPresenter).stopTime();
        checkData();
        if (Build.VERSION.SDK_INT >= 23) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(1280);
        }
        if (((Boolean) SharePreferenceUtils.get(getActivity(), Constants.IS_REF_DATA, false)).booleanValue()) {
            ((MainFragmentPresenter) this.mPresenter).getHistoryData(0, this.selectYear, this.selectMonth, this.selectDay);
            SharePreferenceUtils.put(getActivity(), Constants.IS_REF_DATA, false);
        }
        if (!BleSdkWrapper.isConnected()) {
            this.ivConState.setImageResource(R.mipmap.unlink_state);
        } else {
            if (this.selectYear == this.calendar.get(1) && this.selectMonth == this.calendar.get(2) + 1 && this.selectDay == this.calendar.get(5) && !AppApplication.getInstance().isSysndata()) {
                ((MainFragmentPresenter) this.mPresenter).startTime();
            }
            this.ivConState.setImageResource(R.mipmap.link_state);
        }
        if (SharePreferenceUtils.get(getActivity(), Constants.HASEUPDATE, "").equals("1") && BleSdkWrapper.isBind() && BleSdkWrapper.isConnected() && BleScanTool.getInstance().isBluetoothOpen()) {
            if (!AppApplication.getInstance().isSysndata()) {
                this.mPullRefreshScrollView.setRefreshing(true);
                this.handler.post(this.timeoutRun);
                this.handler.post(this.progressRun);
                AppApplication.getInstance().setSysndata(true);
                if (((MainFragmentPresenter) this.mPresenter).synchLoaing) {
                    this.haseSynch = true;
                } else {
                    ((MainFragmentPresenter) this.mPresenter).synchDate();
                }
                SharePreferenceUtils.put(getActivity(), Constants.HASEUPDATE, "0");
                return;
            }
            SharePreferenceUtils.put(getActivity(), Constants.HASEUPDATE, "0");
        }
    }

    private void checkData() {
        int i = this.calendar.get(1);
        int i2 = this.calendar.get(2) + 1;
        int i3 = this.calendar.get(5);
        this.calendar = Calendar.getInstance();
        int i4 = this.calendar.get(1);
        int i5 = this.calendar.get(2) + 1;
        int i6 = this.calendar.get(5);
        if (!(i == i4 && i2 == i5 && i3 == i6)) {
            this.selectYear = i4;
            this.selectMonth = i5;
            this.selectDay = i6;
        }
        changeDateUpdateUI();
    }

    public void oninVisiable() {
        super.oninVisiable();
        ((MainFragmentPresenter) this.mPresenter).stopTime();
    }

    /* access modifiers changed from: package-private */
    public void changePaseDay() {
        if (ButtonUtils.isFastDoubleClick(R.id.ivPreDate, 500)) {
            return;
        }
        if (!AppApplication.getInstance().isSysndata()) {
            String specifiedDayBefore = DateUtil.getSpecifiedDayBefore(this.selectYear + "-" + this.selectMonth + "-" + this.selectDay);
            this.tvSynProgress.setText(specifiedDayBefore);
            this.selectYear = Integer.parseInt(specifiedDayBefore.split("-")[0]);
            this.selectMonth = Integer.parseInt(specifiedDayBefore.split("-")[1]);
            this.selectDay = Integer.parseInt(specifiedDayBefore.split("-")[2]);
            if (this.selecetCalendarDate == null) {
                this.selecetCalendarDate = new CalendarDate();
            }
            CalendarDate calendarDate = this.selecetCalendarDate;
            calendarDate.year = this.selectYear;
            calendarDate.month = this.selectMonth;
            calendarDate.day = this.selectDay;
            changeDateUpdateUI();
            return;
        }
        showToast(getActivity().getResources().getString(R.string.server_sync_data));
    }

    /* access modifiers changed from: package-private */
    public void changeNextDay() {
        if (ButtonUtils.isFastDoubleClick(R.id.ivNextDate, 500)) {
            return;
        }
        if (!AppApplication.getInstance().isSysndata()) {
            String specifiedDayAfter = DateUtil.getSpecifiedDayAfter(this.selectYear + "-" + this.selectMonth + "-" + this.selectDay);
            this.selectYear = Integer.parseInt(specifiedDayAfter.split("-")[0]);
            this.selectMonth = Integer.parseInt(specifiedDayAfter.split("-")[1]);
            this.selectDay = Integer.parseInt(specifiedDayAfter.split("-")[2]);
            if (this.selecetCalendarDate == null) {
                this.selecetCalendarDate = new CalendarDate();
            }
            CalendarDate calendarDate = this.selecetCalendarDate;
            calendarDate.year = this.selectYear;
            calendarDate.month = this.selectMonth;
            calendarDate.day = this.selectDay;
            changeDateUpdateUI();
            return;
        }
        showToast(getActivity().getResources().getString(R.string.server_sync_data));
    }

    /* access modifiers changed from: private */
    public void changeDateUpdateUI() {
        if (!(this.selectYear == this.calendar.get(1) && this.selectMonth == this.calendar.get(2) + 1 && this.selectDay >= this.calendar.get(5)) && this.selectYear <= this.calendar.get(1) && (this.selectYear != this.calendar.get(1) || this.selectMonth <= this.calendar.get(2) + 1)) {
            this.ivNextDate.setVisibility(View.VISIBLE);
            TextView textView = this.tvSynProgress;
            textView.setText(this.selectYear + "-" + this.selectMonth + "-" + this.selectDay);
            ((MainFragmentPresenter) this.mPresenter).stopTime();
            ((MainFragmentPresenter) this.mPresenter).getHistoryData(1, this.selectYear, this.selectMonth, this.selectDay);
            return;
        }
        this.selectYear = this.calendar.get(1);
        this.selectMonth = this.calendar.get(2) + 1;
        this.selectDay = this.calendar.get(5);
        this.ivNextDate.setVisibility(View.GONE);
        this.tvSynProgress.setText(getActivity().getResources().getString(R.string.today));
        if (!BleSdkWrapper.isConnected() || !BleSdkWrapper.isBind()) {
            if (this.currentSport == null) {
                this.currentSport = new HealthSport();
            }
            updateSportVaule(this.currentSport);
        } else {
            ((MainFragmentPresenter) this.mPresenter).startTime();
            updateSportVaule(this.currentSport);
        }
        requestSuccess(5, null);
        requestSuccess(6, null);
    }

    /* access modifiers changed from: protected */
    public void initGridviewData(MainVO mainVO) {
        int i;
        AppApplication appApplication;
        if (mainVO == null) {
            mainVO = new MainVO();
        }
        if (mainVO.healthSport != null) {
            this.tvRecordType.setText(mainVO.sportType + " ");
            this.tvRecordVaule1.setText(mainVO.sportHour);
            this.tvRecordVaule2.setText(mainVO.sportMin);
        } else {
            this.tvRecordType.setText("");
            this.tvRecordVaule1.setText("-");
            this.tvRecordVaule2.setText("-");
        }
        String str = "0";
        if (mainVO.healthHeartRate == null) {
            this.tvHeartVaule.setText("-");
        } else if (mainVO.heartRate.equals(str)) {
            this.tvHeartVaule.setText("-");
        } else {
            this.tvHeartVaule.setText(mainVO.heartRate);
        }
        if (mainVO.healthSleep != null) {
            this.tvSleepVaule1.setText(mainVO.sleepHour);
            this.tvSleepVaule2.setText(mainVO.sleepMin);
        } else {
            this.tvSleepVaule1.setText("-");
            this.tvSleepVaule2.setText("-");
        }
        TextView textView = this.tvCal;
        if (mainVO.healthSport != null) {
            str = mainVO.healthSport.getTotalCalory() + "";
        }
        textView.setText(str);
        float totalDistance = mainVO.healthSport == null ? 0.0f : ((float) mainVO.healthSport.getTotalDistance()) / 1000.0f;
        if (!BleSdkWrapper.isDistUnitKm()) {
            totalDistance = UnitUtil.km2mile(totalDistance);
        }
        this.tvDistance.setText(NumUtil.formatPoint((double) totalDistance, 2) + "");
        TextView textView2 = this.tvDistanceUnit;
        if (BleSdkWrapper.isDistUnitKm()) {
            appApplication = AppApplication.getInstance();
            i = R.string.unit_kilometer;
        } else {
            appApplication = AppApplication.getInstance();
            i = R.string.unit_mi;
        }
        textView2.setText(appApplication.getString(i));
        this.sport_centerDataPie.setSportGoal(SPHelper.getDeviceConfig().goal.goalStep);
        if (mainVO.healthSport == null) {
            this.sport_centerDataPie.setSportSteps(0);
        } else {
            this.sport_centerDataPie.setSportSteps(mainVO.healthSport.getTotalStepCount());
        }
    }

    /* access modifiers changed from: package-private */
    public void toDetail(View view) {
        if (!ButtonUtils.isFastDoubleClick(view.getId(), 1000)) {
            ((MainFragmentPresenter) this.mPresenter).stopTime();
            int id = view.getId();
            if (id == R.id.llCal) {
                DetailActivity.startActivity(getActivity(), DetailType.CAL, ProDbUtils.getDate(this.selectYear, this.selectMonth, this.selectDay));
            } else if (id == R.id.llDistance) {
                DetailActivity.startActivity(getActivity(), DetailType.DISTANCE, ProDbUtils.getDate(this.selectYear, this.selectMonth, this.selectDay));
            } else if (id == R.id.sport_centerDataPie) {
                DetailActivity.startActivity(getActivity(), DetailType.STEP, ProDbUtils.getDate(this.selectYear, this.selectMonth, this.selectDay));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void changeDate() {
        if (!AppApplication.getInstance().isSysndata()) {
            if (this.clendarDialog == null) {
                this.clendarDialog = new CalendarDialog((Context) Objects.requireNonNull(getContext()));
            }
            CalendarDialog calendarDialog = this.clendarDialog;
            calendarDialog.mOnSelectDateListener = this.onSelectDateListener;
            CalendarDate calendarDate = this.selecetCalendarDate;
            if (calendarDate != null) {
                calendarDialog.showDialog(calendarDate);
            } else {
                calendarDialog.showDialog();
            }
        } else {
            showToast(getActivity().getResources().getString(R.string.server_sync_data));
        }
    }

    /* access modifiers changed from: package-private */
    public void layoutClick(View view) {
        switch (view.getId()) {
            case R.id.layout1 /*2131296596*/:
                IntentUtil.goToActivity(getActivity(), SportOutActivity.class);
                return;
            case R.id.layout2 /*2131296597*/:
                Bundle bundle = new Bundle();
                bundle.putSerializable("date", ProDbUtils.getDate(this.selectYear, this.selectMonth, this.selectDay));
                IntentUtil.goToActivity(getActivity(), SportRecordActivityNew.class, bundle);
                return;
            case R.id.layout3 /*2131296598*/:
                DetailHeartRateActivity.startActivity(getActivity(), ProDbUtils.getDate(this.selectYear, this.selectMonth, this.selectDay));
                return;
            case R.id.layout4 /*2131296599*/:
                DetailSleepActivity.startActivity(getActivity(), ProDbUtils.getDate(this.selectYear, this.selectMonth, this.selectDay));
                return;
            case R.id.layout5 /*2131296600*/:
                if (!BleSdkWrapper.isConnected() || !BleSdkWrapper.isBind()) {
                    showToast(getActivity().getResources().getString(R.string.disConnected));
                    return;
                } else if (AppApplication.getInstance().isSysndata()) {
                    showToast(getActivity().getResources().getString(R.string.server_sync_data));
                    return;
                } else {
                    IntentUtil.goToActivity(getActivity(), AlarmListActivity.class);
                    return;
                }
            case R.id.layout6 /*2131296601*/:
                if (!BleSdkWrapper.isConnected() || !BleSdkWrapper.isBind()) {
                    showToast(getActivity().getResources().getString(R.string.disConnected));
                    return;
                } else if (AppApplication.getInstance().isSysndata()) {
                    showToast(getActivity().getResources().getString(R.string.server_sync_data));
                    return;
                } else {
                    IntentUtil.goToActivity(getActivity(), RemindSportSetActivity.class);
                    return;
                }
            default:
                return;
        }
    }

    public void requestSuccess(int i, MainVO mainVO) {
        int i2;
        String str;
        if (i == 0 || i == 1) {
            this.showMainVO = mainVO;
            initGridviewData(this.showMainVO);
        } else if (i == 2) {
            this.haseSynch = false;
            if (this.selectYear != this.calendar.get(1) || this.selectMonth != this.calendar.get(2) + 1 || this.selectDay != this.calendar.get(5)) {
                ((MainFragmentPresenter) this.mPresenter).stopTime();
            } else if (!AppApplication.getInstance().isSysndata()) {
                ((MainFragmentPresenter) this.mPresenter).startTime();
            }
        } else if (i == 3) {
            updateSportVaule((HealthSport) AppApplication.getInstance().getDaoSession().getHealthSportDao().queryBuilder().where(HealthSportDao.Properties.Year.eq(Integer.valueOf(this.selectYear)), HealthSportDao.Properties.Month.eq(Integer.valueOf(this.selectMonth)), HealthSportDao.Properties.Day.eq(Integer.valueOf(this.selectDay))).orderDesc(HealthSportDao.Properties.Date).build().unique());
        } else if (i == 5) {
            int geeSleepTime = ((MainFragmentPresenter) this.mPresenter).geeSleepTime(this.selectYear, this.selectMonth, this.selectDay);
            if (geeSleepTime == 0) {
                this.tvSleepVaule1.setText("-");
                this.tvSleepVaule2.setText("-");
                return;
            }
            TextView textView = this.tvSleepVaule1;
            textView.setText((geeSleepTime / 60) + "");
            TextView textView2 = this.tvSleepVaule2;
            textView2.setText((geeSleepTime % 60) + "");
        } else if (i == 6) {
            List list = AppApplication.getInstance().getDaoSession().getHealthActivityDao().queryBuilder().where(HealthActivityDao.Properties.Year.eq(Integer.valueOf(this.selectYear)), HealthActivityDao.Properties.Month.eq(Integer.valueOf(this.selectMonth)), HealthActivityDao.Properties.Day.eq(Integer.valueOf(this.selectDay))).orderDesc(HealthActivityDao.Properties.Date).build().list();
            if (!ObjectUtil.isCollectionEmpty((Collection) list)) {
                i2 = ((HealthActivity) list.get(0)).getDurations();
                str = CacheHelper.getSportName(((HealthActivity) list.get(0)).getType());
            } else {
                str = "";
                i2 = 0;
            }
            TextView textView3 = this.tvRecordType;
            textView3.setText(str + " ");
            if (i2 == 0) {
                this.tvRecordVaule1.setText("-");
                this.tvRecordVaule2.setText("-");
                return;
            }
            TextView textView4 = this.tvRecordVaule1;
            textView4.setText((i2 / 3600) + "");
            TextView textView5 = this.tvRecordVaule2;
            textView5.setText(((i2 % 3600) / 60) + "");
        }
    }

    public void updateSportVaule(HealthSport healthSport) {
        this.currentSport = healthSport;
        this.userBean = AppApplication.getInstance().getUserBean();
        if (healthSport == null || this.userBean == null) {
            this.tvCal.setText("0");
            float f = 0.0f;
            if (!BleSdkWrapper.isDistUnitKm()) {
                f = UnitUtil.km2mile(0.0f);
            }
            TextView textView = this.tvDistance;
            textView.setText(NumUtil.formatPoint((double) f, 2) + "");
            this.sport_centerDataPie.setSportGoal(SPHelper.getDeviceConfig().goal.goalStep);
            this.sport_centerDataPie.setSportSteps(0);
            this.tvDistanceUnit.setText(BleSdkWrapper.isDistUnitKm() ? AppApplication.getInstance().getString(R.string.unit_kilometer) : AppApplication.getInstance().getString(R.string.unit_mi));
            return;
        }
        int totalStepCount = healthSport.getTotalStepCount();
        double stepDistance = (double) (this.userBean.getStepDistance() * this.userBean.getWeight() * totalStepCount);
        Double.isNaN(stepDistance);
        healthSport.setTotalCalory((int) ((stepDistance * 0.78d) / 100000.0d));
        healthSport.setTotalDistance((this.userBean.getStepDistance() * totalStepCount) / 100);
        TextView textView2 = this.tvCal;
        textView2.setText(healthSport.getTotalCalory() + "");
        float totalDistance = ((float) healthSport.getTotalDistance()) / 1000.0f;
        if (!BleSdkWrapper.isDistUnitKm()) {
            totalDistance = UnitUtil.km2mile(totalDistance);
        }
        TextView textView3 = this.tvDistance;
        textView3.setText(NumUtil.formatPoint((double) totalDistance, 2) + "");
        this.tvDistanceUnit.setText(BleSdkWrapper.isDistUnitKm() ? AppApplication.getInstance().getString(R.string.unit_kilometer) : AppApplication.getInstance().getString(R.string.unit_mi));
        this.sport_centerDataPie.setSportGoal(SPHelper.getDeviceConfig().goal.goalStep);
        this.sport_centerDataPie.setSportSteps(healthSport.getTotalStepCount());
    }

    public void updateHeartVaule(int i, int i2) {
        if (i2 == 0) {
            this.tvHeartVaule.setText("--");
            return;
        }
        TextView textView = this.tvHeartVaule;
        textView.setText(i2 + "");
    }

    public void requestFaild(int i) {
        if (i == 2) {
            ((MainFragmentPresenter) this.mPresenter).stopTime();
            this.timeoutT = 0;
            this.progressIndex = 0;
            this.handler.removeCallbacks(this.timeoutRun);
            this.handler.removeCallbacks(this.progressRun);
            AppApplication.getInstance().setSysndata(false);
            this.mPullRefreshScrollView.onRefreshComplete();
        }
    }

    public void updateProgress(int i) {
        this.timeoutT = 0;
        this.nowProgress = i;
    }

    public void updateSych() {
        if (this.haseSynch) {
            this.haseSynch = false;
            ((MainFragmentPresenter) this.mPresenter).synchDate();
        }
    }

    public void updateConnect() {
        if (!BleSdkWrapper.isConnected() || !BleScanTool.getInstance().isBluetoothOpen()) {
            if (AppApplication.getInstance().isSysndata()) {
                this.mPullRefreshScrollView.onRefreshComplete();
                AppApplication.getInstance().setSysndata(false);
            }
            if (!BleScanTool.getInstance().isBluetoothOpen()) {
                showToast(getActivity().getResources().getString(R.string.disConnected));
            }
            this.ivConState.setImageResource(R.mipmap.unlink_state);
            ((MainFragmentPresenter) this.mPresenter).stopTime();
            return;
        }
        this.ivConState.setImageResource(R.mipmap.link_state);
    }

    public void timeout() {
        this.timeoutT = 0;
        this.nowProgress = 100;
    }

    public void synchData() {
        if (BleSdkWrapper.isBind() && BleSdkWrapper.isConnected() && BleScanTool.getInstance().isBluetoothOpen()) {
            AppApplication.getInstance().setSysndata(true);
            this.handler.post(this.timeoutRun);
            this.handler.post(this.progressRun);
            this.mPullRefreshScrollView.setRefreshing(true);
            if (((MainFragmentPresenter) this.mPresenter).synchLoaing) {
                this.haseSynch = true;
            } else {
                ((MainFragmentPresenter) this.mPresenter).synchDate();
            }
        }
    }

    public void connect() {
        if (!AppApplication.getInstance().isSysndata()) {
            this.ivConState.setImageResource(R.mipmap.link_state);
            this.mPullRefreshScrollView.setRefreshing(true);
            this.handler.post(this.timeoutRun);
            this.handler.post(this.progressRun);
            AppApplication.getInstance().setSysndata(true);
            if (((MainFragmentPresenter) this.mPresenter).synchLoaing) {
                this.haseSynch = true;
            } else {
                ((MainFragmentPresenter) this.mPresenter).synchDate();
            }
        }
    }

    public void updateSport() {
        int i;
        String str;
        List list = AppApplication.getInstance().getDaoSession().getHealthActivityDao().queryBuilder().orderDesc(HealthActivityDao.Properties.Date).build().list();
        if (list == null || list.size() <= 0) {
            str = "";
            i = 0;
        } else {
            i = ((HealthActivity) list.get(0)).getDurations();
            str = CacheHelper.getSportName(((HealthActivity) list.get(0)).getType());
        }
        TextView textView = this.tvRecordType;
        textView.setText(str + " ");
        TextView textView2 = this.tvRecordVaule1;
        textView2.setText((i / 3600) + "");
        TextView textView3 = this.tvRecordVaule2;
        textView3.setText(((i % 3600) / 60) + "");
    }
}
