package com.wade.fit.ui.main.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.notification.NotificationServiceManager;
import com.wade.fit.ui.device.fragment.DeviceFragment;
import com.wade.fit.ui.main.fragment.MainFragment;
import com.wade.fit.ui.mine.fragment.MineFragment;
import com.wade.fit.service.AssistService;
import com.wade.fit.util.AppUtil;
import com.wade.fit.util.PermissionUtil;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BleManager;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.util.thirddataplatform.GoogleFitPresenter;
import com.wade.fit.views.NotifyRadioButton;

/* renamed from: com.wade.fit.ui.main.activity.MainActivity */
public class MainActivity extends BaseActivity implements Constants {
    private static final int REQUEST_PERMISSION_BODY_SENSORS = 100;
    private int currentIndex;
    private DeviceFragment deviceFragment;
    private Fragment[] fragments = new Fragment[3];
    private boolean isUpdateDevice = false;
    private long lastOnBackTime = 0;
    FrameLayout mActivityFrame;
    private MainFragment mainFragment;
    private MineFragment mineFragment;
    NotificationServiceManager notificationServiceManager = new NotificationServiceManager();
    NotifyRadioButton rbTabDevice;
    NotifyRadioButton rbTabEcg;
    NotifyRadioButton rbTabMainpage;
    NotifyRadioButton rbTabUser;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_main;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        GoogleFitPresenter.getInstance().setActivity(this);
        BleManager.getInstance().reConnect();
        this.currentIndex = -1;
        this.layoutTitle.setVisibility(View.GONE);
        this.mainFragment = new MainFragment();
        this.deviceFragment = new DeviceFragment();
        this.mineFragment = new MineFragment();
        Fragment[] fragmentArr = this.fragments;
        fragmentArr[0] = this.mainFragment;
        fragmentArr[1] = this.deviceFragment;
        fragmentArr[2] = this.mineFragment;
        startService(new Intent(this, AssistService.class));
        showFragment(0);
        this.rbTabEcg.setVisibility(View.GONE);
        if (SharePreferenceUtils.getBool(this, Constants.GOOGLE_REQUEST, true)) {
            requestFit();
        }
    }

    private void requestFit() {
        if (AppUtil.isInstallAppByPackageName(this, "com.google.android.apps.fitness")) {
            SharePreferenceUtils.putBool(Constants.GOOGLE_REQUEST, false);
            if (PermissionUtil.checkSelfPermission("android.permission.BODY_SENSORS")) {
                SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 1);
                GoogleFitPresenter.getInstance().connectGoogle();
                return;
            }
            requestPermissions(100, "android.permission.BODY_SENSORS");
        }
    }

    public void onBackPressed() {
        if (System.currentTimeMillis() - this.lastOnBackTime < 2000) {
            moveTaskToBack(true);
            return;
        }
        this.lastOnBackTime = System.currentTimeMillis();
        showToast(getResources().getString(R.string.exit_app));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LogUtil.d("onDestroy");
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtil.d("onNewIntent");
        showFragment(0);
        this.mainFragment.synchData();
    }

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
        super.handleMessage(baseMessage);
        int type = baseMessage.getType();
        if (type != 101 && type != 102 && type != 104) {
            if (type == 1002) {
                showFragment(0);
                this.mainFragment.synchData();
            } else if (type != 1003) {
                switch (type) {
                    case 301:
                    case 302:
                        this.mainFragment.updateConnect();
                        if (this.isUpdateDevice) {
                            this.deviceFragment.updateConnect();
                            return;
                        }
                        return;
                    case 303:
                        LogUtil.m5266d("aaa", "进入：" + SharePreferenceUtils.get(this, Constants.UPDATECONFIG, Constants.NOUPDATE));
                        if (SharePreferenceUtils.get(this, Constants.UPDATECONFIG, Constants.NOUPDATE).equals(Constants.NOUPDATE)) {
                            if (this.currentIndex == 0) {
                                this.mainFragment.connect();
                            } else {
                                SharePreferenceUtils.put(this, Constants.HASEUPDATE, "1");
                            }
                            if (this.isUpdateDevice) {
                                this.deviceFragment.updateConnect();
                                return;
                            }
                            return;
                        }
                        return;
                    case 304:
                        AppApplication.getInstance().setSysndata(false);
                        this.mainFragment.timeout();
                        return;
                    default:
                        return;
                }
            } else {
                this.mainFragment.updateSport();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1) {
            if (i2 == 200) {
                showFragment(0);
                this.mainFragment.synchData();
            }
        } else if (i != 3) {
        } else {
            if (i2 != -1) {
                SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 0);
                return;
            }
            SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 1);
            GoogleFitPresenter.getInstance().handleSignInResult(i, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.notificationServiceManager.bindIntelligentNotificationService(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rb_tab_device /*2131296773*/:
                if (this.currentIndex != 1) {
                    showFragment(1);
                    this.isUpdateDevice = true;
                    return;
                }
                return;
            case R.id.rb_tab_ecg /*2131296774*/:
            default:
                return;
            case R.id.rb_tab_mainpage /*2131296775*/:
                if (this.currentIndex != 0) {
                    showFragment(0);
                    return;
                }
                return;
            case R.id.rb_tab_user /*2131296776*/:
                if (this.currentIndex != 2) {
                    showFragment(2);
                    return;
                }
                return;
        }
    }

    public void showFragment(int i) {
        if (this.currentIndex != i) {
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            int i2 = this.currentIndex;
            if (i2 != -1) {
                beginTransaction.hide(this.fragments[i2]);
            }
            this.currentIndex = i;
            if (!this.fragments[i].isAdded()) {
                beginTransaction.add((int) R.id.activity_frame, this.fragments[i]);
            } else {
                beginTransaction.show(this.fragments[i]);
            }
            beginTransaction.commit();
            changeBottomUI(i);
        }
    }

    private void changeBottomUI(int i) {
        if (i == 0) {
            this.rbTabMainpage.setChecked(true);
            this.rbTabEcg.setChecked(false);
            this.rbTabDevice.setChecked(false);
            this.rbTabUser.setChecked(false);
        } else if (i == 1) {
            this.rbTabMainpage.setChecked(false);
            this.rbTabEcg.setChecked(false);
            this.rbTabDevice.setChecked(true);
            this.rbTabUser.setChecked(false);
        } else if (i == 2) {
            this.rbTabMainpage.setChecked(false);
            this.rbTabEcg.setChecked(false);
            this.rbTabDevice.setChecked(false);
            this.rbTabUser.setChecked(true);
        }
    }

    public void requestPermissionsSuccess(int i) {
        super.requestPermissionsSuccess(i);
        requestFit();
    }

    public void requestPermissionsFail(int i) {
        super.requestPermissionsFail(i);
        if (100 == i) {
            SharePreferenceUtils.putInt(this, Constants.GOOGLE_FIT_KEY, 0);
        }
    }
}
