package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.QuestionActivity_ViewBinding */
public class QuestionActivity_ViewBinding implements Unbinder {
    private QuestionActivity target;

    public QuestionActivity_ViewBinding(QuestionActivity questionActivity) {
        this(questionActivity, questionActivity.getWindow().getDecorView());
    }

    public QuestionActivity_ViewBinding(QuestionActivity questionActivity, View view) {
        this.target = questionActivity;
        questionActivity.listView = (ListView) Utils.findRequiredViewAsType(view, R.id.listView, "field 'listView'", ListView.class);
    }

    public void unbind() {
        QuestionActivity questionActivity = this.target;
        if (questionActivity != null) {
            this.target = null;
            questionActivity.listView = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
