package com.wade.fit.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import android.view.TouchDelegate;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.Adapter;
import android.widget.ListView;

public class ViewUtil {
    public static float calculateRectW(int i, int i2, float f, float f2) {
        if (f2 >= 1.0f) {
            f2 = 1.0f;
        }
        if (f2 <= 0.0f) {
            f2 = 0.0f;
        }
        if (i2 == 0) {
            return 0.0f;
        }
        return ((float) i) * ((f2 * f) / ((float) i2));
    }

    public static void addDefaultScreenArea(View view, int i, int i2, int i3, int i4) {
        final View view2 = view;
        final int i5 = i;
        final int i6 = i2;
        final int i7 = i3;
        final int i8 = i4;
        ((View) view.getParent()).post(new Runnable() {
            /* class com.wade.fit.util.ViewUtil.C26491 */

            public void run() {
                Rect rect = new Rect();
                view2.setEnabled(true);
                view2.getHitRect(rect);
                rect.top -= i5;
                rect.bottom += i6;
                rect.left -= i7;
                rect.right += i8;
                TouchDelegate touchDelegate = new TouchDelegate(rect, view2);
                if (View.class.isInstance(view2.getParent())) {
                    ((View) view2.getParent()).setTouchDelegate(touchDelegate);
                }
            }
        });
    }

    public static void addDefaultScreenArea(final View view, final int i) {
        ((View) view.getParent()).post(new Runnable() {
            /* class com.wade.fit.util.ViewUtil.C26502 */

            public void run() {
                Rect rect = new Rect();
                view.setEnabled(true);
                view.getHitRect(rect);
                rect.top -= i;
                rect.bottom += i;
                rect.left -= i;
                rect.right += i;
                TouchDelegate touchDelegate = new TouchDelegate(rect, view);
                if (View.class.isInstance(view.getParent())) {
                    ((View) view.getParent()).setTouchDelegate(touchDelegate);
                }
            }
        });
    }

    public static int getColorBetweenAB(int i, int i2, float f, int i3) {
        int i4 = (16711680 & i) >> 16;
        float f2 = (float) i3;
        float f3 = ((((float) (((i2 & 16711680) >> 16) - i4)) / f) * f2) + ((float) i4);
        int i5 = 65280 & i;
        int i6 = i & 255;
        return Color.rgb((int) f3, (int) (((((float) (((i2 & MotionEventCompat.ACTION_POINTER_INDEX_MASK) - i5) >> 8)) / f) * f2) + ((float) (i5 >> 8))), (int) (((((float) ((i2 & 255) - i6)) / f) * f2) + ((float) i6)));
    }

    public static Bitmap blur(Bitmap bitmap, Context context) {
        RenderScript create = RenderScript.create(context);
        Allocation createFromBitmap = Allocation.createFromBitmap(create, bitmap);
        ScriptIntrinsicBlur create2 = ScriptIntrinsicBlur.create(create, createFromBitmap.getElement());
        create2.setInput(createFromBitmap);
        create2.setRadius(25.0f);
        create2.forEach(createFromBitmap);
        createFromBitmap.copyTo(bitmap);
        return bitmap;
    }

    public static float getTextHeight(Paint paint) {
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        return fontMetrics.bottom - fontMetrics.top;
    }

    public static float getTextRectWidth(Paint paint, String str) {
        if (TextUtils.isEmpty(str)) {
            return 0.0f;
        }
        paint.getTextBounds(str, 0, str.length(), new Rect());
        return paint.measureText(str);
    }

    public static float getTextRectHeight(Paint paint, String str) {
        if (TextUtils.isEmpty(str)) {
            return 0.0f;
        }
        Rect rect = new Rect();
        paint.getTextBounds(str, 0, str.length(), rect);
        return (float) rect.height();
    }

    public static void startRotateAnimation(View view) {
        view.setVisibility(View.VISIBLE);
        view.clearAnimation();
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 359.0f, 1, 0.5f, 1, 0.5f);
        rotateAnimation.setDuration(500);
        rotateAnimation.setRepeatCount(-1);
        view.startAnimation(rotateAnimation);
    }

    public static void drawRect(Canvas canvas, float f, float f2, float f3, int i, int i2, float f4, float f5, float f6, Paint paint) {
        float f7 = f2 + f6;
        canvas.drawRect(f, f7, f + calculateRectW(i, i2, f4, f5), (f3 + f7) - f6, paint);
    }

    public static void drawText(Canvas canvas, int i, float f, float f2, float f3, float f4, float f5, int i2, String str, String str2) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setColor(i2);
        String str3 = (i / 60) + "";
        String str4 = (i % 60) + "";
        paint.setTextSize(f4);
        float textRectHeight = getTextRectHeight(paint, str3);
        canvas.drawText(str3, f, f2, paint);
        float textRectWidth = f + f3 + getTextRectWidth(paint, str3);
        paint.setTextSize(f5);
        float textRectHeight2 = getTextRectHeight(paint, str);
        float f6 = f2 - ((textRectHeight > textRectHeight2 ? textRectHeight - textRectHeight2 : textRectHeight2 - textRectHeight) / 4.0f);
        canvas.drawText(str, textRectWidth, f6, paint);
        float textRectWidth2 = textRectWidth + f3 + getTextRectWidth(paint, str);
        paint.setTextSize(f4);
        canvas.drawText(str4, textRectWidth2, f2, paint);
        paint.setTextSize(f5);
        canvas.drawText(str2, textRectWidth2 + f3 + getTextRectWidth(paint, str4), f6, paint);
    }

    public static void drawText(Canvas canvas, int i, float f, float f2, float f3, float f4, float f5, int i2, String str, String str2, Paint.Align align) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextAlign(align);
        paint.setColor(i2);
        String format = String.format("%02d", Integer.valueOf(i / 60));
        String format2 = String.format("%02d", Integer.valueOf(i % 60));
        paint.setTextSize(f4);
        float textRectHeight = getTextRectHeight(paint, format);
        paint.setTextSize(f5);
        float textRectHeight2 = getTextRectHeight(paint, str);
        float f6 = textRectHeight > textRectHeight2 ? textRectHeight - textRectHeight2 : textRectHeight2 - textRectHeight;
        if (align == Paint.Align.LEFT) {
            paint.setTextSize(f4);
            canvas.drawText(format, f, f2, paint);
            float textRectWidth = f + f3 + getTextRectWidth(paint, format);
            paint.setTextSize(f5);
            float f7 = f2 - (f6 / 4.0f);
            canvas.drawText(str, textRectWidth, f7, paint);
            float textRectWidth2 = textRectWidth + f3 + getTextRectWidth(paint, str);
            paint.setTextSize(f4);
            canvas.drawText(format2, textRectWidth2, f2, paint);
            paint.setTextSize(f5);
            canvas.drawText(str2, textRectWidth2 + f3 + getTextRectWidth(paint, format2), f7, paint);
        } else if (align == Paint.Align.CENTER) {
            paint.setTextAlign(Paint.Align.RIGHT);
            float f8 = f3 / 2.0f;
            float f9 = f - f8;
            paint.setTextSize(f5);
            float f10 = f2 - (f6 / 4.0f);
            canvas.drawText(str, f9, f10, paint);
            paint.setTextAlign(Paint.Align.RIGHT);
            paint.setTextSize(f4);
            canvas.drawText(format, (f9 - f3) - getTextRectWidth(paint, str), f2, paint);
            paint.setTextAlign(Paint.Align.LEFT);
            float f11 = f + f8;
            paint.setTextSize(f4);
            canvas.drawText(format2, f11, f2, paint);
            paint.setTextAlign(Paint.Align.LEFT);
            paint.setTextSize(f5);
            canvas.drawText(str2, f11 + f3 + getTextRectWidth(paint, format2), f10, paint);
        } else if (align == Paint.Align.RIGHT) {
            paint.setTextSize(f5);
            float f12 = f2 - (f6 / 4.0f);
            canvas.drawText(str2, f, f12, paint);
            float textRectWidth3 = (f - f3) - getTextRectWidth(paint, str2);
            paint.setTextSize(f4);
            canvas.drawText(format2, textRectWidth3, f2, paint);
            float textRectWidth4 = (textRectWidth3 - f3) - getTextRectWidth(paint, format2);
            paint.setTextSize(f5);
            canvas.drawText(str, textRectWidth4, f12, paint);
            paint.setTextSize(f4);
            canvas.drawText(format, (textRectWidth4 - f3) - getTextRectWidth(paint, str), f2, paint);
        }
    }

    public static float px2Dp(int i, Context context) {
        return ((float) i) * context.getResources().getDisplayMetrics().density;
    }

    public static int getTotalHeightOfListView(ListView listView, Adapter adapter) {
        if (adapter == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < adapter.getCount(); i2++) {
            View view = adapter.getView(i2, null, listView);
            if (view.getMeasuredHeight() == 0) {
                view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            }
            i += view.getMeasuredHeight();
        }
        return i + (listView.getDividerHeight() * (adapter.getCount() - 1));
    }

    public static boolean getTotalHeightOfListView(ListView listView, Adapter adapter, int i) {
        if (adapter == null) {
            return false;
        }
        System.currentTimeMillis();
        int i2 = 0;
        for (int i3 = 0; i3 < adapter.getCount(); i3++) {
            View view = adapter.getView(i3, null, listView);
            if (view.getMeasuredHeight() == 0) {
                view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            }
            i2 += view.getMeasuredHeight();
            if (i2 > i) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0006  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void drawLine(android.graphics.Canvas r10, float r11, float r12, float r13, float r14, android.graphics.Paint r15) {
        /*
            float r0 = r14 / r13
            int r0 = (int) r0
            r1 = 0
        L_0x0004:
            if (r1 >= r0) goto L_0x0024
            int r1 = r1 + 1
            float r2 = (float) r1
            float r2 = r2 * r13
            float r9 = r2 + r11
            r3 = 1073741824(0x40000000, float:2.0)
            float r3 = r13 / r3
            float r2 = r2 + r3
            float r2 = r2 + r11
            r3 = r10
            r4 = r9
            r5 = r12
            r6 = r2
            r7 = r12
            r8 = r15
            r3.drawLine(r4, r5, r6, r7, r8)
            int r3 = (r9 > r14 ? 1 : (r9 == r14 ? 0 : -1))
            if (r3 >= 0) goto L_0x0024
            int r2 = (r2 > r14 ? 1 : (r2 == r14 ? 0 : -1))
            if (r2 < 0) goto L_0x0004
        L_0x0024:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.ViewUtil.drawLine(android.graphics.Canvas, float, float, float, float, android.graphics.Paint):void");
    }
}
