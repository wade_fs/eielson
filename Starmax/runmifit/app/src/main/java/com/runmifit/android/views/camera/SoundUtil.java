package com.wade.fit.views.camera;

import android.content.Context;
import android.media.SoundPool;
import com.wade.fit.R;

public class SoundUtil {
    private Context context;
    private int musicId;
    private SoundPool spl = new SoundPool(10, 1, 5);

    public SoundUtil(Context context2) {
        this.musicId = this.spl.load(context2, R.raw.shot, 1);
        this.context = context2;
    }

    public Context getContext() {
        return this.context;
    }

    public void play() {
        int i;
        SoundPool soundPool = this.spl;
        if (soundPool != null && (i = this.musicId) != 0) {
            soundPool.play(i, 1.0f, 1.0f, 0, 0, 1.0f);
        }
    }

    public void destory() {
        SoundPool soundPool = this.spl;
        if (soundPool != null) {
            soundPool.release();
        }
    }
}
