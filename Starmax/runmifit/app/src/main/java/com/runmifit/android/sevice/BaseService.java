package com.wade.fit.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.ObjectUtil;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class BaseService<P extends BasePersenter> extends Service {
    protected P mPersenter;

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        EventBusHelper.register(this);
        initPresenter();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleMessageInner(BaseMessage baseMessage) {
        handleMessage(baseMessage);
    }

    private void initPresenter() {
        this.mPersenter = (BasePersenter) ObjectUtil.getParameterizedType(getClass());
        P p = this.mPersenter;
        if (p != null) {
            p.attachView((IBaseView) this);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        P p = this.mPersenter;
        if (p != null) {
            p.detachView();
        }
        EventBusHelper.unregister(this);
    }
}
