package com.wade.fit.persenter.mine;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.UserBean;

public interface IGetUserInfoView extends IBaseView {
    UserBean getCurrentUserBean();
}
