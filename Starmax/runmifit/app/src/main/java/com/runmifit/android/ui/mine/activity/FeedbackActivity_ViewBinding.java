package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.FeedbackActivity_ViewBinding */
public class FeedbackActivity_ViewBinding implements Unbinder {
    private FeedbackActivity target;
    private View view2131296335;

    public FeedbackActivity_ViewBinding(FeedbackActivity feedbackActivity) {
        this(feedbackActivity, feedbackActivity.getWindow().getDecorView());
    }

    public FeedbackActivity_ViewBinding(final FeedbackActivity feedbackActivity, View view) {
        this.target = feedbackActivity;
        feedbackActivity.edContent = (EditText) Utils.findRequiredViewAsType(view, R.id.et_feed_content, "field 'edContent'", EditText.class);
        feedbackActivity.edEmail = (EditText) Utils.findRequiredViewAsType(view, R.id.et_email, "field 'edEmail'", EditText.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.btnSave, "field 'btnSave' and method 'toCommit'");
        feedbackActivity.btnSave = (Button) Utils.castView(findRequiredView, R.id.btnSave, "field 'btnSave'", Button.class);
        this.view2131296335 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.FeedbackActivity_ViewBinding.C25781 */

            public void doClick(View view) {
                feedbackActivity.toCommit();
            }
        });
        feedbackActivity.tvContentLength = (TextView) Utils.findRequiredViewAsType(view, R.id.tv_content_length, "field 'tvContentLength'", TextView.class);
    }

    public void unbind() {
        FeedbackActivity feedbackActivity = this.target;
        if (feedbackActivity != null) {
            this.target = null;
            feedbackActivity.edContent = null;
            feedbackActivity.edEmail = null;
            feedbackActivity.btnSave = null;
            feedbackActivity.tvContentLength = null;
            this.view2131296335.setOnClickListener(null);
            this.view2131296335 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
