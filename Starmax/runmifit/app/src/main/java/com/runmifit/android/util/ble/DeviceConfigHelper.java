package com.wade.fit.util.ble;

import com.wade.fit.app.AppApplication;
import com.wade.fit.app.BleContant;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.AlarmBean;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.DeviceState;
import com.wade.fit.model.bean.Goal;
import com.wade.fit.model.bean.HeartRateBean;
import com.wade.fit.model.bean.LongSitBean;
import com.wade.fit.model.bean.SleepTime;
import com.wade.fit.model.bean.SleepTimeBean;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.task.AsyncTask;
import com.wade.fit.task.RunTask;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.List;

public class DeviceConfigHelper extends TimeOut implements Runnable, BleContant {
    private BleCallback bleCallback;
    int progress = 6;
    /* access modifiers changed from: private */
    public int state = 105;

    public void synchDeviceConfig(BleCallback bleCallback2) {
        final BleCallback bleCallback3 = bleCallback2;
        if (this.state == 106) {
            LogUtil.dAndSave("已经有同步配置信息任务了", Constants.SYCN_PATH);
            return;
        }
        this.bleCallback = bleCallback3;
        this.state = 106;
        poseTimeOut(this, 20000);
        LogUtil.dAndSave("******同步配置信息开始*********", Constants.SYCN_PATH);
        C26651 r2 = new RunTask<UserBean>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26651 */

            public void onPreExecute(UserBean userBean) {
                LogUtil.dAndSave("同步配置信息---setUserInfo>", Constants.SYCN_PATH);
                BleSdkWrapper.getUserInfo(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26651.C26661 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.callback(106, 1);
            }
        };
        C26692 r3 = new RunTask<LongSitBean>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26692 */

            public void onPreExecute(LongSitBean longSitBean) {
                LogUtil.dAndSave("同步配置信息---setLongSit>", Constants.SYCN_PATH);
                BleSdkWrapper.getLongSit(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26692.C26701 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.callback(106, 2);
                DeviceConfigHelper.this.progress = 5;
            }
        };
        C26713 r4 = new RunTask<DeviceState>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26713 */

            public void onPreExecute(DeviceState deviceState) {
                LogUtil.dAndSave("同步配置信息---setDeviceState>", Constants.SYCN_PATH);
                DeviceConfigHelper.this.callback(106, 3);
                BleSdkWrapper.getDeviceState(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26713.C26721 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.progress = 5;
            }
        };
        C26734 r5 = new RunTask<Goal>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26734 */

            public void onPreExecute(Goal goal) {
                LogUtil.dAndSave("同步配置信息---setTarget>", Constants.SYCN_PATH);
                BleSdkWrapper.getTarget(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26734.C26741 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.callback(106, 4);
                DeviceConfigHelper.this.progress = 5;
            }
        };
        C26755 r6 = new RunTask<HeartRateBean>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26755 */

            public void onPreExecute(HeartRateBean heartRateBean) {
                LogUtil.dAndSave("同步配置信息---setHartRong>", Constants.SYCN_PATH);
                BleSdkWrapper.getHartRong(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26755.C26761 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.callback(106, 4);
                DeviceConfigHelper.this.progress = 5;
            }
        };
        C26776 r7 = new RunTask<SleepTimeBean>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26776 */

            public void onPreExecute(SleepTimeBean sleepTimeBean) {
                LogUtil.dAndSave("同步配置信息---setSleepTime>", Constants.SYCN_PATH);
                BleSdkWrapper.getSleepStatus(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26776.C26781 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.callback(106, 5);
                DeviceConfigHelper.this.progress = 5;
            }
        };
        C26797 r8 = new RunTask<Object>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26797 */

            public void onPreExecute(Object obj) {
                LogUtil.dAndSave("同步配置信息---getPower>", Constants.SYCN_PATH);
                BleSdkWrapper.getPower(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26797.C26801 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.callback(106, 6);
                DeviceConfigHelper.this.progress = 5;
            }
        };
        C26818 r9 = new RunTask<Object>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26818 */

            public void onPreExecute(Object obj) {
                LogUtil.dAndSave("同步配置信息---getDeviceInfo>", Constants.SYCN_PATH);
                BleSdkWrapper.getDeviceInfo(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26818.C26821 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.callback(106, 7);
                DeviceConfigHelper.this.progress = 5;
            }
        };
        C26839 r10 = new RunTask<Object>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C26839 */

            public void onPreExecute(Object obj) {
                LogUtil.dAndSave("同步配置信息---getNotice>", Constants.SYCN_PATH);
                BleSdkWrapper.getNotice(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C26839.C26841 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        bleCallback3.setSuccess();
                    }
                }, false);
                DeviceConfigHelper.this.callback(106, 8);
                DeviceConfigHelper.this.progress = 5;
            }
        };
        C266710 r11 = new RunTask<List<AlarmBean>>() {
            /* class com.wade.fit.util.ble.DeviceConfigHelper.C266710 */

            public /* bridge */ /* synthetic */ void onPreExecute(Object obj) {
                onPreExecute((List<AlarmBean>) ((List) obj));
            }

            public void onPreExecute(List<AlarmBean> list) {
                LogUtil.dAndSave("同步配置信息---setAlarm>", Constants.SYCN_PATH);
                BleSdkWrapper.getAlarmList(new MyBleCallback(super) {
                    /* class com.wade.fit.util.ble.DeviceConfigHelper.C266710.C26681 */

                    public void complete(int i, Object obj) {
                        super.complete(i, obj);
                        DeviceConfigHelper.this.progress++;
                        bleCallback3.setSuccess();
                        if ((obj instanceof HandlerBleDataResult) && ((HandlerBleDataResult) obj).isComplete) {
                            LogUtil.dAndSave("******同步配置信息完成 ********* 成功", Constants.SYCN_PATH);
                            DeviceConfigHelper.this.removeTimeOutTask();
                            int unused = DeviceConfigHelper.this.state = 107;
                            DeviceConfigHelper.this.callback(DeviceConfigHelper.this.state, 10);
                        }
                    }
                }, false);
            }
        };
        UserBean userBean = AppApplication.getInstance().getUserBean();
        DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
        AsyncTask asyncTask = new AsyncTask();
        ArrayList arrayList = new ArrayList();
        SleepTimeBean sleepTimeBean = new SleepTimeBean();
        ArrayList arrayList2 = new ArrayList();
        C266710 r16 = r11;
        SleepTime sleepTime = new SleepTime();
        sleepTime.startH = 10;
        sleepTime.startM = 0;
        sleepTime.endH = 20;
        sleepTime.endM = 0;
        sleepTime.state = 1;
        arrayList2.add(sleepTime);
        sleepTimeBean.sleepTimes = arrayList2;
        asyncTask.execute(r2, userBean).execute(r3, deviceConfig.longSitBean).execute(r4, deviceConfig.deviceState).execute(r5, deviceConfig.goal).execute(r7, sleepTimeBean).execute(r6, null).execute(r8, "").execute(r9, "").execute(r10, "").execute(r16, arrayList);
    }

    public void run() {
        this.state = 108;
        LogUtil.dAndSave("******同步配置信息完成 ********* 失败", Constants.SYCN_PATH);
        callback(this.state, 0);
    }

    /* access modifiers changed from: private */
    public void callback(int i, int i2) {
        BleCallback bleCallback2 = this.bleCallback;
        if (bleCallback2 != null) {
            bleCallback2.complete(i, Integer.valueOf(i2));
        }
    }

    class MyBleCallback implements BleCallback {
        RunTask runTask;

        public void setSuccess() {
        }

        public MyBleCallback(RunTask runTask2) {
            this.runTask = runTask2;
        }

        public void complete(int i, Object obj) {
            StringBuilder sb = new StringBuilder();
            sb.append("resultCode:");
            sb.append(i);
            if (obj == null) {
                obj = "data is null";
            }
            sb.append(obj);
            LogUtil.d(sb.toString());
            this.runTask.runFinish();
        }
    }
}
