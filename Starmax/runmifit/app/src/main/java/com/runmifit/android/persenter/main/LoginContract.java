package com.wade.fit.persenter.main;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.BaseBean;

public interface LoginContract {

    public interface View extends IBaseView {
        void LoginSuccess(int i, BaseBean baseBean);

        void loginFaild(int i);

        void registerFaild(int i);

        void registerSuccess();

        void sendCodeFaild(int i);

        void sendCodeSuccess();
    }

    public interface Presenter {
        void loginByFacebook(String str, String str2);

        void regiestAccount(String str, String str2, String str3);

        void requestLogin(String str, String str2);

        void requestLoginByWechat(String str, String str2);

        void sendCode(String str);
    }
}
