package com.wade.fit.persenter.main;

import com.wade.fit.base.IBaseView;
import com.wade.fit.greendao.bean.HealthSport;
import com.wade.fit.views.MainVO;

public interface MainFragmentContract {

    public interface Presenter {
        int geeSleepTime(int i, int i2, int i3);

        void getHistoryData(int i, int i2, int i3, int i4);

        void startTime();

        void stopTime();

        void synchDate();
    }

    public interface View extends IBaseView {
        void requestFaild(int i);

        void requestSuccess(int i, MainVO mainVO);

        void updateHeartVaule(int i, int i2);

        void updateProgress(int i);

        void updateSportVaule(HealthSport healthSport);

        void updateSych();
    }
}
