package com.wade.fit.persenter.mine;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.UserBean;

public interface PersonContract {

    public interface Presenter {
        void setUserInfo(UserBean userBean);
    }

    public interface View extends IBaseView {
        void requestFaild();

        void requestSuccess();
    }
}
