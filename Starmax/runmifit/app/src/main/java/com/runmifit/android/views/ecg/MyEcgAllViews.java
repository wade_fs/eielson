package com.wade.fit.views.ecg;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.tamic.novate.download.MimeType;
import java.util.ArrayList;
import java.util.List;

public class MyEcgAllViews extends View {
    private static float OFFSET_HEART_VALUE = 4096.0f;
    private static int offsetBS = 1;
    private static float startX;
    private static float x_changed;
    private int dataNum_per_grid = 30;
    private int data_num;
    public List<Float> data_source = new ArrayList();
    private float gap_grid;
    private float gap_x;
    private int grid_hori;
    private int grid_ver;
    private int height;
    private Paint mPaint01;
    private float multiple_for_rect_width;
    private final int offset = 1;
    private float offset_x_max;
    private float rect_gap_x;
    private int rect_high = 80;
    private float rect_width;
    private int width;
    private float x_change;
    private int xori;

    public MyEcgAllViews(Context context) {
        super(context);
    }

    public MyEcgAllViews(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public MyEcgAllViews(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private void init() {
        this.mPaint01 = new Paint();
        this.mPaint01.setColor(Color.parseColor("#FF0000"));
        this.mPaint01.setStyle(Paint.Style.STROKE);
        this.mPaint01.setStrokeWidth(2.0f);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (z) {
            this.xori = 0;
            this.gap_grid = 30.0f;
            this.width = getWidth();
            this.height = getHeight();
            int i5 = this.height;
            float f = this.gap_grid;
            this.grid_hori = i5 / ((int) f);
            this.grid_ver = this.width / ((int) f);
            this.gap_x = f / ((float) this.dataNum_per_grid);
            this.data_num = this.data_source.size();
            this.x_change = 0.0f;
            x_changed = 0.0f;
            int i6 = this.width;
            float f2 = this.gap_x;
            int i7 = this.data_num;
            this.offset_x_max = ((float) i6) - (((float) i7) * f2);
            this.rect_gap_x = ((float) i6) / ((float) i7);
            this.rect_width = (((float) i6) * ((float) i6)) / (f2 * ((float) i7));
            this.multiple_for_rect_width = ((float) i6) / this.rect_width;
            Log.e(MimeType.JSON, "本页面宽： " + this.width + "  高:" + this.height);
            Log.e(MimeType.JSON, "两点间横坐标间距:" + this.gap_x + "   矩形区域两点间横坐标间距：" + this.rect_gap_x);
        }
        super.onLayout(z, i, i2, i3, i4);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        drawing(canvas);
    }

    private void drawing(Canvas canvas) {
        Path path = new Path();
        x_changed += this.x_change;
        float f = x_changed;
        int i = this.xori;
        if (f > ((float) i)) {
            x_changed = (float) i;
        } else {
            float f2 = this.offset_x_max;
            if (f < f2) {
                x_changed = f2;
            }
        }
        Path path2 = path;
        float f3 = 0.0f;
        int i2 = 1;
        for (int i3 = 0; i3 < this.data_source.size(); i3++) {
            if (i3 == this.data_source.size() - 1) {
                path2.lineTo(f3, ((float) ((getHeight() / 5) * i2)) + (((this.data_source.get(i3).floatValue() * ((float) getHeight())) / OFFSET_HEART_VALUE) / 4.0f));
                canvas.drawPath(path2, this.mPaint01);
            } else {
                double d = (double) f3;
                Double.isNaN(d);
                f3 = (float) (d + 0.5d);
                if (f3 < ((float) this.width)) {
                    path2.lineTo(f3, ((float) ((getHeight() / 5) * i2)) + (((this.data_source.get(i3).floatValue() * ((float) getHeight())) / OFFSET_HEART_VALUE) / 4.0f));
                } else {
                    canvas.drawPath(path2, this.mPaint01);
                    i2++;
                    Path path3 = new Path();
                    path3.lineTo(0.0f, ((float) ((getHeight() / 5) * i2)) + (((this.data_source.get(i3).floatValue() * ((float) getHeight())) / OFFSET_HEART_VALUE) / 4.0f));
                    path2 = path3;
                    f3 = 0.0f;
                }
            }
        }
    }

    public void setData(List<Float> list) {
        this.data_source = list;
        invalidate();
    }
}
