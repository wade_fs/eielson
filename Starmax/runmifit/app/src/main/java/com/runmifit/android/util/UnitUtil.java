package com.wade.fit.util;

import android.app.Activity;
import android.content.res.Resources;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.util.ble.BleSdkWrapper;
import java.math.BigDecimal;

public class UnitUtil {
    public static final float KG_UNIT = 0.4535924f;
    public static final float POUND_UNIT = 2.2046225f;

    public static int cm2inchs(int i) {
        return (int) (((float) i) * 0.3937008f);
    }

    public static int ft2in(int i) {
        return i * 12;
    }

    public static float getKg2Pound(float f) {
        return f * 2.2046225f;
    }

    public static float getKg2St(float f) {
        return f * 0.157f;
    }

    public static float getPound2Kg(float f) {
        return f * 0.4535924f;
    }

    public static float getPound2St(float f) {
        return f * 0.07f;
    }

    public static float getSt2Kg(float f) {
        return f * 6.35f;
    }

    public static float getSt2Lb(float f) {
        return f * 14.0f;
    }

    public static int inch2cm(int i) {
        return (int) (((float) i) / 0.3937008f);
    }

    public static int inchs2cm(int i) {
        return (int) (((float) i) / 0.3937008f);
    }

    public static float km2mile(float f) {
        return f * 0.6213712f;
    }

    public static int lb2kg(float f) {
        return (int) (f * 0.4535924f);
    }

    public static float mile2km(float f) {
        return f * 1.609344f;
    }

    public static int st2lb(int i) {
        return i * 14;
    }

    public static int newInch2cm(int[] iArr) {
        return inch2cm(iArr);
    }

    public static int newCm2inch(int[] iArr) {
        return (int) (((float) (iArr[0] + iArr[1])) / 3.0172415f);
    }

    public static int inch2cm(int[] iArr) {
        iArr[1] = (iArr[0] * 12) + iArr[1];
        return (int) (((float) iArr[1]) / 0.3937008f);
    }

    public static int inch2inch(int[] iArr) {
        return (iArr[0] * 12) + iArr[1];
    }

    public static int[] inch2inch(int i) {
        return new int[]{i / 12, i % 12};
    }

    public static int[] cm2inch(int i) {
        int[] iArr = new int[2];
        iArr[1] = cm2inchs(i);
        iArr[0] = iArr[1] / 12;
        iArr[1] = iArr[1] % 12;
        return iArr;
    }

    public static int kg2lb(float f) {
        double d = (double) f;
        Double.isNaN(d);
        return (int) (d * 2.2046226d);
    }

    public static int kg2st(int i) {
        double d = (double) i;
        Double.isNaN(d);
        return (int) (d * 0.157d);
    }

    public static int lb2st(int i) {
        double d = (double) i;
        Double.isNaN(d);
        return (int) (d * 0.071d);
    }

    public static int st2kg(int i) {
        double d = (double) i;
        Double.isNaN(d);
        return (int) (d * 6.35d);
    }

    public static String getUnitByType(Activity activity, int i) {
        return getUnitByType(activity);
    }

    public static String getUnitByType(Activity activity) {
        if (BleSdkWrapper.isDistUnitKm()) {
            return AppApplication.getInstance().getResources().getString(R.string.unit_kilometer);
        }
        return AppApplication.getInstance().getResources().getString(R.string.unit_mi);
    }

    public static String getUnitByType() {
        return getUnitByType(null);
    }

    public static String getUnit(Resources resources, int i) {
        return getUnitByType(null);
    }

    public static float getKm2mile(float f) {
        return km2mile(f);
    }

    public static float getmile2Km(float f) {
        return mile2km(f);
    }

    public static float getWeight(String str) {
        return NumUtil.parseFloat(String.valueOf(new BigDecimal(str).setScale(0, 4)));
    }

    public static int getMode() {
        return BleSdkWrapper.getDistanceUnit();
    }

    public static String getUnitStr(int i, int i2) {
        if (i2 == 0) {
            return String.valueOf(i);
        }
        int[] inch2inch = inch2inch(i);
        return inch2inch[0] + "'" + inch2inch[1] + "\"";
    }

    public static int getPaceInt(int i, int i2) {
        float f;
        float km2mile;
        if (i2 == 0) {
            return 0;
        }
        if (BleSdkWrapper.isDistUnitKm()) {
            f = ((float) i) * 1000.0f;
            km2mile = (float) i2;
        } else {
            f = ((float) i) * 1000.0f;
            km2mile = getKm2mile((float) i2);
        }
        return (int) (f / km2mile);
    }

    public static String getPaceStr(int i, int i2) {
        return DateUtil.computeTimeMS(getPaceInt(i, i2));
    }

    public static String getUnitStr() {
        return !BleSdkWrapper.isDistUnitKm() ? AppApplication.getInstance().getResources().getString(R.string.unit_mi) : AppApplication.getInstance().getResources().getString(R.string.unit_kilometer);
    }
}
