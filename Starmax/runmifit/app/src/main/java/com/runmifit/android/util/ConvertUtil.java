package com.wade.fit.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.View;
import com.google.common.base.Ascii;
import com.google.common.primitives.UnsignedBytes;
import com.wade.fit.app.Constant;
import com.wade.fit.util.file.FileUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class ConvertUtil {
    public static final int BYTE = 1;

    /* renamed from: GB */
    public static final int f5224GB = 1073741824;

    /* renamed from: KB */
    public static final int f5225KB = 1024;

    /* renamed from: MB */
    public static final int f5226MB = 1048576;
    static final char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private ConvertUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    public static String bytes2HexString(byte[] bArr) {
        char[] cArr = new char[(bArr.length << 1)];
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            int i3 = i + 1;
            char[] cArr2 = hexDigits;
            cArr[i] = cArr2[(bArr[i2] >>> 4) & 15];
            i = i3 + 1;
            cArr[i3] = cArr2[bArr[i2] & Ascii.f4512SI];
        }
        return new String(cArr);
    }

    public static byte[] hexString2Bytes(String str) {
        int length = str.length() + 1;
        if (length % 2 == 0) {
            str = "0" + str;
        }
        char[] charArray = str.toUpperCase().toCharArray();
        byte[] bArr = new byte[(length >> 1)];
        for (int i = 0; i < length; i += 2) {
            bArr[i >> 1] = (byte) ((hex2Dec(charArray[i]) << 4) | hex2Dec(charArray[i + 1]));
        }
        return bArr;
    }

    private static int hex2Dec(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'A' && c <= 'F') {
            return (c - 'A') + 10;
        }
        throw new IllegalArgumentException();
    }

    public static byte[] chars2Bytes(char[] cArr) {
        int length = cArr.length;
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) cArr[i];
        }
        return bArr;
    }

    public static char[] bytes2Chars(byte[] bArr) {
        int length = bArr.length;
        char[] cArr = new char[length];
        for (int i = 0; i < length; i++) {
            cArr[i] = (char) (bArr[i] & UnsignedBytes.MAX_VALUE);
        }
        return cArr;
    }

    /* renamed from: com.wade.fit.util.ConvertUtil$1 */
    static /* synthetic */ class C26381 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$app$Constant$MemoryUnit = new int[Constant.MemoryUnit.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.wade.fit.app.Constant$MemoryUnit[] r0 = com.wade.fit.app.Constant.MemoryUnit.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.util.ConvertUtil.C26381.$SwitchMap$com$wade.fit$app$Constant$MemoryUnit = r0
                int[] r0 = com.wade.fit.util.ConvertUtil.C26381.$SwitchMap$com$wade.fit$app$Constant$MemoryUnit     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.app.Constant$MemoryUnit r1 = com.wade.fit.app.Constant.MemoryUnit.BYTE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.util.ConvertUtil.C26381.$SwitchMap$com$wade.fit$app$Constant$MemoryUnit     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.app.Constant$MemoryUnit r1 = com.wade.fit.app.Constant.MemoryUnit.KB     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.util.ConvertUtil.C26381.$SwitchMap$com$wade.fit$app$Constant$MemoryUnit     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.app.Constant$MemoryUnit r1 = com.wade.fit.app.Constant.MemoryUnit.MB     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.wade.fit.util.ConvertUtil.C26381.$SwitchMap$com$wade.fit$app$Constant$MemoryUnit     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.wade.fit.app.Constant$MemoryUnit r1 = com.wade.fit.app.Constant.MemoryUnit.GB     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.ConvertUtil.C26381.<clinit>():void");
        }
    }

    public static double byte2Size(long j, Constant.MemoryUnit memoryUnit) {
        double d;
        double d2;
        if (j < 0) {
            return -1.0d;
        }
        int i = C26381.$SwitchMap$com$wade.fit$app$Constant$MemoryUnit[memoryUnit.ordinal()];
        if (i == 2) {
            d = (double) j;
            d2 = 1024.0d;
            Double.isNaN(d);
        } else if (i == 3) {
            d = (double) j;
            d2 = 1048576.0d;
            Double.isNaN(d);
        } else if (i != 4) {
            d = (double) j;
            d2 = 1.0d;
            Double.isNaN(d);
        } else {
            d = (double) j;
            d2 = 1.073741824E9d;
            Double.isNaN(d);
        }
        return d / d2;
    }

    public static long size2Byte(long j, Constant.MemoryUnit memoryUnit) {
        if (j < 0) {
            return -1;
        }
        int i = C26381.$SwitchMap$com$wade.fit$app$Constant$MemoryUnit[memoryUnit.ordinal()];
        return j * (i != 2 ? i != 3 ? i != 4 ? 1 : 1073741824 : PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED : 1024);
    }

    public static String byte2FitSize(long j) {
        if (j < 0) {
            return "shouldn't be less than zero!";
        }
        if (j < 1024) {
            return String.format(Locale.getDefault(), "%.3fB", Double.valueOf((double) j));
        } else if (j < PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
            Locale locale = Locale.getDefault();
            double d = (double) j;
            Double.isNaN(d);
            return String.format(locale, "%.3fKB", Double.valueOf(d / 1024.0d));
        } else if (j < 1073741824) {
            Locale locale2 = Locale.getDefault();
            double d2 = (double) j;
            Double.isNaN(d2);
            return String.format(locale2, "%.3fMB", Double.valueOf(d2 / 1048576.0d));
        } else {
            Locale locale3 = Locale.getDefault();
            double d3 = (double) j;
            Double.isNaN(d3);
            return String.format(locale3, "%.3fGB", Double.valueOf(d3 / 1.073741824E9d));
        }
    }

    public static ByteArrayOutputStream input2OutputStream(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr, 0, 1024);
                if (read == -1) {
                    return byteArrayOutputStream;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            FileUtil.closeIO(inputStream);
        }
    }

    public ByteArrayInputStream output2InputStream(OutputStream outputStream) {
        if (outputStream == null) {
            return null;
        }
        return new ByteArrayInputStream(((ByteArrayOutputStream) outputStream).toByteArray());
    }

    public static byte[] inputStream2Bytes(InputStream inputStream) {
        return input2OutputStream(inputStream).toByteArray();
    }

    public static InputStream bytes2InputStream(byte[] bArr) {
        return new ByteArrayInputStream(bArr);
    }

    public static byte[] outputStream2Bytes(OutputStream outputStream) {
        if (outputStream == null) {
            return null;
        }
        return ((ByteArrayOutputStream) outputStream).toByteArray();
    }

    public static OutputStream bytes2OutputStream(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayOutputStream byteArrayOutputStream2 = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byteArrayOutputStream.write(bArr);
                FileUtil.closeIO(byteArrayOutputStream);
                return byteArrayOutputStream;
            } catch (IOException e) {
                e = e;
                try {
                    e.printStackTrace();
                    FileUtil.closeIO(byteArrayOutputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    FileUtil.closeIO(byteArrayOutputStream2);
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            byteArrayOutputStream = null;
            e.printStackTrace();
            FileUtil.closeIO(byteArrayOutputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            FileUtil.closeIO(byteArrayOutputStream2);
            throw th;
        }
    }

    public static String inputStream2String(InputStream inputStream, String str) {
        if (inputStream != null && !StringUtils.isSpace(str)) {
            try {
                return new String(inputStream2Bytes(inputStream), str);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static InputStream string2InputStream(String str, String str2) {
        if (str != null && !StringUtils.isSpace(str2)) {
            try {
                return new ByteArrayInputStream(str.getBytes(str2));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String outputStream2String(OutputStream outputStream, String str) {
        if (outputStream == null) {
            return null;
        }
        try {
            return new String(outputStream2Bytes(outputStream), str);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static OutputStream string2OutputStream(String str, String str2) {
        if (str != null && !StringUtils.isSpace(str2)) {
            try {
                return bytes2OutputStream(str.getBytes(str2));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static byte[] bitmap2Bytes(Bitmap bitmap, Bitmap.CompressFormat compressFormat) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(compressFormat, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static Bitmap bytes2Bitmap(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return null;
        }
        return BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
    }

    public static Bitmap drawable2Bitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        return ((BitmapDrawable) drawable).getBitmap();
    }

    public static Drawable bitmap2Drawable(Resources resources, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        return new BitmapDrawable(resources, bitmap);
    }

    public static byte[] drawable2Bytes(Drawable drawable, Bitmap.CompressFormat compressFormat) {
        return bitmap2Bytes(drawable2Bitmap(drawable), compressFormat);
    }

    public static Drawable bytes2Drawable(Resources resources, byte[] bArr) {
        return bitmap2Drawable(resources, bytes2Bitmap(bArr));
    }

    public static Bitmap view2Bitmap(View view) {
        if (view == null) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        Drawable background = view.getBackground();
        if (background != null) {
            background.draw(canvas);
        } else {
            canvas.drawColor(-1);
        }
        view.draw(canvas);
        return createBitmap;
    }

    public static int dp2px(Context context, float f) {
        return (int) ((f * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int px2dp(Context context, float f) {
        return (int) ((f / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int sp2px(Context context, float f) {
        return (int) ((f * context.getResources().getDisplayMetrics().scaledDensity) + 0.5f);
    }

    public static int px2sp(Context context, float f) {
        return (int) ((f / context.getResources().getDisplayMetrics().scaledDensity) + 0.5f);
    }
}
