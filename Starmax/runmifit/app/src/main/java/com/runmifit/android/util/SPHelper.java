package com.wade.fit.util;

import android.text.TextUtils;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.Alarm;
import com.wade.fit.model.bean.AppNotice;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.DeviceState;
import com.wade.fit.model.bean.Function;
import com.wade.fit.model.bean.HeartRateInterval;
import com.wade.fit.model.bean.LongSit;
import com.wade.fit.model.bean.SleepTimeBean;
import com.wade.fit.model.bean.UserBean;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SPHelper {
    public static Function getFunction() {
        Function function = (Function) GsonUtil.fromJson((String) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.FUNCTION, ""), Function.class);
        return function == null ? new Function() : function;
    }

    public static DeviceState getDeviceState() {
        return getDeviceConfig().deviceState;
    }

    public static HeartRateInterval getInterval() {
        return getDeviceConfig().interval;
    }

    public static LongSit getLongSit() {
        return (LongSit) GsonUtil.fromJson((String) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.LONG_SIT_KEY, ""), LongSit.class);
    }

    public static void saveLongSit(LongSit longSit) {
        SharePreferenceUtils.put(AppApplication.getInstance(), Constants.LONG_SIT_KEY, GsonUtil.toJson(longSit));
    }

    public static void saveInterval(HeartRateInterval heartRateInterval) {
        if (heartRateInterval != null) {
            DeviceConfig deviceConfig = getDeviceConfig();
            deviceConfig.interval = heartRateInterval;
            saveDeviceConfig(deviceConfig);
        }
    }

    public static void saveDeviceState(DeviceState deviceState) {
        DeviceConfig deviceConfig = getDeviceConfig();
        deviceConfig.deviceState = deviceState;
        saveDeviceConfig(deviceConfig);
    }

    public static void saveBLEDevice(BLEDevice bLEDevice) {
        if (bLEDevice != null) {
            DeviceConfig deviceConfig = getDeviceConfig();
            if (deviceConfig.bleDeviceList == null) {
                deviceConfig.bleDeviceList = new ArrayList();
            } else if (!deviceConfig.bleDeviceList.isEmpty()) {
                deviceConfig.bleDeviceList.clear();
            }
            deviceConfig.bleDeviceList.add(bLEDevice);
            saveDeviceConfig(deviceConfig);
        }
    }

    public static BLEDevice getBindBLEDevice() {
        DeviceConfig deviceConfig = getDeviceConfig();
        if (ObjectUtil.isCollectionEmpty((Collection) deviceConfig.bleDeviceList)) {
            return null;
        }
        return deviceConfig.bleDeviceList.get(0);
    }

    public static String getBindMac() {
        BLEDevice bindBLEDevice = getBindBLEDevice();
        if (bindBLEDevice == null) {
            return null;
        }
        return bindBLEDevice.mDeviceAddress;
    }

    public static void saveMusic(boolean z, String str) {
        DeviceConfig deviceConfig = getDeviceConfig();
        deviceConfig.isMusic = z;
        deviceConfig.musicPackageName = str;
        saveDeviceConfig(deviceConfig);
    }

    public static void saveAlarms(List<Alarm> list) {
        if (list != null) {
            DeviceConfig deviceConfig = getDeviceConfig();
            deviceConfig.alarms = list;
            saveDeviceConfig(deviceConfig);
        }
    }

    public static List<Alarm> getAlarms() {
        return getDeviceConfig().alarms;
    }

    public static void saveAppNotice(AppNotice appNotice) {
        if (!ObjectUtil.isCollectionEmpty(appNotice)) {
            DeviceConfig deviceConfig = getDeviceConfig();
            deviceConfig.notice = appNotice;
            saveDeviceConfig(deviceConfig);
        }
    }

    public static void saveisTestTime(boolean z) {
        DeviceConfig deviceConfig = getDeviceConfig();
        deviceConfig.isTestTime = z;
        saveDeviceConfig(deviceConfig);
    }

    public static void saveSleep(SleepTimeBean sleepTimeBean) {
        DeviceConfig deviceConfig = getDeviceConfig();
        deviceConfig.sleepTimeBean = sleepTimeBean;
        saveDeviceConfig(deviceConfig);
    }

    public static void saveisMessage(boolean z) {
        DeviceConfig deviceConfig = getDeviceConfig();
        deviceConfig.isMessage = z;
        saveDeviceConfig(deviceConfig);
    }

    public static void saveisCall(boolean z) {
        DeviceConfig deviceConfig = getDeviceConfig();
        deviceConfig.isCall = z;
        saveDeviceConfig(deviceConfig);
    }

    public static void saveDeviceConfig(DeviceConfig deviceConfig) {
        saveDeviceConfig(deviceConfig, true);
    }

    public static void saveDeviceConfig(DeviceConfig deviceConfig, boolean z) {
        if (!ObjectUtil.isCollectionEmpty(deviceConfig)) {
            if (z) {
                CacheHelper.getInstance().isLogin();
            }
            SharePreferenceUtils.put(AppApplication.getInstance(), Constants.DEVICE_CONFIG_KEY, GsonUtil.toJson(deviceConfig));
        }
    }

    public static DeviceConfig getDeviceConfig() {
        String str = (String) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.DEVICE_CONFIG_KEY, "");
        if (str == null) {
            return null;
        }
        DeviceConfig deviceConfig = (DeviceConfig) GsonUtil.fromJson(str, DeviceConfig.class);
        return deviceConfig == null ? new DeviceConfig() : deviceConfig;
    }

    private static UserBean createDefaultUserBean() {
        UserBean userBean = new UserBean();
        userBean.setUserId("0");
        userBean.setUserName("");
        userBean.setYear(DateUtil.todayYearMonthDay()[0] - 20);
        userBean.setMonth(1);
        userBean.setDay(1);
        userBean.setBirthday(userBean.getYear() + "-" + userBean.getMonth() + "-" + userBean.getDay());
        userBean.setHeight(Alarm.STATUS_NOT_DISPLAY);
        userBean.setHeightLb(68);
        userBean.setWeight(Constants.DEFAULT_WEIGHT_KG);
        userBean.setWeightLb(132);
        userBean.setWeightSt(10);
        userBean.setStepDistance(75);
        saveUserBean(userBean);
        return userBean;
    }

    public static void saveUserBean(UserBean userBean) {
        if (userBean == null) {
            SharePreferenceUtils.put(AppApplication.getInstance(), Constants.USER_INFO_KEY, "");
        } else {
            SharePreferenceUtils.put(AppApplication.getInstance(), Constants.USER_INFO_KEY, GsonUtil.toJson(userBean));
        }
    }

    public static UserBean getCurrentUserBean() {
        String str = (String) SharePreferenceUtils.get(AppApplication.getInstance(), Constants.USER_INFO_KEY, "");
        if (str == null || TextUtils.isEmpty(str)) {
            return createDefaultUserBean();
        }
        return (UserBean) GsonUtil.fromJson(str, UserBean.class);
    }
}
