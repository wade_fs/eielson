package com.wade.fit.util;

public class BaseCmdUtil {
    public static final int BYTE_LEN_TOTAL = 20;

    public static boolean isHealthCmd(byte[] bArr) {
        return getCmdId(bArr) == 8;
    }

    public static void copy(byte[] bArr, byte[] bArr2) {
        if (bArr != null && bArr2 != null) {
            int length = bArr.length;
            int length2 = bArr2.length;
            if (length >= length2) {
                length = length2;
            }
            for (int i = 0; i < length; i++) {
                bArr2[i] = bArr[i];
            }
        }
    }

    public static byte[] createCmd(byte b, byte b2, byte[] bArr) {
        byte[] createNullCmd = createNullCmd();
        int i = 0;
        createNullCmd[0] = b;
        createNullCmd[1] = b2;
        int length = bArr.length;
        int i2 = 2;
        while (i < length) {
            int i3 = i2 + 1;
            createNullCmd[i2] = bArr[i];
            if (i3 >= 20) {
                break;
            }
            i++;
            i2 = i3;
        }
        return createNullCmd;
    }

    public static byte[] createCmd(byte b, byte b2, byte b3) {
        byte[] createNullCmd = createNullCmd();
        createNullCmd[0] = b;
        createNullCmd[1] = b2;
        createNullCmd[2] = b3;
        return createNullCmd;
    }

    private static byte[] createNullCmd() {
        byte[] bArr = new byte[20];
        for (int i = 0; i < 20; i++) {
            bArr[i] = 0;
        }
        return bArr;
    }

    public static byte getCmdId(byte[] bArr) {
        return bArr[0];
    }

    public static byte getCmdKey(byte[] bArr) {
        if (bArr == null || bArr.length != 20) {
            return -1;
        }
        return bArr[1];
    }
}
