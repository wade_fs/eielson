package com.wade.fit.util.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.BleContant;
import com.wade.fit.app.Constants;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.util.AsyncTaskUtil;
import com.wade.fit.util.BaseCmdUtil;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.PhoneSystemUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ThreadUtil;
import com.wade.fit.util.ble.BleManager;
import com.wade.fit.util.ble.BleScanTool;
import com.wade.fit.util.log.LogUtil;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class BleManager implements Constants.BLE_RESULT_CODE, BleContant {
    private static final int MAX_CMD_QUEUE_SIZE = 10;
    private static final int MAX_DELAY = 60000;
    private static final int MAX_RECONNECT_COUNT = Integer.MAX_VALUE;
    public static final UUID RX_UPDATE_UUID = UUID.fromString("00001530-1212-efde-1523-785feabcd123");
    private static final String connecteTime = "connecteTime";
    private static BleManager mInstance = null;
    private static final Object mLock = new Object();
    /* access modifiers changed from: private */
    public final int MAX_CONN_TIME;
    MyBluetoothGattCallback bluetoothGattCallback = new MyBluetoothGattCallback();
    private Request cacheRequst;
    private LinkedList<Request> cacheTaskQueue = new LinkedList<>();
    /* access modifiers changed from: private */
    public BLEDevice currentDevice;
    BaseDataHandler dataHandler = BaseDataHandler.getInstance();
    private int discoverServicesCount;
    public IReconnectCallBack iReconnectCallBack;
    private boolean isDelaySend = false;
    public boolean isInitCon = false;
    private boolean isOrder;
    /* access modifiers changed from: private */
    public boolean isResolverState = true;
    /* access modifiers changed from: private */
    public boolean isScanDevice;
    private boolean isStrLog = false;
    private boolean isWaitComplete;
    private Request lastRequest;
    private BleScanTool mBleScanTool = BleScanTool.getInstance();
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    /* access modifiers changed from: private */
    public BluetoothGatt mBluetoothGatt;
    /* access modifiers changed from: private */
    public int mConnTime;
    private Timer mConnTimer;
    /* access modifiers changed from: private */
    public int mConnectFialCount = 0;
    private int mConnectionState = 0;
    private Context mContext;
    /* access modifiers changed from: private */
    public boolean mDiscoverServices;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public CopyOnWriteArrayList<AppBleListener> mListeners = new CopyOnWriteArrayList<>();
    private boolean mOpenNotificationSuccess;
    /* access modifiers changed from: private */
    public boolean mOperationInProgress;
    /* access modifiers changed from: private */
    public int mScanFaildCount = 0;
    /* access modifiers changed from: private */
    public final LinkedList<Request> mTaskQueue = new LinkedList<>();
    private PendingHandler mWriteHandler;
    private BluetoothGattCharacteristic mWriteNormalGattCharacteristic;
    private boolean notifyEcg;
    private boolean notifyNormal;
    private BleScanTool.ScanDeviceListener scanDevice;
    private SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss|SSS", Locale.CHINESE);
    private LinkedList<Request> sendQueue = new LinkedList<>();
    private long startTime;
    private boolean statEventIsConnectToDevice;
    private long statEventLastStartConnectTimeMs;
    private Runnable timeOut = new Runnable() {
        /* class com.wade.fit.util.ble.BleManager.C265310 */

        public void run() {
            BleClient.showMessage("发送超时了..");
            EventBusHelper.post(304);
            boolean unused = BleManager.this.mOperationInProgress = false;
            BleManager.this.handleTaskQueue();
            BleManager.this.nextRequest();
        }
    };
    public boolean unConnect = true;

    public interface IReconnectCallBack {
        void reconnectFaild();
    }

    static /* synthetic */ int access$1008(BleManager bleManager) {
        int i = bleManager.mConnTime;
        bleManager.mConnTime = i + 1;
        return i;
    }

    static /* synthetic */ int access$708(BleManager bleManager) {
        int i = bleManager.mScanFaildCount;
        bleManager.mScanFaildCount = i + 1;
        return i;
    }

    public void setResolverState(boolean z) {
        this.isResolverState = z;
    }

    private BleManager() {
        TimeOutTask.getInstance();
        this.mHandler = new Handler() {
            /* class com.wade.fit.util.ble.BleManager.C26521 */

            public void handleMessage(Message message) {
                if (BleManager.this.mOperationInProgress) {
                    BleClient.showMessage("命令发送超时了 , 当前队列长度 = " + BleManager.this.mTaskQueue.size());
                    if (BleManager.this.mTaskQueue.size() > 10) {
                        BleClient.showMessage("当前队列长度超过10个 , 清空队列");
                        BleManager.this.clearTaskQueue();
                        BleManager.this.disconnectBluethoothConnection();
                    }
                    boolean unused = BleManager.this.mOperationInProgress = false;
                    BleManager.this.nextRequest();
                }
                super.handleMessage(message);
            }
        };
        this.scanDevice = new BleScanTool.ScanDeviceListener() {
            /* class com.wade.fit.util.ble.BleManager.C26552 */

            public void onFind(BLEDevice bLEDevice) {
                if (BleManager.this.isBind() && BleManager.this.isResolverState && !BleManager.this.isScanDevice && bLEDevice != null && !TextUtils.isEmpty(bLEDevice.mDeviceAddress) && bLEDevice.mDeviceAddress.equals(BleManager.this.getMac())) {
                    boolean unused = BleManager.this.isScanDevice = true;
                    int unused2 = BleManager.this.mScanFaildCount = 0;
                    BleClient.showMessage("找到设备, 停止扫描, 去连接设备......");
                    BleClient.scanDevices(false);
                    BleManager.this.connect(bLEDevice);
                }
            }

            public void onFinish() {
                if (BleManager.this.isBind()) {
                    BleClient.showMessage("扫描结束");
                    LogUtil.d("isScanDevice:" + BleManager.this.isScanDevice);
                    if (BleManager.this.isResolverState && !BleManager.this.isScanDevice) {
                        BleManager.access$708(BleManager.this);
                        int access$700 = BleManager.this.mScanFaildCount * 1000;
                        if (access$700 > 60000) {
                            access$700 = 60000;
                        }
                        BleClient.showMessage("未扫描到设备，过[" + access$700 + "]秒后再去扫描");
                        BleManager.this.mHandler.postDelayed(new Runnable() {
                            /* class com.wade.fit.util.ble.$$Lambda$BleManager$2$D1IxFcpaZu8ruXPhKgZCqDM06i0 */

                            public final void run() {
                                BleManager.C26552.this.lambda$onFinish$0$BleManager$2();
                            }
                        }, (long) access$700);
                    }
                }
            }

            public /* synthetic */ void lambda$onFinish$0$BleManager$2() {
                BleManager.this.reConnect();
            }
        };
        this.mConnTime = 0;
        this.MAX_CONN_TIME = 12;
        this.mConnTimer = null;
        this.statEventIsConnectToDevice = false;
        this.statEventLastStartConnectTimeMs = 0;
        this.discoverServicesCount = 0;
    }

    public static synchronized BleManager getInstance() {
        BleManager bleManager;
        synchronized (BleManager.class) {
            if (mInstance == null) {
                mInstance = new BleManager();
            }
            bleManager = mInstance;
        }
        return bleManager;
    }

    public void init(Context context) {
        this.mContext = context;
        this.mBluetoothAdapter = this.mBleScanTool.getBluetoothAdapter();
        this.mWriteHandler = new PendingHandler(Looper.myLooper());
        BleClient.setScanDeviceListener(this.scanDevice);
    }

    private boolean getAndroidSystemVersionLessThanFive() {
        return Build.VERSION.SDK_INT <= 20;
    }

    public boolean connect(BLEDevice bLEDevice) {
        this.currentDevice = bLEDevice;
        this.isInitCon = false;
        LogUtil.m5264d(this.currentDevice);
        if (!this.mBleScanTool.isBluetoothOpen()) {
            BleClient.showMessage("蓝牙未打开,不连接......");
            return false;
        } else if (TextUtils.isEmpty(bLEDevice.mDeviceAddress)) {
            BleClient.showMessage("蓝牙地址为空,不连接......");
            return false;
        } else if (deviceConnecting()) {
            BleClient.showMessage("设备正在连接中,不连接......");
            return false;
        } else if (isConnBluetoothSuccess()) {
            BleClient.showMessage("蓝牙连接成功,不连接......");
            return false;
        } else {
            BluetoothDevice remoteDevice = this.mBluetoothAdapter.getRemoteDevice(bLEDevice.mDeviceAddress);
            if (remoteDevice == null) {
                BleClient.showMessage("获取的远程设备不存在,不连接......");
                return false;
            }
            TextUtils.isEmpty(remoteDevice.getName());
            synchronized (mLock) {
                if (this.mBluetoothGatt != null) {
                    this.mBluetoothGatt.close();
                    this.mBluetoothGatt = null;
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (BleScanTool.getInstance().isScanning()) {
                this.isScanDevice = true;
                BleClient.scanDevices(false);
            }
            callOnBleConnecting(bLEDevice.mDeviceAddress);
            if (getAndroidSystemVersionLessThanFive()) {
                this.mHandler.post(new Runnable(remoteDevice) {
                    /* class com.wade.fit.util.ble.$$Lambda$BleManager$HFtGt_BBXWKLSEnRKz4lmGlgWxs */
                    private final /* synthetic */ BluetoothDevice f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void run() {
                        BleManager.this.lambda$connect$0$BleManager(this.f$1);
                    }
                });
            } else {
                LogUtil.d("connectGatt......");
                this.mBluetoothGatt = remoteDevice.connectGatt(this.mContext, false, this.bluetoothGattCallback);
            }
            return true;
        }
    }

    public /* synthetic */ void lambda$connect$0$BleManager(BluetoothDevice bluetoothDevice) {
        LogUtil.d("connectGatt......");
        this.mBluetoothGatt = bluetoothDevice.connectGatt(this.mContext, false, this.bluetoothGattCallback);
    }

    public boolean reConnect() {
        if (!BleSdkWrapper.isBind()) {
            BleClient.showMessage("未绑定设备,不重连....");
            return false;
        } else if (!this.mBleScanTool.isBluetoothOpen()) {
            BleClient.showMessage("蓝牙未开启,不重连....");
            return false;
        } else if (deviceConnecting()) {
            BleClient.showMessage("正在连接中,不重连......");
            return false;
        } else if (isConnBluetoothSuccess()) {
            BleClient.showMessage("蓝牙连接成功,不重连......");
            return false;
        } else {
            BleClient.showMessage("设备未被连接,正在尝试重连......");
            BLEDevice bindBLEDevice = SPHelper.getBindBLEDevice();
            if (this.mConnectFialCount < 2 && bindBLEDevice != null) {
                LogUtil.d("connect.......");
                connect(bindBLEDevice);
            } else if (bindBLEDevice != null) {
                LogUtil.d("scanDevices.......");
                BleClient.scanDevices();
            } else {
                LogUtil.d("设备为null");
            }
            if (this.statEventLastStartConnectTimeMs != 0) {
                return true;
            }
            this.statEventLastStartConnectTimeMs = System.currentTimeMillis();
            return true;
        }
    }

    private void startConnTimer() {
        if (this.mConnTimer == null) {
            this.mConnTimer = new Timer();
        }
        this.mConnTimer.schedule(new TimerTask() {
            /* class com.wade.fit.util.ble.BleManager.C26563 */

            public void run() {
                BleManager.access$1008(BleManager.this);
                BleClient.showMessage(" 连接中 ====> , 连接时间 = " + BleManager.this.mConnTime);
                if (BleManager.this.mConnTime >= BleManager.this.MAX_CONN_TIME) {
                    BleClient.showMessage("连接中 ====>  连接超时");
                    BleManager.this.cancelConnTimer();
                    if (BleManager.this.isBind()) {
                        BleManager.this.disconnectBluethoothConnection();
                    } else {
                        BleManager.this.disconnectBluethoothConnection();
                        BleManager.this.callOnBleConnectOutTime();
                    }
                    BleManager.this.callOnBLEDisConnected();
                    BleManager.this.close();
                    if (BleManager.this.isBind() && BleManager.this.unConnect) {
                        BleManager.this.reConnect();
                    }
                }
            }
        }, 0, 1000);
    }

    public void cancelConnTimer() {
        this.mConnTime = 0;
        Timer timer = this.mConnTimer;
        if (timer != null) {
            timer.cancel();
            this.mConnTimer = null;
        }
    }

    /* access modifiers changed from: private */
    public void handleTaskQueue() {
        if (this.mTaskQueue.size() > 10) {
            BleClient.showMessage("发送队列数量超限，强制移除旧命令，保留最后2个命令");
            for (int i = 0; i < 8; i++) {
                this.mTaskQueue.pollFirst();
            }
        }
    }

    private void statConnectFailOrBreakEvent(int i, int i2) {
        if (isBind()) {
            this.statEventIsConnectToDevice = false;
        }
    }

    private void disconnectedSuccess() {
        callOnBLEDisConnected();
        this.isInitCon = false;
        if (isBind()) {
            long longValue = ((Long) SharePreferenceUtils.get(AppApplication.getInstance(), connecteTime, 0L)).longValue();
            int intValue = ((Integer) SharePreferenceUtils.get(AppApplication.getInstance(), "unLine", 0)).intValue() + 1;
            SharePreferenceUtils.put(AppApplication.getContext(), "unLine", Integer.valueOf(intValue));
            BleClient.showMessage("蓝牙连接持续时长[" + (((System.currentTimeMillis() - longValue) / 1000) / 60) + "]分钟,断线次数:" + intValue);
            if (!this.mBleScanTool.isBluetoothOpen()) {
                BleClient.showMessage("蓝牙未开启");
            } else if (TextUtils.isEmpty(getMac())) {
                BleClient.showMessage("蓝牙地址为空");
            } else if (isConnBluetoothSuccess()) {
                BleClient.showMessage("蓝牙连接成功");
            } else if (!this.unConnect) {
                BleClient.showMessage("断线不再重连");
            } else {
                this.mConnectFialCount++;
                BleClient.showMessage("蓝牙连接失败,重连次数:" + this.mConnectFialCount);
                if (this.mConnectFialCount < 20) {
                    new AsyncTaskUtil().setIAsyncTaskCallBack(new AsyncTaskUtil.IAsyncTaskCallBack() {
                        /* class com.wade.fit.util.ble.BleManager.C26574 */

                        public void onPostExecute(Object obj) {
                            BleClient.showMessage("");
                            BleManager.this.reConnect();
                        }

                        public Object doInBackground(String... strArr) {
                            try {
                                Thread.sleep(BleManager.this.mConnectFialCount % 5 == 0 ? 60000 : 10000);
                                return null;
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    }).execute("");
                    return;
                }
                this.mConnectFialCount = 0;
                callOnBleConnectOutTime();
                IReconnectCallBack iReconnectCallBack2 = this.iReconnectCallBack;
                if (iReconnectCallBack2 != null) {
                    iReconnectCallBack2.reconnectFaild();
                }
            }
        }
    }

    private void connectedSuccess() {
        this.statEventIsConnectToDevice = true;
        BleClient.showMessage("蓝牙连接成功处理 ====> 从连接开始到连上蓝牙总用时 = " + ((System.currentTimeMillis() - this.startTime) / 1000));
        SharePreferenceUtils.put(AppApplication.getContext(), connecteTime, Long.valueOf(System.currentTimeMillis()));
        this.mHandler.removeCallbacksAndMessages(null);
        this.mConnectFialCount = 0;
        this.discoverServicesCount = 0;
        this.mConnectionState = 2;
        if (this.mBluetoothGatt == null) {
            return;
        }
        if (getAndroidSystemVersionLessThanFive()) {
            this.mHandler.post(new Runnable() {
                /* class com.wade.fit.util.ble.$$Lambda$BleManager$Ei067OtA889izBL7bAMffGUIio */

                public final void run() {
                    BleManager.this.lambda$connectedSuccess$1$BleManager();
                }
            });
            return;
        }
        this.mDiscoverServices = this.mBluetoothGatt.discoverServices();
        if (!this.mDiscoverServices) {
            BleClient.showMessage("开启通知服务失败，则再去尝试1次");
            this.mHandler.postDelayed(new Runnable() {
                /* class com.wade.fit.util.ble.$$Lambda$BleManager$udN4Z5bFuvX_x8yU1ZHdxt2yOMg */

                public final void run() {
                    BleManager.this.lambda$connectedSuccess$2$BleManager();
                }
            }, 200);
        }
    }

    public /* synthetic */ void lambda$connectedSuccess$1$BleManager() {
        this.mDiscoverServices = this.mBluetoothGatt.discoverServices();
        if (!this.mDiscoverServices) {
            BleClient.showMessage("开启通知服务失败，则再去尝试1次");
            this.mHandler.postDelayed(new Runnable() {
                /* class com.wade.fit.util.ble.BleManager.C26585 */

                public void run() {
                    BleManager bleManager = BleManager.this;
                    boolean unused = bleManager.mDiscoverServices = bleManager.mBluetoothGatt.discoverServices();
                    if (!BleManager.this.mDiscoverServices) {
                        BleManager.this.serviceDiscoverFail();
                    }
                }
            }, 200);
        }
    }

    public /* synthetic */ void lambda$connectedSuccess$2$BleManager() {
        this.mDiscoverServices = this.mBluetoothGatt.discoverServices();
        if (!this.mDiscoverServices) {
            serviceDiscoverFail();
        }
    }

    /* access modifiers changed from: private */
    public void serviceDiscoverSuccess(BluetoothGatt bluetoothGatt, int i) {
        BleClient.showMessage("开启服务成功");
        if (BleHelper.isInDfuMode(bluetoothGatt)) {
            BleClient.showMessage("device in dfu mode");
            handleDfuMode();
            return;
        }
        boolean z = false;
        this.mOperationInProgress = false;
        nextRequest();
        notifyServiceDiscover(bluetoothGatt, i);
        this.discoverServicesCount = 0;
        this.notifyNormal = BleGattAttributes.enablePeerDeviceNotifyNormal(bluetoothGatt, true);
        StringBuilder sb = new StringBuilder();
        sb.append("开启第1个通知");
        String str = "成功";
        sb.append(this.notifyNormal ? str : "失败");
        BleClient.showMessage(sb.toString());
        if (!this.notifyNormal) {
            this.mHandler.postDelayed(new Runnable(bluetoothGatt) {
                /* class com.wade.fit.util.ble.$$Lambda$BleManager$ueSg53WTjtAYQj6Jo9jWQOVTzl4 */
                private final /* synthetic */ BluetoothGatt f$1;

                {
                    this.f$1 = r2;
                }

                public final void run() {
                    BleManager.this.lambda$serviceDiscoverSuccess$3$BleManager(this.f$1);
                }
            }, 200);
            return;
        }
        Iterator<BluetoothGattService> it = bluetoothGatt.getServices().iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next().getUuid().equals(BleGattAttributes.SERVICE_UUID_ECG)) {
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        AppApplication.isHaveEcg = z;
        if (z) {
            this.notifyEcg = BleGattAttributes.enablePeerDeviceNotifyEcg(bluetoothGatt, true);
            StringBuilder sb2 = new StringBuilder();
            sb2.append("开启第2个通知");
            if (!this.notifyEcg) {
                str = "失败";
            }
            sb2.append(str);
            BleClient.showMessage(sb2.toString());
            if (!this.notifyEcg) {
                this.mHandler.postDelayed(new Runnable(bluetoothGatt) {
                    /* class com.wade.fit.util.ble.$$Lambda$BleManager$HlzuDPyVVYxECnfLTIY9LmJ76Q8 */
                    private final /* synthetic */ BluetoothGatt f$1;

                    {
                        this.f$1 = r2;
                    }

                    public final void run() {
                        BleManager.this.lambda$serviceDiscoverSuccess$4$BleManager(this.f$1);
                    }
                }, 200);
            } else {
                callOnBLEConnected(bluetoothGatt);
            }
        } else {
            callOnBLEConnected(bluetoothGatt);
        }
    }

    public /* synthetic */ void lambda$serviceDiscoverSuccess$3$BleManager(BluetoothGatt bluetoothGatt) {
        BleClient.showMessage("开启第1个通知失败，则再去尝试1次");
        this.notifyNormal = BleGattAttributes.enablePeerDeviceNotifyNormal(bluetoothGatt, true);
        StringBuilder sb = new StringBuilder();
        sb.append("开启第1个通知");
        sb.append(this.notifyNormal ? "成功" : "失败");
        BleClient.showMessage(sb.toString());
        if (!this.notifyNormal) {
            openNormalNotifyFail();
        }
    }

    public /* synthetic */ void lambda$serviceDiscoverSuccess$4$BleManager(BluetoothGatt bluetoothGatt) {
        BleClient.showMessage("开启第2个通知失败，则再去尝试1次");
        this.notifyEcg = BleGattAttributes.enablePeerDeviceNotifyEcg(bluetoothGatt, true);
        StringBuilder sb = new StringBuilder();
        sb.append("开启第2个通知");
        sb.append(this.notifyEcg ? "成功" : "失败");
        BleClient.showMessage(sb.toString());
        if (!this.notifyEcg) {
            openNormalNotifyFail();
        } else {
            callOnBLEConnected(bluetoothGatt);
        }
    }

    private void handleDfuMode() {
        this.mHandler.post(new Runnable() {
            /* class com.wade.fit.util.ble.BleManager.C26596 */

            public void run() {
                BleManager.this.disconnectBluethoothConnection();
                BleManager.getInstance().unConnect = false;
                Iterator it = BleManager.this.mListeners.iterator();
                while (it.hasNext()) {
                    ((AppBleListener) it.next()).onInDfuMode(BleManager.this.currentDevice);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void serviceDiscoverFail() {
        this.discoverServicesCount++;
        BleClient.showMessage("开启服务失败，次数 = " + this.discoverServicesCount);
        disconnectBluethoothConnection();
    }

    private void openNormalNotifyFail() {
        this.discoverServicesCount++;
        BleClient.showMessage("开启服务失败，次数 = " + this.discoverServicesCount);
        disconnectBluethoothConnection();
    }

    private void notifyServiceDiscover(BluetoothGatt bluetoothGatt, int i) {
        this.mHandler.post(new Runnable(bluetoothGatt, i) {
            /* class com.wade.fit.util.ble.$$Lambda$BleManager$07q6iSTMf1uR1LSItcBJz3y3Xo8 */
            private final /* synthetic */ BluetoothGatt f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void run() {
                BleManager.this.lambda$notifyServiceDiscover$5$BleManager(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$notifyServiceDiscover$5$BleManager(BluetoothGatt bluetoothGatt, int i) {
        Iterator<AppBleListener> it = this.mListeners.iterator();
        while (it.hasNext()) {
            it.next().onServiceDiscover(bluetoothGatt, i);
        }
    }

    public void clearTaskQueue() {
        this.mTaskQueue.clear();
        this.sendQueue.clear();
        this.cacheTaskQueue.clear();
        this.mOperationInProgress = false;
    }

    public synchronized void enqueue(byte[] bArr, BleCallback bleCallback) {
        enqueue(0, bArr, bleCallback);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void enqueue(int r7, byte[] r8, com.wade.fit.util.ble.BleCallback r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            boolean r0 = r6.isDeviceConnected()     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0034
            android.bluetooth.BluetoothGatt r0 = r6.mBluetoothGatt     // Catch:{ all -> 0x003b }
            if (r0 != 0) goto L_0x0011
            java.lang.String r7 = "mBluetoothGatt is null!"
            com.wade.fit.util.log.LogUtil.m5268e(r7)     // Catch:{ all -> 0x003b }
            goto L_0x0039
        L_0x0011:
            android.bluetooth.BluetoothGattCharacteristic r0 = r6.mWriteNormalGattCharacteristic     // Catch:{ all -> 0x003b }
            if (r0 != 0) goto L_0x0028
            android.bluetooth.BluetoothGatt r0 = r6.mBluetoothGatt     // Catch:{ all -> 0x003b }
            android.bluetooth.BluetoothGattCharacteristic r0 = com.wade.fit.util.ble.BleGattAttributes.getNormalWriteCharacteristic(r0)     // Catch:{ all -> 0x003b }
            r6.mWriteNormalGattCharacteristic = r0     // Catch:{ all -> 0x003b }
            android.bluetooth.BluetoothGattCharacteristic r0 = r6.mWriteNormalGattCharacteristic     // Catch:{ all -> 0x003b }
            if (r0 != 0) goto L_0x0028
            java.lang.String r7 = "mWriteNormalGattCharacteristic is nul"
            com.wade.fit.util.log.LogUtil.m5268e(r7)     // Catch:{ all -> 0x003b }
            monitor-exit(r6)
            return
        L_0x0028:
            android.bluetooth.BluetoothGatt r2 = r6.mBluetoothGatt     // Catch:{ all -> 0x003b }
            android.bluetooth.BluetoothGattCharacteristic r3 = r6.mWriteNormalGattCharacteristic     // Catch:{ all -> 0x003b }
            r0 = r6
            r1 = r7
            r4 = r8
            r5 = r9
            r0.enqueue(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x003b }
            goto L_0x0039
        L_0x0034:
            java.lang.String r7 = "设备未连接...."
            com.wade.fit.util.log.LogUtil.m5268e(r7)     // Catch:{ all -> 0x003b }
        L_0x0039:
            monitor-exit(r6)
            return
        L_0x003b:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.ble.BleManager.enqueue(int, byte[], com.wade.fit.util.ble.BleCallback):void");
    }

    public synchronized boolean enqueue(int i, Request request) {
        if (request == null) {
            return false;
        }
        BleClient.showMessage("enqueue，flag:" + i + ",request.flag:" + request.flag + ",isWaitComplete:" + this.isWaitComplete);
        int i2 = request.flag;
        this.isDelaySend = false;
        if (i2 == 0) {
            BleClient.showMessage("正常命令，isOrder:" + this.isOrder);
            if (this.isOrder) {
                BleClient.showMessage("连续命令还未发送完成，放入缓存 size:" + this.cacheTaskQueue.size());
                this.cacheTaskQueue.add(request);
                if (this.cacheTaskQueue.size() >= 5) {
                    this.isOrder = false;
                    this.isDelaySend = false;
                }
            } else {
                this.mTaskQueue.add(request);
                BleClient.showMessage("添加了1条消息,消息队列里面有<" + this.mTaskQueue.size() + ">条消息等待发送 mOperationInProgress:" + this.mOperationInProgress);
            }
        } else if (i2 == 1) {
            this.isDelaySend = true;
            BleClient.showMessage("发现连续命令，开始");
            this.isOrder = true;
            this.mTaskQueue.add(request);
        } else if (i2 == 2) {
            BleClient.showMessage("发现连续命令，继续");
            this.mTaskQueue.add(request);
            this.isDelaySend = true;
        } else if (i2 == 3) {
            BleClient.showMessage("发现连续命令，结束");
            this.isOrder = false;
            this.isDelaySend = false;
            this.mTaskQueue.add(request);
            cacheToActiveTastQueue();
        } else if (i2 == 4) {
            this.isWaitComplete = true;
            this.mTaskQueue.add(request);
        } else if (i2 == 200) {
            this.mTaskQueue.add(request);
        }
        nextRequest();
        return true;
    }

    private void cacheToActiveTastQueue() {
        if (!this.cacheTaskQueue.isEmpty()) {
            while (true) {
                Request poll = this.cacheTaskQueue.poll();
                if (poll != null) {
                    this.mTaskQueue.add(poll);
                } else {
                    return;
                }
            }
        }
    }

    private void addCacheTaskQueue(Request request) {
        BleClient.showMessage("放入缓存列队 size:" + this.cacheTaskQueue.size());
        this.cacheTaskQueue.add(request);
    }

    /* access modifiers changed from: private */
    public void nextRequest() {
        Request request;
        BleClient.showMessage(toString() + "...mOperationInProgress:" + this.mOperationInProgress + ",发送队列size:" + this.mTaskQueue.size());
        if (!this.mOperationInProgress && this.mTaskQueue.size() != 0) {
            try {
                request = this.mTaskQueue.poll();
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                request = null;
            }
            if (request != null) {
                BluetoothGattCharacteristic bluetoothGattCharacteristic = request.characteristic;
                bluetoothGattCharacteristic.setValue(request.value);
                this.mOperationInProgress = true;
                if (request.flag == 0 || request.flag == 3 || request.flag == 200) {
                    this.sendQueue.add(request);
                }
                this.lastRequest = request;
                BleClient.showMessage("发送数据:" + ByteDataConvertUtil.bytesToHexString(request.value));
                boolean internalWriteCharacteristic = internalWriteCharacteristic(bluetoothGattCharacteristic);
                BleClient.showMessage("result:" + internalWriteCharacteristic + ",sendQueue.size:" + this.sendQueue.size());
                if (!internalWriteCharacteristic) {
                    this.mOperationInProgress = false;
                    if (request.callback != null) {
                        request.callback.complete(0, null);
                    }
                    nextRequest();
                    return;
                }
                TimeOutTask.getInstance().postTimeOut(this.timeOut, 2000);
            }
        }
    }

    private synchronized void enqueue(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr) {
        enqueue(bluetoothGatt, bluetoothGattCharacteristic, bArr, null);
    }

    private synchronized void enqueue(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback) {
        enqueue(0, bluetoothGatt, bluetoothGattCharacteristic, bArr, bleCallback);
    }

    private synchronized void enqueue(int i, BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, BleCallback bleCallback) {
        if (bluetoothGattCharacteristic != null) {
            try {
                if ((bluetoothGattCharacteristic.getProperties() | 8) > 0) {
                    enqueue(i, Request.newWriteRequest(bluetoothGattCharacteristic, bArr, bleCallback, i));
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        if (bluetoothGattCharacteristic != null) {
            BleClient.showMessage("发送数据失败 , c.getProperties():" + bluetoothGattCharacteristic.getProperties());
        } else {
            BleClient.showMessage("发送数据失败 , c is null");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0020, code lost:
        if (r1 == false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean internalWriteCharacteristic(android.bluetooth.BluetoothGattCharacteristic r5) {
        /*
            r4 = this;
            java.lang.String r0 = "writeCharacteristic 发送异常....断线重连"
            r1 = 0
            if (r5 != 0) goto L_0x000b
            java.lang.String r5 = "internalWriteCharacteristic....characteristic is null"
            com.wade.fit.util.log.LogUtil.d(r5)
            return r1
        L_0x000b:
            android.bluetooth.BluetoothGatt r2 = r4.mBluetoothGatt
            if (r2 != 0) goto L_0x0015
            java.lang.String r5 = "internalWriteCharacteristic....mBluetoothGatt is null"
            com.wade.fit.util.log.LogUtil.d(r5)
            return r1
        L_0x0015:
            int r3 = r5.getWriteType()
            r5.setWriteType(r3)
            boolean r1 = r2.writeCharacteristic(r5)     // Catch:{ Exception -> 0x002e }
            if (r1 != 0) goto L_0x0038
        L_0x0022:
            com.wade.fit.util.ble.BleClient.showMessage(r0)
            r4.disconnectBluethoothConnection()
            r4.reConnect()
            goto L_0x0038
        L_0x002c:
            r5 = move-exception
            goto L_0x0039
        L_0x002e:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x002c }
            com.wade.fit.util.ble.BleClient.showMessage(r0)     // Catch:{ all -> 0x002c }
            r4.mConnectionState = r1     // Catch:{ all -> 0x002c }
            goto L_0x0022
        L_0x0038:
            return r1
        L_0x0039:
            com.wade.fit.util.ble.BleClient.showMessage(r0)
            r4.disconnectBluethoothConnection()
            r4.reConnect()
            goto L_0x0044
        L_0x0043:
            throw r5
        L_0x0044:
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.ble.BleManager.internalWriteCharacteristic(android.bluetooth.BluetoothGattCharacteristic):boolean");
    }

    public void callOnBleConnectOutTime() {
        BleClient.showMessage("连接超时处理");
        this.mHandler.post(new Runnable() {
            /* class com.wade.fit.util.ble.BleManager.C26607 */

            public void run() {
                Iterator it = BleManager.this.mListeners.iterator();
                while (it.hasNext()) {
                    ((AppBleListener) it.next()).onBLEConnectTimeOut();
                }
            }
        });
    }

    private void callOnBleConnecting(String str) {
        BleClient.showMessage("设备未被连接,正在连接中...");
        this.mBluetoothDeviceAddress = str;
        this.startTime = System.currentTimeMillis();
        this.isScanDevice = false;
        this.mOpenNotificationSuccess = false;
        this.mConnectionState = 1;
        this.mHandler.post(new Runnable() {
            /* class com.wade.fit.util.ble.BleManager.C26618 */

            public void run() {
                Iterator it = BleManager.this.mListeners.iterator();
                while (it.hasNext()) {
                    ((AppBleListener) it.next()).onBLEConnecting(BleManager.this.getMac());
                }
            }
        });
        if (this.mConnTimer == null) {
            startConnTimer();
        }
        if (this.statEventLastStartConnectTimeMs == 0) {
            this.statEventLastStartConnectTimeMs = System.currentTimeMillis();
        }
    }

    private void callOnBLEConnected(BluetoothGatt bluetoothGatt) {
        this.statEventLastStartConnectTimeMs = 0;
        BleClient.showMessage("蓝牙连接成功，总用时 = " + ((System.currentTimeMillis() - this.startTime) / 1000));
        cancelConnTimer();
        this.mOpenNotificationSuccess = true;
        this.mHandler.post(new Runnable(bluetoothGatt) {
            /* class com.wade.fit.util.ble.$$Lambda$BleManager$yTiKkBRs3xOdQtiHVjERaov5x4 */
            private final /* synthetic */ BluetoothGatt f$1;

            {
                this.f$1 = r2;
            }

            public final void run() {
                BleManager.this.lambda$callOnBLEConnected$6$BleManager(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$callOnBLEConnected$6$BleManager(BluetoothGatt bluetoothGatt) {
        Iterator<AppBleListener> it = this.mListeners.iterator();
        while (it.hasNext()) {
            it.next().onBLEConnected(bluetoothGatt);
        }
    }

    public void callOnBLEDisConnected() {
        BleClient.showMessage("蓝牙断开连接......");
        this.mHandler.removeCallbacksAndMessages(null);
        this.mConnectionState = 0;
        EventBusHelper.post(302);
        this.mHandler.post(new Runnable() {
            /* class com.wade.fit.util.ble.$$Lambda$BleManager$FEVITUB19voAW5pEfS7tZLHH5ZI */

            public final void run() {
                BleManager.this.lambda$callOnBLEDisConnected$7$BleManager();
            }
        });
        clearTaskQueue();
        this.mWriteNormalGattCharacteristic = null;
        this.isScanDevice = false;
        this.mOpenNotificationSuccess = false;
    }

    public /* synthetic */ void lambda$callOnBLEDisConnected$7$BleManager() {
        Iterator<AppBleListener> it = this.mListeners.iterator();
        while (it.hasNext()) {
            it.next().onBLEDisConnected(getMac());
        }
    }

    public void disconnectBluethoothConnection() {
        clearTaskQueue();
        BluetoothGatt bluetoothGatt = this.mBluetoothGatt;
        if (bluetoothGatt != null) {
            BleHelper.refreshDeviceCache(bluetoothGatt);
            if (getAndroidSystemVersionLessThanFive()) {
                this.mHandler.post(new Runnable() {
                    /* class com.wade.fit.util.ble.BleManager.C26629 */

                    public void run() {
                        if (BleManager.this.mBluetoothGatt != null) {
                            BleManager.this.mBluetoothGatt.disconnect();
                        }
                    }
                });
                return;
            }
            BluetoothGatt bluetoothGatt2 = this.mBluetoothGatt;
            if (bluetoothGatt2 != null) {
                bluetoothGatt2.disconnect();
            }
            if (PhoneSystemUtil.getPhoneDeviceBrand().equalsIgnoreCase("Honor") && PhoneSystemUtil.getPhoneSystemModel().equalsIgnoreCase("CHM-TL00")) {
                close();
                this.mHandler.post(new Runnable() {
                    /* class com.wade.fit.util.ble.$$Lambda$BleManager$kgnLBw1EDNH4yt9IqiJ_2uSjRk8 */

                    public final void run() {
                        BleManager.this.lambda$disconnectBluethoothConnection$8$BleManager();
                    }
                });
            }
        }
    }

    public /* synthetic */ void lambda$disconnectBluethoothConnection$8$BleManager() {
        Iterator<AppBleListener> it = this.mListeners.iterator();
        while (it.hasNext()) {
            it.next().onBLEDisConnected(getMac());
        }
    }

    public void cleanConnectionState() {
        this.mConnectionState = 0;
    }

    public void close() {
        this.statEventIsConnectToDevice = false;
        BleClient.showMessage("关闭蓝牙资源");
        this.mConnectionState = 0;
        clearTaskQueue();
        this.mWriteNormalGattCharacteristic = null;
        this.isScanDevice = false;
        this.discoverServicesCount = 0;
        this.mOperationInProgress = false;
        this.mOpenNotificationSuccess = false;
        synchronized (mLock) {
            if (this.mBluetoothGatt != null) {
                try {
                    this.mBluetoothGatt.close();
                } catch (Exception e) {
                    BleClient.showMessage(e.getMessage());
                }
                BleHelper.refreshDeviceCache(this.mBluetoothGatt);
                this.mBluetoothGatt = null;
            }
        }
    }

    public boolean isDeviceConnected() {
        return this.mOpenNotificationSuccess;
    }

    public boolean isConnBluetoothSuccess() {
        return this.mConnectionState == 2;
    }

    private boolean deviceConnecting() {
        return this.mConnectionState == 1;
    }

    /* access modifiers changed from: private */
    public String getMac() {
        return TextUtils.isEmpty(SPHelper.getBindMac()) ? this.mBluetoothDeviceAddress : SPHelper.getBindMac();
    }

    /* access modifiers changed from: private */
    public boolean isBind() {
        return BleSdkWrapper.isBind();
    }

    public void setBleListener(AppBleListener appBleListener) {
        if (appBleListener != null && !this.mListeners.contains(appBleListener)) {
            this.mListeners.add(appBleListener);
        }
    }

    public void removeListener(AppBleListener appBleListener) {
        if (appBleListener != null && !this.mListeners.isEmpty()) {
            this.mListeners.remove(appBleListener);
        }
    }

    /* access modifiers changed from: private */
    public void handlerConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        if (this.isResolverState) {
            cancelConnTimer();
            if (i != 0) {
                statConnectFailOrBreakEvent(i, i2);
                disconnectedSuccess();
            } else if (i2 == 0) {
                bluetoothGatt.close();
                disconnectedSuccess();
            } else if (i2 == 2) {
                connectedSuccess();
            }
        }
    }

    /* access modifiers changed from: private */
    public void reciverBleData(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        HandlerBleDataResult handlerBleDataResult;
        byte[] bArr = new byte[20];
        BaseCmdUtil.copy(bluetoothGattCharacteristic.getValue(), bArr);
        BleClient.showMessage("接收到手环数据===" + ByteDataConvertUtil.bytesToHexString(bArr));
        Object handler = this.dataHandler.handler(bArr);
        Request poll = this.sendQueue.poll();
        LogUtil.m5264d(handler == null ? "result is null" : handler);
        LogUtil.m5264d(poll == null ? "request is null" : poll);
        TimeOutTask.getInstance().remove(this.timeOut);
        if (poll != null) {
            LogUtil.d("flag:" + poll.flag);
            if (poll.flag == 200) {
                this.cacheRequst = poll;
            } else if (poll.flag == 3) {
                if (handler == null) {
                    handlerBleDataResult = new HandlerBleDataResult();
                } else {
                    handlerBleDataResult = handler instanceof HandlerBleDataResult ? (HandlerBleDataResult) handler : null;
                }
                if (handlerBleDataResult != null) {
                    handlerBleDataResult.isComplete = true;
                    callback(poll, handlerBleDataResult);
                    return;
                }
            }
        }
        if (!(handler instanceof HandlerBleDataResult)) {
            callback(poll, handler);
        } else if (((HandlerBleDataResult) handler).isComplete) {
            callback(this.cacheRequst, handler);
            this.cacheRequst = null;
        } else {
            callback(this.cacheRequst, handler);
        }
    }

    /* access modifiers changed from: private */
    public void sendBleSuccess() {
        BleClient.showMessage("发送消息成功 ,isDelaySend:" + this.isDelaySend + ", 还有[" + this.mTaskQueue.size() + "]条消息未发送");
        handleTaskQueue();
        this.mOperationInProgress = false;
        ThreadUtil.delayTask(new Runnable() {
            /* class com.wade.fit.util.ble.BleManager.C265411 */

            public void run() {
                BleManager.this.nextRequest();
            }
        }, 500);
    }

    private void callback(Request request, Object obj) {
        if (request != null && request.callback != null) {
            ThreadUtil.runOnMainThread(new Runnable(obj) {
                /* class com.wade.fit.util.ble.$$Lambda$BleManager$q3SISABIBNP2kgztyjZLRMYkQqQ */
                private final /* synthetic */ Object f$1;

                {
                    this.f$1 = r2;
                }

                public final void run() {
                    Request.this.callback.complete(1, this.f$1);
                }
            });
        }
    }

    private class MyBluetoothGattCallback extends BluetoothGattCallback {
        private MyBluetoothGattCallback() {
        }

        public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
            BleClient.showMessage("onConnectionStateChange , [status = " + i + ", newState = " + i2 + ",isResolverState = " + BleManager.this.isResolverState + "]" + toString());
            BleManager.this.handlerConnectionStateChange(bluetoothGatt, i, i2);
        }

        public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
            BleClient.showMessage("onServicesDiscovered , [status = " + i + ",isResolverState = " + BleManager.this.isResolverState + "]");
            if (!BleManager.this.isResolverState) {
                return;
            }
            if (i == 0) {
                BleManager.this.serviceDiscoverSuccess(bluetoothGatt, i);
            } else {
                BleManager.this.serviceDiscoverFail();
            }
        }

        public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
            BleClient.showMessage("onDescriptorWrite ,status: " + i + " , isResolverState = " + BleManager.this.isResolverState);
            BleSdkWrapper.setTime(new BleCallback() {
                /* class com.wade.fit.util.ble.BleManager.MyBluetoothGattCallback.C26631 */

                public void setSuccess() {
                }

                public void complete(int i, Object obj) {
                    BleClient.showMessage("设置时间完成..." + i);
                    Iterator it = BleManager.this.mListeners.iterator();
                    BleManager.this.isInitCon = true;
                    while (it.hasNext()) {
                        ((AppBleListener) it.next()).initComplete();
                    }
                }
            });
        }

        public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            BleManager.this.reciverBleData(bluetoothGatt, bluetoothGattCharacteristic);
        }

        public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            String bytesToHexString = ByteDataConvertUtil.bytesToHexString(bluetoothGattCharacteristic.getValue());
            BleManager.this.sendBleSuccess();
            LogUtil.d("onCharacteristicWrite:" + bytesToHexString);
        }
    }
}
