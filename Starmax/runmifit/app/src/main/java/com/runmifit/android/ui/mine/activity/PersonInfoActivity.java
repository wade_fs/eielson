package com.wade.fit.ui.mine.activity;

import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.ui.device.activity.ScanDeviceReadyActivity;
import com.wade.fit.ui.main.activity.MainActivity;
import com.wade.fit.persenter.mine.PersonContract;
import com.wade.fit.persenter.mine.PersonPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.util.ble.BleCallback;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.views.dialog.WheelViewDialog;

/* renamed from: com.wade.fit.ui.mine.activity.PersonInfoActivity */
public class PersonInfoActivity extends BaseMvpActivity<PersonPresenter> implements PersonContract.View {
    Button btnSave;
    TextView tvHeight;
    TextView tvWeight;
    /* access modifiers changed from: private */
    public UserBean userBean;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_person_info;
    }

    public void requestFaild() {
    }

    public void initView() {
        int i;
        Resources resources;
        StringBuilder sb;
        int i2;
        Resources resources2;
        StringBuilder sb2;
        this.btnSave.setText((int) R.string.next_step);
        this.titleBack.setImageResource(R.mipmap.back_black);
        this.layoutTitle.setBackgroundColor(getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(9216);
        }
        this.bgView.setVisibility(View.GONE);
        this.userBean = AppApplication.getInstance().getUserBean();
        TextView textView = this.tvHeight;
        if (BleSdkWrapper.isDistUnitKm()) {
            sb = new StringBuilder();
            sb.append(this.userBean.getHeight());
            resources = getResources();
            i = R.string.unit_cm_2;
        } else {
            sb = new StringBuilder();
            sb.append(this.userBean.getHeightLb() / 12);
            sb.append(getResources().getString(R.string.unit_feet));
            sb.append(this.userBean.getHeightLb() % 12);
            resources = getResources();
            i = R.string.unit_inch;
        }
        sb.append(resources.getString(i));
        textView.setText(sb.toString());
        TextView textView2 = this.tvWeight;
        if (BleSdkWrapper.isDistUnitKm()) {
            sb2 = new StringBuilder();
            sb2.append(this.userBean.getWeight());
            resources2 = getResources();
            i2 = R.string.unit_kg;
        } else {
            sb2 = new StringBuilder();
            sb2.append(this.userBean.getWeightLb());
            resources2 = getResources();
            i2 = R.string.unit_lbs;
        }
        sb2.append(resources2.getString(i2));
        textView2.setText(sb2.toString());
    }

    /* access modifiers changed from: package-private */
    public void selectHeight() {
        if (ButtonUtils.isFastDoubleClick(R.id.tvHeight, 1000)) {
            return;
        }
        if (BleSdkWrapper.isDistUnitKm()) {
            DialogHelperNew.showWheelHeightDialog(this, this.userBean.getHeight(), new WheelViewDialog.OnSelectClick() {
                /* class com.wade.fit.ui.mine.activity.$$Lambda$PersonInfoActivity$klpNHqGNRC6g5B2lNN6mwl6j340 */

                public final void onSelect(int i, int i2, int i3) {
                    PersonInfoActivity.this.lambda$selectHeight$0$PersonInfoActivity(i, i2, i3);
                }
            });
        } else {
            DialogHelperNew.showWheelHeightMileDialog(this, this.userBean.getHeightLb(), new WheelViewDialog.OnSelectClick() {
                /* class com.wade.fit.ui.mine.activity.PersonInfoActivity.C25871 */

                public void onSelect(int i, int i2, int i3) {
                    int i4 = i + 1;
                    int i5 = (i4 * 12) + i2;
                    int inch2cm = UnitUtil.inch2cm(i5);
                    PersonInfoActivity.this.userBean.setHeightLb(i5);
                    PersonInfoActivity.this.userBean.setHeight(inch2cm);
                    TextView textView = PersonInfoActivity.this.tvHeight;
                    textView.setText(i4 + PersonInfoActivity.this.getResources().getString(R.string.unit_feet) + i2 + PersonInfoActivity.this.getResources().getString(R.string.unit_inch));
                }
            });
        }
    }

    public /* synthetic */ void lambda$selectHeight$0$PersonInfoActivity(int i, int i2, int i3) {
        Resources resources;
        StringBuilder sb;
        int i4;
        if (BleSdkWrapper.isDistUnitKm()) {
            this.userBean.setHeight(i);
            this.userBean.setHeightLb(UnitUtil.cm2inchs(i));
        } else {
            this.userBean.setHeight(UnitUtil.inch2cm(i));
            this.userBean.setHeightLb(i);
        }
        TextView textView = this.tvHeight;
        if (BleSdkWrapper.isDistUnitKm()) {
            sb = new StringBuilder();
            sb.append(this.userBean.getHeight());
            resources = getResources();
            i4 = R.string.unit_cm_2;
        } else {
            sb = new StringBuilder();
            sb.append(this.userBean.getHeightLb() / 12);
            sb.append(getResources().getString(R.string.unit_feet));
            sb.append(this.userBean.getHeightLb() % 12);
            resources = getResources();
            i4 = R.string.unit_inch;
        }
        sb.append(resources.getString(i4));
        textView.setText(sb.toString());
    }

    /* access modifiers changed from: package-private */
    public void selectWeight() {
        if (!ButtonUtils.isFastDoubleClick(R.id.tvWeight, 1000)) {
            DialogHelperNew.showWheelWeightDialog(this, BleSdkWrapper.isDistUnitKm() ? this.userBean.getWeight() : this.userBean.getWeightLb(), new WheelViewDialog.OnSelectClick() {
                /* class com.wade.fit.ui.mine.activity.$$Lambda$PersonInfoActivity$4PB43ND1FTh2duEJ6HaPfqQqJE4 */

                public final void onSelect(int i, int i2, int i3) {
                    PersonInfoActivity.this.lambda$selectWeight$1$PersonInfoActivity(i, i2, i3);
                }
            });
        }
    }

    public /* synthetic */ void lambda$selectWeight$1$PersonInfoActivity(int i, int i2, int i3) {
        Resources resources;
        StringBuilder sb;
        int i4;
        if (BleSdkWrapper.isDistUnitKm()) {
            this.userBean.setWeight(i * 10);
            this.userBean.setWeightLb(UnitUtil.kg2lb((float) i));
        } else {
            this.userBean.setWeightLb(i);
            this.userBean.setWeight(UnitUtil.lb2kg((float) i) * 10);
        }
        TextView textView = this.tvWeight;
        if (BleSdkWrapper.isDistUnitKm()) {
            sb = new StringBuilder();
            sb.append(this.userBean.getWeight());
            resources = getResources();
            i4 = R.string.unit_kg;
        } else {
            sb = new StringBuilder();
            sb.append(this.userBean.getWeightLb());
            resources = getResources();
            i4 = R.string.unit_lbs;
        }
        sb.append(resources.getString(i4));
        textView.setText(sb.toString());
    }

    /* access modifiers changed from: package-private */
    public void toNext() {
        AppApplication.getInstance().setUserBean(this.userBean);
        SPHelper.saveUserBean(this.userBean);
        ((PersonPresenter) this.mPresenter).setUserInfo(this.userBean);
    }

    public void requestSuccess() {
        hideLoading();
        if (BleSdkWrapper.isConnected()) {
            BleSdkWrapper.setUserInfo(this.userBean, new BleCallback() {
                /* class com.wade.fit.ui.mine.activity.PersonInfoActivity.C25882 */

                public void setSuccess() {
                }

                public void complete(int i, Object obj) {
                    BleSdkWrapper.setDeviceState(null);
                    IntentUtil.goToActivity(PersonInfoActivity.this, MainActivity.class);
                }
            });
        } else if (BleSdkWrapper.isBind()) {
            IntentUtil.goToActivity(this, MainActivity.class);
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("from", "PersonInfoActivity");
            IntentUtil.goToActivityAndFinish(this, ScanDeviceReadyActivity.class, bundle);
        }
    }
}
