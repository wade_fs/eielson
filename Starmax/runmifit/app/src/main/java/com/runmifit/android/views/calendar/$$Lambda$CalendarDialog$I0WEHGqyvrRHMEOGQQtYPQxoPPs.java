package com.wade.fit.views.calendar;

import com.ldf.calendar.component.CalendarAttr;
import com.ldf.calendar.component.CalendarViewAdapter;

/* renamed from: com.wade.fit.views.calendar.-$$Lambda$CalendarDialog$I0WEHGqyvrRHMEOGQQtYPQxoPPs  reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$CalendarDialog$I0WEHGqyvrRHMEOGQQtYPQxoPPs implements CalendarViewAdapter.OnCalendarTypeChanged {
    public static final /* synthetic */ $$Lambda$CalendarDialog$I0WEHGqyvrRHMEOGQQtYPQxoPPs INSTANCE = new $$Lambda$CalendarDialog$I0WEHGqyvrRHMEOGQQtYPQxoPPs();

    private /* synthetic */ $$Lambda$CalendarDialog$I0WEHGqyvrRHMEOGQQtYPQxoPPs() {
    }

    public final void onCalendarTypeChanged(CalendarAttr.CalendarType calendarType) {
        CalendarDialog.lambda$initCalendarView$0(calendarType);
    }
}
