package com.wade.fit.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import com.wade.fit.R;

public class DialogManager {
    public static ProgressDialog showWaitDialog(Context context, String str) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        if (TextUtils.isEmpty(str)) {
            str = context.getString(R.string.state_loading);
        }
        progressDialog.setMessage(str);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }
}
