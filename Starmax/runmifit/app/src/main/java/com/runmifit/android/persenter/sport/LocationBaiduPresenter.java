package com.wade.fit.persenter.sport;

import android.content.Context;
import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.wade.fit.model.bean.LatLngBean;
import com.wade.fit.model.bean.LocationMessage;
import com.wade.fit.util.StringUtils;
import com.wade.fit.util.log.LogUtil;

public class LocationBaiduPresenter extends BaseLocationPresenter {
    int count;
    LocationClient mlocationClient;

    /* access modifiers changed from: private */
    public int translateGps(int i) {
        if (i != 0) {
            return (i == 1 || i != 2) ? 1 : 2;
        }
        return 0;
    }

    public void init(Context context) {
        super.init(context);
        this.mlocationClient = new LocationClient(context);
        LocationClientOption locationClientOption = new LocationClientOption();
        MyLocationListener myLocationListener = new MyLocationListener();
        locationClientOption.setLocationMode(LocationClientOption.LocationMode.Device_Sensors);
        locationClientOption.setCoorType("bd09ll");
        locationClientOption.setScanSpan(2000);
        locationClientOption.setIsNeedAddress(true);
        locationClientOption.setIsNeedLocationDescribe(true);
        locationClientOption.setNeedDeviceDirect(false);
        locationClientOption.setLocationNotify(true);
        locationClientOption.setIgnoreKillProcess(true);
        locationClientOption.setIsNeedLocationDescribe(true);
        locationClientOption.setIsNeedLocationPoiList(true);
        locationClientOption.SetIgnoreCacheException(false);
        locationClientOption.setOpenGps(true);
        locationClientOption.setIsNeedAltitude(false);
        this.mlocationClient.registerLocationListener(myLocationListener);
        this.mlocationClient.setLocOption(locationClientOption);
    }

    public void stopLocation() {
        if (this.mlocationClient != null) {
            super.stopLocation();
            this.mlocationClient.stop();
            LogUtil.d("stopLocation");
        }
    }

    public void startLocation(boolean z) {
        super.startLocation(z);
        LogUtil.d("startLocation");
        this.mlocationClient.start();
    }

    public class MyLocationListener extends BDAbstractLocationListener {
        public MyLocationListener() {
        }

        public void onReceiveLocation(BDLocation bDLocation) {
            double latitude = bDLocation.getLatitude();
            double longitude = bDLocation.getLongitude();
            float radius = bDLocation.getRadius();
            bDLocation.getCoorType();
            int locType = bDLocation.getLocType();
            LogUtil.d("onReceiveLocation ：" + latitude + "," + longitude);
            StringBuilder sb = new StringBuilder();
            sb.append("radius ：");
            sb.append(radius);
            LogUtil.d(sb.toString());
            LogUtil.d("onReceiveLocation ：");
            LogUtil.d("onReceiveLocation ：" + locType);
            if (bDLocation != null) {
                LatLngBean latLngBean = new LatLngBean();
                latLngBean.setLatitude(latitude);
                latLngBean.setLongitude(longitude);
                LocationMessage locationMessage = new LocationMessage(202, latLngBean);
                locationMessage.accurac = (float) bDLocation.getGpsAccuracyStatus();
                locationMessage.city = bDLocation.getCity();
                locationMessage.country = bDLocation.getCountry();
                locationMessage.speed = StringUtils.format("%.2f", Float.valueOf(bDLocation.getSpeed()));
                locationMessage.gpsAccuracyStatus = LocationBaiduPresenter.this.translateGps(bDLocation.getGpsAccuracyStatus());
                LocationBaiduPresenter.this.onLocationChanged(locationMessage);
                return;
            }
            LogUtil.d("定位失败");
        }
    }

    private void setLatLng(LatLngBean latLngBean, BDLocation bDLocation) {
        double longitude = bDLocation.getLongitude();
        double latitude = bDLocation.getLatitude();
        double sqrt = Math.sqrt((longitude * longitude) + (latitude * latitude)) + (Math.sin(latitude * 3.141592653589793d) * 2.0E-5d);
        double atan2 = Math.atan2(latitude, longitude) + (Math.cos(longitude * 3.141592653589793d) * 3.0E-6d);
        latLngBean.setLatitude((sqrt * Math.sin(atan2)) + 0.006d);
        latLngBean.setLongitude((Math.cos(atan2) * sqrt) + 0.0065d);
    }
}
