package com.wade.fit.model.bean;

public enum WeekMonthYearEnum {
    WEEK,
    MONTH,
    YEAR
}
