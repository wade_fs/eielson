package com.wade.fit.util.thirddataplatform;

import android.util.Log;
import com.baidu.mobstat.Config;

public class DebugLog {
    static String className = null;
    static int lineNumber = 0;
    static boolean logSwitch = true;
    static String methodName;

    private DebugLog() {
    }

    public static void setLogSwitch(boolean z) {
        logSwitch = z;
    }

    public static boolean isDebuggable() {
        return logSwitch;
    }

    private static String createLog(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        stringBuffer.append(methodName);
        stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
        stringBuffer.append(lineNumber);
        stringBuffer.append("]");
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    private static void getMethodNames(StackTraceElement[] stackTraceElementArr) {
        className = stackTraceElementArr[1].getFileName();
        methodName = stackTraceElementArr[1].getMethodName();
        lineNumber = stackTraceElementArr[1].getLineNumber();
    }

    private static void getMethodNames(StackTraceElement[] stackTraceElementArr, int i) {
        className = stackTraceElementArr[1].getFileName();
        methodName = stackTraceElementArr[1].getMethodName();
        lineNumber = stackTraceElementArr[1].getLineNumber();
    }

    /* renamed from: e */
    public static void m6219e(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.d(className, createLog(str));
        }
    }

    /* renamed from: i */
    public static void m6220i(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.i(className, createLog(str));
        }
    }

    /* renamed from: d */
    public static void m6217d(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.i(className, createLog(str));
        }
    }

    /* renamed from: d2 */
    public static void m6218d2(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace(), 1);
            Log.i(className, createLog(str));
        }
    }

    /* renamed from: v */
    public static void m6221v(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.v(className, createLog(str));
        }
    }

    /* renamed from: w */
    public static void m6222w(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.w(className, createLog(str));
        }
    }

    public static void wtf(String str) {
        if (isDebuggable()) {
            getMethodNames(new Throwable().getStackTrace());
            Log.wtf(className, createLog(str));
        }
    }
}
