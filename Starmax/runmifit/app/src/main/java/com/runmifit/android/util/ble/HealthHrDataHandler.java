package com.wade.fit.util.ble;

import com.google.common.primitives.UnsignedBytes;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.greendao.gen.HealthHeartRateDao;
import com.wade.fit.greendao.gen.HealthHeartRateItemDao;
import com.wade.fit.model.bean.HeartRateBean;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.GsonUtil;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HealthHrDataHandler {
    int dataIndex = 0;
    List<byte[]> datas = new ArrayList();

    public void init(byte[] bArr) {
        this.datas.clear();
        int bytesToInt = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 1, 3));
        if (bytesToInt % 20 == 0) {
            this.dataIndex = bytesToInt / 20;
        } else {
            this.dataIndex = (bytesToInt / 20) + 1;
        }
        this.datas.add(bArr);
        LogUtil.d("数据长度:" + bytesToInt + ",dataIndex:" + this.dataIndex);
    }

    public HandlerBleDataResult receiverHistory(byte[] bArr) {
        this.datas.add(bArr);
        LogUtil.d("receiverHistory:" + this.datas.size());
        if (this.datas.size() == this.dataIndex) {
            handler(this.datas);
        }
        HandlerBleDataResult handlerBleDataResult = new HandlerBleDataResult();
        handlerBleDataResult.isComplete = this.datas.size() == this.dataIndex;
        handlerBleDataResult.hasNext = true;
        return handlerBleDataResult;
    }

    public Object handler(List<byte[]> list) {
        return handlerHistory(list);
    }

    public HealthHeartRate handlerCurrent(byte[] bArr) {
        HealthHeartRate healthHeartRate;
        LogUtil.d("获取当前心率数据");
        byte b = bArr[4] & UnsignedBytes.MAX_VALUE;
        byte b2 = bArr[5] & UnsignedBytes.MAX_VALUE;
        byte b3 = bArr[6] & UnsignedBytes.MAX_VALUE;
        byte b4 = bArr[7] & UnsignedBytes.MAX_VALUE;
        LogUtil.d("heartValue:" + ((int) b) + ",fz:" + ((int) b2) + ",sh:" + ((int) b3) + ",xy:" + ((int) b4));
        int[] iArr = DateUtil.todayYearMonthDay();
        List list = AppApplication.getInstance().getDaoSession().getHealthHeartRateDao().queryBuilder().where(HealthHeartRateDao.Properties.Year.eq(Integer.valueOf(iArr[0])), HealthHeartRateDao.Properties.Month.eq(Integer.valueOf(iArr[1])), HealthHeartRateDao.Properties.Day.eq(Integer.valueOf(iArr[2]))).orderDesc(HealthHeartRateDao.Properties.Date).build().list();
        if (list == null || list.isEmpty()) {
            healthHeartRate = new HealthHeartRate();
            healthHeartRate.setDate(ProDbUtils.getDate(iArr[0], iArr[1], iArr[2]).getTime());
            healthHeartRate.setYear(iArr[0]);
            healthHeartRate.setMonth(iArr[1]);
            healthHeartRate.setDay(iArr[2]);
            healthHeartRate.setUserId(AppApplication.getInstance().getUserBean().getUserId());
        } else {
            healthHeartRate = (HealthHeartRate) list.get(0);
        }
        healthHeartRate.setSilentHeart(b);
        HeartRateBean heartRateBean = new HeartRateBean();
        if (b2 > 0 && b3 > 0) {
            heartRateBean.heartRateValue = b;
            heartRateBean.fzValue = b2;
            heartRateBean.ssValue = b3;
            heartRateBean.OxygenValue = b4;
            heartRateBean.date = ProDbUtils.getDate(iArr[0], iArr[1], iArr[2]);
            SharePreferenceUtils.put(AppApplication.getInstance(), Constants.LAST_HEART_RATE_BEAN, GsonUtil.toJson(heartRateBean));
        }
        return healthHeartRate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(int, int):int}
     arg types: [byte, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(int, int):int} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(int, int):int}
     arg types: [byte, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int} */
    private List<HealthHeartRateItem> handlerHistory(List<byte[]> list) {
        List<byte[]> list2 = list;
        ArrayList arrayList = new ArrayList();
        byte[] bArr = list2.get(0);
        byte[] bArr2 = new byte[(list.size() * 20)];
        for (int i = 0; i < list.size(); i++) {
            byte[] bArr3 = list2.get(i);
            for (int i2 = 0; i2 < bArr3.length; i2++) {
                bArr2[(i * 20) + i2] = bArr3[i2];
            }
        }
        int bytesToInt = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 1, 3));
        byte b = bArr[3];
        int bytesToInt2 = ByteDataConvertUtil.bytesToInt(Arrays.copyOfRange(bArr, 4, 6));
        byte b2 = bArr[6];
        byte b3 = bArr[7];
        byte b4 = bArr[8];
        LogUtil.m5266d("***************心率血压解析数据 length:" + bytesToInt + ",总条数:" + (bytesToInt / 20) + ",操作类型:" + ((int) b) + ",年:" + bytesToInt2 + ",月:" + ((int) b2) + ",日:" + ((int) b3) + ",周期:" + ((int) b4) + "********", Constants.SYCN_PATH);
        HealthHeartRateDao healthHeartRateDao = AppApplication.getInstance().getDaoSession().getHealthHeartRateDao();
        HealthHeartRate healthHeartRate = new HealthHeartRate();
        healthHeartRate.setDate(ProDbUtils.getDate(bytesToInt2, b2, b3).getTime());
        healthHeartRate.setYear(bytesToInt2);
        healthHeartRate.setMonth(b2);
        healthHeartRate.setDay(b3);
        ArrayList arrayList2 = new ArrayList();
        int i3 = 1440 / b4;
        int i4 = 9;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        boolean z = false;
        while (true) {
            int i8 = i4 + 4;
            if (i8 >= bArr2.length || i5 >= i3) {
                HealthHeartRateDao healthHeartRateDao2 = healthHeartRateDao;
                ArrayList arrayList3 = arrayList;
                LogUtil.d("数据 大小 :" + i5);
            } else {
                byte b5 = bArr2[i4] & UnsignedBytes.MAX_VALUE;
                int i9 = i8;
                byte b6 = bArr2[i4 + 1] & UnsignedBytes.MAX_VALUE;
                int i10 = i3;
                byte b7 = bArr2[i4 + 2] & UnsignedBytes.MAX_VALUE;
                byte b8 = bArr2[i4 + 3] & UnsignedBytes.MAX_VALUE;
                i5++;
                byte[] bArr4 = bArr2;
                HealthHeartRateItem healthHeartRateItem = new HealthHeartRateItem();
                healthHeartRateItem.setOffsetMinute(b4);
                healthHeartRateItem.setHeartRaveValue(b5);
                HealthHeartRateDao healthHeartRateDao3 = healthHeartRateDao;
                ArrayList arrayList4 = arrayList;
                healthHeartRateItem.setDate(healthHeartRate.getDate());
                healthHeartRateItem.setSs(b7);
                healthHeartRateItem.setFz(b6);
                healthHeartRateItem.setOxygen(b8);
                healthHeartRateItem.setYear(bytesToInt2);
                healthHeartRateItem.setMonth(b2);
                healthHeartRateItem.setDay(b3);
                if (b5 > 0) {
                    i6++;
                    i7 += b5;
                    if (!z) {
                        healthHeartRate.setMaxHr(b5);
                        healthHeartRate.setMinHr(b5);
                        z = true;
                    } else {
                        healthHeartRate.setMaxHr(Math.max((int) b5, healthHeartRate.getMaxHr()));
                        healthHeartRate.setMinHr(Math.min((int) b5, healthHeartRate.getMinHr()));
                    }
                }
                healthHeartRate.setSilentHeart(b5);
                arrayList2.add(healthHeartRateItem);
                bArr2 = bArr4;
                i4 = i9;
                i3 = i10;
                arrayList = arrayList4;
                healthHeartRateDao = healthHeartRateDao3;
            }
        }
        HealthHeartRateDao healthHeartRateDao22 = healthHeartRateDao;
        ArrayList arrayList32 = arrayList;
        LogUtil.d("数据 大小 :" + i5);
        if (i6 > 0) {
            healthHeartRate.setAvgHr(i7 / i6);
        }
        LogUtil.d("数据 大小 :" + i7 + "/" + i6);
        healthHeartRateDao22.queryBuilder().where(HealthHeartRateDao.Properties.Year.eq(Integer.valueOf(bytesToInt2)), HealthHeartRateDao.Properties.Month.eq(Integer.valueOf(b2)), HealthHeartRateDao.Properties.Day.eq(Integer.valueOf(b3))).buildDelete().executeDeleteWithoutDetachingEntities();
        healthHeartRateDao22.insert(healthHeartRate);
        HealthHeartRateItemDao healthHeartRateItemDao = AppApplication.getInstance().getDaoSession().getHealthHeartRateItemDao();
        healthHeartRateItemDao.queryBuilder().where(HealthHeartRateItemDao.Properties.Year.eq(Integer.valueOf(bytesToInt2)), HealthHeartRateItemDao.Properties.Month.eq(Integer.valueOf(b2)), HealthHeartRateItemDao.Properties.Day.eq(Integer.valueOf(b3))).buildDelete().executeDeleteWithoutDetachingEntities();
        healthHeartRateItemDao.insertInTx(arrayList2);
        LogUtil.m5266d("***************心率血压解析数据完毕*********************", Constants.SYCN_PATH);
        return arrayList32;
    }
}
