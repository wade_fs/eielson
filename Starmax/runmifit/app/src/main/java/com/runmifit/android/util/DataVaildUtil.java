package com.wade.fit.util;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.wade.fit.R;
import java.util.regex.Pattern;

public class DataVaildUtil {
    public static boolean validEmail(Context context, String str) {
        Resources resources = context.getResources();
        if (TextUtils.isEmpty(str)) {
            ToastUtil.showToast(context, resources.getString(R.string.empty_email));
            return false;
        } else if (isEmail(str)) {
            return true;
        } else {
            ToastUtil.showToast(context, resources.getString(R.string.email_not_valid));
            return false;
        }
    }

    private static boolean validData(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            return true;
        }
        ToastUtil.showToast(context, str2);
        return false;
    }

    public static boolean validData(Context context, String str, int i) {
        return validData(context, str, context.getResources().getString(i));
    }

    public static boolean validPwd(Context context, String str) {
        Resources resources = context.getResources();
        if (TextUtils.isEmpty(str)) {
            ToastUtil.showToast(context, resources.getString(R.string.empty_pwd));
            return false;
        } else if (str.trim().length() < 6) {
            ToastUtil.showToast(context, resources.getString(R.string.pwd_not_valid));
            return false;
        } else if (str.trim().length() <= 20) {
            return true;
        } else {
            ToastUtil.showToast(context, resources.getString(R.string.pwd_not_valid2));
            return false;
        }
    }

    public static boolean vaildEmailAndPwd(Context context, String str, String str2) {
        Resources resources = context.getResources();
        if (TextUtils.isEmpty(str)) {
            ToastUtil.showToast(context, resources.getString(R.string.empty_email));
            return false;
        } else if (!isEmail(str)) {
            ToastUtil.showToast(context, resources.getString(R.string.email_not_valid));
            return false;
        } else if (TextUtils.isEmpty(str2)) {
            ToastUtil.showToast(context, resources.getString(R.string.empty_pwd));
            return false;
        } else if (str2.trim().length() < 6) {
            ToastUtil.showToast(context, resources.getString(R.string.pwd_not_valid));
            return false;
        } else if (str2.trim().length() <= 20) {
            return true;
        } else {
            ToastUtil.showToast(context, resources.getString(R.string.pwd_not_valid2));
            return false;
        }
    }

    public static boolean vaildEmailAndPwd(Context context, String str, String str2, boolean z) {
        Resources resources = context.getResources();
        if (TextUtils.isEmpty(str)) {
            if (z) {
                ToastUtil.showToast(context, resources.getString(R.string.empty_email));
            }
            return false;
        } else if (!isEmail(str)) {
            if (z) {
                ToastUtil.showToast(context, resources.getString(R.string.email_not_valid));
            }
            return false;
        } else if (TextUtils.isEmpty(str2)) {
            if (z) {
                ToastUtil.showToast(context, resources.getString(R.string.empty_pwd));
            }
            return false;
        } else if (str2.trim().length() < 6) {
            if (z) {
                ToastUtil.showToast(context, resources.getString(R.string.pwd_not_valid));
            }
            return false;
        } else if (str2.trim().length() <= 20) {
            return true;
        } else {
            if (z) {
                ToastUtil.showToast(context, resources.getString(R.string.pwd_not_valid2));
            }
            return false;
        }
    }

    private static boolean isEmail(String str) {
        return str.matches("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
    }

    public static boolean vaildUserName(String str) {
        return Pattern.compile("delete|sleep|drop|truncate|%20|=|<|>|%|\\+|\\\\|'|\"|!=|<|>").matcher(str).find();
    }
}
