package com.wade.fit.ui.sport.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.model.bean.BaseMessage;
import com.wade.fit.util.CommonUtil;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.NetUtils;
import com.wade.fit.util.baidumap.MyOrientationListener;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/* renamed from: com.wade.fit.ui.sport.activity.SportOutActivity */
public class SportOutActivity extends BaseActivity {
    ImageView imgSportBike;
    ImageView imgSportRun;
    ImageView imgSportWalk;
    /* access modifiers changed from: private */
    public boolean isLocation = true;
    /* access modifiers changed from: private */
    public LatLng last;
    LinearLayout llSportBike;
    LinearLayout llSportRun;
    LinearLayout llSportWalk;
    private BitmapDescriptor loactionDB;
    /* access modifiers changed from: private */
    public String locationTime = "";
    private BaiduMap mBaiduMap;
    private float mCurrentX;
    /* access modifiers changed from: private */
    public float mCurrentZoom;
    private Dialog mDialog;
    private LocationClient mLocClient;
    /* access modifiers changed from: private */
    public BDLocation mLocation;
    MapView mMapView;
    private MyLocationListenner myListener;
    private MyOrientationListener myOrientationListener;
    private String[] permissionsLocation = {"android.permission.ACCESS_FINE_LOCATION"};
    private List<LatLng> points = new ArrayList();
    private HealthActivity saveHealth;
    /* access modifiers changed from: private */
    public LatLng temporary;
    TextView tvSportBike;
    TextView tvSportRun;
    TextView tvSportWalk;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_sport_out;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.titleName.setText(getResources().getString(R.string.outdoor_sport));
        this.saveHealth = new HealthActivity();
        this.saveHealth.setDataFrom(1);
        Calendar instance = Calendar.getInstance();
        this.saveHealth.setDate(instance.getTimeInMillis());
        this.saveHealth.setYear(instance.get(1));
        this.saveHealth.setMonth(instance.get(2) + 1);
        this.saveHealth.setDay(instance.get(5));
        this.saveHealth.setHour(instance.get(11));
        this.saveHealth.setMinute(instance.get(12));
        this.saveHealth.setSecond(instance.get(13));
        setTypeView(0);
        this.loactionDB = BitmapDescriptorFactory.fromResource(R.mipmap.run_go);
        this.mBaiduMap = this.mMapView.getMap();
        this.mBaiduMap.setMyLocationEnabled(true);
        this.mMapView.removeViewAt(1);
        this.mMapView.removeViewAt(2);
        this.mMapView.showZoomControls(false);
        this.mCurrentZoom = 18.0f;
        this.mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
            /* class com.wade.fit.ui.sport.activity.SportOutActivity.C26191 */

            public void onMapStatusChange(MapStatus mapStatus) {
            }

            public void onMapStatusChangeStart(MapStatus mapStatus) {
            }

            public void onMapStatusChangeStart(MapStatus mapStatus, int i) {
            }

            public void onMapStatusChangeFinish(MapStatus mapStatus) {
                float unused = SportOutActivity.this.mCurrentZoom = mapStatus.zoom;
            }
        });
        this.myOrientationListener = new MyOrientationListener(this);
        this.myOrientationListener.start();
        this.myOrientationListener.setOnOrientationListener(new MyOrientationListener.OnOrientationListener() {
            /* class com.wade.fit.ui.sport.activity.$$Lambda$SportOutActivity$JvrGmCMVBKbsfZfs0v2YJysob_Q */

            public final void onOrientationChanged(float f) {
                SportOutActivity.this.lambda$initView$0$SportOutActivity(f);
            }
        });
        this.mBaiduMap.setMyLocationConfiguration(new MyLocationConfiguration(MyLocationConfiguration.LocationMode.FOLLOWING, true, null));
        this.mLocClient = new LocationClient(this);
        this.myListener = new MyLocationListenner();
        this.mLocClient.registerLocationListener(this.myListener);
        LocationClientOption locationClientOption = new LocationClientOption();
        locationClientOption.setLocationMode(LocationClientOption.LocationMode.Device_Sensors);
        locationClientOption.setOpenGps(true);
        locationClientOption.setCoorType("bd09ll");
        locationClientOption.setScanSpan(1000);
        this.mLocClient.setLocOption(locationClientOption);
        if (!NetUtils.isConnected(this)) {
            showToast(getResources().getString(R.string.httpConnError));
        } else if (!CommonUtil.isOPen(this)) {
            DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_location_title), getResources().getString(R.string.permisson_location_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                /* class com.wade.fit.ui.sport.activity.$$Lambda$SportOutActivity$5WoKTr0gpBFmBlIYmz6cOFY7DWI */

                public final void onClick(View view) {
                    SportOutActivity.this.lambda$initView$1$SportOutActivity(view);
                }
            }, new View.OnClickListener() {
                /* class com.wade.fit.ui.sport.activity.$$Lambda$SportOutActivity$_KNFTy071qXuAmGO2B8MvkYn8E */

                public final void onClick(View view) {
                    SportOutActivity.this.lambda$initView$2$SportOutActivity(view);
                }
            });
        } else if (!checkSelfPermission(this.permissionsLocation)) {
            requestPermissions(1001, this.permissionsLocation);
        } else {
            this.myOrientationListener.start();
            this.mLocClient.start();
        }
    }

    public /* synthetic */ void lambda$initView$0$SportOutActivity(float f) {
        this.mCurrentX = f;
        if (this.mLocation != null) {
            this.mBaiduMap.setMyLocationData(new MyLocationData.Builder().direction(this.mCurrentX).accuracy(this.mLocation.getRadius()).latitude(this.mLocation.getLatitude()).longitude(this.mLocation.getLongitude()).build());
            this.mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(MyLocationConfiguration.LocationMode.NORMAL, true, this.loactionDB));
        }
    }

    public /* synthetic */ void lambda$initView$1$SportOutActivity(View view) {
        Intent intent = new Intent();
        intent.setAction("android.settings.LOCATION_SOURCE_SETTINGS");
        startActivityForResult(intent, 1000);
    }

    public /* synthetic */ void lambda$initView$2$SportOutActivity(View view) {
        finish();
    }

    /* access modifiers changed from: package-private */
    public void setTypeRun() {
        setTypeView(0);
    }

    /* access modifiers changed from: package-private */
    public void setTypeWalk() {
        setTypeView(1);
    }

    /* access modifiers changed from: package-private */
    public void setTypeBike() {
        setTypeView(2);
    }

    /* access modifiers changed from: protected */
    public void handleMessage(BaseMessage baseMessage) {
        super.handleMessage(baseMessage);
        int type = baseMessage.getType();
        if (type == 1004) {
            showToast(getResources().getString(R.string.state_network_error));
        } else if (type == 1005) {
            this.mLocClient.start();
        }
    }

    /* access modifiers changed from: package-private */
    public void startSport() {
        if (this.isLocation) {
            showToast(getResources().getString(R.string.location_tips));
            return;
        }
        Bundle bundle = new Bundle();
        LatLng latLng = this.last;
        if (latLng != null) {
            bundle.putDouble("pointLat", latLng.latitude);
            bundle.putDouble("pointLon", this.last.longitude);
        } else {
            bundle.putDouble("pointLat", 0.0d);
            bundle.putDouble("pointLon", 0.0d);
        }
        LatLng latLng2 = this.temporary;
        if (latLng2 != null) {
            bundle.putDouble("tempPointLat", latLng2.latitude);
            bundle.putDouble("tempPointLon", this.temporary.longitude);
        }
        bundle.putSerializable("saveHearth", this.saveHealth);
        bundle.putFloat("mCurrentZoom", this.mCurrentZoom);
        bundle.putString("locationTime", this.locationTime);
        IntentUtil.goToActivityAndFinish(this, SportActivity.class, bundle);
    }

    public void setTypeView(int i) {
        if (i == 0) {
            this.saveHealth.setType(1);
            this.imgSportRun.setImageResource(R.mipmap.sport_run_select);
            this.tvSportRun.setTextColor(getResources().getColor(R.color.sport_run_txt));
            this.imgSportWalk.setImageResource(R.mipmap.sport_walk_gray);
            this.tvSportWalk.setTextColor(getResources().getColor(R.color.small_font_color));
            this.imgSportBike.setImageResource(R.mipmap.sport_bike_gray);
            this.tvSportBike.setTextColor(getResources().getColor(R.color.small_font_color));
        } else if (i == 1) {
            this.saveHealth.setType(0);
            this.imgSportRun.setImageResource(R.mipmap.sport_run_gray);
            this.tvSportRun.setTextColor(getResources().getColor(R.color.small_font_color));
            this.imgSportWalk.setImageResource(R.mipmap.sport_walk_select);
            this.tvSportWalk.setTextColor(getResources().getColor(R.color.sport_walk_txt));
            this.imgSportBike.setImageResource(R.mipmap.sport_bike_gray);
            this.tvSportBike.setTextColor(getResources().getColor(R.color.small_font_color));
        } else if (i == 2) {
            this.saveHealth.setType(3);
            this.imgSportRun.setImageResource(R.mipmap.sport_run_gray);
            this.tvSportRun.setTextColor(getResources().getColor(R.color.small_font_color));
            this.imgSportWalk.setImageResource(R.mipmap.sport_walk_gray);
            this.tvSportWalk.setTextColor(getResources().getColor(R.color.small_font_color));
            this.imgSportBike.setImageResource(R.mipmap.sport_bike_select);
            this.tvSportBike.setTextColor(getResources().getColor(R.color.sport_bike_txt));
        }
    }

    /* renamed from: com.wade.fit.ui.sport.activity.SportOutActivity$MyLocationListenner */
    public class MyLocationListenner implements BDLocationListener {
        public MyLocationListenner() {
        }

        public void onReceiveLocation(BDLocation bDLocation) {
            if (bDLocation != null && SportOutActivity.this.mMapView != null) {
                BDLocation unused = SportOutActivity.this.mLocation = bDLocation;
                LogUtil.d("location:" + bDLocation.getLocType() + "," + bDLocation.getLatitude() + "," + bDLocation.getLongitude());
                LatLng unused2 = SportOutActivity.this.temporary = new LatLng(bDLocation.getLatitude(), bDLocation.getLongitude());
                SportOutActivity.this.locateAndZoom(bDLocation, new LatLng(bDLocation.getLatitude(), bDLocation.getLongitude()));
                boolean unused3 = SportOutActivity.this.isLocation = false;
                if (bDLocation.getLocType() == 61) {
                    LatLng access$500 = SportOutActivity.this.getMostAccuracyLocation(bDLocation);
                    LatLng unused4 = SportOutActivity.this.last = access$500;
                    if (access$500 != null) {
                        String unused5 = SportOutActivity.this.locationTime = bDLocation.getTime();
                        SportOutActivity.this.locateAndZoom(bDLocation, access$500);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public LatLng getMostAccuracyLocation(BDLocation bDLocation) {
        if (bDLocation.getRadius() > 50.0f) {
            return null;
        }
        LatLng latLng = new LatLng(bDLocation.getLatitude(), bDLocation.getLongitude());
        if (DistanceUtil.getDistance(this.last, latLng) > 10.0d) {
            this.last = latLng;
            this.points.clear();
            return null;
        }
        this.points.add(latLng);
        this.last = latLng;
        if (this.points.size() < 5) {
            return null;
        }
        this.points.clear();
        return latLng;
    }

    /* access modifiers changed from: private */
    public void locateAndZoom(BDLocation bDLocation, LatLng latLng) {
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(latLng).zoom(this.mCurrentZoom);
        this.mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        LocationClient locationClient = this.mLocClient;
        if (locationClient != null) {
            locationClient.unRegisterLocationListener(this.myListener);
            this.mLocClient.stop();
        }
        BaiduMap baiduMap = this.mBaiduMap;
        if (baiduMap != null) {
            baiduMap.setMyLocationEnabled(false);
        }
        MapView mapView = this.mMapView;
        if (mapView != null) {
            mapView.getMap().clear();
            this.mMapView.onDestroy();
            this.mMapView = null;
        }
        BitmapDescriptor bitmapDescriptor = this.loactionDB;
        if (bitmapDescriptor != null) {
            bitmapDescriptor.recycle();
        }
        this.myOrientationListener.stop();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1000) {
            if (!CommonUtil.isOPen(this)) {
                finish();
            } else if (!checkSelfPermission(this.permissionsLocation)) {
                requestPermissions(1001, this.permissionsLocation);
            } else {
                this.mLocClient.start();
            }
        } else if (i != 1001) {
        } else {
            if (!checkSelfPermission(this.permissionsLocation)) {
                requestPermissions(1001, this.permissionsLocation);
            } else {
                this.mLocClient.start();
            }
        }
    }

    public void requestPermissionsSuccess(int i) {
        if (i == 1001) {
            this.mLocClient.start();
        }
    }

    public void requestPermissionsFail(int i) {
        int i2 = 0;
        while (true) {
            String[] strArr = this.permissionsLocation;
            if (i2 >= strArr.length) {
                return;
            }
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, strArr[i2])) {
                this.mDialog = DialogHelperNew.showRemindDialog(this, getResources().getString(R.string.permisson_location_title), getResources().getString(R.string.permisson_location_tips), getResources().getString(R.string.permisson_location_open), new View.OnClickListener() {
                    /* class com.wade.fit.ui.sport.activity.$$Lambda$SportOutActivity$KMAIKUBmMfM9MM87R5IzlROsqLc */

                    public final void onClick(View view) {
                        SportOutActivity.this.lambda$requestPermissionsFail$3$SportOutActivity(view);
                    }
                }, new View.OnClickListener() {
                    /* class com.wade.fit.ui.sport.activity.$$Lambda$SportOutActivity$X8rLAXQbXNhr57dIW0cvhNJ9ZsU */

                    public final void onClick(View view) {
                        SportOutActivity.this.lambda$requestPermissionsFail$4$SportOutActivity(view);
                    }
                });
                return;
            } else {
                finish();
                i2++;
            }
        }
    }

    public /* synthetic */ void lambda$requestPermissionsFail$3$SportOutActivity(View view) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", getApplicationContext().getPackageName(), null));
        startActivityForResult(intent, 1001);
    }

    public /* synthetic */ void lambda$requestPermissionsFail$4$SportOutActivity(View view) {
        this.mDialog.dismiss();
        finish();
    }
}
