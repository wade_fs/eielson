package com.wade.fit.model.bean;

public class KmSpeedBean {
    public static final int EVERY_KM_SPEED = 0;
    public static final int TOTAL_TIME = 1;
    private int distance;
    private int evgTime;
    private boolean isFullOneKm;
    private boolean isKm;
    private String kmCount;
    private String speed;
    private String totalTime;
    private int type;

    public int getDistance() {
        return this.distance;
    }

    public void setDistance(int i) {
        this.distance = i;
    }

    public boolean isFullOneKm() {
        return this.isFullOneKm;
    }

    public void setFullOneKm(boolean z) {
        this.isFullOneKm = z;
    }

    public boolean isKm() {
        return this.isKm;
    }

    public void setKm(boolean z) {
        this.isKm = z;
    }

    public int getEvgTime() {
        return this.evgTime;
    }

    public void setEvgTime(int i) {
        this.evgTime = i;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int i) {
        this.type = i;
    }

    public String getTotalTime() {
        return this.totalTime;
    }

    public void setTotalTime(String str) {
        this.totalTime = str;
    }

    public String getKmCount() {
        return this.kmCount;
    }

    public void setKmCount(String str) {
        this.kmCount = str;
    }

    public String getSpeed() {
        return this.speed;
    }

    public void setSpeed(String str) {
        this.speed = str;
    }
}
