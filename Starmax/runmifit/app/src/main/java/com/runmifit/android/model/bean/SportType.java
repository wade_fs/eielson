package com.wade.fit.model.bean;

public class SportType {
    public static final int SPORT_TYPE_AEROBICS = 17;
    public static final int SPORT_TYPE_BADMINTON = 7;
    public static final int SPORT_TYPE_BASEBALL = 26;
    public static final int SPORT_TYPE_BASKETBALL = 21;
    public static final int SPORT_TYPE_CARDIO = 34;
    public static final int SPORT_TYPE_CLIMB = 6;
    public static final int SPORT_TYPE_CROSS_TRAIN = 33;
    public static final int SPORT_TYPE_CYCLING = 3;
    public static final int SPORT_TYPE_DANCING = 29;
    public static final int SPORT_TYPE_DUMBBELLS = 15;
    public static final int SPORT_TYPE_DYNAMIC = 10;
    public static final int SPORT_TYPE_ELLIPOSID = 11;
    public static final int SPORT_TYPE_FITNESS = 9;
    public static final int SPORT_TYPE_GOLF = 25;
    public static final int SPORT_TYPE_LIFTING = 16;
    public static final int SPORT_TYPE_NULL = 0;
    public static final int SPORT_TYPE_ONFOOT = 4;
    public static final int SPORT_TYPE_OTHER = 8;
    public static final int SPORT_TYPE_PILATES = 32;
    public static final int SPORT_TYPE_PINGPONG = 20;
    public static final int SPORT_TYPE_PLANK = 37;
    public static final int SPORT_TYPE_PUSHUP = 14;
    public static final int SPORT_TYPE_ROLLER = 28;
    public static final int SPORT_TYPE_ROLLER_MACHINE = 31;
    public static final int SPORT_TYPE_ROPE = 19;
    public static final int SPORT_TYPE_RUN = 2;
    public static final int SPORT_TYPE_SIT_UP = 13;
    public static final int SPORT_TYPE_SKI = 27;
    public static final int SPORT_TYPE_SOCKER = 22;
    public static final int SPORT_TYPE_SQUARE_DANCE = 36;
    public static final int SPORT_TYPE_SWIM = 5;
    public static final int SPORT_TYPE_TENNISBALL = 24;
    public static final int SPORT_TYPE_TREADMILL = 12;
    public static final int SPORT_TYPE_VOLLEYBALL = 23;
    public static final int SPORT_TYPE_WALK = 1;
    public static final int SPORT_TYPE_YOGA = 18;
    public static final int SPORT_TYPE_ZUMBA = 35;
}
