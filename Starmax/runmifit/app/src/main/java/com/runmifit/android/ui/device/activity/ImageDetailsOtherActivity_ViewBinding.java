package com.wade.fit.ui.device.activity;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.device.activity.ImageDetailsOtherActivity_ViewBinding */
public class ImageDetailsOtherActivity_ViewBinding implements Unbinder {
    private ImageDetailsOtherActivity target;
    private View view2131297085;

    public ImageDetailsOtherActivity_ViewBinding(ImageDetailsOtherActivity imageDetailsOtherActivity) {
        this(imageDetailsOtherActivity, imageDetailsOtherActivity.getWindow().getDecorView());
    }

    public ImageDetailsOtherActivity_ViewBinding(final ImageDetailsOtherActivity imageDetailsOtherActivity, View view) {
        this.target = imageDetailsOtherActivity;
        imageDetailsOtherActivity.viewPager = (ViewPager) Utils.findRequiredViewAsType(view, R.id.view_pager, "field 'viewPager'", ViewPager.class);
        imageDetailsOtherActivity.pageText = (TextView) Utils.findRequiredViewAsType(view, R.id.page_text, "field 'pageText'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.tv_cancel, "field 'tv_cancel' and method 'onViewClicked'");
        imageDetailsOtherActivity.tv_cancel = (TextView) Utils.castView(findRequiredView, R.id.tv_cancel, "field 'tv_cancel'", TextView.class);
        this.view2131297085 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.ImageDetailsOtherActivity_ViewBinding.C24701 */

            public void doClick(View view) {
                imageDetailsOtherActivity.onViewClicked();
            }
        });
    }

    public void unbind() {
        ImageDetailsOtherActivity imageDetailsOtherActivity = this.target;
        if (imageDetailsOtherActivity != null) {
            this.target = null;
            imageDetailsOtherActivity.viewPager = null;
            imageDetailsOtherActivity.pageText = null;
            imageDetailsOtherActivity.tv_cancel = null;
            this.view2131297085.setOnClickListener(null);
            this.view2131297085 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
