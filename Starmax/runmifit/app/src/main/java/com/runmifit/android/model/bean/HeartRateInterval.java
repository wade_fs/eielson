package com.wade.fit.model.bean;

public class HeartRateInterval {
    private static final int DEFAULT_MAX_VALUE = 220;
    private static final int DEFAULT_REGION_VALUE = 225;
    public int aerobicThreshold = 165;
    public int burnFatThreshold = 100;
    public int customWarinValue = DEFAULT_MAX_VALUE;
    public boolean isCustomHr = false;
    public boolean isCustomWarin = false;
    public int limintThreshold = 100;
    public int maxHr = DEFAULT_MAX_VALUE;
    public int minHr = 100;
    public int userMaxHR;

    public int getBurnFatThreshold() {
        return this.burnFatThreshold;
    }

    public void setBurnFatThreshold(int i) {
        this.burnFatThreshold = i;
    }

    public int getAerobicThreshold() {
        return this.aerobicThreshold;
    }

    public void setAerobicThreshold(int i) {
        this.aerobicThreshold = i;
    }

    public int getLimintThreshold() {
        return this.limintThreshold;
    }

    public void setLimintThreshold(int i) {
        this.limintThreshold = i;
    }

    public void setUserMaxHR(int i) {
        this.userMaxHR = i;
    }
}
