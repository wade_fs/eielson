package com.wade.fit.persenter.device;

import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.bean.Alarm;
import com.wade.fit.model.bean.AlarmBean;
import com.wade.fit.persenter.device.AlarmContract;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ble.BleSdkWrapper;
import java.util.ArrayList;
import java.util.List;

public class AlarmPresenter extends BasePersenter<AlarmContract.View> implements AlarmContract.Presenter {
    public void saveAlarmToDevice(List<Alarm> list) {
        if (BleSdkWrapper.isConnected()) {
            ArrayList arrayList = new ArrayList();
            for (Alarm alarm : list) {
                AlarmBean alarmBean = new AlarmBean();
                alarmBean.hour = alarm.getAlarmHour();
                alarmBean.min = alarm.getAlarmMinute();
                alarmBean.type = alarm.getAlarmType();
                byte[] bArr = new byte[8];
                int i = 0;
                bArr[0] = alarm.getOn_off() ? (byte) 1 : 0;
                while (i < alarm.getWeekRepeat().length) {
                    int i2 = i + 1;
                    bArr[i2] = alarm.getWeekRepeat()[i] ? (byte) 1 : 0;
                    i = i2;
                }
                alarmBean.weeks = bArr;
                arrayList.add(alarmBean);
            }
            BleSdkWrapper.setAlarm(arrayList, null);
            SPHelper.saveAlarms(list);
            return;
        }
        ((AlarmContract.View) this.mView).showMsg(AppApplication.getInstance().getResources().getString(R.string.disConnected));
    }
}
