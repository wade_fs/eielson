package com.wade.fit.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.wade.fit.app.AppApplication;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.service.AssistService;
import com.wade.fit.util.PhoneUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.ble.BaseDataHandler;
import com.wade.fit.util.ble.BleCallbackWrapper;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.ble.DeviceCallbackWrapper;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;

public class AssistService extends Service {
    private static final String ACTION_VOLUME_CHANGED = "android.media.VOLUME_CHANGED_ACTION";
    private Uri SMS_INBOX = Uri.parse("content://sms/inbox");
    private Uri SMS_URI = Uri.parse("content://sms/");
    /* access modifiers changed from: private */
    public Handler callHandler = new Handler();
    Runnable callRun = new Runnable() {
        /* class com.wade.fit.service.AssistService.C24354 */

        public void run() {
            if (AssistService.this.isRemind) {
                LogUtil.dAndSave("发送命令到手环", LogUtil.LOG_PATH);
                if (AssistService.this.contactName == null || TextUtils.isEmpty(AssistService.this.contactName)) {
                    BleSdkWrapper.setMessage(0, AssistService.this.phoneNumber, AssistService.this.phoneNumber, new BleCallbackWrapper() {
                        /* class com.wade.fit.service.AssistService.C24354.C24372 */

                        public void setSuccess() {
                        }
                    });
                } else {
                    BleSdkWrapper.setMessage(0, AssistService.this.contactName, AssistService.this.phoneNumber, new BleCallbackWrapper() {
                        /* class com.wade.fit.service.AssistService.C24354.C24361 */

                        public void setSuccess() {
                        }
                    });
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public String contactName;
    /* access modifiers changed from: private */
    public long exitTime = 0;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public boolean hasFirstReigsterPhone;
    /* access modifiers changed from: private */
    public boolean isCommingPhone = false;
    /* access modifiers changed from: private */
    public boolean isRemind = false;
    /* access modifiers changed from: private */
    public boolean isRingOrVibrate = true;
    long lastDate = -1;
    /* access modifiers changed from: private */
    public MediaPlayer mMediaPlayer;
    /* access modifiers changed from: private */
    public Vibrator mVib;
    /* access modifiers changed from: private */
    public String phoneNumber;
    private PhoneStateListener phoneStateListener;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        /* class com.wade.fit.service.AssistService.C24332 */

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AssistService.ACTION_VOLUME_CHANGED)) {
                boolean unused = AssistService.this.isRingOrVibrate = false;
                try {
                    if (AssistService.this.mMediaPlayer != null && AssistService.this.mMediaPlayer.isPlaying()) {
                        AssistService.this.mMediaPlayer.stop();
                        AssistService.this.mMediaPlayer.release();
                        MediaPlayer unused2 = AssistService.this.mMediaPlayer = null;
                    }
                    if (AssistService.this.mVib != null && AssistService.this.mVib.hasVibrator()) {
                        AssistService.this.mVib.cancel();
                        Vibrator unused3 = AssistService.this.mVib = null;
                    }
                } catch (Exception unused4) {
                }
            }
        }
    };
    private SmsObserver smsObserver;
    private TelephonyManager tpm;
    Runnable vibrateAndMediaRunnable = new Runnable() {
        /* class com.wade.fit.service.AssistService.C24321 */

        public void run() {
            if (AssistService.this.isRingOrVibrate) {
                if (System.currentTimeMillis() - AssistService.this.exitTime >= 10000) {
                    if (AssistService.this.mMediaPlayer != null && AssistService.this.mMediaPlayer.isPlaying()) {
                        AssistService.this.mMediaPlayer.stop();
                        AssistService.this.mMediaPlayer.release();
                        MediaPlayer unused = AssistService.this.mMediaPlayer = null;
                    }
                    if (AssistService.this.mVib != null) {
                        AssistService.this.mVib.cancel();
                        Vibrator unused2 = AssistService.this.mVib = null;
                    }
                }
                AssistService.this.handler.postDelayed(this, 1000);
                return;
            }
            AssistService.this.handler.removeCallbacks(this);
        }
    };

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        LogUtil.d("onCreate ");
        BaseDataHandler.getInstance().addDeviceCallback(new DeviceCallbackWrapper() {
            /* class com.wade.fit.service.AssistService.C24343 */

            public void findPhone() {
                super.findPhone();
                LogUtil.d("收到手环命令--开始寻找手机设置成功");
                boolean unused = AssistService.this.isRingOrVibrate = true;
                AssistService.this.playRingtone(true);
            }

            public void answerRingingCall() {
                super.answerRingingCall();
                DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
                if (deviceConfig != null && deviceConfig.isDisturbMode) {
                    PhoneUtil.answerRingingCall(AssistService.this.getApplicationContext());
                }
            }

            public void endRingingCall() {
                super.endRingingCall();
                DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
                if (deviceConfig != null && deviceConfig.isDisturbMode) {
                    PhoneUtil.endCall(AssistService.this.getApplicationContext());
                }
            }
        });
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        boolean z = false;
        boolean z2 = (ContextCompat.checkSelfPermission(this, "android.permission.READ_PHONE_STATE") == 0 && ContextCompat.checkSelfPermission(this, "android.permission.READ_CONTACTS") == 0 && ContextCompat.checkSelfPermission(this, "android.permission.CALL_PHONE") == 0) ? false : true;
        if (ContextCompat.checkSelfPermission(this, "android.permission.READ_SMS") != 0) {
            z = true;
        }
        LogUtil.d("phonePermissions:" + z2);
        LogUtil.d("smsPermissions:" + z);
        if (!z2) {
            this.tpm = (TelephonyManager) getSystemService("phone");
            this.phoneStateListener = new MyPhoneStateListener();
            registerPhoneListener();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ACTION_VOLUME_CHANGED);
            intentFilter.addAction("android.intent.action.HEADSET_PLUG");
            intentFilter.addAction("android.intent.action.MEDIA_BUTTON");
            registerReceiver(this.receiver, intentFilter);
        }
        if (!z) {
            this.smsObserver = new SmsObserver(this.handler);
            try {
                getContentResolver().registerContentObserver(this.SMS_URI, true, this.smsObserver);
            } catch (Exception unused) {
            }
        }
        return super.onStartCommand(intent, i, i2);
    }

    /* access modifiers changed from: private */
    public void playRingtone(boolean z) {
        boolean z2;
        this.handler.removeCallbacks(this.vibrateAndMediaRunnable);
        if (this.mMediaPlayer == null) {
            this.mMediaPlayer = new MediaPlayer();
        }
        if (this.mVib == null && z) {
            this.mVib = (Vibrator) getSystemService("vibrator");
        }
        try {
            z2 = this.mMediaPlayer.isPlaying();
        } catch (Exception unused) {
            this.mMediaPlayer = null;
            this.mMediaPlayer = new MediaPlayer();
            z2 = false;
        }
        if (z2) {
            this.mMediaPlayer.stop();
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
            this.mMediaPlayer = new MediaPlayer();
        }
        try {
            this.mMediaPlayer.setDataSource(this, getSystemDefultRingtoneUri());
            this.mMediaPlayer.setLooping(true);
            this.mMediaPlayer.prepare();
            this.exitTime = System.currentTimeMillis();
            this.mMediaPlayer.start();
            this.handler.postDelayed(this.vibrateAndMediaRunnable, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (z) {
            this.mVib.vibrate(new long[]{500, 2000}, 0);
        }
    }

    private Uri getSystemDefultRingtoneUri() {
        return RingtoneManager.getActualDefaultRingtoneUri(this, 1);
    }

    private void registerPhoneListener() {
        this.tpm.listen(this.phoneStateListener, 32);
    }

    private void unregisterPhoneListener() {
        PhoneStateListener phoneStateListener2;
        TelephonyManager telephonyManager = this.tpm;
        if (telephonyManager != null && (phoneStateListener2 = this.phoneStateListener) != null) {
            telephonyManager.listen(phoneStateListener2, 0);
        }
    }

    /* access modifiers changed from: private */
    public void sendData(String str, String str2) {
        LogUtil.dAndSave("sendData   phoneNumber: " + str + " ---contactName:" + str2, LogUtil.LOG_PATH);
        this.contactName = str2;
        this.callHandler.postDelayed(this.callRun, 1000);
    }

    /* access modifiers changed from: private */
    public void sendMessage() {
        Cursor query;
        LogUtil.d("sendMessage");
        ContentResolver contentResolver = getContentResolver();
        String[] strArr = {"body", "address", "person", "read", "date", "_id"};
        if (ContextCompat.checkSelfPermission(AppApplication.getInstance(), "android.permission.READ_SMS") == 0 && (query = contentResolver.query(this.SMS_INBOX, strArr, "read=? and type=?", new String[]{"0", "1"}, "date desc limit 1")) != null) {
            boolean z = false;
            String str = "138";
            long j = 0;
            String str2 = "0438";
            while (query.moveToNext()) {
                str = query.getString(query.getColumnIndex("address"));
                str2 = query.getString(query.getColumnIndex("body"));
                j = query.getLong(query.getColumnIndex("date"));
                LogUtil.d("date:" + j);
                LogUtil.d("lastDate:" + this.lastDate);
                LogUtil.d("body:" + str2);
                z = true;
            }
            query.close();
            if (!z) {
                LogUtil.d("没有未读短信.....");
            } else if (this.lastDate != j) {
                this.lastDate = j;
                long currentTimeMillis = System.currentTimeMillis();
                LogUtil.d("date:" + j);
                LogUtil.d("currDate:" + currentTimeMillis);
                LogUtil.d("ddd:" + ((currentTimeMillis - j) / 1000));
                String contactNameFromPhoneBook = getContactNameFromPhoneBook(this, str);
                if (contactNameFromPhoneBook.equals("")) {
                    contactNameFromPhoneBook = str;
                }
                LogUtil.d("phoneNumber:" + str + ",contact:" + contactNameFromPhoneBook + ",body:" + str2);
                BleSdkWrapper.setMessage(1, contactNameFromPhoneBook, str2, null);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        LogUtil.d("onDestroy......");
        unregisterPhoneListener();
        if (this.smsObserver != null) {
            getContentResolver().unregisterContentObserver(this.smsObserver);
        }
    }

    private class MyPhoneStateListener extends PhoneStateListener {
        private MyPhoneStateListener() {
        }

        public void onCallForwardingIndicatorChanged(boolean z) {
            super.onCallForwardingIndicatorChanged(z);
            LogUtil.d("onCallForwardingIndicatorChanged");
        }

        public void onCallStateChanged(int i, String str) {
            super.onCallStateChanged(i, str);
            LogUtil.d("TelephonyManager   state = " + i + " ---incomingNumber" + str);
            String unused = AssistService.this.phoneNumber = str;
            AssistService assistService = AssistService.this;
            String unused2 = assistService.contactName = AssistService.getContactNameFromPhoneBook(assistService, assistService.phoneNumber);
            if (!AssistService.this.hasFirstReigsterPhone) {
                boolean unused3 = AssistService.this.hasFirstReigsterPhone = true;
                return;
            }
            DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
            if (deviceConfig != null && deviceConfig.isDisturbMode) {
                LogUtil.d("protocolUtils.getNotice().getCallonOff():" + deviceConfig.isCall);
                if (deviceConfig.isCall) {
                    if (i == 0) {
                        LogUtil.d("空闲");
                        if (AssistService.this.isCommingPhone) {
                            boolean unused4 = AssistService.this.isRemind = false;
                        }
                        boolean unused5 = AssistService.this.isRemind = false;
                        AssistService.this.callHandler.removeCallbacks(AssistService.this.callRun);
                        AssistService.this.callHandler.removeCallbacksAndMessages(null);
                    } else if (i == 1) {
                        LogUtil.dAndSave("来电", LogUtil.LOG_PATH);
                        boolean unused6 = AssistService.this.isCommingPhone = true;
                        boolean unused7 = AssistService.this.isRemind = true;
                        AssistService assistService2 = AssistService.this;
                        assistService2.sendData(str, assistService2.contactName);
                    } else if (i == 2) {
                        LogUtil.d("摘机");
                        AssistService.this.callHandler.removeCallbacks(AssistService.this.callRun);
                        AssistService.this.callHandler.removeCallbacksAndMessages(null);
                        boolean unused8 = AssistService.this.isRemind = false;
                    }
                }
            }
        }
    }

    class SmsObserver extends ContentObserver {
        SmsObserver(Handler handler) {
            super(handler);
        }

        public void onChange(boolean z) {
            LogUtil.d("onChange..............selfChange:" + z);
            DeviceConfig deviceConfig = SPHelper.getDeviceConfig();
            if (deviceConfig != null && deviceConfig.isDisturbMode && deviceConfig.isMessage) {
                AssistService.this.handler.postDelayed(new Runnable() {
                    /* class com.wade.fit.service.$$Lambda$AssistService$SmsObserver$3Yz1VYA6bWmtdJnpeO68f6KuALM */

                    public final void run() {
                        AssistService.SmsObserver.this.lambda$onChange$0$AssistService$SmsObserver();
                    }
                }, 3000);
            }
        }

        public /* synthetic */ void lambda$onChange$0$AssistService$SmsObserver() {
            try {
                AssistService.this.sendMessage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public boolean deliverSelfNotifications() {
            return super.deliverSelfNotifications();
        }
    }

    public static String getContactNameFromPhoneBook(Context context, String str) {
        Cursor query;
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        boolean z = true;
        boolean z2 = ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") != 0;
        DebugLog.m6203d("isPermissions:" + z2);
        if (ContextCompat.checkSelfPermission(context, "android.permission.READ_CONTACTS") == 0) {
            z = false;
        }
        DebugLog.m6203d("isPermissions:" + z2);
        if (z2 || z || (query = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"display_name", "number"}, null, null, null)) == null || !query.moveToFirst()) {
            return "";
        }
        String string = query.getString(query.getColumnIndex("display_name"));
        query.close();
        return string;
    }
}
