package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthSleep;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class HealthSleepDao extends AbstractDao<HealthSleep, Void> {
    public static final String TABLENAME = "HEALTH_SLEEP";

    public static class Properties {
        public static final Property AwakeCount = new Property(12, Integer.TYPE, "awakeCount", false, "AWAKE_COUNT");
        public static final Property Date = new Property(18, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(4, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");
        public static final Property DeepSleepCount = new Property(11, Integer.TYPE, "deepSleepCount", false, "DEEP_SLEEP_COUNT");
        public static final Property DeepSleepMinutes = new Property(14, Integer.TYPE, "deepSleepMinutes", false, "DEEP_SLEEP_MINUTES");
        public static final Property EyeMinutes = new Property(17, Integer.TYPE, "eyeMinutes", false, "EYE_MINUTES");
        public static final Property IsUploaded = new Property(0, Boolean.TYPE, "isUploaded", false, "IS_UPLOADED");
        public static final Property LightSleepCount = new Property(10, Integer.TYPE, "lightSleepCount", false, "LIGHT_SLEEP_COUNT");
        public static final Property LightSleepMinutes = new Property(13, Integer.TYPE, "lightSleepMinutes", false, "LIGHT_SLEEP_MINUTES");
        public static final Property MacAddress = new Property(1, String.class, "macAddress", false, "MAC_ADDRESS");
        public static final Property Month = new Property(3, Integer.TYPE, "month", false, "MONTH");
        public static final Property Remark = new Property(20, String.class, "remark", false, "REMARK");
        public static final Property SleepEndedTimeH = new Property(5, Integer.TYPE, "sleepEndedTimeH", false, "SLEEP_ENDED_TIME_H");
        public static final Property SleepEndedTimeM = new Property(6, Integer.TYPE, "sleepEndedTimeM", false, "SLEEP_ENDED_TIME_M");
        public static final Property SleepMinutes = new Property(16, Integer.TYPE, "sleepMinutes", false, "SLEEP_MINUTES");
        public static final Property SleepstartedTimeH = new Property(7, Integer.TYPE, "sleepstartedTimeH", false, "SLEEPSTARTED_TIME_H");
        public static final Property SleepstartedTimeM = new Property(8, Integer.TYPE, "sleepstartedTimeM", false, "SLEEPSTARTED_TIME_M");
        public static final Property TotalSleepMinutes = new Property(9, Integer.TYPE, "totalSleepMinutes", false, "TOTAL_SLEEP_MINUTES");
        public static final Property UserId = new Property(19, String.class, Constants.userId, false, "USER_ID");
        public static final Property WakeMunutes = new Property(15, Integer.TYPE, "wakeMunutes", false, "WAKE_MUNUTES");
        public static final Property Year = new Property(2, Integer.TYPE, "year", false, "YEAR");
    }

    public Void getKey(HealthSleep healthSleep) {
        return null;
    }

    public boolean hasKey(HealthSleep healthSleep) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(HealthSleep healthSleep, long j) {
        return null;
    }

    public HealthSleepDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public HealthSleepDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"HEALTH_SLEEP\" (\"IS_UPLOADED\" INTEGER NOT NULL ,\"MAC_ADDRESS\" TEXT,\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"SLEEP_ENDED_TIME_H\" INTEGER NOT NULL ,\"SLEEP_ENDED_TIME_M\" INTEGER NOT NULL ,\"SLEEPSTARTED_TIME_H\" INTEGER NOT NULL ,\"SLEEPSTARTED_TIME_M\" INTEGER NOT NULL ,\"TOTAL_SLEEP_MINUTES\" INTEGER NOT NULL ,\"LIGHT_SLEEP_COUNT\" INTEGER NOT NULL ,\"DEEP_SLEEP_COUNT\" INTEGER NOT NULL ,\"AWAKE_COUNT\" INTEGER NOT NULL ,\"LIGHT_SLEEP_MINUTES\" INTEGER NOT NULL ,\"DEEP_SLEEP_MINUTES\" INTEGER NOT NULL ,\"WAKE_MUNUTES\" INTEGER NOT NULL ,\"SLEEP_MINUTES\" INTEGER NOT NULL ,\"EYE_MINUTES\" INTEGER NOT NULL ,\"DATE\" INTEGER NOT NULL ,\"USER_ID\" TEXT,\"REMARK\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"HEALTH_SLEEP\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, HealthSleep healthSleep) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, healthSleep.getIsUploaded() ? 1 : 0);
        String macAddress = healthSleep.getMacAddress();
        if (macAddress != null) {
            databaseStatement.bindString(2, macAddress);
        }
        databaseStatement.bindLong(3, (long) healthSleep.getYear());
        databaseStatement.bindLong(4, (long) healthSleep.getMonth());
        databaseStatement.bindLong(5, (long) healthSleep.getDay());
        databaseStatement.bindLong(6, (long) healthSleep.getSleepEndedTimeH());
        databaseStatement.bindLong(7, (long) healthSleep.getSleepEndedTimeM());
        databaseStatement.bindLong(8, (long) healthSleep.getSleepstartedTimeH());
        databaseStatement.bindLong(9, (long) healthSleep.getSleepstartedTimeM());
        databaseStatement.bindLong(10, (long) healthSleep.getTotalSleepMinutes());
        databaseStatement.bindLong(11, (long) healthSleep.getLightSleepCount());
        databaseStatement.bindLong(12, (long) healthSleep.getDeepSleepCount());
        databaseStatement.bindLong(13, (long) healthSleep.getAwakeCount());
        databaseStatement.bindLong(14, (long) healthSleep.getLightSleepMinutes());
        databaseStatement.bindLong(15, (long) healthSleep.getDeepSleepMinutes());
        databaseStatement.bindLong(16, (long) healthSleep.getWakeMunutes());
        databaseStatement.bindLong(17, (long) healthSleep.getSleepMinutes());
        databaseStatement.bindLong(18, (long) healthSleep.getEyeMinutes());
        databaseStatement.bindLong(19, healthSleep.getDate());
        String userId = healthSleep.getUserId();
        if (userId != null) {
            databaseStatement.bindString(20, userId);
        }
        String remark = healthSleep.getRemark();
        if (remark != null) {
            databaseStatement.bindString(21, remark);
        }
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, HealthSleep healthSleep) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, healthSleep.getIsUploaded() ? 1 : 0);
        String macAddress = healthSleep.getMacAddress();
        if (macAddress != null) {
            sQLiteStatement.bindString(2, macAddress);
        }
        sQLiteStatement.bindLong(3, (long) healthSleep.getYear());
        sQLiteStatement.bindLong(4, (long) healthSleep.getMonth());
        sQLiteStatement.bindLong(5, (long) healthSleep.getDay());
        sQLiteStatement.bindLong(6, (long) healthSleep.getSleepEndedTimeH());
        sQLiteStatement.bindLong(7, (long) healthSleep.getSleepEndedTimeM());
        sQLiteStatement.bindLong(8, (long) healthSleep.getSleepstartedTimeH());
        sQLiteStatement.bindLong(9, (long) healthSleep.getSleepstartedTimeM());
        sQLiteStatement.bindLong(10, (long) healthSleep.getTotalSleepMinutes());
        sQLiteStatement.bindLong(11, (long) healthSleep.getLightSleepCount());
        sQLiteStatement.bindLong(12, (long) healthSleep.getDeepSleepCount());
        sQLiteStatement.bindLong(13, (long) healthSleep.getAwakeCount());
        sQLiteStatement.bindLong(14, (long) healthSleep.getLightSleepMinutes());
        sQLiteStatement.bindLong(15, (long) healthSleep.getDeepSleepMinutes());
        sQLiteStatement.bindLong(16, (long) healthSleep.getWakeMunutes());
        sQLiteStatement.bindLong(17, (long) healthSleep.getSleepMinutes());
        sQLiteStatement.bindLong(18, (long) healthSleep.getEyeMinutes());
        sQLiteStatement.bindLong(19, healthSleep.getDate());
        String userId = healthSleep.getUserId();
        if (userId != null) {
            sQLiteStatement.bindString(20, userId);
        }
        String remark = healthSleep.getRemark();
        if (remark != null) {
            sQLiteStatement.bindString(21, remark);
        }
    }

    public HealthSleep readEntity(Cursor cursor, int i) {
        Cursor cursor2 = cursor;
        boolean z = cursor2.getShort(i + 0) != 0;
        int i2 = i + 1;
        String string = cursor2.isNull(i2) ? null : cursor2.getString(i2);
        int i3 = cursor2.getInt(i + 2);
        int i4 = cursor2.getInt(i + 3);
        int i5 = cursor2.getInt(i + 4);
        int i6 = cursor2.getInt(i + 5);
        int i7 = cursor2.getInt(i + 6);
        int i8 = cursor2.getInt(i + 7);
        int i9 = cursor2.getInt(i + 8);
        int i10 = cursor2.getInt(i + 9);
        int i11 = cursor2.getInt(i + 10);
        int i12 = cursor2.getInt(i + 11);
        int i13 = cursor2.getInt(i + 12);
        int i14 = cursor2.getInt(i + 13);
        int i15 = cursor2.getInt(i + 14);
        int i16 = cursor2.getInt(i + 15);
        int i17 = cursor2.getInt(i + 16);
        int i18 = cursor2.getInt(i + 17);
        long j = cursor2.getLong(i + 18);
        int i19 = i + 19;
        String string2 = cursor2.isNull(i19) ? null : cursor2.getString(i19);
        int i20 = i + 20;
        return new HealthSleep(z, string, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18, j, string2, cursor2.isNull(i20) ? null : cursor2.getString(i20));
    }

    public void readEntity(Cursor cursor, HealthSleep healthSleep, int i) {
        healthSleep.setIsUploaded(cursor.getShort(i + 0) != 0);
        int i2 = i + 1;
        String str = null;
        healthSleep.setMacAddress(cursor.isNull(i2) ? null : cursor.getString(i2));
        healthSleep.setYear(cursor.getInt(i + 2));
        healthSleep.setMonth(cursor.getInt(i + 3));
        healthSleep.setDay(cursor.getInt(i + 4));
        healthSleep.setSleepEndedTimeH(cursor.getInt(i + 5));
        healthSleep.setSleepEndedTimeM(cursor.getInt(i + 6));
        healthSleep.setSleepstartedTimeH(cursor.getInt(i + 7));
        healthSleep.setSleepstartedTimeM(cursor.getInt(i + 8));
        healthSleep.setTotalSleepMinutes(cursor.getInt(i + 9));
        healthSleep.setLightSleepCount(cursor.getInt(i + 10));
        healthSleep.setDeepSleepCount(cursor.getInt(i + 11));
        healthSleep.setAwakeCount(cursor.getInt(i + 12));
        healthSleep.setLightSleepMinutes(cursor.getInt(i + 13));
        healthSleep.setDeepSleepMinutes(cursor.getInt(i + 14));
        healthSleep.setWakeMunutes(cursor.getInt(i + 15));
        healthSleep.setSleepMinutes(cursor.getInt(i + 16));
        healthSleep.setEyeMinutes(cursor.getInt(i + 17));
        healthSleep.setDate(cursor.getLong(i + 18));
        int i3 = i + 19;
        healthSleep.setUserId(cursor.isNull(i3) ? null : cursor.getString(i3));
        int i4 = i + 20;
        if (!cursor.isNull(i4)) {
            str = cursor.getString(i4);
        }
        healthSleep.setRemark(str);
    }
}
