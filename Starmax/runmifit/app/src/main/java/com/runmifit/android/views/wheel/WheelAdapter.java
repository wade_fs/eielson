package com.wade.fit.views.wheel;

public interface WheelAdapter {
    String getItem(int i);

    int getItemsCount();

    int getMaximumLength();
}
