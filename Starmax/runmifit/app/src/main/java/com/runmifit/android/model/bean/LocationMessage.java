package com.wade.fit.model.bean;

public class LocationMessage extends BaseMessage<LatLngBean> {
    public float accurac;
    public String city;
    public String country;
    public int gpsAccuracyStatus;
    public boolean isValid = true;
    public String speed;
    public int totalDistance;

    public LocationMessage(int i, LatLngBean latLngBean) {
        super(i, latLngBean);
    }

    public LocationMessage(int i) {
        super(i);
    }

    public String toString() {
        return "LocationMessage{city='" + this.city + '\'' + ", accurac=" + this.accurac + ", speed='" + this.speed + '\'' + ", country='" + this.country + '\'' + ", totalDistance='" + this.totalDistance + '\'' + ", data='" + getData() + '\'' + ", gpsAccuracyStatus=" + this.gpsAccuracyStatus + '}';
    }
}
