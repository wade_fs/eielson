package com.wade.fit.model.bean;

import java.util.ArrayList;
import java.util.List;

public class SleepTimeBean {
    public List<SleepTime> sleepTimes = new ArrayList();
    public int state;
    public boolean[] weeks = {true, true, true, true, true, true, true};

    public String toString() {
        return "SleepTimeBean{sleepTimes=" + this.sleepTimes + ", state=" + this.state + '}';
    }
}
