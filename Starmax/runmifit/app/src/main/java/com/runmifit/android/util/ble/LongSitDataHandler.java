package com.wade.fit.util.ble;

import com.wade.fit.model.bean.LongSit;
import com.wade.fit.util.SPHelper;

public class LongSitDataHandler {
    public void handlerLongSitData(byte[] bArr) {
        LongSit longSit = new LongSit();
        if (bArr[3] == 1) {
            longSit.setOnOff(true);
        } else {
            longSit.setOnOff(false);
        }
        longSit.setStartHour(ByteDataConvertUtil.Byte2Int(bArr[4]));
        longSit.setEndHour(ByteDataConvertUtil.Byte2Int(bArr[5]));
        boolean[] zArr = new boolean[7];
        byte[] Int2Bit8 = ByteDataConvertUtil.Int2Bit8(ByteDataConvertUtil.Byte2Int(bArr[6]));
        for (int i = 1; i < Int2Bit8.length; i++) {
            if (Int2Bit8[i] == 0) {
                zArr[i - 1] = false;
            } else {
                zArr[i - 1] = true;
            }
        }
        longSit.setWeeks(zArr);
        longSit.setInterval(ByteDataConvertUtil.Byte2Int(bArr[7]) * 5);
        SPHelper.saveLongSit(longSit);
    }
}
