package com.wade.fit.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.baidu.mobstat.Config;
import com.wade.fit.greendao.bean.EcgRecordInfo;
import com.tencent.open.SocialConstants;
import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.internal.DaoConfig;

public class EcgRecordInfoDao extends AbstractDao<EcgRecordInfo, Void> {
    public static final String TABLENAME = "ECG_RECORD_INFO";

    public static class Properties {
        public static final Property AvgHeart = new Property(4, Integer.TYPE, "avgHeart", false, "AVG_HEART");
        public static final Property Date = new Property(0, Long.TYPE, "date", false, "DATE");
        public static final Property Day = new Property(3, Integer.TYPE, Config.TRACE_VISIT_RECENT_DAY, false, "DAY");
        public static final Property Desc = new Property(6, String.class, SocialConstants.PARAM_APP_DESC, false, "DESC");
        public static final Property Month = new Property(2, Integer.TYPE, "month", false, "MONTH");
        public static final Property State = new Property(5, Integer.TYPE, "state", false, "STATE");
        public static final Property Year = new Property(1, Integer.TYPE, "year", false, "YEAR");
    }

    public Void getKey(EcgRecordInfo ecgRecordInfo) {
        return null;
    }

    public boolean hasKey(EcgRecordInfo ecgRecordInfo) {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean isEntityUpdateable() {
        return true;
    }

    public Void readKey(Cursor cursor, int i) {
        return null;
    }

    /* access modifiers changed from: protected */
    public final Void updateKeyAfterInsert(EcgRecordInfo ecgRecordInfo, long j) {
        return null;
    }

    public EcgRecordInfoDao(DaoConfig daoConfig) {
        super(daoConfig);
    }

    public EcgRecordInfoDao(DaoConfig daoConfig, DaoSession daoSession) {
        super(daoConfig, daoSession);
    }

    public static void createTable(Database database, boolean z) {
        String str = z ? "IF NOT EXISTS " : "";
        database.execSQL("CREATE TABLE " + str + "\"ECG_RECORD_INFO\" (\"DATE\" INTEGER NOT NULL ,\"YEAR\" INTEGER NOT NULL ,\"MONTH\" INTEGER NOT NULL ,\"DAY\" INTEGER NOT NULL ,\"AVG_HEART\" INTEGER NOT NULL ,\"STATE\" INTEGER NOT NULL ,\"DESC\" TEXT);");
    }

    public static void dropTable(Database database, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ");
        sb.append(z ? "IF EXISTS " : "");
        sb.append("\"ECG_RECORD_INFO\"");
        database.execSQL(sb.toString());
    }

    /* access modifiers changed from: protected */
    public final void bindValues(DatabaseStatement databaseStatement, EcgRecordInfo ecgRecordInfo) {
        databaseStatement.clearBindings();
        databaseStatement.bindLong(1, ecgRecordInfo.getDate());
        databaseStatement.bindLong(2, (long) ecgRecordInfo.getYear());
        databaseStatement.bindLong(3, (long) ecgRecordInfo.getMonth());
        databaseStatement.bindLong(4, (long) ecgRecordInfo.getDay());
        databaseStatement.bindLong(5, (long) ecgRecordInfo.getAvgHeart());
        databaseStatement.bindLong(6, (long) ecgRecordInfo.getState());
        String desc = ecgRecordInfo.getDesc();
        if (desc != null) {
            databaseStatement.bindString(7, desc);
        }
    }

    /* access modifiers changed from: protected */
    public final void bindValues(SQLiteStatement sQLiteStatement, EcgRecordInfo ecgRecordInfo) {
        sQLiteStatement.clearBindings();
        sQLiteStatement.bindLong(1, ecgRecordInfo.getDate());
        sQLiteStatement.bindLong(2, (long) ecgRecordInfo.getYear());
        sQLiteStatement.bindLong(3, (long) ecgRecordInfo.getMonth());
        sQLiteStatement.bindLong(4, (long) ecgRecordInfo.getDay());
        sQLiteStatement.bindLong(5, (long) ecgRecordInfo.getAvgHeart());
        sQLiteStatement.bindLong(6, (long) ecgRecordInfo.getState());
        String desc = ecgRecordInfo.getDesc();
        if (desc != null) {
            sQLiteStatement.bindString(7, desc);
        }
    }

    public EcgRecordInfo readEntity(Cursor cursor, int i) {
        long j = cursor.getLong(i + 0);
        int i2 = cursor.getInt(i + 1);
        int i3 = cursor.getInt(i + 2);
        int i4 = cursor.getInt(i + 3);
        int i5 = cursor.getInt(i + 4);
        int i6 = cursor.getInt(i + 5);
        int i7 = i + 6;
        return new EcgRecordInfo(j, i2, i3, i4, i5, i6, cursor.isNull(i7) ? null : cursor.getString(i7));
    }

    public void readEntity(Cursor cursor, EcgRecordInfo ecgRecordInfo, int i) {
        ecgRecordInfo.setDate(cursor.getLong(i + 0));
        ecgRecordInfo.setYear(cursor.getInt(i + 1));
        ecgRecordInfo.setMonth(cursor.getInt(i + 2));
        ecgRecordInfo.setDay(cursor.getInt(i + 3));
        ecgRecordInfo.setAvgHeart(cursor.getInt(i + 4));
        ecgRecordInfo.setState(cursor.getInt(i + 5));
        int i2 = i + 6;
        ecgRecordInfo.setDesc(cursor.isNull(i2) ? null : cursor.getString(i2));
    }
}
