package com.wade.fit.views.camera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import com.wade.fit.util.AppUtil;
import com.wade.fit.util.image.BitmapUtil;
import com.wade.fit.util.log.DebugLog;
import java.io.ByteArrayOutputStream;

public class ImageUtil {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap getRotateBitmap(Bitmap bitmap, float f) {
        Matrix matrix = new Matrix();
        matrix.postRotate(f);
        int appMaxMemory = AppUtil.getAppMaxMemory();
        int bitmapSize = BitmapUtil.getBitmapSize(bitmap);
        int i = appMaxMemory / 3;
        DebugLog.m6203d("appMaxMemory:" + appMaxMemory + ",bitmapSize:" + bitmapSize + ",temp:" + i);
        if (i < bitmapSize) {
            bitmap = zoomImage(bitmap, bitmapSize / 2);
        }
        Bitmap bitmap2 = bitmap;
        DebugLog.m6203d("压缩后的bitmap:" + BitmapUtil.getBitmapSize(bitmap2));
        return Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.getWidth(), bitmap2.getHeight(), matrix, false);
    }

    public static Bitmap getThumbnail(String str, int i, int i2) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        options.inSampleSize = caculateInSampleSize(options, i, i2);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(str, options);
    }

    private static int caculateInSampleSize(BitmapFactory.Options options, int i, int i2) {
        int i3 = options.outWidth;
        int i4 = options.outHeight;
        if (i3 > i || i4 > i2) {
            return Math.max(Math.round((((float) i3) * 1.0f) / ((float) i)), Math.round((((float) i4) * 1.0f) / ((float) i2)));
        }
        return 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap zoomImage(Bitmap bitmap, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, byteArrayOutputStream);
        int i2 = i * 1024;
        float sqrt = (float) Math.sqrt((double) (((float) i2) / ((float) byteArrayOutputStream.toByteArray().length)));
        Matrix matrix = new Matrix();
        matrix.setScale(sqrt, sqrt);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        byteArrayOutputStream.reset();
        createBitmap.compress(Bitmap.CompressFormat.JPEG, 85, byteArrayOutputStream);
        Bitmap bitmap2 = createBitmap;
        while (byteArrayOutputStream.toByteArray().length > i2) {
            DebugLog.m6203d("bitmap:" + (byteArrayOutputStream.toByteArray().length / 1024) + "M");
            matrix.setScale(0.9f, 0.9f);
            bitmap2 = Bitmap.createBitmap(bitmap2, 0, 0, bitmap2.getWidth(), bitmap2.getHeight(), matrix, true);
            byteArrayOutputStream.reset();
            bitmap2.compress(Bitmap.CompressFormat.JPEG, 85, byteArrayOutputStream);
        }
        return bitmap2;
    }
}
