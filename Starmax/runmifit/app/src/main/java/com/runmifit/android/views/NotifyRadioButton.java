package com.wade.fit.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;
import android.util.TypedValue;
import com.wade.fit.R;

public class NotifyRadioButton extends AppCompatRadioButton {
    private Context context;
    boolean notify;
    Paint paint = new Paint(1);
    float radius;

    public NotifyRadioButton(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.radius = TypedValue.applyDimension(0, 10.0f, context2.getResources().getDisplayMetrics());
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.notify) {
            this.paint.setColor(this.context.getResources().getColor(R.color.theme_tittle_bg));
            Rect bounds = getCompoundDrawables()[1].getBounds();
            canvas.drawCircle((float) ((getMeasuredWidth() / 2) + (bounds.width() / 2)), (float) (getPaddingTop() + (bounds.height() / 5)), this.radius, this.paint);
            return;
        }
        this.paint.setColor(0);
        canvas.drawLine(0.0f, 0.0f, 0.0f, 0.0f, this.paint);
    }

    public void notify(boolean z) {
        this.notify = z;
        invalidate();
    }
}
