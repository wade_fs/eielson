package com.wade.fit.persenter.sport;

import android.os.Handler;
import android.text.format.Time;
import com.google.gson.Gson;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthGpsItem;
import com.wade.fit.model.bean.AppExchangeDataStartPara;
import com.wade.fit.model.bean.LatLngBean;
import com.wade.fit.model.bean.LocationMessage;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.EventBusHelper;
import com.wade.fit.util.SportDataHelper;
import com.wade.fit.util.ThreadUtil;
import com.wade.fit.util.ble.BleSdkWrapper;
import com.wade.fit.util.log.DebugLog;
import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimerTask;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class DeviceSportRunPresenter implements Constants {
    private static final int TIME_OUT_CONNECT = 15000;
    static DeviceSportRunPresenter defaultInstance;
    private final int SPORT_STATE_NONE = 0;
    private final int SPORT_STATE_PAUSE = 3;
    private final int SPORT_STATE_RUNNING = 1;
    private final int SPORT_STATE_STOP = 2;
    private final int UPDATE_DATA_INTEVALE = 1000;
    /* access modifiers changed from: private */
    public long changeIndex;
    /* access modifiers changed from: private */
    public ISportConnectCallBack connectCallBack;
    /* access modifiers changed from: private */
    public LatLngBean currentLatLng;
    private Runnable disConnRun = new Runnable() {
        /* class com.wade.fit.persenter.sport.DeviceSportRunPresenter.C15281 */

        public void run() {
            DebugLog.m6203d("超时了........");
            boolean unused = DeviceSportRunPresenter.this.isAppComplete = true;
            if (DeviceSportRunPresenter.this.connectCallBack != null) {
                DeviceSportRunPresenter.this.connectCallBack.connecetTimeOut();
            }
        }
    };
    private String fileName;
    /* access modifiers changed from: private */
    public int gpsSignValue = 1;
    private Handler handler = new Handler();
    /* access modifiers changed from: private */
    public int heartRate;
    private List<Integer> heartRateList = new ArrayList();
    private ISportStartCallBack iSportStart;
    /* access modifiers changed from: private */
    public boolean isAppComplete;
    protected boolean isCompleteRun = true;
    private boolean isFirstDisConnt = false;
    private boolean isLocation = false;
    protected boolean isRunning = false;
    private boolean isSaveData = true;
    /* access modifiers changed from: private */
    public boolean isSendCmd = true;
    /* access modifiers changed from: private */
    public long lastCurrentTimeMillis = 0;
    public List<LatLngBean> latLngDomainList = new ArrayList();
    private int maxHeartRate = 0;
    RunTimerTask runTimerTask = new RunTimerTask();
    private int sendDistance = 0;
    private LinkedHashMap<Integer, int[]> serialHeartRate = new LinkedHashMap<>();
    private boolean setConTimeOut;
    /* access modifiers changed from: private */
    public ISportRunCallBack sportRunCallBack;
    /* access modifiers changed from: private */
    public int sportState = 0;
    private AppExchangeDataStartPara switchDataAppStart = new AppExchangeDataStartPara();
    private int target_type;
    private int target_value;
    /* access modifiers changed from: private */
    public HealthActivity trainDomain;
    private int type;
    /* access modifiers changed from: private */
    public Handler updateHandler = new Handler();

    public interface ISportConnectCallBack {
        void bleConnect();

        void bleDisconnect();

        void connecetTimeOut();
    }

    public interface ISportRunCallBack {
        void sportPause(boolean z);

        void sportResume(boolean z);

        void sportRunning(HealthActivity healthActivity, int i, LatLngBean latLngBean, int i2);

        void sportStop(boolean z);
    }

    public interface ISportStartCallBack {
        void sportStartFaild();

        void sportStartFaildByLowPower();

        void sportStartSuccess();
    }

    public void savePicImg(String str) {
    }

    static /* synthetic */ long access$1208(DeviceSportRunPresenter deviceSportRunPresenter) {
        long j = deviceSportRunPresenter.changeIndex;
        deviceSportRunPresenter.changeIndex = 1 + j;
        return j;
    }

    public boolean isAppComplete() {
        return this.isAppComplete;
    }

    public boolean isLocation() {
        return this.isLocation;
    }

    public void setLocation(boolean z) {
        this.isLocation = z;
    }

    public boolean isRunning() {
        return this.isRunning;
    }

    public void setRunning(boolean z) {
        this.isRunning = z;
    }

    public boolean isCompleteRun() {
        return this.isCompleteRun;
    }

    public boolean isSetConTimeOut() {
        return this.setConTimeOut;
    }

    public void setSetConTimeOut(boolean z) {
        this.setConTimeOut = z;
    }

    public static DeviceSportRunPresenter getDefault() {
        if (defaultInstance == null) {
            synchronized (DeviceSportRunPresenter.class) {
                if (defaultInstance == null) {
                    defaultInstance = new DeviceSportRunPresenter();
                }
            }
        }
        return defaultInstance;
    }

    public void startRun(int i, int i2, int i3, boolean z, ISportStartCallBack iSportStartCallBack) {
        this.isAppComplete = z;
        this.type = i;
        initSportValue(i, i2, i3);
        initSwithchData(i, i2, i3, 0);
        this.iSportStart = iSportStartCallBack;
        this.maxHeartRate = 0;
        sportStartSuccess();
    }

    public void removeSportStartCallback() {
        this.iSportStart = null;
    }

    private void locationState(boolean z) {
        EventBusHelper.post(z ? 200 : 201);
    }

    public void pauserRun() {
        this.isSendCmd = true;
        if (this.isAppComplete || !BleSdkWrapper.isConnected()) {
            pauseSuccess();
            return;
        }
        LogUtil.d("*************************pauserRun");
        pauseSuccess();
    }

    public void continueOnPauseRun() {
        this.isSendCmd = true;
        if (this.sportState == 3) {
            resumeRun();
        } else {
            pauserRun();
        }
    }

    private void pauseSuccess() {
        this.sportState = 3;
        LogUtil.d("*************************pauseSuccess");
        stopUpdateTimer();
        locationState(false);
        ISportRunCallBack iSportRunCallBack = this.sportRunCallBack;
        if (iSportRunCallBack != null) {
            iSportRunCallBack.sportPause(true);
        }
    }

    private void resumeSuccess(boolean z) {
        this.sportState = 1;
        startUpdateTimer(z);
        locationState(true);
        ISportRunCallBack iSportRunCallBack = this.sportRunCallBack;
        if (iSportRunCallBack != null) {
            iSportRunCallBack.sportResume(true);
        }
    }

    public void resumeRun() {
        this.isSendCmd = true;
        if (this.isAppComplete || !BleSdkWrapper.isConnected()) {
            resumeSuccess(true);
            return;
        }
        resumeSuccess(true);
        BleSdkWrapper.appSwitchRestore(this.switchDataAppStart.day, this.switchDataAppStart.hour, this.switchDataAppStart.minute, this.switchDataAppStart.second);
    }

    public void setSportRunCallBack(ISportRunCallBack iSportRunCallBack) {
        this.sportRunCallBack = iSportRunCallBack;
    }

    public void stopRun(final boolean z) {
        DebugLog.m6203d("结束运动------isAppComplete:" + this.isAppComplete + ",isConnected:" + BleSdkWrapper.isConnected());
        this.isSaveData = z;
        ThreadUtil.runOnMainThread(new Runnable() {
            /* class com.wade.fit.persenter.sport.DeviceSportRunPresenter.C15292 */

            public void run() {
                if (DeviceSportRunPresenter.this.isAppComplete || !BleSdkWrapper.isConnected()) {
                    DeviceSportRunPresenter.this.stopSuccess(z);
                }
            }
        });
    }

    public void endCmd(int i) {
        BleSdkWrapper.appSwitchDataEnd(this.switchDataAppStart.day, this.switchDataAppStart.hour, this.switchDataAppStart.minute, this.switchDataAppStart.second, this.type, this.trainDomain.getDurations(), this.trainDomain.getCalories(), this.trainDomain.getDistance(), i);
    }

    /* access modifiers changed from: private */
    public void stopSuccess(boolean z) {
        LogUtil.d("stopSuccess");
        EventBusHelper.unregister(this);
        this.isCompleteRun = true;
        this.sportState = 2;
        this.isSaveData = z;
        stopUpdateTimer();
        locationState(false);
        saveSportRunData();
        ISportRunCallBack iSportRunCallBack = this.sportRunCallBack;
        if (iSportRunCallBack != null) {
            iSportRunCallBack.sportStop(true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleMessage(LocationMessage locationMessage) {
        if (locationMessage.getType() == 202) {
            locationChange(locationMessage);
        }
    }

    private void saveSportRunData() {
        DebugLog.m6203d("----saveSportRunData---" + this.isSaveData);
        if (this.isSaveData) {
            this.trainDomain.setHr_data_vlaue_json(new Gson().toJson(this.heartRateList));
            DebugLog.m6203d("----经纬度---" + this.latLngDomainList.size());
            ArrayList arrayList = new ArrayList();
            for (LatLngBean latLngBean : this.latLngDomainList) {
                HealthGpsItem healthGpsItem = new HealthGpsItem();
                healthGpsItem.setLatitude(Double.valueOf(latLngBean.latitude));
                healthGpsItem.setLongitude(Double.valueOf(latLngBean.longitude));
                healthGpsItem.setDate(Long.valueOf(this.trainDomain.getDate()));
                healthGpsItem.setStartDate(latLngBean.getCurrentTimeMillis());
                arrayList.add(healthGpsItem);
            }
            this.trainDomain.setDataFrom(1);
        }
    }

    public void close() {
        this.sportRunCallBack = null;
        this.connectCallBack = null;
        unRegisterConnectCallBcak();
        stopUpdateTimer();
    }

    public void forceStartRun() {
        startCmd(1);
    }

    private void initSportValue(int i, int i2, int i3) {
        this.type = i;
        this.target_type = i2;
        this.target_value = i3;
        this.type = i;
        this.sportState = 0;
    }

    public void setConnectCallBack(ISportConnectCallBack iSportConnectCallBack) {
        this.connectCallBack = iSportConnectCallBack;
    }

    public void unRegisterConnectCallBcak() {
        this.handler.removeCallbacks(this.disConnRun);
    }

    public void disConnectRunning() {
        this.isFirstDisConnt = false;
    }

    private void handlerReplay(int i) {
        ISportStartCallBack iSportStartCallBack;
        if (i == 0) {
            sportStartSuccess();
        } else if (i == 1) {
            ISportStartCallBack iSportStartCallBack2 = this.iSportStart;
            if (iSportStartCallBack2 != null) {
                iSportStartCallBack2.sportStartFaild();
            }
        } else if (i == 2 && (iSportStartCallBack = this.iSportStart) != null) {
            iSportStartCallBack.sportStartFaildByLowPower();
        }
    }

    public void sportStart() {
        sportStartSuccess();
    }

    private void sportStartSuccess() {
        DebugLog.m6203d("sportStartSuccess");
        this.sportState = 1;
        this.isCompleteRun = false;
        initTrainDomain();
        EventBusHelper.register(this);
        this.changeIndex = 0;
        this.gpsSignValue = 1;
        this.latLngDomainList.clear();
        this.heartRateList.clear();
        this.serialHeartRate.clear();
        stopUpdateTimer();
        startUpdateTimer(true);
        if (this.type <= 4) {
            locationState(true);
        }
        ISportStartCallBack iSportStartCallBack = this.iSportStart;
        if (iSportStartCallBack != null) {
            iSportStartCallBack.sportStartSuccess();
        }
    }

    private void initTrainDomain() {
        this.trainDomain = new HealthActivity();
        this.trainDomain.setType(this.type);
        this.trainDomain.setDate(DateUtil.getDateByHMS(this.switchDataAppStart.hour, this.switchDataAppStart.minute, this.switchDataAppStart.second).getTime());
        this.trainDomain.setDataFrom(1);
        int[] iArr = DateUtil.todayYearMonthDay();
        this.trainDomain.setYear(iArr[0]);
        this.trainDomain.setMonth(iArr[1]);
        this.trainDomain.setDay(iArr[2]);
        this.trainDomain.setHour(this.switchDataAppStart.hour);
        this.trainDomain.setMinute(this.switchDataAppStart.minute);
        this.trainDomain.setSecond(this.switchDataAppStart.second);
        this.trainDomain.setMacAddress(BleSdkWrapper.getBindMac());
    }

    /* access modifiers changed from: private */
    public void completeCalorie() {
        this.trainDomain.getCalories();
        int weight = AppApplication.getInstance().getUserBean().getWeight();
        double distance = (double) this.trainDomain.getDistance();
        Double.isNaN(distance);
        this.trainDomain.setCalories(SportDataHelper.getSportGpsCarloy(this.type, (float) weight, distance / 1000.0d));
    }

    public void startUpdateTimer(boolean z) {
        this.lastCurrentTimeMillis = System.currentTimeMillis() / 1000;
        this.updateHandler.removeCallbacks(this.runTimerTask);
        if (z) {
            this.updateHandler.postDelayed(this.runTimerTask, 1000);
        } else {
            this.updateHandler.post(this.runTimerTask);
        }
    }

    /* access modifiers changed from: private */
    public void changeCmd() {
        this.trainDomain.getDurations();
        this.trainDomain.getCalories();
    }

    private void stopUpdateTimer() {
        this.updateHandler.removeCallbacks(this.runTimerTask);
    }

    private void startCmd(int i) {
        this.switchDataAppStart.force_start = i;
        DebugLog.m6203d("发送开始命令:" + this.switchDataAppStart.toString());
        BleSdkWrapper.appSwitchDataStart(this.switchDataAppStart);
    }

    public void initSwithchData(int i, int i2, int i3, int i4) {
        Time time = new Time(Time.getCurrentTimezone());
        time.setToNow();
        this.switchDataAppStart.day = time.monthDay;
        this.switchDataAppStart.hour = time.hour;
        this.switchDataAppStart.minute = time.minute;
        this.switchDataAppStart.second = time.second;
        AppExchangeDataStartPara appExchangeDataStartPara = this.switchDataAppStart;
        appExchangeDataStartPara.sportType = i;
        appExchangeDataStartPara.target_type = i2;
        appExchangeDataStartPara.target_value = i3;
        appExchangeDataStartPara.force_start = i4;
    }

    private void locationChange(LocationMessage locationMessage) {
        this.gpsSignValue = locationMessage.gpsAccuracyStatus;
        if (this.isAppComplete || !BleSdkWrapper.isConnected()) {
            this.trainDomain.setDistance(locationMessage.totalDistance);
        } else {
            this.sendDistance = locationMessage.totalDistance;
        }
        this.currentLatLng = (LatLngBean) locationMessage.getData();
        LatLngBean latLngBean = this.currentLatLng;
        if (latLngBean != null) {
            this.latLngDomainList.add(latLngBean);
        }
    }

    public void setSendCmd(boolean z) {
        this.isSendCmd = z;
    }

    private class RunTimerTask extends TimerTask {
        private RunTimerTask() {
        }

        public void run() {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            if (DeviceSportRunPresenter.this.sportState == 1) {
                DeviceSportRunPresenter.this.trainDomain.setDurations(DeviceSportRunPresenter.this.trainDomain.getDurations() + Math.max(1, (int) (currentTimeMillis - DeviceSportRunPresenter.this.lastCurrentTimeMillis)));
                long unused = DeviceSportRunPresenter.this.lastCurrentTimeMillis = currentTimeMillis;
                LogUtil.d("durations():" + DeviceSportRunPresenter.this.trainDomain.getDurations());
                DeviceSportRunPresenter.this.completeCalorie();
                ThreadUtil.runOnMainThread(new Runnable() {
                    /* class com.wade.fit.persenter.sport.DeviceSportRunPresenter.RunTimerTask.C15301 */

                    public void run() {
                        if (DeviceSportRunPresenter.this.sportRunCallBack != null) {
                            DeviceSportRunPresenter.this.sportRunCallBack.sportRunning(DeviceSportRunPresenter.this.trainDomain, DeviceSportRunPresenter.this.heartRate, DeviceSportRunPresenter.this.currentLatLng, DeviceSportRunPresenter.this.gpsSignValue);
                        }
                    }
                });
                DeviceSportRunPresenter.access$1208(DeviceSportRunPresenter.this);
                if (DeviceSportRunPresenter.this.changeIndex % 2 == 0 && !DeviceSportRunPresenter.this.isAppComplete && BleSdkWrapper.isConnected() && DeviceSportRunPresenter.this.isSendCmd) {
                    DeviceSportRunPresenter.this.changeCmd();
                }
                DeviceSportRunPresenter.this.updateHandler.postDelayed(this, 1000);
            }
        }
    }
}
