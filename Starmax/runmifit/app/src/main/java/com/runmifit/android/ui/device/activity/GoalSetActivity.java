package com.wade.fit.ui.device.activity;

import android.view.View;
import com.wade.fit.R;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.persenter.device.GoalSetContract;
import com.wade.fit.persenter.device.GoalSetPersenter;
import com.wade.fit.util.DialogHelperNew;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.UnitUtil;
import com.wade.fit.views.ItemLableValue;
import com.wade.fit.views.dialog.WheelViewDialog;

/* renamed from: com.wade.fit.ui.device.activity.GoalSetActivity */
public class GoalSetActivity extends BaseMvpActivity<GoalSetPersenter> implements Constants, GoalSetContract.View {
    private final int CMD_CAL = 1;
    private final int CMD_DISTANCE = 0;
    private final int CMD_STEP = 2;
    private DeviceConfig config;
    private int hisValueCal;
    private int hisValueDistace;
    private int hisValueStep;
    ItemLableValue ilCal;
    ItemLableValue ilDistance;
    ItemLableValue ilStep;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_goal_set;
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.mine_target_set));
        this.config = SPHelper.getDeviceConfig();
        ItemLableValue itemLableValue = this.ilStep;
        itemLableValue.setValue(this.config.goal.goalStep + " " + getResources().getString(R.string.unit_steps));
        ItemLableValue itemLableValue2 = this.ilDistance;
        itemLableValue2.setValue(this.config.goal.goalDistanceKm + " " + UnitUtil.getUnitStr());
        ItemLableValue itemLableValue3 = this.ilCal;
        itemLableValue3.setValue(this.config.goal.goalCal + " " + getResources().getString(R.string.unit_calorie));
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ilCal) {
            DialogHelperNew.showWheelDialog(this, this.config.goal.goalCal / 10, 10, 1000, 10, new WheelViewDialog.OnSelectClick() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$GoalSetActivity$ql8yQydQ7UsH_Jbj5oPmFUVJAPE */

                public final void onSelect(int i, int i2, int i3) {
                    GoalSetActivity.this.lambda$onClick$1$GoalSetActivity(i, i2, i3);
                }
            });
        } else if (id == R.id.ilDistance) {
            DialogHelperNew.showWheelDialog(this, this.config.goal.goalDistanceKm, 2, 20, new WheelViewDialog.OnSelectClick() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$GoalSetActivity$DlEwUU8D9T4SbyF2TeoKREvHqY4 */

                public final void onSelect(int i, int i2, int i3) {
                    GoalSetActivity.this.lambda$onClick$0$GoalSetActivity(i, i2, i3);
                }
            });
        } else if (id == R.id.ilStep) {
            DialogHelperNew.showWheelDialog(this, this.config.goal.goalStep / 1000, 1, 20000, 1000, new WheelViewDialog.OnSelectClick() {
                /* class com.wade.fit.ui.device.activity.$$Lambda$GoalSetActivity$jGZiwzYjRNzWDHczJPZif8N5i4w */

                public final void onSelect(int i, int i2, int i3) {
                    GoalSetActivity.this.lambda$onClick$2$GoalSetActivity(i, i2, i3);
                }
            });
        }
    }

    public /* synthetic */ void lambda$onClick$0$GoalSetActivity(int i, int i2, int i3) {
        this.hisValueDistace = this.config.goal.goalDistanceKm;
        if (this.hisValueDistace != i) {
            this.config.goal.distancestate = 1;
            this.config.goal.goalDistanceKm = i;
            ItemLableValue itemLableValue = this.ilDistance;
            itemLableValue.setValue(i + " " + UnitUtil.getUnitStr());
            ((GoalSetPersenter) this.mPresenter).saveGold(0, this.config.goal);
        }
    }

    public /* synthetic */ void lambda$onClick$1$GoalSetActivity(int i, int i2, int i3) {
        this.hisValueCal = this.config.goal.goalCal;
        if (this.hisValueCal != i) {
            this.config.goal.calstate = 1;
            this.config.goal.goalCal = i;
            ItemLableValue itemLableValue = this.ilCal;
            itemLableValue.setValue(this.config.goal.goalCal + " " + getResources().getString(R.string.unit_calorie));
            ((GoalSetPersenter) this.mPresenter).saveGold(1, this.config.goal);
        }
    }

    public /* synthetic */ void lambda$onClick$2$GoalSetActivity(int i, int i2, int i3) {
        this.hisValueStep = this.config.goal.goalStep;
        if (this.hisValueStep != i) {
            this.config.goal.goalStep = i;
            this.config.goal.stepstate = 1;
            ItemLableValue itemLableValue = this.ilStep;
            itemLableValue.setValue(this.config.goal.goalStep + " " + getResources().getString(R.string.unit_steps));
            ((GoalSetPersenter) this.mPresenter).saveGold(2, this.config.goal);
        }
    }

    public void saveSuccess(int i) {
        SPHelper.saveDeviceConfig(this.config);
    }

    public void saveFaild(int i) {
        if (i == 0) {
            this.config.goal.goalDistanceKm = this.hisValueDistace;
            ItemLableValue itemLableValue = this.ilDistance;
            itemLableValue.setValue(this.hisValueDistace + " " + UnitUtil.getUnitStr());
        } else if (i == 1) {
            this.config.goal.goalDistanceKm = this.hisValueCal;
            ItemLableValue itemLableValue2 = this.ilCal;
            itemLableValue2.setValue(this.hisValueCal + " " + getResources().getString(R.string.unit_calorie));
        } else if (i == 2) {
            this.config.goal.goalDistanceKm = this.hisValueStep;
            ItemLableValue itemLableValue3 = this.ilCal;
            itemLableValue3.setValue(this.hisValueStep + " " + getResources().getString(R.string.unit_step));
        }
    }
}
