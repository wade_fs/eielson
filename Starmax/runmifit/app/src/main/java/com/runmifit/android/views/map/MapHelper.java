package com.wade.fit.views.map;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.wade.fit.model.bean.LatLngBean;

public class MapHelper {
    public static int getMapSource() {
        return 0;
    }

    public static double getDistance(LatLngBean latLngBean, LatLngBean latLngBean2) {
        return DistanceUtil.getDistance(new LatLng(latLngBean.latitude, latLngBean.longitude), new LatLng(latLngBean2.latitude, latLngBean2.longitude));
    }

    public static void gaoDeToBaidu(LatLngBean latLngBean) {
        double longitude = latLngBean.getLongitude();
        double latitude = latLngBean.getLatitude();
        double sqrt = Math.sqrt((longitude * longitude) + (latitude * latitude)) + (Math.sin(latitude * 52.35987755982988d) * 2.0E-5d);
        double atan2 = Math.atan2(latitude, longitude) + (Math.cos(longitude * 52.35987755982988d) * 3.0E-6d);
        latLngBean.longitude = (Math.cos(atan2) * sqrt) + 0.0065d;
        latLngBean.latitude = (sqrt * Math.sin(atan2)) + 0.006d;
    }
}
