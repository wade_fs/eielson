package com.wade.fit.model.bean;

public class BaseMessage<T> {
    private T data;
    private int type;

    public BaseMessage(int i, T t) {
        this.type = i;
        this.data = t;
    }

    public BaseMessage(int i) {
        this.type = i;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int i) {
        this.type = i;
    }

    public void setData(T t) {
        this.data = t;
    }

    public T getData() {
        return this.data;
    }

    public String toString() {
        return "BaseMessage{type=" + this.type + ", data=" + ((Object) this.data) + '}';
    }
}
