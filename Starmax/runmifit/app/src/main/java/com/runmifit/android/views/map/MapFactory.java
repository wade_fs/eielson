package com.wade.fit.views.map;

import android.app.Activity;
import android.view.View;
import android.view.ViewStub;
import android.widget.ZoomControls;
import com.baidu.mapapi.map.TextureMapView;
import com.wade.fit.R;
import com.wade.fit.base.BaseMap;

public class MapFactory {
    public static BaseMap getMap() {
        MapHelper.getMapSource();
        return new BaiduMap();
    }

    public static View getMapView(Activity activity) {
        if (MapHelper.getMapSource() != 0) {
            return null;
        }
        ((ViewStub) activity.findViewById(R.id.vs_baidu)).inflate();
        View findViewById = activity.findViewById(R.id.mapViewBaidu);
        initBaiduMapSetting((TextureMapView) findViewById);
        return findViewById;
    }

    private static void initBaiduMapSetting(TextureMapView textureMapView) {
        textureMapView.showZoomControls(false);
        textureMapView.getMap().getUiSettings().setRotateGesturesEnabled(false);
        int childCount = textureMapView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = textureMapView.getChildAt(i);
            if (childAt instanceof ZoomControls) {
                childAt.setVisibility(View.INVISIBLE);
            }
        }
    }
}
