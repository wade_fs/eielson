package com.wade.fit.util;

import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import com.google.android.gms.common.util.CrashUtils;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.LibContext;
import java.util.ArrayList;
import java.util.List;

public class PermissionUtil {
    private RequsetResult requsetResult;

    public interface RequsetResult {
        void requestPermissionsFail(int i);

        void requestPermissionsSuccess(int i);
    }

    public static boolean isOverMarshmallow() {
        return Build.VERSION.SDK_INT >= 23;
    }

    public void setRequsetResult(RequsetResult requsetResult2) {
        this.requsetResult = requsetResult2;
    }

    public static List<String> findDeniedPermissions(String... strArr) {
        if (Build.VERSION.SDK_INT < 23) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (ContextCompat.checkSelfPermission(LibContext.getAppContext(), str) != 0) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }

    public static boolean isGrantedDrawOverlays() {
        if (Build.VERSION.SDK_INT < 26) {
            return Settings.canDrawOverlays(AppApplication.getInstance());
        }
        AppOpsManager appOpsManager = (AppOpsManager) AppApplication.getInstance().getSystemService("appops");
        if (appOpsManager == null) {
            return false;
        }
        int checkOpNoThrow = appOpsManager.checkOpNoThrow("android:system_alert_window", Process.myUid(), AppApplication.getInstance().getPackageName());
        if (checkOpNoThrow == 0 || checkOpNoThrow == 1) {
            return true;
        }
        return false;
    }

    public static boolean checkSelfPermission(String... strArr) {
        return findDeniedPermissions(strArr).isEmpty();
    }

    private static void startWriteSettingsActivity(Activity activity, int i) {
        Intent intent = new Intent("android.settings.action.MANAGE_WRITE_SETTINGS");
        intent.setData(Uri.parse("package:" + AppApplication.getInstance().getPackageName()));
        if (!isIntentAvailable(intent)) {
            launchAppDetailsSettings();
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    public static void launchAppDetailsSettings() {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.parse("package:" + AppApplication.getInstance().getPackageName()));
        if (isIntentAvailable(intent)) {
            AppApplication.getInstance().startActivity(intent.addFlags(CrashUtils.ErrorDialogData.BINDER_CRASH));
        }
    }

    private static boolean isIntentAvailable(Intent intent) {
        return AppApplication.getInstance().getPackageManager().queryIntentActivities(intent, 65536).size() > 0;
    }

    public static void requestPermissions(Object obj, int i, String... strArr) {
        if (isOverMarshmallow()) {
            boolean z = obj instanceof Activity;
            if (z) {
                Activity activity = (Activity) obj;
            } else if (obj instanceof Fragment) {
                ((Fragment) obj).getActivity();
            }
            List<String> findDeniedPermissions = findDeniedPermissions(strArr);
            if (findDeniedPermissions.size() <= 0) {
                return;
            }
            if (z) {
                ((Activity) obj).requestPermissions((String[]) findDeniedPermissions.toArray(new String[findDeniedPermissions.size()]), i);
            } else if (obj instanceof Fragment) {
                ((Fragment) obj).requestPermissions((String[]) findDeniedPermissions.toArray(new String[findDeniedPermissions.size()]), i);
            } else {
                throw new IllegalArgumentException(obj.getClass().getName() + " is not supported");
            }
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < iArr.length; i2++) {
            if (iArr[i2] != 0) {
                arrayList.add(strArr[i2]);
            }
        }
        if (arrayList.size() > 0) {
            this.requsetResult.requestPermissionsFail(i);
        } else {
            this.requsetResult.requestPermissionsSuccess(i);
        }
    }
}
