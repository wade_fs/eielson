package com.wade.fit.model.bean;

public class AppNotice {
    public boolean email;
    public boolean facebook;
    public boolean instagram;
    public boolean line;
    public boolean linked;
    public boolean messager;

    /* renamed from: qq */
    public boolean f5221qq;
    public boolean skype;
    public boolean twitter;

    /* renamed from: vk */
    public boolean f5222vk;
    public boolean wechat;
    public boolean whatsApp;

    public String toString() {
        return "AppNotice{qq=" + this.f5221qq + ", facebook=" + this.facebook + ", wechat=" + this.wechat + ", linked=" + this.linked + ", skype=" + this.skype + ", instagram=" + this.instagram + ", twitter=" + this.twitter + ", line=" + this.line + ", whatsApp=" + this.whatsApp + ", vk=" + this.f5222vk + ", messager=" + this.messager + '}';
    }
}
