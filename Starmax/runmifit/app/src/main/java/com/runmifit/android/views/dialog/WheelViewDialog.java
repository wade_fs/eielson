package com.wade.fit.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.views.wheel.NewWheelView;

public class WheelViewDialog extends Dialog {
    private OnSelectClick onSelectClick;
    private int startOffset1;
    private int startOffset2;
    private int startOffset3;
    NewWheelView wv_1;
    NewWheelView wv_2;
    NewWheelView wv_3;

    public interface OnSelectClick {
        void onSelect(int i, int i2, int i3);
    }

    public WheelViewDialog(Context context) {
        super(context, R.style.MyDialog);
        init();
    }

    public void setOnSelectClick(OnSelectClick onSelectClick2) {
        this.onSelectClick = onSelectClick2;
    }

    public WheelViewDialog(Context context, int i) {
        super(context, i);
        init();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public NewWheelView getWv_1() {
        return this.wv_1;
    }

    public NewWheelView getWv_2() {
        return this.wv_2;
    }

    public NewWheelView getWv_3() {
        return this.wv_3;
    }

    public void setStartOffset1(int i) {
        this.startOffset1 = i;
    }

    public void setStartOffset2(int i) {
        this.startOffset2 = i;
    }

    public void setStartOffset3(int i) {
        this.startOffset3 = i;
    }

    private void init() {
        setContentView((int) R.layout.dialog_wheelviews);
        getWindow().setGravity(80);
        setCancelable(true);
        getWindow().getAttributes().width = ScreenUtil.getScreenWidth(getContext());
        this.wv_1 = (NewWheelView) findViewById(R.id.wv_1);
        this.wv_2 = (NewWheelView) findViewById(R.id.wv_2);
        this.wv_3 = (NewWheelView) findViewById(R.id.wv_3);
        findViewById(R.id.cancle).setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.views.dialog.$$Lambda$WheelViewDialog$o_ShEU6T7g5bUdASLHDQAJbWNRk */

            public final void onClick(View view) {
                WheelViewDialog.this.lambda$init$0$WheelViewDialog(view);
            }
        });
        findViewById(R.id.set).setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.views.dialog.$$Lambda$WheelViewDialog$uy58E6bYfCHPTYWk6mKVnvjy09Y */

            public final void onClick(View view) {
                WheelViewDialog.this.lambda$init$1$WheelViewDialog(view);
            }
        });
    }

    public /* synthetic */ void lambda$init$0$WheelViewDialog(View view) {
        dismiss();
    }

    public /* synthetic */ void lambda$init$1$WheelViewDialog(View view) {
        OnSelectClick onSelectClick2 = this.onSelectClick;
        if (onSelectClick2 != null) {
            onSelectClick2.onSelect(this.wv_1.getCurrentItem() + this.startOffset1, this.wv_2.getCurrentItem() + this.startOffset2, this.wv_3.getCurrentItem() + this.startOffset3);
        }
        dismiss();
    }
}
