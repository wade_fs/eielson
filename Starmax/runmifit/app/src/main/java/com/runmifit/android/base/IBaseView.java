package com.wade.fit.base;

public interface IBaseView {
    void goBack();

    void hideLoading();

    void showLoading();

    void showLoadingFalse();

    void showMsg(String str);

    void showNetError(String str);
}
