package com.wade.fit.model.bean;

import java.io.Serializable;
import java.util.Arrays;

public class Alarm implements Serializable {
    public static final int STATUS_DISPLAY = 85;
    public static final int STATUS_NOT_DISPLAY = 170;
    public static final int TYPE_CUSTOMIZE = 7;
    public static final int TYPE_ENGAGEMENT = 4;
    public static final int TYPE_EXERCISE = 2;
    public static final int TYPE_GATHERING = 5;
    public static final int TYPE_GETUP = 0;
    public static final int TYPE_MEDICINE = 3;
    public static final int TYPE_MEETING = 6;
    public static final int TYPE_SLEEP = 1;
    private static final long serialVersionUID = 1;
    private int alarmHour = 7;
    private int alarmId;
    private int alarmMinute = 30;
    private int alarmRepetitions;
    private int alarmSnoozeDuration;
    private int alarmStatus = 85;
    private int alarmType = 6;
    public boolean isOnce = true;
    private boolean on_off = true;
    private boolean[] weekRepeat = new boolean[7];

    public boolean[] getWeekRepeat() {
        return this.weekRepeat;
    }

    public void setWeekRepeat(boolean[] zArr) {
        this.weekRepeat = zArr;
        this.alarmRepetitions = toByte(zArr, this.on_off);
    }

    public int getAlarmId() {
        return this.alarmId;
    }

    public void setAlarmId(int i) {
        this.alarmId = i;
    }

    public int getAlarmStatus() {
        return this.alarmStatus;
    }

    public void setAlarmStatus(int i) {
        this.alarmStatus = i;
    }

    public int getAlarmType() {
        return this.alarmType;
    }

    public void setAlarmType(int i) {
        this.alarmType = i;
    }

    public int getAlarmHour() {
        return this.alarmHour;
    }

    public void setAlarmHour(int i) {
        this.alarmHour = i;
    }

    public int getAlarmMinute() {
        return this.alarmMinute;
    }

    public void setAlarmMinute(int i) {
        this.alarmMinute = i;
    }

    public void setAlarmSnoozeDuration(int i) {
        this.alarmSnoozeDuration = i;
    }

    public int getAlarmSnoozeDuration() {
        return this.alarmSnoozeDuration;
    }

    public boolean getOn_off() {
        return this.on_off;
    }

    public void setOn_off(boolean z) {
        this.on_off = z;
        this.alarmRepetitions = toByte(this.weekRepeat, z);
    }

    private int toByte(boolean[] zArr, boolean z) {
        int i = 0;
        for (int i2 = 0; i2 < 7; i2++) {
            if (zArr[i2]) {
                i |= 1 << (i2 + 1);
            }
        }
        return z ? i | 1 : i;
    }

    public String toString() {
        return "Alarm{alarmId=" + this.alarmId + ", alarmStatus=" + this.alarmStatus + ", alarmType=" + this.alarmType + ", alarmHour=" + this.alarmHour + ", alarmMinute=" + this.alarmMinute + ", on_off=" + this.on_off + ", alarmRepetitions=" + this.alarmRepetitions + ", weekRepeat=" + Arrays.toString(this.weekRepeat) + ", alarmSnoozeDuration=" + this.alarmSnoozeDuration + '}';
    }
}
