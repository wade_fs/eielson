package com.wade.fit.util.ble;

import com.wade.fit.util.log.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeviceControllerDataHandler {
    public List<DeviceCallback> deviceCallbackList = new ArrayList();

    public boolean isDeviceControl(byte[] bArr) {
        byte[] bArr2 = {-112, 8, 0};
        if (bArr.length < bArr2.length) {
            return false;
        }
        for (int i = 0; i < bArr2.length; i++) {
            if (bArr2[i] != bArr[i]) {
                return false;
            }
        }
        return true;
    }

    public void addDeviceCallback(DeviceCallback deviceCallback) {
        if (!this.deviceCallbackList.contains(deviceCallback)) {
            this.deviceCallbackList.add(deviceCallback);
        }
    }

    public void removeDeviceCallback(DeviceCallback deviceCallback) {
        this.deviceCallbackList.remove(deviceCallback);
    }

    public Object handlerData(byte[] bArr) {
        if (Arrays.equals(bArr, CmdHelper.camare())) {
            LogUtil.d("拍照");
            for (DeviceCallback deviceCallback : this.deviceCallbackList) {
                deviceCallback.camare();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.enterCamare())) {
            LogUtil.d("enterCamare");
            for (DeviceCallback deviceCallback2 : this.deviceCallbackList) {
                deviceCallback2.enterCamare();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.exitCamare())) {
            LogUtil.d("exitCamare");
            for (DeviceCallback deviceCallback3 : this.deviceCallbackList) {
                deviceCallback3.exitCamare();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.musicControl())) {
            LogUtil.d("musicControl");
            for (DeviceCallback deviceCallback4 : this.deviceCallbackList) {
                deviceCallback4.musicControl();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.preMusic())) {
            LogUtil.d("preMusic");
            for (DeviceCallback deviceCallback5 : this.deviceCallbackList) {
                deviceCallback5.preMusic();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.nextMusic())) {
            LogUtil.d("nextMusic");
            for (DeviceCallback deviceCallback6 : this.deviceCallbackList) {
                deviceCallback6.nextMusic();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.exitMusic())) {
            LogUtil.d("exitMusic");
            for (DeviceCallback deviceCallback7 : this.deviceCallbackList) {
                deviceCallback7.exitMusic();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.findPhone())) {
            LogUtil.d("findPhone");
            for (DeviceCallback deviceCallback8 : this.deviceCallbackList) {
                deviceCallback8.findPhone();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.answerRingingCall())) {
            LogUtil.d("answerRingingCall");
            for (DeviceCallback deviceCallback9 : this.deviceCallbackList) {
                deviceCallback9.answerRingingCall();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.endRingingCall())) {
            LogUtil.d("endRingingCall");
            for (DeviceCallback deviceCallback10 : this.deviceCallbackList) {
                deviceCallback10.endRingingCall();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.sos())) {
            LogUtil.d("sos");
            for (DeviceCallback deviceCallback11 : this.deviceCallbackList) {
                deviceCallback11.sos();
            }
            return null;
        } else if (Arrays.equals(bArr, CmdHelper.completeCmd(CmdHelper.voiceAdd()))) {
            LogUtil.d("voiceAdd");
            for (DeviceCallback deviceCallback12 : this.deviceCallbackList) {
                deviceCallback12.addVol();
            }
            return null;
        } else if (!Arrays.equals(bArr, CmdHelper.completeCmd(CmdHelper.voiceSub()))) {
            return null;
        } else {
            LogUtil.d("voiceSub");
            for (DeviceCallback deviceCallback13 : this.deviceCallbackList) {
                deviceCallback13.subVol();
            }
            return null;
        }
    }
}
