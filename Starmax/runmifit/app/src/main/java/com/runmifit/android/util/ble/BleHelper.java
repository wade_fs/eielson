package com.wade.fit.util.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.os.Build;
import android.os.PowerManager;
import com.wade.fit.app.AppApplication;
import java.lang.reflect.Method;

public class BleHelper {
    public static boolean isBluetoothOpen() {
        BluetoothAdapter adapter = ((BluetoothManager) AppApplication.getContext().getSystemService("bluetooth")).getAdapter();
        return adapter != null && adapter.isEnabled();
    }

    public static void refreshDeviceCache(BluetoothGatt bluetoothGatt) {
        try {
            Method method = bluetoothGatt.getClass().getMethod("refresh", new Class[0]);
            if (method != null) {
                method.invoke(bluetoothGatt, new Object[0]);
            }
        } catch (Exception e) {
            BleClient.showMessage(e.getMessage());
        }
    }

    public static boolean isNeedScan() {
        PowerManager powerManager = (PowerManager) BleClient.getInstance().getContext().getSystemService("power");
        if (powerManager == null) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= 20) {
            return powerManager.isInteractive();
        }
        return powerManager.isScreenOn();
    }

    public static boolean isInDfuMode(BluetoothGatt bluetoothGatt) {
        return bluetoothGatt.getService(BleManager.RX_UPDATE_UUID) != null;
    }

    public static boolean openBLE() {
        BluetoothAdapter adapter = ((BluetoothManager) AppApplication.getInstance().getSystemService("bluetooth")).getAdapter();
        if (adapter == null) {
            return false;
        }
        return adapter.enable();
    }
}
