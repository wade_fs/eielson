package com.wade.fit.views.wheel;

public interface OnWheelScrollListener {
    void onScrollingFinished(WheelView wheelView);

    void onScrollingStarted(WheelView wheelView);
}
