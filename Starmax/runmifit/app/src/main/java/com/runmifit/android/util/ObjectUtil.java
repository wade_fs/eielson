package com.wade.fit.util;

import android.os.Build;
import android.support.v4.util.SimpleArrayMap;
import android.util.LongSparseArray;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.util.SparseLongArray;
import com.google.gson.Gson;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ObjectUtil {
    public static boolean isCollectionEmpty(Object obj) {
        return obj == null;
    }

    public static <E> List<E> deepCopy(List<E> list) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            new ObjectOutputStream(byteArrayOutputStream).writeObject(list);
            return (List) new ObjectInputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray())).readObject();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    public static <P> P getParameterizedType(Class<?> cls) {
        Type genericSuperclass = cls.getGenericSuperclass();
        if (!(genericSuperclass instanceof ParameterizedType)) {
            return null;
        }
        try {
            return ((Class) ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0]).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Class<?> getParameterizedType(Class<?> cls, boolean z) {
        Type genericSuperclass = cls.getGenericSuperclass();
        if (z) {
            try {
                genericSuperclass = cls.getGenericInterfaces()[0];
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        if (genericSuperclass instanceof ParameterizedType) {
            return (Class) ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
        }
        return null;
    }

    public static <T> T checkNotNull(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    public static boolean isCollectionEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj.getClass().isArray() && Array.getLength(obj) == 0) {
            return true;
        }
        if ((obj instanceof CharSequence) && obj.toString().length() == 0) {
            return true;
        }
        if ((obj instanceof Collection) && ((Collection) obj).isEmpty()) {
            return true;
        }
        if ((obj instanceof Map) && ((Map) obj).isEmpty()) {
            return true;
        }
        if ((obj instanceof SimpleArrayMap) && ((SimpleArrayMap) obj).isEmpty()) {
            return true;
        }
        if ((obj instanceof SparseArray) && ((SparseArray) obj).size() == 0) {
            return true;
        }
        if ((obj instanceof SparseBooleanArray) && ((SparseBooleanArray) obj).size() == 0) {
            return true;
        }
        if ((obj instanceof SparseIntArray) && ((SparseIntArray) obj).size() == 0) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= 18 && (obj instanceof SparseLongArray) && ((SparseLongArray) obj).size() == 0) {
            return true;
        }
        boolean z = obj instanceof LongSparseArray;
        if (z && ((LongSparseArray) obj).size() == 0) {
            return true;
        }
        if (Build.VERSION.SDK_INT < 16 || !z || ((LongSparseArray) obj).size() != 0) {
            return false;
        }
        return true;
    }

    public static <T> T deepClone(T t, Type type) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(gson.toJson((Object) t), type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
