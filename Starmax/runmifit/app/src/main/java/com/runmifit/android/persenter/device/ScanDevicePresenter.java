package com.wade.fit.persenter.device;

import android.bluetooth.BluetoothGatt;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BasePersenter;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.persenter.device.ScanDeviceContract;
import com.wade.fit.util.SPHelper;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ble.BaseAppBleListener;
import com.wade.fit.util.ble.BleScanTool;
import com.wade.fit.util.ble.BleSdkWrapper;

public class ScanDevicePresenter extends BasePersenter<ScanDeviceContract.View> implements ScanDeviceContract.Presenter {
    /* access modifiers changed from: private */
    public BaseAppBleListener baseAppBleListener = new BaseAppBleListener() {
        /* class com.wade.fit.persenter.device.ScanDevicePresenter.C23921 */

        public void onBLEConnected(BluetoothGatt bluetoothGatt) {
            super.onBLEConnected(bluetoothGatt);
        }

        public void initComplete() {
            super.initComplete();
            BleSdkWrapper.removeListener(ScanDevicePresenter.this.baseAppBleListener);
            ScanDevicePresenter.this.connectDevice.isBind = true;
            SPHelper.saveBLEDevice(ScanDevicePresenter.this.connectDevice);
            ((ScanDeviceContract.View) ScanDevicePresenter.this.mView).requestSuccess(1, ScanDevicePresenter.this.connectDevice);
        }

        public void onBLEDisConnected(String str) {
            super.onBLEDisConnected(str);
            BleSdkWrapper.removeListener(ScanDevicePresenter.this.baseAppBleListener);
            ((ScanDeviceContract.View) ScanDevicePresenter.this.mView).showMsg(AppApplication.getInstance().getResources().getString(R.string.bind_param_setting_fail));
            ((ScanDeviceContract.View) ScanDevicePresenter.this.mView).requestFaild();
        }

        public void onBLEConnectTimeOut() {
            super.onBLEConnectTimeOut();
            SharePreferenceUtils.put(AppApplication.getInstance(), Constants.UPDATECONFIG, Constants.NOUPDATE);
            BleSdkWrapper.removeListener(ScanDevicePresenter.this.baseAppBleListener);
            ((ScanDeviceContract.View) ScanDevicePresenter.this.mView).showMsg(AppApplication.getInstance().getResources().getString(R.string.ble_connect_timeout));
            ((ScanDeviceContract.View) ScanDevicePresenter.this.mView).requestFaild();
        }
    };
    /* access modifiers changed from: private */
    public BLEDevice connectDevice;
    private BleScanTool.ScanDeviceListener scanCallback = new BleScanTool.ScanDeviceListener() {
        /* class com.wade.fit.persenter.device.ScanDevicePresenter.C23932 */

        public void onFind(BLEDevice bLEDevice) {
            if (bLEDevice.mDeviceName.contains("S5") || bLEDevice.mDeviceName.contains("S10") || bLEDevice.mDeviceName.contains("CS20") || bLEDevice.mDeviceName.contains("CV10") || bLEDevice.mDeviceName.contains("Symphony") || bLEDevice.mDeviceName.contains("Empress") || bLEDevice.mDeviceName.contains("C31")) {
                ((ScanDeviceContract.View) ScanDevicePresenter.this.mView).requestSuccess(0, bLEDevice);
            }
        }

        public void onFinish() {
            BleSdkWrapper.stopScanDevices();
        }
    };

    public void startScanBle(int i) {
        if (BleSdkWrapper.isConnected()) {
            BleSdkWrapper.disConnect();
        }
        BleSdkWrapper.stopScanDevices();
        BleSdkWrapper.startScanDevices(this.scanCallback);
    }

    public void stopScanBle() {
        BleSdkWrapper.stopScanDevices();
        BleScanTool.getInstance().removeScanDeviceListener(this.scanCallback);
    }

    public void connecting(int i, BLEDevice bLEDevice) {
        if (bLEDevice != null) {
            this.connectDevice = bLEDevice;
            stopScanBle();
            if (BleSdkWrapper.isConnected()) {
                BleSdkWrapper.disConnect();
            }
            BleSdkWrapper.setBleListener(this.baseAppBleListener);
            BleSdkWrapper.connect(bLEDevice);
        }
    }

    public void detachView() {
        super.detachView();
        BleSdkWrapper.removeListener(this.baseAppBleListener);
        BleScanTool.getInstance().removeScanDeviceListener(this.scanCallback);
    }
}
