package com.wade.fit.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.wade.fit.util.AsyncTaskUtil;
import java.util.concurrent.CountDownLatch;

public class DownDataService extends Service {
    CountDownLatch countDownLatch;
    int index = 0;

    /* access modifiers changed from: package-private */
    public void getBp() {
    }

    /* access modifiers changed from: package-private */
    public void getHr() {
    }

    /* access modifiers changed from: package-private */
    public void getSleep() {
    }

    /* access modifiers changed from: package-private */
    public void getSport() {
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (this.countDownLatch == null) {
            this.countDownLatch = new CountDownLatch(4);
            getSport();
            getSleep();
            getHr();
            getBp();
            checkComplete();
        }
        return super.onStartCommand(intent, i, i2);
    }

    /* access modifiers changed from: package-private */
    public void checkComplete() {
        new AsyncTaskUtil(new AsyncTaskUtil.IAsyncTaskCallBack() {
            /* class com.wade.fit.service.DownDataService.C24381 */

            public Object doInBackground(String... strArr) {
                try {
                    DownDataService.this.countDownLatch.wait();
                    return null;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            public void onPostExecute(Object obj) {
                DownDataService.this.countDownLatch = null;
            }
        });
    }
}
