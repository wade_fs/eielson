package com.wade.fit.model.net;

import okhttp3.Interceptor;
import okhttp3.Response;

/* renamed from: com.wade.fit.model.net.-$$Lambda$ApiManager$1yjR0n2ISntVWwP8NPp5fmu4XNI  reason: invalid class name */
/* compiled from: lambda */
public final /* synthetic */ class $$Lambda$ApiManager$1yjR0n2ISntVWwP8NPp5fmu4XNI implements Interceptor {
    public static final /* synthetic */ $$Lambda$ApiManager$1yjR0n2ISntVWwP8NPp5fmu4XNI INSTANCE = new $$Lambda$ApiManager$1yjR0n2ISntVWwP8NPp5fmu4XNI();

    private /* synthetic */ $$Lambda$ApiManager$1yjR0n2ISntVWwP8NPp5fmu4XNI() {
    }

    public final Response intercept(Interceptor.Chain chain) {
        return ApiManager.lambda$getRetrofit$1(chain);
    }
}
