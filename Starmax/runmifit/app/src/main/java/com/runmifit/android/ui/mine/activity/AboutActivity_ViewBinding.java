package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.AboutActivity_ViewBinding */
public class AboutActivity_ViewBinding implements Unbinder {
    private AboutActivity target;
    private View view2131296754;
    private View view2131296977;
    private View view2131297077;

    public AboutActivity_ViewBinding(AboutActivity aboutActivity) {
        this(aboutActivity, aboutActivity.getWindow().getDecorView());
    }

    public AboutActivity_ViewBinding(final AboutActivity aboutActivity, View view) {
        this.target = aboutActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.tvVersion, "field 'tvVersion' and method 'onClick'");
        aboutActivity.tvVersion = (TextView) Utils.castView(findRequiredView, R.id.tvVersion, "field 'tvVersion'", TextView.class);
        this.view2131297077 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.AboutActivity_ViewBinding.C25701 */

            public void doClick(View view) {
                aboutActivity.onClick(view);
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.tvAppUpdate, "field 'tvAppUpdate' and method 'onClick'");
        aboutActivity.tvAppUpdate = (TextView) Utils.castView(findRequiredView2, R.id.tvAppUpdate, "field 'tvAppUpdate'", TextView.class);
        this.view2131296977 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.AboutActivity_ViewBinding.C25712 */

            public void doClick(View view) {
                aboutActivity.onClick(view);
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.privacy, "field 'privacy' and method 'onShow'");
        aboutActivity.privacy = (TextView) Utils.castView(findRequiredView3, R.id.privacy, "field 'privacy'", TextView.class);
        this.view2131296754 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.AboutActivity_ViewBinding.C25723 */

            public void doClick(View view) {
                aboutActivity.onShow(view);
            }
        });
    }

    public void unbind() {
        AboutActivity aboutActivity = this.target;
        if (aboutActivity != null) {
            this.target = null;
            aboutActivity.tvVersion = null;
            aboutActivity.tvAppUpdate = null;
            aboutActivity.privacy = null;
            this.view2131297077.setOnClickListener(null);
            this.view2131297077 = null;
            this.view2131296977.setOnClickListener(null);
            this.view2131296977 = null;
            this.view2131296754.setOnClickListener(null);
            this.view2131296754 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
