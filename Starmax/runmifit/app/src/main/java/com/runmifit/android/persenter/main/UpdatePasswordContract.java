package com.wade.fit.persenter.main;

import com.wade.fit.base.IBaseView;

public interface UpdatePasswordContract {

    public interface Presenter {
        void sendCode(String str);

        void updatePassword(String str, String str2, String str3);
    }

    public interface View extends IBaseView {
        void sendCodeFaild(int i);

        void sendCodeSuccess();

        void updateFaild(int i);

        void updateSuccess();
    }
}
