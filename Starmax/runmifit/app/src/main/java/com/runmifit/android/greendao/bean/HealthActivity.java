package com.wade.fit.greendao.bean;

import java.io.Serializable;

public class HealthActivity implements Serializable {
    public static final int DATA_FROM_APP = 1;
    public static final int DATA_FROM_DEVICE = 2;
    public static final long serialVersionUID = 111;
    private int aerobic_mins;
    private String avgSpeed;
    private int avg_hr_value;
    private int burn_fat_mins;
    private int calories;
    private int dataFrom = 2;
    private long date;
    private int day;
    private int distance;
    private int durations;
    private int hour;
    private int hr_data_interval_minute;
    private String hr_data_vlaue_json;
    private boolean isUploaded;
    private int limit_mins;
    private String macAddress;
    private int max_hr_value;
    private int minute;
    private int month;
    private String remark;
    private int second;
    private int step;
    private int type;
    private String userId;
    private int year;

    public HealthActivity() {
    }

    public HealthActivity(boolean z, String str, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, String str2, int i13, int i14, int i15, int i16, int i17, String str3, String str4, int i18, String str5, long j) {
        this.isUploaded = z;
        this.macAddress = str;
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.hour = i4;
        this.minute = i5;
        this.second = i6;
        this.hr_data_interval_minute = i7;
        this.type = i8;
        this.step = i9;
        this.durations = i10;
        this.calories = i11;
        this.distance = i12;
        this.avgSpeed = str2;
        this.avg_hr_value = i13;
        this.max_hr_value = i14;
        this.burn_fat_mins = i15;
        this.aerobic_mins = i16;
        this.limit_mins = i17;
        this.userId = str3;
        this.remark = str4;
        this.dataFrom = i18;
        this.hr_data_vlaue_json = str5;
        this.date = j;
    }

    public String getAvgSpeed() {
        return this.avgSpeed;
    }

    public void setAvgSpeed(String str) {
        this.avgSpeed = str;
    }

    public boolean getIsUploaded() {
        return this.isUploaded;
    }

    public void setIsUploaded(boolean z) {
        this.isUploaded = z;
    }

    public String getMacAddress() {
        return this.macAddress;
    }

    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public int getHour() {
        return this.hour;
    }

    public void setHour(int i) {
        this.hour = i;
    }

    public int getMinute() {
        return this.minute;
    }

    public void setMinute(int i) {
        this.minute = i;
    }

    public int getSecond() {
        return this.second;
    }

    public void setSecond(int i) {
        this.second = i;
    }

    public int getHr_data_interval_minute() {
        return this.hr_data_interval_minute;
    }

    public void setHr_data_interval_minute(int i) {
        this.hr_data_interval_minute = i;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int i) {
        this.type = i;
    }

    public int getStep() {
        return this.step;
    }

    public void setStep(int i) {
        this.step = i;
    }

    public int getDurations() {
        return this.durations;
    }

    public void setDurations(int i) {
        this.durations = i;
    }

    public int getCalories() {
        return this.calories;
    }

    public void setCalories(int i) {
        this.calories = i;
    }

    public int getDistance() {
        return this.distance;
    }

    public void setDistance(int i) {
        this.distance = i;
    }

    public int getAvg_hr_value() {
        return this.avg_hr_value;
    }

    public void setAvg_hr_value(int i) {
        this.avg_hr_value = i;
    }

    public int getMax_hr_value() {
        return this.max_hr_value;
    }

    public void setMax_hr_value(int i) {
        this.max_hr_value = i;
    }

    public int getBurn_fat_mins() {
        return this.burn_fat_mins;
    }

    public void setBurn_fat_mins(int i) {
        this.burn_fat_mins = i;
    }

    public int getAerobic_mins() {
        return this.aerobic_mins;
    }

    public void setAerobic_mins(int i) {
        this.aerobic_mins = i;
    }

    public int getLimit_mins() {
        return this.limit_mins;
    }

    public void setLimit_mins(int i) {
        this.limit_mins = i;
    }

    public int getDataFrom() {
        return this.dataFrom;
    }

    public void setDataFrom(int i) {
        this.dataFrom = i;
    }

    public String getHr_data_vlaue_json() {
        return this.hr_data_vlaue_json;
    }

    public void setHr_data_vlaue_json(String str) {
        this.hr_data_vlaue_json = str;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long j) {
        this.date = j;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String str) {
        this.remark = str;
    }

    public String toString() {
        return "HealthActivity{, year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", hour=" + this.hour + ", minute=" + this.minute + ", second=" + this.second + ", type=" + this.type + ", step=" + this.step + ", durations=" + this.durations + ", calories=" + this.calories + ", distance=" + this.distance + ", avg_hr_value=" + this.avg_hr_value + ", max_hr_value=" + this.max_hr_value + ", date=" + this.date + '}';
    }
}
