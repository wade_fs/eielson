package com.wade.fit.ui.device.bean;

/* renamed from: com.wade.fit.ui.device.bean.WeekDayBean */
public class WeekDayBean {
    private boolean isSelect;
    private String weekDayName;

    public WeekDayBean(String str, boolean z) {
        this.weekDayName = str;
        this.isSelect = z;
    }

    public String getWeekDayName() {
        return this.weekDayName;
    }

    public void setWeekDayName(String str) {
        this.weekDayName = str;
    }

    public boolean isSelect() {
        return this.isSelect;
    }

    public void setSelect(boolean z) {
        this.isSelect = z;
    }
}
