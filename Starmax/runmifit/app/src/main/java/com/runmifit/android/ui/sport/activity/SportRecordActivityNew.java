package com.wade.fit.ui.sport.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.wade.fit.R;
import com.wade.fit.base.BaseCalendarActivity;
import com.wade.fit.greendao.bean.HealthActivity;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.model.bean.BarChartProperties;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.model.bean.DetailType;
import com.wade.fit.model.bean.HealthMonthActivity;
import com.wade.fit.model.bean.SportHistoryVo;
import com.wade.fit.ui.sport.adapter.SportActivityAdapter;
import com.wade.fit.persenter.sport.ISportHistoryView;
import com.wade.fit.persenter.sport.SportHistoryPresenter;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.log.LogUtil;
import com.wade.fit.views.CustomExpandableListView;
import com.wade.fit.views.SportRecordChart;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/* renamed from: com.wade.fit.ui.sport.activity.SportRecordActivityNew */
public class SportRecordActivityNew extends BaseCalendarActivity<SportHistoryPresenter> implements ISportHistoryView {
    private final int RESULT_ACTIVITY_RESULT = 17;
    RelativeLayout barBg;
    private DetailTimeType detailTimeType = DetailTimeType.DAY;
    private SimpleDateFormat formatYM = new SimpleDateFormat("yyyy-MM");
    private SimpleDateFormat formatYMD = new SimpleDateFormat("yyyy-MM-dd");
    ImageView ivNextDate;
    ImageView ivPreDate;
    ImageView ivmSearchDate;
    LinearLayout llNoData;
    private SportActivityAdapter mAdapter;
    private int mChildIndex = 0;
    RadioGroup mDateGroup;
    RadioButton mDayTab;
    NestedScrollView mNestedScrollView;
    RadioButton mOneMonthTab;
    private int mParentIndex = 0;
    CustomExpandableListView mRecyclerView;
    /* access modifiers changed from: private */
    public Date mSearchDate;
    RadioButton mSixMonthTab;
    RadioButton mYearTab;
    LinearLayout nesData;
    private List<HealthMonthActivity> showList = new ArrayList();
    SportRecordChart sportChartCalory;
    SportRecordChart sportChartDistance;
    SportRecordChart sportChartTime;
    TextView tvCalory;
    TextView tvDate;
    TextView tvDistance;
    TextView tvTime;
    View viewCalor;
    View viewDistance;
    View viewTime;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_sport_record;
    }

    public void getByType(SportHistoryVo sportHistoryVo) {
        if (sportHistoryVo.monthDatas == null || sportHistoryVo.monthDatas.size() == 0) {
            this.mNestedScrollView.setVisibility(View.GONE);
            this.llNoData.setVisibility(View.VISIBLE);
            return;
        }
        this.mNestedScrollView.setVisibility(View.VISIBLE);
        this.llNoData.setVisibility(View.GONE);
        this.showList = sportHistoryVo.monthDatas;
        this.mRecyclerView.setVisibility(View.VISIBLE);
        this.mAdapter = new SportActivityAdapter(this, this.showList);
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mRecyclerView.expandGroup(0);
        this.mRecyclerView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            /* class com.wade.fit.ui.sport.activity.$$Lambda$SportRecordActivityNew$IGqRI9mqdL4SK1vDr0nAxHJesA */

            public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
                return SportRecordActivityNew.this.lambda$getByType$0$SportRecordActivityNew(expandableListView, view, i, i2, j);
            }
        });
    }

    public /* synthetic */ boolean lambda$getByType$0$SportRecordActivityNew(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        if (this.showList.get(i).getHealthActivities() == null) {
            return false;
        }
        this.mParentIndex = i;
        this.mChildIndex = i2;
        Bundle bundle = new Bundle();
        bundle.putInt("ACTIVITY_FROM", 1);
        bundle.putSerializable("SPORT_DATA", this.showList.get(i).getHealthActivities().get(i2));
        IntentUtil.goToActivityForResult(this, SportDetailActivityNew.class, bundle, 17);
        return false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == 200 && i == 17) {
            this.showList.get(this.mParentIndex).getHealthActivities().remove(this.mChildIndex);
            if (this.showList.get(this.mParentIndex).getHealthActivities().size() == 0) {
                this.showList.remove(this.mParentIndex);
            }
            if (this.showList.size() == 0) {
                this.mNestedScrollView.setVisibility(View.GONE);
                this.llNoData.setVisibility(View.VISIBLE);
            }
            this.mAdapter.setDate(this.showList);
            updateByDate(((SportHistoryPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.layoutTitle.setVisibility(View.GONE);
        this.mNestedScrollView.smoothScrollTo(0, 20);
        this.mRecyclerView.setFocusable(false);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.barBg.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = ScreenUtil.getStatusHeight(this);
        this.barBg.setLayoutParams(layoutParams);
        ((SportHistoryPresenter) this.mPresenter).getByType();
        this.mSearchDate = (Date) getIntent().getExtras().getSerializable("date");
        updateByDate(((SportHistoryPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
    }

    /* access modifiers changed from: package-private */
    public void finishToback() {
        finish();
    }

    /* access modifiers changed from: package-private */
    public void showTimeData() {
        this.viewTime.setVisibility(View.VISIBLE);
        this.tvTime.setTextColor(getResources().getColor(R.color.normal_font_color));
        this.viewDistance.setVisibility(View.GONE);
        this.tvDistance.setTextColor(getResources().getColor(R.color.small_font_color));
        this.viewCalor.setVisibility(View.GONE);
        this.tvCalory.setTextColor(getResources().getColor(R.color.small_font_color));
        this.sportChartTime.setVisibility(View.VISIBLE);
        this.sportChartDistance.setVisibility(View.GONE);
        this.sportChartCalory.setVisibility(View.GONE);
    }

    /* access modifiers changed from: package-private */
    public void showDistanceData() {
        this.viewTime.setVisibility(View.GONE);
        this.tvTime.setTextColor(getResources().getColor(R.color.small_font_color));
        this.viewDistance.setVisibility(View.VISIBLE);
        this.tvDistance.setTextColor(getResources().getColor(R.color.normal_font_color));
        this.viewCalor.setVisibility(View.GONE);
        this.tvCalory.setTextColor(getResources().getColor(R.color.small_font_color));
        this.sportChartTime.setVisibility(View.GONE);
        this.sportChartDistance.setVisibility(View.VISIBLE);
        this.sportChartCalory.setVisibility(View.GONE);
    }

    /* access modifiers changed from: package-private */
    public void showCaloryData() {
        this.viewTime.setVisibility(View.GONE);
        this.tvTime.setTextColor(getResources().getColor(R.color.small_font_color));
        this.viewDistance.setVisibility(View.GONE);
        this.tvDistance.setTextColor(getResources().getColor(R.color.small_font_color));
        this.viewCalor.setVisibility(View.VISIBLE);
        this.tvCalory.setTextColor(getResources().getColor(R.color.normal_font_color));
        this.sportChartTime.setVisibility(View.GONE);
        this.sportChartDistance.setVisibility(View.GONE);
        this.sportChartCalory.setVisibility(View.VISIBLE);
    }

    /* access modifiers changed from: package-private */
    public void showDayData() {
        this.ivmSearchDate.setVisibility(View.VISIBLE);
        this.ivPreDate.setVisibility(View.VISIBLE);
        this.ivNextDate.setVisibility(View.VISIBLE);
        this.detailTimeType = DetailTimeType.DAY;
        updateByDate(((SportHistoryPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
    }

    /* access modifiers changed from: package-private */
    public void showOneMonthData() {
        this.ivmSearchDate.setVisibility(View.GONE);
        this.ivPreDate.setVisibility(View.GONE);
        this.ivNextDate.setVisibility(View.GONE);
        this.detailTimeType = DetailTimeType.ONE_MONTH;
        updateByDate(((SportHistoryPresenter) this.mPresenter).getOneMonth());
    }

    /* access modifiers changed from: package-private */
    public void showSixMonthData() {
        this.ivmSearchDate.setVisibility(View.GONE);
        this.ivPreDate.setVisibility(View.GONE);
        this.ivNextDate.setVisibility(View.GONE);
        this.detailTimeType = DetailTimeType.SIX_MONTH;
        updateByDate(((SportHistoryPresenter) this.mPresenter).getSixMonth());
    }

    /* access modifiers changed from: package-private */
    public void showYearData() {
        this.ivmSearchDate.setVisibility(View.GONE);
        this.ivPreDate.setVisibility(View.GONE);
        this.ivNextDate.setVisibility(View.GONE);
        this.detailTimeType = DetailTimeType.YEAR;
        updateByDate(((SportHistoryPresenter) this.mPresenter).getYearMonth());
    }

    /* access modifiers changed from: package-private */
    public void selectDay() {
        if (this.detailTimeType == DetailTimeType.DAY) {
            this.clendarDialog.mOnSelectDateListener = new OnSelectDateListener() {
                /* class com.wade.fit.ui.sport.activity.SportRecordActivityNew.C26241 */

                public void onSelectOtherMonth(int i) {
                }

                public void onSelectDate(CalendarDate calendarDate) {
                    Date unused = SportRecordActivityNew.this.mSearchDate = ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day);
                    SportRecordActivityNew sportRecordActivityNew = SportRecordActivityNew.this;
                    sportRecordActivityNew.updateByDate(((SportHistoryPresenter) sportRecordActivityNew.mPresenter).getDetailCurrent(DetailTimeType.DAY, SportRecordActivityNew.this.mSearchDate));
                }
            };
            this.clendarDialog.showDialog();
        }
    }

    /* access modifiers changed from: private */
    public void updateByDate(SportHistoryVo sportHistoryVo) {
        updateDateStr();
        if (this.detailTimeType == DetailTimeType.DAY) {
            changeDateUpdateUI(this.mSearchDate);
        }
        ArrayList arrayList = new ArrayList();
        for (HealthActivity healthActivity : sportHistoryVo.datas) {
            HealthSportItem healthSportItem = new HealthSportItem();
            healthSportItem.setStepCount(healthActivity.getDurations());
            healthSportItem.setDate(healthActivity.getDate());
            healthSportItem.setCalory(healthActivity.getCalories());
            healthSportItem.setDistance(healthActivity.getDistance());
            healthSportItem.setRemark(healthActivity.getRemark());
            healthSportItem.setMonth(healthActivity.getMonth());
            healthSportItem.setYear(healthActivity.getYear());
            healthSportItem.setDay(healthActivity.getDay());
            healthSportItem.setMacAddress(healthActivity.getMacAddress());
            arrayList.add(healthSportItem);
        }
        LogUtil.d("size:" + arrayList.size());
        BarChartProperties barChartProperties = new BarChartProperties();
        barChartProperties.barColor = -136138;
        this.sportChartTime.setData(arrayList, sportHistoryVo.dates, DetailType.STEP, barChartProperties);
        BarChartProperties barChartProperties2 = new BarChartProperties();
        barChartProperties2.barColor = Color.parseColor("#21C2FC");
        this.sportChartDistance.setData(arrayList, sportHistoryVo.dates, DetailType.DISTANCE, barChartProperties2);
        BarChartProperties barChartProperties3 = new BarChartProperties();
        barChartProperties3.barColor = Color.parseColor("#FC6620");
        this.sportChartCalory.setData(arrayList, sportHistoryVo.dates, DetailType.CAL, barChartProperties3);
    }

    private void updateDateStr() {
        Calendar instance = Calendar.getInstance();
        if (this.detailTimeType == DetailTimeType.DAY) {
            Date date = this.mSearchDate;
            if (date == null) {
                date = new Date();
            }
            this.mSearchDate = date;
            this.tvDate.setText(this.formatYMD.format(this.mSearchDate));
        }
        if (this.detailTimeType == DetailTimeType.ONE_MONTH) {
            instance.add(2, -1);
            this.tvDate.setText(this.formatYMD.format(instance.getTime()));
            this.tvDate.append("～");
            this.tvDate.append(this.formatYMD.format(new Date()));
        }
        if (this.detailTimeType == DetailTimeType.SIX_MONTH) {
            instance.add(2, -6);
            this.tvDate.setText(this.formatYM.format(instance.getTime()));
            this.tvDate.append("～");
            this.tvDate.append(this.formatYM.format(new Date()));
        }
        if (this.detailTimeType == DetailTimeType.YEAR) {
            instance.add(1, -1);
            this.tvDate.setText(this.formatYM.format(instance.getTime()));
            this.tvDate.append("～");
            this.tvDate.append(this.formatYM.format(new Date()));
        }
    }

    /* access modifiers changed from: package-private */
    public void changeNextDay() {
        this.mSearchDate = DateUtil.getSpecifiedDayAfterDate(this.formatYMD.format(this.mSearchDate));
        updateByDate(((SportHistoryPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
    }

    /* access modifiers changed from: package-private */
    public void changePreDay() {
        this.mSearchDate = DateUtil.getSpecifiedDayBeforeDate(this.formatYMD.format(this.mSearchDate));
        updateByDate(((SportHistoryPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
    }

    private void changeDateUpdateUI(Date date) {
        if (this.formatYMD.format(date).equals(this.formatYMD.format(Calendar.getInstance().getTime()))) {
            this.ivNextDate.setVisibility(View.INVISIBLE);
            this.tvDate.setText(getResources().getString(R.string.today));
            return;
        }
        this.ivNextDate.setVisibility(View.VISIBLE);
        this.tvDate.setText(this.formatYMD.format(date));
    }
}
