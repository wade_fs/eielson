package com.wade.fit.persenter.device;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.Alarm;
import java.util.List;

public interface AlarmContract {

    public interface Presenter {
        void saveAlarmToDevice(List<Alarm> list);
    }

    public interface View extends IBaseView {
        void saveFaild();

        void saveSuccess();
    }
}
