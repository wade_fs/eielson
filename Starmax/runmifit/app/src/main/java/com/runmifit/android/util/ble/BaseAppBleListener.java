package com.wade.fit.util.ble;

import android.bluetooth.BluetoothGatt;
import com.wade.fit.model.bean.BLEDevice;
import com.wade.fit.util.log.DebugLog;

public class BaseAppBleListener implements AppBleListener {
    public void onInDfuMode(BLEDevice bLEDevice) {
    }

    public void onBlueToothError(int i) {
        mo32793d("onBlueToothError");
    }

    public void onBLEConnecting(String str) {
        mo32793d("onBLEConnecting");
    }

    public void onBLEConnected(BluetoothGatt bluetoothGatt) {
        mo32793d("onBLEConnected");
    }

    public void onServiceDiscover(BluetoothGatt bluetoothGatt, int i) {
        mo32793d("onServiceDiscover");
    }

    public void onBLEDisConnected(String str) {
        mo32793d("onBLEDisConnected");
    }

    public void onBLEConnectTimeOut() {
        mo32793d("onBLEConnectTimeOut");
    }

    public void onDataSendTimeOut(byte[] bArr) {
        mo32793d("onDataSendTimeOut");
    }

    public void initComplete() {
        mo32793d("initComplete");
    }

    /* renamed from: d */
    public void mo32793d(String str) {
        DebugLog.m6203d(toString() + "," + str);
    }
}
