package com.wade.fit.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import com.wade.fit.R;

public class ValueStateTextView extends AppCompatTextView {
    private static final int[] OPEN_STATE_SET = {R.attr.state_open};

    /* renamed from: d */
    private Drawable f5250d;
    private int drawableW = 0;
    private boolean isOpen = false;
    private int width = 0;

    public ValueStateTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ValueStateTextView);
        this.f5250d = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        this.width = getMeasuredWidth();
    }

    public boolean isOpen() {
        return this.isOpen;
    }

    public void setOpen(boolean z) {
        this.isOpen = z;
        refreshDrawableState();
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (isOpen()) {
            mergeDrawableStates(onCreateDrawableState, OPEN_STATE_SET);
        }
        return onCreateDrawableState;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = this.f5250d;
        if (drawable != null && drawable.isStateful()) {
            this.f5250d.setState(getDrawableState());
            this.drawableW = this.f5250d.getIntrinsicWidth();
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.f5250d != null) {
            int height = getHeight() / 2;
            Math.min(height - 3, getHeight());
            int i = this.width;
            int i2 = this.drawableW;
            int i3 = (i - i2) / 2;
            int i4 = height - (i2 / 2);
            this.f5250d.setBounds(i3, i4, i2 + i3, i2 + i4);
            this.f5250d.draw(canvas);
        }
        super.onDraw(canvas);
    }
}
