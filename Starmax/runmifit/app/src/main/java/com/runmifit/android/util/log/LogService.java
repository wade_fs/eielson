package com.wade.fit.util.log;

import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import com.wade.fit.app.AppApplication;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LogService {
    private static final String FILE_NAME_PATTERN = "yyyyMMdd";
    private static final String FILE_TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSZ";
    private static final String LINE_SEP = System.getProperty("line.separator");
    private static final String LOG_FILE_PREFIX_NAME = ".log";
    private static final String TAG = "[IDO_APP] LogService";
    private static LogService instance = new LogService();
    private static boolean isPermissionOk = false;
    /* access modifiers changed from: private */
    public Condition mCondition = this.mLock.newCondition();
    /* access modifiers changed from: private */
    public volatile boolean mIsStopLog = false;
    /* access modifiers changed from: private */
    public Lock mLock = new ReentrantLock();
    /* access modifiers changed from: private */
    public ConcurrentLinkedQueue<String[]> mLogQueue = new ConcurrentLinkedQueue<>();
    private Thread mLogThread;
    private Runnable mLooperRunnable = new Runnable() {
        /* class com.wade.fit.util.log.LogService.C27031 */

        public void run() {
            LogService.this.deleteOutDateLog();
            while (true) {
                if (LogService.this.mIsStopLog) {
                    break;
                }
                LogService.this.mLock.lock();
                try {
                    if (LogService.this.mLogQueue.isEmpty()) {
                        LogService.this.mCondition.await();
                    }
                    if (LogService.this.mIsStopLog) {
                        LogService.this.mLock.unlock();
                        break;
                    }
                    String[] strArr = (String[]) LogService.this.mLogQueue.poll();
                    String str = strArr[0];
                    String str2 = strArr[1];
                    if (LogService.this.createLogFileDir(str)) {
                        LogService.this.writeToFile(str, str2);
                    } else {
                        Log.e(LogService.TAG, "createLogFileDir failed:" + str);
                    }
                    LogService.this.mLock.unlock();
                } catch (InterruptedException e) {
                    Log.e(LogService.TAG, e.getMessage(), e);
                    Thread.currentThread().interrupt();
                } catch (Throwable th) {
                    LogService.this.mLock.unlock();
                    throw th;
                }
            }
            Log.i(LogService.TAG, "exit loop ok!");
        }
    };

    private String getLogPathSdcardDir() {
        return "123";
    }

    private static void checkPermission() {
        int checkSelfPermission = ActivityCompat.checkSelfPermission(AppApplication.getContext(), "android.permission.WRITE_EXTERNAL_STORAGE");
        int checkSelfPermission2 = ActivityCompat.checkSelfPermission(AppApplication.getContext(), "android.permission.READ_EXTERNAL_STORAGE");
        if (checkSelfPermission == 0 && checkSelfPermission2 == 0) {
            isPermissionOk = true;
            return;
        }
        isPermissionOk = false;
        Log.e(TAG, "not allowed permission[WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE], LogTool is disabled!");
    }

    public static void init() {
        Log.i(TAG, "init...");
        checkPermission();
        if (isPermissionOk) {
            instance.start();
        }
    }

    public static void destroy() {
        Log.i(TAG, "destroy...");
        if (isPermissionOk) {
            instance.stop();
        }
    }

    private LogService() {
    }

    /* access modifiers changed from: private */
    public boolean createLogFileDir(String str) {
        File file = new File(str);
        if (!file.exists()) {
            return file.mkdirs();
        }
        return true;
    }

    private void start() {
        this.mIsStopLog = false;
        if (this.mLogThread == null) {
            this.mLogThread = new Thread(this.mLooperRunnable);
        }
        if (!this.mLogThread.isAlive()) {
            this.mLogThread.start();
        }
    }

    private void stop() {
        this.mIsStopLog = true;
        Thread thread = this.mLogThread;
        if (thread != null && thread.isAlive()) {
            this.mLock.lock();
            this.mCondition.signal();
            this.mLock.unlock();
            this.mLogQueue.clear();
            this.mLogThread = null;
        }
    }

    /* renamed from: e */
    public static void m6212e(String str, String str2, String str3) {
        instance.writeLogToBuffer(str, "E", str2, str3);
    }

    /* renamed from: p */
    public static void m6213p(String str, String str2, String str3) {
        instance.writeLogToBuffer(str, "P", str2, str3);
    }

    private void writeLogToBuffer(String str, String str2, String str3, String str4) {
        if (!isPermissionOk) {
            checkPermission();
            return;
        }
        Thread thread = this.mLogThread;
        if (thread == null || !thread.isAlive() || this.mIsStopLog) {
            start();
        }
        if (TextUtils.isEmpty(getLogPathSdcardDir()) || TextUtils.isEmpty(str)) {
            Log.e(TAG, "getLogPathSdcardDir or dirPath is null");
            return;
        }
        this.mLock.lock();
        this.mLogQueue.add(new String[]{str, "[" + getLogTimeString() + "]" + str4 + LINE_SEP});
        this.mCondition.signal();
        this.mLock.unlock();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006b A[SYNTHETIC, Splitter:B:28:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0071 A[SYNTHETIC, Splitter:B:31:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0036  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeToFile(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r6)
            java.lang.String r6 = java.io.File.separator
            r0.append(r6)
            java.lang.String r6 = r5.getFileName()
            r0.append(r6)
            java.lang.String r6 = r0.toString()
            java.io.File r0 = new java.io.File
            r0.<init>(r6)
            boolean r6 = r0.exists()
            r1 = 1
            java.lang.String r2 = "[IDO_APP] LogService"
            if (r6 != 0) goto L_0x0033
            boolean r6 = r0.createNewFile()     // Catch:{ IOException -> 0x002b }
            goto L_0x0034
        L_0x002b:
            r6 = move-exception
            java.lang.String r6 = r6.toString()
            android.util.Log.e(r2, r6)
        L_0x0033:
            r6 = 1
        L_0x0034:
            if (r6 != 0) goto L_0x003c
            java.lang.String r6 = "create log file failed!"
            android.util.Log.e(r2, r6)
            return
        L_0x003c:
            r6 = 0
            java.io.BufferedWriter r3 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0061 }
            java.io.FileWriter r4 = new java.io.FileWriter     // Catch:{ Exception -> 0x0061 }
            r4.<init>(r0, r1)     // Catch:{ Exception -> 0x0061 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0061 }
            r3.write(r7)     // Catch:{ Exception -> 0x005b, all -> 0x0057 }
            r3.close()     // Catch:{ IOException -> 0x004e }
            goto L_0x006e
        L_0x004e:
            r6 = move-exception
            java.lang.String r6 = r6.toString()
            android.util.Log.e(r2, r6)
            goto L_0x006e
        L_0x0057:
            r6 = move-exception
            r7 = r6
            r6 = r3
            goto L_0x006f
        L_0x005b:
            r6 = move-exception
            r7 = r6
            r6 = r3
            goto L_0x0062
        L_0x005f:
            r7 = move-exception
            goto L_0x006f
        L_0x0061:
            r7 = move-exception
        L_0x0062:
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x005f }
            android.util.Log.e(r2, r7)     // Catch:{ all -> 0x005f }
            if (r6 == 0) goto L_0x006e
            r6.close()     // Catch:{ IOException -> 0x004e }
        L_0x006e:
            return
        L_0x006f:
            if (r6 == 0) goto L_0x007d
            r6.close()     // Catch:{ IOException -> 0x0075 }
            goto L_0x007d
        L_0x0075:
            r6 = move-exception
            java.lang.String r6 = r6.toString()
            android.util.Log.e(r2, r6)
        L_0x007d:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.util.log.LogService.writeToFile(java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public void deleteOutDateLog() {
        ArrayList<String> arrayList = new ArrayList<>();
        if (arrayList.size() != 0) {
            for (String str : arrayList) {
                File file = new File(str);
                if (file.exists()) {
                    File[] listFiles = file.listFiles();
                    for (File file2 : listFiles) {
                        if (!file2.isDirectory()) {
                            Date dateBefore = getDateBefore();
                            if (file2.getName().endsWith(LOG_FILE_PREFIX_NAME) && getFileDateByStr(file2.getName().replace(LOG_FILE_PREFIX_NAME, "")).before(dateBefore)) {
                                file2.delete();
                            }
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    private Date getDateBefore() {
        Calendar instance2 = Calendar.getInstance();
        instance2.set(10, 0);
        instance2.set(12, 0);
        instance2.set(13, 0);
        return instance2.getTime();
    }

    private synchronized Date getFileDateByStr(String str) {
        Date time;
        time = Calendar.getInstance().getTime();
        synchronized (LogService.class) {
            try {
                time = new SimpleDateFormat(FILE_NAME_PATTERN, Locale.getDefault()).parse(str);
            } catch (ParseException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
        return time;
    }

    private synchronized String getFileName() {
        String str;
        Date time = Calendar.getInstance().getTime();
        synchronized (LogService.class) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FILE_NAME_PATTERN, Locale.getDefault());
            str = simpleDateFormat.format(time) + LOG_FILE_PREFIX_NAME;
        }
        return str;
    }

    private synchronized String getLogTimeString() {
        String format;
        Date time = Calendar.getInstance().getTime();
        synchronized (LogService.class) {
            format = new SimpleDateFormat(FILE_TIMESTAMP_PATTERN, Locale.getDefault()).format(time);
        }
        return format;
    }
}
