package com.wade.fit.persenter.sport;

import com.wade.fit.base.IBaseView;
import com.wade.fit.model.bean.SportHistoryVo;

public interface ISportHistoryView extends IBaseView {
    void getByType(SportHistoryVo sportHistoryVo);
}
