package com.wade.fit.model.bean;

import java.io.Serializable;

public class LatLngBean implements Serializable, Cloneable {
    public String currentTimeMillis;
    public boolean isGps;
    public double latitude;
    public double longitude;

    public LatLngBean clone() {
        try {
            return (LatLngBean) super.clone();
        } catch (Exception unused) {
            return null;
        }
    }

    public LatLngBean(double d, double d2) {
        this.latitude = d;
        this.longitude = d2;
    }

    public LatLngBean() {
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double d) {
        this.latitude = d;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double d) {
        this.longitude = d;
    }

    public String getCurrentTimeMillis() {
        return this.currentTimeMillis;
    }

    public void setCurrentTimeMillis(String str) {
        this.currentTimeMillis = str;
    }

    public String toString() {
        return "LatLngBean{latitude=" + this.latitude + ", longitude=" + this.longitude + ", currentTimeMillis=" + this.currentTimeMillis + '}';
    }
}
