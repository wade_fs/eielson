package com.wade.fit.ui.device.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.device.activity.AlarmListActivity_ViewBinding */
public class AlarmListActivity_ViewBinding implements Unbinder {
    private AlarmListActivity target;

    public AlarmListActivity_ViewBinding(AlarmListActivity alarmListActivity) {
        this(alarmListActivity, alarmListActivity.getWindow().getDecorView());
    }

    public AlarmListActivity_ViewBinding(AlarmListActivity alarmListActivity, View view) {
        this.target = alarmListActivity;
        alarmListActivity.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.mRecyclerView, "field 'mRecyclerView'", RecyclerView.class);
    }

    public void unbind() {
        AlarmListActivity alarmListActivity = this.target;
        if (alarmListActivity != null) {
            this.target = null;
            alarmListActivity.mRecyclerView = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
