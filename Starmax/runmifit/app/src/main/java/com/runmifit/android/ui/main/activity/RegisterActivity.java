package com.wade.fit.ui.main.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseMvpActivity;
import com.wade.fit.model.BaseBean;
import com.wade.fit.model.bean.Alarm;
import com.wade.fit.model.bean.UserBean;
import com.wade.fit.ui.mine.activity.PersonUnitActivity;
import com.wade.fit.persenter.main.LoginContract;
import com.wade.fit.persenter.main.LoginPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DataVaildUtil;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.IntentUtil;
import com.wade.fit.util.SharePreferenceUtils;
import com.wade.fit.util.ToastUtil;
import java.util.Locale;

/* renamed from: com.wade.fit.ui.main.activity.RegisterActivity */
public class RegisterActivity extends BaseMvpActivity<LoginPresenter> implements LoginContract.View {
    Button btnRegiest;
    EditText etPwd;
    EditText etSmsCode;
    EditText etUsername;
    Handler handler = new Handler();
    ImageView imgClean;
    ImageView imgStatus;
    /* access modifiers changed from: private */
    public boolean isRunTime = false;
    /* access modifiers changed from: private */
    public int longTime;
    private long screenOffTime;
    private long screenOnTime;
    Runnable timeRun = new Runnable() {
        /* class com.wade.fit.ui.main.activity.RegisterActivity.C25361 */

        public void run() {
            RegisterActivity.access$008(RegisterActivity.this);
            if (RegisterActivity.this.longTime < 60) {
                boolean unused = RegisterActivity.this.isRunTime = true;
                TextView textView = RegisterActivity.this.tvSmsCode;
                textView.setText(RegisterActivity.this.getResources().getString(R.string.time_after_again) + "(" + (60 - RegisterActivity.this.longTime) + ")");
                RegisterActivity.this.tvSmsCode.setBackgroundResource(R.drawable.btn_unclick_bg);
                RegisterActivity registerActivity = RegisterActivity.this;
                registerActivity.postHandler(registerActivity.timeRun, 1000);
                return;
            }
            int unused2 = RegisterActivity.this.longTime = 0;
            boolean unused3 = RegisterActivity.this.isRunTime = false;
            RegisterActivity.this.tvSmsCode.setClickable(true);
            RegisterActivity.this.tvSmsCode.setBackgroundResource(R.drawable.btn_submit_bg);
            RegisterActivity.this.tvSmsCode.setText(RegisterActivity.this.getResources().getString(R.string.send_code));
            RegisterActivity.this.handler.removeCallbacks(this);
        }
    };
    TextView tvSmsCode;
    private String userName;
    private String userPwd;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_register;
    }

    public void loginFaild(int i) {
    }

    public void registerFaild(int i) {
    }

    static /* synthetic */ int access$008(RegisterActivity registerActivity) {
        int i = registerActivity.longTime;
        registerActivity.longTime = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        super.initView();
        this.layoutTitle.setBackgroundColor(getResources().getColor(R.color.white));
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(9216);
        }
        this.bgView.setVisibility(View.GONE);
        this.titleBack.setImageResource(R.mipmap.back_black);
        this.btnRegiest.setClickable(false);
        this.etUsername.setText(getIntent().getExtras().getString("email"));
    }

    /* access modifiers changed from: package-private */
    public void textChanged() {
        if (TextUtils.isEmpty(this.etUsername.getText().toString())) {
            this.imgClean.setVisibility(View.GONE);
        } else {
            this.imgClean.setVisibility(View.VISIBLE);
        }
        if (TextUtils.isEmpty(this.etUsername.getText().toString()) || TextUtils.isEmpty(this.etPwd.getText().toString()) || this.etPwd.getText().toString().length() < 6 || TextUtils.isEmpty(this.etSmsCode.getText().toString())) {
            this.btnRegiest.setClickable(false);
            this.btnRegiest.setBackgroundResource(R.drawable.btn_submit_normle);
            return;
        }
        this.btnRegiest.setClickable(true);
        this.btnRegiest.setBackgroundResource(R.drawable.btn_submit_bg);
    }

    /* access modifiers changed from: private */
    public void postHandler(Runnable runnable, long j) {
        this.handler.postDelayed(runnable, j);
    }

    /* access modifiers changed from: package-private */
    public void updatePwdStatus() {
        if (this.imgStatus.getTag().equals("open")) {
            this.imgStatus.setTag("close");
            this.imgStatus.setImageResource(R.mipmap.pwd_open);
            this.etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else if (this.imgStatus.getTag().equals("close")) {
            this.imgStatus.setTag("open");
            this.imgStatus.setImageResource(R.mipmap.pwd_close);
            this.etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        EditText editText = this.etPwd;
        editText.setSelection(editText.getText().length());
    }

    /* access modifiers changed from: package-private */
    public void cleanUserName() {
        this.etUsername.setText("");
    }

    /* access modifiers changed from: package-private */
    public void resiestAccount() {
        if (!ButtonUtils.isFastDoubleClick(R.id.btnRegiest, 1000)) {
            this.userName = this.etUsername.getText().toString();
            this.userPwd = this.etPwd.getText().toString();
            if (!DataVaildUtil.vaildEmailAndPwd(this, this.userName, this.userPwd)) {
                return;
            }
            if (!TextUtils.isEmpty(this.etSmsCode.getText().toString())) {
                showWebViewDialog();
            } else {
                ToastUtil.showToast(getResources().getString(R.string.input_validate_code));
            }
        }
    }

    private void showWebViewDialog() {
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.activity_web_view, (ViewGroup) null);
        final Dialog dialog = new Dialog(this, R.style.theme_dialog_aphe);
        WebView webView = (WebView) inflate.findViewById(R.id.wv_privacy_policy);
        inflate.findViewById(R.id.btnAgree).setOnClickListener(new View.OnClickListener(dialog) {
            /* class com.wade.fit.ui.main.activity.$$Lambda$RegisterActivity$IlMS4vFlpG360EBusIxN42hYJCg */
            private final /* synthetic */ Dialog f$1;

            {
                this.f$1 = r2;
            }

            public final void onClick(View view) {
                RegisterActivity.this.lambda$showWebViewDialog$0$RegisterActivity(this.f$1, view);
            }
        });
        inflate.findViewById(R.id.btnCancal).setOnClickListener(new View.OnClickListener(dialog) {
            /* class com.wade.fit.ui.main.activity.$$Lambda$RegisterActivity$uvgjGcyiz2MUuwcITuIkgPPdclg */
            private final /* synthetic */ Dialog f$0;

            {
                this.f$0 = r1;
            }

            public final void onClick(View view) {
                this.f$0.dismiss();
            }
        });
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            /* class com.wade.fit.ui.main.activity.RegisterActivity.C25372 */

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                return true;
            }

            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                AlertDialog.Builder builder = new AlertDialog.Builder(webView.getContext());
                builder.setMessage("SSL认证失败，是否继续访问？");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener(sslErrorHandler) {
                    /* class com.wade.fit.ui.main.activity.$$Lambda$RegisterActivity$2$NolVCNFtc1RX2xwH39dESAlZhic */
                    private final /* synthetic */ SslErrorHandler f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void onClick(DialogInterface dialogInterface, int i) {
                        this.f$0.proceed();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener(sslErrorHandler) {
                    /* class com.wade.fit.ui.main.activity.$$Lambda$RegisterActivity$2$k5lztyP0sDgaavbmdJOVqOZcT80 */
                    private final /* synthetic */ SslErrorHandler f$0;

                    {
                        this.f$0 = r1;
                    }

                    public final void onClick(DialogInterface dialogInterface, int i) {
                        this.f$0.cancel();
                    }
                });
                builder.create().show();
            }

            public void onPageFinished(WebView webView, String str) {
                dialog.show();
            }
        });
        String language = Locale.getDefault().getLanguage();
        webView.loadUrl((language.contains("CN") || language.contains("zh")) ? "file:///android_asset/privacypolicy.html" : (language.contains("TW") || language.contains("HK")) ? "file:///android_asset/privacypolicy_tw.html" : language.contains("fr") ? "file:///android_asset/privacypolicy_fr.html" : language.contains("es") ? "file:///android_asset/privacypolicy_es.html" : language.contains("de") ? "file:///android_asset/privacypolicy_de.html" : language.contains("ja") ? "file:///android_asset/privacypolicy_ja.html" : language.contains("it") ? "file:///android_asset/privacypolicy_it.html" : "file:///android_asset/privacypolicyEh.html");
        dialog.setContentView(inflate);
        dialog.setCancelable(true);
        dialog.show();
    }

    public /* synthetic */ void lambda$showWebViewDialog$0$RegisterActivity(Dialog dialog, View view) {
        dialog.dismiss();
        ((LoginPresenter) this.mPresenter).regiestAccount(this.userName, this.userPwd, this.etSmsCode.getText().toString());
    }

    /* access modifiers changed from: package-private */
    public void sendCode() {
        if (!ButtonUtils.isFastDoubleClick(R.id.tvSmsCode, 1000)) {
            String obj = this.etUsername.getText().toString();
            if (DataVaildUtil.validEmail(this, obj)) {
                this.tvSmsCode.setClickable(false);
                ((LoginPresenter) this.mPresenter).sendCode(obj);
            }
        }
    }

    public void LoginSuccess(int i, BaseBean baseBean) {
        UserBean userBean = (UserBean) baseBean.getData();
        SharePreferenceUtils.putInt(this, Constants.LOGIN_TYPE, 1);
        SharePreferenceUtils.putString(Constants.USER_NAME, this.userName);
        SharePreferenceUtils.putString(Constants.USER_PASSWORD, this.userPwd);
        SharePreferenceUtils.putString(Constants.USER_TOKEN, userBean.getToken());
        userBean.setYear(DateUtil.todayYearMonthDay()[0] - 20);
        userBean.setMonth(1);
        userBean.setDay(1);
        userBean.setBirthday(userBean.getYear() + "-" + userBean.getMonth() + "-" + userBean.getDay());
        userBean.setUnit(0);
        userBean.setHeight(Alarm.STATUS_NOT_DISPLAY);
        userBean.setHeightLb(68);
        userBean.setWeight(Constants.DEFAULT_WEIGHT_KG);
        userBean.setWeightLb(132);
        userBean.setWeightSt(10);
        userBean.setStepDistance(75);
        AppApplication.getInstance().setUserBean(userBean);
        AppApplication.getInstance().setDatabase();
        IntentUtil.goToActivityAndFinish(this, PersonUnitActivity.class);
    }

    public void registerSuccess() {
        ((LoginPresenter) this.mPresenter).requestLogin(this.userName, this.userPwd);
    }

    public void sendCodeSuccess() {
        postHandler(this.timeRun, 1000);
    }

    public void sendCodeFaild(int i) {
        this.tvSmsCode.setClickable(true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.screenOffTime = System.currentTimeMillis();
        this.handler.removeCallbacks(this.timeRun);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.isRunTime) {
            this.handler.removeCallbacks(this.timeRun);
            this.screenOnTime = System.currentTimeMillis();
            this.longTime = (int) (((long) this.longTime) + ((this.screenOnTime - this.screenOffTime) / 1000));
            postHandler(this.timeRun, 1000);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.handler.removeCallbacks(this.timeRun);
    }
}
