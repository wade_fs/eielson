package com.wade.fit.ui.device.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.base.BaseViewHolder;
import com.wade.fit.base.refresh.BaseDeleteAdapter;
import com.wade.fit.model.bean.Alarm;
import com.wade.fit.ui.device.adapter.AlarmListAdapter;
import com.wade.fit.util.TimeUtil;
import com.wade.fit.views.CustomToggleButton;
import com.wade.fit.views.RvSlideLayout;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.adapter.AlarmListAdapter */
public class AlarmListAdapter extends BaseDeleteAdapter<Alarm, ViewHolder> {
    final String[] amOrPm = {this.mContext.getResources().getString(R.string.am), this.mContext.getResources().getString(R.string.pm)};
    String[] weekArray = new String[0];

    /* renamed from: com.wade.fit.ui.device.adapter.AlarmListAdapter$ViewHolder_ViewBinding */
    public class ViewHolder_ViewBinding implements Unbinder {
        private ViewHolder target;

        public ViewHolder_ViewBinding(ViewHolder viewHolder, View view) {
            this.target = viewHolder;
            viewHolder.alarmTime = (TextView) Utils.findRequiredViewAsType(view, R.id.alarm_time, "field 'alarmTime'", TextView.class);
            viewHolder.alarmCycle = (TextView) Utils.findRequiredViewAsType(view, R.id.alarm_cycle, "field 'alarmCycle'", TextView.class);
            viewHolder.alarmText = (TextView) Utils.findRequiredViewAsType(view, R.id.alarm_text, "field 'alarmText'", TextView.class);
            viewHolder.alarmSwitch = (CustomToggleButton) Utils.findRequiredViewAsType(view, R.id.alarm_switch, "field 'alarmSwitch'", CustomToggleButton.class);
            viewHolder.layoutDelete = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.layout_delete, "field 'layoutDelete'", LinearLayout.class);
            viewHolder.layoutItme = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.layoutItme, "field 'layoutItme'", RelativeLayout.class);
            viewHolder.alarmIcon = (ImageView) Utils.findRequiredViewAsType(view, R.id.alarm_icon, "field 'alarmIcon'", ImageView.class);
            viewHolder.slidlayout = (RvSlideLayout) Utils.findRequiredViewAsType(view, R.id.slidlayout, "field 'slidlayout'", RvSlideLayout.class);
            viewHolder.layoutContent = (RelativeLayout) Utils.findRequiredViewAsType(view, R.id.layoutContent, "field 'layoutContent'", RelativeLayout.class);
        }

        public void unbind() {
            ViewHolder viewHolder = this.target;
            if (viewHolder != null) {
                this.target = null;
                viewHolder.alarmTime = null;
                viewHolder.alarmCycle = null;
                viewHolder.alarmText = null;
                viewHolder.alarmSwitch = null;
                viewHolder.layoutDelete = null;
                viewHolder.layoutItme = null;
                viewHolder.alarmIcon = null;
                viewHolder.slidlayout = null;
                viewHolder.layoutContent = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public AlarmListAdapter(Context context, List<Alarm> list) {
        super(context, list);
        this.weekArray = context.getResources().getStringArray(R.array.weekDay);
    }

    /* access modifiers changed from: protected */
    public void onNormalBindViewHolder(ViewHolder viewHolder, Alarm alarm, int i) {
        int i2;
        viewHolder.alarmTime.setText(TimeUtil.timeFormatter(alarm.getAlarmHour(), alarm.getAlarmMinute(), TimeUtil.is24Hour(this.mContext), this.amOrPm));
        viewHolder.alarmText.setText("");
        viewHolder.alarmSwitch.setSwitchState(alarm.getOn_off());
        StringBuffer stringBuffer = new StringBuffer();
        if (alarm.getWeekRepeat()[0]) {
            stringBuffer.append(this.weekArray[1] + " ");
            i2 = 1;
        } else {
            i2 = 0;
        }
        if (alarm.getWeekRepeat()[1]) {
            i2++;
            stringBuffer.append(this.weekArray[2] + " ");
        }
        if (alarm.getWeekRepeat()[2]) {
            i2++;
            stringBuffer.append(this.weekArray[3] + " ");
        }
        if (alarm.getWeekRepeat()[3]) {
            i2++;
            stringBuffer.append(this.weekArray[4] + " ");
        }
        if (alarm.getWeekRepeat()[4]) {
            i2++;
            stringBuffer.append(this.weekArray[5] + " ");
        }
        if (alarm.getWeekRepeat()[5]) {
            i2++;
            stringBuffer.append(this.weekArray[6] + " ");
        }
        if (alarm.getWeekRepeat()[6]) {
            i2++;
            stringBuffer.append(this.weekArray[0] + " ");
        }
        if (stringBuffer.length() <= 0) {
            TextView textView = viewHolder.alarmCycle;
            textView.setText(setAlarmText(alarm.getAlarmType()) + "," + this.mContext.getResources().getString(R.string.alarm_once));
        } else if (i2 == 7) {
            TextView textView2 = viewHolder.alarmCycle;
            textView2.setText(setAlarmText(alarm.getAlarmType()) + "," + this.mContext.getResources().getString(R.string.alarm_every_day));
        } else {
            TextView textView3 = viewHolder.alarmCycle;
            textView3.setText(setAlarmText(alarm.getAlarmType()) + "," + stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1));
        }
        switch (alarm.getAlarmType()) {
            case 0:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_0);
                break;
            case 1:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_1);
                break;
            case 2:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_2);
                break;
            case 3:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_3);
                break;
            case 4:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_4);
                break;
            case 5:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_5);
                break;
            case 6:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_6);
                break;
            case 7:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_7);
                break;
            case 8:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_8);
                break;
            case 9:
                viewHolder.alarmIcon.setImageResource(R.mipmap.remind_type_9);
                break;
        }
        viewHolder.alarmSwitch.setOnSwitchListener(new CustomToggleButton.OnSwitchListener(viewHolder, i) {
            /* class com.wade.fit.ui.device.adapter.$$Lambda$AlarmListAdapter$hrlN0E7DUkjsWHxKkZL2JeBiuaQ */
            private final /* synthetic */ AlarmListAdapter.ViewHolder f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onSwitched(boolean z) {
                AlarmListAdapter.this.lambda$onNormalBindViewHolder$0$AlarmListAdapter(this.f$1, this.f$2, z);
            }
        });
        viewHolder.layoutDelete.setOnClickListener(new View.OnClickListener(viewHolder, i) {
            /* class com.wade.fit.ui.device.adapter.$$Lambda$AlarmListAdapter$eYXldbISVHlajqllI2c9WD5xMk */
            private final /* synthetic */ AlarmListAdapter.ViewHolder f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                AlarmListAdapter.this.lambda$onNormalBindViewHolder$1$AlarmListAdapter(this.f$1, this.f$2, view);
            }
        });
        viewHolder.slidlayout.setOnClickListener(new View.OnClickListener(viewHolder, i) {
            /* class com.wade.fit.ui.device.adapter.$$Lambda$AlarmListAdapter$QY0vUzaEJkti11QmmW84ORzbhg */
            private final /* synthetic */ AlarmListAdapter.ViewHolder f$1;
            private final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            public final void onClick(View view) {
                AlarmListAdapter.this.lambda$onNormalBindViewHolder$2$AlarmListAdapter(this.f$1, this.f$2, view);
            }
        });
    }

    public /* synthetic */ void lambda$onNormalBindViewHolder$0$AlarmListAdapter(ViewHolder viewHolder, int i, boolean z) {
        this.customClickListener.onCustomClick(viewHolder.alarmSwitch, i);
    }

    public /* synthetic */ void lambda$onNormalBindViewHolder$1$AlarmListAdapter(ViewHolder viewHolder, int i, View view) {
        closeOpenMenu();
        this.customClickListener.onCustomClick(viewHolder.layoutDelete, i);
    }

    public /* synthetic */ void lambda$onNormalBindViewHolder$2$AlarmListAdapter(ViewHolder viewHolder, int i, View view) {
        this.mOnItemClickListener.onItemClick(viewHolder.itemView, i);
    }

    private String setAlarmText(int i) {
        String[] stringArray = this.mContext.getResources().getStringArray(R.array.alarmType);
        if (i <= 9) {
            return stringArray[i];
        }
        return stringArray[0];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ViewHolder(this.inflater.inflate((int) R.layout.item_alarm_list, viewGroup, false));
    }

    /* renamed from: com.wade.fit.ui.device.adapter.AlarmListAdapter$ViewHolder */
    public class ViewHolder extends BaseViewHolder {
        TextView alarmCycle;
        ImageView alarmIcon;
        CustomToggleButton alarmSwitch;
        TextView alarmText;
        TextView alarmTime;
        RelativeLayout layoutContent;
        LinearLayout layoutDelete;
        RelativeLayout layoutItme;
        RvSlideLayout slidlayout;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
