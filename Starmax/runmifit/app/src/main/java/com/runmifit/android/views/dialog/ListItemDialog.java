package com.wade.fit.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wade.fit.R;
import java.util.ArrayList;
import java.util.List;

public class ListItemDialog extends Dialog {
    private Button cancel;
    private boolean haveTitle = false;
    private boolean isSetTextDelRed = true;
    /* access modifiers changed from: private */
    public List<SpannableString> itemSS;
    /* access modifiers changed from: private */
    public List<String> items;
    private AdapterView.OnItemClickListener listener;
    /* access modifiers changed from: private */
    public Context mContext;
    private ListView mListView;
    private String title;

    public boolean haveTitle() {
        return this.haveTitle;
    }

    public void setItemSS(SpannableString[] spannableStringArr) {
        this.itemSS = new ArrayList();
        for (SpannableString spannableString : spannableStringArr) {
            this.itemSS.add(spannableString);
        }
    }

    public ListItemDialog(Context context, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        super(context, z, onCancelListener);
    }

    public ListItemDialog(Context context, int i) {
        super(context, R.style.basedialog_style);
    }

    public ListItemDialog(Context context, String str, String[] strArr, AdapterView.OnItemClickListener onItemClickListener) {
        super(context, R.style.basedialog_style);
        this.mContext = context;
        this.title = str;
        this.items = new ArrayList();
        if (!TextUtils.isEmpty(str)) {
            this.haveTitle = true;
            this.items.add(0, str);
        }
        for (String str2 : strArr) {
            this.items.add(str2);
        }
        this.listener = onItemClickListener;
    }

    public void setSetTextDelRed(boolean z) {
        this.isSetTextDelRed = z;
    }

    public ListItemDialog(Context context, String str, List<String> list, AdapterView.OnItemClickListener onItemClickListener) {
        super(context, R.style.basedialog_style);
        this.mContext = context;
        this.title = str;
        this.items = new ArrayList();
        if (!TextUtils.isEmpty(str)) {
            this.haveTitle = true;
            this.items.add(0, str);
        }
        for (String str2 : list) {
            this.items.add(str2);
        }
        this.listener = onItemClickListener;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_setting_view);
        this.cancel = (Button) findViewById(R.id.setting_dialog_btn);
        this.cancel.setOnClickListener(new View.OnClickListener() {
            /* class com.wade.fit.views.dialog.ListItemDialog.C27291 */

            public void onClick(View view) {
                ListItemDialog.this.dismiss();
            }
        });
        this.mListView = (ListView) findViewById(R.id.seting_dialog_listview);
        this.mListView.setAdapter((android.widget.ListAdapter) new ListAdapter(this.haveTitle, this.mContext));
        this.mListView.setOnItemClickListener(this.listener);
        setCanceledOnTouchOutside(true);
        setCancelable(true);
    }

    class ListAdapter extends BaseAdapter {
        private Context context;
        private boolean haveTitle = false;
        private LayoutInflater mInflater;

        public long getItemId(int i) {
            return (long) i;
        }

        public ListAdapter(boolean z, Context context2) {
            this.mInflater = LayoutInflater.from(ListItemDialog.this.mContext);
            this.haveTitle = z;
            this.context = context2;
        }

        public int getCount() {
            return ListItemDialog.this.items.size();
        }

        public Object getItem(int i) {
            return ListItemDialog.this.items.get(i);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            ViewHolder viewHolder;
            if (view == null) {
                viewHolder = new ViewHolder();
                view2 = this.mInflater.inflate((int) R.layout.layout_setting_item_view, (ViewGroup) null);
                viewHolder.mTextView = (TextView) view2.findViewById(R.id.setting_dialog_tv);
                viewHolder.mTitleView = (TextView) view2.findViewById(R.id.setting_dialog_title);
                viewHolder.mLayout = (RelativeLayout) view2.findViewById(R.id.setting_dialog_layout);
                view2.setTag(viewHolder);
            } else {
                view2 = view;
                viewHolder = (ViewHolder) view.getTag();
            }
            if (i == 0) {
                if (this.haveTitle) {
                    viewHolder.mTitleView.setVisibility(View.VISIBLE);
                    viewHolder.mTextView.setVisibility(View.GONE);
                    viewHolder.mTitleView.setText((CharSequence) ListItemDialog.this.items.get(i));
                    viewHolder.mLayout.setBackgroundResource(R.drawable.settingdialog_topbtn);
                } else {
                    viewHolder.mLayout.setBackgroundResource(R.drawable.settingdialog_topbtn_selector);
                }
            } else if (ListItemDialog.this.items.size() - 1 == i) {
                viewHolder.mTitleView.setVisibility(View.GONE);
                viewHolder.mTextView.setVisibility(View.VISIBLE);
                viewHolder.mLayout.setBackgroundResource(R.drawable.settingdialog_bottombtn_selector);
            } else if (ListItemDialog.this.items.size() - 1 == 1) {
                viewHolder.mTitleView.setVisibility(View.GONE);
                viewHolder.mTextView.setVisibility(View.VISIBLE);
                viewHolder.mLayout.setBackgroundResource(R.drawable.settingdialog_middlebtn_selector);
            } else {
                viewHolder.mTitleView.setVisibility(View.GONE);
                viewHolder.mTextView.setVisibility(View.VISIBLE);
                viewHolder.mLayout.setBackgroundResource(R.drawable.settingdialog_centrebtn_selector);
            }
            if (ListItemDialog.this.items.size() == 1) {
                viewHolder.mTitleView.setVisibility(View.GONE);
                viewHolder.mTextView.setVisibility(View.VISIBLE);
                viewHolder.mLayout.setBackgroundResource(R.drawable.settingdialog_middlebtn_selector);
            }
            if (ListItemDialog.this.itemSS == null || i >= ListItemDialog.this.itemSS.size()) {
                viewHolder.mTextView.setText((CharSequence) ListItemDialog.this.items.get(i));
            } else {
                viewHolder.mTextView.setText((CharSequence) ListItemDialog.this.itemSS.get(i));
            }
            return view2;
        }

        class ViewHolder {
            RelativeLayout mLayout;
            TextView mTextView;
            TextView mTitleView;

            ViewHolder() {
            }
        }
    }

    public void show() {
        getWindow().setGravity(80);
        super.show();
    }
}
