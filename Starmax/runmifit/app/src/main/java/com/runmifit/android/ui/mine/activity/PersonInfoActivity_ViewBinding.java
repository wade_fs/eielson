package com.wade.fit.ui.mine.activity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.PersonInfoActivity_ViewBinding */
public class PersonInfoActivity_ViewBinding implements Unbinder {
    private PersonInfoActivity target;
    private View view2131296335;
    private View view2131297017;
    private View view2131297082;

    public PersonInfoActivity_ViewBinding(PersonInfoActivity personInfoActivity) {
        this(personInfoActivity, personInfoActivity.getWindow().getDecorView());
    }

    public PersonInfoActivity_ViewBinding(final PersonInfoActivity personInfoActivity, View view) {
        this.target = personInfoActivity;
        View findRequiredView = Utils.findRequiredView(view, R.id.btnSave, "field 'btnSave' and method 'toNext'");
        personInfoActivity.btnSave = (Button) Utils.castView(findRequiredView, R.id.btnSave, "field 'btnSave'", Button.class);
        this.view2131296335 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.PersonInfoActivity_ViewBinding.C25891 */

            public void doClick(View view) {
                personInfoActivity.toNext();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.tvHeight, "field 'tvHeight' and method 'selectHeight'");
        personInfoActivity.tvHeight = (TextView) Utils.castView(findRequiredView2, R.id.tvHeight, "field 'tvHeight'", TextView.class);
        this.view2131297017 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.PersonInfoActivity_ViewBinding.C25902 */

            public void doClick(View view) {
                personInfoActivity.selectHeight();
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.tvWeight, "field 'tvWeight' and method 'selectWeight'");
        personInfoActivity.tvWeight = (TextView) Utils.castView(findRequiredView3, R.id.tvWeight, "field 'tvWeight'", TextView.class);
        this.view2131297082 = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.mine.activity.PersonInfoActivity_ViewBinding.C25913 */

            public void doClick(View view) {
                personInfoActivity.selectWeight();
            }
        });
    }

    public void unbind() {
        PersonInfoActivity personInfoActivity = this.target;
        if (personInfoActivity != null) {
            this.target = null;
            personInfoActivity.btnSave = null;
            personInfoActivity.tvHeight = null;
            personInfoActivity.tvWeight = null;
            this.view2131296335.setOnClickListener(null);
            this.view2131296335 = null;
            this.view2131297017.setOnClickListener(null);
            this.view2131297017 = null;
            this.view2131297082.setOnClickListener(null);
            this.view2131297082 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
