package com.wade.fit.views;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.AttributeSet;
import android.view.View;
import com.wade.fit.R;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.model.bean.DetailType;
import com.wade.fit.util.ScreenUtil;
import com.wade.fit.util.ViewUtil;
import com.wade.fit.util.log.DebugLog;
import java.util.ArrayList;
import java.util.List;

public class DetailSportChart extends View {
    private final int ANIM2PROGRESS_MAX = 30;
    private int MAX_VALUE;
    private int MIDDLE_VALUE;
    private int MIN_VALUE;
    private ObjectAnimator anim1;
    private int anim1Progress;
    private ObjectAnimator anim2;
    private int anim2Progress;
    private ObjectAnimator anim3;
    private boolean animing1 = true;
    private boolean animing2;
    private boolean animing3;
    private int barColor;
    private int barNum = 96;
    private Paint barPaint;
    private float barWid;
    private float barWidth;
    private int bitmapH;
    private int bitmapW;
    private float bottomH;
    private float centerH;
    private Paint dataPaint;
    DetailType detailType;
    private int exelColor = 1006632959;
    Paint exelPaint = new Paint(1);
    private Bitmap finishAfterBitmap;
    private Bitmap finishBeforeBitmap;
    private List<HealthSportItem> healthSportItems = new ArrayList();
    private boolean initDraw;
    boolean isAddAvgText = false;
    private boolean isDouble;
    private boolean isDrawTAB;
    private boolean isToday;
    private float[] lineHeight = new float[3];
    private int mHeight;
    private int mWidth;
    private int middleIndex;
    float paddingTop;
    private NinePatchDrawable popup;
    private NinePatchDrawable popupCenter;
    private NinePatchDrawable popupLeft;
    private NinePatchDrawable popupRight;
    private float radTopPadding;
    private RectF selectRect = new RectF();
    private Bitmap sportBitmap;
    private float sportToOneLineH;
    private int tempSteps = 0;
    private int textColor;
    private Paint textPaint;
    private float textSize;
    private Drawable topDrawable;
    private float topH;
    private Bitmap touchBg;
    Paint touchBgPaint;
    private Paint touchPaint;
    private Paint touchPaint2;
    private float touchX;
    private float touchY;
    private Paint unitPaint;
    private List<String> xLables = new ArrayList();
    private float xOffSet;
    private float xWidth;
    private float yAxisLength;
    private float[] yHeight = new float[4];
    private float yScale;
    private int[] yVelocitys;
    private float yZero;

    public DetailSportChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SportBarChart);
        this.textSize = obtainStyledAttributes.getDimension(0, 28.0f);
        this.textColor = obtainStyledAttributes.getColor(1, -1);
        this.barColor = obtainStyledAttributes.getColor(2, -1092544);
        this.topDrawable = obtainStyledAttributes.getDrawable(3);
        obtainStyledAttributes.recycle();
        initPaint();
        this.sportBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.sport_bar_chart_top);
        this.popupLeft = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup_left);
        this.popupRight = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup_right);
        this.popupCenter = (NinePatchDrawable) getResources().getDrawable(R.mipmap.popup);
        this.touchBg = BitmapFactory.decodeResource(getResources(), R.drawable.detail_touch_bg);
        this.finishBeforeBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.finish_before);
        this.finishAfterBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.finish_after);
        this.radTopPadding = (float) (this.finishBeforeBitmap.getHeight() > this.finishBeforeBitmap.getWidth() ? this.finishBeforeBitmap.getHeight() : this.finishBeforeBitmap.getWidth());
    }

    private void initPaint() {
        this.barPaint = new Paint(1);
        this.barPaint.setColor(this.barColor);
        this.touchPaint = new Paint(1);
        this.touchBgPaint = new Paint(1);
        this.touchPaint2 = new Paint(1);
        this.touchPaint.setColor(getResources().getColor(R.color.white));
        this.touchPaint2.setColor(getResources().getColor(R.color.white));
        this.textPaint = new Paint(1);
        this.textPaint.setColor(this.textColor);
        this.textPaint.setTextAlign(Paint.Align.CENTER);
        this.textPaint.setTextSize(this.textSize);
        this.dataPaint = new Paint(1);
        this.dataPaint.setColor(this.textColor);
        this.dataPaint.setTextAlign(Paint.Align.RIGHT);
        this.dataPaint.setTextSize(this.textSize * 1.8f);
        this.unitPaint = new Paint(1);
        this.unitPaint.setColor(this.textColor);
        this.unitPaint.setTextSize(this.textSize);
        this.unitPaint.setTextAlign(Paint.Align.LEFT);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.mWidth = i;
        this.mHeight = i2;
        calculate();
        initYscale();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000b, code lost:
        if (r0 != 3) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r4) {
        /*
            r3 = this;
            int r0 = r4.getAction()
            r1 = 1
            if (r0 == r1) goto L_0x001b
            r2 = 2
            if (r0 == r2) goto L_0x000e
            r4 = 3
            if (r0 == r4) goto L_0x001b
            goto L_0x0021
        L_0x000e:
            float r0 = r4.getX()
            r3.touchX = r0
            float r4 = r4.getX()
            r3.touchY = r4
            goto L_0x0021
        L_0x001b:
            r4 = -1082130432(0xffffffffbf800000, float:-1.0)
            r3.touchX = r4
            r3.touchY = r4
        L_0x0021:
            r3.invalidate()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.DetailSportChart.onTouchEvent(android.view.MotionEvent):boolean");
    }

    private void calculate() {
        this.bitmapH = this.sportBitmap.getHeight();
        this.bitmapW = this.sportBitmap.getWidth();
        this.topH = (float) this.bitmapH;
        this.bottomH = ViewUtil.getTextHeight(this.dataPaint);
        this.paddingTop = (float) ScreenUtil.dip2px(100.0f);
        int i = this.mHeight;
        float f = this.topH;
        this.centerH = ((((float) i) - f) - this.bottomH) - this.paddingTop;
        this.sportToOneLineH = (float) (i / 30);
        float[] fArr = this.yHeight;
        float f2 = this.sportToOneLineH;
        fArr[0] = f2;
        fArr[1] = f2 + f;
        fArr[2] = f + this.centerH;
        fArr[3] = (float) (i - ScreenUtil.dp2px(3.0f, (Activity) getContext()));
        float[] fArr2 = this.lineHeight;
        float f3 = this.topH;
        float f4 = this.sportToOneLineH;
        fArr2[0] = f3 + f4 + f4;
        float f5 = fArr2[0];
        float f6 = this.centerH;
        fArr2[1] = (f5 + (f6 / 2.0f)) - f4;
        fArr2[2] = (f3 + (f6 - f4)) - f4;
        this.barPaint.setTextSize(this.textSize);
        this.xOffSet = ViewUtil.getTextRectWidth(this.barPaint, "100");
        this.barWidth = (((float) this.mWidth) - (this.xOffSet * 3.0f)) / ((float) this.barNum);
        DebugLog.m6203d("mWidth= " + this.mWidth + ",xOffSet= " + this.xOffSet + ",(mWidth - 2 * xOffSet)= " + (((float) this.mWidth) - (this.xOffSet * 2.0f)) + ",barWidth= " + this.barWidth + ",barWid= " + this.barWid);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.exelPaint.setColor(this.exelColor);
        this.barPaint.setShader(null);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.exelPaint);
        drawY(canvas);
        drawX(canvas);
        drawXLine(canvas);
    }

    private void drawXLine(Canvas canvas) {
        this.barPaint.setStrokeWidth(1.0f);
        this.barPaint.setShader(null);
        float f = this.xOffSet;
        float[] fArr = this.lineHeight;
        float f2 = fArr[2];
        float f3 = this.yScale;
        int i = this.MAX_VALUE;
        float f4 = f2 - (((float) i) * f3);
        float f5 = this.paddingTop;
        canvas.drawLine(f * 1.5f, f4 + f5, ((float) this.mWidth) - (f * 1.5f), (fArr[2] - (f3 * ((float) i))) + f5, this.barPaint);
        float f6 = this.xOffSet;
        float[] fArr2 = this.lineHeight;
        float f7 = fArr2[2];
        float f8 = this.yScale;
        int i2 = this.MIDDLE_VALUE;
        float f9 = f7 - (((float) i2) * f8);
        float f10 = this.paddingTop;
        canvas.drawLine(f6 * 1.5f, f9 + f10, ((float) this.mWidth) - (f6 * 1.5f), (fArr2[2] - (f8 * ((float) i2))) + f10, this.barPaint);
        float f11 = this.xOffSet;
        float[] fArr3 = this.lineHeight;
        float f12 = fArr3[2];
        float f13 = this.yScale;
        int i3 = this.MIN_VALUE;
        float f14 = f12 - (((float) i3) * f13);
        float f15 = this.paddingTop;
        canvas.drawLine(f11 * 1.5f, f14 + f15, ((float) this.mWidth) - (f11 * 1.5f), (fArr3[2] - (f13 * ((float) i3))) + f15, this.barPaint);
        float f16 = this.xOffSet;
        float[] fArr4 = this.lineHeight;
        float f17 = fArr4[2];
        float f18 = this.paddingTop;
        canvas.drawLine(f16 * 1.5f, f17 + f18, ((float) this.mWidth) - (f16 * 1.5f), fArr4[2] + f18, this.barPaint);
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x03a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void drawX(android.graphics.Canvas r32) {
        /*
            r31 = this;
            r0 = r31
            r7 = r32
            float r1 = r0.xOffSet
            r8 = 1069547520(0x3fc00000, float:1.5)
            float r2 = r1 * r8
            float[] r3 = r0.lineHeight
            r9 = 2
            r4 = r3[r9]
            float r5 = r0.paddingTop
            float r4 = r4 + r5
            int r6 = r0.mWidth
            float r6 = (float) r6
            float r1 = r1 * r8
            float r6 = r6 - r1
            r1 = r3[r9]
            float r5 = r5 + r1
            android.graphics.Paint r10 = r0.barPaint
            r1 = r32
            r3 = r4
            r4 = r6
            r6 = r10
            r1.drawLine(r2, r3, r4, r5, r6)
            android.graphics.Paint r1 = r0.barPaint
            r2 = 0
            r1.setShader(r2)
            int r1 = r0.mWidth
            float r1 = (float) r1
            float r2 = r0.xOffSet
            r3 = 1077936128(0x40400000, float:3.0)
            float r2 = r2 * r3
            float r1 = r1 - r2
            r0.xWidth = r1
            java.util.List<java.lang.String> r1 = r0.xLables
            int r1 = r1.size()
            float r2 = r0.xWidth
            int r3 = r1 + -1
            float r3 = (float) r3
            float r2 = r2 / r3
            android.graphics.Paint r3 = r0.barPaint
            float r3 = com.wade.fit.util.ViewUtil.getTextHeight(r3)
            android.graphics.Paint r4 = r0.barPaint
            android.content.res.Resources r5 = r31.getResources()
            r10 = 2131099755(0x7f06006b, float:1.7811872E38)
            int r5 = r5.getColor(r10)
            r4.setColor(r5)
            r4 = 0
        L_0x005a:
            if (r4 >= r1) goto L_0x007c
            float r5 = (float) r4
            float r5 = r5 * r2
            float r6 = r0.xOffSet
            float r6 = r6 * r8
            float r5 = r5 + r6
            java.util.List<java.lang.String> r6 = r0.xLables
            java.lang.Object r6 = r6.get(r4)
            java.lang.String r6 = (java.lang.String) r6
            float[] r12 = r0.lineHeight
            r12 = r12[r9]
            float r12 = r12 + r3
            float r13 = r0.paddingTop
            float r12 = r12 + r13
            android.graphics.Paint r13 = r0.barPaint
            r7.drawText(r6, r5, r12, r13)
            int r4 = r4 + 1
            goto L_0x005a
        L_0x007c:
            android.graphics.Paint r1 = r0.barPaint
            com.wade.fit.app.AppApplication r2 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r2 = r2.getResources()
            r12 = 2131100069(0x7f0601a5, float:1.781251E38)
            int r2 = r2.getColor(r12)
            r1.setColor(r2)
            java.util.List<com.wade.fit.greendao.bean.HealthSportItem> r1 = r0.healthSportItems
            if (r1 == 0) goto L_0x042b
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x009c
            goto L_0x042b
        L_0x009c:
            float r1 = r0.xWidth
            r13 = 1092616192(0x41200000, float:10.0)
            int r2 = com.wade.fit.util.ScreenUtil.dip2px(r13)
            float r2 = (float) r2
            float r1 = r1 - r2
            java.util.List<com.wade.fit.greendao.bean.HealthSportItem> r2 = r0.healthSportItems
            int r2 = r2.size()
            r14 = 1
            int r2 = r2 - r14
            float r2 = (float) r2
            float r15 = r1 / r2
            android.graphics.Paint r1 = r0.barPaint
            r16 = 1084227584(0x40a00000, float:5.0)
            int r2 = com.wade.fit.util.ScreenUtil.dip2px(r16)
            float r2 = (float) r2
            float r2 = r15 - r2
            r1.setStrokeWidth(r2)
            float[] r1 = r0.lineHeight
            r1 = r1[r9]
            float r2 = r0.paddingTop
            float r25 = r1 + r2
            r6 = 0
        L_0x00c8:
            java.util.List<com.wade.fit.greendao.bean.HealthSportItem> r1 = r0.healthSportItems
            int r1 = r1.size()
            if (r6 >= r1) goto L_0x042b
            float r1 = (float) r6
            float r1 = r1 * r15
            float r2 = r0.barWid
            float r1 = r1 + r2
            float r2 = r0.xOffSet
            float r2 = r2 * r8
            float r26 = r1 + r2
            java.lang.String r1 = ""
            boolean r2 = r0.isAddAvgText
            int[] r2 = com.wade.fit.views.DetailSportChart.C27071.$SwitchMap$com$wade.fit$model$bean$DetailType
            com.wade.fit.model.bean.DetailType r3 = r0.detailType
            int r3 = r3.ordinal()
            r2 = r2[r3]
            java.lang.String r3 = " "
            if (r2 == r14) goto L_0x0197
            if (r2 == r9) goto L_0x0166
            r4 = 3
            if (r2 == r4) goto L_0x00f7
            r5 = r1
            r2 = 0
            goto L_0x01c8
        L_0x00f7:
            java.util.List<com.wade.fit.greendao.bean.HealthSportItem> r2 = r0.healthSportItems
            java.lang.Object r2 = r2.get(r6)
            com.wade.fit.greendao.bean.HealthSportItem r2 = (com.wade.fit.greendao.bean.HealthSportItem) r2
            int r2 = r2.getDistance()
            boolean r4 = com.wade.fit.util.ble.BleSdkWrapper.isDistUnitKm()
            r5 = 1148846080(0x447a0000, float:1000.0)
            if (r4 == 0) goto L_0x0137
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r1)
            float r1 = (float) r2
            float r1 = r1 / r5
            java.lang.String r1 = com.wade.fit.util.NumUtil.float2String(r1, r9)
            r4.append(r1)
            r4.append(r3)
            com.wade.fit.app.AppApplication r1 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r1 = r1.getResources()
            r3 = 2131624651(0x7f0e02cb, float:1.8876488E38)
            java.lang.String r1 = r1.getString(r3)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            goto L_0x01c7
        L_0x0137:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r1)
            float r1 = (float) r2
            float r1 = r1 / r5
            float r1 = com.wade.fit.util.UnitUtil.km2mile(r1)
            java.lang.String r1 = com.wade.fit.util.NumUtil.float2String(r1, r9)
            r4.append(r1)
            r4.append(r3)
            com.wade.fit.app.AppApplication r1 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r1 = r1.getResources()
            r3 = 2131624654(0x7f0e02ce, float:1.8876494E38)
            java.lang.String r1 = r1.getString(r3)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            goto L_0x01c7
        L_0x0166:
            java.util.List<com.wade.fit.greendao.bean.HealthSportItem> r2 = r0.healthSportItems
            java.lang.Object r2 = r2.get(r6)
            com.wade.fit.greendao.bean.HealthSportItem r2 = (com.wade.fit.greendao.bean.HealthSportItem) r2
            int r2 = r2.getCalory()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r1)
            r4.append(r2)
            r4.append(r3)
            com.wade.fit.app.AppApplication r1 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r1 = r1.getResources()
            r3 = 2131624643(0x7f0e02c3, float:1.8876472E38)
            java.lang.String r1 = r1.getString(r3)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            goto L_0x01c7
        L_0x0197:
            java.util.List<com.wade.fit.greendao.bean.HealthSportItem> r2 = r0.healthSportItems
            java.lang.Object r2 = r2.get(r6)
            com.wade.fit.greendao.bean.HealthSportItem r2 = (com.wade.fit.greendao.bean.HealthSportItem) r2
            int r2 = r2.getStepCount()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r1)
            r4.append(r2)
            r4.append(r3)
            com.wade.fit.app.AppApplication r1 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r1 = r1.getResources()
            r3 = 2131624660(0x7f0e02d4, float:1.8876506E38)
            java.lang.String r1 = r1.getString(r3)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
        L_0x01c7:
            r5 = r1
        L_0x01c8:
            float r1 = r0.yScale
            float r3 = (float) r2
            float r1 = r1 * r3
            float r27 = r25 - r1
            float r1 = r0.touchX
            int r3 = (r1 > r26 ? 1 : (r1 == r26 ? 0 : -1))
            if (r3 <= 0) goto L_0x03be
            float r3 = r26 + r15
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x03be
            if (r2 <= 0) goto L_0x03be
            android.graphics.Paint r1 = r0.barPaint
            android.content.res.Resources r2 = r31.getResources()
            r4 = 2131100157(0x7f0601fd, float:1.7812688E38)
            int r2 = r2.getColor(r4)
            r1.setColor(r2)
            android.graphics.Paint r1 = r0.touchPaint
            android.content.res.Resources r2 = r31.getResources()
            r3 = 2131165530(0x7f07015a, float:1.794528E38)
            float r2 = r2.getDimension(r3)
            r1.setTextSize(r2)
            android.graphics.Paint r1 = r0.touchPaint2
            android.content.res.Resources r2 = r31.getResources()
            r3 = 2131165585(0x7f070191, float:1.7945391E38)
            float r2 = r2.getDimension(r3)
            r1.setTextSize(r2)
            android.graphics.Paint r1 = r0.touchPaint
            android.content.res.Resources r2 = r31.getResources()
            r3 = 2131100179(0x7f060213, float:1.7812732E38)
            int r2 = r2.getColor(r3)
            r1.setColor(r2)
            android.graphics.Paint r1 = r0.touchPaint2
            android.content.res.Resources r2 = r31.getResources()
            int r2 = r2.getColor(r3)
            r1.setColor(r2)
            java.util.List<com.wade.fit.greendao.bean.HealthSportItem> r1 = r0.healthSportItems
            java.lang.Object r1 = r1.get(r6)
            com.wade.fit.greendao.bean.HealthSportItem r1 = (com.wade.fit.greendao.bean.HealthSportItem) r1
            java.lang.String r2 = r1.getRemark()
            android.graphics.Paint r1 = r0.touchPaint
            float r1 = com.wade.fit.util.ViewUtil.getTextRectWidth(r1, r2)
            int r1 = (int) r1
            android.graphics.Paint r3 = r0.touchPaint
            float r3 = com.wade.fit.util.ViewUtil.getTextRectHeight(r3, r2)
            int r3 = (int) r3
            android.graphics.Paint r4 = r0.touchPaint2
            float r4 = com.wade.fit.util.ViewUtil.getTextRectWidth(r4, r5)
            int r4 = (int) r4
            android.graphics.Paint r8 = r0.touchPaint2
            float r8 = com.wade.fit.util.ViewUtil.getTextRectHeight(r8, r5)
            int r8 = (int) r8
            int r9 = java.lang.Math.max(r1, r4)
            int r11 = r3 + r8
            float r14 = r0.yScale
            int r12 = r0.MAX_VALUE
            float r12 = (float) r12
            float r14 = r14 * r12
            float r12 = r25 - r14
            r14 = 1101004800(0x41a00000, float:20.0)
            int r14 = com.wade.fit.util.ScreenUtil.dip2px(r14)
            float r14 = (float) r14
            float r12 = r12 - r14
            r14 = 1073741824(0x40000000, float:2.0)
            int r13 = com.wade.fit.util.ScreenUtil.dip2px(r14)
            float r13 = (float) r13
            float r13 = r27 - r13
            int r10 = com.wade.fit.util.ScreenUtil.dip2px(r14)
            float r10 = (float) r10
            float r10 = r10 + r12
            android.graphics.Paint r14 = r0.touchPaint
            r20 = 1065353216(0x3f800000, float:1.0)
            r21 = r1
            int r1 = com.wade.fit.util.ScreenUtil.dip2px(r20)
            float r1 = (float) r1
            r14.setStrokeWidth(r1)
            android.graphics.Paint r1 = r0.touchPaint
            android.content.res.Resources r14 = r31.getResources()
            r28 = r15
            r15 = 2131099755(0x7f06006b, float:1.7811872E38)
            int r14 = r14.getColor(r15)
            r1.setColor(r14)
            android.graphics.Paint r14 = r0.touchPaint
            r15 = r21
            r1 = r32
            r29 = r2
            r2 = r26
            r7 = r3
            r3 = r13
            r13 = r4
            r4 = r26
            r30 = r5
            r5 = r10
            r10 = r6
            r6 = r14
            r1.drawLine(r2, r3, r4, r5, r6)
            r14 = 1092616192(0x41200000, float:10.0)
            int r1 = com.wade.fit.util.ScreenUtil.dip2px(r14)
            android.graphics.RectF r2 = r0.selectRect
            int r3 = r9 / 2
            float r3 = (float) r3
            float r3 = r26 - r3
            int r3 = (int) r3
            int r3 = r3 - r1
            float r3 = (float) r3
            r2.left = r3
            float r3 = (float) r7
            float r12 = r12 - r3
            float r4 = (float) r8
            float r12 = r12 - r4
            int r5 = (int) r12
            r6 = 1097859072(0x41700000, float:15.0)
            int r7 = com.wade.fit.util.ScreenUtil.dip2px(r6)
            int r5 = r5 - r7
            int r7 = r1 * 2
            int r5 = r5 - r7
            float r5 = (float) r5
            r2.top = r5
            android.graphics.RectF r2 = r0.selectRect
            float r5 = r2.left
            float r8 = (float) r9
            float r5 = r5 + r8
            float r7 = (float) r7
            float r5 = r5 + r7
            r2.right = r5
            android.graphics.RectF r2 = r0.selectRect
            float r5 = r2.top
            float r9 = (float) r11
            float r5 = r5 + r9
            int r6 = com.wade.fit.util.ScreenUtil.dip2px(r6)
            float r6 = (float) r6
            float r5 = r5 + r6
            float r6 = (float) r1
            float r5 = r5 + r6
            r2.bottom = r5
            int r2 = r15 / 2
            float r2 = (float) r2
            float r2 = r26 - r2
            int r5 = r13 / 2
            float r5 = (float) r5
            float r5 = r26 - r5
            android.graphics.RectF r9 = r0.selectRect
            float r9 = r9.right
            int r11 = r31.getWidth()
            float r11 = (float) r11
            int r9 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r9 <= 0) goto L_0x0336
            android.graphics.RectF r2 = r0.selectRect
            int r5 = r31.getWidth()
            int r5 = r5 - r1
            float r1 = (float) r5
            r2.right = r1
            android.graphics.RectF r1 = r0.selectRect
            float r2 = r1.right
            float r2 = r2 - r8
            float r2 = r2 - r7
            r1.left = r2
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            android.graphics.RectF r2 = r0.selectRect
            float r2 = r2.width()
            float r5 = (float) r15
            float r2 = r2 - r5
            r5 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r5
            float r2 = r2 + r1
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            android.graphics.RectF r7 = r0.selectRect
            float r7 = r7.width()
        L_0x0330:
            float r8 = (float) r13
            float r7 = r7 - r8
            float r7 = r7 / r5
            float r5 = r1 + r7
            goto L_0x0364
        L_0x0336:
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            r9 = 0
            int r1 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r1 > 0) goto L_0x0364
            android.graphics.RectF r1 = r0.selectRect
            r1.left = r6
            float r2 = r1.left
            float r2 = r2 + r8
            float r2 = r2 + r7
            r1.right = r2
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            android.graphics.RectF r2 = r0.selectRect
            float r2 = r2.width()
            float r5 = (float) r15
            float r2 = r2 - r5
            r5 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r5
            float r2 = r2 + r1
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.left
            android.graphics.RectF r7 = r0.selectRect
            float r7 = r7.width()
            goto L_0x0330
        L_0x0364:
            android.graphics.Paint r1 = r0.barPaint
            com.wade.fit.app.AppApplication r7 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r7 = r7.getResources()
            r8 = 2131100157(0x7f0601fd, float:1.7812688E38)
            int r7 = r7.getColor(r8)
            r1.setColor(r7)
            android.graphics.RectF r1 = r0.selectRect
            int r7 = com.wade.fit.util.ScreenUtil.dip2px(r16)
            float r7 = (float) r7
            int r8 = com.wade.fit.util.ScreenUtil.dip2px(r16)
            float r8 = (float) r8
            android.graphics.Paint r9 = r0.touchPaint
            r11 = r32
            r11.drawRoundRect(r1, r7, r8, r9)
            android.graphics.RectF r1 = r0.selectRect
            float r1 = r1.top
            float r1 = r1 + r6
            android.graphics.Paint r6 = r0.touchPaint
            android.content.res.Resources r7 = r31.getResources()
            r8 = 2131100179(0x7f060213, float:1.7812732E38)
            int r7 = r7.getColor(r8)
            r6.setColor(r7)
            r6 = r29
            if (r6 == 0) goto L_0x03ab
            float r7 = r1 + r3
            android.graphics.Paint r8 = r0.touchPaint
            r11.drawText(r6, r2, r7, r8)
        L_0x03ab:
            float r1 = r1 + r3
            float r1 = r1 + r4
            int r2 = com.wade.fit.util.ScreenUtil.dip2px(r16)
            float r2 = (float) r2
            float r1 = r1 + r2
            android.graphics.Paint r2 = r0.touchPaint2
            r3 = r30
            r11.drawText(r3, r5, r1, r2)
            r7 = 2131100069(0x7f0601a5, float:1.781251E38)
            goto L_0x03d8
        L_0x03be:
            r10 = r6
            r11 = r7
            r28 = r15
            r14 = 1092616192(0x41200000, float:10.0)
            android.graphics.Paint r1 = r0.barPaint
            com.wade.fit.app.AppApplication r2 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r2 = r2.getResources()
            r7 = 2131100069(0x7f0601a5, float:1.781251E38)
            int r2 = r2.getColor(r7)
            r1.setColor(r2)
        L_0x03d8:
            android.graphics.LinearGradient r1 = new android.graphics.LinearGradient
            com.wade.fit.app.AppApplication r2 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r2 = r2.getResources()
            int r22 = r2.getColor(r7)
            com.wade.fit.app.AppApplication r2 = com.wade.fit.app.AppApplication.getInstance()
            android.content.res.Resources r2 = r2.getResources()
            r3 = 2131100070(0x7f0601a6, float:1.7812511E38)
            int r23 = r2.getColor(r3)
            android.graphics.Shader$TileMode r24 = android.graphics.Shader.TileMode.REPEAT
            r17 = r1
            r18 = r26
            r19 = r25
            r20 = r26
            r21 = r27
            r17.<init>(r18, r19, r20, r21, r22, r23, r24)
            android.graphics.Paint r2 = r0.barPaint
            r2.setShader(r1)
            android.graphics.Paint r6 = r0.barPaint
            r1 = r32
            r2 = r26
            r3 = r25
            r4 = r26
            r5 = r27
            r1.drawLine(r2, r3, r4, r5, r6)
            int r6 = r10 + 1
            r7 = r11
            r15 = r28
            r8 = 1069547520(0x3fc00000, float:1.5)
            r9 = 2
            r10 = 2131099755(0x7f06006b, float:1.7811872E38)
            r12 = 2131100069(0x7f0601a5, float:1.781251E38)
            r13 = 1092616192(0x41200000, float:10.0)
            r14 = 1
            goto L_0x00c8
        L_0x042b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.DetailSportChart.drawX(android.graphics.Canvas):void");
    }

    /* renamed from: com.wade.fit.views.DetailSportChart$1 */
    static /* synthetic */ class C27071 {
        static final /* synthetic */ int[] $SwitchMap$com$wade.fit$model$bean$DetailType = new int[DetailType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.wade.fit.model.bean.DetailType[] r0 = com.wade.fit.model.bean.DetailType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.wade.fit.views.DetailSportChart.C27071.$SwitchMap$com$wade.fit$model$bean$DetailType = r0
                int[] r0 = com.wade.fit.views.DetailSportChart.C27071.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.wade.fit.model.bean.DetailType r1 = com.wade.fit.model.bean.DetailType.STEP     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.wade.fit.views.DetailSportChart.C27071.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x001f }
                com.wade.fit.model.bean.DetailType r1 = com.wade.fit.model.bean.DetailType.CAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.wade.fit.views.DetailSportChart.C27071.$SwitchMap$com$wade.fit$model$bean$DetailType     // Catch:{ NoSuchFieldError -> 0x002a }
                com.wade.fit.model.bean.DetailType r1 = com.wade.fit.model.bean.DetailType.DISTANCE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.DetailSportChart.C27071.<clinit>():void");
        }
    }

    private void drawY(Canvas canvas) {
        this.barPaint.setStrokeWidth(1.0f);
        this.barPaint.setTextSize(this.textSize * 1.0f);
        this.barPaint.setTextAlign(Paint.Align.CENTER);
    }

    public void isAddAvgText(boolean z) {
        this.isAddAvgText = z;
    }

    public void initDatas(List<HealthSportItem> list, List<String> list2, DetailType detailType2) {
        this.detailType = detailType2;
        this.isToday = this.isToday;
        this.healthSportItems = list;
        this.MAX_VALUE = 0;
        boolean z = true;
        if (list == null || list.size() <= 0) {
            this.MAX_VALUE = 0;
        } else {
            for (int i = 0; i < list.size(); i++) {
                HealthSportItem healthSportItem = list.get(i);
                int i2 = C27071.$SwitchMap$com$wade.fit$model$bean$DetailType[detailType2.ordinal()];
                if (i2 != 1) {
                    if (i2 != 2) {
                        if (i2 == 3 && healthSportItem.getDistance() > this.MAX_VALUE) {
                            this.MAX_VALUE = healthSportItem.getDistance();
                        }
                    } else if (healthSportItem.getCalory() > this.MAX_VALUE) {
                        this.MAX_VALUE = healthSportItem.getCalory();
                    }
                } else if (healthSportItem.getStepCount() > this.MAX_VALUE) {
                    this.MAX_VALUE = healthSportItem.getStepCount();
                }
            }
        }
        int i3 = this.MAX_VALUE;
        if (i3 != 0) {
            this.isDrawTAB = true;
            this.MIDDLE_VALUE = (i3 * 2) / 3;
            this.MIN_VALUE = i3 / 3;
        } else {
            this.isDrawTAB = true;
            this.MAX_VALUE = 3000;
            this.MIDDLE_VALUE = 2000;
            this.MIN_VALUE = 1000;
        }
        this.barNum = list.size();
        this.xLables = list2;
        if (this.barNum % 2 != 0) {
            z = false;
        }
        this.isDouble = z;
        this.middleIndex = (this.barNum / 2) - (this.isDouble ? 1 : 0);
        initYscale();
        this.initDraw = false;
        invalidate();
    }

    private void initYscale() {
        int i = this.MAX_VALUE;
        if (i != 0) {
            float[] fArr = this.lineHeight;
            this.yScale = (fArr[2] - fArr[0]) / ((float) i);
        }
    }
}
