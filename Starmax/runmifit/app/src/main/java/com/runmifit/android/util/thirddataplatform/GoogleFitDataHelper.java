package com.wade.fit.util.thirddataplatform;

import com.wade.fit.app.AppApplication;
import com.wade.fit.greendao.bean.HealthHeartRateItem;
import com.wade.fit.greendao.bean.HealthSportItem;
import com.wade.fit.greendao.gen.HealthHeartRateItemDao;
import com.wade.fit.greendao.gen.HealthSleepItemDao;
import com.wade.fit.greendao.gen.HealthSportItemDao;
import com.wade.fit.util.AppSharedPreferencesUtils;
import com.wade.fit.util.SharePreferenceUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.greenrobot.greendao.query.WhereCondition;

public class GoogleFitDataHelper {
    public static final String LAST_SYS_TIME = "LAST_SYS_TIME";
    float allCalory = 0.0f;
    float allDistance = 0.0f;
    int allSleep = 0;
    float avgRate;
    public long currentUploadTime;
    float height;
    boolean isUpload = true;
    int lastUpdateItem = 0;
    public long lastUploadTime;
    private List<HealthSportItem> mAllData = new ArrayList();
    protected AppSharedPreferencesUtils share = AppSharedPreferencesUtils.getInstance();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    int stepAllCount = 0;
    float weight;

    private void initData() {
        this.mAllData.clear();
        this.stepAllCount = 0;
        this.allDistance = 0.0f;
        this.allCalory = 0.0f;
        this.avgRate = 0.0f;
        this.allSleep = 0;
    }

    public void getUploadData() {
        initData();
        Calendar instance = Calendar.getInstance();
        instance.setTime(new Date());
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        this.currentUploadTime = System.currentTimeMillis();
        long timeInMillis = instance.getTimeInMillis();
        this.lastUploadTime = SharePreferenceUtils.getLong(AppApplication.getInstance(), LAST_SYS_TIME, 0L);
        if (this.lastUploadTime < timeInMillis) {
            this.lastUploadTime = timeInMillis;
        }
        List<HealthSportItem> list = AppApplication.getInstance().getDaoSession().getHealthSportItemDao().queryBuilder().where(HealthSportItemDao.Properties.Date.between(Long.valueOf(this.lastUploadTime), Long.valueOf(this.currentUploadTime)), new WhereCondition[0]).orderDesc(HealthSportItemDao.Properties.Date).build().list();
        if (list != null && list.size() > 0) {
            for (HealthSportItem healthSportItem : list) {
                this.stepAllCount += healthSportItem.getStepCount();
                this.allDistance += (float) healthSportItem.getDistance();
                this.allCalory += (float) healthSportItem.getCalory();
            }
        }
        List list2 = AppApplication.getInstance().getDaoSession().getHealthHeartRateItemDao().queryBuilder().where(HealthHeartRateItemDao.Properties.Date.between(Long.valueOf(this.lastUploadTime), Long.valueOf(this.currentUploadTime)), new WhereCondition[0]).orderDesc(HealthHeartRateItemDao.Properties.Date).build().list();
        if (list2 != null && list2.size() > 0) {
            int i = 0;
            while (true) {
                if (i >= list2.size()) {
                    break;
                } else if (((HealthHeartRateItem) list2.get(i)).getHeartRaveValue() != 0) {
                    this.avgRate = (float) ((HealthHeartRateItem) list2.get(i)).getHeartRaveValue();
                    break;
                } else {
                    i++;
                }
            }
        }
        List list3 = AppApplication.getInstance().getDaoSession().getHealthSleepItemDao().queryBuilder().where(HealthSleepItemDao.Properties.Date.between(Long.valueOf(this.lastUploadTime), Long.valueOf(this.currentUploadTime)), new WhereCondition[0]).orderDesc(HealthSleepItemDao.Properties.Date).build().list();
        if (list3 != null) {
            this.allSleep = list3.size() * 10;
        }
    }

    public void uploadSuccess() {
        SharePreferenceUtils.put(AppApplication.getInstance(), LAST_SYS_TIME, Long.valueOf(this.currentUploadTime));
    }
}
