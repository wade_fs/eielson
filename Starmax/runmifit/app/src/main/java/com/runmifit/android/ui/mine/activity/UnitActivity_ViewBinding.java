package com.wade.fit.ui.mine.activity;

import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.wade.fit.R;

/* renamed from: com.wade.fit.ui.mine.activity.UnitActivity_ViewBinding */
public class UnitActivity_ViewBinding implements Unbinder {
    private UnitActivity target;

    public UnitActivity_ViewBinding(UnitActivity unitActivity) {
        this(unitActivity, unitActivity.getWindow().getDecorView());
    }

    public UnitActivity_ViewBinding(UnitActivity unitActivity, View view) {
        this.target = unitActivity;
        unitActivity.ivKm = Utils.findRequiredView(view, R.id.ivKm, "field 'ivKm'");
        unitActivity.ivFt = Utils.findRequiredView(view, R.id.ivFt, "field 'ivFt'");
    }

    public void unbind() {
        UnitActivity unitActivity = this.target;
        if (unitActivity != null) {
            this.target = null;
            unitActivity.ivKm = null;
            unitActivity.ivFt = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
