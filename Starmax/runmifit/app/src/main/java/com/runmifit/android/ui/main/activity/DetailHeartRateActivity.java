package com.wade.fit.ui.main.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.wade.fit.R;
import com.wade.fit.app.AppApplication;
import com.wade.fit.app.Constants;
import com.wade.fit.base.BaseCalendarActivity;
import com.wade.fit.model.bean.DetailTimeType;
import com.wade.fit.model.bean.HeartRateDetailVO;
import com.wade.fit.persenter.main.DetailHrPresenter;
import com.wade.fit.util.ButtonUtils;
import com.wade.fit.util.DateUtil;
import com.wade.fit.util.ProDbUtils;
import com.wade.fit.views.HeartRateChart;
import java.util.Calendar;
import java.util.Date;

/* renamed from: com.wade.fit.ui.main.activity.DetailHeartRateActivity */
public class DetailHeartRateActivity extends BaseCalendarActivity<DetailHrPresenter> implements Constants {
    public static final String DETAILTYPE_KEY = "DETAILTYPE_KEY";
    public static final String DETAIL_DATE_KEY = "DETAIL_DATE_KEY";
    DetailTimeType detailTimeType = DetailTimeType.DAY;
    HeartRateChart heartRateChart;
    ImageView ivNextDate;
    ImageView ivPreDate;
    /* access modifiers changed from: private */
    public Date mSearchDate;
    TextView tvAvHrValue;
    TextView tvDate;
    TextView tvDay;
    TextView tvHrValue;
    TextView tvMaxHrValue;
    TextView tvMonth;
    TextView tvWeek;
    TextView tvYear;

    private void updateByTimeType() {
    }

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_detail_heart_rate;
    }

    public void updateByType() {
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id != R.id.ivSelectedDate && id != R.id.tvDate) {
            switch (id) {
                case R.id.rbDay /*2131296769*/:
                    this.detailTimeType = DetailTimeType.DAY;
                    this.heartRateChart.setType(this.detailTimeType);
                    this.ivPreDate.setVisibility(View.VISIBLE);
                    this.ivNextDate.setVisibility(View.VISIBLE);
                    updateByDate(((DetailHrPresenter) this.mPresenter).getDetailCurrent(this.detailTimeType, this.mSearchDate));
                    return;
                case R.id.rbOneMonth /*2131296770*/:
                    this.detailTimeType = DetailTimeType.ONE_MONTH;
                    this.ivPreDate.setVisibility(View.GONE);
                    this.ivNextDate.setVisibility(View.GONE);
                    updateByDate(((DetailHrPresenter) this.mPresenter).getOneMonth());
                    return;
                case R.id.rbSixMonth /*2131296771*/:
                    this.detailTimeType = DetailTimeType.SIX_MONTH;
                    this.ivPreDate.setVisibility(View.GONE);
                    this.ivNextDate.setVisibility(View.GONE);
                    updateByDate(((DetailHrPresenter) this.mPresenter).getSixMonth());
                    return;
                case R.id.rbYear /*2131296772*/:
                    this.detailTimeType = DetailTimeType.YEAR;
                    this.ivPreDate.setVisibility(View.GONE);
                    this.ivNextDate.setVisibility(View.GONE);
                    updateByDate(((DetailHrPresenter) this.mPresenter).getYearMonth());
                    updateByTimeType();
                    return;
                default:
                    return;
            }
        } else if (this.detailTimeType == DetailTimeType.DAY) {
            this.clendarDialog.mOnSelectDateListener = new OnSelectDateListener() {
                /* class com.wade.fit.ui.main.activity.DetailHeartRateActivity.C24991 */

                public void onSelectOtherMonth(int i) {
                }

                public void onSelectDate(CalendarDate calendarDate) {
                    Date unused = DetailHeartRateActivity.this.mSearchDate = ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day);
                    DetailHeartRateActivity detailHeartRateActivity = DetailHeartRateActivity.this;
                    detailHeartRateActivity.updateByDate(((DetailHrPresenter) detailHeartRateActivity.mPresenter).getDetailCurrent(DetailTimeType.DAY, ProDbUtils.getDate(calendarDate.year, calendarDate.month, calendarDate.day)));
                }
            };
            this.clendarDialog.showDialog();
        }
    }

    /* access modifiers changed from: private */
    public void updateByDate(HeartRateDetailVO heartRateDetailVO) {
        if (this.detailTimeType == DetailTimeType.DAY) {
            changeDateUpdateUI(this.mSearchDate);
        } else {
            this.tvDate.setText(heartRateDetailVO.mainVO.date);
        }
        if (heartRateDetailVO.minValue == 0) {
            this.tvHrValue.setText("--");
        } else {
            TextView textView = this.tvHrValue;
            textView.setText(heartRateDetailVO.minValue + "");
        }
        if (heartRateDetailVO.maxValue == 0) {
            this.tvMaxHrValue.setText("--");
        } else {
            TextView textView2 = this.tvMaxHrValue;
            textView2.setText(heartRateDetailVO.maxValue + "");
        }
        if (heartRateDetailVO.avgValue == 0) {
            this.tvAvHrValue.setText("--");
        } else {
            TextView textView3 = this.tvAvHrValue;
            textView3.setText(heartRateDetailVO.avgValue + "");
        }
        TextView textView4 = this.tvAvHrValue;
        textView4.setText(heartRateDetailVO.avgValue + "");
        this.heartRateChart.setType(this.detailTimeType);
        this.heartRateChart.setDatas(heartRateDetailVO.items, heartRateDetailVO.mainVO.healthHeartRate, false, heartRateDetailVO.dates);
    }

    public static void startActivity(Activity activity, Date date) {
        Intent intent = new Intent(activity, DetailHeartRateActivity.class);
        intent.putExtra("DETAIL_DATE_KEY", date);
        activity.startActivity(intent);
    }

    public void initView() {
        this.titleName.setText(getResources().getString(R.string.heart_rate));
        this.mSearchDate = (Date) getIntent().getSerializableExtra("DETAIL_DATE_KEY");
        if (this.mSearchDate != null) {
            updateByType();
            updateByDate(((DetailHrPresenter) this.mPresenter).getDetailCurrent(this.detailTimeType, this.mSearchDate));
            updateByTimeType();
            Calendar.getInstance().get(1);
            AppApplication.getInstance().getUserBean().getYear();
        }
    }

    /* access modifiers changed from: package-private */
    public void changeNextDay() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ivNextDate, 500)) {
            this.mSearchDate = DateUtil.getSpecifiedDayAfterDate(DateUtil.formatYMD.format(this.mSearchDate));
            updateByDate(((DetailHrPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
        }
    }

    /* access modifiers changed from: package-private */
    public void changePreDay() {
        if (!ButtonUtils.isFastDoubleClick(R.id.ivPreDate, 500)) {
            this.mSearchDate = DateUtil.getSpecifiedDayBeforeDate(DateUtil.formatYMD.format(this.mSearchDate));
            updateByDate(((DetailHrPresenter) this.mPresenter).getDetailCurrent(DetailTimeType.DAY, this.mSearchDate));
        }
    }

    private void changeDateUpdateUI(Date date) {
        if (DateUtil.formatYMD.format(date).equals(DateUtil.formatYMD.format(Calendar.getInstance().getTime()))) {
            this.ivNextDate.setVisibility(View.INVISIBLE);
            this.tvDate.setText(getResources().getString(R.string.today));
            return;
        }
        this.ivNextDate.setVisibility(View.VISIBLE);
        this.tvDate.setText(DateUtil.formatYMD.format(date));
    }
}
