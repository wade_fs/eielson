package com.wade.fit.views.photo;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.baidu.mobstat.Config;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryUtil {
    public static List<GalleryEntity> queryGallery(Context context) {
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data", "bucket_id", "bucket_display_name", "COUNT(1) AS count"}, "0==0) GROUP BY (bucket_id", null, "date_added");
        if (query.moveToFirst()) {
            int columnIndex = query.getColumnIndex("_id");
            int columnIndex2 = query.getColumnIndex("_data");
            int columnIndex3 = query.getColumnIndex("bucket_id");
            int columnIndex4 = query.getColumnIndex("bucket_display_name");
            int columnIndex5 = query.getColumnIndex(Config.TRACE_VISIT_RECENT_COUNT);
            do {
                int i = query.getInt(columnIndex);
                String string = query.getString(columnIndex2);
                int i2 = query.getInt(columnIndex3);
                String string2 = query.getString(columnIndex4);
                int i3 = query.getInt(columnIndex5);
                GalleryEntity galleryEntity = new GalleryEntity();
                galleryEntity.setId(i);
                galleryEntity.setPath(string);
                galleryEntity.setGallery_id(i2);
                galleryEntity.setGallery_name(string2);
                galleryEntity.setCount(i3);
                if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && string.contains("Camera") && string2.equals("Camera")) {
                    arrayList.add(galleryEntity);
                }
            } while (query.moveToNext());
        }
        if (query != null && !query.isClosed()) {
            query.close();
        }
        return arrayList;
    }

    public static boolean deleteImage(Context context, String str) {
        Cursor query = MediaStore.Images.Media.query(context.getContentResolver(), MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, "_data=?", new String[]{str}, null);
        if (!query.moveToFirst()) {
            return new File(str).delete();
        }
        if (context.getContentResolver().delete(ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, query.getLong(0)), null, null) == 1) {
            return true;
        }
        return false;
    }

    public static ArrayList<String> getAllImagePathsByFolder(String str) {
        File file = new File(str);
        if (!file.exists()) {
            file.mkdir();
        }
        String[] list = file.list();
        ArrayList<String> arrayList = new ArrayList<>();
        if (list != null && list.length > 0) {
            for (int length = list.length - 1; length >= 0; length--) {
                if (isImage(list[length])) {
                    arrayList.add(str + File.separator + list[length]);
                }
            }
        }
        return arrayList;
    }

    public static String getFolderByFileName(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (!str.contains("/")) {
            return str;
        }
        return str.substring(0, str.lastIndexOf(File.separator));
    }

    public static boolean isImage(String str) {
        return str.endsWith(".jpg") || str.endsWith(".jpeg");
    }
}
