package com.wade.fit.ui.device.activity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.wade.fit.R;
import com.wade.fit.base.BaseActivity;
import com.wade.fit.model.bean.DeviceConfig;
import com.wade.fit.model.bean.MusicPlayData;
import com.wade.fit.util.MusicUtil;
import com.wade.fit.util.SPHelper;
import com.wade.fit.views.CustomToggleButton;
import java.util.List;

/* renamed from: com.wade.fit.ui.device.activity.MusicControlActivity */
public class MusicControlActivity extends BaseActivity {
    private Activity activity;
    /* access modifiers changed from: private */
    public MusicControlAdapter adapter;
    /* access modifiers changed from: private */
    public DeviceConfig deviceConfig;
    CustomToggleButton mMusicControl;
    ListView mMusicPlayLv;
    private PackageManager mPackageManager;
    TextView mSelectPlayTv;
    private List<MusicPlayData> musicPlayDataList;
    /* access modifiers changed from: private */
    public MusicUtil musicUtil;
    private Resources res;
    /* access modifiers changed from: private */
    public int selectItem = -1;

    /* access modifiers changed from: protected */
    public int getContentView() {
        return R.layout.activity_music_control;
    }

    public void initView() {
        this.activity = this;
        this.res = this.activity.getResources();
        this.musicUtil = new MusicUtil(this.activity, this.mMusicControl);
        this.titleName.setText(getResources().getString(R.string.music_control));
        this.mPackageManager = getPackageManager();
        this.deviceConfig = SPHelper.getDeviceConfig();
        this.musicPlayDataList = MusicUtil.getMusicPlayLists();
        this.mMusicControl.setSwitchState(this.deviceConfig.isMusic);
        if (this.deviceConfig.isMusic) {
            selectMusicPlay(MusicUtil.getSelectedMusic());
        } else {
            this.mMusicPlayLv.setVisibility(View.GONE);
            TextView textView = this.mSelectPlayTv;
            Resources resources = this.res;
            textView.setText(resources.getString(R.string.music_player, resources.getString(R.string.nosetting)));
        }
        this.adapter = new MusicControlAdapter(this.activity, this.musicPlayDataList);
        this.mMusicPlayLv.setAdapter((ListAdapter) this.adapter);
        initEvent();
    }

    /* access modifiers changed from: private */
    public void setMusicState(boolean z) {
        if (z) {
            selectMusicPlay(this.deviceConfig.musicAppName);
            this.mMusicPlayLv.setVisibility(View.VISIBLE);
            return;
        }
        this.mMusicPlayLv.setVisibility(View.GONE);
        TextView textView = this.mSelectPlayTv;
        Resources resources = this.res;
        textView.setText(resources.getString(R.string.music_player, resources.getString(R.string.nosetting)));
    }

    /* access modifiers changed from: private */
    public void selectMusicPlay(String str) {
        if (TextUtils.isEmpty(str)) {
            TextView textView = this.mSelectPlayTv;
            Resources resources = this.res;
            textView.setText(resources.getString(R.string.music_player, resources.getString(R.string.nosetting)));
            return;
        }
        this.mSelectPlayTv.setText(this.res.getString(R.string.music_player, str));
    }

    public void initEvent() {
        this.mMusicControl.setOnSwitchListener(new CustomToggleButton.OnSwitchListener() {
            /* class com.wade.fit.ui.device.activity.MusicControlActivity.C24711 */

            public void onSwitched(boolean z) {
                MusicControlActivity.this.musicUtil.setNoticeMusicPlay(z);
                MusicControlActivity.this.deviceConfig.isMusic = z;
                MusicControlActivity.this.setMusicState(z);
                SPHelper.saveDeviceConfig(MusicControlActivity.this.deviceConfig);
            }
        });
        this.mMusicPlayLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* class com.wade.fit.ui.device.activity.MusicControlActivity.C24722 */

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                MusicPlayData musicPlayData = (MusicPlayData) MusicControlActivity.this.adapter.getItem(i);
                MusicControlActivity.this.selectMusicPlay(musicPlayData.getPlayName());
                MusicControlActivity.this.deviceConfig.musicAppName = musicPlayData.getPlayName();
                MusicControlActivity.this.deviceConfig.musicPackageName = musicPlayData.packageName;
                SPHelper.saveDeviceConfig(MusicControlActivity.this.deviceConfig);
                MusicControlActivity.this.adapter.setSelectItem(i);
                MusicControlActivity.this.adapter.notifyDataSetChanged();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        boolean z;
        super.onResume();
        this.musicUtil.addListener();
        if (this.deviceConfig.isMusic) {
            this.musicUtil.openMusicService();
        }
        this.musicUtil.setNoticeMusicPlay(this.deviceConfig.isMusic);
        if (!TextUtils.isEmpty(this.deviceConfig.musicPackageName)) {
            int i = 0;
            while (true) {
                if (i >= this.musicPlayDataList.size()) {
                    break;
                } else if (this.musicPlayDataList.get(i).getPackageName().equals(this.deviceConfig.musicPackageName)) {
                    this.adapter.setSelectItem(i);
                    this.adapter.notifyDataSetChanged();
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
        }
        z = false;
        if (!z) {
            this.adapter.setSelectItem(0);
            this.adapter.notifyDataSetChanged();
            if (this.musicPlayDataList.size() > 0) {
                this.deviceConfig.musicPackageName = this.musicPlayDataList.get(0).getPackageName();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.musicUtil.removeListener();
        SPHelper.saveDeviceConfig(this.deviceConfig);
    }

    /* renamed from: com.wade.fit.ui.device.activity.MusicControlActivity$MusicControlAdapter */
    private class MusicControlAdapter extends BaseAdapter {
        private Context context;
        private List<MusicPlayData> musicPlayDatas;

        public long getItemId(int i) {
            return (long) i;
        }

        public MusicControlAdapter(Context context2, List<MusicPlayData> list) {
            this.context = context2;
            this.musicPlayDatas = list;
        }

        public int getCount() {
            List<MusicPlayData> list = this.musicPlayDatas;
            if (list == null) {
                return 0;
            }
            return list.size();
        }

        public Object getItem(int i) {
            return this.musicPlayDatas.get(i);
        }

        public void setSelectItem(int i) {
            int unused = MusicControlActivity.this.selectItem = i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            ViewHolder viewHolder;
            if (view == null) {
                viewHolder = new ViewHolder();
                view2 = View.inflate(this.context, R.layout.music_control_item, null);
                viewHolder.image = (ImageView) view2.findViewById(R.id.music_play_item_iv);
                viewHolder.playName = (TextView) view2.findViewById(R.id.music_play_item_tv);
                viewHolder.selectImg = (ImageView) view2.findViewById(R.id.select_music_play_item_iv);
                view2.setTag(viewHolder);
            } else {
                view2 = view;
                viewHolder = (ViewHolder) view.getTag();
            }
            MusicPlayData musicPlayData = (MusicPlayData) getItem(i);
            viewHolder.image.setImageDrawable(musicPlayData.getPlayIcon());
            viewHolder.playName.setText(musicPlayData.getPlayName());
            if (MusicControlActivity.this.selectItem == i) {
                viewHolder.selectImg.setImageResource(R.mipmap.music_control_sel);
            } else {
                viewHolder.selectImg.setImageResource(R.mipmap.music_control_unsel);
            }
            return view2;
        }

        /* renamed from: com.wade.fit.ui.device.activity.MusicControlActivity$MusicControlAdapter$ViewHolder */
        class ViewHolder {
            ImageView image;
            TextView playName;
            ImageView selectImg;

            ViewHolder() {
            }
        }
    }
}
