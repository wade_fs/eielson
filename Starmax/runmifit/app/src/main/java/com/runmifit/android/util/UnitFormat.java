package com.wade.fit.util;

public class UnitFormat {
    public static int ft2in(int i) {
        return i * 12;
    }

    public static float km2mile(float f) {
        return f * 0.6213712f;
    }

    public static float mile2km(float f) {
        return f * 1.609344f;
    }

    public static int st2lb(int i) {
        return i * 14;
    }

    public static int newInch2cm(int[] iArr) {
        return inch2cm(iArr);
    }

    public static int newCm2inch(int[] iArr) {
        return Math.round(((float) (iArr[0] + iArr[1])) / 3.0172415f);
    }

    @Deprecated
    public static int inch2cm(int[] iArr) {
        iArr[1] = (iArr[0] * 12) + iArr[1];
        return Math.round(((float) iArr[1]) * 2.54f);
    }

    public static int inch2cmNew(int[] iArr) {
        return Math.round(((float) ((iArr[0] * 12) + iArr[1])) * 2.54f);
    }

    public static int inch2cm(int i) {
        return Math.round(((float) i) * 2.54f);
    }

    public static int inch2inch(int[] iArr) {
        return (iArr[0] * 12) + iArr[1];
    }

    public static int[] inch2inch(int i) {
        return new int[]{i / 12, i % 12};
    }

    public static int inchs2cm(int i) {
        return Math.round(((float) i) * 2.54f);
    }

    public static int[] cm2inch(int i) {
        int[] iArr = new int[2];
        iArr[1] = cm2inchs(i);
        iArr[0] = iArr[1] / 12;
        iArr[1] = iArr[1] % 12;
        return iArr;
    }

    public static int cm2inchs(int i) {
        return Math.round(((float) i) / 2.54f);
    }

    public static float kg2lb(float f) {
        return (float) Math.round(f * 2.2046225f);
    }

    public static float lb2kg(float f) {
        return (float) Math.round(f * 0.4535924f);
    }

    public static int kg2st(int i) {
        double d = (double) i;
        Double.isNaN(d);
        return (int) Math.round(d * 0.157d);
    }

    public static int lb2st(int i) {
        double d = (double) i;
        Double.isNaN(d);
        return (int) Math.round(d * 0.071d);
    }

    public static int st2kg(int i) {
        double d = (double) i;
        Double.isNaN(d);
        return (int) Math.round(d * 6.35d);
    }
}
