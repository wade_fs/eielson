package com.wade.fit.views.wheel;

import android.content.Context;
import android.view.VelocityTracker;
import android.widget.Scroller;

public class WheelScroller extends Scroller {
    public static final int JUSTIFY_DURATION = 400;
    private int currentIndex;
    private boolean isScrolling;
    private float lastTouchY;
    private int mScrollOffset;
    private VelocityTracker mVelocityTracker;
    final WheelView mWheelView;
    OnWheelChangedListener onWheelChangedListener;

    public WheelScroller(Context context, WheelView wheelView) {
        super(context);
        this.mWheelView = wheelView;
    }

    public void computeScroll() {
        if (this.isScrolling) {
            this.isScrolling = computeScrollOffset();
            doScroll(getCurrY() - this.mScrollOffset);
            if (this.isScrolling) {
                this.mWheelView.postInvalidate();
            } else {
                justify();
            }
        }
    }

    public void stopScrolling() {
        if (!isFinished()) {
            startScroll(0, getCurrY(), 0, 0, 1);
            abortAnimation();
            this.mWheelView.postInvalidate();
            this.isScrolling = false;
        }
    }

    private void doScroll(int i) {
        this.mScrollOffset += i;
        if (!this.mWheelView.isCyclic()) {
            int itemSize = (this.mWheelView.getItemSize() - 1) * this.mWheelView.itemHeight;
            int i2 = this.mScrollOffset;
            if (i2 < 0) {
                this.mScrollOffset = 0;
            } else if (i2 > itemSize) {
                this.mScrollOffset = itemSize;
            }
        }
        notifyWheelChangedListener();
    }

    /* access modifiers changed from: package-private */
    public void notifyWheelChangedListener() {
        int i = this.currentIndex;
        int currentIndex2 = getCurrentIndex();
        if (i != currentIndex2) {
            this.currentIndex = currentIndex2;
            OnWheelChangedListener onWheelChangedListener2 = this.onWheelChangedListener;
            if (onWheelChangedListener2 != null) {
                onWheelChangedListener2.onChanged(this.mWheelView, i, currentIndex2);
            }
        }
    }

    public int getCurrentIndex() {
        int i;
        if (this.mWheelView.getAdapter() == null) {
            return 0;
        }
        int i2 = this.mWheelView.itemHeight;
        int itemSize = this.mWheelView.getItemSize();
        if (itemSize == 0) {
            return 0;
        }
        int i3 = this.mScrollOffset;
        if (i3 < 0) {
            i = (i3 - (i2 / 2)) / i2;
        } else {
            i = (i3 + (i2 / 2)) / i2;
        }
        int i4 = i % itemSize;
        return i4 < 0 ? i4 + itemSize : i4;
    }

    public void setCurrentIndex(int i, boolean z) {
        int i2 = this.mScrollOffset;
        int i3 = (i * this.mWheelView.itemHeight) - i2;
        if (i3 != 0) {
            if (z) {
                this.isScrolling = true;
                startScroll(0, i2, 0, i3, 400);
                this.mWheelView.invalidate();
                return;
            }
            doScroll(i3);
            this.mWheelView.invalidate();
        }
    }

    public int getItemIndex() {
        if (this.mWheelView.itemHeight == 0) {
            return 0;
        }
        return this.mScrollOffset / this.mWheelView.itemHeight;
    }

    public int getItemOffset() {
        if (this.mWheelView.itemHeight == 0) {
            return 0;
        }
        return this.mScrollOffset % this.mWheelView.itemHeight;
    }

    public void reset() {
        this.isScrolling = false;
        this.mScrollOffset = 0;
        notifyWheelChangedListener();
        forceFinished(true);
    }

    /* access modifiers changed from: package-private */
    public void justify() {
        int i = this.mWheelView.itemHeight;
        int i2 = this.mScrollOffset;
        int i3 = i2 % i;
        if (i3 > 0 && i3 < i / 2) {
            this.isScrolling = true;
            startScroll(0, i2, 0, -i3, 400);
            this.mWheelView.invalidate();
        } else if (i3 >= i / 2) {
            this.isScrolling = true;
            startScroll(0, this.mScrollOffset, 0, i - i3, 400);
            this.mWheelView.invalidate();
        } else if (i3 >= 0 || i3 <= (-i) / 2) {
            int i4 = -i;
            if (i3 <= i4 / 2) {
                this.isScrolling = true;
                startScroll(0, this.mScrollOffset, 0, i4 - i3, 400);
                this.mWheelView.invalidate();
            }
        } else {
            this.isScrolling = true;
            startScroll(0, this.mScrollOffset, 0, -i3, 400);
            this.mWheelView.invalidate();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        if (r0 != 3) goto L_0x007f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r12) {
        /*
            r11 = this;
            android.view.VelocityTracker r0 = r11.mVelocityTracker
            if (r0 != 0) goto L_0x000a
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r11.mVelocityTracker = r0
        L_0x000a:
            android.view.VelocityTracker r0 = r11.mVelocityTracker
            r0.addMovement(r12)
            int r0 = r12.getAction()
            r1 = 1
            if (r0 == 0) goto L_0x0073
            if (r0 == r1) goto L_0x0036
            r2 = 2
            if (r0 == r2) goto L_0x001f
            r12 = 3
            if (r0 == r12) goto L_0x0068
            goto L_0x007f
        L_0x001f:
            float r12 = r12.getY()
            float r0 = r11.lastTouchY
            float r0 = r12 - r0
            int r0 = (int) r0
            if (r0 == 0) goto L_0x0033
            int r0 = -r0
            r11.doScroll(r0)
            com.wade.fit.views.wheel.WheelView r0 = r11.mWheelView
            r0.invalidate()
        L_0x0033:
            r11.lastTouchY = r12
            goto L_0x007f
        L_0x0036:
            android.view.VelocityTracker r12 = r11.mVelocityTracker
            r0 = 1000(0x3e8, float:1.401E-42)
            r12.computeCurrentVelocity(r0)
            android.view.VelocityTracker r12 = r11.mVelocityTracker
            float r12 = r12.getYVelocity()
            float r0 = java.lang.Math.abs(r12)
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0065
            r11.isScrolling = r1
            r3 = 0
            int r4 = r11.mScrollOffset
            r5 = 0
            float r12 = -r12
            int r6 = (int) r12
            r7 = 0
            r8 = 0
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
            r10 = 2147483647(0x7fffffff, float:NaN)
            r2 = r11
            r2.fling(r3, r4, r5, r6, r7, r8, r9, r10)
            com.wade.fit.views.wheel.WheelView r12 = r11.mWheelView
            r12.invalidate()
            goto L_0x0068
        L_0x0065:
            r11.justify()
        L_0x0068:
            android.view.VelocityTracker r12 = r11.mVelocityTracker
            if (r12 == 0) goto L_0x007f
            r12.recycle()
            r12 = 0
            r11.mVelocityTracker = r12
            goto L_0x007f
        L_0x0073:
            float r12 = r12.getY()
            r11.lastTouchY = r12
            r11.stopScrolling()
            r11.forceFinished(r1)
        L_0x007f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wade.fit.views.wheel.WheelScroller.onTouchEvent(android.view.MotionEvent):boolean");
    }
}
