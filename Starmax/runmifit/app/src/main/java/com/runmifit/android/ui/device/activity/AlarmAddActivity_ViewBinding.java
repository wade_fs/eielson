package com.wade.fit.ui.device.activity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.wade.fit.R;
import com.wade.fit.views.ItemNewAlarmSet;
import com.wade.fit.views.wheel.WheelView;

/* renamed from: com.wade.fit.ui.device.activity.AlarmAddActivity_ViewBinding */
public class AlarmAddActivity_ViewBinding implements Unbinder {
    private AlarmAddActivity target;
    private View view2131296791;
    private View view2131296792;

    public AlarmAddActivity_ViewBinding(AlarmAddActivity alarmAddActivity) {
        this(alarmAddActivity, alarmAddActivity.getWindow().getDecorView());
    }

    public AlarmAddActivity_ViewBinding(final AlarmAddActivity alarmAddActivity, View view) {
        this.target = alarmAddActivity;
        alarmAddActivity.timeAlarmSet = (ItemNewAlarmSet) Utils.findRequiredViewAsType(view, R.id.time_alarm_set, "field 'timeAlarmSet'", ItemNewAlarmSet.class);
        alarmAddActivity.alarmSetAmPm = (WheelView) Utils.findRequiredViewAsType(view, R.id.alarm_set_am_pm, "field 'alarmSetAmPm'", WheelView.class);
        alarmAddActivity.alarmSetHour = (WheelView) Utils.findRequiredViewAsType(view, R.id.alarm_set_hour, "field 'alarmSetHour'", WheelView.class);
        alarmAddActivity.alarmSetMin = (WheelView) Utils.findRequiredViewAsType(view, R.id.alarm_set_min, "field 'alarmSetMin'", WheelView.class);
        alarmAddActivity.pointWheelView = (WheelView) Utils.findRequiredViewAsType(view, R.id.point, "field 'pointWheelView'", WheelView.class);
        alarmAddActivity.timeAlarmSetContentLl = (LinearLayout) Utils.findRequiredViewAsType(view, R.id.time_alarm_set_content_ll, "field 'timeAlarmSetContentLl'", LinearLayout.class);
        alarmAddActivity.tvAlarmRepeat = (TextView) Utils.findRequiredViewAsType(view, R.id.tvAlarmRepeat, "field 'tvAlarmRepeat'", TextView.class);
        alarmAddActivity.tvAlarmRemind = (TextView) Utils.findRequiredViewAsType(view, R.id.tvAlarmRemind, "field 'tvAlarmRemind'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.rlAlarmRemind, "method 'selectRemind'");
        this.view2131296791 = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.AlarmAddActivity_ViewBinding.C24461 */

            public void doClick(View view) {
                alarmAddActivity.selectRemind();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.rlAlarmRepeat, "method 'selectRepeat'");
        this.view2131296792 = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener() {
            /* class com.wade.fit.ui.device.activity.AlarmAddActivity_ViewBinding.C24472 */

            public void doClick(View view) {
                alarmAddActivity.selectRepeat();
            }
        });
    }

    public void unbind() {
        AlarmAddActivity alarmAddActivity = this.target;
        if (alarmAddActivity != null) {
            this.target = null;
            alarmAddActivity.timeAlarmSet = null;
            alarmAddActivity.alarmSetAmPm = null;
            alarmAddActivity.alarmSetHour = null;
            alarmAddActivity.alarmSetMin = null;
            alarmAddActivity.pointWheelView = null;
            alarmAddActivity.timeAlarmSetContentLl = null;
            alarmAddActivity.tvAlarmRepeat = null;
            alarmAddActivity.tvAlarmRemind = null;
            this.view2131296791.setOnClickListener(null);
            this.view2131296791 = null;
            this.view2131296792.setOnClickListener(null);
            this.view2131296792 = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
