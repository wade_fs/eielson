package com.wade.fit.views;

import com.wade.fit.greendao.bean.HealthHeartRate;
import com.wade.fit.greendao.bean.HealthSleep;
import com.wade.fit.greendao.bean.HealthSport;

public class MainVO {
    public String date;
    public HealthHeartRate healthHeartRate;
    public HealthSleep healthSleep;
    public HealthSport healthSport;
    public String heartRate = "0";
    public String sleepHour = "0";
    public String sleepMin = "0";
    public String sportHour = "0";
    public String sportMin = "0";
    public int sportTotalTime;
    public String sportType = "";
}
