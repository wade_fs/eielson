package com.library.gallery.bean;

public class Image {

    /* renamed from: id */
    private int f4619id;
    private boolean isSelect;
    private String name;
    private String path;

    public int getId() {
        return this.f4619id;
    }

    public void setId(int i) {
        this.f4619id = i;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String str) {
        this.path = str;
    }

    public boolean isSelect() {
        return this.isSelect;
    }

    public void setSelect(boolean z) {
        this.isSelect = z;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Image) {
            return this.path.equals(((Image) obj).getPath());
        }
        return false;
    }
}
