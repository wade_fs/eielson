package com.library.gallery.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class PreviewerViewPager extends ViewPager {
    private boolean isInterceptable;
    private boolean isTransition;
    /* access modifiers changed from: private */
    public int mScrollState;

    public PreviewerViewPager(Context context) {
        this(context, null);
    }

    public PreviewerViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.isInterceptable = false;
        this.isTransition = false;
        this.mScrollState = 0;
        addOnPageChangeListener(new PageChangeListener());
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        if (this.mScrollState != 0) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        if (this.isTransition) {
            int action = motionEvent.getAction();
            motionEvent.setAction(0);
            super.onInterceptTouchEvent(motionEvent);
            motionEvent.setAction(action);
            this.isTransition = false;
        }
        int action2 = motionEvent.getAction();
        if (action2 == 0) {
            this.isInterceptable = false;
        }
        if (action2 != 2 || this.isInterceptable) {
            z = super.onInterceptTouchEvent(motionEvent);
        } else {
            z = false;
        }
        if (!this.isInterceptable || !z) {
            return false;
        }
        return true;
    }

    public void isInterceptable(boolean z) {
        if (!this.isInterceptable && z) {
            this.isTransition = true;
        }
        this.isInterceptable = z;
    }

    private class PageChangeListener extends ViewPager.SimpleOnPageChangeListener {
        private PageChangeListener() {
        }

        public void onPageScrollStateChanged(int i) {
            int unused = PreviewerViewPager.this.mScrollState = i;
        }
    }
}
