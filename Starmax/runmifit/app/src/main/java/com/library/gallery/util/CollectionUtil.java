package com.library.gallery.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class CollectionUtil {

    public interface Checker<T> {
        boolean check(T t);
    }

    public static <T> T[] toArray(List list, Class cls) {
        if (!(list == null || list.size() == 0)) {
            try {
                return list.toArray((Object[]) Array.newInstance(cls, list.size()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static <T> T[] toArray(Set set, Class cls) {
        if (!(set == null || set.size() == 0)) {
            try {
                return set.toArray((Object[]) Array.newInstance(cls, set.size()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static <T> HashSet<T> toHashSet(T[] tArr) {
        if (tArr == null || tArr.length == 0) {
            return null;
        }
        HashSet<T> hashSet = new HashSet<>();
        Collections.addAll(hashSet, tArr);
        return hashSet;
    }

    public static <T> ArrayList<T> toArrayList(T[] tArr) {
        if (tArr == null || tArr.length == 0) {
            return null;
        }
        ArrayList<T> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, tArr);
        return arrayList;
    }

    public static <T> Collection<T> move(List<T> list, int i, int i2) {
        int size = list.size() - 1;
        if (i != i2 && i <= size && i2 <= size) {
            if (i < i2) {
                T t = list.get(i);
                T t2 = list.get(i2);
                list.remove(i);
                list.add(list.indexOf(t2) + 1, t);
            } else {
                T t3 = list.get(i);
                list.remove(i);
                list.add(i2, t3);
            }
        }
        return list;
    }

    public static <T> List<T> filter(List<T> list, Checker<T> checker) {
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            if (!checker.check(it.next())) {
                it.remove();
            }
        }
        return list;
    }

    public static <T> List<T> filter(T[] tArr, Checker<T> checker) {
        return filter(toArrayList(tArr), checker);
    }
}
