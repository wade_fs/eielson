package com.library.gallery;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.library.gallery.util.BitmapUtil;
import com.library.gallery.util.CollectionUtil;
import com.library.gallery.util.StreamUtil;
import com.library.gallery.util.Util;
import com.library.gallery.view.ImagePreviewView;
import com.library.gallery.view.PreviewerViewPager;
import java.io.File;
import java.util.List;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class ImageGalleryActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, EasyPermissions.PermissionCallbacks {
    private static final int PERMISSION_ID = 1;
    public static SelectOptions mOptions;
    private int mCurPosition;
    private Point mDisplayDimens;
    private boolean[] mImageDownloadStatus;
    /* access modifiers changed from: private */
    public PreviewerViewPager mImagePager;
    private ImageView mImageSave;
    /* access modifiers changed from: private */
    public String[] mImageSources;
    private TextView mIndexText;
    /* access modifiers changed from: private */
    public RequestManager mLoader;
    private boolean mNeedSaveLocal;

    interface DoOverrideSizeCallback {
        void onDone(int i, int i2, boolean z);
    }

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPermissionsGranted(int i, List<String> list) {
    }

    public static void show(Context context, SelectOptions selectOptions, int i) {
        if (selectOptions == null) {
            return;
        }
        if (selectOptions.getSelectedImages() != null || selectOptions.getSelectedImages().size() != 0) {
            mOptions = selectOptions;
            Intent intent = new Intent(context, ImageGalleryActivity.class);
            intent.putExtra("position", i);
            context.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTheme(R.style.Theme_Translate);
        setContentView(R.layout.commom_activity_image_gallery);
        initBundle(getIntent().getExtras());
        initWidget();
        initData();
    }

    /* access modifiers changed from: protected */
    public boolean initBundle(Bundle bundle) {
        SelectOptions selectOptions = mOptions;
        if (selectOptions == null) {
            return false;
        }
        this.mImageSources = (String[]) CollectionUtil.toArray(selectOptions.getSelectedImages(), String.class);
        this.mImageDownloadStatus = new boolean[this.mImageSources.length];
        this.mNeedSaveLocal = !TextUtils.isEmpty(mOptions.getSavePath());
        return true;
    }

    private void initWidget() {
        getWindow().setLayout(-1, -1);
        setTitle("");
        this.mImageSave = (ImageView) findViewById(R.id.iv_save);
        this.mImageSave.setOnClickListener(new View.OnClickListener() {
            /* class com.library.gallery.ImageGalleryActivity.C21621 */

            public void onClick(View view) {
                ImageGalleryActivity.this.saveToFileByPermission();
            }
        });
        this.mImagePager = (PreviewerViewPager) findViewById(R.id.vp_image);
        this.mIndexText = (TextView) findViewById(R.id.tv_index);
        this.mImagePager.addOnPageChangeListener(this);
    }

    private void initData() {
        this.mLoader = Glide.with((FragmentActivity) this);
        int length = this.mImageSources.length;
        int i = this.mCurPosition;
        if (i < 0 || i >= length) {
            this.mCurPosition = 0;
        }
        if (length == 1) {
            this.mIndexText.setVisibility(View.GONE);
        }
        this.mImagePager.setAdapter(new ViewPagerAdapter());
        this.mImagePager.setCurrentItem(this.mCurPosition);
        onPageSelected(this.mCurPosition);
    }

    private void changeSaveButtonStatus(boolean z) {
        int i = 8;
        if (this.mNeedSaveLocal) {
            ImageView imageView = this.mImageSave;
            if (z) {
                i = 0;
            }
            imageView.setVisibility(i);
            return;
        }
        this.mImageSave.setVisibility(View.GONE);
    }

    /* access modifiers changed from: private */
    public void updateDownloadStatus(int i, boolean z) {
        this.mImageDownloadStatus[i] = z;
        if (this.mCurPosition == i) {
            changeSaveButtonStatus(z);
        }
    }

    @AfterPermissionGranted(1)
    public void saveToFileByPermission() {
        String[] strArr = {"android.permission.WRITE_EXTERNAL_STORAGE"};
        if (EasyPermissions.hasPermissions(this, strArr)) {
            saveToFile();
        } else {
            EasyPermissions.requestPermissions(this, "请授予保存图片权限", 1, strArr);
        }
    }

    public void onPermissionsDenied(int i, List<String> list) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private void saveToFile() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            Toast.makeText(this, "没有外部存储", 0).show();
            return;
        }
        String savePath = mOptions.getSavePath();
        if (!new File(savePath).exists()) {
            Toast.makeText(this, "没有指定存储路径", 0).show();
            return;
        }
        String str = this.mImageSources[this.mCurPosition];
        Object obj = str;
        if (mOptions.getHeaders() != null) {
            obj = getGlideUrlByUser(str);
        }
        try {
            File file = (File) this.mLoader.load(obj).downloadOnly(Integer.MIN_VALUE, Integer.MIN_VALUE).get();
            if (file == null) {
                return;
            }
            if (file.exists()) {
                String extension = Util.getExtension(file.getAbsolutePath());
                File file2 = new File(savePath);
                if (file2.exists() || file2.mkdirs()) {
                    File file3 = new File(file2, String.format("IMG_%s.%s", Long.valueOf(System.currentTimeMillis()), extension));
                    callSaveStatus(StreamUtil.copyFile(file, file3), file3);
                    return;
                }
                callSaveStatus(false, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            callSaveStatus(false, null);
        }
    }

    private void callSaveStatus(final boolean z, final File file) {
        runOnUiThread(new Runnable() {
            /* class com.library.gallery.ImageGalleryActivity.C21632 */

            public void run() {
                if (z) {
                    ImageGalleryActivity.this.sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));
                    Toast.makeText(ImageGalleryActivity.this, "保存成功", 0).show();
                    return;
                }
                Toast.makeText(ImageGalleryActivity.this, "保存失败", 0).show();
            }
        });
    }

    public void onPageSelected(int i) {
        this.mCurPosition = i;
        this.mIndexText.setText(String.format("%s/%s", Integer.valueOf(i + 1), Integer.valueOf(this.mImageSources.length)));
        changeSaveButtonStatus(this.mImageDownloadStatus[i]);
    }

    /* access modifiers changed from: private */
    public synchronized Point getDisplayDimens() {
        Point point;
        if (this.mDisplayDimens != null) {
            return this.mDisplayDimens;
        }
        Display defaultDisplay = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= 13) {
            point = new Point();
            defaultDisplay.getSize(point);
        } else {
            point = new Point(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        }
        point.y = (int) (((float) point.y) * 0.6f);
        point.x = (int) (((float) point.x) * 0.85f);
        this.mDisplayDimens = point;
        return this.mDisplayDimens;
    }

    private class ViewPagerAdapter extends PagerAdapter implements ImagePreviewView.OnReachBorderListener {
        private View.OnClickListener mFinishClickListener;

        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        private ViewPagerAdapter() {
        }

        public int getCount() {
            return ImageGalleryActivity.this.mImageSources.length;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public Object instantiateItem(ViewGroup viewGroup, int i) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.commom_lay_gallery_page_item_contener, viewGroup, false);
            ImagePreviewView imagePreviewView = (ImagePreviewView) inflate.findViewById(R.id.iv_preview);
            imagePreviewView.setOnReachBorderListener(this);
            ImageView imageView = (ImageView) inflate.findViewById(R.id.iv_default);
            ImageGalleryActivity.this.mLoader.load(ImageGalleryActivity.this.mImageSources[i]).fitCenter().into(imagePreviewView);
            ProgressBar progressBar = (ProgressBar) inflate.findViewById(R.id.progressBar);
            if (ImageGalleryActivity.mOptions.getHeaders() != null) {
                ImageGalleryActivity imageGalleryActivity = ImageGalleryActivity.this;
                loadImage(i, imageGalleryActivity.getGlideUrlByUser(imageGalleryActivity.mImageSources[i]), imagePreviewView, imageView, progressBar);
            } else {
                loadImage(i, ImageGalleryActivity.this.mImageSources[i], imagePreviewView, imageView, progressBar);
            }
            imagePreviewView.setOnClickListener(getListener());
            viewGroup.addView(inflate);
            return inflate;
        }

        private View.OnClickListener getListener() {
            if (this.mFinishClickListener == null) {
                this.mFinishClickListener = new View.OnClickListener() {
                    /* class com.library.gallery.ImageGalleryActivity.ViewPagerAdapter.C21641 */

                    public void onClick(View view) {
                        ImageGalleryActivity.this.finish();
                    }
                };
            }
            return this.mFinishClickListener;
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            viewGroup.removeView((View) obj);
        }

        public void onReachBorder(boolean z) {
            ImageGalleryActivity.this.mImagePager.isInterceptable(z);
        }

        private <T> void loadImage(int i, T t, ImageView imageView, ImageView imageView2, ProgressBar progressBar) {
            final T t2 = t;
            final ProgressBar progressBar2 = progressBar;
            final ImageView imageView3 = imageView2;
            final int i2 = i;
            final ImageView imageView4 = imageView;
            loadImageDoDownAndGetOverrideSize(t, new DoOverrideSizeCallback() {
                /* class com.library.gallery.ImageGalleryActivity.ViewPagerAdapter.C21652 */

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.bumptech.glide.DrawableRequestBuilder.override(int, int):com.bumptech.glide.DrawableRequestBuilder<ModelType>
                 arg types: [int, int]
                 candidates:
                  com.bumptech.glide.DrawableRequestBuilder.override(int, int):com.bumptech.glide.GenericRequestBuilder
                  com.bumptech.glide.GenericRequestBuilder.override(int, int):com.bumptech.glide.GenericRequestBuilder<ModelType, DataType, ResourceType, TranscodeType>
                  com.bumptech.glide.DrawableRequestBuilder.override(int, int):com.bumptech.glide.DrawableRequestBuilder<ModelType> */
                public void onDone(int i, int i2, boolean z) {
                    DrawableRequestBuilder diskCacheStrategy = ImageGalleryActivity.this.mLoader.load(t2).listener((RequestListener) new RequestListener<T, GlideDrawable>() {
                        /* class com.library.gallery.ImageGalleryActivity.ViewPagerAdapter.C21652.C21661 */

                        public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, boolean z, boolean z2) {
                            return onResourceReady((GlideDrawable) obj, obj2, (Target<GlideDrawable>) target, z, z2);
                        }

                        public boolean onException(Exception exc, T t, Target<GlideDrawable> target, boolean z) {
                            if (exc != null) {
                                exc.printStackTrace();
                            }
                            progressBar2.setVisibility(View.GONE);
                            imageView3.setVisibility(View.VISIBLE);
                            ImageGalleryActivity.this.updateDownloadStatus(i2, false);
                            return false;
                        }

                        public boolean onResourceReady(GlideDrawable glideDrawable, T t, Target<GlideDrawable> target, boolean z, boolean z2) {
                            progressBar2.setVisibility(View.GONE);
                            ImageGalleryActivity.this.updateDownloadStatus(i2, true);
                            return false;
                        }
                    }).diskCacheStrategy(DiskCacheStrategy.SOURCE);
                    if (z && i > 0 && i2 > 0) {
                        diskCacheStrategy = diskCacheStrategy.override(i, i2).fitCenter();
                    }
                    diskCacheStrategy.into(imageView4);
                }
            });
        }

        private <T> void loadImageDoDownAndGetOverrideSize(T t, final DoOverrideSizeCallback doOverrideSizeCallback) {
            final FutureTarget<File> downloadOnly = Glide.with((FragmentActivity) ImageGalleryActivity.this).load((Object) t).downloadOnly(Integer.MIN_VALUE, Integer.MIN_VALUE);
            new Thread() {
                /* class com.library.gallery.ImageGalleryActivity.ViewPagerAdapter.C21673 */

                public void run() {
                    final int i;
                    final int i2;
                    try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;
                        BitmapFactory.decodeFile(((File) downloadOnly.get()).getAbsolutePath(), options);
                        int i3 = options.outWidth;
                        int i4 = options.outHeight;
                        BitmapUtil.resetOptions(options);
                        if (i3 <= 0 || i4 <= 0) {
                            ImageGalleryActivity.this.runOnUiThread(new Runnable() {
                                /* class com.library.gallery.ImageGalleryActivity.ViewPagerAdapter.C21673.C21692 */

                                public void run() {
                                    doOverrideSizeCallback.onDone(0, 0, false);
                                }
                            });
                            return;
                        }
                        Point access$600 = ImageGalleryActivity.this.getDisplayDimens();
                        int min = Math.min(Math.min(access$600.y, access$600.x) * 5, 4098);
                        if (((float) i3) / ((float) i4) > ((float) access$600.x) / ((float) access$600.y)) {
                            i = Math.min(i4, access$600.y);
                            i2 = Math.min(i3, min);
                        } else {
                            i2 = Math.min(i3, access$600.x);
                            i = Math.min(i4, min);
                        }
                        ImageGalleryActivity.this.runOnUiThread(new Runnable() {
                            /* class com.library.gallery.ImageGalleryActivity.ViewPagerAdapter.C21673.C21681 */

                            public void run() {
                                doOverrideSizeCallback.onDone(i2, i, true);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        ImageGalleryActivity.this.runOnUiThread(new Runnable() {
                            /* class com.library.gallery.ImageGalleryActivity.ViewPagerAdapter.C21673.C21703 */

                            public void run() {
                                doOverrideSizeCallback.onDone(0, 0, false);
                            }
                        });
                    }
                }
            }.start();
        }
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        EasyPermissions.onRequestPermissionsResult(i, strArr, iArr, this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        mOptions = null;
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public GlideUrl getGlideUrlByUser(String str) {
        SelectOptions selectOptions = mOptions;
        if (selectOptions == null || selectOptions.getHeaders() == null) {
            return new GlideUrl(str);
        }
        return new GlideUrl(str, mOptions.getHeaders());
    }
}
