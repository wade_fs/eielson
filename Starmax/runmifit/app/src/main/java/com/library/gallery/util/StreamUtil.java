package com.library.gallery.util;

import java.io.Closeable;
import java.io.IOException;

public final class StreamUtil {
    public static void close(Closeable... closeableArr) {
        if (closeableArr != null && closeableArr.length != 0) {
            for (Closeable closeable : closeableArr) {
                if (closeable != null) {
                    try {
                        closeable.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: java.io.BufferedInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: java.io.BufferedInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v6, resolved type: java.io.BufferedInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v8, resolved type: java.io.BufferedInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: java.io.BufferedInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v10, resolved type: java.io.BufferedInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v11, resolved type: java.io.BufferedInputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean copyFile(java.io.File r6, java.io.File r7) {
        /*
            java.io.File r0 = r7.getParentFile()
            boolean r1 = r0.exists()
            r2 = 0
            if (r1 != 0) goto L_0x0012
            boolean r0 = r0.mkdirs()
            if (r0 != 0) goto L_0x0012
            return r2
        L_0x0012:
            r0 = 0
            r1 = 2
            r3 = 1
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0052, all -> 0x004f }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0052, all -> 0x004f }
            r5.<init>(r6)     // Catch:{ IOException -> 0x0052, all -> 0x004f }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0052, all -> 0x004f }
            java.io.BufferedOutputStream r6 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x004b, all -> 0x0049 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x004b, all -> 0x0049 }
            r5.<init>(r7)     // Catch:{ IOException -> 0x004b, all -> 0x0049 }
            r6.<init>(r5)     // Catch:{ IOException -> 0x004b, all -> 0x0049 }
            r7 = 4096(0x1000, float:5.74E-42)
            byte[] r7 = new byte[r7]     // Catch:{ IOException -> 0x0047, all -> 0x0045 }
        L_0x002d:
            int r0 = r4.read(r7)     // Catch:{ IOException -> 0x0047, all -> 0x0045 }
            r5 = -1
            if (r0 == r5) goto L_0x0038
            r6.write(r7, r2, r0)     // Catch:{ IOException -> 0x0047, all -> 0x0045 }
            goto L_0x002d
        L_0x0038:
            r6.flush()     // Catch:{ IOException -> 0x0047, all -> 0x0045 }
            java.io.Closeable[] r7 = new java.io.Closeable[r1]
            r7[r2] = r4
            r7[r3] = r6
            close(r7)
            return r3
        L_0x0045:
            r7 = move-exception
            goto L_0x0063
        L_0x0047:
            r7 = move-exception
            goto L_0x004d
        L_0x0049:
            r7 = move-exception
            goto L_0x0064
        L_0x004b:
            r7 = move-exception
            r6 = r0
        L_0x004d:
            r0 = r4
            goto L_0x0054
        L_0x004f:
            r7 = move-exception
            r4 = r0
            goto L_0x0064
        L_0x0052:
            r7 = move-exception
            r6 = r0
        L_0x0054:
            r7.printStackTrace()     // Catch:{ all -> 0x0061 }
            java.io.Closeable[] r7 = new java.io.Closeable[r1]
            r7[r2] = r0
            r7[r3] = r6
            close(r7)
            return r2
        L_0x0061:
            r7 = move-exception
            r4 = r0
        L_0x0063:
            r0 = r6
        L_0x0064:
            java.io.Closeable[] r6 = new java.io.Closeable[r1]
            r6[r2] = r4
            r6[r3] = r0
            close(r6)
            goto L_0x006f
        L_0x006e:
            throw r7
        L_0x006f:
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.library.gallery.util.StreamUtil.copyFile(java.io.File, java.io.File):boolean");
    }
}
