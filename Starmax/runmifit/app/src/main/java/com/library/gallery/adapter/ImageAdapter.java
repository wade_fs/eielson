package com.library.gallery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.library.gallery.R;
import com.library.gallery.bean.Image;
import com.tamic.novate.download.MimeType;

public class ImageAdapter extends BaseRecyclerAdapter<Image> {
    private boolean isSingleSelect;

    public ImageAdapter(Context context) {
        super(context);
    }

    public void setSingleSelect(boolean z) {
        this.isSingleSelect = z;
    }

    public int getItemViewType(int i) {
        return ((Image) getItem(i)).getId() == 0 ? 0 : 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: package-private */
    public RecyclerView.ViewHolder onCreateDefaultViewHolder(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new CamViewHolder(this.mInflater.inflate(R.layout.commom_item_list_cam, viewGroup, false));
        }
        return new ImageViewHolder(this.mInflater.inflate(R.layout.commom_item_list_image, viewGroup, false));
    }

    /* access modifiers changed from: package-private */
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Image image, int i) {
        if (image.getId() != 0) {
            ImageViewHolder imageViewHolder = (ImageViewHolder) viewHolder;
            imageViewHolder.mCheckView.setSelected(image.isSelect());
            int i2 = 0;
            imageViewHolder.mMaskView.setVisibility(image.isSelect() ? 0 : 8);
            imageViewHolder.mGifMask.setVisibility(image.getPath().toLowerCase().endsWith(MimeType.GIF) ? 0 : 8);
            this.mLoader.load(image.getPath()).into(imageViewHolder.mImageView);
            ImageView imageView = imageViewHolder.mCheckView;
            if (this.isSingleSelect) {
                i2 = 8;
            }
            imageView.setVisibility(i2);
        }
    }

    private static class CamViewHolder extends RecyclerView.ViewHolder {
        CamViewHolder(View view) {
            super(view);
        }
    }

    private static class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView mCheckView;
        ImageView mGifMask;
        ImageView mImageView;
        View mMaskView;

        ImageViewHolder(View view) {
            super(view);
            this.mImageView = (ImageView) view.findViewById(R.id.iv_image);
            this.mCheckView = (ImageView) view.findViewById(R.id.cb_selected);
            this.mMaskView = view.findViewById(R.id.lay_mask);
            this.mGifMask = (ImageView) view.findViewById(R.id.iv_is_gif);
        }
    }
}
