package com.library.gallery.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class BitmapUtil {
    public static final int DEFAULT_BUFFER_SIZE = 65536;

    public static BitmapFactory.Options createOptions() {
        return new BitmapFactory.Options();
    }

    public static void resetOptions(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        if (11 <= Build.VERSION.SDK_INT) {
            options.inBitmap = null;
            options.inMutable = true;
        }
    }

    public static String getExtension(String str) {
        BitmapFactory.Options createOptions = createOptions();
        createOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, createOptions);
        String str2 = createOptions.outMimeType;
        return str2.substring(str2.lastIndexOf("/") + 1);
    }

    public static Bitmap decodeBitmap(File file, int i, int i2, byte[] bArr, BitmapFactory.Options options, boolean z) {
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file), bArr == null ? 65536 : bArr.length);
            if (options == null) {
                options = createOptions();
            } else {
                resetOptions(options);
            }
            options.inJustDecodeBounds = true;
            bufferedInputStream.mark(5242880);
            BitmapFactory.decodeStream(bufferedInputStream, null, options);
            try {
                bufferedInputStream.reset();
                calculateScaling(options, i, i2, z);
                if (bArr == null) {
                    bArr = new byte[65536];
                }
                options.inTempStorage = bArr;
                options.inJustDecodeBounds = false;
                Bitmap decodeStream = BitmapFactory.decodeStream(bufferedInputStream, null, options);
                StreamUtil.close(bufferedInputStream);
                resetOptions(options);
                return scaleBitmap(decodeStream, i, i2, true);
            } catch (IOException e) {
                e.printStackTrace();
                StreamUtil.close(bufferedInputStream);
                resetOptions(options);
                return null;
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap scaleBitmap(Bitmap bitmap, float f, boolean z) {
        if (f <= 0.0f || f >= 1.0f) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        matrix.setScale(f, f);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);
        if (z) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int i, int i2, boolean z) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= i && height <= i2) {
            return bitmap;
        }
        float f = (float) width;
        float f2 = (float) height;
        float min = Math.min(((float) i) / f, ((float) i2) / f2);
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) (f * min), (int) (f2 * min), false);
        if (z) {
            bitmap.recycle();
        }
        return createScaledBitmap;
    }

    private static BitmapFactory.Options calculateScaling(BitmapFactory.Options options, int i, int i2, boolean z) {
        int i3 = options.outWidth;
        int i4 = options.outHeight;
        if (i3 <= i && i4 <= i2) {
            return options;
        }
        float f = ((float) i4) / ((float) i2);
        float f2 = (float) i3;
        float max = Math.max(f, f2 / ((float) i));
        int max2 = Math.max(1, Integer.highestOneBit((int) Math.floor((double) max)));
        options.inSampleSize = max2;
        if (z && Build.VERSION.SDK_INT >= 19) {
            options.inTargetDensity = 1000;
            double d = (double) (((f2 / ((float) max2)) / (f2 / max)) * 1000.0f);
            Double.isNaN(d);
            options.inDensity = (int) (d + 0.5d);
            if (options.inTargetDensity != options.inDensity) {
                options.inScaled = true;
            } else {
                options.inTargetDensity = 0;
                options.inDensity = 0;
            }
        }
        return options;
    }

    public static final class Compressor {
        public static File compressImage(File file, long j, int i, int i2, int i3) {
            return compressImage(file, j, i, i2, i3, true);
        }

        public static File compressImage(File file, long j, int i, int i2, int i3, boolean z) {
            return compressImage(file, j, i, i2, i3, null, null, z);
        }

        public static File compressImage(File file, long j, int i, int i2, int i3, byte[] bArr, BitmapFactory.Options options, boolean z) {
            BufferedOutputStream bufferedOutputStream;
            BufferedOutputStream bufferedOutputStream2 = null;
            if (file == null || !file.exists() || !file.canRead()) {
                return null;
            }
            File file2 = new File(file.getParent(), String.format("compress_%s.temp", Long.valueOf(System.currentTimeMillis())));
            if (!file2.exists()) {
                try {
                    if (!file2.createNewFile()) {
                        return null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            Bitmap decodeBitmap = BitmapUtil.decodeBitmap(file, i2, i3, bArr, options, z);
            if (decodeBitmap == null) {
                return null;
            }
            Bitmap.CompressFormat compressFormat = decodeBitmap.hasAlpha() ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG;
            Bitmap bitmap = decodeBitmap;
            boolean z2 = false;
            for (int i4 = 1; i4 <= 10; i4++) {
                int i5 = 92;
                while (true) {
                    try {
                        bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file2));
                        try {
                            bitmap.compress(compressFormat, i5, bufferedOutputStream);
                            StreamUtil.close(bufferedOutputStream);
                            if (file2.length() <= j) {
                                z2 = true;
                                break;
                            } else if (i5 < i) {
                                break;
                            } else {
                                i5--;
                            }
                        } catch (IOException e2) {
                            e = e2;
                            try {
                                e.printStackTrace();
                                bitmap.recycle();
                                StreamUtil.close(bufferedOutputStream);
                                return null;
                            } catch (Throwable th) {
                                th = th;
                                bufferedOutputStream2 = bufferedOutputStream;
                                StreamUtil.close(bufferedOutputStream2);
                                throw th;
                            }
                        }
                    } catch (IOException e3) {
                        e = e3;
                        bufferedOutputStream = null;
                        e.printStackTrace();
                        bitmap.recycle();
                        StreamUtil.close(bufferedOutputStream);
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        StreamUtil.close(bufferedOutputStream2);
                        throw th;
                    }
                }
                if (z2) {
                    break;
                }
                bitmap = BitmapUtil.scaleBitmap(bitmap, 1.0f - (((float) i4) * 0.2f), true);
            }
            bitmap.recycle();
            if (!z2) {
                return null;
            }
            return file2;
        }
    }
}
