package com.library.gallery;

import com.bumptech.glide.load.model.LazyHeaders;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectOptions {
    /* access modifiers changed from: private */
    public boolean hasCam;
    /* access modifiers changed from: private */
    public boolean isCrop;
    /* access modifiers changed from: private */
    public Callback mCallback;
    /* access modifiers changed from: private */
    public int mCropHeight;
    /* access modifiers changed from: private */
    public int mCropWidth;
    /* access modifiers changed from: private */
    public LazyHeaders mHeaders;
    /* access modifiers changed from: private */
    public String mSavePath;
    /* access modifiers changed from: private */
    public int mSelectCount;
    /* access modifiers changed from: private */
    public List<String> mSelectedImages;

    public interface Callback {
        void doSelected(String[] strArr);
    }

    private SelectOptions() {
    }

    public boolean isCrop() {
        return this.isCrop;
    }

    public int getCropWidth() {
        return this.mCropWidth;
    }

    public int getCropHeight() {
        return this.mCropHeight;
    }

    public Callback getCallback() {
        return this.mCallback;
    }

    public boolean isHasCam() {
        return this.hasCam;
    }

    public int getSelectCount() {
        return this.mSelectCount;
    }

    public List<String> getSelectedImages() {
        return this.mSelectedImages;
    }

    public LazyHeaders getHeaders() {
        return this.mHeaders;
    }

    public String getSavePath() {
        return this.mSavePath;
    }

    public static class Builder {
        private Callback callback;
        private int cropHeight;
        private int cropWidth;
        private LazyHeaders.Builder glideHeader;
        private boolean hasCam = true;
        private boolean isCrop;
        private String savePath;
        private int selectCount = 1;
        private List<String> selectedImages = new ArrayList();

        public Builder setCrop(int i, int i2) {
            if (i <= 0 || i2 <= 0) {
                throw new IllegalArgumentException("cropWidth or cropHeight mast be greater than 0 ");
            }
            this.isCrop = true;
            this.cropWidth = i;
            this.cropHeight = i2;
            return this;
        }

        public Builder setCallback(Callback callback2) {
            this.callback = callback2;
            return this;
        }

        public Builder setHasCam(boolean z) {
            this.hasCam = z;
            return this;
        }

        public Builder setSelectCount(int i) {
            if (i <= 0) {
                i = 1;
            }
            this.selectCount = i;
            return this;
        }

        public Builder setSelectedImages(List<String> list) {
            if (!(list == null || list.size() == 0)) {
                this.selectedImages.addAll(list);
            }
            return this;
        }

        public Builder setSelectedImages(String[] strArr) {
            if (!(strArr == null || strArr.length == 0)) {
                if (this.selectedImages == null) {
                    this.selectedImages = new ArrayList();
                }
                this.selectedImages.addAll(Arrays.asList(strArr));
            }
            return this;
        }

        public Builder setGlideHeader(String str, String str2) {
            if (this.glideHeader == null) {
                this.glideHeader = new LazyHeaders.Builder();
            }
            this.glideHeader.addHeader(str, str2);
            return this;
        }

        public Builder setSavaPath(String str) {
            this.savePath = str;
            return this;
        }

        public SelectOptions build() {
            SelectOptions selectOptions = new SelectOptions();
            boolean unused = selectOptions.hasCam = this.hasCam;
            boolean unused2 = selectOptions.isCrop = this.isCrop;
            int unused3 = selectOptions.mCropHeight = this.cropHeight;
            int unused4 = selectOptions.mCropWidth = this.cropWidth;
            Callback unused5 = selectOptions.mCallback = this.callback;
            int unused6 = selectOptions.mSelectCount = this.selectCount;
            List unused7 = selectOptions.mSelectedImages = this.selectedImages;
            String unused8 = selectOptions.mSavePath = this.savePath;
            if (this.isCrop) {
                int unused9 = selectOptions.mSelectCount = 1;
            }
            LazyHeaders.Builder builder = this.glideHeader;
            if (builder != null) {
                LazyHeaders unused10 = selectOptions.mHeaders = builder.build();
            }
            return selectOptions;
        }
    }
}
