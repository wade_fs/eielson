package com.library.gallery.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.library.gallery.util.Util;

public class CropLayout extends FrameLayout {
    private int mCropHeight = 500;
    private CropFloatView mCropView;
    private int mCropWidth = 500;
    private ZoomImageView mZoomImageView;

    public CropLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mZoomImageView = new ZoomImageView(context);
        this.mCropView = new CropFloatView(context);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        addView(this.mZoomImageView, layoutParams);
        addView(this.mCropView, layoutParams);
    }

    public ZoomImageView getImageView() {
        return this.mZoomImageView;
    }

    public Bitmap cropBitmap() {
        return this.mZoomImageView.cropBitmap();
    }

    public void setCropWidth(int i) {
        this.mCropWidth = i;
        this.mCropView.setCropWidth(i);
        this.mZoomImageView.setCropWidth(i);
    }

    public void setCropHeight(int i) {
        this.mCropHeight = i;
        this.mCropView.setCropHeight(i);
        this.mZoomImageView.setCropHeight(i);
    }

    public void start() {
        int screenHeight = Util.getScreenHeight(getContext());
        this.mZoomImageView.setHOffset((Util.getScreenWidth(getContext()) - this.mCropWidth) / 2);
        this.mZoomImageView.setVOffset((screenHeight - this.mCropHeight) / 2);
    }
}
