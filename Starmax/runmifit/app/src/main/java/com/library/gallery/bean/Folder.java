package com.library.gallery.bean;

import java.util.ArrayList;

public class Folder {
    private String albumPath;
    private ArrayList<Image> images = new ArrayList<>();
    private String name;
    private String path;

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String str) {
        this.path = str;
    }

    public ArrayList<Image> getImages() {
        return this.images;
    }

    public String getAlbumPath() {
        return this.albumPath;
    }

    public void setAlbumPath(String str) {
        this.albumPath = str;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Folder)) {
            return false;
        }
        Folder folder = (Folder) obj;
        if (folder.getPath() != null || this.path == null) {
            return folder.getPath().toLowerCase().equals(this.path.toLowerCase());
        }
        return false;
    }
}
