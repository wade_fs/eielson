package com.library.gallery.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

public class ZoomImageView extends AppCompatImageView implements ScaleGestureDetector.OnScaleGestureListener, View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener {
    private float SCALE_MAX;
    /* access modifiers changed from: private */
    public float SCALE_MID;
    private float SCALE_MIN;
    /* access modifiers changed from: private */
    public boolean isAutoScale;
    private boolean isCanDrag;
    private boolean isFirst;
    private boolean isInit;
    private int lastPointerCount;
    private int mCropHeight;
    private int mCropWidth;
    private GestureDetector mGestureDetector;
    private float mLastX;
    private float mLastY;
    private final float[] mMatrixValues;
    private int mOffset;
    /* access modifiers changed from: private */
    public float mScale;
    private ScaleGestureDetector mScaleGestureDetector;
    /* access modifiers changed from: private */
    public Matrix mScaleMatrix;
    private int mVOffset;

    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return true;
    }

    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
    }

    public ZoomImageView(Context context) {
        this(context, null);
    }

    public ZoomImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mOffset = 0;
        this.mVOffset = 0;
        this.SCALE_MAX = 4.0f;
        this.SCALE_MID = 2.0f;
        this.SCALE_MIN = 1.0f;
        this.mScale = 1.0f;
        this.isFirst = true;
        this.mMatrixValues = new float[9];
        this.mScaleGestureDetector = null;
        this.mScaleMatrix = new Matrix();
        setScaleType(ImageView.ScaleType.MATRIX);
        this.mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            /* class com.library.gallery.view.ZoomImageView.C21831 */

            public boolean onDoubleTap(MotionEvent motionEvent) {
                if (ZoomImageView.this.isAutoScale) {
                    return true;
                }
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                if (ZoomImageView.this.getScale() < ZoomImageView.this.SCALE_MID) {
                    ZoomImageView zoomImageView = ZoomImageView.this;
                    zoomImageView.postDelayed(new ScaleRunnable(zoomImageView.SCALE_MID, x, y), 16);
                    boolean unused = ZoomImageView.this.isAutoScale = true;
                } else {
                    ZoomImageView zoomImageView2 = ZoomImageView.this;
                    zoomImageView2.postDelayed(new ScaleRunnable(zoomImageView2.mScale, x, y), 16);
                    boolean unused2 = ZoomImageView.this.isAutoScale = true;
                }
                return true;
            }
        });
        this.mScaleGestureDetector = new ScaleGestureDetector(context, this);
        setOnTouchListener(this);
    }

    private class ScaleRunnable implements Runnable {
        static final float BIGGER = 1.07f;
        static final float SMALLER = 0.93f;
        private float mScale;
        private float mTargetScale;

        /* renamed from: x */
        private float f4620x;

        /* renamed from: y */
        private float f4621y;

        ScaleRunnable(float f, float f2, float f3) {
            this.mTargetScale = f;
            this.f4620x = f2;
            this.f4621y = f3;
            if (ZoomImageView.this.getScale() < this.mTargetScale) {
                this.mScale = BIGGER;
            } else {
                this.mScale = SMALLER;
            }
        }

        public void run() {
            Matrix access$300 = ZoomImageView.this.mScaleMatrix;
            float f = this.mScale;
            access$300.postScale(f, f, this.f4620x, this.f4621y);
            ZoomImageView.this.checkBorder();
            ZoomImageView zoomImageView = ZoomImageView.this;
            zoomImageView.setImageMatrix(zoomImageView.mScaleMatrix);
            float scale = ZoomImageView.this.getScale();
            if ((this.mScale <= 1.0f || scale >= this.mTargetScale) && (this.mScale >= 1.0f || this.mTargetScale >= scale)) {
                float f2 = this.mTargetScale / scale;
                ZoomImageView.this.mScaleMatrix.postScale(f2, f2, this.f4620x, this.f4621y);
                ZoomImageView.this.checkBorder();
                ZoomImageView zoomImageView2 = ZoomImageView.this;
                zoomImageView2.setImageMatrix(zoomImageView2.mScaleMatrix);
                boolean unused = ZoomImageView.this.isAutoScale = false;
                return;
            }
            ZoomImageView.this.postDelayed(this, 16);
        }
    }

    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        float scale = getScale();
        float scaleFactor = scaleGestureDetector.getScaleFactor();
        if (getDrawable() == null) {
            return true;
        }
        if ((scale < this.SCALE_MAX && scaleFactor > this.SCALE_MIN) || (scale > this.mScale && scaleFactor < this.SCALE_MIN)) {
            float f = this.SCALE_MIN;
            if (scaleFactor * scale < f) {
                scaleFactor = f / scale;
            }
            float f2 = this.SCALE_MAX;
            if (scaleFactor * scale > f2) {
                scaleFactor = f2 / scale;
            }
            this.mScaleMatrix.postScale(scaleFactor, scaleFactor, scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
            checkBorder();
            setImageMatrix(this.mScaleMatrix);
        }
        return true;
    }

    private RectF getMatrixRectF() {
        Matrix matrix = this.mScaleMatrix;
        RectF rectF = new RectF();
        Drawable drawable = getDrawable();
        if (drawable != null) {
            rectF.set(0.0f, 0.0f, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            matrix.mapRect(rectF);
        }
        return rectF;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        if (r10 != 3) goto L_0x009d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r10, android.view.MotionEvent r11) {
        /*
            r9 = this;
            android.view.GestureDetector r10 = r9.mGestureDetector
            boolean r10 = r10.onTouchEvent(r11)
            r0 = 1
            if (r10 == 0) goto L_0x000a
            return r0
        L_0x000a:
            android.view.ScaleGestureDetector r10 = r9.mScaleGestureDetector
            r10.onTouchEvent(r11)
            int r10 = r11.getPointerCount()
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
        L_0x0018:
            if (r3 >= r10) goto L_0x0027
            float r6 = r11.getX(r3)
            float r4 = r4 + r6
            float r6 = r11.getY(r3)
            float r5 = r5 + r6
            int r3 = r3 + 1
            goto L_0x0018
        L_0x0027:
            float r3 = (float) r10
            float r4 = r4 / r3
            float r5 = r5 / r3
            int r3 = r9.lastPointerCount
            if (r10 == r3) goto L_0x0034
            r9.isCanDrag = r1
            r9.mLastX = r4
            r9.mLastY = r5
        L_0x0034:
            r9.lastPointerCount = r10
            int r10 = r11.getAction()
            if (r10 == r0) goto L_0x009b
            r11 = 2
            if (r10 == r11) goto L_0x0043
            r11 = 3
            if (r10 == r11) goto L_0x009b
            goto L_0x009d
        L_0x0043:
            float r10 = r9.mLastX
            float r10 = r4 - r10
            float r1 = r9.mLastY
            float r1 = r5 - r1
            boolean r3 = r9.isCanDrag
            if (r3 != 0) goto L_0x0055
            boolean r3 = r9.isCanDrag(r10, r1)
            r9.isCanDrag = r3
        L_0x0055:
            boolean r3 = r9.isCanDrag
            if (r3 == 0) goto L_0x0096
            android.graphics.drawable.Drawable r3 = r9.getDrawable()
            if (r3 == 0) goto L_0x0096
            android.graphics.RectF r3 = r9.getMatrixRectF()
            float r6 = r3.width()
            int r7 = r9.getWidth()
            int r8 = r9.mOffset
            int r8 = r8 * 2
            int r7 = r7 - r8
            float r7 = (float) r7
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 > 0) goto L_0x0076
            r10 = 0
        L_0x0076:
            float r3 = r3.height()
            int r6 = r9.getHeight()
            int r7 = r9.mVOffset
            int r7 = r7 * 2
            int r6 = r6 - r7
            float r11 = (float) r6
            int r11 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r11 > 0) goto L_0x0089
            r1 = 0
        L_0x0089:
            android.graphics.Matrix r11 = r9.mScaleMatrix
            r11.postTranslate(r10, r1)
            r9.checkBorder()
            android.graphics.Matrix r10 = r9.mScaleMatrix
            r9.setImageMatrix(r10)
        L_0x0096:
            r9.mLastX = r4
            r9.mLastY = r5
            goto L_0x009d
        L_0x009b:
            r9.lastPointerCount = r1
        L_0x009d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.library.gallery.view.ZoomImageView.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    public final float getScale() {
        this.mScaleMatrix.getValues(this.mMatrixValues);
        return this.mMatrixValues[0];
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mScaleMatrix = null;
        getViewTreeObserver().removeGlobalOnLayoutListener(this);
    }

    /* access modifiers changed from: protected */
    public boolean setFrame(int i, int i2, int i3, int i4) {
        if (this.isInit) {
            return false;
        }
        boolean frame = super.setFrame(i, i2, i3, i4);
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return false;
        }
        int width = drawable.getBounds().width();
        int height = drawable.getBounds().height();
        if (width > this.mCropWidth || height > this.mCropHeight) {
            return false;
        }
        int width2 = getWidth();
        int height2 = getHeight();
        this.mScale = ((float) width2) / ((float) width);
        this.isInit = true;
        postDelayed(new ScaleRunnable(this.mScale, (float) (width2 / 2), (float) (height2 / 2)), 50);
        this.isAutoScale = false;
        return frame;
    }

    public void onGlobalLayout() {
        Drawable drawable;
        if (this.isFirst && (drawable = getDrawable()) != null) {
            this.mVOffset = (getHeight() - (getWidth() - (this.mOffset * 2))) / 2;
            int width = getWidth();
            int height = getHeight();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            float width2 = (intrinsicWidth >= getWidth() - (this.mOffset * 2) || intrinsicHeight <= getHeight() - (this.mVOffset * 2)) ? 1.0f : ((((float) getWidth()) * 1.0f) - ((float) (this.mOffset * 2))) / ((float) intrinsicWidth);
            if (intrinsicHeight < getHeight() - (this.mVOffset * 2) && intrinsicWidth > getWidth() - (this.mOffset * 2)) {
                width2 = ((((float) getHeight()) * 1.0f) - ((float) (this.mVOffset * 2))) / ((float) intrinsicHeight);
            }
            if (intrinsicWidth < getWidth() - (this.mOffset * 2) && intrinsicHeight < getHeight() - (this.mVOffset * 2)) {
                width2 = Math.max(((((float) getWidth()) * 1.0f) - ((float) (this.mOffset * 2))) / ((float) intrinsicWidth), ((((float) getHeight()) * 1.0f) - ((float) (this.mVOffset * 2))) / ((float) intrinsicHeight));
            }
            this.SCALE_MIN = Math.max(((float) (width - (this.mOffset * 2))) / ((float) intrinsicWidth), ((float) (height - (this.mVOffset * 2))) / ((float) intrinsicHeight));
            if (this.SCALE_MIN >= 1.0f) {
                this.SCALE_MIN = 1.0f;
            }
            this.mScale = width2;
            float f = this.mScale;
            this.SCALE_MID = 2.0f * f;
            this.SCALE_MAX = f * 4.0f;
            this.mScaleMatrix.postTranslate((float) ((width - intrinsicWidth) / 2), (float) ((height - intrinsicHeight) / 2));
            this.mScaleMatrix.postScale(width2, width2, (float) (getWidth() / 2), (float) (getHeight() / 2));
            setImageMatrix(this.mScaleMatrix);
            this.isFirst = false;
        }
    }

    public Bitmap cropBitmap() {
        Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        draw(new Canvas(createBitmap));
        return Bitmap.createBitmap(createBitmap, this.mOffset, this.mVOffset, getWidth() - (this.mOffset * 2), getWidth() - (this.mOffset * 2));
    }

    /* access modifiers changed from: private */
    public void checkBorder() {
        float f;
        RectF matrixRectF = getMatrixRectF();
        int width = getWidth();
        int height = getHeight();
        double width2 = (double) matrixRectF.width();
        Double.isNaN(width2);
        float f2 = 0.0f;
        if (width2 + 0.01d >= ((double) (width - (this.mOffset * 2)))) {
            float f3 = matrixRectF.left > ((float) this.mOffset) ? (-matrixRectF.left) + ((float) this.mOffset) : 0.0f;
            float f4 = matrixRectF.right;
            int i = this.mOffset;
            f = f4 < ((float) (width - i)) ? ((float) (width - i)) - matrixRectF.right : f3;
        } else {
            f = 0.0f;
        }
        double height2 = (double) matrixRectF.height();
        Double.isNaN(height2);
        if (height2 + 0.01d >= ((double) (height - (this.mVOffset * 2)))) {
            if (matrixRectF.top > ((float) this.mVOffset)) {
                f2 = (-matrixRectF.top) + ((float) this.mVOffset);
            }
            float f5 = matrixRectF.bottom;
            int i2 = this.mVOffset;
            if (f5 < ((float) (height - i2))) {
                f2 = ((float) (height - i2)) - matrixRectF.bottom;
            }
        }
        this.mScaleMatrix.postTranslate(f, f2);
    }

    private boolean isCanDrag(float f, float f2) {
        return Math.sqrt((double) ((f * f) + (f2 * f2))) >= 0.0d;
    }

    public void setHOffset(int i) {
        this.mOffset = i;
    }

    public void setVOffset(int i) {
        this.mVOffset = i;
    }

    public void setCropWidth(int i) {
        this.mCropWidth = i;
    }

    public void setCropHeight(int i) {
        this.mCropHeight = i;
    }
}
