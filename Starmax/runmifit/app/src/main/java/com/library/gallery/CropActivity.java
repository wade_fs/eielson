package com.library.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.bumptech.glide.Glide;
import com.library.gallery.view.CropLayout;

public class CropActivity extends AppCompatActivity implements View.OnClickListener {
    private static SelectOptions mOption;
    private View mCancel;
    private View mCrop;
    private CropLayout mCropLayout;

    public static void show(Fragment fragment, SelectOptions selectOptions) {
        Intent intent = new Intent(fragment.getActivity(), CropActivity.class);
        mOption = selectOptions;
        fragment.startActivityForResult(intent, 4);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTheme(R.style.Theme_Translate);
        setContentView(R.layout.commom_activity_crop);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null && supportActionBar.isShowing()) {
            supportActionBar.hide();
        }
        initWidget();
        initData();
    }

    private void initWidget() {
        setTitle("");
        getWindow().setLayout(-1, -1);
        this.mCropLayout = (CropLayout) findViewById(R.id.cropLayout);
        this.mCrop = findViewById(R.id.tv_crop);
        this.mCancel = findViewById(R.id.tv_cancel);
        this.mCrop.setOnClickListener(this);
        this.mCancel.setOnClickListener(this);
    }

    private void initData() {
        Glide.with((FragmentActivity) this).load(mOption.getSelectedImages().get(0)).fitCenter().into(this.mCropLayout.getImageView());
        this.mCropLayout.setCropWidth(mOption.getCropWidth());
        this.mCropLayout.setCropHeight(mOption.getCropHeight());
        this.mCropLayout.start();
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r7) {
        /*
            r6 = this;
            android.view.View r0 = r6.mCrop
            if (r7 != r0) goto L_0x0083
            r7 = 0
            r0 = 0
            r1 = 1
            com.library.gallery.view.CropLayout r2 = r6.mCropLayout     // Catch:{ Exception -> 0x0061, all -> 0x005c }
            android.graphics.Bitmap r2 = r2.cropBitmap()     // Catch:{ Exception -> 0x0061, all -> 0x005c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
            r3.<init>()     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
            java.io.File r4 = r6.getFilesDir()     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
            r3.append(r4)     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
            java.lang.String r4 = "/crop.jpg"
            r3.append(r4)     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0058, all -> 0x0054 }
            android.graphics.Bitmap$CompressFormat r7 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x0052 }
            r5 = 100
            r2.compress(r7, r5, r4)     // Catch:{ Exception -> 0x0052 }
            r4.flush()     // Catch:{ Exception -> 0x0052 }
            r4.close()     // Catch:{ Exception -> 0x0052 }
            android.content.Intent r7 = new android.content.Intent     // Catch:{ Exception -> 0x0052 }
            r7.<init>()     // Catch:{ Exception -> 0x0052 }
            java.lang.String r5 = "crop_path"
            r7.putExtra(r5, r3)     // Catch:{ Exception -> 0x0052 }
            r3 = -1
            r6.setResult(r3, r7)     // Catch:{ Exception -> 0x0052 }
            r6.finish()     // Catch:{ Exception -> 0x0052 }
            if (r2 == 0) goto L_0x004a
            r2.recycle()
        L_0x004a:
            java.io.Closeable[] r7 = new java.io.Closeable[r1]
            r7[r0] = r4
            com.library.gallery.util.StreamUtil.close(r7)
            goto L_0x008a
        L_0x0052:
            r7 = move-exception
            goto L_0x0065
        L_0x0054:
            r3 = move-exception
            r4 = r7
            r7 = r3
            goto L_0x0076
        L_0x0058:
            r3 = move-exception
            r4 = r7
            r7 = r3
            goto L_0x0065
        L_0x005c:
            r2 = move-exception
            r4 = r7
            r7 = r2
            r2 = r4
            goto L_0x0076
        L_0x0061:
            r2 = move-exception
            r4 = r7
            r7 = r2
            r2 = r4
        L_0x0065:
            r7.printStackTrace()     // Catch:{ all -> 0x0075 }
            if (r2 == 0) goto L_0x006d
            r2.recycle()
        L_0x006d:
            java.io.Closeable[] r7 = new java.io.Closeable[r1]
            r7[r0] = r4
            com.library.gallery.util.StreamUtil.close(r7)
            goto L_0x008a
        L_0x0075:
            r7 = move-exception
        L_0x0076:
            if (r2 == 0) goto L_0x007b
            r2.recycle()
        L_0x007b:
            java.io.Closeable[] r1 = new java.io.Closeable[r1]
            r1[r0] = r4
            com.library.gallery.util.StreamUtil.close(r1)
            throw r7
        L_0x0083:
            android.view.View r0 = r6.mCancel
            if (r7 != r0) goto L_0x008a
            r6.finish()
        L_0x008a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.library.gallery.CropActivity.onClick(android.view.View):void");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        mOption = null;
        super.onDestroy();
    }
}
