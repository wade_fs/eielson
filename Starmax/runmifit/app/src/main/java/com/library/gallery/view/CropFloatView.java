package com.library.gallery.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Region;
import android.view.View;
import com.library.gallery.CropDrawable;

class CropFloatView extends View {
    private boolean isCrop;
    private CropDrawable mCropDrawable;
    private Rect mFloatRect = new Rect();

    public CropFloatView(Context context) {
        super(context);
        this.mCropDrawable = new CropDrawable(context);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        cropDrawable();
        canvas.save();
        canvas.clipRect(this.mFloatRect, Region.Op.DIFFERENCE);
        canvas.drawColor(Color.parseColor("#a0000000"));
        canvas.restore();
        this.mCropDrawable.draw(canvas);
    }

    private void cropDrawable() {
        if (!this.isCrop) {
            this.mCropDrawable.setRegion(this.mFloatRect);
            this.isCrop = true;
        }
    }

    public void setCropWidth(int i) {
        this.mCropDrawable.setCropWidth(i);
    }

    public void setCropHeight(int i) {
        this.mCropDrawable.setCropHeight(i);
    }
}
