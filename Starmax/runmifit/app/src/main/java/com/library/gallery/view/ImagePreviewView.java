package com.library.gallery.view;

import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

public class ImagePreviewView extends AppCompatImageView {
    private static final float mMaxScale = 4.0f;
    private static final float mMinScale = 0.4f;
    /* access modifiers changed from: private */
    public boolean isAutoScale;
    private AccelerateInterpolator mAccInterpolator;
    /* access modifiers changed from: private */
    public int mBoundHeight;
    /* access modifiers changed from: private */
    public int mBoundWidth;
    /* access modifiers changed from: private */
    public DecelerateInterpolator mDecInterpolator;
    private GestureDetector mFlatDetector;
    private FloatEvaluator mFloatEvaluator;
    private ScaleGestureDetector mScaleDetector;
    /* access modifiers changed from: private */
    public OnReachBorderListener onReachBorderListener;
    private ValueAnimator.AnimatorUpdateListener onScaleAnimationUpdate;
    private ValueAnimator.AnimatorUpdateListener onTranslateXAnimationUpdate;
    private ValueAnimator.AnimatorUpdateListener onTranslateYAnimationUpdate;
    private ValueAnimator resetScaleAnimator;
    private ValueAnimator resetXAnimator;
    private ValueAnimator resetYAnimator;
    /* access modifiers changed from: private */
    public float scale;
    /* access modifiers changed from: private */
    public float translateLeft;
    /* access modifiers changed from: private */
    public float translateTop;

    public interface OnReachBorderListener {
        void onReachBorder(boolean z);
    }

    /* access modifiers changed from: private */
    public float getDefaultTranslateTop(int i, int i2) {
        float f = ((float) (i - i2)) / 2.0f;
        if (f > 0.0f) {
            return f;
        }
        return 0.0f;
    }

    public void setOnReachBorderListener(OnReachBorderListener onReachBorderListener2) {
        this.onReachBorderListener = onReachBorderListener2;
    }

    public ImagePreviewView(Context context) {
        this(context, null);
    }

    public ImagePreviewView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ImagePreviewView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.scale = 1.0f;
        this.translateLeft = 0.0f;
        this.translateTop = 0.0f;
        this.mBoundWidth = 0;
        this.mBoundHeight = 0;
        this.isAutoScale = false;
        this.mFloatEvaluator = new FloatEvaluator();
        this.mAccInterpolator = new AccelerateInterpolator();
        this.mDecInterpolator = new DecelerateInterpolator();
        this.mScaleDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
        this.mFlatDetector = new GestureDetector(getContext(), new FlatGestureListener());
    }

    public ValueAnimator.AnimatorUpdateListener getOnScaleAnimationUpdate() {
        ValueAnimator.AnimatorUpdateListener animatorUpdateListener = this.onScaleAnimationUpdate;
        if (animatorUpdateListener != null) {
            return animatorUpdateListener;
        }
        this.onScaleAnimationUpdate = new ValueAnimator.AnimatorUpdateListener() {
            /* class com.library.gallery.view.ImagePreviewView.C21791 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = ImagePreviewView.this.scale = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                ImagePreviewView.this.invalidate();
            }
        };
        return this.onScaleAnimationUpdate;
    }

    public ValueAnimator.AnimatorUpdateListener getOnTranslateXAnimationUpdate() {
        ValueAnimator.AnimatorUpdateListener animatorUpdateListener = this.onTranslateXAnimationUpdate;
        if (animatorUpdateListener != null) {
            return animatorUpdateListener;
        }
        this.onTranslateXAnimationUpdate = new ValueAnimator.AnimatorUpdateListener() {
            /* class com.library.gallery.view.ImagePreviewView.C21802 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = ImagePreviewView.this.translateLeft = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                ImagePreviewView.this.invalidate();
            }
        };
        return this.onTranslateXAnimationUpdate;
    }

    public ValueAnimator.AnimatorUpdateListener getOnTranslateYAnimationUpdate() {
        ValueAnimator.AnimatorUpdateListener animatorUpdateListener = this.onTranslateYAnimationUpdate;
        if (animatorUpdateListener != null) {
            return animatorUpdateListener;
        }
        this.onTranslateYAnimationUpdate = new ValueAnimator.AnimatorUpdateListener() {
            /* class com.library.gallery.view.ImagePreviewView.C21813 */

            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float unused = ImagePreviewView.this.translateTop = ((Float) valueAnimator.getAnimatedValue()).floatValue();
                ImagePreviewView.this.invalidate();
            }
        };
        return this.onTranslateYAnimationUpdate;
    }

    /* access modifiers changed from: private */
    public ValueAnimator getResetScaleAnimator() {
        ValueAnimator valueAnimator = this.resetScaleAnimator;
        if (valueAnimator != null) {
            valueAnimator.removeAllUpdateListeners();
        } else {
            this.resetScaleAnimator = ValueAnimator.ofFloat(new float[0]);
        }
        this.resetScaleAnimator.setDuration(150L);
        this.resetScaleAnimator.setInterpolator(this.mAccInterpolator);
        this.resetScaleAnimator.setEvaluator(this.mFloatEvaluator);
        return this.resetScaleAnimator;
    }

    /* access modifiers changed from: private */
    public ValueAnimator getResetXAnimator() {
        ValueAnimator valueAnimator = this.resetXAnimator;
        if (valueAnimator != null) {
            valueAnimator.removeAllUpdateListeners();
        } else {
            this.resetXAnimator = ValueAnimator.ofFloat(new float[0]);
        }
        this.resetXAnimator.setDuration(150L);
        this.resetXAnimator.setInterpolator(this.mAccInterpolator);
        this.resetXAnimator.setEvaluator(this.mFloatEvaluator);
        return this.resetXAnimator;
    }

    /* access modifiers changed from: private */
    public ValueAnimator getResetYAnimator() {
        ValueAnimator valueAnimator = this.resetYAnimator;
        if (valueAnimator != null) {
            valueAnimator.removeAllUpdateListeners();
        } else {
            this.resetYAnimator = ValueAnimator.ofFloat(new float[0]);
        }
        this.resetYAnimator.setDuration(150L);
        this.resetYAnimator.setInterpolator(this.mAccInterpolator);
        this.resetYAnimator.setEvaluator(this.mFloatEvaluator);
        return this.resetYAnimator;
    }

    private void cancelAnimation() {
        ValueAnimator valueAnimator = this.resetScaleAnimator;
        if (valueAnimator != null && valueAnimator.isRunning()) {
            this.resetScaleAnimator.cancel();
        }
        ValueAnimator valueAnimator2 = this.resetXAnimator;
        if (valueAnimator2 != null && valueAnimator2.isRunning()) {
            this.resetXAnimator.cancel();
        }
        ValueAnimator valueAnimator3 = this.resetYAnimator;
        if (valueAnimator3 != null && valueAnimator3.isRunning()) {
            this.resetYAnimator.cancel();
        }
    }

    /* access modifiers changed from: private */
    public float getDiffX() {
        float f = ((float) this.mBoundWidth) * this.scale;
        float f2 = this.translateLeft;
        if (f2 >= 0.0f) {
            return f2;
        }
        if ((((float) getWidth()) - this.translateLeft) - f > 0.0f) {
            return -((((float) getWidth()) - this.translateLeft) - f);
        }
        return 0.0f;
    }

    /* access modifiers changed from: private */
    public float getDiffY() {
        float f = ((float) this.mBoundHeight) * this.scale;
        float f2 = this.translateTop;
        if (f2 >= 0.0f) {
            return f2;
        }
        if ((((float) getHeight()) - this.translateTop) - f > 0.0f) {
            return -((((float) getHeight()) - this.translateTop) - f);
        }
        return 0.0f;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            cancelAnimation();
        }
        this.mFlatDetector.onTouchEvent(motionEvent);
        this.mScaleDetector.onTouchEvent(motionEvent);
        if (action == 1 || action == 3) {
            if (this.isAutoScale) {
                this.isAutoScale = false;
            } else {
                if (this.scale < 1.0f) {
                    ValueAnimator resetScaleAnimator2 = getResetScaleAnimator();
                    resetScaleAnimator2.setFloatValues(this.scale, 1.0f);
                    resetScaleAnimator2.addUpdateListener(getOnScaleAnimationUpdate());
                    resetScaleAnimator2.start();
                }
                float f = this.scale;
                float f2 = ((float) this.mBoundWidth) * f;
                float f3 = ((float) this.mBoundHeight) * f;
                float diffX = getDiffX();
                float diffY = getDiffY();
                if (f2 >= ((float) getWidth()) && diffX != 0.0f) {
                    ValueAnimator resetXAnimator2 = getResetXAnimator();
                    float f4 = this.translateLeft;
                    resetXAnimator2.setFloatValues(f4, f4 - diffX);
                    resetXAnimator2.addUpdateListener(getOnTranslateXAnimationUpdate());
                    resetXAnimator2.start();
                }
                if (f3 >= ((float) getHeight()) && diffY != 0.0f) {
                    ValueAnimator resetYAnimator2 = getResetYAnimator();
                    float f5 = this.translateTop;
                    resetYAnimator2.setFloatValues(f5, f5 - diffY);
                    resetYAnimator2.addUpdateListener(getOnTranslateYAnimationUpdate());
                    resetYAnimator2.start();
                }
                if (f2 < ((float) getWidth()) && f3 >= ((float) getHeight()) && diffX != 0.0f) {
                    ValueAnimator resetXAnimator3 = getResetXAnimator();
                    resetXAnimator3.setFloatValues(this.translateLeft, 0.0f);
                    resetXAnimator3.addUpdateListener(getOnTranslateXAnimationUpdate());
                    resetXAnimator3.start();
                }
                if (f3 < ((float) getHeight()) && f2 >= ((float) getWidth()) && diffY != 0.0f) {
                    ValueAnimator resetYAnimator3 = getResetYAnimator();
                    resetYAnimator3.setFloatValues(this.translateTop, (((float) getHeight()) - f3) / 2.0f);
                    resetYAnimator3.addUpdateListener(getOnTranslateYAnimationUpdate());
                    resetYAnimator3.start();
                }
                if (f2 < ((float) getWidth()) && f3 < ((float) getHeight())) {
                    resetDefaultState();
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void resetDefaultState() {
        if (this.translateLeft != 0.0f) {
            ValueAnimator resetXAnimator2 = getResetXAnimator();
            resetXAnimator2.setFloatValues(this.translateLeft, 0.0f);
            resetXAnimator2.addUpdateListener(getOnTranslateXAnimationUpdate());
            resetXAnimator2.start();
        }
        ValueAnimator resetYAnimator2 = getResetYAnimator();
        resetYAnimator2.setFloatValues(this.translateTop, getDefaultTranslateTop(getHeight(), this.mBoundHeight));
        resetYAnimator2.addUpdateListener(getOnTranslateYAnimationUpdate());
        resetYAnimator2.start();
    }

    /* access modifiers changed from: protected */
    public boolean setFrame(int i, int i2, int i3, int i4) {
        super.setFrame(i, i2, i3, i4);
        if (getDrawable() == null) {
            return false;
        }
        if (this.mBoundWidth != 0 && this.mBoundHeight != 0 && this.scale != 1.0f) {
            return false;
        }
        adjustBounds(getWidth(), getHeight());
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        adjustBounds(i, i2);
    }

    private void adjustBounds(int i, int i2) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            this.mBoundWidth = drawable.getBounds().width();
            this.mBoundHeight = drawable.getBounds().height();
            this.mBoundHeight = (int) (((float) this.mBoundHeight) / (((float) this.mBoundWidth) / ((float) i)));
            this.mBoundWidth = i;
            drawable.setBounds(0, 0, this.mBoundWidth, this.mBoundHeight);
            this.translateLeft = 0.0f;
            this.translateTop = getDefaultTranslateTop(i2, this.mBoundHeight);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (intrinsicWidth != 0 && intrinsicHeight != 0) {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate(this.translateLeft, this.translateTop);
                float f = this.scale;
                canvas.scale(f, f);
                drawable.draw(canvas);
                canvas.restoreToCount(saveCount);
            }
        }
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private ScaleListener() {
        }

        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            float access$500 = ((float) ImagePreviewView.this.mBoundWidth) * ImagePreviewView.this.scale;
            float access$600 = ((float) ImagePreviewView.this.mBoundHeight) * ImagePreviewView.this.scale;
            if ((access$500 > ((float) ImagePreviewView.this.getWidth()) && ImagePreviewView.this.getDiffX() != 0.0f) || (access$600 > ((float) ImagePreviewView.this.getHeight()) && ImagePreviewView.this.getDiffY() != 0.0f)) {
                return false;
            }
            float access$200 = ImagePreviewView.this.scale + ((scaleGestureDetector.getScaleFactor() - 1.0f) * 2.0f);
            if (access$200 == ImagePreviewView.this.scale) {
                return true;
            }
            if (access$200 <= ImagePreviewView.mMinScale || access$200 > ImagePreviewView.mMaxScale) {
                return false;
            }
            float unused = ImagePreviewView.this.scale = access$200;
            float access$5002 = ((float) ImagePreviewView.this.mBoundWidth) * ImagePreviewView.this.scale;
            float access$6002 = ((float) ImagePreviewView.this.mBoundHeight) * ImagePreviewView.this.scale;
            ImagePreviewView imagePreviewView = ImagePreviewView.this;
            float unused2 = imagePreviewView.translateLeft = (((float) imagePreviewView.getWidth()) / 2.0f) - ((((((float) ImagePreviewView.this.getWidth()) / 2.0f) - ImagePreviewView.this.translateLeft) * access$5002) / access$500);
            ImagePreviewView imagePreviewView2 = ImagePreviewView.this;
            float unused3 = imagePreviewView2.translateTop = (((float) imagePreviewView2.getHeight()) / 2.0f) - ((((((float) ImagePreviewView.this.getHeight()) / 2.0f) - ImagePreviewView.this.translateTop) * access$6002) / access$600);
            float access$700 = ImagePreviewView.this.getDiffX();
            float access$800 = ImagePreviewView.this.getDiffY();
            if (access$700 > 0.0f && access$5002 > ((float) ImagePreviewView.this.getWidth())) {
                float unused4 = ImagePreviewView.this.translateLeft = 0.0f;
            }
            if (access$700 < 0.0f && access$5002 > ((float) ImagePreviewView.this.getWidth())) {
                ImagePreviewView imagePreviewView3 = ImagePreviewView.this;
                float unused5 = imagePreviewView3.translateLeft = ((float) imagePreviewView3.getWidth()) - access$5002;
            }
            if (access$800 > 0.0f && access$6002 > ((float) ImagePreviewView.this.getHeight())) {
                float unused6 = ImagePreviewView.this.translateTop = 0.0f;
            }
            if (access$800 < 0.0f && access$6002 > ((float) ImagePreviewView.this.getHeight())) {
                ImagePreviewView imagePreviewView4 = ImagePreviewView.this;
                float unused7 = imagePreviewView4.translateTop = ((float) imagePreviewView4.getHeight()) - access$6002;
            }
            ImagePreviewView.this.invalidate();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public float getExplicitTranslateLeft(float f) {
        float f2 = ((float) this.mBoundWidth) * this.scale;
        if (f > 0.0f) {
            f = 0.0f;
        }
        return (-f) + ((float) getWidth()) > f2 ? ((float) getWidth()) - f2 : f;
    }

    /* access modifiers changed from: private */
    public float getExplicitTranslateTop(float f) {
        float f2 = ((float) this.mBoundHeight) * this.scale;
        if (f > 0.0f) {
            f = 0.0f;
        }
        return (-f) + ((float) getHeight()) > f2 ? ((float) getHeight()) - f2 : f;
    }

    private class FlatGestureListener extends GestureDetector.SimpleOnGestureListener {
        private FlatGestureListener() {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            float access$500 = ((float) ImagePreviewView.this.mBoundWidth) * ImagePreviewView.this.scale;
            if (((float) ImagePreviewView.this.mBoundHeight) * ImagePreviewView.this.scale > ((float) ImagePreviewView.this.getHeight())) {
                ImagePreviewView imagePreviewView = ImagePreviewView.this;
                double access$400 = (double) imagePreviewView.translateTop;
                double d = (double) f2;
                Double.isNaN(d);
                Double.isNaN(access$400);
                float unused = imagePreviewView.translateTop = (float) (access$400 - (d * 1.5d));
                ImagePreviewView imagePreviewView2 = ImagePreviewView.this;
                float unused2 = imagePreviewView2.translateTop = imagePreviewView2.getExplicitTranslateTop(imagePreviewView2.translateTop);
            }
            boolean z = false;
            if (access$500 > ((float) ImagePreviewView.this.getWidth())) {
                ImagePreviewView imagePreviewView3 = ImagePreviewView.this;
                double access$300 = (double) imagePreviewView3.translateLeft;
                double d2 = (double) f;
                Double.isNaN(d2);
                Double.isNaN(access$300);
                float unused3 = imagePreviewView3.translateLeft = (float) (access$300 - (d2 * 1.5d));
                ImagePreviewView imagePreviewView4 = ImagePreviewView.this;
                float access$1000 = imagePreviewView4.getExplicitTranslateLeft(imagePreviewView4.translateLeft);
                if (access$1000 != ImagePreviewView.this.translateLeft) {
                    z = true;
                }
                float unused4 = ImagePreviewView.this.translateLeft = access$1000;
            } else {
                z = true;
            }
            if (ImagePreviewView.this.onReachBorderListener != null) {
                ImagePreviewView.this.onReachBorderListener.onReachBorder(z);
            }
            ImagePreviewView.this.invalidate();
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            return ImagePreviewView.this.performClick();
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            if (((float) ImagePreviewView.this.mBoundWidth) * ImagePreviewView.this.scale > ((float) ImagePreviewView.this.getWidth())) {
                float access$1000 = ImagePreviewView.this.getExplicitTranslateLeft(ImagePreviewView.this.translateLeft + (f * 0.5f * 0.5f * 0.5f));
                ValueAnimator access$1200 = ImagePreviewView.this.getResetXAnimator();
                access$1200.setDuration(300L);
                access$1200.setInterpolator(ImagePreviewView.this.mDecInterpolator);
                access$1200.setFloatValues(ImagePreviewView.this.translateLeft, access$1000);
                access$1200.addUpdateListener(ImagePreviewView.this.getOnTranslateXAnimationUpdate());
                access$1200.start();
            }
            if (((float) ImagePreviewView.this.mBoundHeight) * ImagePreviewView.this.scale > ((float) ImagePreviewView.this.getHeight())) {
                float access$900 = ImagePreviewView.this.getExplicitTranslateTop(ImagePreviewView.this.translateTop + (f2 * 0.5f * 0.5f * 0.5f));
                ValueAnimator access$1400 = ImagePreviewView.this.getResetYAnimator();
                access$1400.setDuration(300L);
                access$1400.setInterpolator(ImagePreviewView.this.mDecInterpolator);
                access$1400.setFloatValues(ImagePreviewView.this.translateTop, access$900);
                access$1400.addUpdateListener(ImagePreviewView.this.getOnTranslateYAnimationUpdate());
                access$1400.start();
            }
            return true;
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            boolean unused = ImagePreviewView.this.isAutoScale = true;
            ValueAnimator access$1600 = ImagePreviewView.this.getResetScaleAnimator();
            if (ImagePreviewView.this.scale == 1.0f) {
                access$1600.setFloatValues(1.0f, 2.0f);
                ValueAnimator access$1200 = ImagePreviewView.this.getResetXAnimator();
                ValueAnimator access$1400 = ImagePreviewView.this.getResetYAnimator();
                access$1200.setFloatValues(ImagePreviewView.this.translateLeft, (((float) ImagePreviewView.this.getWidth()) - (((float) ImagePreviewView.this.mBoundWidth) * 2.0f)) / 2.0f);
                ImagePreviewView imagePreviewView = ImagePreviewView.this;
                access$1400.setFloatValues(ImagePreviewView.this.translateTop, imagePreviewView.getDefaultTranslateTop(imagePreviewView.getHeight(), ImagePreviewView.this.mBoundHeight * 2));
                access$1200.addUpdateListener(ImagePreviewView.this.getOnTranslateXAnimationUpdate());
                access$1400.addUpdateListener(ImagePreviewView.this.getOnTranslateYAnimationUpdate());
                access$1200.start();
                access$1400.start();
            } else {
                access$1600.setFloatValues(ImagePreviewView.this.scale, 1.0f);
                ImagePreviewView.this.resetDefaultState();
            }
            access$1600.addUpdateListener(ImagePreviewView.this.getOnScaleAnimationUpdate());
            access$1600.start();
            return true;
        }
    }
}
