package com.library.gallery;

interface Contract {

    public interface Presenter {
        void requestCamera();

        void requestExternalStorage();

        void setDataView(View view);
    }

    public interface View {
        void onCameraPermissionDenied();

        void onOpenCameraSuccess();
    }
}
