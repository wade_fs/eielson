package com.library.gallery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.library.gallery.R;
import com.library.gallery.bean.Folder;

public class FolderAdapter extends BaseRecyclerAdapter<Folder> {
    public FolderAdapter(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: package-private */
    public RecyclerView.ViewHolder onCreateDefaultViewHolder(ViewGroup viewGroup, int i) {
        return new FolderViewHolder(this.mInflater.inflate(R.layout.commom_item_list_folder, viewGroup, false));
    }

    /* access modifiers changed from: package-private */
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Folder folder, int i) {
        FolderViewHolder folderViewHolder = (FolderViewHolder) viewHolder;
        folderViewHolder.tv_name.setText(folder.getName());
        folderViewHolder.tv_size.setText(String.format("(%s)", Integer.valueOf(folder.getImages().size())));
        this.mLoader.load(folder.getAlbumPath()).into(folderViewHolder.iv_image);
    }

    private static class FolderViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_image;
        TextView tv_name;
        TextView tv_size;

        FolderViewHolder(View view) {
            super(view);
            this.iv_image = (ImageView) view.findViewById(R.id.iv_folder);
            this.tv_name = (TextView) view.findViewById(R.id.tv_folder_name);
            this.tv_size = (TextView) view.findViewById(R.id.tv_size);
        }
    }
}
