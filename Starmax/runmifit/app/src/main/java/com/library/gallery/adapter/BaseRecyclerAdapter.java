package com.library.gallery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter<T> extends RecyclerView.Adapter {
    LayoutInflater mInflater;
    private List<T> mItems = new ArrayList();
    RequestManager mLoader;
    private OnClickListener onClickListener;
    /* access modifiers changed from: private */
    public OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int i, long j);
    }

    /* access modifiers changed from: package-private */
    public abstract void onBindViewHolder(RecyclerView.ViewHolder viewHolder, T t, int i);

    /* access modifiers changed from: package-private */
    public abstract RecyclerView.ViewHolder onCreateDefaultViewHolder(ViewGroup viewGroup, int i);

    BaseRecyclerAdapter(Context context) {
        this.mLoader = Glide.with(context);
        this.mInflater = LayoutInflater.from(context);
        this.onClickListener = new OnClickListener() {
            /* class com.library.gallery.adapter.BaseRecyclerAdapter.C21771 */

            public void onClick(int i, long j) {
                if (BaseRecyclerAdapter.this.onItemClickListener != null) {
                    BaseRecyclerAdapter.this.onItemClickListener.onItemClick(i, j);
                }
            }
        };
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder onCreateDefaultViewHolder = onCreateDefaultViewHolder(viewGroup, i);
        if (onCreateDefaultViewHolder != null) {
            onCreateDefaultViewHolder.itemView.setTag(onCreateDefaultViewHolder);
            onCreateDefaultViewHolder.itemView.setOnClickListener(this.onClickListener);
        }
        return onCreateDefaultViewHolder;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        onBindViewHolder(viewHolder, this.mItems.get(i), i);
    }

    public int getItemCount() {
        return this.mItems.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener2) {
        this.onItemClickListener = onItemClickListener2;
    }

    public void addAll(List<T> list) {
        if (list != null && list.size() > 0) {
            this.mItems.addAll(list);
            notifyItemRangeInserted(this.mItems.size(), list.size());
        }
    }

    public final void addItem(T t) {
        if (t != null) {
            this.mItems.add(t);
            notifyItemChanged(this.mItems.size());
        }
    }

    public void updateItem(int i) {
        if (getItemCount() > i) {
            notifyItemChanged(i);
        }
    }

    public final T getItem(int i) {
        if (i < 0 || i >= this.mItems.size()) {
            return null;
        }
        return this.mItems.get(i);
    }

    public final void resetItem(List<T> list) {
        if (list != null) {
            clear();
            addAll(list);
        }
    }

    public final void clear() {
        this.mItems.clear();
        notifyDataSetChanged();
    }

    static abstract class OnClickListener implements View.OnClickListener {
        public abstract void onClick(int i, long j);

        OnClickListener() {
        }

        public void onClick(View view) {
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            onClick(viewHolder.getAdapterPosition(), viewHolder.getItemId());
        }
    }
}
