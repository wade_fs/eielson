package com.library.gallery.util;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.WindowManager;
import com.library.gallery.bean.Image;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Util {
    public static boolean hasSDCard() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static String getCameraPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/Camera/";
    }

    public static String getSaveImageFullName() {
        return "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
    }

    public static String[] toArray(List<Image> list) {
        int size;
        if (list == null || (size = list.size()) == 0) {
            return null;
        }
        String[] strArr = new String[size];
        int i = 0;
        for (Image image : list) {
            strArr[i] = image.getPath();
            i++;
        }
        return strArr;
    }

    public static int getScreenWidth(Context context) {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();
    }

    public static int getScreenHeight(Context context) {
        return ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getHeight();
    }

    public static String getExtension(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        String str2 = options.outMimeType;
        return str2.substring(str2.lastIndexOf("/") + 1);
    }
}
