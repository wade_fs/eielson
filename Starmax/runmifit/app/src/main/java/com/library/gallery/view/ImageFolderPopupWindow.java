package com.library.gallery.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import com.library.gallery.R;
import com.library.gallery.adapter.BaseRecyclerAdapter;
import com.library.gallery.adapter.FolderAdapter;
import com.library.gallery.bean.Folder;

public class ImageFolderPopupWindow extends PopupWindow implements View.OnAttachStateChangeListener, BaseRecyclerAdapter.OnItemClickListener {
    private FolderAdapter mAdapter;
    private Callback mCallback;
    private RecyclerView mFolderView;

    public interface Callback {
        void onDismiss();

        void onSelect(ImageFolderPopupWindow imageFolderPopupWindow, Folder folder);

        void onShow();
    }

    public ImageFolderPopupWindow(Context context, Callback callback) {
        super(LayoutInflater.from(context).inflate(R.layout.commom_popup_window_folder, (ViewGroup) null), -1, -1);
        this.mCallback = callback;
        setAnimationStyle(R.style.popup_anim_style_alpha);
        setBackgroundDrawable(new ColorDrawable(0));
        setOutsideTouchable(true);
        setFocusable(true);
        View contentView = getContentView();
        contentView.setOnClickListener(new View.OnClickListener() {
            /* class com.library.gallery.view.ImageFolderPopupWindow.C21781 */

            public void onClick(View view) {
                ImageFolderPopupWindow.this.dismiss();
            }
        });
        contentView.addOnAttachStateChangeListener(this);
        this.mFolderView = (RecyclerView) contentView.findViewById(R.id.rv_popup_folder);
        this.mFolderView.setLayoutManager(new LinearLayoutManager(context));
    }

    public void setAdapter(FolderAdapter folderAdapter) {
        this.mAdapter = folderAdapter;
        this.mFolderView.setAdapter(folderAdapter);
        this.mAdapter.setOnItemClickListener(this);
    }

    public void onViewAttachedToWindow(View view) {
        Callback callback = this.mCallback;
        if (callback != null) {
            callback.onShow();
        }
    }

    public void onViewDetachedFromWindow(View view) {
        Callback callback = this.mCallback;
        if (callback != null) {
            callback.onDismiss();
        }
    }

    public void onItemClick(int i, long j) {
        Callback callback = this.mCallback;
        if (callback != null) {
            callback.onSelect(this, (Folder) this.mAdapter.getItem(i));
        }
    }
}
