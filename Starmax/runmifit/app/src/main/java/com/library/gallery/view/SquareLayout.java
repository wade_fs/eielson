package com.library.gallery.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.library.gallery.R;

public class SquareLayout extends FrameLayout {
    private int mBaseDirection;

    public SquareLayout(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public SquareLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet, 0, 0);
    }

    public SquareLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context, attributeSet, i, 0);
    }

    public SquareLayout(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        init(context, attributeSet, i, i2);
    }

    private void init(Context context, AttributeSet attributeSet, int i, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SquareLayout, i, i2);
        this.mBaseDirection = obtainStyledAttributes.getInt(R.styleable.SquareLayout_oscAccordTo, 3);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = this.mBaseDirection;
        if (i3 == 1) {
            super.onMeasure(i, i);
        } else if (i3 == 2) {
            super.onMeasure(i2, i2);
        } else {
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            if (size2 == 0) {
                super.onMeasure(i, i);
            } else if (size == 0) {
                super.onMeasure(i2, i2);
            } else if (size > size2) {
                super.onMeasure(i2, i2);
            } else {
                super.onMeasure(i, i);
            }
        }
    }
}
