package com.library.gallery;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.library.gallery.Contract;
import com.library.gallery.SelectOptions;
import com.library.gallery.adapter.BaseRecyclerAdapter;
import com.library.gallery.adapter.FolderAdapter;
import com.library.gallery.adapter.ImageAdapter;
import com.library.gallery.adapter.SpaceGridItemDecoration;
import com.library.gallery.bean.Folder;
import com.library.gallery.bean.Image;
import com.library.gallery.util.Util;
import com.library.gallery.view.ImageFolderPopupWindow;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImagePickerFragment extends Fragment implements Contract.View, View.OnClickListener, BaseRecyclerAdapter.OnItemClickListener {
    /* access modifiers changed from: private */
    public static SelectOptions mOption;
    ImageButton mBtnBack;
    /* access modifiers changed from: private */
    public String mCamImageName;
    RecyclerView mContentView;
    private LoaderListener mCursorLoader = new LoaderListener();
    private ImageFolderPopupWindow mFolderPopupWindow;
    private ImageAdapter mImageAdapter;
    ImageView mImageArrow;
    /* access modifiers changed from: private */
    public FolderAdapter mImageFolderAdapter;
    private Contract.Presenter mPresenter;
    private View mRootView;
    /* access modifiers changed from: private */
    public List<Image> mSelectedImage;
    TextView mTextDone;
    TextView mTextFolder;
    TextView mTextPreviewView;
    View mToolbar;

    public void onCameraPermissionDenied() {
    }

    public static ImagePickerFragment newInstance(SelectOptions selectOptions) {
        mOption = selectOptions;
        return new ImagePickerFragment();
    }

    public void onAttach(Context context) {
        this.mPresenter = (Contract.Presenter) context;
        this.mPresenter.setDataView(this);
        super.onAttach(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = this.mRootView;
        if (view == null) {
            this.mRootView = layoutInflater.inflate(R.layout.commom_fragment_select_image, viewGroup, false);
            initView();
            initData();
        } else {
            ViewGroup viewGroup2 = (ViewGroup) view.getParent();
            if (viewGroup2 != null) {
                viewGroup2.removeView(this.mRootView);
            }
        }
        return this.mRootView;
    }

    public void setStatusBarColor(int i) {
        Window window = getActivity().getWindow();
        window.clearFlags(67108864);
        window.addFlags(Integer.MIN_VALUE);
        if (Build.VERSION.SDK_INT >= 21) {
            window.setStatusBarColor(getResources().getColor(i));
        }
    }

    private void initView() {
        setStatusBarColor(R.color.colorHome);
        this.mContentView = (RecyclerView) this.mRootView.findViewById(R.id.rv_image);
        this.mTextFolder = (TextView) this.mRootView.findViewById(R.id.tv_folder_name);
        this.mImageArrow = (ImageView) this.mRootView.findViewById(R.id.iv_arrow);
        this.mBtnBack = (ImageButton) this.mRootView.findViewById(R.id.ib_back);
        this.mTextDone = (TextView) this.mRootView.findViewById(R.id.btn_done);
        this.mTextPreviewView = (TextView) this.mRootView.findViewById(R.id.btn_preview);
        this.mToolbar = this.mRootView.findViewById(R.id.toolbar);
        this.mRootView.findViewById(R.id.fl_folder).setOnClickListener(this);
        this.mBtnBack.setOnClickListener(this);
        this.mTextDone.setOnClickListener(this);
        this.mTextPreviewView.setOnClickListener(this);
        this.mTextFolder.setOnClickListener(this);
    }

    private void initData() {
        this.mSelectedImage = new ArrayList();
        this.mContentView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        this.mContentView.addItemDecoration(new SpaceGridItemDecoration(3));
        this.mImageAdapter = new ImageAdapter(getContext());
        this.mImageAdapter.setSingleSelect(mOption.getSelectCount() <= 1);
        this.mRootView.findViewById(R.id.lay_button).setVisibility(mOption.getSelectCount() == 1 ? 8 : 0);
        this.mImageFolderAdapter = new FolderAdapter(getActivity());
        this.mContentView.setAdapter(this.mImageAdapter);
        this.mContentView.setItemAnimator(null);
        this.mImageAdapter.setOnItemClickListener(this);
        if (mOption.getSelectCount() > 1 && mOption.getSelectedImages() != null) {
            for (String str : mOption.getSelectedImages()) {
                if (str != null && new File(str).exists()) {
                    Image image = new Image();
                    image.setSelect(true);
                    image.setPath(str);
                    this.mSelectedImage.add(image);
                }
            }
        }
        getLoaderManager().initLoader(0, null, this.mCursorLoader);
    }

    public void onClick(View view) {
        if (view == this.mBtnBack) {
            getActivity().finish();
        } else if (view == this.mTextDone) {
            handleResult();
        } else if (view == this.mTextPreviewView) {
            if (this.mSelectedImage.size() > 0) {
                ImageGalleryActivity.show(getActivity(), new SelectOptions.Builder().setSelectedImages(Util.toArray(this.mSelectedImage)).build(), 0);
            }
        } else if (view == this.mRootView) {
            showPopupFolderList();
        } else if (view == this.mTextFolder) {
            showPopupFolderList();
        }
    }

    public void onItemClick(int i, long j) {
        if (!mOption.isHasCam()) {
            handleSelectChange(i);
        } else if (i != 0) {
            handleSelectChange(i);
        } else if (this.mSelectedImage.size() < mOption.getSelectCount()) {
            this.mPresenter.requestCamera();
        } else {
            FragmentActivity activity = getActivity();
            Toast.makeText(activity, "最多只能选择 " + mOption.getSelectCount() + " 张图片", 0).show();
        }
    }

    public void onOpenCameraSuccess() {
        toOpenCamera();
    }

    /* access modifiers changed from: private */
    public void handleSelectSizeChange(int i) {
        if (i > 0) {
            this.mTextPreviewView.setEnabled(true);
            this.mTextDone.setEnabled(true);
            this.mTextDone.setText(String.format("%s(%s)", "完成", Integer.valueOf(i)));
            return;
        }
        this.mTextPreviewView.setEnabled(false);
        this.mTextDone.setEnabled(false);
        this.mTextDone.setText("完成");
    }

    private void handleSelectChange(int i) {
        Image image = (Image) this.mImageAdapter.getItem(i);
        if (image != null) {
            int selectCount = mOption.getSelectCount();
            if (selectCount > 1) {
                if (image.isSelect()) {
                    image.setSelect(false);
                    this.mSelectedImage.remove(image);
                    this.mImageAdapter.updateItem(i);
                } else if (this.mSelectedImage.size() == selectCount) {
                    FragmentActivity activity = getActivity();
                    Toast.makeText(activity, "最多只能选择 " + selectCount + " 张照片", 0).show();
                } else {
                    image.setSelect(true);
                    this.mSelectedImage.add(image);
                    this.mImageAdapter.updateItem(i);
                }
                handleSelectSizeChange(this.mSelectedImage.size());
                return;
            }
            this.mSelectedImage.add(image);
            handleResult();
        }
    }

    /* access modifiers changed from: private */
    public void handleResult() {
        if (this.mSelectedImage.size() == 0) {
            return;
        }
        if (mOption.isCrop()) {
            List<String> selectedImages = mOption.getSelectedImages();
            selectedImages.clear();
            selectedImages.add(this.mSelectedImage.get(0).getPath());
            this.mSelectedImage.clear();
            CropActivity.show(super, mOption);
            return;
        }
        mOption.getCallback().doSelected(Util.toArray(this.mSelectedImage));
        getActivity().finish();
    }

    private void showPopupFolderList() {
        if (this.mFolderPopupWindow == null) {
            ImageFolderPopupWindow imageFolderPopupWindow = new ImageFolderPopupWindow(getActivity(), new ImageFolderPopupWindow.Callback() {
                /* class com.library.gallery.ImagePickerFragment.C21711 */

                public void onSelect(ImageFolderPopupWindow imageFolderPopupWindow, Folder folder) {
                    ImagePickerFragment.this.addImagesToAdapter(folder.getImages());
                    ImagePickerFragment.this.mContentView.scrollToPosition(0);
                    imageFolderPopupWindow.dismiss();
                    ImagePickerFragment.this.mTextFolder.setText(folder.getName());
                }

                public void onDismiss() {
                    ImagePickerFragment.this.mImageArrow.setImageResource(R.mipmap.ic_arrow_bottom);
                }

                public void onShow() {
                    ImagePickerFragment.this.mImageArrow.setImageResource(R.mipmap.ic_arrow_top);
                }
            });
            imageFolderPopupWindow.setAdapter(this.mImageFolderAdapter);
            this.mFolderPopupWindow = imageFolderPopupWindow;
        }
        this.mFolderPopupWindow.showAsDropDown(this.mToolbar);
    }

    private void toOpenCamera() {
        String str;
        Uri uri;
        this.mCamImageName = null;
        if (Util.hasSDCard()) {
            str = Util.getCameraPath();
            File file = new File(str);
            if (!file.exists()) {
                file.mkdirs();
            }
        } else {
            str = "";
        }
        if (TextUtils.isEmpty(str)) {
            Toast.makeText(getActivity(), "无法保存照片，请检查SD卡是否挂载", 1).show();
            return;
        }
        this.mCamImageName = Util.getSaveImageFullName();
        File file2 = new File(str, this.mCamImageName);
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (Build.VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile(getContext(), "com.runmifit.android.provider", file2);
        } else {
            uri = Uri.fromFile(file2);
        }
        intent.putExtra("output", uri);
        startActivityForResult(intent, 3);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 != -1) {
            return;
        }
        if (i != 3) {
            if (i == 4 && intent != null) {
                mOption.getCallback().doSelected(new String[]{intent.getStringExtra("crop_path")});
                getActivity().finish();
            }
        } else if (this.mCamImageName != null) {
            getActivity().sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(new File(Util.getCameraPath() + this.mCamImageName))));
        }
    }

    private class LoaderListener implements LoaderManager.LoaderCallbacks<Cursor> {
        private final String[] IMAGE_PROJECTION;

        public void onLoaderReset(Loader<Cursor> loader) {
        }

        private LoaderListener() {
            this.IMAGE_PROJECTION = new String[]{"_data", "_display_name", "date_added", "_id", "mini_thumb_magic", "bucket_display_name"};
        }

        public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
            onLoadFinished((Loader<Cursor>) loader, (Cursor) obj);
        }

        public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
            if (i != 0) {
                return null;
            }
            Context context = ImagePickerFragment.this.getContext();
            Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            String[] strArr = this.IMAGE_PROJECTION;
            return new CursorLoader(context, uri, strArr, null, null, this.IMAGE_PROJECTION[2] + " DESC");
        }

        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            if (cursor != null) {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                Folder folder = new Folder();
                folder.setName(ImagePickerFragment.this.getResources().getString(R.string.all_pic));
                folder.setPath("");
                arrayList2.add(folder);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    do {
                        String string = cursor.getString(cursor.getColumnIndexOrThrow(this.IMAGE_PROJECTION[0]));
                        String string2 = cursor.getString(cursor.getColumnIndexOrThrow(this.IMAGE_PROJECTION[1]));
                        int i = cursor.getInt(cursor.getColumnIndexOrThrow(this.IMAGE_PROJECTION[3]));
                        Image image = new Image();
                        image.setPath(string);
                        image.setName(string2);
                        image.setId(i);
                        arrayList.add(image);
                        if (ImagePickerFragment.this.mCamImageName != null && ImagePickerFragment.this.mCamImageName.equals(image.getName())) {
                            image.setSelect(true);
                            ImagePickerFragment.this.mSelectedImage.add(image);
                        }
                        if (ImagePickerFragment.this.mSelectedImage.size() > 0) {
                            for (Image image2 : ImagePickerFragment.this.mSelectedImage) {
                                if (image2.getPath().equals(image.getPath())) {
                                    image.setSelect(true);
                                }
                            }
                        }
                        File parentFile = new File(string).getParentFile();
                        Folder folder2 = new Folder();
                        folder2.setName(parentFile.getName());
                        folder2.setPath(parentFile.getAbsolutePath());
                        if (!arrayList2.contains(folder2)) {
                            folder2.getImages().add(image);
                            folder2.setAlbumPath(image.getPath());
                            arrayList2.add(folder2);
                        } else {
                            ((Folder) arrayList2.get(arrayList2.indexOf(folder2))).getImages().add(image);
                        }
                    } while (cursor.moveToNext());
                }
                ImagePickerFragment.this.addImagesToAdapter(arrayList);
                folder.getImages().addAll(arrayList);
                String str = null;
                if (ImagePickerFragment.mOption.isHasCam()) {
                    if (arrayList.size() > 1) {
                        str = ((Image) arrayList.get(1)).getPath();
                    }
                    folder.setAlbumPath(str);
                } else {
                    if (arrayList.size() > 0) {
                        str = ((Image) arrayList.get(0)).getPath();
                    }
                    folder.setAlbumPath(str);
                }
                ImagePickerFragment.this.mImageFolderAdapter.resetItem(arrayList2);
                if (ImagePickerFragment.this.mSelectedImage.size() > 0) {
                    ArrayList arrayList3 = new ArrayList();
                    for (Image image3 : ImagePickerFragment.this.mSelectedImage) {
                        if (!new File(image3.getPath()).exists()) {
                            arrayList3.add(image3);
                        }
                    }
                    ImagePickerFragment.this.mSelectedImage.removeAll(arrayList3);
                }
                if (ImagePickerFragment.mOption.getSelectCount() == 1 && ImagePickerFragment.this.mCamImageName != null) {
                    ImagePickerFragment.this.handleResult();
                }
                ImagePickerFragment imagePickerFragment = ImagePickerFragment.this;
                imagePickerFragment.handleSelectSizeChange(imagePickerFragment.mSelectedImage.size());
            }
        }
    }

    /* access modifiers changed from: private */
    public void addImagesToAdapter(ArrayList<Image> arrayList) {
        this.mImageAdapter.clear();
        if (mOption.isHasCam()) {
            this.mImageAdapter.addItem(new Image());
        }
        this.mImageAdapter.addAll(arrayList);
    }

    public void onDestroy() {
        mOption = null;
        super.onDestroy();
    }
}
