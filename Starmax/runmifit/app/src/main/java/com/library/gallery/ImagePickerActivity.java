package com.library.gallery;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import com.library.gallery.Contract;
import java.util.List;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class ImagePickerActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, Contract.Presenter {
    private static final int RC_CAMERA_PERM = 3;
    private static final int RC_EXTERNAL_STORAGE = 4;
    private static SelectOptions mOption;
    private Contract.View mView;

    public void onPermissionsGranted(int i, List<String> list) {
    }

    public static void show(Context context, SelectOptions selectOptions) {
        mOption = selectOptions;
        context.startActivity(new Intent(context, ImagePickerActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.commom_activity_select_image);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null && supportActionBar.isShowing()) {
            supportActionBar.hide();
        }
        requestExternalStorage();
    }

    @AfterPermissionGranted(3)
    public void requestCamera() {
        if (EasyPermissions.hasPermissions(this, "android.permission.CAMERA")) {
            Contract.View view = this.mView;
            if (view != null) {
                view.onOpenCameraSuccess();
                return;
            }
            return;
        }
        EasyPermissions.requestPermissions(this, "", 3, "android.permission.CAMERA");
    }

    @AfterPermissionGranted(4)
    public void requestExternalStorage() {
        if (!EasyPermissions.hasPermissions(this, "android.permission.READ_EXTERNAL_STORAGE")) {
            EasyPermissions.requestPermissions(this, "", 4, "android.permission.READ_EXTERNAL_STORAGE");
        } else if (this.mView == null) {
            handleView();
        }
    }

    public void setDataView(Contract.View view) {
        this.mView = view;
    }

    public void onPermissionsDenied(int i, List<String> list) {
        if (i == 4) {
            removeView();
            showDialog("", "没有权限, 你需要去设置中开启读取手机存储权限.", "去设置", "取消", new DialogInterface.OnClickListener() {
                /* class com.library.gallery.$$Lambda$ImagePickerActivity$TQyovlBfaZFg1CdgIXzcDQAkKMY */

                public final void onClick(DialogInterface dialogInterface, int i) {
                    ImagePickerActivity.this.lambda$onPermissionsDenied$0$ImagePickerActivity(dialogInterface, i);
                }
            }, new DialogInterface.OnClickListener() {
                /* class com.library.gallery.$$Lambda$ImagePickerActivity$KHVbop3YqwbkZLAU4Kx1u3UBiM */

                public final void onClick(DialogInterface dialogInterface, int i) {
                    ImagePickerActivity.this.lambda$onPermissionsDenied$1$ImagePickerActivity(dialogInterface, i);
                }
            });
            return;
        }
        Contract.View view = this.mView;
        if (view != null) {
            view.onCameraPermissionDenied();
        }
        showDialog("", "没有权限, 你需要去设置中开启相机权限.", "去设置", "取消", new DialogInterface.OnClickListener() {
            /* class com.library.gallery.$$Lambda$ImagePickerActivity$wiJ6w5JIHDKJSgQqXmKhaea7h0 */

            public final void onClick(DialogInterface dialogInterface, int i) {
                ImagePickerActivity.this.lambda$onPermissionsDenied$2$ImagePickerActivity(dialogInterface, i);
            }
        }, null);
    }

    public /* synthetic */ void lambda$onPermissionsDenied$0$ImagePickerActivity(DialogInterface dialogInterface, int i) {
        startActivity(new Intent("android.settings.APPLICATION_SETTINGS"));
        finish();
    }

    public /* synthetic */ void lambda$onPermissionsDenied$1$ImagePickerActivity(DialogInterface dialogInterface, int i) {
        finish();
    }

    public /* synthetic */ void lambda$onPermissionsDenied$2$ImagePickerActivity(DialogInterface dialogInterface, int i) {
        startActivity(new Intent("android.settings.APPLICATION_SETTINGS"));
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        EasyPermissions.onRequestPermissionsResult(i, strArr, iArr, this);
    }

    private void removeView() {
        Contract.View view = this.mView;
        if (view != null) {
            try {
                getSupportFragmentManager().beginTransaction().remove((Fragment) view).commitNowAllowingStateLoss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handleView() {
        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_content, ImagePickerFragment.newInstance(mOption)).commitNowAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialog(String str, String str2, String str3, String str4, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        new AlertDialog.Builder(this).setTitle(str).setMessage(str2).setPositiveButton(str3, onClickListener).setNegativeButton(str4, onClickListener2).setCancelable(false).create().show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        mOption = null;
        super.onDestroy();
    }
}
