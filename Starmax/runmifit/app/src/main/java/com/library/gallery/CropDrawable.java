package com.library.gallery;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.library.gallery.util.Util;

public class CropDrawable extends Drawable {
    private int mBottom;
    private Context mContext;
    private Paint mCornerPaint = new Paint();
    private int mCropHeight = 800;
    private int mCropWidth = 800;
    private int mLeft;
    private Paint mLinePaint = new Paint();
    private Paint mNineLinePaint = new Paint();
    private int mRight;
    private int mTop;

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public CropDrawable(Context context) {
        this.mContext = context;
        initPaint();
    }

    private void initPaint() {
        this.mLinePaint.setColor(-1);
        this.mLinePaint.setAntiAlias(true);
        this.mLinePaint.setStrokeWidth(2.0f);
        this.mLinePaint.setStyle(Paint.Style.STROKE);
        this.mNineLinePaint.setColor(-1);
        this.mNineLinePaint.setAntiAlias(true);
        this.mNineLinePaint.setStrokeWidth(1.0f);
        this.mNineLinePaint.setStyle(Paint.Style.STROKE);
        this.mCornerPaint.setColor(-1);
        this.mCornerPaint.setAntiAlias(true);
        this.mCornerPaint.setStrokeWidth(8.0f);
        this.mCornerPaint.setStyle(Paint.Style.FILL);
    }

    public void draw(Canvas canvas) {
        int screenWidth = Util.getScreenWidth(this.mContext);
        int screenHeight = Util.getScreenHeight(this.mContext);
        int i = this.mCropWidth;
        this.mLeft = (screenWidth - i) / 2;
        int i2 = this.mCropHeight;
        this.mTop = (screenHeight - i2) / 2;
        this.mRight = (screenWidth + i) / 2;
        this.mBottom = (screenHeight + i2) / 2;
        Rect rect = new Rect(this.mLeft, this.mTop, this.mRight, this.mBottom);
        canvas.drawRect(rect, this.mLinePaint);
        int i3 = this.mLeft;
        int i4 = this.mTop;
        canvas.drawLine((float) i3, (float) i4, (float) i3, (float) (i4 + 50), this.mCornerPaint);
        int i5 = this.mLeft;
        int i6 = this.mTop;
        Canvas canvas2 = canvas;
        canvas2.drawLine((float) (i5 - 4), (float) i6, (float) (i5 + 50), (float) i6, this.mCornerPaint);
        int i7 = this.mRight;
        int i8 = this.mTop;
        canvas.drawLine((float) i7, (float) i8, (float) i7, (float) (i8 + 50), this.mCornerPaint);
        int i9 = this.mRight;
        int i10 = this.mTop;
        Canvas canvas3 = canvas;
        canvas3.drawLine((float) (i9 - 50), (float) i10, (float) (i9 + 4), (float) i10, this.mCornerPaint);
        int i11 = this.mLeft;
        int i12 = this.mBottom;
        canvas.drawLine((float) i11, (float) i12, (float) (i11 + 50), (float) i12, this.mCornerPaint);
        int i13 = this.mLeft;
        int i14 = this.mBottom;
        canvas.drawLine((float) i13, (float) (i14 - 50), (float) i13, (float) (i14 + 4), this.mCornerPaint);
        int i15 = this.mRight;
        int i16 = this.mBottom;
        canvas.drawLine((float) i15, (float) i16, (float) i15, (float) (i16 - 50), this.mCornerPaint);
        int i17 = this.mRight;
        int i18 = this.mBottom;
        Canvas canvas4 = canvas;
        canvas4.drawLine((float) (i17 - 50), (float) i18, (float) (i17 + 4), (float) i18, this.mCornerPaint);
        int save = canvas.save();
        canvas.clipRect(rect);
        int i19 = this.mCropWidth / 3;
        int i20 = this.mCropHeight / 3;
        int i21 = this.mLeft;
        Canvas canvas5 = canvas;
        canvas5.drawLine((float) (i21 + i19), (float) this.mTop, (float) (i21 + i19), (float) this.mBottom, this.mNineLinePaint);
        int i22 = this.mLeft;
        int i23 = i19 * 2;
        canvas5.drawLine((float) (i22 + i23), (float) this.mTop, (float) (i22 + i23), (float) this.mBottom, this.mNineLinePaint);
        float f = (float) this.mLeft;
        int i24 = this.mTop;
        canvas.drawLine(f, (float) (i24 + i20), (float) this.mRight, (float) (i24 + i20), this.mNineLinePaint);
        float f2 = (float) this.mLeft;
        int i25 = this.mTop;
        int i26 = i20 * 2;
        canvas.drawLine(f2, (float) (i25 + i26), (float) this.mRight, (float) (i25 + i26), this.mNineLinePaint);
        canvas.restoreToCount(save);
    }

    public void setBounds(Rect rect) {
        super.setBounds(new Rect(this.mLeft, this.mTop, this.mRight, this.mBottom));
    }

    public void setRegion(Rect rect) {
        int screenWidth = Util.getScreenWidth(this.mContext);
        int screenHeight = Util.getScreenHeight(this.mContext);
        int i = this.mCropWidth;
        int i2 = this.mCropHeight;
        rect.set((screenWidth - i) / 2, (screenHeight - i2) / 2, (screenWidth + i) / 2, (screenHeight + i2) / 2);
    }

    public int getTop() {
        return this.mCropHeight;
    }

    public int getBottom() {
        return this.mBottom;
    }

    public void setCropWidth(int i) {
        this.mCropWidth = i;
    }

    public void setCropHeight(int i) {
        this.mCropHeight = i;
    }
}
