package pub.devrel.easypermissions;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class EasyPermissions {
    private static final String DIALOG_TAG = "RationaleDialogFragmentCompat";
    private static final String TAG = "EasyPermissions";

    public interface PermissionCallbacks extends ActivityCompat.OnRequestPermissionsResultCallback {
        void onPermissionsDenied(int i, List<String> list);

        void onPermissionsGranted(int i, List<String> list);
    }

    public static boolean hasPermissions(Context context, String... strArr) {
        if (Build.VERSION.SDK_INT < 23) {
            Log.w(TAG, "hasPermissions: API version < M, returning true by default");
            return true;
        }
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            if (!(ContextCompat.checkSelfPermission(context, strArr[i]) == 0)) {
                return false;
            }
        }
        return true;
    }

    public static void requestPermissions(Activity activity, String str, int i, String... strArr) {
        requestPermissions(activity, str, 17039370, 17039360, i, strArr);
    }

    public static void requestPermissions(Activity activity, String str, int i, int i2, int i3, String... strArr) {
        if (hasPermissions(activity, strArr)) {
            notifyAlreadyHasPermissions(activity, i3, strArr);
        } else if (shouldShowRationale(activity, strArr)) {
            showRationaleDialogFragment(activity.getFragmentManager(), str, i, i2, i3, strArr);
        } else {
            ActivityCompat.requestPermissions(activity, strArr, i3);
        }
    }

    public static void requestPermissions(Fragment fragment, String str, int i, String... strArr) {
        requestPermissions(fragment, str, 17039370, 17039360, i, strArr);
    }

    public static void requestPermissions(Fragment fragment, String str, int i, int i2, int i3, String... strArr) {
        if (hasPermissions(fragment.getContext(), strArr)) {
            notifyAlreadyHasPermissions(fragment, i3, strArr);
        } else if (shouldShowRationale(fragment, strArr)) {
            RationaleDialogFragmentCompat.newInstance(i, i2, str, i3, strArr).show(fragment.getChildFragmentManager(), DIALOG_TAG);
        } else {
            fragment.requestPermissions(strArr, i3);
        }
    }

    public static void requestPermissions(android.app.Fragment fragment, String str, int i, String... strArr) {
        requestPermissions(fragment, str, 17039370, 17039360, i, strArr);
    }

    public static void requestPermissions(android.app.Fragment fragment, String str, int i, int i2, int i3, String... strArr) {
        if (hasPermissions(fragment.getActivity(), strArr)) {
            notifyAlreadyHasPermissions(fragment, i3, strArr);
        } else if (shouldShowRationale(fragment, strArr)) {
            showRationaleDialogFragment(fragment.getChildFragmentManager(), str, i, i2, i3, strArr);
        } else {
            fragment.requestPermissions(strArr, i3);
        }
    }

    public static void onRequestPermissionsResult(int i, String[] strArr, int[] iArr, Object... objArr) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i2 = 0; i2 < strArr.length; i2++) {
            String str = strArr[i2];
            if (iArr[i2] == 0) {
                arrayList.add(str);
            } else {
                arrayList2.add(str);
            }
        }
        for (Object obj : objArr) {
            if (!arrayList.isEmpty() && (obj instanceof PermissionCallbacks)) {
                ((PermissionCallbacks) obj).onPermissionsGranted(i, arrayList);
            }
            if (!arrayList2.isEmpty() && (obj instanceof PermissionCallbacks)) {
                ((PermissionCallbacks) obj).onPermissionsDenied(i, arrayList2);
            }
            if (!arrayList.isEmpty() && arrayList2.isEmpty()) {
                runAnnotatedMethods(obj, i);
            }
        }
    }

    public static boolean somePermissionPermanentlyDenied(Activity activity, List<String> list) {
        for (String str : list) {
            if (permissionPermanentlyDenied(activity, str)) {
                return true;
            }
        }
        return false;
    }

    public static boolean somePermissionPermanentlyDenied(Fragment fragment, List<String> list) {
        for (String str : list) {
            if (permissionPermanentlyDenied(fragment, str)) {
                return true;
            }
        }
        return false;
    }

    public static boolean somePermissionPermanentlyDenied(android.app.Fragment fragment, List<String> list) {
        for (String str : list) {
            if (permissionPermanentlyDenied(fragment, str)) {
                return true;
            }
        }
        return false;
    }

    public static boolean permissionPermanentlyDenied(Activity activity, String str) {
        return !shouldShowRequestPermissionRationale(activity, str);
    }

    public static boolean permissionPermanentlyDenied(Fragment fragment, String str) {
        return !shouldShowRequestPermissionRationale(fragment, str);
    }

    public static boolean permissionPermanentlyDenied(android.app.Fragment fragment, String str) {
        return !shouldShowRequestPermissionRationale(fragment, str);
    }

    private static void notifyAlreadyHasPermissions(Object obj, int i, String[] strArr) {
        int[] iArr = new int[strArr.length];
        for (int i2 = 0; i2 < strArr.length; i2++) {
            iArr[i2] = 0;
        }
        onRequestPermissionsResult(i, strArr, iArr, obj);
    }

    private static boolean shouldShowRationale(Object obj, String[] strArr) {
        boolean z = false;
        for (String str : strArr) {
            z = z || shouldShowRequestPermissionRationale(obj, str);
        }
        return z;
    }

    private static boolean shouldShowRequestPermissionRationale(Object obj, String str) {
        if (obj instanceof Activity) {
            return ActivityCompat.shouldShowRequestPermissionRationale((Activity) obj, str);
        }
        if (obj instanceof Fragment) {
            return ((Fragment) obj).shouldShowRequestPermissionRationale(str);
        }
        if (!(obj instanceof android.app.Fragment)) {
            throw new IllegalArgumentException("Object was neither an Activity nor a Fragment.");
        } else if (Build.VERSION.SDK_INT >= 23) {
            return ((android.app.Fragment) obj).shouldShowRequestPermissionRationale(str);
        } else {
            throw new IllegalArgumentException("Target SDK needs to be greater than 23 if caller is android.app.Fragment");
        }
    }

    private static void showRationaleDialogFragment(FragmentManager fragmentManager, String str, int i, int i2, int i3, String... strArr) {
        RationaleDialogFragment.newInstance(i, i2, str, i3, strArr).show(fragmentManager, DIALOG_TAG);
    }

    private static void runAnnotatedMethods(Object obj, int i) {
        Class<?> cls = obj.getClass();
        if (isUsingAndroidAnnotations(obj)) {
            cls = cls.getSuperclass();
        }
        Method[] declaredMethods = cls.getDeclaredMethods();
        for (Method method : declaredMethods) {
            if (method.isAnnotationPresent(AfterPermissionGranted.class) && ((AfterPermissionGranted) method.getAnnotation(AfterPermissionGranted.class)).value() == i) {
                if (method.getParameterTypes().length <= 0) {
                    try {
                        if (!method.isAccessible()) {
                            method.setAccessible(true);
                        }
                        method.invoke(obj, new Object[0]);
                    } catch (IllegalAccessException e) {
                        Log.e(TAG, "runDefaultMethod:IllegalAccessException", e);
                    } catch (InvocationTargetException e2) {
                        Log.e(TAG, "runDefaultMethod:InvocationTargetException", e2);
                    }
                } else {
                    throw new RuntimeException("Cannot execute method " + method.getName() + " because it is non-void method and/or has input parameters.");
                }
            }
        }
    }

    private static boolean isUsingAndroidAnnotations(Object obj) {
        if (!obj.getClass().getSimpleName().endsWith("_")) {
            return false;
        }
        try {
            return Class.forName("org.androidannotations.api.view.HasViews").isInstance(obj);
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }
}
