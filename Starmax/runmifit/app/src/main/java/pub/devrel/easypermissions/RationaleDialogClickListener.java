package pub.devrel.easypermissions;

import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import java.util.Arrays;
import pub.devrel.easypermissions.EasyPermissions;

class RationaleDialogClickListener implements DialogInterface.OnClickListener {
    private EasyPermissions.PermissionCallbacks mCallbacks;
    private RationaleDialogConfig mConfig;
    private Object mHost;

    RationaleDialogClickListener(RationaleDialogFragmentCompat rationaleDialogFragmentCompat, RationaleDialogConfig rationaleDialogConfig, EasyPermissions.PermissionCallbacks permissionCallbacks) {
        Object obj;
        if (rationaleDialogFragmentCompat.getParentFragment() != null) {
            obj = rationaleDialogFragmentCompat.getParentFragment();
        } else {
            obj = rationaleDialogFragmentCompat.getActivity();
        }
        this.mHost = obj;
        this.mConfig = rationaleDialogConfig;
        this.mCallbacks = permissionCallbacks;
    }

    RationaleDialogClickListener(RationaleDialogFragment rationaleDialogFragment, RationaleDialogConfig rationaleDialogConfig, EasyPermissions.PermissionCallbacks permissionCallbacks) {
        Object obj;
        if (Build.VERSION.SDK_INT >= 17) {
            if (rationaleDialogFragment.getParentFragment() != null) {
                obj = rationaleDialogFragment.getParentFragment();
            } else {
                obj = rationaleDialogFragment.getActivity();
            }
            this.mHost = obj;
        } else {
            this.mHost = rationaleDialogFragment.getActivity();
        }
        this.mConfig = rationaleDialogConfig;
        this.mCallbacks = permissionCallbacks;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            Object obj = this.mHost;
            if (obj instanceof Fragment) {
                ((Fragment) obj).requestPermissions(this.mConfig.permissions, this.mConfig.requestCode);
            } else {
                ActivityCompat.requestPermissions((FragmentActivity) obj, this.mConfig.permissions, this.mConfig.requestCode);
            }
        } else {
            notifyPermissionDenied();
        }
    }

    private void notifyPermissionDenied() {
        EasyPermissions.PermissionCallbacks permissionCallbacks = this.mCallbacks;
        if (permissionCallbacks != null) {
            permissionCallbacks.onPermissionsDenied(this.mConfig.requestCode, Arrays.asList(this.mConfig.permissions));
        }
    }
}
