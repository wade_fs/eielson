package pub.devrel.easypermissions;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

public class AppSettingsDialog implements Parcelable, DialogInterface.OnClickListener {
    public static final Parcelable.Creator<AppSettingsDialog> CREATOR = new Parcelable.Creator<AppSettingsDialog>() {
        /* class pub.devrel.easypermissions.AppSettingsDialog.C36621 */

        public AppSettingsDialog createFromParcel(Parcel parcel) {
            return new AppSettingsDialog(parcel);
        }

        public AppSettingsDialog[] newArray(int i) {
            return new AppSettingsDialog[i];
        }
    };
    public static final int DEFAULT_SETTINGS_REQ_CODE = 16061;
    static final String EXTRA_APP_SETTINGS = "extra_app_settings";
    private Object mActivityOrFragment;
    private Context mContext;
    private final String mNegativeButtonText;
    private DialogInterface.OnClickListener mNegativeListener;
    private final String mPositiveButtonText;
    private final String mRationale;
    private final int mRequestCode;
    private final String mTitle;

    public int describeContents() {
        return 0;
    }

    private AppSettingsDialog(Parcel parcel) {
        this.mRationale = parcel.readString();
        this.mTitle = parcel.readString();
        this.mPositiveButtonText = parcel.readString();
        this.mNegativeButtonText = parcel.readString();
        this.mRequestCode = parcel.readInt();
    }

    private AppSettingsDialog(Object obj, Context context, String str, String str2, String str3, String str4, DialogInterface.OnClickListener onClickListener, int i) {
        this.mActivityOrFragment = obj;
        this.mContext = context;
        this.mRationale = str;
        this.mTitle = str2;
        this.mPositiveButtonText = str3;
        this.mNegativeButtonText = str4;
        this.mNegativeListener = onClickListener;
        this.mRequestCode = i;
    }

    /* access modifiers changed from: package-private */
    public void setActivityOrFragment(Object obj) {
        this.mActivityOrFragment = obj;
    }

    /* access modifiers changed from: package-private */
    public void setContext(Context context) {
        this.mContext = context;
    }

    /* access modifiers changed from: package-private */
    public void setNegativeListener(DialogInterface.OnClickListener onClickListener) {
        this.mNegativeListener = onClickListener;
    }

    private void startForResult(Intent intent) {
        Object obj = this.mActivityOrFragment;
        if (obj instanceof Activity) {
            ((Activity) obj).startActivityForResult(intent, this.mRequestCode);
        } else if (obj instanceof Fragment) {
            ((Fragment) obj).startActivityForResult(intent, this.mRequestCode);
        } else if (obj instanceof android.app.Fragment) {
            ((android.app.Fragment) obj).startActivityForResult(intent, this.mRequestCode);
        }
    }

    public void show() {
        if (this.mNegativeListener == null) {
            startForResult(AppSettingsDialogHolderActivity.createShowDialogIntent(this.mContext, this));
        } else {
            showDialog();
        }
    }

    /* access modifiers changed from: package-private */
    public void showDialog() {
        new AlertDialog.Builder(this.mContext).setCancelable(false).setTitle(this.mTitle).setMessage(this.mRationale).setPositiveButton(this.mPositiveButtonText, this).setNegativeButton(this.mNegativeButtonText, this.mNegativeListener).create().show();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", this.mContext.getPackageName(), null));
        startForResult(intent);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mRationale);
        parcel.writeString(this.mTitle);
        parcel.writeString(this.mPositiveButtonText);
        parcel.writeString(this.mNegativeButtonText);
        parcel.writeInt(this.mRequestCode);
    }

    public static class Builder {
        private Object mActivityOrFragment;
        private Context mContext;
        private String mNegativeButton;
        private DialogInterface.OnClickListener mNegativeListener;
        private String mPositiveButton;
        private String mRationale;
        private int mRequestCode = -1;
        private String mTitle;

        @Deprecated
        public Builder(Activity activity, String str) {
            this.mActivityOrFragment = activity;
            this.mContext = activity;
            this.mRationale = str;
        }

        @Deprecated
        public Builder(Fragment fragment, String str) {
            this.mActivityOrFragment = fragment;
            this.mContext = fragment.getContext();
            this.mRationale = str;
        }

        @Deprecated
        public Builder(android.app.Fragment fragment, String str) {
            this.mActivityOrFragment = fragment;
            this.mContext = fragment.getActivity();
            this.mRationale = str;
        }

        public Builder(Activity activity) {
            this.mActivityOrFragment = activity;
            this.mContext = activity;
        }

        public Builder(Fragment fragment) {
            this.mActivityOrFragment = fragment;
            this.mContext = fragment.getContext();
        }

        public Builder(android.app.Fragment fragment) {
            this.mActivityOrFragment = fragment;
            this.mContext = fragment.getActivity();
        }

        public Builder setTitle(String str) {
            this.mTitle = str;
            return this;
        }

        public Builder setTitle(int i) {
            this.mTitle = this.mContext.getString(i);
            return this;
        }

        public Builder setRationale(String str) {
            this.mRationale = str;
            return this;
        }

        public Builder setRationale(int i) {
            this.mRationale = this.mContext.getString(i);
            return this;
        }

        public Builder setPositiveButton(String str) {
            this.mPositiveButton = str;
            return this;
        }

        public Builder setPositiveButton(int i) {
            this.mPositiveButton = this.mContext.getString(i);
            return this;
        }

        @Deprecated
        public Builder setNegativeButton(String str, DialogInterface.OnClickListener onClickListener) {
            this.mNegativeButton = str;
            this.mNegativeListener = onClickListener;
            return this;
        }

        public Builder setNegativeButton(String str) {
            this.mNegativeButton = str;
            return this;
        }

        public Builder setNegativeButton(int i) {
            this.mNegativeButton = this.mContext.getString(i);
            return this;
        }

        public Builder setRequestCode(int i) {
            this.mRequestCode = i;
            return this;
        }

        public AppSettingsDialog build() {
            this.mRationale = TextUtils.isEmpty(this.mRationale) ? this.mContext.getString(C3663R.string.rationale_ask_again) : this.mRationale;
            this.mTitle = TextUtils.isEmpty(this.mTitle) ? this.mContext.getString(C3663R.string.title_settings_dialog) : this.mTitle;
            this.mPositiveButton = TextUtils.isEmpty(this.mPositiveButton) ? this.mContext.getString(17039370) : this.mPositiveButton;
            this.mNegativeButton = TextUtils.isEmpty(this.mNegativeButton) ? this.mContext.getString(17039360) : this.mNegativeButton;
            int i = this.mRequestCode;
            if (i <= 0) {
                i = AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE;
            }
            this.mRequestCode = i;
            return new AppSettingsDialog(this.mActivityOrFragment, this.mContext, this.mRationale, this.mTitle, this.mPositiveButton, this.mNegativeButton, this.mNegativeListener, this.mRequestCode);
        }
    }
}
