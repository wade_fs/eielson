package org.apache.commons.math3.analysis.interpolation;

import java.io.Serializable;
import java.util.Arrays;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NoDataException;
import org.apache.commons.math3.exception.NonMonotonicSequenceException;
import org.apache.commons.math3.exception.NotFiniteNumberException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.MathUtils;

public class LoessInterpolator implements UnivariateInterpolator, Serializable {
    public static final double DEFAULT_ACCURACY = 1.0E-12d;
    public static final double DEFAULT_BANDWIDTH = 0.3d;
    public static final int DEFAULT_ROBUSTNESS_ITERS = 2;
    private static final long serialVersionUID = 5204927143605193821L;
    private final double accuracy;
    private final double bandwidth;
    private final int robustnessIters;

    public LoessInterpolator() {
        this.bandwidth = 0.3d;
        this.robustnessIters = 2;
        this.accuracy = 1.0E-12d;
    }

    public LoessInterpolator(double d, int i) {
        this(d, i, 1.0E-12d);
    }

    public LoessInterpolator(double d, int i, double d2) throws OutOfRangeException, NotPositiveException {
        if (d < 0.0d || d > 1.0d) {
            throw new OutOfRangeException(LocalizedFormats.BANDWIDTH, Double.valueOf(d), 0, 1);
        }
        this.bandwidth = d;
        if (i >= 0) {
            this.robustnessIters = i;
            this.accuracy = d2;
            return;
        }
        throw new NotPositiveException(LocalizedFormats.ROBUSTNESS_ITERATIONS, Integer.valueOf(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.analysis.interpolation.SplineInterpolator.interpolate(double[], double[]):org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction
     arg types: [double[], double[]]
     candidates:
      org.apache.commons.math3.analysis.interpolation.SplineInterpolator.interpolate(double[], double[]):org.apache.commons.math3.analysis.UnivariateFunction
      org.apache.commons.math3.analysis.interpolation.UnivariateInterpolator.interpolate(double[], double[]):org.apache.commons.math3.analysis.UnivariateFunction
      org.apache.commons.math3.analysis.interpolation.SplineInterpolator.interpolate(double[], double[]):org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction */
    public final PolynomialSplineFunction interpolate(double[] dArr, double[] dArr2) throws NonMonotonicSequenceException, DimensionMismatchException, NoDataException, NotFiniteNumberException, NumberIsTooSmallException {
        return new SplineInterpolator().interpolate(dArr, smooth(dArr, dArr2));
    }

    public final double[] smooth(double[] dArr, double[] dArr2, double[] dArr3) throws NonMonotonicSequenceException, DimensionMismatchException, NoDataException, NotFiniteNumberException, NumberIsTooSmallException {
        double[] dArr4 = dArr;
        double[] dArr5 = dArr2;
        double[] dArr6 = dArr3;
        if (dArr4.length == dArr5.length) {
            int length = dArr4.length;
            if (length != 0) {
                checkAllFiniteReal(dArr);
                checkAllFiniteReal(dArr2);
                checkAllFiniteReal(dArr3);
                MathArrays.checkOrder(dArr);
                int i = 0;
                char c = 1;
                if (length == 1) {
                    return new double[]{dArr5[0]};
                }
                int i2 = 2;
                if (length == 2) {
                    return new double[]{dArr5[0], dArr5[1]};
                }
                double d = this.bandwidth;
                double d2 = (double) length;
                Double.isNaN(d2);
                int i3 = (int) (d * d2);
                if (i3 >= 2) {
                    double[] dArr7 = new double[length];
                    double[] dArr8 = new double[length];
                    double[] dArr9 = new double[length];
                    double[] dArr10 = new double[length];
                    Arrays.fill(dArr10, 1.0d);
                    int i4 = 0;
                    while (i4 <= this.robustnessIters) {
                        int[] iArr = new int[i2];
                        iArr[i] = i;
                        iArr[c] = i3 - 1;
                        int i5 = 0;
                        while (true) {
                            double d3 = 0.0d;
                            if (i5 >= length) {
                                break;
                            }
                            double d4 = dArr4[i5];
                            if (i5 > 0) {
                                updateBandwidthInterval(dArr4, dArr6, i5, iArr);
                            }
                            int i6 = iArr[i];
                            int i7 = iArr[c];
                            double abs = FastMath.abs(1.0d / (dArr4[dArr4[i5] - dArr4[i6] > dArr4[i7] - dArr4[i5] ? i6 : i7] - d4));
                            double d5 = 0.0d;
                            double d6 = 0.0d;
                            double d7 = 0.0d;
                            double d8 = 0.0d;
                            double d9 = 0.0d;
                            int i8 = i6;
                            while (i8 <= i7) {
                                double d10 = dArr4[i8];
                                double d11 = dArr5[i8];
                                double tricube = tricube((i8 < i5 ? d4 - d10 : d10 - d4) * abs) * dArr10[i8] * dArr6[i8];
                                double d12 = d10 * tricube;
                                d6 += tricube;
                                d5 += d12;
                                d9 += d10 * d12;
                                d7 += tricube * d11;
                                d8 += d11 * d12;
                                i8++;
                            }
                            double d13 = d5 / d6;
                            double d14 = d7 / d6;
                            double d15 = d8 / d6;
                            double d16 = (d9 / d6) - (d13 * d13);
                            double[] dArr11 = dArr10;
                            int[] iArr2 = iArr;
                            if (FastMath.sqrt(FastMath.abs(d16)) >= this.accuracy) {
                                d3 = (d15 - (d13 * d14)) / d16;
                            }
                            dArr7[i5] = (d3 * d4) + (d14 - (d13 * d3));
                            dArr8[i5] = FastMath.abs(dArr5[i5] - dArr7[i5]);
                            i5++;
                            dArr10 = dArr11;
                            iArr = iArr2;
                            c = 1;
                        }
                        double[] dArr12 = dArr10;
                        if (i4 == this.robustnessIters) {
                            break;
                        }
                        System.arraycopy(dArr8, i, dArr9, i, length);
                        Arrays.sort(dArr9);
                        double d17 = dArr9[length / 2];
                        double d18 = d17;
                        if (FastMath.abs(d17) < this.accuracy) {
                            break;
                        }
                        for (int i9 = 0; i9 < length; i9++) {
                            double d19 = dArr8[i9] / (6.0d * d18);
                            if (d19 >= 1.0d) {
                                dArr12[i9] = 0.0d;
                            } else {
                                double d20 = 1.0d - (d19 * d19);
                                dArr12[i9] = d20 * d20;
                            }
                        }
                        i4++;
                        dArr10 = dArr12;
                        i = 0;
                        c = 1;
                        i2 = 2;
                    }
                    return dArr7;
                }
                throw new NumberIsTooSmallException(LocalizedFormats.BANDWIDTH, Integer.valueOf(i3), 2, true);
            }
            throw new NoDataException();
        }
        throw new DimensionMismatchException(dArr4.length, dArr5.length);
    }

    public final double[] smooth(double[] dArr, double[] dArr2) throws NonMonotonicSequenceException, DimensionMismatchException, NoDataException, NotFiniteNumberException, NumberIsTooSmallException {
        if (dArr.length == dArr2.length) {
            double[] dArr3 = new double[dArr.length];
            Arrays.fill(dArr3, 1.0d);
            return smooth(dArr, dArr2, dArr3);
        }
        throw new DimensionMismatchException(dArr.length, dArr2.length);
    }

    private static void updateBandwidthInterval(double[] dArr, double[] dArr2, int i, int[] iArr) {
        int i2 = iArr[0];
        int nextNonzero = nextNonzero(dArr2, iArr[1]);
        if (nextNonzero < dArr.length && dArr[nextNonzero] - dArr[i] < dArr[i] - dArr[i2]) {
            iArr[0] = nextNonzero(dArr2, iArr[0]);
            iArr[1] = nextNonzero;
        }
    }

    private static int nextNonzero(double[] dArr, int i) {
        do {
            i++;
            if (i >= dArr.length) {
                break;
            }
        } while (dArr[i] == 0.0d);
        return i;
    }

    private static double tricube(double d) {
        double abs = FastMath.abs(d);
        if (abs >= 1.0d) {
            return 0.0d;
        }
        double d2 = 1.0d - ((abs * abs) * abs);
        return d2 * d2 * d2;
    }

    private static void checkAllFiniteReal(double[] dArr) {
        for (double d : dArr) {
            MathUtils.checkFinite(d);
        }
    }
}
