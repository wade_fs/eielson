package org.apache.commons.math3.stat.interval;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.util.FastMath;

public class WilsonScoreInterval implements BinomialConfidenceInterval {
    public ConfidenceInterval createInterval(int i, int i2, double d) {
        int i3 = i;
        IntervalUtils.checkParameters(i, i2, d);
        double inverseCumulativeProbability = new NormalDistribution().inverseCumulativeProbability(1.0d - ((1.0d - d) / 2.0d));
        double pow = FastMath.pow(inverseCumulativeProbability, 2);
        double d2 = (double) i2;
        double d3 = (double) i3;
        Double.isNaN(d2);
        Double.isNaN(d3);
        double d4 = d2 / d3;
        Double.isNaN(d3);
        double d5 = 1.0d / d3;
        double d6 = 1.0d / ((d5 * pow) + 1.0d);
        double d7 = pow;
        double d8 = (double) (i3 * 2);
        Double.isNaN(d8);
        double d9 = ((1.0d / d8) * d7) + d4;
        double sqrt = inverseCumulativeProbability * FastMath.sqrt((d5 * d4 * (1.0d - d4)) + ((1.0d / (FastMath.pow(d3, 2) * 4.0d)) * d7));
        return new ConfidenceInterval((d9 - sqrt) * d6, d6 * (d9 + sqrt), d);
    }
}
