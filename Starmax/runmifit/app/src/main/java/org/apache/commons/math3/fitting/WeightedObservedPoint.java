package org.apache.commons.math3.fitting;

import java.io.Serializable;

public class WeightedObservedPoint implements Serializable {
    private static final long serialVersionUID = 5306874947404636157L;
    private final double weight;

    /* renamed from: x */
    private final double f7993x;

    /* renamed from: y */
    private final double f7994y;

    public WeightedObservedPoint(double d, double d2, double d3) {
        this.weight = d;
        this.f7993x = d2;
        this.f7994y = d3;
    }

    public double getWeight() {
        return this.weight;
    }

    public double getX() {
        return this.f7993x;
    }

    public double getY() {
        return this.f7994y;
    }
}
