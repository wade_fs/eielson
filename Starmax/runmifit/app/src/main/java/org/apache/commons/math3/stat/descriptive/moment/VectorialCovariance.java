package org.apache.commons.math3.stat.descriptive.moment;

import java.io.Serializable;
import java.util.Arrays;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class VectorialCovariance implements Serializable {
    private static final long serialVersionUID = 4118372414238930270L;
    private final boolean isBiasCorrected;

    /* renamed from: n */
    private long f8179n = 0;
    private final double[] productsSums;
    private final double[] sums;

    public VectorialCovariance(int i, boolean z) {
        this.sums = new double[i];
        this.productsSums = new double[((i * (i + 1)) / 2)];
        this.isBiasCorrected = z;
    }

    public void increment(double[] dArr) throws DimensionMismatchException {
        int length = dArr.length;
        double[] dArr2 = this.sums;
        if (length == dArr2.length) {
            int i = 0;
            int i2 = 0;
            while (i < dArr.length) {
                double[] dArr3 = this.sums;
                dArr3[i] = dArr3[i] + dArr[i];
                int i3 = i2;
                int i4 = 0;
                while (i4 <= i) {
                    double[] dArr4 = this.productsSums;
                    dArr4[i3] = dArr4[i3] + (dArr[i] * dArr[i4]);
                    i4++;
                    i3++;
                }
                i++;
                i2 = i3;
            }
            this.f8179n++;
            return;
        }
        throw new DimensionMismatchException(dArr.length, dArr2.length);
    }

    public RealMatrix getResult() {
        int length = this.sums.length;
        RealMatrix createRealMatrix = MatrixUtils.createRealMatrix(length, length);
        long j = this.f8179n;
        if (j > 1) {
            double d = (double) (j * (this.isBiasCorrected ? j - 1 : j));
            Double.isNaN(d);
            double d2 = 1.0d / d;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                int i3 = i2;
                int i4 = 0;
                while (i4 <= i) {
                    double d3 = (double) this.f8179n;
                    int i5 = i3 + 1;
                    double d4 = this.productsSums[i3];
                    Double.isNaN(d3);
                    double[] dArr = this.sums;
                    double d5 = ((d3 * d4) - (dArr[i] * dArr[i4])) * d2;
                    createRealMatrix.setEntry(i, i4, d5);
                    createRealMatrix.setEntry(i4, i, d5);
                    i4++;
                    i3 = i5;
                }
                i++;
                i2 = i3;
            }
        }
        return createRealMatrix;
    }

    public long getN() {
        return this.f8179n;
    }

    public void clear() {
        this.f8179n = 0;
        Arrays.fill(this.sums, 0.0d);
        Arrays.fill(this.productsSums, 0.0d);
    }

    public int hashCode() {
        int i = this.isBiasCorrected ? 1231 : 1237;
        long j = this.f8179n;
        return ((((((i + 31) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + Arrays.hashCode(this.productsSums)) * 31) + Arrays.hashCode(this.sums);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VectorialCovariance)) {
            return false;
        }
        VectorialCovariance vectorialCovariance = (VectorialCovariance) obj;
        return this.isBiasCorrected == vectorialCovariance.isBiasCorrected && this.f8179n == vectorialCovariance.f8179n && Arrays.equals(this.productsSums, vectorialCovariance.productsSums) && Arrays.equals(this.sums, vectorialCovariance.sums);
    }
}
