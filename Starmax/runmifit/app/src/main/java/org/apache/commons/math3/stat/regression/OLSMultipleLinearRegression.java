package org.apache.commons.math3.stat.regression;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.QRDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.moment.SecondMoment;

public class OLSMultipleLinearRegression extends AbstractMultipleLinearRegression {

    /* renamed from: qr */
    private QRDecomposition f8190qr;
    private final double threshold;

    public OLSMultipleLinearRegression() {
        this(0.0d);
    }

    public OLSMultipleLinearRegression(double d) {
        this.f8190qr = null;
        this.threshold = d;
    }

    public void newSampleData(double[] dArr, double[][] dArr2) throws MathIllegalArgumentException {
        validateSampleData(dArr2, dArr);
        newYSampleData(dArr);
        newXSampleData(dArr2);
    }

    public void newSampleData(double[] dArr, int i, int i2) {
        super.newSampleData(dArr, i, i2);
        this.f8190qr = new QRDecomposition(getX(), this.threshold);
    }

    public RealMatrix calculateHat() {
        RealMatrix q = this.f8190qr.getQ();
        int columnDimension = this.f8190qr.getR().getColumnDimension();
        int columnDimension2 = q.getColumnDimension();
        Array2DRowRealMatrix array2DRowRealMatrix = new Array2DRowRealMatrix(columnDimension2, columnDimension2);
        double[][] dataRef = array2DRowRealMatrix.getDataRef();
        for (int i = 0; i < columnDimension2; i++) {
            for (int i2 = 0; i2 < columnDimension2; i2++) {
                if (i != i2 || i >= columnDimension) {
                    dataRef[i][i2] = 0.0d;
                } else {
                    dataRef[i][i2] = 1.0d;
                }
            }
        }
        return q.multiply(array2DRowRealMatrix).multiply(q.transpose());
    }

    public double calculateTotalSumOfSquares() throws MathIllegalArgumentException {
        if (isNoIntercept()) {
            return StatUtils.sumSq(getY().toArray());
        }
        return new SecondMoment().evaluate(getY().toArray());
    }

    public double calculateResidualSumOfSquares() {
        RealVector calculateResiduals = calculateResiduals();
        return calculateResiduals.dotProduct(calculateResiduals);
    }

    public double calculateRSquared() throws MathIllegalArgumentException {
        return 1.0d - (calculateResidualSumOfSquares() / calculateTotalSumOfSquares());
    }

    public double calculateAdjustedRSquared() throws MathIllegalArgumentException {
        double d;
        double rowDimension = (double) getX().getRowDimension();
        if (isNoIntercept()) {
            double columnDimension = (double) getX().getColumnDimension();
            Double.isNaN(rowDimension);
            Double.isNaN(columnDimension);
            Double.isNaN(rowDimension);
            d = (1.0d - calculateRSquared()) * (rowDimension / (rowDimension - columnDimension));
        } else {
            double calculateResidualSumOfSquares = calculateResidualSumOfSquares();
            Double.isNaN(rowDimension);
            double calculateTotalSumOfSquares = calculateTotalSumOfSquares();
            double columnDimension2 = (double) getX().getColumnDimension();
            Double.isNaN(rowDimension);
            Double.isNaN(columnDimension2);
            d = (calculateResidualSumOfSquares * (rowDimension - 1.0d)) / (calculateTotalSumOfSquares * (rowDimension - columnDimension2));
        }
        return 1.0d - d;
    }

    /* access modifiers changed from: protected */
    public void newXSampleData(double[][] dArr) {
        super.newXSampleData(dArr);
        this.f8190qr = new QRDecomposition(getX());
    }

    /* access modifiers changed from: protected */
    public RealVector calculateBeta() {
        return this.f8190qr.getSolver().solve(getY());
    }

    /* access modifiers changed from: protected */
    public RealMatrix calculateBetaVariance() {
        int columnDimension = getX().getColumnDimension() - 1;
        RealMatrix inverse = new LUDecomposition(this.f8190qr.getR().getSubMatrix(0, columnDimension, 0, columnDimension)).getSolver().getInverse();
        return inverse.multiply(inverse.transpose());
    }
}
