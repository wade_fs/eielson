package org.apache.commons.math3.optimization.univariate;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.util.Incrementor;

@Deprecated
public class BracketFinder {
    private static final double EPS_MIN = 1.0E-21d;
    private static final double GOLD = 1.618034d;
    private final Incrementor evaluations;
    private double fHi;
    private double fLo;
    private double fMid;
    private final double growLimit;

    /* renamed from: hi */
    private double f8132hi;

    /* renamed from: lo */
    private double f8133lo;
    private double mid;

    public BracketFinder() {
        this(100.0d, 50);
    }

    public BracketFinder(double d, int i) {
        this.evaluations = new Incrementor();
        if (d <= 0.0d) {
            throw new NotStrictlyPositiveException(Double.valueOf(d));
        } else if (i > 0) {
            this.growLimit = d;
            this.evaluations.setMaximalCount(i);
        } else {
            throw new NotStrictlyPositiveException(Integer.valueOf(i));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a4, code lost:
        r9 = r5;
        r5 = r7;
        r7 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012f, code lost:
        r40 = r7;
        r13 = r2;
        r7 = r15;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void search(org.apache.commons.math3.analysis.UnivariateFunction r37, org.apache.commons.math3.optimization.GoalType r38, double r39, double r41) {
        /*
            r36 = this;
            r0 = r36
            r1 = r37
            org.apache.commons.math3.util.Incrementor r2 = r0.evaluations
            r2.resetCount()
            org.apache.commons.math3.optimization.GoalType r2 = org.apache.commons.math3.optimization.GoalType.MINIMIZE
            r3 = r38
            if (r3 != r2) goto L_0x0014
            r2 = 1
            r2 = r39
            r4 = 1
            goto L_0x0018
        L_0x0014:
            r2 = 0
            r2 = r39
            r4 = 0
        L_0x0018:
            double r5 = r0.eval(r1, r2)
            r7 = r41
            double r9 = r0.eval(r1, r7)
            if (r4 == 0) goto L_0x0029
            int r11 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r11 >= 0) goto L_0x002e
            goto L_0x0038
        L_0x0029:
            int r11 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r11 <= 0) goto L_0x002e
            goto L_0x0038
        L_0x002e:
            r32 = r5
            r5 = r9
            r9 = r32
            r34 = r2
            r2 = r7
            r7 = r34
        L_0x0038:
            double r11 = r2 - r7
            r13 = 4609965796492119705(0x3ff9e3779e9d0e99, double:1.618034)
            double r11 = r11 * r13
            double r11 = r11 + r2
            double r15 = r0.eval(r1, r11)
        L_0x0046:
            if (r4 == 0) goto L_0x004d
            int r17 = (r15 > r5 ? 1 : (r15 == r5 ? 0 : -1))
            if (r17 >= 0) goto L_0x012f
            goto L_0x0051
        L_0x004d:
            int r17 = (r15 > r5 ? 1 : (r15 == r5 ? 0 : -1))
            if (r17 <= 0) goto L_0x012f
        L_0x0051:
            double r17 = r2 - r7
            double r19 = r5 - r15
            double r19 = r19 * r17
            double r21 = r2 - r11
            double r23 = r5 - r9
            double r23 = r23 * r21
            double r25 = r23 - r19
            double r27 = org.apache.commons.math3.util.FastMath.abs(r25)
            r29 = 4292743757239851855(0x3b92e3b40a0e9b4f, double:1.0E-21)
            int r31 = (r27 > r29 ? 1 : (r27 == r29 ? 0 : -1))
            if (r31 >= 0) goto L_0x0072
            r25 = 4297247356867222351(0x3ba2e3b40a0e9b4f, double:2.0E-21)
            goto L_0x0076
        L_0x0072:
            r27 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r25 = r25 * r27
        L_0x0076:
            double r21 = r21 * r23
            double r17 = r17 * r19
            double r21 = r21 - r17
            double r21 = r21 / r25
            double r13 = r2 - r21
            r40 = r7
            double r7 = r0.growLimit
            double r17 = r11 - r2
            double r7 = r7 * r17
            double r7 = r7 + r2
            double r19 = r13 - r11
            double r21 = r2 - r13
            double r21 = r21 * r19
            r23 = 0
            int r25 = (r21 > r23 ? 1 : (r21 == r23 ? 0 : -1))
            if (r25 <= 0) goto L_0x00c6
            double r7 = r0.eval(r1, r13)
            if (r4 == 0) goto L_0x00a0
            int r19 = (r7 > r15 ? 1 : (r7 == r15 ? 0 : -1))
            if (r19 >= 0) goto L_0x00a9
            goto L_0x00a4
        L_0x00a0:
            int r19 = (r7 > r15 ? 1 : (r7 == r15 ? 0 : -1))
            if (r19 <= 0) goto L_0x00a9
        L_0x00a4:
            r9 = r5
            r5 = r7
            r7 = r15
            goto L_0x0135
        L_0x00a9:
            if (r4 == 0) goto L_0x00b0
            int r19 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r19 <= 0) goto L_0x00b8
            goto L_0x00b4
        L_0x00b0:
            int r19 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r19 >= 0) goto L_0x00b8
        L_0x00b4:
            r11 = r13
            r13 = r2
            goto L_0x0133
        L_0x00b8:
            r7 = 4609965796492119705(0x3ff9e3779e9d0e99, double:1.618034)
            double r17 = r17 * r7
            double r7 = r11 + r17
            double r9 = r0.eval(r1, r7)
            goto L_0x00d4
        L_0x00c6:
            double r9 = r13 - r7
            double r21 = r7 - r11
            double r21 = r21 * r9
            int r25 = (r21 > r23 ? 1 : (r21 == r23 ? 0 : -1))
            if (r25 < 0) goto L_0x00e6
            double r9 = r0.eval(r1, r7)
        L_0x00d4:
            r21 = 4609965796492119705(0x3ff9e3779e9d0e99, double:1.618034)
        L_0x00d9:
            r32 = r5
            r5 = r15
            r15 = r9
            r9 = r32
            r34 = r2
            r2 = r11
            r11 = r7
            r7 = r34
            goto L_0x012b
        L_0x00e6:
            double r7 = r11 - r13
            double r9 = r9 * r7
            int r7 = (r9 > r23 ? 1 : (r9 == r23 ? 0 : -1))
            if (r7 <= 0) goto L_0x011d
            double r7 = r0.eval(r1, r13)
            if (r4 == 0) goto L_0x00f9
            int r9 = (r7 > r15 ? 1 : (r7 == r15 ? 0 : -1))
            if (r9 >= 0) goto L_0x0111
            goto L_0x00fd
        L_0x00f9:
            int r9 = (r7 > r15 ? 1 : (r7 == r15 ? 0 : -1))
            if (r9 <= 0) goto L_0x0111
        L_0x00fd:
            r21 = 4609965796492119705(0x3ff9e3779e9d0e99, double:1.618034)
            double r19 = r19 * r21
            double r2 = r13 + r19
            double r5 = r0.eval(r1, r2)
            r9 = r15
            r15 = r5
            r5 = r7
            r7 = r11
            r11 = r2
            r2 = r13
            goto L_0x012b
        L_0x0111:
            r21 = 4609965796492119705(0x3ff9e3779e9d0e99, double:1.618034)
            r9 = r5
            r5 = r15
            r15 = r7
            r7 = r2
            r2 = r11
            r11 = r13
            goto L_0x012b
        L_0x011d:
            r21 = 4609965796492119705(0x3ff9e3779e9d0e99, double:1.618034)
            double r17 = r17 * r21
            double r7 = r11 + r17
            double r9 = r0.eval(r1, r7)
            goto L_0x00d9
        L_0x012b:
            r13 = r21
            goto L_0x0046
        L_0x012f:
            r40 = r7
            r13 = r2
            r7 = r15
        L_0x0133:
            r2 = r40
        L_0x0135:
            r0.f8133lo = r2
            r0.fLo = r9
            r0.mid = r13
            r0.fMid = r5
            r0.f8132hi = r11
            r0.fHi = r7
            double r1 = r0.f8133lo
            double r3 = r0.f8132hi
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x0155
            r0.f8133lo = r3
            r0.f8132hi = r1
            double r1 = r0.fLo
            double r3 = r0.fHi
            r0.fLo = r3
            r0.fHi = r1
        L_0x0155:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optimization.univariate.BracketFinder.search(org.apache.commons.math3.analysis.UnivariateFunction, org.apache.commons.math3.optimization.GoalType, double, double):void");
    }

    public int getMaxEvaluations() {
        return this.evaluations.getMaximalCount();
    }

    public int getEvaluations() {
        return this.evaluations.getCount();
    }

    public double getLo() {
        return this.f8133lo;
    }

    public double getFLo() {
        return this.fLo;
    }

    public double getHi() {
        return this.f8132hi;
    }

    public double getFHi() {
        return this.fHi;
    }

    public double getMid() {
        return this.mid;
    }

    public double getFMid() {
        return this.fMid;
    }

    private double eval(UnivariateFunction univariateFunction, double d) {
        try {
            this.evaluations.incrementCount();
            return univariateFunction.value(d);
        } catch (MaxCountExceededException e) {
            throw new TooManyEvaluationsException(e.getMax());
        }
    }
}
