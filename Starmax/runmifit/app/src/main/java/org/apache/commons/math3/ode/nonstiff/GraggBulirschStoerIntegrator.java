package org.apache.commons.math3.ode.nonstiff;

import org.apache.commons.math3.analysis.solvers.UnivariateSolver;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.ode.events.EventHandler;
import org.apache.commons.math3.ode.sampling.StepHandler;
import org.apache.commons.math3.util.FastMath;

public class GraggBulirschStoerIntegrator extends AdaptiveStepsizeIntegrator {
    private static final String METHOD_NAME = "Gragg-Bulirsch-Stoer";
    private double[][] coeff;
    private int[] costPerStep;
    private double[] costPerTimeUnit;
    private int maxChecks;
    private int maxIter;
    private int maxOrder;
    private int mudif;
    private double[] optimalStep;
    private double orderControl1;
    private double orderControl2;
    private boolean performTest;
    private int[] sequence;
    private double stabilityReduction;
    private double stepControl1;
    private double stepControl2;
    private double stepControl3;
    private double stepControl4;
    private boolean useInterpolationError;

    public GraggBulirschStoerIntegrator(double d, double d2, double d3, double d4) {
        super(METHOD_NAME, d, d2, d3, d4);
        setStabilityCheck(true, -1, -1, -1.0d);
        setControlFactors(-1.0d, -1.0d, -1.0d, -1.0d);
        setOrderControl(-1, -1.0d, -1.0d);
        setInterpolationControl(true, -1);
    }

    public GraggBulirschStoerIntegrator(double d, double d2, double[] dArr, double[] dArr2) {
        super(METHOD_NAME, d, d2, dArr, dArr2);
        setStabilityCheck(true, -1, -1, -1.0d);
        setControlFactors(-1.0d, -1.0d, -1.0d, -1.0d);
        setOrderControl(-1, -1.0d, -1.0d);
        setInterpolationControl(true, -1);
    }

    public void setStabilityCheck(boolean z, int i, int i2, double d) {
        this.performTest = z;
        if (i <= 0) {
            i = 2;
        }
        this.maxIter = i;
        if (i2 <= 0) {
            i2 = 1;
        }
        this.maxChecks = i2;
        if (d < 1.0E-4d || d > 0.9999d) {
            this.stabilityReduction = 0.5d;
        } else {
            this.stabilityReduction = d;
        }
    }

    public void setControlFactors(double d, double d2, double d3, double d4) {
        if (d < 1.0E-4d || d > 0.9999d) {
            this.stepControl1 = 0.65d;
        } else {
            this.stepControl1 = d;
        }
        if (d2 < 1.0E-4d || d2 > 0.9999d) {
            this.stepControl2 = 0.94d;
        } else {
            this.stepControl2 = d2;
        }
        if (d3 < 1.0E-4d || d3 > 0.9999d) {
            this.stepControl3 = 0.02d;
        } else {
            this.stepControl3 = d3;
        }
        if (d4 < 1.0001d || d4 > 999.9d) {
            this.stepControl4 = 4.0d;
        } else {
            this.stepControl4 = d4;
        }
    }

    public void setOrderControl(int i, double d, double d2) {
        if (i <= 6 || i % 2 != 0) {
            this.maxOrder = 18;
        }
        if (d < 1.0E-4d || d > 0.9999d) {
            this.orderControl1 = 0.8d;
        } else {
            this.orderControl1 = d;
        }
        if (d2 < 1.0E-4d || d2 > 0.9999d) {
            this.orderControl2 = 0.9d;
        } else {
            this.orderControl2 = d2;
        }
        initializeArrays();
    }

    public void addStepHandler(StepHandler stepHandler) {
        super.addStepHandler(stepHandler);
        initializeArrays();
    }

    public void addEventHandler(EventHandler eventHandler, double d, double d2, int i, UnivariateSolver univariateSolver) {
        super.addEventHandler(eventHandler, d, d2, i, univariateSolver);
        initializeArrays();
    }

    private void initializeArrays() {
        int i = this.maxOrder / 2;
        int[] iArr = this.sequence;
        if (iArr == null || iArr.length != i) {
            this.sequence = new int[i];
            this.costPerStep = new int[i];
            this.coeff = new double[i][];
            this.costPerTimeUnit = new double[i];
            this.optimalStep = new double[i];
        }
        for (int i2 = 0; i2 < i; i2++) {
            this.sequence[i2] = (i2 * 4) + 2;
        }
        this.costPerStep[0] = this.sequence[0] + 1;
        for (int i3 = 1; i3 < i; i3++) {
            int[] iArr2 = this.costPerStep;
            iArr2[i3] = iArr2[i3 - 1] + this.sequence[i3];
        }
        int i4 = 0;
        while (i4 < i) {
            this.coeff[i4] = i4 > 0 ? new double[i4] : null;
            for (int i5 = 0; i5 < i4; i5++) {
                int[] iArr3 = this.sequence;
                double d = (double) iArr3[i4];
                double d2 = (double) iArr3[(i4 - i5) - 1];
                Double.isNaN(d);
                Double.isNaN(d2);
                double d3 = d / d2;
                this.coeff[i4][i5] = 1.0d / ((d3 * d3) - 1.0d);
            }
            i4++;
        }
    }

    public void setInterpolationControl(boolean z, int i) {
        this.useInterpolationError = z;
        if (i <= 0 || i >= 7) {
            this.mudif = 4;
        } else {
            this.mudif = i;
        }
    }

    private void rescale(double[] dArr, double[] dArr2, double[] dArr3) {
        int i = 0;
        if (this.vecAbsoluteTolerance == null) {
            while (i < dArr3.length) {
                dArr3[i] = this.scalAbsoluteTolerance + (this.scalRelativeTolerance * FastMath.max(FastMath.abs(dArr[i]), FastMath.abs(dArr2[i])));
                i++;
            }
            return;
        }
        while (i < dArr3.length) {
            dArr3[i] = this.vecAbsoluteTolerance[i] + (this.vecRelativeTolerance[i] * FastMath.max(FastMath.abs(dArr[i]), FastMath.abs(dArr2[i])));
            i++;
        }
    }

    private boolean tryStep(double d, double[] dArr, double d2, int i, double[] dArr2, double[][] dArr3, double[] dArr4, double[] dArr5, double[] dArr6) throws MaxCountExceededException, DimensionMismatchException {
        double d3;
        double[] dArr7 = dArr;
        int i2 = i;
        double[] dArr8 = dArr2;
        double[] dArr9 = dArr5;
        int i3 = this.sequence[i2];
        double d4 = (double) i3;
        Double.isNaN(d4);
        double d5 = d2 / d4;
        double d6 = 2.0d * d5;
        double d7 = d + d5;
        int i4 = 0;
        for (int i5 = 0; i5 < dArr7.length; i5++) {
            dArr6[i5] = dArr7[i5];
            dArr9[i5] = dArr7[i5] + (dArr3[0][i5] * d5);
        }
        computeDerivatives(d7, dArr9, dArr3[1]);
        double d8 = d7;
        int i6 = 1;
        while (i6 < i3) {
            if (i6 * 2 == i3) {
                System.arraycopy(dArr9, i4, dArr4, i4, dArr7.length);
            }
            d8 += d5;
            for (int i7 = 0; i7 < dArr7.length; i7++) {
                double d9 = dArr9[i7];
                dArr9[i7] = dArr6[i7] + (dArr3[i6][i7] * d6);
                dArr6[i7] = d9;
            }
            int i8 = i6 + 1;
            computeDerivatives(d8, dArr9, dArr3[i8]);
            if (!this.performTest || i6 > this.maxChecks || i2 >= this.maxIter) {
                d3 = d6;
                i4 = 0;
            } else {
                double d10 = 0.0d;
                d3 = d6;
                double d11 = 0.0d;
                for (int i9 = 0; i9 < dArr8.length; i9++) {
                    double d12 = dArr3[0][i9] / dArr8[i9];
                    d11 += d12 * d12;
                }
                for (int i10 = 0; i10 < dArr8.length; i10++) {
                    double d13 = (dArr3[i8][i10] - dArr3[0][i10]) / dArr8[i10];
                    d10 += d13 * d13;
                }
                i4 = 0;
                if (d10 > FastMath.max(1.0E-15d, d11) * 4.0d) {
                    return false;
                }
            }
            d6 = d3;
            i2 = i;
            dArr8 = dArr2;
            i6 = i8;
        }
        while (i4 < dArr7.length) {
            dArr9[i4] = (dArr6[i4] + dArr9[i4] + (dArr3[i3][i4] * d5)) * 0.5d;
            i4++;
        }
        return true;
    }

    private void extrapolate(int i, int i2, double[][] dArr, double[] dArr2) {
        int i3 = i2;
        double[] dArr3 = dArr2;
        int i4 = 1;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            for (int i5 = 0; i5 < dArr3.length; i5++) {
                int i6 = i3 - i4;
                int i7 = i6 - 1;
                dArr[i7][i5] = dArr[i6][i5] + (this.coeff[i3 + i][i4 - 1] * (dArr[i6][i5] - dArr[i7][i5]));
            }
            i4++;
        }
        for (int i8 = 0; i8 < dArr3.length; i8++) {
            dArr3[i8] = dArr[0][i8] + (this.coeff[i3 + i][i3 - 1] * (dArr[0][i8] - dArr3[i8]));
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v18, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v0, resolved type: double[][]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x052b  */
    /* JADX WARNING: Removed duplicated region for block: B:202:0x05e2  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x05fc  */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x0663  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x067d  */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x0680  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x0685  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x0699 A[LOOP:3: B:23:0x013c->B:224:0x0699, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x068b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void integrate(org.apache.commons.math3.ode.ExpandableStatefulODE r50, double r51) throws org.apache.commons.math3.exception.NumberIsTooSmallException, org.apache.commons.math3.exception.DimensionMismatchException, org.apache.commons.math3.exception.MaxCountExceededException, org.apache.commons.math3.exception.NoBracketingException {
        /*
            r49 = this;
            r12 = r49
            r13 = r50
            r49.sanityChecks(r50, r51)
            r49.setEquations(r50)
            double r0 = r50.getTime()
            r15 = 1
            int r2 = (r51 > r0 ? 1 : (r51 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0015
            r1 = 1
            goto L_0x0016
        L_0x0015:
            r1 = 0
        L_0x0016:
            double[] r2 = r50.getCompleteState()
            java.lang.Object r0 = r2.clone()
            double[] r0 = (double[]) r0
            int r3 = r0.length
            double[] r11 = new double[r3]
            int r3 = r0.length
            double[] r10 = new double[r3]
            int r3 = r0.length
            double[] r9 = new double[r3]
            int r3 = r0.length
            double[] r8 = new double[r3]
            int[] r3 = r12.sequence
            int r4 = r3.length
            int r4 = r4 - r15
            double[][] r7 = new double[r4][]
            int r3 = r3.length
            int r3 = r3 - r15
            double[][] r6 = new double[r3][]
            r3 = 0
        L_0x0037:
            int[] r4 = r12.sequence
            int r5 = r4.length
            int r5 = r5 - r15
            if (r3 >= r5) goto L_0x004a
            int r4 = r0.length
            double[] r4 = new double[r4]
            r7[r3] = r4
            int r4 = r0.length
            double[] r4 = new double[r4]
            r6[r3] = r4
            int r3 = r3 + 1
            goto L_0x0037
        L_0x004a:
            int r3 = r4.length
            double[][][] r5 = new double[r3][][]
            r3 = 0
        L_0x004e:
            int[] r4 = r12.sequence
            int r14 = r4.length
            if (r3 >= r14) goto L_0x0075
            r4 = r4[r3]
            int r4 = r4 + r15
            double[][] r4 = new double[r4][]
            r5[r3] = r4
            r4 = r5[r3]
            r14 = 0
            r4[r14] = r11
            r4 = 0
        L_0x0060:
            int[] r14 = r12.sequence
            r14 = r14[r3]
            if (r4 >= r14) goto L_0x0071
            r14 = r5[r3]
            int r4 = r4 + 1
            int r15 = r2.length
            double[] r15 = new double[r15]
            r14[r4] = r15
            r15 = 1
            goto L_0x0060
        L_0x0071:
            int r3 = r3 + 1
            r15 = 1
            goto L_0x004e
        L_0x0075:
            if (r0 == r2) goto L_0x007c
            int r3 = r2.length
            r4 = 0
            java.lang.System.arraycopy(r2, r4, r0, r4, r3)
        L_0x007c:
            int r3 = r2.length
            double[] r14 = new double[r3]
            int[] r3 = r12.sequence
            int r3 = r3.length
            r15 = 2
            int r3 = r3 * 2
            r4 = 1
            int r3 = r3 + r4
            int r4 = r2.length
            int[] r3 = new int[]{r3, r4}
            java.lang.Class<double> r4 = double.class
            java.lang.Object r3 = java.lang.reflect.Array.newInstance(r4, r3)
            r17 = r3
            double[][] r17 = (double[][]) r17
            int r3 = r12.mainSetDimension
            double[] r4 = new double[r3]
            r12.rescale(r0, r0, r4)
            double[] r3 = r12.vecRelativeTolerance
            r18 = r2
            if (r3 != 0) goto L_0x00a6
            double r2 = r12.scalRelativeTolerance
            goto L_0x00ad
        L_0x00a6:
            double[] r2 = r12.vecRelativeTolerance
            r3 = 0
            r19 = r2[r3]
            r2 = r19
        L_0x00ad:
            r20 = r4
            r19 = r5
            r4 = 4457293557087583675(0x3ddb7cdfd9d7bdbb, double:1.0E-10)
            double r2 = org.apache.commons.math3.util.FastMath.max(r4, r2)
            double r2 = org.apache.commons.math3.util.FastMath.log10(r2)
            int[] r4 = r12.sequence
            int r4 = r4.length
            int r4 = r4 - r15
            r21 = 4603579539098121011(0x3fe3333333333333, double:0.6)
            double r2 = r2 * r21
            r21 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r2 = r21 - r2
            double r2 = org.apache.commons.math3.util.FastMath.floor(r2)
            int r2 = (int) r2
            int r2 = org.apache.commons.math3.util.FastMath.min(r4, r2)
            r3 = 1
            int r23 = org.apache.commons.math3.util.FastMath.max(r3, r2)
            org.apache.commons.math3.ode.nonstiff.GraggBulirschStoerStepInterpolator r2 = new org.apache.commons.math3.ode.nonstiff.GraggBulirschStoerStepInterpolator
            org.apache.commons.math3.ode.EquationsMapper r24 = r50.getPrimaryMapper()
            org.apache.commons.math3.ode.EquationsMapper[] r25 = r50.getSecondaryMappers()
            r3 = r2
            r5 = r20
            r4 = r0
            r5 = r11
            r15 = r6
            r6 = r10
            r13 = r7
            r7 = r14
            r27 = r8
            r8 = r17
            r28 = r9
            r9 = r1
            r29 = r14
            r14 = r10
            r10 = r24
            r24 = r14
            r14 = r11
            r11 = r25
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11)
            double r3 = r50.getTime()
            r2.storeTime(r3)
            double r3 = r50.getTime()
            r12.stepStart = r3
            r6 = 9218868437227405311(0x7fefffffffffffff, double:1.7976931348623157E308)
            double r3 = r50.getTime()
            r11 = r0
            r0 = r49
            r10 = r1
            r8 = r2
            r9 = r18
            r1 = r3
            r3 = r9
            r4 = r51
            r0.initIntegration(r1, r3, r4)
            double[] r0 = r12.costPerTimeUnit
            r30 = 0
            r1 = 0
            r0[r1] = r30
            r12.isLastStep = r1
            r33 = r6
            r25 = r23
            r1 = r30
            r0 = 1
            r18 = 0
            r23 = 1
            r32 = 0
        L_0x013c:
            if (r0 == 0) goto L_0x0170
            r8.shift()
            if (r18 != 0) goto L_0x0148
            double r3 = r12.stepStart
            r12.computeDerivatives(r3, r11, r14)
        L_0x0148:
            if (r23 == 0) goto L_0x0168
            int r0 = r25 * 2
            r1 = 1
            int r2 = r0 + 1
            double r4 = r12.stepStart
            r0 = r49
            r1 = r10
            r3 = r20
            r6 = r11
            r7 = r14
            r35 = r14
            r14 = r8
            r8 = r28
            r36 = r14
            r14 = r9
            r9 = r27
            double r0 = r0.initializeStep(r1, r2, r3, r4, r6, r7, r8, r9)
            r1 = r0
            goto L_0x016d
        L_0x0168:
            r36 = r8
            r35 = r14
            r14 = r9
        L_0x016d:
            r37 = 0
            goto L_0x0177
        L_0x0170:
            r36 = r8
            r35 = r14
            r14 = r9
            r37 = r0
        L_0x0177:
            r12.stepSize = r1
            if (r10 == 0) goto L_0x0184
            double r3 = r12.stepStart
            double r5 = r12.stepSize
            double r3 = r3 + r5
            int r0 = (r3 > r51 ? 1 : (r3 == r51 ? 0 : -1))
            if (r0 > 0) goto L_0x018f
        L_0x0184:
            if (r10 != 0) goto L_0x0195
            double r3 = r12.stepStart
            double r5 = r12.stepSize
            double r3 = r3 + r5
            int r0 = (r3 > r51 ? 1 : (r3 == r51 ? 0 : -1))
            if (r0 >= 0) goto L_0x0195
        L_0x018f:
            double r3 = r12.stepStart
            double r3 = r51 - r3
            r12.stepSize = r3
        L_0x0195:
            double r3 = r12.stepStart
            double r5 = r12.stepSize
            double r3 = r3 + r5
            if (r10 == 0) goto L_0x01a1
            int r0 = (r3 > r51 ? 1 : (r3 == r51 ? 0 : -1))
            if (r0 < 0) goto L_0x01a7
            goto L_0x01a5
        L_0x01a1:
            int r0 = (r3 > r51 ? 1 : (r3 == r51 ? 0 : -1))
            if (r0 > 0) goto L_0x01a7
        L_0x01a5:
            r0 = 1
            goto L_0x01a8
        L_0x01a7:
            r0 = 0
        L_0x01a8:
            r12.isLastStep = r0
            r9 = -1
            r38 = r1
            r8 = r25
            r40 = r33
            r6 = -1
            r25 = 1
            r33 = 0
        L_0x01b6:
            if (r25 == 0) goto L_0x03c5
            int r7 = r6 + 1
            double r1 = r12.stepStart
            double r4 = r12.stepSize
            r34 = r19[r7]
            if (r7 != 0) goto L_0x01c8
            r0 = 0
            r3 = r17[r0]
            r44 = r3
            goto L_0x01ce
        L_0x01c8:
            int r0 = r7 + -1
            r0 = r13[r0]
            r44 = r0
        L_0x01ce:
            if (r7 != 0) goto L_0x01d3
            r45 = r24
            goto L_0x01d9
        L_0x01d3:
            int r0 = r7 + -1
            r0 = r15[r0]
            r45 = r0
        L_0x01d9:
            r0 = r49
            r3 = r11
            r46 = r13
            r47 = r14
            r13 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r6 = r7
            r13 = r7
            r7 = r20
            r14 = r8
            r8 = r34
            r9 = r44
            r34 = r14
            r14 = r10
            r10 = r45
            r48 = r11
            r11 = r28
            boolean r0 = r0.tryStep(r1, r3, r4, r6, r7, r8, r9, r10, r11)
            if (r0 != 0) goto L_0x0219
            double r0 = r12.stepSize
            double r2 = r12.stabilityReduction
            double r0 = r0 * r2
            r2 = 0
            double r0 = r12.filterStep(r0, r14, r2)
            double r38 = org.apache.commons.math3.util.FastMath.abs(r0)
            r6 = r13
            r10 = r14
            r8 = r34
            r13 = r46
            r14 = r47
            r11 = r48
        L_0x0213:
            r9 = -1
            r25 = 0
            r33 = 1
            goto L_0x01b6
        L_0x0219:
            r2 = 0
            if (r13 <= 0) goto L_0x03b8
            r7 = r24
            r12.extrapolate(r2, r13, r15, r7)
            r9 = r20
            r8 = r48
            r12.rescale(r8, r7, r9)
            r3 = r30
            r0 = 0
        L_0x022b:
            int r1 = r12.mainSetDimension
            if (r0 >= r1) goto L_0x0244
            r5 = r7[r0]
            r1 = r15[r2]
            r10 = r1[r0]
            double r5 = r5 - r10
            double r1 = org.apache.commons.math3.util.FastMath.abs(r5)
            r5 = r9[r0]
            double r1 = r1 / r5
            double r1 = r1 * r1
            double r3 = r3 + r1
            int r0 = r0 + 1
            r2 = 0
            goto L_0x022b
        L_0x0244:
            int r0 = r12.mainSetDimension
            double r0 = (double) r0
            java.lang.Double.isNaN(r0)
            double r3 = r3 / r0
            double r0 = org.apache.commons.math3.util.FastMath.sqrt(r3)
            r2 = 4831355200913801216(0x430c6bf526340000, double:1.0E15)
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 > 0) goto L_0x039a
            r2 = 1
            if (r13 <= r2) goto L_0x0261
            int r3 = (r0 > r40 ? 1 : (r0 == r40 ? 0 : -1))
            if (r3 <= 0) goto L_0x0261
            goto L_0x039a
        L_0x0261:
            r3 = 4616189618054758400(0x4010000000000000, double:4.0)
            double r3 = r3 * r0
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r40 = org.apache.commons.math3.util.FastMath.max(r3, r5)
            int r3 = r13 * 2
            int r3 = r3 + r2
            double r2 = (double) r3
            java.lang.Double.isNaN(r2)
            double r2 = r5 / r2
            double r10 = r12.stepControl2
            double r5 = r12.stepControl1
            double r4 = r0 / r5
            double r4 = org.apache.commons.math3.util.FastMath.pow(r4, r2)
            double r10 = r10 / r4
            double r4 = r12.stepControl3
            double r2 = org.apache.commons.math3.util.FastMath.pow(r4, r2)
            double r4 = r12.stepControl4
            double r4 = r2 / r4
            r42 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r2 = r42 / r2
            double r2 = org.apache.commons.math3.util.FastMath.min(r2, r10)
            double r2 = org.apache.commons.math3.util.FastMath.max(r4, r2)
            double[] r4 = r12.optimalStep
            double r5 = r12.stepSize
            double r5 = r5 * r2
            r2 = 1
            double r5 = r12.filterStep(r5, r14, r2)
            double r2 = org.apache.commons.math3.util.FastMath.abs(r5)
            r4[r13] = r2
            double[] r2 = r12.costPerTimeUnit
            int[] r3 = r12.costPerStep
            r3 = r3[r13]
            double r3 = (double) r3
            double[] r5 = r12.optimalStep
            r10 = r5[r13]
            java.lang.Double.isNaN(r3)
            double r3 = r3 / r10
            r2[r13] = r3
            int r3 = r13 - r34
            r4 = -1
            if (r3 == r4) goto L_0x0339
            if (r3 == 0) goto L_0x02f4
            r5 = 1
            if (r3 == r5) goto L_0x02d0
            if (r23 != 0) goto L_0x02c7
            boolean r2 = r12.isLastStep
            if (r2 == 0) goto L_0x0389
        L_0x02c7:
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r2 > 0) goto L_0x0389
            r0 = r34
            goto L_0x02fd
        L_0x02d0:
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r3 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            r10 = r34
            if (r3 <= 0) goto L_0x02fc
            r0 = 1
            if (r10 <= r0) goto L_0x02ec
            int r0 = r10 + -1
            r0 = r2[r0]
            double r5 = r12.orderControl1
            r24 = r2[r10]
            double r5 = r5 * r24
            int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r2 >= 0) goto L_0x02ec
            int r0 = r10 + -1
            goto L_0x02ed
        L_0x02ec:
            r0 = r10
        L_0x02ed:
            double[] r1 = r12.optimalStep
            r38 = r1[r0]
            r33 = 1
            goto L_0x02fd
        L_0x02f4:
            r10 = r34
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r3 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r3 > 0) goto L_0x0301
        L_0x02fc:
            r0 = r10
        L_0x02fd:
            r25 = 0
            goto L_0x038b
        L_0x0301:
            int[] r3 = r12.sequence
            int r5 = r13 + 1
            r5 = r3[r5]
            double r5 = (double) r5
            r11 = 0
            r3 = r3[r11]
            double r3 = (double) r3
            java.lang.Double.isNaN(r5)
            java.lang.Double.isNaN(r3)
            double r5 = r5 / r3
            double r5 = r5 * r5
            int r3 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x0337
            r0 = 1
            if (r10 <= r0) goto L_0x032d
            int r0 = r10 + -1
            r0 = r2[r0]
            double r3 = r12.orderControl1
            r5 = r2[r10]
            double r3 = r3 * r5
            int r2 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r2 >= 0) goto L_0x032d
            int r0 = r10 + -1
            goto L_0x032e
        L_0x032d:
            r0 = r10
        L_0x032e:
            double[] r1 = r12.optimalStep
            r38 = r1[r0]
        L_0x0332:
            r25 = 0
            r33 = 1
            goto L_0x038b
        L_0x0337:
            r0 = r10
            goto L_0x038b
        L_0x0339:
            r10 = r34
            r3 = 1
            if (r10 <= r3) goto L_0x0387
            if (r32 != 0) goto L_0x0387
            r3 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r5 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r5 > 0) goto L_0x0347
            goto L_0x02fc
        L_0x0347:
            int[] r3 = r12.sequence
            r4 = r3[r10]
            double r4 = (double) r4
            int r6 = r10 + 1
            r6 = r3[r6]
            r34 = r10
            double r10 = (double) r6
            java.lang.Double.isNaN(r4)
            java.lang.Double.isNaN(r10)
            double r4 = r4 * r10
            r6 = 0
            r10 = r3[r6]
            r3 = r3[r6]
            int r10 = r10 * r3
            double r10 = (double) r10
            java.lang.Double.isNaN(r10)
            double r4 = r4 / r10
            double r4 = r4 * r4
            int r3 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r3 <= 0) goto L_0x0389
            r0 = 1
            if (r13 <= r0) goto L_0x0381
            int r0 = r13 + -1
            r0 = r2[r0]
            double r3 = r12.orderControl1
            r5 = r2[r13]
            double r3 = r3 * r5
            int r2 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r2 >= 0) goto L_0x0381
            int r0 = r13 + -1
            goto L_0x0382
        L_0x0381:
            r0 = r13
        L_0x0382:
            double[] r1 = r12.optimalStep
            r38 = r1[r0]
            goto L_0x0332
        L_0x0387:
            r34 = r10
        L_0x0389:
            r0 = r34
        L_0x038b:
            r24 = r7
            r11 = r8
            r20 = r9
            r6 = r13
            r10 = r14
            r13 = r46
            r14 = r47
            r9 = -1
            r8 = r0
            goto L_0x01b6
        L_0x039a:
            double r0 = r12.stepSize
            double r2 = r12.stabilityReduction
            double r0 = r0 * r2
            r2 = 0
            double r0 = r12.filterStep(r0, r14, r2)
            double r38 = org.apache.commons.math3.util.FastMath.abs(r0)
            r24 = r7
            r11 = r8
            r20 = r9
            r6 = r13
            r10 = r14
            r8 = r34
            r13 = r46
            r14 = r47
            goto L_0x0213
        L_0x03b8:
            r6 = r13
            r10 = r14
            r8 = r34
            r13 = r46
            r14 = r47
            r11 = r48
            r9 = -1
            goto L_0x01b6
        L_0x03c5:
            r34 = r8
            r8 = r11
            r46 = r13
            r47 = r14
            r9 = r20
            r7 = r24
            r14 = r10
            if (r33 != 0) goto L_0x03de
            double r0 = r12.stepStart
            double r2 = r12.stepSize
            double r0 = r0 + r2
            r10 = r29
            r12.computeDerivatives(r0, r7, r10)
            goto L_0x03e0
        L_0x03de:
            r10 = r29
        L_0x03e0:
            double r0 = r49.getMaxStep()
            if (r33 != 0) goto L_0x0518
            r2 = 1
        L_0x03e7:
            if (r2 > r6) goto L_0x03f4
            r3 = 0
            r4 = r17[r3]
            r11 = r46
            r12.extrapolate(r3, r2, r11, r4)
            int r2 = r2 + 1
            goto L_0x03e7
        L_0x03f4:
            r11 = r46
            int r2 = r6 * 2
            int r3 = r12.mudif
            int r2 = r2 - r3
            int r2 = r2 + 3
            r3 = 0
        L_0x03fe:
            if (r3 >= r2) goto L_0x04cc
            int r4 = r3 / 2
            int[] r5 = r12.sequence
            r5 = r5[r4]
            r23 = r0
            double r0 = (double) r5
            java.lang.Double.isNaN(r0)
            double r0 = r0 * r21
            double r0 = org.apache.commons.math3.util.FastMath.pow(r0, r3)
            r5 = r19[r4]
            int r5 = r5.length
            r13 = 2
            int r5 = r5 / r13
            r25 = r14
            r20 = r15
            r13 = r47
            r15 = 0
        L_0x041e:
            int r14 = r13.length
            if (r15 >= r14) goto L_0x0434
            int r14 = r3 + 1
            r14 = r17[r14]
            r29 = r19[r4]
            int r44 = r5 + r3
            r29 = r29[r44]
            r44 = r29[r15]
            double r44 = r44 * r0
            r14[r15] = r44
            int r15 = r15 + 1
            goto L_0x041e
        L_0x0434:
            r0 = 1
        L_0x0435:
            int r1 = r6 - r4
            if (r0 > r1) goto L_0x0479
            int[] r1 = r12.sequence
            int r5 = r0 + r4
            r1 = r1[r5]
            double r14 = (double) r1
            java.lang.Double.isNaN(r14)
            double r14 = r14 * r21
            double r14 = org.apache.commons.math3.util.FastMath.pow(r14, r3)
            r1 = r19[r5]
            int r1 = r1.length
            r26 = 2
            int r1 = r1 / 2
            r48 = r8
            r29 = r10
            r8 = 0
        L_0x0455:
            int r10 = r13.length
            if (r8 >= r10) goto L_0x046b
            int r10 = r0 + -1
            r10 = r11[r10]
            r44 = r19[r5]
            int r45 = r1 + r3
            r44 = r44[r45]
            r45 = r44[r8]
            double r45 = r45 * r14
            r10[r8] = r45
            int r8 = r8 + 1
            goto L_0x0455
        L_0x046b:
            int r1 = r3 + 1
            r1 = r17[r1]
            r12.extrapolate(r4, r0, r11, r1)
            int r0 = r0 + 1
            r10 = r29
            r8 = r48
            goto L_0x0435
        L_0x0479:
            r48 = r8
            r29 = r10
            r0 = 0
        L_0x047e:
            int r1 = r13.length
            if (r0 >= r1) goto L_0x0490
            int r1 = r3 + 1
            r1 = r17[r1]
            r4 = r1[r0]
            double r14 = r12.stepSize
            double r4 = r4 * r14
            r1[r0] = r4
            int r0 = r0 + 1
            goto L_0x047e
        L_0x0490:
            int r3 = r3 + 1
            int r0 = r3 / 2
        L_0x0494:
            if (r0 > r6) goto L_0x04be
            r1 = r19[r0]
            int r1 = r1.length
            r4 = 1
            int r1 = r1 - r4
        L_0x049b:
            int r4 = r3 * 2
            if (r1 < r4) goto L_0x04bb
            r4 = 0
        L_0x04a0:
            int r5 = r13.length
            if (r4 >= r5) goto L_0x04b8
            r5 = r19[r0]
            r5 = r5[r1]
            r14 = r5[r4]
            r8 = r19[r0]
            int r10 = r1 + -2
            r8 = r8[r10]
            r44 = r8[r4]
            double r14 = r14 - r44
            r5[r4] = r14
            int r4 = r4 + 1
            goto L_0x04a0
        L_0x04b8:
            int r1 = r1 + -1
            goto L_0x049b
        L_0x04bb:
            int r0 = r0 + 1
            goto L_0x0494
        L_0x04be:
            r47 = r13
            r15 = r20
            r0 = r23
            r14 = r25
            r10 = r29
            r8 = r48
            goto L_0x03fe
        L_0x04cc:
            r23 = r0
            r48 = r8
            r29 = r10
            r25 = r14
            r20 = r15
            r13 = r47
            if (r2 < 0) goto L_0x0526
            r0 = r36
            org.apache.commons.math3.ode.nonstiff.GraggBulirschStoerStepInterpolator r0 = (org.apache.commons.math3.ode.nonstiff.GraggBulirschStoerStepInterpolator) r0
            double r3 = r12.stepSize
            r0.computeCoefficients(r2, r3)
            boolean r1 = r12.useInterpolationError
            if (r1 == 0) goto L_0x0526
            double r0 = r0.estimateError(r9)
            double r3 = r12.stepSize
            int r2 = r2 + 4
            double r14 = (double) r2
            java.lang.Double.isNaN(r14)
            r23 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r14 = r23 / r14
            double r14 = org.apache.commons.math3.util.FastMath.pow(r0, r14)
            r10 = r9
            r8 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
            double r8 = org.apache.commons.math3.util.FastMath.max(r14, r8)
            double r3 = r3 / r8
            double r2 = org.apache.commons.math3.util.FastMath.abs(r3)
            r4 = 4621819117588971520(0x4024000000000000, double:10.0)
            int r8 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r8 <= 0) goto L_0x0516
            r8 = r2
            r38 = r8
            r33 = 1
            goto L_0x0529
        L_0x0516:
            r8 = r2
            goto L_0x0529
        L_0x0518:
            r23 = r0
            r48 = r8
            r29 = r10
            r25 = r14
            r20 = r15
            r11 = r46
            r13 = r47
        L_0x0526:
            r10 = r9
            r8 = r23
        L_0x0529:
            if (r33 != 0) goto L_0x0663
            double r0 = r12.stepStart
            double r2 = r12.stepSize
            double r0 = r0 + r2
            r14 = r36
            r14.storeTime(r0)
            r0 = r49
            r1 = r14
            r2 = r7
            r3 = r29
            r4 = r51
            double r0 = r0.acceptStep(r1, r2, r3, r4)
            r12.stepStart = r0
            double r0 = r12.stepStart
            r14.storeTime(r0)
            int r0 = r13.length
            r1 = r48
            r2 = 0
            java.lang.System.arraycopy(r7, r2, r1, r2, r0)
            int r0 = r13.length
            r4 = r29
            r3 = r35
            java.lang.System.arraycopy(r4, r2, r3, r2, r0)
            r15 = 1
            if (r6 != r15) goto L_0x056c
            r35 = r3
            r29 = r4
            r16 = r10
            r46 = r11
            r0 = r34
            if (r32 == 0) goto L_0x0568
            r2 = 1
            goto L_0x0569
        L_0x0568:
            r2 = 2
        L_0x0569:
            r3 = 2
            goto L_0x05e0
        L_0x056c:
            r0 = r34
            if (r6 > r0) goto L_0x05a8
            double[] r2 = r12.costPerTimeUnit
            int r5 = r6 + -1
            r23 = r2[r5]
            r35 = r3
            r29 = r4
            double r3 = r12.orderControl1
            r36 = r2[r6]
            double r3 = r3 * r36
            int r16 = (r23 > r3 ? 1 : (r23 == r3 ? 0 : -1))
            if (r16 >= 0) goto L_0x058a
            r2 = r5
            r16 = r10
            r46 = r11
            goto L_0x0569
        L_0x058a:
            r3 = r2[r6]
            r16 = r10
            r46 = r11
            double r10 = r12.orderControl2
            r23 = r2[r5]
            double r10 = r10 * r23
            int r2 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r2 >= 0) goto L_0x05a6
            int r2 = r6 + 1
            int[] r3 = r12.sequence
            int r3 = r3.length
            r4 = 2
            int r3 = r3 - r4
            int r2 = org.apache.commons.math3.util.FastMath.min(r2, r3)
            goto L_0x0569
        L_0x05a6:
            r2 = r6
            goto L_0x0569
        L_0x05a8:
            r35 = r3
            r29 = r4
            r16 = r10
            r46 = r11
            r4 = 2
            int r2 = r6 + -1
            if (r6 <= r4) goto L_0x05c9
            double[] r3 = r12.costPerTimeUnit
            int r4 = r6 + -2
            r10 = r3[r4]
            r18 = r4
            double r4 = r12.orderControl1
            r23 = r3[r2]
            double r4 = r4 * r23
            int r3 = (r10 > r4 ? 1 : (r10 == r4 ? 0 : -1))
            if (r3 >= 0) goto L_0x05c9
            r2 = r18
        L_0x05c9:
            double[] r3 = r12.costPerTimeUnit
            r4 = r3[r6]
            double r10 = r12.orderControl2
            r23 = r3[r2]
            double r10 = r10 * r23
            int r3 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r3 >= 0) goto L_0x0569
            int[] r2 = r12.sequence
            int r2 = r2.length
            r3 = 2
            int r2 = r2 - r3
            int r2 = org.apache.commons.math3.util.FastMath.min(r6, r2)
        L_0x05e0:
            if (r32 == 0) goto L_0x05fc
            int r2 = org.apache.commons.math3.util.FastMath.min(r2, r6)
            double r4 = r12.stepSize
            double r4 = org.apache.commons.math3.util.FastMath.abs(r4)
            double[] r0 = r12.optimalStep
            r10 = r0[r2]
            double r4 = org.apache.commons.math3.util.FastMath.min(r4, r10)
            r18 = r2
            r38 = r4
            r10 = r25
            r2 = 0
            goto L_0x065b
        L_0x05fc:
            if (r2 > r6) goto L_0x0608
            double[] r0 = r12.optimalStep
            r4 = r0[r2]
            r18 = r2
            r10 = r25
        L_0x0606:
            r2 = 0
            goto L_0x0659
        L_0x0608:
            if (r6 >= r0) goto L_0x063b
            double[] r0 = r12.costPerTimeUnit
            r4 = r0[r6]
            double r10 = r12.orderControl2
            int r18 = r6 + -1
            r23 = r0[r18]
            double r10 = r10 * r23
            int r0 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x063b
            double[] r0 = r12.optimalStep
            r4 = r0[r6]
            int[] r0 = r12.costPerStep
            int r10 = r2 + 1
            r10 = r0[r10]
            double r10 = (double) r10
            java.lang.Double.isNaN(r10)
            double r4 = r4 * r10
            r0 = r0[r6]
            double r10 = (double) r0
            java.lang.Double.isNaN(r10)
            double r4 = r4 / r10
            r10 = r25
            r0 = 0
            double r4 = r12.filterStep(r4, r10, r0)
            r18 = r2
            goto L_0x0606
        L_0x063b:
            r10 = r25
            double[] r0 = r12.optimalStep
            r4 = r0[r6]
            int[] r0 = r12.costPerStep
            r11 = r0[r2]
            r18 = r2
            double r2 = (double) r11
            java.lang.Double.isNaN(r2)
            double r4 = r4 * r2
            r0 = r0[r6]
            double r2 = (double) r0
            java.lang.Double.isNaN(r2)
            double r4 = r4 / r2
            r2 = 0
            double r4 = r12.filterStep(r4, r10, r2)
        L_0x0659:
            r38 = r4
        L_0x065b:
            r25 = r18
            r3 = r38
            r0 = 1
            r18 = 1
            goto L_0x0677
        L_0x0663:
            r16 = r10
            r46 = r11
            r10 = r25
            r0 = r34
            r14 = r36
            r1 = r48
            r2 = 0
            r15 = 1
            r25 = r0
            r0 = r37
            r3 = r38
        L_0x0677:
            double r3 = org.apache.commons.math3.util.FastMath.min(r3, r8)
            if (r10 != 0) goto L_0x067e
            double r3 = -r3
        L_0x067e:
            if (r33 == 0) goto L_0x0685
            r12.isLastStep = r2
            r32 = 1
            goto L_0x0687
        L_0x0685:
            r32 = 0
        L_0x0687:
            boolean r5 = r12.isLastStep
            if (r5 == 0) goto L_0x0699
            double r2 = r12.stepStart
            r5 = r50
            r5.setTime(r2)
            r5.setCompleteState(r1)
            r49.resetInternalState()
            return
        L_0x0699:
            r5 = r50
            r11 = r1
            r1 = r3
            r24 = r7
            r9 = r13
            r8 = r14
            r15 = r20
            r14 = r35
            r33 = r40
            r13 = r46
            r23 = 0
            r20 = r16
            goto L_0x013c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.ode.nonstiff.GraggBulirschStoerIntegrator.integrate(org.apache.commons.math3.ode.ExpandableStatefulODE, double):void");
    }
}
