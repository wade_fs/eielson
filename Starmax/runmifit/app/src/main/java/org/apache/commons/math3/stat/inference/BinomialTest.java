package org.apache.commons.math3.stat.inference;

public class BinomialTest {
    public boolean binomialTest(int i, int i2, double d, AlternativeHypothesis alternativeHypothesis, double d2) {
        return binomialTest(i, i2, d, alternativeHypothesis) < d2;
    }

    /* renamed from: org.apache.commons.math3.stat.inference.BinomialTest$1 */
    static /* synthetic */ class C36111 {

        /* renamed from: $SwitchMap$org$apache$commons$math3$stat$inference$AlternativeHypothesis */
        static final /* synthetic */ int[] f8186xf16bff9 = new int[AlternativeHypothesis.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                org.apache.commons.math3.stat.inference.AlternativeHypothesis[] r0 = org.apache.commons.math3.stat.inference.AlternativeHypothesis.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.stat.inference.BinomialTest.C36111.f8186xf16bff9 = r0
                int[] r0 = org.apache.commons.math3.stat.inference.BinomialTest.C36111.f8186xf16bff9     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.stat.inference.AlternativeHypothesis r1 = org.apache.commons.math3.stat.inference.AlternativeHypothesis.GREATER_THAN     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.apache.commons.math3.stat.inference.BinomialTest.C36111.f8186xf16bff9     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.stat.inference.AlternativeHypothesis r1 = org.apache.commons.math3.stat.inference.AlternativeHypothesis.LESS_THAN     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = org.apache.commons.math3.stat.inference.BinomialTest.C36111.f8186xf16bff9     // Catch:{ NoSuchFieldError -> 0x002a }
                org.apache.commons.math3.stat.inference.AlternativeHypothesis r1 = org.apache.commons.math3.stat.inference.AlternativeHypothesis.TWO_SIDED     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.stat.inference.BinomialTest.C36111.<clinit>():void");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x004f A[EDGE_INSN: B:41:0x004f->B:24:0x004f ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public double binomialTest(int r9, int r10, double r11, org.apache.commons.math3.stat.inference.AlternativeHypothesis r13) {
        /*
            r8 = this;
            if (r9 < 0) goto L_0x00a8
            if (r10 < 0) goto L_0x009e
            r0 = 0
            r2 = 0
            r3 = 1
            int r4 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r4 < 0) goto L_0x008c
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r6 = (r11 > r4 ? 1 : (r11 == r4 ? 0 : -1))
            if (r6 > 0) goto L_0x008c
            r6 = 2
            if (r9 < r10) goto L_0x0076
            if (r13 == 0) goto L_0x0070
            org.apache.commons.math3.distribution.BinomialDistribution r7 = new org.apache.commons.math3.distribution.BinomialDistribution
            r7.<init>(r9, r11)
            int[] r11 = org.apache.commons.math3.stat.inference.BinomialTest.C36111.f8186xf16bff9
            int r12 = r13.ordinal()
            r11 = r11[r12]
            if (r11 == r3) goto L_0x0069
            if (r11 == r6) goto L_0x0064
            r12 = 3
            if (r11 != r12) goto L_0x0050
        L_0x002b:
            double r11 = r7.probability(r2)
            double r3 = r7.probability(r9)
            int r13 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r13 != 0) goto L_0x0041
            r3 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r11 = r11 * r3
            double r0 = r0 + r11
            int r2 = r2 + 1
        L_0x003e:
            int r9 = r9 + -1
            goto L_0x004b
        L_0x0041:
            int r13 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r13 >= 0) goto L_0x0049
            double r0 = r0 + r11
            int r2 = r2 + 1
            goto L_0x004b
        L_0x0049:
            double r0 = r0 + r3
            goto L_0x003e
        L_0x004b:
            if (r2 > r10) goto L_0x004f
            if (r9 >= r10) goto L_0x002b
        L_0x004f:
            return r0
        L_0x0050:
            org.apache.commons.math3.exception.MathInternalError r9 = new org.apache.commons.math3.exception.MathInternalError
            org.apache.commons.math3.exception.util.LocalizedFormats r10 = org.apache.commons.math3.exception.util.LocalizedFormats.OUT_OF_RANGE_SIMPLE
            java.lang.Object[] r11 = new java.lang.Object[r12]
            r11[r2] = r13
            org.apache.commons.math3.stat.inference.AlternativeHypothesis r12 = org.apache.commons.math3.stat.inference.AlternativeHypothesis.TWO_SIDED
            r11[r3] = r12
            org.apache.commons.math3.stat.inference.AlternativeHypothesis r12 = org.apache.commons.math3.stat.inference.AlternativeHypothesis.LESS_THAN
            r11[r6] = r12
            r9.<init>(r10, r11)
            throw r9
        L_0x0064:
            double r9 = r7.cumulativeProbability(r10)
            return r9
        L_0x0069:
            int r10 = r10 - r3
            double r9 = r7.cumulativeProbability(r10)
            double r4 = r4 - r9
            return r4
        L_0x0070:
            org.apache.commons.math3.exception.NullArgumentException r9 = new org.apache.commons.math3.exception.NullArgumentException
            r9.<init>()
            throw r9
        L_0x0076:
            org.apache.commons.math3.exception.MathIllegalArgumentException r11 = new org.apache.commons.math3.exception.MathIllegalArgumentException
            org.apache.commons.math3.exception.util.LocalizedFormats r12 = org.apache.commons.math3.exception.util.LocalizedFormats.BINOMIAL_INVALID_PARAMETERS_ORDER
            java.lang.Object[] r13 = new java.lang.Object[r6]
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r13[r2] = r9
            java.lang.Integer r9 = java.lang.Integer.valueOf(r10)
            r13[r3] = r9
            r11.<init>(r12, r13)
            throw r11
        L_0x008c:
            org.apache.commons.math3.exception.OutOfRangeException r9 = new org.apache.commons.math3.exception.OutOfRangeException
            java.lang.Double r10 = java.lang.Double.valueOf(r11)
            java.lang.Integer r11 = java.lang.Integer.valueOf(r2)
            java.lang.Integer r12 = java.lang.Integer.valueOf(r3)
            r9.<init>(r10, r11, r12)
            throw r9
        L_0x009e:
            org.apache.commons.math3.exception.NotPositiveException r9 = new org.apache.commons.math3.exception.NotPositiveException
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            r9.<init>(r10)
            throw r9
        L_0x00a8:
            org.apache.commons.math3.exception.NotPositiveException r10 = new org.apache.commons.math3.exception.NotPositiveException
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r10.<init>(r9)
            goto L_0x00b3
        L_0x00b2:
            throw r10
        L_0x00b3:
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.stat.inference.BinomialTest.binomialTest(int, int, double, org.apache.commons.math3.stat.inference.AlternativeHypothesis):double");
    }
}
