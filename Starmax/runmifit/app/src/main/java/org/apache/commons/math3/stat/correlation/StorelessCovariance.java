package org.apache.commons.math3.stat.correlation;

import java.lang.reflect.Array;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MathUnsupportedOperationException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class StorelessCovariance extends Covariance {
    private StorelessBivariateCovariance[] covMatrix;
    private int dimension;

    public StorelessCovariance(int i) {
        this(i, true);
    }

    public StorelessCovariance(int i, boolean z) {
        this.dimension = i;
        int i2 = this.dimension;
        this.covMatrix = new StorelessBivariateCovariance[((i2 * (i2 + 1)) / 2)];
        initializeMatrix(z);
    }

    private void initializeMatrix(boolean z) {
        for (int i = 0; i < this.dimension; i++) {
            for (int i2 = 0; i2 < this.dimension; i2++) {
                setElement(i, i2, new StorelessBivariateCovariance(z));
            }
        }
    }

    private int indexOf(int i, int i2) {
        return i2 < i ? ((i * (i + 1)) / 2) + i2 : i + ((i2 * (i2 + 1)) / 2);
    }

    private StorelessBivariateCovariance getElement(int i, int i2) {
        return this.covMatrix[indexOf(i, i2)];
    }

    private void setElement(int i, int i2, StorelessBivariateCovariance storelessBivariateCovariance) {
        this.covMatrix[indexOf(i, i2)] = storelessBivariateCovariance;
    }

    public double getCovariance(int i, int i2) throws NumberIsTooSmallException {
        return getElement(i, i2).getResult();
    }

    public void increment(double[] dArr) throws DimensionMismatchException {
        int length = dArr.length;
        int i = this.dimension;
        if (length == i) {
            for (int i2 = 0; i2 < length; i2++) {
                for (int i3 = i2; i3 < length; i3++) {
                    getElement(i2, i3).increment(dArr[i2], dArr[i3]);
                }
            }
            return;
        }
        throw new DimensionMismatchException(length, i);
    }

    public void append(StorelessCovariance storelessCovariance) throws DimensionMismatchException {
        int i = storelessCovariance.dimension;
        int i2 = this.dimension;
        if (i == i2) {
            for (int i3 = 0; i3 < this.dimension; i3++) {
                for (int i4 = i3; i4 < this.dimension; i4++) {
                    getElement(i3, i4).append(storelessCovariance.getElement(i3, i4));
                }
            }
            return;
        }
        throw new DimensionMismatchException(i, i2);
    }

    public RealMatrix getCovarianceMatrix() throws NumberIsTooSmallException {
        return MatrixUtils.createRealMatrix(getData());
    }

    public double[][] getData() throws NumberIsTooSmallException {
        int i = this.dimension;
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, i);
        for (int i2 = 0; i2 < this.dimension; i2++) {
            for (int i3 = 0; i3 < this.dimension; i3++) {
                dArr[i2][i3] = getElement(i2, i3).getResult();
            }
        }
        return dArr;
    }

    public int getN() throws MathUnsupportedOperationException {
        throw new MathUnsupportedOperationException();
    }
}
