package org.apache.commons.math3.distribution;

import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well19937c;
import org.apache.commons.math3.special.Beta;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.commons.math3.util.FastMath;

public class PascalDistribution extends AbstractIntegerDistribution {
    private static final long serialVersionUID = 6751309484392813623L;
    private final double log1mProbabilityOfSuccess;
    private final double logProbabilityOfSuccess;
    private final int numberOfSuccesses;
    private final double probabilityOfSuccess;

    public int getSupportLowerBound() {
        return 0;
    }

    public int getSupportUpperBound() {
        return Integer.MAX_VALUE;
    }

    public boolean isSupportConnected() {
        return true;
    }

    public PascalDistribution(int i, double d) throws NotStrictlyPositiveException, OutOfRangeException {
        this(new Well19937c(), i, d);
    }

    public PascalDistribution(RandomGenerator randomGenerator, int i, double d) throws NotStrictlyPositiveException, OutOfRangeException {
        super(randomGenerator);
        if (i <= 0) {
            throw new NotStrictlyPositiveException(LocalizedFormats.NUMBER_OF_SUCCESSES, Integer.valueOf(i));
        } else if (d < 0.0d || d > 1.0d) {
            throw new OutOfRangeException(Double.valueOf(d), 0, 1);
        } else {
            this.numberOfSuccesses = i;
            this.probabilityOfSuccess = d;
            this.logProbabilityOfSuccess = FastMath.log(d);
            this.log1mProbabilityOfSuccess = FastMath.log1p(-d);
        }
    }

    public int getNumberOfSuccesses() {
        return this.numberOfSuccesses;
    }

    public double getProbabilityOfSuccess() {
        return this.probabilityOfSuccess;
    }

    public double probability(int i) {
        if (i < 0) {
            return 0.0d;
        }
        int i2 = this.numberOfSuccesses;
        return CombinatoricsUtils.binomialCoefficientDouble((i + i2) - 1, i2 - 1) * FastMath.pow(this.probabilityOfSuccess, this.numberOfSuccesses) * FastMath.pow(1.0d - this.probabilityOfSuccess, i);
    }

    public double logProbability(int i) {
        if (i < 0) {
            return Double.NEGATIVE_INFINITY;
        }
        int i2 = this.numberOfSuccesses;
        double binomialCoefficientLog = CombinatoricsUtils.binomialCoefficientLog((i + i2) - 1, i2 - 1);
        double d = this.logProbabilityOfSuccess;
        double d2 = (double) this.numberOfSuccesses;
        Double.isNaN(d2);
        double d3 = binomialCoefficientLog + (d * d2);
        double d4 = this.log1mProbabilityOfSuccess;
        double d5 = (double) i;
        Double.isNaN(d5);
        return d3 + (d4 * d5);
    }

    public double cumulativeProbability(int i) {
        if (i < 0) {
            return 0.0d;
        }
        double d = this.probabilityOfSuccess;
        double d2 = (double) this.numberOfSuccesses;
        double d3 = (double) i;
        Double.isNaN(d3);
        return Beta.regularizedBeta(d, d2, 1.0d + d3);
    }

    public double getNumericalMean() {
        double probabilityOfSuccess2 = getProbabilityOfSuccess();
        double numberOfSuccesses2 = (double) getNumberOfSuccesses();
        Double.isNaN(numberOfSuccesses2);
        return (numberOfSuccesses2 * (1.0d - probabilityOfSuccess2)) / probabilityOfSuccess2;
    }

    public double getNumericalVariance() {
        double probabilityOfSuccess2 = getProbabilityOfSuccess();
        double numberOfSuccesses2 = (double) getNumberOfSuccesses();
        Double.isNaN(numberOfSuccesses2);
        return (numberOfSuccesses2 * (1.0d - probabilityOfSuccess2)) / (probabilityOfSuccess2 * probabilityOfSuccess2);
    }
}
