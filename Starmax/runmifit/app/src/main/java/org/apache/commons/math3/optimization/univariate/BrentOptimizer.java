package org.apache.commons.math3.optimization.univariate;

import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.optimization.ConvergenceChecker;
import org.apache.commons.math3.util.FastMath;

@Deprecated
public class BrentOptimizer extends BaseAbstractUnivariateOptimizer {
    private static final double GOLDEN_SECTION = ((3.0d - FastMath.sqrt(5.0d)) * 0.5d);
    private static final double MIN_RELATIVE_TOLERANCE = (FastMath.ulp(1.0d) * 2.0d);
    private final double absoluteThreshold;
    private final double relativeThreshold;

    public BrentOptimizer(double d, double d2, ConvergenceChecker<UnivariatePointValuePair> convergenceChecker) {
        super(convergenceChecker);
        if (d < MIN_RELATIVE_TOLERANCE) {
            throw new NumberIsTooSmallException(Double.valueOf(d), Double.valueOf(MIN_RELATIVE_TOLERANCE), true);
        } else if (d2 > 0.0d) {
            this.relativeThreshold = d;
            this.absoluteThreshold = d2;
        } else {
            throw new NotStrictlyPositiveException(Double.valueOf(d2));
        }
    }

    public BrentOptimizer(double d, double d2) {
        this(d, d2, null);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x016f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair doOptimize() {
        /*
            r51 = this;
            r0 = r51
            org.apache.commons.math3.optimization.GoalType r1 = r51.getGoalType()
            org.apache.commons.math3.optimization.GoalType r2 = org.apache.commons.math3.optimization.GoalType.MINIMIZE
            if (r1 != r2) goto L_0x000c
            r1 = 1
            goto L_0x000d
        L_0x000c:
            r1 = 0
        L_0x000d:
            double r5 = r51.getMin()
            double r7 = r51.getStartValue()
            double r9 = r51.getMax()
            org.apache.commons.math3.optimization.ConvergenceChecker r2 = r51.getConvergenceChecker()
            int r11 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r11 >= 0) goto L_0x0022
            goto L_0x0027
        L_0x0022:
            r49 = r5
            r5 = r9
            r9 = r49
        L_0x0027:
            double r11 = r0.computeObjectiveValue(r7)
            if (r1 != 0) goto L_0x002e
            double r11 = -r11
        L_0x002e:
            r13 = 0
            org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair r14 = new org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair
            if (r1 == 0) goto L_0x0035
            r3 = r11
            goto L_0x0036
        L_0x0035:
            double r3 = -r11
        L_0x0036:
            r14.<init>(r7, r3)
            r3 = r7
            r20 = r11
            r22 = r20
            r25 = r22
            r24 = r13
            r11 = r14
            r15 = 0
            r16 = 0
            r27 = 0
            r12 = r3
        L_0x0049:
            double r29 = r5 + r9
            r31 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r29 = r29 * r31
            r33 = r14
            r34 = r15
            double r14 = r0.relativeThreshold
            double r35 = org.apache.commons.math3.util.FastMath.abs(r7)
            double r14 = r14 * r35
            r35 = r1
            r36 = r2
            double r1 = r0.absoluteThreshold
            double r14 = r14 + r1
            r1 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r37 = r14 * r1
            double r39 = r7 - r29
            double r39 = org.apache.commons.math3.util.FastMath.abs(r39)
            double r41 = r9 - r5
            double r41 = r41 * r31
            double r41 = r37 - r41
            int r43 = (r39 > r41 ? 1 : (r39 == r41 ? 0 : -1))
            if (r43 > 0) goto L_0x0079
            r39 = 1
            goto L_0x007b
        L_0x0079:
            r39 = 0
        L_0x007b:
            if (r39 != 0) goto L_0x01be
            double r39 = org.apache.commons.math3.util.FastMath.abs(r16)
            int r24 = (r39 > r14 ? 1 : (r39 == r14 ? 0 : -1))
            if (r24 <= 0) goto L_0x00f7
            double r39 = r7 - r3
            double r41 = r20 - r22
            double r41 = r41 * r39
            double r43 = r7 - r12
            double r45 = r20 - r25
            double r45 = r45 * r43
            double r43 = r43 * r45
            double r39 = r39 * r41
            r47 = r12
            double r12 = r43 - r39
            double r45 = r45 - r41
            double r1 = r1 * r45
            r18 = 0
            int r24 = (r1 > r18 ? 1 : (r1 == r18 ? 0 : -1))
            if (r24 <= 0) goto L_0x00a5
            double r12 = -r12
            goto L_0x00a6
        L_0x00a5:
            double r1 = -r1
        L_0x00a6:
            double r39 = r5 - r7
            double r41 = r1 * r39
            int r24 = (r12 > r41 ? 1 : (r12 == r41 ? 0 : -1))
            if (r24 <= 0) goto L_0x00e8
            double r41 = r9 - r7
            double r41 = r41 * r1
            int r24 = (r12 > r41 ? 1 : (r12 == r41 ? 0 : -1))
            if (r24 >= 0) goto L_0x00e8
            double r41 = org.apache.commons.math3.util.FastMath.abs(r12)
            double r31 = r31 * r1
            double r31 = r31 * r16
            double r16 = org.apache.commons.math3.util.FastMath.abs(r31)
            int r24 = (r41 > r16 ? 1 : (r41 == r16 ? 0 : -1))
            if (r24 >= 0) goto L_0x00e8
            double r12 = r12 / r1
            double r1 = r7 + r12
            double r16 = r1 - r5
            int r24 = (r16 > r37 ? 1 : (r16 == r37 ? 0 : -1))
            if (r24 < 0) goto L_0x00d9
            double r1 = r9 - r1
            int r16 = (r1 > r37 ? 1 : (r1 == r37 ? 0 : -1))
            if (r16 >= 0) goto L_0x00d6
            goto L_0x00d9
        L_0x00d6:
            r16 = r27
            goto L_0x0108
        L_0x00d9:
            int r1 = (r7 > r29 ? 1 : (r7 == r29 ? 0 : -1))
            if (r1 > 0) goto L_0x00e2
            r16 = r27
            r27 = r14
            goto L_0x010a
        L_0x00e2:
            double r1 = -r14
            r16 = r27
            r27 = r1
            goto L_0x010a
        L_0x00e8:
            int r1 = (r7 > r29 ? 1 : (r7 == r29 ? 0 : -1))
            if (r1 >= 0) goto L_0x00ee
            double r39 = r9 - r7
        L_0x00ee:
            double r1 = org.apache.commons.math3.optimization.univariate.BrentOptimizer.GOLDEN_SECTION
            double r1 = r1 * r39
            r27 = r1
            r16 = r39
            goto L_0x010a
        L_0x00f7:
            r47 = r12
            int r1 = (r7 > r29 ? 1 : (r7 == r29 ? 0 : -1))
            if (r1 >= 0) goto L_0x0100
            double r1 = r9 - r7
            goto L_0x0102
        L_0x0100:
            double r1 = r5 - r7
        L_0x0102:
            double r12 = org.apache.commons.math3.optimization.univariate.BrentOptimizer.GOLDEN_SECTION
            double r12 = r12 * r1
            r16 = r1
        L_0x0108:
            r27 = r12
        L_0x010a:
            double r1 = org.apache.commons.math3.util.FastMath.abs(r27)
            int r12 = (r1 > r14 ? 1 : (r1 == r14 ? 0 : -1))
            if (r12 >= 0) goto L_0x011e
            r1 = 0
            int r12 = (r27 > r1 ? 1 : (r27 == r1 ? 0 : -1))
            if (r12 < 0) goto L_0x011b
            double r14 = r14 + r7
            r12 = r14
            goto L_0x0122
        L_0x011b:
            double r12 = r7 - r14
            goto L_0x0122
        L_0x011e:
            r1 = 0
            double r12 = r7 + r27
        L_0x0122:
            double r14 = r0.computeObjectiveValue(r12)
            if (r35 != 0) goto L_0x0129
            double r14 = -r14
        L_0x0129:
            org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair r1 = new org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair
            r29 = r5
            if (r35 == 0) goto L_0x0131
            r5 = r14
            goto L_0x0132
        L_0x0131:
            double r5 = -r14
        L_0x0132:
            r1.<init>(r12, r5)
            r2 = r35
            org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair r5 = r0.best(r11, r1, r2)
            r6 = r33
            org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair r5 = r0.best(r6, r5, r2)
            r31 = r9
            r9 = r34
            r6 = r36
            if (r36 == 0) goto L_0x0150
            boolean r10 = r6.converged(r9, r11, r1)
            if (r10 == 0) goto L_0x0150
            return r5
        L_0x0150:
            int r10 = (r14 > r20 ? 1 : (r14 == r20 ? 0 : -1))
            if (r10 > 0) goto L_0x016f
            int r10 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r10 >= 0) goto L_0x015b
            r31 = r7
            goto L_0x015d
        L_0x015b:
            r29 = r7
        L_0x015d:
            r10 = r5
            r36 = r6
            r22 = r25
            r5 = r29
            r25 = r20
            r20 = r14
            r49 = r7
            r7 = r12
            r12 = r3
            r3 = r49
            goto L_0x01b1
        L_0x016f:
            int r10 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r10 >= 0) goto L_0x0176
            r29 = r12
            goto L_0x0178
        L_0x0176:
            r31 = r12
        L_0x0178:
            int r10 = (r14 > r25 ? 1 : (r14 == r25 ? 0 : -1))
            if (r10 <= 0) goto L_0x01a3
            boolean r10 = org.apache.commons.math3.util.Precision.equals(r3, r7)
            if (r10 == 0) goto L_0x0183
            goto L_0x01a3
        L_0x0183:
            int r10 = (r14 > r22 ? 1 : (r14 == r22 ? 0 : -1))
            if (r10 <= 0) goto L_0x019b
            r10 = r5
            r36 = r6
            r5 = r47
            boolean r24 = org.apache.commons.math3.util.Precision.equals(r5, r7)
            if (r24 != 0) goto L_0x019e
            boolean r24 = org.apache.commons.math3.util.Precision.equals(r5, r3)
            if (r24 == 0) goto L_0x0199
            goto L_0x019e
        L_0x0199:
            r12 = r5
            goto L_0x01a0
        L_0x019b:
            r10 = r5
            r36 = r6
        L_0x019e:
            r22 = r14
        L_0x01a0:
            r5 = r29
            goto L_0x01b1
        L_0x01a3:
            r10 = r5
            r36 = r6
            r22 = r25
            r5 = r29
            r25 = r14
            r49 = r3
            r3 = r12
            r12 = r49
        L_0x01b1:
            int r15 = r9 + 1
            r14 = r10
            r24 = r11
            r9 = r31
            r11 = r1
            r1 = r2
            r2 = r36
            goto L_0x0049
        L_0x01be:
            r13 = r24
            r6 = r33
            r2 = r35
            org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair r1 = r0.best(r13, r11, r2)
            org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair r1 = r0.best(r6, r1, r2)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optimization.univariate.BrentOptimizer.doOptimize():org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair");
    }

    private UnivariatePointValuePair best(UnivariatePointValuePair univariatePointValuePair, UnivariatePointValuePair univariatePointValuePair2, boolean z) {
        if (univariatePointValuePair == null) {
            return univariatePointValuePair2;
        }
        if (univariatePointValuePair2 == null) {
            return univariatePointValuePair;
        }
        return z ? univariatePointValuePair.getValue() <= univariatePointValuePair2.getValue() ? univariatePointValuePair : univariatePointValuePair2 : univariatePointValuePair.getValue() >= univariatePointValuePair2.getValue() ? univariatePointValuePair : univariatePointValuePair2;
    }
}
