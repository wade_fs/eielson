package org.apache.commons.math3.optimization.direct;

import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.optimization.ConvergenceChecker;
import org.apache.commons.math3.optimization.GoalType;
import org.apache.commons.math3.optimization.MultivariateOptimizer;
import org.apache.commons.math3.optimization.PointValuePair;
import org.apache.commons.math3.optimization.univariate.BracketFinder;
import org.apache.commons.math3.optimization.univariate.BrentOptimizer;
import org.apache.commons.math3.optimization.univariate.SimpleUnivariateValueChecker;
import org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair;
import org.apache.commons.math3.util.FastMath;

@Deprecated
public class PowellOptimizer extends BaseAbstractMultivariateOptimizer<MultivariateFunction> implements MultivariateOptimizer {
    private static final double MIN_RELATIVE_TOLERANCE = (FastMath.ulp(1.0d) * 2.0d);
    private final double absoluteThreshold;
    private final LineSearch line;
    private final double relativeThreshold;

    public PowellOptimizer(double d, double d2, ConvergenceChecker<PointValuePair> convergenceChecker) {
        this(d, d2, FastMath.sqrt(d), FastMath.sqrt(d2), convergenceChecker);
    }

    public PowellOptimizer(double d, double d2, double d3, double d4, ConvergenceChecker<PointValuePair> convergenceChecker) {
        super(convergenceChecker);
        if (d < MIN_RELATIVE_TOLERANCE) {
            throw new NumberIsTooSmallException(Double.valueOf(d), Double.valueOf(MIN_RELATIVE_TOLERANCE), true);
        } else if (d2 > 0.0d) {
            this.relativeThreshold = d;
            this.absoluteThreshold = d2;
            this.line = new LineSearch(d3, d4);
        } else {
            throw new NotStrictlyPositiveException(Double.valueOf(d2));
        }
    }

    public PowellOptimizer(double d, double d2) {
        this(d, d2, null);
    }

    public PowellOptimizer(double d, double d2, double d3, double d4) {
        this(d, d2, d3, d4, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v2, resolved type: double[]} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.math3.optimization.PointValuePair doOptimize() {
        /*
            r29 = this;
            r0 = r29
            org.apache.commons.math3.optimization.GoalType r1 = r29.getGoalType()
            double[] r2 = r29.getStartPoint()
            int r3 = r2.length
            int[] r4 = new int[]{r3, r3}
            java.lang.Class<double> r5 = double.class
            java.lang.Object r4 = java.lang.reflect.Array.newInstance(r5, r4)
            double[][] r4 = (double[][]) r4
            r5 = 0
            r6 = 0
        L_0x0019:
            if (r6 >= r3) goto L_0x0024
            r7 = r4[r6]
            r8 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r7[r6] = r8
            int r6 = r6 + 1
            goto L_0x0019
        L_0x0024:
            org.apache.commons.math3.optimization.ConvergenceChecker r6 = r29.getConvergenceChecker()
            double r7 = r0.computeObjectiveValue(r2)
            java.lang.Object r9 = r2.clone()
            double[] r9 = (double[]) r9
            r10 = r9
            r9 = r2
            r2 = 0
        L_0x0035:
            r11 = 1
            int r2 = r2 + r11
            r14 = r7
            r11 = r9
            r9 = 0
            r16 = 0
            r18 = 0
        L_0x003e:
            if (r9 >= r3) goto L_0x0071
            r19 = r4[r9]
            double[] r12 = org.apache.commons.math3.util.MathArrays.copyOf(r19)
            org.apache.commons.math3.optimization.direct.PowellOptimizer$LineSearch r13 = r0.line
            org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair r13 = r13.search(r11, r12)
            double r20 = r13.getValue()
            r19 = r3
            r22 = r4
            double r3 = r13.getPoint()
            double[][] r3 = r0.newPointAndDirection(r11, r12, r3)
            r11 = r3[r5]
            double r14 = r14 - r20
            int r3 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r3 <= 0) goto L_0x0068
            r18 = r9
            r16 = r14
        L_0x0068:
            int r9 = r9 + 1
            r3 = r19
            r14 = r20
            r4 = r22
            goto L_0x003e
        L_0x0071:
            r19 = r3
            r22 = r4
            double r3 = r7 - r14
            r12 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r20 = r3 * r12
            double r12 = r0.relativeThreshold
            double r25 = org.apache.commons.math3.util.FastMath.abs(r7)
            double r27 = org.apache.commons.math3.util.FastMath.abs(r14)
            double r25 = r25 + r27
            double r12 = r12 * r25
            r25 = r6
            double r5 = r0.absoluteThreshold
            double r12 = r12 + r5
            int r5 = (r20 > r12 ? 1 : (r20 == r12 ? 0 : -1))
            if (r5 > 0) goto L_0x0094
            r5 = 1
            goto L_0x0095
        L_0x0094:
            r5 = 0
        L_0x0095:
            org.apache.commons.math3.optimization.PointValuePair r6 = new org.apache.commons.math3.optimization.PointValuePair
            r6.<init>(r10, r7)
            org.apache.commons.math3.optimization.PointValuePair r12 = new org.apache.commons.math3.optimization.PointValuePair
            r12.<init>(r11, r14)
            if (r5 != 0) goto L_0x00aa
            if (r25 == 0) goto L_0x00aa
            r13 = r25
            boolean r5 = r13.converged(r2, r6, r12)
            goto L_0x00ac
        L_0x00aa:
            r13 = r25
        L_0x00ac:
            if (r5 == 0) goto L_0x00be
            org.apache.commons.math3.optimization.GoalType r2 = org.apache.commons.math3.optimization.GoalType.MINIMIZE
            if (r1 != r2) goto L_0x00b8
            int r1 = (r14 > r7 ? 1 : (r14 == r7 ? 0 : -1))
            if (r1 >= 0) goto L_0x00b7
            r6 = r12
        L_0x00b7:
            return r6
        L_0x00b8:
            int r1 = (r14 > r7 ? 1 : (r14 == r7 ? 0 : -1))
            if (r1 <= 0) goto L_0x00bd
            r6 = r12
        L_0x00bd:
            return r6
        L_0x00be:
            r5 = r19
            double[] r6 = new double[r5]
            double[] r12 = new double[r5]
            r9 = 0
        L_0x00c5:
            if (r9 >= r5) goto L_0x00de
            r20 = r11[r9]
            r25 = r10[r9]
            double r20 = r20 - r25
            r6[r9] = r20
            r20 = r11[r9]
            r23 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r20 = r20 * r23
            r25 = r10[r9]
            double r20 = r20 - r25
            r12[r9] = r20
            int r9 = r9 + 1
            goto L_0x00c5
        L_0x00de:
            java.lang.Object r9 = r11.clone()
            r10 = r9
            double[] r10 = (double[]) r10
            double r20 = r0.computeObjectiveValue(r12)
            int r9 = (r7 > r20 ? 1 : (r7 == r20 ? 0 : -1))
            if (r9 <= 0) goto L_0x012d
            double r25 = r7 + r20
            r23 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r27 = r14 * r23
            double r25 = r25 - r27
            double r25 = r25 * r23
            double r3 = r3 - r16
            double r3 = r3 * r3
            double r25 = r25 * r3
            double r7 = r7 - r20
            double r16 = r16 * r7
            double r16 = r16 * r7
            double r25 = r25 - r16
            r3 = 0
            int r7 = (r25 > r3 ? 1 : (r25 == r3 ? 0 : -1))
            if (r7 >= 0) goto L_0x012d
            org.apache.commons.math3.optimization.direct.PowellOptimizer$LineSearch r3 = r0.line
            org.apache.commons.math3.optimization.univariate.UnivariatePointValuePair r3 = r3.search(r11, r6)
            double r7 = r3.getValue()
            double r3 = r3.getPoint()
            double[][] r3 = r0.newPointAndDirection(r11, r6, r3)
            r4 = 0
            r6 = r3[r4]
            int r9 = r5 + -1
            r11 = r22[r9]
            r22[r18] = r11
            r11 = 1
            r3 = r3[r11]
            r22[r9] = r3
            r9 = r6
            goto L_0x0130
        L_0x012d:
            r4 = 0
            r9 = r11
            r7 = r14
        L_0x0130:
            r3 = r5
            r6 = r13
            r4 = r22
            r5 = 0
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optimization.direct.PowellOptimizer.doOptimize():org.apache.commons.math3.optimization.PointValuePair");
    }

    private double[][] newPointAndDirection(double[] dArr, double[] dArr2, double d) {
        int length = dArr.length;
        double[] dArr3 = new double[length];
        double[] dArr4 = new double[length];
        for (int i = 0; i < length; i++) {
            dArr4[i] = dArr2[i] * d;
            dArr3[i] = dArr[i] + dArr4[i];
        }
        return new double[][]{dArr3, dArr4};
    }

    private class LineSearch extends BrentOptimizer {
        private static final double ABS_TOL_UNUSED = Double.MIN_VALUE;
        private static final double REL_TOL_UNUSED = 1.0E-15d;
        private final BracketFinder bracket = new BracketFinder();

        LineSearch(double d, double d2) {
            super(1.0E-15d, ABS_TOL_UNUSED, new SimpleUnivariateValueChecker(d, d2));
        }

        public UnivariatePointValuePair search(final double[] dArr, final double[] dArr2) {
            final int length = dArr.length;
            C35961 r8 = new UnivariateFunction() {
                /* class org.apache.commons.math3.optimization.direct.PowellOptimizer.LineSearch.C35961 */

                public double value(double d) {
                    double[] dArr = new double[length];
                    for (int i = 0; i < length; i++) {
                        dArr[i] = dArr[i] + (dArr2[i] * d);
                    }
                    return PowellOptimizer.this.computeObjectiveValue(dArr);
                }
            };
            GoalType goalType = PowellOptimizer.this.getGoalType();
            this.bracket.search(r8, goalType, 0.0d, 1.0d);
            return optimize(Integer.MAX_VALUE, r8, goalType, this.bracket.getLo(), this.bracket.getHi(), this.bracket.getMid());
        }
    }
}
