package org.apache.commons.math3.p067ml.distance;

import org.apache.commons.math3.util.MathArrays;

/* renamed from: org.apache.commons.math3.ml.distance.ManhattanDistance */
public class ManhattanDistance implements DistanceMeasure {
    private static final long serialVersionUID = -9108154600539125566L;

    public double compute(double[] dArr, double[] dArr2) {
        return MathArrays.distance1(dArr, dArr2);
    }
}
