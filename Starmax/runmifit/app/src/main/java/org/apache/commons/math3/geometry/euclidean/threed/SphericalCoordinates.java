package org.apache.commons.math3.geometry.euclidean.threed;

import java.io.Serializable;
import java.lang.reflect.Array;
import org.apache.commons.math3.util.FastMath;

public class SphericalCoordinates implements Serializable {
    private static final long serialVersionUID = 20130206;
    private double[][] jacobian;
    private final double phi;
    private double[][] phiHessian;

    /* renamed from: r */
    private final double f8018r;
    private double[][] rHessian;
    private final double theta;
    private double[][] thetaHessian;

    /* renamed from: v */
    private final Vector3D f8019v;

    public SphericalCoordinates(Vector3D vector3D) {
        this.f8019v = vector3D;
        this.f8018r = vector3D.getNorm();
        this.theta = vector3D.getAlpha();
        this.phi = FastMath.acos(vector3D.getZ() / this.f8018r);
    }

    public SphericalCoordinates(double d, double d2, double d3) {
        double d4 = d;
        double cos = FastMath.cos(d2);
        double sin = FastMath.sin(d2);
        double cos2 = FastMath.cos(d3);
        double sin2 = FastMath.sin(d3);
        this.f8018r = d4;
        this.theta = d2;
        this.phi = d3;
        double d5 = d4 * cos2;
        Vector3D vector3D = r11;
        Vector3D vector3D2 = new Vector3D(cos * d4 * sin2, d4 * sin * sin2, d5);
        this.f8019v = vector3D;
    }

    public Vector3D getCartesian() {
        return this.f8019v;
    }

    public double getR() {
        return this.f8018r;
    }

    public double getTheta() {
        return this.theta;
    }

    public double getPhi() {
        return this.phi;
    }

    public double[] toCartesianGradient(double[] dArr) {
        computeJacobian();
        double d = dArr[0];
        double[][] dArr2 = this.jacobian;
        return new double[]{(d * dArr2[0][0]) + (dArr[1] * dArr2[1][0]) + (dArr[2] * dArr2[2][0]), (dArr[0] * dArr2[0][1]) + (dArr[1] * dArr2[1][1]) + (dArr[2] * dArr2[2][1]), (dArr[0] * dArr2[0][2]) + (dArr[2] * dArr2[2][2])};
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v11, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v12, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v13, resolved type: double[]} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public double[][] toCartesianHessian(double[][] r19, double[] r20) {
        /*
            r18 = this;
            r0 = r18
            java.lang.Class<double> r1 = double.class
            r18.computeJacobian()
            r18.computeHessians()
            r2 = 3
            int[] r3 = new int[]{r2, r2}
            java.lang.Object r3 = java.lang.reflect.Array.newInstance(r1, r3)
            double[][] r3 = (double[][]) r3
            int[] r2 = new int[]{r2, r2}
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r1, r2)
            double[][] r1 = (double[][]) r1
            r2 = 0
            r4 = r3[r2]
            r5 = r19[r2]
            r6 = r5[r2]
            double[][] r5 = r0.jacobian
            r8 = r5[r2]
            r9 = r8[r2]
            double r6 = r6 * r9
            r8 = 1
            r9 = r19[r8]
            r10 = r9[r2]
            r9 = r5[r8]
            r12 = r9[r2]
            double r10 = r10 * r12
            double r6 = r6 + r10
            r9 = 2
            r10 = r19[r9]
            r11 = r10[r2]
            r10 = r5[r9]
            r13 = r10[r2]
            double r11 = r11 * r13
            double r6 = r6 + r11
            r4[r2] = r6
            r4 = r3[r2]
            r6 = r19[r2]
            r10 = r6[r2]
            r6 = r5[r2]
            r12 = r6[r8]
            double r10 = r10 * r12
            r6 = r19[r8]
            r12 = r6[r2]
            r6 = r5[r8]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r6 = r19[r9]
            r12 = r6[r2]
            r6 = r5[r9]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r8] = r10
            r4 = r3[r2]
            r6 = r19[r2]
            r10 = r6[r2]
            r6 = r5[r2]
            r12 = r6[r9]
            double r10 = r10 * r12
            r6 = r19[r9]
            r12 = r6[r2]
            r6 = r5[r9]
            r14 = r6[r9]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r9] = r10
            r4 = r3[r8]
            r6 = r19[r8]
            r10 = r6[r2]
            r6 = r5[r2]
            r12 = r6[r2]
            double r10 = r10 * r12
            r6 = r19[r8]
            r12 = r6[r8]
            r6 = r5[r8]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r6 = r19[r9]
            r12 = r6[r8]
            r6 = r5[r9]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r2] = r10
            r4 = r3[r8]
            r6 = r19[r8]
            r10 = r6[r2]
            r6 = r5[r2]
            r12 = r6[r8]
            double r10 = r10 * r12
            r6 = r19[r8]
            r12 = r6[r8]
            r6 = r5[r8]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r6 = r19[r9]
            r12 = r6[r8]
            r6 = r5[r9]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r8] = r10
            r4 = r3[r9]
            r6 = r19[r9]
            r10 = r6[r2]
            r6 = r5[r2]
            r12 = r6[r2]
            double r10 = r10 * r12
            r6 = r19[r9]
            r12 = r6[r8]
            r6 = r5[r8]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r6 = r19[r9]
            r12 = r6[r9]
            r6 = r5[r9]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r2] = r10
            r4 = r3[r9]
            r6 = r19[r9]
            r10 = r6[r2]
            r6 = r5[r2]
            r12 = r6[r8]
            double r10 = r10 * r12
            r6 = r19[r9]
            r12 = r6[r8]
            r6 = r5[r8]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r6 = r19[r9]
            r12 = r6[r9]
            r6 = r5[r9]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r8] = r10
            r4 = r3[r9]
            r6 = r19[r9]
            r10 = r6[r2]
            r6 = r5[r2]
            r12 = r6[r9]
            double r10 = r10 * r12
            r6 = r19[r9]
            r12 = r6[r9]
            r6 = r5[r9]
            r14 = r6[r9]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r9] = r10
            r4 = r1[r2]
            r6 = r5[r2]
            r10 = r6[r2]
            r6 = r3[r2]
            r12 = r6[r2]
            double r10 = r10 * r12
            r6 = r5[r8]
            r12 = r6[r2]
            r6 = r3[r8]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r6 = r5[r9]
            r12 = r6[r2]
            r6 = r3[r9]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r2] = r10
            r4 = r1[r8]
            r6 = r5[r2]
            r10 = r6[r8]
            r6 = r3[r2]
            r12 = r6[r2]
            double r10 = r10 * r12
            r6 = r5[r8]
            r12 = r6[r8]
            r6 = r3[r8]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r6 = r5[r9]
            r12 = r6[r8]
            r6 = r3[r9]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r2] = r10
            r4 = r1[r9]
            r6 = r5[r2]
            r10 = r6[r9]
            r6 = r3[r2]
            r12 = r6[r2]
            double r10 = r10 * r12
            r6 = r5[r9]
            r12 = r6[r9]
            r6 = r3[r9]
            r14 = r6[r2]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r2] = r10
            r4 = r1[r8]
            r6 = r5[r2]
            r10 = r6[r8]
            r6 = r3[r2]
            r12 = r6[r8]
            double r10 = r10 * r12
            r6 = r5[r8]
            r12 = r6[r8]
            r6 = r3[r8]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r6 = r5[r9]
            r12 = r6[r8]
            r6 = r3[r9]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r8] = r10
            r4 = r1[r9]
            r6 = r5[r2]
            r10 = r6[r9]
            r6 = r3[r2]
            r12 = r6[r8]
            double r10 = r10 * r12
            r6 = r5[r9]
            r12 = r6[r9]
            r6 = r3[r9]
            r14 = r6[r8]
            double r12 = r12 * r14
            double r10 = r10 + r12
            r4[r8] = r10
            r4 = r1[r9]
            r6 = r5[r2]
            r10 = r6[r9]
            r6 = r3[r2]
            r12 = r6[r9]
            double r10 = r10 * r12
            r5 = r5[r9]
            r6 = r5[r9]
            r3 = r3[r9]
            r12 = r3[r9]
            double r6 = r6 * r12
            double r10 = r10 + r6
            r4[r9] = r10
            r3 = r1[r2]
            r4 = r3[r2]
            r6 = r20[r2]
            double[][] r10 = r0.rHessian
            r11 = r10[r2]
            r12 = r11[r2]
            double r6 = r6 * r12
            r11 = r20[r8]
            double[][] r13 = r0.thetaHessian
            r14 = r13[r2]
            r15 = r14[r2]
            double r11 = r11 * r15
            double r6 = r6 + r11
            r11 = r20[r9]
            double[][] r14 = r0.phiHessian
            r15 = r14[r2]
            r16 = r15[r2]
            double r11 = r11 * r16
            double r6 = r6 + r11
            double r4 = r4 + r6
            r3[r2] = r4
            r3 = r1[r8]
            r4 = r3[r2]
            r6 = r20[r2]
            r11 = r10[r8]
            r15 = r11[r2]
            double r6 = r6 * r15
            r11 = r20[r8]
            r15 = r13[r8]
            r16 = r15[r2]
            double r11 = r11 * r16
            double r6 = r6 + r11
            r11 = r20[r9]
            r15 = r14[r8]
            r16 = r15[r2]
            double r11 = r11 * r16
            double r6 = r6 + r11
            double r4 = r4 + r6
            r3[r2] = r4
            r3 = r1[r9]
            r4 = r3[r2]
            r6 = r20[r2]
            r11 = r10[r9]
            r15 = r11[r2]
            double r6 = r6 * r15
            r11 = r20[r9]
            r15 = r14[r9]
            r16 = r15[r2]
            double r11 = r11 * r16
            double r6 = r6 + r11
            double r4 = r4 + r6
            r3[r2] = r4
            r3 = r1[r8]
            r4 = r3[r8]
            r6 = r20[r2]
            r11 = r10[r8]
            r15 = r11[r8]
            double r6 = r6 * r15
            r11 = r20[r8]
            r13 = r13[r8]
            r15 = r13[r8]
            double r11 = r11 * r15
            double r6 = r6 + r11
            r11 = r20[r9]
            r13 = r14[r8]
            r15 = r13[r8]
            double r11 = r11 * r15
            double r6 = r6 + r11
            double r4 = r4 + r6
            r3[r8] = r4
            r3 = r1[r9]
            r4 = r3[r8]
            r6 = r20[r2]
            r11 = r10[r9]
            r12 = r11[r8]
            double r6 = r6 * r12
            r11 = r20[r9]
            r13 = r14[r9]
            r15 = r13[r8]
            double r11 = r11 * r15
            double r6 = r6 + r11
            double r4 = r4 + r6
            r3[r8] = r4
            r3 = r1[r9]
            r4 = r3[r9]
            r6 = r20[r2]
            r10 = r10[r9]
            r11 = r10[r9]
            double r6 = r6 * r11
            r10 = r20[r9]
            r12 = r14[r9]
            r13 = r12[r9]
            double r10 = r10 * r13
            double r6 = r6 + r10
            double r4 = r4 + r6
            r3[r9] = r4
            r3 = r1[r2]
            r4 = r1[r8]
            r5 = r4[r2]
            r3[r8] = r5
            r3 = r1[r2]
            r4 = r1[r9]
            r5 = r4[r2]
            r3[r9] = r5
            r2 = r1[r8]
            r3 = r1[r9]
            r4 = r3[r8]
            r2[r9] = r4
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.geometry.euclidean.threed.SphericalCoordinates.toCartesianHessian(double[][], double[]):double[][]");
    }

    private void computeJacobian() {
        if (this.jacobian == null) {
            double x = this.f8019v.getX();
            double y = this.f8019v.getY();
            double z = this.f8019v.getZ();
            double d = (x * x) + (y * y);
            double sqrt = FastMath.sqrt(d);
            this.jacobian = (double[][]) Array.newInstance(double.class, 3, 3);
            double[][] dArr = this.jacobian;
            double[] dArr2 = dArr[0];
            double d2 = sqrt;
            double d3 = this.f8018r;
            dArr2[0] = x / d3;
            dArr[0][1] = y / d3;
            dArr[0][2] = z / d3;
            double d4 = (z * z) + d;
            dArr[1][0] = (-y) / d;
            dArr[1][1] = x / d;
            double d5 = d2 * d4;
            dArr[2][0] = (x * z) / d5;
            dArr[2][1] = (y * z) / d5;
            dArr[2][2] = (-d2) / d4;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    private void computeHessians() {
        Class<double> cls = double.class;
        if (this.rHessian == null) {
            double x = this.f8019v.getX();
            double y = this.f8019v.getY();
            double z = this.f8019v.getZ();
            double d = x * x;
            double d2 = y * y;
            double d3 = z * z;
            double d4 = d + d2;
            double sqrt = FastMath.sqrt(d4);
            double d5 = d4 + d3;
            double d6 = d2;
            double d7 = this.f8018r;
            double d8 = x / d4;
            double d9 = y / d4;
            double d10 = (x / d7) / d5;
            double d11 = (y / d7) / d5;
            double d12 = (z / d7) / d5;
            double d13 = d;
            this.rHessian = (double[][]) Array.newInstance((Class<?>) cls, 3, 3);
            double[][] dArr = this.rHessian;
            double d14 = y * d11;
            double d15 = z * d12;
            dArr[0][0] = d14 + d15;
            double[][] dArr2 = dArr;
            double d16 = -x;
            dArr[1][0] = d11 * d16;
            double d17 = d3;
            dArr2[2][0] = (-z) * d10;
            double d18 = d10 * x;
            dArr2[1][1] = d18 + d15;
            dArr2[2][1] = (-y) * d12;
            dArr2[2][2] = d18 + d14;
            dArr2[0][1] = dArr2[1][0];
            dArr2[0][2] = dArr2[2][0];
            dArr2[1][2] = dArr2[2][1];
            this.thetaHessian = (double[][]) Array.newInstance((Class<?>) cls, 2, 2);
            double[][] dArr3 = this.thetaHessian;
            dArr3[0][0] = d8 * 2.0d * d9;
            dArr3[1][0] = (d9 * d9) - (d8 * d8);
            dArr3[1][1] = d8 * -2.0d * d9;
            dArr3[0][1] = dArr3[1][0];
            double d19 = sqrt * d5;
            double d20 = sqrt * d19;
            double d21 = d19 * d5;
            double d22 = d21 * d4;
            double d23 = (3.0d * d4) + d17;
            this.phiHessian = (double[][]) Array.newInstance((Class<?>) cls, 3, 3);
            double[][] dArr4 = this.phiHessian;
            dArr4[0][0] = ((d20 - (d13 * d23)) * z) / d22;
            dArr4[1][0] = (((d16 * y) * z) * d23) / d22;
            double d24 = d4 - d17;
            dArr4[2][0] = (x * d24) / d21;
            dArr4[1][1] = (z * (d20 - (d6 * d23))) / d22;
            dArr4[2][1] = (y * d24) / d21;
            dArr4[2][2] = ((sqrt * 2.0d) * d12) / this.f8018r;
            dArr4[0][1] = dArr4[1][0];
            dArr4[0][2] = dArr4[2][0];
            dArr4[1][2] = dArr4[2][1];
        }
    }

    private Object writeReplace() {
        return new DataTransferObject(this.f8019v.getX(), this.f8019v.getY(), this.f8019v.getZ());
    }

    private static class DataTransferObject implements Serializable {
        private static final long serialVersionUID = 20130206;

        /* renamed from: x */
        private final double f8020x;

        /* renamed from: y */
        private final double f8021y;

        /* renamed from: z */
        private final double f8022z;

        public DataTransferObject(double d, double d2, double d3) {
            this.f8020x = d;
            this.f8021y = d2;
            this.f8022z = d3;
        }

        private Object readResolve() {
            return new SphericalCoordinates(new Vector3D(this.f8020x, this.f8021y, this.f8022z));
        }
    }
}
