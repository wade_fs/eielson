package org.apache.commons.math3.linear;

import org.apache.commons.math3.util.FastMath;

public class RRQRDecomposition extends QRDecomposition {
    private RealMatrix cachedP;

    /* renamed from: p */
    private int[] f8039p;

    public RRQRDecomposition(RealMatrix realMatrix) {
        this(realMatrix, 0.0d);
    }

    public RRQRDecomposition(RealMatrix realMatrix, double d) {
        super(realMatrix, d);
    }

    /* access modifiers changed from: protected */
    public void decompose(double[][] dArr) {
        this.f8039p = new int[dArr.length];
        int i = 0;
        while (true) {
            int[] iArr = this.f8039p;
            if (i < iArr.length) {
                iArr[i] = i;
                i++;
            } else {
                super.decompose(dArr);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void performHouseholderReflection(int i, double[][] dArr) {
        int i2 = i;
        double[][] dArr2 = dArr;
        int i3 = i2;
        int i4 = i3;
        double d = 0.0d;
        while (i3 < dArr2.length) {
            double d2 = 0.0d;
            for (int i5 = 0; i5 < dArr2[i3].length; i5++) {
                d2 += dArr2[i3][i5] * dArr2[i3][i5];
            }
            if (d2 > d) {
                i4 = i3;
                d = d2;
            }
            i3++;
        }
        if (i4 != i2) {
            double[] dArr3 = dArr2[i2];
            dArr2[i2] = dArr2[i4];
            dArr2[i4] = dArr3;
            int[] iArr = this.f8039p;
            int i6 = iArr[i2];
            iArr[i2] = iArr[i4];
            iArr[i4] = i6;
        }
        super.performHouseholderReflection(i, dArr);
    }

    public RealMatrix getP() {
        if (this.cachedP == null) {
            int length = this.f8039p.length;
            this.cachedP = MatrixUtils.createRealMatrix(length, length);
            for (int i = 0; i < length; i++) {
                this.cachedP.setEntry(this.f8039p[i], i, 1.0d);
            }
        }
        return this.cachedP;
    }

    public int getRank(double d) {
        RealMatrix r = getR();
        int rowDimension = r.getRowDimension();
        int columnDimension = r.getColumnDimension();
        double frobeniusNorm = r.getFrobeniusNorm();
        int i = 1;
        double d2 = frobeniusNorm;
        while (i < FastMath.min(rowDimension, columnDimension)) {
            double frobeniusNorm2 = r.getSubMatrix(i, rowDimension - 1, i, columnDimension - 1).getFrobeniusNorm();
            if (frobeniusNorm2 == 0.0d || (frobeniusNorm2 / d2) * frobeniusNorm < d) {
                break;
            }
            i++;
            d2 = frobeniusNorm2;
        }
        return i;
    }

    public DecompositionSolver getSolver() {
        return new Solver(super.getSolver(), getP());
    }

    private static class Solver implements DecompositionSolver {

        /* renamed from: p */
        private RealMatrix f8040p;
        private final DecompositionSolver upper;

        private Solver(DecompositionSolver decompositionSolver, RealMatrix realMatrix) {
            this.upper = decompositionSolver;
            this.f8040p = realMatrix;
        }

        public boolean isNonSingular() {
            return this.upper.isNonSingular();
        }

        public RealVector solve(RealVector realVector) {
            return this.f8040p.operate(this.upper.solve(realVector));
        }

        public RealMatrix solve(RealMatrix realMatrix) {
            return this.f8040p.multiply(this.upper.solve(realMatrix));
        }

        public RealMatrix getInverse() {
            return solve(MatrixUtils.createRealIdentityMatrix(this.f8040p.getRowDimension()));
        }
    }
}
