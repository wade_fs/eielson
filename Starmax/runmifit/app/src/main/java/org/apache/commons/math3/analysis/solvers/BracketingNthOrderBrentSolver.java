package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.exception.MathInternalError;
import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Precision;

public class BracketingNthOrderBrentSolver extends AbstractUnivariateSolver implements BracketedUnivariateSolver<UnivariateFunction> {
    private static final double DEFAULT_ABSOLUTE_ACCURACY = 1.0E-6d;
    private static final int DEFAULT_MAXIMAL_ORDER = 5;
    private static final int MAXIMAL_AGING = 2;
    private static final double REDUCTION_FACTOR = 0.0625d;
    private AllowedSolution allowed;
    private final int maximalOrder;

    public BracketingNthOrderBrentSolver() {
        this(1.0E-6d, 5);
    }

    public BracketingNthOrderBrentSolver(double d, int i) throws NumberIsTooSmallException {
        super(d);
        if (i >= 2) {
            this.maximalOrder = i;
            this.allowed = AllowedSolution.ANY_SIDE;
            return;
        }
        throw new NumberIsTooSmallException(Integer.valueOf(i), 2, true);
    }

    public BracketingNthOrderBrentSolver(double d, double d2, int i) throws NumberIsTooSmallException {
        super(d, d2);
        if (i >= 2) {
            this.maximalOrder = i;
            this.allowed = AllowedSolution.ANY_SIDE;
            return;
        }
        throw new NumberIsTooSmallException(Integer.valueOf(i), 2, true);
    }

    public BracketingNthOrderBrentSolver(double d, double d2, double d3, int i) throws NumberIsTooSmallException {
        super(d, d2, d3);
        if (i >= 2) {
            this.maximalOrder = i;
            this.allowed = AllowedSolution.ANY_SIDE;
            return;
        }
        throw new NumberIsTooSmallException(Integer.valueOf(i), 2, true);
    }

    public int getMaximalOrder() {
        return this.maximalOrder;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.Precision.equals(double, double, int):boolean
     arg types: [double, int, int]
     candidates:
      org.apache.commons.math3.util.Precision.equals(double, double, double):boolean
      org.apache.commons.math3.util.Precision.equals(float, float, float):boolean
      org.apache.commons.math3.util.Precision.equals(float, float, int):boolean
      org.apache.commons.math3.util.Precision.equals(double, double, int):boolean */
    /* access modifiers changed from: protected */
    public double doSolve() throws TooManyEvaluationsException, NumberIsTooLargeException, NoBracketingException {
        int i;
        int i2;
        double d;
        double d2;
        int i3;
        int i4;
        double d3;
        double[] dArr;
        double guessX;
        double[] dArr2;
        int i5;
        int i6 = this.maximalOrder;
        double[] dArr3 = new double[(i6 + 1)];
        double[] dArr4 = new double[(i6 + 1)];
        dArr3[0] = getMin();
        dArr3[1] = getStartValue();
        int i7 = 2;
        dArr3[2] = getMax();
        verifySequence(dArr3[0], dArr3[1], dArr3[2]);
        dArr4[1] = computeObjectiveValue(dArr3[1]);
        if (Precision.equals(dArr4[1], 0.0d, 1)) {
            return dArr3[1];
        }
        dArr4[0] = computeObjectiveValue(dArr3[0]);
        if (Precision.equals(dArr4[0], 0.0d, 1)) {
            return dArr3[0];
        }
        if (dArr4[0] * dArr4[1] < 0.0d) {
            i2 = 1;
            i = 2;
        } else {
            dArr4[2] = computeObjectiveValue(dArr3[2]);
            if (Precision.equals(dArr4[2], 0.0d, 1)) {
                return dArr3[2];
            }
            if (dArr4[1] * dArr4[2] < 0.0d) {
                i2 = 2;
                i = 3;
            } else {
                double[] dArr5 = dArr4;
                throw new NoBracketingException(dArr3[0], dArr3[2], dArr5[0], dArr5[2]);
            }
        }
        double[] dArr6 = new double[dArr3.length];
        int i8 = i2 - 1;
        double d4 = dArr3[i8];
        double d5 = dArr4[i8];
        double abs = FastMath.abs(d5);
        double d6 = dArr3[i2];
        double d7 = dArr4[i2];
        int i9 = i2;
        int i10 = i;
        double abs2 = FastMath.abs(d7);
        int i11 = 0;
        double d8 = d4;
        int i12 = 0;
        double d9 = abs;
        double d10 = d5;
        double d11 = d8;
        while (true) {
            double[] dArr7 = dArr4;
            double d12 = d6 - d11;
            if (d12 <= getAbsoluteAccuracy() + (getRelativeAccuracy() * FastMath.max(FastMath.abs(d11), FastMath.abs(d6))) || FastMath.max(d9, abs2) < getFunctionValueAccuracy()) {
                double d13 = abs2;
                double d14 = d9;
                int i13 = C34981.f7966xe6390080[this.allowed.ordinal()];
            } else {
                if (i12 >= i7) {
                    int i14 = i12 - 2;
                    double d15 = (double) ((1 << i14) - 1);
                    int i15 = i14 + 1;
                    d2 = abs2;
                    double d16 = (double) i15;
                    Double.isNaN(d15);
                    Double.isNaN(d16);
                    Double.isNaN(d15);
                    Double.isNaN(d16);
                    d = ((d15 * d10) - ((REDUCTION_FACTOR * d16) * d7)) / (d15 + d16);
                } else {
                    d2 = abs2;
                    if (i11 >= 2) {
                        int i16 = i11 - 2;
                        double d17 = (double) (i16 + 1);
                        double d18 = (double) ((1 << i16) - 1);
                        Double.isNaN(d18);
                        Double.isNaN(d17);
                        Double.isNaN(d17);
                        Double.isNaN(d18);
                        d = ((d18 * d7) - ((REDUCTION_FACTOR * d17) * d10)) / (d17 + d18);
                    } else {
                        d = 0.0d;
                    }
                }
                int i17 = i10;
                int i18 = 0;
                while (true) {
                    System.arraycopy(dArr3, i18, dArr6, i18, i17 - i18);
                    i3 = i11;
                    i4 = i12;
                    d3 = d9;
                    dArr = dArr6;
                    guessX = guessX(d, dArr6, dArr7, i18, i17);
                    if (guessX <= d11 || guessX >= d6) {
                        if (i9 - i18 >= i17 - i9) {
                            i18++;
                        } else {
                            i17--;
                        }
                        guessX = Double.NaN;
                    }
                    if (Double.isNaN(guessX) && i17 - i18 > 1) {
                        i11 = i3;
                        i12 = i4;
                        d9 = d3;
                        dArr6 = dArr;
                    }
                }
                if (Double.isNaN(guessX)) {
                    guessX = d11 + (d12 * 0.5d);
                    i18 = i9 - 1;
                    i17 = i9;
                }
                double computeObjectiveValue = computeObjectiveValue(guessX);
                if (Precision.equals(computeObjectiveValue, 0.0d, 1)) {
                    return guessX;
                }
                if (i10 <= 2 || (i5 = i17 - i18) == i10) {
                    dArr2 = dArr7;
                    if (i10 == dArr3.length) {
                        i10--;
                        if (i9 >= (dArr3.length + 1) / 2) {
                            System.arraycopy(dArr3, 1, dArr3, 0, i10);
                            System.arraycopy(dArr2, 1, dArr2, 0, i10);
                            i9--;
                        }
                    }
                } else {
                    System.arraycopy(dArr3, i18, dArr3, 0, i5);
                    dArr2 = dArr7;
                    System.arraycopy(dArr2, i18, dArr2, 0, i5);
                    i9 -= i18;
                    i10 = i5;
                }
                int i19 = i9 + 1;
                int i20 = i10 - i9;
                System.arraycopy(dArr3, i9, dArr3, i19, i20);
                dArr3[i9] = guessX;
                System.arraycopy(dArr2, i9, dArr2, i19, i20);
                dArr2[i9] = computeObjectiveValue;
                i10++;
                if (computeObjectiveValue * d10 <= 0.0d) {
                    d6 = guessX;
                    d7 = computeObjectiveValue;
                    abs2 = FastMath.abs(computeObjectiveValue);
                    i12 = i4 + 1;
                    d9 = d3;
                    i11 = 0;
                } else {
                    d11 = guessX;
                    d10 = computeObjectiveValue;
                    i9 = i19;
                    i11 = i3 + 1;
                    d9 = FastMath.abs(computeObjectiveValue);
                    abs2 = d2;
                    i12 = 0;
                }
                dArr4 = dArr2;
                dArr6 = dArr;
                i7 = 2;
            }
        }
        double d132 = abs2;
        double d142 = d9;
        int i132 = C34981.f7966xe6390080[this.allowed.ordinal()];
        if (i132 == 1) {
            return d142 < d132 ? d11 : d6;
        }
        if (i132 == 2) {
            return d11;
        }
        if (i132 == 3) {
            return d6;
        }
        if (i132 == 4) {
            return d10 <= 0.0d ? d11 : d6;
        }
        if (i132 == 5) {
            return d10 < 0.0d ? d6 : d11;
        }
        throw new MathInternalError();
    }

    /* renamed from: org.apache.commons.math3.analysis.solvers.BracketingNthOrderBrentSolver$1 */
    static /* synthetic */ class C34981 {

        /* renamed from: $SwitchMap$org$apache$commons$math3$analysis$solvers$AllowedSolution */
        static final /* synthetic */ int[] f7966xe6390080 = new int[AllowedSolution.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                org.apache.commons.math3.analysis.solvers.AllowedSolution[] r0 = org.apache.commons.math3.analysis.solvers.AllowedSolution.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.analysis.solvers.BracketingNthOrderBrentSolver.C34981.f7966xe6390080 = r0
                int[] r0 = org.apache.commons.math3.analysis.solvers.BracketingNthOrderBrentSolver.C34981.f7966xe6390080     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.ANY_SIDE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.apache.commons.math3.analysis.solvers.BracketingNthOrderBrentSolver.C34981.f7966xe6390080     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.LEFT_SIDE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = org.apache.commons.math3.analysis.solvers.BracketingNthOrderBrentSolver.C34981.f7966xe6390080     // Catch:{ NoSuchFieldError -> 0x002a }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.RIGHT_SIDE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = org.apache.commons.math3.analysis.solvers.BracketingNthOrderBrentSolver.C34981.f7966xe6390080     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.BELOW_SIDE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = org.apache.commons.math3.analysis.solvers.BracketingNthOrderBrentSolver.C34981.f7966xe6390080     // Catch:{ NoSuchFieldError -> 0x0040 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.ABOVE_SIDE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.analysis.solvers.BracketingNthOrderBrentSolver.C34981.<clinit>():void");
        }
    }

    private double guessX(double d, double[] dArr, double[] dArr2, int i, int i2) {
        int i3;
        int i4 = i;
        int i5 = i4;
        while (true) {
            i3 = i2 - 1;
            if (i5 >= i3) {
                break;
            }
            int i6 = i5 + 1;
            int i7 = i6 - i4;
            while (i3 > i5) {
                dArr[i3] = (dArr[i3] - dArr[i3 - 1]) / (dArr2[i3] - dArr2[i3 - i7]);
                i3--;
            }
            i5 = i6;
        }
        double d2 = 0.0d;
        while (i3 >= i4) {
            d2 = (d2 * (d - dArr2[i3])) + dArr[i3];
            i3--;
        }
        return d2;
    }

    public double solve(int i, UnivariateFunction univariateFunction, double d, double d2, AllowedSolution allowedSolution) throws TooManyEvaluationsException, NumberIsTooLargeException, NoBracketingException {
        this.allowed = allowedSolution;
        return super.solve(i, univariateFunction, d, d2);
    }

    public double solve(int i, UnivariateFunction univariateFunction, double d, double d2, double d3, AllowedSolution allowedSolution) throws TooManyEvaluationsException, NumberIsTooLargeException, NoBracketingException {
        this.allowed = allowedSolution;
        return super.solve(i, univariateFunction, d, d2, d3);
    }
}
