package org.apache.commons.math3.p067ml.neuralnet.sofm;

import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.p067ml.neuralnet.sofm.util.ExponentialDecayFunction;
import org.apache.commons.math3.p067ml.neuralnet.sofm.util.QuasiSigmoidDecayFunction;

/* renamed from: org.apache.commons.math3.ml.neuralnet.sofm.LearningFactorFunctionFactory */
public class LearningFactorFunctionFactory {
    private LearningFactorFunctionFactory() {
    }

    public static LearningFactorFunction exponentialDecay(double d, double d2, long j) {
        if (d <= 0.0d || d > 1.0d) {
            throw new OutOfRangeException(Double.valueOf(d), 0, 1);
        }
        final double d3 = d;
        final double d4 = d2;
        final long j2 = j;
        return new LearningFactorFunction() {
            /* class org.apache.commons.math3.p067ml.neuralnet.sofm.LearningFactorFunctionFactory.C35651 */
            private final ExponentialDecayFunction decay = new ExponentialDecayFunction(d3, d4, j2);

            public double value(long j) {
                return this.decay.value(j);
            }
        };
    }

    public static LearningFactorFunction quasiSigmoidDecay(double d, double d2, long j) {
        if (d <= 0.0d || d > 1.0d) {
            throw new OutOfRangeException(Double.valueOf(d), 0, 1);
        }
        final double d3 = d;
        final double d4 = d2;
        final long j2 = j;
        return new LearningFactorFunction() {
            /* class org.apache.commons.math3.p067ml.neuralnet.sofm.LearningFactorFunctionFactory.C35662 */
            private final QuasiSigmoidDecayFunction decay = new QuasiSigmoidDecayFunction(d3, d4, j2);

            public double value(long j) {
                return this.decay.value(j);
            }
        };
    }
}
