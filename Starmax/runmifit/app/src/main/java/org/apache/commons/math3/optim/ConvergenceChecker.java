package org.apache.commons.math3.optim;

public interface ConvergenceChecker<PAIR> {
    boolean converged(int i, Object obj, Object obj2);
}
