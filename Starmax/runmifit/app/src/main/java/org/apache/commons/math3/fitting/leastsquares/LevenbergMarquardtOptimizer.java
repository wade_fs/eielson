package org.apache.commons.math3.fitting.leastsquares;

import java.util.Arrays;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Precision;

public class LevenbergMarquardtOptimizer implements LeastSquaresOptimizer {
    private static final double TWO_EPS = (Precision.EPSILON * 2.0d);
    private final double costRelativeTolerance;
    private final double initialStepBoundFactor;
    private final double orthoTolerance;
    private final double parRelativeTolerance;
    private final double qrRankingThreshold;

    public LevenbergMarquardtOptimizer() {
        this(100.0d, 1.0E-10d, 1.0E-10d, 1.0E-10d, Precision.SAFE_MIN);
    }

    public LevenbergMarquardtOptimizer(double d, double d2, double d3, double d4, double d5) {
        this.initialStepBoundFactor = d;
        this.costRelativeTolerance = d2;
        this.parRelativeTolerance = d3;
        this.orthoTolerance = d4;
        this.qrRankingThreshold = d5;
    }

    public LevenbergMarquardtOptimizer withInitialStepBoundFactor(double d) {
        return new LevenbergMarquardtOptimizer(d, this.costRelativeTolerance, this.parRelativeTolerance, this.orthoTolerance, this.qrRankingThreshold);
    }

    public LevenbergMarquardtOptimizer withCostRelativeTolerance(double d) {
        return new LevenbergMarquardtOptimizer(this.initialStepBoundFactor, d, this.parRelativeTolerance, this.orthoTolerance, this.qrRankingThreshold);
    }

    public LevenbergMarquardtOptimizer withParameterRelativeTolerance(double d) {
        return new LevenbergMarquardtOptimizer(this.initialStepBoundFactor, this.costRelativeTolerance, d, this.orthoTolerance, this.qrRankingThreshold);
    }

    public LevenbergMarquardtOptimizer withOrthoTolerance(double d) {
        return new LevenbergMarquardtOptimizer(this.initialStepBoundFactor, this.costRelativeTolerance, this.parRelativeTolerance, d, this.qrRankingThreshold);
    }

    public LevenbergMarquardtOptimizer withRankingThreshold(double d) {
        return new LevenbergMarquardtOptimizer(this.initialStepBoundFactor, this.costRelativeTolerance, this.parRelativeTolerance, this.orthoTolerance, d);
    }

    public double getInitialStepBoundFactor() {
        return this.initialStepBoundFactor;
    }

    public double getCostRelativeTolerance() {
        return this.costRelativeTolerance;
    }

    public double getParameterRelativeTolerance() {
        return this.parRelativeTolerance;
    }

    public double getOrthoTolerance() {
        return this.orthoTolerance;
    }

    public double getRankingThreshold() {
        return this.qrRankingThreshold;
    }

    /* JADX WARNING: Removed duplicated region for block: B:102:0x02cf  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x033c  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0370 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0294  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer.Optimum optimize(org.apache.commons.math3.fitting.leastsquares.LeastSquaresProblem r63) {
        /*
            r62 = this;
            r13 = r62
            r14 = r63
            int r15 = r63.getObservationSize()
            int r11 = r63.getParameterSize()
            org.apache.commons.math3.linear.RealVector r0 = r63.getStart()
            double[] r12 = r0.toArray()
            org.apache.commons.math3.util.Incrementor r16 = r63.getIterationCounter()
            org.apache.commons.math3.util.Incrementor r17 = r63.getEvaluationCounter()
            org.apache.commons.math3.optim.ConvergenceChecker r10 = r63.getConvergenceChecker()
            int r9 = org.apache.commons.math3.util.FastMath.min(r15, r11)
            double[] r8 = new double[r11]
            double[] r7 = new double[r11]
            double[] r6 = new double[r11]
            double[] r0 = new double[r15]
            double[] r5 = new double[r15]
            double[] r4 = new double[r11]
            double[] r2 = new double[r11]
            double[] r3 = new double[r11]
            r17.incrementCount()
            org.apache.commons.math3.linear.ArrayRealVector r0 = new org.apache.commons.math3.linear.ArrayRealVector
            r0.<init>(r12)
            org.apache.commons.math3.fitting.leastsquares.LeastSquaresProblem$Evaluation r0 = r14.evaluate(r0)
            org.apache.commons.math3.linear.RealVector r1 = r0.getResiduals()
            double[] r1 = r1.toArray()
            double r18 = r0.getCost()
            r20 = 0
            r22 = r18
            r24 = r20
            r26 = r24
            r28 = r26
            r18 = 1
            r61 = r1
            r1 = r0
            r0 = r61
        L_0x005d:
            r16.incrementCount()
            org.apache.commons.math3.linear.RealMatrix r14 = r1.getJacobian()
            org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer$InternalData r14 = r13.qrDecomposition(r14, r9)
            double[][] r30 = r14.weightedJacobian
            int[] r31 = r14.permutation
            double[] r32 = r14.diagR
            double[] r33 = r14.jacNorm
            r34 = 0
            r35 = r2
            r2 = 0
        L_0x007d:
            if (r2 >= r15) goto L_0x0086
            r36 = r0[r2]
            r5[r2] = r36
            int r2 = r2 + 1
            goto L_0x007d
        L_0x0086:
            r13.qTy(r5, r14)
            r2 = 0
        L_0x008a:
            if (r2 >= r9) goto L_0x0097
            r36 = r31[r2]
            r37 = r30[r2]
            r38 = r32[r36]
            r37[r36] = r38
            int r2 = r2 + 1
            goto L_0x008a
        L_0x0097:
            r36 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            if (r18 == 0) goto L_0x00c9
            r26 = r20
            r2 = 0
        L_0x009e:
            if (r2 >= r11) goto L_0x00b5
            r28 = r33[r2]
            int r32 = (r28 > r20 ? 1 : (r28 == r20 ? 0 : -1))
            if (r32 != 0) goto L_0x00a8
            r28 = r36
        L_0x00a8:
            r38 = r12[r2]
            double r38 = r38 * r28
            double r38 = r38 * r38
            double r26 = r26 + r38
            r7[r2] = r28
            int r2 = r2 + 1
            goto L_0x009e
        L_0x00b5:
            double r28 = org.apache.commons.math3.util.FastMath.sqrt(r26)
            int r2 = (r28 > r20 ? 1 : (r28 == r20 ? 0 : -1))
            r32 = r3
            if (r2 != 0) goto L_0x00c2
            double r2 = r13.initialStepBoundFactor
            goto L_0x00c6
        L_0x00c2:
            double r2 = r13.initialStepBoundFactor
            double r2 = r2 * r28
        L_0x00c6:
            r26 = r2
            goto L_0x00cb
        L_0x00c9:
            r32 = r3
        L_0x00cb:
            int r2 = (r22 > r20 ? 1 : (r22 == r20 ? 0 : -1))
            if (r2 == 0) goto L_0x011a
            r38 = r4
            r3 = r20
            r2 = 0
        L_0x00d4:
            if (r2 >= r9) goto L_0x0112
            r39 = r31[r2]
            r40 = r33[r39]
            int r42 = (r40 > r20 ? 1 : (r40 == r20 ? 0 : -1))
            if (r42 == 0) goto L_0x0103
            r42 = r0
            r43 = r20
            r0 = 0
        L_0x00e3:
            if (r0 > r2) goto L_0x00f2
            r45 = r30[r0]
            r46 = r45[r39]
            r48 = r5[r0]
            double r46 = r46 * r48
            double r43 = r43 + r46
            int r0 = r0 + 1
            goto L_0x00e3
        L_0x00f2:
            double r43 = org.apache.commons.math3.util.FastMath.abs(r43)
            double r40 = r40 * r22
            r45 = r14
            r39 = r15
            double r14 = r43 / r40
            double r3 = org.apache.commons.math3.util.FastMath.max(r3, r14)
            goto L_0x0109
        L_0x0103:
            r42 = r0
            r45 = r14
            r39 = r15
        L_0x0109:
            int r2 = r2 + 1
            r15 = r39
            r0 = r42
            r14 = r45
            goto L_0x00d4
        L_0x0112:
            r42 = r0
            r45 = r14
            r39 = r15
            r14 = r3
            goto L_0x0124
        L_0x011a:
            r42 = r0
            r38 = r4
            r45 = r14
            r39 = r15
            r14 = r20
        L_0x0124:
            double r2 = r13.orthoTolerance
            int r0 = (r14 > r2 ? 1 : (r14 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x0138
            org.apache.commons.math3.fitting.leastsquares.OptimumImpl r0 = new org.apache.commons.math3.fitting.leastsquares.OptimumImpl
            int r2 = r17.getCount()
            int r3 = r16.getCount()
            r0.<init>(r1, r2, r3)
            return r0
        L_0x0138:
            r0 = 0
        L_0x0139:
            if (r0 >= r11) goto L_0x0150
            r2 = r7[r0]
            r41 = r10
            r40 = r11
            r10 = r33[r0]
            double r2 = org.apache.commons.math3.util.FastMath.max(r2, r10)
            r7[r0] = r2
            int r0 = r0 + 1
            r11 = r40
            r10 = r41
            goto L_0x0139
        L_0x0150:
            r41 = r10
            r40 = r11
            r0 = r1
            r2 = r20
            r10 = r26
        L_0x0159:
            r26 = 4547007122018943789(0x3f1a36e2eb1c432d, double:1.0E-4)
            int r4 = (r2 > r26 ? 1 : (r2 == r26 ? 0 : -1))
            if (r4 >= 0) goto L_0x0383
            r0 = 0
        L_0x0163:
            if (r0 >= r9) goto L_0x016e
            r2 = r31[r0]
            r3 = r12[r2]
            r6[r2] = r3
            int r0 = r0 + 1
            goto L_0x0163
        L_0x016e:
            r0 = r62
            r4 = r1
            r1 = r5
            r33 = r32
            r32 = r35
            r2 = r10
            r43 = r14
            r35 = r38
            r14 = r4
            r4 = r7
            r15 = r5
            r5 = r45
            r38 = r6
            r6 = r9
            r46 = r7
            r7 = r35
            r47 = r8
            r8 = r32
            r48 = r15
            r15 = r9
            r9 = r33
            r61 = r41
            r41 = r14
            r13 = r10
            r11 = r61
            r10 = r47
            r51 = r13
            r50 = r40
            r14 = r11
            r13 = r12
            r11 = r24
            double r0 = r0.determineLMParameter(r1, r2, r4, r5, r6, r7, r8, r9, r10, r11)
            r3 = r20
            r2 = 0
        L_0x01a8:
            if (r2 >= r15) goto L_0x01c4
            r5 = r31[r2]
            r6 = r47[r5]
            double r6 = -r6
            r47[r5] = r6
            r6 = r38[r5]
            r8 = r47[r5]
            double r6 = r6 + r8
            r13[r5] = r6
            r6 = r46[r5]
            r8 = r47[r5]
            double r6 = r6 * r8
            double r6 = r6 * r6
            double r3 = r3 + r6
            int r2 = r2 + 1
            goto L_0x01a8
        L_0x01c4:
            double r2 = org.apache.commons.math3.util.FastMath.sqrt(r3)
            r4 = r51
            if (r18 == 0) goto L_0x01d1
            double r10 = org.apache.commons.math3.util.FastMath.min(r4, r2)
            r4 = r10
        L_0x01d1:
            r17.incrementCount()
            org.apache.commons.math3.linear.ArrayRealVector r6 = new org.apache.commons.math3.linear.ArrayRealVector
            r6.<init>(r13)
            r7 = r63
            r8 = 1
            org.apache.commons.math3.fitting.leastsquares.LeastSquaresProblem$Evaluation r6 = r7.evaluate(r6)
            org.apache.commons.math3.linear.RealVector r9 = r6.getResiduals()
            double[] r42 = r9.toArray()
            double r9 = r6.getCost()
            r11 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r24 = 4591870180066957722(0x3fb999999999999a, double:0.1)
            double r51 = r9 * r24
            int r19 = (r51 > r22 ? 1 : (r51 == r22 ? 0 : -1))
            if (r19 >= 0) goto L_0x01ff
            double r11 = r9 / r22
            double r11 = r11 * r11
            double r11 = r36 - r11
        L_0x01ff:
            r8 = 0
        L_0x0200:
            if (r8 >= r15) goto L_0x021f
            r40 = r31[r8]
            r53 = r47[r40]
            r35[r8] = r20
            r7 = 0
        L_0x0209:
            if (r7 > r8) goto L_0x021a
            r55 = r35[r7]
            r49 = r30[r7]
            r57 = r49[r40]
            double r57 = r57 * r53
            double r55 = r55 + r57
            r35[r7] = r55
            int r7 = r7 + 1
            goto L_0x0209
        L_0x021a:
            int r8 = r8 + 1
            r7 = r63
            goto L_0x0200
        L_0x021f:
            r53 = r20
            r7 = 0
        L_0x0222:
            if (r7 >= r15) goto L_0x022f
            r55 = r35[r7]
            r57 = r35[r7]
            double r55 = r55 * r57
            double r53 = r53 + r55
            int r7 = r7 + 1
            goto L_0x0222
        L_0x022f:
            double r7 = r22 * r22
            double r53 = r53 / r7
            double r55 = r0 * r2
            double r55 = r55 * r2
            double r55 = r55 / r7
            r7 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r57 = r55 * r7
            double r57 = r53 + r57
            double r7 = r53 + r55
            double r7 = -r7
            int r40 = (r57 > r20 ? 1 : (r57 == r20 ? 0 : -1))
            if (r40 != 0) goto L_0x0249
            r53 = r20
            goto L_0x024b
        L_0x0249:
            double r53 = r11 / r57
        L_0x024b:
            r55 = 4598175219545276416(0x3fd0000000000000, double:0.25)
            r59 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            int r40 = (r53 > r55 ? 1 : (r53 == r55 ? 0 : -1))
            if (r40 > 0) goto L_0x0279
            int r40 = (r11 > r20 ? 1 : (r11 == r20 ? 0 : -1))
            if (r40 >= 0) goto L_0x0260
            double r55 = r7 * r59
            double r59 = r59 * r11
            double r7 = r7 + r59
            double r55 = r55 / r7
            goto L_0x0262
        L_0x0260:
            r55 = r59
        L_0x0262:
            int r7 = (r51 > r22 ? 1 : (r51 == r22 ? 0 : -1))
            if (r7 >= 0) goto L_0x026a
            int r7 = (r55 > r24 ? 1 : (r55 == r24 ? 0 : -1))
            if (r7 >= 0) goto L_0x026c
        L_0x026a:
            r55 = r24
        L_0x026c:
            r7 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r2 = r2 * r7
            double r2 = org.apache.commons.math3.util.FastMath.min(r4, r2)
            double r2 = r2 * r55
            double r0 = r0 / r55
            goto L_0x028e
        L_0x0279:
            int r7 = (r0 > r20 ? 1 : (r0 == r20 ? 0 : -1))
            if (r7 == 0) goto L_0x0288
            r7 = 4604930618986332160(0x3fe8000000000000, double:0.75)
            int r24 = (r53 > r7 ? 1 : (r53 == r7 ? 0 : -1))
            if (r24 < 0) goto L_0x0284
            goto L_0x0288
        L_0x0284:
            r24 = r0
            r2 = r4
            goto L_0x0290
        L_0x0288:
            r4 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r2 = r2 * r4
            double r0 = r0 * r59
        L_0x028e:
            r24 = r0
        L_0x0290:
            int r0 = (r53 > r26 ? 1 : (r53 == r26 ? 0 : -1))
            if (r0 < 0) goto L_0x02cf
            r4 = r20
            r1 = r50
            r0 = 0
        L_0x0299:
            if (r0 >= r1) goto L_0x02a7
            r7 = r46[r0]
            r22 = r13[r0]
            double r7 = r7 * r22
            double r7 = r7 * r7
            double r4 = r4 + r7
            int r0 = r0 + 1
            goto L_0x0299
        L_0x02a7:
            double r28 = org.apache.commons.math3.util.FastMath.sqrt(r4)
            if (r14 == 0) goto L_0x02c7
            int r0 = r16.getCount()
            r4 = r41
            boolean r0 = r14.converged(r0, r4, r6)
            if (r0 == 0) goto L_0x02c9
            org.apache.commons.math3.fitting.leastsquares.OptimumImpl r0 = new org.apache.commons.math3.fitting.leastsquares.OptimumImpl
            int r1 = r17.getCount()
            int r2 = r16.getCount()
            r0.<init>(r6, r1, r2)
            return r0
        L_0x02c7:
            r4 = r41
        L_0x02c9:
            r0 = r6
            r22 = r9
            r18 = 0
            goto L_0x02e0
        L_0x02cf:
            r4 = r41
            r1 = r50
            r0 = 0
        L_0x02d4:
            if (r0 >= r15) goto L_0x02df
            r5 = r31[r0]
            r6 = r38[r5]
            r13[r5] = r6
            int r0 = r0 + 1
            goto L_0x02d4
        L_0x02df:
            r0 = r4
        L_0x02e0:
            double r5 = org.apache.commons.math3.util.FastMath.abs(r11)
            r7 = r62
            double r8 = r7.costRelativeTolerance
            int r10 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r10 > 0) goto L_0x02f6
            int r5 = (r57 > r8 ? 1 : (r57 == r8 ? 0 : -1))
            if (r5 > 0) goto L_0x02f6
            r5 = 4611686018427387904(0x4000000000000000, double:2.0)
            int r8 = (r53 > r5 ? 1 : (r53 == r5 ? 0 : -1))
            if (r8 <= 0) goto L_0x02fe
        L_0x02f6:
            double r5 = r7.parRelativeTolerance
            double r5 = r5 * r28
            int r8 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r8 > 0) goto L_0x030c
        L_0x02fe:
            org.apache.commons.math3.fitting.leastsquares.OptimumImpl r1 = new org.apache.commons.math3.fitting.leastsquares.OptimumImpl
            int r2 = r17.getCount()
            int r3 = r16.getCount()
            r1.<init>(r0, r2, r3)
            return r1
        L_0x030c:
            double r5 = org.apache.commons.math3.util.FastMath.abs(r11)
            double r8 = org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer.TWO_EPS
            int r10 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r10 > 0) goto L_0x0334
            int r5 = (r57 > r8 ? 1 : (r57 == r8 ? 0 : -1))
            if (r5 > 0) goto L_0x0334
            r5 = 4611686018427387904(0x4000000000000000, double:2.0)
            int r8 = (r53 > r5 ? 1 : (r53 == r5 ? 0 : -1))
            if (r8 <= 0) goto L_0x0321
            goto L_0x0334
        L_0x0321:
            org.apache.commons.math3.exception.ConvergenceException r0 = new org.apache.commons.math3.exception.ConvergenceException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.TOO_SMALL_COST_RELATIVE_TOLERANCE
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            double r3 = r7.costRelativeTolerance
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            r2[r34] = r3
            r0.<init>(r1, r2)
            throw r0
        L_0x0334:
            double r5 = org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer.TWO_EPS
            double r8 = r5 * r28
            int r10 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1))
            if (r10 <= 0) goto L_0x0370
            int r8 = (r43 > r5 ? 1 : (r43 == r5 ? 0 : -1))
            if (r8 <= 0) goto L_0x035d
            r40 = r1
            r10 = r2
            r1 = r4
            r12 = r13
            r41 = r14
            r9 = r15
            r6 = r38
            r14 = r43
            r8 = r47
            r5 = r48
            r2 = r53
            r13 = r7
            r38 = r35
            r7 = r46
            r35 = r32
            r32 = r33
            goto L_0x0159
        L_0x035d:
            org.apache.commons.math3.exception.ConvergenceException r0 = new org.apache.commons.math3.exception.ConvergenceException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.TOO_SMALL_ORTHOGONALITY_TOLERANCE
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            double r3 = r7.orthoTolerance
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            r2[r34] = r3
            r0.<init>(r1, r2)
            throw r0
        L_0x0370:
            r2 = 1
            org.apache.commons.math3.exception.ConvergenceException r0 = new org.apache.commons.math3.exception.ConvergenceException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.TOO_SMALL_PARAMETERS_RELATIVE_TOLERANCE
            java.lang.Object[] r2 = new java.lang.Object[r2]
            double r3 = r7.parRelativeTolerance
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            r2[r34] = r3
            r0.<init>(r1, r2)
            throw r0
        L_0x0383:
            r46 = r7
            r26 = r10
            r7 = r13
            r1 = r0
            r3 = r32
            r2 = r35
            r4 = r38
            r15 = r39
            r11 = r40
            r10 = r41
            r0 = r42
            r7 = r46
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer.optimize(org.apache.commons.math3.fitting.leastsquares.LeastSquaresProblem):org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer$Optimum");
    }

    private static class InternalData {
        /* access modifiers changed from: private */
        public final double[] beta;
        /* access modifiers changed from: private */
        public final double[] diagR;
        /* access modifiers changed from: private */
        public final double[] jacNorm;
        /* access modifiers changed from: private */
        public final int[] permutation;
        /* access modifiers changed from: private */
        public final int rank;
        /* access modifiers changed from: private */
        public final double[][] weightedJacobian;

        InternalData(double[][] dArr, int[] iArr, int i, double[] dArr2, double[] dArr3, double[] dArr4) {
            this.weightedJacobian = dArr;
            this.permutation = iArr;
            this.rank = i;
            this.diagR = dArr2;
            this.jacNorm = dArr3;
            this.beta = dArr4;
        }
    }

    private double determineLMParameter(double[] dArr, double d, double[] dArr2, InternalData internalData, int i, double[] dArr3, double[] dArr4, double[] dArr5, double[] dArr6, double d2) {
        double d3;
        double d4;
        double d5;
        double d6 = d;
        int i2 = i;
        double[][] access$000 = internalData.weightedJacobian;
        int[] access$100 = internalData.permutation;
        int access$400 = internalData.rank;
        double[] access$200 = internalData.diagR;
        int i3 = 0;
        int length = access$000[0].length;
        for (int i4 = 0; i4 < access$400; i4++) {
            dArr6[access$100[i4]] = dArr[i4];
        }
        for (int i5 = access$400; i5 < length; i5++) {
            dArr6[access$100[i5]] = 0.0d;
        }
        for (int i6 = access$400 - 1; i6 >= 0; i6--) {
            int i7 = access$100[i6];
            double d7 = dArr6[i7] / access$200[i7];
            for (int i8 = 0; i8 < i6; i8++) {
                int i9 = access$100[i8];
                dArr6[i9] = dArr6[i9] - (access$000[i8][i7] * d7);
            }
            dArr6[i7] = d7;
        }
        double d8 = 0.0d;
        for (int i10 = 0; i10 < i2; i10++) {
            int i11 = access$100[i10];
            double d9 = dArr2[i11] * dArr6[i11];
            dArr3[i11] = d9;
            d8 += d9 * d9;
        }
        double sqrt = FastMath.sqrt(d8);
        double d10 = sqrt - d6;
        double d11 = d6 * 0.1d;
        if (d10 <= d11) {
            return 0.0d;
        }
        if (access$400 == i2) {
            for (int i12 = 0; i12 < i2; i12++) {
                int i13 = access$100[i12];
                dArr3[i13] = dArr3[i13] * (dArr2[i13] / sqrt);
            }
            double d12 = 0.0d;
            int i14 = 0;
            while (i14 < i2) {
                int i15 = access$100[i14];
                double d13 = 0.0d;
                while (i3 < i14) {
                    d13 += access$000[i3][i15] * dArr3[access$100[i3]];
                    i3++;
                }
                double d14 = (dArr3[i15] - d13) / access$200[i15];
                dArr3[i15] = d14;
                d12 += d14 * d14;
                i14++;
                i3 = 0;
            }
            d3 = d10 / (d6 * d12);
        } else {
            d3 = 0.0d;
        }
        double d15 = 0.0d;
        for (int i16 = 0; i16 < i2; i16++) {
            int i17 = access$100[i16];
            double d16 = 0.0d;
            for (int i18 = 0; i18 <= i16; i18++) {
                d16 += access$000[i18][i17] * dArr[i18];
            }
            double d17 = d16 / dArr2[i17];
            d15 += d17 * d17;
        }
        double sqrt2 = FastMath.sqrt(d15);
        double d18 = sqrt2 / d6;
        if (d18 == 0.0d) {
            d18 = Precision.SAFE_MIN / FastMath.min(d6, 0.1d);
        }
        double d19 = d10;
        double d20 = d18;
        double d21 = d19;
        double min = FastMath.min(d20, FastMath.max(d2, d3));
        double d22 = 0.0d;
        if (min == 0.0d) {
            min = sqrt2 / sqrt;
        }
        double d23 = d3;
        double d24 = min;
        int i19 = 10;
        double d25 = d20;
        while (i19 >= 0) {
            if (d24 == d22) {
                d24 = FastMath.max(Precision.SAFE_MIN, 0.001d * d25);
            }
            double d26 = d24;
            double sqrt3 = FastMath.sqrt(d26);
            for (int i20 = 0; i20 < i2; i20++) {
                int i21 = access$100[i20];
                dArr3[i21] = dArr2[i21] * sqrt3;
            }
            int i22 = i19;
            double d27 = d25;
            double d28 = d26;
            double d29 = d23;
            determineLMDirection(dArr, dArr3, dArr4, internalData, i, dArr5, dArr6);
            double d30 = 0.0d;
            for (int i23 = 0; i23 < i2; i23++) {
                int i24 = access$100[i23];
                double d31 = dArr2[i24] * dArr6[i24];
                dArr5[i24] = d31;
                d30 += d31 * d31;
            }
            double sqrt4 = FastMath.sqrt(d30);
            double d32 = sqrt4 - d6;
            if (FastMath.abs(d32) <= d11 || (d29 == 0.0d && d32 <= d21 && d21 < 0.0d)) {
                return d28;
            }
            for (int i25 = 0; i25 < i2; i25++) {
                int i26 = access$100[i25];
                dArr3[i26] = (dArr5[i26] * dArr2[i26]) / sqrt4;
            }
            int i27 = 0;
            while (i27 < i2) {
                int i28 = access$100[i27];
                dArr3[i28] = dArr3[i28] / dArr4[i27];
                double d33 = dArr3[i28];
                i27++;
                for (int i29 = i27; i29 < i2; i29++) {
                    int i30 = access$100[i29];
                    dArr3[i30] = dArr3[i30] - (access$000[i29][i28] * d33);
                }
            }
            double d34 = 0.0d;
            for (int i31 = 0; i31 < i2; i31++) {
                double d35 = dArr3[access$100[i31]];
                d34 += d35 * d35;
            }
            double d36 = d32 / (d6 * d34);
            if (d32 > 0.0d) {
                d4 = d28;
                d29 = FastMath.max(d29, d4);
                d5 = d27;
            } else {
                d4 = d28;
                d5 = d27;
                if (d32 < 0.0d) {
                    d5 = FastMath.min(d5, d4);
                }
            }
            d24 = FastMath.max(d29, d36 + d4);
            d21 = d32;
            d22 = 0.0d;
            double d37 = d29;
            i19 = i22 - 1;
            d25 = d5;
            d23 = d37;
        }
        return d24;
    }

    private void determineLMDirection(double[] dArr, double[] dArr2, double[] dArr3, InternalData internalData, int i, double[] dArr4, double[] dArr5) {
        int[] iArr;
        double d;
        double d2;
        double[] dArr6 = dArr3;
        int i2 = i;
        double[] dArr7 = dArr5;
        int[] access$100 = internalData.permutation;
        double[][] access$000 = internalData.weightedJacobian;
        double[] access$200 = internalData.diagR;
        int i3 = 0;
        while (i3 < i2) {
            int i4 = access$100[i3];
            int i5 = i3 + 1;
            for (int i6 = i5; i6 < i2; i6++) {
                access$000[i6][i4] = access$000[i3][access$100[i6]];
            }
            dArr7[i3] = access$200[i4];
            dArr4[i3] = dArr[i3];
            i3 = i5;
        }
        int i7 = 0;
        while (true) {
            double d3 = 0.0d;
            if (i7 >= i2) {
                break;
            }
            double d4 = dArr2[access$100[i7]];
            if (d4 != 0.0d) {
                Arrays.fill(dArr6, i7 + 1, dArr6.length, 0.0d);
            }
            dArr6[i7] = d4;
            int i8 = i7;
            double d5 = 0.0d;
            while (i8 < i2) {
                int i9 = access$100[i8];
                if (dArr6[i8] != d3) {
                    double d6 = access$000[i8][i9];
                    if (FastMath.abs(d6) < FastMath.abs(dArr6[i8])) {
                        double d7 = d6 / dArr6[i8];
                        d = 1.0d / FastMath.sqrt((d7 * d7) + 1.0d);
                        d2 = d7 * d;
                    } else {
                        double d8 = dArr6[i8] / d6;
                        double sqrt = 1.0d / FastMath.sqrt((d8 * d8) + 1.0d);
                        d = sqrt * d8;
                        d2 = sqrt;
                    }
                    double d9 = d;
                    access$000[i8][i9] = (d6 * d2) + (dArr6[i8] * d9);
                    double d10 = (dArr4[i8] * d2) + (d9 * d5);
                    iArr = access$100;
                    double d11 = -d9;
                    double d12 = (dArr4[i8] * d11) + (d5 * d2);
                    dArr4[i8] = d10;
                    for (int i10 = i8 + 1; i10 < i2; i10++) {
                        double d13 = access$000[i10][i9];
                        double d14 = (d2 * d13) + (dArr6[i10] * d9);
                        dArr6[i10] = (d13 * d11) + (dArr6[i10] * d2);
                        access$000[i10][i9] = d14;
                    }
                    d5 = d12;
                } else {
                    iArr = access$100;
                }
                i8++;
                access$100 = iArr;
                d3 = 0.0d;
            }
            int[] iArr2 = access$100;
            dArr6[i7] = access$000[i7][iArr2[i7]];
            double[] dArr8 = dArr5;
            access$000[i7][iArr2[i7]] = dArr8[i7];
            i7++;
            dArr7 = dArr8;
            access$100 = iArr2;
        }
        double[] dArr9 = dArr7;
        int[] iArr3 = access$100;
        int i11 = i2;
        for (int i12 = 0; i12 < i2; i12++) {
            if (dArr6[i12] == 0.0d && i11 == i2) {
                i11 = i12;
            }
            if (i11 < i2) {
                dArr4[i12] = 0.0d;
            }
        }
        if (i11 > 0) {
            for (int i13 = i11 - 1; i13 >= 0; i13--) {
                int i14 = iArr3[i13];
                double d15 = 0.0d;
                for (int i15 = i13 + 1; i15 < i11; i15++) {
                    d15 += access$000[i15][i14] * dArr4[i15];
                }
                dArr4[i13] = (dArr4[i13] - d15) / dArr6[i13];
            }
        }
        for (int i16 = 0; i16 < dArr9.length; i16++) {
            dArr9[iArr3[i16]] = dArr4[i16];
        }
    }

    private InternalData qrDecomposition(RealMatrix realMatrix, int i) throws ConvergenceException {
        double[][] data = realMatrix.scalarMultiply(-1.0d).getData();
        int length = data.length;
        int length2 = data[0].length;
        int[] iArr = new int[length2];
        double[] dArr = new double[length2];
        double[] dArr2 = new double[length2];
        double[] dArr3 = new double[length2];
        for (int i2 = 0; i2 < length2; i2++) {
            iArr[i2] = i2;
            double d = 0.0d;
            for (double[] dArr4 : data) {
                double d2 = dArr4[i2];
                d += d2 * d2;
            }
            dArr2[i2] = FastMath.sqrt(d);
        }
        for (int i3 = 0; i3 < length2; i3++) {
            double d3 = Double.NEGATIVE_INFINITY;
            int i4 = -1;
            for (int i5 = i3; i5 < length2; i5++) {
                double d4 = 0.0d;
                for (int i6 = i3; i6 < length; i6++) {
                    double d5 = data[i6][iArr[i5]];
                    d4 += d5 * d5;
                }
                if (Double.isInfinite(d4) || Double.isNaN(d4)) {
                    throw new ConvergenceException(LocalizedFormats.UNABLE_TO_PERFORM_QR_DECOMPOSITION_ON_JACOBIAN, Integer.valueOf(length), Integer.valueOf(length2));
                }
                if (d4 > d3) {
                    i4 = i5;
                    d3 = d4;
                }
            }
            if (d3 <= this.qrRankingThreshold) {
                return new InternalData(data, iArr, i3, dArr, dArr2, dArr3);
            }
            int i7 = iArr[i4];
            iArr[i4] = iArr[i3];
            iArr[i3] = i7;
            double d6 = data[i3][i7];
            int i8 = (d6 > 0.0d ? 1 : (d6 == 0.0d ? 0 : -1));
            double sqrt = FastMath.sqrt(d3);
            if (i8 > 0) {
                sqrt = -sqrt;
            }
            double d7 = 1.0d / (d3 - (d6 * sqrt));
            dArr3[i7] = d7;
            dArr[i7] = sqrt;
            double[] dArr5 = data[i3];
            dArr5[i7] = dArr5[i7] - sqrt;
            for (int i9 = (length2 - 1) - i3; i9 > 0; i9--) {
                double d8 = 0.0d;
                for (int i10 = i3; i10 < length; i10++) {
                    d8 += data[i10][i7] * data[i10][iArr[i3 + i9]];
                }
                double d9 = d8 * d7;
                for (int i11 = i3; i11 < length; i11++) {
                    double[] dArr6 = data[i11];
                    int i12 = iArr[i3 + i9];
                    dArr6[i12] = dArr6[i12] - (data[i11][i7] * d9);
                }
            }
        }
        return new InternalData(data, iArr, i, dArr, dArr2, dArr3);
    }

    private void qTy(double[] dArr, InternalData internalData) {
        double[][] access$000 = internalData.weightedJacobian;
        int[] access$100 = internalData.permutation;
        double[] access$500 = internalData.beta;
        int length = access$000.length;
        int length2 = access$000[0].length;
        for (int i = 0; i < length2; i++) {
            int i2 = access$100[i];
            double d = 0.0d;
            for (int i3 = i; i3 < length; i3++) {
                d += access$000[i3][i2] * dArr[i3];
            }
            double d2 = d * access$500[i2];
            for (int i4 = i; i4 < length; i4++) {
                dArr[i4] = dArr[i4] - (access$000[i4][i2] * d2);
            }
        }
    }
}
