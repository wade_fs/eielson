package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.util.FastMath;

public class BrentSolver extends AbstractUnivariateSolver {
    private static final double DEFAULT_ABSOLUTE_ACCURACY = 1.0E-6d;

    public BrentSolver() {
        this(1.0E-6d);
    }

    public BrentSolver(double d) {
        super(d);
    }

    public BrentSolver(double d, double d2) {
        super(d, d2);
    }

    public BrentSolver(double d, double d2, double d3) {
        super(d, d2, d3);
    }

    /* access modifiers changed from: protected */
    public double doSolve() throws NoBracketingException, TooManyEvaluationsException, NumberIsTooLargeException {
        double min = getMin();
        double max = getMax();
        double startValue = getStartValue();
        double functionValueAccuracy = getFunctionValueAccuracy();
        verifySequence(min, startValue, max);
        double computeObjectiveValue = computeObjectiveValue(startValue);
        if (FastMath.abs(computeObjectiveValue) <= functionValueAccuracy) {
            return startValue;
        }
        double computeObjectiveValue2 = computeObjectiveValue(min);
        if (FastMath.abs(computeObjectiveValue2) <= functionValueAccuracy) {
            return min;
        }
        if (computeObjectiveValue * computeObjectiveValue2 < 0.0d) {
            return brent(min, startValue, computeObjectiveValue2, computeObjectiveValue);
        }
        double computeObjectiveValue3 = computeObjectiveValue(max);
        if (FastMath.abs(computeObjectiveValue3) <= functionValueAccuracy) {
            return max;
        }
        if (computeObjectiveValue * computeObjectiveValue3 < 0.0d) {
            return brent(startValue, max, computeObjectiveValue, computeObjectiveValue3);
        }
        throw new NoBracketingException(min, max, computeObjectiveValue2, computeObjectiveValue3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private double brent(double r43, double r45, double r47, double r49) {
        /*
            r42 = this;
            double r0 = r45 - r43
            double r2 = r42.getAbsoluteAccuracy()
            double r4 = r42.getRelativeAccuracy()
            r10 = r43
            r16 = r10
            r8 = r45
            r14 = r47
            r6 = r49
            r12 = r0
            r18 = r12
            r0 = r14
        L_0x0018:
            double r20 = org.apache.commons.math3.util.FastMath.abs(r0)
            double r22 = org.apache.commons.math3.util.FastMath.abs(r6)
            int r24 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
            if (r24 >= 0) goto L_0x002a
            r14 = r0
            r0 = r8
            r16 = r10
            r10 = r6
            goto L_0x0038
        L_0x002a:
            r38 = r0
            r0 = r16
            r16 = r8
            r8 = r10
            r10 = r38
            r40 = r6
            r6 = r14
            r14 = r40
        L_0x0038:
            r20 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r22 = r4 * r20
            double r24 = org.apache.commons.math3.util.FastMath.abs(r16)
            double r22 = r22 * r24
            double r22 = r22 + r2
            double r24 = r8 - r16
            r26 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r24 = r24 * r26
            double r28 = org.apache.commons.math3.util.FastMath.abs(r24)
            int r30 = (r28 > r22 ? 1 : (r28 == r22 ? 0 : -1))
            if (r30 <= 0) goto L_0x0116
            r28 = r2
            r2 = 0
            boolean r30 = org.apache.commons.math3.util.Precision.equals(r14, r2)
            if (r30 == 0) goto L_0x005e
            goto L_0x0116
        L_0x005e:
            double r30 = org.apache.commons.math3.util.FastMath.abs(r12)
            int r32 = (r30 > r22 ? 1 : (r30 == r22 ? 0 : -1))
            if (r32 < 0) goto L_0x00cb
            double r30 = org.apache.commons.math3.util.FastMath.abs(r6)
            double r32 = org.apache.commons.math3.util.FastMath.abs(r14)
            int r34 = (r30 > r32 ? 1 : (r30 == r32 ? 0 : -1))
            if (r34 > 0) goto L_0x0073
            goto L_0x00cb
        L_0x0073:
            double r30 = r14 / r6
            r32 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r34 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r34 != 0) goto L_0x0082
            double r20 = r20 * r24
            double r20 = r20 * r30
            double r32 = r32 - r30
            goto L_0x009f
        L_0x0082:
            double r6 = r6 / r10
            double r34 = r14 / r10
            double r20 = r20 * r24
            double r20 = r20 * r6
            double r36 = r6 - r34
            double r20 = r20 * r36
            double r0 = r16 - r0
            double r34 = r34 - r32
            double r0 = r0 * r34
            double r20 = r20 - r0
            double r20 = r20 * r30
            double r6 = r6 - r32
            double r6 = r6 * r34
            double r30 = r30 - r32
            double r32 = r6 * r30
        L_0x009f:
            r0 = r20
            r6 = r32
            int r20 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r20 <= 0) goto L_0x00a9
            double r6 = -r6
            goto L_0x00aa
        L_0x00a9:
            double r0 = -r0
        L_0x00aa:
            r20 = 4609434218613702656(0x3ff8000000000000, double:1.5)
            double r20 = r20 * r24
            double r20 = r20 * r6
            double r30 = r22 * r6
            double r30 = org.apache.commons.math3.util.FastMath.abs(r30)
            double r20 = r20 - r30
            int r30 = (r0 > r20 ? 1 : (r0 == r20 ? 0 : -1))
            if (r30 >= 0) goto L_0x00cb
            double r12 = r12 * r26
            double r12 = r12 * r6
            double r12 = org.apache.commons.math3.util.FastMath.abs(r12)
            int r20 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r20 < 0) goto L_0x00c9
            goto L_0x00cb
        L_0x00c9:
            double r0 = r0 / r6
            goto L_0x00cf
        L_0x00cb:
            r0 = r24
            r18 = r0
        L_0x00cf:
            double r6 = org.apache.commons.math3.util.FastMath.abs(r0)
            int r12 = (r6 > r22 ? 1 : (r6 == r22 ? 0 : -1))
            if (r12 <= 0) goto L_0x00dd
            double r6 = r16 + r0
        L_0x00d9:
            r12 = r6
            r6 = r42
            goto L_0x00eb
        L_0x00dd:
            int r6 = (r24 > r2 ? 1 : (r24 == r2 ? 0 : -1))
            if (r6 <= 0) goto L_0x00e8
            double r22 = r16 + r22
            r6 = r42
            r12 = r22
            goto L_0x00eb
        L_0x00e8:
            double r6 = r16 - r22
            goto L_0x00d9
        L_0x00eb:
            double r20 = r6.computeObjectiveValue(r12)
            int r7 = (r20 > r2 ? 1 : (r20 == r2 ? 0 : -1))
            if (r7 <= 0) goto L_0x00f7
            int r7 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r7 > 0) goto L_0x00ff
        L_0x00f7:
            int r7 = (r20 > r2 ? 1 : (r20 == r2 ? 0 : -1))
            if (r7 > 0) goto L_0x0108
            int r7 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r7 > 0) goto L_0x0108
        L_0x00ff:
            double r0 = r12 - r16
            r2 = r0
            r18 = r2
            r0 = r14
            r10 = r16
            goto L_0x010b
        L_0x0108:
            r2 = r0
            r0 = r10
            r10 = r8
        L_0x010b:
            r8 = r12
            r12 = r18
            r6 = r20
            r18 = r2
            r2 = r28
            goto L_0x0018
        L_0x0116:
            r6 = r42
            return r16
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.analysis.solvers.BrentSolver.brent(double, double, double, double):double");
    }
}
