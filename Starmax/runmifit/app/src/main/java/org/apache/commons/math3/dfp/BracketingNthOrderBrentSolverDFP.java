package org.apache.commons.math3.dfp;

import org.apache.commons.math3.analysis.solvers.AllowedSolution;
import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.util.Incrementor;

public class BracketingNthOrderBrentSolverDFP {
    private static final int MAXIMAL_AGING = 2;
    private final Dfp absoluteAccuracy;
    private final Incrementor evaluations = new Incrementor();
    private final Dfp functionValueAccuracy;
    private final int maximalOrder;
    private final Dfp relativeAccuracy;

    public BracketingNthOrderBrentSolverDFP(Dfp dfp, Dfp dfp2, Dfp dfp3, int i) throws NumberIsTooSmallException {
        if (i >= 2) {
            this.maximalOrder = i;
            this.absoluteAccuracy = dfp2;
            this.relativeAccuracy = dfp;
            this.functionValueAccuracy = dfp3;
            return;
        }
        throw new NumberIsTooSmallException(Integer.valueOf(i), 2, true);
    }

    public int getMaximalOrder() {
        return this.maximalOrder;
    }

    public int getMaxEvaluations() {
        return this.evaluations.getMaximalCount();
    }

    public int getEvaluations() {
        return this.evaluations.getCount();
    }

    public Dfp getAbsoluteAccuracy() {
        return this.absoluteAccuracy;
    }

    public Dfp getRelativeAccuracy() {
        return this.relativeAccuracy;
    }

    public Dfp getFunctionValueAccuracy() {
        return this.functionValueAccuracy;
    }

    public Dfp solve(int i, UnivariateDfpFunction univariateDfpFunction, Dfp dfp, Dfp dfp2, AllowedSolution allowedSolution) throws NullArgumentException, NoBracketingException {
        return solve(i, univariateDfpFunction, dfp, dfp2, dfp.add(dfp2).divide(2), allowedSolution);
    }

    /* JADX WARNING: Removed duplicated region for block: B:102:0x01c0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0209  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0219  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.math3.dfp.Dfp solve(int r30, org.apache.commons.math3.dfp.UnivariateDfpFunction r31, org.apache.commons.math3.dfp.Dfp r32, org.apache.commons.math3.dfp.Dfp r33, org.apache.commons.math3.dfp.Dfp r34, org.apache.commons.math3.analysis.solvers.AllowedSolution r35) throws org.apache.commons.math3.exception.NullArgumentException, org.apache.commons.math3.exception.NoBracketingException {
        /*
            r29 = this;
            r6 = r29
            r7 = r31
            org.apache.commons.math3.util.MathUtils.checkNotNull(r31)
            org.apache.commons.math3.util.Incrementor r0 = r6.evaluations
            r1 = r30
            r0.setMaximalCount(r1)
            org.apache.commons.math3.util.Incrementor r0 = r6.evaluations
            r0.resetCount()
            org.apache.commons.math3.dfp.Dfp r8 = r34.getZero()
            r9 = 3
            r10 = 1
            org.apache.commons.math3.dfp.Dfp r11 = r8.newInstance(r10, r9)
            int r0 = r6.maximalOrder
            int r1 = r0 + 1
            org.apache.commons.math3.dfp.Dfp[] r12 = new org.apache.commons.math3.dfp.Dfp[r1]
            int r0 = r0 + r10
            org.apache.commons.math3.dfp.Dfp[] r13 = new org.apache.commons.math3.dfp.Dfp[r0]
            r14 = 0
            r12[r14] = r32
            r12[r10] = r34
            r15 = 2
            r12[r15] = r33
            org.apache.commons.math3.util.Incrementor r0 = r6.evaluations
            r0.incrementCount()
            r0 = r12[r10]
            org.apache.commons.math3.dfp.Dfp r0 = r7.value(r0)
            r13[r10] = r0
            r0 = r13[r10]
            boolean r0 = r0.isZero()
            if (r0 == 0) goto L_0x0046
            r0 = r12[r10]
            return r0
        L_0x0046:
            org.apache.commons.math3.util.Incrementor r0 = r6.evaluations
            r0.incrementCount()
            r0 = r12[r14]
            org.apache.commons.math3.dfp.Dfp r0 = r7.value(r0)
            r13[r14] = r0
            r0 = r13[r14]
            boolean r0 = r0.isZero()
            if (r0 == 0) goto L_0x005e
            r0 = r12[r14]
            return r0
        L_0x005e:
            r0 = r13[r14]
            r1 = r13[r10]
            org.apache.commons.math3.dfp.Dfp r0 = r0.multiply(r1)
            boolean r0 = r0.negativeOrNull()
            if (r0 == 0) goto L_0x006f
            r0 = 1
            r1 = 2
            goto L_0x0097
        L_0x006f:
            org.apache.commons.math3.util.Incrementor r0 = r6.evaluations
            r0.incrementCount()
            r0 = r12[r15]
            org.apache.commons.math3.dfp.Dfp r0 = r7.value(r0)
            r13[r15] = r0
            r0 = r13[r15]
            boolean r0 = r0.isZero()
            if (r0 == 0) goto L_0x0087
            r0 = r12[r15]
            return r0
        L_0x0087:
            r0 = r13[r10]
            r1 = r13[r15]
            org.apache.commons.math3.dfp.Dfp r0 = r0.multiply(r1)
            boolean r0 = r0.negativeOrNull()
            if (r0 == 0) goto L_0x0277
            r0 = 2
            r1 = 3
        L_0x0097:
            int r2 = r12.length
            org.apache.commons.math3.dfp.Dfp[] r5 = new org.apache.commons.math3.dfp.Dfp[r2]
            int r2 = r0 + -1
            r3 = r12[r2]
            r2 = r13[r2]
            org.apache.commons.math3.dfp.Dfp r4 = r3.abs()
            org.apache.commons.math3.dfp.Dfp r16 = r2.abs()
            r17 = r12[r0]
            r18 = r13[r0]
            org.apache.commons.math3.dfp.Dfp r9 = r17.abs()
            org.apache.commons.math3.dfp.Dfp r19 = r18.abs()
            r23 = r0
            r22 = r1
            r10 = r2
            r0 = r3
            r3 = r16
            r1 = r17
            r21 = r18
            r2 = r19
            r20 = 0
        L_0x00c4:
            boolean r18 = r4.lessThan(r9)
            if (r18 == 0) goto L_0x00cc
            r15 = r9
            goto L_0x00cd
        L_0x00cc:
            r15 = r4
        L_0x00cd:
            boolean r18 = r3.lessThan(r2)
            if (r18 == 0) goto L_0x00d8
            r18 = r2
            r19 = r3
            goto L_0x00dd
        L_0x00d8:
            r18 = r2
            r2 = r3
            r19 = r2
        L_0x00dd:
            org.apache.commons.math3.dfp.Dfp r3 = r6.absoluteAccuracy
            r33 = r4
            org.apache.commons.math3.dfp.Dfp r4 = r6.relativeAccuracy
            org.apache.commons.math3.dfp.Dfp r4 = r4.multiply(r15)
            org.apache.commons.math3.dfp.Dfp r3 = r3.add(r4)
            org.apache.commons.math3.dfp.Dfp r4 = r1.subtract(r0)
            org.apache.commons.math3.dfp.Dfp r3 = r4.subtract(r3)
            boolean r3 = r3.negativeOrNull()
            if (r3 != 0) goto L_0x0237
            org.apache.commons.math3.dfp.Dfp r3 = r6.functionValueAccuracy
            boolean r2 = r2.lessThan(r3)
            if (r2 == 0) goto L_0x0103
            goto L_0x0237
        L_0x0103:
            r2 = 16
            r3 = 2
            if (r14 < r3) goto L_0x0117
            r15 = r21
            org.apache.commons.math3.dfp.Dfp r2 = r15.divide(r2)
            org.apache.commons.math3.dfp.Dfp r2 = r2.negate()
            r4 = r20
        L_0x0114:
            r20 = r2
            goto L_0x0128
        L_0x0117:
            r4 = r20
            r15 = r21
            if (r4 < r3) goto L_0x0126
            org.apache.commons.math3.dfp.Dfp r2 = r10.divide(r2)
            org.apache.commons.math3.dfp.Dfp r2 = r2.negate()
            goto L_0x0114
        L_0x0126:
            r20 = r8
        L_0x0128:
            r21 = r22
            r3 = 0
        L_0x012b:
            int r2 = r21 - r3
            java.lang.System.arraycopy(r12, r3, r5, r3, r2)
            r2 = r0
            r0 = r29
            r34 = r9
            r9 = r1
            r1 = r20
            r24 = r11
            r11 = r18
            r18 = r15
            r15 = r2
            r2 = r5
            r25 = r11
            r11 = r19
            r19 = r3
            r3 = r13
            r26 = r33
            r27 = r4
            r4 = r19
            r28 = r5
            r5 = r21
            org.apache.commons.math3.dfp.Dfp r0 = r0.guessX(r1, r2, r3, r4, r5)
            boolean r1 = r0.greaterThan(r15)
            if (r1 == 0) goto L_0x0168
            boolean r1 = r0.lessThan(r9)
            if (r1 != 0) goto L_0x0162
            goto L_0x0168
        L_0x0162:
            r1 = r0
            r3 = r19
            r0 = r23
            goto L_0x0179
        L_0x0168:
            r0 = r23
            int r1 = r0 - r19
            int r2 = r21 - r0
            if (r1 < r2) goto L_0x0173
            int r3 = r19 + 1
            goto L_0x0177
        L_0x0173:
            int r21 = r21 + -1
            r3 = r19
        L_0x0177:
            r1 = r24
        L_0x0179:
            boolean r2 = r1.isNaN()
            if (r2 == 0) goto L_0x019a
            int r2 = r21 - r3
            r4 = 1
            if (r2 > r4) goto L_0x0185
            goto L_0x019a
        L_0x0185:
            r23 = r0
            r1 = r9
            r19 = r11
            r0 = r15
            r15 = r18
            r11 = r24
            r18 = r25
            r33 = r26
            r4 = r27
            r5 = r28
            r9 = r34
            goto L_0x012b
        L_0x019a:
            boolean r2 = r1.isNaN()
            if (r2 == 0) goto L_0x01b1
            org.apache.commons.math3.dfp.Dfp r1 = r9.subtract(r15)
            r2 = 2
            org.apache.commons.math3.dfp.Dfp r1 = r1.divide(r2)
            org.apache.commons.math3.dfp.Dfp r1 = r15.add(r1)
            int r3 = r0 + -1
            r21 = r0
        L_0x01b1:
            org.apache.commons.math3.util.Incrementor r2 = r6.evaluations
            r2.incrementCount()
            org.apache.commons.math3.dfp.Dfp r2 = r7.value(r1)
            boolean r4 = r2.isZero()
            if (r4 == 0) goto L_0x01c1
            return r1
        L_0x01c1:
            r4 = r22
            r5 = 2
            if (r4 <= r5) goto L_0x01d7
            int r5 = r21 - r3
            if (r5 == r4) goto L_0x01d7
            r6 = 0
            java.lang.System.arraycopy(r12, r3, r12, r6, r5)
            java.lang.System.arraycopy(r13, r3, r13, r6, r5)
            int r23 = r0 - r3
            r3 = r5
        L_0x01d4:
            r0 = r23
            goto L_0x01ee
        L_0x01d7:
            int r3 = r12.length
            if (r4 != r3) goto L_0x01ed
            int r3 = r4 + -1
            int r4 = r12.length
            r5 = 1
            int r4 = r4 + r5
            r6 = 2
            int r4 = r4 / r6
            if (r0 < r4) goto L_0x01ee
            r4 = 0
            java.lang.System.arraycopy(r12, r5, r12, r4, r3)
            java.lang.System.arraycopy(r13, r5, r13, r4, r3)
            int r23 = r0 + -1
            goto L_0x01d4
        L_0x01ed:
            r3 = r4
        L_0x01ee:
            int r4 = r0 + 1
            int r5 = r3 - r0
            java.lang.System.arraycopy(r12, r0, r12, r4, r5)
            r12[r0] = r1
            java.lang.System.arraycopy(r13, r0, r13, r4, r5)
            r13[r0] = r2
            r5 = 1
            int r22 = r3 + 1
            org.apache.commons.math3.dfp.Dfp r3 = r2.multiply(r10)
            boolean r3 = r3.negativeOrNull()
            if (r3 == 0) goto L_0x0219
            org.apache.commons.math3.dfp.Dfp r3 = r2.abs()
            int r14 = r14 + 1
            r23 = r0
            r21 = r2
            r2 = r3
            r3 = r11
            r0 = r15
            r20 = 0
            goto L_0x022a
        L_0x0219:
            org.apache.commons.math3.dfp.Dfp r0 = r2.abs()
            int r20 = r27 + 1
            r3 = r0
            r0 = r1
            r10 = r2
            r23 = r4
            r1 = r9
            r21 = r18
            r2 = r25
            r14 = 0
        L_0x022a:
            r6 = r29
            r9 = r34
            r11 = r24
            r4 = r26
            r5 = r28
            r15 = 2
            goto L_0x00c4
        L_0x0237:
            r15 = r0
            r9 = r1
            r25 = r18
            r11 = r19
            int[] r0 = org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.C35011.f7975xe6390080
            int r1 = r35.ordinal()
            r0 = r0[r1]
            r1 = 1
            if (r0 == r1) goto L_0x026d
            r1 = 2
            if (r0 == r1) goto L_0x026c
            r1 = 3
            if (r0 == r1) goto L_0x026b
            r1 = 4
            if (r0 == r1) goto L_0x0264
            r1 = 5
            if (r0 != r1) goto L_0x025d
            boolean r0 = r10.lessThan(r8)
            if (r0 == 0) goto L_0x025b
            goto L_0x025c
        L_0x025b:
            r9 = r15
        L_0x025c:
            return r9
        L_0x025d:
            org.apache.commons.math3.exception.MathInternalError r0 = new org.apache.commons.math3.exception.MathInternalError
            r1 = 0
            r0.<init>(r1)
            throw r0
        L_0x0264:
            boolean r0 = r10.lessThan(r8)
            if (r0 == 0) goto L_0x026b
            r9 = r15
        L_0x026b:
            return r9
        L_0x026c:
            return r15
        L_0x026d:
            r2 = r25
            boolean r0 = r11.lessThan(r2)
            if (r0 == 0) goto L_0x0276
            r9 = r15
        L_0x0276:
            return r9
        L_0x0277:
            org.apache.commons.math3.exception.NoBracketingException r9 = new org.apache.commons.math3.exception.NoBracketingException
            r0 = 0
            r1 = r12[r0]
            double r1 = r1.toDouble()
            r3 = 2
            r4 = r12[r3]
            double r4 = r4.toDouble()
            r0 = r13[r0]
            double r6 = r0.toDouble()
            r0 = r13[r3]
            double r10 = r0.toDouble()
            r0 = r9
            r3 = r4
            r5 = r6
            r7 = r10
            r0.<init>(r1, r3, r5, r7)
            goto L_0x029c
        L_0x029b:
            throw r9
        L_0x029c:
            goto L_0x029b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.solve(int, org.apache.commons.math3.dfp.UnivariateDfpFunction, org.apache.commons.math3.dfp.Dfp, org.apache.commons.math3.dfp.Dfp, org.apache.commons.math3.dfp.Dfp, org.apache.commons.math3.analysis.solvers.AllowedSolution):org.apache.commons.math3.dfp.Dfp");
    }

    /* renamed from: org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP$1 */
    static /* synthetic */ class C35011 {

        /* renamed from: $SwitchMap$org$apache$commons$math3$analysis$solvers$AllowedSolution */
        static final /* synthetic */ int[] f7975xe6390080 = new int[AllowedSolution.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                org.apache.commons.math3.analysis.solvers.AllowedSolution[] r0 = org.apache.commons.math3.analysis.solvers.AllowedSolution.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.C35011.f7975xe6390080 = r0
                int[] r0 = org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.C35011.f7975xe6390080     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.ANY_SIDE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.C35011.f7975xe6390080     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.LEFT_SIDE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.C35011.f7975xe6390080     // Catch:{ NoSuchFieldError -> 0x002a }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.RIGHT_SIDE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.C35011.f7975xe6390080     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.BELOW_SIDE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.C35011.f7975xe6390080     // Catch:{ NoSuchFieldError -> 0x0040 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r1 = org.apache.commons.math3.analysis.solvers.AllowedSolution.ABOVE_SIDE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.dfp.BracketingNthOrderBrentSolverDFP.C35011.<clinit>():void");
        }
    }

    private Dfp guessX(Dfp dfp, Dfp[] dfpArr, Dfp[] dfpArr2, int i, int i2) {
        int i3;
        int i4 = i;
        while (true) {
            i3 = i2 - 1;
            if (i4 >= i3) {
                break;
            }
            int i5 = i4 + 1;
            int i6 = i5 - i;
            while (i3 > i4) {
                dfpArr[i3] = dfpArr[i3].subtract(dfpArr[i3 - 1]).divide(dfpArr2[i3].subtract(dfpArr2[i3 - i6]));
                i3--;
            }
            i4 = i5;
        }
        Dfp zero = dfp.getZero();
        while (i3 >= i) {
            zero = dfpArr[i3].add(zero.multiply(dfp.subtract(dfpArr2[i3])));
            i3--;
        }
        return zero;
    }
}
