package org.apache.commons.math3.linear;

import java.lang.reflect.Array;
import java.util.Arrays;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.util.FastMath;

public class QRDecomposition {
    private RealMatrix cachedH;
    private RealMatrix cachedQ;
    private RealMatrix cachedQT;
    private RealMatrix cachedR;
    private double[][] qrt;
    private double[] rDiag;
    private final double threshold;

    public QRDecomposition(RealMatrix realMatrix) {
        this(realMatrix, 0.0d);
    }

    public QRDecomposition(RealMatrix realMatrix, double d) {
        this.threshold = d;
        int rowDimension = realMatrix.getRowDimension();
        int columnDimension = realMatrix.getColumnDimension();
        this.qrt = realMatrix.transpose().getData();
        this.rDiag = new double[FastMath.min(rowDimension, columnDimension)];
        this.cachedQ = null;
        this.cachedQT = null;
        this.cachedR = null;
        this.cachedH = null;
        decompose(this.qrt);
    }

    /* access modifiers changed from: protected */
    public void decompose(double[][] dArr) {
        int i = 0;
        while (true) {
            double[][] dArr2 = this.qrt;
            if (i < FastMath.min(dArr2.length, dArr2[0].length)) {
                performHouseholderReflection(i, this.qrt);
                i++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void performHouseholderReflection(int i, double[][] dArr) {
        double[] dArr2 = this.qrt[i];
        double d = 0.0d;
        for (int i2 = i; i2 < dArr2.length; i2++) {
            double d2 = dArr2[i2];
            d += d2 * d2;
        }
        double sqrt = dArr2[i] > 0.0d ? -FastMath.sqrt(d) : FastMath.sqrt(d);
        this.rDiag[i] = sqrt;
        if (sqrt != 0.0d) {
            dArr2[i] = dArr2[i] - sqrt;
            int i3 = i + 1;
            while (true) {
                double[][] dArr3 = this.qrt;
                if (i3 < dArr3.length) {
                    double[] dArr4 = dArr3[i3];
                    double d3 = 0.0d;
                    for (int i4 = i; i4 < dArr4.length; i4++) {
                        d3 -= dArr4[i4] * dArr2[i4];
                    }
                    double d4 = d3 / (dArr2[i] * sqrt);
                    for (int i5 = i; i5 < dArr4.length; i5++) {
                        dArr4[i5] = dArr4[i5] - (dArr2[i5] * d4);
                    }
                    i3++;
                } else {
                    return;
                }
            }
        }
    }

    public RealMatrix getR() {
        if (this.cachedR == null) {
            double[][] dArr = this.qrt;
            int length = dArr.length;
            int length2 = dArr[0].length;
            double[][] dArr2 = (double[][]) Array.newInstance(double.class, length2, length);
            for (int min = FastMath.min(length2, length) - 1; min >= 0; min--) {
                dArr2[min][min] = this.rDiag[min];
                for (int i = min + 1; i < length; i++) {
                    dArr2[min][i] = this.qrt[i][min];
                }
            }
            this.cachedR = MatrixUtils.createRealMatrix(dArr2);
        }
        return this.cachedR;
    }

    public RealMatrix getQ() {
        if (this.cachedQ == null) {
            this.cachedQ = getQT().transpose();
        }
        return this.cachedQ;
    }

    public RealMatrix getQT() {
        double d;
        if (this.cachedQT == null) {
            double[][] dArr = this.qrt;
            int length = dArr.length;
            int length2 = dArr[0].length;
            double[][] dArr2 = (double[][]) Array.newInstance(double.class, length2, length2);
            int i = length2 - 1;
            while (true) {
                d = 1.0d;
                if (i < FastMath.min(length2, length)) {
                    break;
                }
                dArr2[i][i] = 1.0d;
                i--;
            }
            int min = FastMath.min(length2, length) - 1;
            while (min >= 0) {
                double[] dArr3 = this.qrt[min];
                dArr2[min][min] = d;
                if (dArr3[min] != 0.0d) {
                    for (int i2 = min; i2 < length2; i2++) {
                        double d2 = 0.0d;
                        for (int i3 = min; i3 < length2; i3++) {
                            d2 -= dArr2[i2][i3] * dArr3[i3];
                        }
                        double d3 = d2 / (this.rDiag[min] * dArr3[min]);
                        for (int i4 = min; i4 < length2; i4++) {
                            double[] dArr4 = dArr2[i2];
                            dArr4[i4] = dArr4[i4] + ((-d3) * dArr3[i4]);
                        }
                    }
                }
                min--;
                d = 1.0d;
            }
            this.cachedQT = MatrixUtils.createRealMatrix(dArr2);
        }
        return this.cachedQT;
    }

    public RealMatrix getH() {
        int i;
        if (this.cachedH == null) {
            double[][] dArr = this.qrt;
            int length = dArr.length;
            int length2 = dArr[0].length;
            double[][] dArr2 = (double[][]) Array.newInstance(double.class, length2, length);
            int i2 = 0;
            while (i2 < length2) {
                int i3 = 0;
                while (true) {
                    i = i2 + 1;
                    if (i3 >= FastMath.min(i, length)) {
                        break;
                    }
                    dArr2[i2][i3] = this.qrt[i3][i2] / (-this.rDiag[i3]);
                    i3++;
                }
                i2 = i;
            }
            this.cachedH = MatrixUtils.createRealMatrix(dArr2);
        }
        return this.cachedH;
    }

    public DecompositionSolver getSolver() {
        return new Solver(this.qrt, this.rDiag, this.threshold);
    }

    private static class Solver implements DecompositionSolver {
        private final double[][] qrt;
        private final double[] rDiag;
        private final double threshold;

        private Solver(double[][] dArr, double[] dArr2, double d) {
            this.qrt = dArr;
            this.rDiag = dArr2;
            this.threshold = d;
        }

        public boolean isNonSingular() {
            for (double d : this.rDiag) {
                if (FastMath.abs(d) <= this.threshold) {
                    return false;
                }
            }
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void
         arg types: [double[], int]
         candidates:
          org.apache.commons.math3.linear.ArrayRealVector.<init>(int, double):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.RealVector):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, boolean):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, double[]):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.RealVector, org.apache.commons.math3.linear.ArrayRealVector):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], org.apache.commons.math3.linear.ArrayRealVector):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], double[]):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void */
        public RealVector solve(RealVector realVector) {
            double[][] dArr = this.qrt;
            int length = dArr.length;
            int length2 = dArr[0].length;
            if (realVector.getDimension() != length2) {
                throw new DimensionMismatchException(realVector.getDimension(), length2);
            } else if (isNonSingular()) {
                double[] dArr2 = new double[length];
                double[] array = realVector.toArray();
                for (int i = 0; i < FastMath.min(length2, length); i++) {
                    double[] dArr3 = this.qrt[i];
                    double d = 0.0d;
                    for (int i2 = i; i2 < length2; i2++) {
                        d += array[i2] * dArr3[i2];
                    }
                    double d2 = d / (this.rDiag[i] * dArr3[i]);
                    for (int i3 = i; i3 < length2; i3++) {
                        array[i3] = array[i3] + (dArr3[i3] * d2);
                    }
                }
                for (int length3 = this.rDiag.length - 1; length3 >= 0; length3--) {
                    array[length3] = array[length3] / this.rDiag[length3];
                    double d3 = array[length3];
                    double[] dArr4 = this.qrt[length3];
                    dArr2[length3] = d3;
                    for (int i4 = 0; i4 < length3; i4++) {
                        array[i4] = array[i4] - (dArr4[i4] * d3);
                    }
                }
                return new ArrayRealVector(dArr2, false);
            } else {
                throw new SingularMatrixException();
            }
        }

        public RealMatrix solve(RealMatrix realMatrix) {
            double d;
            double[][] dArr = this.qrt;
            int length = dArr.length;
            int length2 = dArr[0].length;
            if (realMatrix.getRowDimension() != length2) {
                throw new DimensionMismatchException(realMatrix.getRowDimension(), length2);
            } else if (isNonSingular()) {
                int columnDimension = realMatrix.getColumnDimension();
                int i = ((columnDimension + 52) - 1) / 52;
                double[][] createBlocksLayout = BlockRealMatrix.createBlocksLayout(length, columnDimension);
                double[][] dArr2 = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), 52);
                double[] dArr3 = new double[52];
                int i2 = 0;
                while (i2 < i) {
                    int i3 = i2 * 52;
                    int min = FastMath.min(i3 + 52, columnDimension);
                    int i4 = min - i3;
                    realMatrix.copySubMatrix(0, length2 - 1, i3, min - 1, dArr2);
                    int i5 = 0;
                    while (true) {
                        d = 1.0d;
                        if (i5 >= FastMath.min(length2, length)) {
                            break;
                        }
                        double[] dArr4 = this.qrt[i5];
                        double d2 = 1.0d / (this.rDiag[i5] * dArr4[i5]);
                        int i6 = length;
                        Arrays.fill(dArr3, 0, i4, 0.0d);
                        int i7 = i5;
                        while (i7 < length2) {
                            double d3 = dArr4[i7];
                            double[] dArr5 = dArr2[i7];
                            int i8 = columnDimension;
                            for (int i9 = 0; i9 < i4; i9++) {
                                dArr3[i9] = dArr3[i9] + (dArr5[i9] * d3);
                            }
                            i7++;
                            columnDimension = i8;
                        }
                        int i10 = columnDimension;
                        for (int i11 = 0; i11 < i4; i11++) {
                            dArr3[i11] = dArr3[i11] * d2;
                        }
                        for (int i12 = i5; i12 < length2; i12++) {
                            double d4 = dArr4[i12];
                            double[] dArr6 = dArr2[i12];
                            for (int i13 = 0; i13 < i4; i13++) {
                                dArr6[i13] = dArr6[i13] + (dArr3[i13] * d4);
                            }
                        }
                        i5++;
                        length = i6;
                        columnDimension = i10;
                    }
                    int i14 = length;
                    int i15 = columnDimension;
                    int length3 = this.rDiag.length - 1;
                    while (length3 >= 0) {
                        int i16 = length3 / 52;
                        int i17 = i16 * 52;
                        double d5 = d / this.rDiag[length3];
                        double[] dArr7 = dArr2[length3];
                        double[] dArr8 = createBlocksLayout[(i16 * i) + i2];
                        int i18 = (length3 - i17) * i4;
                        int i19 = 0;
                        while (i19 < i4) {
                            dArr7[i19] = dArr7[i19] * d5;
                            dArr8[i18] = dArr7[i19];
                            i19++;
                            i18++;
                        }
                        double[] dArr9 = this.qrt[length3];
                        for (int i20 = 0; i20 < length3; i20++) {
                            double d6 = dArr9[i20];
                            double[] dArr10 = dArr2[i20];
                            for (int i21 = 0; i21 < i4; i21++) {
                                dArr10[i21] = dArr10[i21] - (dArr7[i21] * d6);
                            }
                        }
                        length3--;
                        d = 1.0d;
                    }
                    i2++;
                    length = i14;
                    columnDimension = i15;
                }
                return new BlockRealMatrix(length, columnDimension, createBlocksLayout, false);
            } else {
                throw new SingularMatrixException();
            }
        }

        public RealMatrix getInverse() {
            return solve(MatrixUtils.createRealIdentityMatrix(this.qrt[0].length));
        }
    }
}
