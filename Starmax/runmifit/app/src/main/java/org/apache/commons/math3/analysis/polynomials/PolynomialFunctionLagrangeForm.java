package org.apache.commons.math3.analysis.polynomials;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NonMonotonicSequenceException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;

public class PolynomialFunctionLagrangeForm implements UnivariateFunction {
    private double[] coefficients;
    private boolean coefficientsComputed = false;

    /* renamed from: x */
    private final double[] f7957x;

    /* renamed from: y */
    private final double[] f7958y;

    public PolynomialFunctionLagrangeForm(double[] dArr, double[] dArr2) throws DimensionMismatchException, NumberIsTooSmallException, NonMonotonicSequenceException {
        this.f7957x = new double[dArr.length];
        this.f7958y = new double[dArr2.length];
        System.arraycopy(dArr, 0, this.f7957x, 0, dArr.length);
        System.arraycopy(dArr2, 0, this.f7958y, 0, dArr2.length);
        if (!verifyInterpolationArray(dArr, dArr2, false)) {
            MathArrays.sortInPlace(this.f7957x, this.f7958y);
            verifyInterpolationArray(this.f7957x, this.f7958y, true);
        }
    }

    public double value(double d) {
        return evaluateInternal(this.f7957x, this.f7958y, d);
    }

    public int degree() {
        return this.f7957x.length - 1;
    }

    public double[] getInterpolatingPoints() {
        double[] dArr = this.f7957x;
        double[] dArr2 = new double[dArr.length];
        System.arraycopy(dArr, 0, dArr2, 0, dArr.length);
        return dArr2;
    }

    public double[] getInterpolatingValues() {
        double[] dArr = this.f7958y;
        double[] dArr2 = new double[dArr.length];
        System.arraycopy(dArr, 0, dArr2, 0, dArr.length);
        return dArr2;
    }

    public double[] getCoefficients() {
        if (!this.coefficientsComputed) {
            computeCoefficients();
        }
        double[] dArr = this.coefficients;
        double[] dArr2 = new double[dArr.length];
        System.arraycopy(dArr, 0, dArr2, 0, dArr.length);
        return dArr2;
    }

    public static double evaluate(double[] dArr, double[] dArr2, double d) throws DimensionMismatchException, NumberIsTooSmallException, NonMonotonicSequenceException {
        if (verifyInterpolationArray(dArr, dArr2, false)) {
            return evaluateInternal(dArr, dArr2, d);
        }
        double[] dArr3 = new double[dArr.length];
        double[] dArr4 = new double[dArr2.length];
        System.arraycopy(dArr, 0, dArr3, 0, dArr.length);
        System.arraycopy(dArr2, 0, dArr4, 0, dArr2.length);
        MathArrays.sortInPlace(dArr3, dArr4);
        verifyInterpolationArray(dArr3, dArr4, true);
        return evaluateInternal(dArr3, dArr4, d);
    }

    private static double evaluateInternal(double[] dArr, double[] dArr2, double d) {
        int i;
        double d2;
        double[] dArr3 = dArr;
        int length = dArr3.length;
        double[] dArr4 = new double[length];
        double[] dArr5 = new double[length];
        double d3 = Double.POSITIVE_INFINITY;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            dArr4[i3] = dArr2[i3];
            dArr5[i3] = dArr2[i3];
            double abs = FastMath.abs(d - dArr3[i3]);
            if (abs < d3) {
                i2 = i3;
                d3 = abs;
            }
        }
        double d4 = dArr2[i2];
        for (int i4 = 1; i4 < length; i4++) {
            int i5 = 0;
            while (true) {
                i = length - i4;
                if (i5 >= i) {
                    break;
                }
                int i6 = i4 + i5;
                double d5 = dArr3[i5] - dArr3[i6];
                int i7 = i5 + 1;
                double d6 = (dArr4[i7] - dArr5[i5]) / d5;
                dArr4[i5] = (dArr3[i5] - d) * d6;
                dArr5[i5] = (dArr3[i6] - d) * d6;
                i5 = i7;
            }
            double d7 = (double) (i + 1);
            Double.isNaN(d7);
            if (((double) i2) < d7 * 0.5d) {
                d2 = dArr4[i2];
            } else {
                i2--;
                d2 = dArr5[i2];
            }
            d4 += d2;
        }
        return d4;
    }

    /* access modifiers changed from: protected */
    public void computeCoefficients() {
        int degree = degree() + 1;
        this.coefficients = new double[degree];
        for (int i = 0; i < degree; i++) {
            this.coefficients[i] = 0.0d;
        }
        double[] dArr = new double[(degree + 1)];
        dArr[0] = 1.0d;
        int i2 = 0;
        while (i2 < degree) {
            for (int i3 = i2; i3 > 0; i3--) {
                dArr[i3] = dArr[i3 - 1] - (dArr[i3] * this.f7957x[i2]);
            }
            dArr[0] = dArr[0] * (-this.f7957x[i2]);
            i2++;
            dArr[i2] = 1.0d;
        }
        double[] dArr2 = new double[degree];
        for (int i4 = 0; i4 < degree; i4++) {
            double d = 1.0d;
            for (int i5 = 0; i5 < degree; i5++) {
                if (i4 != i5) {
                    double[] dArr3 = this.f7957x;
                    d *= dArr3[i4] - dArr3[i5];
                }
            }
            double d2 = this.f7958y[i4] / d;
            int i6 = degree - 1;
            dArr2[i6] = dArr[degree];
            double[] dArr4 = this.coefficients;
            dArr4[i6] = dArr4[i6] + (dArr2[i6] * d2);
            for (int i7 = degree - 2; i7 >= 0; i7--) {
                int i8 = i7 + 1;
                dArr2[i7] = dArr[i8] + (dArr2[i8] * this.f7957x[i4]);
                double[] dArr5 = this.coefficients;
                dArr5[i7] = dArr5[i7] + (dArr2[i7] * d2);
            }
        }
        this.coefficientsComputed = true;
    }

    public static boolean verifyInterpolationArray(double[] dArr, double[] dArr2, boolean z) throws DimensionMismatchException, NumberIsTooSmallException, NonMonotonicSequenceException {
        if (dArr.length != dArr2.length) {
            throw new DimensionMismatchException(dArr.length, dArr2.length);
        } else if (dArr.length >= 2) {
            return MathArrays.checkOrder(dArr, MathArrays.OrderDirection.INCREASING, true, z);
        } else {
            throw new NumberIsTooSmallException(LocalizedFormats.WRONG_NUMBER_OF_POINTS, 2, Integer.valueOf(dArr.length), true);
        }
    }
}
