package org.apache.commons.math3.geometry.partitioning;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import org.apache.commons.math3.exception.MathInternalError;
import org.apache.commons.math3.geometry.Point;
import org.apache.commons.math3.geometry.Space;
import org.apache.commons.math3.geometry.Vector;
import org.apache.commons.math3.geometry.partitioning.BSPTreeVisitor;
import org.apache.commons.math3.geometry.partitioning.Region;
import org.apache.commons.math3.geometry.partitioning.SubHyperplane;

public abstract class AbstractRegion<S extends Space, T extends Space> implements Region<S> {
    private Point<S> barycenter;
    private double size;
    private final double tolerance;
    private BSPTree<S> tree;

    public abstract AbstractRegion<S, T> buildNew(BSPTree bSPTree);

    /* access modifiers changed from: protected */
    public abstract void computeGeometricalProperties();

    protected AbstractRegion(double d) {
        this.tree = new BSPTree<>(Boolean.TRUE);
        this.tolerance = d;
    }

    protected AbstractRegion(BSPTree bSPTree, double d) {
        this.tree = bSPTree;
        this.tolerance = d;
    }

    protected AbstractRegion(Collection collection, double d) {
        this.tolerance = d;
        if (collection.size() == 0) {
            this.tree = new BSPTree<>(Boolean.TRUE);
            return;
        }
        TreeSet treeSet = new TreeSet(new Comparator<SubHyperplane<S>>() {
            /* class org.apache.commons.math3.geometry.partitioning.AbstractRegion.C35311 */

            public int compare(SubHyperplane<S> subHyperplane, SubHyperplane<S> subHyperplane2) {
                if (subHyperplane2.getSize() < subHyperplane.getSize()) {
                    return -1;
                }
                return subHyperplane == subHyperplane2 ? 0 : 1;
            }
        });
        treeSet.addAll(collection);
        this.tree = new BSPTree<>();
        insertCuts(this.tree, treeSet);
        this.tree.visit(new BSPTreeVisitor<S>() {
            /* class org.apache.commons.math3.geometry.partitioning.AbstractRegion.C35322 */

            public void visitInternalNode(BSPTree<S> bSPTree) {
            }

            public BSPTreeVisitor.Order visitOrder(BSPTree<S> bSPTree) {
                return BSPTreeVisitor.Order.PLUS_SUB_MINUS;
            }

            public void visitLeafNode(BSPTree<S> bSPTree) {
                if (bSPTree.getParent() == null || bSPTree == bSPTree.getParent().getMinus()) {
                    bSPTree.setAttribute(Boolean.TRUE);
                } else {
                    bSPTree.setAttribute(Boolean.FALSE);
                }
            }
        });
    }

    public AbstractRegion(Hyperplane[] hyperplaneArr, double d) {
        this.tolerance = d;
        if (hyperplaneArr == null || hyperplaneArr.length == 0) {
            this.tree = new BSPTree<>(Boolean.FALSE);
            return;
        }
        this.tree = hyperplaneArr[0].wholeSpace().getTree(false);
        BSPTree<S> bSPTree = this.tree;
        bSPTree.setAttribute(Boolean.TRUE);
        for (Hyperplane hyperplane : hyperplaneArr) {
            if (bSPTree.insertCut(hyperplane)) {
                bSPTree.setAttribute(null);
                bSPTree.getPlus().setAttribute(Boolean.FALSE);
                bSPTree = bSPTree.getMinus();
                bSPTree.setAttribute(Boolean.TRUE);
            }
        }
    }

    public double getTolerance() {
        return this.tolerance;
    }

    private void insertCuts(BSPTree<S> bSPTree, Collection<SubHyperplane<S>> collection) {
        Hyperplane hyperplane;
        Iterator<SubHyperplane<S>> it = collection.iterator();
        loop0:
        while (true) {
            hyperplane = null;
            while (true) {
                if (hyperplane == null && it.hasNext()) {
                    hyperplane = it.next().getHyperplane();
                    if (!bSPTree.insertCut(hyperplane.copySelf())) {
                    }
                }
            }
        }
        if (it.hasNext()) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            while (it.hasNext()) {
                SubHyperplane next = it.next();
                int i = C35333.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side[next.side(hyperplane).ordinal()];
                if (i == 1) {
                    arrayList.add(next);
                } else if (i == 2) {
                    arrayList2.add(next);
                } else if (i == 3) {
                    SubHyperplane.SplitSubHyperplane split = next.split(hyperplane);
                    arrayList.add(split.getPlus());
                    arrayList2.add(split.getMinus());
                }
            }
            insertCuts(bSPTree.getPlus(), arrayList);
            insertCuts(bSPTree.getMinus(), arrayList2);
        }
    }

    /* renamed from: org.apache.commons.math3.geometry.partitioning.AbstractRegion$3 */
    static /* synthetic */ class C35333 {
        static final /* synthetic */ int[] $SwitchMap$org$apache$commons$math3$geometry$partitioning$Side = new int[Side.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                org.apache.commons.math3.geometry.partitioning.Side[] r0 = org.apache.commons.math3.geometry.partitioning.Side.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.geometry.partitioning.AbstractRegion.C35333.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side = r0
                int[] r0 = org.apache.commons.math3.geometry.partitioning.AbstractRegion.C35333.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.geometry.partitioning.Side r1 = org.apache.commons.math3.geometry.partitioning.Side.PLUS     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.apache.commons.math3.geometry.partitioning.AbstractRegion.C35333.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.geometry.partitioning.Side r1 = org.apache.commons.math3.geometry.partitioning.Side.MINUS     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = org.apache.commons.math3.geometry.partitioning.AbstractRegion.C35333.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side     // Catch:{ NoSuchFieldError -> 0x002a }
                org.apache.commons.math3.geometry.partitioning.Side r1 = org.apache.commons.math3.geometry.partitioning.Side.BOTH     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.geometry.partitioning.AbstractRegion.C35333.<clinit>():void");
        }
    }

    public AbstractRegion<S, T> copySelf() {
        return buildNew((BSPTree) this.tree.copySelf());
    }

    public boolean isEmpty() {
        return isEmpty(this.tree);
    }

    public boolean isEmpty(BSPTree<S> bSPTree) {
        if (bSPTree.getCut() == null) {
            return !((Boolean) bSPTree.getAttribute()).booleanValue();
        }
        if (!isEmpty(bSPTree.getMinus()) || !isEmpty(bSPTree.getPlus())) {
            return false;
        }
        return true;
    }

    public boolean isFull() {
        return isFull(this.tree);
    }

    public boolean isFull(BSPTree<S> bSPTree) {
        if (bSPTree.getCut() == null) {
            return ((Boolean) bSPTree.getAttribute()).booleanValue();
        }
        return isFull(bSPTree.getMinus()) && isFull(bSPTree.getPlus());
    }

    public boolean contains(Region<S> region) {
        return new RegionFactory().difference(region, this).isEmpty();
    }

    public BoundaryProjection<S> projectToBoundary(Point<S> point) {
        BoundaryProjector boundaryProjector = new BoundaryProjector(point);
        getTree(true).visit(boundaryProjector);
        return boundaryProjector.getProjection();
    }

    public Region.Location checkPoint(Vector<S> vector) {
        return checkPoint((Point) vector);
    }

    public Region.Location checkPoint(Point<S> point) {
        return checkPoint(this.tree, point);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.geometry.partitioning.AbstractRegion.checkPoint(org.apache.commons.math3.geometry.partitioning.BSPTree, org.apache.commons.math3.geometry.Point):org.apache.commons.math3.geometry.partitioning.Region$Location
     arg types: [org.apache.commons.math3.geometry.partitioning.BSPTree<S>, org.apache.commons.math3.geometry.Vector<S>]
     candidates:
      org.apache.commons.math3.geometry.partitioning.AbstractRegion.checkPoint(org.apache.commons.math3.geometry.partitioning.BSPTree, org.apache.commons.math3.geometry.Vector):org.apache.commons.math3.geometry.partitioning.Region$Location
      org.apache.commons.math3.geometry.partitioning.AbstractRegion.checkPoint(org.apache.commons.math3.geometry.partitioning.BSPTree, org.apache.commons.math3.geometry.Point):org.apache.commons.math3.geometry.partitioning.Region$Location */
    /* access modifiers changed from: protected */
    public Region.Location checkPoint(BSPTree<S> bSPTree, Vector<S> vector) {
        return checkPoint((BSPTree) bSPTree, (Point) vector);
    }

    /* access modifiers changed from: protected */
    public Region.Location checkPoint(BSPTree<S> bSPTree, Point<S> point) {
        BSPTree<S> cell = bSPTree.getCell(point, this.tolerance);
        if (cell.getCut() == null) {
            return ((Boolean) cell.getAttribute()).booleanValue() ? Region.Location.INSIDE : Region.Location.OUTSIDE;
        }
        Region.Location checkPoint = checkPoint(cell.getMinus(), point);
        return checkPoint == checkPoint(cell.getPlus(), point) ? checkPoint : Region.Location.BOUNDARY;
    }

    public BSPTree<S> getTree(boolean z) {
        if (z && this.tree.getCut() != null && this.tree.getAttribute() == null) {
            this.tree.visit(new BoundaryBuilder());
        }
        return this.tree;
    }

    private static class BoundaryBuilder<S extends Space> implements BSPTreeVisitor<S> {
        public void visitLeafNode(BSPTree<S> bSPTree) {
        }

        private BoundaryBuilder() {
        }

        public BSPTreeVisitor.Order visitOrder(BSPTree<S> bSPTree) {
            return BSPTreeVisitor.Order.PLUS_MINUS_SUB;
        }

        public void visitInternalNode(BSPTree<S> bSPTree) {
            SubHyperplane subHyperplane;
            SubHyperplane[] subHyperplaneArr;
            SubHyperplane[] subHyperplaneArr2 = (SubHyperplane[]) Array.newInstance(SubHyperplane.class, 2);
            characterize(bSPTree.getPlus(), bSPTree.getCut().copySelf(), subHyperplaneArr2);
            SubHyperplane subHyperplane2 = null;
            if (subHyperplaneArr2[0] != null && !subHyperplaneArr2[0].isEmpty()) {
                SubHyperplane[] subHyperplaneArr3 = (SubHyperplane[]) Array.newInstance(SubHyperplane.class, 2);
                characterize(bSPTree.getMinus(), subHyperplaneArr2[0], subHyperplaneArr3);
                if (subHyperplaneArr3[1] != null && !subHyperplaneArr3[1].isEmpty()) {
                    subHyperplane = subHyperplaneArr3[1];
                    if (subHyperplaneArr2[1] != null && !subHyperplaneArr2[1].isEmpty()) {
                        subHyperplaneArr = (SubHyperplane[]) Array.newInstance(SubHyperplane.class, 2);
                        characterize(bSPTree.getMinus(), subHyperplaneArr2[1], subHyperplaneArr);
                        if (subHyperplaneArr[0] != null && !subHyperplaneArr[0].isEmpty()) {
                            subHyperplane2 = subHyperplaneArr[0];
                        }
                    }
                    bSPTree.setAttribute(new BoundaryAttribute(subHyperplane, subHyperplane2));
                }
            }
            subHyperplane = null;
            subHyperplaneArr = (SubHyperplane[]) Array.newInstance(SubHyperplane.class, 2);
            characterize(bSPTree.getMinus(), subHyperplaneArr2[1], subHyperplaneArr);
            subHyperplane2 = subHyperplaneArr[0];
            bSPTree.setAttribute(new BoundaryAttribute(subHyperplane, subHyperplane2));
        }

        private void characterize(BSPTree<S> bSPTree, SubHyperplane<S> subHyperplane, SubHyperplane<S>[] subHyperplaneArr) {
            if (bSPTree.getCut() != null) {
                Hyperplane<S> hyperplane = bSPTree.getCut().getHyperplane();
                int i = C35333.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side[subHyperplane.side(hyperplane).ordinal()];
                if (i == 1) {
                    characterize(bSPTree.getPlus(), subHyperplane, subHyperplaneArr);
                } else if (i == 2) {
                    characterize(bSPTree.getMinus(), subHyperplane, subHyperplaneArr);
                } else if (i == 3) {
                    SubHyperplane.SplitSubHyperplane<S> split = subHyperplane.split(hyperplane);
                    characterize(bSPTree.getPlus(), split.getPlus(), subHyperplaneArr);
                    characterize(bSPTree.getMinus(), split.getMinus(), subHyperplaneArr);
                } else {
                    throw new MathInternalError();
                }
            } else if (((Boolean) bSPTree.getAttribute()).booleanValue()) {
                if (subHyperplaneArr[1] == null) {
                    subHyperplaneArr[1] = subHyperplane;
                } else {
                    subHyperplaneArr[1] = subHyperplaneArr[1].reunite(subHyperplane);
                }
            } else if (subHyperplaneArr[0] == null) {
                subHyperplaneArr[0] = subHyperplane;
            } else {
                subHyperplaneArr[0] = subHyperplaneArr[0].reunite(subHyperplane);
            }
        }
    }

    public double getBoundarySize() {
        BoundarySizeVisitor boundarySizeVisitor = new BoundarySizeVisitor();
        getTree(true).visit(boundarySizeVisitor);
        return boundarySizeVisitor.getSize();
    }

    public double getSize() {
        if (this.barycenter == null) {
            computeGeometricalProperties();
        }
        return this.size;
    }

    /* access modifiers changed from: protected */
    public void setSize(double d) {
        this.size = d;
    }

    public Point<S> getBarycenter() {
        if (this.barycenter == null) {
            computeGeometricalProperties();
        }
        return this.barycenter;
    }

    /* access modifiers changed from: protected */
    public void setBarycenter(Vector vector) {
        setBarycenter((Point) vector);
    }

    /* access modifiers changed from: protected */
    public void setBarycenter(Point point) {
        this.barycenter = point;
    }

    public Side side(Hyperplane<S> hyperplane) {
        Sides sides = new Sides();
        recurseSides(this.tree, hyperplane.wholeHyperplane(), sides);
        return sides.plusFound() ? sides.minusFound() ? Side.BOTH : Side.PLUS : sides.minusFound() ? Side.MINUS : Side.HYPER;
    }

    private void recurseSides(BSPTree<S> bSPTree, SubHyperplane<S> subHyperplane, Sides sides) {
        if (bSPTree.getCut() != null) {
            Hyperplane<S> hyperplane = bSPTree.getCut().getHyperplane();
            int i = C35333.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side[subHyperplane.side(hyperplane).ordinal()];
            if (i == 1) {
                if (bSPTree.getCut().side(subHyperplane.getHyperplane()) == Side.PLUS) {
                    if (!isEmpty(bSPTree.getMinus())) {
                        sides.rememberPlusFound();
                    }
                } else if (!isEmpty(bSPTree.getMinus())) {
                    sides.rememberMinusFound();
                }
                if (!sides.plusFound() || !sides.minusFound()) {
                    recurseSides(bSPTree.getPlus(), subHyperplane, sides);
                }
            } else if (i == 2) {
                if (bSPTree.getCut().side(subHyperplane.getHyperplane()) == Side.PLUS) {
                    if (!isEmpty(bSPTree.getPlus())) {
                        sides.rememberPlusFound();
                    }
                } else if (!isEmpty(bSPTree.getPlus())) {
                    sides.rememberMinusFound();
                }
                if (!sides.plusFound() || !sides.minusFound()) {
                    recurseSides(bSPTree.getMinus(), subHyperplane, sides);
                }
            } else if (i == 3) {
                SubHyperplane.SplitSubHyperplane<S> split = subHyperplane.split(hyperplane);
                recurseSides(bSPTree.getPlus(), split.getPlus(), sides);
                if (!sides.plusFound() || !sides.minusFound()) {
                    recurseSides(bSPTree.getMinus(), split.getMinus(), sides);
                }
            } else if (bSPTree.getCut().getHyperplane().sameOrientationAs(subHyperplane.getHyperplane())) {
                if (bSPTree.getPlus().getCut() != null || ((Boolean) bSPTree.getPlus().getAttribute()).booleanValue()) {
                    sides.rememberPlusFound();
                }
                if (bSPTree.getMinus().getCut() != null || ((Boolean) bSPTree.getMinus().getAttribute()).booleanValue()) {
                    sides.rememberMinusFound();
                }
            } else {
                if (bSPTree.getPlus().getCut() != null || ((Boolean) bSPTree.getPlus().getAttribute()).booleanValue()) {
                    sides.rememberMinusFound();
                }
                if (bSPTree.getMinus().getCut() != null || ((Boolean) bSPTree.getMinus().getAttribute()).booleanValue()) {
                    sides.rememberPlusFound();
                }
            }
        } else if (((Boolean) bSPTree.getAttribute()).booleanValue()) {
            sides.rememberPlusFound();
            sides.rememberMinusFound();
        }
    }

    private static final class Sides {
        private boolean minusFound = false;
        private boolean plusFound = false;

        public void rememberPlusFound() {
            this.plusFound = true;
        }

        public boolean plusFound() {
            return this.plusFound;
        }

        public void rememberMinusFound() {
            this.minusFound = true;
        }

        public boolean minusFound() {
            return this.minusFound;
        }
    }

    public SubHyperplane<S> intersection(SubHyperplane<S> subHyperplane) {
        return recurseIntersection(this.tree, subHyperplane);
    }

    private SubHyperplane<S> recurseIntersection(BSPTree<S> bSPTree, SubHyperplane<S> subHyperplane) {
        if (bSPTree.getCut() != null) {
            Hyperplane<S> hyperplane = bSPTree.getCut().getHyperplane();
            int i = C35333.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side[subHyperplane.side(hyperplane).ordinal()];
            if (i == 1) {
                return recurseIntersection(bSPTree.getPlus(), subHyperplane);
            }
            if (i == 2) {
                return recurseIntersection(bSPTree.getMinus(), subHyperplane);
            }
            if (i != 3) {
                return recurseIntersection(bSPTree.getPlus(), recurseIntersection(bSPTree.getMinus(), subHyperplane));
            }
            SubHyperplane.SplitSubHyperplane<S> split = subHyperplane.split(hyperplane);
            SubHyperplane<S> recurseIntersection = recurseIntersection(bSPTree.getPlus(), split.getPlus());
            SubHyperplane<S> recurseIntersection2 = recurseIntersection(bSPTree.getMinus(), split.getMinus());
            if (recurseIntersection == null) {
                return recurseIntersection2;
            }
            if (recurseIntersection2 == null) {
                return recurseIntersection;
            }
            return recurseIntersection.reunite(recurseIntersection2);
        } else if (((Boolean) bSPTree.getAttribute()).booleanValue()) {
            return subHyperplane.copySelf();
        } else {
            return null;
        }
    }

    public AbstractRegion<S, T> applyTransform(Transform<S, T> transform) {
        return buildNew((BSPTree) recurseTransform(getTree(false), transform));
    }

    private BSPTree<S> recurseTransform(BSPTree<S> bSPTree, Transform<S, T> transform) {
        if (bSPTree.getCut() == null) {
            return new BSPTree<>(bSPTree.getAttribute());
        }
        AbstractSubHyperplane<S, T> applyTransform = ((AbstractSubHyperplane) bSPTree.getCut()).applyTransform(transform);
        BoundaryAttribute boundaryAttribute = (BoundaryAttribute) bSPTree.getAttribute();
        if (boundaryAttribute != null) {
            AbstractSubHyperplane<S, T> abstractSubHyperplane = null;
            AbstractSubHyperplane<S, T> applyTransform2 = boundaryAttribute.getPlusOutside() == null ? null : ((AbstractSubHyperplane) boundaryAttribute.getPlusOutside()).applyTransform(transform);
            if (boundaryAttribute.getPlusInside() != null) {
                abstractSubHyperplane = ((AbstractSubHyperplane) boundaryAttribute.getPlusInside()).applyTransform(transform);
            }
            boundaryAttribute = new BoundaryAttribute(applyTransform2, abstractSubHyperplane);
        }
        return new BSPTree<>(applyTransform, recurseTransform(bSPTree.getPlus(), transform), recurseTransform(bSPTree.getMinus(), transform), boundaryAttribute);
    }
}
