package org.apache.commons.math3.linear;

import com.google.common.base.Ascii;
import java.lang.reflect.Array;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.MathUnsupportedOperationException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Precision;

public class EigenDecomposition {
    private static final double EPSILON = 1.0E-12d;
    private RealMatrix cachedD;
    private RealMatrix cachedV;
    private RealMatrix cachedVt;
    private ArrayRealVector[] eigenvectors;
    private double[] imagEigenvalues;
    private final boolean isSymmetric;
    private double[] main;
    private byte maxIter;
    private double[] realEigenvalues;
    private double[] secondary;
    private TriDiagonalTransformer transformer;

    public EigenDecomposition(RealMatrix realMatrix) throws MathArithmeticException {
        this.maxIter = Ascii.f4511RS;
        double rowDimension = (double) (realMatrix.getRowDimension() * 10 * realMatrix.getColumnDimension());
        double d = Precision.EPSILON;
        Double.isNaN(rowDimension);
        this.isSymmetric = MatrixUtils.isSymmetric(realMatrix, rowDimension * d);
        if (this.isSymmetric) {
            transformToTridiagonal(realMatrix);
            findEigenVectors(this.transformer.getQ().getData());
            return;
        }
        findEigenVectorsFromSchur(transformToSchur(realMatrix));
    }

    @Deprecated
    public EigenDecomposition(RealMatrix realMatrix, double d) throws MathArithmeticException {
        this(realMatrix);
    }

    public EigenDecomposition(double[] dArr, double[] dArr2) {
        this.maxIter = Ascii.f4511RS;
        this.isSymmetric = true;
        this.main = (double[]) dArr.clone();
        this.secondary = (double[]) dArr2.clone();
        this.transformer = null;
        int length = dArr.length;
        double[][] dArr3 = (double[][]) Array.newInstance(double.class, length, length);
        for (int i = 0; i < length; i++) {
            dArr3[i][i] = 1.0d;
        }
        findEigenVectors(dArr3);
    }

    @Deprecated
    public EigenDecomposition(double[] dArr, double[] dArr2, double d) {
        this(dArr, dArr2);
    }

    public RealMatrix getV() {
        if (this.cachedV == null) {
            int length = this.eigenvectors.length;
            this.cachedV = MatrixUtils.createRealMatrix(length, length);
            for (int i = 0; i < length; i++) {
                this.cachedV.setColumnVector(i, this.eigenvectors[i]);
            }
        }
        return this.cachedV;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.Precision.compareTo(double, double, double):int
     arg types: [double, int, int]
     candidates:
      org.apache.commons.math3.util.Precision.compareTo(double, double, int):int
      org.apache.commons.math3.util.Precision.compareTo(double, double, double):int */
    public RealMatrix getD() {
        if (this.cachedD == null) {
            this.cachedD = MatrixUtils.createRealDiagonalMatrix(this.realEigenvalues);
            int i = 0;
            while (true) {
                double[] dArr = this.imagEigenvalues;
                if (i >= dArr.length) {
                    break;
                }
                if (Precision.compareTo(dArr[i], 0.0d, 1.0E-12d) > 0) {
                    this.cachedD.setEntry(i, i + 1, this.imagEigenvalues[i]);
                } else if (Precision.compareTo(this.imagEigenvalues[i], 0.0d, 1.0E-12d) < 0) {
                    this.cachedD.setEntry(i, i - 1, this.imagEigenvalues[i]);
                }
                i++;
            }
        }
        return this.cachedD;
    }

    public RealMatrix getVT() {
        if (this.cachedVt == null) {
            int length = this.eigenvectors.length;
            this.cachedVt = MatrixUtils.createRealMatrix(length, length);
            for (int i = 0; i < length; i++) {
                this.cachedVt.setRowVector(i, this.eigenvectors[i]);
            }
        }
        return this.cachedVt;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.Precision.equals(double, double, double):boolean
     arg types: [double, int, int]
     candidates:
      org.apache.commons.math3.util.Precision.equals(double, double, int):boolean
      org.apache.commons.math3.util.Precision.equals(float, float, float):boolean
      org.apache.commons.math3.util.Precision.equals(float, float, int):boolean
      org.apache.commons.math3.util.Precision.equals(double, double, double):boolean */
    public boolean hasComplexEigenvalues() {
        int i = 0;
        while (true) {
            double[] dArr = this.imagEigenvalues;
            if (i >= dArr.length) {
                return false;
            }
            if (!Precision.equals(dArr[i], 0.0d, 1.0E-12d)) {
                return true;
            }
            i++;
        }
    }

    public double[] getRealEigenvalues() {
        return (double[]) this.realEigenvalues.clone();
    }

    public double getRealEigenvalue(int i) {
        return this.realEigenvalues[i];
    }

    public double[] getImagEigenvalues() {
        return (double[]) this.imagEigenvalues.clone();
    }

    public double getImagEigenvalue(int i) {
        return this.imagEigenvalues[i];
    }

    public RealVector getEigenvector(int i) {
        return this.eigenvectors[i].copy();
    }

    public double getDeterminant() {
        double d = 1.0d;
        for (double d2 : this.realEigenvalues) {
            d *= d2;
        }
        return d;
    }

    public RealMatrix getSquareRoot() {
        if (this.isSymmetric) {
            double[] dArr = new double[this.realEigenvalues.length];
            int i = 0;
            while (true) {
                double[] dArr2 = this.realEigenvalues;
                if (i < dArr2.length) {
                    double d = dArr2[i];
                    if (d > 0.0d) {
                        dArr[i] = FastMath.sqrt(d);
                        i++;
                    } else {
                        throw new MathUnsupportedOperationException();
                    }
                } else {
                    RealMatrix createRealDiagonalMatrix = MatrixUtils.createRealDiagonalMatrix(dArr);
                    RealMatrix v = getV();
                    return v.multiply(createRealDiagonalMatrix).multiply(getVT());
                }
            }
        } else {
            throw new MathUnsupportedOperationException();
        }
    }

    public DecompositionSolver getSolver() {
        if (!hasComplexEigenvalues()) {
            return new Solver(this.realEigenvalues, this.imagEigenvalues, this.eigenvectors);
        }
        throw new MathUnsupportedOperationException();
    }

    private static class Solver implements DecompositionSolver {
        private final ArrayRealVector[] eigenvectors;
        private double[] imagEigenvalues;
        private double[] realEigenvalues;

        private Solver(double[] dArr, double[] dArr2, ArrayRealVector[] arrayRealVectorArr) {
            this.realEigenvalues = dArr;
            this.imagEigenvalues = dArr2;
            this.eigenvectors = arrayRealVectorArr;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void
         arg types: [double[], int]
         candidates:
          org.apache.commons.math3.linear.ArrayRealVector.<init>(int, double):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.RealVector):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, boolean):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, double[]):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.RealVector, org.apache.commons.math3.linear.ArrayRealVector):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], org.apache.commons.math3.linear.ArrayRealVector):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], double[]):void
          org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void */
        public RealVector solve(RealVector realVector) {
            if (isNonSingular()) {
                int length = this.realEigenvalues.length;
                if (realVector.getDimension() == length) {
                    double[] dArr = new double[length];
                    for (int i = 0; i < length; i++) {
                        ArrayRealVector arrayRealVector = this.eigenvectors[i];
                        double[] dataRef = arrayRealVector.getDataRef();
                        double dotProduct = arrayRealVector.dotProduct(realVector) / this.realEigenvalues[i];
                        for (int i2 = 0; i2 < length; i2++) {
                            dArr[i2] = dArr[i2] + (dataRef[i2] * dotProduct);
                        }
                    }
                    return new ArrayRealVector(dArr, false);
                }
                throw new DimensionMismatchException(realVector.getDimension(), length);
            }
            throw new SingularMatrixException();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
         arg types: [double[][], int]
         candidates:
          org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
          org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
        public RealMatrix solve(RealMatrix realMatrix) {
            if (isNonSingular()) {
                int length = this.realEigenvalues.length;
                if (realMatrix.getRowDimension() == length) {
                    int columnDimension = realMatrix.getColumnDimension();
                    double[][] dArr = (double[][]) Array.newInstance(double.class, length, columnDimension);
                    double[] dArr2 = new double[length];
                    for (int i = 0; i < columnDimension; i++) {
                        for (int i2 = 0; i2 < length; i2++) {
                            dArr2[i2] = realMatrix.getEntry(i2, i);
                            dArr[i2][i] = 0.0d;
                        }
                        for (int i3 = 0; i3 < length; i3++) {
                            ArrayRealVector arrayRealVector = this.eigenvectors[i3];
                            double[] dataRef = arrayRealVector.getDataRef();
                            double d = 0.0d;
                            for (int i4 = 0; i4 < length; i4++) {
                                d += arrayRealVector.getEntry(i4) * dArr2[i4];
                            }
                            double d2 = d / this.realEigenvalues[i3];
                            for (int i5 = 0; i5 < length; i5++) {
                                double[] dArr3 = dArr[i5];
                                dArr3[i] = dArr3[i] + (dataRef[i5] * d2);
                            }
                        }
                    }
                    return new Array2DRowRealMatrix(dArr, false);
                }
                throw new DimensionMismatchException(realMatrix.getRowDimension(), length);
            }
            throw new SingularMatrixException();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.apache.commons.math3.util.Precision.equals(double, double, double):boolean
         arg types: [double, int, int]
         candidates:
          org.apache.commons.math3.util.Precision.equals(double, double, int):boolean
          org.apache.commons.math3.util.Precision.equals(float, float, float):boolean
          org.apache.commons.math3.util.Precision.equals(float, float, int):boolean
          org.apache.commons.math3.util.Precision.equals(double, double, double):boolean */
        public boolean isNonSingular() {
            double d = 0.0d;
            for (int i = 0; i < this.realEigenvalues.length; i++) {
                d = FastMath.max(d, eigenvalueNorm(i));
            }
            if (d == 0.0d) {
                return false;
            }
            for (int i2 = 0; i2 < this.realEigenvalues.length; i2++) {
                if (Precision.equals(eigenvalueNorm(i2) / d, 0.0d, 1.0E-12d)) {
                    return false;
                }
            }
            return true;
        }

        private double eigenvalueNorm(int i) {
            double d = this.realEigenvalues[i];
            double d2 = this.imagEigenvalues[i];
            return FastMath.sqrt((d * d) + (d2 * d2));
        }

        public RealMatrix getInverse() {
            if (isNonSingular()) {
                int length = this.realEigenvalues.length;
                double[][] dArr = (double[][]) Array.newInstance(double.class, length, length);
                for (int i = 0; i < length; i++) {
                    double[] dArr2 = dArr[i];
                    for (int i2 = 0; i2 < length; i2++) {
                        double d = 0.0d;
                        for (int i3 = 0; i3 < length; i3++) {
                            double[] dataRef = this.eigenvectors[i3].getDataRef();
                            d += (dataRef[i] * dataRef[i2]) / this.realEigenvalues[i3];
                        }
                        dArr2[i2] = d;
                    }
                }
                return MatrixUtils.createRealMatrix(dArr);
            }
            throw new SingularMatrixException();
        }
    }

    private void transformToTridiagonal(RealMatrix realMatrix) {
        this.transformer = new TriDiagonalTransformer(realMatrix);
        this.main = this.transformer.getMainDiagonalRef();
        this.secondary = this.transformer.getSecondaryDiagonalRef();
    }

    private void findEigenVectors(double[][] dArr) {
        int i;
        int i2;
        double d;
        double d2;
        double d3;
        double d4;
        double d5;
        double[][] dArr2 = (double[][]) dArr.clone();
        int length = this.main.length;
        this.realEigenvalues = new double[length];
        this.imagEigenvalues = new double[length];
        double[] dArr3 = new double[length];
        int i3 = 0;
        while (true) {
            i = length - 1;
            if (i3 >= i) {
                break;
            }
            this.realEigenvalues[i3] = this.main[i3];
            dArr3[i3] = this.secondary[i3];
            i3++;
        }
        this.realEigenvalues[i] = this.main[i];
        dArr3[i] = 0.0d;
        double d6 = 0.0d;
        for (int i4 = 0; i4 < length; i4++) {
            if (FastMath.abs(this.realEigenvalues[i4]) > d6) {
                d6 = FastMath.abs(this.realEigenvalues[i4]);
            }
            if (FastMath.abs(dArr3[i4]) > d6) {
                d6 = FastMath.abs(dArr3[i4]);
            }
        }
        if (d6 != 0.0d) {
            for (int i5 = 0; i5 < length; i5++) {
                if (FastMath.abs(this.realEigenvalues[i5]) <= Precision.EPSILON * d6) {
                    this.realEigenvalues[i5] = 0.0d;
                }
                if (FastMath.abs(dArr3[i5]) <= Precision.EPSILON * d6) {
                    dArr3[i5] = 0.0d;
                }
            }
        }
        for (int i6 = 0; i6 < length; i6++) {
            int i7 = 0;
            do {
                i2 = i6;
                while (i2 < i) {
                    int i8 = i2 + 1;
                    double abs = FastMath.abs(this.realEigenvalues[i2]) + FastMath.abs(this.realEigenvalues[i8]);
                    if (FastMath.abs(dArr3[i2]) + abs == abs) {
                        break;
                    }
                    i2 = i8;
                }
                if (i2 != i6) {
                    if (i7 != this.maxIter) {
                        i7++;
                        double[] dArr4 = this.realEigenvalues;
                        double d7 = (dArr4[i6 + 1] - dArr4[i6]) / (dArr3[i6] * 2.0d);
                        double sqrt = FastMath.sqrt((d7 * d7) + 1.0d);
                        if (d7 < 0.0d) {
                            double[] dArr5 = this.realEigenvalues;
                            d2 = dArr5[i2] - dArr5[i6];
                            d = dArr3[i6];
                            d3 = d7 - sqrt;
                        } else {
                            double[] dArr6 = this.realEigenvalues;
                            d2 = dArr6[i2] - dArr6[i6];
                            d = dArr3[i6];
                            d3 = d7 + sqrt;
                        }
                        double d8 = d2 + (d / d3);
                        int i9 = i2 - 1;
                        double d9 = 0.0d;
                        double d10 = sqrt;
                        double d11 = 1.0d;
                        double d12 = 1.0d;
                        while (true) {
                            if (i9 < i6) {
                                break;
                            }
                            double d13 = d11 * dArr3[i9];
                            double d14 = d12 * dArr3[i9];
                            if (FastMath.abs(d13) >= FastMath.abs(d8)) {
                                double d15 = d8 / d13;
                                d4 = FastMath.sqrt((d15 * d15) + 1.0d);
                                dArr3[i9 + 1] = d13 * d4;
                                d11 = 1.0d / d4;
                                d5 = d15 * d11;
                            } else {
                                double d16 = d13 / d8;
                                double sqrt2 = FastMath.sqrt((d16 * d16) + 1.0d);
                                dArr3[i9 + 1] = d8 * sqrt2;
                                double d17 = 1.0d / sqrt2;
                                d11 = d16 * d17;
                                double d18 = sqrt2;
                                d5 = d17;
                                d4 = d18;
                            }
                            int i10 = i9 + 1;
                            if (dArr3[i10] == 0.0d) {
                                double[] dArr7 = this.realEigenvalues;
                                dArr7[i10] = dArr7[i10] - d9;
                                dArr3[i2] = 0.0d;
                                d10 = d4;
                                break;
                            }
                            double[] dArr8 = this.realEigenvalues;
                            double d19 = dArr8[i10] - d9;
                            double d20 = ((dArr8[i9] - d19) * d11) + (d5 * 2.0d * d14);
                            double d21 = d11 * d20;
                            dArr8[i10] = d19 + d21;
                            d8 = (d5 * d20) - d14;
                            for (int i11 = 0; i11 < length; i11++) {
                                double d22 = dArr2[i11][i10];
                                dArr2[i11][i10] = (dArr2[i11][i9] * d11) + (d5 * d22);
                                dArr2[i11][i9] = (dArr2[i11][i9] * d5) - (d22 * d11);
                            }
                            i9--;
                            d12 = d5;
                            d10 = d20;
                            d9 = d21;
                        }
                        if (d10 != 0.0d || i9 < i6) {
                            double[] dArr9 = this.realEigenvalues;
                            dArr9[i6] = dArr9[i6] - d9;
                            dArr3[i6] = d8;
                            dArr3[i2] = 0.0d;
                        }
                    } else {
                        throw new MaxCountExceededException(LocalizedFormats.CONVERGENCE_FAILED, Byte.valueOf(this.maxIter), new Object[0]);
                    }
                }
            } while (i2 != i6);
        }
        int i12 = 0;
        while (i12 < length) {
            int i13 = i12 + 1;
            double d23 = this.realEigenvalues[i12];
            int i14 = i12;
            for (int i15 = i13; i15 < length; i15++) {
                double[] dArr10 = this.realEigenvalues;
                if (dArr10[i15] > d23) {
                    d23 = dArr10[i15];
                    i14 = i15;
                }
            }
            if (i14 != i12) {
                double[] dArr11 = this.realEigenvalues;
                dArr11[i14] = dArr11[i12];
                dArr11[i12] = d23;
                for (int i16 = 0; i16 < length; i16++) {
                    double d24 = dArr2[i16][i12];
                    dArr2[i16][i12] = dArr2[i16][i14];
                    dArr2[i16][i14] = d24;
                }
            }
            i12 = i13;
        }
        double d25 = 0.0d;
        for (int i17 = 0; i17 < length; i17++) {
            if (FastMath.abs(this.realEigenvalues[i17]) > d25) {
                d25 = FastMath.abs(this.realEigenvalues[i17]);
            }
        }
        if (d25 != 0.0d) {
            for (int i18 = 0; i18 < length; i18++) {
                if (FastMath.abs(this.realEigenvalues[i18]) < Precision.EPSILON * d25) {
                    this.realEigenvalues[i18] = 0.0d;
                }
            }
        }
        this.eigenvectors = new ArrayRealVector[length];
        double[] dArr12 = new double[length];
        for (int i19 = 0; i19 < length; i19++) {
            for (int i20 = 0; i20 < length; i20++) {
                dArr12[i20] = dArr2[i20][i19];
            }
            this.eigenvectors[i19] = new ArrayRealVector(dArr12);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.Precision.equals(double, double, double):boolean
     arg types: [double, int, int]
     candidates:
      org.apache.commons.math3.util.Precision.equals(double, double, int):boolean
      org.apache.commons.math3.util.Precision.equals(float, float, float):boolean
      org.apache.commons.math3.util.Precision.equals(float, float, int):boolean
      org.apache.commons.math3.util.Precision.equals(double, double, double):boolean */
    private SchurTransformer transformToSchur(RealMatrix realMatrix) {
        SchurTransformer schurTransformer = new SchurTransformer(realMatrix);
        double[][] data = schurTransformer.getT().getData();
        this.realEigenvalues = new double[data.length];
        this.imagEigenvalues = new double[data.length];
        int i = 0;
        while (true) {
            double[] dArr = this.realEigenvalues;
            if (i >= dArr.length) {
                return schurTransformer;
            }
            if (i != dArr.length - 1) {
                int i2 = i + 1;
                if (!Precision.equals(data[i2][i], 0.0d, 1.0E-12d)) {
                    double d = data[i2][i2];
                    double d2 = (data[i][i] - d) * 0.5d;
                    double sqrt = FastMath.sqrt(FastMath.abs((d2 * d2) + (data[i2][i] * data[i][i2])));
                    double[] dArr2 = this.realEigenvalues;
                    double d3 = d + d2;
                    dArr2[i] = d3;
                    double[] dArr3 = this.imagEigenvalues;
                    dArr3[i] = sqrt;
                    dArr2[i2] = d3;
                    dArr3[i2] = -sqrt;
                    i = i2;
                    i++;
                }
            }
            this.realEigenvalues[i] = data[i][i];
            i++;
        }
    }

    private Complex cdiv(double d, double d2, double d3, double d4) {
        return new Complex(d, d2).divide(new Complex(d3, d4));
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v0, resolved type: double[][]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v0, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v1, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v1, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v2, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v10, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r31v0, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v16, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v6, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v23, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v12, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v11, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v12, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v13, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v27, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r33v2, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v28, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r35v1, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v23, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v24, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v24, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v15, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v35, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v20, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v26, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v27, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v38, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v19, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v20, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v12, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v39, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v21, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v13, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v22, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v14, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v37, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v38, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v14, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v29, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v15, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v30, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r33v3, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v31, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v17, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v32, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r33v4, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v46, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v25, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v47, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v16, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v41, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v42, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v50, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v43, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v35, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v51, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v45, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v36, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v27, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v19, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v52, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v6, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v15, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v27, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v7, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v53, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v20, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v10, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v59, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v29, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v60, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v12, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v63, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v65, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v66, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v67, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v68, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v8, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v9, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v9, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v26, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v50, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v31, resolved type: double} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.Precision.equals(double, double, double):boolean
     arg types: [double, int, int]
     candidates:
      org.apache.commons.math3.util.Precision.equals(double, double, int):boolean
      org.apache.commons.math3.util.Precision.equals(float, float, float):boolean
      org.apache.commons.math3.util.Precision.equals(float, float, int):boolean
      org.apache.commons.math3.util.Precision.equals(double, double, double):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.Precision.compareTo(double, double, double):int
     arg types: [double, int, int]
     candidates:
      org.apache.commons.math3.util.Precision.compareTo(double, double, int):int
      org.apache.commons.math3.util.Precision.compareTo(double, double, double):int */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0123  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void findEigenVectorsFromSchur(org.apache.commons.math3.linear.SchurTransformer r50) throws org.apache.commons.math3.exception.MathArithmeticException {
        /*
            r49 = this;
            r9 = r49
            org.apache.commons.math3.linear.RealMatrix r0 = r50.getT()
            double[][] r10 = r0.getData()
            org.apache.commons.math3.linear.RealMatrix r0 = r50.getP()
            double[][] r11 = r0.getData()
            int r12 = r10.length
            r13 = 0
            r14 = 0
            r16 = r14
            r0 = 0
        L_0x0019:
            if (r0 >= r12) goto L_0x0033
            int r1 = r0 + -1
            int r1 = org.apache.commons.math3.util.FastMath.max(r1, r13)
        L_0x0021:
            if (r1 >= r12) goto L_0x0030
            r2 = r10[r0]
            r3 = r2[r1]
            double r2 = org.apache.commons.math3.util.FastMath.abs(r3)
            double r16 = r16 + r2
            int r1 = r1 + 1
            goto L_0x0021
        L_0x0030:
            int r0 = r0 + 1
            goto L_0x0019
        L_0x0033:
            r3 = 0
            r5 = 4427486594234968593(0x3d719799812dea11, double:1.0E-12)
            r1 = r16
            boolean r0 = org.apache.commons.math3.util.Precision.equals(r1, r3, r5)
            if (r0 != 0) goto L_0x040d
            int r7 = r12 + -1
            r8 = r7
            r18 = r14
            r20 = r18
            r22 = r20
        L_0x004b:
            if (r8 < 0) goto L_0x03b8
            double[] r0 = r9.realEigenvalues
            r24 = r0[r8]
            double[] r0 = r9.imagEigenvalues
            r5 = r0[r8]
            boolean r0 = org.apache.commons.math3.util.Precision.equals(r5, r14)
            r26 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            if (r0 == 0) goto L_0x0144
            r0 = r10[r8]
            r0[r8] = r26
            int r0 = r8 + -1
            r1 = r8
            r2 = r18
        L_0x0066:
            if (r0 < 0) goto L_0x013d
            r4 = r10[r0]
            r5 = r4[r0]
            double r5 = r5 - r24
            r4 = r1
            r13 = r14
        L_0x0070:
            if (r4 > r8) goto L_0x0081
            r15 = r10[r0]
            r18 = r15[r4]
            r15 = r10[r4]
            r22 = r15[r8]
            double r18 = r18 * r22
            double r13 = r13 + r18
            int r4 = r4 + 1
            goto L_0x0070
        L_0x0081:
            double[] r4 = r9.imagEigenvalues
            r30 = r4[r0]
            r32 = 0
            r34 = 4427486594234968593(0x3d719799812dea11, double:1.0E-12)
            int r4 = org.apache.commons.math3.util.Precision.compareTo(r30, r32, r34)
            if (r4 >= 0) goto L_0x009a
            r20 = r5
            r15 = r11
            r30 = r12
            r2 = r13
            goto L_0x0131
        L_0x009a:
            double[] r1 = r9.imagEigenvalues
            r15 = r11
            r30 = r12
            r11 = r1[r0]
            r18 = r2
            r1 = 0
            boolean r3 = org.apache.commons.math3.util.Precision.equals(r11, r1)
            if (r3 == 0) goto L_0x00c1
            int r3 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r3 == 0) goto L_0x00b6
            r1 = r10[r0]
            double r2 = -r13
            double r2 = r2 / r5
            r1[r8] = r2
            goto L_0x0102
        L_0x00b6:
            r1 = r10[r0]
            double r2 = -r13
            double r4 = org.apache.commons.math3.util.Precision.EPSILON
            double r4 = r4 * r16
            double r2 = r2 / r4
            r1[r8] = r2
            goto L_0x0102
        L_0x00c1:
            r1 = r10[r0]
            int r2 = r0 + 1
            r3 = r1[r2]
            r1 = r10[r2]
            r11 = r1[r0]
            double[] r1 = r9.realEigenvalues
            r22 = r1[r0]
            double r22 = r22 - r24
            r31 = r1[r0]
            double r31 = r31 - r24
            double r22 = r22 * r31
            double[] r1 = r9.imagEigenvalues
            r31 = r1[r0]
            r33 = r1[r0]
            double r31 = r31 * r33
            double r22 = r22 + r31
            double r31 = r3 * r18
            double r33 = r20 * r13
            double r31 = r31 - r33
            double r31 = r31 / r22
            r1 = r10[r0]
            r1[r8] = r31
            double r22 = org.apache.commons.math3.util.FastMath.abs(r3)
            double r33 = org.apache.commons.math3.util.FastMath.abs(r20)
            int r1 = (r22 > r33 ? 1 : (r22 == r33 ? 0 : -1))
            if (r1 <= 0) goto L_0x0105
            r1 = r10[r2]
            double r11 = -r13
            double r5 = r5 * r31
            double r11 = r11 - r5
            double r11 = r11 / r3
            r1[r8] = r11
        L_0x0102:
            r2 = r18
            goto L_0x0111
        L_0x0105:
            r1 = r10[r2]
            r2 = r18
            double r4 = -r2
            double r11 = r11 * r31
            double r4 = r4 - r11
            double r4 = r4 / r20
            r1[r8] = r4
        L_0x0111:
            r1 = r10[r0]
            r4 = r1[r8]
            double r4 = org.apache.commons.math3.util.FastMath.abs(r4)
            double r11 = org.apache.commons.math3.util.Precision.EPSILON
            double r11 = r11 * r4
            double r11 = r11 * r4
            int r1 = (r11 > r26 ? 1 : (r11 == r26 ? 0 : -1))
            if (r1 <= 0) goto L_0x0130
            r1 = r0
        L_0x0124:
            if (r1 > r8) goto L_0x0130
            r6 = r10[r1]
            r11 = r6[r8]
            double r11 = r11 / r4
            r6[r8] = r11
            int r1 = r1 + 1
            goto L_0x0124
        L_0x0130:
            r1 = r0
        L_0x0131:
            int r0 = r0 + -1
            r22 = r13
            r11 = r15
            r12 = r30
            r13 = 0
            r14 = 0
            goto L_0x0066
        L_0x013d:
            r15 = r11
            r30 = r12
            r18 = r2
            goto L_0x03a9
        L_0x0144:
            r30 = r12
            r0 = r14
            r15 = r11
            int r2 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r2 >= 0) goto L_0x03a9
            int r11 = r8 + -1
            r0 = r10[r8]
            r1 = r0[r11]
            double r0 = org.apache.commons.math3.util.FastMath.abs(r1)
            r2 = r10[r11]
            r3 = r2[r8]
            double r2 = org.apache.commons.math3.util.FastMath.abs(r3)
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x0181
            r0 = r10[r11]
            r1 = r10[r8]
            r2 = r1[r11]
            double r1 = r5 / r2
            r0[r11] = r1
            r0 = r10[r11]
            r1 = r10[r8]
            r2 = r1[r8]
            double r2 = r2 - r24
            double r1 = -r2
            r3 = r10[r8]
            r12 = r3[r11]
            double r1 = r1 / r12
            r0[r8] = r1
            r31 = r5
            r12 = r7
            r13 = r8
            goto L_0x01ab
        L_0x0181:
            r1 = 0
            r0 = r10[r11]
            r3 = r0[r8]
            double r3 = -r3
            r0 = r10[r11]
            r12 = r0[r11]
            double r12 = r12 - r24
            r0 = r49
            r31 = r5
            r5 = r12
            r12 = r7
            r13 = r8
            r7 = r31
            org.apache.commons.math3.complex.Complex r0 = r0.cdiv(r1, r3, r5, r7)
            r1 = r10[r11]
            double r2 = r0.getReal()
            r1[r11] = r2
            r1 = r10[r11]
            double r2 = r0.getImaginary()
            r1[r13] = r2
        L_0x01ab:
            r0 = r10[r13]
            r1 = 0
            r0[r11] = r1
            r0 = r10[r13]
            r0[r13] = r26
            int r8 = r13 + -2
            r14 = r8
            r0 = r11
            r7 = r18
            r5 = r22
        L_0x01bd:
            if (r14 < 0) goto L_0x039e
            r1 = r0
            r18 = r7
            r3 = 0
            r7 = 0
        L_0x01c6:
            if (r1 > r13) goto L_0x01e3
            r2 = r10[r14]
            r22 = r2[r1]
            r2 = r10[r1]
            r33 = r2[r11]
            double r22 = r22 * r33
            double r3 = r3 + r22
            r2 = r10[r14]
            r22 = r2[r1]
            r2 = r10[r1]
            r33 = r2[r13]
            double r22 = r22 * r33
            double r7 = r7 + r22
            int r1 = r1 + 1
            goto L_0x01c6
        L_0x01e3:
            r1 = r10[r14]
            r22 = r1[r14]
            double r22 = r22 - r24
            double[] r1 = r9.imagEigenvalues
            r33 = r1[r14]
            r35 = 0
            r37 = 4427486594234968593(0x3d719799812dea11, double:1.0E-12)
            int r1 = org.apache.commons.math3.util.Precision.compareTo(r33, r35, r37)
            if (r1 >= 0) goto L_0x0204
            r5 = r3
            r37 = r12
            r20 = r22
            r22 = r7
            r7 = r13
            goto L_0x0395
        L_0x0204:
            double[] r0 = r9.imagEigenvalues
            r1 = r0[r14]
            r33 = r5
            r5 = 0
            boolean r0 = org.apache.commons.math3.util.Precision.equals(r1, r5)
            if (r0 == 0) goto L_0x023e
            double r1 = -r3
            double r3 = -r7
            r0 = r49
            r7 = r33
            r5 = r22
            r41 = r7
            r39 = r18
            r7 = r31
            org.apache.commons.math3.complex.Complex r0 = r0.cdiv(r1, r3, r5, r7)
            r1 = r10[r14]
            double r2 = r0.getReal()
            r1[r11] = r2
            r1 = r10[r14]
            double r2 = r0.getImaginary()
            r1[r13] = r2
            r37 = r12
            r38 = r13
            r22 = r39
            r12 = r41
            goto L_0x035d
        L_0x023e:
            r39 = r18
            r41 = r33
            r0 = r10[r14]
            int r18 = r14 + 1
            r33 = r0[r18]
            r0 = r10[r18]
            r35 = r0[r14]
            double[] r0 = r9.realEigenvalues
            r1 = r0[r14]
            double r1 = r1 - r24
            r5 = r0[r14]
            double r5 = r5 - r24
            double r1 = r1 * r5
            double[] r5 = r9.imagEigenvalues
            r37 = r5[r14]
            r43 = r5[r14]
            double r37 = r37 * r43
            double r1 = r1 + r37
            double r5 = r31 * r31
            double r1 = r1 - r5
            r5 = r0[r14]
            double r5 = r5 - r24
            r37 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r5 = r5 * r37
            double r5 = r5 * r31
            r37 = r12
            r38 = r13
            r12 = 0
            boolean r0 = org.apache.commons.math3.util.Precision.equals(r1, r12)
            if (r0 == 0) goto L_0x02a6
            boolean r0 = org.apache.commons.math3.util.Precision.equals(r5, r12)
            if (r0 == 0) goto L_0x02a6
            double r0 = org.apache.commons.math3.util.Precision.EPSILON
            double r0 = r0 * r16
            double r28 = org.apache.commons.math3.util.FastMath.abs(r22)
            double r43 = org.apache.commons.math3.util.FastMath.abs(r31)
            double r28 = r28 + r43
            double r43 = org.apache.commons.math3.util.FastMath.abs(r33)
            double r28 = r28 + r43
            double r43 = org.apache.commons.math3.util.FastMath.abs(r35)
            double r28 = r28 + r43
            double r43 = org.apache.commons.math3.util.FastMath.abs(r20)
            double r28 = r28 + r43
            double r0 = r0 * r28
            r28 = r0
            goto L_0x02a8
        L_0x02a6:
            r28 = r1
        L_0x02a8:
            r1 = r41
            double r41 = r33 * r1
            double r43 = r20 * r3
            double r41 = r41 - r43
            double r43 = r31 * r7
            double r41 = r41 + r43
            r12 = r39
            double r39 = r33 * r12
            double r45 = r20 * r7
            double r39 = r39 - r45
            double r45 = r31 * r3
            double r39 = r39 - r45
            r0 = r49
            r45 = r12
            r12 = r1
            r1 = r41
            r41 = r12
            r12 = r3
            r3 = r39
            r39 = r5
            r5 = r28
            r47 = r7
            r7 = r39
            org.apache.commons.math3.complex.Complex r0 = r0.cdiv(r1, r3, r5, r7)
            r1 = r10[r14]
            double r2 = r0.getReal()
            r1[r11] = r2
            r1 = r10[r14]
            double r2 = r0.getImaginary()
            r1[r38] = r2
            double r0 = org.apache.commons.math3.util.FastMath.abs(r33)
            double r2 = org.apache.commons.math3.util.FastMath.abs(r20)
            double r4 = org.apache.commons.math3.util.FastMath.abs(r31)
            double r2 = r2 + r4
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x032b
            r0 = r10[r18]
            double r1 = -r12
            r3 = r10[r14]
            r4 = r3[r11]
            double r4 = r4 * r22
            double r1 = r1 - r4
            r3 = r10[r14]
            r4 = r3[r38]
            double r5 = r31 * r4
            double r1 = r1 + r5
            double r1 = r1 / r33
            r0[r11] = r1
            r0 = r10[r18]
            r7 = r47
            double r1 = -r7
            r3 = r10[r14]
            r4 = r3[r38]
            double r22 = r22 * r4
            double r1 = r1 - r22
            r3 = r10[r14]
            r4 = r3[r11]
            double r5 = r31 * r4
            double r1 = r1 - r5
            double r1 = r1 / r33
            r0[r38] = r1
            r12 = r41
            r22 = r45
            goto L_0x035d
        L_0x032b:
            r12 = r41
            double r0 = -r12
            r2 = r10[r14]
            r3 = r2[r11]
            double r3 = r3 * r35
            double r1 = r0 - r3
            r7 = r45
            double r3 = -r7
            r0 = r10[r14]
            r5 = r0[r38]
            double r35 = r35 * r5
            double r3 = r3 - r35
            r0 = r49
            r5 = r20
            r22 = r7
            r7 = r31
            org.apache.commons.math3.complex.Complex r0 = r0.cdiv(r1, r3, r5, r7)
            r1 = r10[r18]
            double r2 = r0.getReal()
            r1[r11] = r2
            r1 = r10[r18]
            double r2 = r0.getImaginary()
            r1[r38] = r2
        L_0x035d:
            r0 = r10[r14]
            r1 = r0[r11]
            double r0 = org.apache.commons.math3.util.FastMath.abs(r1)
            r2 = r10[r14]
            r3 = r2[r38]
            double r2 = org.apache.commons.math3.util.FastMath.abs(r3)
            double r0 = org.apache.commons.math3.util.FastMath.max(r0, r2)
            double r2 = org.apache.commons.math3.util.Precision.EPSILON
            double r2 = r2 * r0
            double r2 = r2 * r0
            int r4 = (r2 > r26 ? 1 : (r2 == r26 ? 0 : -1))
            if (r4 <= 0) goto L_0x0391
            r2 = r14
            r7 = r38
        L_0x037e:
            if (r2 > r7) goto L_0x0393
            r3 = r10[r2]
            r4 = r3[r11]
            double r4 = r4 / r0
            r3[r11] = r4
            r3 = r10[r2]
            r4 = r3[r7]
            double r4 = r4 / r0
            r3[r7] = r4
            int r2 = r2 + 1
            goto L_0x037e
        L_0x0391:
            r7 = r38
        L_0x0393:
            r5 = r12
            r0 = r14
        L_0x0395:
            int r14 = r14 + -1
            r13 = r7
            r7 = r22
            r12 = r37
            goto L_0x01bd
        L_0x039e:
            r22 = r7
            r37 = r12
            r7 = r13
            r12 = r5
            r18 = r22
            r22 = r12
            goto L_0x03ac
        L_0x03a9:
            r37 = r7
            r7 = r8
        L_0x03ac:
            int r8 = r7 + -1
            r11 = r15
            r12 = r30
            r7 = r37
            r13 = 0
            r14 = 0
            goto L_0x004b
        L_0x03b8:
            r37 = r7
            r15 = r11
            r30 = r12
            r0 = r37
        L_0x03bf:
            if (r0 < 0) goto L_0x03e9
            r12 = r37
            r1 = 0
        L_0x03c4:
            if (r1 > r12) goto L_0x03e4
            r2 = 0
            r3 = 0
        L_0x03c9:
            int r5 = org.apache.commons.math3.util.FastMath.min(r0, r12)
            if (r2 > r5) goto L_0x03dd
            r5 = r15[r1]
            r6 = r5[r2]
            r5 = r10[r2]
            r13 = r5[r0]
            double r6 = r6 * r13
            double r3 = r3 + r6
            int r2 = r2 + 1
            goto L_0x03c9
        L_0x03dd:
            r2 = r15[r1]
            r2[r0] = r3
            int r1 = r1 + 1
            goto L_0x03c4
        L_0x03e4:
            int r0 = r0 + -1
            r37 = r12
            goto L_0x03bf
        L_0x03e9:
            r0 = r30
            org.apache.commons.math3.linear.ArrayRealVector[] r1 = new org.apache.commons.math3.linear.ArrayRealVector[r0]
            r9.eigenvectors = r1
            double[] r1 = new double[r0]
            r2 = 0
        L_0x03f2:
            if (r2 >= r0) goto L_0x040c
            r3 = 0
        L_0x03f5:
            if (r3 >= r0) goto L_0x0400
            r4 = r15[r3]
            r5 = r4[r2]
            r1[r3] = r5
            int r3 = r3 + 1
            goto L_0x03f5
        L_0x0400:
            org.apache.commons.math3.linear.ArrayRealVector[] r3 = r9.eigenvectors
            org.apache.commons.math3.linear.ArrayRealVector r4 = new org.apache.commons.math3.linear.ArrayRealVector
            r4.<init>(r1)
            r3[r2] = r4
            int r2 = r2 + 1
            goto L_0x03f2
        L_0x040c:
            return
        L_0x040d:
            org.apache.commons.math3.exception.MathArithmeticException r0 = new org.apache.commons.math3.exception.MathArithmeticException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.ZERO_NORM
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r0.<init>(r1, r2)
            goto L_0x0419
        L_0x0418:
            throw r0
        L_0x0419:
            goto L_0x0418
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.linear.EigenDecomposition.findEigenVectorsFromSchur(org.apache.commons.math3.linear.SchurTransformer):void");
    }
}
