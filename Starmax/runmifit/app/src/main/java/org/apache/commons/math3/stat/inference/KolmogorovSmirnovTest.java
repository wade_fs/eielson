package org.apache.commons.math3.stat.inference;

import java.util.Arrays;
import java.util.Iterator;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.exception.InsufficientDataException;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.TooManyIterationsException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.fraction.BigFraction;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.FieldMatrix;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well19937c;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;

public class KolmogorovSmirnovTest {
    protected static final double KS_SUM_CAUCHY_CRITERION = 1.0E-20d;
    protected static final int LARGE_SAMPLE_PRODUCT = 10000;
    protected static final int MAXIMUM_PARTIAL_SUM_COUNT = 100000;
    protected static final int MONTE_CARLO_ITERATIONS = 1000000;
    protected static final int SMALL_SAMPLE_PRODUCT = 200;
    private final RandomGenerator rng;

    public KolmogorovSmirnovTest() {
        this.rng = new Well19937c();
    }

    public KolmogorovSmirnovTest(RandomGenerator randomGenerator) {
        this.rng = randomGenerator;
    }

    public double kolmogorovSmirnovTest(RealDistribution realDistribution, double[] dArr, boolean z) {
        return 1.0d - cdf(kolmogorovSmirnovStatistic(realDistribution, dArr), dArr.length, z);
    }

    public double kolmogorovSmirnovStatistic(RealDistribution realDistribution, double[] dArr) {
        checkArray(dArr);
        int length = dArr.length;
        double d = (double) length;
        double[] dArr2 = new double[length];
        System.arraycopy(dArr, 0, dArr2, 0, length);
        Arrays.sort(dArr2);
        double d2 = 0.0d;
        for (int i = 1; i <= length; i++) {
            int i2 = i - 1;
            double cumulativeProbability = realDistribution.cumulativeProbability(dArr2[i2]);
            double d3 = (double) i2;
            Double.isNaN(d3);
            Double.isNaN(d);
            double d4 = (double) i;
            Double.isNaN(d4);
            Double.isNaN(d);
            double max = FastMath.max(cumulativeProbability - (d3 / d), (d4 / d) - cumulativeProbability);
            if (max > d2) {
                d2 = max;
            }
        }
        return d2;
    }

    public double kolmogorovSmirnovTest(double[] dArr, double[] dArr2, boolean z) {
        if (dArr.length * dArr2.length < 200) {
            return exactP(kolmogorovSmirnovStatistic(dArr, dArr2), dArr.length, dArr2.length, z);
        }
        if (dArr.length * dArr2.length < 10000) {
            return monteCarloP(kolmogorovSmirnovStatistic(dArr, dArr2), dArr.length, dArr2.length, z, MONTE_CARLO_ITERATIONS);
        }
        return approximateP(kolmogorovSmirnovStatistic(dArr, dArr2), dArr.length, dArr2.length);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.kolmogorovSmirnovTest(double[], double[], boolean):double
     arg types: [double[], double[], int]
     candidates:
      org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.kolmogorovSmirnovTest(org.apache.commons.math3.distribution.RealDistribution, double[], boolean):double
      org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.kolmogorovSmirnovTest(org.apache.commons.math3.distribution.RealDistribution, double[], double):boolean
      org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.kolmogorovSmirnovTest(double[], double[], boolean):double */
    public double kolmogorovSmirnovTest(double[] dArr, double[] dArr2) {
        return kolmogorovSmirnovTest(dArr, dArr2, true);
    }

    public double kolmogorovSmirnovStatistic(double[] dArr, double[] dArr2) {
        double d;
        double d2;
        double d3;
        checkArray(dArr);
        checkArray(dArr2);
        double[] copyOf = MathArrays.copyOf(dArr);
        double[] copyOf2 = MathArrays.copyOf(dArr2);
        Arrays.sort(copyOf);
        Arrays.sort(copyOf2);
        int length = copyOf.length;
        int length2 = copyOf2.length;
        int i = 0;
        double d4 = 0.0d;
        int i2 = 0;
        while (true) {
            d = 1.0d;
            if (i2 >= length) {
                break;
            }
            double d5 = (double) i2;
            Double.isNaN(d5);
            double d6 = (double) length;
            Double.isNaN(d6);
            double d7 = (d5 + 1.0d) / d6;
            int binarySearch = Arrays.binarySearch(copyOf2, copyOf[i2]);
            if (binarySearch >= 0) {
                double d8 = (double) binarySearch;
                Double.isNaN(d8);
                d3 = d8 + 1.0d;
            } else {
                double d9 = (double) (-binarySearch);
                Double.isNaN(d9);
                d3 = d9 - 1.0d;
            }
            double d10 = (double) length2;
            Double.isNaN(d10);
            double abs = FastMath.abs(d7 - (d3 / d10));
            if (abs > d4) {
                d4 = abs;
            }
            i2++;
        }
        while (i < length2) {
            double d11 = (double) i;
            Double.isNaN(d11);
            double d12 = (double) length2;
            Double.isNaN(d12);
            double d13 = (d11 + d) / d12;
            int binarySearch2 = Arrays.binarySearch(copyOf, copyOf2[i]);
            if (binarySearch2 >= 0) {
                double d14 = (double) binarySearch2;
                Double.isNaN(d14);
                double d15 = d14 + d;
                double d16 = (double) length;
                Double.isNaN(d16);
                d2 = d15 / d16;
            } else {
                double d17 = (double) (-binarySearch2);
                Double.isNaN(d17);
                double d18 = (double) length;
                Double.isNaN(d18);
                d2 = (d17 - 1.0d) / d18;
            }
            double abs2 = FastMath.abs(d2 - d13);
            if (abs2 > d4) {
                d4 = abs2;
            }
            i++;
            d = 1.0d;
        }
        return d4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.kolmogorovSmirnovTest(org.apache.commons.math3.distribution.RealDistribution, double[], boolean):double
     arg types: [org.apache.commons.math3.distribution.RealDistribution, double[], int]
     candidates:
      org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.kolmogorovSmirnovTest(double[], double[], boolean):double
      org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.kolmogorovSmirnovTest(org.apache.commons.math3.distribution.RealDistribution, double[], double):boolean
      org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.kolmogorovSmirnovTest(org.apache.commons.math3.distribution.RealDistribution, double[], boolean):double */
    public double kolmogorovSmirnovTest(RealDistribution realDistribution, double[] dArr) {
        return kolmogorovSmirnovTest(realDistribution, dArr, false);
    }

    public boolean kolmogorovSmirnovTest(RealDistribution realDistribution, double[] dArr, double d) {
        if (d > 0.0d && d <= 0.5d) {
            return kolmogorovSmirnovTest(realDistribution, dArr) < d;
        }
        throw new OutOfRangeException(LocalizedFormats.OUT_OF_BOUND_SIGNIFICANCE_LEVEL, Double.valueOf(d), 0, Double.valueOf(0.5d));
    }

    public double cdf(double d, int i) throws MathArithmeticException {
        return cdf(d, i, false);
    }

    public double cdfExact(double d, int i) throws MathArithmeticException {
        return cdf(d, i, true);
    }

    public double cdf(double d, int i, boolean z) throws MathArithmeticException {
        double d2 = (double) i;
        double d3 = 1.0d;
        Double.isNaN(d2);
        double d4 = 1.0d / d2;
        double d5 = 0.5d * d4;
        if (d <= d5) {
            return 0.0d;
        }
        if (d5 < d && d <= d4) {
            double d6 = (d * 2.0d) - d4;
            for (int i2 = 1; i2 <= i; i2++) {
                double d7 = (double) i2;
                Double.isNaN(d7);
                d3 *= d7 * d6;
            }
            return d3;
        } else if (1.0d - d4 <= d && d < 1.0d) {
            return 1.0d - (Math.pow(1.0d - d, d2) * 2.0d);
        } else {
            if (1.0d <= d) {
                return 1.0d;
            }
            return z ? exactK(d, i) : roundedK(d, i);
        }
    }

    private double exactK(double d, int i) throws MathArithmeticException {
        double d2 = (double) i;
        Double.isNaN(d2);
        int ceil = ((int) Math.ceil(d2 * d)) - 1;
        BigFraction entry = createH(d, i).power(i).getEntry(ceil, ceil);
        for (int i2 = 1; i2 <= i; i2++) {
            entry = entry.multiply(i2).divide(i);
        }
        return entry.bigDecimalValue(20, 4).doubleValue();
    }

    private double roundedK(double d, int i) throws MathArithmeticException {
        double d2 = (double) i;
        Double.isNaN(d2);
        int ceil = (int) Math.ceil(d2 * d);
        FieldMatrix<BigFraction> createH = createH(d, i);
        int rowDimension = createH.getRowDimension();
        Array2DRowRealMatrix array2DRowRealMatrix = new Array2DRowRealMatrix(rowDimension, rowDimension);
        for (int i2 = 0; i2 < rowDimension; i2++) {
            for (int i3 = 0; i3 < rowDimension; i3++) {
                array2DRowRealMatrix.setEntry(i2, i3, createH.getEntry(i2, i3).doubleValue());
            }
        }
        int i4 = ceil - 1;
        double entry = array2DRowRealMatrix.power(i).getEntry(i4, i4);
        for (int i5 = 1; i5 <= i; i5++) {
            double d3 = (double) i5;
            Double.isNaN(d3);
            Double.isNaN(d2);
            entry *= d3 / d2;
        }
        return entry;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(20:2|3|4|5|8|(4:10|(3:12|(2:14|46)(2:15|45)|16)|44|17)|43|18|(1:20)|47|21|(1:23)|48|24|(1:26)|27|(3:29|(2:30|(3:32|(3:34|(1:36)|52)(1:51)|37)(1:50))|38)|49|39|40) */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x003a, code lost:
        r4 = new org.apache.commons.math3.fraction.BigFraction(r0, 1.0E-5d, 10000);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x002b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.commons.math3.linear.FieldMatrix<org.apache.commons.math3.fraction.BigFraction> createH(double r11, int r13) throws org.apache.commons.math3.exception.NumberIsTooLargeException, org.apache.commons.math3.fraction.FractionConversionException {
        /*
            r10 = this;
            double r0 = (double) r13
            java.lang.Double.isNaN(r0)
            double r0 = r0 * r11
            double r11 = java.lang.Math.ceil(r0)
            int r11 = (int) r11
            int r12 = r11 * 2
            r13 = 1
            int r12 = r12 - r13
            double r2 = (double) r11
            java.lang.Double.isNaN(r2)
            double r0 = r2 - r0
            r2 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r11 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x00fe
            org.apache.commons.math3.fraction.BigFraction r2 = new org.apache.commons.math3.fraction.BigFraction     // Catch:{ FractionConversionException -> 0x002b }
            r7 = 4307583784117748259(0x3bc79ca10c924223, double:1.0E-20)
            r9 = 10000(0x2710, float:1.4013E-41)
            r4 = r2
            r5 = r0
            r4.<init>(r5, r7, r9)     // Catch:{ FractionConversionException -> 0x002b }
            goto L_0x0048
        L_0x002b:
            org.apache.commons.math3.fraction.BigFraction r2 = new org.apache.commons.math3.fraction.BigFraction     // Catch:{ FractionConversionException -> 0x003a }
            r7 = 4457293557087583675(0x3ddb7cdfd9d7bdbb, double:1.0E-10)
            r9 = 10000(0x2710, float:1.4013E-41)
            r4 = r2
            r5 = r0
            r4.<init>(r5, r7, r9)     // Catch:{ FractionConversionException -> 0x003a }
            goto L_0x0048
        L_0x003a:
            org.apache.commons.math3.fraction.BigFraction r2 = new org.apache.commons.math3.fraction.BigFraction
            r7 = 4532020583610935537(0x3ee4f8b588e368f1, double:1.0E-5)
            r9 = 10000(0x2710, float:1.4013E-41)
            r4 = r2
            r5 = r0
            r4.<init>(r5, r7, r9)
        L_0x0048:
            int[] r0 = new int[]{r12, r12}
            java.lang.Class<org.apache.commons.math3.fraction.BigFraction> r1 = org.apache.commons.math3.fraction.BigFraction.class
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r1, r0)
            org.apache.commons.math3.fraction.BigFraction[][] r0 = (org.apache.commons.math3.fraction.BigFraction[][]) r0
            r1 = 0
        L_0x0055:
            if (r1 >= r12) goto L_0x0072
            r3 = 0
        L_0x0058:
            if (r3 >= r12) goto L_0x006f
            int r4 = r1 - r3
            int r4 = r4 + r13
            if (r4 >= 0) goto L_0x0066
            r4 = r0[r1]
            org.apache.commons.math3.fraction.BigFraction r5 = org.apache.commons.math3.fraction.BigFraction.ZERO
            r4[r3] = r5
            goto L_0x006c
        L_0x0066:
            r4 = r0[r1]
            org.apache.commons.math3.fraction.BigFraction r5 = org.apache.commons.math3.fraction.BigFraction.ONE
            r4[r3] = r5
        L_0x006c:
            int r3 = r3 + 1
            goto L_0x0058
        L_0x006f:
            int r1 = r1 + 1
            goto L_0x0055
        L_0x0072:
            org.apache.commons.math3.fraction.BigFraction[] r1 = new org.apache.commons.math3.fraction.BigFraction[r12]
            r1[r11] = r2
            r3 = 1
        L_0x0077:
            if (r3 >= r12) goto L_0x0086
            int r4 = r3 + -1
            r4 = r1[r4]
            org.apache.commons.math3.fraction.BigFraction r4 = r2.multiply(r4)
            r1[r3] = r4
            int r3 = r3 + 1
            goto L_0x0077
        L_0x0086:
            r3 = 0
        L_0x0087:
            if (r3 >= r12) goto L_0x00ad
            r4 = r0[r3]
            r5 = r0[r3]
            r5 = r5[r11]
            r6 = r1[r3]
            org.apache.commons.math3.fraction.BigFraction r5 = r5.subtract(r6)
            r4[r11] = r5
            int r4 = r12 + -1
            r5 = r0[r4]
            r4 = r0[r4]
            r4 = r4[r3]
            int r6 = r12 - r3
            int r6 = r6 - r13
            r6 = r1[r6]
            org.apache.commons.math3.fraction.BigFraction r4 = r4.subtract(r6)
            r5[r3] = r4
            int r3 = r3 + 1
            goto L_0x0087
        L_0x00ad:
            org.apache.commons.math3.fraction.BigFraction r1 = org.apache.commons.math3.fraction.BigFraction.ONE_HALF
            int r1 = r2.compareTo(r1)
            r3 = 2
            if (r1 != r13) goto L_0x00d0
            int r1 = r12 + -1
            r4 = r0[r1]
            r1 = r0[r1]
            r1 = r1[r11]
            org.apache.commons.math3.fraction.BigFraction r2 = r2.multiply(r3)
            org.apache.commons.math3.fraction.BigFraction r2 = r2.subtract(r13)
            org.apache.commons.math3.fraction.BigFraction r2 = r2.pow(r12)
            org.apache.commons.math3.fraction.BigFraction r1 = r1.add(r2)
            r4[r11] = r1
        L_0x00d0:
            r1 = 0
        L_0x00d1:
            if (r1 >= r12) goto L_0x00f4
            r2 = 0
        L_0x00d4:
            int r4 = r1 + 1
            if (r2 >= r4) goto L_0x00f2
            int r4 = r1 - r2
            int r4 = r4 + r13
            if (r4 <= 0) goto L_0x00ef
            r5 = 2
        L_0x00de:
            if (r5 > r4) goto L_0x00ef
            r6 = r0[r1]
            r7 = r0[r1]
            r7 = r7[r2]
            org.apache.commons.math3.fraction.BigFraction r7 = r7.divide(r5)
            r6[r2] = r7
            int r5 = r5 + 1
            goto L_0x00de
        L_0x00ef:
            int r2 = r2 + 1
            goto L_0x00d4
        L_0x00f2:
            r1 = r4
            goto L_0x00d1
        L_0x00f4:
            org.apache.commons.math3.linear.Array2DRowFieldMatrix r11 = new org.apache.commons.math3.linear.Array2DRowFieldMatrix
            org.apache.commons.math3.fraction.BigFractionField r12 = org.apache.commons.math3.fraction.BigFractionField.getInstance()
            r11.<init>(r12, r0)
            return r11
        L_0x00fe:
            org.apache.commons.math3.exception.NumberIsTooLargeException r12 = new org.apache.commons.math3.exception.NumberIsTooLargeException
            java.lang.Double r13 = java.lang.Double.valueOf(r0)
            java.lang.Double r0 = java.lang.Double.valueOf(r2)
            r12.<init>(r13, r0, r11)
            goto L_0x010d
        L_0x010c:
            throw r12
        L_0x010d:
            goto L_0x010c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest.createH(double, int):org.apache.commons.math3.linear.FieldMatrix");
    }

    private void checkArray(double[] dArr) {
        if (dArr == null) {
            throw new NullArgumentException(LocalizedFormats.NULL_NOT_ALLOWED, new Object[0]);
        } else if (dArr.length < 2) {
            throw new InsufficientDataException(LocalizedFormats.INSUFFICIENT_OBSERVED_POINTS_IN_SAMPLE, Integer.valueOf(dArr.length), 2);
        }
    }

    public double ksSum(double d, double d2, int i) {
        int i2 = i;
        double d3 = -2.0d * d * d;
        double d4 = 1.0d;
        double d5 = 0.5d;
        int i3 = -1;
        long j = 1;
        while (d4 > d2 && j < ((long) i2)) {
            double d6 = (double) j;
            Double.isNaN(d6);
            Double.isNaN(d6);
            d4 = FastMath.exp(d3 * d6 * d6);
            double d7 = (double) i3;
            Double.isNaN(d7);
            d5 += d7 * d4;
            i3 *= -1;
            j++;
        }
        if (j != ((long) i2)) {
            return d5 * 2.0d;
        }
        throw new TooManyIterationsException(Integer.valueOf(i));
    }

    public double exactP(double d, int i, int i2, boolean z) {
        int i3 = i;
        int i4 = i2;
        int i5 = i3 + i4;
        Iterator<int[]> combinationsIterator = CombinatoricsUtils.combinationsIterator(i5, i3);
        double[] dArr = new double[i3];
        double[] dArr2 = new double[i4];
        long j = 0;
        while (combinationsIterator.hasNext()) {
            int[] next = combinationsIterator.next();
            int i6 = 0;
            int i7 = 0;
            for (int i8 = 0; i8 < i5; i8++) {
                if (i6 >= i3 || next[i6] != i8) {
                    dArr2[i7] = (double) i8;
                    i7++;
                } else {
                    dArr[i6] = (double) i8;
                    i6++;
                }
            }
            double kolmogorovSmirnovStatistic = kolmogorovSmirnovStatistic(dArr, dArr2);
            if (kolmogorovSmirnovStatistic > d || (kolmogorovSmirnovStatistic == d && !z)) {
                j++;
            }
        }
        double d2 = (double) j;
        double binomialCoefficient = (double) CombinatoricsUtils.binomialCoefficient(i5, i3);
        Double.isNaN(d2);
        Double.isNaN(binomialCoefficient);
        return d2 / binomialCoefficient;
    }

    public double approximateP(double d, int i, int i2) {
        double d2 = (double) i2;
        double d3 = (double) i;
        Double.isNaN(d2);
        Double.isNaN(d3);
        Double.isNaN(d2);
        Double.isNaN(d3);
        return 1.0d - ksSum(d * FastMath.sqrt((d2 * d3) / (d2 + d3)), KS_SUM_CAUCHY_CRITERION, MAXIMUM_PARTIAL_SUM_COUNT);
    }

    public double monteCarloP(double d, int i, int i2, boolean z, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        int[] natural = MathArrays.natural(i5 + i4);
        double[] dArr = new double[i4];
        double[] dArr2 = new double[i5];
        int i7 = 0;
        for (int i8 = 0; i8 < i6; i8++) {
            copyPartition(dArr, dArr2, natural, i, i2);
            double kolmogorovSmirnovStatistic = kolmogorovSmirnovStatistic(dArr, dArr2);
            if (kolmogorovSmirnovStatistic > d || (kolmogorovSmirnovStatistic == d && !z)) {
                i7++;
            }
            MathArrays.shuffle(natural, this.rng);
            Arrays.sort(natural, 0, i4);
        }
        double d2 = (double) i7;
        double d3 = (double) i6;
        Double.isNaN(d2);
        Double.isNaN(d3);
        return d2 / d3;
    }

    private void copyPartition(double[] dArr, double[] dArr2, int[] iArr, int i, int i2) {
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i + i2; i5++) {
            if (i3 >= i || iArr[i3] != i5) {
                dArr2[i4] = (double) i5;
                i4++;
            } else {
                dArr[i3] = (double) i5;
                i3++;
            }
        }
    }
}
