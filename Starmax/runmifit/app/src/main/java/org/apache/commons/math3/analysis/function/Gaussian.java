package org.apache.commons.math3.analysis.function;

import java.util.Arrays;
import org.apache.commons.math3.analysis.DifferentiableUnivariateFunction;
import org.apache.commons.math3.analysis.FunctionUtils;
import org.apache.commons.math3.analysis.ParametricUnivariateFunction;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.differentiation.DerivativeStructure;
import org.apache.commons.math3.analysis.differentiation.UnivariateDifferentiableFunction;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Precision;

public class Gaussian implements UnivariateDifferentiableFunction, DifferentiableUnivariateFunction {
    private final double i2s2;

    /* renamed from: is */
    private final double f7939is;
    private final double mean;
    private final double norm;

    public Gaussian(double d, double d2, double d3) throws NotStrictlyPositiveException {
        if (d3 > 0.0d) {
            this.norm = d;
            this.mean = d2;
            this.f7939is = 1.0d / d3;
            double d4 = this.f7939is;
            this.i2s2 = 0.5d * d4 * d4;
            return;
        }
        throw new NotStrictlyPositiveException(Double.valueOf(d3));
    }

    public Gaussian(double d, double d2) throws NotStrictlyPositiveException {
        this(1.0d / (FastMath.sqrt(6.283185307179586d) * d2), d, d2);
    }

    public Gaussian() {
        this(0.0d, 1.0d);
    }

    public double value(double d) {
        return value(d - this.mean, this.norm, this.i2s2);
    }

    @Deprecated
    public UnivariateFunction derivative() {
        return FunctionUtils.toDifferentiableUnivariateFunction(this).derivative();
    }

    public static class Parametric implements ParametricUnivariateFunction {
        public double value(double d, double... dArr) throws NullArgumentException, DimensionMismatchException, NotStrictlyPositiveException {
            validateParameters(dArr);
            return Gaussian.value(d - dArr[1], dArr[0], 1.0d / ((dArr[2] * 2.0d) * dArr[2]));
        }

        public double[] gradient(double d, double... dArr) throws NullArgumentException, DimensionMismatchException, NotStrictlyPositiveException {
            double[] dArr2 = dArr;
            validateParameters(dArr2);
            double d2 = dArr2[0];
            double d3 = d - dArr2[1];
            double d4 = dArr2[2];
            double d5 = 1.0d / ((d4 * 2.0d) * d4);
            double access$000 = Gaussian.value(d3, 1.0d, d5);
            double d6 = d2 * access$000 * 2.0d * d5 * d3;
            return new double[]{access$000, d6, (d3 * d6) / d4};
        }

        private void validateParameters(double[] dArr) throws NullArgumentException, DimensionMismatchException, NotStrictlyPositiveException {
            if (dArr == null) {
                throw new NullArgumentException();
            } else if (dArr.length != 3) {
                throw new DimensionMismatchException(dArr.length, 3);
            } else if (dArr[2] <= 0.0d) {
                throw new NotStrictlyPositiveException(Double.valueOf(dArr[2]));
            }
        }
    }

    /* access modifiers changed from: private */
    public static double value(double d, double d2, double d3) {
        return d2 * FastMath.exp((-d) * d * d3);
    }

    public DerivativeStructure value(DerivativeStructure derivativeStructure) throws DimensionMismatchException {
        double d;
        double value = this.f7939is * (derivativeStructure.getValue() - this.mean);
        double[] dArr = new double[(derivativeStructure.getOrder() + 1)];
        double[] dArr2 = new double[dArr.length];
        dArr2[0] = 1.0d;
        double d2 = value * value;
        double exp = this.norm * FastMath.exp(-0.5d * d2);
        if (exp <= Precision.SAFE_MIN) {
            Arrays.fill(dArr, 0.0d);
        } else {
            dArr[0] = exp;
            double d3 = exp;
            int i = 1;
            while (i < dArr.length) {
                dArr2[i] = -dArr2[i - 1];
                int i2 = i;
                double d4 = 0.0d;
                while (i2 >= 0) {
                    d4 = (d4 * d2) + dArr2[i2];
                    if (i2 > 2) {
                        int i3 = i2 - 1;
                        d = d2;
                        double d5 = (double) i3;
                        double d6 = dArr2[i3];
                        Double.isNaN(d5);
                        dArr2[i2 - 2] = (d5 * d6) - dArr2[i2 - 3];
                    } else {
                        d = d2;
                        if (i2 == 2) {
                            dArr2[0] = dArr2[1];
                            i2 -= 2;
                            d2 = d;
                        }
                    }
                    i2 -= 2;
                    d2 = d;
                }
                double d7 = d2;
                if ((i & 1) == 1) {
                    d4 *= value;
                }
                d3 *= this.f7939is;
                dArr[i] = d4 * d3;
                i++;
                d2 = d7;
            }
        }
        return derivativeStructure.compose(dArr);
    }
}
