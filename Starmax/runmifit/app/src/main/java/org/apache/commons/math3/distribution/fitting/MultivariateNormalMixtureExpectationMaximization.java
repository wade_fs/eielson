package org.apache.commons.math3.distribution.fitting;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.math3.distribution.MixtureMultivariateNormalDistribution;
import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularMatrixException;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Pair;

public class MultivariateNormalMixtureExpectationMaximization {
    private static final int DEFAULT_MAX_ITERATIONS = 1000;
    private static final double DEFAULT_THRESHOLD = 1.0E-5d;
    private final double[][] data;
    private MixtureMultivariateNormalDistribution fittedModel;
    private double logLikelihood = 0.0d;

    public MultivariateNormalMixtureExpectationMaximization(double[][] dArr) throws NotStrictlyPositiveException, DimensionMismatchException, NumberIsTooSmallException {
        if (dArr.length >= 1) {
            this.data = (double[][]) Array.newInstance(double.class, dArr.length, dArr[0].length);
            int i = 0;
            while (i < dArr.length) {
                if (dArr[i].length != dArr[0].length) {
                    throw new DimensionMismatchException(dArr[i].length, dArr[0].length);
                } else if (dArr[i].length >= 2) {
                    this.data[i] = MathArrays.copyOf(dArr[i], dArr[i].length);
                    i++;
                } else {
                    throw new NumberIsTooSmallException(LocalizedFormats.NUMBER_TOO_SMALL, Integer.valueOf(dArr[i].length), 2, true);
                }
            }
            return;
        }
        throw new NotStrictlyPositiveException(Integer.valueOf(dArr.length));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    public void fit(MixtureMultivariateNormalDistribution mixtureMultivariateNormalDistribution, int i, double d) throws SingularMatrixException, NotStrictlyPositiveException, DimensionMismatchException {
        int i2 = i;
        Class<double> cls = double.class;
        if (i2 < 1) {
            throw new NotStrictlyPositiveException(Integer.valueOf(i));
        } else if (d >= Double.MIN_VALUE) {
            double[][] dArr = this.data;
            int length = dArr.length;
            int length2 = dArr[0].length;
            int size = mixtureMultivariateNormalDistribution.getComponents().size();
            int length3 = ((MultivariateNormalDistribution) ((Pair) mixtureMultivariateNormalDistribution.getComponents().get(0)).getSecond()).getMeans().length;
            if (length3 == length2) {
                this.logLikelihood = Double.NEGATIVE_INFINITY;
                this.fittedModel = new MixtureMultivariateNormalDistribution(mixtureMultivariateNormalDistribution.getComponents());
                int i3 = 0;
                double d2 = 0.0d;
                while (true) {
                    int i4 = i3 + 1;
                    if (i3 <= i2 && FastMath.abs(d2 - this.logLikelihood) > d) {
                        double d3 = this.logLikelihood;
                        List components = this.fittedModel.getComponents();
                        double[] dArr2 = new double[size];
                        MultivariateNormalDistribution[] multivariateNormalDistributionArr = new MultivariateNormalDistribution[size];
                        for (int i5 = 0; i5 < size; i5++) {
                            dArr2[i5] = ((Double) ((Pair) components.get(i5)).getFirst()).doubleValue();
                            multivariateNormalDistributionArr[i5] = (MultivariateNormalDistribution) ((Pair) components.get(i5)).getSecond();
                        }
                        double[][] dArr3 = (double[][]) Array.newInstance((Class<?>) cls, length, size);
                        double[] dArr4 = new double[size];
                        double[][] dArr5 = (double[][]) Array.newInstance((Class<?>) cls, size, length2);
                        double d4 = 0.0d;
                        for (int i6 = 0; i6 < length; i6++) {
                            double density = this.fittedModel.density(this.data[i6]);
                            d4 += FastMath.log(density);
                            int i7 = 0;
                            while (i7 < size) {
                                double d5 = d3;
                                dArr3[i6][i7] = (dArr2[i7] * multivariateNormalDistributionArr[i7].density(this.data[i6])) / density;
                                dArr4[i7] = dArr4[i7] + dArr3[i6][i7];
                                for (int i8 = 0; i8 < length2; i8++) {
                                    double[] dArr6 = dArr5[i7];
                                    dArr6[i8] = dArr6[i8] + (dArr3[i6][i7] * this.data[i6][i8]);
                                }
                                i7++;
                                d3 = d5;
                            }
                        }
                        double d6 = d3;
                        double d7 = (double) length;
                        Double.isNaN(d7);
                        this.logLikelihood = d4 / d7;
                        double[] dArr7 = new double[size];
                        double[][] dArr8 = (double[][]) Array.newInstance((Class<?>) cls, size, length2);
                        for (int i9 = 0; i9 < size; i9++) {
                            double d8 = dArr4[i9];
                            Double.isNaN(d7);
                            dArr7[i9] = d8 / d7;
                            for (int i10 = 0; i10 < length2; i10++) {
                                dArr8[i9][i10] = dArr5[i9][i10] / dArr4[i9];
                            }
                        }
                        RealMatrix[] realMatrixArr = new RealMatrix[size];
                        for (int i11 = 0; i11 < size; i11++) {
                            realMatrixArr[i11] = new Array2DRowRealMatrix(length2, length2);
                        }
                        for (int i12 = 0; i12 < length; i12++) {
                            int i13 = 0;
                            while (i13 < size) {
                                Array2DRowRealMatrix array2DRowRealMatrix = new Array2DRowRealMatrix(MathArrays.ebeSubtract(this.data[i12], dArr8[i13]));
                                realMatrixArr[i13] = realMatrixArr[i13].add(array2DRowRealMatrix.multiply(array2DRowRealMatrix.transpose()).scalarMultiply(dArr3[i12][i13]));
                                i13++;
                                i4 = i4;
                            }
                        }
                        int i14 = i4;
                        double[][][] dArr9 = (double[][][]) Array.newInstance((Class<?>) cls, size, length2, length2);
                        for (int i15 = 0; i15 < size; i15++) {
                            realMatrixArr[i15] = realMatrixArr[i15].scalarMultiply(1.0d / dArr4[i15]);
                            dArr9[i15] = realMatrixArr[i15].getData();
                        }
                        this.fittedModel = new MixtureMultivariateNormalDistribution(dArr7, dArr8, dArr9);
                        i3 = i14;
                        i2 = i;
                        d2 = d6;
                    }
                }
                if (FastMath.abs(d2 - this.logLikelihood) > d) {
                    throw new ConvergenceException();
                }
                return;
            }
            throw new DimensionMismatchException(length3, length2);
        } else {
            throw new NotStrictlyPositiveException(Double.valueOf(d));
        }
    }

    public void fit(MixtureMultivariateNormalDistribution mixtureMultivariateNormalDistribution) throws SingularMatrixException, NotStrictlyPositiveException {
        fit(mixtureMultivariateNormalDistribution, 1000, 1.0E-5d);
    }

    public static MixtureMultivariateNormalDistribution estimate(double[][] dArr, int i) throws NotStrictlyPositiveException, DimensionMismatchException {
        double[][] dArr2 = dArr;
        int i2 = i;
        if (dArr2.length < 2) {
            throw new NotStrictlyPositiveException(Integer.valueOf(dArr2.length));
        } else if (i2 < 2) {
            throw new NumberIsTooSmallException(Integer.valueOf(i), 2, true);
        } else if (i2 <= dArr2.length) {
            int length = dArr2.length;
            int i3 = 0;
            int length2 = dArr2[0].length;
            DataRow[] dataRowArr = new DataRow[length];
            for (int i4 = 0; i4 < length; i4++) {
                dataRowArr[i4] = new DataRow(dArr2[i4]);
            }
            Arrays.sort(dataRowArr);
            double d = (double) i2;
            Double.isNaN(d);
            double d2 = 1.0d / d;
            ArrayList arrayList = new ArrayList(i2);
            int i5 = 0;
            while (i5 < i2) {
                int i6 = (i5 * length) / i2;
                i5++;
                int i7 = (i5 * length) / i2;
                int i8 = i7 - i6;
                double[][] dArr3 = (double[][]) Array.newInstance(double.class, i8, length2);
                double[] dArr4 = new double[length2];
                int i9 = 0;
                while (i6 < i7) {
                    while (i3 < length2) {
                        double d3 = dataRowArr[i6].getRow()[i3];
                        dArr4[i3] = dArr4[i3] + d3;
                        dArr3[i9][i3] = d3;
                        i3++;
                    }
                    i6++;
                    i9++;
                    i3 = 0;
                }
                double d4 = (double) i8;
                Double.isNaN(d4);
                MathArrays.scaleInPlace(1.0d / d4, dArr4);
                arrayList.add(new Pair(Double.valueOf(d2), new MultivariateNormalDistribution(dArr4, new Covariance(dArr3).getCovarianceMatrix().getData())));
                i3 = 0;
            }
            return new MixtureMultivariateNormalDistribution(arrayList);
        } else {
            throw new NumberIsTooLargeException(Integer.valueOf(i), Integer.valueOf(dArr2.length), true);
        }
    }

    public double getLogLikelihood() {
        return this.logLikelihood;
    }

    public MixtureMultivariateNormalDistribution getFittedModel() {
        return new MixtureMultivariateNormalDistribution(this.fittedModel.getComponents());
    }

    private static class DataRow implements Comparable<DataRow> {
        private Double mean = Double.valueOf(0.0d);
        private final double[] row;

        DataRow(double[] dArr) {
            this.row = dArr;
            for (double d : dArr) {
                this.mean = Double.valueOf(this.mean.doubleValue() + d);
            }
            double doubleValue = this.mean.doubleValue();
            double length = (double) dArr.length;
            Double.isNaN(length);
            this.mean = Double.valueOf(doubleValue / length);
        }

        public int compareTo(DataRow dataRow) {
            return this.mean.compareTo(dataRow.mean);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof DataRow) {
                return MathArrays.equals(this.row, ((DataRow) obj).row);
            }
            return false;
        }

        public int hashCode() {
            return Arrays.hashCode(this.row);
        }

        public double[] getRow() {
            return this.row;
        }
    }
}
