package org.apache.commons.math3.p067ml.clustering.evaluation;

import java.util.List;
import org.apache.commons.math3.p067ml.clustering.CentroidCluster;
import org.apache.commons.math3.p067ml.clustering.Cluster;
import org.apache.commons.math3.p067ml.clustering.Clusterable;
import org.apache.commons.math3.p067ml.clustering.DoublePoint;
import org.apache.commons.math3.p067ml.distance.DistanceMeasure;
import org.apache.commons.math3.p067ml.distance.EuclideanDistance;

/* renamed from: org.apache.commons.math3.ml.clustering.evaluation.ClusterEvaluator */
public abstract class ClusterEvaluator<T extends Clusterable> {
    private final DistanceMeasure measure;

    public boolean isBetterScore(double d, double d2) {
        return d < d2;
    }

    public abstract double score(List<? extends Cluster<T>> list);

    public ClusterEvaluator() {
        this(new EuclideanDistance());
    }

    public ClusterEvaluator(DistanceMeasure distanceMeasure) {
        this.measure = distanceMeasure;
    }

    /* access modifiers changed from: protected */
    public double distance(Clusterable clusterable, Clusterable clusterable2) {
        return this.measure.compute(clusterable.getPoint(), clusterable2.getPoint());
    }

    /* access modifiers changed from: protected */
    public Clusterable centroidOf(Cluster<T> cluster) {
        List<T> points = cluster.getPoints();
        if (points.isEmpty()) {
            return null;
        }
        if (cluster instanceof CentroidCluster) {
            return ((CentroidCluster) cluster).getCenter();
        }
        double[] dArr = new double[((Clusterable) points.get(0)).getPoint().length];
        for (T t : points) {
            double[] point = t.getPoint();
            for (int i = 0; i < dArr.length; i++) {
                dArr[i] = dArr[i] + point[i];
            }
        }
        for (int i2 = 0; i2 < dArr.length; i2++) {
            double d = dArr[i2];
            double size = (double) points.size();
            Double.isNaN(size);
            dArr[i2] = d / size;
        }
        return new DoublePoint(dArr);
    }
}
