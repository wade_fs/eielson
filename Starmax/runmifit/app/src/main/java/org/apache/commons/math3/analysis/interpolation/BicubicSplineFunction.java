package org.apache.commons.math3.analysis.interpolation;

import java.lang.reflect.Array;
import org.apache.commons.math3.analysis.BivariateFunction;
import org.apache.commons.math3.exception.OutOfRangeException;

/* compiled from: BicubicSplineInterpolatingFunction */
class BicubicSplineFunction implements BivariateFunction {

    /* renamed from: N */
    private static final short f7953N = 4;

    /* renamed from: a */
    private final double[][] f7954a = ((double[][]) Array.newInstance(double.class, 4, 4));
    private BivariateFunction partialDerivativeX;
    private BivariateFunction partialDerivativeXX;
    private BivariateFunction partialDerivativeXY;
    private BivariateFunction partialDerivativeY;
    private BivariateFunction partialDerivativeYY;

    public BicubicSplineFunction(double[] dArr) {
        for (int i = 0; i < 4; i++) {
            for (int i2 = 0; i2 < 4; i2++) {
                this.f7954a[i][i2] = dArr[(i * 4) + i2];
            }
        }
    }

    public double value(double d, double d2) {
        if (d < 0.0d || d > 1.0d) {
            throw new OutOfRangeException(Double.valueOf(d), 0, 1);
        } else if (d2 < 0.0d || d2 > 1.0d) {
            throw new OutOfRangeException(Double.valueOf(d2), 0, 1);
        } else {
            double d3 = d * d;
            double[] dArr = {1.0d, d, d3, d3 * d};
            double d4 = d2 * d2;
            return apply(dArr, new double[]{1.0d, d2, d4, d4 * d2}, this.f7954a);
        }
    }

    /* access modifiers changed from: private */
    public double apply(double[] dArr, double[] dArr2, double[][] dArr3) {
        double d = 0.0d;
        int i = 0;
        while (i < 4) {
            double d2 = d;
            for (int i2 = 0; i2 < 4; i2++) {
                d2 += dArr3[i][i2] * dArr[i] * dArr2[i2];
            }
            i++;
            d = d2;
        }
        return d;
    }

    public BivariateFunction partialDerivativeX() {
        if (this.partialDerivativeX == null) {
            computePartialDerivatives();
        }
        return this.partialDerivativeX;
    }

    public BivariateFunction partialDerivativeY() {
        if (this.partialDerivativeY == null) {
            computePartialDerivatives();
        }
        return this.partialDerivativeY;
    }

    public BivariateFunction partialDerivativeXX() {
        if (this.partialDerivativeXX == null) {
            computePartialDerivatives();
        }
        return this.partialDerivativeXX;
    }

    public BivariateFunction partialDerivativeYY() {
        if (this.partialDerivativeYY == null) {
            computePartialDerivatives();
        }
        return this.partialDerivativeYY;
    }

    public BivariateFunction partialDerivativeXY() {
        if (this.partialDerivativeXY == null) {
            computePartialDerivatives();
        }
        return this.partialDerivativeXY;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    private void computePartialDerivatives() {
        Class<double> cls = double.class;
        final double[][] dArr = (double[][]) Array.newInstance((Class<?>) cls, 4, 4);
        final double[][] dArr2 = (double[][]) Array.newInstance((Class<?>) cls, 4, 4);
        final double[][] dArr3 = (double[][]) Array.newInstance((Class<?>) cls, 4, 4);
        final double[][] dArr4 = (double[][]) Array.newInstance((Class<?>) cls, 4, 4);
        final double[][] dArr5 = (double[][]) Array.newInstance((Class<?>) cls, 4, 4);
        for (int i = 0; i < 4; i++) {
            for (int i2 = 0; i2 < 4; i2++) {
                double d = this.f7954a[i][i2];
                double[] dArr6 = dArr[i];
                double d2 = (double) i;
                Double.isNaN(d2);
                dArr6[i2] = d2 * d;
                double[] dArr7 = dArr2[i];
                double d3 = (double) i2;
                Double.isNaN(d3);
                dArr7[i2] = d * d3;
                double[] dArr8 = dArr3[i];
                double d4 = (double) (i - 1);
                double d5 = dArr[i][i2];
                Double.isNaN(d4);
                dArr8[i2] = d4 * d5;
                double[] dArr9 = dArr4[i];
                double d6 = (double) (i2 - 1);
                double d7 = dArr2[i][i2];
                Double.isNaN(d6);
                dArr9[i2] = d6 * d7;
                double[] dArr10 = dArr5[i];
                double d8 = dArr[i][i2];
                Double.isNaN(d3);
                dArr10[i2] = d3 * d8;
            }
        }
        this.partialDerivativeX = new BivariateFunction() {
            /* class org.apache.commons.math3.analysis.interpolation.BicubicSplineFunction.C34861 */

            public double value(double d, double d2) {
                double d3 = d2 * d2;
                return BicubicSplineFunction.this.apply(new double[]{0.0d, 1.0d, d, d * d}, new double[]{1.0d, d2, d3, d3 * d2}, dArr);
            }
        };
        this.partialDerivativeY = new BivariateFunction() {
            /* class org.apache.commons.math3.analysis.interpolation.BicubicSplineFunction.C34872 */

            public double value(double d, double d2) {
                double d3 = d * d;
                return BicubicSplineFunction.this.apply(new double[]{1.0d, d, d3, d3 * d}, new double[]{0.0d, 1.0d, d2, d2 * d2}, dArr2);
            }
        };
        this.partialDerivativeXX = new BivariateFunction() {
            /* class org.apache.commons.math3.analysis.interpolation.BicubicSplineFunction.C34883 */

            public double value(double d, double d2) {
                double[] dArr = {0.0d, 0.0d, 1.0d, d};
                double d3 = d2 * d2;
                return BicubicSplineFunction.this.apply(dArr, new double[]{1.0d, d2, d3, d3 * d2}, dArr3);
            }
        };
        this.partialDerivativeYY = new BivariateFunction() {
            /* class org.apache.commons.math3.analysis.interpolation.BicubicSplineFunction.C34894 */

            public double value(double d, double d2) {
                double d3 = d * d;
                return BicubicSplineFunction.this.apply(new double[]{1.0d, d, d3, d3 * d}, new double[]{0.0d, 0.0d, 1.0d, d2}, dArr4);
            }
        };
        this.partialDerivativeXY = new BivariateFunction() {
            /* class org.apache.commons.math3.analysis.interpolation.BicubicSplineFunction.C34905 */

            public double value(double d, double d2) {
                return BicubicSplineFunction.this.apply(new double[]{0.0d, 1.0d, d, d * d}, new double[]{0.0d, 1.0d, d2, d2 * d2}, dArr5);
            }
        };
    }
}
