package org.apache.commons.math3.geometry.partitioning;

import org.apache.commons.math3.geometry.Space;
import org.apache.commons.math3.geometry.partitioning.SubHyperplane;

public abstract class AbstractSubHyperplane<S extends Space, T extends Space> implements SubHyperplane<S> {
    private final Hyperplane<S> hyperplane;
    private final Region<T> remainingRegion;

    /* access modifiers changed from: protected */
    public abstract AbstractSubHyperplane<S, T> buildNew(Hyperplane<S> hyperplane2, Region<T> region);

    public abstract Side side(Hyperplane<S> hyperplane2);

    public abstract SubHyperplane.SplitSubHyperplane<S> split(Hyperplane<S> hyperplane2);

    protected AbstractSubHyperplane(Hyperplane<S> hyperplane2, Region<T> region) {
        this.hyperplane = hyperplane2;
        this.remainingRegion = region;
    }

    public AbstractSubHyperplane<S, T> copySelf() {
        return buildNew(this.hyperplane.copySelf(), this.remainingRegion);
    }

    public Hyperplane<S> getHyperplane() {
        return this.hyperplane;
    }

    public Region<T> getRemainingRegion() {
        return this.remainingRegion;
    }

    public double getSize() {
        return this.remainingRegion.getSize();
    }

    public AbstractSubHyperplane<S, T> reunite(SubHyperplane<S> subHyperplane) {
        return buildNew(this.hyperplane, new RegionFactory().union(this.remainingRegion, ((AbstractSubHyperplane) subHyperplane).remainingRegion));
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [org.apache.commons.math3.geometry.partitioning.Transform<S, T>, org.apache.commons.math3.geometry.partitioning.Transform] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.math3.geometry.partitioning.AbstractSubHyperplane<S, T> applyTransform(org.apache.commons.math3.geometry.partitioning.Transform<S, T> r4) {
        /*
            r3 = this;
            org.apache.commons.math3.geometry.partitioning.Hyperplane<S> r0 = r3.hyperplane
            org.apache.commons.math3.geometry.partitioning.Hyperplane r0 = r4.apply(r0)
            org.apache.commons.math3.geometry.partitioning.Region<T> r1 = r3.remainingRegion
            r2 = 0
            org.apache.commons.math3.geometry.partitioning.BSPTree r1 = r1.getTree(r2)
            org.apache.commons.math3.geometry.partitioning.BSPTree r4 = r3.recurseTransform(r1, r0, r4)
            org.apache.commons.math3.geometry.partitioning.Region<T> r1 = r3.remainingRegion
            org.apache.commons.math3.geometry.partitioning.Region r4 = r1.buildNew(r4)
            org.apache.commons.math3.geometry.partitioning.AbstractSubHyperplane r4 = r3.buildNew(r0, r4)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.geometry.partitioning.AbstractSubHyperplane.applyTransform(org.apache.commons.math3.geometry.partitioning.Transform):org.apache.commons.math3.geometry.partitioning.AbstractSubHyperplane");
    }

    private BSPTree<T> recurseTransform(BSPTree<T> bSPTree, Hyperplane<S> hyperplane2, Transform<S, T> transform) {
        if (bSPTree.getCut() == null) {
            return new BSPTree<>(bSPTree.getAttribute());
        }
        BoundaryAttribute boundaryAttribute = (BoundaryAttribute) bSPTree.getAttribute();
        if (boundaryAttribute != null) {
            SubHyperplane<T> subHyperplane = null;
            SubHyperplane<T> apply = boundaryAttribute.getPlusOutside() == null ? null : transform.apply(boundaryAttribute.getPlusOutside(), this.hyperplane, hyperplane2);
            if (boundaryAttribute.getPlusInside() != null) {
                subHyperplane = transform.apply(boundaryAttribute.getPlusInside(), this.hyperplane, hyperplane2);
            }
            boundaryAttribute = new BoundaryAttribute(apply, subHyperplane);
        }
        return new BSPTree<>(transform.apply(bSPTree.getCut(), this.hyperplane, hyperplane2), recurseTransform(bSPTree.getPlus(), hyperplane2, transform), recurseTransform(bSPTree.getMinus(), hyperplane2, transform), boundaryAttribute);
    }

    public boolean isEmpty() {
        return this.remainingRegion.isEmpty();
    }
}
