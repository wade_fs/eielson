package org.apache.commons.math3.ode.nonstiff;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.ode.EquationsMapper;
import org.apache.commons.math3.ode.ExpandableStatefulODE;
import org.apache.commons.math3.ode.sampling.NordsieckStepInterpolator;
import org.apache.commons.math3.util.FastMath;

public class AdamsBashforthIntegrator extends AdamsIntegrator {
    private static final String METHOD_NAME = "Adams-Bashforth";

    public AdamsBashforthIntegrator(int i, double d, double d2, double d3, double d4) throws NumberIsTooSmallException {
        super(METHOD_NAME, i, i, d, d2, d3, d4);
    }

    public AdamsBashforthIntegrator(int i, double d, double d2, double[] dArr, double[] dArr2) throws IllegalArgumentException {
        super(METHOD_NAME, i, i, d, d2, dArr, dArr2);
    }

    public void integrate(ExpandableStatefulODE expandableStatefulODE, double d) throws NumberIsTooSmallException, DimensionMismatchException, MaxCountExceededException, NoBracketingException {
        NordsieckStepInterpolator nordsieckStepInterpolator;
        boolean z;
        boolean z2;
        double d2;
        double d3;
        ExpandableStatefulODE expandableStatefulODE2 = expandableStatefulODE;
        sanityChecks(expandableStatefulODE, d);
        setEquations(expandableStatefulODE);
        boolean z3 = d > expandableStatefulODE.getTime();
        double[] completeState = expandableStatefulODE.getCompleteState();
        double[] dArr = (double[]) completeState.clone();
        double[] dArr2 = new double[dArr.length];
        NordsieckStepInterpolator nordsieckStepInterpolator2 = new NordsieckStepInterpolator();
        nordsieckStepInterpolator2.reinitialize(dArr, z3, expandableStatefulODE.getPrimaryMapper(), expandableStatefulODE.getSecondaryMappers());
        double d4 = d;
        initIntegration(expandableStatefulODE.getTime(), completeState, d4);
        start(expandableStatefulODE.getTime(), dArr, d4);
        NordsieckStepInterpolator nordsieckStepInterpolator3 = nordsieckStepInterpolator2;
        nordsieckStepInterpolator2.reinitialize(this.stepStart, this.stepSize, this.scaled, this.nordsieck);
        nordsieckStepInterpolator3.storeTime(this.stepStart);
        int rowDimension = this.nordsieck.getRowDimension() - 1;
        double d5 = this.stepSize;
        nordsieckStepInterpolator3.rescale(d5);
        this.isLastStep = false;
        while (true) {
            double d6 = 10.0d;
            while (d6 >= 1.0d) {
                this.stepSize = d5;
                double d7 = 0.0d;
                int i = 0;
                while (i < this.mainSetDimension) {
                    double abs = FastMath.abs(dArr[i]);
                    NordsieckStepInterpolator nordsieckStepInterpolator4 = nordsieckStepInterpolator;
                    if (this.vecAbsoluteTolerance == null) {
                        d3 = this.scalAbsoluteTolerance;
                        d2 = this.scalRelativeTolerance;
                    } else {
                        d3 = this.vecAbsoluteTolerance[i];
                        d2 = this.vecRelativeTolerance[i];
                    }
                    double entry = this.nordsieck.getEntry(rowDimension, i) / (d3 + (d2 * abs));
                    d7 += entry * entry;
                    i++;
                    nordsieckStepInterpolator = nordsieckStepInterpolator4;
                }
                NordsieckStepInterpolator nordsieckStepInterpolator5 = nordsieckStepInterpolator;
                double d8 = (double) this.mainSetDimension;
                Double.isNaN(d8);
                d6 = FastMath.sqrt(d7 / d8);
                if (d6 >= 1.0d) {
                    d5 = filterStep(this.stepSize * computeStepGrowShrinkFactor(d6), z3, false);
                    NordsieckStepInterpolator nordsieckStepInterpolator6 = nordsieckStepInterpolator5;
                    nordsieckStepInterpolator6.rescale(d5);
                    nordsieckStepInterpolator = nordsieckStepInterpolator6;
                } else {
                    nordsieckStepInterpolator = nordsieckStepInterpolator5;
                }
            }
            NordsieckStepInterpolator nordsieckStepInterpolator7 = nordsieckStepInterpolator;
            int i2 = 0;
            double d9 = d5;
            double d10 = this.stepSize + this.stepStart;
            nordsieckStepInterpolator7.shift();
            nordsieckStepInterpolator7.setInterpolatedTime(d10);
            ExpandableStatefulODE expandable = getExpandable();
            expandable.getPrimaryMapper().insertEquationData(nordsieckStepInterpolator7.getInterpolatedState(), dArr);
            EquationsMapper[] secondaryMappers = expandable.getSecondaryMappers();
            int length = secondaryMappers.length;
            int i3 = 0;
            while (i3 < length) {
                secondaryMappers[i3].insertEquationData(nordsieckStepInterpolator7.getInterpolatedSecondaryState(i2), dArr);
                i2++;
                i3++;
                d6 = d6;
            }
            double d11 = d6;
            computeDerivatives(d10, dArr, dArr2);
            double[] dArr3 = new double[completeState.length];
            for (int i4 = 0; i4 < completeState.length; i4++) {
                dArr3[i4] = this.stepSize * dArr2[i4];
            }
            Array2DRowRealMatrix updateHighOrderDerivativesPhase1 = updateHighOrderDerivativesPhase1(this.nordsieck);
            updateHighOrderDerivativesPhase2(this.scaled, dArr3, updateHighOrderDerivativesPhase1);
            double d12 = d10;
            nordsieckStepInterpolator7.reinitialize(d12, this.stepSize, dArr3, updateHighOrderDerivativesPhase1);
            nordsieckStepInterpolator7.storeTime(d10);
            boolean z4 = z3;
            double[] dArr4 = completeState;
            double d13 = d11;
            int i5 = rowDimension;
            this.stepStart = acceptStep(nordsieckStepInterpolator7, dArr, dArr2, d);
            this.scaled = dArr3;
            this.nordsieck = updateHighOrderDerivativesPhase1;
            nordsieckStepInterpolator7.reinitialize(d12, this.stepSize, this.scaled, this.nordsieck);
            if (!this.isLastStep) {
                nordsieckStepInterpolator7.storeTime(this.stepStart);
                if (this.resetOccurred) {
                    start(this.stepStart, dArr, d);
                    nordsieckStepInterpolator7.reinitialize(this.stepStart, this.stepSize, this.scaled, this.nordsieck);
                }
                double computeStepGrowShrinkFactor = this.stepSize * computeStepGrowShrinkFactor(d13);
                double d14 = this.stepStart + computeStepGrowShrinkFactor;
                if (!z4 ? d14 > d : d14 < d) {
                    z = z4;
                    z2 = false;
                } else {
                    z = z4;
                    z2 = true;
                }
                d5 = filterStep(computeStepGrowShrinkFactor, z, z2);
                double d15 = this.stepStart + d5;
                if (!z ? d15 <= d : d15 >= d) {
                    d5 = d - this.stepStart;
                }
                nordsieckStepInterpolator7.rescale(d5);
            } else {
                z = z4;
                d5 = d9;
            }
            if (this.isLastStep) {
                expandableStatefulODE2.setTime(this.stepStart);
                expandableStatefulODE2.setCompleteState(dArr);
                resetInternalState();
                return;
            }
            z3 = z;
            rowDimension = i5;
            completeState = dArr4;
            nordsieckStepInterpolator3 = nordsieckStepInterpolator7;
        }
    }
}
