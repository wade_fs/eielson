package org.apache.commons.math3.stat.descriptive.moment;

import java.io.Serializable;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.stat.descriptive.AbstractStorelessUnivariateStatistic;
import org.apache.commons.math3.util.MathUtils;

class FirstMoment extends AbstractStorelessUnivariateStatistic implements Serializable {
    private static final long serialVersionUID = 6112755307178490473L;
    protected double dev;

    /* renamed from: m1 */
    protected double f8174m1;

    /* renamed from: n */
    protected long f8175n;
    protected double nDev;

    public FirstMoment() {
        this.f8175n = 0;
        this.f8174m1 = Double.NaN;
        this.dev = Double.NaN;
        this.nDev = Double.NaN;
    }

    public FirstMoment(FirstMoment firstMoment) throws NullArgumentException {
        copy(firstMoment, this);
    }

    public void increment(double d) {
        if (this.f8175n == 0) {
            this.f8174m1 = 0.0d;
        }
        this.f8175n++;
        double d2 = (double) this.f8175n;
        double d3 = this.f8174m1;
        this.dev = d - d3;
        double d4 = this.dev;
        Double.isNaN(d2);
        this.nDev = d4 / d2;
        this.f8174m1 = d3 + this.nDev;
    }

    public void clear() {
        this.f8174m1 = Double.NaN;
        this.f8175n = 0;
        this.dev = Double.NaN;
        this.nDev = Double.NaN;
    }

    public double getResult() {
        return this.f8174m1;
    }

    public long getN() {
        return this.f8175n;
    }

    public FirstMoment copy() {
        FirstMoment firstMoment = new FirstMoment();
        copy(this, firstMoment);
        return firstMoment;
    }

    public static void copy(FirstMoment firstMoment, FirstMoment firstMoment2) throws NullArgumentException {
        MathUtils.checkNotNull(firstMoment);
        MathUtils.checkNotNull(firstMoment2);
        firstMoment2.setData(firstMoment.getDataRef());
        firstMoment2.f8175n = firstMoment.f8175n;
        firstMoment2.f8174m1 = firstMoment.f8174m1;
        firstMoment2.dev = firstMoment.dev;
        firstMoment2.nDev = firstMoment.nDev;
    }
}
