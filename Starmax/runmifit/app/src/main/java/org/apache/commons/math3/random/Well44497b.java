package org.apache.commons.math3.random;

public class Well44497b extends AbstractWell {

    /* renamed from: K */
    private static final int f8159K = 44497;

    /* renamed from: M1 */
    private static final int f8160M1 = 23;

    /* renamed from: M2 */
    private static final int f8161M2 = 481;

    /* renamed from: M3 */
    private static final int f8162M3 = 229;
    private static final long serialVersionUID = 4032007538246675492L;

    public Well44497b() {
        super(f8159K, 23, f8161M2, f8162M3);
    }

    public Well44497b(int i) {
        super((int) f8159K, 23, (int) f8161M2, (int) f8162M3, i);
    }

    public Well44497b(int[] iArr) {
        super((int) f8159K, 23, (int) f8161M2, (int) f8162M3, iArr);
    }

    public Well44497b(long j) {
        super((int) f8159K, 23, (int) f8161M2, (int) f8162M3, j);
    }

    /* access modifiers changed from: protected */
    public int next(int i) {
        int i2 = this.iRm1[this.index];
        int i3 = this.iRm2[this.index];
        int i4 = this.f8137v[this.index];
        int i5 = this.f8137v[this.f8134i1[this.index]];
        int i6 = this.f8137v[this.f8135i2[this.index]];
        int i7 = this.f8137v[this.f8136i3[this.index]];
        int i8 = (this.f8137v[i2] & -32768) ^ (this.f8137v[i3] & 32767);
        int i9 = (i4 ^ (i4 << 24)) ^ (i5 ^ (i5 >>> 30));
        int i10 = ((i6 << 10) ^ i6) ^ (i7 << 26);
        int i11 = i9 ^ i10;
        int i12 = ((i10 << 9) ^ (i10 >>> 23)) & -67108865;
        if ((i10 & 131072) != 0) {
            i12 ^= -1221985044;
        }
        int i13 = (((i9 ^ (i9 >>> 20)) ^ i8) ^ i12) ^ i11;
        this.f8137v[this.index] = i11;
        this.f8137v[i2] = i13;
        int[] iArr = this.f8137v;
        iArr[i3] = iArr[i3] & -32768;
        this.index = i2;
        int i14 = ((i13 << 7) & -1814227968) ^ i13;
        return (i14 ^ ((i14 << 15) & -99516416)) >>> (32 - i);
    }
}
