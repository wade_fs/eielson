package org.apache.commons.math3.analysis.differentiation;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.MathInternalError;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;

public class DSCompiler {
    private static AtomicReference<DSCompiler[][]> compilers = new AtomicReference<>(null);
    private final int[][][] compIndirection;
    private final int[][] derivativesIndirection;
    private final int[] lowerIndirection;
    private final int[][][] multIndirection;
    private final int order;
    private final int parameters;
    private final int[][] sizes;

    private DSCompiler(int i, int i2, DSCompiler dSCompiler, DSCompiler dSCompiler2) throws NumberIsTooLargeException {
        this.parameters = i;
        this.order = i2;
        this.sizes = compileSizes(i, i2, dSCompiler);
        this.derivativesIndirection = compileDerivativesIndirection(i, i2, dSCompiler, dSCompiler2);
        this.lowerIndirection = compileLowerIndirection(i, i2, dSCompiler, dSCompiler2);
        this.multIndirection = compileMultiplicationIndirection(i, i2, dSCompiler, dSCompiler2, this.lowerIndirection);
        this.compIndirection = compileCompositionIndirection(i, i2, dSCompiler, dSCompiler2, this.sizes, this.derivativesIndirection);
    }

    public static DSCompiler getCompiler(int i, int i2) throws NumberIsTooLargeException {
        int i3;
        int i4;
        DSCompiler dSCompiler;
        DSCompiler[][] dSCompilerArr = compilers.get();
        if (dSCompilerArr != null && dSCompilerArr.length > i && dSCompilerArr[i].length > i2 && dSCompilerArr[i][i2] != null) {
            return dSCompilerArr[i][i2];
        }
        if (dSCompilerArr == null) {
            i3 = 0;
        } else {
            i3 = dSCompilerArr.length;
        }
        int max = FastMath.max(i, i3);
        if (dSCompilerArr == null) {
            i4 = 0;
        } else {
            i4 = dSCompilerArr[0].length;
        }
        DSCompiler[][] dSCompilerArr2 = (DSCompiler[][]) Array.newInstance(DSCompiler.class, max + 1, FastMath.max(i2, i4) + 1);
        if (dSCompilerArr != null) {
            for (int i5 = 0; i5 < dSCompilerArr.length; i5++) {
                System.arraycopy(dSCompilerArr[i5], 0, dSCompilerArr2[i5], 0, dSCompilerArr[i5].length);
            }
        }
        for (int i6 = 0; i6 <= i + i2; i6++) {
            for (int max2 = FastMath.max(0, i6 - i); max2 <= FastMath.min(i2, i6); max2++) {
                int i7 = i6 - max2;
                if (dSCompilerArr2[i7][max2] == null) {
                    DSCompiler dSCompiler2 = null;
                    if (i7 == 0) {
                        dSCompiler = null;
                    } else {
                        dSCompiler = dSCompilerArr2[i7 - 1][max2];
                    }
                    if (max2 != 0) {
                        dSCompiler2 = dSCompilerArr2[i7][max2 - 1];
                    }
                    dSCompilerArr2[i7][max2] = new DSCompiler(i7, max2, dSCompiler, dSCompiler2);
                }
            }
        }
        compilers.compareAndSet(dSCompilerArr, dSCompilerArr2);
        return dSCompilerArr2[i][i2];
    }

    private static int[][] compileSizes(int i, int i2, DSCompiler dSCompiler) {
        int[][] iArr = (int[][]) Array.newInstance(int.class, i + 1, i2 + 1);
        int i3 = 0;
        if (i == 0) {
            Arrays.fill(iArr[0], 1);
        } else {
            System.arraycopy(dSCompiler.sizes, 0, iArr, 0, i);
            iArr[i][0] = 1;
            while (i3 < i2) {
                int i4 = i3 + 1;
                iArr[i][i4] = iArr[i][i3] + iArr[i - 1][i4];
                i3 = i4;
            }
        }
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<int>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    private static int[][] compileDerivativesIndirection(int i, int i2, DSCompiler dSCompiler, DSCompiler dSCompiler2) {
        Class<int> cls = int.class;
        if (i == 0 || i2 == 0) {
            return (int[][]) Array.newInstance((Class<?>) cls, 1, i);
        }
        int length = dSCompiler.derivativesIndirection.length;
        int length2 = dSCompiler2.derivativesIndirection.length;
        int[][] iArr = (int[][]) Array.newInstance((Class<?>) cls, length + length2, i);
        for (int i3 = 0; i3 < length; i3++) {
            System.arraycopy(dSCompiler.derivativesIndirection[i3], 0, iArr[i3], 0, i - 1);
        }
        for (int i4 = 0; i4 < length2; i4++) {
            int i5 = length + i4;
            System.arraycopy(dSCompiler2.derivativesIndirection[i4], 0, iArr[i5], 0, i);
            int[] iArr2 = iArr[i5];
            int i6 = i - 1;
            iArr2[i6] = iArr2[i6] + 1;
        }
        return iArr;
    }

    private static int[] compileLowerIndirection(int i, int i2, DSCompiler dSCompiler, DSCompiler dSCompiler2) {
        if (i == 0 || i2 <= 1) {
            return new int[]{0};
        }
        int[] iArr = dSCompiler.lowerIndirection;
        int length = iArr.length;
        int length2 = dSCompiler2.lowerIndirection.length;
        int[] iArr2 = new int[(length + length2)];
        System.arraycopy(iArr, 0, iArr2, 0, length);
        for (int i3 = 0; i3 < length2; i3++) {
            iArr2[length + i3] = dSCompiler.getSize() + dSCompiler2.lowerIndirection[i3];
        }
        return iArr2;
    }

    private static int[][][] compileMultiplicationIndirection(int i, int i2, DSCompiler dSCompiler, DSCompiler dSCompiler2, int[] iArr) {
        DSCompiler dSCompiler3 = dSCompiler2;
        int i3 = 3;
        if (i == 0 || i2 == 0) {
            return new int[][][]{new int[][]{new int[]{1, 0, 0}}};
        }
        int[][][] iArr2 = dSCompiler.multIndirection;
        int length = iArr2.length;
        int length2 = dSCompiler3.multIndirection.length;
        int[][][] iArr3 = new int[(length + length2)][][];
        System.arraycopy(iArr2, 0, iArr3, 0, length);
        int i4 = 0;
        while (i4 < length2) {
            int[][] iArr4 = dSCompiler3.multIndirection[i4];
            ArrayList arrayList = new ArrayList(iArr4.length * 2);
            for (int i5 = 0; i5 < iArr4.length; i5++) {
                int[] iArr5 = new int[i3];
                iArr5[0] = iArr4[i5][0];
                iArr5[1] = iArr[iArr4[i5][1]];
                iArr5[2] = iArr4[i5][2] + length;
                arrayList.add(iArr5);
                int[] iArr6 = new int[i3];
                iArr6[0] = iArr4[i5][0];
                iArr6[1] = iArr4[i5][1] + length;
                iArr6[2] = iArr[iArr4[i5][2]];
                arrayList.add(iArr6);
            }
            ArrayList arrayList2 = new ArrayList(arrayList.size());
            for (int i6 = 0; i6 < arrayList.size(); i6++) {
                int[] iArr7 = (int[]) arrayList.get(i6);
                if (iArr7[0] > 0) {
                    for (int i7 = i6 + 1; i7 < arrayList.size(); i7++) {
                        int[] iArr8 = (int[]) arrayList.get(i7);
                        if (iArr7[1] == iArr8[1] && iArr7[2] == iArr8[2]) {
                            iArr7[0] = iArr7[0] + iArr8[0];
                            iArr8[0] = 0;
                        }
                    }
                    arrayList2.add(iArr7);
                }
            }
            iArr3[length + i4] = (int[][]) arrayList2.toArray(new int[arrayList2.size()][]);
            i4++;
            i3 = 3;
        }
        return iArr3;
    }

    private static int[][][] compileCompositionIndirection(int i, int i2, DSCompiler dSCompiler, DSCompiler dSCompiler2, int[][] iArr, int[][] iArr2) throws NumberIsTooLargeException {
        int i3 = i;
        int i4 = i2;
        DSCompiler dSCompiler3 = dSCompiler2;
        int[][] iArr3 = iArr;
        char c = 0;
        int i5 = 1;
        if (i3 == 0 || i4 == 0) {
            return new int[][][]{new int[][]{new int[]{1, 0}}};
        }
        int[][][] iArr4 = dSCompiler.compIndirection;
        int length = iArr4.length;
        int length2 = dSCompiler3.compIndirection.length;
        int[][][] iArr5 = new int[(length + length2)][][];
        System.arraycopy(iArr4, 0, iArr5, 0, length);
        int i6 = 0;
        while (i6 < length2) {
            ArrayList arrayList = new ArrayList();
            int[][] iArr6 = dSCompiler3.compIndirection[i6];
            int length3 = iArr6.length;
            int i7 = 0;
            while (i7 < length3) {
                int[] iArr7 = iArr6[i7];
                int[] iArr8 = new int[(iArr7.length + i5)];
                iArr8[c] = iArr7[c];
                iArr8[i5] = iArr7[i5] + 1;
                int[] iArr9 = new int[i3];
                int i8 = i3 - 1;
                iArr9[i8] = i5;
                iArr8[iArr7.length] = getPartialDerivativeIndex(i3, i4, iArr3, iArr9);
                int i9 = i7;
                int i10 = 2;
                while (i10 < iArr7.length) {
                    iArr8[i10] = convertIndex(iArr7[i10], i, dSCompiler3.derivativesIndirection, i, i2, iArr);
                    i10++;
                    iArr7 = iArr7;
                    arrayList = arrayList;
                    length2 = length2;
                    length3 = length3;
                    iArr6 = iArr6;
                    i6 = i6;
                    iArr5 = iArr5;
                }
                int i11 = length3;
                int[][] iArr10 = iArr6;
                int i12 = i6;
                int i13 = length2;
                int[][][] iArr11 = iArr5;
                int[] iArr12 = iArr7;
                ArrayList arrayList2 = arrayList;
                Arrays.sort(iArr8, 2, iArr8.length);
                arrayList2.add(iArr8);
                int i14 = 2;
                while (i14 < iArr12.length) {
                    int[] iArr13 = new int[iArr12.length];
                    iArr13[0] = iArr12[0];
                    iArr13[1] = iArr12[1];
                    int i15 = 2;
                    while (i15 < iArr12.length) {
                        int i16 = i15;
                        iArr13[i16] = convertIndex(iArr12[i15], i, dSCompiler3.derivativesIndirection, i, i2, iArr);
                        if (i16 == i14) {
                            System.arraycopy(iArr2[iArr13[i16]], 0, iArr9, 0, i3);
                            iArr9[i8] = iArr9[i8] + 1;
                            iArr13[i16] = getPartialDerivativeIndex(i3, i4, iArr3, iArr9);
                        }
                        i15 = i16 + 1;
                        dSCompiler3 = dSCompiler2;
                    }
                    Arrays.sort(iArr13, 2, iArr13.length);
                    arrayList2.add(iArr13);
                    i14++;
                    dSCompiler3 = dSCompiler2;
                }
                i7 = i9 + 1;
                dSCompiler3 = dSCompiler2;
                arrayList = arrayList2;
                length2 = i13;
                length3 = i11;
                iArr6 = iArr10;
                i6 = i12;
                c = 0;
                i5 = 1;
                iArr5 = iArr11;
            }
            int i17 = i6;
            int i18 = length2;
            int[][][] iArr14 = iArr5;
            ArrayList arrayList3 = arrayList;
            ArrayList arrayList4 = new ArrayList(arrayList3.size());
            for (int i19 = 0; i19 < arrayList3.size(); i19++) {
                int[] iArr15 = (int[]) arrayList3.get(i19);
                if (iArr15[0] > 0) {
                    for (int i20 = i19 + 1; i20 < arrayList3.size(); i20++) {
                        int[] iArr16 = (int[]) arrayList3.get(i20);
                        boolean z = iArr15.length == iArr16.length;
                        int i21 = 1;
                        while (z && i21 < iArr15.length) {
                            z &= iArr15[i21] == iArr16[i21];
                            i21++;
                        }
                        if (z) {
                            iArr15[0] = iArr15[0] + iArr16[0];
                            iArr16[0] = 0;
                        }
                    }
                    arrayList4.add(iArr15);
                }
            }
            iArr14[length + i17] = (int[][]) arrayList4.toArray(new int[arrayList4.size()][]);
            i6 = i17 + 1;
            iArr5 = iArr14;
            dSCompiler3 = dSCompiler2;
            length2 = i18;
            c = 0;
            i5 = 1;
        }
        return iArr5;
    }

    public int getPartialDerivativeIndex(int... iArr) throws DimensionMismatchException, NumberIsTooLargeException {
        if (iArr.length == getFreeParameters()) {
            return getPartialDerivativeIndex(this.parameters, this.order, this.sizes, iArr);
        }
        throw new DimensionMismatchException(iArr.length, getFreeParameters());
    }

    private static int getPartialDerivativeIndex(int i, int i2, int[][] iArr, int... iArr2) throws NumberIsTooLargeException {
        int i3 = i - 1;
        int i4 = 0;
        int i5 = i2;
        int i6 = 0;
        while (i3 >= 0) {
            int i7 = iArr2[i3];
            i6 += i7;
            if (i6 <= i2) {
                while (true) {
                    int i8 = i7 - 1;
                    if (i7 <= 0) {
                        break;
                    }
                    i4 += iArr[i3][i5];
                    i7 = i8;
                    i5--;
                }
                i3--;
            } else {
                throw new NumberIsTooLargeException(Integer.valueOf(i6), Integer.valueOf(i2), true);
            }
        }
        return i4;
    }

    private static int convertIndex(int i, int i2, int[][] iArr, int i3, int i4, int[][] iArr2) throws NumberIsTooLargeException {
        int[] iArr3 = new int[i3];
        System.arraycopy(iArr[i], 0, iArr3, 0, FastMath.min(i2, i3));
        return getPartialDerivativeIndex(i3, i4, iArr2, iArr3);
    }

    public int[] getPartialDerivativeOrders(int i) {
        return this.derivativesIndirection[i];
    }

    public int getFreeParameters() {
        return this.parameters;
    }

    public int getOrder() {
        return this.order;
    }

    public int getSize() {
        return this.sizes[this.parameters][this.order];
    }

    public void linearCombination(double d, double[] dArr, int i, double d2, double[] dArr2, int i2, double[] dArr3, int i3) {
        for (int i4 = 0; i4 < getSize(); i4++) {
            dArr3[i3 + i4] = MathArrays.linearCombination(d, dArr[i + i4], d2, dArr2[i2 + i4]);
        }
    }

    public void linearCombination(double d, double[] dArr, int i, double d2, double[] dArr2, int i2, double d3, double[] dArr3, int i3, double[] dArr4, int i4) {
        for (int i5 = 0; i5 < getSize(); i5++) {
            dArr4[i4 + i5] = MathArrays.linearCombination(d, dArr[i + i5], d2, dArr2[i2 + i5], d3, dArr3[i3 + i5]);
        }
    }

    public void linearCombination(double d, double[] dArr, int i, double d2, double[] dArr2, int i2, double d3, double[] dArr3, int i3, double d4, double[] dArr4, int i4, double[] dArr5, int i5) {
        for (int i6 = 0; i6 < getSize(); i6++) {
            dArr5[i5 + i6] = MathArrays.linearCombination(d, dArr[i + i6], d2, dArr2[i2 + i6], d3, dArr3[i3 + i6], d4, dArr4[i4 + i6]);
        }
    }

    public void add(double[] dArr, int i, double[] dArr2, int i2, double[] dArr3, int i3) {
        for (int i4 = 0; i4 < getSize(); i4++) {
            dArr3[i3 + i4] = dArr[i + i4] + dArr2[i2 + i4];
        }
    }

    public void subtract(double[] dArr, int i, double[] dArr2, int i2, double[] dArr3, int i3) {
        for (int i4 = 0; i4 < getSize(); i4++) {
            dArr3[i3 + i4] = dArr[i + i4] - dArr2[i2 + i4];
        }
    }

    public void multiply(double[] dArr, int i, double[] dArr2, int i2, double[] dArr3, int i3) {
        int i4 = 0;
        while (true) {
            int[][][] iArr = this.multIndirection;
            if (i4 < iArr.length) {
                int[][] iArr2 = iArr[i4];
                double d = 0.0d;
                for (int i5 = 0; i5 < iArr2.length; i5++) {
                    double d2 = (double) iArr2[i5][0];
                    double d3 = dArr[iArr2[i5][1] + i];
                    Double.isNaN(d2);
                    d += d2 * d3 * dArr2[iArr2[i5][2] + i2];
                }
                dArr3[i3 + i4] = d;
                i4++;
            } else {
                return;
            }
        }
    }

    public void divide(double[] dArr, int i, double[] dArr2, int i2, double[] dArr3, int i3) {
        double[] dArr4 = new double[getSize()];
        int i4 = i;
        pow(dArr2, i4, -1, dArr4, 0);
        multiply(dArr, i4, dArr4, 0, dArr3, i3);
    }

    public void remainder(double[] dArr, int i, double[] dArr2, int i2, double[] dArr3, int i3) {
        double IEEEremainder = FastMath.IEEEremainder(dArr[i], dArr2[i2]);
        double rint = FastMath.rint((dArr[i] - IEEEremainder) / dArr2[i2]);
        dArr3[i3] = IEEEremainder;
        for (int i4 = 1; i4 < getSize(); i4++) {
            dArr3[i3 + i4] = dArr[i + i4] - (dArr2[i2 + i4] * rint);
        }
    }

    public void pow(double d, double[] dArr, int i, double[] dArr2, int i2) {
        int i3 = 1;
        double[] dArr3 = new double[(this.order + 1)];
        if (d != 0.0d) {
            dArr3[0] = FastMath.pow(d, dArr[i]);
            double log = FastMath.log(d);
            while (i3 < dArr3.length) {
                dArr3[i3] = dArr3[i3 - 1] * log;
                i3++;
            }
        } else if (dArr[i] == 0.0d) {
            dArr3[0] = 1.0d;
            double d2 = Double.POSITIVE_INFINITY;
            while (i3 < dArr3.length) {
                d2 = -d2;
                dArr3[i3] = d2;
                i3++;
            }
        } else if (dArr[i] < 0.0d) {
            Arrays.fill(dArr3, Double.NaN);
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void pow(double[] dArr, int i, double d, double[] dArr2, int i2) {
        int i3 = this.order;
        double[] dArr3 = new double[(i3 + 1)];
        double d2 = dArr[i];
        double d3 = (double) i3;
        Double.isNaN(d3);
        double pow = FastMath.pow(d2, d - d3);
        for (int i4 = this.order; i4 > 0; i4--) {
            dArr3[i4] = pow;
            pow *= dArr[i];
        }
        dArr3[0] = pow;
        double d4 = d;
        for (int i5 = 1; i5 <= this.order; i5++) {
            dArr3[i5] = dArr3[i5] * d4;
            double d5 = (double) i5;
            Double.isNaN(d5);
            d4 *= d - d5;
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void pow(double[] dArr, int i, int i2, double[] dArr2, int i3) {
        if (i2 == 0) {
            dArr2[i3] = 1.0d;
            Arrays.fill(dArr2, i3 + 1, i3 + getSize(), 0.0d);
            return;
        }
        int i4 = this.order;
        double[] dArr3 = new double[(i4 + 1)];
        if (i2 > 0) {
            int min = FastMath.min(i4, i2);
            double pow = FastMath.pow(dArr[i], i2 - min);
            while (min > 0) {
                dArr3[min] = pow;
                pow *= dArr[i];
                min--;
            }
            dArr3[0] = pow;
        } else {
            double d = 1.0d / dArr[i];
            double pow2 = FastMath.pow(d, -i2);
            for (int i5 = 0; i5 <= this.order; i5++) {
                dArr3[i5] = pow2;
                pow2 *= d;
            }
        }
        double d2 = (double) i2;
        for (int i6 = 1; i6 <= this.order; i6++) {
            dArr3[i6] = dArr3[i6] * d2;
            double d3 = (double) (i2 - i6);
            Double.isNaN(d3);
            d2 *= d3;
        }
        compose(dArr, i, dArr3, dArr2, i3);
    }

    public void pow(double[] dArr, int i, double[] dArr2, int i2, double[] dArr3, int i3) {
        double[] dArr4 = new double[getSize()];
        log(dArr, i, dArr4, 0);
        double[] dArr5 = new double[getSize()];
        multiply(dArr4, 0, dArr2, i2, dArr5, 0);
        exp(dArr5, 0, dArr3, i3);
    }

    public void rootN(double[] dArr, int i, int i2, double[] dArr2, int i3) {
        double d;
        double d2;
        double d3;
        int i4 = i2;
        double[] dArr3 = new double[(this.order + 1)];
        if (i4 == 2) {
            dArr3[0] = FastMath.sqrt(dArr[i]);
            d = 0.5d / dArr3[0];
        } else {
            if (i4 == 3) {
                dArr3[0] = FastMath.cbrt(dArr[i]);
                d2 = dArr3[0] * 3.0d;
                d3 = dArr3[0];
            } else {
                double d4 = dArr[i];
                d2 = (double) i4;
                Double.isNaN(d2);
                dArr3[0] = FastMath.pow(d4, 1.0d / d2);
                d3 = FastMath.pow(dArr3[0], i4 - 1);
                Double.isNaN(d2);
            }
            d = 1.0d / (d2 * d3);
        }
        double d5 = (double) i4;
        Double.isNaN(d5);
        double d6 = 1.0d / d5;
        double d7 = 1.0d / dArr[i];
        for (int i5 = 1; i5 <= this.order; i5++) {
            dArr3[i5] = d;
            double d8 = (double) i5;
            Double.isNaN(d8);
            d *= (d6 - d8) * d7;
        }
        compose(dArr, i, dArr3, dArr2, i3);
    }

    public void exp(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        Arrays.fill(dArr3, FastMath.exp(dArr[i]));
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void expm1(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        dArr3[0] = FastMath.expm1(dArr[i]);
        Arrays.fill(dArr3, 1, this.order + 1, FastMath.exp(dArr[i]));
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void log(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        dArr3[0] = FastMath.log(dArr[i]);
        if (this.order > 0) {
            double d = 1.0d / dArr[i];
            double d2 = d;
            for (int i3 = 1; i3 <= this.order; i3++) {
                dArr3[i3] = d2;
                double d3 = (double) (-i3);
                Double.isNaN(d3);
                d2 *= d3 * d;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void log1p(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        dArr3[0] = FastMath.log1p(dArr[i]);
        if (this.order > 0) {
            double d = 1.0d / (dArr[i] + 1.0d);
            double d2 = d;
            for (int i3 = 1; i3 <= this.order; i3++) {
                dArr3[i3] = d2;
                double d3 = (double) (-i3);
                Double.isNaN(d3);
                d2 *= d3 * d;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void log10(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        dArr3[0] = FastMath.log10(dArr[i]);
        if (this.order > 0) {
            double d = 1.0d / dArr[i];
            double log = d / FastMath.log(10.0d);
            for (int i3 = 1; i3 <= this.order; i3++) {
                dArr3[i3] = log;
                double d2 = (double) (-i3);
                Double.isNaN(d2);
                log *= d2 * d;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void cos(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        dArr3[0] = FastMath.cos(dArr[i]);
        if (this.order > 0) {
            dArr3[1] = -FastMath.sin(dArr[i]);
            for (int i3 = 2; i3 <= this.order; i3++) {
                dArr3[i3] = -dArr3[i3 - 2];
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void sin(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        dArr3[0] = FastMath.sin(dArr[i]);
        if (this.order > 0) {
            dArr3[1] = FastMath.cos(dArr[i]);
            for (int i3 = 2; i3 <= this.order; i3++) {
                dArr3[i3] = -dArr3[i3 - 2];
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void tan(double[] dArr, int i, double[] dArr2, int i2) {
        double d;
        double d2;
        int i3;
        double[] dArr3 = new double[(this.order + 1)];
        double tan = FastMath.tan(dArr[i]);
        dArr3[0] = tan;
        int i4 = this.order;
        if (i4 > 0) {
            int i5 = 2;
            double[] dArr4 = new double[(i4 + 2)];
            dArr4[1] = 1.0d;
            double d3 = tan * tan;
            int i6 = 1;
            while (i6 <= this.order) {
                int i7 = i6 + 1;
                double d4 = (double) i6;
                double d5 = dArr4[i6];
                Double.isNaN(d4);
                dArr4[i7] = d4 * d5;
                double d6 = 0.0d;
                int i8 = i7;
                while (i8 >= 0) {
                    double d7 = (d6 * d3) + dArr4[i8];
                    if (i8 > i5) {
                        int i9 = i8 - 1;
                        d2 = d3;
                        double d8 = (double) i9;
                        double d9 = dArr4[i9];
                        Double.isNaN(d8);
                        double d10 = d8 * d9;
                        int i10 = i8 - 3;
                        i3 = i7;
                        d = d7;
                        double d11 = (double) i10;
                        double d12 = dArr4[i10];
                        Double.isNaN(d11);
                        dArr4[i8 - 2] = d10 + (d11 * d12);
                    } else {
                        d2 = d3;
                        i3 = i7;
                        d = d7;
                        if (i8 == 2) {
                            dArr4[0] = dArr4[1];
                            i8 -= 2;
                            i7 = i3;
                            d3 = d2;
                            d6 = d;
                            i5 = 2;
                        }
                    }
                    i8 -= 2;
                    i7 = i3;
                    d3 = d2;
                    d6 = d;
                    i5 = 2;
                }
                double d13 = d3;
                int i11 = i7;
                if ((i6 & 1) == 0) {
                    d6 *= tan;
                }
                dArr3[i6] = d6;
                i6 = i11;
                d3 = d13;
                i5 = 2;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void acos(double[] dArr, int i, double[] dArr2, int i2) {
        double d;
        double d2;
        DSCompiler dSCompiler = this;
        double[] dArr3 = new double[(dSCompiler.order + 1)];
        double d3 = dArr[i];
        dArr3[0] = FastMath.acos(d3);
        int i3 = dSCompiler.order;
        if (i3 > 0) {
            double[] dArr4 = new double[i3];
            dArr4[0] = -1.0d;
            double d4 = d3 * d3;
            double d5 = 1.0d / (1.0d - d4);
            double sqrt = FastMath.sqrt(d5);
            dArr3[1] = dArr4[0] * sqrt;
            int i4 = 2;
            double d6 = sqrt;
            int i5 = 2;
            while (i5 <= dSCompiler.order) {
                double d7 = 0.0d;
                int i6 = i5 - 1;
                double d8 = (double) i6;
                double d9 = dArr4[i5 - 2];
                Double.isNaN(d8);
                dArr4[i6] = d8 * d9;
                while (i6 >= 0) {
                    d7 = (d7 * d4) + dArr4[i6];
                    if (i6 > i4) {
                        int i7 = i6 - 1;
                        d2 = d6;
                        double d10 = (double) i7;
                        double d11 = dArr4[i7];
                        Double.isNaN(d10);
                        double d12 = d10 * d11;
                        d = d4;
                        double d13 = (double) ((i5 * 2) - i6);
                        double d14 = dArr4[i6 - 3];
                        Double.isNaN(d13);
                        dArr4[i6 - 2] = d12 + (d13 * d14);
                    } else {
                        d = d4;
                        d2 = d6;
                        if (i6 == 2) {
                            dArr4[0] = dArr4[1];
                            i6 -= 2;
                            i4 = 2;
                            d6 = d2;
                            d4 = d;
                        }
                    }
                    i6 -= 2;
                    i4 = 2;
                    d6 = d2;
                    d4 = d;
                }
                double d15 = d4;
                double d16 = d6;
                if ((i5 & 1) == 0) {
                    d7 *= d3;
                }
                d6 = d16 * d5;
                dArr3[i5] = d7 * d6;
                i5++;
                i4 = 2;
                dSCompiler = this;
                d4 = d15;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void asin(double[] dArr, int i, double[] dArr2, int i2) {
        double d;
        double d2;
        double[] dArr3 = new double[(this.order + 1)];
        double d3 = dArr[i];
        dArr3[0] = FastMath.asin(d3);
        int i3 = this.order;
        if (i3 > 0) {
            double[] dArr4 = new double[i3];
            dArr4[0] = 1.0d;
            double d4 = d3 * d3;
            double d5 = 1.0d / (1.0d - d4);
            double sqrt = FastMath.sqrt(d5);
            dArr3[1] = dArr4[0] * sqrt;
            int i4 = 2;
            double d6 = sqrt;
            int i5 = 2;
            while (i5 <= this.order) {
                double d7 = 0.0d;
                int i6 = i5 - 1;
                double d8 = (double) i6;
                double d9 = dArr4[i5 - 2];
                Double.isNaN(d8);
                dArr4[i6] = d8 * d9;
                while (i6 >= 0) {
                    d7 = (d7 * d4) + dArr4[i6];
                    if (i6 > i4) {
                        int i7 = i6 - 1;
                        d2 = d6;
                        double d10 = (double) i7;
                        double d11 = dArr4[i7];
                        Double.isNaN(d10);
                        double d12 = d10 * d11;
                        d = d4;
                        double d13 = (double) ((i5 * 2) - i6);
                        double d14 = dArr4[i6 - 3];
                        Double.isNaN(d13);
                        dArr4[i6 - 2] = d12 + (d13 * d14);
                    } else {
                        d = d4;
                        d2 = d6;
                        if (i6 == 2) {
                            dArr4[0] = dArr4[1];
                            i6 -= 2;
                            d6 = d2;
                            d4 = d;
                            i4 = 2;
                        }
                    }
                    i6 -= 2;
                    d6 = d2;
                    d4 = d;
                    i4 = 2;
                }
                double d15 = d4;
                double d16 = d6;
                if ((i5 & 1) == 0) {
                    d7 *= d3;
                }
                d6 = d16 * d5;
                dArr3[i5] = d7 * d6;
                i5++;
                d4 = d15;
                i4 = 2;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void atan(double[] dArr, int i, double[] dArr2, int i2) {
        double d;
        int i3;
        double[] dArr3 = new double[(this.order + 1)];
        double d2 = dArr[i];
        dArr3[0] = FastMath.atan(d2);
        int i4 = this.order;
        if (i4 > 0) {
            double[] dArr4 = new double[i4];
            dArr4[0] = 1.0d;
            double d3 = d2 * d2;
            double d4 = 1.0d / (d3 + 1.0d);
            dArr3[1] = dArr4[0] * d4;
            int i5 = 2;
            double d5 = d4;
            int i6 = 2;
            while (i6 <= this.order) {
                int i7 = i6 - 1;
                double d6 = (double) (-i6);
                double d7 = dArr4[i6 - 2];
                Double.isNaN(d6);
                dArr4[i7] = d6 * d7;
                int i8 = i7;
                double d8 = 0.0d;
                while (i8 >= 0) {
                    d8 = (d8 * d3) + dArr4[i8];
                    if (i8 > i5) {
                        int i9 = i8 - 1;
                        i3 = i6;
                        double d9 = (double) i9;
                        double d10 = dArr4[i9];
                        Double.isNaN(d9);
                        double d11 = d9 * d10;
                        d = d3;
                        double d12 = (double) (i9 - (i3 * 2));
                        double d13 = dArr4[i8 - 3];
                        Double.isNaN(d12);
                        dArr4[i8 - 2] = d11 + (d12 * d13);
                    } else {
                        d = d3;
                        i3 = i6;
                        if (i8 == 2) {
                            dArr4[0] = dArr4[1];
                            i8 -= 2;
                            i6 = i3;
                            d3 = d;
                            i5 = 2;
                        }
                    }
                    i8 -= 2;
                    i6 = i3;
                    d3 = d;
                    i5 = 2;
                }
                double d14 = d3;
                int i10 = i6;
                if ((i10 & 1) == 0) {
                    d8 *= d2;
                }
                d5 *= d4;
                dArr3[i10] = d8 * d5;
                i6 = i10 + 1;
                d3 = d14;
                i5 = 2;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void atan2(double[] dArr, int i, double[] dArr2, int i2, double[] dArr3, int i3) {
        double[] dArr4 = new double[getSize()];
        multiply(dArr2, i2, dArr2, i2, dArr4, 0);
        double[] dArr5 = new double[getSize()];
        double[] dArr6 = dArr5;
        multiply(dArr, i, dArr, i, dArr6, 0);
        add(dArr4, 0, dArr5, 0, dArr6, 0);
        rootN(dArr5, 0, 2, dArr4, 0);
        if (dArr2[i2] >= 0.0d) {
            add(dArr4, 0, dArr2, i2, dArr5, 0);
            divide(dArr, i, dArr5, 0, dArr4, 0);
            atan(dArr4, 0, dArr5, 0);
            for (int i4 = 0; i4 < dArr5.length; i4++) {
                dArr3[i3 + i4] = dArr5[i4] * 2.0d;
            }
        } else {
            subtract(dArr4, 0, dArr2, i2, dArr5, 0);
            divide(dArr, i, dArr5, 0, dArr4, 0);
            atan(dArr4, 0, dArr5, 0);
            dArr3[i3] = (dArr5[0] <= 0.0d ? -3.141592653589793d : 3.141592653589793d) - (dArr5[0] * 2.0d);
            for (int i5 = 1; i5 < dArr5.length; i5++) {
                dArr3[i3 + i5] = dArr5[i5] * -2.0d;
            }
        }
        dArr3[i3] = FastMath.atan2(dArr[i], dArr2[i2]);
    }

    public void cosh(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        dArr3[0] = FastMath.cosh(dArr[i]);
        if (this.order > 0) {
            dArr3[1] = FastMath.sinh(dArr[i]);
            for (int i3 = 2; i3 <= this.order; i3++) {
                dArr3[i3] = dArr3[i3 - 2];
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void sinh(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3 = new double[(this.order + 1)];
        dArr3[0] = FastMath.sinh(dArr[i]);
        if (this.order > 0) {
            dArr3[1] = FastMath.cosh(dArr[i]);
            for (int i3 = 2; i3 <= this.order; i3++) {
                dArr3[i3] = dArr3[i3 - 2];
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void tanh(double[] dArr, int i, double[] dArr2, int i2) {
        double d;
        double d2;
        int i3;
        double[] dArr3 = new double[(this.order + 1)];
        double tanh = FastMath.tanh(dArr[i]);
        dArr3[0] = tanh;
        int i4 = this.order;
        if (i4 > 0) {
            int i5 = 2;
            double[] dArr4 = new double[(i4 + 2)];
            dArr4[1] = 1.0d;
            double d3 = tanh * tanh;
            int i6 = 1;
            while (i6 <= this.order) {
                int i7 = i6 + 1;
                double d4 = (double) (-i6);
                double d5 = dArr4[i6];
                Double.isNaN(d4);
                dArr4[i7] = d4 * d5;
                double d6 = 0.0d;
                int i8 = i7;
                while (i8 >= 0) {
                    double d7 = (d6 * d3) + dArr4[i8];
                    if (i8 > i5) {
                        int i9 = i8 - 1;
                        d2 = d3;
                        double d8 = (double) i9;
                        double d9 = dArr4[i9];
                        Double.isNaN(d8);
                        double d10 = d8 * d9;
                        int i10 = i8 - 3;
                        i3 = i7;
                        d = d7;
                        double d11 = (double) i10;
                        double d12 = dArr4[i10];
                        Double.isNaN(d11);
                        dArr4[i8 - 2] = d10 - (d11 * d12);
                    } else {
                        d2 = d3;
                        i3 = i7;
                        d = d7;
                        if (i8 == 2) {
                            dArr4[0] = dArr4[1];
                            i8 -= 2;
                            i7 = i3;
                            d3 = d2;
                            d6 = d;
                            i5 = 2;
                        }
                    }
                    i8 -= 2;
                    i7 = i3;
                    d3 = d2;
                    d6 = d;
                    i5 = 2;
                }
                double d13 = d3;
                int i11 = i7;
                if ((i6 & 1) == 0) {
                    d6 *= tanh;
                }
                dArr3[i6] = d6;
                i6 = i11;
                d3 = d13;
                i5 = 2;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void acosh(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3;
        double[] dArr4 = new double[(this.order + 1)];
        double d = dArr[i];
        dArr4[0] = FastMath.acosh(d);
        int i3 = this.order;
        if (i3 > 0) {
            double[] dArr5 = new double[i3];
            dArr5[0] = 1.0d;
            double d2 = d * d;
            double d3 = 1.0d / (d2 - 1.0d);
            double sqrt = FastMath.sqrt(d3);
            dArr4[1] = dArr5[0] * sqrt;
            double d4 = sqrt;
            int i4 = 2;
            while (i4 <= this.order) {
                double d5 = 0.0d;
                int i5 = i4 - 1;
                double d6 = d4;
                double d7 = (double) (1 - i4);
                double d8 = dArr5[i4 - 2];
                Double.isNaN(d7);
                dArr5[i5] = d7 * d8;
                while (i5 >= 0) {
                    d5 = (d5 * d2) + dArr5[i5];
                    if (i5 > 2) {
                        double d9 = (double) (1 - i5);
                        double d10 = dArr5[i5 - 1];
                        Double.isNaN(d9);
                        dArr3 = dArr5;
                        double d11 = (double) (i5 - (i4 * 2));
                        double d12 = dArr3[i5 - 3];
                        Double.isNaN(d11);
                        dArr3[i5 - 2] = (d9 * d10) + (d11 * d12);
                    } else {
                        dArr3 = dArr5;
                        if (i5 == 2) {
                            dArr3[0] = -dArr3[1];
                            i5 -= 2;
                            dArr5 = dArr3;
                        }
                    }
                    i5 -= 2;
                    dArr5 = dArr3;
                }
                double[] dArr6 = dArr5;
                if ((i4 & 1) == 0) {
                    d5 *= d;
                }
                d4 = d6 * d3;
                dArr4[i4] = d5 * d4;
                i4++;
                dArr5 = dArr6;
            }
        }
        compose(dArr, i, dArr4, dArr2, i2);
    }

    public void asinh(double[] dArr, int i, double[] dArr2, int i2) {
        double[] dArr3;
        double[] dArr4 = new double[(this.order + 1)];
        double d = dArr[i];
        dArr4[0] = FastMath.asinh(d);
        int i3 = this.order;
        if (i3 > 0) {
            double[] dArr5 = new double[i3];
            dArr5[0] = 1.0d;
            double d2 = d * d;
            double d3 = 1.0d / (d2 + 1.0d);
            double sqrt = FastMath.sqrt(d3);
            dArr4[1] = dArr5[0] * sqrt;
            double d4 = sqrt;
            int i4 = 2;
            while (i4 <= this.order) {
                double d5 = 0.0d;
                int i5 = i4 - 1;
                double d6 = d4;
                double d7 = (double) (1 - i4);
                double d8 = dArr5[i4 - 2];
                Double.isNaN(d7);
                dArr5[i5] = d7 * d8;
                while (i5 >= 0) {
                    d5 = (d5 * d2) + dArr5[i5];
                    if (i5 > 2) {
                        int i6 = i5 - 1;
                        double d9 = (double) i6;
                        double d10 = dArr5[i6];
                        Double.isNaN(d9);
                        dArr3 = dArr5;
                        double d11 = (double) (i5 - (i4 * 2));
                        double d12 = dArr3[i5 - 3];
                        Double.isNaN(d11);
                        dArr3[i5 - 2] = (d9 * d10) + (d11 * d12);
                    } else {
                        dArr3 = dArr5;
                        if (i5 == 2) {
                            dArr3[0] = dArr3[1];
                            i5 -= 2;
                            dArr5 = dArr3;
                        }
                    }
                    i5 -= 2;
                    dArr5 = dArr3;
                }
                double[] dArr6 = dArr5;
                if ((i4 & 1) == 0) {
                    d5 *= d;
                }
                d4 = d6 * d3;
                dArr4[i4] = d5 * d4;
                i4++;
                dArr5 = dArr6;
            }
        }
        compose(dArr, i, dArr4, dArr2, i2);
    }

    public void atanh(double[] dArr, int i, double[] dArr2, int i2) {
        double d;
        double d2;
        DSCompiler dSCompiler = this;
        double[] dArr3 = new double[(dSCompiler.order + 1)];
        double d3 = dArr[i];
        dArr3[0] = FastMath.atanh(d3);
        int i3 = dSCompiler.order;
        if (i3 > 0) {
            double[] dArr4 = new double[i3];
            dArr4[0] = 1.0d;
            double d4 = d3 * d3;
            double d5 = 1.0d / (1.0d - d4);
            dArr3[1] = dArr4[0] * d5;
            int i4 = 2;
            double d6 = d5;
            int i5 = 2;
            while (i5 <= dSCompiler.order) {
                double d7 = 0.0d;
                int i6 = i5 - 1;
                double d8 = (double) i5;
                double d9 = dArr4[i5 - 2];
                Double.isNaN(d8);
                dArr4[i6] = d8 * d9;
                int i7 = i6;
                while (i7 >= 0) {
                    d7 = (d7 * d4) + dArr4[i7];
                    if (i7 > i4) {
                        int i8 = i7 - 1;
                        d2 = d4;
                        double d10 = (double) i8;
                        double d11 = dArr4[i8];
                        Double.isNaN(d10);
                        double d12 = d10 * d11;
                        d = d5;
                        double d13 = (double) (((i5 * 2) - i7) + 1);
                        double d14 = dArr4[i7 - 3];
                        Double.isNaN(d13);
                        dArr4[i7 - 2] = d12 + (d13 * d14);
                    } else {
                        d = d5;
                        d2 = d4;
                        if (i7 == 2) {
                            dArr4[0] = dArr4[1];
                            i7 -= 2;
                            d4 = d2;
                            d5 = d;
                            i4 = 2;
                        }
                    }
                    i7 -= 2;
                    d4 = d2;
                    d5 = d;
                    i4 = 2;
                }
                double d15 = d5;
                double d16 = d4;
                if ((i5 & 1) == 0) {
                    d7 *= d3;
                }
                d6 *= d15;
                dArr3[i5] = d7 * d6;
                i5++;
                dSCompiler = this;
                d4 = d16;
                d5 = d15;
                i4 = 2;
            }
        }
        compose(dArr, i, dArr3, dArr2, i2);
    }

    public void compose(double[] dArr, int i, double[] dArr2, double[] dArr3, int i2) {
        int i3 = 0;
        while (true) {
            int[][][] iArr = this.compIndirection;
            if (i3 < iArr.length) {
                int[][] iArr2 = iArr[i3];
                double d = 0.0d;
                for (int[] iArr3 : iArr2) {
                    double d2 = (double) iArr3[0];
                    double d3 = dArr2[iArr3[1]];
                    Double.isNaN(d2);
                    double d4 = d2 * d3;
                    for (int i4 = 2; i4 < iArr3.length; i4++) {
                        d4 *= dArr[iArr3[i4] + i];
                    }
                    d += d4;
                }
                dArr3[i2 + i3] = d;
                i3++;
            } else {
                return;
            }
        }
    }

    public double taylor(double[] dArr, int i, double... dArr2) throws MathArithmeticException {
        double d = 0.0d;
        for (int size = getSize() - 1; size >= 0; size--) {
            int[] partialDerivativeOrders = getPartialDerivativeOrders(size);
            double d2 = dArr[i + size];
            for (int i2 = 0; i2 < partialDerivativeOrders.length; i2++) {
                if (partialDerivativeOrders[i2] > 0) {
                    try {
                        double pow = FastMath.pow(dArr2[i2], partialDerivativeOrders[i2]);
                        double factorial = (double) CombinatoricsUtils.factorial(partialDerivativeOrders[i2]);
                        Double.isNaN(factorial);
                        d2 *= pow / factorial;
                    } catch (NotPositiveException e) {
                        throw new MathInternalError(e);
                    }
                }
            }
            d += d2;
        }
        return d;
    }

    public void checkCompatibility(DSCompiler dSCompiler) throws DimensionMismatchException {
        int i = this.parameters;
        int i2 = dSCompiler.parameters;
        if (i == i2) {
            int i3 = this.order;
            int i4 = dSCompiler.order;
            if (i3 != i4) {
                throw new DimensionMismatchException(i3, i4);
            }
            return;
        }
        throw new DimensionMismatchException(i, i2);
    }
}
