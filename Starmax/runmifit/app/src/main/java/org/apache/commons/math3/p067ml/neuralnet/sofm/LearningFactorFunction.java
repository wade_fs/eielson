package org.apache.commons.math3.p067ml.neuralnet.sofm;

/* renamed from: org.apache.commons.math3.ml.neuralnet.sofm.LearningFactorFunction */
public interface LearningFactorFunction {
    double value(long j);
}
