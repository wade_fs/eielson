package org.apache.commons.math3.stat.interval;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.util.FastMath;

public class NormalApproximationInterval implements BinomialConfidenceInterval {
    public ConfidenceInterval createInterval(int i, int i2, double d) {
        IntervalUtils.checkParameters(i, i2, d);
        double d2 = (double) i2;
        double d3 = (double) i;
        Double.isNaN(d2);
        Double.isNaN(d3);
        double d4 = d2 / d3;
        double inverseCumulativeProbability = new NormalDistribution().inverseCumulativeProbability(1.0d - ((1.0d - d) / 2.0d));
        Double.isNaN(d3);
        double sqrt = inverseCumulativeProbability * FastMath.sqrt((1.0d / d3) * d4 * (1.0d - d4));
        return new ConfidenceInterval(d4 - sqrt, d4 + sqrt, d);
    }
}
