package org.apache.commons.math3.exception;

import org.apache.commons.math3.exception.util.Localizable;
import org.apache.commons.math3.exception.util.LocalizedFormats;

public class OutOfRangeException extends MathIllegalNumberException {
    private static final long serialVersionUID = 111601815794403609L;

    /* renamed from: hi */
    private final Number f7987hi;

    /* renamed from: lo */
    private final Number f7988lo;

    public OutOfRangeException(Number number, Number number2, Number number3) {
        this(LocalizedFormats.OUT_OF_RANGE_SIMPLE, number, number2, number3);
    }

    public OutOfRangeException(Localizable localizable, Number number, Number number2, Number number3) {
        super(localizable, number, number2, number3);
        this.f7988lo = number2;
        this.f7987hi = number3;
    }

    public Number getLo() {
        return this.f7988lo;
    }

    public Number getHi() {
        return this.f7987hi;
    }
}
