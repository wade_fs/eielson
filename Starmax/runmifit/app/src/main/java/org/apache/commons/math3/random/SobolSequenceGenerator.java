package org.apache.commons.math3.random;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Arrays;
import org.apache.commons.math3.exception.MathParseException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.util.FastMath;

public class SobolSequenceGenerator implements RandomVectorGenerator {
    private static final int BITS = 52;
    private static final String FILE_CHARSET = "US-ASCII";
    private static final int MAX_DIMENSION = 1000;
    private static final String RESOURCE_NAME = "/assets/org/apache/commons/math3/random/new-joe-kuo-6.1000";
    private static final double SCALE = FastMath.pow(2.0d, 52);
    private int count = 0;
    private final int dimension;
    private final long[][] direction;

    /* renamed from: x */
    private final long[] f8141x;

    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0038 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SobolSequenceGenerator(int r4) throws org.apache.commons.math3.exception.OutOfRangeException {
        /*
            r3 = this;
            r3.<init>()
            r0 = 0
            r3.count = r0
            r0 = 1000(0x3e8, float:1.401E-42)
            r1 = 1
            if (r4 < r1) goto L_0x004e
            if (r4 > r0) goto L_0x004e
            java.lang.Class r0 = r3.getClass()
            java.lang.String r1 = "/assets/org/apache/commons/math3/random/new-joe-kuo-6.1000"
            java.io.InputStream r0 = r0.getResourceAsStream(r1)
            if (r0 == 0) goto L_0x0048
            r3.dimension = r4
            r1 = 53
            int[] r1 = new int[]{r4, r1}
            java.lang.Class<long> r2 = long.class
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r2, r1)
            long[][] r1 = (long[][]) r1
            r3.direction = r1
            long[] r4 = new long[r4]
            r3.f8141x = r4
            r3.initFromStream(r0)     // Catch:{ IOException -> 0x003e, MathParseException -> 0x0038 }
            r0.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0035:
            return
        L_0x0036:
            r4 = move-exception
            goto L_0x0044
        L_0x0038:
            org.apache.commons.math3.exception.MathInternalError r4 = new org.apache.commons.math3.exception.MathInternalError     // Catch:{ all -> 0x0036 }
            r4.<init>()     // Catch:{ all -> 0x0036 }
            throw r4     // Catch:{ all -> 0x0036 }
        L_0x003e:
            org.apache.commons.math3.exception.MathInternalError r4 = new org.apache.commons.math3.exception.MathInternalError     // Catch:{ all -> 0x0036 }
            r4.<init>()     // Catch:{ all -> 0x0036 }
            throw r4     // Catch:{ all -> 0x0036 }
        L_0x0044:
            r0.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0047:
            throw r4
        L_0x0048:
            org.apache.commons.math3.exception.MathInternalError r4 = new org.apache.commons.math3.exception.MathInternalError
            r4.<init>()
            throw r4
        L_0x004e:
            org.apache.commons.math3.exception.OutOfRangeException r2 = new org.apache.commons.math3.exception.OutOfRangeException
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2.<init>(r4, r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.random.SobolSequenceGenerator.<init>(int):void");
    }

    public SobolSequenceGenerator(int i, InputStream inputStream) throws NotStrictlyPositiveException, MathParseException, IOException {
        if (i >= 1) {
            this.dimension = i;
            this.direction = (long[][]) Array.newInstance(long.class, i, 53);
            this.f8141x = new long[i];
            int initFromStream = initFromStream(inputStream);
            if (initFromStream < i) {
                throw new OutOfRangeException(Integer.valueOf(i), 1, Integer.valueOf(initFromStream));
            }
            return;
        }
        throw new NotStrictlyPositiveException(Integer.valueOf(i));
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x007c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int initFromStream(java.io.InputStream r13) throws org.apache.commons.math3.exception.MathParseException, java.io.IOException {
        /*
            r12 = this;
            r0 = 1
            r1 = 1
        L_0x0002:
            r2 = 52
            if (r1 > r2) goto L_0x0014
            long[][] r3 = r12.direction
            r4 = 0
            r3 = r3[r4]
            r4 = 1
            int r2 = r2 - r1
            long r4 = r4 << r2
            r3[r1] = r4
            int r1 = r1 + 1
            goto L_0x0002
        L_0x0014:
            java.lang.String r1 = "US-ASCII"
            java.nio.charset.Charset r1 = java.nio.charset.Charset.forName(r1)
            java.io.BufferedReader r2 = new java.io.BufferedReader
            java.io.InputStreamReader r3 = new java.io.InputStreamReader
            r3.<init>(r13, r1)
            r2.<init>(r3)
            r13 = -1
            r2.readLine()     // Catch:{ all -> 0x008c }
            r1 = 2
            r3 = 2
            r4 = 1
        L_0x002b:
            java.lang.String r5 = r2.readLine()     // Catch:{ all -> 0x008c }
            if (r5 == 0) goto L_0x0088
            java.util.StringTokenizer r13 = new java.util.StringTokenizer     // Catch:{ all -> 0x008c }
            java.lang.String r6 = " "
            r13.<init>(r5, r6)     // Catch:{ all -> 0x008c }
            java.lang.String r6 = r13.nextToken()     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            if (r6 < r1) goto L_0x0070
            int r7 = r12.dimension     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            if (r6 > r7) goto L_0x0070
            java.lang.String r7 = r13.nextToken()     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            int r7 = java.lang.Integer.parseInt(r7)     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            java.lang.String r8 = r13.nextToken()     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            int r9 = r7 + 1
            int[] r9 = new int[r9]     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            r10 = 1
        L_0x005b:
            if (r10 > r7) goto L_0x006a
            java.lang.String r11 = r13.nextToken()     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            int r11 = java.lang.Integer.parseInt(r11)     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            r9[r10] = r11     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            int r10 = r10 + 1
            goto L_0x005b
        L_0x006a:
            int r13 = r4 + 1
            r12.initDirectionVector(r4, r8, r9)     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            r4 = r13
        L_0x0070:
            int r13 = r12.dimension     // Catch:{ NoSuchElementException -> 0x0082, NumberFormatException -> 0x007c }
            if (r6 <= r13) goto L_0x0078
            r2.close()
            return r6
        L_0x0078:
            int r3 = r3 + 1
            r13 = r6
            goto L_0x002b
        L_0x007c:
            org.apache.commons.math3.exception.MathParseException r13 = new org.apache.commons.math3.exception.MathParseException     // Catch:{ all -> 0x008c }
            r13.<init>(r5, r3)     // Catch:{ all -> 0x008c }
            throw r13     // Catch:{ all -> 0x008c }
        L_0x0082:
            org.apache.commons.math3.exception.MathParseException r13 = new org.apache.commons.math3.exception.MathParseException     // Catch:{ all -> 0x008c }
            r13.<init>(r5, r3)     // Catch:{ all -> 0x008c }
            throw r13     // Catch:{ all -> 0x008c }
        L_0x0088:
            r2.close()
            return r13
        L_0x008c:
            r13 = move-exception
            r2.close()
            goto L_0x0092
        L_0x0091:
            throw r13
        L_0x0092:
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.random.SobolSequenceGenerator.initFromStream(java.io.InputStream):int");
    }

    private void initDirectionVector(int i, int i2, int[] iArr) {
        int[] iArr2 = iArr;
        int length = iArr2.length - 1;
        for (int i3 = 1; i3 <= length; i3++) {
            this.direction[i][i3] = ((long) iArr2[i3]) << (52 - i3);
        }
        for (int i4 = length + 1; i4 <= 52; i4++) {
            long[][] jArr = this.direction;
            int i5 = i4 - length;
            jArr[i][i4] = (jArr[i][i5] >> length) ^ jArr[i][i5];
            int i6 = 1;
            while (true) {
                int i7 = length - 1;
                if (i6 > i7) {
                    break;
                }
                long[][] jArr2 = this.direction;
                long[] jArr3 = jArr2[i];
                jArr3[i4] = jArr3[i4] ^ (((long) ((i2 >> (i7 - i6)) & 1)) * jArr2[i][i4 - i6]);
                i6++;
            }
        }
    }

    public double[] nextVector() {
        double[] dArr = new double[this.dimension];
        int i = this.count;
        if (i == 0) {
            this.count = i + 1;
            return dArr;
        }
        int i2 = i - 1;
        int i3 = 1;
        while ((i2 & 1) == 1) {
            i2 >>= 1;
            i3++;
        }
        for (int i4 = 0; i4 < this.dimension; i4++) {
            long[] jArr = this.f8141x;
            jArr[i4] = jArr[i4] ^ this.direction[i4][i3];
            double d = (double) jArr[i4];
            double d2 = SCALE;
            Double.isNaN(d);
            dArr[i4] = d / d2;
        }
        this.count++;
        return dArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(long[], long):void}
     arg types: [long[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(long[], long):void} */
    public double[] skipTo(int i) throws NotPositiveException {
        if (i == 0) {
            Arrays.fill(this.f8141x, 0L);
        } else {
            int i2 = i - 1;
            long j = (long) (i2 ^ (i2 >> 1));
            for (int i3 = 0; i3 < this.dimension; i3++) {
                long j2 = 0;
                for (int i4 = 1; i4 <= 52; i4++) {
                    long j3 = j >> (i4 - 1);
                    if (j3 == 0) {
                        break;
                    }
                    j2 ^= (j3 & 1) * this.direction[i3][i4];
                }
                this.f8141x[i3] = j2;
            }
        }
        this.count = i;
        return nextVector();
    }

    public int getNextIndex() {
        return this.count;
    }
}
