package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;

public abstract class AbstractPolynomialSolver extends BaseAbstractUnivariateSolver<PolynomialFunction> implements PolynomialSolver {
    private PolynomialFunction polynomialFunction;

    protected AbstractPolynomialSolver(double d) {
        super(d);
    }

    protected AbstractPolynomialSolver(double d, double d2) {
        super(d, d2);
    }

    protected AbstractPolynomialSolver(double d, double d2, double d3) {
        super(d, d2, d3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.analysis.solvers.BaseAbstractUnivariateSolver.setup(int, org.apache.commons.math3.analysis.UnivariateFunction, double, double, double):void
     arg types: [int, org.apache.commons.math3.analysis.polynomials.PolynomialFunction, double, double, double]
     candidates:
      org.apache.commons.math3.analysis.solvers.AbstractPolynomialSolver.setup(int, org.apache.commons.math3.analysis.polynomials.PolynomialFunction, double, double, double):void
      org.apache.commons.math3.analysis.solvers.BaseAbstractUnivariateSolver.setup(int, org.apache.commons.math3.analysis.UnivariateFunction, double, double, double):void */
    /* access modifiers changed from: protected */
    public void setup(int i, PolynomialFunction polynomialFunction2, double d, double d2, double d3) {
        super.setup(i, (UnivariateFunction) polynomialFunction2, d, d2, d3);
        this.polynomialFunction = polynomialFunction2;
    }

    /* access modifiers changed from: protected */
    public double[] getCoefficients() {
        return this.polynomialFunction.getCoefficients();
    }
}
