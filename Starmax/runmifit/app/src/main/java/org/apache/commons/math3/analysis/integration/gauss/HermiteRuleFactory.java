package org.apache.commons.math3.analysis.integration.gauss;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Pair;

public class HermiteRuleFactory extends BaseRuleFactory<Double> {

    /* renamed from: H0 */
    private static final double f7951H0 = 0.7511255444649425d;

    /* renamed from: H1 */
    private static final double f7952H1 = 1.0622519320271968d;
    private static final double SQRT_PI = 1.772453850905516d;

    /* access modifiers changed from: protected */
    public Pair<Double[], Double[]> computeRule(int i) throws DimensionMismatchException {
        double d;
        double d2;
        double d3;
        int i2 = i;
        Double valueOf = Double.valueOf(0.0d);
        int i3 = 0;
        int i4 = 1;
        if (i2 == 1) {
            return new Pair<>(new Double[]{valueOf}, new Double[]{Double.valueOf((double) SQRT_PI)});
        }
        int i5 = i2 - 1;
        Double[] dArr = (Double[]) getRuleInternal(i5).getFirst();
        Double[] dArr2 = new Double[i2];
        Double[] dArr3 = new Double[i2];
        double sqrt = FastMath.sqrt((double) (i5 * 2));
        double sqrt2 = FastMath.sqrt((double) (i2 * 2));
        int i6 = i2 / 2;
        while (true) {
            d = f7951H0;
            if (i3 >= i6) {
                break;
            }
            if (i3 == 0) {
                d2 = -sqrt;
            } else {
                d2 = dArr[i3 - 1].doubleValue();
            }
            if (i6 == i4) {
                d3 = -0.5d;
            } else {
                d3 = dArr[i3].doubleValue();
            }
            double d4 = d2 * f7952H1;
            double d5 = 0.7511255444649425d;
            while (i4 < i2) {
                int i7 = i4 + 1;
                double d6 = sqrt;
                double d7 = (double) i7;
                Double.isNaN(d7);
                double sqrt3 = FastMath.sqrt(2.0d / d7);
                int i8 = i7;
                double d8 = (double) i4;
                Double.isNaN(d8);
                Double.isNaN(d7);
                d5 = d4;
                sqrt = d6;
                i4 = i8;
                d4 = ((sqrt3 * d2) * d4) - (FastMath.sqrt(d8 / d7) * d5);
                dArr = dArr;
            }
            Double[] dArr4 = dArr;
            double d9 = sqrt;
            double d10 = d2;
            double d11 = d4;
            boolean z = false;
            double d12 = d3;
            double d13 = (d2 + d3) * 0.5d;
            double d14 = 0.7511255444649425d;
            while (!z) {
                double d15 = d13;
                boolean z2 = d12 - d10 <= Math.ulp(d13);
                double d16 = d15 * f7952H1;
                double d17 = 0.7511255444649425d;
                int i9 = 1;
                while (i9 < i2) {
                    int i10 = i9 + 1;
                    double d18 = (double) i10;
                    Double.isNaN(d18);
                    double sqrt4 = FastMath.sqrt(2.0d / d18);
                    double d19 = (double) i9;
                    Double.isNaN(d19);
                    Double.isNaN(d18);
                    d17 = d16;
                    d16 = ((sqrt4 * d15) * d16) - (FastMath.sqrt(d19 / d18) * d17);
                    i9 = i10;
                }
                if (!z2) {
                    if (d11 * d16 < 0.0d) {
                        d12 = d15;
                    } else {
                        d10 = d15;
                        d11 = d16;
                    }
                    d13 = (d10 + d12) * 0.5d;
                    z = z2;
                    d14 = d17;
                } else {
                    d13 = d15;
                    z = z2;
                    d14 = d17;
                }
            }
            double d20 = d13;
            double d21 = d14 * sqrt2;
            double d22 = 2.0d / (d21 * d21);
            dArr2[i3] = Double.valueOf(d20);
            dArr3[i3] = Double.valueOf(d22);
            int i11 = i5 - i3;
            dArr2[i11] = Double.valueOf(-d20);
            dArr3[i11] = Double.valueOf(d22);
            i3++;
            sqrt = d9;
            dArr = dArr4;
            i4 = 1;
        }
        if (i2 % 2 != 0) {
            for (int i12 = 1; i12 < i2; i12 += 2) {
                double d23 = (double) (i12 + 1);
                double d24 = (double) i12;
                Double.isNaN(d24);
                Double.isNaN(d23);
                d *= -FastMath.sqrt(d24 / d23);
            }
            double d25 = sqrt2 * d;
            dArr2[i6] = valueOf;
            dArr3[i6] = Double.valueOf(2.0d / (d25 * d25));
        }
        return new Pair<>(dArr2, dArr3);
    }
}
