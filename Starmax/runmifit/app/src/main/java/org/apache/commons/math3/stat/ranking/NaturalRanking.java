package org.apache.commons.math3.stat.ranking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.math3.exception.MathInternalError;
import org.apache.commons.math3.exception.NotANumberException;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.util.FastMath;

public class NaturalRanking implements RankingAlgorithm {
    public static final NaNStrategy DEFAULT_NAN_STRATEGY = NaNStrategy.FAILED;
    public static final TiesStrategy DEFAULT_TIES_STRATEGY = TiesStrategy.AVERAGE;
    private final NaNStrategy nanStrategy;
    private final RandomDataGenerator randomData;
    private final TiesStrategy tiesStrategy;

    public NaturalRanking() {
        this.tiesStrategy = DEFAULT_TIES_STRATEGY;
        this.nanStrategy = DEFAULT_NAN_STRATEGY;
        this.randomData = null;
    }

    public NaturalRanking(TiesStrategy tiesStrategy2) {
        this.tiesStrategy = tiesStrategy2;
        this.nanStrategy = DEFAULT_NAN_STRATEGY;
        this.randomData = new RandomDataGenerator();
    }

    public NaturalRanking(NaNStrategy naNStrategy) {
        this.nanStrategy = naNStrategy;
        this.tiesStrategy = DEFAULT_TIES_STRATEGY;
        this.randomData = null;
    }

    public NaturalRanking(NaNStrategy naNStrategy, TiesStrategy tiesStrategy2) {
        this.nanStrategy = naNStrategy;
        this.tiesStrategy = tiesStrategy2;
        this.randomData = new RandomDataGenerator();
    }

    public NaturalRanking(RandomGenerator randomGenerator) {
        this.tiesStrategy = TiesStrategy.RANDOM;
        this.nanStrategy = DEFAULT_NAN_STRATEGY;
        this.randomData = new RandomDataGenerator(randomGenerator);
    }

    public NaturalRanking(NaNStrategy naNStrategy, RandomGenerator randomGenerator) {
        this.nanStrategy = naNStrategy;
        this.tiesStrategy = TiesStrategy.RANDOM;
        this.randomData = new RandomDataGenerator(randomGenerator);
    }

    public NaNStrategy getNanStrategy() {
        return this.nanStrategy;
    }

    public TiesStrategy getTiesStrategy() {
        return this.tiesStrategy;
    }

    public double[] rank(double[] dArr) {
        IntDoublePair[] intDoublePairArr = new IntDoublePair[dArr.length];
        for (int i = 0; i < dArr.length; i++) {
            intDoublePairArr[i] = new IntDoublePair(dArr[i], i);
        }
        List<Integer> list = null;
        int i2 = C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$NaNStrategy[this.nanStrategy.ordinal()];
        if (i2 == 1) {
            recodeNaNs(intDoublePairArr, Double.POSITIVE_INFINITY);
        } else if (i2 == 2) {
            recodeNaNs(intDoublePairArr, Double.NEGATIVE_INFINITY);
        } else if (i2 == 3) {
            intDoublePairArr = removeNaNs(intDoublePairArr);
        } else if (i2 == 4) {
            list = getNanPositions(intDoublePairArr);
        } else if (i2 == 5) {
            list = getNanPositions(intDoublePairArr);
            if (list.size() > 0) {
                throw new NotANumberException();
            }
        } else {
            throw new MathInternalError();
        }
        Arrays.sort(intDoublePairArr);
        double[] dArr2 = new double[intDoublePairArr.length];
        dArr2[intDoublePairArr[0].getPosition()] = (double) 1;
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf(intDoublePairArr[0].getPosition()));
        int i3 = 1;
        for (int i4 = 1; i4 < intDoublePairArr.length; i4++) {
            if (Double.compare(intDoublePairArr[i4].getValue(), intDoublePairArr[i4 - 1].getValue()) > 0) {
                i3 = i4 + 1;
                if (arrayList.size() > 1) {
                    resolveTie(dArr2, arrayList);
                }
                arrayList = new ArrayList();
                arrayList.add(Integer.valueOf(intDoublePairArr[i4].getPosition()));
            } else {
                arrayList.add(Integer.valueOf(intDoublePairArr[i4].getPosition()));
            }
            dArr2[intDoublePairArr[i4].getPosition()] = (double) i3;
        }
        if (arrayList.size() > 1) {
            resolveTie(dArr2, arrayList);
        }
        if (this.nanStrategy == NaNStrategy.FIXED) {
            restoreNaNs(dArr2, list);
        }
        return dArr2;
    }

    private IntDoublePair[] removeNaNs(IntDoublePair[] intDoublePairArr) {
        if (!containsNaNs(intDoublePairArr)) {
            return intDoublePairArr;
        }
        IntDoublePair[] intDoublePairArr2 = new IntDoublePair[intDoublePairArr.length];
        int i = 0;
        for (int i2 = 0; i2 < intDoublePairArr.length; i2++) {
            if (Double.isNaN(intDoublePairArr[i2].getValue())) {
                for (int i3 = i2 + 1; i3 < intDoublePairArr.length; i3++) {
                    intDoublePairArr[i3] = new IntDoublePair(intDoublePairArr[i3].getValue(), intDoublePairArr[i3].getPosition() - 1);
                }
            } else {
                intDoublePairArr2[i] = new IntDoublePair(intDoublePairArr[i2].getValue(), intDoublePairArr[i2].getPosition());
                i++;
            }
        }
        IntDoublePair[] intDoublePairArr3 = new IntDoublePair[i];
        System.arraycopy(intDoublePairArr2, 0, intDoublePairArr3, 0, i);
        return intDoublePairArr3;
    }

    private void recodeNaNs(IntDoublePair[] intDoublePairArr, double d) {
        for (int i = 0; i < intDoublePairArr.length; i++) {
            if (Double.isNaN(intDoublePairArr[i].getValue())) {
                intDoublePairArr[i] = new IntDoublePair(d, intDoublePairArr[i].getPosition());
            }
        }
    }

    private boolean containsNaNs(IntDoublePair[] intDoublePairArr) {
        for (IntDoublePair intDoublePair : intDoublePairArr) {
            if (Double.isNaN(intDoublePair.getValue())) {
                return true;
            }
        }
        return false;
    }

    private void resolveTie(double[] dArr, List<Integer> list) {
        int i = 0;
        double d = dArr[list.get(0).intValue()];
        int size = list.size();
        int i2 = C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$TiesStrategy[this.tiesStrategy.ordinal()];
        if (i2 == 1) {
            double d2 = (double) size;
            Double.isNaN(d2);
            fill(dArr, list, (((d * 2.0d) + d2) - 1.0d) / 2.0d);
        } else if (i2 == 2) {
            double d3 = (double) size;
            Double.isNaN(d3);
            fill(dArr, list, (d + d3) - 1.0d);
        } else if (i2 == 3) {
            fill(dArr, list, d);
        } else if (i2 == 4) {
            long round = FastMath.round(d);
            for (Integer num : list) {
                dArr[num.intValue()] = (double) this.randomData.nextLong(round, (((long) size) + round) - 1);
            }
        } else if (i2 == 5) {
            long round2 = FastMath.round(d);
            for (Integer num2 : list) {
                dArr[num2.intValue()] = (double) (((long) i) + round2);
                i++;
            }
        } else {
            throw new MathInternalError();
        }
    }

    /* renamed from: org.apache.commons.math3.stat.ranking.NaturalRanking$1 */
    static /* synthetic */ class C36131 {
        static final /* synthetic */ int[] $SwitchMap$org$apache$commons$math3$stat$ranking$NaNStrategy = new int[NaNStrategy.values().length];
        static final /* synthetic */ int[] $SwitchMap$org$apache$commons$math3$stat$ranking$TiesStrategy = new int[TiesStrategy.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|23|24|25|26|27|28|(3:29|30|32)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|23|24|25|26|27|28|(3:29|30|32)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|21|22|23|24|25|26|27|28|(3:29|30|32)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x005d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0067 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0071 */
        static {
            /*
                org.apache.commons.math3.stat.ranking.TiesStrategy[] r0 = org.apache.commons.math3.stat.ranking.TiesStrategy.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$TiesStrategy = r0
                r0 = 1
                int[] r1 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$TiesStrategy     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.stat.ranking.TiesStrategy r2 = org.apache.commons.math3.stat.ranking.TiesStrategy.AVERAGE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$TiesStrategy     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.stat.ranking.TiesStrategy r3 = org.apache.commons.math3.stat.ranking.TiesStrategy.MAXIMUM     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$TiesStrategy     // Catch:{ NoSuchFieldError -> 0x002a }
                org.apache.commons.math3.stat.ranking.TiesStrategy r4 = org.apache.commons.math3.stat.ranking.TiesStrategy.MINIMUM     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$TiesStrategy     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.apache.commons.math3.stat.ranking.TiesStrategy r5 = org.apache.commons.math3.stat.ranking.TiesStrategy.RANDOM     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                r4 = 5
                int[] r5 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$TiesStrategy     // Catch:{ NoSuchFieldError -> 0x0040 }
                org.apache.commons.math3.stat.ranking.TiesStrategy r6 = org.apache.commons.math3.stat.ranking.TiesStrategy.SEQUENTIAL     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r5[r6] = r4     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                org.apache.commons.math3.stat.ranking.NaNStrategy[] r5 = org.apache.commons.math3.stat.ranking.NaNStrategy.values()
                int r5 = r5.length
                int[] r5 = new int[r5]
                org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$NaNStrategy = r5
                int[] r5 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$NaNStrategy     // Catch:{ NoSuchFieldError -> 0x0053 }
                org.apache.commons.math3.stat.ranking.NaNStrategy r6 = org.apache.commons.math3.stat.ranking.NaNStrategy.MAXIMAL     // Catch:{ NoSuchFieldError -> 0x0053 }
                int r6 = r6.ordinal()     // Catch:{ NoSuchFieldError -> 0x0053 }
                r5[r6] = r0     // Catch:{ NoSuchFieldError -> 0x0053 }
            L_0x0053:
                int[] r0 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$NaNStrategy     // Catch:{ NoSuchFieldError -> 0x005d }
                org.apache.commons.math3.stat.ranking.NaNStrategy r5 = org.apache.commons.math3.stat.ranking.NaNStrategy.MINIMAL     // Catch:{ NoSuchFieldError -> 0x005d }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x005d }
                r0[r5] = r1     // Catch:{ NoSuchFieldError -> 0x005d }
            L_0x005d:
                int[] r0 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$NaNStrategy     // Catch:{ NoSuchFieldError -> 0x0067 }
                org.apache.commons.math3.stat.ranking.NaNStrategy r1 = org.apache.commons.math3.stat.ranking.NaNStrategy.REMOVED     // Catch:{ NoSuchFieldError -> 0x0067 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0067 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0067 }
            L_0x0067:
                int[] r0 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$NaNStrategy     // Catch:{ NoSuchFieldError -> 0x0071 }
                org.apache.commons.math3.stat.ranking.NaNStrategy r1 = org.apache.commons.math3.stat.ranking.NaNStrategy.FIXED     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.$SwitchMap$org$apache$commons$math3$stat$ranking$NaNStrategy     // Catch:{ NoSuchFieldError -> 0x007b }
                org.apache.commons.math3.stat.ranking.NaNStrategy r1 = org.apache.commons.math3.stat.ranking.NaNStrategy.FAILED     // Catch:{ NoSuchFieldError -> 0x007b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007b }
                r0[r1] = r4     // Catch:{ NoSuchFieldError -> 0x007b }
            L_0x007b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.stat.ranking.NaturalRanking.C36131.<clinit>():void");
        }
    }

    private void fill(double[] dArr, List<Integer> list, double d) {
        for (Integer num : list) {
            dArr[num.intValue()] = d;
        }
    }

    private void restoreNaNs(double[] dArr, List<Integer> list) {
        if (list.size() != 0) {
            for (Integer num : list) {
                dArr[num.intValue()] = Double.NaN;
            }
        }
    }

    private List<Integer> getNanPositions(IntDoublePair[] intDoublePairArr) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < intDoublePairArr.length; i++) {
            if (Double.isNaN(intDoublePairArr[i].getValue())) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        return arrayList;
    }

    private static class IntDoublePair implements Comparable<IntDoublePair> {
        private final int position;
        private final double value;

        public IntDoublePair(double d, int i) {
            this.value = d;
            this.position = i;
        }

        public int compareTo(IntDoublePair intDoublePair) {
            return Double.compare(this.value, intDoublePair.value);
        }

        public double getValue() {
            return this.value;
        }

        public int getPosition() {
            return this.position;
        }
    }
}
