package org.apache.commons.math3.fraction;

import java.io.Serializable;
import java.math.BigInteger;
import org.apache.commons.math3.FieldElement;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.ArithmeticUtils;

public class Fraction extends Number implements FieldElement<Fraction>, Comparable<Fraction>, Serializable {
    private static final double DEFAULT_EPSILON = 1.0E-5d;
    public static final Fraction FOUR_FIFTHS = new Fraction(4, 5);
    public static final Fraction MINUS_ONE = new Fraction(-1, 1);
    public static final Fraction ONE = new Fraction(1, 1);
    public static final Fraction ONE_FIFTH = new Fraction(1, 5);
    public static final Fraction ONE_HALF = new Fraction(1, 2);
    public static final Fraction ONE_QUARTER = new Fraction(1, 4);
    public static final Fraction ONE_THIRD = new Fraction(1, 3);
    public static final Fraction THREE_FIFTHS = new Fraction(3, 5);
    public static final Fraction THREE_QUARTERS = new Fraction(3, 4);
    public static final Fraction TWO = new Fraction(2, 1);
    public static final Fraction TWO_FIFTHS = new Fraction(2, 5);
    public static final Fraction TWO_QUARTERS = new Fraction(2, 4);
    public static final Fraction TWO_THIRDS = new Fraction(2, 3);
    public static final Fraction ZERO = new Fraction(0, 1);
    private static final long serialVersionUID = 3698073679419233275L;
    private final int denominator;
    private final int numerator;

    public Fraction(double d) throws FractionConversionException {
        this(d, 1.0E-5d, 100);
    }

    public Fraction(double d, double d2, int i) throws FractionConversionException {
        this(d, d2, Integer.MAX_VALUE, i);
    }

    public Fraction(double d, int i) throws FractionConversionException {
        this(d, 0.0d, i, 100);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b5, code lost:
        if (r38 != 0.0d) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00be, code lost:
        if (org.apache.commons.math3.util.FastMath.abs(r17) >= ((long) r1)) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00c0, code lost:
        r11 = r17;
        r13 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e8, code lost:
        throw new org.apache.commons.math3.fraction.FractionConversionException(r36, r9, r6);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Fraction(double r36, double r38, int r40, int r41) throws org.apache.commons.math3.fraction.FractionConversionException {
        /*
            r35 = this;
            r0 = r35
            r2 = r36
            r1 = r40
            r4 = r41
            r35.<init>()
            double r5 = org.apache.commons.math3.util.FastMath.floor(r36)
            long r5 = (long) r5
            long r7 = org.apache.commons.math3.util.FastMath.abs(r5)
            r9 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 > 0) goto L_0x00e9
            double r7 = (double) r5
            java.lang.Double.isNaN(r7)
            double r7 = r7 - r2
            double r7 = org.apache.commons.math3.util.FastMath.abs(r7)
            r11 = 1
            int r12 = (r7 > r38 ? 1 : (r7 == r38 ? 0 : -1))
            if (r12 >= 0) goto L_0x002f
            int r1 = (int) r5
            r0.numerator = r1
            r0.denominator = r11
            return
        L_0x002f:
            r7 = 0
            r12 = 0
            r13 = 1
            r19 = r7
            r15 = r13
            r17 = r15
            r21 = 0
            r7 = r5
            r13 = r7
            r5 = r2
        L_0x003e:
            int r12 = r12 + r11
            r22 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r24 = r12
            double r11 = (double) r7
            java.lang.Double.isNaN(r11)
            double r11 = r5 - r11
            double r22 = r22 / r11
            double r11 = org.apache.commons.math3.util.FastMath.floor(r22)
            long r11 = (long) r11
            long r25 = r11 * r13
            long r9 = r25 + r15
            long r25 = r11 * r17
            r29 = r5
            r31 = r7
            long r6 = r25 + r19
            long r25 = org.apache.commons.math3.util.FastMath.abs(r9)
            r27 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r5 = (r25 > r27 ? 1 : (r25 == r27 ? 0 : -1))
            if (r5 > 0) goto L_0x00ad
            long r25 = org.apache.commons.math3.util.FastMath.abs(r6)
            int r5 = (r25 > r27 ? 1 : (r25 == r27 ? 0 : -1))
            if (r5 <= 0) goto L_0x0070
            goto L_0x00ad
        L_0x0070:
            r25 = r11
            double r11 = (double) r9
            r33 = r13
            double r13 = (double) r6
            java.lang.Double.isNaN(r11)
            java.lang.Double.isNaN(r13)
            double r11 = r11 / r13
            r5 = r24
            if (r5 >= r4) goto L_0x0099
            double r11 = r11 - r2
            double r11 = org.apache.commons.math3.util.FastMath.abs(r11)
            int r8 = (r11 > r38 ? 1 : (r11 == r38 ? 0 : -1))
            if (r8 <= 0) goto L_0x0099
            long r11 = (long) r1
            int r8 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r8 >= 0) goto L_0x0099
            r13 = r9
            r19 = r17
            r29 = r22
            r15 = r33
            r17 = r6
            goto L_0x009f
        L_0x0099:
            r25 = r31
            r13 = r33
            r21 = 1
        L_0x009f:
            if (r21 == 0) goto L_0x00a4
            r11 = r17
            goto L_0x00c4
        L_0x00a4:
            r12 = r5
            r7 = r25
            r9 = r27
            r5 = r29
            r11 = 1
            goto L_0x003e
        L_0x00ad:
            r33 = r13
            r5 = r24
            r11 = 0
            int r8 = (r38 > r11 ? 1 : (r38 == r11 ? 0 : -1))
            if (r8 != 0) goto L_0x00df
            long r11 = org.apache.commons.math3.util.FastMath.abs(r17)
            long r13 = (long) r1
            int r8 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r8 >= 0) goto L_0x00df
            r11 = r17
            r13 = r33
        L_0x00c4:
            if (r5 >= r4) goto L_0x00d9
            long r1 = (long) r1
            int r3 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r3 >= 0) goto L_0x00d2
            int r1 = (int) r9
            r0.numerator = r1
            int r1 = (int) r6
            r0.denominator = r1
            goto L_0x00d8
        L_0x00d2:
            int r1 = (int) r13
            r0.numerator = r1
            int r1 = (int) r11
            r0.denominator = r1
        L_0x00d8:
            return
        L_0x00d9:
            org.apache.commons.math3.fraction.FractionConversionException r1 = new org.apache.commons.math3.fraction.FractionConversionException
            r1.<init>(r2, r4)
            throw r1
        L_0x00df:
            org.apache.commons.math3.fraction.FractionConversionException r8 = new org.apache.commons.math3.fraction.FractionConversionException
            r1 = r8
            r2 = r36
            r4 = r9
            r1.<init>(r2, r4, r6)
            throw r8
        L_0x00e9:
            org.apache.commons.math3.fraction.FractionConversionException r8 = new org.apache.commons.math3.fraction.FractionConversionException
            r9 = 1
            r1 = r8
            r2 = r36
            r4 = r5
            r6 = r9
            r1.<init>(r2, r4, r6)
            goto L_0x00f7
        L_0x00f6:
            throw r8
        L_0x00f7:
            goto L_0x00f6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.fraction.Fraction.<init>(double, double, int, int):void");
    }

    public Fraction(int i) {
        this(i, 1);
    }

    public Fraction(int i, int i2) {
        if (i2 != 0) {
            if (i2 < 0) {
                if (i == Integer.MIN_VALUE || i2 == Integer.MIN_VALUE) {
                    throw new MathArithmeticException(LocalizedFormats.OVERFLOW_IN_FRACTION, Integer.valueOf(i), Integer.valueOf(i2));
                }
                i = -i;
                i2 = -i2;
            }
            int gcd = ArithmeticUtils.gcd(i, i2);
            if (gcd > 1) {
                i /= gcd;
                i2 /= gcd;
            }
            if (i2 < 0) {
                i = -i;
                i2 = -i2;
            }
            this.numerator = i;
            this.denominator = i2;
            return;
        }
        throw new MathArithmeticException(LocalizedFormats.ZERO_DENOMINATOR_IN_FRACTION, Integer.valueOf(i), Integer.valueOf(i2));
    }

    public Fraction abs() {
        return this.numerator >= 0 ? this : negate();
    }

    public int compareTo(Fraction fraction) {
        long j = ((long) this.numerator) * ((long) fraction.denominator);
        long j2 = ((long) this.denominator) * ((long) fraction.numerator);
        if (j < j2) {
            return -1;
        }
        return j > j2 ? 1 : 0;
    }

    public double doubleValue() {
        double d = (double) this.numerator;
        double d2 = (double) this.denominator;
        Double.isNaN(d);
        Double.isNaN(d2);
        return d / d2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Fraction)) {
            return false;
        }
        Fraction fraction = (Fraction) obj;
        if (this.numerator == fraction.numerator && this.denominator == fraction.denominator) {
            return true;
        }
        return false;
    }

    public float floatValue() {
        return (float) doubleValue();
    }

    public int getDenominator() {
        return this.denominator;
    }

    public int getNumerator() {
        return this.numerator;
    }

    public int hashCode() {
        return ((this.numerator + 629) * 37) + this.denominator;
    }

    public int intValue() {
        return (int) doubleValue();
    }

    public long longValue() {
        return (long) doubleValue();
    }

    public Fraction negate() {
        int i = this.numerator;
        if (i != Integer.MIN_VALUE) {
            return new Fraction(-i, this.denominator);
        }
        throw new MathArithmeticException(LocalizedFormats.OVERFLOW_IN_FRACTION, Integer.valueOf(this.numerator), Integer.valueOf(this.denominator));
    }

    public Fraction reciprocal() {
        return new Fraction(this.denominator, this.numerator);
    }

    public Fraction add(Fraction fraction) {
        return addSub(fraction, true);
    }

    public Fraction add(int i) {
        int i2 = this.numerator;
        int i3 = this.denominator;
        return new Fraction(i2 + (i * i3), i3);
    }

    public Fraction subtract(Fraction fraction) {
        return addSub(fraction, false);
    }

    public Fraction subtract(int i) {
        int i2 = this.numerator;
        int i3 = this.denominator;
        return new Fraction(i2 - (i * i3), i3);
    }

    private Fraction addSub(Fraction fraction, boolean z) {
        int i;
        if (fraction == null) {
            throw new NullArgumentException(LocalizedFormats.FRACTION, new Object[0]);
        } else if (this.numerator == 0) {
            return z ? fraction : fraction.negate();
        } else {
            if (fraction.numerator == 0) {
                return this;
            }
            int gcd = ArithmeticUtils.gcd(this.denominator, fraction.denominator);
            if (gcd == 1) {
                int mulAndCheck = ArithmeticUtils.mulAndCheck(this.numerator, fraction.denominator);
                int mulAndCheck2 = ArithmeticUtils.mulAndCheck(fraction.numerator, this.denominator);
                return new Fraction(z ? ArithmeticUtils.addAndCheck(mulAndCheck, mulAndCheck2) : ArithmeticUtils.subAndCheck(mulAndCheck, mulAndCheck2), ArithmeticUtils.mulAndCheck(this.denominator, fraction.denominator));
            }
            BigInteger multiply = BigInteger.valueOf((long) this.numerator).multiply(BigInteger.valueOf((long) (fraction.denominator / gcd)));
            BigInteger multiply2 = BigInteger.valueOf((long) fraction.numerator).multiply(BigInteger.valueOf((long) (this.denominator / gcd)));
            BigInteger add = z ? multiply.add(multiply2) : multiply.subtract(multiply2);
            int intValue = add.mod(BigInteger.valueOf((long) gcd)).intValue();
            if (intValue == 0) {
                i = gcd;
            } else {
                i = ArithmeticUtils.gcd(intValue, gcd);
            }
            BigInteger divide = add.divide(BigInteger.valueOf((long) i));
            if (divide.bitLength() <= 31) {
                return new Fraction(divide.intValue(), ArithmeticUtils.mulAndCheck(this.denominator / gcd, fraction.denominator / i));
            }
            throw new MathArithmeticException(LocalizedFormats.NUMERATOR_OVERFLOW_AFTER_MULTIPLY, divide);
        }
    }

    public Fraction multiply(Fraction fraction) {
        if (fraction != null) {
            int i = this.numerator;
            if (i == 0 || fraction.numerator == 0) {
                return ZERO;
            }
            int gcd = ArithmeticUtils.gcd(i, fraction.denominator);
            int gcd2 = ArithmeticUtils.gcd(fraction.numerator, this.denominator);
            return getReducedFraction(ArithmeticUtils.mulAndCheck(this.numerator / gcd, fraction.numerator / gcd2), ArithmeticUtils.mulAndCheck(this.denominator / gcd2, fraction.denominator / gcd));
        }
        throw new NullArgumentException(LocalizedFormats.FRACTION, new Object[0]);
    }

    public Fraction multiply(int i) {
        return new Fraction(this.numerator * i, this.denominator);
    }

    public Fraction divide(Fraction fraction) {
        if (fraction == null) {
            throw new NullArgumentException(LocalizedFormats.FRACTION, new Object[0]);
        } else if (fraction.numerator != 0) {
            return multiply(fraction.reciprocal());
        } else {
            throw new MathArithmeticException(LocalizedFormats.ZERO_FRACTION_TO_DIVIDE_BY, Integer.valueOf(fraction.numerator), Integer.valueOf(fraction.denominator));
        }
    }

    public Fraction divide(int i) {
        return new Fraction(this.numerator, this.denominator * i);
    }

    public double percentageValue() {
        return doubleValue() * 100.0d;
    }

    public static Fraction getReducedFraction(int i, int i2) {
        if (i2 == 0) {
            throw new MathArithmeticException(LocalizedFormats.ZERO_DENOMINATOR_IN_FRACTION, Integer.valueOf(i), Integer.valueOf(i2));
        } else if (i == 0) {
            return ZERO;
        } else {
            if (i2 == Integer.MIN_VALUE && (i & 1) == 0) {
                i /= 2;
                i2 /= 2;
            }
            if (i2 < 0) {
                if (i == Integer.MIN_VALUE || i2 == Integer.MIN_VALUE) {
                    throw new MathArithmeticException(LocalizedFormats.OVERFLOW_IN_FRACTION, Integer.valueOf(i), Integer.valueOf(i2));
                }
                i = -i;
                i2 = -i2;
            }
            int gcd = ArithmeticUtils.gcd(i, i2);
            return new Fraction(i / gcd, i2 / gcd);
        }
    }

    public String toString() {
        if (this.denominator == 1) {
            return Integer.toString(this.numerator);
        }
        if (this.numerator == 0) {
            return "0";
        }
        return this.numerator + " / " + this.denominator;
    }

    public FractionField getField() {
        return FractionField.getInstance();
    }
}
