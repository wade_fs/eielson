package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.util.FastMath;

public class RiddersSolver extends AbstractUnivariateSolver {
    private static final double DEFAULT_ABSOLUTE_ACCURACY = 1.0E-6d;

    public RiddersSolver() {
        this(1.0E-6d);
    }

    public RiddersSolver(double d) {
        super(d);
    }

    public RiddersSolver(double d, double d2) {
        super(d, d2);
    }

    /* access modifiers changed from: protected */
    public double doSolve() throws TooManyEvaluationsException, NoBracketingException {
        double d;
        double d2;
        double min = getMin();
        double max = getMax();
        double computeObjectiveValue = computeObjectiveValue(min);
        double computeObjectiveValue2 = computeObjectiveValue(max);
        if (computeObjectiveValue == 0.0d) {
            return min;
        }
        if (computeObjectiveValue2 == 0.0d) {
            return max;
        }
        verifyBracketing(min, max);
        double absoluteAccuracy = getAbsoluteAccuracy();
        double functionValueAccuracy = getFunctionValueAccuracy();
        double relativeAccuracy = getRelativeAccuracy();
        double d3 = Double.POSITIVE_INFINITY;
        while (true) {
            double d4 = (min + max) * 0.5d;
            double computeObjectiveValue3 = computeObjectiveValue(d4);
            if (FastMath.abs(computeObjectiveValue3) <= functionValueAccuracy) {
                return d4;
            }
            double signum = ((FastMath.signum(computeObjectiveValue2) * FastMath.signum(computeObjectiveValue3)) * (d4 - min)) / FastMath.sqrt(1.0d - ((computeObjectiveValue * computeObjectiveValue2) / (computeObjectiveValue3 * computeObjectiveValue3)));
            double d5 = min;
            double d6 = d4 - signum;
            double computeObjectiveValue4 = computeObjectiveValue(d6);
            double d7 = max;
            if (FastMath.abs(d6 - d3) <= FastMath.max(relativeAccuracy * FastMath.abs(d6), absoluteAccuracy) || FastMath.abs(computeObjectiveValue4) <= functionValueAccuracy) {
                return d6;
            }
            if (signum > 0.0d) {
                if (FastMath.signum(computeObjectiveValue) + FastMath.signum(computeObjectiveValue4) == 0.0d) {
                    d = d6;
                    computeObjectiveValue2 = computeObjectiveValue4;
                    d3 = d6;
                    min = d2;
                    max = d;
                } else {
                    d2 = d6;
                    d7 = d4;
                    computeObjectiveValue2 = computeObjectiveValue3;
                }
            } else if (FastMath.signum(computeObjectiveValue2) + FastMath.signum(computeObjectiveValue4) == 0.0d) {
                d2 = d6;
            } else {
                d = d6;
                d5 = d4;
                computeObjectiveValue = computeObjectiveValue3;
                computeObjectiveValue2 = computeObjectiveValue4;
                d3 = d6;
                min = d2;
                max = d;
            }
            computeObjectiveValue = computeObjectiveValue4;
            d3 = d6;
            min = d2;
            max = d;
        }
    }
}
