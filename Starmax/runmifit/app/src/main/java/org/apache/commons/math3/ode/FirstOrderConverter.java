package org.apache.commons.math3.ode;

public class FirstOrderConverter implements FirstOrderDifferentialEquations {
    private final int dimension;
    private final SecondOrderDifferentialEquations equations;

    /* renamed from: z */
    private final double[] f8063z;
    private final double[] zDDot;
    private final double[] zDot;

    public FirstOrderConverter(SecondOrderDifferentialEquations secondOrderDifferentialEquations) {
        this.equations = secondOrderDifferentialEquations;
        this.dimension = secondOrderDifferentialEquations.getDimension();
        int i = this.dimension;
        this.f8063z = new double[i];
        this.zDot = new double[i];
        this.zDDot = new double[i];
    }

    public int getDimension() {
        return this.dimension * 2;
    }

    public void computeDerivatives(double d, double[] dArr, double[] dArr2) {
        System.arraycopy(dArr, 0, this.f8063z, 0, this.dimension);
        int i = this.dimension;
        System.arraycopy(dArr, i, this.zDot, 0, i);
        this.equations.computeSecondDerivatives(d, this.f8063z, this.zDot, this.zDDot);
        System.arraycopy(this.zDot, 0, dArr2, 0, this.dimension);
        double[] dArr3 = this.zDDot;
        int i2 = this.dimension;
        System.arraycopy(dArr3, 0, dArr2, i2, i2);
    }
}
