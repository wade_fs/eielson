package org.apache.commons.math3.analysis.differentiation;

import java.io.Serializable;
import java.lang.reflect.Array;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.UnivariateMatrixFunction;
import org.apache.commons.math3.analysis.UnivariateVectorFunction;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.util.FastMath;

public class FiniteDifferencesDifferentiator implements UnivariateFunctionDifferentiator, UnivariateVectorFunctionDifferentiator, UnivariateMatrixFunctionDifferentiator, Serializable {
    private static final long serialVersionUID = 20120917;
    /* access modifiers changed from: private */
    public final double halfSampleSpan;
    /* access modifiers changed from: private */
    public final int nbPoints;
    /* access modifiers changed from: private */
    public final double stepSize;
    /* access modifiers changed from: private */
    public final double tMax;
    /* access modifiers changed from: private */
    public final double tMin;

    public FiniteDifferencesDifferentiator(int i, double d) throws NotPositiveException, NumberIsTooSmallException {
        this(i, d, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    public FiniteDifferencesDifferentiator(int i, double d, double d2, double d3) throws NotPositiveException, NumberIsTooSmallException, NumberIsTooLargeException {
        if (i > 1) {
            this.nbPoints = i;
            if (d > 0.0d) {
                this.stepSize = d;
                double d4 = (double) (i - 1);
                Double.isNaN(d4);
                this.halfSampleSpan = d * 0.5d * d4;
                double d5 = this.halfSampleSpan;
                double d6 = d3 - d2;
                if (d5 * 2.0d < d6) {
                    double ulp = FastMath.ulp(d5);
                    double d7 = this.halfSampleSpan;
                    this.tMin = d2 + d7 + ulp;
                    this.tMax = (d3 - d7) - ulp;
                    return;
                }
                throw new NumberIsTooLargeException(Double.valueOf(d5 * 2.0d), Double.valueOf(d6), false);
            }
            throw new NotPositiveException(Double.valueOf(d));
        }
        throw new NumberIsTooSmallException(Double.valueOf(d), 1, false);
    }

    public int getNbPoints() {
        return this.nbPoints;
    }

    public double getStepSize() {
        return this.stepSize;
    }

    /* access modifiers changed from: private */
    public DerivativeStructure evaluate(DerivativeStructure derivativeStructure, double d, double[] dArr) throws NumberIsTooLargeException {
        int i = this.nbPoints;
        double[] dArr2 = new double[i];
        double[] dArr3 = new double[i];
        for (int i2 = 0; i2 < this.nbPoints; i2++) {
            dArr3[i2] = dArr[i2];
            for (int i3 = 1; i3 <= i2; i3++) {
                int i4 = i2 - i3;
                double d2 = dArr3[i4 + 1] - dArr3[i4];
                double d3 = (double) i3;
                double d4 = this.stepSize;
                Double.isNaN(d3);
                dArr3[i4] = d2 / (d3 * d4);
            }
            dArr2[i2] = dArr3[0];
        }
        int order = derivativeStructure.getOrder();
        int freeParameters = derivativeStructure.getFreeParameters();
        double[] allDerivatives = derivativeStructure.getAllDerivatives();
        double value = derivativeStructure.getValue() - d;
        DerivativeStructure derivativeStructure2 = null;
        DerivativeStructure derivativeStructure3 = new DerivativeStructure(freeParameters, order, 0.0d);
        for (int i5 = 0; i5 < this.nbPoints; i5++) {
            if (i5 == 0) {
                derivativeStructure2 = new DerivativeStructure(freeParameters, order, 1.0d);
            } else {
                double d5 = (double) (i5 - 1);
                double d6 = this.stepSize;
                Double.isNaN(d5);
                allDerivatives[0] = value - (d5 * d6);
                derivativeStructure2 = derivativeStructure2.multiply(new DerivativeStructure(freeParameters, order, allDerivatives));
            }
            derivativeStructure3 = derivativeStructure3.add(derivativeStructure2.multiply(dArr2[i5]));
        }
        return derivativeStructure3;
    }

    public UnivariateDifferentiableFunction differentiate(final UnivariateFunction univariateFunction) {
        return new UnivariateDifferentiableFunction() {
            /* class org.apache.commons.math3.analysis.differentiation.FiniteDifferencesDifferentiator.C34811 */

            public double value(double d) throws MathIllegalArgumentException {
                return univariateFunction.value(d);
            }

            public DerivativeStructure value(DerivativeStructure derivativeStructure) throws MathIllegalArgumentException {
                if (derivativeStructure.getOrder() < FiniteDifferencesDifferentiator.this.nbPoints) {
                    double max = FastMath.max(FastMath.min(derivativeStructure.getValue(), FiniteDifferencesDifferentiator.this.tMax), FiniteDifferencesDifferentiator.this.tMin) - FiniteDifferencesDifferentiator.this.halfSampleSpan;
                    double[] dArr = new double[FiniteDifferencesDifferentiator.this.nbPoints];
                    for (int i = 0; i < FiniteDifferencesDifferentiator.this.nbPoints; i++) {
                        UnivariateFunction univariateFunction = univariateFunction;
                        double d = (double) i;
                        double access$400 = FiniteDifferencesDifferentiator.this.stepSize;
                        Double.isNaN(d);
                        dArr[i] = univariateFunction.value((d * access$400) + max);
                    }
                    return FiniteDifferencesDifferentiator.this.evaluate(derivativeStructure, max, dArr);
                }
                throw new NumberIsTooLargeException(Integer.valueOf(derivativeStructure.getOrder()), Integer.valueOf(FiniteDifferencesDifferentiator.this.nbPoints), false);
            }
        };
    }

    public UnivariateDifferentiableVectorFunction differentiate(final UnivariateVectorFunction univariateVectorFunction) {
        return new UnivariateDifferentiableVectorFunction() {
            /* class org.apache.commons.math3.analysis.differentiation.FiniteDifferencesDifferentiator.C34822 */

            public double[] value(double d) throws MathIllegalArgumentException {
                return univariateVectorFunction.value(d);
            }

            public DerivativeStructure[] value(DerivativeStructure derivativeStructure) throws MathIllegalArgumentException {
                if (derivativeStructure.getOrder() < FiniteDifferencesDifferentiator.this.nbPoints) {
                    double max = FastMath.max(FastMath.min(derivativeStructure.getValue(), FiniteDifferencesDifferentiator.this.tMax), FiniteDifferencesDifferentiator.this.tMin) - FiniteDifferencesDifferentiator.this.halfSampleSpan;
                    double[][] dArr = null;
                    for (int i = 0; i < FiniteDifferencesDifferentiator.this.nbPoints; i++) {
                        UnivariateVectorFunction univariateVectorFunction = univariateVectorFunction;
                        double d = (double) i;
                        double access$400 = FiniteDifferencesDifferentiator.this.stepSize;
                        Double.isNaN(d);
                        double[] value = univariateVectorFunction.value((d * access$400) + max);
                        if (i == 0) {
                            dArr = (double[][]) Array.newInstance(double.class, value.length, FiniteDifferencesDifferentiator.this.nbPoints);
                        }
                        for (int i2 = 0; i2 < value.length; i2++) {
                            dArr[i2][i] = value[i2];
                        }
                    }
                    DerivativeStructure[] derivativeStructureArr = new DerivativeStructure[dArr.length];
                    for (int i3 = 0; i3 < derivativeStructureArr.length; i3++) {
                        derivativeStructureArr[i3] = FiniteDifferencesDifferentiator.this.evaluate(derivativeStructure, max, dArr[i3]);
                    }
                    return derivativeStructureArr;
                }
                throw new NumberIsTooLargeException(Integer.valueOf(derivativeStructure.getOrder()), Integer.valueOf(FiniteDifferencesDifferentiator.this.nbPoints), false);
            }
        };
    }

    public UnivariateDifferentiableMatrixFunction differentiate(final UnivariateMatrixFunction univariateMatrixFunction) {
        return new UnivariateDifferentiableMatrixFunction() {
            /* class org.apache.commons.math3.analysis.differentiation.FiniteDifferencesDifferentiator.C34833 */

            public double[][] value(double d) throws MathIllegalArgumentException {
                return univariateMatrixFunction.value(d);
            }

            public DerivativeStructure[][] value(DerivativeStructure derivativeStructure) throws MathIllegalArgumentException {
                if (derivativeStructure.getOrder() < FiniteDifferencesDifferentiator.this.nbPoints) {
                    double max = FastMath.max(FastMath.min(derivativeStructure.getValue(), FiniteDifferencesDifferentiator.this.tMax), FiniteDifferencesDifferentiator.this.tMin) - FiniteDifferencesDifferentiator.this.halfSampleSpan;
                    double[][][] dArr = null;
                    for (int i = 0; i < FiniteDifferencesDifferentiator.this.nbPoints; i++) {
                        UnivariateMatrixFunction univariateMatrixFunction = univariateMatrixFunction;
                        double d = (double) i;
                        double access$400 = FiniteDifferencesDifferentiator.this.stepSize;
                        Double.isNaN(d);
                        double[][] value = univariateMatrixFunction.value((d * access$400) + max);
                        if (i == 0) {
                            dArr = (double[][][]) Array.newInstance(double.class, value.length, value[0].length, FiniteDifferencesDifferentiator.this.nbPoints);
                        }
                        for (int i2 = 0; i2 < value.length; i2++) {
                            for (int i3 = 0; i3 < value[i2].length; i3++) {
                                dArr[i2][i3][i] = value[i2][i3];
                            }
                        }
                    }
                    DerivativeStructure[][] derivativeStructureArr = (DerivativeStructure[][]) Array.newInstance(DerivativeStructure.class, dArr.length, dArr[0].length);
                    for (int i4 = 0; i4 < derivativeStructureArr.length; i4++) {
                        for (int i5 = 0; i5 < dArr[i4].length; i5++) {
                            derivativeStructureArr[i4][i5] = FiniteDifferencesDifferentiator.this.evaluate(derivativeStructure, max, dArr[i4][i5]);
                        }
                    }
                    return derivativeStructureArr;
                }
                throw new NumberIsTooLargeException(Integer.valueOf(derivativeStructure.getOrder()), Integer.valueOf(FiniteDifferencesDifferentiator.this.nbPoints), false);
            }
        };
    }
}
