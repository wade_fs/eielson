package org.apache.commons.math3.optim.nonlinear.scalar.noderiv;

import org.apache.commons.math3.exception.MathUnsupportedOperationException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.LineSearch;
import org.apache.commons.math3.optim.nonlinear.scalar.MultivariateOptimizer;
import org.apache.commons.math3.util.FastMath;

public class PowellOptimizer extends MultivariateOptimizer {
    private static final double MIN_RELATIVE_TOLERANCE = (FastMath.ulp(1.0d) * 2.0d);
    private final double absoluteThreshold;
    private final LineSearch line;
    private final double relativeThreshold;

    public PowellOptimizer(double d, double d2, ConvergenceChecker<PointValuePair> convergenceChecker) {
        this(d, d2, FastMath.sqrt(d), FastMath.sqrt(d2), convergenceChecker);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PowellOptimizer(double d, double d2, double d3, double d4, ConvergenceChecker<PointValuePair> convergenceChecker) {
        super(convergenceChecker);
        double d5 = d;
        double d6 = d2;
        if (d5 < MIN_RELATIVE_TOLERANCE) {
            throw new NumberIsTooSmallException(Double.valueOf(d), Double.valueOf(MIN_RELATIVE_TOLERANCE), true);
        } else if (d6 > 0.0d) {
            this.relativeThreshold = d5;
            this.absoluteThreshold = d6;
            this.line = new LineSearch(super, d3, d4, 1.0d);
        } else {
            throw new NotStrictlyPositiveException(Double.valueOf(d2));
        }
    }

    public PowellOptimizer(double d, double d2) {
        this(d, d2, null);
    }

    public PowellOptimizer(double d, double d2, double d3, double d4) {
        this(d, d2, d3, d4, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v3, resolved type: double[]} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.math3.optim.PointValuePair doOptimize() {
        /*
            r30 = this;
            r0 = r30
            r30.checkParameters()
            org.apache.commons.math3.optim.nonlinear.scalar.GoalType r1 = r30.getGoalType()
            double[] r2 = r30.getStartPoint()
            int r3 = r2.length
            int[] r4 = new int[]{r3, r3}
            java.lang.Class<double> r5 = double.class
            java.lang.Object r4 = java.lang.reflect.Array.newInstance(r5, r4)
            double[][] r4 = (double[][]) r4
            r5 = 0
            r6 = 0
        L_0x001c:
            if (r6 >= r3) goto L_0x0027
            r7 = r4[r6]
            r8 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r7[r6] = r8
            int r6 = r6 + 1
            goto L_0x001c
        L_0x0027:
            org.apache.commons.math3.optim.ConvergenceChecker r6 = r30.getConvergenceChecker()
            double r7 = r0.computeObjectiveValue(r2)
            java.lang.Object r9 = r2.clone()
            double[] r9 = (double[]) r9
        L_0x0035:
            r30.incrementIterationCount()
            r14 = r2
            r12 = r7
            r2 = 0
            r15 = 0
            r17 = 0
        L_0x003f:
            if (r2 >= r3) goto L_0x0071
            r18 = r4[r2]
            double[] r10 = org.apache.commons.math3.util.MathArrays.copyOf(r18)
            org.apache.commons.math3.optim.nonlinear.scalar.LineSearch r11 = r0.line
            org.apache.commons.math3.optim.univariate.UnivariatePointValuePair r11 = r11.search(r14, r10)
            double r19 = r11.getValue()
            r18 = r3
            r21 = r4
            double r3 = r11.getPoint()
            double[][] r3 = r0.newPointAndDirection(r14, r10, r3)
            r14 = r3[r5]
            double r12 = r12 - r19
            int r3 = (r12 > r15 ? 1 : (r12 == r15 ? 0 : -1))
            if (r3 <= 0) goto L_0x0068
            r17 = r2
            r15 = r12
        L_0x0068:
            int r2 = r2 + 1
            r3 = r18
            r12 = r19
            r4 = r21
            goto L_0x003f
        L_0x0071:
            r18 = r3
            r21 = r4
            double r2 = r7 - r12
            r10 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r19 = r2 * r10
            double r10 = r0.relativeThreshold
            double r24 = org.apache.commons.math3.util.FastMath.abs(r7)
            double r26 = org.apache.commons.math3.util.FastMath.abs(r12)
            double r24 = r24 + r26
            double r10 = r10 * r24
            r24 = r6
            double r5 = r0.absoluteThreshold
            double r10 = r10 + r5
            int r6 = (r19 > r10 ? 1 : (r19 == r10 ? 0 : -1))
            if (r6 > 0) goto L_0x0094
            r6 = 1
            goto L_0x0095
        L_0x0094:
            r6 = 0
        L_0x0095:
            org.apache.commons.math3.optim.PointValuePair r10 = new org.apache.commons.math3.optim.PointValuePair
            r10.<init>(r9, r7)
            org.apache.commons.math3.optim.PointValuePair r11 = new org.apache.commons.math3.optim.PointValuePair
            r11.<init>(r14, r12)
            if (r6 != 0) goto L_0x00ae
            if (r24 == 0) goto L_0x00ae
            int r6 = r30.getIterations()
            r4 = r24
            boolean r6 = r4.converged(r6, r10, r11)
            goto L_0x00b0
        L_0x00ae:
            r4 = r24
        L_0x00b0:
            if (r6 == 0) goto L_0x00c2
            org.apache.commons.math3.optim.nonlinear.scalar.GoalType r2 = org.apache.commons.math3.optim.nonlinear.scalar.GoalType.MINIMIZE
            if (r1 != r2) goto L_0x00bc
            int r1 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r1 >= 0) goto L_0x00bb
            r10 = r11
        L_0x00bb:
            return r10
        L_0x00bc:
            int r1 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r1 <= 0) goto L_0x00c1
            r10 = r11
        L_0x00c1:
            return r10
        L_0x00c2:
            r6 = r18
            double[] r10 = new double[r6]
            double[] r11 = new double[r6]
            r5 = 0
        L_0x00c9:
            if (r5 >= r6) goto L_0x00e2
            r24 = r14[r5]
            r26 = r9[r5]
            double r24 = r24 - r26
            r10[r5] = r24
            r24 = r14[r5]
            r22 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r24 = r24 * r22
            r26 = r9[r5]
            double r24 = r24 - r26
            r11[r5] = r24
            int r5 = r5 + 1
            goto L_0x00c9
        L_0x00e2:
            java.lang.Object r5 = r14.clone()
            r9 = r5
            double[] r9 = (double[]) r9
            double r24 = r0.computeObjectiveValue(r11)
            int r5 = (r7 > r24 ? 1 : (r7 == r24 ? 0 : -1))
            if (r5 <= 0) goto L_0x0130
            double r26 = r7 + r24
            r22 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r28 = r12 * r22
            double r26 = r26 - r28
            double r26 = r26 * r22
            double r2 = r2 - r15
            double r2 = r2 * r2
            double r26 = r26 * r2
            double r7 = r7 - r24
            double r15 = r15 * r7
            double r15 = r15 * r7
            double r26 = r26 - r15
            r2 = 0
            int r5 = (r26 > r2 ? 1 : (r26 == r2 ? 0 : -1))
            if (r5 >= 0) goto L_0x0130
            org.apache.commons.math3.optim.nonlinear.scalar.LineSearch r2 = r0.line
            org.apache.commons.math3.optim.univariate.UnivariatePointValuePair r2 = r2.search(r14, r10)
            double r7 = r2.getValue()
            double r2 = r2.getPoint()
            double[][] r2 = r0.newPointAndDirection(r14, r10, r2)
            r3 = 0
            r5 = r2[r3]
            int r10 = r6 + -1
            r11 = r21[r10]
            r21[r17] = r11
            r11 = 1
            r2 = r2[r11]
            r21[r10] = r2
            r2 = r5
            goto L_0x0133
        L_0x0130:
            r3 = 0
            r7 = r12
            r2 = r14
        L_0x0133:
            r3 = r6
            r5 = 0
            r6 = r4
            r4 = r21
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optim.nonlinear.scalar.noderiv.PowellOptimizer.doOptimize():org.apache.commons.math3.optim.PointValuePair");
    }

    private double[][] newPointAndDirection(double[] dArr, double[] dArr2, double d) {
        int length = dArr.length;
        double[] dArr3 = new double[length];
        double[] dArr4 = new double[length];
        for (int i = 0; i < length; i++) {
            dArr4[i] = dArr2[i] * d;
            dArr3[i] = dArr[i] + dArr4[i];
        }
        return new double[][]{dArr3, dArr4};
    }

    private void checkParameters() {
        if (getLowerBound() != null || getUpperBound() != null) {
            throw new MathUnsupportedOperationException(LocalizedFormats.CONSTRAINT, new Object[0]);
        }
    }
}
