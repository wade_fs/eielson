package org.apache.commons.math3.ode.nonstiff;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.ode.AbstractIntegrator;
import org.apache.commons.math3.ode.ExpandableStatefulODE;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.util.FastMath;

public abstract class RungeKuttaIntegrator extends AbstractIntegrator {

    /* renamed from: a */
    private final double[][] f8095a;

    /* renamed from: b */
    private final double[] f8096b;

    /* renamed from: c */
    private final double[] f8097c;
    private final RungeKuttaStepInterpolator prototype;
    private final double step;

    protected RungeKuttaIntegrator(String str, double[] dArr, double[][] dArr2, double[] dArr3, RungeKuttaStepInterpolator rungeKuttaStepInterpolator, double d) {
        super(str);
        this.f8097c = dArr;
        this.f8095a = dArr2;
        this.f8096b = dArr3;
        this.prototype = rungeKuttaStepInterpolator;
        this.step = FastMath.abs(d);
    }

    public void integrate(ExpandableStatefulODE expandableStatefulODE, double d) throws NumberIsTooSmallException, DimensionMismatchException, MaxCountExceededException, NoBracketingException {
        sanityChecks(expandableStatefulODE, d);
        setEquations(expandableStatefulODE);
        char c = 0;
        boolean z = d > expandableStatefulODE.getTime();
        double[] completeState = expandableStatefulODE.getCompleteState();
        double[] dArr = (double[]) completeState.clone();
        int length = this.f8097c.length + 1;
        double[][] dArr2 = new double[length][];
        for (int i = 0; i < length; i++) {
            dArr2[i] = new double[completeState.length];
        }
        double[] dArr3 = (double[]) completeState.clone();
        double[] dArr4 = new double[completeState.length];
        RungeKuttaStepInterpolator rungeKuttaStepInterpolator = (RungeKuttaStepInterpolator) this.prototype.copy();
        RungeKuttaStepInterpolator rungeKuttaStepInterpolator2 = rungeKuttaStepInterpolator;
        double[] dArr5 = dArr4;
        double[] dArr6 = dArr3;
        rungeKuttaStepInterpolator.reinitialize(super, dArr3, dArr2, z, expandableStatefulODE.getPrimaryMapper(), expandableStatefulODE.getSecondaryMappers());
        rungeKuttaStepInterpolator2.storeTime(expandableStatefulODE.getTime());
        this.stepStart = expandableStatefulODE.getTime();
        double d2 = this.step;
        if (!z) {
            d2 = -d2;
        }
        this.stepSize = d2;
        initIntegration(expandableStatefulODE.getTime(), completeState, d);
        this.isLastStep = false;
        while (true) {
            rungeKuttaStepInterpolator2.shift();
            computeDerivatives(this.stepStart, dArr, dArr2[c]);
            int i2 = 1;
            while (i2 < length) {
                int i3 = 0;
                while (i3 < completeState.length) {
                    int i4 = i2 - 1;
                    double d3 = this.f8095a[i4][c] * dArr2[c][i3];
                    for (int i5 = 1; i5 < i2; i5++) {
                        d3 += this.f8095a[i4][i5] * dArr2[i5][i3];
                    }
                    dArr6[i3] = dArr[i3] + (this.stepSize * d3);
                    i3++;
                    z = z;
                    c = 0;
                }
                computeDerivatives(this.stepStart + (this.f8097c[i2 - 1] * this.stepSize), dArr6, dArr2[i2]);
                i2++;
                rungeKuttaStepInterpolator2 = rungeKuttaStepInterpolator2;
                z = z;
                c = 0;
            }
            boolean z2 = z;
            double[] dArr7 = dArr6;
            RungeKuttaStepInterpolator rungeKuttaStepInterpolator3 = rungeKuttaStepInterpolator2;
            for (int i6 = 0; i6 < completeState.length; i6++) {
                double d4 = this.f8096b[0] * dArr2[0][i6];
                for (int i7 = 1; i7 < length; i7++) {
                    d4 += this.f8096b[i7] * dArr2[i7][i6];
                }
                dArr7[i6] = dArr[i6] + (this.stepSize * d4);
            }
            rungeKuttaStepInterpolator3.storeTime(this.stepStart + this.stepSize);
            System.arraycopy(dArr7, 0, dArr, 0, completeState.length);
            double[] dArr8 = dArr5;
            System.arraycopy(dArr2[length - 1], 0, dArr8, 0, completeState.length);
            this.stepStart = acceptStep(rungeKuttaStepInterpolator3, dArr, dArr8, d);
            if (!this.isLastStep) {
                rungeKuttaStepInterpolator3.storeTime(this.stepStart);
                double d5 = this.stepStart + this.stepSize;
                if (!z2 ? d5 <= d : d5 >= d) {
                    this.stepSize = d - this.stepStart;
                }
            }
            if (this.isLastStep) {
                ExpandableStatefulODE expandableStatefulODE2 = expandableStatefulODE;
                expandableStatefulODE2.setTime(this.stepStart);
                expandableStatefulODE2.setCompleteState(dArr);
                this.stepStart = Double.NaN;
                this.stepSize = Double.NaN;
                return;
            }
            dArr5 = dArr8;
            dArr6 = dArr7;
            rungeKuttaStepInterpolator2 = rungeKuttaStepInterpolator3;
            z = z2;
            c = 0;
        }
    }

    public double[] singleStep(FirstOrderDifferentialEquations firstOrderDifferentialEquations, double d, double[] dArr, double d2) {
        FirstOrderDifferentialEquations firstOrderDifferentialEquations2 = firstOrderDifferentialEquations;
        double d3 = d;
        double[] dArr2 = dArr;
        double[] dArr3 = (double[]) dArr.clone();
        int length = this.f8097c.length + 1;
        double[][] dArr4 = new double[length][];
        for (int i = 0; i < length; i++) {
            dArr4[i] = new double[dArr2.length];
        }
        double[] dArr5 = (double[]) dArr.clone();
        double d4 = d2 - d3;
        firstOrderDifferentialEquations2.computeDerivatives(d3, dArr3, dArr4[0]);
        for (int i2 = 1; i2 < length; i2++) {
            for (int i3 = 0; i3 < dArr2.length; i3++) {
                int i4 = i2 - 1;
                double d5 = this.f8095a[i4][0] * dArr4[0][i3];
                for (int i5 = 1; i5 < i2; i5++) {
                    d5 += this.f8095a[i4][i5] * dArr4[i5][i3];
                }
                dArr5[i3] = dArr3[i3] + (d5 * d4);
            }
            firstOrderDifferentialEquations2.computeDerivatives((this.f8097c[i2 - 1] * d4) + d3, dArr5, dArr4[i2]);
        }
        for (int i6 = 0; i6 < dArr2.length; i6++) {
            double d6 = this.f8096b[0] * dArr4[0][i6];
            for (int i7 = 1; i7 < length; i7++) {
                d6 += this.f8096b[i7] * dArr4[i7][i6];
            }
            dArr3[i6] = dArr3[i6] + (d6 * d4);
        }
        return dArr3;
    }
}
