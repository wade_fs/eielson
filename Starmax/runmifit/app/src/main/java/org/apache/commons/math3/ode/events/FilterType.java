package org.apache.commons.math3.ode.events;

import org.apache.commons.math3.exception.MathInternalError;

public enum FilterType {
    TRIGGER_ONLY_DECREASING_EVENTS {
        /* access modifiers changed from: protected */
        public boolean getTriggeredIncreasing() {
            return false;
        }

        /* access modifiers changed from: protected */
        public Transformer selectTransformer(Transformer transformer, double d, boolean z) {
            if (z) {
                int i = C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer[transformer.ordinal()];
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                if (i == 5) {
                                    return d <= 0.0d ? Transformer.PLUS : transformer;
                                }
                                throw new MathInternalError();
                            } else if (d <= 0.0d) {
                                return Transformer.MINUS;
                            } else {
                                return transformer;
                            }
                        } else if (d >= 0.0d) {
                            return Transformer.MAX;
                        } else {
                            return transformer;
                        }
                    } else if (d >= 0.0d) {
                        return Transformer.MIN;
                    } else {
                        return transformer;
                    }
                } else if (d > 0.0d) {
                    return Transformer.MAX;
                } else {
                    if (d < 0.0d) {
                        return Transformer.PLUS;
                    }
                    return Transformer.UNINITIALIZED;
                }
            } else {
                int i2 = C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer[transformer.ordinal()];
                if (i2 != 1) {
                    if (i2 != 2) {
                        if (i2 != 3) {
                            if (i2 != 4) {
                                if (i2 == 5) {
                                    return d >= 0.0d ? Transformer.MINUS : transformer;
                                }
                                throw new MathInternalError();
                            } else if (d >= 0.0d) {
                                return Transformer.PLUS;
                            } else {
                                return transformer;
                            }
                        } else if (d <= 0.0d) {
                            return Transformer.MIN;
                        } else {
                            return transformer;
                        }
                    } else if (d <= 0.0d) {
                        return Transformer.MAX;
                    } else {
                        return transformer;
                    }
                } else if (d > 0.0d) {
                    return Transformer.MINUS;
                } else {
                    if (d < 0.0d) {
                        return Transformer.MIN;
                    }
                    return Transformer.UNINITIALIZED;
                }
            }
        }
    },
    TRIGGER_ONLY_INCREASING_EVENTS {
        /* access modifiers changed from: protected */
        public boolean getTriggeredIncreasing() {
            return true;
        }

        /* access modifiers changed from: protected */
        public Transformer selectTransformer(Transformer transformer, double d, boolean z) {
            if (z) {
                int i = C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer[transformer.ordinal()];
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                if (i == 5) {
                                    return d >= 0.0d ? Transformer.MINUS : transformer;
                                }
                                throw new MathInternalError();
                            } else if (d >= 0.0d) {
                                return Transformer.PLUS;
                            } else {
                                return transformer;
                            }
                        } else if (d <= 0.0d) {
                            return Transformer.MIN;
                        } else {
                            return transformer;
                        }
                    } else if (d <= 0.0d) {
                        return Transformer.MAX;
                    } else {
                        return transformer;
                    }
                } else if (d > 0.0d) {
                    return Transformer.PLUS;
                } else {
                    if (d < 0.0d) {
                        return Transformer.MIN;
                    }
                    return Transformer.UNINITIALIZED;
                }
            } else {
                int i2 = C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer[transformer.ordinal()];
                if (i2 != 1) {
                    if (i2 != 2) {
                        if (i2 != 3) {
                            if (i2 != 4) {
                                if (i2 == 5) {
                                    return d <= 0.0d ? Transformer.PLUS : transformer;
                                }
                                throw new MathInternalError();
                            } else if (d <= 0.0d) {
                                return Transformer.MINUS;
                            } else {
                                return transformer;
                            }
                        } else if (d >= 0.0d) {
                            return Transformer.MAX;
                        } else {
                            return transformer;
                        }
                    } else if (d >= 0.0d) {
                        return Transformer.MIN;
                    } else {
                        return transformer;
                    }
                } else if (d > 0.0d) {
                    return Transformer.MAX;
                } else {
                    if (d < 0.0d) {
                        return Transformer.MINUS;
                    }
                    return Transformer.UNINITIALIZED;
                }
            }
        }
    };

    /* access modifiers changed from: protected */
    public abstract boolean getTriggeredIncreasing();

    /* access modifiers changed from: protected */
    public abstract Transformer selectTransformer(Transformer transformer, double d, boolean z);

    /* renamed from: org.apache.commons.math3.ode.events.FilterType$3 */
    static /* synthetic */ class C35763 {
        static final /* synthetic */ int[] $SwitchMap$org$apache$commons$math3$ode$events$Transformer = new int[Transformer.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                org.apache.commons.math3.ode.events.Transformer[] r0 = org.apache.commons.math3.ode.events.Transformer.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.ode.events.FilterType.C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer = r0
                int[] r0 = org.apache.commons.math3.ode.events.FilterType.C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.ode.events.Transformer r1 = org.apache.commons.math3.ode.events.Transformer.UNINITIALIZED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.apache.commons.math3.ode.events.FilterType.C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.ode.events.Transformer r1 = org.apache.commons.math3.ode.events.Transformer.PLUS     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = org.apache.commons.math3.ode.events.FilterType.C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer     // Catch:{ NoSuchFieldError -> 0x002a }
                org.apache.commons.math3.ode.events.Transformer r1 = org.apache.commons.math3.ode.events.Transformer.MINUS     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = org.apache.commons.math3.ode.events.FilterType.C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.apache.commons.math3.ode.events.Transformer r1 = org.apache.commons.math3.ode.events.Transformer.MIN     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = org.apache.commons.math3.ode.events.FilterType.C35763.$SwitchMap$org$apache$commons$math3$ode$events$Transformer     // Catch:{ NoSuchFieldError -> 0x0040 }
                org.apache.commons.math3.ode.events.Transformer r1 = org.apache.commons.math3.ode.events.Transformer.MAX     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.ode.events.FilterType.C35763.<clinit>():void");
        }
    }
}
