package org.apache.commons.math3.dfp;

public class DfpDec extends Dfp {
    protected DfpDec(DfpField dfpField) {
        super(dfpField);
    }

    protected DfpDec(DfpField dfpField, byte b) {
        super(dfpField, b);
    }

    protected DfpDec(DfpField dfpField, int i) {
        super(dfpField, i);
    }

    protected DfpDec(DfpField dfpField, long j) {
        super(dfpField, j);
    }

    protected DfpDec(DfpField dfpField, double d) {
        super(dfpField, d);
        round(0);
    }

    public DfpDec(Dfp dfp) {
        super(super);
        round(0);
    }

    protected DfpDec(DfpField dfpField, String str) {
        super(dfpField, str);
        round(0);
    }

    protected DfpDec(DfpField dfpField, byte b, byte b2) {
        super(dfpField, b, b2);
    }

    public Dfp newInstance() {
        return new DfpDec(getField());
    }

    public Dfp newInstance(byte b) {
        return new DfpDec(getField(), b);
    }

    public Dfp newInstance(int i) {
        return new DfpDec(getField(), i);
    }

    public Dfp newInstance(long j) {
        return new DfpDec(getField(), j);
    }

    public Dfp newInstance(double d) {
        return new DfpDec(getField(), d);
    }

    public Dfp newInstance(Dfp dfp) {
        if (getField().getRadixDigits() == super.getField().getRadixDigits()) {
            return new DfpDec(super);
        }
        getField().setIEEEFlagsBits(1);
        Dfp newInstance = newInstance(getZero());
        super.nans = 3;
        return dotrap(1, "newInstance", super, super);
    }

    public Dfp newInstance(String str) {
        return new DfpDec(getField(), str);
    }

    public Dfp newInstance(byte b, byte b2) {
        return new DfpDec(getField(), b, b2);
    }

    /* access modifiers changed from: protected */
    public int getDecimalDigits() {
        return (getRadixDigits() * 4) - 3;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0098, code lost:
        if (r5 != 0) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a1, code lost:
        if (r5 != 0) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a9, code lost:
        if (r5 == 0) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b1, code lost:
        if ((r6 & 1) != 0) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b8, code lost:
        if (r5 == 0) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c0, code lost:
        if ((r6 & 1) != 1) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c3, code lost:
        if (r0 > 5) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c6, code lost:
        if (r0 >= 5) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00cb, code lost:
        if (r5 == 0) goto L_0x00ce;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int round(int r12) {
        /*
            r11 = this;
            int[] r0 = r11.mant
            int[] r1 = r11.mant
            int r1 = r1.length
            r2 = 1
            int r1 = r1 - r2
            r0 = r0[r1]
            r1 = 0
            if (r0 != 0) goto L_0x000d
            return r1
        L_0x000d:
            int[] r3 = r11.mant
            int r3 = r3.length
            r4 = 4
            int r3 = r3 * 4
            r5 = 1000(0x3e8, float:1.401E-42)
            r6 = r3
            r3 = 1000(0x3e8, float:1.401E-42)
        L_0x0018:
            if (r3 <= r0) goto L_0x001f
            int r3 = r3 / 10
            int r6 = r6 + -1
            goto L_0x0018
        L_0x001f:
            int r0 = r11.getDecimalDigits()
            int r6 = r6 - r0
            int r3 = r6 / 4
            r7 = 0
            r8 = 1
        L_0x0028:
            int r9 = r6 % 4
            if (r7 >= r9) goto L_0x0031
            int r8 = r8 * 10
            int r7 = r7 + 1
            goto L_0x0028
        L_0x0031:
            int[] r6 = r11.mant
            r6 = r6[r3]
            if (r8 > r2) goto L_0x0045
            int[] r7 = r11.mant
            int r7 = r7.length
            int r7 = r7 * 4
            int r7 = r7 + -3
            if (r0 != r7) goto L_0x0045
            int r12 = super.round(r12)
            return r12
        L_0x0045:
            if (r8 != r2) goto L_0x005c
            int[] r0 = r11.mant
            int r7 = r3 + -1
            r0 = r0[r7]
            int r0 = r0 / r5
            int r0 = r0 % 10
            int[] r9 = r11.mant
            r10 = r9[r7]
            int r10 = r10 % r5
            r9[r7] = r10
            int[] r5 = r11.mant
            r5 = r5[r7]
            goto L_0x0065
        L_0x005c:
            int r0 = r6 * 10
            int r0 = r0 / r8
            int r0 = r0 % 10
            int r5 = r8 / 10
            int r5 = r6 % r5
        L_0x0065:
            r12 = r12 | r5
            r5 = r12
            r12 = 0
        L_0x0068:
            if (r12 >= r3) goto L_0x0076
            int[] r7 = r11.mant
            r7 = r7[r12]
            r5 = r5 | r7
            int[] r7 = r11.mant
            r7[r12] = r1
            int r12 = r12 + 1
            goto L_0x0068
        L_0x0076:
            int[] r12 = r11.mant
            int r6 = r6 / r8
            int r7 = r6 * r8
            r12[r3] = r7
            int[] r12 = org.apache.commons.math3.dfp.DfpDec.C35031.$SwitchMap$org$apache$commons$math3$dfp$DfpField$RoundingMode
            org.apache.commons.math3.dfp.DfpField r7 = r11.getField()
            org.apache.commons.math3.dfp.DfpField$RoundingMode r7 = r7.getRoundingMode()
            int r7 = r7.ordinal()
            r12 = r12[r7]
            r7 = 5
            switch(r12) {
                case 1: goto L_0x00ce;
                case 2: goto L_0x00c9;
                case 3: goto L_0x00c6;
                case 4: goto L_0x00c3;
                case 5: goto L_0x00b4;
                case 6: goto L_0x00a5;
                case 7: goto L_0x009b;
                default: goto L_0x0091;
            }
        L_0x0091:
            byte r12 = r11.sign
            r6 = -1
            if (r12 != r6) goto L_0x00ce
            if (r0 != 0) goto L_0x00a3
            if (r5 == 0) goto L_0x00ce
            goto L_0x00a3
        L_0x009b:
            byte r12 = r11.sign
            if (r12 != r2) goto L_0x00ce
            if (r0 != 0) goto L_0x00a3
            if (r5 == 0) goto L_0x00ce
        L_0x00a3:
            r12 = 1
            goto L_0x00cf
        L_0x00a5:
            if (r0 > r7) goto L_0x00a3
            if (r0 != r7) goto L_0x00ab
            if (r5 != 0) goto L_0x00a3
        L_0x00ab:
            if (r0 != r7) goto L_0x00ce
            if (r5 != 0) goto L_0x00ce
            r12 = r6 & 1
            if (r12 != 0) goto L_0x00ce
            goto L_0x00a3
        L_0x00b4:
            if (r0 > r7) goto L_0x00a3
            if (r0 != r7) goto L_0x00ba
            if (r5 != 0) goto L_0x00a3
        L_0x00ba:
            if (r0 != r7) goto L_0x00ce
            if (r5 != 0) goto L_0x00ce
            r12 = r6 & 1
            if (r12 != r2) goto L_0x00ce
            goto L_0x00a3
        L_0x00c3:
            if (r0 <= r7) goto L_0x00ce
            goto L_0x00a3
        L_0x00c6:
            if (r0 < r7) goto L_0x00ce
            goto L_0x00a3
        L_0x00c9:
            if (r0 != 0) goto L_0x00a3
            if (r5 == 0) goto L_0x00ce
            goto L_0x00a3
        L_0x00ce:
            r12 = 0
        L_0x00cf:
            if (r12 == 0) goto L_0x00f3
        L_0x00d1:
            int[] r12 = r11.mant
            int r12 = r12.length
            if (r3 >= r12) goto L_0x00e6
            int[] r12 = r11.mant
            r12 = r12[r3]
            int r12 = r12 + r8
            int r8 = r12 / 10000
            int[] r6 = r11.mant
            int r12 = r12 % 10000
            r6[r3] = r12
            int r3 = r3 + 1
            goto L_0x00d1
        L_0x00e6:
            if (r8 == 0) goto L_0x00f3
            r11.shiftRight()
            int[] r12 = r11.mant
            int[] r3 = r11.mant
            int r3 = r3.length
            int r3 = r3 - r2
            r12[r3] = r8
        L_0x00f3:
            int r12 = r11.exp
            r2 = -32767(0xffffffffffff8001, float:NaN)
            if (r12 >= r2) goto L_0x0103
            org.apache.commons.math3.dfp.DfpField r12 = r11.getField()
            r0 = 8
            r12.setIEEEFlagsBits(r0)
            return r0
        L_0x0103:
            int r12 = r11.exp
            r2 = 32768(0x8000, float:4.5918E-41)
            if (r12 <= r2) goto L_0x0112
            org.apache.commons.math3.dfp.DfpField r12 = r11.getField()
            r12.setIEEEFlagsBits(r4)
            return r4
        L_0x0112:
            if (r0 != 0) goto L_0x0118
            if (r5 == 0) goto L_0x0117
            goto L_0x0118
        L_0x0117:
            return r1
        L_0x0118:
            org.apache.commons.math3.dfp.DfpField r12 = r11.getField()
            r0 = 16
            r12.setIEEEFlagsBits(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.dfp.DfpDec.round(int):int");
    }

    public Dfp nextAfter(Dfp dfp) {
        Dfp dfp2;
        Dfp dfp3;
        if (getField().getRadixDigits() != super.getField().getRadixDigits()) {
            getField().setIEEEFlagsBits(1);
            Dfp newInstance = newInstance(getZero());
            super.nans = 3;
            return dotrap(1, "nextAfter", super, super);
        }
        boolean lessThan = lessThan(super);
        if (equals(dfp)) {
            return newInstance(super);
        }
        if (lessThan(getZero())) {
            lessThan = !lessThan;
        }
        if (lessThan) {
            Dfp copysign = copysign(power10((intLog10() - getDecimalDigits()) + 1), super);
            if (equals(getZero())) {
                copysign = power10K((-32767 - this.mant.length) - 1);
            }
            if (super.equals(getZero())) {
                dfp2 = copysign(newInstance(getZero()), super);
            } else {
                dfp2 = add(super);
            }
        } else {
            Dfp copysign2 = copysign(power10(intLog10()), super);
            if (equals(copysign2)) {
                dfp3 = super.divide(power10(getDecimalDigits()));
            } else {
                dfp3 = super.divide(power10(getDecimalDigits() - 1));
            }
            if (equals(getZero())) {
                dfp3 = power10K((-32767 - this.mant.length) - 1);
            }
            if (super.equals(getZero())) {
                dfp2 = copysign(newInstance(getZero()), super);
            } else {
                dfp2 = subtract(super);
            }
        }
        if (super.classify() == 1 && classify() != 1) {
            getField().setIEEEFlagsBits(16);
            dfp2 = dotrap(16, "nextAfter", super, super);
        }
        if (!super.equals(getZero()) || equals(getZero())) {
            return super;
        }
        getField().setIEEEFlagsBits(16);
        return dotrap(16, "nextAfter", super, super);
    }
}
