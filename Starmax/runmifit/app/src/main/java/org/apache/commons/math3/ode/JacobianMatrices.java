package org.apache.commons.math3.ode;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.util.LocalizedFormats;

public class JacobianMatrices {
    /* access modifiers changed from: private */
    public boolean dirtyParameter;
    private ExpandableStatefulODE efode;
    private int index;
    /* access modifiers changed from: private */
    public List<ParameterJacobianProvider> jacobianProviders;
    /* access modifiers changed from: private */
    public MainStateJacobianProvider jode;
    private double[] matricesData;
    /* access modifiers changed from: private */
    public int paramDim;
    /* access modifiers changed from: private */
    public ParameterizedODE pode;
    /* access modifiers changed from: private */
    public ParameterConfiguration[] selectedParameters;
    /* access modifiers changed from: private */
    public int stateDim;

    public JacobianMatrices(FirstOrderDifferentialEquations firstOrderDifferentialEquations, double[] dArr, String... strArr) throws DimensionMismatchException {
        this(new MainStateJacobianWrapper(firstOrderDifferentialEquations, dArr), strArr);
    }

    public JacobianMatrices(MainStateJacobianProvider mainStateJacobianProvider, String... strArr) {
        this.efode = null;
        this.index = -1;
        this.jode = mainStateJacobianProvider;
        this.pode = null;
        this.stateDim = mainStateJacobianProvider.getDimension();
        int i = 0;
        if (strArr == null) {
            this.selectedParameters = null;
            this.paramDim = 0;
        } else {
            this.selectedParameters = new ParameterConfiguration[strArr.length];
            for (int i2 = 0; i2 < strArr.length; i2++) {
                this.selectedParameters[i2] = new ParameterConfiguration(strArr[i2], Double.NaN);
            }
            this.paramDim = strArr.length;
        }
        this.dirtyParameter = false;
        this.jacobianProviders = new ArrayList();
        int i3 = this.stateDim;
        this.matricesData = new double[((this.paramDim + i3) * i3)];
        while (true) {
            int i4 = this.stateDim;
            if (i < i4) {
                this.matricesData[(i4 + 1) * i] = 1.0d;
                i++;
            } else {
                return;
            }
        }
    }

    public void registerVariationalEquations(ExpandableStatefulODE expandableStatefulODE) throws DimensionMismatchException, MismatchedEquations {
        FirstOrderDifferentialEquations firstOrderDifferentialEquations = this.jode;
        if (firstOrderDifferentialEquations instanceof MainStateJacobianWrapper) {
            firstOrderDifferentialEquations = ((MainStateJacobianWrapper) firstOrderDifferentialEquations).ode;
        }
        if (expandableStatefulODE.getPrimary() == firstOrderDifferentialEquations) {
            this.efode = expandableStatefulODE;
            this.index = this.efode.addSecondaryEquations(new JacobiansSecondaryEquations());
            this.efode.setSecondaryState(this.index, this.matricesData);
            return;
        }
        throw new MismatchedEquations();
    }

    public void addParameterJacobianProvider(ParameterJacobianProvider parameterJacobianProvider) {
        this.jacobianProviders.add(parameterJacobianProvider);
    }

    public void setParameterizedODE(ParameterizedODE parameterizedODE) {
        this.pode = parameterizedODE;
        this.dirtyParameter = true;
    }

    public void setParameterStep(String str, double d) throws UnknownParameterException {
        ParameterConfiguration[] parameterConfigurationArr = this.selectedParameters;
        for (ParameterConfiguration parameterConfiguration : parameterConfigurationArr) {
            if (str.equals(parameterConfiguration.getParameterName())) {
                parameterConfiguration.setHP(d);
                this.dirtyParameter = true;
                return;
            }
        }
        throw new UnknownParameterException(str);
    }

    public void setInitialMainStateJacobian(double[][] dArr) throws DimensionMismatchException {
        checkDimension(this.stateDim, dArr);
        checkDimension(this.stateDim, dArr[0]);
        int i = 0;
        for (double[] dArr2 : dArr) {
            System.arraycopy(dArr2, 0, this.matricesData, i, this.stateDim);
            i += this.stateDim;
        }
        ExpandableStatefulODE expandableStatefulODE = this.efode;
        if (expandableStatefulODE != null) {
            expandableStatefulODE.setSecondaryState(this.index, this.matricesData);
        }
    }

    public void setInitialParameterJacobian(String str, double[] dArr) throws UnknownParameterException, DimensionMismatchException {
        checkDimension(this.stateDim, dArr);
        int i = this.stateDim;
        int i2 = i * i;
        for (ParameterConfiguration parameterConfiguration : this.selectedParameters) {
            if (str.equals(parameterConfiguration.getParameterName())) {
                System.arraycopy(dArr, 0, this.matricesData, i2, this.stateDim);
                ExpandableStatefulODE expandableStatefulODE = this.efode;
                if (expandableStatefulODE != null) {
                    expandableStatefulODE.setSecondaryState(this.index, this.matricesData);
                    return;
                }
                return;
            }
            i2 += this.stateDim;
        }
        throw new UnknownParameterException(str);
    }

    public void getCurrentMainSetJacobian(double[][] dArr) {
        double[] secondaryState = this.efode.getSecondaryState(this.index);
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = this.stateDim;
            if (i < i3) {
                System.arraycopy(secondaryState, i2, dArr[i], 0, i3);
                i2 += this.stateDim;
                i++;
            } else {
                return;
            }
        }
    }

    public void getCurrentParameterJacobian(String str, double[] dArr) {
        double[] secondaryState = this.efode.getSecondaryState(this.index);
        int i = this.stateDim;
        ParameterConfiguration[] parameterConfigurationArr = this.selectedParameters;
        int length = parameterConfigurationArr.length;
        int i2 = i * i;
        int i3 = 0;
        while (i3 < length) {
            if (parameterConfigurationArr[i3].getParameterName().equals(str)) {
                System.arraycopy(secondaryState, i2, dArr, 0, this.stateDim);
                return;
            } else {
                i2 += this.stateDim;
                i3++;
            }
        }
    }

    private void checkDimension(int i, Object obj) throws DimensionMismatchException {
        int length = obj == null ? 0 : Array.getLength(obj);
        if (length != i) {
            throw new DimensionMismatchException(length, i);
        }
    }

    private class JacobiansSecondaryEquations implements SecondaryEquations {
        private JacobiansSecondaryEquations() {
        }

        public int getDimension() {
            return JacobianMatrices.this.stateDim * (JacobianMatrices.this.stateDim + JacobianMatrices.this.paramDim);
        }

        public void computeDerivatives(double d, double[] dArr, double[] dArr2, double[] dArr3, double[] dArr4) throws MaxCountExceededException, DimensionMismatchException {
            int i;
            int i2;
            int i3;
            double[] dArr5 = dArr4;
            if (JacobianMatrices.this.dirtyParameter && JacobianMatrices.this.paramDim != 0) {
                JacobianMatrices.this.jacobianProviders.add(new ParameterJacobianWrapper(JacobianMatrices.this.jode, JacobianMatrices.this.pode, JacobianMatrices.this.selectedParameters));
                boolean unused = JacobianMatrices.this.dirtyParameter = false;
            }
            double[][] dArr6 = (double[][]) Array.newInstance(double.class, JacobianMatrices.this.stateDim, JacobianMatrices.this.stateDim);
            JacobianMatrices.this.jode.computeMainStateJacobian(d, dArr, dArr2, dArr6);
            for (int i4 = 0; i4 < JacobianMatrices.this.stateDim; i4++) {
                double[] dArr7 = dArr6[i4];
                for (int i5 = 0; i5 < JacobianMatrices.this.stateDim; i5++) {
                    double d2 = 0.0d;
                    int i6 = i5;
                    for (int i7 = 0; i7 < JacobianMatrices.this.stateDim; i7++) {
                        d2 += dArr7[i7] * dArr3[i6];
                        i6 += JacobianMatrices.this.stateDim;
                    }
                    dArr5[(JacobianMatrices.this.stateDim * i4) + i5] = d2;
                }
            }
            if (JacobianMatrices.this.paramDim != 0) {
                double[] dArr8 = new double[JacobianMatrices.this.stateDim];
                int access$200 = JacobianMatrices.this.stateDim * JacobianMatrices.this.stateDim;
                ParameterConfiguration[] access$700 = JacobianMatrices.this.selectedParameters;
                int length = access$700.length;
                int i8 = access$200;
                int i9 = 0;
                while (i9 < length) {
                    ParameterConfiguration parameterConfiguration = access$700[i9];
                    boolean z = false;
                    int i10 = 0;
                    while (!z && i10 < JacobianMatrices.this.jacobianProviders.size()) {
                        ParameterJacobianProvider parameterJacobianProvider = (ParameterJacobianProvider) JacobianMatrices.this.jacobianProviders.get(i10);
                        if (parameterJacobianProvider.isSupported(parameterConfiguration.getParameterName())) {
                            i = i10;
                            i3 = i8;
                            i2 = length;
                            parameterJacobianProvider.computeParameterJacobian(d, dArr, dArr2, parameterConfiguration.getParameterName(), dArr8);
                            for (int i11 = 0; i11 < JacobianMatrices.this.stateDim; i11++) {
                                double[] dArr9 = dArr6[i11];
                                int i12 = i3;
                                double d3 = dArr8[i11];
                                for (int i13 = 0; i13 < JacobianMatrices.this.stateDim; i13++) {
                                    d3 += dArr9[i13] * dArr3[i12];
                                    i12++;
                                }
                                dArr5[i3 + i11] = d3;
                            }
                            z = true;
                        } else {
                            i = i10;
                            i3 = i8;
                            i2 = length;
                        }
                        i10 = i + 1;
                        i8 = i3;
                        length = i2;
                    }
                    int i14 = i8;
                    int i15 = length;
                    if (!z) {
                        Arrays.fill(dArr5, i14, i14 + JacobianMatrices.this.stateDim, 0.0d);
                    }
                    i8 = i14 + JacobianMatrices.this.stateDim;
                    i9++;
                    length = i15;
                }
            }
        }
    }

    private static class MainStateJacobianWrapper implements MainStateJacobianProvider {

        /* renamed from: hY */
        private final double[] f8064hY;
        /* access modifiers changed from: private */
        public final FirstOrderDifferentialEquations ode;

        public MainStateJacobianWrapper(FirstOrderDifferentialEquations firstOrderDifferentialEquations, double[] dArr) throws DimensionMismatchException {
            this.ode = firstOrderDifferentialEquations;
            this.f8064hY = (double[]) dArr.clone();
            if (dArr.length != firstOrderDifferentialEquations.getDimension()) {
                throw new DimensionMismatchException(firstOrderDifferentialEquations.getDimension(), dArr.length);
            }
        }

        public int getDimension() {
            return this.ode.getDimension();
        }

        public void computeDerivatives(double d, double[] dArr, double[] dArr2) throws MaxCountExceededException, DimensionMismatchException {
            this.ode.computeDerivatives(d, dArr, dArr2);
        }

        public void computeMainStateJacobian(double d, double[] dArr, double[] dArr2, double[][] dArr3) throws MaxCountExceededException, DimensionMismatchException {
            double[] dArr4 = dArr;
            int dimension = this.ode.getDimension();
            double[] dArr5 = new double[dimension];
            for (int i = 0; i < dimension; i++) {
                double d2 = dArr4[i];
                dArr4[i] = dArr4[i] + this.f8064hY[i];
                this.ode.computeDerivatives(d, dArr4, dArr5);
                for (int i2 = 0; i2 < dimension; i2++) {
                    dArr3[i2][i] = (dArr5[i2] - dArr2[i2]) / this.f8064hY[i];
                }
                dArr4[i] = d2;
            }
        }
    }

    public static class MismatchedEquations extends MathIllegalArgumentException {
        private static final long serialVersionUID = 20120902;

        public MismatchedEquations() {
            super(LocalizedFormats.UNMATCHED_ODE_IN_EXPANDED_SET, new Object[0]);
        }
    }
}
