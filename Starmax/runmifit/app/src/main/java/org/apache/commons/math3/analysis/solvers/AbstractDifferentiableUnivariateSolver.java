package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.analysis.DifferentiableUnivariateFunction;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.exception.TooManyEvaluationsException;

@Deprecated
public abstract class AbstractDifferentiableUnivariateSolver extends BaseAbstractUnivariateSolver<DifferentiableUnivariateFunction> implements DifferentiableUnivariateSolver {
    private UnivariateFunction functionDerivative;

    protected AbstractDifferentiableUnivariateSolver(double d) {
        super(d);
    }

    protected AbstractDifferentiableUnivariateSolver(double d, double d2, double d3) {
        super(d, d2, d3);
    }

    /* access modifiers changed from: protected */
    public double computeDerivativeObjectiveValue(double d) throws TooManyEvaluationsException {
        incrementEvaluationCount();
        return this.functionDerivative.value(d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.analysis.solvers.BaseAbstractUnivariateSolver.setup(int, org.apache.commons.math3.analysis.UnivariateFunction, double, double, double):void
     arg types: [int, org.apache.commons.math3.analysis.DifferentiableUnivariateFunction, double, double, double]
     candidates:
      org.apache.commons.math3.analysis.solvers.AbstractDifferentiableUnivariateSolver.setup(int, org.apache.commons.math3.analysis.DifferentiableUnivariateFunction, double, double, double):void
      org.apache.commons.math3.analysis.solvers.BaseAbstractUnivariateSolver.setup(int, org.apache.commons.math3.analysis.UnivariateFunction, double, double, double):void */
    /* access modifiers changed from: protected */
    public void setup(int i, DifferentiableUnivariateFunction differentiableUnivariateFunction, double d, double d2, double d3) {
        super.setup(i, (UnivariateFunction) differentiableUnivariateFunction, d, d2, d3);
        this.functionDerivative = differentiableUnivariateFunction.derivative();
    }
}
