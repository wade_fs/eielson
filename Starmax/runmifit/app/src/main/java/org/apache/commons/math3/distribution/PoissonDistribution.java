package org.apache.commons.math3.distribution;

import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well19937c;
import org.apache.commons.math3.special.Gamma;
import org.apache.commons.math3.util.FastMath;

public class PoissonDistribution extends AbstractIntegerDistribution {
    public static final double DEFAULT_EPSILON = 1.0E-12d;
    public static final int DEFAULT_MAX_ITERATIONS = 10000000;
    private static final long serialVersionUID = -3349935121172596109L;
    private final double epsilon;
    private final ExponentialDistribution exponential;
    private final int maxIterations;
    private final double mean;
    private final NormalDistribution normal;

    public int getSupportLowerBound() {
        return 0;
    }

    public int getSupportUpperBound() {
        return Integer.MAX_VALUE;
    }

    public boolean isSupportConnected() {
        return true;
    }

    public PoissonDistribution(double d) throws NotStrictlyPositiveException {
        this(d, 1.0E-12d, DEFAULT_MAX_ITERATIONS);
    }

    public PoissonDistribution(double d, double d2, int i) throws NotStrictlyPositiveException {
        this(new Well19937c(), d, d2, i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PoissonDistribution(RandomGenerator randomGenerator, double d, double d2, int i) throws NotStrictlyPositiveException {
        super(randomGenerator);
        double d3 = d;
        if (d3 > 0.0d) {
            this.mean = d3;
            this.epsilon = d2;
            this.maxIterations = i;
            this.normal = new NormalDistribution(randomGenerator, d, FastMath.sqrt(d), 1.0E-9d);
            this.exponential = new ExponentialDistribution(randomGenerator, 1.0d, 1.0E-9d);
            return;
        }
        throw new NotStrictlyPositiveException(LocalizedFormats.MEAN, Double.valueOf(d));
    }

    public PoissonDistribution(double d, double d2) throws NotStrictlyPositiveException {
        this(d, d2, DEFAULT_MAX_ITERATIONS);
    }

    public PoissonDistribution(double d, int i) {
        this(d, 1.0E-12d, i);
    }

    public double getMean() {
        return this.mean;
    }

    public double probability(int i) {
        double logProbability = logProbability(i);
        if (logProbability == Double.NEGATIVE_INFINITY) {
            return 0.0d;
        }
        return FastMath.exp(logProbability);
    }

    public double logProbability(int i) {
        if (i < 0 || i == Integer.MAX_VALUE) {
            return Double.NEGATIVE_INFINITY;
        }
        if (i == 0) {
            return -this.mean;
        }
        double d = (double) i;
        return (((-SaddlePointExpansion.getStirlingError(d)) - SaddlePointExpansion.getDeviancePart(d, this.mean)) - (FastMath.log(6.283185307179586d) * 0.5d)) - (FastMath.log(d) * 0.5d);
    }

    public double cumulativeProbability(int i) {
        if (i < 0) {
            return 0.0d;
        }
        if (i == Integer.MAX_VALUE) {
            return 1.0d;
        }
        double d = (double) i;
        Double.isNaN(d);
        return Gamma.regularizedGammaQ(d + 1.0d, this.mean, this.epsilon, this.maxIterations);
    }

    public double normalApproximateProbability(int i) {
        NormalDistribution normalDistribution = this.normal;
        double d = (double) i;
        Double.isNaN(d);
        return normalDistribution.cumulativeProbability(d + 0.5d);
    }

    public double getNumericalMean() {
        return getMean();
    }

    public double getNumericalVariance() {
        return getMean();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.FastMath.min(long, long):long
     arg types: [long, int]
     candidates:
      org.apache.commons.math3.util.FastMath.min(double, double):double
      org.apache.commons.math3.util.FastMath.min(float, float):float
      org.apache.commons.math3.util.FastMath.min(int, int):int
      org.apache.commons.math3.util.FastMath.min(long, long):long */
    public int sample() {
        return (int) FastMath.min(nextPoisson(this.mean), 2147483647L);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0148, code lost:
        r7 = r7 + r37;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long nextPoisson(double r44) {
        /*
            r43 = this;
            r0 = r43
            r1 = r44
            r3 = 0
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r7 = 4630826316843712512(0x4044000000000000, double:40.0)
            int r9 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r9 >= 0) goto L_0x0030
            double r7 = -r1
            double r7 = org.apache.commons.math3.util.FastMath.exp(r7)
        L_0x0013:
            double r9 = (double) r3
            r11 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r11 = r11 * r1
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 >= 0) goto L_0x002f
            org.apache.commons.math3.random.RandomGenerator r9 = r0.random
            double r9 = r9.nextDouble()
            double r5 = r5 * r9
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 < 0) goto L_0x002f
            r9 = 1
            long r3 = r3 + r9
            goto L_0x0013
        L_0x002f:
            return r3
        L_0x0030:
            double r7 = org.apache.commons.math3.util.FastMath.floor(r44)
            double r1 = r1 - r7
            double r9 = org.apache.commons.math3.util.FastMath.log(r7)
            int r11 = (int) r7
            double r11 = org.apache.commons.math3.util.CombinatoricsUtils.factorialLog(r11)
            r13 = 1
            int r15 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r15 >= 0) goto L_0x0045
            goto L_0x0049
        L_0x0045:
            long r3 = r0.nextPoisson(r1)
        L_0x0049:
            r1 = 4629700416936869888(0x4040000000000000, double:32.0)
            double r1 = r1 * r7
            r13 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
            double r1 = r1 / r13
            double r1 = r1 + r5
            double r1 = org.apache.commons.math3.util.FastMath.log(r1)
            double r1 = r1 * r7
            double r1 = org.apache.commons.math3.util.FastMath.sqrt(r1)
            r15 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r17 = r1 / r15
            double r19 = r7 * r15
            double r21 = r19 + r1
            double r13 = r13 * r21
            double r13 = org.apache.commons.math3.util.FastMath.sqrt(r13)
            r23 = 4620693217682128896(0x4020000000000000, double:8.0)
            double r23 = r23 * r7
            double r23 = r5 / r23
            double r25 = org.apache.commons.math3.util.FastMath.exp(r23)
            double r13 = r13 * r25
            double r25 = r21 / r1
            r44 = r3
            double r3 = -r1
            double r27 = r1 + r5
            double r3 = r3 * r27
            double r3 = r3 / r21
            double r3 = org.apache.commons.math3.util.FastMath.exp(r3)
            double r3 = r3 * r25
            double r27 = r13 + r3
            double r27 = r27 + r5
            double r13 = r13 / r27
            double r3 = r3 / r27
        L_0x0091:
            org.apache.commons.math3.random.RandomGenerator r5 = r0.random
            double r5 = r5.nextDouble()
            r29 = 0
            int r31 = (r5 > r13 ? 1 : (r5 == r13 ? 0 : -1))
            if (r31 > 0) goto L_0x00df
            org.apache.commons.math3.random.RandomGenerator r5 = r0.random
            double r5 = r5.nextGaussian()
            double r31 = r7 + r17
            double r31 = org.apache.commons.math3.util.FastMath.sqrt(r31)
            double r31 = r31 * r5
            r33 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r31 = r31 - r33
            int r33 = (r31 > r1 ? 1 : (r31 == r1 ? 0 : -1))
            if (r33 > 0) goto L_0x0091
            r33 = r11
            double r11 = -r7
            int r35 = (r31 > r11 ? 1 : (r31 == r11 ? 0 : -1))
            if (r35 >= 0) goto L_0x00bd
            r11 = r33
            goto L_0x0091
        L_0x00bd:
            int r11 = (r31 > r29 ? 1 : (r31 == r29 ? 0 : -1))
            if (r11 >= 0) goto L_0x00c6
            double r11 = org.apache.commons.math3.util.FastMath.floor(r31)
            goto L_0x00ca
        L_0x00c6:
            double r11 = org.apache.commons.math3.util.FastMath.ceil(r31)
        L_0x00ca:
            org.apache.commons.math3.distribution.ExponentialDistribution r15 = r0.exponential
            r37 = r11
            double r11 = r15.sample()
            double r11 = -r11
            double r5 = r5 * r5
            r15 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r5 = r5 / r15
            double r11 = r11 - r5
            double r11 = r11 + r23
            r5 = r11
            r15 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            goto L_0x010a
        L_0x00df:
            r33 = r11
            double r11 = r13 + r3
            int r15 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r15 <= 0) goto L_0x00e9
            goto L_0x016e
        L_0x00e9:
            org.apache.commons.math3.distribution.ExponentialDistribution r5 = r0.exponential
            double r5 = r5.sample()
            double r5 = r5 * r25
            double r31 = r1 + r5
            double r11 = org.apache.commons.math3.util.FastMath.ceil(r31)
            org.apache.commons.math3.distribution.ExponentialDistribution r5 = r0.exponential
            double r5 = r5.sample()
            double r5 = -r5
            r15 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r27 = r31 + r15
            double r27 = r27 * r1
            double r27 = r27 / r21
            double r5 = r5 - r27
            r37 = r11
        L_0x010a:
            int r11 = (r31 > r29 ? 1 : (r31 == r29 ? 0 : -1))
            if (r11 >= 0) goto L_0x0110
            r11 = 1
            goto L_0x0111
        L_0x0110:
            r11 = 0
        L_0x0111:
            double r29 = r37 + r15
            double r15 = r37 * r29
            r31 = r1
            double r0 = r15 / r19
            r15 = r3
            double r2 = -r0
            int r4 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0122
            if (r11 != 0) goto L_0x0122
            goto L_0x0148
        L_0x0122:
            r2 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r35 = r37 * r2
            r27 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r35 = r35 + r27
            r39 = 4618441417868443648(0x4018000000000000, double:6.0)
            double r39 = r39 * r7
            double r35 = r35 / r39
            double r35 = r35 - r27
            double r35 = r35 * r0
            double r0 = r0 * r0
            r39 = 4613937818241073152(0x4008000000000000, double:3.0)
            double r11 = (double) r11
            java.lang.Double.isNaN(r11)
            double r11 = r11 * r29
            double r11 = r11 + r7
            double r11 = r11 * r39
            double r0 = r0 / r11
            double r0 = r35 - r0
            int r4 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r4 >= 0) goto L_0x014b
        L_0x0148:
            double r7 = r7 + r37
            goto L_0x016e
        L_0x014b:
            int r0 = (r5 > r35 ? 1 : (r5 == r35 ? 0 : -1))
            if (r0 <= 0) goto L_0x015c
        L_0x014f:
            r0 = r43
            r11 = r33
            r41 = r2
            r1 = r31
            r3 = r15
            r15 = r41
            goto L_0x0091
        L_0x015c:
            double r0 = r37 * r9
            double r11 = r37 + r7
            int r4 = (int) r11
            double r29 = org.apache.commons.math3.util.CombinatoricsUtils.factorialLog(r4)
            double r0 = r0 - r29
            double r0 = r0 + r33
            int r4 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r4 >= 0) goto L_0x014f
            r7 = r11
        L_0x016e:
            long r0 = (long) r7
            long r3 = r44 + r0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.distribution.PoissonDistribution.nextPoisson(double):long");
    }
}
