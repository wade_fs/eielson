package org.apache.commons.math3.analysis.integration;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.util.FastMath;

public class TrapezoidIntegrator extends BaseAbstractUnivariateIntegrator {
    public static final int TRAPEZOID_MAX_ITERATIONS_COUNT = 64;

    /* renamed from: s */
    private double f7950s;

    public TrapezoidIntegrator(double d, double d2, int i, int i2) throws NotStrictlyPositiveException, NumberIsTooSmallException, NumberIsTooLargeException {
        super(d, d2, i, i2);
        if (i2 > 64) {
            throw new NumberIsTooLargeException(Integer.valueOf(i2), 64, false);
        }
    }

    public TrapezoidIntegrator(int i, int i2) throws NotStrictlyPositiveException, NumberIsTooSmallException, NumberIsTooLargeException {
        super(i, i2);
        if (i2 > 64) {
            throw new NumberIsTooLargeException(Integer.valueOf(i2), 64, false);
        }
    }

    public TrapezoidIntegrator() {
        super(3, 64);
    }

    /* access modifiers changed from: package-private */
    public double stage(BaseAbstractUnivariateIntegrator baseAbstractUnivariateIntegrator, int i) throws TooManyEvaluationsException {
        BaseAbstractUnivariateIntegrator baseAbstractUnivariateIntegrator2 = baseAbstractUnivariateIntegrator;
        if (i == 0) {
            double max = super.getMax();
            double min = super.getMin();
            this.f7950s = (max - min) * 0.5d * (super.computeObjectiveValue(min) + super.computeObjectiveValue(max));
            return this.f7950s;
        }
        long j = 1 << (i - 1);
        double max2 = super.getMax();
        double min2 = super.getMin();
        double d = (double) j;
        Double.isNaN(d);
        double d2 = (max2 - min2) / d;
        double d3 = min2 + (d2 * 0.5d);
        double d4 = 0.0d;
        for (long j2 = 0; j2 < j; j2++) {
            d4 += super.computeObjectiveValue(d3);
            d3 += d2;
        }
        this.f7950s = (this.f7950s + (d4 * d2)) * 0.5d;
        return this.f7950s;
    }

    /* access modifiers changed from: protected */
    public double doIntegrate() throws MathIllegalArgumentException, TooManyEvaluationsException, MaxCountExceededException {
        double stage;
        double stage2 = stage(super, 0);
        this.iterations.incrementCount();
        while (true) {
            int count = this.iterations.getCount();
            stage = stage(super, count);
            if (count >= getMinimalIterationCount()) {
                double abs = FastMath.abs(stage - stage2);
                if (abs <= getRelativeAccuracy() * (FastMath.abs(stage2) + FastMath.abs(stage)) * 0.5d || abs <= getAbsoluteAccuracy()) {
                    return stage;
                }
            }
            this.iterations.incrementCount();
            stage2 = stage;
        }
        return stage;
    }
}
