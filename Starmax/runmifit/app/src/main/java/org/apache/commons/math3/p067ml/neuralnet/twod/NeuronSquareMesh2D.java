package org.apache.commons.math3.p067ml.neuralnet.twod;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import org.apache.commons.math3.exception.MathInternalError;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.p067ml.neuralnet.FeatureInitializer;
import org.apache.commons.math3.p067ml.neuralnet.Network;
import org.apache.commons.math3.p067ml.neuralnet.Neuron;
import org.apache.commons.math3.p067ml.neuralnet.SquareNeighbourhood;

/* renamed from: org.apache.commons.math3.ml.neuralnet.twod.NeuronSquareMesh2D */
public class NeuronSquareMesh2D implements Serializable {
    private final long[][] identifiers;
    private final SquareNeighbourhood neighbourhood;
    private final Network network;
    private final int numberOfColumns;
    private final int numberOfRows;
    private final boolean wrapColumns;
    private final boolean wrapRows;

    NeuronSquareMesh2D(boolean z, boolean z2, SquareNeighbourhood squareNeighbourhood, double[][][] dArr) {
        this.numberOfRows = dArr.length;
        this.numberOfColumns = dArr[0].length;
        int i = this.numberOfRows;
        if (i >= 2) {
            int i2 = this.numberOfColumns;
            if (i2 >= 2) {
                this.wrapRows = z;
                this.wrapColumns = z2;
                this.neighbourhood = squareNeighbourhood;
                this.network = new Network(0, dArr[0][0].length);
                this.identifiers = (long[][]) Array.newInstance(long.class, this.numberOfRows, this.numberOfColumns);
                for (int i3 = 0; i3 < this.numberOfRows; i3++) {
                    for (int i4 = 0; i4 < this.numberOfColumns; i4++) {
                        this.identifiers[i3][i4] = this.network.createNeuron(dArr[i3][i4]);
                    }
                }
                createLinks();
                return;
            }
            throw new NumberIsTooSmallException(Integer.valueOf(i2), 2, true);
        }
        throw new NumberIsTooSmallException(Integer.valueOf(i), 2, true);
    }

    public NeuronSquareMesh2D(int i, boolean z, int i2, boolean z2, SquareNeighbourhood squareNeighbourhood, FeatureInitializer[] featureInitializerArr) {
        if (i < 2) {
            throw new NumberIsTooSmallException(Integer.valueOf(i), 2, true);
        } else if (i2 >= 2) {
            this.numberOfRows = i;
            this.wrapRows = z;
            this.numberOfColumns = i2;
            this.wrapColumns = z2;
            this.neighbourhood = squareNeighbourhood;
            this.identifiers = (long[][]) Array.newInstance(long.class, this.numberOfRows, this.numberOfColumns);
            int length = featureInitializerArr.length;
            this.network = new Network(0, length);
            for (int i3 = 0; i3 < i; i3++) {
                for (int i4 = 0; i4 < i2; i4++) {
                    double[] dArr = new double[length];
                    for (int i5 = 0; i5 < length; i5++) {
                        dArr[i5] = featureInitializerArr[i5].value();
                    }
                    this.identifiers[i3][i4] = this.network.createNeuron(dArr);
                }
            }
            createLinks();
        } else {
            throw new NumberIsTooSmallException(Integer.valueOf(i2), 2, true);
        }
    }

    public Network getNetwork() {
        return this.network;
    }

    public int getNumberOfRows() {
        return this.numberOfRows;
    }

    public int getNumberOfColumns() {
        return this.numberOfColumns;
    }

    public Neuron getNeuron(int i, int i2) {
        if (i < 0 || i >= this.numberOfRows) {
            throw new OutOfRangeException(Integer.valueOf(i), 0, Integer.valueOf(this.numberOfRows - 1));
        } else if (i2 >= 0 && i2 < this.numberOfColumns) {
            return this.network.getNeuron(this.identifiers[i][i2]);
        } else {
            throw new OutOfRangeException(Integer.valueOf(i2), 0, Integer.valueOf(this.numberOfColumns - 1));
        }
    }

    private void createLinks() {
        ArrayList<Long> arrayList = new ArrayList<>();
        int i = this.numberOfRows - 1;
        int i2 = this.numberOfColumns - 1;
        for (int i3 = 0; i3 < this.numberOfRows; i3++) {
            for (int i4 = 0; i4 < this.numberOfColumns; i4++) {
                arrayList.clear();
                int i5 = C35691.f8062x358e2f1c[this.neighbourhood.ordinal()];
                if (i5 == 1) {
                    if (i3 > 0) {
                        if (i4 > 0) {
                            arrayList.add(Long.valueOf(this.identifiers[i3 - 1][i4 - 1]));
                        }
                        if (i4 < i2) {
                            arrayList.add(Long.valueOf(this.identifiers[i3 - 1][i4 + 1]));
                        }
                    }
                    if (i3 < i) {
                        if (i4 > 0) {
                            arrayList.add(Long.valueOf(this.identifiers[i3 + 1][i4 - 1]));
                        }
                        if (i4 < i2) {
                            arrayList.add(Long.valueOf(this.identifiers[i3 + 1][i4 + 1]));
                        }
                    }
                    if (this.wrapRows) {
                        if (i3 == 0) {
                            if (i4 > 0) {
                                arrayList.add(Long.valueOf(this.identifiers[i][i4 - 1]));
                            }
                            if (i4 < i2) {
                                arrayList.add(Long.valueOf(this.identifiers[i][i4 + 1]));
                            }
                        } else if (i3 == i) {
                            if (i4 > 0) {
                                arrayList.add(Long.valueOf(this.identifiers[0][i4 - 1]));
                            }
                            if (i4 < i2) {
                                arrayList.add(Long.valueOf(this.identifiers[0][i4 + 1]));
                            }
                        }
                    }
                    if (this.wrapColumns) {
                        if (i4 == 0) {
                            if (i3 > 0) {
                                arrayList.add(Long.valueOf(this.identifiers[i3 - 1][i2]));
                            }
                            if (i3 < i) {
                                arrayList.add(Long.valueOf(this.identifiers[i3 + 1][i2]));
                            }
                        } else if (i4 == i2) {
                            if (i3 > 0) {
                                arrayList.add(Long.valueOf(this.identifiers[i3 - 1][0]));
                            }
                            if (i3 < i) {
                                arrayList.add(Long.valueOf(this.identifiers[i3 + 1][0]));
                            }
                        }
                    }
                    if (this.wrapRows && this.wrapColumns) {
                        if (i3 == 0 && i4 == 0) {
                            arrayList.add(Long.valueOf(this.identifiers[i][i2]));
                        } else if (i3 == 0 && i4 == i2) {
                            arrayList.add(Long.valueOf(this.identifiers[i][0]));
                        } else if (i3 == i && i4 == 0) {
                            arrayList.add(Long.valueOf(this.identifiers[0][i2]));
                        } else if (i3 == i && i4 == i2) {
                            arrayList.add(Long.valueOf(this.identifiers[0][0]));
                        }
                    }
                } else if (i5 != 2) {
                    throw new MathInternalError();
                }
                if (i3 > 0) {
                    arrayList.add(Long.valueOf(this.identifiers[i3 - 1][i4]));
                }
                if (i3 < i) {
                    arrayList.add(Long.valueOf(this.identifiers[i3 + 1][i4]));
                }
                if (this.wrapRows) {
                    if (i3 == 0) {
                        arrayList.add(Long.valueOf(this.identifiers[i][i4]));
                    } else if (i3 == i) {
                        arrayList.add(Long.valueOf(this.identifiers[0][i4]));
                    }
                }
                if (i4 > 0) {
                    arrayList.add(Long.valueOf(this.identifiers[i3][i4 - 1]));
                }
                if (i4 < i2) {
                    arrayList.add(Long.valueOf(this.identifiers[i3][i4 + 1]));
                }
                if (this.wrapColumns) {
                    if (i4 == 0) {
                        arrayList.add(Long.valueOf(this.identifiers[i3][i2]));
                    } else if (i4 == i2) {
                        arrayList.add(Long.valueOf(this.identifiers[i3][0]));
                    }
                }
                Neuron neuron = this.network.getNeuron(this.identifiers[i3][i4]);
                for (Long l : arrayList) {
                    this.network.addLink(neuron, this.network.getNeuron(l.longValue()));
                }
            }
        }
    }

    /* renamed from: org.apache.commons.math3.ml.neuralnet.twod.NeuronSquareMesh2D$1 */
    static /* synthetic */ class C35691 {

        /* renamed from: $SwitchMap$org$apache$commons$math3$ml$neuralnet$SquareNeighbourhood */
        static final /* synthetic */ int[] f8062x358e2f1c = new int[SquareNeighbourhood.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                org.apache.commons.math3.ml.neuralnet.SquareNeighbourhood[] r0 = org.apache.commons.math3.p067ml.neuralnet.SquareNeighbourhood.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.p067ml.neuralnet.twod.NeuronSquareMesh2D.C35691.f8062x358e2f1c = r0
                int[] r0 = org.apache.commons.math3.p067ml.neuralnet.twod.NeuronSquareMesh2D.C35691.f8062x358e2f1c     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.ml.neuralnet.SquareNeighbourhood r1 = org.apache.commons.math3.p067ml.neuralnet.SquareNeighbourhood.MOORE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.apache.commons.math3.p067ml.neuralnet.twod.NeuronSquareMesh2D.C35691.f8062x358e2f1c     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.ml.neuralnet.SquareNeighbourhood r1 = org.apache.commons.math3.p067ml.neuralnet.SquareNeighbourhood.VON_NEUMANN     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.p067ml.neuralnet.twod.NeuronSquareMesh2D.C35691.<clinit>():void");
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        throw new IllegalStateException();
    }

    private Object writeReplace() {
        double[][][] dArr = (double[][][]) Array.newInstance(double[].class, this.numberOfRows, this.numberOfColumns);
        for (int i = 0; i < this.numberOfRows; i++) {
            for (int i2 = 0; i2 < this.numberOfColumns; i2++) {
                dArr[i][i2] = getNeuron(i, i2).getFeatures();
            }
        }
        return new SerializationProxy(this.wrapRows, this.wrapColumns, this.neighbourhood, dArr);
    }

    /* renamed from: org.apache.commons.math3.ml.neuralnet.twod.NeuronSquareMesh2D$SerializationProxy */
    private static class SerializationProxy implements Serializable {
        private static final long serialVersionUID = 20130226;
        private final double[][][] featuresList;
        private final SquareNeighbourhood neighbourhood;
        private final boolean wrapColumns;
        private final boolean wrapRows;

        SerializationProxy(boolean z, boolean z2, SquareNeighbourhood squareNeighbourhood, double[][][] dArr) {
            this.wrapRows = z;
            this.wrapColumns = z2;
            this.neighbourhood = squareNeighbourhood;
            this.featuresList = dArr;
        }

        private Object readResolve() {
            return new NeuronSquareMesh2D(this.wrapRows, this.wrapColumns, this.neighbourhood, this.featuresList);
        }
    }
}
