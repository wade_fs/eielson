package org.apache.commons.math3.p067ml.neuralnet;

/* renamed from: org.apache.commons.math3.ml.neuralnet.UpdateAction */
public interface UpdateAction {
    void update(Network network, double[] dArr);
}
