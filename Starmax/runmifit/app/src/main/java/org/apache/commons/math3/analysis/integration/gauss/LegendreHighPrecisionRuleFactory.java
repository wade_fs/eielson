package org.apache.commons.math3.analysis.integration.gauss;

import java.math.BigDecimal;
import java.math.MathContext;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.util.Pair;

public class LegendreHighPrecisionRuleFactory extends BaseRuleFactory<BigDecimal> {
    private final MathContext mContext;
    private final BigDecimal minusOne;
    private final BigDecimal oneHalf;
    private final BigDecimal two;

    public LegendreHighPrecisionRuleFactory() {
        this(MathContext.DECIMAL128);
    }

    public LegendreHighPrecisionRuleFactory(MathContext mathContext) {
        this.mContext = mathContext;
        this.two = new BigDecimal("2", mathContext);
        this.minusOne = new BigDecimal("-1", mathContext);
        this.oneHalf = new BigDecimal("0.5", mathContext);
    }

    /* access modifiers changed from: protected */
    public Pair<BigDecimal[], BigDecimal[]> computeRule(int i) throws DimensionMismatchException {
        BigDecimal bigDecimal;
        int i2 = i;
        int i3 = 1;
        if (i2 == 1) {
            return new Pair<>(new BigDecimal[]{BigDecimal.ZERO}, new BigDecimal[]{this.two});
        }
        BigDecimal[] bigDecimalArr = (BigDecimal[]) getRuleInternal(i2 - 1).getFirst();
        BigDecimal[] bigDecimalArr2 = new BigDecimal[i2];
        BigDecimal[] bigDecimalArr3 = new BigDecimal[i2];
        int i4 = i2 / 2;
        int i5 = 0;
        while (i5 < i4) {
            BigDecimal bigDecimal2 = i5 == 0 ? this.minusOne : bigDecimalArr[i5 - 1];
            BigDecimal bigDecimal3 = i4 == i3 ? BigDecimal.ONE : bigDecimalArr[i5];
            BigDecimal bigDecimal4 = bigDecimal3;
            BigDecimal bigDecimal5 = BigDecimal.ONE;
            BigDecimal bigDecimal6 = BigDecimal.ONE;
            int i6 = 1;
            BigDecimal bigDecimal7 = bigDecimal2;
            while (i6 < i2) {
                BigDecimal[] bigDecimalArr4 = bigDecimalArr;
                BigDecimal bigDecimal8 = new BigDecimal((i6 * 2) + 1, this.mContext);
                BigDecimal bigDecimal9 = new BigDecimal(i6, this.mContext);
                int i7 = i6 + 1;
                int i8 = i4;
                BigDecimal bigDecimal10 = new BigDecimal(i7, this.mContext);
                BigDecimal divide = bigDecimal7.multiply(bigDecimal2.multiply(bigDecimal8, this.mContext), this.mContext).subtract(bigDecimal5.multiply(bigDecimal9, this.mContext), this.mContext).divide(bigDecimal10, this.mContext);
                BigDecimal divide2 = bigDecimal4.multiply(bigDecimal3.multiply(bigDecimal8, this.mContext), this.mContext).subtract(bigDecimal6.multiply(bigDecimal9, this.mContext), this.mContext).divide(bigDecimal10, this.mContext);
                bigDecimal5 = bigDecimal7;
                bigDecimalArr = bigDecimalArr4;
                i6 = i7;
                bigDecimal7 = divide;
                i4 = i8;
                BigDecimal bigDecimal11 = bigDecimal4;
                bigDecimal4 = divide2;
                bigDecimal6 = bigDecimal11;
            }
            BigDecimal[] bigDecimalArr5 = bigDecimalArr;
            int i9 = i4;
            BigDecimal multiply = bigDecimal2.add(bigDecimal3, this.mContext).multiply(this.oneHalf, this.mContext);
            BigDecimal bigDecimal12 = BigDecimal.ONE;
            BigDecimal bigDecimal13 = multiply;
            BigDecimal bigDecimal14 = bigDecimal13;
            boolean z = false;
            while (!z) {
                z = bigDecimal3.subtract(bigDecimal2, this.mContext).compareTo(bigDecimal13.ulp().multiply(BigDecimal.TEN, this.mContext)) <= 0;
                BigDecimal bigDecimal15 = BigDecimal.ONE;
                bigDecimal14 = bigDecimal13;
                int i10 = 1;
                while (i10 < i2) {
                    BigDecimal bigDecimal16 = new BigDecimal((i10 * 2) + 1, this.mContext);
                    BigDecimal bigDecimal17 = new BigDecimal(i10, this.mContext);
                    i10++;
                    bigDecimal2 = bigDecimal2;
                    BigDecimal divide3 = bigDecimal14.multiply(bigDecimal13.multiply(bigDecimal16, this.mContext), this.mContext).subtract(bigDecimal15.multiply(bigDecimal17, this.mContext), this.mContext).divide(new BigDecimal(i10, this.mContext), this.mContext);
                    bigDecimal15 = bigDecimal14;
                    bigDecimal14 = divide3;
                }
                BigDecimal bigDecimal18 = bigDecimal2;
                if (!z) {
                    if (bigDecimal7.signum() * bigDecimal14.signum() <= 0) {
                        bigDecimal3 = bigDecimal13;
                        bigDecimal13 = bigDecimal18;
                    } else {
                        bigDecimal7 = bigDecimal14;
                    }
                    bigDecimal = bigDecimal13;
                    bigDecimal13 = bigDecimal13.add(bigDecimal3, this.mContext).multiply(this.oneHalf, this.mContext);
                } else {
                    bigDecimal = bigDecimal18;
                }
                bigDecimal12 = bigDecimal15;
            }
            BigDecimal divide4 = BigDecimal.ONE.subtract(bigDecimal13.pow(2, this.mContext), this.mContext).multiply(this.two, this.mContext).divide(bigDecimal12.subtract(bigDecimal13.multiply(bigDecimal14, this.mContext), this.mContext).multiply(new BigDecimal(i2, this.mContext)).pow(2, this.mContext), this.mContext);
            bigDecimalArr2[i5] = bigDecimal13;
            bigDecimalArr3[i5] = divide4;
            int i11 = (i2 - i5) - 1;
            bigDecimalArr2[i11] = bigDecimal13.negate(this.mContext);
            bigDecimalArr3[i11] = divide4;
            i5++;
            bigDecimalArr = bigDecimalArr5;
            i4 = i9;
            i3 = 1;
        }
        int i12 = i4;
        if (i2 % 2 != 0) {
            BigDecimal bigDecimal19 = BigDecimal.ONE;
            for (int i13 = 1; i13 < i2; i13 += 2) {
                bigDecimal19 = bigDecimal19.multiply(new BigDecimal(i13, this.mContext), this.mContext).divide(new BigDecimal(i13 + 1, this.mContext), this.mContext).negate(this.mContext);
            }
            BigDecimal divide5 = this.two.divide(bigDecimal19.multiply(new BigDecimal(i2, this.mContext), this.mContext).pow(2, this.mContext), this.mContext);
            bigDecimalArr2[i12] = BigDecimal.ZERO;
            bigDecimalArr3[i12] = divide5;
        }
        return new Pair<>(bigDecimalArr2, bigDecimalArr3);
    }
}
