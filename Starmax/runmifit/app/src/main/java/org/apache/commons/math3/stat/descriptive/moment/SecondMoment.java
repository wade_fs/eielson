package org.apache.commons.math3.stat.descriptive.moment;

import java.io.Serializable;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.util.MathUtils;

public class SecondMoment extends FirstMoment implements Serializable {
    private static final long serialVersionUID = 3942403127395076445L;

    /* renamed from: m2 */
    protected double f8177m2;

    public /* bridge */ /* synthetic */ long getN() {
        return super.getN();
    }

    public SecondMoment() {
        this.f8177m2 = Double.NaN;
    }

    public SecondMoment(SecondMoment secondMoment) throws NullArgumentException {
        super(super);
        this.f8177m2 = secondMoment.f8177m2;
    }

    public void increment(double d) {
        if (this.f8175n < 1) {
            this.f8177m2 = 0.0d;
            this.f8174m1 = 0.0d;
        }
        super.increment(d);
        double d2 = this.f8177m2;
        double d3 = (double) this.f8175n;
        Double.isNaN(d3);
        this.f8177m2 = d2 + ((d3 - 1.0d) * this.dev * this.nDev);
    }

    public void clear() {
        super.clear();
        this.f8177m2 = Double.NaN;
    }

    public double getResult() {
        return this.f8177m2;
    }

    public SecondMoment copy() {
        SecondMoment secondMoment = new SecondMoment();
        copy(this, secondMoment);
        return secondMoment;
    }

    public static void copy(SecondMoment secondMoment, SecondMoment secondMoment2) throws NullArgumentException {
        MathUtils.checkNotNull(secondMoment);
        MathUtils.checkNotNull(secondMoment2);
        FirstMoment.copy(super, super);
        secondMoment2.f8177m2 = secondMoment.f8177m2;
    }
}
