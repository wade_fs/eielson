package org.apache.commons.math3.p067ml.neuralnet.sofm;

import org.apache.commons.math3.p067ml.neuralnet.sofm.util.ExponentialDecayFunction;
import org.apache.commons.math3.p067ml.neuralnet.sofm.util.QuasiSigmoidDecayFunction;
import org.apache.commons.math3.util.FastMath;

/* renamed from: org.apache.commons.math3.ml.neuralnet.sofm.NeighbourhoodSizeFunctionFactory */
public class NeighbourhoodSizeFunctionFactory {
    private NeighbourhoodSizeFunctionFactory() {
    }

    public static NeighbourhoodSizeFunction exponentialDecay(double d, double d2, long j) {
        final double d3 = d;
        final double d4 = d2;
        final long j2 = j;
        return new NeighbourhoodSizeFunction() {
            /* class org.apache.commons.math3.p067ml.neuralnet.sofm.NeighbourhoodSizeFunctionFactory.C35671 */
            private final ExponentialDecayFunction decay = new ExponentialDecayFunction(d3, d4, j2);

            public int value(long j) {
                return (int) FastMath.rint(this.decay.value(j));
            }
        };
    }

    public static NeighbourhoodSizeFunction quasiSigmoidDecay(double d, double d2, long j) {
        final double d3 = d;
        final double d4 = d2;
        final long j2 = j;
        return new NeighbourhoodSizeFunction() {
            /* class org.apache.commons.math3.p067ml.neuralnet.sofm.NeighbourhoodSizeFunctionFactory.C35682 */
            private final QuasiSigmoidDecayFunction decay = new QuasiSigmoidDecayFunction(d3, d4, j2);

            public int value(long j) {
                return (int) FastMath.rint(this.decay.value(j));
            }
        };
    }
}
