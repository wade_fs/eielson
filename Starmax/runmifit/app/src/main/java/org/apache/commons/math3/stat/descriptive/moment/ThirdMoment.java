package org.apache.commons.math3.stat.descriptive.moment;

import java.io.Serializable;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.util.MathUtils;

class ThirdMoment extends SecondMoment implements Serializable {
    private static final long serialVersionUID = -7818711964045118679L;

    /* renamed from: m3 */
    protected double f8178m3;
    protected double nDevSq;

    public ThirdMoment() {
        this.f8178m3 = Double.NaN;
        this.nDevSq = Double.NaN;
    }

    public ThirdMoment(ThirdMoment thirdMoment) throws NullArgumentException {
        copy(thirdMoment, this);
    }

    public void increment(double d) {
        if (this.f8175n < 1) {
            this.f8174m1 = 0.0d;
            this.f8177m2 = 0.0d;
            this.f8178m3 = 0.0d;
        }
        double d2 = this.f8177m2;
        super.increment(d);
        this.nDevSq = this.nDev * this.nDev;
        double d3 = (double) this.f8175n;
        Double.isNaN(d3);
        Double.isNaN(d3);
        this.f8178m3 = (this.f8178m3 - ((this.nDev * 3.0d) * d2)) + ((d3 - 1.0d) * (d3 - 2.0d) * this.nDevSq * this.dev);
    }

    public double getResult() {
        return this.f8178m3;
    }

    public void clear() {
        super.clear();
        this.f8178m3 = Double.NaN;
        this.nDevSq = Double.NaN;
    }

    public ThirdMoment copy() {
        ThirdMoment thirdMoment = new ThirdMoment();
        copy(this, thirdMoment);
        return thirdMoment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.descriptive.moment.SecondMoment.copy(org.apache.commons.math3.stat.descriptive.moment.SecondMoment, org.apache.commons.math3.stat.descriptive.moment.SecondMoment):void
     arg types: [org.apache.commons.math3.stat.descriptive.moment.ThirdMoment, org.apache.commons.math3.stat.descriptive.moment.ThirdMoment]
     candidates:
      org.apache.commons.math3.stat.descriptive.moment.FirstMoment.copy(org.apache.commons.math3.stat.descriptive.moment.FirstMoment, org.apache.commons.math3.stat.descriptive.moment.FirstMoment):void
      org.apache.commons.math3.stat.descriptive.moment.SecondMoment.copy(org.apache.commons.math3.stat.descriptive.moment.SecondMoment, org.apache.commons.math3.stat.descriptive.moment.SecondMoment):void */
    public static void copy(ThirdMoment thirdMoment, ThirdMoment thirdMoment2) throws NullArgumentException {
        MathUtils.checkNotNull(thirdMoment);
        MathUtils.checkNotNull(thirdMoment2);
        SecondMoment.copy((SecondMoment) super, (SecondMoment) super);
        thirdMoment2.f8178m3 = thirdMoment.f8178m3;
        thirdMoment2.nDevSq = thirdMoment.nDevSq;
    }
}
