package org.apache.commons.math3.p067ml.neuralnet;

/* renamed from: org.apache.commons.math3.ml.neuralnet.FeatureInitializer */
public interface FeatureInitializer {
    double value();
}
