package org.apache.commons.math3.optim.nonlinear.scalar.noderiv;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.optim.OptimizationData;
import org.apache.commons.math3.optim.PointValuePair;

public abstract class AbstractSimplex implements OptimizationData {
    private final int dimension;
    private PointValuePair[] simplex;
    private double[][] startConfiguration;

    public abstract void iterate(MultivariateFunction multivariateFunction, Comparator<PointValuePair> comparator);

    protected AbstractSimplex(int i) {
        this(i, 1.0d);
    }

    protected AbstractSimplex(int i, double d) {
        this(createHypercubeSteps(i, d));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0042, code lost:
        r1 = r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected AbstractSimplex(double[] r10) {
        /*
            r9 = this;
            r9.<init>()
            if (r10 == 0) goto L_0x004b
            int r0 = r10.length
            if (r0 == 0) goto L_0x0045
            int r0 = r10.length
            r9.dimension = r0
            int r0 = r9.dimension
            int[] r0 = new int[]{r0, r0}
            java.lang.Class<double> r1 = double.class
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r1, r0)
            double[][] r0 = (double[][]) r0
            r9.startConfiguration = r0
            r0 = 0
            r1 = 0
        L_0x001d:
            int r2 = r9.dimension
            if (r1 >= r2) goto L_0x0044
            double[][] r2 = r9.startConfiguration
            r2 = r2[r1]
            r3 = 0
        L_0x0026:
            int r4 = r1 + 1
            if (r3 >= r4) goto L_0x0042
            r4 = r10[r3]
            r6 = 0
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x0038
            int r3 = r3 + 1
            java.lang.System.arraycopy(r10, r0, r2, r0, r3)
            goto L_0x0026
        L_0x0038:
            org.apache.commons.math3.exception.ZeroException r10 = new org.apache.commons.math3.exception.ZeroException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.EQUAL_VERTICES_IN_SIMPLEX
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r10.<init>(r1, r0)
            throw r10
        L_0x0042:
            r1 = r4
            goto L_0x001d
        L_0x0044:
            return
        L_0x0045:
            org.apache.commons.math3.exception.ZeroException r10 = new org.apache.commons.math3.exception.ZeroException
            r10.<init>()
            throw r10
        L_0x004b:
            org.apache.commons.math3.exception.NullArgumentException r10 = new org.apache.commons.math3.exception.NullArgumentException
            r10.<init>()
            goto L_0x0052
        L_0x0051:
            throw r10
        L_0x0052:
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optim.nonlinear.scalar.noderiv.AbstractSimplex.<init>(double[]):void");
    }

    protected AbstractSimplex(double[][] dArr) {
        boolean z;
        if (dArr.length > 0) {
            this.dimension = dArr.length - 1;
            int i = this.dimension;
            this.startConfiguration = (double[][]) Array.newInstance(double.class, i, i);
            double[] dArr2 = dArr[0];
            int i2 = 0;
            while (i2 < dArr.length) {
                double[] dArr3 = dArr[i2];
                int length = dArr3.length;
                int i3 = this.dimension;
                if (length == i3) {
                    int i4 = 0;
                    while (i4 < i2) {
                        double[] dArr4 = dArr[i4];
                        int i5 = 0;
                        while (true) {
                            if (i5 >= this.dimension) {
                                z = true;
                                break;
                            } else if (dArr3[i5] != dArr4[i5]) {
                                z = false;
                                break;
                            } else {
                                i5++;
                            }
                        }
                        if (!z) {
                            i4++;
                        } else {
                            throw new MathIllegalArgumentException(LocalizedFormats.EQUAL_VERTICES_IN_SIMPLEX, Integer.valueOf(i2), Integer.valueOf(i4));
                        }
                    }
                    if (i2 > 0) {
                        double[] dArr5 = this.startConfiguration[i2 - 1];
                        for (int i6 = 0; i6 < this.dimension; i6++) {
                            dArr5[i6] = dArr3[i6] - dArr2[i6];
                        }
                    }
                    i2++;
                } else {
                    throw new DimensionMismatchException(dArr3.length, i3);
                }
            }
            return;
        }
        throw new NotStrictlyPositiveException(LocalizedFormats.SIMPLEX_NEED_ONE_POINT, Integer.valueOf(dArr.length));
    }

    public int getDimension() {
        return this.dimension;
    }

    public int getSize() {
        return this.simplex.length;
    }

    public void build(double[] dArr) {
        int i = this.dimension;
        if (i == dArr.length) {
            this.simplex = new PointValuePair[(i + 1)];
            this.simplex[0] = new PointValuePair(dArr, Double.NaN);
            int i2 = 0;
            while (true) {
                int i3 = this.dimension;
                if (i2 < i3) {
                    double[] dArr2 = this.startConfiguration[i2];
                    double[] dArr3 = new double[i3];
                    for (int i4 = 0; i4 < this.dimension; i4++) {
                        dArr3[i4] = dArr[i4] + dArr2[i4];
                    }
                    i2++;
                    this.simplex[i2] = new PointValuePair(dArr3, Double.NaN);
                } else {
                    return;
                }
            }
        } else {
            throw new DimensionMismatchException(i, dArr.length);
        }
    }

    public void evaluate(MultivariateFunction multivariateFunction, Comparator<PointValuePair> comparator) {
        int i = 0;
        while (true) {
            PointValuePair[] pointValuePairArr = this.simplex;
            if (i < pointValuePairArr.length) {
                PointValuePair pointValuePair = pointValuePairArr[i];
                double[] pointRef = pointValuePair.getPointRef();
                if (Double.isNaN(((Double) pointValuePair.getValue()).doubleValue())) {
                    this.simplex[i] = new PointValuePair(pointRef, multivariateFunction.value(pointRef), false);
                }
                i++;
            } else {
                Arrays.sort(pointValuePairArr, comparator);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void replaceWorstPoint(PointValuePair pointValuePair, Comparator<PointValuePair> comparator) {
        int i = 0;
        while (true) {
            int i2 = this.dimension;
            if (i < i2) {
                if (comparator.compare(this.simplex[i], pointValuePair) > 0) {
                    PointValuePair[] pointValuePairArr = this.simplex;
                    PointValuePair pointValuePair2 = pointValuePairArr[i];
                    pointValuePairArr[i] = pointValuePair;
                    pointValuePair = pointValuePair2;
                }
                i++;
            } else {
                this.simplex[i2] = pointValuePair;
                return;
            }
        }
    }

    public PointValuePair[] getPoints() {
        PointValuePair[] pointValuePairArr = this.simplex;
        PointValuePair[] pointValuePairArr2 = new PointValuePair[pointValuePairArr.length];
        System.arraycopy(pointValuePairArr, 0, pointValuePairArr2, 0, pointValuePairArr.length);
        return pointValuePairArr2;
    }

    public PointValuePair getPoint(int i) {
        if (i >= 0) {
            PointValuePair[] pointValuePairArr = this.simplex;
            if (i < pointValuePairArr.length) {
                return pointValuePairArr[i];
            }
        }
        throw new OutOfRangeException(Integer.valueOf(i), 0, Integer.valueOf(this.simplex.length - 1));
    }

    /* access modifiers changed from: protected */
    public void setPoint(int i, PointValuePair pointValuePair) {
        if (i >= 0) {
            PointValuePair[] pointValuePairArr = this.simplex;
            if (i < pointValuePairArr.length) {
                pointValuePairArr[i] = pointValuePair;
                return;
            }
        }
        throw new OutOfRangeException(Integer.valueOf(i), 0, Integer.valueOf(this.simplex.length - 1));
    }

    /* access modifiers changed from: protected */
    public void setPoints(PointValuePair[] pointValuePairArr) {
        int length = pointValuePairArr.length;
        PointValuePair[] pointValuePairArr2 = this.simplex;
        if (length == pointValuePairArr2.length) {
            this.simplex = pointValuePairArr;
            return;
        }
        throw new DimensionMismatchException(pointValuePairArr.length, pointValuePairArr2.length);
    }

    private static double[] createHypercubeSteps(int i, double d) {
        double[] dArr = new double[i];
        for (int i2 = 0; i2 < i; i2++) {
            dArr[i2] = d;
        }
        return dArr;
    }
}
