package org.apache.commons.math3.ode.nonstiff;

import org.apache.commons.math3.ode.sampling.StepInterpolator;
import org.apache.commons.math3.util.FastMath;

class LutherStepInterpolator extends RungeKuttaStepInterpolator {

    /* renamed from: Q */
    private static final double f8094Q = FastMath.sqrt(21.0d);
    private static final long serialVersionUID = 20140416;

    public LutherStepInterpolator() {
    }

    public LutherStepInterpolator(LutherStepInterpolator lutherStepInterpolator) {
        super(super);
    }

    /* access modifiers changed from: protected */
    public StepInterpolator doCopy() {
        return new LutherStepInterpolator(this);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v5, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r27v4, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r27v5, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r27v6, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r21v6, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v12, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v13, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v14, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v15, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v16, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v17, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v18, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r31v22, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r31v23, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v22, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v23, resolved type: double} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v13, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v15, resolved type: java.lang.Object[]} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void computeInterpolatedStateAndDerivatives(double r54, double r56) {
        /*
            r53 = this;
            r0 = r53
            r1 = 4626604192193052672(0x4035000000000000, double:21.0)
            double r1 = r1 * r54
            r3 = -4591560557592576000(0xc047800000000000, double:-47.0)
            double r3 = r3 + r1
            double r3 = r3 * r54
            r5 = 4630263366890291200(0x4042000000000000, double:36.0)
            double r3 = r3 + r5
            double r3 = r3 * r54
            r5 = -4601102559303067238(0xc02599999999999a, double:-10.8)
            double r3 = r3 + r5
            double r3 = r3 * r54
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r3 = r3 + r5
            r5 = 4637581716284768256(0x405c000000000000, double:112.0)
            double r5 = r5 * r54
            r7 = -4582037320880532139(0xc069555555555555, double:-202.66666666666666)
            double r7 = r7 + r5
            double r7 = r7 * r54
            r9 = 4637206416315820715(0x405aaaaaaaaaaaab, double:106.66666666666667)
            double r7 = r7 + r9
            double r7 = r7 * r54
            r9 = -4599376179445908548(0xc02bbbbbbbbbbbbc, double:-13.866666666666667)
            double r7 = r7 + r9
            double r7 = r7 * r54
            r9 = -4575173436292202496(0xc081b80000000000, double:-567.0)
            double r9 = r9 * r54
            r11 = 4617315517961601024(0x4014000000000000, double:5.0)
            double r13 = r9 / r11
            r15 = 4641043858498309325(0x40684ccccccccccd, double:194.4)
            double r13 = r13 + r15
            double r13 = r13 * r54
            r15 = -4586831777983836979(0xc0584ccccccccccd, double:-97.2)
            double r13 = r13 + r15
            double r13 = r13 * r54
            r15 = 4623485449451098604(0x4029eb851eb851ec, double:12.96)
            double r13 = r13 + r15
            double r13 = r13 * r54
            double r15 = org.apache.commons.math3.ode.nonstiff.LutherStepInterpolator.f8094Q
            r17 = 4644741736004845568(0x4075700000000000, double:343.0)
            double r19 = r15 * r17
            r21 = 4650538361306480640(0x408a080000000000, double:833.0)
            double r19 = r19 + r21
            r23 = 4639481672377565184(0x4062c00000000000, double:150.0)
            double r19 = r19 / r23
            r23 = 4644988026609467392(0x4076500000000000, double:357.0)
            double r25 = r15 * r23
            r27 = -4574557709780647936(0xc083e80000000000, double:-637.0)
            double r25 = r27 - r25
            r29 = 4629137466983448576(0x403e000000000000, double:30.0)
            double r25 = r25 / r29
            r29 = 4643756573586358272(0x4071f00000000000, double:287.0)
            double r31 = r15 * r29
            r33 = 4645603753121021952(0x4078800000000000, double:392.0)
            double r31 = r31 + r33
            r35 = 4624633867356078080(0x402e000000000000, double:15.0)
            double r31 = r31 / r35
            r35 = 4632092954238910464(0x4048800000000000, double:49.0)
            double r37 = r15 * r35
            r39 = -4591279082615865344(0xc048800000000000, double:-49.0)
            double r37 = r39 - r37
            double r37 = r37 * r54
            double r37 = r37 / r11
            double r31 = r31 + r37
            double r31 = r31 * r54
            double r25 = r25 + r31
            double r25 = r25 * r54
            double r19 = r19 + r25
            double r19 = r19 * r54
            double r25 = r15 * r17
            double r25 = r21 - r25
            r31 = 4639481672377565184(0x4062c00000000000, double:150.0)
            double r25 = r25 / r31
            double r31 = r15 * r23
            double r31 = r31 + r27
            r37 = 4629137466983448576(0x403e000000000000, double:30.0)
            double r31 = r31 / r37
            double r37 = r15 * r29
            double r37 = r33 - r37
            r41 = 4624633867356078080(0x402e000000000000, double:15.0)
            double r37 = r37 / r41
            double r15 = r15 * r35
            double r15 = r15 + r39
            double r15 = r15 * r54
            double r15 = r15 / r11
            double r37 = r37 + r15
            double r15 = r54 * r37
            double r31 = r31 + r15
            double r15 = r54 * r31
            double r25 = r25 + r15
            double r15 = r54 * r25
            r25 = 4613937818241073152(0x4008000000000000, double:3.0)
            double r25 = r25 * r54
            r31 = -4609434218613702656(0xc008000000000000, double:-3.0)
            double r25 = r25 + r31
            double r25 = r25 * r54
            r31 = 4603579539098121011(0x3fe3333333333333, double:0.6)
            double r25 = r25 + r31
            double r25 = r25 * r54
            double[] r11 = r0.previousState
            r37 = 0
            r41 = 4643985272004935680(0x4072c00000000000, double:300.0)
            r43 = 4627730092099895296(0x4039000000000000, double:25.0)
            if (r11 == 0) goto L_0x0261
            r45 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            int r11 = (r54 > r45 ? 1 : (r54 == r45 ? 0 : -1))
            if (r11 > 0) goto L_0x0261
            r45 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r47 = -4605606158930437734(0xc01599999999999a, double:-5.4)
            r49 = 4622945017495814144(0x4028000000000000, double:12.0)
            r51 = -4600567756847316992(0xc027800000000000, double:-11.75)
            r31 = 4617315517961601024(0x4014000000000000, double:5.0)
            double r1 = r1 / r31
            double r1 = r1 + r51
            double r1 = r1 * r54
            double r1 = r1 + r49
            double r1 = r1 * r54
            double r1 = r1 + r47
            double r1 = r1 * r54
            double r1 = r1 + r45
            r45 = -4603879779073279044(0xc01bbbbbbbbbbbbc, double:-6.933333333333334)
            r47 = 4630200816895466610(0x4041c71c71c71c72, double:35.55555555555556)
            r49 = -4591044520135273131(0xc049555555555555, double:-50.666666666666664)
            double r5 = r5 / r31
            double r5 = r5 + r49
            double r5 = r5 * r54
            double r5 = r5 + r47
            double r5 = r5 * r54
            double r5 = r5 + r45
            double r5 = r5 * r54
            r31 = 4618981849823728108(0x4019eb851eb851ec, double:6.48)
            r45 = -4593615324922563789(0xc040333333333333, double:-32.4)
            r47 = 4632036659243568333(0x40484ccccccccccd, double:48.6)
            double r9 = r9 / r43
            double r9 = r9 + r47
            double r9 = r9 * r54
            double r9 = r9 + r45
            double r9 = r9 * r54
            double r9 = r9 + r31
            double r9 = r9 * r54
            double r31 = org.apache.commons.math3.ode.nonstiff.LutherStepInterpolator.f8094Q
            double r45 = r31 * r17
            double r45 = r45 + r21
            double r45 = r45 / r41
            double r47 = r31 * r23
            double r47 = r27 - r47
            r49 = 4636033603912859648(0x4056800000000000, double:90.0)
            double r47 = r47 / r49
            double r49 = r31 * r29
            double r49 = r49 + r33
            r51 = 4633641066610819072(0x404e000000000000, double:60.0)
            double r49 = r49 / r51
            double r51 = r31 * r35
            double r51 = r39 - r51
            double r51 = r51 * r54
            double r51 = r51 / r43
            double r49 = r49 + r51
            double r49 = r49 * r54
            double r47 = r47 + r49
            double r47 = r47 * r54
            double r45 = r45 + r47
            double r45 = r45 * r54
            double r17 = r17 * r31
            double r21 = r21 - r17
            double r21 = r21 / r41
            double r23 = r23 * r31
            double r23 = r23 + r27
            r17 = 4636033603912859648(0x4056800000000000, double:90.0)
            double r23 = r23 / r17
            double r29 = r29 * r31
            double r33 = r33 - r29
            r17 = 4633641066610819072(0x404e000000000000, double:60.0)
            double r33 = r33 / r17
            double r31 = r31 * r35
            double r31 = r31 + r39
            double r17 = r54 * r31
            double r17 = r17 / r43
            double r33 = r33 + r17
            double r17 = r54 * r33
            double r23 = r23 + r17
            double r17 = r54 * r23
            double r21 = r21 + r17
            double r17 = r54 * r21
            r21 = 4599075939470750515(0x3fd3333333333333, double:0.3)
            r23 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r27 = 4604930618986332160(0x3fe8000000000000, double:0.75)
            double r27 = r27 * r54
            double r27 = r27 + r23
            double r23 = r54 * r27
            double r23 = r23 + r21
            double r21 = r54 * r23
            r11 = 0
        L_0x01d4:
            double[] r12 = r0.interpolatedState
            int r12 = r12.length
            if (r11 >= r12) goto L_0x03e2
            double[][] r12 = r0.yDotK
            r12 = r12[r37]
            r27 = r12[r11]
            double[][] r12 = r0.yDotK
            r23 = 1
            r12 = r12[r23]
            r29 = r12[r11]
            double[][] r12 = r0.yDotK
            r24 = 2
            r12 = r12[r24]
            r31 = r12[r11]
            double[][] r12 = r0.yDotK
            r24 = 3
            r12 = r12[r24]
            r33 = r12[r11]
            double[][] r12 = r0.yDotK
            r24 = 4
            r12 = r12[r24]
            r35 = r12[r11]
            double[][] r12 = r0.yDotK
            r24 = 5
            r12 = r12[r24]
            r38 = r12[r11]
            double[][] r12 = r0.yDotK
            r24 = 6
            r12 = r12[r24]
            r40 = r12[r11]
            double[] r12 = r0.interpolatedState
            r47 = r15
            double[] r15 = r0.previousState
            r42 = r15[r11]
            r15 = r13
            double r13 = r0.f8098h
            double r13 = r13 * r54
            double r49 = r1 * r27
            r51 = 0
            double r29 = r29 * r51
            double r49 = r49 + r29
            double r51 = r5 * r31
            double r49 = r49 + r51
            double r51 = r9 * r33
            double r49 = r49 + r51
            double r51 = r45 * r35
            double r49 = r49 + r51
            double r51 = r17 * r38
            double r49 = r49 + r51
            double r51 = r21 * r40
            double r49 = r49 + r51
            double r13 = r13 * r49
            double r42 = r42 + r13
            r12[r11] = r42
            double[] r12 = r0.interpolatedDerivatives
            double r27 = r27 * r3
            double r27 = r27 + r29
            double r31 = r31 * r7
            double r27 = r27 + r31
            double r13 = r15 * r33
            double r27 = r27 + r13
            double r35 = r35 * r19
            double r27 = r27 + r35
            double r13 = r47 * r38
            double r27 = r27 + r13
            double r40 = r40 * r25
            double r27 = r27 + r40
            r12[r11] = r27
            int r11 = r11 + 1
            r13 = r15
            r15 = r47
            goto L_0x01d4
        L_0x0261:
            r47 = r15
            r15 = r13
            r1 = 4606732058837280358(0x3fee666666666666, double:0.95)
            r5 = -4606675763841938227(0xc011cccccccccccd, double:-4.45)
            r9 = 4620186562724049715(0x401e333333333333, double:7.55)
            r11 = -4596767844661723136(0xc035000000000000, double:-21.0)
            double r11 = r11 * r54
            r13 = 4617315517961601024(0x4014000000000000, double:5.0)
            double r11 = r11 / r13
            double r11 = r11 + r9
            double r9 = r54 * r11
            double r9 = r9 + r5
            double r5 = r54 * r9
            double r5 = r5 + r1
            double r1 = r54 * r5
            r5 = -4636005456415188582(0xbfa999999999999a, double:-0.05)
            double r1 = r1 + r5
            r9 = -4623295297466831849(0xbfd6c16c16c16c17, double:-0.35555555555555557)
            r11 = -4623295297466831849(0xbfd6c16c16c16c17, double:-0.35555555555555557)
            r13 = -4603479459106401667(0xc01d27d27d27d27d, double:-7.288888888888889)
            r17 = 4628649577023816772(0x403c444444444444, double:28.266666666666666)
            r21 = -4585790320570007552(0xc05c000000000000, double:-112.0)
            double r21 = r21 * r54
            r27 = 4617315517961601024(0x4014000000000000, double:5.0)
            double r21 = r21 / r27
            double r21 = r21 + r17
            double r17 = r54 * r21
            double r17 = r17 + r13
            double r13 = r54 * r17
            double r13 = r13 + r11
            double r11 = r54 * r13
            double r11 = r11 + r9
            r9 = 4618981849823728108(0x4019eb851eb851ec, double:6.48)
            r13 = -4595382987776306708(0xc039eb851eb851ec, double:-25.92)
            r17 = 4648198600562573312(0x4081b80000000000, double:567.0)
            double r17 = r17 * r54
            double r17 = r17 / r43
            double r17 = r17 + r13
            double r13 = r54 * r17
            double r13 = r13 + r9
            double r9 = r54 * r13
            double r9 = r9 * r54
            r13 = 4657175013491736576(0x40a19c0000000000, double:2254.0)
            r17 = 4652240405306277888(0x4090140000000000, double:1029.0)
            double r21 = org.apache.commons.math3.ode.nonstiff.LutherStepInterpolator.f8094Q
            double r17 = r17 * r21
            double r17 = r17 + r13
            r13 = 4651127699538968576(0x408c200000000000, double:900.0)
            double r17 = r17 / r13
            r13 = -4569623101595189248(0xc095700000000000, double:-1372.0)
            r27 = 4650661506608791552(0x408a780000000000, double:847.0)
            double r27 = r27 * r21
            double r13 = r13 - r27
            double r13 = r13 / r41
            double r27 = r21 * r35
            double r27 = r27 + r35
            double r27 = r27 * r54
            double r27 = r27 / r43
            double r13 = r13 + r27
            double r13 = r13 * r54
            double r17 = r17 + r13
            double r13 = r54 * r17
            r17 = -4624796497342622015(0xbfd16c16c16c16c1, double:-0.2722222222222222)
            double r13 = r13 + r17
            double r13 = r13 * r54
            double r13 = r13 + r17
            r27 = 4657175013491736576(0x40a19c0000000000, double:2254.0)
            r29 = 4652240405306277888(0x4090140000000000, double:1029.0)
            double r29 = r29 * r21
            double r27 = r27 - r29
            r29 = 4651127699538968576(0x408c200000000000, double:900.0)
            double r27 = r27 / r29
            r29 = -4569623101595189248(0xc095700000000000, double:-1372.0)
            r31 = 4650661506608791552(0x408a780000000000, double:847.0)
            double r31 = r31 * r21
            double r31 = r31 + r29
            double r31 = r31 / r41
            double r21 = r21 * r35
            double r35 = r35 - r21
            double r21 = r54 * r35
            double r21 = r21 / r43
            double r31 = r31 + r21
            double r21 = r54 * r31
            double r27 = r27 + r21
            double r21 = r54 * r27
            double r21 = r21 + r17
            double r21 = r21 * r54
            double r21 = r21 + r17
            r17 = 4598175219545276416(0x3fd0000000000000, double:0.25)
            r27 = -4618441417868443648(0xbfe8000000000000, double:-0.75)
            double r27 = r27 * r54
            double r27 = r27 + r17
            double r17 = r54 * r27
            double r17 = r17 + r5
            double r17 = r17 * r54
            double r17 = r17 + r5
            r5 = 0
        L_0x035b:
            double[] r6 = r0.interpolatedState
            int r6 = r6.length
            if (r5 >= r6) goto L_0x03e2
            double[][] r6 = r0.yDotK
            r6 = r6[r37]
            r27 = r6[r5]
            double[][] r6 = r0.yDotK
            r23 = 1
            r6 = r6[r23]
            r29 = r6[r5]
            double[][] r6 = r0.yDotK
            r24 = 2
            r6 = r6[r24]
            r31 = r6[r5]
            double[][] r6 = r0.yDotK
            r24 = 3
            r6 = r6[r24]
            r33 = r6[r5]
            double[][] r6 = r0.yDotK
            r24 = 4
            r6 = r6[r24]
            r35 = r6[r5]
            double[][] r6 = r0.yDotK
            r24 = 5
            r6 = r6[r24]
            r38 = r6[r5]
            double[][] r6 = r0.yDotK
            r24 = 6
            r6 = r6[r24]
            r40 = r6[r5]
            double[] r6 = r0.interpolatedState
            r42 = r15
            double[] r15 = r0.currentState
            r44 = r15[r5]
            double r15 = r1 * r27
            r49 = 0
            double r29 = r29 * r49
            double r15 = r15 + r29
            double r49 = r11 * r31
            double r15 = r15 + r49
            double r49 = r9 * r33
            double r15 = r15 + r49
            double r49 = r13 * r35
            double r15 = r15 + r49
            double r49 = r21 * r38
            double r15 = r15 + r49
            double r49 = r17 * r40
            double r15 = r15 + r49
            double r15 = r15 * r56
            double r44 = r44 + r15
            r6[r5] = r44
            double[] r6 = r0.interpolatedDerivatives
            double r27 = r27 * r3
            double r27 = r27 + r29
            double r31 = r31 * r7
            double r27 = r27 + r31
            double r15 = r42 * r33
            double r27 = r27 + r15
            double r35 = r35 * r19
            double r27 = r27 + r35
            double r15 = r47 * r38
            double r27 = r27 + r15
            double r40 = r40 * r25
            double r27 = r27 + r40
            r6[r5] = r27
            int r5 = r5 + 1
            r15 = r42
            goto L_0x035b
        L_0x03e2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.ode.nonstiff.LutherStepInterpolator.computeInterpolatedStateAndDerivatives(double, double):void");
    }
}
