package org.apache.commons.math3.p067ml.clustering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.p067ml.clustering.Clusterable;
import org.apache.commons.math3.p067ml.distance.DistanceMeasure;
import org.apache.commons.math3.p067ml.distance.EuclideanDistance;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.stat.descriptive.moment.Variance;
import org.apache.commons.math3.util.MathUtils;

/* renamed from: org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer */
public class KMeansPlusPlusClusterer<T extends Clusterable> extends Clusterer<T> {
    private final EmptyClusterStrategy emptyStrategy;

    /* renamed from: k */
    private final int f8059k;
    private final int maxIterations;
    private final RandomGenerator random;

    /* renamed from: org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer$EmptyClusterStrategy */
    public enum EmptyClusterStrategy {
        LARGEST_VARIANCE,
        LARGEST_POINTS_NUMBER,
        FARTHEST_POINT,
        ERROR
    }

    public KMeansPlusPlusClusterer(int i) {
        this(i, -1);
    }

    public KMeansPlusPlusClusterer(int i, int i2) {
        this(i, i2, new EuclideanDistance());
    }

    public KMeansPlusPlusClusterer(int i, int i2, DistanceMeasure distanceMeasure) {
        this(i, i2, distanceMeasure, new JDKRandomGenerator());
    }

    public KMeansPlusPlusClusterer(int i, int i2, DistanceMeasure distanceMeasure, RandomGenerator randomGenerator) {
        this(i, i2, distanceMeasure, randomGenerator, EmptyClusterStrategy.LARGEST_VARIANCE);
    }

    public KMeansPlusPlusClusterer(int i, int i2, DistanceMeasure distanceMeasure, RandomGenerator randomGenerator, EmptyClusterStrategy emptyClusterStrategy) {
        super(distanceMeasure);
        this.f8059k = i;
        this.maxIterations = i2;
        this.random = randomGenerator;
        this.emptyStrategy = emptyClusterStrategy;
    }

    public int getK() {
        return this.f8059k;
    }

    public int getMaxIterations() {
        return this.maxIterations;
    }

    public RandomGenerator getRandomGenerator() {
        return this.random;
    }

    public EmptyClusterStrategy getEmptyClusterStrategy() {
        return this.emptyStrategy;
    }

    public List<CentroidCluster<T>> cluster(Collection<T> collection) throws MathIllegalArgumentException, ConvergenceException {
        Clusterable clusterable;
        MathUtils.checkNotNull(collection);
        if (collection.size() >= this.f8059k) {
            List<CentroidCluster<T>> chooseInitialCenters = chooseInitialCenters(collection);
            int[] iArr = new int[collection.size()];
            assignPointsToClusters(chooseInitialCenters, collection, iArr);
            int i = this.maxIterations;
            if (i < 0) {
                i = Integer.MAX_VALUE;
            }
            ArrayList arrayList = chooseInitialCenters;
            int i2 = 0;
            while (i2 < i) {
                ArrayList arrayList2 = new ArrayList();
                boolean z = false;
                for (CentroidCluster centroidCluster : arrayList) {
                    boolean z2 = true;
                    if (centroidCluster.getPoints().isEmpty()) {
                        int i3 = C35621.f8060x18024d6[this.emptyStrategy.ordinal()];
                        if (i3 == 1) {
                            clusterable = getPointFromLargestVarianceCluster(arrayList);
                        } else if (i3 == 2) {
                            clusterable = getPointFromLargestNumberCluster(arrayList);
                        } else if (i3 == 3) {
                            clusterable = getFarthestPoint(arrayList);
                        } else {
                            throw new ConvergenceException(LocalizedFormats.EMPTY_CLUSTER_IN_K_MEANS, new Object[0]);
                        }
                    } else {
                        z2 = z;
                        clusterable = centroidOf(centroidCluster.getPoints(), centroidCluster.getCenter().getPoint().length);
                    }
                    arrayList2.add(new CentroidCluster(clusterable));
                    z = z2;
                }
                if (assignPointsToClusters(arrayList2, collection, iArr) == 0 && !z) {
                    return arrayList2;
                }
                i2++;
                arrayList = arrayList2;
            }
            return arrayList;
        }
        throw new NumberIsTooSmallException(Integer.valueOf(collection.size()), Integer.valueOf(this.f8059k), false);
    }

    /* renamed from: org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer$1 */
    static /* synthetic */ class C35621 {

        /* renamed from: $SwitchMap$org$apache$commons$math3$ml$clustering$KMeansPlusPlusClusterer$EmptyClusterStrategy */
        static final /* synthetic */ int[] f8060x18024d6 = new int[EmptyClusterStrategy.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer$EmptyClusterStrategy[] r0 = org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.EmptyClusterStrategy.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.C35621.f8060x18024d6 = r0
                int[] r0 = org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.C35621.f8060x18024d6     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer$EmptyClusterStrategy r1 = org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.EmptyClusterStrategy.LARGEST_VARIANCE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.C35621.f8060x18024d6     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer$EmptyClusterStrategy r1 = org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.EmptyClusterStrategy.LARGEST_POINTS_NUMBER     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.C35621.f8060x18024d6     // Catch:{ NoSuchFieldError -> 0x002a }
                org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer$EmptyClusterStrategy r1 = org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.EmptyClusterStrategy.FARTHEST_POINT     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.p067ml.clustering.KMeansPlusPlusClusterer.C35621.<clinit>():void");
        }
    }

    private int assignPointsToClusters(List<CentroidCluster<T>> list, Collection<T> collection, int[] iArr) {
        int i = 0;
        int i2 = 0;
        for (T t : collection) {
            int nearestCluster = getNearestCluster(list, t);
            if (nearestCluster != iArr[i2]) {
                i++;
            }
            list.get(nearestCluster).addPoint(t);
            iArr[i2] = nearestCluster;
            i2++;
        }
        return i;
    }

    private List<CentroidCluster<T>> chooseInitialCenters(Collection<T> collection) {
        List unmodifiableList = Collections.unmodifiableList(new ArrayList(collection));
        int size = unmodifiableList.size();
        boolean[] zArr = new boolean[size];
        ArrayList arrayList = new ArrayList();
        int nextInt = this.random.nextInt(size);
        Clusterable clusterable = (Clusterable) unmodifiableList.get(nextInt);
        arrayList.add(new CentroidCluster(clusterable));
        zArr[nextInt] = true;
        double[] dArr = new double[size];
        for (int i = 0; i < size; i++) {
            if (i != nextInt) {
                double distance = distance(clusterable, (Clusterable) unmodifiableList.get(i));
                dArr[i] = distance * distance;
            }
        }
        while (arrayList.size() < this.f8059k) {
            double d = 0.0d;
            for (int i2 = 0; i2 < size; i2++) {
                if (!zArr[i2]) {
                    d += dArr[i2];
                }
            }
            double nextDouble = this.random.nextDouble() * d;
            double d2 = 0.0d;
            int i3 = 0;
            while (true) {
                if (i3 >= size) {
                    i3 = -1;
                    break;
                }
                if (!zArr[i3]) {
                    d2 += dArr[i3];
                    if (d2 >= nextDouble) {
                        break;
                    }
                }
                i3++;
            }
            if (i3 == -1) {
                int i4 = size - 1;
                while (true) {
                    if (i4 < 0) {
                        break;
                    } else if (!zArr[i4]) {
                        i3 = i4;
                        break;
                    } else {
                        i4--;
                    }
                }
            }
            if (i3 < 0) {
                break;
            }
            Clusterable clusterable2 = (Clusterable) unmodifiableList.get(i3);
            arrayList.add(new CentroidCluster(clusterable2));
            zArr[i3] = true;
            if (arrayList.size() < this.f8059k) {
                for (int i5 = 0; i5 < size; i5++) {
                    if (!zArr[i5]) {
                        double distance2 = distance(clusterable2, (Clusterable) unmodifiableList.get(i5));
                        double d3 = distance2 * distance2;
                        if (d3 < dArr[i5]) {
                            dArr[i5] = d3;
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    private T getPointFromLargestVarianceCluster(Collection<CentroidCluster<T>> collection) throws ConvergenceException {
        double d = Double.NEGATIVE_INFINITY;
        CentroidCluster centroidCluster = null;
        for (CentroidCluster centroidCluster2 : collection) {
            if (!centroidCluster2.getPoints().isEmpty()) {
                Clusterable center = centroidCluster2.getCenter();
                Variance variance = new Variance();
                for (Clusterable clusterable : centroidCluster2.getPoints()) {
                    variance.increment(distance(clusterable, center));
                }
                double result = variance.getResult();
                if (result > d) {
                    centroidCluster = centroidCluster2;
                    d = result;
                }
            }
        }
        if (centroidCluster != null) {
            List points = centroidCluster.getPoints();
            return (Clusterable) points.remove(this.random.nextInt(points.size()));
        }
        throw new ConvergenceException(LocalizedFormats.EMPTY_CLUSTER_IN_K_MEANS, new Object[0]);
    }

    private T getPointFromLargestNumberCluster(Collection<? extends Cluster<T>> collection) throws ConvergenceException {
        Cluster cluster = null;
        int i = 0;
        for (Cluster cluster2 : collection) {
            int size = cluster2.getPoints().size();
            if (size > i) {
                cluster = cluster2;
                i = size;
            }
        }
        if (cluster != null) {
            List points = cluster.getPoints();
            return (Clusterable) points.remove(this.random.nextInt(points.size()));
        }
        throw new ConvergenceException(LocalizedFormats.EMPTY_CLUSTER_IN_K_MEANS, new Object[0]);
    }

    private T getFarthestPoint(Collection<CentroidCluster<T>> collection) throws ConvergenceException {
        Iterator<CentroidCluster<T>> it = collection.iterator();
        double d = Double.NEGATIVE_INFINITY;
        CentroidCluster centroidCluster = null;
        int i = -1;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            CentroidCluster next = it.next();
            Clusterable center = next.getCenter();
            List points = next.getPoints();
            for (int i2 = 0; i2 < points.size(); i2++) {
                double distance = distance((Clusterable) points.get(i2), center);
                if (distance > d) {
                    centroidCluster = next;
                    i = i2;
                    d = distance;
                }
            }
        }
        if (centroidCluster != null) {
            return (Clusterable) centroidCluster.getPoints().remove(i);
        }
        throw new ConvergenceException(LocalizedFormats.EMPTY_CLUSTER_IN_K_MEANS, new Object[0]);
    }

    private int getNearestCluster(Collection<CentroidCluster<T>> collection, T t) {
        int i = 0;
        double d = Double.MAX_VALUE;
        int i2 = 0;
        for (CentroidCluster<T> centroidCluster : collection) {
            double distance = distance(t, centroidCluster.getCenter());
            if (distance < d) {
                i = i2;
                d = distance;
            }
            i2++;
        }
        return i;
    }

    private Clusterable centroidOf(Collection<T> collection, int i) {
        int i2;
        double[] dArr = new double[i];
        Iterator<T> it = collection.iterator();
        while (true) {
            i2 = 0;
            if (!it.hasNext()) {
                break;
            }
            double[] point = ((Clusterable) it.next()).getPoint();
            while (i2 < dArr.length) {
                dArr[i2] = dArr[i2] + point[i2];
                i2++;
            }
        }
        while (i2 < dArr.length) {
            double d = dArr[i2];
            double size = (double) collection.size();
            Double.isNaN(size);
            dArr[i2] = d / size;
            i2++;
        }
        return new DoublePoint(dArr);
    }
}
