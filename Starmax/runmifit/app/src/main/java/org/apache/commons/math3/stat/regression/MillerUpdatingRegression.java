package org.apache.commons.math3.stat.regression;

import java.util.Arrays;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Precision;

public class MillerUpdatingRegression implements UpdatingMultipleLinearRegression {

    /* renamed from: d */
    private final double[] f8188d;
    private final double epsilon;
    private boolean hasIntercept;
    private final boolean[] lindep;
    private long nobs;
    private final int nvars;

    /* renamed from: r */
    private final double[] f8189r;
    private final double[] rhs;
    private final double[] rss;
    private boolean rss_set;
    private double sserr;
    private double sumsqy;
    private double sumy;
    private final double[] tol;
    private boolean tol_set;
    private final int[] vorder;
    private final double[] work_sing;
    private final double[] work_tolset;
    private final double[] x_sing;

    private MillerUpdatingRegression() {
        this(-1, false, Double.NaN);
    }

    public MillerUpdatingRegression(int i, boolean z, double d) throws ModelSpecificationException {
        this.nobs = 0;
        this.sserr = 0.0d;
        this.rss_set = false;
        this.tol_set = false;
        this.sumy = 0.0d;
        this.sumsqy = 0.0d;
        if (i >= 1) {
            if (z) {
                this.nvars = i + 1;
            } else {
                this.nvars = i;
            }
            this.hasIntercept = z;
            this.nobs = 0;
            int i2 = this.nvars;
            this.f8188d = new double[i2];
            this.rhs = new double[i2];
            this.f8189r = new double[(((i2 - 1) * i2) / 2)];
            this.tol = new double[i2];
            this.rss = new double[i2];
            this.vorder = new int[i2];
            this.x_sing = new double[i2];
            this.work_sing = new double[i2];
            this.work_tolset = new double[i2];
            this.lindep = new boolean[i2];
            for (int i3 = 0; i3 < this.nvars; i3++) {
                this.vorder[i3] = i3;
            }
            if (d > 0.0d) {
                this.epsilon = d;
            } else {
                this.epsilon = -d;
            }
        } else {
            throw new ModelSpecificationException(LocalizedFormats.NO_REGRESSORS, new Object[0]);
        }
    }

    public MillerUpdatingRegression(int i, boolean z) throws ModelSpecificationException {
        this(i, z, Precision.EPSILON);
    }

    public boolean hasIntercept() {
        return this.hasIntercept;
    }

    public long getN() {
        return this.nobs;
    }

    public void addObservation(double[] dArr, double d) throws ModelSpecificationException {
        if ((this.hasIntercept || dArr.length == this.nvars) && (!this.hasIntercept || dArr.length + 1 == this.nvars)) {
            if (!this.hasIntercept) {
                include(MathArrays.copyOf(dArr, dArr.length), 1.0d, d);
            } else {
                double[] dArr2 = new double[(dArr.length + 1)];
                System.arraycopy(dArr, 0, dArr2, 1, dArr.length);
                dArr2[0] = 1.0d;
                include(dArr2, 1.0d, d);
            }
            this.nobs++;
            return;
        }
        throw new ModelSpecificationException(LocalizedFormats.INVALID_REGRESSION_OBSERVATION, Integer.valueOf(dArr.length), Integer.valueOf(this.nvars));
    }

    public void addObservations(double[][] dArr, double[] dArr2) throws ModelSpecificationException {
        int i = 0;
        if (dArr == null || dArr2 == null || dArr.length != dArr2.length) {
            LocalizedFormats localizedFormats = LocalizedFormats.DIMENSIONS_MISMATCH_SIMPLE;
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(dArr == null ? 0 : dArr.length);
            if (dArr2 != null) {
                i = dArr2.length;
            }
            objArr[1] = Integer.valueOf(i);
            throw new ModelSpecificationException(localizedFormats, objArr);
        } else if (dArr.length == 0) {
            throw new ModelSpecificationException(LocalizedFormats.NO_DATA, new Object[0]);
        } else if (dArr[0].length + 1 <= dArr.length) {
            while (i < dArr.length) {
                addObservation(dArr[i], dArr2[i]);
                i++;
            }
        } else {
            throw new ModelSpecificationException(LocalizedFormats.NOT_ENOUGH_DATA_FOR_NUMBER_OF_PREDICTORS, Integer.valueOf(dArr.length), Integer.valueOf(dArr[0].length));
        }
    }

    private void include(double[] dArr, double d, double d2) {
        double d3;
        double d4;
        double[] dArr2 = dArr;
        double d5 = d2;
        int i = 0;
        this.rss_set = false;
        this.sumy = smartAdd(d5, this.sumy);
        this.sumsqy = smartAdd(this.sumsqy, d5 * d5);
        double d6 = d5;
        int i2 = 0;
        double d7 = d;
        while (i < dArr2.length) {
            if (d7 != 0.0d) {
                double d8 = dArr2[i];
                if (d8 == 0.0d) {
                    i2 += (this.nvars - i) - 1;
                } else {
                    double d9 = this.f8188d[i];
                    double d10 = d7 * d8;
                    if (d9 != 0.0d) {
                        double d11 = d10 * d8;
                        d3 = smartAdd(d9, d11);
                        if (FastMath.abs(d11 / d9) > Precision.EPSILON) {
                            d7 = (d7 * d9) / d3;
                        }
                        d4 = d7;
                    } else {
                        d3 = d10 * d8;
                        d4 = 0.0d;
                    }
                    this.f8188d[i] = d3;
                    int i3 = i + 1;
                    while (i3 < this.nvars) {
                        double d12 = d4;
                        double d13 = dArr2[i3];
                        double d14 = d6;
                        dArr2[i3] = smartAdd(d13, (-d8) * this.f8189r[i2]);
                        if (d9 != 0.0d) {
                            double[] dArr3 = this.f8189r;
                            dArr3[i2] = smartAdd(dArr3[i2] * d9, d13 * d10) / d3;
                        } else {
                            this.f8189r[i2] = d13 / d8;
                        }
                        i2++;
                        i3++;
                        d4 = d12;
                        d6 = d14;
                    }
                    double d15 = d4;
                    double d16 = d6;
                    double smartAdd = smartAdd(d16, (-d8) * this.rhs[i]);
                    if (d9 != 0.0d) {
                        double[] dArr4 = this.rhs;
                        dArr4[i] = smartAdd(d9 * dArr4[i], d16 * d10) / d3;
                    } else {
                        this.rhs[i] = d16 / d8;
                    }
                    d6 = smartAdd;
                    d7 = d15;
                }
                i++;
            } else {
                return;
            }
        }
        this.sserr = smartAdd(this.sserr, d7 * d6 * d6);
    }

    private double smartAdd(double d, double d2) {
        double abs = FastMath.abs(d);
        double abs2 = FastMath.abs(d2);
        return abs > abs2 ? abs2 > abs * Precision.EPSILON ? d + d2 : d : abs > abs2 * Precision.EPSILON ? d + d2 : d2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
     arg types: [boolean[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void} */
    public void clear() {
        Arrays.fill(this.f8188d, 0.0d);
        Arrays.fill(this.rhs, 0.0d);
        Arrays.fill(this.f8189r, 0.0d);
        Arrays.fill(this.tol, 0.0d);
        Arrays.fill(this.rss, 0.0d);
        Arrays.fill(this.work_tolset, 0.0d);
        Arrays.fill(this.work_sing, 0.0d);
        Arrays.fill(this.x_sing, 0.0d);
        Arrays.fill(this.lindep, false);
        for (int i = 0; i < this.nvars; i++) {
            this.vorder[i] = i;
        }
        this.nobs = 0;
        this.sserr = 0.0d;
        this.sumy = 0.0d;
        this.sumsqy = 0.0d;
        this.rss_set = false;
        this.tol_set = false;
    }

    private void tolset() {
        double d = this.epsilon;
        for (int i = 0; i < this.nvars; i++) {
            this.work_tolset[i] = FastMath.sqrt(this.f8188d[i]);
        }
        this.tol[0] = this.work_tolset[0] * d;
        for (int i2 = 1; i2 < this.nvars; i2++) {
            double d2 = this.work_tolset[i2];
            int i3 = i2 - 1;
            for (int i4 = 0; i4 < i2; i4++) {
                d2 += FastMath.abs(this.f8189r[i3]) * this.work_tolset[i4];
                i3 += (this.nvars - i4) - 2;
            }
            this.tol[i2] = d2 * d;
        }
        this.tol_set = true;
    }

    private double[] regcf(int i) throws ModelSpecificationException {
        if (i < 1) {
            throw new ModelSpecificationException(LocalizedFormats.NO_REGRESSORS, new Object[0]);
        } else if (i <= this.nvars) {
            if (!this.tol_set) {
                tolset();
            }
            double[] dArr = new double[i];
            boolean z = false;
            for (int i2 = i - 1; i2 > -1; i2--) {
                if (FastMath.sqrt(this.f8188d[i2]) < this.tol[i2]) {
                    dArr[i2] = 0.0d;
                    this.f8188d[i2] = 0.0d;
                    z = true;
                } else {
                    dArr[i2] = this.rhs[i2];
                    int i3 = this.nvars;
                    int i4 = ((((i3 + i3) - i2) - 1) * i2) / 2;
                    for (int i5 = i2 + 1; i5 < i; i5++) {
                        dArr[i2] = smartAdd(dArr[i2], (-this.f8189r[i4]) * dArr[i5]);
                        i4++;
                    }
                }
            }
            if (z) {
                for (int i6 = 0; i6 < i; i6++) {
                    if (this.lindep[i6]) {
                        dArr[i6] = Double.NaN;
                    }
                }
            }
            return dArr;
        } else {
            throw new ModelSpecificationException(LocalizedFormats.TOO_MANY_REGRESSORS, Integer.valueOf(i), Integer.valueOf(this.nvars));
        }
    }

    private void singcheck() {
        for (int i = 0; i < this.nvars; i++) {
            this.work_sing[i] = FastMath.sqrt(this.f8188d[i]);
        }
        for (int i2 = 0; i2 < this.nvars; i2++) {
            double d = this.tol[i2];
            int i3 = i2 - 1;
            int i4 = i3;
            for (int i5 = 0; i5 < i3; i5++) {
                if (FastMath.abs(this.f8189r[i4]) * this.work_sing[i5] < d) {
                    this.f8189r[i4] = 0.0d;
                }
                i4 += (this.nvars - i5) - 2;
            }
            boolean[] zArr = this.lindep;
            zArr[i2] = false;
            if (this.work_sing[i2] < d) {
                zArr[i2] = true;
                if (i2 < this.nvars - 1) {
                    Arrays.fill(this.x_sing, 0.0d);
                    int i6 = this.nvars;
                    int i7 = ((((i6 + i6) - i2) - 1) * i2) / 2;
                    int i8 = i2 + 1;
                    while (i8 < this.nvars) {
                        double[] dArr = this.x_sing;
                        double[] dArr2 = this.f8189r;
                        dArr[i8] = dArr2[i7];
                        dArr2[i7] = 0.0d;
                        i8++;
                        i7++;
                    }
                    double[] dArr3 = this.rhs;
                    double d2 = dArr3[i2];
                    double[] dArr4 = this.f8188d;
                    double d3 = dArr4[i2];
                    dArr4[i2] = 0.0d;
                    dArr3[i2] = 0.0d;
                    include(this.x_sing, d3, d2);
                } else {
                    double d4 = this.sserr;
                    double d5 = this.f8188d[i2];
                    double[] dArr5 = this.rhs;
                    this.sserr = d4 + (d5 * dArr5[i2] * dArr5[i2]);
                }
            }
        }
    }

    /* renamed from: ss */
    private void m8170ss() {
        double d = this.sserr;
        double[] dArr = this.rss;
        int i = this.nvars;
        dArr[i - 1] = d;
        for (int i2 = i - 1; i2 > 0; i2--) {
            double d2 = this.f8188d[i2];
            double[] dArr2 = this.rhs;
            d += d2 * dArr2[i2] * dArr2[i2];
            this.rss[i2 - 1] = d;
        }
        this.rss_set = true;
    }

    private double[] cov(int i) {
        double d;
        double[] dArr;
        double d2;
        double[] dArr2;
        int i2 = i;
        if (this.nobs <= ((long) i2)) {
            return null;
        }
        int i3 = 0;
        double d3 = 0.0d;
        int i4 = 0;
        while (true) {
            d = 1.0d;
            if (i4 >= i2) {
                break;
            }
            if (!this.lindep[i4]) {
                d3 += 1.0d;
            }
            i4++;
        }
        int i5 = i2 - 1;
        double d4 = this.rss[i5];
        double d5 = (double) this.nobs;
        Double.isNaN(d5);
        double d6 = d4 / (d5 - d3);
        double[] dArr3 = new double[((i2 * i5) / 2)];
        inverse(dArr3, i2);
        double[] dArr4 = new double[(((i2 + 1) * i2) / 2)];
        Arrays.fill(dArr4, Double.NaN);
        int i6 = 0;
        while (i3 < i2) {
            if (!this.lindep[i3]) {
                int i7 = i3;
                int i8 = i6;
                while (i7 < i2) {
                    if (!this.lindep[i7]) {
                        int i9 = (i6 + i7) - i3;
                        if (i3 == i7) {
                            d2 = d / this.f8188d[i7];
                        } else {
                            d2 = dArr3[i9 - 1] / this.f8188d[i7];
                        }
                        int i10 = i7 + 1;
                        int i11 = i9;
                        int i12 = i8;
                        int i13 = i10;
                        while (i13 < i2) {
                            if (!this.lindep[i13]) {
                                dArr2 = dArr3;
                                d2 += (dArr3[i11] * dArr3[i12]) / this.f8188d[i13];
                            } else {
                                dArr2 = dArr3;
                            }
                            i11++;
                            i12++;
                            i13++;
                            dArr3 = dArr2;
                        }
                        dArr = dArr3;
                        dArr4[((i10 * i7) / 2) + i3] = d2 * d6;
                        i8 = i12;
                    } else {
                        dArr = dArr3;
                        i8 += (i2 - i7) - 1;
                    }
                    i7++;
                    dArr3 = dArr;
                    d = 1.0d;
                }
            }
            i6 += (i2 - i3) - 1;
            i3++;
            dArr3 = dArr3;
            d = 1.0d;
        }
        return dArr4;
    }

    private void inverse(double[] dArr, int i) {
        int i2 = i - 1;
        int i3 = ((i * i2) / 2) - 1;
        Arrays.fill(dArr, Double.NaN);
        while (i2 > 0) {
            if (!this.lindep[i2]) {
                int i4 = this.nvars;
                int i5 = ((i2 - 1) * ((i4 + i4) - i2)) / 2;
                int i6 = i3;
                for (int i7 = i; i7 > i2; i7--) {
                    int i8 = i5;
                    int i9 = i6;
                    double d = 0.0d;
                    for (int i10 = i2; i10 < i7 - 1; i10++) {
                        i9 += (i - i10) - 1;
                        if (!this.lindep[i10]) {
                            d += (-this.f8189r[i8]) * dArr[i9];
                        }
                        i8++;
                    }
                    dArr[i6] = d - this.f8189r[i8];
                    i6--;
                }
                i3 = i6;
            } else {
                i3 -= i - i2;
            }
            i2--;
        }
    }

    public double[] getPartialCorrelations(int i) {
        int i2;
        int i3 = i;
        int i4 = this.nvars;
        double[] dArr = new double[((((i4 - i3) + 1) * (i4 - i3)) / 2)];
        int i5 = -i3;
        int i6 = i3 + 1;
        int i7 = -i6;
        double[] dArr2 = new double[(i4 - i3)];
        double[] dArr3 = new double[((i4 - i3) - 1)];
        int i8 = ((i4 - i3) * ((i4 - i3) - 1)) / 2;
        if (i3 < -1 || i3 >= i4) {
            return null;
        }
        int i9 = (i4 - 1) - i3;
        int length = this.f8189r.length - ((i9 * (i9 + 1)) / 2);
        double[] dArr4 = this.f8188d;
        double d = 0.0d;
        if (dArr4[i3] > 0.0d) {
            dArr2[i3 + i5] = 1.0d / FastMath.sqrt(dArr4[i3]);
        }
        while (i6 < this.nvars) {
            double d2 = this.f8188d[i6];
            int i10 = ((length + i6) - 1) - i3;
            for (int i11 = i3; i11 < i6; i11++) {
                double d3 = this.f8188d[i11];
                double[] dArr5 = this.f8189r;
                d2 += d3 * dArr5[i10] * dArr5[i10];
                i10 += (this.nvars - i11) - 2;
            }
            if (d2 > 0.0d) {
                dArr2[i6 + i5] = 1.0d / FastMath.sqrt(d2);
            } else {
                dArr2[i6 + i5] = 0.0d;
            }
            i6++;
        }
        double d4 = this.sserr;
        for (int i12 = i3; i12 < this.nvars; i12++) {
            double d5 = this.f8188d[i12];
            double[] dArr6 = this.rhs;
            d4 += d5 * dArr6[i12] * dArr6[i12];
        }
        if (d4 > 0.0d) {
            d4 = 1.0d / FastMath.sqrt(d4);
        }
        int i13 = i3;
        while (i13 < this.nvars) {
            Arrays.fill(dArr3, d);
            int i14 = ((length + i13) - i3) - 1;
            double d6 = d;
            int i15 = i3;
            while (i15 < i13) {
                int i16 = i14 + 1;
                int i17 = i13 + 1;
                while (true) {
                    i2 = this.nvars;
                    if (i17 >= i2) {
                        break;
                    }
                    int i18 = i17 + i7;
                    double d7 = dArr3[i18];
                    int i19 = length;
                    double d8 = this.f8188d[i15];
                    double[] dArr7 = this.f8189r;
                    dArr3[i18] = d7 + (d8 * dArr7[i14] * dArr7[i16]);
                    i16++;
                    i17++;
                    length = i19;
                }
                d6 += this.f8188d[i15] * this.f8189r[i14] * this.rhs[i15];
                i14 += (i2 - i15) - 2;
                i15++;
                length = length;
            }
            int i20 = length;
            int i21 = i14 + 1;
            int i22 = i13 + 1;
            for (int i23 = i22; i23 < this.nvars; i23++) {
                int i24 = i23 + i7;
                dArr3[i24] = dArr3[i24] + (this.f8188d[i13] * this.f8189r[i21]);
                i21++;
                dArr[(((((i23 - 1) - i3) * (i23 - i3)) / 2) + i13) - i3] = dArr3[i24] * dArr2[i13 + i5] * dArr2[i23 + i5];
            }
            int i25 = i13 + i5;
            dArr[i25 + i8] = (d6 + (this.f8188d[i13] * this.rhs[i13])) * dArr2[i25] * d4;
            i13 = i22;
            length = i20;
            d = 0.0d;
        }
        return dArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void vmove(int r24, int r25) {
        /*
            r23 = this;
            r0 = r23
            r1 = r24
            r2 = r25
            if (r1 != r2) goto L_0x0009
            return
        L_0x0009:
            boolean r3 = r0.rss_set
            if (r3 != 0) goto L_0x0010
            r23.m8170ss()
        L_0x0010:
            r3 = 1
            if (r1 >= r2) goto L_0x0016
            int r2 = r2 - r1
            r5 = 1
            goto L_0x001c
        L_0x0016:
            int r4 = r1 + -1
            r5 = -1
            int r2 = r1 - r2
            r1 = r4
        L_0x001c:
            r6 = r1
            r1 = 0
            r7 = 0
        L_0x001f:
            if (r1 >= r2) goto L_0x0170
            int r8 = r0.nvars
            int r9 = r8 + r8
            int r9 = r9 - r6
            int r9 = r9 - r3
            int r9 = r9 * r6
            int r9 = r9 / 2
            int r8 = r8 + r9
            int r8 = r8 - r6
            int r8 = r8 - r3
            int r10 = r6 + 1
            double[] r11 = r0.f8188d
            r12 = r11[r6]
            r14 = r11[r10]
            r24 = r5
            double r4 = r0.epsilon
            int r11 = (r12 > r4 ? 1 : (r12 == r4 ? 0 : -1))
            if (r11 > 0) goto L_0x0042
            int r11 = (r14 > r4 ? 1 : (r14 == r4 ? 0 : -1))
            if (r11 <= 0) goto L_0x0124
        L_0x0042:
            double[] r4 = r0.f8189r
            r16 = r4[r9]
            double r4 = org.apache.commons.math3.util.FastMath.abs(r16)
            double r18 = org.apache.commons.math3.util.FastMath.sqrt(r12)
            double r4 = r4 * r18
            double[] r11 = r0.tol
            r18 = r11[r10]
            r20 = 0
            int r11 = (r4 > r18 ? 1 : (r4 == r18 ? 0 : -1))
            if (r11 >= 0) goto L_0x005c
            r16 = r20
        L_0x005c:
            double r4 = r0.epsilon
            int r11 = (r12 > r4 ? 1 : (r12 == r4 ? 0 : -1))
            if (r11 < 0) goto L_0x00a3
            double r4 = org.apache.commons.math3.util.FastMath.abs(r16)
            r11 = r7
            r18 = r8
            double r7 = r0.epsilon
            int r19 = (r4 > r7 ? 1 : (r4 == r7 ? 0 : -1))
            if (r19 >= 0) goto L_0x0070
            goto L_0x00a5
        L_0x0070:
            int r4 = (r14 > r7 ? 1 : (r14 == r7 ? 0 : -1))
            if (r4 >= 0) goto L_0x00a1
            double[] r4 = r0.f8188d
            double r7 = r12 * r16
            double r7 = r7 * r16
            r4[r6] = r7
            double[] r4 = r0.f8189r
            r7 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r7 = r7 / r16
            r4[r9] = r7
            int r4 = r9 + 1
        L_0x0086:
            int r5 = r0.nvars
            int r5 = r5 + r9
            int r5 = r5 - r6
            int r5 = r5 - r3
            if (r4 >= r5) goto L_0x0098
            double[] r5 = r0.f8189r
            r7 = r5[r4]
            double r7 = r7 / r16
            r5[r4] = r7
            int r4 = r4 + 1
            goto L_0x0086
        L_0x0098:
            double[] r4 = r0.rhs
            r7 = r4[r6]
            double r7 = r7 / r16
            r4[r6] = r7
            goto L_0x00d4
        L_0x00a1:
            r7 = r11
            goto L_0x00d5
        L_0x00a3:
            r18 = r8
        L_0x00a5:
            double[] r4 = r0.f8188d
            r4[r6] = r14
            r4[r10] = r12
            double[] r4 = r0.f8189r
            r4[r9] = r20
            int r4 = r6 + 2
            r8 = r18
        L_0x00b3:
            int r5 = r0.nvars
            if (r4 >= r5) goto L_0x00c8
            int r9 = r9 + 1
            double[] r5 = r0.f8189r
            r16 = r5[r9]
            r18 = r5[r8]
            r5[r9] = r18
            r5[r8] = r16
            int r8 = r8 + 1
            int r4 = r4 + 1
            goto L_0x00b3
        L_0x00c8:
            double[] r4 = r0.rhs
            r16 = r4[r6]
            r18 = r4[r10]
            r4[r6] = r18
            r4[r10] = r16
            r18 = r8
        L_0x00d4:
            r7 = 1
        L_0x00d5:
            if (r7 != 0) goto L_0x0124
            double r4 = r12 * r16
            double r19 = r4 * r16
            double r19 = r14 + r19
            double r14 = r14 / r19
            double r4 = r4 / r19
            double r12 = r12 * r14
            double[] r8 = r0.f8188d
            r8[r6] = r19
            r8[r10] = r12
            double[] r8 = r0.f8189r
            r8[r9] = r4
            int r8 = r6 + 2
        L_0x00ef:
            int r11 = r0.nvars
            if (r8 >= r11) goto L_0x010f
            int r9 = r9 + r3
            double[] r11 = r0.f8189r
            r12 = r11[r9]
            r19 = r11[r18]
            double r19 = r19 * r14
            double r21 = r4 * r12
            double r19 = r19 + r21
            r11[r9] = r19
            r19 = r11[r18]
            double r19 = r19 * r16
            double r12 = r12 - r19
            r11[r18] = r12
            int r18 = r18 + 1
            int r8 = r8 + 1
            goto L_0x00ef
        L_0x010f:
            double[] r8 = r0.rhs
            r11 = r8[r6]
            r18 = r8[r10]
            double r14 = r14 * r18
            double r4 = r4 * r11
            double r14 = r14 + r4
            r8[r6] = r14
            r4 = r8[r10]
            double r16 = r16 * r4
            double r11 = r11 - r16
            r8[r10] = r11
        L_0x0124:
            if (r6 <= 0) goto L_0x013f
            r5 = r6
            r4 = 0
        L_0x0128:
            if (r4 >= r6) goto L_0x013f
            double[] r8 = r0.f8189r
            r11 = r8[r5]
            int r9 = r5 + -1
            r13 = r8[r9]
            r8[r5] = r13
            r8[r9] = r11
            int r8 = r0.nvars
            int r8 = r8 - r4
            int r8 = r8 + -2
            int r5 = r5 + r8
            int r4 = r4 + 1
            goto L_0x0128
        L_0x013f:
            int[] r4 = r0.vorder
            r5 = r4[r6]
            r8 = r4[r10]
            r4[r6] = r8
            r4[r10] = r5
            double[] r4 = r0.tol
            r8 = r4[r6]
            r11 = r4[r10]
            r4[r6] = r11
            r4[r10] = r8
            double[] r4 = r0.rss
            r8 = r4[r10]
            double[] r5 = r0.f8188d
            r11 = r5[r10]
            double[] r5 = r0.rhs
            r13 = r5[r10]
            double r11 = r11 * r13
            r13 = r5[r10]
            double r11 = r11 * r13
            double r8 = r8 + r11
            r4[r6] = r8
            int r6 = r6 + r24
            int r1 = r1 + 1
            r5 = r24
            goto L_0x001f
        L_0x0170:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.stat.regression.MillerUpdatingRegression.vmove(int, int):void");
    }

    private int reorderRegressors(int[] iArr, int i) {
        if (iArr.length < 1 || iArr.length > (this.nvars + 1) - i) {
            return -1;
        }
        int i2 = i;
        int i3 = i2;
        while (i2 < this.nvars) {
            int i4 = this.vorder[i2];
            int i5 = 0;
            while (true) {
                if (i5 >= iArr.length) {
                    break;
                } else if (i4 != iArr[i5] || i2 <= i3) {
                    i5++;
                } else {
                    vmove(i2, i3);
                    i3++;
                    if (i3 >= iArr.length + i) {
                        return 0;
                    }
                }
            }
            i2++;
        }
        return 0;
    }

    public double getDiagonalOfHatMatrix(double[] dArr) {
        double[] dArr2 = dArr;
        int i = this.nvars;
        double[] dArr3 = new double[i];
        if (dArr2.length > i) {
            return Double.NaN;
        }
        if (this.hasIntercept) {
            double[] dArr4 = new double[(dArr2.length + 1)];
            dArr4[0] = 1.0d;
            System.arraycopy(dArr2, 0, dArr4, 1, dArr2.length);
            dArr2 = dArr4;
        }
        double d = 0.0d;
        for (int i2 = 0; i2 < dArr2.length; i2++) {
            if (FastMath.sqrt(this.f8188d[i2]) < this.tol[i2]) {
                dArr3[i2] = 0.0d;
            } else {
                double d2 = dArr2[i2];
                int i3 = i2 - 1;
                for (int i4 = 0; i4 < i2; i4++) {
                    d2 = smartAdd(d2, (-dArr3[i4]) * this.f8189r[i3]);
                    i3 += (this.nvars - i4) - 2;
                }
                dArr3[i2] = d2;
                d = smartAdd(d, (d2 * d2) / this.f8188d[i2]);
            }
        }
        return d;
    }

    public int[] getOrderOfRegressors() {
        return MathArrays.copyOf(this.vorder);
    }

    public RegressionResults regress() throws ModelSpecificationException {
        return regress(this.nvars);
    }

    public RegressionResults regress(int i) throws ModelSpecificationException {
        boolean z;
        int i2;
        int i3 = i;
        if (this.nobs <= ((long) i3)) {
            throw new ModelSpecificationException(LocalizedFormats.NOT_ENOUGH_DATA_FOR_NUMBER_OF_PREDICTORS, Long.valueOf(this.nobs), Integer.valueOf(i));
        } else if (i3 <= this.nvars) {
            tolset();
            singcheck();
            double[] regcf = regcf(i);
            m8170ss();
            double[] cov = cov(i);
            int i4 = 0;
            int i5 = 0;
            while (true) {
                boolean[] zArr = this.lindep;
                if (i4 >= zArr.length) {
                    break;
                }
                if (!zArr[i4]) {
                    i5++;
                }
                i4++;
            }
            int i6 = 0;
            while (true) {
                if (i6 >= i3) {
                    z = false;
                    break;
                } else if (this.vorder[i6] != i6) {
                    z = true;
                    break;
                } else {
                    i6++;
                }
            }
            if (!z) {
                return new RegressionResults(regcf, new double[][]{cov}, true, this.nobs, i5, this.sumy, this.sumsqy, this.sserr, this.hasIntercept, false);
            }
            double[] dArr = new double[regcf.length];
            double[] dArr2 = new double[cov.length];
            int[] iArr = new int[regcf.length];
            for (int i7 = 0; i7 < this.nvars; i7++) {
                for (int i8 = 0; i8 < i3; i8++) {
                    if (this.vorder[i8] == i7) {
                        dArr[i7] = regcf[i8];
                        iArr[i7] = i8;
                    }
                }
            }
            int i9 = 0;
            int i10 = 0;
            while (i9 < regcf.length) {
                int i11 = iArr[i9];
                int i12 = i10;
                int i13 = 0;
                while (i13 <= i9) {
                    int i14 = iArr[i13];
                    if (i11 > i14) {
                        i2 = (((i11 + 1) * i11) / 2) + i14;
                    } else {
                        i2 = ((i14 * (i14 + 1)) / 2) + i11;
                    }
                    dArr2[i12] = cov[i2];
                    i13++;
                    i12++;
                }
                i9++;
                i10 = i12;
            }
            return new RegressionResults(dArr, new double[][]{dArr2}, true, this.nobs, i5, this.sumy, this.sumsqy, this.sserr, this.hasIntercept, false);
        } else {
            throw new ModelSpecificationException(LocalizedFormats.TOO_MANY_REGRESSORS, Integer.valueOf(i), Integer.valueOf(this.nvars));
        }
    }

    public RegressionResults regress(int[] iArr) throws ModelSpecificationException {
        boolean z;
        int i;
        int[] iArr2 = iArr;
        int length = iArr2.length;
        int i2 = this.nvars;
        if (length > i2) {
            throw new ModelSpecificationException(LocalizedFormats.TOO_MANY_REGRESSORS, Integer.valueOf(iArr2.length), Integer.valueOf(this.nvars));
        } else if (this.nobs > ((long) i2)) {
            Arrays.sort(iArr);
            int i3 = 0;
            int i4 = 0;
            while (i3 < iArr2.length) {
                if (i3 < this.nvars) {
                    if (i3 > 0 && iArr2[i3] == iArr2[i3 - 1]) {
                        iArr2[i3] = -1;
                        i4++;
                    }
                    i3++;
                } else {
                    throw new ModelSpecificationException(LocalizedFormats.INDEX_LARGER_THAN_MAX, Integer.valueOf(i3), Integer.valueOf(this.nvars));
                }
            }
            if (i4 > 0) {
                int[] iArr3 = new int[(iArr2.length - i4)];
                int i5 = 0;
                for (int i6 = 0; i6 < iArr2.length; i6++) {
                    if (iArr2[i6] > -1) {
                        iArr3[i5] = iArr2[i6];
                        i5++;
                    }
                }
                iArr2 = iArr3;
            }
            reorderRegressors(iArr2, 0);
            tolset();
            singcheck();
            double[] regcf = regcf(iArr2.length);
            m8170ss();
            double[] cov = cov(iArr2.length);
            int i7 = 0;
            int i8 = 0;
            while (true) {
                boolean[] zArr = this.lindep;
                if (i7 >= zArr.length) {
                    break;
                }
                if (!zArr[i7]) {
                    i8++;
                }
                i7++;
            }
            int i9 = 0;
            while (true) {
                if (i9 >= this.nvars) {
                    z = false;
                    break;
                } else if (this.vorder[i9] != iArr2[i9]) {
                    z = true;
                    break;
                } else {
                    i9++;
                }
            }
            if (!z) {
                return new RegressionResults(regcf, new double[][]{cov}, true, this.nobs, i8, this.sumy, this.sumsqy, this.sserr, this.hasIntercept, false);
            }
            double[] dArr = new double[regcf.length];
            int[] iArr4 = new int[regcf.length];
            for (int i10 = 0; i10 < iArr2.length; i10++) {
                int i11 = 0;
                while (true) {
                    int[] iArr5 = this.vorder;
                    if (i11 >= iArr5.length) {
                        break;
                    }
                    if (iArr5[i11] == iArr2[i10]) {
                        dArr[i10] = regcf[i11];
                        iArr4[i10] = i11;
                    }
                    i11++;
                }
            }
            double[] dArr2 = new double[cov.length];
            int i12 = 0;
            int i13 = 0;
            while (i12 < regcf.length) {
                int i14 = iArr4[i12];
                int i15 = i13;
                int i16 = 0;
                while (i16 <= i12) {
                    int i17 = iArr4[i16];
                    if (i14 > i17) {
                        i = (((i14 + 1) * i14) / 2) + i17;
                    } else {
                        i = ((i17 * (i17 + 1)) / 2) + i14;
                    }
                    dArr2[i15] = cov[i];
                    i16++;
                    i15++;
                }
                i12++;
                i13 = i15;
            }
            return new RegressionResults(dArr, new double[][]{dArr2}, true, this.nobs, i8, this.sumy, this.sumsqy, this.sserr, this.hasIntercept, false);
        } else {
            throw new ModelSpecificationException(LocalizedFormats.NOT_ENOUGH_DATA_FOR_NUMBER_OF_PREDICTORS, Long.valueOf(this.nobs), Integer.valueOf(this.nvars));
        }
    }
}
