package org.apache.commons.math3.stat.interval;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.util.FastMath;

public class AgrestiCoullInterval implements BinomialConfidenceInterval {
    public ConfidenceInterval createInterval(int i, int i2, double d) {
        IntervalUtils.checkParameters(i, i2, d);
        double inverseCumulativeProbability = new NormalDistribution().inverseCumulativeProbability(1.0d - ((1.0d - d) / 2.0d));
        double pow = FastMath.pow(inverseCumulativeProbability, 2);
        double d2 = (double) i;
        Double.isNaN(d2);
        double d3 = 1.0d / (d2 + pow);
        double d4 = (double) i2;
        Double.isNaN(d4);
        double d5 = (d4 + (pow * 0.5d)) * d3;
        double sqrt = inverseCumulativeProbability * FastMath.sqrt(d3 * d5 * (1.0d - d5));
        return new ConfidenceInterval(d5 - sqrt, d5 + sqrt, d);
    }
}
