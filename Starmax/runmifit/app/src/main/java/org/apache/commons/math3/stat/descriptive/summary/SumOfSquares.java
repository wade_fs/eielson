package org.apache.commons.math3.stat.descriptive.summary;

import java.io.Serializable;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.stat.descriptive.AbstractStorelessUnivariateStatistic;
import org.apache.commons.math3.util.MathUtils;

public class SumOfSquares extends AbstractStorelessUnivariateStatistic implements Serializable {
    private static final long serialVersionUID = 1460986908574398008L;

    /* renamed from: n */
    private long f8185n;
    private double value;

    public SumOfSquares() {
        this.f8185n = 0;
        this.value = 0.0d;
    }

    public SumOfSquares(SumOfSquares sumOfSquares) throws NullArgumentException {
        copy(sumOfSquares, this);
    }

    public void increment(double d) {
        this.value += d * d;
        this.f8185n++;
    }

    public double getResult() {
        return this.value;
    }

    public long getN() {
        return this.f8185n;
    }

    public void clear() {
        this.value = 0.0d;
        this.f8185n = 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.descriptive.AbstractUnivariateStatistic.test(double[], int, int, boolean):boolean
     arg types: [double[], int, int, int]
     candidates:
      org.apache.commons.math3.stat.descriptive.AbstractUnivariateStatistic.test(double[], double[], int, int):boolean
      org.apache.commons.math3.stat.descriptive.AbstractUnivariateStatistic.test(double[], int, int, boolean):boolean */
    public double evaluate(double[] dArr, int i, int i2) throws MathIllegalArgumentException {
        if (!test(dArr, i, i2, true)) {
            return Double.NaN;
        }
        double d = 0.0d;
        for (int i3 = i; i3 < i + i2; i3++) {
            d += dArr[i3] * dArr[i3];
        }
        return d;
    }

    public SumOfSquares copy() {
        SumOfSquares sumOfSquares = new SumOfSquares();
        copy(this, sumOfSquares);
        return sumOfSquares;
    }

    public static void copy(SumOfSquares sumOfSquares, SumOfSquares sumOfSquares2) throws NullArgumentException {
        MathUtils.checkNotNull(sumOfSquares);
        MathUtils.checkNotNull(sumOfSquares2);
        sumOfSquares2.setData(sumOfSquares.getDataRef());
        sumOfSquares2.f8185n = sumOfSquares.f8185n;
        sumOfSquares2.value = sumOfSquares.value;
    }
}
