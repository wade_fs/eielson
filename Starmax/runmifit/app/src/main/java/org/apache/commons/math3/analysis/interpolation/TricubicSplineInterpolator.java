package org.apache.commons.math3.analysis.interpolation;

import java.lang.reflect.Array;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NoDataException;
import org.apache.commons.math3.exception.NonMonotonicSequenceException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.util.MathArrays;

public class TricubicSplineInterpolator implements TrivariateGridInterpolator {
    private int nextIndex(int i, int i2) {
        int i3 = i + 1;
        return i3 < i2 ? i3 : i3 - 1;
    }

    private int previousIndex(int i) {
        int i2 = i - 1;
        if (i2 >= 0) {
            return i2;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolator.interpolate(double[], double[], double[][]):org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolatingFunction
     arg types: [double[], double[], double[][]]
     candidates:
      org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolator.interpolate(double[], double[], double[][]):org.apache.commons.math3.analysis.BivariateFunction
      org.apache.commons.math3.analysis.interpolation.BivariateGridInterpolator.interpolate(double[], double[], double[][]):org.apache.commons.math3.analysis.BivariateFunction
      org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolator.interpolate(double[], double[], double[][]):org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolatingFunction */
    public TricubicSplineInterpolatingFunction interpolate(double[] dArr, double[] dArr2, double[] dArr3, double[][][] dArr4) throws NoDataException, NumberIsTooSmallException, DimensionMismatchException, NonMonotonicSequenceException {
        double[] dArr5 = dArr;
        double[] dArr6 = dArr2;
        double[] dArr7 = dArr3;
        double[][][] dArr8 = dArr4;
        Class<double> cls = double.class;
        if (dArr5.length == 0 || dArr6.length == 0 || dArr7.length == 0 || dArr8.length == 0) {
            throw new NoDataException();
        } else if (dArr5.length == dArr8.length) {
            MathArrays.checkOrder(dArr);
            MathArrays.checkOrder(dArr2);
            MathArrays.checkOrder(dArr3);
            int length = dArr5.length;
            int length2 = dArr6.length;
            int length3 = dArr7.length;
            double[][][] dArr9 = (double[][][]) Array.newInstance((Class<?>) cls, length3, length, length2);
            double[][][] dArr10 = (double[][][]) Array.newInstance((Class<?>) cls, length2, length3, length);
            int i = 0;
            while (i < length) {
                if (dArr8[i].length == length2) {
                    int i2 = 0;
                    while (i2 < length2) {
                        if (dArr8[i][i2].length == length3) {
                            for (int i3 = 0; i3 < length3; i3++) {
                                double d = dArr8[i][i2][i3];
                                dArr9[i3][i][i2] = d;
                                dArr10[i2][i3][i] = d;
                            }
                            i2++;
                        } else {
                            throw new DimensionMismatchException(dArr8[i][i2].length, length3);
                        }
                    }
                    i++;
                } else {
                    throw new DimensionMismatchException(dArr8[i].length, length2);
                }
            }
            BicubicSplineInterpolator bicubicSplineInterpolator = new BicubicSplineInterpolator();
            BicubicSplineInterpolatingFunction[] bicubicSplineInterpolatingFunctionArr = new BicubicSplineInterpolatingFunction[length];
            for (int i4 = 0; i4 < length; i4++) {
                bicubicSplineInterpolatingFunctionArr[i4] = bicubicSplineInterpolator.interpolate(dArr6, dArr7, dArr8[i4]);
            }
            BicubicSplineInterpolatingFunction[] bicubicSplineInterpolatingFunctionArr2 = new BicubicSplineInterpolatingFunction[length2];
            for (int i5 = 0; i5 < length2; i5++) {
                bicubicSplineInterpolatingFunctionArr2[i5] = bicubicSplineInterpolator.interpolate(dArr7, dArr5, dArr10[i5]);
            }
            BicubicSplineInterpolatingFunction[] bicubicSplineInterpolatingFunctionArr3 = new BicubicSplineInterpolatingFunction[length3];
            for (int i6 = 0; i6 < length3; i6++) {
                bicubicSplineInterpolatingFunctionArr3[i6] = bicubicSplineInterpolator.interpolate(dArr5, dArr6, dArr9[i6]);
            }
            double[][][] dArr11 = (double[][][]) Array.newInstance((Class<?>) cls, length, length2, length3);
            double[][][] dArr12 = (double[][][]) Array.newInstance((Class<?>) cls, length, length2, length3);
            double[][][] dArr13 = (double[][][]) Array.newInstance((Class<?>) cls, length, length2, length3);
            int i7 = 0;
            while (i7 < length3) {
                BicubicSplineInterpolatingFunction bicubicSplineInterpolatingFunction = bicubicSplineInterpolatingFunctionArr3[i7];
                BicubicSplineInterpolatingFunction[] bicubicSplineInterpolatingFunctionArr4 = bicubicSplineInterpolatingFunctionArr3;
                int i8 = 0;
                while (i8 < length) {
                    BicubicSplineInterpolatingFunction[] bicubicSplineInterpolatingFunctionArr5 = bicubicSplineInterpolatingFunctionArr;
                    BicubicSplineInterpolatingFunction[] bicubicSplineInterpolatingFunctionArr6 = bicubicSplineInterpolatingFunctionArr2;
                    double d2 = dArr5[i8];
                    int i9 = 0;
                    while (i9 < length2) {
                        Class<double> cls2 = cls;
                        double d3 = dArr6[i9];
                        dArr11[i8][i9][i7] = bicubicSplineInterpolatingFunction.partialDerivativeX(d2, d3);
                        dArr12[i8][i9][i7] = bicubicSplineInterpolatingFunction.partialDerivativeY(d2, d3);
                        dArr13[i8][i9][i7] = bicubicSplineInterpolatingFunction.partialDerivativeXY(d2, d3);
                        i9++;
                        cls = cls2;
                    }
                    i8++;
                    dArr5 = dArr;
                    bicubicSplineInterpolatingFunctionArr = bicubicSplineInterpolatingFunctionArr5;
                    bicubicSplineInterpolatingFunctionArr2 = bicubicSplineInterpolatingFunctionArr6;
                }
                i7++;
                dArr5 = dArr;
                bicubicSplineInterpolatingFunctionArr3 = bicubicSplineInterpolatingFunctionArr4;
            }
            BicubicSplineInterpolatingFunction[] bicubicSplineInterpolatingFunctionArr7 = bicubicSplineInterpolatingFunctionArr;
            BicubicSplineInterpolatingFunction[] bicubicSplineInterpolatingFunctionArr8 = bicubicSplineInterpolatingFunctionArr2;
            double[][][] dArr14 = (double[][][]) Array.newInstance((Class<?>) cls, length, length2, length3);
            double[][][] dArr15 = (double[][][]) Array.newInstance((Class<?>) cls, length, length2, length3);
            for (int i10 = 0; i10 < length; i10++) {
                BicubicSplineInterpolatingFunction bicubicSplineInterpolatingFunction2 = bicubicSplineInterpolatingFunctionArr7[i10];
                int i11 = 0;
                while (i11 < length2) {
                    double d4 = dArr6[i11];
                    double[][][] dArr16 = dArr13;
                    int i12 = 0;
                    while (i12 < length3) {
                        int i13 = length;
                        double d5 = dArr7[i12];
                        dArr14[i10][i11][i12] = bicubicSplineInterpolatingFunction2.partialDerivativeY(d4, d5);
                        dArr15[i10][i11][i12] = bicubicSplineInterpolatingFunction2.partialDerivativeXY(d4, d5);
                        i12++;
                        length2 = length2;
                        length = i13;
                    }
                    i11++;
                    dArr13 = dArr16;
                }
            }
            int i14 = length;
            int i15 = length2;
            double[][][] dArr17 = dArr13;
            double[][][] dArr18 = (double[][][]) Array.newInstance((Class<?>) cls, i14, i15, length3);
            int i16 = 0;
            while (i16 < i15) {
                BicubicSplineInterpolatingFunction bicubicSplineInterpolatingFunction3 = bicubicSplineInterpolatingFunctionArr8[i16];
                int i17 = 0;
                while (i17 < length3) {
                    double d6 = dArr7[i17];
                    double[][][] dArr19 = dArr15;
                    int i18 = 0;
                    while (i18 < i14) {
                        dArr18[i18][i16][i17] = bicubicSplineInterpolatingFunction3.partialDerivativeXY(d6, dArr[i18]);
                        i18++;
                        dArr14 = dArr14;
                    }
                    i17++;
                    dArr7 = dArr3;
                    dArr15 = dArr19;
                    dArr14 = dArr14;
                }
                i16++;
                dArr7 = dArr3;
                dArr14 = dArr14;
            }
            double[][][] dArr20 = dArr14;
            double[][][] dArr21 = dArr15;
            double[] dArr22 = dArr;
            double[][][] dArr23 = (double[][][]) Array.newInstance((Class<?>) cls, i14, i15, length3);
            int i19 = 0;
            while (i19 < i14) {
                int nextIndex = nextIndex(i19, i14);
                int previousIndex = previousIndex(i19);
                int i20 = 0;
                while (i20 < i15) {
                    int nextIndex2 = nextIndex(i20, i15);
                    int previousIndex2 = previousIndex(i20);
                    int i21 = 0;
                    while (i21 < length3) {
                        int nextIndex3 = nextIndex(i21, length3);
                        int previousIndex3 = previousIndex(i21);
                        int i22 = i15;
                        double[][][] dArr24 = dArr4;
                        dArr23[i19][i20][i21] = (((((((dArr24[nextIndex][nextIndex2][nextIndex3] - dArr24[nextIndex][previousIndex2][nextIndex3]) - dArr24[previousIndex][nextIndex2][nextIndex3]) + dArr24[previousIndex][previousIndex2][nextIndex3]) - dArr24[nextIndex][nextIndex2][previousIndex3]) + dArr24[nextIndex][previousIndex2][previousIndex3]) + dArr24[previousIndex][nextIndex2][previousIndex3]) - dArr24[previousIndex][previousIndex2][previousIndex3]) / (((dArr22[nextIndex] - dArr22[previousIndex]) * (dArr2[nextIndex2] - dArr2[previousIndex2])) * (dArr3[nextIndex3] - dArr3[previousIndex3]));
                        i21++;
                        i15 = i22;
                    }
                    i20++;
                    i15 = i15;
                }
                i19++;
                i15 = i15;
            }
            return new TricubicSplineInterpolatingFunction(dArr, dArr2, dArr3, dArr4, dArr11, dArr12, dArr20, dArr17, dArr18, dArr21, dArr23);
        } else {
            throw new DimensionMismatchException(dArr5.length, dArr8.length);
        }
    }
}
