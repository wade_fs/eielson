package org.apache.commons.math3.stat.descriptive.rank;

import java.io.Serializable;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.stat.descriptive.AbstractStorelessUnivariateStatistic;
import org.apache.commons.math3.util.MathUtils;

public class Max extends AbstractStorelessUnivariateStatistic implements Serializable {
    private static final long serialVersionUID = -5593383832225844641L;

    /* renamed from: n */
    private long f8180n;
    private double value;

    public Max() {
        this.f8180n = 0;
        this.value = Double.NaN;
    }

    public Max(Max max) throws NullArgumentException {
        copy(max, this);
    }

    public void increment(double d) {
        double d2 = this.value;
        if (d > d2 || Double.isNaN(d2)) {
            this.value = d;
        }
        this.f8180n++;
    }

    public void clear() {
        this.value = Double.NaN;
        this.f8180n = 0;
    }

    public double getResult() {
        return this.value;
    }

    public long getN() {
        return this.f8180n;
    }

    public double evaluate(double[] dArr, int i, int i2) throws MathIllegalArgumentException {
        if (!test(dArr, i, i2)) {
            return Double.NaN;
        }
        double d = dArr[i];
        for (int i3 = i; i3 < i + i2; i3++) {
            if (!Double.isNaN(dArr[i3]) && d <= dArr[i3]) {
                d = dArr[i3];
            }
        }
        return d;
    }

    public Max copy() {
        Max max = new Max();
        copy(this, max);
        return max;
    }

    public static void copy(Max max, Max max2) throws NullArgumentException {
        MathUtils.checkNotNull(max);
        MathUtils.checkNotNull(max2);
        max2.setData(max.getDataRef());
        max2.f8180n = max.f8180n;
        max2.value = max.value;
    }
}
