package org.apache.commons.math3.random;

public class Well19937c extends AbstractWell {

    /* renamed from: K */
    private static final int f8151K = 19937;

    /* renamed from: M1 */
    private static final int f8152M1 = 70;

    /* renamed from: M2 */
    private static final int f8153M2 = 179;

    /* renamed from: M3 */
    private static final int f8154M3 = 449;
    private static final long serialVersionUID = -7203498180754925124L;

    public Well19937c() {
        super(f8151K, 70, f8153M2, f8154M3);
    }

    public Well19937c(int i) {
        super((int) f8151K, 70, (int) f8153M2, (int) f8154M3, i);
    }

    public Well19937c(int[] iArr) {
        super((int) f8151K, 70, (int) f8153M2, (int) f8154M3, iArr);
    }

    public Well19937c(long j) {
        super((int) f8151K, 70, (int) f8153M2, (int) f8154M3, j);
    }

    /* access modifiers changed from: protected */
    public int next(int i) {
        int i2 = this.iRm1[this.index];
        int i3 = this.iRm2[this.index];
        int i4 = this.f8137v[this.index];
        int i5 = this.f8137v[this.f8134i1[this.index]];
        int i6 = this.f8137v[this.f8135i2[this.index]];
        int i7 = this.f8137v[this.f8136i3[this.index]];
        int i8 = (i4 ^ (i4 << 25)) ^ (i5 ^ (i5 >>> 27));
        int i9 = (i6 >>> 9) ^ ((i7 >>> 1) ^ i7);
        int i10 = i8 ^ i9;
        int i11 = (((i8 ^ (i8 << 9)) ^ ((this.f8137v[i2] & Integer.MIN_VALUE) ^ (this.f8137v[i3] & Integer.MAX_VALUE))) ^ (i9 ^ (i9 << 21))) ^ ((i10 >>> 21) ^ i10);
        this.f8137v[this.index] = i10;
        this.f8137v[i2] = i11;
        int[] iArr = this.f8137v;
        iArr[i3] = iArr[i3] & Integer.MIN_VALUE;
        this.index = i2;
        int i12 = ((i11 << 7) & -462547200) ^ i11;
        return (i12 ^ ((i12 << 15) & -1685684224)) >>> (32 - i);
    }
}
