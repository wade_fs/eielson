package org.apache.commons.math3.complex;

import java.io.Serializable;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.ZeroException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathUtils;
import org.apache.commons.math3.util.Precision;

public final class Quaternion implements Serializable {

    /* renamed from: I */
    public static final Quaternion f7968I = new Quaternion(0.0d, 1.0d, 0.0d, 0.0d);
    public static final Quaternion IDENTITY = new Quaternion(1.0d, 0.0d, 0.0d, 0.0d);

    /* renamed from: J */
    public static final Quaternion f7969J = new Quaternion(0.0d, 0.0d, 1.0d, 0.0d);

    /* renamed from: K */
    public static final Quaternion f7970K = new Quaternion(0.0d, 0.0d, 0.0d, 1.0d);
    public static final Quaternion ZERO = new Quaternion(0.0d, 0.0d, 0.0d, 0.0d);
    private static final long serialVersionUID = 20092012;

    /* renamed from: q0 */
    private final double f7971q0;

    /* renamed from: q1 */
    private final double f7972q1;

    /* renamed from: q2 */
    private final double f7973q2;

    /* renamed from: q3 */
    private final double f7974q3;

    public Quaternion(double d, double d2, double d3, double d4) {
        this.f7971q0 = d;
        this.f7972q1 = d2;
        this.f7973q2 = d3;
        this.f7974q3 = d4;
    }

    public Quaternion(double d, double[] dArr) throws DimensionMismatchException {
        if (dArr.length == 3) {
            this.f7971q0 = d;
            this.f7972q1 = dArr[0];
            this.f7973q2 = dArr[1];
            this.f7974q3 = dArr[2];
            return;
        }
        throw new DimensionMismatchException(dArr.length, 3);
    }

    public Quaternion(double[] dArr) {
        this(0.0d, dArr);
    }

    public Quaternion getConjugate() {
        return new Quaternion(this.f7971q0, -this.f7972q1, -this.f7973q2, -this.f7974q3);
    }

    public static Quaternion multiply(Quaternion quaternion, Quaternion quaternion2) {
        double q0 = quaternion.getQ0();
        double q1 = quaternion.getQ1();
        double q2 = quaternion.getQ2();
        double q3 = quaternion.getQ3();
        double q02 = quaternion2.getQ0();
        double q12 = quaternion2.getQ1();
        double q22 = quaternion2.getQ2();
        double q32 = quaternion2.getQ3();
        return new Quaternion((((q0 * q02) - (q1 * q12)) - (q2 * q22)) - (q3 * q32), (((q0 * q12) + (q1 * q02)) + (q2 * q32)) - (q3 * q22), ((q0 * q22) - (q1 * q32)) + (q2 * q02) + (q3 * q12), (((q0 * q32) + (q1 * q22)) - (q2 * q12)) + (q3 * q02));
    }

    public Quaternion multiply(Quaternion quaternion) {
        return multiply(this, quaternion);
    }

    public static Quaternion add(Quaternion quaternion, Quaternion quaternion2) {
        return new Quaternion(quaternion.getQ0() + quaternion2.getQ0(), quaternion.getQ1() + quaternion2.getQ1(), quaternion.getQ2() + quaternion2.getQ2(), quaternion.getQ3() + quaternion2.getQ3());
    }

    public Quaternion add(Quaternion quaternion) {
        return add(this, quaternion);
    }

    public static Quaternion subtract(Quaternion quaternion, Quaternion quaternion2) {
        return new Quaternion(quaternion.getQ0() - quaternion2.getQ0(), quaternion.getQ1() - quaternion2.getQ1(), quaternion.getQ2() - quaternion2.getQ2(), quaternion.getQ3() - quaternion2.getQ3());
    }

    public Quaternion subtract(Quaternion quaternion) {
        return subtract(this, quaternion);
    }

    public static double dotProduct(Quaternion quaternion, Quaternion quaternion2) {
        return (quaternion.getQ0() * quaternion2.getQ0()) + (quaternion.getQ1() * quaternion2.getQ1()) + (quaternion.getQ2() * quaternion2.getQ2()) + (quaternion.getQ3() * quaternion2.getQ3());
    }

    public double dotProduct(Quaternion quaternion) {
        return dotProduct(this, quaternion);
    }

    public double getNorm() {
        double d = this.f7971q0;
        double d2 = this.f7972q1;
        double d3 = (d * d) + (d2 * d2);
        double d4 = this.f7973q2;
        double d5 = d3 + (d4 * d4);
        double d6 = this.f7974q3;
        return FastMath.sqrt(d5 + (d6 * d6));
    }

    public Quaternion normalize() {
        double norm = getNorm();
        if (norm >= Precision.SAFE_MIN) {
            return new Quaternion(this.f7971q0 / norm, this.f7972q1 / norm, this.f7973q2 / norm, this.f7974q3 / norm);
        }
        throw new ZeroException(LocalizedFormats.NORM, Double.valueOf(norm));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Quaternion)) {
            return false;
        }
        Quaternion quaternion = (Quaternion) obj;
        if (this.f7971q0 == quaternion.getQ0() && this.f7972q1 == quaternion.getQ1() && this.f7973q2 == quaternion.getQ2() && this.f7974q3 == quaternion.getQ3()) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 17;
        for (double d : new double[]{this.f7971q0, this.f7972q1, this.f7973q2, this.f7974q3}) {
            i = (i * 31) + MathUtils.hash(d);
        }
        return i;
    }

    public boolean equals(Quaternion quaternion, double d) {
        return Precision.equals(this.f7971q0, quaternion.getQ0(), d) && Precision.equals(this.f7972q1, quaternion.getQ1(), d) && Precision.equals(this.f7973q2, quaternion.getQ2(), d) && Precision.equals(this.f7974q3, quaternion.getQ3(), d);
    }

    public boolean isUnitQuaternion(double d) {
        return Precision.equals(getNorm(), 1.0d, d);
    }

    public boolean isPureQuaternion(double d) {
        return FastMath.abs(getQ0()) <= d;
    }

    public Quaternion getPositivePolarForm() {
        if (getQ0() >= 0.0d) {
            return normalize();
        }
        Quaternion normalize = normalize();
        return new Quaternion(-normalize.getQ0(), -normalize.getQ1(), -normalize.getQ2(), -normalize.getQ3());
    }

    public Quaternion getInverse() {
        double d = this.f7971q0;
        double d2 = this.f7972q1;
        double d3 = (d * d) + (d2 * d2);
        double d4 = this.f7973q2;
        double d5 = d3 + (d4 * d4);
        double d6 = this.f7974q3;
        double d7 = d5 + (d6 * d6);
        if (d7 >= Precision.SAFE_MIN) {
            return new Quaternion(this.f7971q0 / d7, (-this.f7972q1) / d7, (-this.f7973q2) / d7, (-this.f7974q3) / d7);
        }
        throw new ZeroException(LocalizedFormats.NORM, Double.valueOf(d7));
    }

    public double getQ0() {
        return this.f7971q0;
    }

    public double getQ1() {
        return this.f7972q1;
    }

    public double getQ2() {
        return this.f7973q2;
    }

    public double getQ3() {
        return this.f7974q3;
    }

    public double getScalarPart() {
        return getQ0();
    }

    public double[] getVectorPart() {
        return new double[]{getQ1(), getQ2(), getQ3()};
    }

    public Quaternion multiply(double d) {
        return new Quaternion(d * this.f7971q0, this.f7972q1 * d, this.f7973q2 * d, this.f7974q3 * d);
    }

    public String toString() {
        return "[" + this.f7971q0 + " " + this.f7972q1 + " " + this.f7973q2 + " " + this.f7974q3 + "]";
    }
}
