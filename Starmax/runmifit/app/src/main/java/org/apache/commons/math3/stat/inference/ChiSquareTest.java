package org.apache.commons.math3.stat.inference;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.ZeroException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;

public class ChiSquareTest {
    public double chiSquare(double[] dArr, long[] jArr) throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException {
        boolean z;
        double d;
        double d2;
        double[] dArr2 = dArr;
        long[] jArr2 = jArr;
        if (dArr2.length < 2) {
            throw new DimensionMismatchException(dArr2.length, 2);
        } else if (dArr2.length == jArr2.length) {
            MathArrays.checkPositive(dArr);
            MathArrays.checkNonNegative(jArr);
            double d3 = 0.0d;
            double d4 = 0.0d;
            double d5 = 0.0d;
            for (int i = 0; i < jArr2.length; i++) {
                d4 += dArr2[i];
                double d6 = (double) jArr2[i];
                Double.isNaN(d6);
                d5 += d6;
            }
            double d7 = 1.0d;
            if (FastMath.abs(d4 - d5) > 1.0E-5d) {
                d7 = d5 / d4;
                z = true;
            } else {
                z = false;
            }
            for (int i2 = 0; i2 < jArr2.length; i2++) {
                if (z) {
                    double d8 = (double) jArr2[i2];
                    Double.isNaN(d8);
                    double d9 = d8 - (dArr2[i2] * d7);
                    d2 = d9 * d9;
                    d = dArr2[i2] * d7;
                } else {
                    double d10 = (double) jArr2[i2];
                    double d11 = dArr2[i2];
                    Double.isNaN(d10);
                    double d12 = d10 - d11;
                    d2 = d12 * d12;
                    d = dArr2[i2];
                }
                d3 += d2 / d;
            }
            return d3;
        } else {
            throw new DimensionMismatchException(dArr2.length, jArr2.length);
        }
    }

    public double chiSquareTest(double[] dArr, long[] jArr) throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException, MaxCountExceededException {
        double length = (double) dArr.length;
        Double.isNaN(length);
        return 1.0d - new ChiSquaredDistribution(length - 1.0d).cumulativeProbability(chiSquare(dArr, jArr));
    }

    public boolean chiSquareTest(double[] dArr, long[] jArr, double d) throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException, OutOfRangeException, MaxCountExceededException {
        if (d > 0.0d && d <= 0.5d) {
            return chiSquareTest(dArr, jArr) < d;
        }
        throw new OutOfRangeException(LocalizedFormats.OUT_OF_BOUND_SIGNIFICANCE_LEVEL, Double.valueOf(d), 0, Double.valueOf(0.5d));
    }

    public double chiSquare(long[][] jArr) throws NullArgumentException, NotPositiveException, DimensionMismatchException {
        long[][] jArr2 = jArr;
        checkArray(jArr);
        int length = jArr2.length;
        int length2 = jArr2[0].length;
        double[] dArr = new double[length];
        double[] dArr2 = new double[length2];
        int i = 0;
        double d = 0.0d;
        while (i < length) {
            double d2 = d;
            for (int i2 = 0; i2 < length2; i2++) {
                double d3 = dArr[i];
                double d4 = (double) jArr2[i][i2];
                Double.isNaN(d4);
                dArr[i] = d3 + d4;
                double d5 = dArr2[i2];
                double d6 = (double) jArr2[i][i2];
                Double.isNaN(d6);
                dArr2[i2] = d5 + d6;
                double d7 = (double) jArr2[i][i2];
                Double.isNaN(d7);
                d2 += d7;
            }
            i++;
            d = d2;
        }
        double d8 = 0.0d;
        for (int i3 = 0; i3 < length; i3++) {
            int i4 = 0;
            while (i4 < length2) {
                double d9 = (dArr[i3] * dArr2[i4]) / d;
                double d10 = (double) jArr2[i3][i4];
                Double.isNaN(d10);
                double d11 = (double) jArr2[i3][i4];
                Double.isNaN(d11);
                d8 += ((d10 - d9) * (d11 - d9)) / d9;
                i4++;
                length2 = length2;
            }
        }
        return d8;
    }

    public double chiSquareTest(long[][] jArr) throws NullArgumentException, DimensionMismatchException, NotPositiveException, MaxCountExceededException {
        checkArray(jArr);
        double length = (double) jArr.length;
        Double.isNaN(length);
        double length2 = (double) jArr[0].length;
        Double.isNaN(length2);
        return 1.0d - new ChiSquaredDistribution((length - 1.0d) * (length2 - 1.0d)).cumulativeProbability(chiSquare(jArr));
    }

    public boolean chiSquareTest(long[][] jArr, double d) throws NullArgumentException, DimensionMismatchException, NotPositiveException, OutOfRangeException, MaxCountExceededException {
        if (d > 0.0d && d <= 0.5d) {
            return chiSquareTest(jArr) < d;
        }
        throw new OutOfRangeException(LocalizedFormats.OUT_OF_BOUND_SIGNIFICANCE_LEVEL, Double.valueOf(d), 0, Double.valueOf(0.5d));
    }

    public double chiSquareDataSetsComparison(long[] jArr, long[] jArr2) throws DimensionMismatchException, NotPositiveException, ZeroException {
        double d;
        double d2;
        long[] jArr3 = jArr;
        long[] jArr4 = jArr2;
        if (jArr3.length < 2) {
            throw new DimensionMismatchException(jArr3.length, 2);
        } else if (jArr3.length == jArr4.length) {
            MathArrays.checkNonNegative(jArr);
            MathArrays.checkNonNegative(jArr2);
            char c = 0;
            long j = 0;
            long j2 = 0;
            long j3 = 0;
            for (int i = 0; i < jArr3.length; i++) {
                j2 += jArr3[i];
                j3 += jArr4[i];
            }
            if (j2 == 0 || j3 == 0) {
                throw new ZeroException();
            }
            boolean z = j2 != j3;
            double d3 = 0.0d;
            if (z) {
                double d4 = (double) j2;
                double d5 = (double) j3;
                Double.isNaN(d4);
                Double.isNaN(d5);
                d = FastMath.sqrt(d4 / d5);
            } else {
                d = 0.0d;
            }
            int i2 = 0;
            while (i2 < jArr3.length) {
                if (jArr3[i2] == j && jArr4[i2] == j) {
                    LocalizedFormats localizedFormats = LocalizedFormats.OBSERVED_COUNTS_BOTTH_ZERO_FOR_ENTRY;
                    Object[] objArr = new Object[1];
                    objArr[c] = Integer.valueOf(i2);
                    throw new ZeroException(localizedFormats, objArr);
                }
                double d6 = (double) jArr3[i2];
                double d7 = (double) jArr4[i2];
                if (z) {
                    Double.isNaN(d6);
                    Double.isNaN(d7);
                    d2 = (d6 / d) - (d7 * d);
                } else {
                    Double.isNaN(d6);
                    Double.isNaN(d7);
                    d2 = d6 - d7;
                }
                Double.isNaN(d6);
                Double.isNaN(d7);
                d3 += (d2 * d2) / (d6 + d7);
                i2++;
                c = 0;
                j = 0;
            }
            return d3;
        } else {
            throw new DimensionMismatchException(jArr3.length, jArr4.length);
        }
    }

    public double chiSquareTestDataSetsComparison(long[] jArr, long[] jArr2) throws DimensionMismatchException, NotPositiveException, ZeroException, MaxCountExceededException {
        double length = (double) jArr.length;
        Double.isNaN(length);
        return 1.0d - new ChiSquaredDistribution(length - 1.0d).cumulativeProbability(chiSquareDataSetsComparison(jArr, jArr2));
    }

    public boolean chiSquareTestDataSetsComparison(long[] jArr, long[] jArr2, double d) throws DimensionMismatchException, NotPositiveException, ZeroException, OutOfRangeException, MaxCountExceededException {
        if (d > 0.0d && d <= 0.5d) {
            return chiSquareTestDataSetsComparison(jArr, jArr2) < d;
        }
        throw new OutOfRangeException(LocalizedFormats.OUT_OF_BOUND_SIGNIFICANCE_LEVEL, Double.valueOf(d), 0, Double.valueOf(0.5d));
    }

    private void checkArray(long[][] jArr) throws NullArgumentException, DimensionMismatchException, NotPositiveException {
        if (jArr.length < 2) {
            throw new DimensionMismatchException(jArr.length, 2);
        } else if (jArr[0].length >= 2) {
            MathArrays.checkRectangular(jArr);
            MathArrays.checkNonNegative(jArr);
        } else {
            throw new DimensionMismatchException(jArr[0].length, 2);
        }
    }
}
