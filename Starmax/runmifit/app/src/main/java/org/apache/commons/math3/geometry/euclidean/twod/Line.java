package org.apache.commons.math3.geometry.euclidean.twod;

import java.awt.geom.AffineTransform;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.geometry.Point;
import org.apache.commons.math3.geometry.Vector;
import org.apache.commons.math3.geometry.euclidean.oned.Euclidean1D;
import org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet;
import org.apache.commons.math3.geometry.euclidean.oned.OrientedPoint;
import org.apache.commons.math3.geometry.euclidean.oned.Vector1D;
import org.apache.commons.math3.geometry.partitioning.Embedding;
import org.apache.commons.math3.geometry.partitioning.Hyperplane;
import org.apache.commons.math3.geometry.partitioning.SubHyperplane;
import org.apache.commons.math3.geometry.partitioning.Transform;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathUtils;

public class Line implements Hyperplane<Euclidean2D>, Embedding<Euclidean2D, Euclidean1D> {
    private static final double DEFAULT_TOLERANCE = 1.0E-10d;
    private double angle;
    /* access modifiers changed from: private */
    public double cos;
    /* access modifiers changed from: private */
    public double originOffset;
    /* access modifiers changed from: private */
    public double sin;
    /* access modifiers changed from: private */
    public final double tolerance;

    public Line(Vector2D vector2D, Vector2D vector2D2, double d) {
        reset(vector2D, vector2D2);
        this.tolerance = d;
    }

    public Line(Vector2D vector2D, double d, double d2) {
        reset(vector2D, d);
        this.tolerance = d2;
    }

    private Line(double d, double d2, double d3, double d4, double d5) {
        this.angle = d;
        this.cos = d2;
        this.sin = d3;
        this.originOffset = d4;
        this.tolerance = d5;
    }

    @Deprecated
    public Line(Vector2D vector2D, Vector2D vector2D2) {
        this(vector2D, vector2D2, 1.0E-10d);
    }

    @Deprecated
    public Line(Vector2D vector2D, double d) {
        this(vector2D, d, 1.0E-10d);
    }

    public Line(Line line) {
        this.angle = MathUtils.normalizeAngle(line.angle, 3.141592653589793d);
        this.cos = FastMath.cos(this.angle);
        this.sin = FastMath.sin(this.angle);
        this.originOffset = line.originOffset;
        this.tolerance = line.tolerance;
    }

    public Line copySelf() {
        return new Line(this);
    }

    public void reset(Vector2D vector2D, Vector2D vector2D2) {
        double x = vector2D2.getX() - vector2D.getX();
        double y = vector2D2.getY() - vector2D.getY();
        double hypot = FastMath.hypot(x, y);
        if (hypot == 0.0d) {
            this.angle = 0.0d;
            this.cos = 1.0d;
            this.sin = 0.0d;
            this.originOffset = vector2D.getY();
            return;
        }
        this.angle = FastMath.atan2(-y, -x) + 3.141592653589793d;
        this.cos = FastMath.cos(this.angle);
        this.sin = FastMath.sin(this.angle);
        this.originOffset = ((vector2D2.getX() * vector2D.getY()) - (vector2D.getX() * vector2D2.getY())) / hypot;
    }

    public void reset(Vector2D vector2D, double d) {
        this.angle = MathUtils.normalizeAngle(d, 3.141592653589793d);
        this.cos = FastMath.cos(this.angle);
        this.sin = FastMath.sin(this.angle);
        this.originOffset = (this.cos * vector2D.getY()) - (this.sin * vector2D.getX());
    }

    public void revertSelf() {
        double d = this.angle;
        if (d < 3.141592653589793d) {
            this.angle = d + 3.141592653589793d;
        } else {
            this.angle = d - 3.141592653589793d;
        }
        this.cos = -this.cos;
        this.sin = -this.sin;
        this.originOffset = -this.originOffset;
    }

    public Line getReverse() {
        double d = this.angle;
        return new Line(d < 3.141592653589793d ? d + 3.141592653589793d : d - 3.141592653589793d, -this.cos, -this.sin, -this.originOffset, this.tolerance);
    }

    public Vector1D toSubSpace(Vector<Euclidean2D> vector) {
        return toSubSpace((Point<Euclidean2D>) vector);
    }

    public Vector2D toSpace(Vector<Euclidean1D> vector) {
        return toSpace((Point<Euclidean1D>) vector);
    }

    public Vector1D toSubSpace(Point<Euclidean2D> point) {
        Vector2D vector2D = (Vector2D) point;
        return new Vector1D((this.cos * vector2D.getX()) + (this.sin * vector2D.getY()));
    }

    public Vector2D toSpace(Point<Euclidean1D> point) {
        double x = ((Vector1D) point).getX();
        double d = this.cos;
        double d2 = this.originOffset;
        double d3 = this.sin;
        return new Vector2D((x * d) - (d2 * d3), (x * d3) + (d2 * d));
    }

    public Vector2D intersection(Line line) {
        double d = (this.sin * line.cos) - (line.sin * this.cos);
        if (FastMath.abs(d) < this.tolerance) {
            return null;
        }
        double d2 = this.cos;
        double d3 = line.originOffset;
        double d4 = line.cos;
        double d5 = this.originOffset;
        return new Vector2D(((d2 * d3) - (d4 * d5)) / d, ((this.sin * d3) - (line.sin * d5)) / d);
    }

    public Point<Euclidean2D> project(Point<Euclidean2D> point) {
        return toSpace((Vector<Euclidean1D>) toSubSpace(point));
    }

    public double getTolerance() {
        return this.tolerance;
    }

    public SubLine wholeHyperplane() {
        return new SubLine(this, new IntervalsSet(this.tolerance));
    }

    public PolygonsSet wholeSpace() {
        return new PolygonsSet(this.tolerance);
    }

    public double getOffset(Line line) {
        double d = this.originOffset;
        int i = (((this.cos * line.cos) + (this.sin * line.sin)) > 0.0d ? 1 : (((this.cos * line.cos) + (this.sin * line.sin)) == 0.0d ? 0 : -1));
        double d2 = line.originOffset;
        if (i > 0) {
            d2 = -d2;
        }
        return d + d2;
    }

    public double getOffset(Vector<Euclidean2D> vector) {
        return getOffset((Point<Euclidean2D>) vector);
    }

    public double getOffset(Point<Euclidean2D> point) {
        Vector2D vector2D = (Vector2D) point;
        return ((this.sin * vector2D.getX()) - (this.cos * vector2D.getY())) + this.originOffset;
    }

    public boolean sameOrientationAs(Hyperplane<Euclidean2D> hyperplane) {
        Line line = (Line) hyperplane;
        return (this.sin * line.sin) + (this.cos * line.cos) >= 0.0d;
    }

    public Vector2D getPointAt(Vector1D vector1D, double d) {
        double x = vector1D.getX();
        double d2 = d - this.originOffset;
        double d3 = this.cos;
        double d4 = this.sin;
        return new Vector2D((x * d3) + (d2 * d4), (x * d4) - (d2 * d3));
    }

    public boolean contains(Vector2D vector2D) {
        return FastMath.abs(getOffset(vector2D)) < this.tolerance;
    }

    public double distance(Vector2D vector2D) {
        return FastMath.abs(getOffset((Vector<Euclidean2D>) vector2D));
    }

    public boolean isParallelTo(Line line) {
        return FastMath.abs((this.sin * line.cos) - (this.cos * line.sin)) < this.tolerance;
    }

    public void translateToPoint(Vector2D vector2D) {
        this.originOffset = (this.cos * vector2D.getY()) - (this.sin * vector2D.getX());
    }

    public double getAngle() {
        return MathUtils.normalizeAngle(this.angle, 3.141592653589793d);
    }

    public void setAngle(double d) {
        this.angle = MathUtils.normalizeAngle(d, 3.141592653589793d);
        this.cos = FastMath.cos(this.angle);
        this.sin = FastMath.sin(this.angle);
    }

    public double getOriginOffset() {
        return this.originOffset;
    }

    public void setOriginOffset(double d) {
        this.originOffset = d;
    }

    public static Transform<Euclidean2D, Euclidean1D> getTransform(AffineTransform affineTransform) throws MathIllegalArgumentException {
        return new LineTransform(affineTransform);
    }

    private static class LineTransform implements Transform<Euclidean2D, Euclidean1D> {
        private double c11;
        private double c1X;
        private double c1Y;
        private double cX1;
        private double cXX;
        private double cXY;
        private double cY1;
        private double cYX;
        private double cYY;

        public LineTransform(AffineTransform affineTransform) throws MathIllegalArgumentException {
            double[] dArr = new double[6];
            affineTransform.getMatrix(dArr);
            this.cXX = dArr[0];
            this.cXY = dArr[2];
            this.cX1 = dArr[4];
            this.cYX = dArr[1];
            this.cYY = dArr[3];
            this.cY1 = dArr[5];
            double d = this.cXY;
            double d2 = this.cY1;
            double d3 = this.cYY;
            double d4 = this.cX1;
            this.c1Y = (d * d2) - (d3 * d4);
            double d5 = this.cXX;
            double d6 = this.cYX;
            this.c1X = (d2 * d5) - (d4 * d6);
            this.c11 = (d5 * d3) - (d6 * d);
            if (FastMath.abs(this.c11) < 1.0E-20d) {
                throw new MathIllegalArgumentException(LocalizedFormats.NON_INVERTIBLE_TRANSFORM, new Object[0]);
            }
        }

        public Vector2D apply(Point<Euclidean2D> point) {
            Vector2D vector2D = (Vector2D) point;
            double x = vector2D.getX();
            double y = vector2D.getY();
            return new Vector2D((this.cXX * x) + (this.cXY * y) + this.cX1, (this.cYX * x) + (this.cYY * y) + this.cY1);
        }

        public Line apply(Hyperplane<Euclidean2D> hyperplane) {
            Line line = (Line) hyperplane;
            double access$000 = (this.c1X * line.cos) + (this.c1Y * line.sin) + (this.c11 * line.originOffset);
            double access$0002 = (this.cXX * line.cos) + (this.cXY * line.sin);
            double access$0003 = (this.cYX * line.cos) + (this.cYY * line.sin);
            double sqrt = 1.0d / FastMath.sqrt((access$0003 * access$0003) + (access$0002 * access$0002));
            return new Line(FastMath.atan2(-access$0003, -access$0002) + 3.141592653589793d, sqrt * access$0002, sqrt * access$0003, sqrt * access$000, line.tolerance);
        }

        public SubHyperplane<Euclidean1D> apply(SubHyperplane<Euclidean1D> subHyperplane, Hyperplane<Euclidean2D> hyperplane, Hyperplane<Euclidean2D> hyperplane2) {
            OrientedPoint orientedPoint = (OrientedPoint) subHyperplane.getHyperplane();
            Line line = (Line) hyperplane;
            return new OrientedPoint(((Line) hyperplane2).toSubSpace((Vector<Euclidean2D>) apply((Point<Euclidean2D>) line.toSpace((Vector<Euclidean1D>) orientedPoint.getLocation()))), orientedPoint.isDirect(), line.tolerance).wholeHyperplane();
        }
    }
}
