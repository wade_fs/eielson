package org.apache.commons.math3.linear;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.util.ExceptionContext;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.IterationManager;
import org.apache.commons.math3.util.MathUtils;

public class SymmLQ extends PreconditionedIterativeLinearSolver {
    private static final String OPERATOR = "operator";
    private static final String THRESHOLD = "threshold";
    private static final String VECTOR = "vector";
    private static final String VECTOR1 = "vector1";
    private static final String VECTOR2 = "vector2";
    private final boolean check;
    private final double delta;

    private static class State {
        static final double CBRT_MACH_PREC = FastMath.cbrt(MACH_PREC);
        static final double MACH_PREC = FastMath.ulp(1.0d);

        /* renamed from: a */
        private final RealLinearOperator f8050a;

        /* renamed from: b */
        private final RealVector f8051b;
        private boolean bIsNull;
        private double beta;
        private double beta1;
        private double bstep;
        private double cgnorm;
        private final boolean check;
        private double dbar;
        private final double delta;
        private double gammaZeta;
        private double gbar;
        private double gmax;
        private double gmin;
        private final boolean goodb;
        private boolean hasConverged;
        private double lqnorm;

        /* renamed from: m */
        private final RealLinearOperator f8052m;

        /* renamed from: mb */
        private final RealVector f8053mb;
        private double minusEpsZeta;
        private double oldb;

        /* renamed from: r1 */
        private RealVector f8054r1;

        /* renamed from: r2 */
        private RealVector f8055r2;
        private double rnorm;
        private final double shift;
        private double snprod;
        private double tnorm;
        private RealVector wbar;

        /* renamed from: xL */
        private final RealVector f8056xL;

        /* renamed from: y */
        private RealVector f8057y;
        private double ynorm2;

        public State(RealLinearOperator realLinearOperator, RealLinearOperator realLinearOperator2, RealVector realVector, boolean z, double d, double d2, boolean z2) {
            this.f8050a = realLinearOperator;
            this.f8052m = realLinearOperator2;
            this.f8051b = realVector;
            this.f8056xL = new ArrayRealVector(realVector.getDimension());
            this.goodb = z;
            this.shift = d;
            this.f8053mb = realLinearOperator2 != null ? realLinearOperator2.operate(realVector) : realVector;
            this.hasConverged = false;
            this.check = z2;
            this.delta = d2;
        }

        private static void checkSymmetry(RealLinearOperator realLinearOperator, RealVector realVector, RealVector realVector2, RealVector realVector3) throws NonSelfAdjointOperatorException {
            double dotProduct = realVector2.dotProduct(realVector2);
            double dotProduct2 = realVector.dotProduct(realVector3);
            double d = (MACH_PREC + dotProduct) * CBRT_MACH_PREC;
            if (FastMath.abs(dotProduct - dotProduct2) > d) {
                NonSelfAdjointOperatorException nonSelfAdjointOperatorException = new NonSelfAdjointOperatorException();
                ExceptionContext context = nonSelfAdjointOperatorException.getContext();
                context.setValue("operator", realLinearOperator);
                context.setValue(SymmLQ.VECTOR1, realVector);
                context.setValue(SymmLQ.VECTOR2, realVector2);
                context.setValue(SymmLQ.THRESHOLD, Double.valueOf(d));
                throw nonSelfAdjointOperatorException;
            }
        }

        private static void throwNPDLOException(RealLinearOperator realLinearOperator, RealVector realVector) throws NonPositiveDefiniteOperatorException {
            NonPositiveDefiniteOperatorException nonPositiveDefiniteOperatorException = new NonPositiveDefiniteOperatorException();
            ExceptionContext context = nonPositiveDefiniteOperatorException.getContext();
            context.setValue("operator", realLinearOperator);
            context.setValue("vector", realVector);
            throw nonPositiveDefiniteOperatorException;
        }

        private static void daxpy(double d, RealVector realVector, RealVector realVector2) {
            int dimension = realVector.getDimension();
            for (int i = 0; i < dimension; i++) {
                realVector2.setEntry(i, (realVector.getEntry(i) * d) + realVector2.getEntry(i));
            }
        }

        private static void daxpbypz(double d, RealVector realVector, double d2, RealVector realVector2, RealVector realVector3) {
            int dimension = realVector3.getDimension();
            for (int i = 0; i < dimension; i++) {
                realVector3.setEntry(i, (realVector.getEntry(i) * d) + (realVector2.getEntry(i) * d2) + realVector3.getEntry(i));
            }
        }

        /* access modifiers changed from: package-private */
        public void refineSolution(RealVector realVector) {
            int dimension = this.f8056xL.getDimension();
            int i = 0;
            if (this.lqnorm >= this.cgnorm) {
                double sqrt = FastMath.sqrt(this.tnorm);
                double d = this.gbar;
                if (d == 0.0d) {
                    d = MACH_PREC * sqrt;
                }
                double d2 = this.gammaZeta / d;
                double d3 = (this.bstep + (this.snprod * d2)) / this.beta1;
                if (!this.goodb) {
                    while (i < dimension) {
                        realVector.setEntry(i, this.f8056xL.getEntry(i) + (this.wbar.getEntry(i) * d2));
                        i++;
                    }
                    return;
                }
                while (i < dimension) {
                    realVector.setEntry(i, this.f8056xL.getEntry(i) + (this.wbar.getEntry(i) * d2) + (this.f8053mb.getEntry(i) * d3));
                    i++;
                }
            } else if (!this.goodb) {
                realVector.setSubVector(0, this.f8056xL);
            } else {
                double d4 = this.bstep / this.beta1;
                while (i < dimension) {
                    realVector.setEntry(i, this.f8056xL.getEntry(i) + (this.f8053mb.getEntry(i) * d4));
                    i++;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void init() {
            this.f8056xL.set(0.0d);
            this.f8054r1 = this.f8051b.copy();
            RealLinearOperator realLinearOperator = this.f8052m;
            this.f8057y = realLinearOperator == null ? this.f8051b.copy() : realLinearOperator.operate(this.f8054r1);
            RealLinearOperator realLinearOperator2 = this.f8052m;
            if (realLinearOperator2 != null && this.check) {
                RealVector realVector = this.f8054r1;
                RealVector realVector2 = this.f8057y;
                checkSymmetry(realLinearOperator2, realVector, realVector2, realLinearOperator2.operate(realVector2));
            }
            this.beta1 = this.f8054r1.dotProduct(this.f8057y);
            if (this.beta1 < 0.0d) {
                throwNPDLOException(this.f8052m, this.f8057y);
            }
            double d = this.beta1;
            if (d == 0.0d) {
                this.bIsNull = true;
                return;
            }
            this.bIsNull = false;
            this.beta1 = FastMath.sqrt(d);
            RealVector mapMultiply = this.f8057y.mapMultiply(1.0d / this.beta1);
            this.f8057y = this.f8050a.operate(mapMultiply);
            if (this.check) {
                RealLinearOperator realLinearOperator3 = this.f8050a;
                RealVector realVector3 = this.f8057y;
                checkSymmetry(realLinearOperator3, mapMultiply, realVector3, realLinearOperator3.operate(realVector3));
            }
            daxpy(-this.shift, mapMultiply, this.f8057y);
            double dotProduct = mapMultiply.dotProduct(this.f8057y);
            daxpy((-dotProduct) / this.beta1, this.f8054r1, this.f8057y);
            daxpy((-mapMultiply.dotProduct(this.f8057y)) / mapMultiply.dotProduct(mapMultiply), mapMultiply, this.f8057y);
            this.f8055r2 = this.f8057y.copy();
            RealLinearOperator realLinearOperator4 = this.f8052m;
            if (realLinearOperator4 != null) {
                this.f8057y = realLinearOperator4.operate(this.f8055r2);
            }
            this.oldb = this.beta1;
            this.beta = this.f8055r2.dotProduct(this.f8057y);
            if (this.beta < 0.0d) {
                throwNPDLOException(this.f8052m, this.f8057y);
            }
            this.beta = FastMath.sqrt(this.beta);
            double d2 = this.beta1;
            this.cgnorm = d2;
            this.gbar = dotProduct;
            double d3 = this.beta;
            this.dbar = d3;
            this.gammaZeta = d2;
            this.minusEpsZeta = 0.0d;
            this.bstep = 0.0d;
            this.snprod = 1.0d;
            this.tnorm = (dotProduct * dotProduct) + (d3 * d3);
            this.ynorm2 = 0.0d;
            this.gmax = FastMath.abs(dotProduct) + MACH_PREC;
            this.gmin = this.gmax;
            if (this.goodb) {
                this.wbar = new ArrayRealVector(this.f8050a.getRowDimension());
                this.wbar.set(0.0d);
            } else {
                this.wbar = mapMultiply;
            }
            updateNorms();
        }

        /* access modifiers changed from: package-private */
        public void update() {
            RealVector mapMultiply = this.f8057y.mapMultiply(1.0d / this.beta);
            this.f8057y = this.f8050a.operate(mapMultiply);
            daxpbypz(-this.shift, mapMultiply, (-this.beta) / this.oldb, this.f8054r1, this.f8057y);
            double dotProduct = mapMultiply.dotProduct(this.f8057y);
            daxpy((-dotProduct) / this.beta, this.f8055r2, this.f8057y);
            this.f8054r1 = this.f8055r2;
            this.f8055r2 = this.f8057y;
            RealLinearOperator realLinearOperator = this.f8052m;
            if (realLinearOperator != null) {
                this.f8057y = realLinearOperator.operate(this.f8055r2);
            }
            this.oldb = this.beta;
            this.beta = this.f8055r2.dotProduct(this.f8057y);
            if (this.beta < 0.0d) {
                throwNPDLOException(this.f8052m, this.f8057y);
            }
            this.beta = FastMath.sqrt(this.beta);
            double d = this.tnorm;
            double d2 = this.oldb;
            double d3 = this.beta;
            this.tnorm = d + (dotProduct * dotProduct) + (d2 * d2) + (d3 * d3);
            double d4 = this.gbar;
            double sqrt = FastMath.sqrt((d4 * d4) + (d2 * d2));
            double d5 = this.gbar / sqrt;
            double d6 = this.oldb / sqrt;
            double d7 = this.dbar;
            double d8 = (d5 * d7) + (d6 * dotProduct);
            this.gbar = (d7 * d6) - (dotProduct * d5);
            double d9 = this.beta;
            double d10 = d6 * d9;
            this.dbar = (-d5) * d9;
            double d11 = this.gammaZeta / sqrt;
            double d12 = d11 * d5;
            double d13 = d11 * d6;
            double d14 = d10;
            int dimension = this.f8056xL.getDimension();
            int i = 0;
            while (i < dimension) {
                double entry = this.f8056xL.getEntry(i);
                double entry2 = mapMultiply.getEntry(i);
                double entry3 = this.wbar.getEntry(i);
                this.f8056xL.setEntry(i, entry + (entry3 * d12) + (entry2 * d13));
                this.wbar.setEntry(i, (entry3 * d6) - (entry2 * d5));
                i++;
                dimension = dimension;
                d12 = d12;
            }
            double d15 = this.bstep;
            double d16 = this.snprod;
            this.bstep = d15 + (d5 * d16 * d11);
            this.snprod = d16 * d6;
            this.gmax = FastMath.max(this.gmax, sqrt);
            this.gmin = FastMath.min(this.gmin, sqrt);
            this.ynorm2 += d11 * d11;
            this.gammaZeta = this.minusEpsZeta - (d8 * d11);
            this.minusEpsZeta = (-d14) * d11;
            updateNorms();
        }

        private void updateNorms() {
            double d;
            double sqrt = FastMath.sqrt(this.tnorm);
            double sqrt2 = FastMath.sqrt(this.ynorm2);
            double d2 = MACH_PREC;
            double d3 = sqrt * d2;
            double d4 = sqrt * sqrt2;
            double d5 = d2 * d4;
            double d6 = d4 * this.delta;
            double d7 = this.gbar;
            if (d7 == 0.0d) {
                d7 = d3;
            }
            double d8 = this.gammaZeta;
            double d9 = this.minusEpsZeta;
            this.lqnorm = FastMath.sqrt((d8 * d8) + (d9 * d9));
            this.cgnorm = ((this.snprod * this.beta1) * this.beta) / FastMath.abs(d7);
            if (this.lqnorm <= this.cgnorm) {
                d = this.gmax / this.gmin;
            } else {
                d = this.gmax / FastMath.min(this.gmin, FastMath.abs(d7));
            }
            if (MACH_PREC * d >= 0.1d) {
                throw new IllConditionedOperatorException(d);
            } else if (this.beta1 > d5) {
                this.rnorm = FastMath.min(this.cgnorm, this.lqnorm);
                double d10 = this.cgnorm;
                this.hasConverged = d10 <= d5 || d10 <= d6;
            } else {
                throw new SingularOperatorException();
            }
        }

        /* access modifiers changed from: package-private */
        public boolean hasConverged() {
            return this.hasConverged;
        }

        /* access modifiers changed from: package-private */
        public boolean bEqualsNullVector() {
            return this.bIsNull;
        }

        /* access modifiers changed from: package-private */
        public boolean betaEqualsZero() {
            return this.beta < MACH_PREC;
        }

        /* access modifiers changed from: package-private */
        public double getNormOfResidual() {
            return this.rnorm;
        }
    }

    public SymmLQ(int i, double d, boolean z) {
        super(i);
        this.delta = d;
        this.check = z;
    }

    public SymmLQ(IterationManager iterationManager, double d, boolean z) {
        super(iterationManager);
        this.delta = d;
        this.check = z;
    }

    public final boolean getCheck() {
        return this.check;
    }

    public RealVector solve(RealLinearOperator realLinearOperator, RealLinearOperator realLinearOperator2, RealVector realVector) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, MaxCountExceededException, NonSelfAdjointOperatorException, NonPositiveDefiniteOperatorException, IllConditionedOperatorException {
        MathUtils.checkNotNull(realLinearOperator);
        return solveInPlace(realLinearOperator, realLinearOperator2, realVector, new ArrayRealVector(realLinearOperator.getColumnDimension()), false, 0.0d);
    }

    public RealVector solve(RealLinearOperator realLinearOperator, RealLinearOperator realLinearOperator2, RealVector realVector, boolean z, double d) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, MaxCountExceededException, NonSelfAdjointOperatorException, NonPositiveDefiniteOperatorException, IllConditionedOperatorException {
        MathUtils.checkNotNull(realLinearOperator);
        return solveInPlace(realLinearOperator, realLinearOperator2, realVector, new ArrayRealVector(realLinearOperator.getColumnDimension()), z, d);
    }

    public RealVector solve(RealLinearOperator realLinearOperator, RealLinearOperator realLinearOperator2, RealVector realVector, RealVector realVector2) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, NonSelfAdjointOperatorException, NonPositiveDefiniteOperatorException, IllConditionedOperatorException, MaxCountExceededException {
        MathUtils.checkNotNull(realVector2);
        return solveInPlace(realLinearOperator, realLinearOperator2, realVector, realVector2.copy(), false, 0.0d);
    }

    public RealVector solve(RealLinearOperator realLinearOperator, RealVector realVector) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, NonSelfAdjointOperatorException, IllConditionedOperatorException, MaxCountExceededException {
        MathUtils.checkNotNull(realLinearOperator);
        ArrayRealVector arrayRealVector = new ArrayRealVector(realLinearOperator.getColumnDimension());
        arrayRealVector.set(0.0d);
        return solveInPlace(realLinearOperator, null, realVector, arrayRealVector, false, 0.0d);
    }

    public RealVector solve(RealLinearOperator realLinearOperator, RealVector realVector, boolean z, double d) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, NonSelfAdjointOperatorException, IllConditionedOperatorException, MaxCountExceededException {
        MathUtils.checkNotNull(realLinearOperator);
        return solveInPlace(realLinearOperator, null, realVector, new ArrayRealVector(realLinearOperator.getColumnDimension()), z, d);
    }

    public RealVector solve(RealLinearOperator realLinearOperator, RealVector realVector, RealVector realVector2) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, NonSelfAdjointOperatorException, IllConditionedOperatorException, MaxCountExceededException {
        MathUtils.checkNotNull(realVector2);
        return solveInPlace(realLinearOperator, null, realVector, realVector2.copy(), false, 0.0d);
    }

    public RealVector solveInPlace(RealLinearOperator realLinearOperator, RealLinearOperator realLinearOperator2, RealVector realVector, RealVector realVector2) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, NonSelfAdjointOperatorException, NonPositiveDefiniteOperatorException, IllConditionedOperatorException, MaxCountExceededException {
        return solveInPlace(realLinearOperator, realLinearOperator2, realVector, realVector2, false, 0.0d);
    }

    public RealVector solveInPlace(RealLinearOperator realLinearOperator, RealLinearOperator realLinearOperator2, RealVector realVector, RealVector realVector2, boolean z, double d) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, NonSelfAdjointOperatorException, NonPositiveDefiniteOperatorException, IllConditionedOperatorException, MaxCountExceededException {
        RealVector realVector3 = realVector2;
        checkParameters(realLinearOperator, realLinearOperator2, realVector, realVector2);
        IterationManager iterationManager = getIterationManager();
        iterationManager.resetIterationCount();
        iterationManager.incrementIterationCount();
        State state = new State(realLinearOperator, realLinearOperator2, realVector, z, d, this.delta, this.check);
        state.init();
        state.refineSolution(realVector3);
        State state2 = state;
        DefaultIterativeLinearSolverEvent defaultIterativeLinearSolverEvent = new DefaultIterativeLinearSolverEvent(this, iterationManager.getIterations(), realVector2, realVector, state.getNormOfResidual());
        if (state2.bEqualsNullVector()) {
            iterationManager.fireTerminationEvent(defaultIterativeLinearSolverEvent);
            return realVector3;
        }
        boolean z2 = state2.betaEqualsZero() || state2.hasConverged();
        iterationManager.fireInitializationEvent(defaultIterativeLinearSolverEvent);
        if (!z2) {
            do {
                iterationManager.incrementIterationCount();
                RealVector realVector4 = realVector2;
                RealVector realVector5 = realVector;
                iterationManager.fireIterationStartedEvent(new DefaultIterativeLinearSolverEvent(this, iterationManager.getIterations(), realVector4, realVector5, state2.getNormOfResidual()));
                state2.update();
                state2.refineSolution(realVector3);
                iterationManager.fireIterationPerformedEvent(new DefaultIterativeLinearSolverEvent(this, iterationManager.getIterations(), realVector4, realVector5, state2.getNormOfResidual()));
            } while (!state2.hasConverged());
        }
        iterationManager.fireTerminationEvent(new DefaultIterativeLinearSolverEvent(this, iterationManager.getIterations(), realVector2, realVector, state2.getNormOfResidual()));
        return realVector3;
    }

    public RealVector solveInPlace(RealLinearOperator realLinearOperator, RealVector realVector, RealVector realVector2) throws NullArgumentException, NonSquareOperatorException, DimensionMismatchException, NonSelfAdjointOperatorException, IllConditionedOperatorException, MaxCountExceededException {
        return solveInPlace(realLinearOperator, null, realVector, realVector2, false, 0.0d);
    }
}
