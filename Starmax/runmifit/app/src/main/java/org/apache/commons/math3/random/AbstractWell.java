package org.apache.commons.math3.random;

import java.io.Serializable;
import org.apache.commons.math3.util.FastMath;

public abstract class AbstractWell extends BitsStreamGenerator implements Serializable {
    private static final long serialVersionUID = -817701723016583596L;

    /* renamed from: i1 */
    protected final int[] f8134i1;

    /* renamed from: i2 */
    protected final int[] f8135i2;

    /* renamed from: i3 */
    protected final int[] f8136i3;
    protected final int[] iRm1;
    protected final int[] iRm2;
    protected int index;

    /* renamed from: v */
    protected final int[] f8137v;

    /* access modifiers changed from: protected */
    public abstract int next(int i);

    protected AbstractWell(int i, int i2, int i3, int i4) {
        this(i, i2, i3, i4, (int[]) null);
    }

    protected AbstractWell(int i, int i2, int i3, int i4, int i5) {
        this(i, i2, i3, i4, new int[]{i5});
    }

    protected AbstractWell(int i, int i2, int i3, int i4, int[] iArr) {
        int i5 = ((i + 32) - 1) / 32;
        this.f8137v = new int[i5];
        this.index = 0;
        this.iRm1 = new int[i5];
        this.iRm2 = new int[i5];
        this.f8134i1 = new int[i5];
        this.f8135i2 = new int[i5];
        this.f8136i3 = new int[i5];
        for (int i6 = 0; i6 < i5; i6++) {
            int i7 = i6 + i5;
            this.iRm1[i6] = (i7 - 1) % i5;
            this.iRm2[i6] = (i7 - 2) % i5;
            this.f8134i1[i6] = (i6 + i2) % i5;
            this.f8135i2[i6] = (i6 + i3) % i5;
            this.f8136i3[i6] = (i6 + i4) % i5;
        }
        setSeed(iArr);
    }

    protected AbstractWell(int i, int i2, int i3, int i4, long j) {
        this(i, i2, i3, i4, new int[]{(int) (j >>> 32), (int) (j & 4294967295L)});
    }

    public void setSeed(int i) {
        setSeed(new int[]{i});
    }

    public void setSeed(int[] iArr) {
        if (iArr == null) {
            setSeed(System.currentTimeMillis() + ((long) System.identityHashCode(this)));
            return;
        }
        int[] iArr2 = this.f8137v;
        System.arraycopy(iArr, 0, iArr2, 0, FastMath.min(iArr.length, iArr2.length));
        if (iArr.length < this.f8137v.length) {
            int length = iArr.length;
            while (true) {
                int[] iArr3 = this.f8137v;
                if (length >= iArr3.length) {
                    break;
                }
                long j = (long) iArr3[length - iArr.length];
                iArr3[length] = (int) ((((j ^ (j >> 30)) * 1812433253) + ((long) length)) & 4294967295L);
                length++;
            }
        }
        this.index = 0;
        clear();
    }

    public void setSeed(long j) {
        setSeed(new int[]{(int) (j >>> 32), (int) (j & 4294967295L)});
    }
}
