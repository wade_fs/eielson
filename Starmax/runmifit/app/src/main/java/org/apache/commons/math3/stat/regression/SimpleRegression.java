package org.apache.commons.math3.stat.regression;

import java.io.Serializable;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NoDataException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Precision;

public class SimpleRegression implements Serializable, UpdatingMultipleLinearRegression {
    private static final long serialVersionUID = -3004689053607543335L;
    private final boolean hasIntercept;

    /* renamed from: n */
    private long f8191n;
    private double sumX;
    private double sumXX;
    private double sumXY;
    private double sumY;
    private double sumYY;
    private double xbar;
    private double ybar;

    public SimpleRegression() {
        this(true);
    }

    public SimpleRegression(boolean z) {
        this.sumX = 0.0d;
        this.sumXX = 0.0d;
        this.sumY = 0.0d;
        this.sumYY = 0.0d;
        this.sumXY = 0.0d;
        this.f8191n = 0;
        this.xbar = 0.0d;
        this.ybar = 0.0d;
        this.hasIntercept = z;
    }

    public void addData(double d, double d2) {
        double d3 = d;
        double d4 = d2;
        long j = this.f8191n;
        if (j == 0) {
            this.xbar = d3;
            this.ybar = d4;
        } else if (this.hasIntercept) {
            double d5 = (double) j;
            Double.isNaN(d5);
            double d6 = d5 + 1.0d;
            double d7 = (double) j;
            double d8 = (double) j;
            Double.isNaN(d8);
            Double.isNaN(d7);
            double d9 = d7 / (d8 + 1.0d);
            double d10 = this.xbar;
            double d11 = d3 - d10;
            double d12 = this.ybar;
            double d13 = d4 - d12;
            this.sumXX += d11 * d11 * d9;
            this.sumYY += d13 * d13 * d9;
            this.sumXY += d11 * d13 * d9;
            this.xbar = d10 + (d11 / d6);
            this.ybar = d12 + (d13 / d6);
        }
        if (!this.hasIntercept) {
            this.sumXX += d3 * d3;
            this.sumYY += d2 * d2;
            this.sumXY += d3 * d2;
        }
        this.sumX += d3;
        this.sumY += d2;
        this.f8191n++;
    }

    public void append(SimpleRegression simpleRegression) {
        SimpleRegression simpleRegression2 = simpleRegression;
        long j = this.f8191n;
        if (j == 0) {
            this.xbar = simpleRegression2.xbar;
            this.ybar = simpleRegression2.ybar;
            this.sumXX = simpleRegression2.sumXX;
            this.sumYY = simpleRegression2.sumYY;
            this.sumXY = simpleRegression2.sumXY;
        } else if (this.hasIntercept) {
            long j2 = simpleRegression2.f8191n;
            double d = (double) j2;
            double d2 = (double) (j2 + j);
            Double.isNaN(d);
            Double.isNaN(d2);
            double d3 = d / d2;
            double d4 = (double) (j * j2);
            double d5 = (double) (j2 + j);
            Double.isNaN(d4);
            Double.isNaN(d5);
            double d6 = d4 / d5;
            double d7 = simpleRegression2.xbar;
            double d8 = this.xbar;
            double d9 = d7 - d8;
            double d10 = simpleRegression2.ybar;
            double d11 = this.ybar;
            double d12 = d10 - d11;
            this.sumXX += simpleRegression2.sumXX + (d9 * d9 * d6);
            this.sumYY += simpleRegression2.sumYY + (d12 * d12 * d6);
            this.sumXY += simpleRegression2.sumXY + (d9 * d12 * d6);
            this.xbar = d8 + (d9 * d3);
            this.ybar = d11 + (d12 * d3);
        } else {
            this.sumXX += simpleRegression2.sumXX;
            this.sumYY += simpleRegression2.sumYY;
            this.sumXY += simpleRegression2.sumXY;
        }
        this.sumX += simpleRegression2.sumX;
        this.sumY += simpleRegression2.sumY;
        this.f8191n += simpleRegression2.f8191n;
    }

    public void removeData(double d, double d2) {
        long j = this.f8191n;
        if (j > 0) {
            if (this.hasIntercept) {
                double d3 = (double) j;
                Double.isNaN(d3);
                double d4 = d3 - 1.0d;
                double d5 = (double) j;
                double d6 = (double) j;
                Double.isNaN(d6);
                Double.isNaN(d5);
                double d7 = d5 / (d6 - 1.0d);
                double d8 = this.xbar;
                double d9 = d - d8;
                double d10 = this.ybar;
                double d11 = d2 - d10;
                this.sumXX -= (d9 * d9) * d7;
                this.sumYY -= (d11 * d11) * d7;
                this.sumXY -= (d9 * d11) * d7;
                this.xbar = d8 - (d9 / d4);
                this.ybar = d10 - (d11 / d4);
            } else {
                double d12 = (double) j;
                Double.isNaN(d12);
                double d13 = d12 - 1.0d;
                this.sumXX -= d * d;
                this.sumYY -= d2 * d2;
                this.sumXY -= d * d2;
                this.xbar -= d / d13;
                this.ybar -= d2 / d13;
            }
            this.sumX -= d;
            this.sumY -= d2;
            this.f8191n--;
        }
    }

    public void addData(double[][] dArr) throws ModelSpecificationException {
        int i = 0;
        while (i < dArr.length) {
            if (dArr[i].length >= 2) {
                addData(dArr[i][0], dArr[i][1]);
                i++;
            } else {
                throw new ModelSpecificationException(LocalizedFormats.INVALID_REGRESSION_OBSERVATION, Integer.valueOf(dArr[i].length), 2);
            }
        }
    }

    public void addObservation(double[] dArr, double d) throws ModelSpecificationException {
        if (dArr == null || dArr.length == 0) {
            LocalizedFormats localizedFormats = LocalizedFormats.INVALID_REGRESSION_OBSERVATION;
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(dArr != null ? dArr.length : 0);
            objArr[1] = 1;
            throw new ModelSpecificationException(localizedFormats, objArr);
        }
        addData(dArr[0], d);
    }

    public void addObservations(double[][] dArr, double[] dArr2) throws ModelSpecificationException {
        int i = 0;
        if (dArr == null || dArr2 == null || dArr.length != dArr2.length) {
            LocalizedFormats localizedFormats = LocalizedFormats.DIMENSIONS_MISMATCH_SIMPLE;
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(dArr == null ? 0 : dArr.length);
            if (dArr2 != null) {
                i = dArr2.length;
            }
            objArr[1] = Integer.valueOf(i);
            throw new ModelSpecificationException(localizedFormats, objArr);
        }
        boolean z = true;
        for (int i2 = 0; i2 < dArr.length; i2++) {
            if (dArr[i2] == null || dArr[i2].length == 0) {
                z = false;
            }
        }
        if (z) {
            for (int i3 = 0; i3 < dArr.length; i3++) {
                addData(dArr[i3][0], dArr2[i3]);
            }
            return;
        }
        throw new ModelSpecificationException(LocalizedFormats.NOT_ENOUGH_DATA_FOR_NUMBER_OF_PREDICTORS, 0, 1);
    }

    public void removeData(double[][] dArr) {
        for (int i = 0; i < dArr.length && this.f8191n > 0; i++) {
            removeData(dArr[i][0], dArr[i][1]);
        }
    }

    public void clear() {
        this.sumX = 0.0d;
        this.sumXX = 0.0d;
        this.sumY = 0.0d;
        this.sumYY = 0.0d;
        this.sumXY = 0.0d;
        this.f8191n = 0;
    }

    public long getN() {
        return this.f8191n;
    }

    public double predict(double d) {
        double slope = getSlope();
        return this.hasIntercept ? getIntercept(slope) + (slope * d) : slope * d;
    }

    public double getIntercept() {
        if (this.hasIntercept) {
            return getIntercept(getSlope());
        }
        return 0.0d;
    }

    public boolean hasIntercept() {
        return this.hasIntercept;
    }

    public double getSlope() {
        if (this.f8191n >= 2 && FastMath.abs(this.sumXX) >= 4.9E-323d) {
            return this.sumXY / this.sumXX;
        }
        return Double.NaN;
    }

    public double getSumSquaredErrors() {
        double d = this.sumYY;
        double d2 = this.sumXY;
        return FastMath.max(0.0d, d - ((d2 * d2) / this.sumXX));
    }

    public double getTotalSumSquares() {
        if (this.f8191n < 2) {
            return Double.NaN;
        }
        return this.sumYY;
    }

    public double getXSumSquares() {
        if (this.f8191n < 2) {
            return Double.NaN;
        }
        return this.sumXX;
    }

    public double getSumOfCrossProducts() {
        return this.sumXY;
    }

    public double getRegressionSumSquares() {
        return getRegressionSumSquares(getSlope());
    }

    public double getMeanSquareError() {
        long j;
        long j2;
        double d;
        if (this.f8191n < 3) {
            return Double.NaN;
        }
        if (this.hasIntercept) {
            d = getSumSquaredErrors();
            j2 = this.f8191n;
            j = 2;
        } else {
            d = getSumSquaredErrors();
            j2 = this.f8191n;
            j = 1;
        }
        double d2 = (double) (j2 - j);
        Double.isNaN(d2);
        return d / d2;
    }

    public double getR() {
        double slope = getSlope();
        double sqrt = FastMath.sqrt(getRSquare());
        return slope < 0.0d ? -sqrt : sqrt;
    }

    public double getRSquare() {
        double totalSumSquares = getTotalSumSquares();
        return (totalSumSquares - getSumSquaredErrors()) / totalSumSquares;
    }

    public double getInterceptStdErr() {
        if (!this.hasIntercept) {
            return Double.NaN;
        }
        double meanSquareError = getMeanSquareError();
        double d = (double) this.f8191n;
        Double.isNaN(d);
        double d2 = 1.0d / d;
        double d3 = this.xbar;
        return FastMath.sqrt(meanSquareError * (d2 + ((d3 * d3) / this.sumXX)));
    }

    public double getSlopeStdErr() {
        return FastMath.sqrt(getMeanSquareError() / this.sumXX);
    }

    public double getSlopeConfidenceInterval() throws OutOfRangeException {
        return getSlopeConfidenceInterval(0.05d);
    }

    public double getSlopeConfidenceInterval(double d) throws OutOfRangeException {
        long j = this.f8191n;
        if (j < 3) {
            return Double.NaN;
        }
        if (d >= 1.0d || d <= 0.0d) {
            throw new OutOfRangeException(LocalizedFormats.SIGNIFICANCE_LEVEL, Double.valueOf(d), 0, 1);
        }
        return getSlopeStdErr() * new TDistribution((double) (j - 2)).inverseCumulativeProbability(1.0d - (d / 2.0d));
    }

    public double getSignificance() {
        long j = this.f8191n;
        if (j < 3) {
            return Double.NaN;
        }
        return (1.0d - new TDistribution((double) (j - 2)).cumulativeProbability(FastMath.abs(getSlope()) / getSlopeStdErr())) * 2.0d;
    }

    private double getIntercept(double d) {
        if (!this.hasIntercept) {
            return 0.0d;
        }
        double d2 = this.sumY - (d * this.sumX);
        double d3 = (double) this.f8191n;
        Double.isNaN(d3);
        return d2 / d3;
    }

    private double getRegressionSumSquares(double d) {
        return d * d * this.sumXX;
    }

    public RegressionResults regress() throws ModelSpecificationException, NoDataException {
        if (this.hasIntercept) {
            if (this.f8191n < 3) {
                throw new NoDataException(LocalizedFormats.NOT_ENOUGH_DATA_REGRESSION);
            } else if (FastMath.abs(this.sumXX) > Precision.SAFE_MIN) {
                double[] dArr = {getIntercept(), getSlope()};
                double meanSquareError = getMeanSquareError();
                double d = this.sumYY;
                double d2 = this.sumY;
                long j = this.f8191n;
                double d3 = (double) j;
                Double.isNaN(d3);
                double d4 = d + ((d2 * d2) / d3);
                double d5 = this.xbar;
                double d6 = this.sumXX;
                double d7 = d6;
                double d8 = (double) j;
                Double.isNaN(d8);
                return new RegressionResults(dArr, new double[][]{new double[]{(((d5 * d5) / d6) + (1.0d / d8)) * meanSquareError, ((-d5) * meanSquareError) / d7, meanSquareError / d7}}, true, j, 2, d2, d4, getSumSquaredErrors(), true, false);
            } else {
                double d9 = this.sumY;
                long j2 = this.f8191n;
                double d10 = (double) j2;
                Double.isNaN(d10);
                double[] dArr2 = {d9 / d10, Double.NaN};
                double d11 = this.ybar;
                double d12 = (double) j2;
                Double.isNaN(d12);
                return new RegressionResults(dArr2, new double[][]{new double[]{d11 / (d12 - 1.0d), Double.NaN, Double.NaN}}, true, j2, 1, d9, this.sumYY, getSumSquaredErrors(), true, false);
            }
        } else if (this.f8191n < 2) {
            throw new NoDataException(LocalizedFormats.NOT_ENOUGH_DATA_REGRESSION);
        } else if (!Double.isNaN(this.sumXX)) {
            double meanSquareError2 = getMeanSquareError();
            double d13 = this.sumXX;
            double[] dArr3 = {this.sumXY / d13};
            double[][] dArr4 = {new double[]{meanSquareError2 / d13}};
            long j3 = this.f8191n;
            double d14 = this.sumY;
            return new RegressionResults(dArr3, dArr4, true, j3, 1, d14, this.sumYY, getSumSquaredErrors(), false, false);
        } else {
            return new RegressionResults(new double[]{Double.NaN}, new double[][]{new double[]{Double.NaN}}, true, this.f8191n, 1, Double.NaN, Double.NaN, Double.NaN, false, false);
        }
    }

    public RegressionResults regress(int[] iArr) throws MathIllegalArgumentException {
        int[] iArr2 = iArr;
        if (iArr2 == null || iArr2.length == 0) {
            throw new MathIllegalArgumentException(LocalizedFormats.ARRAY_ZERO_LENGTH_OR_NULL_NOT_ALLOWED, new Object[0]);
        }
        int i = 2;
        if (iArr2.length > 2 || (iArr2.length > 1 && !this.hasIntercept)) {
            LocalizedFormats localizedFormats = LocalizedFormats.ARRAY_SIZE_EXCEEDS_MAX_VARIABLES;
            Object[] objArr = new Object[1];
            if (iArr2.length > 1 && !this.hasIntercept) {
                i = 1;
            }
            objArr[0] = Integer.valueOf(i);
            throw new ModelSpecificationException(localizedFormats, objArr);
        } else if (this.hasIntercept) {
            if (iArr2.length == 2) {
                if (iArr2[0] == 1) {
                    throw new ModelSpecificationException(LocalizedFormats.NOT_INCREASING_SEQUENCE, new Object[0]);
                } else if (iArr2[0] != 0) {
                    throw new OutOfRangeException(Integer.valueOf(iArr2[0]), 0, 1);
                } else if (iArr2[1] == 1) {
                    return regress();
                } else {
                    throw new OutOfRangeException(Integer.valueOf(iArr2[0]), 0, 1);
                }
            } else if (iArr2[0] == 1 || iArr2[0] == 0) {
                double d = this.sumY;
                long j = this.f8191n;
                double d2 = (double) j;
                Double.isNaN(d2);
                double d3 = (d * d) / d2;
                double d4 = this.sumYY;
                double d5 = d4 + d3;
                if (iArr2[0] == 0) {
                    double d6 = (double) ((j - 1) * j);
                    Double.isNaN(d6);
                    double[] dArr = {d4 / d6};
                    double d7 = d4;
                    return new RegressionResults(new double[]{this.ybar}, new double[][]{dArr}, true, j, 1, d, d5 + d3, d7, true, false);
                } else if (iArr2[0] != 1) {
                    return null;
                } else {
                    double d8 = this.sumXX;
                    double d9 = this.sumX;
                    double d10 = (double) j;
                    Double.isNaN(d10);
                    double d11 = d8 + ((d9 * d9) / d10);
                    double d12 = this.sumXY;
                    double d13 = (double) j;
                    Double.isNaN(d13);
                    double d14 = d12 + ((d9 * d) / d13);
                    double max = FastMath.max(0.0d, d5 - ((d14 * d14) / d11));
                    double d15 = (double) (this.f8191n - 1);
                    Double.isNaN(d15);
                    double d16 = max / d15;
                    if (!Double.isNaN(d11)) {
                        return new RegressionResults(new double[]{d14 / d11}, new double[][]{new double[]{d16 / d11}}, true, this.f8191n, 1, this.sumY, d5, max, false, false);
                    }
                    return new RegressionResults(new double[]{Double.NaN}, new double[][]{new double[]{Double.NaN}}, true, this.f8191n, 1, Double.NaN, Double.NaN, Double.NaN, false, false);
                }
            } else {
                throw new OutOfRangeException(Integer.valueOf(iArr2[0]), 0, 1);
            }
        } else if (iArr2[0] == 0) {
            return regress();
        } else {
            throw new OutOfRangeException(Integer.valueOf(iArr2[0]), 0, 0);
        }
    }
}
