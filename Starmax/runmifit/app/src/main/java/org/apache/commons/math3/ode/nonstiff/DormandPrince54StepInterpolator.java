package org.apache.commons.math3.ode.nonstiff;

import org.apache.commons.math3.ode.AbstractIntegrator;
import org.apache.commons.math3.ode.EquationsMapper;
import org.apache.commons.math3.ode.sampling.StepInterpolator;

class DormandPrince54StepInterpolator extends RungeKuttaStepInterpolator {
    private static final double A70 = 0.09114583333333333d;
    private static final double A72 = 0.44923629829290207d;
    private static final double A73 = 0.6510416666666666d;
    private static final double A74 = -0.322376179245283d;
    private static final double A75 = 0.13095238095238096d;

    /* renamed from: D0 */
    private static final double f8077D0 = -1.1270175653862835d;

    /* renamed from: D2 */
    private static final double f8078D2 = 2.675424484351598d;

    /* renamed from: D3 */
    private static final double f8079D3 = -5.685526961588504d;

    /* renamed from: D4 */
    private static final double f8080D4 = 3.5219323679207912d;

    /* renamed from: D5 */
    private static final double f8081D5 = -1.7672812570757455d;

    /* renamed from: D6 */
    private static final double f8082D6 = 2.382468931778144d;
    private static final long serialVersionUID = 20111120;

    /* renamed from: v1 */
    private double[] f8083v1;

    /* renamed from: v2 */
    private double[] f8084v2;

    /* renamed from: v3 */
    private double[] f8085v3;

    /* renamed from: v4 */
    private double[] f8086v4;
    private boolean vectorsInitialized;

    public DormandPrince54StepInterpolator() {
        this.f8083v1 = null;
        this.f8084v2 = null;
        this.f8085v3 = null;
        this.f8086v4 = null;
        this.vectorsInitialized = false;
    }

    public DormandPrince54StepInterpolator(DormandPrince54StepInterpolator dormandPrince54StepInterpolator) {
        super(super);
        double[] dArr = dormandPrince54StepInterpolator.f8083v1;
        if (dArr == null) {
            this.f8083v1 = null;
            this.f8084v2 = null;
            this.f8085v3 = null;
            this.f8086v4 = null;
            this.vectorsInitialized = false;
            return;
        }
        this.f8083v1 = (double[]) dArr.clone();
        this.f8084v2 = (double[]) dormandPrince54StepInterpolator.f8084v2.clone();
        this.f8085v3 = (double[]) dormandPrince54StepInterpolator.f8085v3.clone();
        this.f8086v4 = (double[]) dormandPrince54StepInterpolator.f8086v4.clone();
        this.vectorsInitialized = dormandPrince54StepInterpolator.vectorsInitialized;
    }

    /* access modifiers changed from: protected */
    public StepInterpolator doCopy() {
        return new DormandPrince54StepInterpolator(this);
    }

    public void reinitialize(AbstractIntegrator abstractIntegrator, double[] dArr, double[][] dArr2, boolean z, EquationsMapper equationsMapper, EquationsMapper[] equationsMapperArr) {
        super.reinitialize(abstractIntegrator, dArr, dArr2, z, equationsMapper, equationsMapperArr);
        this.f8083v1 = null;
        this.f8084v2 = null;
        this.f8085v3 = null;
        this.f8086v4 = null;
        this.vectorsInitialized = false;
    }

    public void storeTime(double d) {
        super.storeTime(d);
        this.vectorsInitialized = false;
    }

    /* access modifiers changed from: protected */
    public void computeInterpolatedStateAndDerivatives(double d, double d2) {
        char c = 0;
        if (!this.vectorsInitialized) {
            if (this.f8083v1 == null) {
                this.f8083v1 = new double[this.interpolatedState.length];
                this.f8084v2 = new double[this.interpolatedState.length];
                this.f8085v3 = new double[this.interpolatedState.length];
                this.f8086v4 = new double[this.interpolatedState.length];
            }
            int i = 0;
            while (i < this.interpolatedState.length) {
                double d3 = this.yDotK[c][i];
                double d4 = this.yDotK[2][i];
                double d5 = this.yDotK[3][i];
                double d6 = this.yDotK[4][i];
                double d7 = this.yDotK[5][i];
                double d8 = this.yDotK[6][i];
                double[] dArr = this.f8083v1;
                dArr[i] = (A70 * d3) + (A72 * d4) + (A73 * d5) + (A74 * d6) + (A75 * d7);
                double[] dArr2 = this.f8084v2;
                dArr2[i] = d3 - dArr[i];
                this.f8085v3[i] = (dArr[i] - dArr2[i]) - d8;
                this.f8086v4[i] = (d3 * f8077D0) + (d4 * f8078D2) + (d5 * f8079D3) + (d6 * f8080D4) + (d7 * f8081D5) + (d8 * f8082D6);
                i++;
                c = 0;
            }
            this.vectorsInitialized = true;
        }
        double d9 = 1.0d - d;
        double d10 = d * 2.0d;
        double d11 = 1.0d - d10;
        double d12 = (2.0d - (d * 3.0d)) * d;
        double d13 = d10 * (((d10 - 3.0d) * d) + 1.0d);
        if (this.previousState == null || d > 0.5d) {
            for (int i2 = 0; i2 < this.interpolatedState.length; i2++) {
                this.interpolatedState[i2] = this.currentState[i2] - ((this.f8083v1[i2] - ((this.f8084v2[i2] + ((this.f8085v3[i2] + (this.f8086v4[i2] * d9)) * d)) * d)) * d2);
                this.interpolatedDerivatives[i2] = this.f8083v1[i2] + (this.f8084v2[i2] * d11) + (this.f8085v3[i2] * d12) + (this.f8086v4[i2] * d13);
            }
            return;
        }
        for (int i3 = 0; i3 < this.interpolatedState.length; i3++) {
            this.interpolatedState[i3] = this.previousState[i3] + (this.f8098h * d * (this.f8083v1[i3] + ((this.f8084v2[i3] + ((this.f8085v3[i3] + (this.f8086v4[i3] * d9)) * d)) * d9)));
            this.interpolatedDerivatives[i3] = this.f8083v1[i3] + (this.f8084v2[i3] * d11) + (this.f8085v3[i3] * d12) + (this.f8086v4[i3] * d13);
        }
    }
}
