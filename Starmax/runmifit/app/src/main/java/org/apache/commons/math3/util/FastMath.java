package org.apache.commons.math3.util;

import java.io.PrintStream;

public class FastMath {
    private static final double[] CBRTTWO = {0.6299605249474366d, 0.7937005259840998d, 1.0d, 1.2599210498948732d, 1.5874010519681994d};
    private static final double[] COSINE_TABLE_A = {1.0d, 0.9921976327896118d, 0.9689123630523682d, 0.9305076599121094d, 0.8775825500488281d, 0.8109631538391113d, 0.7316888570785522d, 0.6409968137741089d, 0.5403022766113281d, 0.4311765432357788d, 0.3153223395347595d, 0.19454771280288696d, 0.07073719799518585d, -0.05417713522911072d};
    private static final double[] COSINE_TABLE_B = {0.0d, 3.4439717236742845E-8d, 5.865827662008209E-8d, -3.7999795083850525E-8d, 1.184154459111628E-8d, -3.43338934259355E-8d, 1.1795268640216787E-8d, 4.438921624363781E-8d, 2.925681159240093E-8d, -2.6437112632041807E-8d, 2.2860509143963117E-8d, -4.813899778443457E-9d, 3.6725170580355583E-9d, 2.0217439756338078E-10d};

    /* renamed from: E */
    public static final double f8201E = 2.718281828459045d;
    private static final double[] EIGHTHS = {0.0d, 0.125d, F_1_4, 0.375d, 0.5d, 0.625d, F_3_4, F_7_8, 1.0d, 1.125d, 1.25d, 1.375d, 1.5d, 1.625d};
    static final int EXP_FRAC_TABLE_LEN = 1025;
    static final int EXP_INT_TABLE_LEN = 1500;
    static final int EXP_INT_TABLE_MAX_INDEX = 750;
    private static final double F_11_12 = 0.9166666666666666d;
    private static final double F_13_14 = 0.9285714285714286d;
    private static final double F_15_16 = 0.9375d;
    private static final double F_1_11 = 0.09090909090909091d;
    private static final double F_1_13 = 0.07692307692307693d;
    private static final double F_1_15 = 0.06666666666666667d;
    private static final double F_1_17 = 0.058823529411764705d;
    private static final double F_1_2 = 0.5d;
    private static final double F_1_3 = 0.3333333333333333d;
    private static final double F_1_4 = 0.25d;
    private static final double F_1_5 = 0.2d;
    private static final double F_1_7 = 0.14285714285714285d;
    private static final double F_1_9 = 0.1111111111111111d;
    private static final double F_3_4 = 0.75d;
    private static final double F_5_6 = 0.8333333333333334d;
    private static final double F_7_8 = 0.875d;
    private static final double F_9_10 = 0.9d;
    private static final long HEX_40000000 = 1073741824;
    private static final double LN_2_A = 0.6931470632553101d;
    private static final double LN_2_B = 1.1730463525082348E-7d;
    private static final double[][] LN_HI_PREC_COEF = {new double[]{1.0d, -6.032174644509064E-23d}, new double[]{-0.25d, -0.25d}, new double[]{0.3333333134651184d, 1.9868161777724352E-8d}, new double[]{-0.2499999701976776d, -2.957007209750105E-8d}, new double[]{0.19999954104423523d, 1.5830993332061267E-10d}, new double[]{-0.16624879837036133d, -2.6033824355191673E-8d}};
    static final int LN_MANT_LEN = 1024;
    private static final double[][] LN_QUICK_COEF = {new double[]{1.0d, 5.669184079525E-24d}, new double[]{-0.25d, -0.25d}, new double[]{0.3333333134651184d, 1.986821492305628E-8d}, new double[]{-0.25d, -6.663542893624021E-14d}, new double[]{0.19999998807907104d, 1.1921056801463227E-8d}, new double[]{-0.1666666567325592d, -7.800414592973399E-9d}, new double[]{0.1428571343421936d, 5.650007086920087E-9d}, new double[]{-0.12502530217170715d, -7.44321345601866E-11d}, new double[]{0.11113807559013367d, 9.219544613762692E-9d}};
    private static final double LOG_MAX_VALUE = StrictMath.log(Double.MAX_VALUE);
    private static final long MASK_30BITS = -1073741824;
    private static final int MASK_NON_SIGN_INT = Integer.MAX_VALUE;
    private static final long MASK_NON_SIGN_LONG = Long.MAX_VALUE;

    /* renamed from: PI */
    public static final double f8202PI = 3.141592653589793d;
    private static final long[] PI_O_4_BITS = {-3958705157555305932L, -4267615245585081135L};
    private static final long[] RECIP_2PI = {2935890503282001226L, 9154082963658192752L, 3952090531849364496L, 9193070505571053912L, 7910884519577875640L, 113236205062349959L, 4577762542105553359L, -5034868814120038111L, 4208363204685324176L, 5648769086999809661L, 2819561105158720014L, -4035746434778044925L, -302932621132653753L, -2644281811660520851L, -3183605296591799669L, 6722166367014452318L, -3512299194304650054L, -7278142539171889152L};
    private static final boolean RECOMPUTE_TABLES_AT_RUNTIME = false;
    private static final double[] SINE_TABLE_A = {0.0d, 0.1246747374534607d, 0.24740394949913025d, 0.366272509098053d, 0.4794255495071411d, 0.5850973129272461d, 0.6816387176513672d, 0.7675435543060303d, 0.8414709568023682d, 0.902267575263977d, 0.9489846229553223d, 0.9808930158615112d, 0.9974949359893799d, 0.9985313415527344d};
    private static final double[] SINE_TABLE_B = {0.0d, -4.068233003401932E-9d, 9.755392680573412E-9d, 1.9987994582857286E-8d, -1.0902938113007961E-8d, -3.9986783938944604E-8d, 4.23719669792332E-8d, -5.207000323380292E-8d, 2.800552834259E-8d, 1.883511811213715E-8d, -3.5997360512765566E-9d, 4.116164446561962E-8d, 5.0614674548127384E-8d, -1.0129027912496858E-9d};
    private static final int SINE_TABLE_LEN = 14;
    private static final double[] TANGENT_TABLE_A = {0.0d, 0.1256551444530487d, 0.25534194707870483d, 0.3936265707015991d, 0.5463024377822876d, 0.7214844226837158d, 0.9315965175628662d, 1.1974215507507324d, 1.5574076175689697d, 2.092571258544922d, 3.0095696449279785d, 5.041914939880371d, 14.101419448852539d, -18.430862426757812d};
    private static final double[] TANGENT_TABLE_B = {0.0d, -7.877917738262007E-9d, -2.5857668567479893E-8d, 5.2240336371356666E-9d, 5.206150291559893E-8d, 1.8307188599677033E-8d, -5.7618793749770706E-8d, 7.848361555046424E-8d, 1.0708593250394448E-7d, 1.7827257129423813E-8d, 2.893485277253286E-8d, 3.1660099222737955E-7d, 4.983191803254889E-7d, -3.356118100840571E-7d};
    private static final double TWO_POWER_52 = 4.503599627370496E15d;
    private static final double TWO_POWER_53 = 9.007199254740992E15d;

    public static int abs(int i) {
        int i2 = i >>> 31;
        return (i ^ ((i2 ^ -1) + 1)) + i2;
    }

    public static long abs(long j) {
        long j2 = j >>> 63;
        return (j ^ ((-1 ^ j2) + 1)) + j2;
    }

    public static int max(int i, int i2) {
        return i <= i2 ? i2 : i;
    }

    public static long max(long j, long j2) {
        return j <= j2 ? j2 : j;
    }

    public static int min(int i, int i2) {
        return i <= i2 ? i : i2;
    }

    public static long min(long j, long j2) {
        return j <= j2 ? j : j2;
    }

    private static double polyCosine(double d) {
        double d2 = d * d;
        return ((((((2.479773539153719E-5d * d2) - 3231.288930800263d) * d2) + 0.041666666666621166d) * d2) - 8.000000000000002d) * d2;
    }

    private static double polySine(double d) {
        double d2 = d * d;
        return ((((((2.7553817452272217E-6d * d2) - 22521.49865654966d) * d2) + 0.008333333333329196d) * d2) - 26.666666666666668d) * d2 * d;
    }

    public static double pow(double d, int i) {
        double d2;
        int i2 = i;
        double d3 = 1.0d;
        if (i2 == 0) {
            return 1.0d;
        }
        if (i2 < 0) {
            i2 = -i2;
            d2 = 1.0d / d;
        } else {
            d2 = d;
        }
        double d4 = d2 * 1.34217729E8d;
        double d5 = d4 - (d4 - d2);
        double d6 = d2 - d5;
        double d7 = 0.0d;
        while (i2 != 0) {
            if ((i2 & 1) != 0) {
                double d8 = d3 * d2;
                double d9 = d3 * 1.34217729E8d;
                double d10 = d9 - (d9 - d3);
                double d11 = d3 - d10;
                d7 = (d7 * d2) + ((d11 * d6) - (((d8 - (d10 * d5)) - (d11 * d5)) - (d10 * d6)));
                d3 = d8;
            }
            double d12 = d5 * d2;
            double d13 = d5 * 1.34217729E8d;
            double d14 = d13 - (d13 - d5);
            double d15 = d5 - d14;
            double d16 = d12 * 1.34217729E8d;
            d5 = d16 - (d16 - d12);
            d6 = (d6 * d2) + ((d15 * d6) - (((d12 - (d14 * d5)) - (d15 * d5)) - (d14 * d6))) + (d12 - d5);
            d2 = d5 + d6;
            i2 >>= 1;
        }
        return d3 + d7;
    }

    public static double signum(double d) {
        if (d < 0.0d) {
            return -1.0d;
        }
        if (d > 0.0d) {
            return 1.0d;
        }
        return d;
    }

    public static float signum(float f) {
        if (f < 0.0f) {
            return -1.0f;
        }
        if (f > 0.0f) {
            return 1.0f;
        }
        return f;
    }

    private FastMath() {
    }

    private static double doubleHighPart(double d) {
        if (d <= (-Precision.SAFE_MIN) || d >= Precision.SAFE_MIN) {
            return Double.longBitsToDouble(Double.doubleToRawLongBits(d) & MASK_30BITS);
        }
        return d;
    }

    public static double sqrt(double d) {
        return Math.sqrt(d);
    }

    public static double cosh(double d) {
        double exp;
        double exp2;
        double d2 = d;
        if (d2 != d2) {
            return d2;
        }
        if (d2 > 20.0d) {
            if (d2 >= LOG_MAX_VALUE) {
                exp2 = exp(d2 * 0.5d);
            } else {
                exp = exp(d);
                return exp * 0.5d;
            }
        } else if (d2 >= -20.0d) {
            double[] dArr = new double[2];
            if (d2 < 0.0d) {
                d2 = -d2;
            }
            exp(d2, 0.0d, dArr);
            double d3 = dArr[0] + dArr[1];
            double d4 = -((d3 - dArr[0]) - dArr[1]);
            double d5 = d3 * 1.073741824E9d;
            double d6 = (d3 + d5) - d5;
            double d7 = d3 - d6;
            double d8 = 1.0d / d3;
            double d9 = 1.073741824E9d * d8;
            double d10 = (d8 + d9) - d9;
            double d11 = d8 - d10;
            double d12 = d11 + (((((1.0d - (d6 * d10)) - (d6 * d11)) - (d7 * d10)) - (d7 * d11)) * d8) + ((-d4) * d8 * d8);
            double d13 = d3 + d10;
            double d14 = d4 + (-((d13 - d3) - d10));
            double d15 = d13 + d12;
            return (d15 + d14 + (-((d15 - d13) - d12))) * 0.5d;
        } else if (d2 <= (-LOG_MAX_VALUE)) {
            exp2 = exp(d2 * -0.5d);
        } else {
            exp = exp(-d2);
            return exp * 0.5d;
        }
        return 0.5d * exp2 * exp2;
    }

    public static double sinh(double d) {
        boolean z;
        double d2;
        double d3;
        double d4;
        double exp;
        double exp2;
        double d5 = d;
        if (d5 != d5) {
            return d5;
        }
        double d6 = 0.5d;
        if (d5 > 20.0d) {
            if (d5 >= LOG_MAX_VALUE) {
                exp2 = exp(d5 * 0.5d);
            } else {
                exp = exp(d);
                return exp * d6;
            }
        } else if (d5 < -20.0d) {
            d6 = -0.5d;
            if (d5 <= (-LOG_MAX_VALUE)) {
                exp2 = exp(d5 * -0.5d);
            } else {
                exp = exp(-d5);
                return exp * d6;
            }
        } else if (d5 == 0.0d) {
            return d5;
        } else {
            if (d5 < 0.0d) {
                d5 = -d5;
                z = true;
            } else {
                z = false;
            }
            if (d5 > F_1_4) {
                double[] dArr = new double[2];
                exp(d5, 0.0d, dArr);
                double d7 = dArr[0] + dArr[1];
                double d8 = -((d7 - dArr[0]) - dArr[1]);
                double d9 = d7 * 1.073741824E9d;
                double d10 = (d7 + d9) - d9;
                double d11 = d7 - d10;
                double d12 = 1.0d / d7;
                double d13 = 1.073741824E9d * d12;
                double d14 = (d12 + d13) - d13;
                double d15 = d12 - d14;
                double d16 = -d14;
                double d17 = -(d15 + (((((1.0d - (d10 * d14)) - (d10 * d15)) - (d11 * d14)) - (d11 * d15)) * d12) + ((-d8) * d12 * d12));
                double d18 = d7 + d16;
                d3 = d8 + (-((d18 - d7) - d16));
                d4 = d18 + d17;
                d2 = -((d4 - d18) - d17);
            } else {
                double[] dArr2 = new double[2];
                expm1(d5, dArr2);
                double d19 = dArr2[0] + dArr2[1];
                double d20 = -((d19 - dArr2[0]) - dArr2[1]);
                double d21 = d19 + 1.0d;
                double d22 = 1.0d / d21;
                double d23 = d19 * d22;
                double d24 = d23 * 1.073741824E9d;
                double d25 = (d23 + d24) - d24;
                double d26 = d23 - d25;
                double d27 = 1.073741824E9d * d21;
                double d28 = (d21 + d27) - d27;
                double d29 = d21 - d28;
                double d30 = d26 + (((((d19 - (d28 * d25)) - (d28 * d26)) - (d29 * d25)) - (d29 * d26)) * d22) + (d20 * d22) + ((-d19) * ((-((d21 - 1.0d) - d19)) + d20) * d22 * d22);
                double d31 = d19 + d25;
                d3 = d20 + (-((d31 - d19) - d25));
                d4 = d31 + d30;
                d2 = -((d4 - d31) - d30);
            }
            double d32 = (d4 + d3 + d2) * 0.5d;
            return z ? -d32 : d32;
        }
        return d6 * exp2 * exp2;
    }

    public static double tanh(double d) {
        boolean z;
        double d2;
        double d3;
        double d4 = d;
        if (d4 != d4) {
            return d4;
        }
        if (d4 > 20.0d) {
            return 1.0d;
        }
        if (d4 < -20.0d) {
            return -1.0d;
        }
        if (d4 == 0.0d) {
            return d4;
        }
        if (d4 < 0.0d) {
            d4 = -d4;
            z = true;
        } else {
            z = false;
        }
        if (d4 >= 0.5d) {
            double[] dArr = new double[2];
            exp(d4 * 2.0d, 0.0d, dArr);
            double d5 = dArr[0] + dArr[1];
            double d6 = -((d5 - dArr[0]) - dArr[1]);
            double d7 = -1.0d + d5;
            double d8 = d7 + d6;
            double d9 = (-((d7 + 1.0d) - d5)) + (-((d8 - d7) - d6));
            double d10 = d5 + 1.0d;
            double d11 = d10 + d6;
            double d12 = (-((d10 - 1.0d) - d5)) + (-((d11 - d10) - d6));
            double d13 = d11 * 1.073741824E9d;
            double d14 = (d11 + d13) - d13;
            double d15 = d11 - d14;
            double d16 = d8 / d11;
            double d17 = 1.073741824E9d * d16;
            d2 = (d16 + d17) - d17;
            double d18 = d16 - d2;
            d3 = d18 + (((((d8 - (d14 * d2)) - (d14 * d18)) - (d15 * d2)) - (d15 * d18)) / d11) + (d9 / d11) + ((((-d12) * d8) / d11) / d11);
        } else {
            double[] dArr2 = new double[2];
            expm1(d4 * 2.0d, dArr2);
            double d19 = dArr2[0] + dArr2[1];
            double d20 = -((d19 - dArr2[0]) - dArr2[1]);
            double d21 = d19 + 2.0d;
            double d22 = d21 + d20;
            double d23 = (-((d21 - 2.0d) - d19)) + (-((d22 - d21) - d20));
            double d24 = d22 * 1.073741824E9d;
            double d25 = (d22 + d24) - d24;
            double d26 = d22 - d25;
            double d27 = d19 / d22;
            double d28 = 1.073741824E9d * d27;
            d2 = (d27 + d28) - d28;
            double d29 = d27 - d2;
            d3 = d29 + (((((d19 - (d25 * d2)) - (d25 * d29)) - (d26 * d2)) - (d26 * d29)) / d22) + (d20 / d22) + ((((-d23) * d19) / d22) / d22);
        }
        double d30 = d2 + d3;
        return z ? -d30 : d30;
    }

    public static double acosh(double d) {
        return log(d + sqrt((d * d) - 1.0d));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00b5, code lost:
        if (r0 > 0.0036d) goto L_0x008a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static double asinh(double r37) {
        /*
            r0 = r37
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x000b
            r2 = 1
            double r0 = -r0
            goto L_0x000c
        L_0x000b:
            r2 = 0
        L_0x000c:
            r3 = 4595184829392702407(0x3fc5604189374bc7, double:0.167)
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r7 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r7 <= 0) goto L_0x0025
            double r3 = r0 * r0
            double r3 = r3 + r5
            double r3 = sqrt(r3)
            double r3 = r3 + r0
            double r0 = log(r3)
            goto L_0x00b8
        L_0x0025:
            double r3 = r0 * r0
            r7 = 4591654007284843938(0x3fb8d4fdf3b645a2, double:0.097)
            r9 = 4606281698874543309(0x3feccccccccccccd, double:0.9)
            r11 = 4606431818862122325(0x3fed555555555555, double:0.9166666666666666)
            r13 = 4590207312512236308(0x3fb3b13b13b13b14, double:0.07692307692307693)
            r15 = 4591215111030249286(0x3fb745d1745d1746, double:0.09090909090909091)
            r17 = 4605681218924227243(0x3feaaaaaaaaaaaab, double:0.8333333333333334)
            r19 = 4606056518893174784(0x3fec000000000000, double:0.875)
            r21 = 4592670820000712476(0x3fbc71c71c71c71c, double:0.1111111111111111)
            r23 = 4594314991293244562(0x3fc2492492492492, double:0.14285714285714285)
            r25 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            r27 = 4604930618986332160(0x3fe8000000000000, double:0.75)
            r29 = 4596373779694328218(0x3fc999999999999a, double:0.2)
            r31 = 4599676419421066581(0x3fd5555555555555, double:0.3333333333333333)
            int r33 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r33 <= 0) goto L_0x00a4
            r7 = 4589468260265693457(0x3fb1111111111111, double:0.06666666666666667)
            r33 = 4588638185040256542(0x3fae1e1e1e1e1e1e, double:0.058823529411764705)
            double r33 = r33 * r3
            r35 = 4606619468846596096(0x3fee000000000000, double:0.9375)
            double r33 = r33 * r35
            double r7 = r7 - r33
            double r7 = r7 * r3
            r33 = 4606539047424678766(0x3fedb6db6db6db6e, double:0.9285714285714286)
            double r7 = r7 * r33
            double r13 = r13 - r7
        L_0x007f:
            double r13 = r13 * r3
            double r13 = r13 * r11
            double r15 = r15 - r13
            double r15 = r15 * r3
            double r15 = r15 * r9
            double r21 = r21 - r15
        L_0x008a:
            double r21 = r21 * r3
            double r21 = r21 * r19
            double r23 = r23 - r21
            double r23 = r23 * r3
            double r23 = r23 * r17
            double r29 = r29 - r23
        L_0x0096:
            double r29 = r29 * r3
            double r29 = r29 * r27
            double r31 = r31 - r29
            double r3 = r3 * r31
            double r3 = r3 * r25
            double r5 = r5 - r3
            double r0 = r0 * r5
            goto L_0x00b8
        L_0x00a4:
            r7 = 4585348967806525243(0x3fa26e978d4fdf3b, double:0.036)
            int r33 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r33 <= 0) goto L_0x00ae
            goto L_0x007f
        L_0x00ae:
            r7 = 4570447457359481746(0x3f6d7dbf487fcb92, double:0.0036)
            int r9 = (r0 > r7 ? 1 : (r0 == r7 ? 0 : -1))
            if (r9 <= 0) goto L_0x0096
            goto L_0x008a
        L_0x00b8:
            if (r2 == 0) goto L_0x00bb
            double r0 = -r0
        L_0x00bb:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.util.FastMath.asinh(double):double");
    }

    public static double atanh(double d) {
        boolean z;
        double d2;
        double d3 = d;
        if (d3 < 0.0d) {
            z = true;
            d3 = -d3;
        } else {
            z = false;
        }
        if (d3 > 0.15d) {
            d2 = 0.5d;
            d3 = log((d3 + 1.0d) / (1.0d - d3));
        } else {
            double d4 = d3 * d3;
            d2 = (d3 > 0.087d ? d4 * ((((((((((((((F_1_17 * d4) + F_1_15) * d4) + F_1_13) * d4) + F_1_11) * d4) + F_1_9) * d4) + F_1_7) * d4) + F_1_5) * d4) + F_1_3) : d3 > 0.031d ? d4 * ((((((((((F_1_13 * d4) + F_1_11) * d4) + F_1_9) * d4) + F_1_7) * d4) + F_1_5) * d4) + F_1_3) : d3 > 0.003d ? d4 * ((((((F_1_9 * d4) + F_1_7) * d4) + F_1_5) * d4) + F_1_3) : d4 * ((F_1_5 * d4) + F_1_3)) + 1.0d;
        }
        double d5 = d3 * d2;
        return z ? -d5 : d5;
    }

    public static double nextUp(double d) {
        return nextAfter(d, Double.POSITIVE_INFINITY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.FastMath.nextAfter(float, double):float
     arg types: [float, int]
     candidates:
      org.apache.commons.math3.util.FastMath.nextAfter(double, double):double
      org.apache.commons.math3.util.FastMath.nextAfter(float, double):float */
    public static float nextUp(float f) {
        return nextAfter(f, Double.POSITIVE_INFINITY);
    }

    public static double random() {
        return Math.random();
    }

    public static double exp(double d) {
        return exp(d, 0.0d, null);
    }

    private static double exp(double d, double d2, double[] dArr) {
        double d3;
        double d4;
        int i;
        double d5;
        double d6 = d;
        double d7 = d2;
        double[] dArr2 = dArr;
        if (d6 < 0.0d) {
            int i2 = (int) (-d6);
            if (i2 > 746) {
                if (dArr2 != null) {
                    dArr2[0] = 0.0d;
                    dArr2[1] = 0.0d;
                }
                return 0.0d;
            } else if (i2 > 709) {
                double exp = exp(d6 + 40.19140625d, d7, dArr2) / 2.85040095144011776E17d;
                if (dArr2 != null) {
                    dArr2[0] = dArr2[0] / 2.85040095144011776E17d;
                    dArr2[1] = dArr2[1] / 2.85040095144011776E17d;
                }
                return exp;
            } else if (i2 == 709) {
                double exp2 = exp(d6 + 1.494140625d, d7, dArr2) / 4.455505956692757d;
                if (dArr2 != null) {
                    dArr2[0] = dArr2[0] / 4.455505956692757d;
                    dArr2[1] = dArr2[1] / 4.455505956692757d;
                }
                return exp2;
            } else {
                int i3 = i2 + 1;
                int i4 = 750 - i3;
                d4 = ExpIntTable.EXP_INT_TABLE_A[i4];
                d3 = ExpIntTable.EXP_INT_TABLE_B[i4];
                i = -i3;
            }
        } else {
            int i5 = (int) d6;
            if (i5 > 709) {
                if (dArr2 != null) {
                    dArr2[0] = Double.POSITIVE_INFINITY;
                    dArr2[1] = 0.0d;
                }
                return Double.POSITIVE_INFINITY;
            }
            double[] access$000 = ExpIntTable.EXP_INT_TABLE_A;
            int i6 = i5 + EXP_INT_TABLE_MAX_INDEX;
            d4 = access$000[i6];
            d3 = ExpIntTable.EXP_INT_TABLE_B[i6];
            i = i5;
        }
        double d8 = (double) i;
        Double.isNaN(d8);
        int i7 = (int) ((d6 - d8) * 1024.0d);
        double d9 = ExpFracTable.EXP_FRAC_TABLE_A[i7];
        double d10 = ExpFracTable.EXP_FRAC_TABLE_B[i7];
        double d11 = (double) i7;
        Double.isNaN(d11);
        Double.isNaN(d8);
        double d12 = d6 - (d8 + (d11 / 1024.0d));
        double d13 = (((((((0.04168701738764507d * d12) + 0.1666666505023083d) * d12) + 0.5000000000042687d) * d12) + 1.0d) * d12) - 1.1409003175371524E20d;
        double d14 = d4 * d9;
        double d15 = (d4 * d10) + (d9 * d3) + (d3 * d10);
        double d16 = d15 + d14;
        if (d7 != 0.0d) {
            double d17 = d16 * d7;
            d5 = (d17 * d13) + d17 + (d16 * d13) + d15 + d14;
        } else {
            d5 = (d16 * d13) + d15 + d14;
        }
        if (dArr2 != null) {
            dArr2[0] = d14;
            double d18 = d16 * d7;
            dArr2[1] = (d18 * d13) + d18 + (d16 * d13) + d15;
        }
        return d5;
    }

    public static double expm1(double d) {
        return expm1(d, null);
    }

    private static double expm1(double d, double[] dArr) {
        boolean z;
        double d2 = d;
        if (d2 != d2 || d2 == 0.0d) {
            return d2;
        }
        if (d2 <= -1.0d || d2 >= 1.0d) {
            double[] dArr2 = new double[2];
            exp(d2, 0.0d, dArr2);
            if (d2 > 0.0d) {
                return (dArr2[0] - 4.0d) + dArr2[1];
            }
            double d3 = dArr2[0] - 4.0d;
            return d3 + (-((1.0d + d3) - dArr2[0])) + dArr2[1];
        }
        if (d2 < 0.0d) {
            d2 = -d2;
            z = true;
        } else {
            z = false;
        }
        int i = (int) (d2 * 1024.0d);
        double d4 = ExpFracTable.EXP_FRAC_TABLE_A[i] - 1.0d;
        double d5 = ExpFracTable.EXP_FRAC_TABLE_B[i];
        double d6 = d4 + d5;
        double d7 = d6 * 1.073741824E9d;
        double d8 = (d6 + d7) - d7;
        double d9 = (-((d6 - d4) - d5)) + (d6 - d8);
        double d10 = (double) i;
        Double.isNaN(d10);
        double d11 = d2 - (d10 / 1024.0d);
        double d12 = ((((((0.008336750013465571d * d11) + 0.041666663879186654d) * d11) + 0.16666666666745392d) * d11) + 0.49999999999999994d) * d11 * d11;
        double d13 = d11 + d12;
        double d14 = d13 * 1.073741824E9d;
        double d15 = (d13 + d14) - d14;
        double d16 = (-((d13 - d11) - d12)) + (d13 - d15);
        double d17 = d15 * d8;
        double d18 = d15 * d9;
        double d19 = d17 + d18;
        double d20 = d16 * d8;
        double d21 = d19 + d20;
        double d22 = (-((d19 - d17) - d18)) + (-((d21 - d19) - d20));
        double d23 = d16 * d9;
        double d24 = d21 + d23;
        double d25 = d22 + (-((d24 - d21) - d23));
        double d26 = d24 + d8;
        double d27 = d26 + d15;
        double d28 = d25 + (-((d26 - d8) - d24)) + (-((d27 - d26) - d15));
        double d29 = d27 + d9;
        double d30 = d28 + (-((d29 - d27) - d9));
        double d31 = d29 + d16;
        double d32 = d30 + (-((d31 - d29) - d16));
        if (z) {
            double d33 = d31 + 1.0d;
            double d34 = 1.0d / d33;
            double d35 = d31 * d34;
            double d36 = d35 * 1.073741824E9d;
            double d37 = (-((d33 - 1.0d) - d31)) + d32;
            double d38 = (d35 + d36) - d36;
            double d39 = d35 - d38;
            double d40 = 1.073741824E9d * d33;
            double d41 = (d33 + d40) - d40;
            double d42 = d33 - d41;
            d31 = -d38;
            d32 = -(d39 + (((((d31 - (d41 * d38)) - (d41 * d39)) - (d42 * d38)) - (d42 * d39)) * d34) + (d32 * d34) + ((-d31) * d37 * d34 * d34));
        }
        if (dArr != null) {
            dArr[0] = d31;
            dArr[1] = d32;
        }
        return d31 + d32;
    }

    public static double log(double d) {
        return log(d, (double[]) null);
    }

    private static double log(double d, double[] dArr) {
        double d2;
        double d3;
        if (d == 0.0d) {
            return Double.NEGATIVE_INFINITY;
        }
        long doubleToRawLongBits = Double.doubleToRawLongBits(d);
        if (((Long.MIN_VALUE & doubleToRawLongBits) != 0 || d != d) && d != 0.0d) {
            if (dArr != null) {
                dArr[0] = Double.NaN;
            }
            return Double.NaN;
        } else if (d == Double.POSITIVE_INFINITY) {
            if (dArr != null) {
                dArr[0] = Double.POSITIVE_INFINITY;
            }
            return Double.POSITIVE_INFINITY;
        } else {
            int i = ((int) (doubleToRawLongBits >> 52)) - 1023;
            if ((9218868437227405312L & doubleToRawLongBits) == 0) {
                if (d == 0.0d) {
                    if (dArr != null) {
                        dArr[0] = Double.NEGATIVE_INFINITY;
                    }
                    return Double.NEGATIVE_INFINITY;
                }
                doubleToRawLongBits <<= 1;
                while ((4503599627370496L & doubleToRawLongBits) == 0) {
                    i--;
                    doubleToRawLongBits <<= 1;
                }
            }
            if ((i == -1 || i == 0) && d < 1.01d && d > 0.99d && dArr == null) {
                double d4 = d - 1.0d;
                double d5 = d4 * 1.073741824E9d;
                double d6 = (d4 + d5) - d5;
                double d7 = d4 - d6;
                double[][] dArr2 = LN_QUICK_COEF;
                double[] dArr3 = dArr2[dArr2.length - 1];
                double d8 = dArr3[0];
                double d9 = dArr3[1];
                for (int length = dArr2.length - 2; length >= 0; length--) {
                    double d10 = d8 * d6;
                    double d11 = (d8 * d7) + (d9 * d6) + (d9 * d7);
                    double d12 = d10 * 1.073741824E9d;
                    double d13 = (d10 + d12) - d12;
                    double d14 = (d10 - d13) + d11;
                    double[] dArr4 = LN_QUICK_COEF[length];
                    double d15 = d13 + dArr4[0];
                    double d16 = d15 * 1.073741824E9d;
                    d8 = (d15 + d16) - d16;
                    d9 = (d15 - d8) + d14 + dArr4[1];
                }
                double d17 = d8 * d6;
                double d18 = 1.073741824E9d * d17;
                double d19 = (d17 + d18) - d18;
                return d19 + (d17 - d19) + (d8 * d7) + (d6 * d9) + (d9 * d7);
            }
            long j = 4499201580859392L & doubleToRawLongBits;
            double[] dArr5 = lnMant.LN_MANT[(int) (j >> 42)];
            double d20 = (double) (doubleToRawLongBits & 4398046511103L);
            double d21 = (double) j;
            Double.isNaN(d21);
            double d22 = d21 + TWO_POWER_52;
            Double.isNaN(d20);
            double d23 = d20 / d22;
            if (dArr != null) {
                double d24 = d23 * 1.073741824E9d;
                double d25 = (d23 + d24) - d24;
                double d26 = d23 - d25;
                Double.isNaN(d20);
                double d27 = d26 + (((d20 - (d25 * d22)) - (d26 * d22)) / d22);
                double[][] dArr6 = LN_HI_PREC_COEF;
                double[] dArr7 = dArr6[dArr6.length - 1];
                double d28 = dArr7[0];
                double d29 = dArr7[1];
                for (int length2 = dArr6.length - 2; length2 >= 0; length2--) {
                    double d30 = d28 * d25;
                    double d31 = (d28 * d27) + (d29 * d25) + (d29 * d27);
                    double d32 = d30 * 1.073741824E9d;
                    double d33 = (d30 + d32) - d32;
                    double d34 = (d30 - d33) + d31;
                    double[] dArr8 = LN_HI_PREC_COEF[length2];
                    double d35 = d33 + dArr8[0];
                    double d36 = d35 * 1.073741824E9d;
                    d28 = (d35 + d36) - d36;
                    d29 = (d35 - d28) + d34 + dArr8[1];
                }
                double d37 = d28 * d25;
                double d38 = (d28 * d27) + (d25 * d29) + (d29 * d27);
                d2 = d37 + d38;
                d3 = -((d2 - d37) - d38);
            } else {
                d2 = d23 * ((((((((((-0.16624882440418567d * d23) + 0.19999954120254515d) * d23) - 16.00000002972804d) * d23) + 0.3333333333332802d) * d23) - 8.0d) * d23) + 1.0d);
                d3 = 0.0d;
            }
            double d39 = (double) i;
            Double.isNaN(d39);
            double d40 = LN_2_A * d39;
            double d41 = dArr5[0] + d40;
            double d42 = d41 + d2;
            double d43 = (-((d41 - d40) - dArr5[0])) + 0.0d + (-((d42 - d41) - d2));
            Double.isNaN(d39);
            double d44 = d39 * LN_2_B;
            double d45 = d42 + d44;
            double d46 = d43 + (-((d45 - d42) - d44));
            double d47 = dArr5[1] + d45;
            double d48 = d46 + (-((d47 - d45) - dArr5[1]));
            double d49 = d47 + d3;
            double d50 = d48 + (-((d49 - d47) - d3));
            if (dArr != null) {
                dArr[0] = d49;
                dArr[1] = d50;
            }
            return d49 + d50;
        }
    }

    public static double log1p(double d) {
        if (d == -1.0d) {
            return Double.NEGATIVE_INFINITY;
        }
        if (d == Double.POSITIVE_INFINITY) {
            return Double.POSITIVE_INFINITY;
        }
        if (d <= 1.0E-6d && d >= -1.0E-6d) {
            return ((((F_1_3 * d) - 0.5d) * d) + 1.0d) * d;
        }
        double d2 = d + 1.0d;
        double d3 = -((d2 - 1.0d) - d);
        double[] dArr = new double[2];
        double log = log(d2, dArr);
        if (Double.isInfinite(log)) {
            return log;
        }
        double d4 = d3 / d2;
        return (((0.5d * d4) + 1.0d) * d4) + dArr[1] + dArr[0];
    }

    public static double log10(double d) {
        double[] dArr = new double[2];
        double log = log(d, dArr);
        if (Double.isInfinite(log)) {
            return log;
        }
        double d2 = dArr[0] * 1.073741824E9d;
        double d3 = (dArr[0] + d2) - d2;
        double d4 = (dArr[0] - d3) + dArr[1];
        return (d4 * 1.9699272335463627E-8d) + (1.9699272335463627E-8d * d3) + (d4 * 0.4342944622039795d) + (d3 * 0.4342944622039795d);
    }

    public static double log(double d, double d2) {
        return log(d2) / log(d);
    }

    public static double pow(double d, double d2) {
        double d3;
        double d4 = d;
        double d5 = d2;
        double[] dArr = new double[2];
        if (d5 == 0.0d) {
            return 1.0d;
        }
        if (d4 != d4) {
            return d4;
        }
        if (d4 == 0.0d) {
            if ((Double.doubleToRawLongBits(d) & Long.MIN_VALUE) != 0) {
                long j = (long) d5;
                if (d5 < 0.0d && d5 == ((double) j) && (j & 1) == 1) {
                    return Double.NEGATIVE_INFINITY;
                }
                if (d5 > 0.0d && d5 == ((double) j) && (j & 1) == 1) {
                    return -0.0d;
                }
            }
            if (d5 < 0.0d) {
                return Double.POSITIVE_INFINITY;
            }
            return d5 > 0.0d ? 0.0d : Double.NaN;
        } else if (d4 == Double.POSITIVE_INFINITY) {
            if (d5 != d5) {
                return d5;
            }
            return d5 < 0.0d ? 0.0d : Double.POSITIVE_INFINITY;
        } else if (d5 == Double.POSITIVE_INFINITY) {
            double d6 = d4 * d4;
            if (d6 == 1.0d) {
                return Double.NaN;
            }
            return d6 > 1.0d ? Double.POSITIVE_INFINITY : 0.0d;
        } else {
            if (d4 == Double.NEGATIVE_INFINITY) {
                if (d5 != d5) {
                    return d5;
                }
                if (d5 < 0.0d) {
                    long j2 = (long) d5;
                    return (d5 == ((double) j2) && (j2 & 1) == 1) ? -0.0d : 0.0d;
                } else if (d5 > 0.0d) {
                    long j3 = (long) d5;
                    return (d5 == ((double) j3) && (j3 & 1) == 1) ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY;
                }
            }
            if (d5 == Double.NEGATIVE_INFINITY) {
                double d7 = d4 * d4;
                if (d7 == 1.0d) {
                    return Double.NaN;
                }
                return d7 < 1.0d ? Double.POSITIVE_INFINITY : 0.0d;
            } else if (d4 >= 0.0d) {
                if (d5 >= 8.0E298d || d5 <= -8.0E298d) {
                    double d8 = d5 * 9.313225746154785E-10d;
                    d3 = (((9.313225746154785E-10d * d8) + d8) - d8) * 1.073741824E9d * 1.073741824E9d;
                } else {
                    double d9 = d5 * 1.073741824E9d;
                    d3 = (d5 + d9) - d9;
                }
                double d10 = d5 - d3;
                double log = log(d4, dArr);
                if (Double.isInfinite(log)) {
                    return log;
                }
                double d11 = dArr[0];
                double d12 = 1.073741824E9d * d11;
                double d13 = (d11 + d12) - d12;
                double d14 = dArr[1] + (d11 - d13);
                double d15 = d13 * d3;
                double d16 = (d13 * d10) + (d3 * d14) + (d14 * d10);
                double d17 = d15 + d16;
                double d18 = -((d17 - d15) - d16);
                return exp(d17, ((((((((0.008333333333333333d * d18) + 0.041666666666666664d) * d18) + 0.16666666666666666d) * d18) + 0.5d) * d18) + 1.0d) * d18, null);
            } else if (d5 >= TWO_POWER_53 || d5 <= -9.007199254740992E15d) {
                return pow(-d4, d5);
            } else {
                long j4 = (long) d5;
                if (d5 != ((double) j4)) {
                    return Double.NaN;
                }
                int i = ((j4 & 1) > 0 ? 1 : ((j4 & 1) == 0 ? 0 : -1));
                double pow = pow(-d4, d5);
                return i == 0 ? pow : -pow;
            }
        }
    }

    private static double sinQ(double d, double d2) {
        double d3;
        int i = (int) ((8.0d * d) + 0.5d);
        double d4 = d - EIGHTHS[i];
        double d5 = SINE_TABLE_A[i];
        double d6 = SINE_TABLE_B[i];
        double d7 = COSINE_TABLE_A[i];
        double d8 = COSINE_TABLE_B[i];
        double polySine = polySine(d4);
        double polyCosine = polyCosine(d4);
        double d9 = 1.073741824E9d * d4;
        double d10 = (d4 + d9) - d9;
        double d11 = polySine + (d4 - d10);
        double d12 = d5 + 0.0d;
        double d13 = d8;
        double d14 = d7 * d10;
        double d15 = d12 + d14;
        double d16 = (-((d12 - 0.0d) - d5)) + 0.0d + (-((d15 - d12) - d14)) + (d5 * polyCosine) + (d7 * d11) + d6 + (d13 * d10) + (d6 * polyCosine) + (d13 * d11);
        if (d2 != 0.0d) {
            double d17 = (((d7 + d13) * (polyCosine + 1.0d)) - ((d5 + d6) * (d10 + d11))) * d2;
            d3 = d15 + d17;
            d16 += -((d3 - d15) - d17);
        } else {
            d3 = d15;
        }
        return d3 + d16;
    }

    private static double cosQ(double d, double d2) {
        double d3 = 1.5707963267948966d - d;
        return sinQ(d3, (-((d3 - 1.5707963267948966d) + d)) + (6.123233995736766E-17d - d2));
    }

    private static double tanQ(double d, double d2, boolean z) {
        int i = (int) ((8.0d * d) + 0.5d);
        double d3 = d - EIGHTHS[i];
        double d4 = SINE_TABLE_A[i];
        double d5 = SINE_TABLE_B[i];
        double d6 = COSINE_TABLE_A[i];
        double d7 = COSINE_TABLE_B[i];
        double polySine = polySine(d3);
        double polyCosine = polyCosine(d3);
        double d8 = d3 * 1.073741824E9d;
        double d9 = (d3 + d8) - d8;
        double d10 = polySine + (d3 - d9);
        double d11 = d4 + 0.0d;
        double d12 = d5;
        double d13 = d6 * d9;
        double d14 = d11 + d13;
        double d15 = (-((d11 - 0.0d) - d4)) + 0.0d + (-((d14 - d11) - d13)) + (d4 * polyCosine) + (d6 * d10) + d12 + (d7 * d9) + (d12 * polyCosine) + (d7 * d10);
        double d16 = d14 + d15;
        double d17 = -((d16 - d14) - d15);
        double d18 = d6 * 1.0d;
        double d19 = d18 + 0.0d;
        double d20 = d16;
        double d21 = (-d4) * d9;
        double d22 = d19 + d21;
        double d23 = ((((-((d19 - 0.0d) - d18)) + 0.0d) + (-((d22 - d19) - d21))) + (((1.0d * d7) + (d6 * polyCosine)) + (d7 * polyCosine))) - (((d12 * d9) + (d4 * d10)) + (d12 * d10));
        double d24 = d22 + d23;
        double d25 = -((d24 - d22) - d23);
        if (!z) {
            double d26 = d24;
            d24 = d20;
            d20 = d26;
            double d27 = d25;
            d25 = d17;
            d17 = d27;
        }
        double d28 = d24 / d20;
        double d29 = d28 * 1.073741824E9d;
        double d30 = (d28 + d29) - d29;
        double d31 = d28 - d30;
        double d32 = 1.073741824E9d * d20;
        double d33 = (d20 + d32) - d32;
        double d34 = d20 - d33;
        double d35 = (((((d24 - (d30 * d33)) - (d30 * d34)) - (d33 * d31)) - (d31 * d34)) / d20) + (d25 / d20) + ((((-d24) * d17) / d20) / d20);
        if (d2 != 0.0d) {
            double d36 = d2 + (d28 * d28 * d2);
            if (z) {
                d36 = -d36;
            }
            d35 += d36;
        }
        return d28 + d35;
    }

    private static void reducePayneHanek(double d, double[] dArr) {
        long j;
        long j2;
        long j3;
        long doubleToRawLongBits = Double.doubleToRawLongBits(d);
        int i = (((int) ((doubleToRawLongBits >> 52) & 2047)) - 1023) + 1;
        long j4 = ((doubleToRawLongBits & 4503599627370495L) | 4503599627370496L) << 11;
        int i2 = i >> 6;
        int i3 = i - (i2 << 6);
        if (i3 != 0) {
            long j5 = i2 == 0 ? 0 : RECIP_2PI[i2 - 1] << i3;
            long[] jArr = RECIP_2PI;
            int i4 = 64 - i3;
            j3 = j5 | (jArr[i2] >>> i4);
            int i5 = i2 + 1;
            j2 = (jArr[i2] << i3) | (jArr[i5] >>> i4);
            j = (jArr[i2 + 2] >>> i4) | (jArr[i5] << i3);
        } else {
            if (i2 == 0) {
                j3 = 0;
            } else {
                j3 = RECIP_2PI[i2 - 1];
            }
            long[] jArr2 = RECIP_2PI;
            j2 = jArr2[i2];
            j = jArr2[i2 + 1];
        }
        long j6 = j4 >>> 32;
        long j7 = j4 & 4294967295L;
        long j8 = j2 >>> 32;
        long j9 = j2 & 4294967295L;
        long j10 = j6 * j8;
        long j11 = j7 * j9;
        long j12 = j8 * j7;
        long j13 = j9 * j6;
        long j14 = j11 + (j13 << 32);
        long j15 = j10 + (j13 >>> 32);
        boolean z = (j11 & Long.MIN_VALUE) != 0;
        boolean z2 = (j13 & 2147483648L) != 0;
        long j16 = j14 & Long.MIN_VALUE;
        boolean z3 = j16 != 0;
        if ((z && z2) || ((z || z2) && !z3)) {
            j15++;
        }
        boolean z4 = j16 != 0;
        boolean z5 = (j12 & 2147483648L) != 0;
        long j17 = j14 + (j12 << 32);
        long j18 = j15 + (j12 >>> 32);
        long j19 = j17 & Long.MIN_VALUE;
        boolean z6 = j19 != 0;
        if ((z4 && z5) || ((z4 || z5) && !z6)) {
            j18++;
        }
        long j20 = j >>> 32;
        long j21 = (j6 * j20) + (((j20 * j7) + ((j & 4294967295L) * j6)) >>> 32);
        boolean z7 = j19 != 0;
        boolean z8 = (j21 & Long.MIN_VALUE) != 0;
        long j22 = j17 + j21;
        boolean z9 = (j22 & Long.MIN_VALUE) != 0;
        if ((z7 && z8) || ((z7 || z8) && !z9)) {
            j18++;
        }
        long j23 = j3 >>> 32;
        long j24 = j3 & 4294967295L;
        long j25 = j18 + (j7 * j24) + (((j7 * j23) + (j6 * j24)) << 32);
        int i6 = (int) (j25 >>> 62);
        long j26 = (j25 << 2) | (j22 >>> 62);
        long j27 = j22 << 2;
        long j28 = j26 >>> 32;
        long j29 = j26 & 4294967295L;
        long[] jArr3 = PI_O_4_BITS;
        long j30 = jArr3[0] >>> 32;
        long j31 = jArr3[0] & 4294967295L;
        long j32 = j28 * j30;
        long j33 = j29 * j31;
        long j34 = j30 * j29;
        long j35 = j31 * j28;
        long j36 = j33 + (j35 << 32);
        long j37 = j32 + (j35 >>> 32);
        boolean z10 = (j33 & Long.MIN_VALUE) != 0;
        boolean z11 = (j35 & 2147483648L) != 0;
        long j38 = j36 & Long.MIN_VALUE;
        boolean z12 = j38 != 0;
        if ((z10 && z11) || ((z10 || z11) && !z12)) {
            j37++;
        }
        boolean z13 = j38 != 0;
        boolean z14 = (j34 & 2147483648L) != 0;
        long j39 = j36 + (j34 << 32);
        long j40 = j37 + (j34 >>> 32);
        long j41 = j39 & Long.MIN_VALUE;
        boolean z15 = j41 != 0;
        if ((z13 && z14) || ((z13 || z14) && !z15)) {
            j40++;
        }
        long[] jArr4 = PI_O_4_BITS;
        long j42 = jArr4[1] >>> 32;
        long j43 = (j28 * j42) + (((j29 * j42) + (j28 * (jArr4[1] & 4294967295L))) >>> 32);
        boolean z16 = j41 != 0;
        boolean z17 = (j43 & Long.MIN_VALUE) != 0;
        long j44 = j39 + j43;
        long j45 = j44 & Long.MIN_VALUE;
        boolean z18 = j45 != 0;
        if ((z16 && z17) || ((z16 || z17) && !z18)) {
            j40++;
        }
        long j46 = j27 >>> 32;
        long[] jArr5 = PI_O_4_BITS;
        long j47 = jArr5[0] >>> 32;
        long j48 = (j46 * j47) + ((((j27 & 4294967295L) * j47) + (j46 * (jArr5[0] & 4294967295L))) >>> 32);
        boolean z19 = j45 != 0;
        boolean z20 = (j48 & Long.MIN_VALUE) != 0;
        long j49 = j44 + j48;
        boolean z21 = (j49 & Long.MIN_VALUE) != 0;
        if ((z19 && z20) || ((z19 || z20) && !z21)) {
            j40++;
        }
        double d2 = (double) (j40 >>> 12);
        Double.isNaN(d2);
        double d3 = d2 / TWO_POWER_52;
        double d4 = (double) (((j40 & 4095) << 40) + (j49 >>> 24));
        Double.isNaN(d4);
        double d5 = (d4 / TWO_POWER_52) / TWO_POWER_52;
        double d6 = d3 + d5;
        dArr[0] = (double) i6;
        dArr[1] = d6 * 2.0d;
        dArr[2] = (-((d6 - d3) - d5)) * 2.0d;
    }

    public static double sin(double d) {
        boolean z;
        double d2;
        double d3;
        double sinQ;
        int i = 0;
        if (d < 0.0d) {
            d2 = -d;
            z = true;
        } else {
            z = false;
            d2 = d;
        }
        if (d2 == 0.0d) {
            if (Double.doubleToRawLongBits(d) < 0) {
                return -0.0d;
            }
            return 0.0d;
        } else if (d2 != d2 || d2 == Double.POSITIVE_INFINITY) {
            return Double.NaN;
        } else {
            if (d2 > 3294198.0d) {
                double[] dArr = new double[3];
                reducePayneHanek(d2, dArr);
                i = ((int) dArr[0]) & 3;
                d2 = dArr[1];
                d3 = dArr[2];
            } else if (d2 > 1.5707963267948966d) {
                CodyWaite codyWaite = new CodyWaite(d2);
                d2 = codyWaite.getRemA();
                d3 = codyWaite.getRemB();
                i = codyWaite.getK() & 3;
            } else {
                d3 = 0.0d;
            }
            if (z) {
                i ^= 2;
            }
            if (i == 0) {
                return sinQ(d2, d3);
            }
            if (i == 1) {
                return cosQ(d2, d3);
            }
            if (i == 2) {
                sinQ = sinQ(d2, d3);
            } else if (i != 3) {
                return Double.NaN;
            } else {
                sinQ = cosQ(d2, d3);
            }
            return -sinQ;
        }
    }

    public static double cos(double d) {
        double sinQ;
        double d2 = 0.0d;
        if (d < 0.0d) {
            d = -d;
        }
        if (d != d || d == Double.POSITIVE_INFINITY) {
            return Double.NaN;
        }
        int i = 0;
        if (d > 3294198.0d) {
            double[] dArr = new double[3];
            reducePayneHanek(d, dArr);
            i = ((int) dArr[0]) & 3;
            d = dArr[1];
            d2 = dArr[2];
        } else if (d > 1.5707963267948966d) {
            CodyWaite codyWaite = new CodyWaite(d);
            i = codyWaite.getK() & 3;
            d = codyWaite.getRemA();
            d2 = codyWaite.getRemB();
        }
        if (i == 0) {
            return cosQ(d, d2);
        }
        if (i == 1) {
            sinQ = sinQ(d, d2);
        } else if (i == 2) {
            sinQ = cosQ(d, d2);
        } else if (i != 3) {
            return Double.NaN;
        } else {
            return sinQ(d, d2);
        }
        return -sinQ;
    }

    public static double tan(double d) {
        boolean z;
        double d2;
        int i;
        double d3;
        double d4;
        double d5;
        double d6 = 0.0d;
        if (d < 0.0d) {
            d2 = -d;
            z = true;
        } else {
            z = false;
            d2 = d;
        }
        if (d2 == 0.0d) {
            if (Double.doubleToRawLongBits(d) < 0) {
                return -0.0d;
            }
            return 0.0d;
        } else if (d2 != d2 || d2 == Double.POSITIVE_INFINITY) {
            return Double.NaN;
        } else {
            if (d2 > 3294198.0d) {
                double[] dArr = new double[3];
                reducePayneHanek(d2, dArr);
                i = ((int) dArr[0]) & 3;
                d2 = dArr[1];
                d6 = dArr[2];
            } else if (d2 > 1.5707963267948966d) {
                CodyWaite codyWaite = new CodyWaite(d2);
                i = codyWaite.getK() & 3;
                d2 = codyWaite.getRemA();
                d6 = codyWaite.getRemB();
            } else {
                i = 0;
            }
            if (d2 > 1.5d) {
                double d7 = 1.5707963267948966d - d2;
                double d8 = (-((d7 - 1.5707963267948966d) + d2)) + (6.123233995736766E-17d - d6);
                d4 = d7 + d8;
                d3 = -((d4 - d7) - d8);
                i ^= 1;
                z = !z;
            } else {
                double d9 = d6;
                d4 = d2;
                d3 = d9;
            }
            if ((i & 1) == 0) {
                d5 = tanQ(d4, d3, false);
            } else {
                d5 = -tanQ(d4, d3, true);
            }
            return z ? -d5 : d5;
        }
    }

    public static double atan(double d) {
        return atan(d, 0.0d, false);
    }

    private static double atan(double d, double d2, boolean z) {
        boolean z2;
        double d3;
        int i;
        double d4;
        double d5;
        double d6 = d;
        if (d6 != 0.0d) {
            if (d6 < 0.0d) {
                d6 = -d6;
                d3 = -d2;
                z2 = true;
            } else {
                d3 = d2;
                z2 = false;
            }
            if (d6 > 1.633123935319537E16d) {
                return z2 ^ z ? -1.5707963267948966d : 1.5707963267948966d;
            }
            if (d6 < 1.0d) {
                i = (int) ((((-1.7168146928204135d * d6 * d6) + 8.0d) * d6) + 0.5d);
            } else {
                double d7 = 1.0d / d6;
                i = (int) ((-(((-1.7168146928204135d * d7 * d7) + 8.0d) * d7)) + 13.07d);
            }
            double d8 = TANGENT_TABLE_A[i];
            double d9 = TANGENT_TABLE_B[i];
            double d10 = d6 - d8;
            double d11 = (-((d10 - d6) + d8)) + (d3 - d9);
            double d12 = d10 + d11;
            double d13 = -((d12 - d10) - d11);
            double d14 = d6 * 1.073741824E9d;
            double d15 = (d6 + d14) - d14;
            double d16 = d3 + ((d6 + d3) - d15);
            if (i == 0) {
                double d17 = 1.0d / (((d15 + d16) * (d8 + d9)) + 1.0d);
                d4 = d12 * d17;
                d5 = d13 * d17;
            } else {
                double d18 = d15 * d8;
                double d19 = d18 + 1.0d;
                double d20 = (d8 * d16) + (d15 * d9);
                double d21 = d19 + d20;
                double d22 = (-((d19 - 1.0d) - d18)) + (-((d21 - d19) - d20)) + (d16 * d9);
                double d23 = d12 / d21;
                double d24 = d23 * 1.073741824E9d;
                double d25 = (d23 + d24) - d24;
                double d26 = d23 - d25;
                double d27 = 1.073741824E9d * d21;
                double d28 = (d21 + d27) - d27;
                double d29 = d21 - d28;
                d5 = (((((d12 - (d25 * d28)) - (d25 * d29)) - (d28 * d26)) - (d26 * d29)) / d21) + ((((-d12) * d22) / d21) / d21) + (d13 / d21);
                d4 = d23;
            }
            double d30 = d4 * d4;
            double d31 = ((((((((((0.07490822288864472d * d30) - 0.09088450866185192d) * d30) + 0.11111095942313305d) * d30) - 0.1428571423679182d) * d30) + 0.19999999999923582d) * d30) - 0.33333333333333287d) * d30 * d4;
            double d32 = d4 + d31;
            double d33 = (-((d32 - d4) - d31)) + (d5 / (d30 + 1.0d));
            double d34 = EIGHTHS[i];
            double d35 = d34 + d32;
            double d36 = d35 + d33;
            double d37 = (-((d35 - d34) - d32)) + (-((d36 - d35) - d33));
            double d38 = d36 + d37;
            if (z) {
                double d39 = 3.141592653589793d - d38;
                d38 = (-((d39 - 3.141592653589793d) + d38)) + (1.2246467991473532E-16d - (-((d38 - d36) - d37))) + d39;
            }
            return z2 ^ z ? -d38 : d38;
        } else if (z) {
            return copySign(3.141592653589793d, d6);
        } else {
            return d6;
        }
    }

    public static double atan2(double d, double d2) {
        double d3 = d;
        if (d2 != d2 || d3 != d3) {
            return Double.NaN;
        }
        if (d3 == 0.0d) {
            double d4 = d2 * d3;
            double d5 = 1.0d / d2;
            double d6 = 1.0d / d3;
            if (d5 == 0.0d) {
                return d2 > 0.0d ? d3 : copySign(3.141592653589793d, d3);
            }
            if (d2 < 0.0d || d5 < 0.0d) {
                return (d3 < 0.0d || d6 < 0.0d) ? -3.141592653589793d : 3.141592653589793d;
            }
            return d4;
        } else if (d3 == Double.POSITIVE_INFINITY) {
            if (d2 == Double.POSITIVE_INFINITY) {
                return 0.7853981633974483d;
            }
            return d2 == Double.NEGATIVE_INFINITY ? 2.356194490192345d : 1.5707963267948966d;
        } else if (d3 != Double.NEGATIVE_INFINITY) {
            if (d2 == Double.POSITIVE_INFINITY) {
                if (d3 <= 0.0d) {
                    double d7 = 1.0d / d3;
                    if (d7 <= 0.0d) {
                        if (d3 < 0.0d || d7 < 0.0d) {
                            return -0.0d;
                        }
                    }
                }
                return 0.0d;
            }
            if (d2 == Double.NEGATIVE_INFINITY) {
                if (d3 <= 0.0d) {
                    double d8 = 1.0d / d3;
                    if (d8 <= 0.0d) {
                        if (d3 < 0.0d || d8 < 0.0d) {
                            return -3.141592653589793d;
                        }
                    }
                }
                return 3.141592653589793d;
            }
            if (d2 == 0.0d) {
                if (d3 <= 0.0d) {
                    double d9 = 1.0d / d3;
                    if (d9 <= 0.0d) {
                        if (d3 < 0.0d || d9 < 0.0d) {
                            return -1.5707963267948966d;
                        }
                    }
                }
                return 1.5707963267948966d;
            }
            double d10 = d3 / d2;
            boolean z = true;
            if (Double.isInfinite(d10)) {
                if (d2 >= 0.0d) {
                    z = false;
                }
                return atan(d10, 0.0d, z);
            }
            double doubleHighPart = doubleHighPart(d10);
            double d11 = d10 - doubleHighPart;
            double doubleHighPart2 = doubleHighPart(d2);
            double d12 = d2 - doubleHighPart2;
            double d13 = d11 + (((((d3 - (doubleHighPart * doubleHighPart2)) - (doubleHighPart * d12)) - (doubleHighPart2 * d11)) - (d12 * d11)) / d2);
            double d14 = doubleHighPart + d13;
            double d15 = -((d14 - doubleHighPart) - d13);
            if (d14 == 0.0d) {
                d14 = copySign(0.0d, d3);
            }
            if (d2 >= 0.0d) {
                z = false;
            }
            return atan(d14, d15, z);
        } else if (d2 == Double.POSITIVE_INFINITY) {
            return -0.7853981633974483d;
        } else {
            return d2 == Double.NEGATIVE_INFINITY ? -2.356194490192345d : -1.5707963267948966d;
        }
    }

    public static double asin(double d) {
        double d2 = d;
        if (d2 != d2 || d2 > 1.0d || d2 < -1.0d) {
            return Double.NaN;
        }
        if (d2 == 1.0d) {
            return 1.5707963267948966d;
        }
        if (d2 == -1.0d) {
            return -1.5707963267948966d;
        }
        if (d2 == 0.0d) {
            return d2;
        }
        double d3 = d2 * 1.073741824E9d;
        double d4 = (d2 + d3) - d3;
        double d5 = d2 - d4;
        double d6 = d4 * d4;
        double d7 = (d4 * d5 * 2.0d) + (d5 * d5);
        double d8 = -d6;
        double d9 = -d7;
        double d10 = d8 + 1.0d;
        double d11 = d10 + d9;
        double d12 = (-((d10 - 1.0d) - d8)) + (-((d11 - d10) - d9));
        double sqrt = sqrt(d11);
        double d13 = sqrt * 1.073741824E9d;
        double d14 = (sqrt + d13) - d13;
        double d15 = sqrt - d14;
        double d16 = 2.0d * sqrt;
        double d17 = d15 + ((((d11 - (d14 * d14)) - ((d14 * 2.0d) * d15)) - (d15 * d15)) / d16);
        double d18 = d12 / d16;
        double d19 = d2 / sqrt;
        double d20 = 1.073741824E9d * d19;
        double d21 = (d19 + d20) - d20;
        double d22 = d19 - d21;
        double d23 = d22 + (((((d2 - (d21 * d14)) - (d21 * d17)) - (d14 * d22)) - (d17 * d22)) / sqrt) + ((((-d2) * d18) / sqrt) / sqrt);
        double d24 = d21 + d23;
        return atan(d24, -((d24 - d21) - d23), false);
    }

    public static double acos(double d) {
        if (d != d || d > 1.0d || d < -1.0d) {
            return Double.NaN;
        }
        if (d == -1.0d) {
            return 3.141592653589793d;
        }
        if (d == 1.0d) {
            return 0.0d;
        }
        if (d == 0.0d) {
            return 1.5707963267948966d;
        }
        double d2 = d * 1.073741824E9d;
        double d3 = (d + d2) - d2;
        double d4 = d - d3;
        double d5 = -(d3 * d3);
        double d6 = -((d3 * d4 * 2.0d) + (d4 * d4));
        double d7 = d5 + 1.0d;
        double d8 = d7 + d6;
        double d9 = (-((d7 - 1.0d) - d5)) + (-((d8 - d7) - d6));
        double sqrt = sqrt(d8);
        double d10 = 1.073741824E9d * sqrt;
        double d11 = (sqrt + d10) - d10;
        double d12 = sqrt - d11;
        double d13 = sqrt * 2.0d;
        double d14 = d12 + ((((d8 - (d11 * d11)) - ((d11 * 2.0d) * d12)) - (d12 * d12)) / d13) + (d9 / d13);
        double d15 = d11 + d14;
        double d16 = -((d15 - d11) - d14);
        double d17 = d15 / d;
        if (Double.isInfinite(d17)) {
            return 1.5707963267948966d;
        }
        double doubleHighPart = doubleHighPart(d17);
        double d18 = d17 - doubleHighPart;
        double d19 = d18 + (((((d15 - (doubleHighPart * d3)) - (doubleHighPart * d4)) - (d3 * d18)) - (d4 * d18)) / d) + (d16 / d);
        double d20 = doubleHighPart + d19;
        return atan(d20, -((d20 - doubleHighPart) - d19), d < 0.0d);
    }

    public static double cbrt(double d) {
        long j;
        double d2;
        int i;
        boolean z;
        long doubleToRawLongBits = Double.doubleToRawLongBits(d);
        int i2 = ((int) ((doubleToRawLongBits >> 52) & 2047)) - 1023;
        if (i2 != -1023) {
            j = doubleToRawLongBits;
            i = i2;
            z = false;
            d2 = d;
        } else if (d == 0.0d) {
            return d;
        } else {
            z = true;
            d2 = 1.8014398509481984E16d * d;
            j = Double.doubleToRawLongBits(d2);
            i = ((int) (2047 & (j >> 52))) - 1023;
        }
        if (i == 1024) {
            return d2;
        }
        double longBitsToDouble = Double.longBitsToDouble((((long) (((i / 3) + 1023) & 2047)) << 52) | (Long.MIN_VALUE & j));
        double longBitsToDouble2 = Double.longBitsToDouble((j & 4503599627370495L) | 4607182418800017408L);
        double d3 = ((((((((-0.010714690733195933d * longBitsToDouble2) + 0.0875862700108075d) * longBitsToDouble2) - 14.214349574856733d) * longBitsToDouble2) + 0.7249995199969751d) * longBitsToDouble2) + 0.5039018405998233d) * CBRTTWO[(i % 3) + 2];
        double d4 = d2 / ((longBitsToDouble * longBitsToDouble) * longBitsToDouble);
        double d5 = d3 + ((d4 - ((d3 * d3) * d3)) / ((d3 * 3.0d) * d3));
        double d6 = d5 + ((d4 - ((d5 * d5) * d5)) / ((d5 * 3.0d) * d5));
        double d7 = d6 * 1.073741824E9d;
        double d8 = (d6 + d7) - d7;
        double d9 = d6 - d8;
        double d10 = d8 * d8;
        double d11 = 1.073741824E9d * d10;
        double d12 = (d10 + d11) - d11;
        double d13 = (d8 * d9 * 2.0d) + (d9 * d9) + (d10 - d12);
        double d14 = d12 * d8;
        double d15 = d4 - d14;
        double d16 = (d6 + ((d15 + ((-((d15 - d4) + d14)) - (((d12 * d9) + (d8 * d13)) + (d13 * d9)))) / ((3.0d * d6) * d6))) * longBitsToDouble;
        return z ? d16 * 3.814697265625E-6d : d16;
    }

    public static double toRadians(double d) {
        if (Double.isInfinite(d) || d == 0.0d) {
            return d;
        }
        double doubleHighPart = doubleHighPart(d);
        double d2 = d - doubleHighPart;
        double d3 = (d2 * 1.997844754509471E-9d) + (d2 * 0.01745329052209854d) + (1.997844754509471E-9d * doubleHighPart) + (doubleHighPart * 0.01745329052209854d);
        return d3 == 0.0d ? d3 * d : d3;
    }

    public static double toDegrees(double d) {
        if (Double.isInfinite(d) || d == 0.0d) {
            return d;
        }
        double doubleHighPart = doubleHighPart(d);
        double d2 = d - doubleHighPart;
        return (d2 * 3.145894820876798E-6d) + (d2 * 57.2957763671875d) + (3.145894820876798E-6d * doubleHighPart) + (doubleHighPart * 57.2957763671875d);
    }

    public static float abs(float f) {
        return Float.intBitsToFloat(Float.floatToRawIntBits(f) & Integer.MAX_VALUE);
    }

    public static double abs(double d) {
        return Double.longBitsToDouble(Double.doubleToRawLongBits(d) & MASK_NON_SIGN_LONG);
    }

    public static double ulp(double d) {
        if (Double.isInfinite(d)) {
            return Double.POSITIVE_INFINITY;
        }
        return abs(d - Double.longBitsToDouble(Double.doubleToRawLongBits(d) ^ 1));
    }

    public static float ulp(float f) {
        if (Float.isInfinite(f)) {
            return Float.POSITIVE_INFINITY;
        }
        return abs(f - Float.intBitsToFloat(Float.floatToIntBits(f) ^ 1));
    }

    public static double scalb(double d, int i) {
        int i2 = i;
        if (i2 > -1023 && i2 < 1024) {
            return Double.longBitsToDouble(((long) (i2 + 1023)) << 52) * d;
        }
        if (Double.isNaN(d) || Double.isInfinite(d) || d == 0.0d) {
            return d;
        }
        if (i2 < -2098) {
            return d > 0.0d ? 0.0d : -0.0d;
        }
        if (i2 > 2097) {
            return d > 0.0d ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
        }
        long doubleToRawLongBits = Double.doubleToRawLongBits(d);
        long j = Long.MIN_VALUE & doubleToRawLongBits;
        int i3 = ((int) (doubleToRawLongBits >>> 52)) & 2047;
        long j2 = doubleToRawLongBits & 4503599627370495L;
        int i4 = i3 + i2;
        if (i2 < 0) {
            if (i4 > 0) {
                return Double.longBitsToDouble((((long) i4) << 52) | j | j2);
            }
            if (i4 <= -53) {
                return j == 0 ? 0.0d : -0.0d;
            }
            long j3 = 4503599627370496L | j2;
            long j4 = (1 << (-i4)) & j3;
            long j5 = j3 >>> (1 - i4);
            if (j4 != 0) {
                j5++;
            }
            return Double.longBitsToDouble(j5 | j);
        } else if (i3 == 0) {
            while ((j2 >>> 52) != 1) {
                j2 <<= 1;
                i4--;
            }
            int i5 = i4 + 1;
            long j6 = j2 & 4503599627370495L;
            if (i5 < 2047) {
                return Double.longBitsToDouble((((long) i5) << 52) | j | j6);
            }
            if (j == 0) {
                return Double.POSITIVE_INFINITY;
            }
            return Double.NEGATIVE_INFINITY;
        } else if (i4 < 2047) {
            return Double.longBitsToDouble((((long) i4) << 52) | j | j2);
        } else {
            if (j == 0) {
                return Double.POSITIVE_INFINITY;
            }
            return Double.NEGATIVE_INFINITY;
        }
    }

    public static float scalb(float f, int i) {
        if (i > -127 && i < 128) {
            return f * Float.intBitsToFloat((i + FMParserConstants.CLOSE_PAREN) << 23);
        }
        if (Float.isNaN(f) || Float.isInfinite(f) || f == 0.0f) {
            return f;
        }
        if (i < -277) {
            return f > 0.0f ? 0.0f : -0.0f;
        }
        if (i > 276) {
            return f > 0.0f ? Float.POSITIVE_INFINITY : Float.NEGATIVE_INFINITY;
        }
        int floatToIntBits = Float.floatToIntBits(f);
        int i2 = Integer.MIN_VALUE & floatToIntBits;
        int i3 = (floatToIntBits >>> 23) & 255;
        int i4 = floatToIntBits & 8388607;
        int i5 = i3 + i;
        if (i < 0) {
            if (i5 > 0) {
                return Float.intBitsToFloat(i4 | (i5 << 23) | i2);
            }
            if (i5 > -24) {
                int i6 = i4 | 8388608;
                int i7 = (1 << (-i5)) & i6;
                int i8 = i6 >>> (1 - i5);
                if (i7 != 0) {
                    i8++;
                }
                return Float.intBitsToFloat(i8 | i2);
            } else if (i2 == 0) {
                return 0.0f;
            } else {
                return -0.0f;
            }
        } else if (i3 == 0) {
            while ((i4 >>> 23) != 1) {
                i4 <<= 1;
                i5--;
            }
            int i9 = i5 + 1;
            int i10 = i4 & 8388607;
            if (i9 < 255) {
                return Float.intBitsToFloat(i10 | (i9 << 23) | i2);
            }
            if (i2 == 0) {
                return Float.POSITIVE_INFINITY;
            }
            return Float.NEGATIVE_INFINITY;
        } else if (i5 < 255) {
            return Float.intBitsToFloat(i4 | (i5 << 23) | i2);
        } else {
            if (i2 == 0) {
                return Float.POSITIVE_INFINITY;
            }
            return Float.NEGATIVE_INFINITY;
        }
    }

    public static double nextAfter(double d, double d2) {
        if (Double.isNaN(d) || Double.isNaN(d2)) {
            return Double.NaN;
        }
        if (d == d2) {
            return d2;
        }
        if (Double.isInfinite(d)) {
            return d < 0.0d ? -1.7976931348623157E308d : Double.MAX_VALUE;
        }
        if (d == 0.0d) {
            return d2 < 0.0d ? -4.9E-324d : Double.MIN_VALUE;
        }
        long doubleToRawLongBits = Double.doubleToRawLongBits(d);
        long j = Long.MIN_VALUE & doubleToRawLongBits;
        boolean z = true;
        boolean z2 = d2 < d;
        if (j != 0) {
            z = false;
        }
        if (z2 ^ z) {
            return Double.longBitsToDouble(j | ((doubleToRawLongBits & MASK_NON_SIGN_LONG) + 1));
        }
        return Double.longBitsToDouble(j | ((doubleToRawLongBits & MASK_NON_SIGN_LONG) - 1));
    }

    public static float nextAfter(float f, double d) {
        double d2 = (double) f;
        if (Double.isNaN(d2) || Double.isNaN(d)) {
            return Float.NaN;
        }
        if (d2 == d) {
            return (float) d;
        }
        if (Float.isInfinite(f)) {
            return f < 0.0f ? -3.4028235E38f : Float.MAX_VALUE;
        }
        if (f == 0.0f) {
            return d < 0.0d ? -1.4E-45f : Float.MIN_VALUE;
        }
        int floatToIntBits = Float.floatToIntBits(f);
        int i = Integer.MIN_VALUE & floatToIntBits;
        boolean z = false;
        boolean z2 = d < d2;
        if (i == 0) {
            z = true;
        }
        if (z2 ^ z) {
            return Float.intBitsToFloat(((floatToIntBits & Integer.MAX_VALUE) + 1) | i);
        }
        return Float.intBitsToFloat(((floatToIntBits & Integer.MAX_VALUE) - 1) | i);
    }

    public static double floor(double d) {
        if (d != d || d >= TWO_POWER_52 || d <= -4.503599627370496E15d) {
            return d;
        }
        long j = (long) d;
        if (d < 0.0d && ((double) j) != d) {
            j--;
        }
        if (j != 0) {
            return (double) j;
        }
        double d2 = (double) j;
        Double.isNaN(d2);
        return d * d2;
    }

    public static double ceil(double d) {
        if (d != d) {
            return d;
        }
        double floor = floor(d);
        if (floor == d) {
            return floor;
        }
        double d2 = floor + 1.0d;
        return d2 == 0.0d ? d * d2 : d2;
    }

    public static double rint(double d) {
        double floor = floor(d);
        double d2 = d - floor;
        if (d2 <= 0.5d) {
            return (d2 >= 0.5d && (((long) floor) & 1) != 0) ? floor + 1.0d : floor;
        }
        if (floor == -1.0d) {
            return -0.0d;
        }
        return floor + 1.0d;
    }

    public static long round(double d) {
        return (long) floor(d + 0.5d);
    }

    public static int round(float f) {
        return (int) floor((double) (f + 0.5f));
    }

    public static float min(float f, float f2) {
        if (f > f2) {
            return f2;
        }
        if (f < f2) {
            return f;
        }
        if (f != f2) {
            return Float.NaN;
        }
        return Float.floatToRawIntBits(f) == Integer.MIN_VALUE ? f : f2;
    }

    public static double min(double d, double d2) {
        if (d > d2) {
            return d2;
        }
        if (d < d2) {
            return d;
        }
        if (d != d2) {
            return Double.NaN;
        }
        return Double.doubleToRawLongBits(d) == Long.MIN_VALUE ? d : d2;
    }

    public static float max(float f, float f2) {
        if (f > f2) {
            return f;
        }
        if (f < f2) {
            return f2;
        }
        if (f != f2) {
            return Float.NaN;
        }
        return Float.floatToRawIntBits(f) == Integer.MIN_VALUE ? f2 : f;
    }

    public static double max(double d, double d2) {
        if (d > d2) {
            return d;
        }
        if (d < d2) {
            return d2;
        }
        if (d != d2) {
            return Double.NaN;
        }
        return Double.doubleToRawLongBits(d) == Long.MIN_VALUE ? d2 : d;
    }

    public static double hypot(double d, double d2) {
        if (Double.isInfinite(d) || Double.isInfinite(d2)) {
            return Double.POSITIVE_INFINITY;
        }
        if (Double.isNaN(d) || Double.isNaN(d2)) {
            return Double.NaN;
        }
        int exponent = getExponent(d);
        int exponent2 = getExponent(d2);
        if (exponent > exponent2 + 27) {
            return abs(d);
        }
        if (exponent2 > exponent + 27) {
            return abs(d2);
        }
        int i = (exponent + exponent2) / 2;
        int i2 = -i;
        double scalb = scalb(d, i2);
        double scalb2 = scalb(d2, i2);
        return scalb(sqrt((scalb * scalb) + (scalb2 * scalb2)), i);
    }

    public static double IEEEremainder(double d, double d2) {
        return StrictMath.IEEEremainder(d, d2);
    }

    public static double copySign(double d, double d2) {
        return (Double.doubleToRawLongBits(d2) ^ Double.doubleToRawLongBits(d)) >= 0 ? d : -d;
    }

    public static float copySign(float f, float f2) {
        return (Float.floatToRawIntBits(f2) ^ Float.floatToRawIntBits(f)) >= 0 ? f : -f;
    }

    public static int getExponent(double d) {
        return ((int) ((Double.doubleToRawLongBits(d) >>> 52) & 2047)) - 1023;
    }

    public static int getExponent(float f) {
        return ((Float.floatToRawIntBits(f) >>> 23) & 255) - 127;
    }

    public static void main(String[] strArr) {
        PrintStream printStream = System.out;
        FastMathCalc.printarray(printStream, "EXP_INT_TABLE_A", 1500, ExpIntTable.EXP_INT_TABLE_A);
        FastMathCalc.printarray(printStream, "EXP_INT_TABLE_B", 1500, ExpIntTable.EXP_INT_TABLE_B);
        FastMathCalc.printarray(printStream, "EXP_FRAC_TABLE_A", 1025, ExpFracTable.EXP_FRAC_TABLE_A);
        FastMathCalc.printarray(printStream, "EXP_FRAC_TABLE_B", 1025, ExpFracTable.EXP_FRAC_TABLE_B);
        FastMathCalc.printarray(printStream, "LN_MANT", 1024, lnMant.LN_MANT);
        FastMathCalc.printarray(printStream, "SINE_TABLE_A", 14, SINE_TABLE_A);
        FastMathCalc.printarray(printStream, "SINE_TABLE_B", 14, SINE_TABLE_B);
        FastMathCalc.printarray(printStream, "COSINE_TABLE_A", 14, COSINE_TABLE_A);
        FastMathCalc.printarray(printStream, "COSINE_TABLE_B", 14, COSINE_TABLE_B);
        FastMathCalc.printarray(printStream, "TANGENT_TABLE_A", 14, TANGENT_TABLE_A);
        FastMathCalc.printarray(printStream, "TANGENT_TABLE_B", 14, TANGENT_TABLE_B);
    }

    private static class ExpIntTable {
        /* access modifiers changed from: private */
        public static final double[] EXP_INT_TABLE_A = FastMathLiteralArrays.loadExpIntA();
        /* access modifiers changed from: private */
        public static final double[] EXP_INT_TABLE_B = FastMathLiteralArrays.loadExpIntB();

        private ExpIntTable() {
        }
    }

    private static class ExpFracTable {
        /* access modifiers changed from: private */
        public static final double[] EXP_FRAC_TABLE_A = FastMathLiteralArrays.loadExpFracA();
        /* access modifiers changed from: private */
        public static final double[] EXP_FRAC_TABLE_B = FastMathLiteralArrays.loadExpFracB();

        private ExpFracTable() {
        }
    }

    private static class lnMant {
        /* access modifiers changed from: private */
        public static final double[][] LN_MANT = FastMathLiteralArrays.loadLnMant();

        private lnMant() {
        }
    }

    private static class CodyWaite {
        private final int finalK;
        private final double finalRemA;
        private final double finalRemB;

        CodyWaite(double d) {
            int i = (int) (0.6366197723675814d * d);
            while (true) {
                double d2 = (double) (-i);
                Double.isNaN(d2);
                double d3 = 1.570796251296997d * d2;
                double d4 = d + d3;
                Double.isNaN(d2);
                double d5 = 7.549789948768648E-8d * d2;
                double d6 = d5 + d4;
                double d7 = (-((d4 - d) - d3)) + (-((d6 - d4) - d5));
                Double.isNaN(d2);
                double d8 = d2 * 6.123233995736766E-17d;
                double d9 = d8 + d6;
                double d10 = d7 + (-((d9 - d6) - d8));
                if (d9 > 0.0d) {
                    this.finalK = i;
                    this.finalRemA = d9;
                    this.finalRemB = d10;
                    return;
                }
                i--;
            }
        }

        /* access modifiers changed from: package-private */
        public int getK() {
            return this.finalK;
        }

        /* access modifiers changed from: package-private */
        public double getRemA() {
            return this.finalRemA;
        }

        /* access modifiers changed from: package-private */
        public double getRemB() {
            return this.finalRemB;
        }
    }
}
