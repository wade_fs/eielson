package org.apache.commons.math3.random;

import com.baidu.location.BDLocation;
import com.tencent.bugly.beta.tinker.TinkerReport;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.util.MathUtils;

public class HaltonSequenceGenerator implements RandomVectorGenerator {
    private static final int[] PRIMES = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, FMParserConstants.CLOSE_PAREN, FMParserConstants.f7456AS, FMParserConstants.ID_START_CHAR, FMParserConstants.DIRECTIVE_END, 149, TinkerReport.KEY_APPLIED_PACKAGE_CHECK_DEX_META, TinkerReport.KEY_APPLIED_PACKAGE_CHECK_RES_META, 163, BDLocation.TypeServerError, 173};
    private static final int[] WEIGHTS = {1, 2, 3, 3, 8, 11, 12, 14, 7, 18, 12, 13, 17, 18, 29, 14, 18, 43, 41, 44, 40, 30, 47, 65, 71, 28, 40, 60, 79, 89, 56, 50, 52, 61, 108, 56, 66, 63, 60, 66};
    private final int[] base;
    private int count;
    private final int dimension;
    private final int[] weight;

    public HaltonSequenceGenerator(int i) throws OutOfRangeException {
        this(i, PRIMES, WEIGHTS);
    }

    public HaltonSequenceGenerator(int i, int[] iArr, int[] iArr2) throws NullArgumentException, OutOfRangeException, DimensionMismatchException {
        int[] iArr3;
        this.count = 0;
        MathUtils.checkNotNull(iArr);
        if (i < 1 || i > iArr.length) {
            throw new OutOfRangeException(Integer.valueOf(i), 1, Integer.valueOf(PRIMES.length));
        } else if (iArr2 == null || iArr2.length == iArr.length) {
            this.dimension = i;
            this.base = (int[]) iArr.clone();
            if (iArr2 == null) {
                iArr3 = null;
            } else {
                iArr3 = (int[]) iArr2.clone();
            }
            this.weight = iArr3;
            this.count = 0;
        } else {
            throw new DimensionMismatchException(iArr2.length, iArr.length);
        }
    }

    public double[] nextVector() {
        double[] dArr = new double[this.dimension];
        for (int i = 0; i < this.dimension; i++) {
            int i2 = this.count;
            double d = 1.0d;
            double d2 = (double) this.base[i];
            Double.isNaN(d2);
            while (true) {
                d /= d2;
                if (i2 <= 0) {
                    break;
                }
                int[] iArr = this.base;
                int scramble = scramble(i, 0, iArr[i], i2 % iArr[i]);
                double d3 = dArr[i];
                double d4 = (double) scramble;
                Double.isNaN(d4);
                dArr[i] = d3 + (d4 * d);
                int[] iArr2 = this.base;
                i2 /= iArr2[i];
                d2 = (double) iArr2[i];
                Double.isNaN(d2);
            }
        }
        this.count++;
        return dArr;
    }

    /* access modifiers changed from: protected */
    public int scramble(int i, int i2, int i3, int i4) {
        int[] iArr = this.weight;
        return iArr != null ? (iArr[i] * i4) % i3 : i4;
    }

    public double[] skipTo(int i) throws NotPositiveException {
        this.count = i;
        return nextVector();
    }

    public int getNextIndex() {
        return this.count;
    }
}
