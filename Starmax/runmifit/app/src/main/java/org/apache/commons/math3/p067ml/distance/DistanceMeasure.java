package org.apache.commons.math3.p067ml.distance;

import java.io.Serializable;

/* renamed from: org.apache.commons.math3.ml.distance.DistanceMeasure */
public interface DistanceMeasure extends Serializable {
    double compute(double[] dArr, double[] dArr2);
}
