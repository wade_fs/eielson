package org.apache.commons.math3.p067ml.neuralnet;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.function.Constant;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.commons.math3.random.RandomGenerator;

/* renamed from: org.apache.commons.math3.ml.neuralnet.FeatureInitializerFactory */
public class FeatureInitializerFactory {
    private FeatureInitializerFactory() {
    }

    public static FeatureInitializer uniform(RandomGenerator randomGenerator, double d, double d2) {
        return randomize(new UniformRealDistribution(randomGenerator, d, d2), function(new Constant(0.0d), 0.0d, 0.0d));
    }

    public static FeatureInitializer uniform(double d, double d2) {
        return randomize(new UniformRealDistribution(d, d2), function(new Constant(0.0d), 0.0d, 0.0d));
    }

    public static FeatureInitializer function(UnivariateFunction univariateFunction, double d, double d2) {
        final double d3 = d;
        final UnivariateFunction univariateFunction2 = univariateFunction;
        final double d4 = d2;
        return new FeatureInitializer() {
            /* class org.apache.commons.math3.p067ml.neuralnet.FeatureInitializerFactory.C35631 */
            private double arg = d3;

            public double value() {
                double value = univariateFunction2.value(this.arg);
                this.arg += d4;
                return value;
            }
        };
    }

    public static FeatureInitializer randomize(final RealDistribution realDistribution, final FeatureInitializer featureInitializer) {
        return new FeatureInitializer() {
            /* class org.apache.commons.math3.p067ml.neuralnet.FeatureInitializerFactory.C35642 */

            public double value() {
                return featureInitializer.value() + realDistribution.sample();
            }
        };
    }
}
