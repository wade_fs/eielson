package org.apache.commons.math3.fitting.leastsquares;

import org.apache.commons.math3.analysis.MultivariateMatrixFunction;
import org.apache.commons.math3.analysis.MultivariateVectorFunction;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresProblem;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.optim.PointVectorValuePair;

public class LeastSquaresBuilder {
    private ConvergenceChecker<LeastSquaresProblem.Evaluation> checker;
    private int maxEvaluations;
    private int maxIterations;
    private MultivariateJacobianFunction model;
    private RealVector start;
    private RealVector target;
    private RealMatrix weight;

    public LeastSquaresProblem build() {
        return LeastSquaresFactory.create(this.model, this.target, this.start, this.weight, this.checker, this.maxEvaluations, this.maxIterations);
    }

    public LeastSquaresBuilder maxEvaluations(int i) {
        this.maxEvaluations = i;
        return this;
    }

    public LeastSquaresBuilder maxIterations(int i) {
        this.maxIterations = i;
        return this;
    }

    public LeastSquaresBuilder checker(ConvergenceChecker<LeastSquaresProblem.Evaluation> convergenceChecker) {
        this.checker = convergenceChecker;
        return this;
    }

    public LeastSquaresBuilder checkerPair(ConvergenceChecker<PointVectorValuePair> convergenceChecker) {
        return checker(LeastSquaresFactory.evaluationChecker(convergenceChecker));
    }

    public LeastSquaresBuilder model(MultivariateVectorFunction multivariateVectorFunction, MultivariateMatrixFunction multivariateMatrixFunction) {
        return model(LeastSquaresFactory.model(multivariateVectorFunction, multivariateMatrixFunction));
    }

    public LeastSquaresBuilder model(MultivariateJacobianFunction multivariateJacobianFunction) {
        this.model = multivariateJacobianFunction;
        return this;
    }

    public LeastSquaresBuilder target(RealVector realVector) {
        this.target = realVector;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void
     arg types: [double[], int]
     candidates:
      org.apache.commons.math3.linear.ArrayRealVector.<init>(int, double):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.RealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, boolean):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, double[]):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.RealVector, org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], double[]):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void */
    public LeastSquaresBuilder target(double[] dArr) {
        return target(new ArrayRealVector(dArr, false));
    }

    public LeastSquaresBuilder start(RealVector realVector) {
        this.start = realVector;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void
     arg types: [double[], int]
     candidates:
      org.apache.commons.math3.linear.ArrayRealVector.<init>(int, double):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.RealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, boolean):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, double[]):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.RealVector, org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], double[]):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void */
    public LeastSquaresBuilder start(double[] dArr) {
        return start(new ArrayRealVector(dArr, false));
    }

    public LeastSquaresBuilder weight(RealMatrix realMatrix) {
        this.weight = realMatrix;
        return this;
    }
}
