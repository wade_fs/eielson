package org.apache.commons.math3.geometry.euclidean.twod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.math3.exception.MathInternalError;
import org.apache.commons.math3.geometry.Point;
import org.apache.commons.math3.geometry.euclidean.oned.Euclidean1D;
import org.apache.commons.math3.geometry.euclidean.oned.Interval;
import org.apache.commons.math3.geometry.euclidean.oned.IntervalsSet;
import org.apache.commons.math3.geometry.euclidean.oned.Vector1D;
import org.apache.commons.math3.geometry.partitioning.AbstractRegion;
import org.apache.commons.math3.geometry.partitioning.AbstractSubHyperplane;
import org.apache.commons.math3.geometry.partitioning.BSPTree;
import org.apache.commons.math3.geometry.partitioning.BSPTreeVisitor;
import org.apache.commons.math3.geometry.partitioning.BoundaryAttribute;
import org.apache.commons.math3.geometry.partitioning.Side;
import org.apache.commons.math3.geometry.partitioning.SubHyperplane;
import org.apache.commons.math3.geometry.partitioning.utilities.AVLTree;
import org.apache.commons.math3.geometry.partitioning.utilities.OrderedTuple;
import org.apache.commons.math3.util.FastMath;

public class PolygonsSet extends AbstractRegion<Euclidean2D, Euclidean1D> {
    private static final double DEFAULT_TOLERANCE = 1.0E-10d;
    private Vector2D[][] vertices;

    public PolygonsSet(double d) {
        super(d);
    }

    public PolygonsSet(BSPTree<Euclidean2D> bSPTree, double d) {
        super(bSPTree, d);
    }

    public PolygonsSet(Collection<SubHyperplane<Euclidean2D>> collection, double d) {
        super(collection, d);
    }

    public PolygonsSet(double d, double d2, double d3, double d4, double d5) {
        super(boxBoundary(d, d2, d3, d4, d5), d5);
    }

    public PolygonsSet(double d, Vector2D... vector2DArr) {
        super(verticesToTree(d, vector2DArr), d);
    }

    @Deprecated
    public PolygonsSet() {
        this(1.0E-10d);
    }

    @Deprecated
    public PolygonsSet(BSPTree<Euclidean2D> bSPTree) {
        this(bSPTree, 1.0E-10d);
    }

    @Deprecated
    public PolygonsSet(Collection<SubHyperplane<Euclidean2D>> collection) {
        this(collection, 1.0E-10d);
    }

    @Deprecated
    public PolygonsSet(double d, double d2, double d3, double d4) {
        this(d, d2, d3, d4, 1.0E-10d);
    }

    private static Line[] boxBoundary(double d, double d2, double d3, double d4, double d5) {
        if (d >= d2 - d5 || d3 >= d4 - d5) {
            return null;
        }
        Vector2D vector2D = new Vector2D(d, d3);
        Vector2D vector2D2 = new Vector2D(d, d4);
        Vector2D vector2D3 = new Vector2D(d2, d3);
        Vector2D vector2D4 = new Vector2D(d2, d4);
        return new Line[]{new Line(vector2D, vector2D3, d5), new Line(vector2D3, vector2D4, d5), new Line(vector2D4, vector2D2, d5), new Line(vector2D2, vector2D, d5)};
    }

    private static BSPTree<Euclidean2D> verticesToTree(double d, Vector2D... vector2DArr) {
        int length = vector2DArr.length;
        if (length == 0) {
            return new BSPTree<>(Boolean.TRUE);
        }
        Vertex[] vertexArr = new Vertex[length];
        for (int i = 0; i < length; i++) {
            vertexArr[i] = new Vertex(vector2DArr[i]);
        }
        ArrayList arrayList = new ArrayList(length);
        int i2 = 0;
        while (i2 < length) {
            Vertex vertex = vertexArr[i2];
            i2++;
            Vertex vertex2 = vertexArr[i2 % length];
            Line sharedLineWith = vertex.sharedLineWith(vertex2);
            if (sharedLineWith == null) {
                sharedLineWith = new Line(vertex.getLocation(), vertex2.getLocation(), d);
            }
            arrayList.add(new Edge(vertex, vertex2, sharedLineWith));
            for (Vertex vertex3 : vertexArr) {
                if (!(vertex3 == vertex || vertex3 == vertex2 || FastMath.abs(sharedLineWith.getOffset((Point<Euclidean2D>) vertex3.getLocation())) > d)) {
                    vertex3.bindWith(sharedLineWith);
                }
            }
        }
        BSPTree<Euclidean2D> bSPTree = new BSPTree<>();
        insertEdges(d, bSPTree, arrayList);
        return bSPTree;
    }

    private static void insertEdges(double d, BSPTree<Euclidean2D> bSPTree, List<Edge> list) {
        double d2 = d;
        BSPTree<Euclidean2D> bSPTree2 = bSPTree;
        Edge edge = null;
        int i = 0;
        while (edge == null && i < list.size()) {
            int i2 = i + 1;
            Edge edge2 = list.get(i);
            if (edge2.getNode() != null || !bSPTree2.insertCut(edge2.getLine())) {
                i = i2;
                edge = null;
            } else {
                edge2.setNode(bSPTree2);
                Edge edge3 = edge2;
                i = i2;
                edge = edge3;
            }
        }
        if (edge == null) {
            BSPTree<Euclidean2D> parent = bSPTree.getParent();
            if (parent == null || bSPTree2 == parent.getMinus()) {
                bSPTree2.setAttribute(Boolean.TRUE);
            } else {
                bSPTree2.setAttribute(Boolean.FALSE);
            }
        } else {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (Edge edge4 : list) {
                if (edge4 != edge) {
                    double offset = edge.getLine().getOffset((Point<Euclidean2D>) edge4.getStart().getLocation());
                    double offset2 = edge.getLine().getOffset((Point<Euclidean2D>) edge4.getEnd().getLocation());
                    Side side = FastMath.abs(offset) <= d2 ? Side.HYPER : offset < 0.0d ? Side.MINUS : Side.PLUS;
                    Side side2 = FastMath.abs(offset2) <= d2 ? Side.HYPER : offset2 < 0.0d ? Side.MINUS : Side.PLUS;
                    int i3 = C35291.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side[side.ordinal()];
                    if (i3 != 1) {
                        if (i3 != 2) {
                            if (side2 == Side.PLUS) {
                                arrayList.add(edge4);
                            } else if (side2 == Side.MINUS) {
                                arrayList2.add(edge4);
                            }
                        } else if (side2 == Side.PLUS) {
                            Vertex split = edge4.split(edge.getLine());
                            arrayList2.add(split.getIncoming());
                            arrayList.add(split.getOutgoing());
                        } else {
                            arrayList2.add(edge4);
                        }
                    } else if (side2 == Side.MINUS) {
                        Vertex split2 = edge4.split(edge.getLine());
                        arrayList2.add(split2.getOutgoing());
                        arrayList.add(split2.getIncoming());
                    } else {
                        arrayList.add(edge4);
                    }
                }
            }
            if (!arrayList.isEmpty()) {
                insertEdges(d2, bSPTree.getPlus(), arrayList);
            } else {
                bSPTree.getPlus().setAttribute(Boolean.FALSE);
            }
            if (!arrayList2.isEmpty()) {
                insertEdges(d2, bSPTree.getMinus(), arrayList2);
            } else {
                bSPTree.getMinus().setAttribute(Boolean.TRUE);
            }
        }
    }

    /* renamed from: org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet$1 */
    static /* synthetic */ class C35291 {
        static final /* synthetic */ int[] $SwitchMap$org$apache$commons$math3$geometry$partitioning$Side = new int[Side.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                org.apache.commons.math3.geometry.partitioning.Side[] r0 = org.apache.commons.math3.geometry.partitioning.Side.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet.C35291.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side = r0
                int[] r0 = org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet.C35291.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.geometry.partitioning.Side r1 = org.apache.commons.math3.geometry.partitioning.Side.PLUS     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet.C35291.$SwitchMap$org$apache$commons$math3$geometry$partitioning$Side     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.geometry.partitioning.Side r1 = org.apache.commons.math3.geometry.partitioning.Side.MINUS     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.geometry.euclidean.twod.PolygonsSet.C35291.<clinit>():void");
        }
    }

    private static class Vertex {
        private Edge incoming = null;
        private final List<Line> lines = new ArrayList();
        private final Vector2D location;
        private Edge outgoing = null;

        public Vertex(Vector2D vector2D) {
            this.location = vector2D;
        }

        public Vector2D getLocation() {
            return this.location;
        }

        public void bindWith(Line line) {
            this.lines.add(line);
        }

        public Line sharedLineWith(Vertex vertex) {
            for (Line line : this.lines) {
                Iterator<Line> it = vertex.lines.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (line == it.next()) {
                            return line;
                        }
                    }
                }
            }
            return null;
        }

        public void setIncoming(Edge edge) {
            this.incoming = edge;
            bindWith(edge.getLine());
        }

        public Edge getIncoming() {
            return this.incoming;
        }

        public void setOutgoing(Edge edge) {
            this.outgoing = edge;
            bindWith(edge.getLine());
        }

        public Edge getOutgoing() {
            return this.outgoing;
        }
    }

    private static class Edge {
        private final Vertex end;
        private final Line line;
        private BSPTree<Euclidean2D> node = null;
        private final Vertex start;

        public Edge(Vertex vertex, Vertex vertex2, Line line2) {
            this.start = vertex;
            this.end = vertex2;
            this.line = line2;
            vertex.setOutgoing(this);
            vertex2.setIncoming(this);
        }

        public Vertex getStart() {
            return this.start;
        }

        public Vertex getEnd() {
            return this.end;
        }

        public Line getLine() {
            return this.line;
        }

        public void setNode(BSPTree<Euclidean2D> bSPTree) {
            this.node = bSPTree;
        }

        public BSPTree<Euclidean2D> getNode() {
            return this.node;
        }

        public Vertex split(Line line2) {
            Vertex vertex = new Vertex(this.line.intersection(line2));
            vertex.bindWith(line2);
            Edge edge = new Edge(this.start, vertex, this.line);
            Edge edge2 = new Edge(vertex, this.end, this.line);
            edge.node = this.node;
            edge2.node = this.node;
            return vertex;
        }
    }

    public PolygonsSet buildNew(BSPTree<Euclidean2D> bSPTree) {
        return new PolygonsSet(bSPTree, getTolerance());
    }

    /* access modifiers changed from: protected */
    public void computeGeometricalProperties() {
        Vector2D[][] vertices2 = getVertices();
        if (vertices2.length == 0) {
            BSPTree tree = getTree(false);
            if (tree.getCut() != null || !((Boolean) tree.getAttribute()).booleanValue()) {
                setSize(0.0d);
                setBarycenter((Point) new Vector2D(0.0d, 0.0d));
                return;
            }
            setSize(Double.POSITIVE_INFINITY);
            setBarycenter((Point) Vector2D.NaN);
        } else if (vertices2[0][0] == null) {
            setSize(Double.POSITIVE_INFINITY);
            setBarycenter((Point) Vector2D.NaN);
        } else {
            int length = vertices2.length;
            double d = 0.0d;
            double d2 = 0.0d;
            double d3 = 0.0d;
            int i = 0;
            while (i < length) {
                Vector2D[] vector2DArr = vertices2[i];
                double x = vector2DArr[vector2DArr.length - 1].getX();
                double y = vector2DArr[vector2DArr.length - 1].getY();
                int length2 = vector2DArr.length;
                double d4 = y;
                double d5 = x;
                double d6 = d3;
                double d7 = d2;
                double d8 = d;
                int i2 = 0;
                while (i2 < length2) {
                    Vector2D vector2D = vector2DArr[i2];
                    double x2 = vector2D.getX();
                    double y2 = vector2D.getY();
                    double d9 = (d5 * y2) - (d4 * x2);
                    d8 += d9;
                    d7 += (d5 + x2) * d9;
                    d6 += d9 * (d4 + y2);
                    i2++;
                    d5 = x2;
                    d4 = y2;
                }
                i++;
                d = d8;
                d2 = d7;
                d3 = d6;
            }
            if (d < 0.0d) {
                setSize(Double.POSITIVE_INFINITY);
                setBarycenter((Point) Vector2D.NaN);
                return;
            }
            setSize(d / 2.0d);
            double d10 = d * 3.0d;
            setBarycenter((Point) new Vector2D(d2 / d10, d3 / d10));
        }
    }

    public Vector2D[][] getVertices() {
        Iterator it;
        Iterator it2;
        int i;
        if (this.vertices == null) {
            int i2 = 0;
            if (getTree(false).getCut() == null) {
                this.vertices = new Vector2D[0][];
            } else {
                SegmentsBuilder segmentsBuilder = new SegmentsBuilder();
                getTree(true).visit(segmentsBuilder);
                AVLTree<ComparableSegment> sorted = segmentsBuilder.getSorted();
                ArrayList arrayList = new ArrayList();
                while (!sorted.isEmpty()) {
                    List<ComparableSegment> followLoop = followLoop(sorted.getSmallest(), sorted);
                    if (followLoop != null) {
                        arrayList.add(followLoop);
                    }
                }
                this.vertices = new Vector2D[arrayList.size()][];
                Iterator it3 = arrayList.iterator();
                int i3 = 0;
                while (it3.hasNext()) {
                    List<Segment> list = (List) it3.next();
                    Vector2D vector2D = null;
                    if (list.size() < 2) {
                        Line line = ((ComparableSegment) list.get(i2)).getLine();
                        Vector2D[][] vector2DArr = this.vertices;
                        Vector2D[] vector2DArr2 = new Vector2D[3];
                        vector2DArr2[i2] = null;
                        vector2DArr2[1] = line.toSpace((Point<Euclidean1D>) new Vector1D(-3.4028234663852886E38d));
                        vector2DArr2[2] = line.toSpace((Point<Euclidean1D>) new Vector1D(3.4028234663852886E38d));
                        vector2DArr[i3] = vector2DArr2;
                        it = it3;
                        i3++;
                    } else if (((ComparableSegment) list.get(i2)).getStart() == null) {
                        Vector2D[] vector2DArr3 = new Vector2D[(list.size() + 2)];
                        int i4 = 0;
                        for (Segment segment : list) {
                            if (i4 == 0) {
                                double x = segment.getLine().toSubSpace((Point<Euclidean2D>) segment.getEnd()).getX();
                                it2 = it3;
                                double max = x - FastMath.max(1.0d, FastMath.abs(x / 2.0d));
                                int i5 = i4 + 1;
                                vector2DArr3[i4] = vector2D;
                                i4 = i5 + 1;
                                vector2DArr3[i5] = segment.getLine().toSpace((Point<Euclidean1D>) new Vector1D(max));
                            } else {
                                it2 = it3;
                            }
                            if (i4 < vector2DArr3.length - 1) {
                                i = i4 + 1;
                                vector2DArr3[i4] = segment.getEnd();
                            } else {
                                i = i4;
                            }
                            if (i == vector2DArr3.length - 1) {
                                double x2 = segment.getLine().toSubSpace((Point<Euclidean2D>) segment.getStart()).getX();
                                vector2DArr3[i] = segment.getLine().toSpace((Point<Euclidean1D>) new Vector1D(x2 + FastMath.max(1.0d, FastMath.abs(x2 / 2.0d))));
                                i4 = i + 1;
                            } else {
                                i4 = i;
                            }
                            it3 = it2;
                            vector2D = null;
                        }
                        it = it3;
                        this.vertices[i3] = vector2DArr3;
                        i3++;
                    } else {
                        it = it3;
                        Vector2D[] vector2DArr4 = new Vector2D[list.size()];
                        int i6 = 0;
                        for (Segment segment2 : list) {
                            vector2DArr4[i6] = segment2.getStart();
                            i6++;
                        }
                        this.vertices[i3] = vector2DArr4;
                        i3++;
                    }
                    it3 = it;
                    i2 = 0;
                }
            }
        }
        return (Vector2D[][]) this.vertices.clone();
    }

    private List<ComparableSegment> followLoop(AVLTree<ComparableSegment>.Node node, AVLTree<ComparableSegment> aVLTree) {
        ArrayList arrayList = new ArrayList();
        ComparableSegment comparableSegment = (ComparableSegment) node.getElement();
        arrayList.add(comparableSegment);
        Vector2D start = comparableSegment.getStart();
        Vector2D end = comparableSegment.getEnd();
        node.delete();
        boolean z = comparableSegment.getStart() == null;
        while (end != null && (z || start.distance((Point<Euclidean2D>) end) > 1.0E-10d)) {
            Vector2D vector2D = end;
            ComparableSegment comparableSegment2 = new ComparableSegment(vector2D, -1.0E-10d, -1.0E-10d);
            ComparableSegment comparableSegment3 = r4;
            ComparableSegment comparableSegment4 = new ComparableSegment(vector2D, 1.0E-10d, 1.0E-10d);
            AVLTree<T>.Node notSmaller = aVLTree.getNotSmaller(comparableSegment2);
            AVLTree<T>.Node node2 = null;
            ComparableSegment comparableSegment5 = null;
            double d = Double.POSITIVE_INFINITY;
            while (notSmaller != null && ((ComparableSegment) notSmaller.getElement()).compareTo(comparableSegment3) <= 0) {
                ComparableSegment comparableSegment6 = (ComparableSegment) notSmaller.getElement();
                double distance = end.distance((Point<Euclidean2D>) comparableSegment6.getStart());
                if (distance < d) {
                    node2 = notSmaller;
                    comparableSegment5 = comparableSegment6;
                    d = distance;
                }
                notSmaller = notSmaller.getNext();
            }
            if (d > 1.0E-10d) {
                return null;
            }
            end = comparableSegment5.getEnd();
            arrayList.add(comparableSegment5);
            node2.delete();
        }
        if (arrayList.size() == 2 && !z) {
            return null;
        }
        if (end != null || z) {
            return arrayList;
        }
        throw new MathInternalError();
    }

    private static class ComparableSegment extends Segment implements Comparable<ComparableSegment> {
        private OrderedTuple sortingKey;

        public ComparableSegment(Vector2D vector2D, Vector2D vector2D2, Line line) {
            super(vector2D, vector2D2, line);
            OrderedTuple orderedTuple;
            if (vector2D == null) {
                orderedTuple = new OrderedTuple(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
            } else {
                orderedTuple = new OrderedTuple(vector2D.getX(), vector2D.getY());
            }
            this.sortingKey = orderedTuple;
        }

        public ComparableSegment(Vector2D vector2D, double d, double d2) {
            super(null, null, null);
            this.sortingKey = new OrderedTuple(vector2D.getX() + d, vector2D.getY() + d2);
        }

        public int compareTo(ComparableSegment comparableSegment) {
            return this.sortingKey.compareTo(comparableSegment.sortingKey);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ComparableSegment) || compareTo((ComparableSegment) obj) != 0) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return ((getStart().hashCode() ^ getEnd().hashCode()) ^ getLine().hashCode()) ^ this.sortingKey.hashCode();
        }
    }

    private static class SegmentsBuilder implements BSPTreeVisitor<Euclidean2D> {
        private AVLTree<ComparableSegment> sorted = new AVLTree<>();

        public void visitLeafNode(BSPTree<Euclidean2D> bSPTree) {
        }

        public BSPTreeVisitor.Order visitOrder(BSPTree<Euclidean2D> bSPTree) {
            return BSPTreeVisitor.Order.MINUS_SUB_PLUS;
        }

        public void visitInternalNode(BSPTree<Euclidean2D> bSPTree) {
            BoundaryAttribute boundaryAttribute = (BoundaryAttribute) bSPTree.getAttribute();
            if (boundaryAttribute.getPlusOutside() != null) {
                addContribution(boundaryAttribute.getPlusOutside(), false);
            }
            if (boundaryAttribute.getPlusInside() != null) {
                addContribution(boundaryAttribute.getPlusInside(), true);
            }
        }

        private void addContribution(SubHyperplane<Euclidean2D> subHyperplane, boolean z) {
            Line line = (Line) subHyperplane.getHyperplane();
            for (Interval interval : ((IntervalsSet) ((AbstractSubHyperplane) subHyperplane).getRemainingRegion()).asList()) {
                Vector2D vector2D = null;
                Vector2D space = Double.isInfinite(interval.getInf()) ? null : line.toSpace((Point<Euclidean1D>) new Vector1D(interval.getInf()));
                if (!Double.isInfinite(interval.getSup())) {
                    vector2D = line.toSpace((Point<Euclidean1D>) new Vector1D(interval.getSup()));
                }
                if (z) {
                    this.sorted.insert(new ComparableSegment(vector2D, space, line.getReverse()));
                } else {
                    this.sorted.insert(new ComparableSegment(space, vector2D, line));
                }
            }
        }

        public AVLTree<ComparableSegment> getSorted() {
            return this.sorted;
        }
    }
}
