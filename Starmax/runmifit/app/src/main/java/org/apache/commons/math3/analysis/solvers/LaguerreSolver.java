package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.analysis.polynomials.PolynomialFunction;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.complex.ComplexUtils;
import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NoDataException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;

public class LaguerreSolver extends AbstractPolynomialSolver {
    private static final double DEFAULT_ABSOLUTE_ACCURACY = 1.0E-6d;
    private final ComplexSolver complexSolver;

    public LaguerreSolver() {
        this(1.0E-6d);
    }

    public LaguerreSolver(double d) {
        super(d);
        this.complexSolver = new ComplexSolver();
    }

    public LaguerreSolver(double d, double d2) {
        super(d, d2);
        this.complexSolver = new ComplexSolver();
    }

    public LaguerreSolver(double d, double d2, double d3) {
        super(d, d2, d3);
        this.complexSolver = new ComplexSolver();
    }

    public double doSolve() throws TooManyEvaluationsException, NumberIsTooLargeException, NoBracketingException {
        double min = getMin();
        double max = getMax();
        double startValue = getStartValue();
        double functionValueAccuracy = getFunctionValueAccuracy();
        verifySequence(min, startValue, max);
        double computeObjectiveValue = computeObjectiveValue(startValue);
        if (FastMath.abs(computeObjectiveValue) <= functionValueAccuracy) {
            return startValue;
        }
        double computeObjectiveValue2 = computeObjectiveValue(min);
        if (FastMath.abs(computeObjectiveValue2) <= functionValueAccuracy) {
            return min;
        }
        if (computeObjectiveValue * computeObjectiveValue2 < 0.0d) {
            return laguerre(min, startValue, computeObjectiveValue2, computeObjectiveValue);
        }
        double computeObjectiveValue3 = computeObjectiveValue(max);
        if (FastMath.abs(computeObjectiveValue3) <= functionValueAccuracy) {
            return max;
        }
        if (computeObjectiveValue * computeObjectiveValue3 < 0.0d) {
            return laguerre(startValue, max, computeObjectiveValue, computeObjectiveValue3);
        }
        throw new NoBracketingException(min, max, computeObjectiveValue2, computeObjectiveValue3);
    }

    @Deprecated
    public double laguerre(double d, double d2, double d3, double d4) {
        Complex[] convertToComplex = ComplexUtils.convertToComplex(getCoefficients());
        Complex complex = new Complex((d + d2) * 0.5d, 0.0d);
        Complex solve = this.complexSolver.solve(convertToComplex, complex);
        if (this.complexSolver.isRoot(d, d2, solve)) {
            return solve.getReal();
        }
        Complex[] solveAll = this.complexSolver.solveAll(convertToComplex, complex);
        for (int i = 0; i < solveAll.length; i++) {
            if (this.complexSolver.isRoot(d, d2, solveAll[i])) {
                return solveAll[i].getReal();
            }
        }
        return Double.NaN;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.analysis.solvers.AbstractPolynomialSolver.setup(int, org.apache.commons.math3.analysis.polynomials.PolynomialFunction, double, double, double):void
     arg types: [int, org.apache.commons.math3.analysis.polynomials.PolynomialFunction, int, int, double]
     candidates:
      org.apache.commons.math3.analysis.solvers.AbstractPolynomialSolver.setup(int, org.apache.commons.math3.analysis.UnivariateFunction, double, double, double):void
      org.apache.commons.math3.analysis.solvers.BaseAbstractUnivariateSolver.setup(int, org.apache.commons.math3.analysis.UnivariateFunction, double, double, double):void
      org.apache.commons.math3.analysis.solvers.AbstractPolynomialSolver.setup(int, org.apache.commons.math3.analysis.polynomials.PolynomialFunction, double, double, double):void */
    public Complex[] solveAllComplex(double[] dArr, double d) throws NullArgumentException, NoDataException, TooManyEvaluationsException {
        setup(Integer.MAX_VALUE, new PolynomialFunction(dArr), Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, d);
        return this.complexSolver.solveAll(ComplexUtils.convertToComplex(dArr), new Complex(d, 0.0d));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.analysis.solvers.AbstractPolynomialSolver.setup(int, org.apache.commons.math3.analysis.polynomials.PolynomialFunction, double, double, double):void
     arg types: [int, org.apache.commons.math3.analysis.polynomials.PolynomialFunction, int, int, double]
     candidates:
      org.apache.commons.math3.analysis.solvers.AbstractPolynomialSolver.setup(int, org.apache.commons.math3.analysis.UnivariateFunction, double, double, double):void
      org.apache.commons.math3.analysis.solvers.BaseAbstractUnivariateSolver.setup(int, org.apache.commons.math3.analysis.UnivariateFunction, double, double, double):void
      org.apache.commons.math3.analysis.solvers.AbstractPolynomialSolver.setup(int, org.apache.commons.math3.analysis.polynomials.PolynomialFunction, double, double, double):void */
    public Complex solveComplex(double[] dArr, double d) throws NullArgumentException, NoDataException, TooManyEvaluationsException {
        setup(Integer.MAX_VALUE, new PolynomialFunction(dArr), Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, d);
        return this.complexSolver.solve(ComplexUtils.convertToComplex(dArr), new Complex(d, 0.0d));
    }

    private class ComplexSolver {
        private ComplexSolver() {
        }

        public boolean isRoot(double d, double d2, Complex complex) {
            if (!LaguerreSolver.this.isSequence(d, complex.getReal(), d2)) {
                return false;
            }
            if (FastMath.abs(complex.getImaginary()) <= FastMath.max(LaguerreSolver.this.getRelativeAccuracy() * complex.abs(), LaguerreSolver.this.getAbsoluteAccuracy()) || complex.abs() <= LaguerreSolver.this.getFunctionValueAccuracy()) {
                return true;
            }
            return false;
        }

        public Complex[] solveAll(Complex[] complexArr, Complex complex) throws NullArgumentException, NoDataException, TooManyEvaluationsException {
            if (complexArr != null) {
                int length = complexArr.length - 1;
                if (length != 0) {
                    Complex[] complexArr2 = new Complex[(length + 1)];
                    for (int i = 0; i <= length; i++) {
                        complexArr2[i] = complexArr[i];
                    }
                    Complex[] complexArr3 = new Complex[length];
                    for (int i2 = 0; i2 < length; i2++) {
                        int i3 = length - i2;
                        Complex[] complexArr4 = new Complex[(i3 + 1)];
                        System.arraycopy(complexArr2, 0, complexArr4, 0, complexArr4.length);
                        complexArr3[i2] = solve(complexArr4, complex);
                        Complex complex2 = complexArr2[i3];
                        for (int i4 = i3 - 1; i4 >= 0; i4--) {
                            Complex complex3 = complexArr2[i4];
                            complexArr2[i4] = complex2;
                            complex2 = complex3.add(complex2.multiply(complexArr3[i2]));
                        }
                    }
                    return complexArr3;
                }
                throw new NoDataException(LocalizedFormats.POLYNOMIAL);
            }
            throw new NullArgumentException();
        }

        public Complex solve(Complex[] complexArr, Complex complex) throws NullArgumentException, NoDataException, TooManyEvaluationsException {
            Complex[] complexArr2 = complexArr;
            if (complexArr2 != null) {
                int length = complexArr2.length - 1;
                if (length != 0) {
                    double absoluteAccuracy = LaguerreSolver.this.getAbsoluteAccuracy();
                    double relativeAccuracy = LaguerreSolver.this.getRelativeAccuracy();
                    double functionValueAccuracy = LaguerreSolver.this.getFunctionValueAccuracy();
                    Complex complex2 = new Complex((double) length, 0.0d);
                    int i = length - 1;
                    Complex complex3 = new Complex((double) i, 0.0d);
                    Complex complex4 = new Complex(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
                    Complex complex5 = complex;
                    while (true) {
                        int i2 = length;
                        Complex complex6 = complexArr2[length];
                        Complex complex7 = Complex.ZERO;
                        Complex complex8 = Complex.ZERO;
                        int i3 = i;
                        while (i3 >= 0) {
                            complex8 = complex7.add(complex5.multiply(complex8));
                            complex7 = complex6.add(complex5.multiply(complex7));
                            complex6 = complexArr2[i3].add(complex5.multiply(complex6));
                            i3--;
                            i = i;
                        }
                        int i4 = i;
                        Complex complex9 = complex2;
                        Complex complex10 = complex3;
                        Complex multiply = complex8.multiply(new Complex(2.0d, 0.0d));
                        if (complex5.subtract(complex4).abs() <= FastMath.max(complex5.abs() * relativeAccuracy, absoluteAccuracy) || complex6.abs() <= functionValueAccuracy) {
                            return complex5;
                        }
                        Complex divide = complex7.divide(complex6);
                        Complex multiply2 = divide.multiply(divide);
                        complex2 = complex9;
                        Complex subtract = complex2.multiply(multiply2.subtract(multiply.divide(complex6))).subtract(multiply2);
                        Complex complex11 = complex10;
                        Complex sqrt = complex11.multiply(subtract).sqrt();
                        Complex add = divide.add(sqrt);
                        Complex subtract2 = divide.subtract(sqrt);
                        if (add.abs() > subtract2.abs()) {
                            subtract2 = add;
                        }
                        if (subtract2.equals(new Complex(0.0d, 0.0d))) {
                            complex5 = complex5.add(new Complex(absoluteAccuracy, absoluteAccuracy));
                            complex4 = new Complex(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
                        } else {
                            complex4 = complex5;
                            complex5 = complex5.subtract(complex2.divide(subtract2));
                        }
                        LaguerreSolver.this.incrementEvaluationCount();
                        complexArr2 = complexArr;
                        complex3 = complex11;
                        length = i2;
                        i = i4;
                    }
                } else {
                    throw new NoDataException(LocalizedFormats.POLYNOMIAL);
                }
            } else {
                throw new NullArgumentException();
            }
        }
    }
}
