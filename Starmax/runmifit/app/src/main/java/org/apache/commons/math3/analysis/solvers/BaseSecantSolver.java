package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.MathInternalError;
import org.apache.commons.math3.util.FastMath;

public abstract class BaseSecantSolver extends AbstractUnivariateSolver implements BracketedUnivariateSolver<UnivariateFunction> {
    protected static final double DEFAULT_ABSOLUTE_ACCURACY = 1.0E-6d;
    private AllowedSolution allowed = AllowedSolution.ANY_SIDE;
    private final Method method;

    protected enum Method {
        REGULA_FALSI,
        ILLINOIS,
        PEGASUS
    }

    protected BaseSecantSolver(double d, Method method2) {
        super(d);
        this.method = method2;
    }

    protected BaseSecantSolver(double d, double d2, Method method2) {
        super(d, d2);
        this.method = method2;
    }

    protected BaseSecantSolver(double d, double d2, double d3, Method method2) {
        super(d, d2, d3);
        this.method = method2;
    }

    public double solve(int i, UnivariateFunction univariateFunction, double d, double d2, AllowedSolution allowedSolution) {
        return solve(i, univariateFunction, d, d2, d + ((d2 - d) * 0.5d), allowedSolution);
    }

    public double solve(int i, UnivariateFunction univariateFunction, double d, double d2, double d3, AllowedSolution allowedSolution) {
        this.allowed = allowedSolution;
        return super.solve(i, univariateFunction, d, d2, d3);
    }

    public double solve(int i, UnivariateFunction univariateFunction, double d, double d2, double d3) {
        return solve(i, univariateFunction, d, d2, d3, AllowedSolution.ANY_SIDE);
    }

    /* access modifiers changed from: protected */
    public final double doSolve() throws ConvergenceException {
        double d;
        double min = getMin();
        double max = getMax();
        double computeObjectiveValue = computeObjectiveValue(min);
        double computeObjectiveValue2 = computeObjectiveValue(max);
        double d2 = 0.0d;
        if (computeObjectiveValue == 0.0d) {
            return min;
        }
        if (computeObjectiveValue2 == 0.0d) {
            return max;
        }
        verifyBracketing(min, max);
        double functionValueAccuracy = getFunctionValueAccuracy();
        double absoluteAccuracy = getAbsoluteAccuracy();
        double relativeAccuracy = getRelativeAccuracy();
        boolean z = false;
        while (true) {
            double d3 = min;
            d = max - (((max - min) * computeObjectiveValue2) / (computeObjectiveValue2 - computeObjectiveValue));
            double computeObjectiveValue3 = computeObjectiveValue(d);
            if (computeObjectiveValue3 == d2) {
                return d;
            }
            if (computeObjectiveValue2 * computeObjectiveValue3 < 0.0d) {
                d3 = max;
                z = !z;
                computeObjectiveValue = computeObjectiveValue2;
            } else {
                int i = C34971.f7965xfa67120a[this.method.ordinal()];
                if (i == 1) {
                    computeObjectiveValue *= 0.5d;
                } else if (i == 2) {
                    computeObjectiveValue *= computeObjectiveValue2 / (computeObjectiveValue2 + computeObjectiveValue3);
                } else if (i != 3) {
                    throw new MathInternalError();
                } else if (d == max) {
                    throw new ConvergenceException();
                }
            }
            if (FastMath.abs(computeObjectiveValue3) <= functionValueAccuracy) {
                int i2 = C34971.f7964xe6390080[this.allowed.ordinal()];
                if (i2 != 1) {
                    if (i2 == 2) {
                        if (z) {
                            break;
                        }
                    } else if (i2 != 3) {
                        if (i2 != 4) {
                            if (i2 != 5) {
                                throw new MathInternalError();
                            } else if (computeObjectiveValue3 >= 0.0d) {
                                return d;
                            }
                        } else if (computeObjectiveValue3 <= 0.0d) {
                            return d;
                        }
                    } else if (!z) {
                        return d;
                    }
                } else {
                    break;
                }
            }
            if (FastMath.abs(d - d3) < FastMath.max(FastMath.abs(d) * relativeAccuracy, absoluteAccuracy)) {
                int i3 = C34971.f7964xe6390080[this.allowed.ordinal()];
                if (i3 == 1) {
                    return d;
                }
                if (i3 == 2) {
                    return z ? d : d3;
                }
                if (i3 == 3) {
                    return z ? d3 : d;
                }
                if (i3 == 4) {
                    return computeObjectiveValue3 <= 0.0d ? d : d3;
                }
                if (i3 == 5) {
                    return computeObjectiveValue3 >= 0.0d ? d : d3;
                }
                throw new MathInternalError();
            }
            max = d;
            computeObjectiveValue2 = computeObjectiveValue3;
            min = d3;
            d2 = 0.0d;
        }
        return d;
    }

    /* renamed from: org.apache.commons.math3.analysis.solvers.BaseSecantSolver$1 */
    static /* synthetic */ class C34971 {

        /* renamed from: $SwitchMap$org$apache$commons$math3$analysis$solvers$AllowedSolution */
        static final /* synthetic */ int[] f7964xe6390080 = new int[AllowedSolution.values().length];

        /* renamed from: $SwitchMap$org$apache$commons$math3$analysis$solvers$BaseSecantSolver$Method */
        static final /* synthetic */ int[] f7965xfa67120a = new int[Method.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(19:0|(2:1|2)|3|(2:5|6)|7|9|10|11|12|13|14|15|17|18|19|20|21|22|24) */
        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|9|10|11|12|13|14|15|17|18|19|20|21|22|24) */
        /* JADX WARNING: Can't wrap try/catch for region: R(21:0|1|2|3|5|6|7|9|10|11|12|13|14|15|17|18|19|20|21|22|24) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005d */
        static {
            /*
                org.apache.commons.math3.analysis.solvers.AllowedSolution[] r0 = org.apache.commons.math3.analysis.solvers.AllowedSolution.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7964xe6390080 = r0
                r0 = 1
                int[] r1 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7964xe6390080     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r2 = org.apache.commons.math3.analysis.solvers.AllowedSolution.ANY_SIDE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7964xe6390080     // Catch:{ NoSuchFieldError -> 0x001f }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r3 = org.apache.commons.math3.analysis.solvers.AllowedSolution.LEFT_SIDE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7964xe6390080     // Catch:{ NoSuchFieldError -> 0x002a }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r4 = org.apache.commons.math3.analysis.solvers.AllowedSolution.RIGHT_SIDE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r3 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7964xe6390080     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r4 = org.apache.commons.math3.analysis.solvers.AllowedSolution.BELOW_SIDE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r5 = 4
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r3 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7964xe6390080     // Catch:{ NoSuchFieldError -> 0x0040 }
                org.apache.commons.math3.analysis.solvers.AllowedSolution r4 = org.apache.commons.math3.analysis.solvers.AllowedSolution.ABOVE_SIDE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r5 = 5
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                org.apache.commons.math3.analysis.solvers.BaseSecantSolver$Method[] r3 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.Method.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7965xfa67120a = r3
                int[] r3 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7965xfa67120a     // Catch:{ NoSuchFieldError -> 0x0053 }
                org.apache.commons.math3.analysis.solvers.BaseSecantSolver$Method r4 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.Method.ILLINOIS     // Catch:{ NoSuchFieldError -> 0x0053 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0053 }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0053 }
            L_0x0053:
                int[] r0 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7965xfa67120a     // Catch:{ NoSuchFieldError -> 0x005d }
                org.apache.commons.math3.analysis.solvers.BaseSecantSolver$Method r3 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.Method.PEGASUS     // Catch:{ NoSuchFieldError -> 0x005d }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x005d }
                r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x005d }
            L_0x005d:
                int[] r0 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.f7965xfa67120a     // Catch:{ NoSuchFieldError -> 0x0067 }
                org.apache.commons.math3.analysis.solvers.BaseSecantSolver$Method r1 = org.apache.commons.math3.analysis.solvers.BaseSecantSolver.Method.REGULA_FALSI     // Catch:{ NoSuchFieldError -> 0x0067 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0067 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0067 }
            L_0x0067:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.analysis.solvers.BaseSecantSolver.C34971.<clinit>():void");
        }
    }
}
