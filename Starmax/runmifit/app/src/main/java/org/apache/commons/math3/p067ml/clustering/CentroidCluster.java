package org.apache.commons.math3.p067ml.clustering;

import org.apache.commons.math3.p067ml.clustering.Clusterable;

/* renamed from: org.apache.commons.math3.ml.clustering.CentroidCluster */
public class CentroidCluster<T extends Clusterable> extends Cluster<T> {
    private static final long serialVersionUID = -3075288519071812288L;
    private final Clusterable center;

    public CentroidCluster(Clusterable clusterable) {
        this.center = clusterable;
    }

    public Clusterable getCenter() {
        return this.center;
    }
}
