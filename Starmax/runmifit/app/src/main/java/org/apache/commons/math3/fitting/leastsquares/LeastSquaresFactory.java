package org.apache.commons.math3.fitting.leastsquares;

import org.apache.commons.math3.analysis.MultivariateMatrixFunction;
import org.apache.commons.math3.analysis.MultivariateVectorFunction;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresProblem;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DiagonalMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.optim.AbstractOptimizationProblem;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.optim.PointVectorValuePair;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Incrementor;
import org.apache.commons.math3.util.Pair;

public class LeastSquaresFactory {
    private LeastSquaresFactory() {
    }

    public static LeastSquaresProblem create(MultivariateJacobianFunction multivariateJacobianFunction, RealVector realVector, RealVector realVector2, ConvergenceChecker<LeastSquaresProblem.Evaluation> convergenceChecker, int i, int i2) {
        return new LocalLeastSquaresProblem(multivariateJacobianFunction, realVector, realVector2, convergenceChecker, i, i2);
    }

    public static LeastSquaresProblem create(MultivariateJacobianFunction multivariateJacobianFunction, RealVector realVector, RealVector realVector2, RealMatrix realMatrix, ConvergenceChecker<LeastSquaresProblem.Evaluation> convergenceChecker, int i, int i2) {
        return weightMatrix(create(multivariateJacobianFunction, realVector, realVector2, convergenceChecker, i, i2), realMatrix);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void
     arg types: [double[], int]
     candidates:
      org.apache.commons.math3.linear.ArrayRealVector.<init>(int, double):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.RealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, boolean):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, double[]):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.RealVector, org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], org.apache.commons.math3.linear.ArrayRealVector):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], double[]):void
      org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void */
    public static LeastSquaresProblem create(MultivariateVectorFunction multivariateVectorFunction, MultivariateMatrixFunction multivariateMatrixFunction, double[] dArr, double[] dArr2, RealMatrix realMatrix, ConvergenceChecker<LeastSquaresProblem.Evaluation> convergenceChecker, int i, int i2) {
        return create(model(multivariateVectorFunction, multivariateMatrixFunction), new ArrayRealVector(dArr, false), new ArrayRealVector(dArr2, false), realMatrix, convergenceChecker, i, i2);
    }

    public static LeastSquaresProblem weightMatrix(LeastSquaresProblem leastSquaresProblem, RealMatrix realMatrix) {
        final RealMatrix squareRoot = squareRoot(realMatrix);
        return new LeastSquaresAdapter(leastSquaresProblem) {
            /* class org.apache.commons.math3.fitting.leastsquares.LeastSquaresFactory.C35181 */

            public LeastSquaresProblem.Evaluation evaluate(RealVector realVector) {
                return new DenseWeightedEvaluation(super.evaluate(realVector), squareRoot);
            }
        };
    }

    public static LeastSquaresProblem weightDiagonal(LeastSquaresProblem leastSquaresProblem, RealVector realVector) {
        return weightMatrix(leastSquaresProblem, new DiagonalMatrix(realVector.toArray()));
    }

    public static LeastSquaresProblem countEvaluations(LeastSquaresProblem leastSquaresProblem, final Incrementor incrementor) {
        return new LeastSquaresAdapter(leastSquaresProblem) {
            /* class org.apache.commons.math3.fitting.leastsquares.LeastSquaresFactory.C35192 */

            public LeastSquaresProblem.Evaluation evaluate(RealVector realVector) {
                incrementor.incrementCount();
                return super.evaluate(realVector);
            }
        };
    }

    public static ConvergenceChecker<LeastSquaresProblem.Evaluation> evaluationChecker(final ConvergenceChecker<PointVectorValuePair> convergenceChecker) {
        return new ConvergenceChecker<LeastSquaresProblem.Evaluation>() {
            /* class org.apache.commons.math3.fitting.leastsquares.LeastSquaresFactory.C35203 */

            public boolean converged(int i, LeastSquaresProblem.Evaluation evaluation, LeastSquaresProblem.Evaluation evaluation2) {
                return convergenceChecker.converged(i, new PointVectorValuePair(evaluation.getPoint().toArray(), evaluation.getResiduals().toArray(), false), new PointVectorValuePair(evaluation2.getPoint().toArray(), evaluation2.getResiduals().toArray(), false));
            }
        };
    }

    private static RealMatrix squareRoot(RealMatrix realMatrix) {
        if (!(realMatrix instanceof DiagonalMatrix)) {
            return new EigenDecomposition(realMatrix).getSquareRoot();
        }
        int rowDimension = realMatrix.getRowDimension();
        DiagonalMatrix diagonalMatrix = new DiagonalMatrix(rowDimension);
        for (int i = 0; i < rowDimension; i++) {
            diagonalMatrix.setEntry(i, i, FastMath.sqrt(realMatrix.getEntry(i, i)));
        }
        return diagonalMatrix;
    }

    public static MultivariateJacobianFunction model(final MultivariateVectorFunction multivariateVectorFunction, final MultivariateMatrixFunction multivariateMatrixFunction) {
        return new MultivariateJacobianFunction() {
            /* class org.apache.commons.math3.fitting.leastsquares.LeastSquaresFactory.C35214 */

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void
             arg types: [double[], int]
             candidates:
              org.apache.commons.math3.linear.ArrayRealVector.<init>(int, double):void
              org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector):void
              org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.RealVector):void
              org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, boolean):void
              org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.ArrayRealVector, double[]):void
              org.apache.commons.math3.linear.ArrayRealVector.<init>(org.apache.commons.math3.linear.RealVector, org.apache.commons.math3.linear.ArrayRealVector):void
              org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], org.apache.commons.math3.linear.ArrayRealVector):void
              org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], double[]):void
              org.apache.commons.math3.linear.ArrayRealVector.<init>(double[], boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
             arg types: [double[][], int]
             candidates:
              org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
              org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
            public Pair<RealVector, RealMatrix> value(RealVector realVector) {
                double[] array = realVector.toArray();
                return new Pair<>(new ArrayRealVector(multivariateVectorFunction.value(array), false), new Array2DRowRealMatrix(multivariateMatrixFunction.value(array), false));
            }
        };
    }

    private static class LocalLeastSquaresProblem extends AbstractOptimizationProblem<LeastSquaresProblem.Evaluation> implements LeastSquaresProblem {
        private MultivariateJacobianFunction model;
        private RealVector start;
        private RealVector target;

        LocalLeastSquaresProblem(MultivariateJacobianFunction multivariateJacobianFunction, RealVector realVector, RealVector realVector2, ConvergenceChecker<LeastSquaresProblem.Evaluation> convergenceChecker, int i, int i2) {
            super(i, i2, convergenceChecker);
            this.target = realVector;
            this.model = multivariateJacobianFunction;
            this.start = realVector2;
        }

        public int getObservationSize() {
            return this.target.getDimension();
        }

        public int getParameterSize() {
            return this.start.getDimension();
        }

        public RealVector getStart() {
            RealVector realVector = this.start;
            if (realVector == null) {
                return null;
            }
            return realVector.copy();
        }

        public LeastSquaresProblem.Evaluation evaluate(RealVector realVector) {
            Pair<RealVector, RealMatrix> value = this.model.value(realVector);
            return new UnweightedEvaluation(value.getFirst(), value.getSecond(), this.target, realVector.copy());
        }

        private static class UnweightedEvaluation extends AbstractEvaluation {
            private final RealMatrix jacobian;
            private final RealVector point;
            private final RealVector residuals;

            private UnweightedEvaluation(RealVector realVector, RealMatrix realMatrix, RealVector realVector2, RealVector realVector3) {
                super(realVector2.getDimension());
                this.jacobian = realMatrix;
                this.point = realVector3;
                this.residuals = realVector2.subtract(realVector);
            }

            public RealMatrix getJacobian() {
                return this.jacobian;
            }

            public RealVector getPoint() {
                return this.point;
            }

            public RealVector getResiduals() {
                return this.residuals;
            }
        }
    }
}
