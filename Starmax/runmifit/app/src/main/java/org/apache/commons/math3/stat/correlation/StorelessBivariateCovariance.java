package org.apache.commons.math3.stat.correlation;

import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.util.LocalizedFormats;

class StorelessBivariateCovariance {
    private boolean biasCorrected;
    private double covarianceNumerator;
    private double meanX;
    private double meanY;

    /* renamed from: n */
    private double f8169n;

    public StorelessBivariateCovariance() {
        this(true);
    }

    public StorelessBivariateCovariance(boolean z) {
        this.meanY = 0.0d;
        this.meanX = 0.0d;
        this.f8169n = 0.0d;
        this.covarianceNumerator = 0.0d;
        this.biasCorrected = z;
    }

    public void increment(double d, double d2) {
        this.f8169n += 1.0d;
        double d3 = this.meanX;
        double d4 = d - d3;
        double d5 = this.meanY;
        double d6 = d2 - d5;
        double d7 = this.f8169n;
        this.meanX = d3 + (d4 / d7);
        this.meanY = d5 + (d6 / d7);
        this.covarianceNumerator += ((d7 - 1.0d) / d7) * d4 * d6;
    }

    public void append(StorelessBivariateCovariance storelessBivariateCovariance) {
        StorelessBivariateCovariance storelessBivariateCovariance2 = storelessBivariateCovariance;
        double d = this.f8169n;
        this.f8169n = storelessBivariateCovariance2.f8169n + d;
        double d2 = storelessBivariateCovariance2.meanX;
        double d3 = this.meanX;
        double d4 = d2 - d3;
        double d5 = storelessBivariateCovariance2.meanY;
        double d6 = this.meanY;
        double d7 = d5 - d6;
        double d8 = storelessBivariateCovariance2.f8169n;
        double d9 = d4 * d8;
        double d10 = d4;
        double d11 = this.f8169n;
        this.meanX = d3 + (d9 / d11);
        this.meanY = d6 + ((d7 * d8) / d11);
        this.covarianceNumerator += storelessBivariateCovariance2.covarianceNumerator + (((d * d8) / d11) * d10 * d7);
    }

    public double getN() {
        return this.f8169n;
    }

    public double getResult() throws NumberIsTooSmallException {
        double d;
        double d2 = this.f8169n;
        if (d2 >= 2.0d) {
            if (this.biasCorrected) {
                d = this.covarianceNumerator;
                d2 -= 1.0d;
            } else {
                d = this.covarianceNumerator;
            }
            return d / d2;
        }
        throw new NumberIsTooSmallException(LocalizedFormats.INSUFFICIENT_DIMENSION, Double.valueOf(this.f8169n), 2, true);
    }
}
