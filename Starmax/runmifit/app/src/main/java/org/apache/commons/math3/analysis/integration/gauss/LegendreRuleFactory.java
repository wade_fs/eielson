package org.apache.commons.math3.analysis.integration.gauss;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.util.Pair;

public class LegendreRuleFactory extends BaseRuleFactory<Double> {
    /* access modifiers changed from: protected */
    public Pair<Double[], Double[]> computeRule(int i) throws DimensionMismatchException {
        Double[] dArr;
        int i2;
        Double d;
        double d2;
        double d3;
        int i3 = i;
        Double valueOf = Double.valueOf(0.0d);
        int i4 = 1;
        if (i3 == 1) {
            return new Pair<>(new Double[]{valueOf}, new Double[]{Double.valueOf(2.0d)});
        }
        Double[] dArr2 = (Double[]) getRuleInternal(i3 - 1).getFirst();
        Double[] dArr3 = new Double[i3];
        Double[] dArr4 = new Double[i3];
        int i5 = i3 / 2;
        int i6 = 0;
        while (i6 < i5) {
            if (i6 == 0) {
                d2 = -1.0d;
            } else {
                d2 = dArr[i6 - 1].doubleValue();
            }
            if (i5 == i2) {
                d3 = 1.0d;
            } else {
                d3 = dArr[i6].doubleValue();
            }
            double d4 = d2;
            int i7 = 1;
            double d5 = 1.0d;
            while (i7 < i3) {
                int i8 = i7 + 1;
                double d6 = (double) ((i7 * 2) + 1);
                Double.isNaN(d6);
                double d7 = (double) i7;
                Double.isNaN(d7);
                double d8 = ((d6 * d2) * d4) - (d7 * d5);
                double d9 = (double) i8;
                Double.isNaN(d9);
                i7 = i8;
                d5 = d4;
                d4 = d8 / d9;
            }
            double d10 = d2;
            double d11 = d4;
            double d12 = 1.0d;
            double d13 = (d2 + d3) * 0.5d;
            double d14 = d3;
            double d15 = d13;
            boolean z = false;
            while (!z) {
                z = d14 - d10 <= Math.ulp(d13);
                d15 = d13;
                int i9 = 1;
                d12 = 1.0d;
                while (i9 < i3) {
                    double d16 = (double) ((i9 * 2) + i2);
                    Double.isNaN(d16);
                    double d17 = d13;
                    Double d18 = d;
                    double d19 = (double) i9;
                    Double.isNaN(d19);
                    double d20 = ((d16 * d17) * d15) - (d19 * d12);
                    i9++;
                    double d21 = (double) i9;
                    Double.isNaN(d21);
                    double d22 = d20 / d21;
                    d12 = d15;
                    dArr = dArr;
                    i2 = 1;
                    d15 = d22;
                    d = d18;
                    d13 = d17;
                }
                Double[] dArr5 = dArr;
                double d23 = d13;
                Double d24 = d;
                if (!z) {
                    if (d11 * d15 <= 0.0d) {
                        d14 = d23;
                    } else {
                        d10 = d23;
                        d11 = d15;
                    }
                    dArr = dArr5;
                    i2 = 1;
                    d = d24;
                    d13 = (d10 + d14) * 0.5d;
                } else {
                    d = d24;
                    dArr = dArr5;
                    i2 = 1;
                    d13 = d23;
                }
            }
            double d25 = d13;
            double d26 = (double) i3;
            Double.isNaN(d26);
            double d27 = d26 * (d12 - (d25 * d15));
            double d28 = ((1.0d - (d25 * d25)) * 2.0d) / (d27 * d27);
            dArr3[i6] = Double.valueOf(d25);
            dArr4[i6] = Double.valueOf(d28);
            int i10 = (i3 - i6) - 1;
            dArr3[i10] = Double.valueOf(-d25);
            dArr4[i10] = Double.valueOf(d28);
            i6++;
            valueOf = d;
            dArr2 = dArr;
            i4 = 1;
        }
        Double d29 = d;
        double d30 = 1.0d;
        if (i3 % 2 != 0) {
            for (int i11 = 1; i11 < i3; i11 += 2) {
                double d31 = (double) (-i11);
                Double.isNaN(d31);
                double d32 = (double) (i11 + 1);
                Double.isNaN(d32);
                d30 = (d31 * d30) / d32;
            }
            double d33 = (double) i3;
            Double.isNaN(d33);
            double d34 = d33 * d30;
            dArr3[i5] = d29;
            dArr4[i5] = Double.valueOf(2.0d / (d34 * d34));
        }
        return new Pair<>(dArr3, dArr4);
    }
}
