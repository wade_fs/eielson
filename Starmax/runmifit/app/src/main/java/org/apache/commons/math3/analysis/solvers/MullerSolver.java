package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.util.FastMath;

public class MullerSolver extends AbstractUnivariateSolver {
    private static final double DEFAULT_ABSOLUTE_ACCURACY = 1.0E-6d;

    public MullerSolver() {
        this(1.0E-6d);
    }

    public MullerSolver(double d) {
        super(d);
    }

    public MullerSolver(double d, double d2) {
        super(d, d2);
    }

    /* access modifiers changed from: protected */
    public double doSolve() throws TooManyEvaluationsException, NumberIsTooLargeException, NoBracketingException {
        double min = getMin();
        double max = getMax();
        double startValue = getStartValue();
        double functionValueAccuracy = getFunctionValueAccuracy();
        verifySequence(min, startValue, max);
        double computeObjectiveValue = computeObjectiveValue(min);
        if (FastMath.abs(computeObjectiveValue) < functionValueAccuracy) {
            return min;
        }
        double computeObjectiveValue2 = computeObjectiveValue(max);
        if (FastMath.abs(computeObjectiveValue2) < functionValueAccuracy) {
            return max;
        }
        double computeObjectiveValue3 = computeObjectiveValue(startValue);
        if (FastMath.abs(computeObjectiveValue3) < functionValueAccuracy) {
            return startValue;
        }
        verifyBracketing(min, max);
        if (isBracketing(min, startValue)) {
            return solve(min, startValue, computeObjectiveValue, computeObjectiveValue3);
        }
        return solve(startValue, max, computeObjectiveValue3, computeObjectiveValue2);
    }

    private double solve(double d, double d2, double d3, double d4) throws TooManyEvaluationsException {
        double sqrt;
        double d5;
        long j;
        double d6;
        double relativeAccuracy = getRelativeAccuracy();
        double absoluteAccuracy = getAbsoluteAccuracy();
        double functionValueAccuracy = getFunctionValueAccuracy();
        double d7 = (d + d2) * 0.5d;
        double computeObjectiveValue = computeObjectiveValue(d7);
        double d8 = d;
        double d9 = d2;
        double d10 = d4;
        double d11 = d7;
        double d12 = Double.POSITIVE_INFINITY;
        double d13 = d3;
        while (true) {
            double d14 = d11 - d8;
            double d15 = (computeObjectiveValue - d13) / d14;
            double d16 = d9 - d11;
            double d17 = d9 - d8;
            double d18 = (((d10 - computeObjectiveValue) / d16) - d15) / d17;
            double d19 = d15 + (d14 * d18);
            double d20 = (d19 * d19) - ((4.0d * computeObjectiveValue) * d18);
            double d21 = -2.0d * computeObjectiveValue;
            double sqrt2 = d11 + (d21 / (d19 + FastMath.sqrt(d20)));
            sqrt = isSequence(d8, sqrt2, d9) ? sqrt2 : d11 + (d21 / (d19 - FastMath.sqrt(d20)));
            double computeObjectiveValue2 = computeObjectiveValue(sqrt);
            if (FastMath.abs(sqrt - d12) <= FastMath.max(relativeAccuracy * FastMath.abs(sqrt), absoluteAccuracy) || FastMath.abs(computeObjectiveValue2) <= functionValueAccuracy) {
                return sqrt;
            }
            if (!((sqrt < d11 && d14 > d17 * 0.95d) || (sqrt > d11 && d16 > d17 * 0.95d) || sqrt == d11)) {
                if (sqrt >= d11) {
                    d8 = d11;
                }
                if (sqrt >= d11) {
                    d13 = computeObjectiveValue;
                }
                if (sqrt <= d11) {
                    d9 = d11;
                }
                if (sqrt > d11) {
                    computeObjectiveValue = d10;
                }
                d10 = computeObjectiveValue;
                d11 = sqrt;
                d12 = d11;
                d6 = d8;
                j = 4602678819172646912L;
                d5 = relativeAccuracy;
            } else {
                j = 4602678819172646912L;
                d6 = (d8 + d9) * 0.5d;
                double computeObjectiveValue3 = computeObjectiveValue(d6);
                if (FastMath.signum(d13) + FastMath.signum(computeObjectiveValue3) == 0.0d) {
                    d9 = d6;
                    d6 = d8;
                } else {
                    d13 = computeObjectiveValue3;
                    computeObjectiveValue3 = d10;
                }
                d5 = relativeAccuracy;
                double d22 = (d6 + d9) * 0.5d;
                d10 = computeObjectiveValue3;
                computeObjectiveValue2 = computeObjectiveValue(d22);
                d12 = Double.POSITIVE_INFINITY;
                d11 = d22;
            }
            relativeAccuracy = d5;
            d8 = d6;
            computeObjectiveValue = computeObjectiveValue2;
        }
        return sqrt;
    }
}
