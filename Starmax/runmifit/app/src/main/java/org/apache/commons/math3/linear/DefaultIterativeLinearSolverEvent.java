package org.apache.commons.math3.linear;

import org.apache.commons.math3.exception.MathUnsupportedOperationException;

public class DefaultIterativeLinearSolverEvent extends IterativeLinearSolverEvent {
    private static final long serialVersionUID = 20120129;

    /* renamed from: b */
    private final RealVector f8032b;

    /* renamed from: r */
    private final RealVector f8033r;
    private final double rnorm;

    /* renamed from: x */
    private final RealVector f8034x;

    public DefaultIterativeLinearSolverEvent(Object obj, int i, RealVector realVector, RealVector realVector2, RealVector realVector3, double d) {
        super(obj, i);
        this.f8034x = realVector;
        this.f8032b = realVector2;
        this.f8033r = realVector3;
        this.rnorm = d;
    }

    public DefaultIterativeLinearSolverEvent(Object obj, int i, RealVector realVector, RealVector realVector2, double d) {
        super(obj, i);
        this.f8034x = realVector;
        this.f8032b = realVector2;
        this.f8033r = null;
        this.rnorm = d;
    }

    public double getNormOfResidual() {
        return this.rnorm;
    }

    public RealVector getResidual() {
        RealVector realVector = this.f8033r;
        if (realVector != null) {
            return realVector;
        }
        throw new MathUnsupportedOperationException();
    }

    public RealVector getRightHandSideVector() {
        return this.f8032b;
    }

    public RealVector getSolution() {
        return this.f8034x;
    }

    public boolean providesResidual() {
        return this.f8033r != null;
    }
}
