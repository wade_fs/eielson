package org.apache.commons.math3.geometry.partitioning;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.geometry.Point;
import org.apache.commons.math3.geometry.Space;
import org.apache.commons.math3.geometry.partitioning.BSPTreeVisitor;
import org.apache.commons.math3.geometry.partitioning.Region;
import org.apache.commons.math3.util.FastMath;

class BoundaryProjector<S extends Space, T extends Space> implements BSPTreeVisitor<S> {
    private BSPTree<S> leaf = null;
    private double offset = Double.POSITIVE_INFINITY;
    private final Point<S> original;
    private Point<S> projected = null;

    public BoundaryProjector(Point<S> point) {
        this.original = point;
    }

    public BSPTreeVisitor.Order visitOrder(BSPTree<S> bSPTree) {
        if (bSPTree.getCut().getHyperplane().getOffset(this.original) <= 0.0d) {
            return BSPTreeVisitor.Order.MINUS_SUB_PLUS;
        }
        return BSPTreeVisitor.Order.PLUS_SUB_MINUS;
    }

    public void visitInternalNode(BSPTree<S> bSPTree) {
        Hyperplane<S> hyperplane = bSPTree.getCut().getHyperplane();
        double offset2 = hyperplane.getOffset(this.original);
        if (FastMath.abs(offset2) < this.offset) {
            Point<S> project = hyperplane.project(this.original);
            List<Region<T>> boundaryRegions = boundaryRegions(bSPTree);
            boolean z = false;
            for (Region region : boundaryRegions) {
                if (!z && belongsToPart(project, hyperplane, region)) {
                    this.projected = project;
                    this.offset = FastMath.abs(offset2);
                    z = true;
                }
            }
            if (!z) {
                for (Region<T> region2 : boundaryRegions) {
                    Point<S> singularProjection = singularProjection(project, hyperplane, region2);
                    if (singularProjection != null) {
                        double distance = this.original.distance(singularProjection);
                        if (distance < this.offset) {
                            this.projected = singularProjection;
                            this.offset = distance;
                        }
                    }
                }
            }
        }
    }

    public void visitLeafNode(BSPTree<S> bSPTree) {
        if (this.leaf == null) {
            this.leaf = bSPTree;
        }
    }

    public BoundaryProjection<S> getProjection() {
        this.offset = FastMath.copySign(this.offset, ((Boolean) this.leaf.getAttribute()).booleanValue() ? -1.0d : 1.0d);
        return new BoundaryProjection<>(this.original, this.projected, this.offset);
    }

    private List<Region<T>> boundaryRegions(BSPTree<S> bSPTree) {
        ArrayList arrayList = new ArrayList(2);
        BoundaryAttribute boundaryAttribute = (BoundaryAttribute) bSPTree.getAttribute();
        addRegion(boundaryAttribute.getPlusInside(), arrayList);
        addRegion(boundaryAttribute.getPlusOutside(), arrayList);
        return arrayList;
    }

    private void addRegion(SubHyperplane<S> subHyperplane, List<Region<T>> list) {
        Region remainingRegion;
        if (subHyperplane != null && (remainingRegion = ((AbstractSubHyperplane) subHyperplane).getRemainingRegion()) != null) {
            list.add(remainingRegion);
        }
    }

    private boolean belongsToPart(Point<S> point, Hyperplane<S> hyperplane, Region<T> region) {
        return region.checkPoint(((Embedding) hyperplane).toSubSpace(point)) != Region.Location.OUTSIDE;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [org.apache.commons.math3.geometry.Point<S>, org.apache.commons.math3.geometry.Point] */
    /* JADX WARN: Type inference failed for: r3v0, types: [org.apache.commons.math3.geometry.partitioning.Region, org.apache.commons.math3.geometry.partitioning.Region<T>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.commons.math3.geometry.Point<S> singularProjection(org.apache.commons.math3.geometry.Point<S> r1, org.apache.commons.math3.geometry.partitioning.Hyperplane<S> r2, org.apache.commons.math3.geometry.partitioning.Region<T> r3) {
        /*
            r0 = this;
            org.apache.commons.math3.geometry.partitioning.Embedding r2 = (org.apache.commons.math3.geometry.partitioning.Embedding) r2
            org.apache.commons.math3.geometry.Point r1 = r2.toSubSpace(r1)
            org.apache.commons.math3.geometry.partitioning.BoundaryProjection r1 = r3.projectToBoundary(r1)
            org.apache.commons.math3.geometry.Point r3 = r1.getProjected()
            if (r3 != 0) goto L_0x0012
            r1 = 0
            goto L_0x001a
        L_0x0012:
            org.apache.commons.math3.geometry.Point r1 = r1.getProjected()
            org.apache.commons.math3.geometry.Point r1 = r2.toSpace(r1)
        L_0x001a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.geometry.partitioning.BoundaryProjector.singularProjection(org.apache.commons.math3.geometry.Point, org.apache.commons.math3.geometry.partitioning.Hyperplane, org.apache.commons.math3.geometry.partitioning.Region):org.apache.commons.math3.geometry.Point");
    }
}
