package org.apache.commons.math3.optim.nonlinear.vector.jacobian;

import java.util.Arrays;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.MathUnsupportedOperationException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.optim.PointVectorValuePair;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Precision;

@Deprecated
public class LevenbergMarquardtOptimizer extends AbstractLeastSquaresOptimizer {
    private static final double TWO_EPS = (Precision.EPSILON * 2.0d);
    private double[] beta;
    private final double costRelativeTolerance;
    private double[] diagR;
    private final double initialStepBoundFactor;
    private double[] jacNorm;
    private double[] lmDir;
    private double lmPar;
    private final double orthoTolerance;
    private final double parRelativeTolerance;
    private int[] permutation;
    private final double qrRankingThreshold;
    private int rank;
    private int solvedCols;
    private double[][] weightedJacobian;
    private double[] weightedResidual;

    public LevenbergMarquardtOptimizer() {
        this(100.0d, 1.0E-10d, 1.0E-10d, 1.0E-10d, Precision.SAFE_MIN);
    }

    public LevenbergMarquardtOptimizer(ConvergenceChecker<PointVectorValuePair> convergenceChecker) {
        this(100.0d, convergenceChecker, 1.0E-10d, 1.0E-10d, 1.0E-10d, Precision.SAFE_MIN);
    }

    public LevenbergMarquardtOptimizer(double d, ConvergenceChecker<PointVectorValuePair> convergenceChecker, double d2, double d3, double d4, double d5) {
        super(convergenceChecker);
        this.initialStepBoundFactor = d;
        this.costRelativeTolerance = d2;
        this.parRelativeTolerance = d3;
        this.orthoTolerance = d4;
        this.qrRankingThreshold = d5;
    }

    public LevenbergMarquardtOptimizer(double d, double d2, double d3) {
        this(100.0d, d, d2, d3, Precision.SAFE_MIN);
    }

    public LevenbergMarquardtOptimizer(double d, double d2, double d3, double d4, double d5) {
        super(null);
        this.initialStepBoundFactor = d;
        this.costRelativeTolerance = d2;
        this.parRelativeTolerance = d3;
        this.orthoTolerance = d4;
        this.qrRankingThreshold = d5;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x02e8  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x035c  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x0390 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x02b6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.math3.optim.PointVectorValuePair doOptimize() {
        /*
            r60 = this;
            r8 = r60
            r60.checkParameters()
            double[] r0 = r60.getTarget()
            int r9 = r0.length
            double[] r10 = r60.getStartPoint()
            int r11 = r10.length
            int r0 = org.apache.commons.math3.util.FastMath.min(r9, r11)
            r8.solvedCols = r0
            double[] r0 = new double[r11]
            r8.diagR = r0
            double[] r0 = new double[r11]
            r8.jacNorm = r0
            double[] r0 = new double[r11]
            r8.beta = r0
            int[] r0 = new int[r11]
            r8.permutation = r0
            double[] r0 = new double[r11]
            r8.lmDir = r0
            double[] r12 = new double[r11]
            double[] r13 = new double[r11]
            double[] r0 = new double[r9]
            double[] r1 = new double[r9]
            double[] r14 = new double[r9]
            double[] r15 = new double[r11]
            double[] r7 = new double[r11]
            double[] r6 = new double[r11]
            org.apache.commons.math3.linear.RealMatrix r5 = r60.getWeightSquareRoot()
            double[] r1 = r8.computeObjectiveValue(r10)
            double[] r2 = r8.computeResiduals(r1)
            org.apache.commons.math3.optim.PointVectorValuePair r3 = new org.apache.commons.math3.optim.PointVectorValuePair
            r3.<init>(r10, r1)
            double r16 = r8.computeCost(r2)
            r19 = r6
            r18 = r7
            r6 = 0
            r8.lmPar = r6
            org.apache.commons.math3.optim.ConvergenceChecker r4 = r60.getConvergenceChecker()
            r6 = r16
            r22 = 0
            r24 = 0
            r17 = r3
            r3 = r1
            r1 = r0
            r0 = 1
        L_0x0065:
            r60.incrementIterationCount()
            r26 = r1
            org.apache.commons.math3.linear.RealMatrix r1 = r8.computeWeightedJacobian(r10)
            r8.qrDecomposition(r1)
            double[] r1 = r5.operate(r2)
            r8.weightedResidual = r1
            r27 = 0
            r1 = 0
        L_0x007a:
            if (r1 >= r9) goto L_0x0089
            r28 = r2
            double[] r2 = r8.weightedResidual
            r29 = r2[r1]
            r14[r1] = r29
            int r1 = r1 + 1
            r2 = r28
            goto L_0x007a
        L_0x0089:
            r28 = r2
            r8.qTy(r14)
            r1 = 0
        L_0x008f:
            int r2 = r8.solvedCols
            if (r1 >= r2) goto L_0x00ac
            int[] r2 = r8.permutation
            r2 = r2[r1]
            r29 = r3
            double[][] r3 = r8.weightedJacobian
            r3 = r3[r1]
            r30 = r4
            double[] r4 = r8.diagR
            r31 = r4[r2]
            r3[r2] = r31
            int r1 = r1 + 1
            r3 = r29
            r4 = r30
            goto L_0x008f
        L_0x00ac:
            r29 = r3
            r30 = r4
            r31 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            if (r0 == 0) goto L_0x00e6
            r1 = 0
            r2 = 0
        L_0x00b7:
            if (r1 >= r11) goto L_0x00d2
            double[] r4 = r8.jacNorm
            r22 = r4[r1]
            r20 = 0
            int r4 = (r22 > r20 ? 1 : (r22 == r20 ? 0 : -1))
            if (r4 != 0) goto L_0x00c5
            r22 = r31
        L_0x00c5:
            r24 = r10[r1]
            double r24 = r24 * r22
            double r24 = r24 * r24
            double r2 = r2 + r24
            r12[r1] = r22
            int r1 = r1 + 1
            goto L_0x00b7
        L_0x00d2:
            double r24 = org.apache.commons.math3.util.FastMath.sqrt(r2)
            r20 = 0
            int r1 = (r24 > r20 ? 1 : (r24 == r20 ? 0 : -1))
            if (r1 != 0) goto L_0x00df
            double r1 = r8.initialStepBoundFactor
            goto L_0x00e3
        L_0x00df:
            double r1 = r8.initialStepBoundFactor
            double r1 = r1 * r24
        L_0x00e3:
            r22 = r1
            goto L_0x00e8
        L_0x00e6:
            r20 = 0
        L_0x00e8:
            int r1 = (r6 > r20 ? 1 : (r6 == r20 ? 0 : -1))
            if (r1 == 0) goto L_0x0138
            r2 = r20
            r1 = 0
        L_0x00ef:
            int r4 = r8.solvedCols
            if (r1 >= r4) goto L_0x0131
            int[] r4 = r8.permutation
            r4 = r4[r1]
            r33 = r0
            double[] r0 = r8.jacNorm
            r34 = r0[r4]
            int r0 = (r34 > r20 ? 1 : (r34 == r20 ? 0 : -1))
            if (r0 == 0) goto L_0x0128
            r36 = r20
            r0 = 0
        L_0x0104:
            if (r0 > r1) goto L_0x0119
            r38 = r5
            double[][] r5 = r8.weightedJacobian
            r5 = r5[r0]
            r39 = r5[r4]
            r41 = r14[r0]
            double r39 = r39 * r41
            double r36 = r36 + r39
            int r0 = r0 + 1
            r5 = r38
            goto L_0x0104
        L_0x0119:
            r38 = r5
            double r4 = org.apache.commons.math3.util.FastMath.abs(r36)
            double r34 = r34 * r6
            double r4 = r4 / r34
            double r2 = org.apache.commons.math3.util.FastMath.max(r2, r4)
            goto L_0x012a
        L_0x0128:
            r38 = r5
        L_0x012a:
            int r1 = r1 + 1
            r0 = r33
            r5 = r38
            goto L_0x00ef
        L_0x0131:
            r33 = r0
            r38 = r5
            r34 = r2
            goto L_0x013e
        L_0x0138:
            r33 = r0
            r38 = r5
            r34 = r20
        L_0x013e:
            double r0 = r8.orthoTolerance
            int r2 = (r34 > r0 ? 1 : (r34 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x0148
            r8.setCost(r6)
            return r17
        L_0x0148:
            r0 = 0
        L_0x0149:
            if (r0 >= r11) goto L_0x015a
            r1 = r12[r0]
            double[] r3 = r8.jacNorm
            r4 = r3[r0]
            double r1 = org.apache.commons.math3.util.FastMath.max(r1, r4)
            r12[r0] = r1
            int r0 = r0 + 1
            goto L_0x0149
        L_0x015a:
            r3 = r17
            r0 = r20
            r4 = r26
            r2 = r28
            r5 = r29
            r58 = r6
            r6 = r22
            r22 = r58
        L_0x016a:
            r28 = 4547007122018943789(0x3f1a36e2eb1c432d, double:1.0E-4)
            int r26 = (r0 > r28 ? 1 : (r0 == r28 ? 0 : -1))
            if (r26 >= 0) goto L_0x03a2
            r0 = 0
        L_0x0174:
            int r1 = r8.solvedCols
            if (r0 >= r1) goto L_0x0183
            int[] r1 = r8.permutation
            r1 = r1[r0]
            r2 = r10[r1]
            r13[r1] = r2
            int r0 = r0 + 1
            goto L_0x0174
        L_0x0183:
            double[] r2 = r8.weightedResidual
            r8.weightedResidual = r4
            r0 = r60
            r1 = r14
            r4 = r2
            r2 = r6
            r26 = r9
            r9 = r30
            r30 = r14
            r14 = r4
            r4 = r12
            r43 = r5
            r36 = r38
            r5 = r15
            r16 = r19
            r37 = r20
            r20 = r14
            r19 = r15
            r14 = r6
            r7 = 1
            r6 = r18
            r44 = r17
            r17 = r18
            r7 = r16
            r0.determineLMParameter(r1, r2, r4, r5, r6, r7)
            r1 = r37
            r0 = 0
        L_0x01b1:
            int r3 = r8.solvedCols
            if (r0 >= r3) goto L_0x01d4
            int[] r3 = r8.permutation
            r3 = r3[r0]
            double[] r4 = r8.lmDir
            r5 = r4[r3]
            double r5 = -r5
            r4[r3] = r5
            r5 = r13[r3]
            r39 = r4[r3]
            double r5 = r5 + r39
            r10[r3] = r5
            r5 = r12[r3]
            r3 = r4[r3]
            double r5 = r5 * r3
            double r5 = r5 * r5
            double r1 = r1 + r5
            int r0 = r0 + 1
            goto L_0x01b1
        L_0x01d4:
            double r0 = org.apache.commons.math3.util.FastMath.sqrt(r1)
            if (r33 == 0) goto L_0x01df
            double r6 = org.apache.commons.math3.util.FastMath.min(r14, r0)
            goto L_0x01e0
        L_0x01df:
            r6 = r14
        L_0x01e0:
            double[] r5 = r8.computeObjectiveValue(r10)
            double[] r2 = r8.computeResiduals(r5)
            org.apache.commons.math3.optim.PointVectorValuePair r3 = new org.apache.commons.math3.optim.PointVectorValuePair
            r3.<init>(r10, r5)
            double r14 = r8.computeCost(r2)
            r39 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r41 = 4591870180066957722(0x3fb999999999999a, double:0.1)
            double r45 = r14 * r41
            int r4 = (r45 > r22 ? 1 : (r45 == r22 ? 0 : -1))
            if (r4 >= 0) goto L_0x0204
            double r39 = r14 / r22
            double r39 = r39 * r39
            double r39 = r31 - r39
        L_0x0204:
            r18 = r2
            r4 = 0
        L_0x0207:
            int r2 = r8.solvedCols
            if (r4 >= r2) goto L_0x0236
            int[] r2 = r8.permutation
            r2 = r2[r4]
            r21 = r5
            double[] r5 = r8.lmDir
            r47 = r5[r2]
            r19[r4] = r37
            r5 = 0
        L_0x0218:
            if (r5 > r4) goto L_0x022f
            r49 = r19[r5]
            r51 = r13
            double[][] r13 = r8.weightedJacobian
            r13 = r13[r5]
            r52 = r13[r2]
            double r52 = r52 * r47
            double r49 = r49 + r52
            r19[r5] = r49
            int r5 = r5 + 1
            r13 = r51
            goto L_0x0218
        L_0x022f:
            r51 = r13
            int r4 = r4 + 1
            r5 = r21
            goto L_0x0207
        L_0x0236:
            r21 = r5
            r51 = r13
            r4 = r37
            r2 = 0
        L_0x023d:
            int r13 = r8.solvedCols
            if (r2 >= r13) goto L_0x024c
            r47 = r19[r2]
            r49 = r19[r2]
            double r47 = r47 * r49
            double r4 = r4 + r47
            int r2 = r2 + 1
            goto L_0x023d
        L_0x024c:
            double r47 = r22 * r22
            double r4 = r4 / r47
            r49 = r14
            double r13 = r8.lmPar
            double r13 = r13 * r0
            double r13 = r13 * r0
            double r13 = r13 / r47
            r47 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r52 = r13 * r47
            double r52 = r4 + r52
            double r4 = r4 + r13
            double r4 = -r4
            int r2 = (r52 > r37 ? 1 : (r52 == r37 ? 0 : -1))
            if (r2 != 0) goto L_0x0269
            r13 = r37
            goto L_0x026b
        L_0x0269:
            double r13 = r39 / r52
        L_0x026b:
            r54 = 4598175219545276416(0x3fd0000000000000, double:0.25)
            r56 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            int r2 = (r13 > r54 ? 1 : (r13 == r54 ? 0 : -1))
            if (r2 > 0) goto L_0x029d
            int r2 = (r39 > r37 ? 1 : (r39 == r37 ? 0 : -1))
            if (r2 >= 0) goto L_0x0280
            double r54 = r4 * r56
            double r56 = r56 * r39
            double r4 = r4 + r56
            double r54 = r54 / r4
            goto L_0x0282
        L_0x0280:
            r54 = r56
        L_0x0282:
            int r2 = (r45 > r22 ? 1 : (r45 == r22 ? 0 : -1))
            if (r2 >= 0) goto L_0x028a
            int r2 = (r54 > r41 ? 1 : (r54 == r41 ? 0 : -1))
            if (r2 >= 0) goto L_0x028c
        L_0x028a:
            r54 = r41
        L_0x028c:
            r4 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r0 = r0 * r4
            double r0 = org.apache.commons.math3.util.FastMath.min(r6, r0)
            double r0 = r0 * r54
            double r4 = r8.lmPar
            double r4 = r4 / r54
            r8.lmPar = r4
            goto L_0x02b1
        L_0x029d:
            double r4 = r8.lmPar
            int r2 = (r4 > r37 ? 1 : (r4 == r37 ? 0 : -1))
            if (r2 == 0) goto L_0x02a9
            r4 = 4604930618986332160(0x3fe8000000000000, double:0.75)
            int r2 = (r13 > r4 ? 1 : (r13 == r4 ? 0 : -1))
            if (r2 < 0) goto L_0x02b2
        L_0x02a9:
            double r0 = r0 * r47
            double r4 = r8.lmPar
            double r4 = r4 * r56
            r8.lmPar = r4
        L_0x02b1:
            r6 = r0
        L_0x02b2:
            int r0 = (r13 > r28 ? 1 : (r13 == r28 ? 0 : -1))
            if (r0 < 0) goto L_0x02e8
            r1 = r37
            r0 = 0
        L_0x02b9:
            if (r0 >= r11) goto L_0x02c7
            r4 = r12[r0]
            r22 = r10[r0]
            double r4 = r4 * r22
            double r4 = r4 * r4
            double r1 = r1 + r4
            int r0 = r0 + 1
            goto L_0x02b9
        L_0x02c7:
            double r24 = org.apache.commons.math3.util.FastMath.sqrt(r1)
            if (r9 == 0) goto L_0x02df
            int r0 = r60.getIterations()
            r1 = r44
            boolean r0 = r9.converged(r0, r1, r3)
            r4 = r49
            if (r0 == 0) goto L_0x02e3
            r8.setCost(r4)
            return r3
        L_0x02df:
            r1 = r44
            r4 = r49
        L_0x02e3:
            r0 = r20
            r33 = 0
            goto L_0x030c
        L_0x02e8:
            r1 = r44
            r0 = 0
        L_0x02eb:
            int r2 = r8.solvedCols
            if (r0 >= r2) goto L_0x02fa
            int[] r2 = r8.permutation
            r2 = r2[r0]
            r3 = r51[r2]
            r10[r2] = r3
            int r0 = r0 + 1
            goto L_0x02eb
        L_0x02fa:
            double[] r0 = r8.weightedResidual
            r2 = r20
            r8.weightedResidual = r2
            org.apache.commons.math3.optim.PointVectorValuePair r2 = new org.apache.commons.math3.optim.PointVectorValuePair
            r5 = r43
            r2.<init>(r10, r5)
            r3 = r2
            r21 = r5
            r4 = r22
        L_0x030c:
            double r22 = org.apache.commons.math3.util.FastMath.abs(r39)
            r2 = r0
            r44 = r1
            double r0 = r8.costRelativeTolerance
            int r15 = (r22 > r0 ? 1 : (r22 == r0 ? 0 : -1))
            if (r15 > 0) goto L_0x0321
            int r15 = (r52 > r0 ? 1 : (r52 == r0 ? 0 : -1))
            if (r15 > 0) goto L_0x0321
            int r0 = (r13 > r47 ? 1 : (r13 == r47 ? 0 : -1))
            if (r0 <= 0) goto L_0x0329
        L_0x0321:
            double r0 = r8.parRelativeTolerance
            double r0 = r0 * r24
            int r15 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r15 > 0) goto L_0x032d
        L_0x0329:
            r8.setCost(r4)
            return r3
        L_0x032d:
            double r0 = org.apache.commons.math3.util.FastMath.abs(r39)
            double r22 = org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer.TWO_EPS
            int r15 = (r0 > r22 ? 1 : (r0 == r22 ? 0 : -1))
            if (r15 > 0) goto L_0x0353
            int r0 = (r52 > r22 ? 1 : (r52 == r22 ? 0 : -1))
            if (r0 > 0) goto L_0x0353
            int r0 = (r13 > r47 ? 1 : (r13 == r47 ? 0 : -1))
            if (r0 <= 0) goto L_0x0340
            goto L_0x0353
        L_0x0340:
            org.apache.commons.math3.exception.ConvergenceException r0 = new org.apache.commons.math3.exception.ConvergenceException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.TOO_SMALL_COST_RELATIVE_TOLERANCE
            r15 = 1
            java.lang.Object[] r2 = new java.lang.Object[r15]
            double r3 = r8.costRelativeTolerance
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            r2[r27] = r3
            r0.<init>(r1, r2)
            throw r0
        L_0x0353:
            r15 = 1
            double r0 = org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer.TWO_EPS
            double r22 = r0 * r24
            int r20 = (r6 > r22 ? 1 : (r6 == r22 ? 0 : -1))
            if (r20 <= 0) goto L_0x0390
            int r20 = (r34 > r0 ? 1 : (r34 == r0 ? 0 : -1))
            if (r20 <= 0) goto L_0x037e
            r22 = r4
            r0 = r13
            r15 = r19
            r5 = r21
            r14 = r30
            r20 = r37
            r13 = r51
            r4 = r2
            r30 = r9
            r19 = r16
            r2 = r18
            r9 = r26
            r38 = r36
            r18 = r17
            r17 = r44
            goto L_0x016a
        L_0x037e:
            org.apache.commons.math3.exception.ConvergenceException r0 = new org.apache.commons.math3.exception.ConvergenceException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.TOO_SMALL_ORTHOGONALITY_TOLERANCE
            java.lang.Object[] r2 = new java.lang.Object[r15]
            double r3 = r8.orthoTolerance
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            r2[r27] = r3
            r0.<init>(r1, r2)
            throw r0
        L_0x0390:
            org.apache.commons.math3.exception.ConvergenceException r0 = new org.apache.commons.math3.exception.ConvergenceException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.TOO_SMALL_PARAMETERS_RELATIVE_TOLERANCE
            java.lang.Object[] r2 = new java.lang.Object[r15]
            double r3 = r8.parRelativeTolerance
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            r2[r27] = r3
            r0.<init>(r1, r2)
            throw r0
        L_0x03a2:
            r26 = r9
            r16 = r19
            r9 = r30
            r36 = r38
            r0 = 1
            r30 = r14
            r19 = r15
            r14 = r6
            r17 = r3
            r1 = r4
            r3 = r5
            r4 = r9
            r6 = r22
            r9 = r26
            r0 = r33
            r5 = r36
            r22 = r14
            r15 = r19
            r14 = r30
            r19 = r16
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer.doOptimize():org.apache.commons.math3.optim.PointVectorValuePair");
    }

    private void determineLMParameter(double[] dArr, double d, double[] dArr2, double[] dArr3, double[] dArr4, double[] dArr5) {
        int i;
        double d2;
        double d3;
        double[] dArr6 = dArr;
        double d4 = d;
        double[] dArr7 = dArr3;
        int length = this.weightedJacobian[0].length;
        int i2 = 0;
        while (true) {
            i = this.rank;
            if (i2 >= i) {
                break;
            }
            this.lmDir[this.permutation[i2]] = dArr6[i2];
            i2++;
        }
        while (i < length) {
            this.lmDir[this.permutation[i]] = 0.0d;
            i++;
        }
        for (int i3 = this.rank - 1; i3 >= 0; i3--) {
            int i4 = this.permutation[i3];
            double d5 = this.lmDir[i4] / this.diagR[i4];
            for (int i5 = 0; i5 < i3; i5++) {
                double[] dArr8 = this.lmDir;
                int i6 = this.permutation[i5];
                dArr8[i6] = dArr8[i6] - (this.weightedJacobian[i5][i4] * d5);
            }
            this.lmDir[i4] = d5;
        }
        double d6 = 0.0d;
        for (int i7 = 0; i7 < this.solvedCols; i7++) {
            int i8 = this.permutation[i7];
            double d7 = dArr2[i8] * this.lmDir[i8];
            dArr7[i8] = d7;
            d6 += d7 * d7;
        }
        double sqrt = FastMath.sqrt(d6);
        double d8 = sqrt - d4;
        double d9 = d4 * 0.1d;
        if (d8 <= d9) {
            this.lmPar = 0.0d;
            return;
        }
        if (this.rank == this.solvedCols) {
            for (int i9 = 0; i9 < this.solvedCols; i9++) {
                int i10 = this.permutation[i9];
                dArr7[i10] = dArr7[i10] * (dArr2[i10] / sqrt);
            }
            int i11 = 0;
            double d10 = 0.0d;
            while (i11 < this.solvedCols) {
                int i12 = this.permutation[i11];
                int i13 = 0;
                double d11 = 0.0d;
                while (i13 < i11) {
                    d11 += this.weightedJacobian[i13][i12] * dArr7[this.permutation[i13]];
                    i13++;
                    d9 = d9;
                }
                double d12 = d9;
                double d13 = (dArr7[i12] - d11) / this.diagR[i12];
                dArr7[i12] = d13;
                d10 += d13 * d13;
                i11++;
                d9 = d12;
            }
            d2 = d9;
            d3 = d8 / (d4 * d10);
        } else {
            d2 = d9;
            d3 = 0.0d;
        }
        double d14 = d8;
        double d15 = 0.0d;
        for (int i14 = 0; i14 < this.solvedCols; i14++) {
            int i15 = this.permutation[i14];
            double d16 = 0.0d;
            for (int i16 = 0; i16 <= i14; i16++) {
                d16 += this.weightedJacobian[i16][i15] * dArr6[i16];
            }
            double d17 = d16 / dArr2[i15];
            d15 += d17 * d17;
        }
        double sqrt2 = FastMath.sqrt(d15);
        double d18 = sqrt2 / d4;
        if (d18 == 0.0d) {
            d18 = Precision.SAFE_MIN / FastMath.min(d4, 0.1d);
        }
        this.lmPar = FastMath.min(d18, FastMath.max(this.lmPar, d3));
        double d19 = 0.0d;
        if (this.lmPar == 0.0d) {
            this.lmPar = sqrt2 / sqrt;
        }
        int i17 = 10;
        while (i17 >= 0) {
            if (this.lmPar == d19) {
                this.lmPar = FastMath.max(Precision.SAFE_MIN, 0.001d * d18);
            }
            double sqrt3 = FastMath.sqrt(this.lmPar);
            for (int i18 = 0; i18 < this.solvedCols; i18++) {
                int i19 = this.permutation[i18];
                dArr7[i19] = dArr2[i19] * sqrt3;
            }
            double[] dArr9 = dArr4;
            double[] dArr10 = dArr5;
            determineLMDirection(dArr6, dArr7, dArr9, dArr10);
            double d20 = 0.0d;
            for (int i20 = 0; i20 < this.solvedCols; i20++) {
                int i21 = this.permutation[i20];
                double d21 = dArr2[i21] * this.lmDir[i21];
                dArr10[i21] = d21;
                d20 += d21 * d21;
            }
            double sqrt4 = FastMath.sqrt(d20);
            double d22 = sqrt4 - d4;
            if (FastMath.abs(d22) <= d2) {
                return;
            }
            if (d3 != 0.0d || d22 > d14 || d14 >= 0.0d) {
                for (int i22 = 0; i22 < this.solvedCols; i22++) {
                    int i23 = this.permutation[i22];
                    dArr7[i23] = (dArr10[i23] * dArr2[i23]) / sqrt4;
                }
                int i24 = 0;
                while (i24 < this.solvedCols) {
                    int i25 = this.permutation[i24];
                    dArr7[i25] = dArr7[i25] / dArr9[i24];
                    double d23 = dArr7[i25];
                    i24++;
                    for (int i26 = i24; i26 < this.solvedCols; i26++) {
                        int i27 = this.permutation[i26];
                        dArr7[i27] = dArr7[i27] - (this.weightedJacobian[i26][i25] * d23);
                    }
                }
                double d24 = 0.0d;
                for (int i28 = 0; i28 < this.solvedCols; i28++) {
                    double d25 = dArr7[this.permutation[i28]];
                    d24 += d25 * d25;
                }
                double d26 = d22 / (d24 * d4);
                if (d22 > 0.0d) {
                    d3 = FastMath.max(d3, this.lmPar);
                } else if (d22 < 0.0d) {
                    d18 = FastMath.min(d18, this.lmPar);
                }
                this.lmPar = FastMath.max(d3, this.lmPar + d26);
                i17--;
                dArr6 = dArr;
                d4 = d;
                d14 = d22;
                d19 = 0.0d;
            } else {
                return;
            }
        }
    }

    private void determineLMDirection(double[] dArr, double[] dArr2, double[] dArr3, double[] dArr4) {
        int i;
        int i2;
        double d;
        double d2;
        double[] dArr5 = dArr3;
        int i3 = 0;
        while (i3 < this.solvedCols) {
            int i4 = this.permutation[i3];
            int i5 = i3 + 1;
            for (int i6 = i5; i6 < this.solvedCols; i6++) {
                double[][] dArr6 = this.weightedJacobian;
                dArr6[i6][i4] = dArr6[i3][this.permutation[i6]];
            }
            this.lmDir[i3] = this.diagR[i4];
            dArr4[i3] = dArr[i3];
            i3 = i5;
        }
        int i7 = 0;
        while (true) {
            i = this.solvedCols;
            double d3 = 0.0d;
            if (i7 >= i) {
                break;
            }
            double d4 = dArr2[this.permutation[i7]];
            if (d4 != 0.0d) {
                Arrays.fill(dArr5, i7 + 1, dArr5.length, 0.0d);
            }
            dArr5[i7] = d4;
            int i8 = i7;
            double d5 = 0.0d;
            while (i8 < this.solvedCols) {
                int i9 = this.permutation[i8];
                if (dArr5[i8] != d3) {
                    double d6 = this.weightedJacobian[i8][i9];
                    if (FastMath.abs(d6) < FastMath.abs(dArr5[i8])) {
                        double d7 = d6 / dArr5[i8];
                        d = 1.0d / FastMath.sqrt((d7 * d7) + 1.0d);
                        d2 = d7 * d;
                    } else {
                        double d8 = dArr5[i8] / d6;
                        double sqrt = 1.0d / FastMath.sqrt((d8 * d8) + 1.0d);
                        d = sqrt * d8;
                        d2 = sqrt;
                    }
                    double d9 = d;
                    this.weightedJacobian[i8][i9] = (d6 * d2) + (dArr5[i8] * d9);
                    double d10 = (dArr4[i8] * d2) + (d9 * d5);
                    i2 = i7;
                    double d11 = -d9;
                    double d12 = (dArr4[i8] * d11) + (d5 * d2);
                    dArr4[i8] = d10;
                    for (int i10 = i8 + 1; i10 < this.solvedCols; i10++) {
                        double[][] dArr7 = this.weightedJacobian;
                        double d13 = dArr7[i10][i9];
                        double d14 = (d2 * d13) + (dArr5[i10] * d9);
                        dArr5[i10] = (d13 * d11) + (dArr5[i10] * d2);
                        dArr7[i10][i9] = d14;
                    }
                    d5 = d12;
                } else {
                    i2 = i7;
                }
                i8++;
                i7 = i2;
                d3 = 0.0d;
            }
            int i11 = i7;
            double[][] dArr8 = this.weightedJacobian;
            double[] dArr9 = dArr8[i11];
            int[] iArr = this.permutation;
            dArr5[i11] = dArr9[iArr[i11]];
            dArr8[i11][iArr[i11]] = this.lmDir[i11];
            i7 = i11 + 1;
        }
        int i12 = 0;
        while (true) {
            int i13 = this.solvedCols;
            if (i12 >= i13) {
                break;
            }
            if (dArr5[i12] == 0.0d && i == i13) {
                i = i12;
            }
            if (i < this.solvedCols) {
                dArr4[i12] = 0.0d;
            }
            i12++;
        }
        if (i > 0) {
            for (int i14 = i - 1; i14 >= 0; i14--) {
                int i15 = this.permutation[i14];
                double d15 = 0.0d;
                for (int i16 = i14 + 1; i16 < i; i16++) {
                    d15 += this.weightedJacobian[i16][i15] * dArr4[i16];
                }
                dArr4[i14] = (dArr4[i14] - d15) / dArr5[i14];
            }
        }
        int i17 = 0;
        while (true) {
            double[] dArr10 = this.lmDir;
            if (i17 < dArr10.length) {
                dArr10[this.permutation[i17]] = dArr4[i17];
                i17++;
            } else {
                return;
            }
        }
    }

    private void qrDecomposition(RealMatrix realMatrix) throws ConvergenceException {
        this.weightedJacobian = realMatrix.scalarMultiply(-1.0d).getData();
        double[][] dArr = this.weightedJacobian;
        int length = dArr.length;
        int length2 = dArr[0].length;
        for (int i = 0; i < length2; i++) {
            this.permutation[i] = i;
            double d = 0.0d;
            for (int i2 = 0; i2 < length; i2++) {
                double d2 = this.weightedJacobian[i2][i];
                d += d2 * d2;
            }
            this.jacNorm[i] = FastMath.sqrt(d);
        }
        for (int i3 = 0; i3 < length2; i3++) {
            double d3 = Double.NEGATIVE_INFINITY;
            int i4 = -1;
            for (int i5 = i3; i5 < length2; i5++) {
                double d4 = 0.0d;
                for (int i6 = i3; i6 < length; i6++) {
                    double d5 = this.weightedJacobian[i6][this.permutation[i5]];
                    d4 += d5 * d5;
                }
                if (Double.isInfinite(d4) || Double.isNaN(d4)) {
                    throw new ConvergenceException(LocalizedFormats.UNABLE_TO_PERFORM_QR_DECOMPOSITION_ON_JACOBIAN, Integer.valueOf(length), Integer.valueOf(length2));
                }
                if (d4 > d3) {
                    i4 = i5;
                    d3 = d4;
                }
            }
            if (d3 <= this.qrRankingThreshold) {
                this.rank = i3;
                return;
            }
            int[] iArr = this.permutation;
            int i7 = iArr[i4];
            iArr[i4] = iArr[i3];
            iArr[i3] = i7;
            double d6 = this.weightedJacobian[i3][i7];
            int i8 = (d6 > 0.0d ? 1 : (d6 == 0.0d ? 0 : -1));
            double sqrt = FastMath.sqrt(d3);
            if (i8 > 0) {
                sqrt = -sqrt;
            }
            double d7 = 1.0d / (d3 - (d6 * sqrt));
            this.beta[i7] = d7;
            this.diagR[i7] = sqrt;
            double[] dArr2 = this.weightedJacobian[i3];
            dArr2[i7] = dArr2[i7] - sqrt;
            for (int i9 = (length2 - 1) - i3; i9 > 0; i9--) {
                double d8 = 0.0d;
                for (int i10 = i3; i10 < length; i10++) {
                    double[][] dArr3 = this.weightedJacobian;
                    d8 += dArr3[i10][i7] * dArr3[i10][this.permutation[i3 + i9]];
                }
                double d9 = d8 * d7;
                for (int i11 = i3; i11 < length; i11++) {
                    double[][] dArr4 = this.weightedJacobian;
                    double[] dArr5 = dArr4[i11];
                    int i12 = this.permutation[i3 + i9];
                    dArr5[i12] = dArr5[i12] - (dArr4[i11][i7] * d9);
                }
            }
        }
        this.rank = this.solvedCols;
    }

    private void qTy(double[] dArr) {
        double[][] dArr2 = this.weightedJacobian;
        int length = dArr2.length;
        int length2 = dArr2[0].length;
        for (int i = 0; i < length2; i++) {
            int i2 = this.permutation[i];
            double d = 0.0d;
            for (int i3 = i; i3 < length; i3++) {
                d += this.weightedJacobian[i3][i2] * dArr[i3];
            }
            double d2 = d * this.beta[i2];
            for (int i4 = i; i4 < length; i4++) {
                dArr[i4] = dArr[i4] - (this.weightedJacobian[i4][i2] * d2);
            }
        }
    }

    private void checkParameters() {
        if (getLowerBound() != null || getUpperBound() != null) {
            throw new MathUnsupportedOperationException(LocalizedFormats.CONSTRAINT, new Object[0]);
        }
    }
}
