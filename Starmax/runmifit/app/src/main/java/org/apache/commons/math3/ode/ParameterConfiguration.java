package org.apache.commons.math3.ode;

import java.io.Serializable;

class ParameterConfiguration implements Serializable {
    private static final long serialVersionUID = 2247518849090889379L;

    /* renamed from: hP */
    private double f8067hP;
    private String parameterName;

    public ParameterConfiguration(String str, double d) {
        this.parameterName = str;
        this.f8067hP = d;
    }

    public String getParameterName() {
        return this.parameterName;
    }

    public double getHP() {
        return this.f8067hP;
    }

    public void setHP(double d) {
        this.f8067hP = d;
    }
}
