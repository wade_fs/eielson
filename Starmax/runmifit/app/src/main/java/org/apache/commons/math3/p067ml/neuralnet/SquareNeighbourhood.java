package org.apache.commons.math3.p067ml.neuralnet;

/* renamed from: org.apache.commons.math3.ml.neuralnet.SquareNeighbourhood */
public enum SquareNeighbourhood {
    VON_NEUMANN,
    MOORE
}
