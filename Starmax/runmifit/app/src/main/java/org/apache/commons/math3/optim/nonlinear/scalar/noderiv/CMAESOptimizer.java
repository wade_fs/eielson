package org.apache.commons.math3.optim.nonlinear.scalar.noderiv;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.optim.ConvergenceChecker;
import org.apache.commons.math3.optim.OptimizationData;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.MultivariateOptimizer;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;

public class CMAESOptimizer extends MultivariateOptimizer {

    /* renamed from: B */
    private RealMatrix f8103B;

    /* renamed from: BD */
    private RealMatrix f8104BD;

    /* renamed from: C */
    private RealMatrix f8105C;

    /* renamed from: D */
    private RealMatrix f8106D;

    /* renamed from: cc */
    private double f8107cc;
    private double ccov1;
    private double ccov1Sep;
    private double ccovmu;
    private double ccovmuSep;
    private final int checkFeasableCount;
    private double chiN;

    /* renamed from: cs */
    private double f8108cs;
    private double damps;
    private RealMatrix diagC;
    private RealMatrix diagD;
    private int diagonalOnly;
    private int dimension;
    private double[] fitnessHistory;
    private final boolean generateStatistics;
    private int historySize;
    private double[] inputSigma;
    private final boolean isActiveCMA;
    /* access modifiers changed from: private */
    public boolean isMinimize = true;
    private int iterations;
    private int lambda;
    private double logMu2;
    private final int maxIterations;

    /* renamed from: mu */
    private int f8109mu;
    private double mueff;
    private double normps;

    /* renamed from: pc */
    private RealMatrix f8110pc;

    /* renamed from: ps */
    private RealMatrix f8111ps;
    private final RandomGenerator random;
    private double sigma;
    private final List<RealMatrix> statisticsDHistory = new ArrayList();
    private final List<Double> statisticsFitnessHistory = new ArrayList();
    private final List<RealMatrix> statisticsMeanHistory = new ArrayList();
    private final List<Double> statisticsSigmaHistory = new ArrayList();
    private final double stopFitness;
    private double stopTolFun;
    private double stopTolHistFun;
    private double stopTolUpX;
    private double stopTolX;
    private RealMatrix weights;
    private RealMatrix xmean;

    public CMAESOptimizer(int i, double d, boolean z, int i2, int i3, RandomGenerator randomGenerator, boolean z2, ConvergenceChecker<PointValuePair> convergenceChecker) {
        super(convergenceChecker);
        this.maxIterations = i;
        this.stopFitness = d;
        this.isActiveCMA = z;
        this.diagonalOnly = i2;
        this.checkFeasableCount = i3;
        this.random = randomGenerator;
        this.generateStatistics = z2;
    }

    public List<Double> getStatisticsSigmaHistory() {
        return this.statisticsSigmaHistory;
    }

    public List<RealMatrix> getStatisticsMeanHistory() {
        return this.statisticsMeanHistory;
    }

    public List<Double> getStatisticsFitnessHistory() {
        return this.statisticsFitnessHistory;
    }

    public List<RealMatrix> getStatisticsDHistory() {
        return this.statisticsDHistory;
    }

    public static class Sigma implements OptimizationData {
        private final double[] sigma;

        public Sigma(double[] dArr) throws NotPositiveException {
            int i = 0;
            while (i < dArr.length) {
                if (dArr[i] >= 0.0d) {
                    i++;
                } else {
                    throw new NotPositiveException(Double.valueOf(dArr[i]));
                }
            }
            this.sigma = (double[]) dArr.clone();
        }

        public double[] getSigma() {
            return (double[]) this.sigma.clone();
        }
    }

    public static class PopulationSize implements OptimizationData {
        private final int lambda;

        public PopulationSize(int i) throws NotStrictlyPositiveException {
            if (i > 0) {
                this.lambda = i;
                return;
            }
            throw new NotStrictlyPositiveException(Integer.valueOf(i));
        }

        public int getPopulationSize() {
            return this.lambda;
        }
    }

    public PointValuePair optimize(OptimizationData... optimizationDataArr) throws TooManyEvaluationsException, DimensionMismatchException {
        return super.optimize(optimizationDataArr);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x02e9  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x031c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01f8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0204  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0226  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x023a  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x026b  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x028c  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x02ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.commons.math3.optim.PointValuePair doOptimize() {
        /*
            r25 = this;
            r6 = r25
            org.apache.commons.math3.optim.nonlinear.scalar.GoalType r0 = r25.getGoalType()
            org.apache.commons.math3.optim.nonlinear.scalar.GoalType r1 = org.apache.commons.math3.optim.nonlinear.scalar.GoalType.MINIMIZE
            boolean r0 = r0.equals(r1)
            r6.isMinimize = r0
            org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer$FitnessFunction r7 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer$FitnessFunction
            r7.<init>()
            double[] r0 = r25.getStartPoint()
            int r1 = r0.length
            r6.dimension = r1
            r6.initializeCMA(r0)
            r8 = 0
            r6.iterations = r8
            org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer$ValuePenaltyPair r0 = r7.value(r0)
            double r1 = r0.value
            double r3 = r0.penalty
            double r1 = r1 + r3
            double[] r0 = r6.fitnessHistory
            push(r0, r1)
            org.apache.commons.math3.optim.PointValuePair r0 = new org.apache.commons.math3.optim.PointValuePair
            double[] r3 = r25.getStartPoint()
            boolean r4 = r6.isMinimize
            if (r4 == 0) goto L_0x003e
            r4 = r1
            goto L_0x003f
        L_0x003e:
            double r4 = -r1
        L_0x003f:
            r0.<init>(r3, r4)
            r9 = 1
            r6.iterations = r9
            r13 = r0
            r11 = r1
            r14 = 0
        L_0x0048:
            int r0 = r6.iterations
            int r1 = r6.maxIterations
            if (r0 > r1) goto L_0x032d
            r25.incrementIterationCount()
            int r0 = r6.dimension
            int r1 = r6.lambda
            org.apache.commons.math3.linear.RealMatrix r3 = r6.randn1(r0, r1)
            int r0 = r6.dimension
            int r1 = r6.lambda
            org.apache.commons.math3.linear.RealMatrix r0 = zeros(r0, r1)
            int r1 = r6.lambda
            double[] r15 = new double[r1]
            org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer$ValuePenaltyPair[] r1 = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer.ValuePenaltyPair[r1]
            r2 = 0
        L_0x0068:
            int r4 = r6.lambda
            if (r2 >= r4) goto L_0x00e0
            r4 = 0
            r5 = 0
        L_0x006e:
            int r10 = r6.checkFeasableCount
            int r10 = r10 + r9
            if (r4 >= r10) goto L_0x00c9
            int r5 = r6.diagonalOnly
            if (r5 > 0) goto L_0x0091
            org.apache.commons.math3.linear.RealMatrix r5 = r6.xmean
            org.apache.commons.math3.linear.RealMatrix r10 = r6.f8104BD
            org.apache.commons.math3.linear.RealMatrix r9 = r3.getColumnMatrix(r2)
            org.apache.commons.math3.linear.RealMatrix r9 = r10.multiply(r9)
            r10 = r13
            r16 = r14
            double r13 = r6.sigma
            org.apache.commons.math3.linear.RealMatrix r9 = r9.scalarMultiply(r13)
            org.apache.commons.math3.linear.RealMatrix r5 = r5.add(r9)
            goto L_0x00aa
        L_0x0091:
            r10 = r13
            r16 = r14
            org.apache.commons.math3.linear.RealMatrix r5 = r6.xmean
            org.apache.commons.math3.linear.RealMatrix r9 = r6.diagD
            org.apache.commons.math3.linear.RealMatrix r13 = r3.getColumnMatrix(r2)
            org.apache.commons.math3.linear.RealMatrix r9 = times(r9, r13)
            double r13 = r6.sigma
            org.apache.commons.math3.linear.RealMatrix r9 = r9.scalarMultiply(r13)
            org.apache.commons.math3.linear.RealMatrix r5 = r5.add(r9)
        L_0x00aa:
            int r9 = r6.checkFeasableCount
            if (r4 >= r9) goto L_0x00cc
            double[] r9 = r5.getColumn(r8)
            boolean r9 = r7.isFeasible(r9)
            if (r9 == 0) goto L_0x00b9
            goto L_0x00cc
        L_0x00b9:
            int r9 = r6.dimension
            double[] r9 = r6.randn(r9)
            r3.setColumn(r2, r9)
            int r4 = r4 + 1
            r13 = r10
            r14 = r16
            r9 = 1
            goto L_0x006e
        L_0x00c9:
            r10 = r13
            r16 = r14
        L_0x00cc:
            copyColumn(r5, r8, r0, r2)
            double[] r4 = r0.getColumn(r2)     // Catch:{ TooManyEvaluationsException -> 0x032e }
            org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer$ValuePenaltyPair r4 = r7.value(r4)     // Catch:{ TooManyEvaluationsException -> 0x032e }
            r1[r2] = r4     // Catch:{ TooManyEvaluationsException -> 0x032e }
            int r2 = r2 + 1
            r13 = r10
            r14 = r16
            r9 = 1
            goto L_0x0068
        L_0x00e0:
            r10 = r13
            r16 = r14
            double r4 = r6.valueRange(r1)
            r2 = 0
        L_0x00e8:
            int r9 = r1.length
            if (r2 >= r9) goto L_0x0100
            r9 = r1[r2]
            double r13 = r9.value
            r9 = r1[r2]
            double r17 = r9.penalty
            double r17 = r17 * r4
            double r13 = r13 + r17
            r15[r2] = r13
            int r2 = r2 + 1
            goto L_0x00e8
        L_0x0100:
            int[] r9 = r6.sortedIndices(r15)
            org.apache.commons.math3.linear.RealMatrix r5 = r6.xmean
            int r1 = r6.f8109mu
            int[] r1 = org.apache.commons.math3.util.MathArrays.copyOf(r9, r1)
            org.apache.commons.math3.linear.RealMatrix r13 = selectColumns(r0, r1)
            org.apache.commons.math3.linear.RealMatrix r0 = r6.weights
            org.apache.commons.math3.linear.RealMatrix r0 = r13.multiply(r0)
            r6.xmean = r0
            int r0 = r6.f8109mu
            int[] r0 = org.apache.commons.math3.util.MathArrays.copyOf(r9, r0)
            org.apache.commons.math3.linear.RealMatrix r0 = selectColumns(r3, r0)
            org.apache.commons.math3.linear.RealMatrix r1 = r6.weights
            org.apache.commons.math3.linear.RealMatrix r1 = r0.multiply(r1)
            boolean r1 = r6.updateEvolutionPaths(r1, r5)
            int r2 = r6.diagonalOnly
            if (r2 > 0) goto L_0x0138
            r0 = r25
            r2 = r13
            r4 = r9
            r0.updateCovariance(r1, r2, r3, r4, r5)
            goto L_0x013b
        L_0x0138:
            r6.updateCovarianceDiagonalOnly(r1, r0)
        L_0x013b:
            double r0 = r6.sigma
            double r2 = r6.normps
            double r4 = r6.chiN
            double r2 = r2 / r4
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r2 = r2 - r4
            r17 = r9
            double r8 = r6.f8108cs
            double r2 = r2 * r8
            double r8 = r6.damps
            double r2 = r2 / r8
            double r2 = org.apache.commons.math3.util.FastMath.min(r4, r2)
            double r2 = org.apache.commons.math3.util.FastMath.exp(r2)
            double r0 = r0 * r2
            r6.sigma = r0
            r0 = 0
            r1 = r17[r0]
            r1 = r15[r1]
            r3 = r17
            int r4 = r3.length
            r5 = 1
            int r4 = r4 - r5
            r4 = r3[r4]
            r4 = r15[r4]
            int r8 = (r11 > r1 ? 1 : (r11 == r1 ? 0 : -1))
            if (r8 <= 0) goto L_0x0199
            org.apache.commons.math3.optim.PointValuePair r8 = new org.apache.commons.math3.optim.PointValuePair
            double[] r9 = r13.getColumn(r0)
            double[] r0 = r7.repair(r9)
            boolean r9 = r6.isMinimize
            if (r9 == 0) goto L_0x017c
            r11 = r1
            goto L_0x017d
        L_0x017c:
            double r11 = -r1
        L_0x017d:
            r8.<init>(r0, r11)
            org.apache.commons.math3.optim.ConvergenceChecker r0 = r25.getConvergenceChecker()
            if (r0 == 0) goto L_0x0196
            org.apache.commons.math3.optim.ConvergenceChecker r0 = r25.getConvergenceChecker()
            int r9 = r6.iterations
            boolean r0 = r0.converged(r9, r8, r10)
            if (r0 == 0) goto L_0x0196
        L_0x0192:
            r20 = r8
            goto L_0x0330
        L_0x0196:
            r11 = r1
            r0 = r15
            goto L_0x019d
        L_0x0199:
            r8 = r10
            r0 = r15
            r10 = r16
        L_0x019d:
            double r14 = r6.stopFitness
            r16 = 0
            int r18 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r18 == 0) goto L_0x01b0
            boolean r9 = r6.isMinimize
            if (r9 == 0) goto L_0x01aa
            goto L_0x01ab
        L_0x01aa:
            double r14 = -r14
        L_0x01ab:
            int r9 = (r1 > r14 ? 1 : (r1 == r14 ? 0 : -1))
            if (r9 >= 0) goto L_0x01b0
            goto L_0x0192
        L_0x01b0:
            org.apache.commons.math3.linear.RealMatrix r9 = r6.diagC
            org.apache.commons.math3.linear.RealMatrix r9 = sqrt(r9)
            r14 = 0
            double[] r9 = r9.getColumn(r14)
            org.apache.commons.math3.linear.RealMatrix r15 = r6.f8110pc
            double[] r15 = r15.getColumn(r14)
            r19 = r7
            r14 = 0
        L_0x01c4:
            int r7 = r6.dimension
            if (r14 >= r7) goto L_0x01f8
            r20 = r8
            double r7 = r6.sigma
            r21 = r15[r14]
            r23 = r11
            double r11 = org.apache.commons.math3.util.FastMath.abs(r21)
            r21 = r1
            r2 = r0
            r0 = r9[r14]
            double r0 = org.apache.commons.math3.util.FastMath.max(r11, r0)
            double r7 = r7 * r0
            double r0 = r6.stopTolX
            int r11 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            if (r11 <= 0) goto L_0x01e6
            goto L_0x01ff
        L_0x01e6:
            int r0 = r6.dimension
            r1 = 1
            int r0 = r0 - r1
            if (r14 < r0) goto L_0x01ee
            goto L_0x0330
        L_0x01ee:
            int r14 = r14 + 1
            r0 = r2
            r8 = r20
            r1 = r21
            r11 = r23
            goto L_0x01c4
        L_0x01f8:
            r21 = r1
            r20 = r8
            r23 = r11
            r2 = r0
        L_0x01ff:
            r0 = 0
        L_0x0200:
            int r1 = r6.dimension
            if (r0 >= r1) goto L_0x0215
            double r7 = r6.sigma
            r11 = r9[r0]
            double r7 = r7 * r11
            double r11 = r6.stopTolUpX
            int r1 = (r7 > r11 ? 1 : (r7 == r11 ? 0 : -1))
            if (r1 <= 0) goto L_0x0212
            goto L_0x0330
        L_0x0212:
            int r0 = r0 + 1
            goto L_0x0200
        L_0x0215:
            double[] r0 = r6.fitnessHistory
            double r0 = min(r0)
            double[] r7 = r6.fitnessHistory
            double r7 = max(r7)
            int r9 = r6.iterations
            r11 = 2
            if (r9 <= r11) goto L_0x023a
            double r4 = org.apache.commons.math3.util.FastMath.max(r7, r4)
            r14 = r21
            double r21 = org.apache.commons.math3.util.FastMath.min(r0, r14)
            double r4 = r4 - r21
            double r11 = r6.stopTolFun
            int r21 = (r4 > r11 ? 1 : (r4 == r11 ? 0 : -1))
            if (r21 >= 0) goto L_0x023c
            goto L_0x0330
        L_0x023a:
            r14 = r21
        L_0x023c:
            int r4 = r6.iterations
            double[] r5 = r6.fitnessHistory
            int r5 = r5.length
            if (r4 <= r5) goto L_0x024d
            double r4 = r7 - r0
            double r11 = r6.stopTolHistFun
            int r21 = (r4 > r11 ? 1 : (r4 == r11 ? 0 : -1))
            if (r21 >= 0) goto L_0x024d
            goto L_0x0330
        L_0x024d:
            org.apache.commons.math3.linear.RealMatrix r4 = r6.diagD
            double r4 = max(r4)
            org.apache.commons.math3.linear.RealMatrix r11 = r6.diagD
            double r11 = min(r11)
            double r4 = r4 / r11
            r11 = 4711630319722168320(0x416312d000000000, double:1.0E7)
            int r21 = (r4 > r11 ? 1 : (r4 == r11 ? 0 : -1))
            if (r21 <= 0) goto L_0x0265
            goto L_0x0330
        L_0x0265:
            org.apache.commons.math3.optim.ConvergenceChecker r4 = r25.getConvergenceChecker()
            if (r4 == 0) goto L_0x028c
            org.apache.commons.math3.optim.PointValuePair r4 = new org.apache.commons.math3.optim.PointValuePair
            r5 = 0
            double[] r11 = r13.getColumn(r5)
            boolean r12 = r6.isMinimize
            if (r12 == 0) goto L_0x0278
            r12 = r14
            goto L_0x0279
        L_0x0278:
            double r12 = -r14
        L_0x0279:
            r4.<init>(r11, r12)
            if (r10 == 0) goto L_0x028e
            org.apache.commons.math3.optim.ConvergenceChecker r11 = r25.getConvergenceChecker()
            int r12 = r6.iterations
            boolean r10 = r11.converged(r12, r4, r10)
            if (r10 == 0) goto L_0x028e
            goto L_0x0330
        L_0x028c:
            r5 = 0
            r4 = r10
        L_0x028e:
            r10 = 4591870180066957722(0x3fb999999999999a, double:0.1)
            int r12 = r6.lambda
            double r12 = (double) r12
            r21 = 4616189618054758400(0x4010000000000000, double:4.0)
            java.lang.Double.isNaN(r12)
            double r12 = r12 / r21
            double r12 = r12 + r10
            int r10 = (int) r12
            r3 = r3[r10]
            r10 = r2[r3]
            r2 = 4596373779694328218(0x3fc999999999999a, double:0.2)
            int r12 = (r23 > r10 ? 1 : (r23 == r10 ? 0 : -1))
            if (r12 != 0) goto L_0x02be
            double r10 = r6.sigma
            double r12 = r6.f8108cs
            r21 = r10
            double r9 = r6.damps
            double r12 = r12 / r9
            double r12 = r12 + r2
            double r9 = org.apache.commons.math3.util.FastMath.exp(r12)
            double r10 = r21 * r9
            r6.sigma = r10
        L_0x02be:
            int r9 = r6.iterations
            r10 = 2
            if (r9 <= r10) goto L_0x02e0
            double r7 = org.apache.commons.math3.util.FastMath.max(r7, r14)
            double r0 = org.apache.commons.math3.util.FastMath.min(r0, r14)
            double r7 = r7 - r0
            int r0 = (r7 > r16 ? 1 : (r7 == r16 ? 0 : -1))
            if (r0 != 0) goto L_0x02e0
            double r0 = r6.sigma
            double r7 = r6.f8108cs
            double r9 = r6.damps
            double r7 = r7 / r9
            double r7 = r7 + r2
            double r2 = org.apache.commons.math3.util.FastMath.exp(r7)
            double r0 = r0 * r2
            r6.sigma = r0
        L_0x02e0:
            double[] r0 = r6.fitnessHistory
            push(r0, r14)
            boolean r0 = r6.generateStatistics
            if (r0 == 0) goto L_0x031c
            java.util.List<java.lang.Double> r0 = r6.statisticsSigmaHistory
            double r1 = r6.sigma
            java.lang.Double r1 = java.lang.Double.valueOf(r1)
            r0.add(r1)
            java.util.List<java.lang.Double> r0 = r6.statisticsFitnessHistory
            java.lang.Double r1 = java.lang.Double.valueOf(r14)
            r0.add(r1)
            java.util.List<org.apache.commons.math3.linear.RealMatrix> r0 = r6.statisticsMeanHistory
            org.apache.commons.math3.linear.RealMatrix r1 = r6.xmean
            org.apache.commons.math3.linear.RealMatrix r1 = r1.transpose()
            r0.add(r1)
            java.util.List<org.apache.commons.math3.linear.RealMatrix> r0 = r6.statisticsDHistory
            org.apache.commons.math3.linear.RealMatrix r1 = r6.diagD
            org.apache.commons.math3.linear.RealMatrix r1 = r1.transpose()
            r2 = 4681608360884174848(0x40f86a0000000000, double:100000.0)
            org.apache.commons.math3.linear.RealMatrix r1 = r1.scalarMultiply(r2)
            r0.add(r1)
        L_0x031c:
            int r0 = r6.iterations
            r1 = 1
            int r0 = r0 + r1
            r6.iterations = r0
            r14 = r4
            r7 = r19
            r13 = r20
            r11 = r23
            r8 = 0
            r9 = 1
            goto L_0x0048
        L_0x032d:
            r10 = r13
        L_0x032e:
            r20 = r10
        L_0x0330:
            return r20
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer.doOptimize():org.apache.commons.math3.optim.PointValuePair");
    }

    /* access modifiers changed from: protected */
    public void parseOptimizationData(OptimizationData... optimizationDataArr) {
        super.parseOptimizationData(optimizationDataArr);
        for (OptimizationData optimizationData : optimizationDataArr) {
            if (optimizationData instanceof Sigma) {
                this.inputSigma = ((Sigma) optimizationData).getSigma();
            } else if (optimizationData instanceof PopulationSize) {
                this.lambda = ((PopulationSize) optimizationData).getPopulationSize();
            }
        }
        checkParameters();
    }

    private void checkParameters() {
        double[] startPoint = getStartPoint();
        double[] lowerBound = getLowerBound();
        double[] upperBound = getUpperBound();
        double[] dArr = this.inputSigma;
        if (dArr == null) {
            return;
        }
        if (dArr.length == startPoint.length) {
            int i = 0;
            while (i < startPoint.length) {
                double[] dArr2 = this.inputSigma;
                if (dArr2[i] <= upperBound[i] - lowerBound[i]) {
                    i++;
                } else {
                    throw new OutOfRangeException(Double.valueOf(dArr2[i]), 0, Double.valueOf(upperBound[i] - lowerBound[i]));
                }
            }
            return;
        }
        throw new DimensionMismatchException(dArr.length, startPoint.length);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private void initializeCMA(double[] dArr) {
        double[] dArr2 = dArr;
        int i = this.lambda;
        if (i > 0) {
            double[][] dArr3 = (double[][]) Array.newInstance(double.class, dArr2.length, 1);
            for (int i2 = 0; i2 < dArr2.length; i2++) {
                dArr3[i2][0] = this.inputSigma[i2];
            }
            Array2DRowRealMatrix array2DRowRealMatrix = new Array2DRowRealMatrix(dArr3, false);
            this.sigma = max(array2DRowRealMatrix);
            this.stopTolUpX = max(array2DRowRealMatrix) * 1000.0d;
            this.stopTolX = max(array2DRowRealMatrix) * 1.0E-11d;
            this.stopTolFun = 1.0E-12d;
            this.stopTolHistFun = 1.0E-13d;
            this.f8109mu = this.lambda / 2;
            double d = (double) this.f8109mu;
            Double.isNaN(d);
            this.logMu2 = FastMath.log(d + 0.5d);
            this.weights = log(sequence(1.0d, (double) this.f8109mu, 1.0d)).scalarMultiply(-1.0d).scalarAdd(this.logMu2);
            double d2 = 0.0d;
            double d3 = 0.0d;
            for (int i3 = 0; i3 < this.f8109mu; i3++) {
                double entry = this.weights.getEntry(i3, 0);
                d2 += entry;
                d3 += entry * entry;
            }
            this.weights = this.weights.scalarMultiply(1.0d / d2);
            this.mueff = (d2 * d2) / d3;
            double d4 = this.mueff;
            int i4 = this.dimension;
            double d5 = (double) i4;
            Double.isNaN(d5);
            double d6 = (double) (i4 + 4);
            double d7 = (double) i4;
            Double.isNaN(d7);
            Double.isNaN(d6);
            this.f8107cc = ((d4 / d5) + 4.0d) / (d6 + ((d4 * 2.0d) / d7));
            double d8 = (double) i4;
            Double.isNaN(d8);
            this.f8108cs = (d4 + 2.0d) / ((d8 + d4) + 3.0d);
            double d9 = (double) (i4 + 1);
            Double.isNaN(d9);
            double d10 = (double) this.dimension;
            double d11 = (double) this.maxIterations;
            Double.isNaN(d11);
            Double.isNaN(d10);
            this.damps = (((FastMath.max(0.0d, FastMath.sqrt((d4 - 1.0d) / d9) - 1.0d) * 2.0d) + 1.0d) * FastMath.max(0.3d, 1.0d - (d10 / (d11 + 1.0E-6d)))) + this.f8108cs;
            int i5 = this.dimension;
            double d12 = (double) i5;
            Double.isNaN(d12);
            double d13 = (double) i5;
            Double.isNaN(d13);
            double d14 = this.mueff;
            this.ccov1 = 2.0d / (((d12 + 1.3d) * (d13 + 1.3d)) + d14);
            double d15 = (double) ((i5 + 2) * (i5 + 2));
            Double.isNaN(d15);
            this.ccovmu = FastMath.min(1.0d - this.ccov1, (((d14 - 2.0d) + (1.0d / d14)) * 2.0d) / (d15 + d14));
            double d16 = this.ccov1;
            double d17 = (double) this.dimension;
            Double.isNaN(d17);
            this.ccov1Sep = FastMath.min(1.0d, (d16 * (d17 + 1.5d)) / 3.0d);
            double d18 = this.ccovmu;
            double d19 = (double) this.dimension;
            Double.isNaN(d19);
            this.ccovmuSep = FastMath.min(1.0d - this.ccov1, (d18 * (d19 + 1.5d)) / 3.0d);
            double sqrt = FastMath.sqrt((double) this.dimension);
            int i6 = this.dimension;
            double d20 = (double) i6;
            Double.isNaN(d20);
            double d21 = (double) i6;
            Double.isNaN(d21);
            double d22 = (double) i6;
            Double.isNaN(d22);
            this.chiN = sqrt * ((1.0d - (1.0d / (d20 * 4.0d))) + (1.0d / ((d21 * 21.0d) * d22)));
            this.xmean = MatrixUtils.createColumnRealMatrix(dArr);
            this.diagD = array2DRowRealMatrix.scalarMultiply(1.0d / this.sigma);
            this.diagC = square(this.diagD);
            this.f8110pc = zeros(this.dimension, 1);
            this.f8111ps = zeros(this.dimension, 1);
            this.normps = this.f8111ps.getFrobeniusNorm();
            int i7 = this.dimension;
            this.f8103B = eye(i7, i7);
            this.f8106D = ones(this.dimension, 1);
            this.f8104BD = times(this.f8103B, repmat(this.diagD.transpose(), this.dimension, 1));
            this.f8105C = this.f8103B.multiply(diag(square(this.f8106D)).multiply(this.f8103B.transpose()));
            double d23 = (double) (this.dimension * 30);
            double d24 = (double) this.lambda;
            Double.isNaN(d23);
            Double.isNaN(d24);
            this.historySize = ((int) (d23 / d24)) + 10;
            this.fitnessHistory = new double[this.historySize];
            for (int i8 = 0; i8 < this.historySize; i8++) {
                this.fitnessHistory[i8] = Double.MAX_VALUE;
            }
            return;
        }
        throw new NotStrictlyPositiveException(Integer.valueOf(i));
    }

    private boolean updateEvolutionPaths(RealMatrix realMatrix, RealMatrix realMatrix2) {
        RealMatrix scalarMultiply = this.f8111ps.scalarMultiply(1.0d - this.f8108cs);
        RealMatrix multiply = this.f8103B.multiply(realMatrix);
        double d = this.f8108cs;
        this.f8111ps = scalarMultiply.add(multiply.scalarMultiply(FastMath.sqrt(d * (2.0d - d) * this.mueff)));
        this.normps = this.f8111ps.getFrobeniusNorm();
        double sqrt = (this.normps / FastMath.sqrt(1.0d - FastMath.pow(1.0d - this.f8108cs, this.iterations * 2))) / this.chiN;
        double d2 = (double) this.dimension;
        Double.isNaN(d2);
        boolean z = sqrt < (2.0d / (d2 + 1.0d)) + 1.4d;
        this.f8110pc = this.f8110pc.scalarMultiply(1.0d - this.f8107cc);
        if (z) {
            RealMatrix realMatrix3 = this.f8110pc;
            RealMatrix subtract = this.xmean.subtract(realMatrix2);
            double d3 = this.f8107cc;
            this.f8110pc = realMatrix3.add(subtract.scalarMultiply(FastMath.sqrt((d3 * (2.0d - d3)) * this.mueff) / this.sigma));
        }
        return z;
    }

    private void updateCovarianceDiagonalOnly(boolean z, RealMatrix realMatrix) {
        double d;
        if (z) {
            d = 0.0d;
        } else {
            double d2 = this.ccov1Sep;
            double d3 = this.f8107cc;
            d = d2 * d3 * (2.0d - d3);
        }
        this.diagC = this.diagC.scalarMultiply(d + ((1.0d - this.ccov1Sep) - this.ccovmuSep)).add(square(this.f8110pc).scalarMultiply(this.ccov1Sep)).add(times(this.diagC, square(realMatrix).multiply(this.weights)).scalarMultiply(this.ccovmuSep));
        this.diagD = sqrt(this.diagC);
        int i = this.diagonalOnly;
        if (i > 1 && this.iterations > i) {
            this.diagonalOnly = 0;
            int i2 = this.dimension;
            this.f8103B = eye(i2, i2);
            this.f8104BD = diag(this.diagD);
            this.f8105C = diag(this.diagC);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.FastMath.pow(double, double):double
     arg types: [double, int]
     candidates:
      org.apache.commons.math3.util.FastMath.pow(double, int):double
      org.apache.commons.math3.util.FastMath.pow(double, double):double */
    private void updateCovariance(boolean z, RealMatrix realMatrix, RealMatrix realMatrix2, int[] iArr, RealMatrix realMatrix3) {
        double d;
        double d2;
        if (this.ccov1 + this.ccovmu > 0.0d) {
            RealMatrix scalarMultiply = realMatrix.subtract(repmat(realMatrix3, 1, this.f8109mu)).scalarMultiply(1.0d / this.sigma);
            RealMatrix realMatrix4 = this.f8110pc;
            RealMatrix scalarMultiply2 = realMatrix4.multiply(realMatrix4.transpose()).scalarMultiply(this.ccov1);
            if (z) {
                d2 = 0.0d;
            } else {
                double d3 = this.ccov1;
                double d4 = this.f8107cc;
                d2 = d3 * d4 * (2.0d - d4);
            }
            double d5 = this.ccovmu;
            double d6 = d2 + ((1.0d - this.ccov1) - d5);
            if (this.isActiveCMA) {
                d = (((1.0d - d5) * 0.25d) * this.mueff) / (FastMath.pow((double) (this.dimension + 2), 1.5d) + (this.mueff * 2.0d));
                RealMatrix selectColumns = selectColumns(realMatrix2, MathArrays.copyOf(reverse(iArr), this.f8109mu));
                RealMatrix sqrt = sqrt(sumRows(square(selectColumns)));
                int[] sortedIndices = sortedIndices(sqrt.getRow(0));
                RealMatrix selectColumns2 = selectColumns(divide(selectColumns(sqrt, reverse(sortedIndices)), selectColumns(sqrt, sortedIndices)), inverse(sortedIndices));
                double entry = 0.33999999999999997d / square(selectColumns2).multiply(this.weights).getEntry(0, 0);
                if (d > entry) {
                    d = entry;
                }
                RealMatrix multiply = this.f8104BD.multiply(times(selectColumns, repmat(selectColumns2, this.dimension, 1)));
                double d7 = 0.5d * d;
                this.f8105C = this.f8105C.scalarMultiply(d6 + d7).add(scalarMultiply2).add(scalarMultiply.scalarMultiply(this.ccovmu + d7).multiply(times(repmat(this.weights, 1, this.dimension), scalarMultiply.transpose()))).subtract(multiply.multiply(diag(this.weights)).multiply(multiply.transpose()).scalarMultiply(d));
                updateBD(d);
            }
            this.f8105C = this.f8105C.scalarMultiply(d6).add(scalarMultiply2).add(scalarMultiply.scalarMultiply(this.ccovmu).multiply(times(repmat(this.weights, 1, this.dimension), scalarMultiply.transpose())));
        }
        d = 0.0d;
        updateBD(d);
    }

    private void updateBD(double d) {
        double d2 = this.ccov1;
        double d3 = this.ccovmu;
        if (d2 + d3 + d > 0.0d) {
            double d4 = (double) this.iterations;
            Double.isNaN(d4);
            double d5 = (double) this.dimension;
            Double.isNaN(d5);
            if ((((d4 % 1.0d) / ((d2 + d3) + d)) / d5) / 10.0d < 1.0d) {
                this.f8105C = triu(this.f8105C, 0).add(triu(this.f8105C, 1).transpose());
                EigenDecomposition eigenDecomposition = new EigenDecomposition(this.f8105C);
                this.f8103B = eigenDecomposition.getV();
                this.f8106D = eigenDecomposition.getD();
                this.diagD = diag(this.f8106D);
                if (min(this.diagD) <= 0.0d) {
                    for (int i = 0; i < this.dimension; i++) {
                        if (this.diagD.getEntry(i, 0) < 0.0d) {
                            this.diagD.setEntry(i, 0, 0.0d);
                        }
                    }
                    double max = max(this.diagD) / 1.0E14d;
                    RealMatrix realMatrix = this.f8105C;
                    int i2 = this.dimension;
                    this.f8105C = realMatrix.add(eye(i2, i2).scalarMultiply(max));
                    this.diagD = this.diagD.add(ones(this.dimension, 1).scalarMultiply(max));
                }
                if (max(this.diagD) > min(this.diagD) * 1.0E14d) {
                    double max2 = (max(this.diagD) / 1.0E14d) - min(this.diagD);
                    RealMatrix realMatrix2 = this.f8105C;
                    int i3 = this.dimension;
                    this.f8105C = realMatrix2.add(eye(i3, i3).scalarMultiply(max2));
                    this.diagD = this.diagD.add(ones(this.dimension, 1).scalarMultiply(max2));
                }
                this.diagC = diag(this.f8105C);
                this.diagD = sqrt(this.diagD);
                this.f8104BD = times(this.f8103B, repmat(this.diagD.transpose(), this.dimension, 1));
            }
        }
    }

    private static void push(double[] dArr, double d) {
        for (int length = dArr.length - 1; length > 0; length--) {
            dArr[length] = dArr[length - 1];
        }
        dArr[0] = d;
    }

    private int[] sortedIndices(double[] dArr) {
        DoubleIndex[] doubleIndexArr = new DoubleIndex[dArr.length];
        for (int i = 0; i < dArr.length; i++) {
            doubleIndexArr[i] = new DoubleIndex(dArr[i], i);
        }
        Arrays.sort(doubleIndexArr);
        int[] iArr = new int[dArr.length];
        for (int i2 = 0; i2 < dArr.length; i2++) {
            iArr[i2] = doubleIndexArr[i2].index;
        }
        return iArr;
    }

    private double valueRange(ValuePenaltyPair[] valuePenaltyPairArr) {
        double d = Double.NEGATIVE_INFINITY;
        double d2 = Double.MAX_VALUE;
        for (ValuePenaltyPair valuePenaltyPair : valuePenaltyPairArr) {
            if (valuePenaltyPair.value > d) {
                d = valuePenaltyPair.value;
            }
            if (valuePenaltyPair.value < d2) {
                d2 = valuePenaltyPair.value;
            }
        }
        return d - d2;
    }

    private static class DoubleIndex implements Comparable<DoubleIndex> {
        /* access modifiers changed from: private */
        public final int index;
        private final double value;

        DoubleIndex(double d, int i) {
            this.value = d;
            this.index = i;
        }

        public int compareTo(DoubleIndex doubleIndex) {
            return Double.compare(this.value, doubleIndex.value);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DoubleIndex) || Double.compare(this.value, ((DoubleIndex) obj).value) != 0) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            long doubleToLongBits = Double.doubleToLongBits(this.value);
            return (int) ((doubleToLongBits ^ ((doubleToLongBits >>> 32) ^ 1438542)) & -1);
        }
    }

    private static class ValuePenaltyPair {
        /* access modifiers changed from: private */
        public double penalty;
        /* access modifiers changed from: private */
        public double value;

        public ValuePenaltyPair(double d, double d2) {
            this.value = d;
            this.penalty = d2;
        }
    }

    private class FitnessFunction {
        private final boolean isRepairMode = true;

        public FitnessFunction() {
        }

        public ValuePenaltyPair value(double[] dArr) {
            double d;
            double d2;
            if (this.isRepairMode) {
                double[] repair = repair(dArr);
                d2 = CMAESOptimizer.this.computeObjectiveValue(repair);
                d = penalty(dArr, repair);
            } else {
                d2 = CMAESOptimizer.this.computeObjectiveValue(dArr);
                d = 0.0d;
            }
            if (!CMAESOptimizer.this.isMinimize) {
                d2 = -d2;
            }
            if (!CMAESOptimizer.this.isMinimize) {
                d = -d;
            }
            return new ValuePenaltyPair(d2, d);
        }

        public boolean isFeasible(double[] dArr) {
            double[] lowerBound = CMAESOptimizer.this.getLowerBound();
            double[] upperBound = CMAESOptimizer.this.getUpperBound();
            for (int i = 0; i < dArr.length; i++) {
                if (dArr[i] < lowerBound[i] || dArr[i] > upperBound[i]) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: private */
        public double[] repair(double[] dArr) {
            double[] lowerBound = CMAESOptimizer.this.getLowerBound();
            double[] upperBound = CMAESOptimizer.this.getUpperBound();
            double[] dArr2 = new double[dArr.length];
            for (int i = 0; i < dArr.length; i++) {
                if (dArr[i] < lowerBound[i]) {
                    dArr2[i] = lowerBound[i];
                } else if (dArr[i] > upperBound[i]) {
                    dArr2[i] = upperBound[i];
                } else {
                    dArr2[i] = dArr[i];
                }
            }
            return dArr2;
        }

        private double penalty(double[] dArr, double[] dArr2) {
            double d = 0.0d;
            for (int i = 0; i < dArr.length; i++) {
                d += FastMath.abs(dArr[i] - dArr2[i]);
            }
            return CMAESOptimizer.this.isMinimize ? d : -d;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix log(RealMatrix realMatrix) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                dArr[i][i2] = FastMath.log(realMatrix.getEntry(i, i2));
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix sqrt(RealMatrix realMatrix) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                dArr[i][i2] = FastMath.sqrt(realMatrix.getEntry(i, i2));
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix square(RealMatrix realMatrix) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                double entry = realMatrix.getEntry(i, i2);
                dArr[i][i2] = entry * entry;
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix times(RealMatrix realMatrix, RealMatrix realMatrix2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                dArr[i][i2] = realMatrix.getEntry(i, i2) * realMatrix2.getEntry(i, i2);
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix divide(RealMatrix realMatrix, RealMatrix realMatrix2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                dArr[i][i2] = realMatrix.getEntry(i, i2) / realMatrix2.getEntry(i, i2);
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix selectColumns(RealMatrix realMatrix, int[] iArr) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), iArr.length);
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < iArr.length; i2++) {
                dArr[i][i2] = realMatrix.getEntry(i, iArr[i2]);
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix triu(RealMatrix realMatrix, int i) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        int i2 = 0;
        while (i2 < realMatrix.getRowDimension()) {
            for (int i3 = 0; i3 < realMatrix.getColumnDimension(); i3++) {
                dArr[i2][i3] = i2 <= i3 - i ? realMatrix.getEntry(i2, i3) : 0.0d;
            }
            i2++;
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix sumRows(RealMatrix realMatrix) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, 1, realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getColumnDimension(); i++) {
            double d = 0.0d;
            for (int i2 = 0; i2 < realMatrix.getRowDimension(); i2++) {
                d += realMatrix.getEntry(i2, i);
            }
            dArr[0][i] = d;
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix diag(RealMatrix realMatrix) {
        Class<double> cls = double.class;
        if (realMatrix.getColumnDimension() == 1) {
            double[][] dArr = (double[][]) Array.newInstance((Class<?>) cls, realMatrix.getRowDimension(), realMatrix.getRowDimension());
            for (int i = 0; i < realMatrix.getRowDimension(); i++) {
                dArr[i][i] = realMatrix.getEntry(i, 0);
            }
            return new Array2DRowRealMatrix(dArr, false);
        }
        double[][] dArr2 = (double[][]) Array.newInstance((Class<?>) cls, realMatrix.getRowDimension(), 1);
        for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
            dArr2[i2][0] = realMatrix.getEntry(i2, i2);
        }
        return new Array2DRowRealMatrix(dArr2, false);
    }

    private static void copyColumn(RealMatrix realMatrix, int i, RealMatrix realMatrix2, int i2) {
        for (int i3 = 0; i3 < realMatrix.getRowDimension(); i3++) {
            realMatrix2.setEntry(i3, i2, realMatrix.getEntry(i3, i));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix ones(int i, int i2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, i2);
        for (int i3 = 0; i3 < i; i3++) {
            Arrays.fill(dArr[i3], 1.0d);
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix eye(int i, int i2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, i2);
        for (int i3 = 0; i3 < i; i3++) {
            if (i3 < i2) {
                dArr[i3][i3] = 1.0d;
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    private static RealMatrix zeros(int i, int i2) {
        return new Array2DRowRealMatrix(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix repmat(RealMatrix realMatrix, int i, int i2) {
        int rowDimension = realMatrix.getRowDimension();
        int columnDimension = realMatrix.getColumnDimension();
        int i3 = i * rowDimension;
        int i4 = i2 * columnDimension;
        double[][] dArr = (double[][]) Array.newInstance(double.class, i3, i4);
        for (int i5 = 0; i5 < i3; i5++) {
            for (int i6 = 0; i6 < i4; i6++) {
                dArr[i5][i6] = realMatrix.getEntry(i5 % rowDimension, i6 % columnDimension);
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix sequence(double d, double d2, double d3) {
        int i = (int) (((d2 - d) / d3) + 1.0d);
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, 1);
        double d4 = d;
        for (int i2 = 0; i2 < i; i2++) {
            dArr[i2][0] = d4;
            d4 += d3;
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    private static double max(RealMatrix realMatrix) {
        double d = -1.7976931348623157E308d;
        int i = 0;
        while (i < realMatrix.getRowDimension()) {
            double d2 = d;
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                double entry = realMatrix.getEntry(i, i2);
                if (d2 < entry) {
                    d2 = entry;
                }
            }
            i++;
            d = d2;
        }
        return d;
    }

    private static double min(RealMatrix realMatrix) {
        double d = Double.MAX_VALUE;
        int i = 0;
        while (i < realMatrix.getRowDimension()) {
            double d2 = d;
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                double entry = realMatrix.getEntry(i, i2);
                if (d2 > entry) {
                    d2 = entry;
                }
            }
            i++;
            d = d2;
        }
        return d;
    }

    private static double max(double[] dArr) {
        double d = -1.7976931348623157E308d;
        for (int i = 0; i < dArr.length; i++) {
            if (d < dArr[i]) {
                d = dArr[i];
            }
        }
        return d;
    }

    private static double min(double[] dArr) {
        double d = Double.MAX_VALUE;
        for (int i = 0; i < dArr.length; i++) {
            if (d > dArr[i]) {
                d = dArr[i];
            }
        }
        return d;
    }

    private static int[] inverse(int[] iArr) {
        int[] iArr2 = new int[iArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr2[iArr[i]] = i;
        }
        return iArr2;
    }

    private static int[] reverse(int[] iArr) {
        int[] iArr2 = new int[iArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr2[i] = iArr[(iArr.length - i) - 1];
        }
        return iArr2;
    }

    private double[] randn(int i) {
        double[] dArr = new double[i];
        for (int i2 = 0; i2 < i; i2++) {
            dArr[i2] = this.random.nextGaussian();
        }
        return dArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private RealMatrix randn1(int i, int i2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, i2);
        for (int i3 = 0; i3 < i; i3++) {
            for (int i4 = 0; i4 < i2; i4++) {
                dArr[i3][i4] = this.random.nextGaussian();
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }
}
