package org.apache.commons.math3.ode.events;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.solvers.AllowedSolution;
import org.apache.commons.math3.analysis.solvers.BracketedUnivariateSolver;
import org.apache.commons.math3.analysis.solvers.PegasusSolver;
import org.apache.commons.math3.analysis.solvers.UnivariateSolver;
import org.apache.commons.math3.analysis.solvers.UnivariateSolverUtils;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.ode.EquationsMapper;
import org.apache.commons.math3.ode.ExpandableStatefulODE;
import org.apache.commons.math3.ode.events.EventHandler;
import org.apache.commons.math3.ode.sampling.StepInterpolator;
import org.apache.commons.math3.util.FastMath;

public class EventState {
    private final double convergence;
    private ExpandableStatefulODE expandable = null;
    private boolean forward;

    /* renamed from: g0 */
    private double f8068g0 = Double.NaN;
    private boolean g0Positive = true;
    /* access modifiers changed from: private */
    public final EventHandler handler;
    private boolean increasing = true;
    private final double maxCheckInterval;
    private final int maxIterationCount;
    private EventHandler.Action nextAction = EventHandler.Action.CONTINUE;
    private boolean pendingEvent = false;
    private double pendingEventTime = Double.NaN;
    private double previousEventTime = Double.NaN;
    private final UnivariateSolver solver;

    /* renamed from: t0 */
    private double f8069t0 = Double.NaN;

    public EventState(EventHandler eventHandler, double d, double d2, int i, UnivariateSolver univariateSolver) {
        this.handler = eventHandler;
        this.maxCheckInterval = d;
        this.convergence = FastMath.abs(d2);
        this.maxIterationCount = i;
        this.solver = univariateSolver;
    }

    public EventHandler getEventHandler() {
        return this.handler;
    }

    public void setExpandable(ExpandableStatefulODE expandableStatefulODE) {
        this.expandable = expandableStatefulODE;
    }

    public double getMaxCheckInterval() {
        return this.maxCheckInterval;
    }

    public double getConvergence() {
        return this.convergence;
    }

    public int getMaxIterationCount() {
        return this.maxIterationCount;
    }

    public void reinitializeBegin(StepInterpolator stepInterpolator) throws MaxCountExceededException {
        this.f8069t0 = stepInterpolator.getPreviousTime();
        stepInterpolator.setInterpolatedTime(this.f8069t0);
        this.f8068g0 = this.handler.mo41725g(this.f8069t0, getCompleteState(stepInterpolator));
        if (this.f8068g0 == 0.0d) {
            double max = this.f8069t0 + (FastMath.max(this.solver.getAbsoluteAccuracy(), FastMath.abs(this.solver.getRelativeAccuracy() * this.f8069t0)) * 0.5d);
            stepInterpolator.setInterpolatedTime(max);
            this.f8068g0 = this.handler.mo41725g(max, getCompleteState(stepInterpolator));
        }
        this.g0Positive = this.f8068g0 >= 0.0d;
    }

    /* access modifiers changed from: private */
    public double[] getCompleteState(StepInterpolator stepInterpolator) {
        double[] dArr = new double[this.expandable.getTotalDimension()];
        this.expandable.getPrimaryMapper().insertEquationData(stepInterpolator.getInterpolatedState(), dArr);
        EquationsMapper[] secondaryMappers = this.expandable.getSecondaryMappers();
        int length = secondaryMappers.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            secondaryMappers[i].insertEquationData(stepInterpolator.getInterpolatedSecondaryState(i2), dArr);
            i++;
            i2++;
        }
        return dArr;
    }

    public boolean evaluateStep(StepInterpolator stepInterpolator) throws MaxCountExceededException, NoBracketingException {
        double d;
        C35731 r5;
        C35731 r27;
        double d2;
        double d3;
        final StepInterpolator stepInterpolator2 = stepInterpolator;
        try {
            this.forward = stepInterpolator.isForward();
            double currentTime = stepInterpolator.getCurrentTime() - this.f8069t0;
            if (FastMath.abs(currentTime) < this.convergence) {
                return false;
            }
            int max = FastMath.max(1, (int) FastMath.ceil(FastMath.abs(currentTime) / this.maxCheckInterval));
            double d4 = (double) max;
            Double.isNaN(d4);
            double d5 = currentTime / d4;
            C35731 r6 = new UnivariateFunction() {
                /* class org.apache.commons.math3.ode.events.EventState.C35731 */

                public double value(double d) throws LocalMaxCountExceededException {
                    try {
                        stepInterpolator2.setInterpolatedTime(d);
                        return EventState.this.handler.mo41725g(d, EventState.this.getCompleteState(stepInterpolator2));
                    } catch (MaxCountExceededException e) {
                        throw new LocalMaxCountExceededException(e);
                    }
                }
            };
            double d6 = this.f8069t0;
            double d7 = this.f8068g0;
            double d8 = d6;
            int i = 0;
            while (i < max) {
                double d9 = this.f8069t0;
                double d10 = (double) (i + 1);
                Double.isNaN(d10);
                double d11 = d9 + (d10 * d5);
                stepInterpolator2.setInterpolatedTime(d11);
                double g = this.handler.mo41725g(d11, getCompleteState(stepInterpolator));
                if (this.g0Positive ^ (g >= 0.0d)) {
                    this.increasing = g >= d7;
                    if (this.solver instanceof BracketedUnivariateSolver) {
                        BracketedUnivariateSolver bracketedUnivariateSolver = (BracketedUnivariateSolver) this.solver;
                        if (this.forward) {
                            d = d11;
                            d2 = bracketedUnivariateSolver.solve(this.maxIterationCount, r6, d8, d11, AllowedSolution.RIGHT_SIDE);
                        } else {
                            d = d11;
                            d2 = bracketedUnivariateSolver.solve(this.maxIterationCount, r6, d, d8, AllowedSolution.LEFT_SIDE);
                        }
                        r27 = r6;
                    } else {
                        d = d11;
                        double solve = this.forward ? this.solver.solve(this.maxIterationCount, r6, d8, d) : this.solver.solve(this.maxIterationCount, r6, d, d8);
                        int evaluations = this.maxIterationCount - this.solver.getEvaluations();
                        r27 = r6;
                        PegasusSolver pegasusSolver = new PegasusSolver(this.solver.getRelativeAccuracy(), this.solver.getAbsoluteAccuracy());
                        d2 = this.forward ? UnivariateSolverUtils.forceSide(evaluations, r27, pegasusSolver, solve, d8, d, AllowedSolution.RIGHT_SIDE) : UnivariateSolverUtils.forceSide(evaluations, r27, pegasusSolver, solve, d, d8, AllowedSolution.LEFT_SIDE);
                    }
                    if (Double.isNaN(this.previousEventTime) || FastMath.abs(d2 - d8) > this.convergence || FastMath.abs(d2 - this.previousEventTime) > this.convergence) {
                        r5 = r27;
                        if (Double.isNaN(this.previousEventTime) || FastMath.abs(this.previousEventTime - d2) > this.convergence) {
                            this.pendingEventTime = d2;
                            this.pendingEvent = true;
                            return true;
                        }
                    } else {
                        while (true) {
                            d3 = this.forward ? d8 + this.convergence : d8 - this.convergence;
                            r5 = r27;
                            g = r5.value(d3);
                            if (!(this.g0Positive ^ (g >= 0.0d))) {
                                break;
                            }
                            if (!(this.forward ^ (d3 >= d))) {
                                break;
                            }
                            r27 = r5;
                            d8 = d3;
                        }
                        i--;
                        d = d3;
                    }
                } else {
                    r5 = r6;
                    d = d11;
                }
                d7 = g;
                d8 = d;
                i++;
                r6 = r5;
            }
            this.pendingEvent = false;
            this.pendingEventTime = Double.NaN;
            return false;
        } catch (LocalMaxCountExceededException e) {
            throw e.getException();
        }
    }

    public double getEventTime() {
        if (this.pendingEvent) {
            return this.pendingEventTime;
        }
        return this.forward ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
    }

    public void stepAccepted(double d, double[] dArr) {
        this.f8069t0 = d;
        this.f8068g0 = this.handler.mo41725g(d, dArr);
        boolean z = true;
        if (!this.pendingEvent || FastMath.abs(this.pendingEventTime - d) > this.convergence) {
            if (this.f8068g0 < 0.0d) {
                z = false;
            }
            this.g0Positive = z;
            this.nextAction = EventHandler.Action.CONTINUE;
            return;
        }
        this.previousEventTime = d;
        boolean z2 = this.increasing;
        this.g0Positive = z2;
        this.nextAction = this.handler.eventOccurred(d, dArr, !(z2 ^ this.forward));
    }

    public boolean stop() {
        return this.nextAction == EventHandler.Action.STOP;
    }

    public boolean reset(double d, double[] dArr) {
        if (!this.pendingEvent || FastMath.abs(this.pendingEventTime - d) > this.convergence) {
            return false;
        }
        if (this.nextAction == EventHandler.Action.RESET_STATE) {
            this.handler.resetState(d, dArr);
        }
        this.pendingEvent = false;
        this.pendingEventTime = Double.NaN;
        if (this.nextAction == EventHandler.Action.RESET_STATE || this.nextAction == EventHandler.Action.RESET_DERIVATIVES) {
            return true;
        }
        return false;
    }

    private static class LocalMaxCountExceededException extends RuntimeException {
        private static final long serialVersionUID = 20120901;
        private final MaxCountExceededException wrapped;

        public LocalMaxCountExceededException(MaxCountExceededException maxCountExceededException) {
            this.wrapped = maxCountExceededException;
        }

        public MaxCountExceededException getException() {
            return this.wrapped;
        }
    }
}
