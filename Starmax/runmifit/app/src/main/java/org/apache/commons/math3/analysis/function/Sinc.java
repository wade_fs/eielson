package org.apache.commons.math3.analysis.function;

import org.apache.commons.math3.analysis.DifferentiableUnivariateFunction;
import org.apache.commons.math3.analysis.FunctionUtils;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.differentiation.DerivativeStructure;
import org.apache.commons.math3.analysis.differentiation.UnivariateDifferentiableFunction;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.util.FastMath;

public class Sinc implements UnivariateDifferentiableFunction, DifferentiableUnivariateFunction {
    private static final double SHORTCUT = 0.006d;
    private final boolean normalized;

    public Sinc() {
        this(false);
    }

    public Sinc(boolean z) {
        this.normalized = z;
    }

    public double value(double d) {
        double sin;
        if (this.normalized) {
            d *= 3.141592653589793d;
        }
        if (FastMath.abs(d) <= SHORTCUT) {
            double d2 = d * d;
            double d3 = (d2 - 20.0d) * d2;
            d = 120.0d;
            sin = d3 + 120.0d;
        } else {
            sin = FastMath.sin(d);
        }
        return sin / d;
    }

    @Deprecated
    public UnivariateFunction derivative() {
        return FunctionUtils.toDifferentiableUnivariateFunction(this).derivative();
    }

    public DerivativeStructure value(DerivativeStructure derivativeStructure) throws DimensionMismatchException {
        double[] dArr;
        double[] dArr2;
        double d;
        int i;
        Sinc sinc = this;
        double d2 = 1.0d;
        double value = (sinc.normalized ? 3.141592653589793d : 1.0d) * derivativeStructure.getValue();
        double d3 = value * value;
        double[] dArr3 = new double[(derivativeStructure.getOrder() + 1)];
        int i2 = 0;
        if (FastMath.abs(value) <= SHORTCUT) {
            while (i2 < dArr3.length) {
                int i3 = i2 / 2;
                if ((i2 & 1) == 0) {
                    double d4 = (double) ((i3 & 1) == 0 ? 1 : -1);
                    double d5 = (double) (i2 + 1);
                    Double.isNaN(d5);
                    double d6 = d2 / d5;
                    double d7 = (double) ((i2 * 2) + 6);
                    Double.isNaN(d7);
                    double d8 = d2 / d7;
                    double d9 = (double) ((i2 * 24) + 120);
                    Double.isNaN(d9);
                    Double.isNaN(d4);
                    dArr3[i2] = d4 * (d6 - ((d8 - (d3 / d9)) * d3));
                } else {
                    double d10 = (i3 & 1) == 0 ? -value : value;
                    double d11 = (double) (i2 + 2);
                    Double.isNaN(d11);
                    double d12 = (double) ((i2 * 6) + 24);
                    Double.isNaN(d12);
                    double d13 = (double) ((i2 * 120) + 720);
                    Double.isNaN(d13);
                    dArr3[i2] = d10 * ((1.0d / d11) - (((1.0d / d12) - (d3 / d13)) * d3));
                }
                i2++;
                d2 = 1.0d;
            }
            dArr = dArr3;
        } else {
            double d14 = 1.0d / value;
            double cos = FastMath.cos(value);
            double sin = FastMath.sin(value);
            dArr3[0] = d14 * sin;
            double[] dArr4 = new double[dArr3.length];
            dArr4[0] = 1.0d;
            double d15 = d14;
            int i4 = 1;
            while (i4 < dArr3.length) {
                double d16 = 0.0d;
                if ((i4 & 1) == 0) {
                    dArr4[i4] = 0.0d;
                    i = i4;
                    d = 0.0d;
                } else {
                    i = i4 - 1;
                    dArr4[i4] = dArr4[i];
                    d = dArr4[i4];
                }
                while (i > 1) {
                    double d17 = (double) (i - i4);
                    double d18 = dArr4[i];
                    Double.isNaN(d17);
                    int i5 = i - 1;
                    dArr4[i] = (d17 * d18) - dArr4[i5];
                    d16 = (d16 * d3) + dArr4[i];
                    double d19 = (double) (i5 - i4);
                    double d20 = dArr4[i5];
                    Double.isNaN(d19);
                    dArr4[i5] = (d19 * d20) + dArr4[i - 2];
                    d = (d * d3) + dArr4[i5];
                    i -= 2;
                    dArr3 = dArr3;
                }
                double[] dArr5 = dArr3;
                double d21 = dArr4[0];
                double d22 = (double) (-i4);
                Double.isNaN(d22);
                dArr4[0] = d21 * d22;
                d15 *= d14;
                dArr5[i4] = ((((d16 * d3) + dArr4[0]) * sin) + (d * value * cos)) * d15;
                i4++;
                dArr3 = dArr5;
            }
            dArr = dArr3;
            sinc = this;
        }
        if (sinc.normalized) {
            dArr2 = dArr;
            double d23 = 3.141592653589793d;
            for (int i6 = 1; i6 < dArr2.length; i6++) {
                dArr2[i6] = dArr2[i6] * d23;
                d23 *= 3.141592653589793d;
            }
        } else {
            dArr2 = dArr;
        }
        return derivativeStructure.compose(dArr2);
    }
}
