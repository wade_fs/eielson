package org.apache.commons.math3.geometry.euclidean.threed;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.math3.fraction.BigFraction;
import org.apache.commons.math3.geometry.Vector;
import org.apache.commons.math3.geometry.enclosing.EnclosingBall;
import org.apache.commons.math3.geometry.enclosing.SupportBallGenerator;
import org.apache.commons.math3.geometry.euclidean.twod.DiskGenerator;
import org.apache.commons.math3.geometry.euclidean.twod.Euclidean2D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.util.FastMath;

public class SphereGenerator implements SupportBallGenerator<Euclidean3D, Vector3D> {
    public EnclosingBall<Euclidean3D, Vector3D> ballOnSupport(List<Vector3D> list) {
        List<Vector3D> list2 = list;
        if (list.size() < 1) {
            return new EnclosingBall<>(Vector3D.ZERO, Double.NEGATIVE_INFINITY, new Vector3D[0]);
        }
        Vector3D vector3D = list2.get(0);
        if (list.size() < 2) {
            return new EnclosingBall<>(vector3D, 0.0d, vector3D);
        }
        Vector3D vector3D2 = list2.get(1);
        if (list.size() < 3) {
            return new EnclosingBall<>(new Vector3D(0.5d, vector3D, 0.5d, vector3D2), vector3D.distance((Vector<Euclidean3D>) vector3D2) * 0.5d, vector3D, vector3D2);
        }
        Vector3D vector3D3 = list2.get(2);
        if (list.size() < 4) {
            Plane plane = new Plane(vector3D, vector3D2, vector3D3, (vector3D.getNorm1() + vector3D2.getNorm1() + vector3D3.getNorm1()) * 1.0E-10d);
            EnclosingBall<Euclidean2D, Vector2D> ballOnSupport = new DiskGenerator().ballOnSupport(Arrays.asList(plane.toSubSpace((Vector<Euclidean3D>) vector3D), plane.toSubSpace((Vector<Euclidean3D>) vector3D2), plane.toSubSpace((Vector<Euclidean3D>) vector3D3)));
            return new EnclosingBall<>(plane.toSpace((Vector<Euclidean2D>) ballOnSupport.getCenter()), ballOnSupport.getRadius(), vector3D, vector3D2, vector3D3);
        }
        Vector3D vector3D4 = list2.get(3);
        BigFraction[] bigFractionArr = {new BigFraction(vector3D.getX()), new BigFraction(vector3D2.getX()), new BigFraction(vector3D3.getX()), new BigFraction(vector3D4.getX())};
        BigFraction[] bigFractionArr2 = {new BigFraction(vector3D.getY()), new BigFraction(vector3D2.getY()), new BigFraction(vector3D3.getY()), new BigFraction(vector3D4.getY())};
        BigFraction[] bigFractionArr3 = bigFractionArr;
        BigFraction[] bigFractionArr4 = {new BigFraction(vector3D.getZ()), new BigFraction(vector3D2.getZ()), new BigFraction(vector3D3.getZ()), new BigFraction(vector3D4.getZ())};
        BigFraction[] bigFractionArr5 = {bigFractionArr3[0].multiply(bigFractionArr3[0]).add(bigFractionArr2[0].multiply(bigFractionArr2[0])).add(bigFractionArr4[0].multiply(bigFractionArr4[0])), bigFractionArr3[1].multiply(bigFractionArr3[1]).add(bigFractionArr2[1].multiply(bigFractionArr2[1])).add(bigFractionArr4[1].multiply(bigFractionArr4[1])), bigFractionArr3[2].multiply(bigFractionArr3[2]).add(bigFractionArr2[2].multiply(bigFractionArr2[2])).add(bigFractionArr4[2].multiply(bigFractionArr4[2])), bigFractionArr3[3].multiply(bigFractionArr3[3]).add(bigFractionArr2[3].multiply(bigFractionArr2[3])).add(bigFractionArr4[3].multiply(bigFractionArr4[3]))};
        BigFraction[] bigFractionArr6 = bigFractionArr2;
        BigFraction multiply = minor(bigFractionArr3, bigFractionArr6, bigFractionArr4).multiply(2);
        BigFraction minor = minor(bigFractionArr5, bigFractionArr6, bigFractionArr4);
        BigFraction minor2 = minor(bigFractionArr5, bigFractionArr3, bigFractionArr4);
        BigFraction minor3 = minor(bigFractionArr5, bigFractionArr3, bigFractionArr6);
        BigFraction divide = minor.divide(multiply);
        BigFraction negate = minor2.divide(multiply).negate();
        BigFraction divide2 = minor3.divide(multiply);
        BigFraction subtract = bigFractionArr3[0].subtract(divide);
        BigFraction subtract2 = bigFractionArr6[0].subtract(negate);
        BigFraction subtract3 = bigFractionArr4[0].subtract(divide2);
        BigFraction add = subtract.multiply(subtract).add(subtract2.multiply(subtract2)).add(subtract3.multiply(subtract3));
        return new EnclosingBall<>(new Vector3D(divide.doubleValue(), negate.doubleValue(), divide2.doubleValue()), FastMath.sqrt(add.doubleValue()), vector3D, vector3D2, vector3D3, vector3D4);
    }

    private BigFraction minor(BigFraction[] bigFractionArr, BigFraction[] bigFractionArr2, BigFraction[] bigFractionArr3) {
        return bigFractionArr2[0].multiply(bigFractionArr3[1]).multiply(bigFractionArr[2].subtract(bigFractionArr[3])).add(bigFractionArr2[0].multiply(bigFractionArr3[2]).multiply(bigFractionArr[3].subtract(bigFractionArr[1]))).add(bigFractionArr2[0].multiply(bigFractionArr3[3]).multiply(bigFractionArr[1].subtract(bigFractionArr[2]))).add(bigFractionArr2[1].multiply(bigFractionArr3[0]).multiply(bigFractionArr[3].subtract(bigFractionArr[2]))).add(bigFractionArr2[1].multiply(bigFractionArr3[2]).multiply(bigFractionArr[0].subtract(bigFractionArr[3]))).add(bigFractionArr2[1].multiply(bigFractionArr3[3]).multiply(bigFractionArr[2].subtract(bigFractionArr[0]))).add(bigFractionArr2[2].multiply(bigFractionArr3[0]).multiply(bigFractionArr[1].subtract(bigFractionArr[3]))).add(bigFractionArr2[2].multiply(bigFractionArr3[1]).multiply(bigFractionArr[3].subtract(bigFractionArr[0]))).add(bigFractionArr2[2].multiply(bigFractionArr3[3]).multiply(bigFractionArr[0].subtract(bigFractionArr[1]))).add(bigFractionArr2[3].multiply(bigFractionArr3[0]).multiply(bigFractionArr[2].subtract(bigFractionArr[1]))).add(bigFractionArr2[3].multiply(bigFractionArr3[1]).multiply(bigFractionArr[0].subtract(bigFractionArr[2]))).add(bigFractionArr2[3].multiply(bigFractionArr3[2]).multiply(bigFractionArr[1].subtract(bigFractionArr[0])));
    }
}
