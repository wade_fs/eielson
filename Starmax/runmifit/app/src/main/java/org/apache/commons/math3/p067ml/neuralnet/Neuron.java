package org.apache.commons.math3.p067ml.neuralnet;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.util.Precision;

/* renamed from: org.apache.commons.math3.ml.neuralnet.Neuron */
public class Neuron implements Serializable {
    private static final long serialVersionUID = 20130207;
    private final AtomicReference<double[]> features;
    private final long identifier;
    private final int size;

    Neuron(long j, double[] dArr) {
        this.identifier = j;
        this.size = dArr.length;
        this.features = new AtomicReference<>(dArr.clone());
    }

    public long getIdentifier() {
        return this.identifier;
    }

    public int getSize() {
        return this.size;
    }

    public double[] getFeatures() {
        return (double[]) this.features.get().clone();
    }

    public boolean compareAndSetFeatures(double[] dArr, double[] dArr2) {
        int length = dArr2.length;
        int i = this.size;
        if (length == i) {
            double[] dArr3 = this.features.get();
            if (containSameValues(dArr3, dArr) && this.features.compareAndSet(dArr3, dArr2.clone())) {
                return true;
            }
            return false;
        }
        throw new DimensionMismatchException(dArr2.length, i);
    }

    private boolean containSameValues(double[] dArr, double[] dArr2) {
        int length = dArr2.length;
        int i = this.size;
        if (length == i) {
            for (int i2 = 0; i2 < this.size; i2++) {
                if (!Precision.equals(dArr[i2], dArr2[i2])) {
                    return false;
                }
            }
            return true;
        }
        throw new DimensionMismatchException(dArr2.length, i);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        throw new IllegalStateException();
    }

    private Object writeReplace() {
        return new SerializationProxy(this.identifier, this.features.get());
    }

    /* renamed from: org.apache.commons.math3.ml.neuralnet.Neuron$SerializationProxy */
    private static class SerializationProxy implements Serializable {
        private static final long serialVersionUID = 20130207;
        private final double[] features;
        private final long identifier;

        SerializationProxy(long j, double[] dArr) {
            this.identifier = j;
            this.features = dArr;
        }

        private Object readResolve() {
            return new Neuron(this.identifier, this.features);
        }
    }
}
