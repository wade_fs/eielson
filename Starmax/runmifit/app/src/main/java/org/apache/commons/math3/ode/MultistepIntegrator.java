package org.apache.commons.math3.ode;

import java.lang.reflect.Array;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.ode.nonstiff.AdaptiveStepsizeIntegrator;
import org.apache.commons.math3.ode.nonstiff.DormandPrince853Integrator;
import org.apache.commons.math3.ode.sampling.StepHandler;
import org.apache.commons.math3.ode.sampling.StepInterpolator;
import org.apache.commons.math3.util.FastMath;

public abstract class MultistepIntegrator extends AdaptiveStepsizeIntegrator {
    private double exp;
    private double maxGrowth;
    private double minReduction;
    private final int nSteps;
    protected Array2DRowRealMatrix nordsieck;
    private double safety;
    protected double[] scaled;
    private FirstOrderIntegrator starter;

    public interface NordsieckTransformer {
        Array2DRowRealMatrix initializeHighOrderDerivatives(double d, double[] dArr, double[][] dArr2, double[][] dArr3);
    }

    /* access modifiers changed from: protected */
    public abstract Array2DRowRealMatrix initializeHighOrderDerivatives(double d, double[] dArr, double[][] dArr2, double[][] dArr3);

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected MultistepIntegrator(String str, int i, int i2, double d, double d2, double d3, double d4) throws NumberIsTooSmallException {
        super(str, d, d2, d3, d4);
        int i3 = i;
        if (i3 >= 2) {
            this.starter = new DormandPrince853Integrator(d, d2, d3, d4);
            this.nSteps = i3;
            double d5 = (double) i2;
            Double.isNaN(d5);
            this.exp = -1.0d / d5;
            setSafety(0.9d);
            setMinReduction(0.2d);
            setMaxGrowth(FastMath.pow(2.0d, -this.exp));
            return;
        }
        throw new NumberIsTooSmallException(LocalizedFormats.INTEGRATION_METHOD_NEEDS_AT_LEAST_TWO_PREVIOUS_POINTS, Integer.valueOf(i), 2, true);
    }

    protected MultistepIntegrator(String str, int i, int i2, double d, double d2, double[] dArr, double[] dArr2) {
        super(str, d, d2, dArr, dArr2);
        this.starter = new DormandPrince853Integrator(d, d2, dArr, dArr2);
        this.nSteps = i;
        double d3 = (double) i2;
        Double.isNaN(d3);
        this.exp = -1.0d / d3;
        setSafety(0.9d);
        setMinReduction(0.2d);
        setMaxGrowth(FastMath.pow(2.0d, -this.exp));
    }

    public ODEIntegrator getStarterIntegrator() {
        return this.starter;
    }

    public void setStarterIntegrator(FirstOrderIntegrator firstOrderIntegrator) {
        this.starter = firstOrderIntegrator;
    }

    /* access modifiers changed from: protected */
    public void start(double d, double[] dArr, double d2) throws DimensionMismatchException, NumberIsTooSmallException, MaxCountExceededException, NoBracketingException {
        this.starter.clearEventHandlers();
        this.starter.clearStepHandlers();
        this.starter.addStepHandler(new NordsieckInitializer(this, this.nSteps, dArr.length));
        try {
            if (this.starter instanceof AbstractIntegrator) {
                ((AbstractIntegrator) this.starter).integrate(getExpandable(), d2);
            } else {
                this.starter.integrate(new FirstOrderDifferentialEquations() {
                    /* class org.apache.commons.math3.ode.MultistepIntegrator.C35721 */

                    public int getDimension() {
                        return MultistepIntegrator.this.getExpandable().getTotalDimension();
                    }

                    public void computeDerivatives(double d, double[] dArr, double[] dArr2) {
                        MultistepIntegrator.this.getExpandable().computeDerivatives(d, dArr, dArr2);
                    }
                }, d, dArr, d2, new double[dArr.length]);
            }
        } catch (InitializationCompletedMarkerException unused) {
            getEvaluationsCounter().incrementCount(this.starter.getEvaluations());
        }
        this.starter.clearStepHandlers();
    }

    public double getMinReduction() {
        return this.minReduction;
    }

    public void setMinReduction(double d) {
        this.minReduction = d;
    }

    public double getMaxGrowth() {
        return this.maxGrowth;
    }

    public void setMaxGrowth(double d) {
        this.maxGrowth = d;
    }

    public double getSafety() {
        return this.safety;
    }

    public void setSafety(double d) {
        this.safety = d;
    }

    /* access modifiers changed from: protected */
    public double computeStepGrowShrinkFactor(double d) {
        return FastMath.min(this.maxGrowth, FastMath.max(this.minReduction, this.safety * FastMath.pow(d, this.exp)));
    }

    private class NordsieckInitializer implements StepHandler {
        private int count = 0;

        /* renamed from: t */
        private final double[] f8065t;
        final /* synthetic */ MultistepIntegrator this$0;

        /* renamed from: y */
        private final double[][] f8066y;
        private final double[][] yDot;

        public void init(double d, double[] dArr, double d2) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
         arg types: [java.lang.Class<double>, int[]]
         candidates:
          ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
          ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
        public NordsieckInitializer(MultistepIntegrator multistepIntegrator, int i, int i2) {
            Class<double> cls = double.class;
            this.this$0 = multistepIntegrator;
            this.f8065t = new double[i];
            this.f8066y = (double[][]) Array.newInstance((Class<?>) cls, i, i2);
            this.yDot = (double[][]) Array.newInstance((Class<?>) cls, i, i2);
        }

        public void handleStep(StepInterpolator stepInterpolator, boolean z) throws MaxCountExceededException {
            double previousTime = stepInterpolator.getPreviousTime();
            double currentTime = stepInterpolator.getCurrentTime();
            if (this.count == 0) {
                stepInterpolator.setInterpolatedTime(previousTime);
                this.f8065t[0] = previousTime;
                ExpandableStatefulODE expandable = this.this$0.getExpandable();
                EquationsMapper primaryMapper = expandable.getPrimaryMapper();
                primaryMapper.insertEquationData(stepInterpolator.getInterpolatedState(), this.f8066y[this.count]);
                primaryMapper.insertEquationData(stepInterpolator.getInterpolatedDerivatives(), this.yDot[this.count]);
                EquationsMapper[] secondaryMappers = expandable.getSecondaryMappers();
                int i = 0;
                for (EquationsMapper equationsMapper : secondaryMappers) {
                    equationsMapper.insertEquationData(stepInterpolator.getInterpolatedSecondaryState(i), this.f8066y[this.count]);
                    equationsMapper.insertEquationData(stepInterpolator.getInterpolatedSecondaryDerivatives(i), this.yDot[this.count]);
                    i++;
                }
            }
            this.count++;
            stepInterpolator.setInterpolatedTime(currentTime);
            this.f8065t[this.count] = currentTime;
            ExpandableStatefulODE expandable2 = this.this$0.getExpandable();
            EquationsMapper primaryMapper2 = expandable2.getPrimaryMapper();
            primaryMapper2.insertEquationData(stepInterpolator.getInterpolatedState(), this.f8066y[this.count]);
            primaryMapper2.insertEquationData(stepInterpolator.getInterpolatedDerivatives(), this.yDot[this.count]);
            EquationsMapper[] secondaryMappers2 = expandable2.getSecondaryMappers();
            int i2 = 0;
            for (EquationsMapper equationsMapper2 : secondaryMappers2) {
                equationsMapper2.insertEquationData(stepInterpolator.getInterpolatedSecondaryState(i2), this.f8066y[this.count]);
                equationsMapper2.insertEquationData(stepInterpolator.getInterpolatedSecondaryDerivatives(i2), this.yDot[this.count]);
                i2++;
            }
            int i3 = this.count;
            double[] dArr = this.f8065t;
            if (i3 == dArr.length - 1) {
                MultistepIntegrator multistepIntegrator = this.this$0;
                multistepIntegrator.stepStart = dArr[0];
                double d = dArr[dArr.length - 1] - dArr[0];
                double length = (double) (dArr.length - 1);
                Double.isNaN(length);
                multistepIntegrator.stepSize = d / length;
                multistepIntegrator.scaled = (double[]) this.yDot[0].clone();
                for (int i4 = 0; i4 < this.this$0.scaled.length; i4++) {
                    double[] dArr2 = this.this$0.scaled;
                    dArr2[i4] = dArr2[i4] * this.this$0.stepSize;
                }
                MultistepIntegrator multistepIntegrator2 = this.this$0;
                multistepIntegrator2.nordsieck = multistepIntegrator2.initializeHighOrderDerivatives(multistepIntegrator2.stepSize, this.f8065t, this.f8066y, this.yDot);
                throw new InitializationCompletedMarkerException();
            }
        }
    }

    private static class InitializationCompletedMarkerException extends RuntimeException {
        private static final long serialVersionUID = -1914085471038046418L;

        public InitializationCompletedMarkerException() {
            super((Throwable) null);
        }
    }
}
