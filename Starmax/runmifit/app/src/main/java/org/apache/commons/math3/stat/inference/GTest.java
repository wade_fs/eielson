package org.apache.commons.math3.stat.inference;

import java.lang.reflect.Array;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.ZeroException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;

public class GTest {
    /* renamed from: g */
    public double mo42555g(double[] dArr, long[] jArr) throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException {
        boolean z;
        double d;
        double d2;
        double[] dArr2 = dArr;
        long[] jArr2 = jArr;
        if (dArr2.length < 2) {
            throw new DimensionMismatchException(dArr2.length, 2);
        } else if (dArr2.length == jArr2.length) {
            MathArrays.checkPositive(dArr);
            MathArrays.checkNonNegative(jArr);
            double d3 = 0.0d;
            double d4 = 0.0d;
            double d5 = 0.0d;
            for (int i = 0; i < jArr2.length; i++) {
                d4 += dArr2[i];
                double d6 = (double) jArr2[i];
                Double.isNaN(d6);
                d5 += d6;
            }
            double d7 = 1.0d;
            if (FastMath.abs(d4 - d5) > 1.0E-5d) {
                d7 = d5 / d4;
                z = true;
            } else {
                z = false;
            }
            for (int i2 = 0; i2 < jArr2.length; i2++) {
                if (z) {
                    d2 = (double) jArr2[i2];
                    d = dArr2[i2] * d7;
                } else {
                    d2 = (double) jArr2[i2];
                    d = dArr2[i2];
                }
                Double.isNaN(d2);
                double log = FastMath.log(d2 / d);
                double d8 = (double) jArr2[i2];
                Double.isNaN(d8);
                d3 += d8 * log;
            }
            return d3 * 2.0d;
        } else {
            throw new DimensionMismatchException(dArr2.length, jArr2.length);
        }
    }

    public double gTest(double[] dArr, long[] jArr) throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException, MaxCountExceededException {
        double length = (double) dArr.length;
        Double.isNaN(length);
        return 1.0d - new ChiSquaredDistribution(length - 1.0d).cumulativeProbability(mo42555g(dArr, jArr));
    }

    public double gTestIntrinsic(double[] dArr, long[] jArr) throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException, MaxCountExceededException {
        double length = (double) dArr.length;
        Double.isNaN(length);
        return 1.0d - new ChiSquaredDistribution(length - 2.0d).cumulativeProbability(mo42555g(dArr, jArr));
    }

    public boolean gTest(double[] dArr, long[] jArr, double d) throws NotPositiveException, NotStrictlyPositiveException, DimensionMismatchException, OutOfRangeException, MaxCountExceededException {
        if (d > 0.0d && d <= 0.5d) {
            return gTest(dArr, jArr) < d;
        }
        throw new OutOfRangeException(LocalizedFormats.OUT_OF_BOUND_SIGNIFICANCE_LEVEL, Double.valueOf(d), 0, Double.valueOf(0.5d));
    }

    private double entropy(long[][] jArr) {
        double d = 0.0d;
        int i = 0;
        while (i < jArr.length) {
            double d2 = d;
            for (int i2 = 0; i2 < jArr[i].length; i2++) {
                double d3 = (double) jArr[i][i2];
                Double.isNaN(d3);
                d2 += d3;
            }
            i++;
            d = d2;
        }
        double d4 = 0.0d;
        for (int i3 = 0; i3 < jArr.length; i3++) {
            for (int i4 = 0; i4 < jArr[i3].length; i4++) {
                if (jArr[i3][i4] != 0) {
                    double d5 = (double) jArr[i3][i4];
                    Double.isNaN(d5);
                    double d6 = d5 / d;
                    d4 += d6 * FastMath.log(d6);
                }
            }
        }
        return -d4;
    }

    private double entropy(long[] jArr) {
        double d = 0.0d;
        double d2 = 0.0d;
        for (long j : jArr) {
            double d3 = (double) j;
            Double.isNaN(d3);
            d2 += d3;
        }
        for (int i = 0; i < jArr.length; i++) {
            if (jArr[i] != 0) {
                double d4 = (double) jArr[i];
                Double.isNaN(d4);
                double d5 = d4 / d2;
                d += d5 * FastMath.log(d5);
            }
        }
        return -d;
    }

    public double gDataSetsComparison(long[] jArr, long[] jArr2) throws DimensionMismatchException, NotPositiveException, ZeroException {
        long[] jArr3 = jArr;
        long[] jArr4 = jArr2;
        if (jArr3.length < 2) {
            throw new DimensionMismatchException(jArr3.length, 2);
        } else if (jArr3.length == jArr4.length) {
            MathArrays.checkNonNegative(jArr);
            MathArrays.checkNonNegative(jArr2);
            long[] jArr5 = new long[jArr3.length];
            long[][] jArr6 = (long[][]) Array.newInstance(long.class, 2, jArr3.length);
            long j = 0;
            long j2 = 0;
            for (int i = 0; i < jArr3.length; i++) {
                if (jArr3[i] == 0 && jArr4[i] == 0) {
                    throw new ZeroException(LocalizedFormats.OBSERVED_COUNTS_BOTTH_ZERO_FOR_ENTRY, Integer.valueOf(i));
                }
                j += jArr3[i];
                j2 += jArr4[i];
                jArr5[i] = jArr3[i] + jArr4[i];
                jArr6[0][i] = jArr3[i];
                jArr6[1][i] = jArr4[i];
            }
            if (j == 0 || j2 == 0) {
                throw new ZeroException();
            }
            long[] jArr7 = {j, j2};
            double d = (double) j;
            double d2 = (double) j2;
            Double.isNaN(d);
            Double.isNaN(d2);
            return (d + d2) * 2.0d * ((entropy(jArr7) + entropy(jArr5)) - entropy(jArr6));
        } else {
            throw new DimensionMismatchException(jArr3.length, jArr4.length);
        }
    }

    public double rootLogLikelihoodRatio(long j, long j2, long j3, long j4) {
        double sqrt = FastMath.sqrt(gDataSetsComparison(new long[]{j, j2}, new long[]{j3, j4}));
        double d = (double) j;
        double d2 = (double) (j + j2);
        Double.isNaN(d);
        Double.isNaN(d2);
        double d3 = d / d2;
        double d4 = (double) j3;
        double d5 = (double) (j3 + j4);
        Double.isNaN(d4);
        Double.isNaN(d5);
        return d3 < d4 / d5 ? -sqrt : sqrt;
    }

    public double gTestDataSetsComparison(long[] jArr, long[] jArr2) throws DimensionMismatchException, NotPositiveException, ZeroException, MaxCountExceededException {
        double length = (double) jArr.length;
        Double.isNaN(length);
        return 1.0d - new ChiSquaredDistribution(length - 1.0d).cumulativeProbability(gDataSetsComparison(jArr, jArr2));
    }

    public boolean gTestDataSetsComparison(long[] jArr, long[] jArr2, double d) throws DimensionMismatchException, NotPositiveException, ZeroException, OutOfRangeException, MaxCountExceededException {
        if (d > 0.0d && d <= 0.5d) {
            return gTestDataSetsComparison(jArr, jArr2) < d;
        }
        throw new OutOfRangeException(LocalizedFormats.OUT_OF_BOUND_SIGNIFICANCE_LEVEL, Double.valueOf(d), 0, Double.valueOf(0.5d));
    }
}
