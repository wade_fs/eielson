package org.apache.commons.math3.stat.correlation;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

public class Covariance {
    private final RealMatrix covarianceMatrix;

    /* renamed from: n */
    private final int f8168n;

    public Covariance() {
        this.covarianceMatrix = null;
        this.f8168n = 0;
    }

    public Covariance(double[][] dArr, boolean z) throws MathIllegalArgumentException, NotStrictlyPositiveException {
        this(new BlockRealMatrix(dArr), z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.correlation.Covariance.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.stat.correlation.Covariance.<init>(org.apache.commons.math3.linear.RealMatrix, boolean):void
      org.apache.commons.math3.stat.correlation.Covariance.<init>(double[][], boolean):void */
    public Covariance(double[][] dArr) throws MathIllegalArgumentException, NotStrictlyPositiveException {
        this(dArr, true);
    }

    public Covariance(RealMatrix realMatrix, boolean z) throws MathIllegalArgumentException {
        checkSufficientData(realMatrix);
        this.f8168n = realMatrix.getRowDimension();
        this.covarianceMatrix = computeCovarianceMatrix(realMatrix, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.correlation.Covariance.<init>(org.apache.commons.math3.linear.RealMatrix, boolean):void
     arg types: [org.apache.commons.math3.linear.RealMatrix, int]
     candidates:
      org.apache.commons.math3.stat.correlation.Covariance.<init>(double[][], boolean):void
      org.apache.commons.math3.stat.correlation.Covariance.<init>(org.apache.commons.math3.linear.RealMatrix, boolean):void */
    public Covariance(RealMatrix realMatrix) throws MathIllegalArgumentException {
        this(realMatrix, true);
    }

    public RealMatrix getCovarianceMatrix() {
        return this.covarianceMatrix;
    }

    public int getN() {
        return this.f8168n;
    }

    /* access modifiers changed from: protected */
    public RealMatrix computeCovarianceMatrix(RealMatrix realMatrix, boolean z) throws MathIllegalArgumentException {
        int columnDimension = realMatrix.getColumnDimension();
        Variance variance = new Variance(z);
        BlockRealMatrix blockRealMatrix = new BlockRealMatrix(columnDimension, columnDimension);
        for (int i = 0; i < columnDimension; i++) {
            for (int i2 = 0; i2 < i; i2++) {
                double covariance = covariance(realMatrix.getColumn(i), realMatrix.getColumn(i2), z);
                blockRealMatrix.setEntry(i, i2, covariance);
                blockRealMatrix.setEntry(i2, i, covariance);
            }
            blockRealMatrix.setEntry(i, i, variance.evaluate(realMatrix.getColumn(i)));
        }
        return blockRealMatrix;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.correlation.Covariance.computeCovarianceMatrix(org.apache.commons.math3.linear.RealMatrix, boolean):org.apache.commons.math3.linear.RealMatrix
     arg types: [org.apache.commons.math3.linear.RealMatrix, int]
     candidates:
      org.apache.commons.math3.stat.correlation.Covariance.computeCovarianceMatrix(double[][], boolean):org.apache.commons.math3.linear.RealMatrix
      org.apache.commons.math3.stat.correlation.Covariance.computeCovarianceMatrix(org.apache.commons.math3.linear.RealMatrix, boolean):org.apache.commons.math3.linear.RealMatrix */
    /* access modifiers changed from: protected */
    public RealMatrix computeCovarianceMatrix(RealMatrix realMatrix) throws MathIllegalArgumentException {
        return computeCovarianceMatrix(realMatrix, true);
    }

    /* access modifiers changed from: protected */
    public RealMatrix computeCovarianceMatrix(double[][] dArr, boolean z) throws MathIllegalArgumentException, NotStrictlyPositiveException {
        return computeCovarianceMatrix(new BlockRealMatrix(dArr), z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.correlation.Covariance.computeCovarianceMatrix(double[][], boolean):org.apache.commons.math3.linear.RealMatrix
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.stat.correlation.Covariance.computeCovarianceMatrix(org.apache.commons.math3.linear.RealMatrix, boolean):org.apache.commons.math3.linear.RealMatrix
      org.apache.commons.math3.stat.correlation.Covariance.computeCovarianceMatrix(double[][], boolean):org.apache.commons.math3.linear.RealMatrix */
    /* access modifiers changed from: protected */
    public RealMatrix computeCovarianceMatrix(double[][] dArr) throws MathIllegalArgumentException, NotStrictlyPositiveException {
        return computeCovarianceMatrix(dArr, true);
    }

    public double covariance(double[] dArr, double[] dArr2, boolean z) throws MathIllegalArgumentException {
        double[] dArr3 = dArr;
        double[] dArr4 = dArr2;
        Mean mean = new Mean();
        int length = dArr3.length;
        int i = 0;
        if (length != dArr4.length) {
            throw new MathIllegalArgumentException(LocalizedFormats.DIMENSIONS_MISMATCH_SIMPLE, Integer.valueOf(length), Integer.valueOf(dArr4.length));
        } else if (length >= 2) {
            double evaluate = mean.evaluate(dArr3);
            double evaluate2 = mean.evaluate(dArr4);
            double d = 0.0d;
            while (i < length) {
                i++;
                double d2 = (double) i;
                Double.isNaN(d2);
                d += (((dArr3[i] - evaluate) * (dArr4[i] - evaluate2)) - d) / d2;
                evaluate = evaluate;
            }
            if (!z) {
                return d;
            }
            double d3 = (double) length;
            double d4 = (double) (length - 1);
            Double.isNaN(d3);
            Double.isNaN(d4);
            return d * (d3 / d4);
        } else {
            throw new MathIllegalArgumentException(LocalizedFormats.INSUFFICIENT_OBSERVED_POINTS_IN_SAMPLE, Integer.valueOf(length), 2);
        }
    }

    public double covariance(double[] dArr, double[] dArr2) throws MathIllegalArgumentException {
        return covariance(dArr, dArr2, true);
    }

    private void checkSufficientData(RealMatrix realMatrix) throws MathIllegalArgumentException {
        int rowDimension = realMatrix.getRowDimension();
        int columnDimension = realMatrix.getColumnDimension();
        if (rowDimension < 2 || columnDimension < 1) {
            throw new MathIllegalArgumentException(LocalizedFormats.INSUFFICIENT_ROWS_AND_COLUMNS, Integer.valueOf(rowDimension), Integer.valueOf(columnDimension));
        }
    }
}
