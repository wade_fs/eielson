package org.apache.commons.math3.linear;

import java.lang.reflect.Array;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.util.LocalizedFormats;

public class SingularValueDecomposition {
    private static final double EPS = 2.220446049250313E-16d;
    private static final double TINY = 1.6033346880071782E-291d;
    private RealMatrix cachedS;
    private final RealMatrix cachedU;
    private RealMatrix cachedUt;
    private final RealMatrix cachedV;
    private RealMatrix cachedVt;

    /* renamed from: m */
    private final int f8048m;

    /* renamed from: n */
    private final int f8049n;
    /* access modifiers changed from: private */
    public final double[] singularValues;
    private final double tol;
    private final boolean transposed;

    /* JADX WARN: Type inference failed for: r10v6 */
    /* JADX WARN: Type inference failed for: r7v6 */
    /* JADX WARN: Type inference failed for: r7v7 */
    /* JADX WARN: Type inference failed for: r15v20 */
    /* JADX WARN: Type inference failed for: r27v4 */
    /* JADX WARN: Type inference failed for: r6v27 */
    /* JADX WARN: Type inference failed for: r7v8 */
    /* JADX WARN: Type inference failed for: r5v18 */
    /* JADX WARN: Type inference failed for: r5v19 */
    /* JADX WARN: Type inference failed for: r5v22 */
    /* JADX WARN: Type inference failed for: r5v32 */
    /* JADX WARN: Type inference failed for: r6v33 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SingularValueDecomposition(org.apache.commons.math3.linear.RealMatrix r33) {
        /*
            r32 = this;
            r0 = r32
            java.lang.Class<double> r1 = double.class
            r32.<init>()
            int r2 = r33.getRowDimension()
            int r3 = r33.getColumnDimension()
            r4 = 0
            r5 = 1
            if (r2 >= r3) goto L_0x002a
            r0.transposed = r5
            org.apache.commons.math3.linear.RealMatrix r2 = r33.transpose()
            double[][] r2 = r2.getData()
            int r3 = r33.getColumnDimension()
            r0.f8048m = r3
            int r3 = r33.getRowDimension()
            r0.f8049n = r3
            goto L_0x003c
        L_0x002a:
            r0.transposed = r4
            double[][] r2 = r33.getData()
            int r3 = r33.getRowDimension()
            r0.f8048m = r3
            int r3 = r33.getColumnDimension()
            r0.f8049n = r3
        L_0x003c:
            int r3 = r0.f8049n
            double[] r6 = new double[r3]
            r0.singularValues = r6
            int r6 = r0.f8048m
            int[] r3 = new int[]{r6, r3}
            java.lang.Object r3 = java.lang.reflect.Array.newInstance(r1, r3)
            double[][] r3 = (double[][]) r3
            int r6 = r0.f8049n
            int[] r6 = new int[]{r6, r6}
            java.lang.Object r1 = java.lang.reflect.Array.newInstance(r1, r6)
            double[][] r1 = (double[][]) r1
            int r6 = r0.f8049n
            double[] r7 = new double[r6]
            int r8 = r0.f8048m
            double[] r9 = new double[r8]
            int r8 = r8 - r5
            int r6 = org.apache.commons.math3.util.FastMath.min(r8, r6)
            int r8 = r0.f8049n
            r10 = 2
            int r8 = r8 - r10
            int r8 = org.apache.commons.math3.util.FastMath.max(r4, r8)
            r11 = 0
        L_0x0070:
            int r12 = org.apache.commons.math3.util.FastMath.max(r6, r8)
            r15 = 0
            if (r11 >= r12) goto L_0x01f0
            if (r11 >= r6) goto L_0x00d5
            double[] r12 = r0.singularValues
            r12[r11] = r15
            r12 = r11
        L_0x007f:
            int r4 = r0.f8048m
            if (r12 >= r4) goto L_0x0099
            double[] r4 = r0.singularValues
            r33 = r6
            r5 = r4[r11]
            r17 = r2[r12]
            r13 = r17[r11]
            double r5 = org.apache.commons.math3.util.FastMath.hypot(r5, r13)
            r4[r11] = r5
            int r12 = r12 + 1
            r6 = r33
            r5 = 1
            goto L_0x007f
        L_0x0099:
            r33 = r6
            double[] r4 = r0.singularValues
            r5 = r4[r11]
            int r12 = (r5 > r15 ? 1 : (r5 == r15 ? 0 : -1))
            if (r12 == 0) goto L_0x00cd
            r5 = r2[r11]
            r12 = r5[r11]
            int r5 = (r12 > r15 ? 1 : (r12 == r15 ? 0 : -1))
            if (r5 >= 0) goto L_0x00b0
            r5 = r4[r11]
            double r5 = -r5
            r4[r11] = r5
        L_0x00b0:
            r4 = r11
        L_0x00b1:
            int r5 = r0.f8048m
            if (r4 >= r5) goto L_0x00c4
            r5 = r2[r4]
            r12 = r5[r11]
            double[] r6 = r0.singularValues
            r20 = r6[r11]
            double r12 = r12 / r20
            r5[r11] = r12
            int r4 = r4 + 1
            goto L_0x00b1
        L_0x00c4:
            r4 = r2[r11]
            r5 = r4[r11]
            r12 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r5 = r5 + r12
            r4[r11] = r5
        L_0x00cd:
            double[] r4 = r0.singularValues
            r5 = r4[r11]
            double r5 = -r5
            r4[r11] = r5
            goto L_0x00d7
        L_0x00d5:
            r33 = r6
        L_0x00d7:
            int r4 = r11 + 1
            r5 = r4
        L_0x00da:
            int r6 = r0.f8049n
            if (r5 >= r6) goto L_0x0128
            r6 = r33
            if (r11 >= r6) goto L_0x011c
            double[] r12 = r0.singularValues
            r13 = r12[r11]
            int r12 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r12 == 0) goto L_0x011c
            r12 = r11
            r13 = r15
        L_0x00ec:
            int r10 = r0.f8048m
            if (r12 >= r10) goto L_0x00ff
            r10 = r2[r12]
            r20 = r10[r11]
            r10 = r2[r12]
            r22 = r10[r5]
            double r20 = r20 * r22
            double r13 = r13 + r20
            int r12 = r12 + 1
            goto L_0x00ec
        L_0x00ff:
            double r12 = -r13
            r10 = r2[r11]
            r20 = r10[r11]
            double r12 = r12 / r20
            r10 = r11
        L_0x0107:
            int r14 = r0.f8048m
            if (r10 >= r14) goto L_0x011c
            r14 = r2[r10]
            r20 = r14[r5]
            r17 = r2[r10]
            r22 = r17[r11]
            double r22 = r22 * r12
            double r20 = r20 + r22
            r14[r5] = r20
            int r10 = r10 + 1
            goto L_0x0107
        L_0x011c:
            r10 = r2[r11]
            r12 = r10[r5]
            r7[r5] = r12
            int r5 = r5 + 1
            r33 = r6
            r10 = 2
            goto L_0x00da
        L_0x0128:
            r6 = r33
            if (r11 >= r6) goto L_0x013c
            r5 = r11
        L_0x012d:
            int r10 = r0.f8048m
            if (r5 >= r10) goto L_0x013c
            r10 = r3[r5]
            r12 = r2[r5]
            r13 = r12[r11]
            r10[r11] = r13
            int r5 = r5 + 1
            goto L_0x012d
        L_0x013c:
            if (r11 >= r8) goto L_0x01e6
            r7[r11] = r15
            r5 = r4
        L_0x0141:
            int r10 = r0.f8049n
            if (r5 >= r10) goto L_0x0156
            r12 = r7[r11]
            r14 = r1
            r10 = r2
            r1 = r7[r5]
            double r1 = org.apache.commons.math3.util.FastMath.hypot(r12, r1)
            r7[r11] = r1
            int r5 = r5 + 1
            r2 = r10
            r1 = r14
            goto L_0x0141
        L_0x0156:
            r14 = r1
            r10 = r2
            r1 = r7[r11]
            int r5 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r5 == 0) goto L_0x0180
            r1 = r7[r4]
            int r5 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r5 >= 0) goto L_0x0169
            r1 = r7[r11]
            double r1 = -r1
            r7[r11] = r1
        L_0x0169:
            r1 = r4
        L_0x016a:
            int r2 = r0.f8049n
            if (r1 >= r2) goto L_0x0179
            r12 = r7[r1]
            r20 = r7[r11]
            double r12 = r12 / r20
            r7[r1] = r12
            int r1 = r1 + 1
            goto L_0x016a
        L_0x0179:
            r1 = r7[r4]
            r12 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r1 = r1 + r12
            r7[r4] = r1
        L_0x0180:
            r1 = r7[r11]
            double r1 = -r1
            r7[r11] = r1
            int r1 = r0.f8048m
            if (r4 >= r1) goto L_0x01d8
            r1 = r7[r11]
            int r5 = (r1 > r15 ? 1 : (r1 == r15 ? 0 : -1))
            if (r5 == 0) goto L_0x01d8
            r1 = r4
        L_0x0190:
            int r2 = r0.f8048m
            if (r1 >= r2) goto L_0x0199
            r9[r1] = r15
            int r1 = r1 + 1
            goto L_0x0190
        L_0x0199:
            r1 = r4
        L_0x019a:
            int r2 = r0.f8049n
            if (r1 >= r2) goto L_0x01b6
            r2 = r4
        L_0x019f:
            int r5 = r0.f8048m
            if (r2 >= r5) goto L_0x01b3
            r12 = r9[r2]
            r15 = r7[r1]
            r5 = r10[r2]
            r17 = r5[r1]
            double r15 = r15 * r17
            double r12 = r12 + r15
            r9[r2] = r12
            int r2 = r2 + 1
            goto L_0x019f
        L_0x01b3:
            int r1 = r1 + 1
            goto L_0x019a
        L_0x01b6:
            r1 = r4
        L_0x01b7:
            int r2 = r0.f8049n
            if (r1 >= r2) goto L_0x01d8
            r12 = r7[r1]
            double r12 = -r12
            r15 = r7[r4]
            double r12 = r12 / r15
            r2 = r4
        L_0x01c2:
            int r5 = r0.f8048m
            if (r2 >= r5) goto L_0x01d5
            r5 = r10[r2]
            r15 = r5[r1]
            r17 = r9[r2]
            double r17 = r17 * r12
            double r15 = r15 + r17
            r5[r1] = r15
            int r2 = r2 + 1
            goto L_0x01c2
        L_0x01d5:
            int r1 = r1 + 1
            goto L_0x01b7
        L_0x01d8:
            r1 = r4
        L_0x01d9:
            int r2 = r0.f8049n
            if (r1 >= r2) goto L_0x01e8
            r2 = r14[r1]
            r12 = r7[r1]
            r2[r11] = r12
            int r1 = r1 + 1
            goto L_0x01d9
        L_0x01e6:
            r14 = r1
            r10 = r2
        L_0x01e8:
            r11 = r4
            r2 = r10
            r1 = r14
            r4 = 0
            r5 = 1
            r10 = 2
            goto L_0x0070
        L_0x01f0:
            r14 = r1
            r10 = r2
            int r1 = r0.f8049n
            if (r6 >= r1) goto L_0x01fe
            double[] r2 = r0.singularValues
            r4 = r10[r6]
            r11 = r4[r6]
            r2[r6] = r11
        L_0x01fe:
            int r2 = r0.f8048m
            if (r2 >= r1) goto L_0x0208
            double[] r2 = r0.singularValues
            int r4 = r1 + -1
            r2[r4] = r15
        L_0x0208:
            int r2 = r8 + 1
            if (r2 >= r1) goto L_0x0214
            r2 = r10[r8]
            int r4 = r1 + -1
            r4 = r2[r4]
            r7[r8] = r4
        L_0x0214:
            int r2 = r1 + -1
            r7[r2] = r15
            r4 = r6
        L_0x0219:
            int r5 = r0.f8049n
            if (r4 >= r5) goto L_0x0232
            r5 = 0
        L_0x021e:
            int r9 = r0.f8048m
            if (r5 >= r9) goto L_0x0229
            r9 = r3[r5]
            r9[r4] = r15
            int r5 = r5 + 1
            goto L_0x021e
        L_0x0229:
            r5 = r3[r4]
            r9 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r5[r4] = r9
            int r4 = r4 + 1
            goto L_0x0219
        L_0x0232:
            r4 = 1
            int r6 = r6 - r4
        L_0x0234:
            if (r6 < 0) goto L_0x02b4
            double[] r4 = r0.singularValues
            r9 = r4[r6]
            int r4 = (r9 > r15 ? 1 : (r9 == r15 ? 0 : -1))
            if (r4 == 0) goto L_0x029f
            int r4 = r6 + 1
        L_0x0240:
            int r5 = r0.f8049n
            if (r4 >= r5) goto L_0x0277
            r5 = r6
            r9 = r15
        L_0x0246:
            int r11 = r0.f8048m
            if (r5 >= r11) goto L_0x0258
            r11 = r3[r5]
            r12 = r11[r6]
            r11 = r3[r5]
            r20 = r11[r4]
            double r12 = r12 * r20
            double r9 = r9 + r12
            int r5 = r5 + 1
            goto L_0x0246
        L_0x0258:
            double r9 = -r9
            r5 = r3[r6]
            r11 = r5[r6]
            double r9 = r9 / r11
            r5 = r6
        L_0x025f:
            int r11 = r0.f8048m
            if (r5 >= r11) goto L_0x0274
            r11 = r3[r5]
            r12 = r11[r4]
            r17 = r3[r5]
            r20 = r17[r6]
            double r20 = r20 * r9
            double r12 = r12 + r20
            r11[r4] = r12
            int r5 = r5 + 1
            goto L_0x025f
        L_0x0274:
            int r4 = r4 + 1
            goto L_0x0240
        L_0x0277:
            r4 = r6
        L_0x0278:
            int r5 = r0.f8048m
            if (r4 >= r5) goto L_0x0288
            r5 = r3[r4]
            r9 = r3[r4]
            r10 = r9[r6]
            double r9 = -r10
            r5[r6] = r9
            int r4 = r4 + 1
            goto L_0x0278
        L_0x0288:
            r4 = r3[r6]
            r5 = r3[r6]
            r9 = r5[r6]
            r11 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r9 = r9 + r11
            r4[r6] = r9
            r4 = 0
        L_0x0294:
            int r5 = r6 + -1
            if (r4 >= r5) goto L_0x02b1
            r5 = r3[r4]
            r5[r6] = r15
            int r4 = r4 + 1
            goto L_0x0294
        L_0x029f:
            r4 = 0
        L_0x02a0:
            int r5 = r0.f8048m
            if (r4 >= r5) goto L_0x02ab
            r5 = r3[r4]
            r5[r6] = r15
            int r4 = r4 + 1
            goto L_0x02a0
        L_0x02ab:
            r4 = r3[r6]
            r9 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r4[r6] = r9
        L_0x02b1:
            int r6 = r6 + -1
            goto L_0x0234
        L_0x02b4:
            int r4 = r0.f8049n
            r5 = 1
            int r4 = r4 - r5
        L_0x02b8:
            if (r4 < 0) goto L_0x0312
            if (r4 >= r8) goto L_0x02fd
            r5 = r7[r4]
            int r9 = (r5 > r15 ? 1 : (r5 == r15 ? 0 : -1))
            if (r9 == 0) goto L_0x02fd
            int r5 = r4 + 1
            r6 = r5
        L_0x02c5:
            int r9 = r0.f8049n
            if (r6 >= r9) goto L_0x02fd
            r9 = r5
            r10 = r15
        L_0x02cb:
            int r12 = r0.f8049n
            if (r9 >= r12) goto L_0x02de
            r12 = r14[r9]
            r20 = r12[r4]
            r12 = r14[r9]
            r22 = r12[r6]
            double r20 = r20 * r22
            double r10 = r10 + r20
            int r9 = r9 + 1
            goto L_0x02cb
        L_0x02de:
            double r9 = -r10
            r11 = r14[r5]
            r12 = r11[r4]
            double r9 = r9 / r12
            r11 = r5
        L_0x02e5:
            int r12 = r0.f8049n
            if (r11 >= r12) goto L_0x02fa
            r12 = r14[r11]
            r20 = r12[r6]
            r13 = r14[r11]
            r22 = r13[r4]
            double r22 = r22 * r9
            double r20 = r20 + r22
            r12[r6] = r20
            int r11 = r11 + 1
            goto L_0x02e5
        L_0x02fa:
            int r6 = r6 + 1
            goto L_0x02c5
        L_0x02fd:
            r5 = 0
        L_0x02fe:
            int r6 = r0.f8049n
            if (r5 >= r6) goto L_0x0309
            r6 = r14[r5]
            r6[r4] = r15
            int r5 = r5 + 1
            goto L_0x02fe
        L_0x0309:
            r5 = r14[r4]
            r9 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r5[r4] = r9
            int r4 = r4 + -1
            goto L_0x02b8
        L_0x0312:
            r4 = 4372995238176751616(0x3cb0000000000000, double:2.220446049250313E-16)
            if (r1 <= 0) goto L_0x0664
            int r6 = r1 + -2
            r8 = r6
        L_0x0319:
            r9 = 256705178760118272(0x390000000000000, double:1.6033346880071782E-291)
            if (r8 < 0) goto L_0x0344
            double[] r11 = r0.singularValues
            r12 = r11[r8]
            double r11 = org.apache.commons.math3.util.FastMath.abs(r12)
            double[] r13 = r0.singularValues
            int r17 = r8 + 1
            r17 = r13[r17]
            double r17 = org.apache.commons.math3.util.FastMath.abs(r17)
            double r11 = r11 + r17
            double r11 = r11 * r4
            double r11 = r11 + r9
            r17 = r7[r8]
            double r17 = org.apache.commons.math3.util.FastMath.abs(r17)
            int r13 = (r17 > r11 ? 1 : (r17 == r11 ? 0 : -1))
            if (r13 > 0) goto L_0x0341
            r7[r8] = r15
            goto L_0x0344
        L_0x0341:
            int r8 = r8 + -1
            goto L_0x0319
        L_0x0344:
            if (r8 != r6) goto L_0x0349
            r5 = 4
            r4 = 1
            goto L_0x0392
        L_0x0349:
            int r12 = r1 + -1
            r13 = r12
        L_0x034c:
            if (r13 < r8) goto L_0x0385
            if (r13 != r8) goto L_0x0351
            goto L_0x0385
        L_0x0351:
            if (r13 == r1) goto L_0x035a
            r17 = r7[r13]
            double r17 = org.apache.commons.math3.util.FastMath.abs(r17)
            goto L_0x035c
        L_0x035a:
            r17 = r15
        L_0x035c:
            int r11 = r8 + 1
            if (r13 == r11) goto L_0x0369
            int r11 = r13 + -1
            r20 = r7[r11]
            double r20 = org.apache.commons.math3.util.FastMath.abs(r20)
            goto L_0x036b
        L_0x0369:
            r20 = r15
        L_0x036b:
            double r17 = r17 + r20
            double[] r11 = r0.singularValues
            r20 = r11[r13]
            double r20 = org.apache.commons.math3.util.FastMath.abs(r20)
            double r17 = r17 * r4
            double r17 = r17 + r9
            int r11 = (r20 > r17 ? 1 : (r20 == r17 ? 0 : -1))
            if (r11 > 0) goto L_0x0382
            double[] r4 = r0.singularValues
            r4[r13] = r15
            goto L_0x0385
        L_0x0382:
            int r13 = r13 + -1
            goto L_0x034c
        L_0x0385:
            if (r13 != r8) goto L_0x038a
            r4 = 1
            r5 = 3
            goto L_0x0392
        L_0x038a:
            if (r13 != r12) goto L_0x038f
            r4 = 1
            r5 = 1
            goto L_0x0392
        L_0x038f:
            r8 = r13
            r4 = 1
            r5 = 2
        L_0x0392:
            int r8 = r8 + r4
            if (r5 == r4) goto L_0x05f0
            r9 = 2
            if (r5 == r9) goto L_0x058e
            r4 = 3
            if (r5 == r4) goto L_0x041c
            double[] r4 = r0.singularValues
            r5 = r4[r8]
            int r10 = (r5 > r15 ? 1 : (r5 == r15 ? 0 : -1))
            if (r10 > 0) goto L_0x03bf
            r5 = r4[r8]
            int r10 = (r5 > r15 ? 1 : (r5 == r15 ? 0 : -1))
            if (r10 >= 0) goto L_0x03ad
            r5 = r4[r8]
            double r5 = -r5
            goto L_0x03ae
        L_0x03ad:
            r5 = r15
        L_0x03ae:
            r4[r8] = r5
            r4 = 0
        L_0x03b1:
            if (r4 > r2) goto L_0x03bf
            r5 = r14[r4]
            r6 = r14[r4]
            r10 = r6[r8]
            double r10 = -r10
            r5[r8] = r10
            int r4 = r4 + 1
            goto L_0x03b1
        L_0x03bf:
            if (r8 >= r2) goto L_0x0414
            double[] r4 = r0.singularValues
            r5 = r4[r8]
            int r10 = r8 + 1
            r11 = r4[r10]
            int r13 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r13 < 0) goto L_0x03ce
            goto L_0x0414
        L_0x03ce:
            r5 = r4[r8]
            r11 = r4[r10]
            r4[r8] = r11
            r4[r10] = r5
            int r4 = r0.f8049n
            r5 = 1
            int r4 = r4 - r5
            if (r8 >= r4) goto L_0x03f4
            r4 = 0
        L_0x03dd:
            int r5 = r0.f8049n
            if (r4 >= r5) goto L_0x03f4
            r5 = r14[r4]
            r11 = r5[r10]
            r5 = r14[r4]
            r6 = r14[r4]
            r17 = r6[r8]
            r5[r10] = r17
            r5 = r14[r4]
            r5[r8] = r11
            int r4 = r4 + 1
            goto L_0x03dd
        L_0x03f4:
            int r4 = r0.f8048m
            r5 = 1
            int r4 = r4 - r5
            if (r8 >= r4) goto L_0x0412
            r4 = 0
        L_0x03fb:
            int r5 = r0.f8048m
            if (r4 >= r5) goto L_0x0412
            r5 = r3[r4]
            r11 = r5[r10]
            r5 = r3[r4]
            r6 = r3[r4]
            r17 = r6[r8]
            r5[r10] = r17
            r5 = r3[r4]
            r5[r8] = r11
            int r4 = r4 + 1
            goto L_0x03fb
        L_0x0412:
            r8 = r10
            goto L_0x03bf
        L_0x0414:
            int r1 = r1 + -1
            r33 = r2
            r16 = r7
            goto L_0x065c
        L_0x041c:
            double[] r4 = r0.singularValues
            int r5 = r1 + -1
            r10 = r4[r5]
            double r10 = org.apache.commons.math3.util.FastMath.abs(r10)
            double[] r4 = r0.singularValues
            r12 = r4[r6]
            double r12 = org.apache.commons.math3.util.FastMath.abs(r12)
            double r10 = org.apache.commons.math3.util.FastMath.max(r10, r12)
            r12 = r7[r6]
            double r12 = org.apache.commons.math3.util.FastMath.abs(r12)
            double r10 = org.apache.commons.math3.util.FastMath.max(r10, r12)
            double[] r4 = r0.singularValues
            r12 = r4[r8]
            double r12 = org.apache.commons.math3.util.FastMath.abs(r12)
            double r10 = org.apache.commons.math3.util.FastMath.max(r10, r12)
            r12 = r7[r8]
            double r12 = org.apache.commons.math3.util.FastMath.abs(r12)
            double r10 = org.apache.commons.math3.util.FastMath.max(r10, r12)
            double[] r4 = r0.singularValues
            r12 = r4[r5]
            double r12 = r12 / r10
            r17 = r4[r6]
            double r17 = r17 / r10
            r19 = r7[r6]
            double r19 = r19 / r10
            r21 = r4[r8]
            double r21 = r21 / r10
            r23 = r7[r8]
            double r23 = r23 / r10
            double r10 = r17 + r12
            double r17 = r17 - r12
            double r10 = r10 * r17
            double r17 = r19 * r19
            double r10 = r10 + r17
            r17 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r10 = r10 / r17
            double r19 = r19 * r12
            double r19 = r19 * r19
            int r4 = (r10 > r15 ? 1 : (r10 == r15 ? 0 : -1))
            if (r4 != 0) goto L_0x0488
            int r4 = (r19 > r15 ? 1 : (r19 == r15 ? 0 : -1))
            if (r4 == 0) goto L_0x0482
            goto L_0x0488
        L_0x0482:
            r25 = r1
            r33 = r2
            r1 = r15
            goto L_0x049c
        L_0x0488:
            double r17 = r10 * r10
            double r17 = r17 + r19
            r25 = r1
            r33 = r2
            double r1 = org.apache.commons.math3.util.FastMath.sqrt(r17)
            int r4 = (r10 > r15 ? 1 : (r10 == r15 ? 0 : -1))
            if (r4 >= 0) goto L_0x0499
            double r1 = -r1
        L_0x0499:
            double r10 = r10 + r1
            double r1 = r19 / r10
        L_0x049c:
            double r10 = r21 + r12
            double r12 = r21 - r12
            double r10 = r10 * r12
            double r10 = r10 + r1
            double r21 = r21 * r23
            r1 = r8
            r12 = r21
        L_0x04a8:
            if (r1 >= r5) goto L_0x0584
            double r17 = org.apache.commons.math3.util.FastMath.hypot(r10, r12)
            double r10 = r10 / r17
            double r12 = r12 / r17
            if (r1 == r8) goto L_0x04b8
            int r2 = r1 + -1
            r7[r2] = r17
        L_0x04b8:
            double[] r2 = r0.singularValues
            r17 = r2[r1]
            double r17 = r17 * r10
            r19 = r7[r1]
            double r19 = r19 * r12
            r21 = r5
            double r4 = r17 + r19
            r17 = r7[r1]
            double r17 = r17 * r10
            r19 = r2[r1]
            double r19 = r19 * r12
            double r17 = r17 - r19
            r7[r1] = r17
            int r17 = r1 + 1
            r18 = r2[r17]
            r22 = r8
            double r8 = r12 * r18
            r18 = r2[r17]
            double r18 = r18 * r10
            r2[r17] = r18
            r2 = 0
        L_0x04e1:
            int r15 = r0.f8049n
            if (r2 >= r15) goto L_0x0515
            r15 = r14[r2]
            r23 = r15[r1]
            double r23 = r23 * r10
            r15 = r14[r2]
            r26 = r15[r17]
            double r26 = r26 * r12
            double r23 = r23 + r26
            r15 = r14[r2]
            r26 = r6
            r16 = r7
            double r6 = -r12
            r27 = r14[r2]
            r28 = r27[r1]
            double r6 = r6 * r28
            r27 = r14[r2]
            r28 = r27[r17]
            double r28 = r28 * r10
            double r6 = r6 + r28
            r15[r17] = r6
            r6 = r14[r2]
            r6[r1] = r23
            int r2 = r2 + 1
            r7 = r16
            r6 = r26
            goto L_0x04e1
        L_0x0515:
            r26 = r6
            r16 = r7
            double r6 = org.apache.commons.math3.util.FastMath.hypot(r4, r8)
            double r4 = r4 / r6
            double r8 = r8 / r6
            double[] r2 = r0.singularValues
            r2[r1] = r6
            r6 = r16[r1]
            double r6 = r6 * r4
            r10 = r2[r17]
            double r10 = r10 * r8
            double r10 = r10 + r6
            double r6 = -r8
            r12 = r16[r1]
            double r12 = r12 * r6
            r23 = r2[r17]
            double r23 = r23 * r4
            double r12 = r12 + r23
            r2[r17] = r12
            r12 = r16[r17]
            double r12 = r12 * r8
            r23 = r16[r17]
            double r23 = r23 * r4
            r16[r17] = r23
            int r2 = r0.f8048m
            r15 = 1
            int r2 = r2 - r15
            if (r1 >= r2) goto L_0x0575
            r2 = 0
        L_0x054a:
            int r15 = r0.f8048m
            if (r2 >= r15) goto L_0x0575
            r15 = r3[r2]
            r23 = r15[r1]
            double r23 = r23 * r4
            r15 = r3[r2]
            r27 = r15[r17]
            double r27 = r27 * r8
            double r23 = r23 + r27
            r15 = r3[r2]
            r27 = r3[r2]
            r28 = r27[r1]
            double r28 = r28 * r6
            r27 = r3[r2]
            r30 = r27[r17]
            double r30 = r30 * r4
            double r28 = r28 + r30
            r15[r17] = r28
            r15 = r3[r2]
            r15[r1] = r23
            int r2 = r2 + 1
            goto L_0x054a
        L_0x0575:
            r7 = r16
            r1 = r17
            r5 = r21
            r8 = r22
            r6 = r26
            r9 = 2
            r15 = 0
            goto L_0x04a8
        L_0x0584:
            r26 = r6
            r16 = r7
            r16[r26] = r10
            r1 = r25
            goto L_0x065c
        L_0x058e:
            r25 = r1
            r33 = r2
            r16 = r7
            r22 = r8
            int r8 = r22 + -1
            r1 = r16[r8]
            r4 = 0
            r16[r8] = r4
            r4 = r1
            r2 = r22
            r1 = r25
        L_0x05a3:
            if (r2 >= r1) goto L_0x065c
            double[] r6 = r0.singularValues
            r9 = r6[r2]
            double r6 = org.apache.commons.math3.util.FastMath.hypot(r9, r4)
            double[] r9 = r0.singularValues
            r10 = r9[r2]
            double r10 = r10 / r6
            double r4 = r4 / r6
            r9[r2] = r6
            double r6 = -r4
            r12 = r16[r2]
            double r12 = r12 * r6
            r21 = r16[r2]
            double r21 = r21 * r10
            r16[r2] = r21
            r9 = 0
        L_0x05c1:
            int r15 = r0.f8048m
            if (r9 >= r15) goto L_0x05ec
            r15 = r3[r9]
            r21 = r15[r2]
            double r21 = r21 * r10
            r15 = r3[r9]
            r23 = r15[r8]
            double r23 = r23 * r4
            double r21 = r21 + r23
            r15 = r3[r9]
            r17 = r3[r9]
            r23 = r17[r2]
            double r23 = r23 * r6
            r17 = r3[r9]
            r25 = r17[r8]
            double r25 = r25 * r10
            double r23 = r23 + r25
            r15[r8] = r23
            r15 = r3[r9]
            r15[r2] = r21
            int r9 = r9 + 1
            goto L_0x05c1
        L_0x05ec:
            int r2 = r2 + 1
            r4 = r12
            goto L_0x05a3
        L_0x05f0:
            r33 = r2
            r26 = r6
            r16 = r7
            r22 = r8
            r4 = r16[r26]
            r6 = 0
            r16[r26] = r6
            r2 = r26
        L_0x0600:
            if (r2 < r8) goto L_0x065c
            double[] r9 = r0.singularValues
            r10 = r9[r2]
            double r9 = org.apache.commons.math3.util.FastMath.hypot(r10, r4)
            double[] r11 = r0.singularValues
            r12 = r11[r2]
            double r12 = r12 / r9
            double r6 = r4 / r9
            r11[r2] = r9
            if (r2 == r8) goto L_0x0622
            double r4 = -r6
            int r9 = r2 + -1
            r10 = r16[r9]
            double r4 = r4 * r10
            r10 = r16[r9]
            double r10 = r10 * r12
            r16[r9] = r10
        L_0x0622:
            r9 = 0
        L_0x0623:
            int r10 = r0.f8049n
            if (r9 >= r10) goto L_0x0655
            r10 = r14[r9]
            r21 = r10[r2]
            double r21 = r21 * r12
            r10 = r14[r9]
            int r11 = r1 + -1
            r23 = r10[r11]
            double r23 = r23 * r6
            double r21 = r21 + r23
            r10 = r14[r9]
            r23 = r4
            double r4 = -r6
            r15 = r14[r9]
            r25 = r15[r2]
            double r4 = r4 * r25
            r15 = r14[r9]
            r25 = r15[r11]
            double r25 = r25 * r12
            double r4 = r4 + r25
            r10[r11] = r4
            r4 = r14[r9]
            r4[r2] = r21
            int r9 = r9 + 1
            r4 = r23
            goto L_0x0623
        L_0x0655:
            r23 = r4
            int r2 = r2 + -1
            r6 = 0
            goto L_0x0600
        L_0x065c:
            r2 = r33
            r7 = r16
            r15 = 0
            goto L_0x0312
        L_0x0664:
            int r1 = r0.f8048m
            double r1 = (double) r1
            double[] r6 = r0.singularValues
            r7 = 0
            r7 = r6[r7]
            java.lang.Double.isNaN(r1)
            double r1 = r1 * r7
            double r1 = r1 * r4
            double r4 = org.apache.commons.math3.util.Precision.SAFE_MIN
            double r4 = org.apache.commons.math3.util.FastMath.sqrt(r4)
            double r1 = org.apache.commons.math3.util.FastMath.max(r1, r4)
            r0.tol = r1
            boolean r1 = r0.transposed
            if (r1 != 0) goto L_0x0690
            org.apache.commons.math3.linear.RealMatrix r1 = org.apache.commons.math3.linear.MatrixUtils.createRealMatrix(r3)
            r0.cachedU = r1
            org.apache.commons.math3.linear.RealMatrix r1 = org.apache.commons.math3.linear.MatrixUtils.createRealMatrix(r14)
            r0.cachedV = r1
            goto L_0x069c
        L_0x0690:
            org.apache.commons.math3.linear.RealMatrix r1 = org.apache.commons.math3.linear.MatrixUtils.createRealMatrix(r14)
            r0.cachedU = r1
            org.apache.commons.math3.linear.RealMatrix r1 = org.apache.commons.math3.linear.MatrixUtils.createRealMatrix(r3)
            r0.cachedV = r1
        L_0x069c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.linear.SingularValueDecomposition.<init>(org.apache.commons.math3.linear.RealMatrix):void");
    }

    public RealMatrix getU() {
        return this.cachedU;
    }

    public RealMatrix getUT() {
        if (this.cachedUt == null) {
            this.cachedUt = getU().transpose();
        }
        return this.cachedUt;
    }

    public RealMatrix getS() {
        if (this.cachedS == null) {
            this.cachedS = MatrixUtils.createRealDiagonalMatrix(this.singularValues);
        }
        return this.cachedS;
    }

    public double[] getSingularValues() {
        return (double[]) this.singularValues.clone();
    }

    public RealMatrix getV() {
        return this.cachedV;
    }

    public RealMatrix getVT() {
        if (this.cachedVt == null) {
            this.cachedVt = getV().transpose();
        }
        return this.cachedVt;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    public RealMatrix getCovariance(double d) {
        int length = this.singularValues.length;
        int i = 0;
        while (i < length && this.singularValues[i] >= d) {
            i++;
        }
        if (i != 0) {
            final double[][] dArr = (double[][]) Array.newInstance(double.class, i, length);
            getVT().walkInOptimizedOrder(new DefaultRealMatrixPreservingVisitor() {
                /* class org.apache.commons.math3.linear.SingularValueDecomposition.C35611 */

                public void visit(int i, int i2, double d) {
                    dArr[i][i2] = d / SingularValueDecomposition.this.singularValues[i];
                }
            }, 0, i - 1, 0, length - 1);
            Array2DRowRealMatrix array2DRowRealMatrix = new Array2DRowRealMatrix(dArr, false);
            return array2DRowRealMatrix.transpose().multiply(array2DRowRealMatrix);
        }
        throw new NumberIsTooLargeException(LocalizedFormats.TOO_LARGE_CUTOFF_SINGULAR_VALUE, Double.valueOf(d), Double.valueOf(this.singularValues[0]), true);
    }

    public double getNorm() {
        return this.singularValues[0];
    }

    public double getConditionNumber() {
        double[] dArr = this.singularValues;
        return dArr[0] / dArr[this.f8049n - 1];
    }

    public double getInverseConditionNumber() {
        double[] dArr = this.singularValues;
        return dArr[this.f8049n - 1] / dArr[0];
    }

    public int getRank() {
        int i = 0;
        int i2 = 0;
        while (true) {
            double[] dArr = this.singularValues;
            if (i >= dArr.length) {
                return i2;
            }
            if (dArr[i] > this.tol) {
                i2++;
            }
            i++;
        }
    }

    public DecompositionSolver getSolver() {
        return new Solver(this.singularValues, getUT(), getV(), getRank() == this.f8048m, this.tol);
    }

    private static class Solver implements DecompositionSolver {
        private boolean nonSingular;
        private final RealMatrix pseudoInverse;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
         arg types: [double[][], int]
         candidates:
          org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
          org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
        private Solver(double[] dArr, RealMatrix realMatrix, RealMatrix realMatrix2, boolean z, double d) {
            double[][] data = realMatrix.getData();
            for (int i = 0; i < dArr.length; i++) {
                double d2 = dArr[i] > d ? 1.0d / dArr[i] : 0.0d;
                double[] dArr2 = data[i];
                for (int i2 = 0; i2 < dArr2.length; i2++) {
                    dArr2[i2] = dArr2[i2] * d2;
                }
            }
            this.pseudoInverse = realMatrix2.multiply(new Array2DRowRealMatrix(data, false));
            this.nonSingular = z;
        }

        public RealVector solve(RealVector realVector) {
            return this.pseudoInverse.operate(realVector);
        }

        public RealMatrix solve(RealMatrix realMatrix) {
            return this.pseudoInverse.multiply(realMatrix);
        }

        public boolean isNonSingular() {
            return this.nonSingular;
        }

        public RealMatrix getInverse() {
            return this.pseudoInverse;
        }
    }
}
