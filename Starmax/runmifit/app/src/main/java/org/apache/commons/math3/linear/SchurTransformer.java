package org.apache.commons.math3.linear;

import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Precision;

class SchurTransformer {
    private static final int MAX_ITERATIONS = 100;
    private RealMatrix cachedP;
    private RealMatrix cachedPt;
    private RealMatrix cachedT;
    private final double epsilon = Precision.EPSILON;
    private final double[][] matrixP;
    private final double[][] matrixT;

    public SchurTransformer(RealMatrix realMatrix) {
        if (realMatrix.isSquare()) {
            HessenbergTransformer hessenbergTransformer = new HessenbergTransformer(realMatrix);
            this.matrixT = hessenbergTransformer.getH().getData();
            this.matrixP = hessenbergTransformer.getP().getData();
            this.cachedT = null;
            this.cachedP = null;
            this.cachedPt = null;
            transform();
            return;
        }
        throw new NonSquareMatrixException(realMatrix.getRowDimension(), realMatrix.getColumnDimension());
    }

    public RealMatrix getP() {
        if (this.cachedP == null) {
            this.cachedP = MatrixUtils.createRealMatrix(this.matrixP);
        }
        return this.cachedP;
    }

    public RealMatrix getPT() {
        if (this.cachedPt == null) {
            this.cachedPt = getP().transpose();
        }
        return this.cachedPt;
    }

    public RealMatrix getT() {
        if (this.cachedT == null) {
            this.cachedT = MatrixUtils.createRealMatrix(this.matrixT);
        }
        return this.cachedT;
    }

    private void transform() {
        double d;
        int i;
        int length = this.matrixT.length;
        double norm = getNorm();
        ShiftInfo shiftInfo = new ShiftInfo();
        int i2 = length - 1;
        int i3 = i2;
        int i4 = 0;
        while (i3 >= 0) {
            int findSmallSubDiagonalElement = findSmallSubDiagonalElement(i3, norm);
            if (findSmallSubDiagonalElement == i3) {
                double[] dArr = this.matrixT[i3];
                dArr[i3] = dArr[i3] + shiftInfo.exShift;
                i3--;
                d = norm;
            } else {
                int i5 = i3 - 1;
                if (findSmallSubDiagonalElement == i5) {
                    double[][] dArr2 = this.matrixT;
                    double d2 = (dArr2[i5][i5] - dArr2[i3][i3]) / 2.0d;
                    double d3 = (d2 * d2) + (dArr2[i3][i5] * dArr2[i5][i3]);
                    double[] dArr3 = dArr2[i3];
                    d = norm;
                    dArr3[i3] = dArr3[i3] + shiftInfo.exShift;
                    double[] dArr4 = this.matrixT[i5];
                    int i6 = i3;
                    dArr4[i5] = dArr4[i5] + shiftInfo.exShift;
                    if (d3 >= 0.0d) {
                        double sqrt = FastMath.sqrt(FastMath.abs(d3));
                        double d4 = d2 >= 0.0d ? d2 + sqrt : d2 - sqrt;
                        double d5 = this.matrixT[i6][i5];
                        double abs = FastMath.abs(d5) + FastMath.abs(d4);
                        double d6 = d5 / abs;
                        double d7 = d4 / abs;
                        double sqrt2 = FastMath.sqrt((d6 * d6) + (d7 * d7));
                        double d8 = d6 / sqrt2;
                        double d9 = d7 / sqrt2;
                        for (int i7 = i5; i7 < length; i7++) {
                            double[][] dArr5 = this.matrixT;
                            double d10 = dArr5[i5][i7];
                            dArr5[i5][i7] = (d9 * d10) + (dArr5[i6][i7] * d8);
                            dArr5[i6][i7] = (dArr5[i6][i7] * d9) - (d10 * d8);
                        }
                        i = i6;
                        for (int i8 = 0; i8 <= i; i8++) {
                            double[][] dArr6 = this.matrixT;
                            double d11 = dArr6[i8][i5];
                            dArr6[i8][i5] = (d9 * d11) + (dArr6[i8][i] * d8);
                            dArr6[i8][i] = (dArr6[i8][i] * d9) - (d11 * d8);
                        }
                        for (int i9 = 0; i9 <= i2; i9++) {
                            double[][] dArr7 = this.matrixP;
                            double d12 = dArr7[i9][i5];
                            dArr7[i9][i5] = (d9 * d12) + (dArr7[i9][i] * d8);
                            dArr7[i9][i] = (dArr7[i9][i] * d9) - (d12 * d8);
                        }
                    } else {
                        i = i6;
                    }
                    i3 = i - 2;
                } else {
                    d = norm;
                    int i10 = i3;
                    computeShift(findSmallSubDiagonalElement, i10, i4, shiftInfo);
                    int i11 = i4 + 1;
                    if (i11 <= 100) {
                        double[] dArr8 = new double[3];
                        performDoubleQRStep(findSmallSubDiagonalElement, initQRStep(findSmallSubDiagonalElement, i10, shiftInfo, dArr8), i10, shiftInfo, dArr8);
                        i4 = i11;
                        i3 = i10;
                        norm = d;
                    } else {
                        throw new MaxCountExceededException(LocalizedFormats.CONVERGENCE_FAILED, 100, new Object[0]);
                    }
                }
            }
            i4 = 0;
            norm = d;
        }
    }

    private double getNorm() {
        double d = 0.0d;
        for (int i = 0; i < this.matrixT.length; i++) {
            int max = FastMath.max(i - 1, 0);
            while (true) {
                double[][] dArr = this.matrixT;
                if (max >= dArr.length) {
                    break;
                }
                d += FastMath.abs(dArr[i][max]);
                max++;
            }
        }
        return d;
    }

    private int findSmallSubDiagonalElement(int i, double d) {
        while (i > 0) {
            int i2 = i - 1;
            double abs = FastMath.abs(this.matrixT[i2][i2]) + FastMath.abs(this.matrixT[i][i]);
            if (abs == 0.0d) {
                abs = d;
            }
            if (FastMath.abs(this.matrixT[i][i2]) < this.epsilon * abs) {
                break;
            }
            i--;
        }
        return i;
    }

    private void computeShift(int i, int i2, int i3, ShiftInfo shiftInfo) {
        int i4 = i2;
        int i5 = i3;
        ShiftInfo shiftInfo2 = shiftInfo;
        double[][] dArr = this.matrixT;
        shiftInfo2.f8046x = dArr[i4][i4];
        shiftInfo2.f8045w = 0.0d;
        shiftInfo2.f8047y = 0.0d;
        if (i < i4) {
            int i6 = i4 - 1;
            shiftInfo2.f8047y = dArr[i6][i6];
            shiftInfo2.f8045w = dArr[i4][i6] * dArr[i6][i4];
        }
        if (i5 == 10) {
            shiftInfo2.exShift += shiftInfo2.f8046x;
            for (int i7 = 0; i7 <= i4; i7++) {
                double[] dArr2 = this.matrixT[i7];
                dArr2[i7] = dArr2[i7] - shiftInfo2.f8046x;
            }
            int i8 = i4 - 1;
            double abs = FastMath.abs(this.matrixT[i4][i8]) + FastMath.abs(this.matrixT[i8][i4 - 2]);
            double d = 0.75d * abs;
            shiftInfo2.f8046x = d;
            shiftInfo2.f8047y = d;
            shiftInfo2.f8045w = -0.4375d * abs * abs;
        }
        if (i5 == 30) {
            double d2 = (shiftInfo2.f8047y - shiftInfo2.f8046x) / 2.0d;
            double d3 = (d2 * d2) + shiftInfo2.f8045w;
            if (d3 > 0.0d) {
                double sqrt = FastMath.sqrt(d3);
                if (shiftInfo2.f8047y < shiftInfo2.f8046x) {
                    sqrt = -sqrt;
                }
                double d4 = shiftInfo2.f8046x - (shiftInfo2.f8045w / (((shiftInfo2.f8047y - shiftInfo2.f8046x) / 2.0d) + sqrt));
                for (int i9 = 0; i9 <= i4; i9++) {
                    double[] dArr3 = this.matrixT[i9];
                    dArr3[i9] = dArr3[i9] - d4;
                }
                shiftInfo2.exShift += d4;
                shiftInfo2.f8045w = 0.964d;
                shiftInfo2.f8047y = 0.964d;
                shiftInfo2.f8046x = 0.964d;
            }
        }
    }

    private int initQRStep(int i, int i2, ShiftInfo shiftInfo, double[] dArr) {
        int i3 = i;
        ShiftInfo shiftInfo2 = shiftInfo;
        int i4 = i2 - 2;
        while (i4 >= i3) {
            double d = this.matrixT[i4][i4];
            double d2 = shiftInfo2.f8046x - d;
            double d3 = shiftInfo2.f8047y - d;
            double d4 = (d2 * d3) - shiftInfo2.f8045w;
            double[][] dArr2 = this.matrixT;
            int i5 = i4 + 1;
            dArr[0] = (d4 / dArr2[i5][i4]) + dArr2[i4][i5];
            dArr[1] = ((dArr2[i5][i5] - d) - d2) - d3;
            dArr[2] = dArr2[i4 + 2][i5];
            if (i4 == i3) {
                break;
            }
            int i6 = i4 - 1;
            if (FastMath.abs(dArr2[i4][i6]) * (FastMath.abs(dArr[1]) + FastMath.abs(dArr[2])) < this.epsilon * FastMath.abs(dArr[0]) * (FastMath.abs(this.matrixT[i6][i6]) + FastMath.abs(d) + FastMath.abs(this.matrixT[i5][i5]))) {
                break;
            }
            i4--;
        }
        return i4;
    }

    private void performDoubleQRStep(int i, int i2, int i3, ShiftInfo shiftInfo, double[] dArr) {
        int i4;
        double d;
        double d2;
        double d3;
        int i5 = i2;
        int i6 = i3;
        ShiftInfo shiftInfo2 = shiftInfo;
        int length = this.matrixT.length;
        double d4 = dArr[0];
        double d5 = dArr[1];
        double d6 = dArr[2];
        double d7 = d5;
        double d8 = d4;
        int i7 = i5;
        while (true) {
            int i8 = i6 - 1;
            if (i7 > i8) {
                break;
            }
            boolean z = i7 != i8;
            if (i7 != i5) {
                double[][] dArr2 = this.matrixT;
                int i9 = i7 - 1;
                double d9 = dArr2[i7][i9];
                double d10 = dArr2[i7 + 1][i9];
                double d11 = z ? dArr2[i7 + 2][i9] : 0.0d;
                shiftInfo2.f8046x = FastMath.abs(d9) + FastMath.abs(d10) + FastMath.abs(d11);
                if (Precision.equals(shiftInfo2.f8046x, 0.0d, this.epsilon)) {
                    i4 = i7;
                    d8 = d9;
                    d7 = d10;
                    d6 = d11;
                    i7 = i4 + 1;
                    i5 = i2;
                    i6 = i3;
                } else {
                    d8 = d9 / shiftInfo2.f8046x;
                    d7 = d10 / shiftInfo2.f8046x;
                    d6 = d11 / shiftInfo2.f8046x;
                }
            }
            int i10 = i7;
            double sqrt = FastMath.sqrt((d8 * d8) + (d7 * d7) + (d6 * d6));
            if (d8 < 0.0d) {
                sqrt = -sqrt;
            }
            if (sqrt != 0.0d) {
                i4 = i10;
                if (i4 != i5) {
                    d2 = d6;
                    d = d7;
                    this.matrixT[i4][i4 - 1] = (-sqrt) * shiftInfo2.f8046x;
                } else {
                    d = d7;
                    d2 = d6;
                    if (i != i5) {
                        double[][] dArr3 = this.matrixT;
                        int i11 = i4 - 1;
                        dArr3[i4][i11] = -dArr3[i4][i11];
                    }
                }
                double d12 = d8 + sqrt;
                shiftInfo2.f8046x = d12 / sqrt;
                shiftInfo2.f8047y = d / sqrt;
                double d13 = d2 / sqrt;
                double d14 = d / d12;
                double d15 = d2 / d12;
                int i12 = i4;
                while (i12 < length) {
                    double[][] dArr4 = this.matrixT;
                    int i13 = i4 + 1;
                    double d16 = dArr4[i4][i12] + (dArr4[i13][i12] * d14);
                    if (z) {
                        int i14 = i4 + 2;
                        d16 += dArr4[i14][i12] * d15;
                        double[] dArr5 = dArr4[i14];
                        dArr5[i12] = dArr5[i12] - (d16 * d13);
                    }
                    double[] dArr6 = this.matrixT[i4];
                    dArr6[i12] = dArr6[i12] - (shiftInfo2.f8046x * d16);
                    double[] dArr7 = this.matrixT[i13];
                    dArr7[i12] = dArr7[i12] - (shiftInfo2.f8047y * d16);
                    i12++;
                    d12 = d16;
                    d14 = d14;
                }
                double d17 = d14;
                int i15 = 0;
                while (true) {
                    if (i15 > FastMath.min(i3, i4 + 3)) {
                        break;
                    }
                    double d18 = shiftInfo2.f8047y;
                    double[][] dArr8 = this.matrixT;
                    int i16 = i4 + 1;
                    d3 = (shiftInfo2.f8046x * this.matrixT[i15][i4]) + (d18 * dArr8[i15][i16]);
                    if (z) {
                        int i17 = i4 + 2;
                        d3 += dArr8[i15][i17] * d13;
                        double[] dArr9 = dArr8[i15];
                        dArr9[i17] = dArr9[i17] - (d3 * d15);
                    }
                    double[][] dArr10 = this.matrixT;
                    double[] dArr11 = dArr10[i15];
                    dArr11[i4] = dArr11[i4] - d3;
                    double[] dArr12 = dArr10[i15];
                    dArr12[i16] = dArr12[i16] - (d3 * d17);
                    i15++;
                }
                int length2 = this.matrixT.length - 1;
                double d19 = d3;
                int i18 = 0;
                while (i18 <= length2) {
                    boolean z2 = z;
                    double d20 = shiftInfo2.f8047y;
                    double[][] dArr13 = this.matrixP;
                    int i19 = i4 + 1;
                    d19 = (shiftInfo2.f8046x * this.matrixP[i18][i4]) + (d20 * dArr13[i18][i19]);
                    if (z2) {
                        int i20 = i4 + 2;
                        d19 += dArr13[i18][i20] * d13;
                        double[] dArr14 = dArr13[i18];
                        dArr14[i20] = dArr14[i20] - (d19 * d15);
                    }
                    double[][] dArr15 = this.matrixP;
                    double[] dArr16 = dArr15[i18];
                    dArr16[i4] = dArr16[i4] - d19;
                    double[] dArr17 = dArr15[i18];
                    dArr17[i19] = dArr17[i19] - (d19 * d17);
                    i18++;
                    z = z2;
                }
                d6 = d15;
                d8 = d19;
                d7 = d17;
            } else {
                i4 = i10;
            }
            i7 = i4 + 1;
            i5 = i2;
            i6 = i3;
        }
        int i21 = i2 + 2;
        int i22 = i3;
        for (int i23 = i21; i23 <= i22; i23++) {
            double[][] dArr18 = this.matrixT;
            dArr18[i23][i23 - 2] = 0.0d;
            if (i23 > i21) {
                dArr18[i23][i23 - 3] = 0.0d;
            }
        }
    }

    private static class ShiftInfo {
        double exShift;

        /* renamed from: w */
        double f8045w;

        /* renamed from: x */
        double f8046x;

        /* renamed from: y */
        double f8047y;

        private ShiftInfo() {
        }
    }
}
