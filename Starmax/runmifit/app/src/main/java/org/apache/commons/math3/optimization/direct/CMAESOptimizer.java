package org.apache.commons.math3.optimization.direct;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NotPositiveException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.optimization.ConvergenceChecker;
import org.apache.commons.math3.optimization.GoalType;
import org.apache.commons.math3.optimization.MultivariateOptimizer;
import org.apache.commons.math3.optimization.OptimizationData;
import org.apache.commons.math3.optimization.PointValuePair;
import org.apache.commons.math3.optimization.SimpleValueChecker;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;

@Deprecated
public class CMAESOptimizer extends BaseAbstractMultivariateSimpleBoundsOptimizer<MultivariateFunction> implements MultivariateOptimizer {
    public static final int DEFAULT_CHECKFEASABLECOUNT = 0;
    public static final int DEFAULT_DIAGONALONLY = 0;
    public static final boolean DEFAULT_ISACTIVECMA = true;
    public static final int DEFAULT_MAXITERATIONS = 30000;
    public static final RandomGenerator DEFAULT_RANDOMGENERATOR = new MersenneTwister();
    public static final double DEFAULT_STOPFITNESS = 0.0d;

    /* renamed from: B */
    private RealMatrix f8114B;

    /* renamed from: BD */
    private RealMatrix f8115BD;

    /* renamed from: C */
    private RealMatrix f8116C;

    /* renamed from: D */
    private RealMatrix f8117D;

    /* renamed from: cc */
    private double f8118cc;
    private double ccov1;
    private double ccov1Sep;
    private double ccovmu;
    private double ccovmuSep;
    private int checkFeasableCount;
    private double chiN;

    /* renamed from: cs */
    private double f8119cs;
    private double damps;
    private RealMatrix diagC;
    private RealMatrix diagD;
    private int diagonalOnly;
    private int dimension;
    private double[] fitnessHistory;
    private boolean generateStatistics;
    private int historySize;
    private double[] inputSigma;
    private boolean isActiveCMA;
    /* access modifiers changed from: private */
    public boolean isMinimize;
    private int iterations;
    private int lambda;
    private double logMu2;
    private int maxIterations;

    /* renamed from: mu */
    private int f8120mu;
    private double mueff;
    private double normps;

    /* renamed from: pc */
    private RealMatrix f8121pc;

    /* renamed from: ps */
    private RealMatrix f8122ps;
    private RandomGenerator random;
    private double sigma;
    private List<RealMatrix> statisticsDHistory;
    private List<Double> statisticsFitnessHistory;
    private List<RealMatrix> statisticsMeanHistory;
    private List<Double> statisticsSigmaHistory;
    private double stopFitness;
    private double stopTolFun;
    private double stopTolHistFun;
    private double stopTolUpX;
    private double stopTolX;
    private RealMatrix weights;
    private RealMatrix xmean;

    @Deprecated
    public CMAESOptimizer() {
        this(0);
    }

    @Deprecated
    public CMAESOptimizer(int i) {
        this(i, null, 30000, 0.0d, true, 0, 0, DEFAULT_RANDOMGENERATOR, false, null);
    }

    @Deprecated
    public CMAESOptimizer(int i, double[] dArr) {
        this(i, dArr, 30000, 0.0d, true, 0, 0, DEFAULT_RANDOMGENERATOR, false);
    }

    @Deprecated
    public CMAESOptimizer(int i, double[] dArr, int i2, double d, boolean z, int i3, int i4, RandomGenerator randomGenerator, boolean z2) {
        this(i, dArr, i2, d, z, i3, i4, randomGenerator, z2, new SimpleValueChecker());
    }

    @Deprecated
    public CMAESOptimizer(int i, double[] dArr, int i2, double d, boolean z, int i3, int i4, RandomGenerator randomGenerator, boolean z2, ConvergenceChecker<PointValuePair> convergenceChecker) {
        super(convergenceChecker);
        double[] dArr2;
        this.diagonalOnly = 0;
        this.isMinimize = true;
        this.generateStatistics = false;
        this.statisticsSigmaHistory = new ArrayList();
        this.statisticsMeanHistory = new ArrayList();
        this.statisticsFitnessHistory = new ArrayList();
        this.statisticsDHistory = new ArrayList();
        this.lambda = i;
        if (dArr == null) {
            dArr2 = null;
        } else {
            dArr2 = (double[]) dArr.clone();
        }
        this.inputSigma = dArr2;
        this.maxIterations = i2;
        this.stopFitness = d;
        this.isActiveCMA = z;
        this.diagonalOnly = i3;
        this.checkFeasableCount = i4;
        this.random = randomGenerator;
        this.generateStatistics = z2;
    }

    public CMAESOptimizer(int i, double d, boolean z, int i2, int i3, RandomGenerator randomGenerator, boolean z2, ConvergenceChecker<PointValuePair> convergenceChecker) {
        super(convergenceChecker);
        this.diagonalOnly = 0;
        this.isMinimize = true;
        this.generateStatistics = false;
        this.statisticsSigmaHistory = new ArrayList();
        this.statisticsMeanHistory = new ArrayList();
        this.statisticsFitnessHistory = new ArrayList();
        this.statisticsDHistory = new ArrayList();
        this.maxIterations = i;
        this.stopFitness = d;
        this.isActiveCMA = z;
        this.diagonalOnly = i2;
        this.checkFeasableCount = i3;
        this.random = randomGenerator;
        this.generateStatistics = z2;
    }

    public List<Double> getStatisticsSigmaHistory() {
        return this.statisticsSigmaHistory;
    }

    public List<RealMatrix> getStatisticsMeanHistory() {
        return this.statisticsMeanHistory;
    }

    public List<Double> getStatisticsFitnessHistory() {
        return this.statisticsFitnessHistory;
    }

    public List<RealMatrix> getStatisticsDHistory() {
        return this.statisticsDHistory;
    }

    public static class Sigma implements OptimizationData {
        private final double[] sigma;

        public Sigma(double[] dArr) throws NotPositiveException {
            int i = 0;
            while (i < dArr.length) {
                if (dArr[i] >= 0.0d) {
                    i++;
                } else {
                    throw new NotPositiveException(Double.valueOf(dArr[i]));
                }
            }
            this.sigma = (double[]) dArr.clone();
        }

        public double[] getSigma() {
            return (double[]) this.sigma.clone();
        }
    }

    public static class PopulationSize implements OptimizationData {
        private final int lambda;

        public PopulationSize(int i) throws NotStrictlyPositiveException {
            if (i > 0) {
                this.lambda = i;
                return;
            }
            throw new NotStrictlyPositiveException(Integer.valueOf(i));
        }

        public int getPopulationSize() {
            return this.lambda;
        }
    }

    /* access modifiers changed from: protected */
    public PointValuePair optimizeInternal(int i, MultivariateFunction multivariateFunction, GoalType goalType, OptimizationData... optimizationDataArr) {
        parseOptimizationData(optimizationDataArr);
        return super.optimizeInternal(i, multivariateFunction, goalType, optimizationDataArr);
    }

    /* access modifiers changed from: protected */
    public PointValuePair doOptimize() {
        PointValuePair pointValuePair;
        RealMatrix realMatrix;
        PointValuePair pointValuePair2;
        double d;
        double[] dArr;
        double d2;
        PointValuePair pointValuePair3;
        PointValuePair pointValuePair4;
        checkParameters();
        this.isMinimize = getGoalType().equals(GoalType.MINIMIZE);
        FitnessFunction fitnessFunction = new FitnessFunction();
        double[] startPoint = getStartPoint();
        this.dimension = startPoint.length;
        initializeCMA(startPoint);
        int i = 0;
        this.iterations = 0;
        double value = fitnessFunction.value(startPoint);
        push(this.fitnessHistory, value);
        PointValuePair pointValuePair5 = new PointValuePair(getStartPoint(), this.isMinimize ? value : -value);
        int i2 = 1;
        this.iterations = 1;
        double d3 = value;
        PointValuePair pointValuePair6 = pointValuePair5;
        PointValuePair pointValuePair7 = null;
        while (this.iterations <= this.maxIterations) {
            RealMatrix randn1 = randn1(this.dimension, this.lambda);
            RealMatrix zeros = zeros(this.dimension, this.lambda);
            double[] dArr2 = new double[this.lambda];
            int i3 = 0;
            while (i3 < this.lambda) {
                int i4 = 0;
                RealMatrix realMatrix2 = null;
                while (i4 < this.checkFeasableCount + i2) {
                    if (this.diagonalOnly <= 0) {
                        realMatrix2 = this.xmean.add(this.f8115BD.multiply(randn1.getColumnMatrix(i3)).scalarMultiply(this.sigma));
                    } else {
                        realMatrix2 = this.xmean.add(times(this.diagD, randn1.getColumnMatrix(i3)).scalarMultiply(this.sigma));
                    }
                    if (i4 >= this.checkFeasableCount || fitnessFunction.isFeasible(realMatrix2.getColumn(i))) {
                        break;
                    }
                    randn1.setColumn(i3, randn(this.dimension));
                    i4++;
                    i2 = 1;
                }
                copyColumn(realMatrix2, i, zeros, i3);
                try {
                    dArr2[i3] = fitnessFunction.value(zeros.getColumn(i3));
                    i3++;
                    i2 = 1;
                } catch (TooManyEvaluationsException unused) {
                }
            }
            int[] sortedIndices = sortedIndices(dArr2);
            RealMatrix realMatrix3 = this.xmean;
            RealMatrix selectColumns = selectColumns(zeros, MathArrays.copyOf(sortedIndices, this.f8120mu));
            this.xmean = selectColumns.multiply(this.weights);
            RealMatrix selectColumns2 = selectColumns(randn1, MathArrays.copyOf(sortedIndices, this.f8120mu));
            boolean updateEvolutionPaths = updateEvolutionPaths(selectColumns2.multiply(this.weights), realMatrix3);
            if (this.diagonalOnly <= 0) {
                updateCovariance(updateEvolutionPaths, selectColumns, randn1, sortedIndices, realMatrix3);
            } else {
                updateCovarianceDiagonalOnly(updateEvolutionPaths, selectColumns2);
            }
            int[] iArr = sortedIndices;
            this.sigma *= FastMath.exp(FastMath.min(1.0d, (((this.normps / this.chiN) - 1.0d) * this.f8119cs) / this.damps));
            double d4 = dArr2[iArr[0]];
            int[] iArr2 = iArr;
            double d5 = dArr2[iArr2[iArr2.length - 1]];
            if (d3 > d4) {
                PointValuePair pointValuePair8 = new PointValuePair(fitnessFunction.repair(selectColumns.getColumn(0)), this.isMinimize ? d4 : -d4);
                if (getConvergenceChecker() != null && getConvergenceChecker().converged(this.iterations, pointValuePair8, pointValuePair6)) {
                    return pointValuePair8;
                }
                d3 = d4;
                pointValuePair = pointValuePair8;
            } else {
                pointValuePair = pointValuePair6;
                pointValuePair6 = pointValuePair7;
            }
            double d6 = this.stopFitness;
            if (d6 != 0.0d) {
                if (!this.isMinimize) {
                    d6 = -d6;
                }
                if (d4 < d6) {
                    return pointValuePair;
                }
            }
            double[] column = sqrt(this.diagC).getColumn(0);
            double[] column2 = this.f8121pc.getColumn(0);
            PointValuePair pointValuePair9 = pointValuePair;
            int i5 = 0;
            while (true) {
                if (i5 >= this.dimension) {
                    realMatrix = selectColumns;
                    pointValuePair2 = pointValuePair6;
                    d = d3;
                    dArr = dArr2;
                    break;
                }
                d = d3;
                double[] dArr3 = column2;
                dArr = dArr2;
                realMatrix = selectColumns;
                pointValuePair2 = pointValuePair6;
                if (this.sigma * FastMath.max(FastMath.abs(column2[i5]), column[i5]) > this.stopTolX) {
                    break;
                } else if (i5 >= this.dimension - 1) {
                    return pointValuePair9;
                } else {
                    i5++;
                    dArr2 = dArr;
                    d3 = d;
                    selectColumns = realMatrix;
                    pointValuePair6 = pointValuePair2;
                    column2 = dArr3;
                }
            }
            for (int i6 = 0; i6 < this.dimension; i6++) {
                if (this.sigma * column[i6] > this.stopTolUpX) {
                    return pointValuePair9;
                }
            }
            double min = min(this.fitnessHistory);
            double max = max(this.fitnessHistory);
            if (this.iterations > 2) {
                d2 = d5;
                if (FastMath.max(max, d5) - FastMath.min(min, d4) < this.stopTolFun) {
                    return pointValuePair9;
                }
            } else {
                d2 = d5;
            }
            if ((this.iterations > this.fitnessHistory.length && max - min < this.stopTolHistFun) || max(this.diagD) / min(this.diagD) > 1.0E7d) {
                return pointValuePair9;
            }
            if (getConvergenceChecker() != null) {
                PointValuePair pointValuePair10 = new PointValuePair(realMatrix.getColumn(0), this.isMinimize ? d4 : -d4);
                if (pointValuePair2 != null && getConvergenceChecker().converged(this.iterations, pointValuePair10, pointValuePair2)) {
                    return pointValuePair9;
                }
                pointValuePair3 = pointValuePair10;
            } else {
                pointValuePair3 = pointValuePair2;
            }
            double d7 = (double) this.lambda;
            Double.isNaN(d7);
            if (d == dArr[iArr2[(int) ((d7 / 4.0d) + 0.1d)]]) {
                pointValuePair4 = pointValuePair3;
                this.sigma *= FastMath.exp((this.f8119cs / this.damps) + 0.2d);
            } else {
                pointValuePair4 = pointValuePair3;
            }
            if (this.iterations > 2 && FastMath.max(max, d4) - FastMath.min(min, d4) == 0.0d) {
                this.sigma *= FastMath.exp((this.f8119cs / this.damps) + 0.2d);
            }
            push(this.fitnessHistory, d4);
            fitnessFunction.setValueRange(d2 - d4);
            if (this.generateStatistics) {
                this.statisticsSigmaHistory.add(Double.valueOf(this.sigma));
                this.statisticsFitnessHistory.add(Double.valueOf(d4));
                this.statisticsMeanHistory.add(this.xmean.transpose());
                this.statisticsDHistory.add(this.diagD.transpose().scalarMultiply(100000.0d));
            }
            this.iterations++;
            pointValuePair7 = pointValuePair4;
            pointValuePair6 = pointValuePair9;
            d3 = d;
            i = 0;
            i2 = 1;
        }
        return pointValuePair6;
    }

    private void parseOptimizationData(OptimizationData... optimizationDataArr) {
        for (OptimizationData optimizationData : optimizationDataArr) {
            if (optimizationData instanceof Sigma) {
                this.inputSigma = ((Sigma) optimizationData).getSigma();
            } else if (optimizationData instanceof PopulationSize) {
                this.lambda = ((PopulationSize) optimizationData).getPopulationSize();
            }
        }
    }

    private void checkParameters() {
        double[] startPoint = getStartPoint();
        double[] lowerBound = getLowerBound();
        double[] upperBound = getUpperBound();
        double[] dArr = this.inputSigma;
        if (dArr == null) {
            return;
        }
        if (dArr.length == startPoint.length) {
            int i = 0;
            while (i < startPoint.length) {
                double[] dArr2 = this.inputSigma;
                if (dArr2[i] < 0.0d) {
                    throw new NotPositiveException(Double.valueOf(dArr2[i]));
                } else if (dArr2[i] <= upperBound[i] - lowerBound[i]) {
                    i++;
                } else {
                    throw new OutOfRangeException(Double.valueOf(dArr2[i]), 0, Double.valueOf(upperBound[i] - lowerBound[i]));
                }
            }
            return;
        }
        throw new DimensionMismatchException(dArr.length, startPoint.length);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private void initializeCMA(double[] dArr) {
        double[] dArr2 = dArr;
        if (this.lambda <= 0) {
            this.lambda = ((int) (FastMath.log((double) this.dimension) * 3.0d)) + 4;
        }
        double[][] dArr3 = (double[][]) Array.newInstance(double.class, dArr2.length, 1);
        for (int i = 0; i < dArr2.length; i++) {
            double[] dArr4 = dArr3[i];
            double[] dArr5 = this.inputSigma;
            dArr4[0] = dArr5 == null ? 0.3d : dArr5[i];
        }
        Array2DRowRealMatrix array2DRowRealMatrix = new Array2DRowRealMatrix(dArr3, false);
        this.sigma = max(array2DRowRealMatrix);
        this.stopTolUpX = max(array2DRowRealMatrix) * 1000.0d;
        this.stopTolX = max(array2DRowRealMatrix) * 1.0E-11d;
        this.stopTolFun = 1.0E-12d;
        this.stopTolHistFun = 1.0E-13d;
        this.f8120mu = this.lambda / 2;
        double d = (double) this.f8120mu;
        Double.isNaN(d);
        this.logMu2 = FastMath.log(d + 0.5d);
        this.weights = log(sequence(1.0d, (double) this.f8120mu, 1.0d)).scalarMultiply(-1.0d).scalarAdd(this.logMu2);
        double d2 = 0.0d;
        double d3 = 0.0d;
        for (int i2 = 0; i2 < this.f8120mu; i2++) {
            double entry = this.weights.getEntry(i2, 0);
            d2 += entry;
            d3 += entry * entry;
        }
        Array2DRowRealMatrix array2DRowRealMatrix2 = array2DRowRealMatrix;
        this.weights = this.weights.scalarMultiply(1.0d / d2);
        this.mueff = (d2 * d2) / d3;
        double d4 = this.mueff;
        int i3 = this.dimension;
        double d5 = (double) i3;
        Double.isNaN(d5);
        double d6 = (double) (i3 + 4);
        double d7 = (double) i3;
        Double.isNaN(d7);
        Double.isNaN(d6);
        this.f8118cc = ((d4 / d5) + 4.0d) / (d6 + ((d4 * 2.0d) / d7));
        double d8 = (double) i3;
        Double.isNaN(d8);
        this.f8119cs = (d4 + 2.0d) / ((d8 + d4) + 3.0d);
        double d9 = (double) (i3 + 1);
        Double.isNaN(d9);
        double d10 = (double) this.dimension;
        double d11 = (double) this.maxIterations;
        Double.isNaN(d11);
        Double.isNaN(d10);
        this.damps = (((FastMath.max(0.0d, FastMath.sqrt((d4 - 1.0d) / d9) - 1.0d) * 2.0d) + 1.0d) * FastMath.max(0.3d, 1.0d - (d10 / (d11 + 1.0E-6d)))) + this.f8119cs;
        int i4 = this.dimension;
        double d12 = (double) i4;
        Double.isNaN(d12);
        double d13 = (double) i4;
        Double.isNaN(d13);
        double d14 = this.mueff;
        this.ccov1 = 2.0d / (((d12 + 1.3d) * (d13 + 1.3d)) + d14);
        double d15 = (double) ((i4 + 2) * (i4 + 2));
        Double.isNaN(d15);
        this.ccovmu = FastMath.min(1.0d - this.ccov1, (((d14 - 2.0d) + (1.0d / d14)) * 2.0d) / (d15 + d14));
        double d16 = this.ccov1;
        double d17 = (double) this.dimension;
        Double.isNaN(d17);
        this.ccov1Sep = FastMath.min(1.0d, (d16 * (d17 + 1.5d)) / 3.0d);
        double d18 = this.ccovmu;
        double d19 = (double) this.dimension;
        Double.isNaN(d19);
        this.ccovmuSep = FastMath.min(1.0d - this.ccov1, (d18 * (d19 + 1.5d)) / 3.0d);
        double sqrt = FastMath.sqrt((double) this.dimension);
        int i5 = this.dimension;
        double d20 = (double) i5;
        Double.isNaN(d20);
        double d21 = (double) i5;
        Double.isNaN(d21);
        double d22 = (double) i5;
        Double.isNaN(d22);
        this.chiN = sqrt * ((1.0d - (1.0d / (d20 * 4.0d))) + (1.0d / ((d21 * 21.0d) * d22)));
        this.xmean = MatrixUtils.createColumnRealMatrix(dArr);
        this.diagD = array2DRowRealMatrix2.scalarMultiply(1.0d / this.sigma);
        this.diagC = square(this.diagD);
        this.f8121pc = zeros(this.dimension, 1);
        this.f8122ps = zeros(this.dimension, 1);
        this.normps = this.f8122ps.getFrobeniusNorm();
        int i6 = this.dimension;
        this.f8114B = eye(i6, i6);
        this.f8117D = ones(this.dimension, 1);
        this.f8115BD = times(this.f8114B, repmat(this.diagD.transpose(), this.dimension, 1));
        this.f8116C = this.f8114B.multiply(diag(square(this.f8117D)).multiply(this.f8114B.transpose()));
        double d23 = (double) (this.dimension * 30);
        double d24 = (double) this.lambda;
        Double.isNaN(d23);
        Double.isNaN(d24);
        this.historySize = ((int) (d23 / d24)) + 10;
        this.fitnessHistory = new double[this.historySize];
        for (int i7 = 0; i7 < this.historySize; i7++) {
            this.fitnessHistory[i7] = Double.MAX_VALUE;
        }
    }

    private boolean updateEvolutionPaths(RealMatrix realMatrix, RealMatrix realMatrix2) {
        RealMatrix scalarMultiply = this.f8122ps.scalarMultiply(1.0d - this.f8119cs);
        RealMatrix multiply = this.f8114B.multiply(realMatrix);
        double d = this.f8119cs;
        this.f8122ps = scalarMultiply.add(multiply.scalarMultiply(FastMath.sqrt(d * (2.0d - d) * this.mueff)));
        this.normps = this.f8122ps.getFrobeniusNorm();
        double sqrt = (this.normps / FastMath.sqrt(1.0d - FastMath.pow(1.0d - this.f8119cs, this.iterations * 2))) / this.chiN;
        double d2 = (double) this.dimension;
        Double.isNaN(d2);
        boolean z = sqrt < (2.0d / (d2 + 1.0d)) + 1.4d;
        this.f8121pc = this.f8121pc.scalarMultiply(1.0d - this.f8118cc);
        if (z) {
            RealMatrix realMatrix3 = this.f8121pc;
            RealMatrix subtract = this.xmean.subtract(realMatrix2);
            double d3 = this.f8118cc;
            this.f8121pc = realMatrix3.add(subtract.scalarMultiply(FastMath.sqrt((d3 * (2.0d - d3)) * this.mueff) / this.sigma));
        }
        return z;
    }

    private void updateCovarianceDiagonalOnly(boolean z, RealMatrix realMatrix) {
        double d;
        if (z) {
            d = 0.0d;
        } else {
            double d2 = this.ccov1Sep;
            double d3 = this.f8118cc;
            d = d2 * d3 * (2.0d - d3);
        }
        this.diagC = this.diagC.scalarMultiply(d + ((1.0d - this.ccov1Sep) - this.ccovmuSep)).add(square(this.f8121pc).scalarMultiply(this.ccov1Sep)).add(times(this.diagC, square(realMatrix).multiply(this.weights)).scalarMultiply(this.ccovmuSep));
        this.diagD = sqrt(this.diagC);
        int i = this.diagonalOnly;
        if (i > 1 && this.iterations > i) {
            this.diagonalOnly = 0;
            int i2 = this.dimension;
            this.f8114B = eye(i2, i2);
            this.f8115BD = diag(this.diagD);
            this.f8116C = diag(this.diagC);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.util.FastMath.pow(double, double):double
     arg types: [double, int]
     candidates:
      org.apache.commons.math3.util.FastMath.pow(double, int):double
      org.apache.commons.math3.util.FastMath.pow(double, double):double */
    private void updateCovariance(boolean z, RealMatrix realMatrix, RealMatrix realMatrix2, int[] iArr, RealMatrix realMatrix3) {
        double d;
        double d2;
        if (this.ccov1 + this.ccovmu > 0.0d) {
            RealMatrix scalarMultiply = realMatrix.subtract(repmat(realMatrix3, 1, this.f8120mu)).scalarMultiply(1.0d / this.sigma);
            RealMatrix realMatrix4 = this.f8121pc;
            RealMatrix scalarMultiply2 = realMatrix4.multiply(realMatrix4.transpose()).scalarMultiply(this.ccov1);
            if (z) {
                d2 = 0.0d;
            } else {
                double d3 = this.ccov1;
                double d4 = this.f8118cc;
                d2 = d3 * d4 * (2.0d - d4);
            }
            double d5 = this.ccovmu;
            double d6 = d2 + ((1.0d - this.ccov1) - d5);
            if (this.isActiveCMA) {
                d = (((1.0d - d5) * 0.25d) * this.mueff) / (FastMath.pow((double) (this.dimension + 2), 1.5d) + (this.mueff * 2.0d));
                RealMatrix selectColumns = selectColumns(realMatrix2, MathArrays.copyOf(reverse(iArr), this.f8120mu));
                RealMatrix sqrt = sqrt(sumRows(square(selectColumns)));
                int[] sortedIndices = sortedIndices(sqrt.getRow(0));
                RealMatrix selectColumns2 = selectColumns(divide(selectColumns(sqrt, reverse(sortedIndices)), selectColumns(sqrt, sortedIndices)), inverse(sortedIndices));
                double entry = 0.33999999999999997d / square(selectColumns2).multiply(this.weights).getEntry(0, 0);
                if (d > entry) {
                    d = entry;
                }
                RealMatrix multiply = this.f8115BD.multiply(times(selectColumns, repmat(selectColumns2, this.dimension, 1)));
                double d7 = 0.5d * d;
                this.f8116C = this.f8116C.scalarMultiply(d6 + d7).add(scalarMultiply2).add(scalarMultiply.scalarMultiply(this.ccovmu + d7).multiply(times(repmat(this.weights, 1, this.dimension), scalarMultiply.transpose()))).subtract(multiply.multiply(diag(this.weights)).multiply(multiply.transpose()).scalarMultiply(d));
                updateBD(d);
            }
            this.f8116C = this.f8116C.scalarMultiply(d6).add(scalarMultiply2).add(scalarMultiply.scalarMultiply(this.ccovmu).multiply(times(repmat(this.weights, 1, this.dimension), scalarMultiply.transpose())));
        }
        d = 0.0d;
        updateBD(d);
    }

    private void updateBD(double d) {
        double d2 = this.ccov1;
        double d3 = this.ccovmu;
        if (d2 + d3 + d > 0.0d) {
            double d4 = (double) this.iterations;
            Double.isNaN(d4);
            double d5 = (double) this.dimension;
            Double.isNaN(d5);
            if ((((d4 % 1.0d) / ((d2 + d3) + d)) / d5) / 10.0d < 1.0d) {
                this.f8116C = triu(this.f8116C, 0).add(triu(this.f8116C, 1).transpose());
                EigenDecomposition eigenDecomposition = new EigenDecomposition(this.f8116C);
                this.f8114B = eigenDecomposition.getV();
                this.f8117D = eigenDecomposition.getD();
                this.diagD = diag(this.f8117D);
                if (min(this.diagD) <= 0.0d) {
                    for (int i = 0; i < this.dimension; i++) {
                        if (this.diagD.getEntry(i, 0) < 0.0d) {
                            this.diagD.setEntry(i, 0, 0.0d);
                        }
                    }
                    double max = max(this.diagD) / 1.0E14d;
                    RealMatrix realMatrix = this.f8116C;
                    int i2 = this.dimension;
                    this.f8116C = realMatrix.add(eye(i2, i2).scalarMultiply(max));
                    this.diagD = this.diagD.add(ones(this.dimension, 1).scalarMultiply(max));
                }
                if (max(this.diagD) > min(this.diagD) * 1.0E14d) {
                    double max2 = (max(this.diagD) / 1.0E14d) - min(this.diagD);
                    RealMatrix realMatrix2 = this.f8116C;
                    int i3 = this.dimension;
                    this.f8116C = realMatrix2.add(eye(i3, i3).scalarMultiply(max2));
                    this.diagD = this.diagD.add(ones(this.dimension, 1).scalarMultiply(max2));
                }
                this.diagC = diag(this.f8116C);
                this.diagD = sqrt(this.diagD);
                this.f8115BD = times(this.f8114B, repmat(this.diagD.transpose(), this.dimension, 1));
            }
        }
    }

    private static void push(double[] dArr, double d) {
        for (int length = dArr.length - 1; length > 0; length--) {
            dArr[length] = dArr[length - 1];
        }
        dArr[0] = d;
    }

    private int[] sortedIndices(double[] dArr) {
        DoubleIndex[] doubleIndexArr = new DoubleIndex[dArr.length];
        for (int i = 0; i < dArr.length; i++) {
            doubleIndexArr[i] = new DoubleIndex(dArr[i], i);
        }
        Arrays.sort(doubleIndexArr);
        int[] iArr = new int[dArr.length];
        for (int i2 = 0; i2 < dArr.length; i2++) {
            iArr[i2] = doubleIndexArr[i2].index;
        }
        return iArr;
    }

    private static class DoubleIndex implements Comparable<DoubleIndex> {
        /* access modifiers changed from: private */
        public final int index;
        private final double value;

        DoubleIndex(double d, int i) {
            this.value = d;
            this.index = i;
        }

        public int compareTo(DoubleIndex doubleIndex) {
            return Double.compare(this.value, doubleIndex.value);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DoubleIndex) || Double.compare(this.value, ((DoubleIndex) obj).value) != 0) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            long doubleToLongBits = Double.doubleToLongBits(this.value);
            return (int) ((doubleToLongBits ^ ((doubleToLongBits >>> 32) ^ 1438542)) & -1);
        }
    }

    private class FitnessFunction {
        private final boolean isRepairMode = true;
        private double valueRange = 1.0d;

        public FitnessFunction() {
        }

        public double value(double[] dArr) {
            double d;
            if (this.isRepairMode) {
                double[] repair = repair(dArr);
                d = CMAESOptimizer.this.computeObjectiveValue(repair) + penalty(dArr, repair);
            } else {
                d = CMAESOptimizer.this.computeObjectiveValue(dArr);
            }
            return CMAESOptimizer.this.isMinimize ? d : -d;
        }

        public boolean isFeasible(double[] dArr) {
            double[] lowerBound = CMAESOptimizer.this.getLowerBound();
            double[] upperBound = CMAESOptimizer.this.getUpperBound();
            for (int i = 0; i < dArr.length; i++) {
                if (dArr[i] < lowerBound[i] || dArr[i] > upperBound[i]) {
                    return false;
                }
            }
            return true;
        }

        public void setValueRange(double d) {
            this.valueRange = d;
        }

        /* access modifiers changed from: private */
        public double[] repair(double[] dArr) {
            double[] lowerBound = CMAESOptimizer.this.getLowerBound();
            double[] upperBound = CMAESOptimizer.this.getUpperBound();
            double[] dArr2 = new double[dArr.length];
            for (int i = 0; i < dArr.length; i++) {
                if (dArr[i] < lowerBound[i]) {
                    dArr2[i] = lowerBound[i];
                } else if (dArr[i] > upperBound[i]) {
                    dArr2[i] = upperBound[i];
                } else {
                    dArr2[i] = dArr[i];
                }
            }
            return dArr2;
        }

        private double penalty(double[] dArr, double[] dArr2) {
            double d = 0.0d;
            for (int i = 0; i < dArr.length; i++) {
                d += FastMath.abs(dArr[i] - dArr2[i]) * this.valueRange;
            }
            return CMAESOptimizer.this.isMinimize ? d : -d;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix log(RealMatrix realMatrix) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                dArr[i][i2] = FastMath.log(realMatrix.getEntry(i, i2));
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix sqrt(RealMatrix realMatrix) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                dArr[i][i2] = FastMath.sqrt(realMatrix.getEntry(i, i2));
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix square(RealMatrix realMatrix) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                double entry = realMatrix.getEntry(i, i2);
                dArr[i][i2] = entry * entry;
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix times(RealMatrix realMatrix, RealMatrix realMatrix2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                dArr[i][i2] = realMatrix.getEntry(i, i2) * realMatrix2.getEntry(i, i2);
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix divide(RealMatrix realMatrix, RealMatrix realMatrix2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                dArr[i][i2] = realMatrix.getEntry(i, i2) / realMatrix2.getEntry(i, i2);
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix selectColumns(RealMatrix realMatrix, int[] iArr) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), iArr.length);
        for (int i = 0; i < realMatrix.getRowDimension(); i++) {
            for (int i2 = 0; i2 < iArr.length; i2++) {
                dArr[i][i2] = realMatrix.getEntry(i, iArr[i2]);
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix triu(RealMatrix realMatrix, int i) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, realMatrix.getRowDimension(), realMatrix.getColumnDimension());
        int i2 = 0;
        while (i2 < realMatrix.getRowDimension()) {
            for (int i3 = 0; i3 < realMatrix.getColumnDimension(); i3++) {
                dArr[i2][i3] = i2 <= i3 - i ? realMatrix.getEntry(i2, i3) : 0.0d;
            }
            i2++;
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix sumRows(RealMatrix realMatrix) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, 1, realMatrix.getColumnDimension());
        for (int i = 0; i < realMatrix.getColumnDimension(); i++) {
            double d = 0.0d;
            for (int i2 = 0; i2 < realMatrix.getRowDimension(); i2++) {
                d += realMatrix.getEntry(i2, i);
            }
            dArr[0][i] = d;
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix diag(RealMatrix realMatrix) {
        Class<double> cls = double.class;
        if (realMatrix.getColumnDimension() == 1) {
            double[][] dArr = (double[][]) Array.newInstance((Class<?>) cls, realMatrix.getRowDimension(), realMatrix.getRowDimension());
            for (int i = 0; i < realMatrix.getRowDimension(); i++) {
                dArr[i][i] = realMatrix.getEntry(i, 0);
            }
            return new Array2DRowRealMatrix(dArr, false);
        }
        double[][] dArr2 = (double[][]) Array.newInstance((Class<?>) cls, realMatrix.getRowDimension(), 1);
        for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
            dArr2[i2][0] = realMatrix.getEntry(i2, i2);
        }
        return new Array2DRowRealMatrix(dArr2, false);
    }

    private static void copyColumn(RealMatrix realMatrix, int i, RealMatrix realMatrix2, int i2) {
        for (int i3 = 0; i3 < realMatrix.getRowDimension(); i3++) {
            realMatrix2.setEntry(i3, i2, realMatrix.getEntry(i3, i));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix ones(int i, int i2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, i2);
        for (int i3 = 0; i3 < i; i3++) {
            Arrays.fill(dArr[i3], 1.0d);
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix eye(int i, int i2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, i2);
        for (int i3 = 0; i3 < i; i3++) {
            if (i3 < i2) {
                dArr[i3][i3] = 1.0d;
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    private static RealMatrix zeros(int i, int i2) {
        return new Array2DRowRealMatrix(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix repmat(RealMatrix realMatrix, int i, int i2) {
        int rowDimension = realMatrix.getRowDimension();
        int columnDimension = realMatrix.getColumnDimension();
        int i3 = i * rowDimension;
        int i4 = i2 * columnDimension;
        double[][] dArr = (double[][]) Array.newInstance(double.class, i3, i4);
        for (int i5 = 0; i5 < i3; i5++) {
            for (int i6 = 0; i6 < i4; i6++) {
                dArr[i5][i6] = realMatrix.getEntry(i5 % rowDimension, i6 % columnDimension);
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private static RealMatrix sequence(double d, double d2, double d3) {
        int i = (int) (((d2 - d) / d3) + 1.0d);
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, 1);
        double d4 = d;
        for (int i2 = 0; i2 < i; i2++) {
            dArr[i2][0] = d4;
            d4 += d3;
        }
        return new Array2DRowRealMatrix(dArr, false);
    }

    private static double max(RealMatrix realMatrix) {
        double d = -1.7976931348623157E308d;
        int i = 0;
        while (i < realMatrix.getRowDimension()) {
            double d2 = d;
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                double entry = realMatrix.getEntry(i, i2);
                if (d2 < entry) {
                    d2 = entry;
                }
            }
            i++;
            d = d2;
        }
        return d;
    }

    private static double min(RealMatrix realMatrix) {
        double d = Double.MAX_VALUE;
        int i = 0;
        while (i < realMatrix.getRowDimension()) {
            double d2 = d;
            for (int i2 = 0; i2 < realMatrix.getColumnDimension(); i2++) {
                double entry = realMatrix.getEntry(i, i2);
                if (d2 > entry) {
                    d2 = entry;
                }
            }
            i++;
            d = d2;
        }
        return d;
    }

    private static double max(double[] dArr) {
        double d = -1.7976931348623157E308d;
        for (int i = 0; i < dArr.length; i++) {
            if (d < dArr[i]) {
                d = dArr[i];
            }
        }
        return d;
    }

    private static double min(double[] dArr) {
        double d = Double.MAX_VALUE;
        for (int i = 0; i < dArr.length; i++) {
            if (d > dArr[i]) {
                d = dArr[i];
            }
        }
        return d;
    }

    private static int[] inverse(int[] iArr) {
        int[] iArr2 = new int[iArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr2[iArr[i]] = i;
        }
        return iArr2;
    }

    private static int[] reverse(int[] iArr) {
        int[] iArr2 = new int[iArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr2[i] = iArr[(iArr.length - i) - 1];
        }
        return iArr2;
    }

    private double[] randn(int i) {
        double[] dArr = new double[i];
        for (int i2 = 0; i2 < i; i2++) {
            dArr[i2] = this.random.nextGaussian();
        }
        return dArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    private RealMatrix randn1(int i, int i2) {
        double[][] dArr = (double[][]) Array.newInstance(double.class, i, i2);
        for (int i3 = 0; i3 < i; i3++) {
            for (int i4 = 0; i4 < i2; i4++) {
                dArr[i3][i4] = this.random.nextGaussian();
            }
        }
        return new Array2DRowRealMatrix(dArr, false);
    }
}
