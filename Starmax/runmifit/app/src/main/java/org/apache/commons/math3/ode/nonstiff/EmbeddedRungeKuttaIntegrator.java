package org.apache.commons.math3.ode.nonstiff;

public abstract class EmbeddedRungeKuttaIntegrator extends AdaptiveStepsizeIntegrator {

    /* renamed from: a */
    private final double[][] f8089a;

    /* renamed from: b */
    private final double[] f8090b;

    /* renamed from: c */
    private final double[] f8091c;
    private final double exp;
    private final boolean fsal;
    private double maxGrowth;
    private double minReduction;
    private final RungeKuttaStepInterpolator prototype;
    private double safety;

    /* access modifiers changed from: protected */
    public abstract double estimateError(double[][] dArr, double[] dArr2, double[] dArr3, double d);

    public abstract int getOrder();

    protected EmbeddedRungeKuttaIntegrator(String str, boolean z, double[] dArr, double[][] dArr2, double[] dArr3, RungeKuttaStepInterpolator rungeKuttaStepInterpolator, double d, double d2, double d3, double d4) {
        super(str, d, d2, d3, d4);
        this.fsal = z;
        this.f8091c = dArr;
        this.f8089a = dArr2;
        this.f8090b = dArr3;
        this.prototype = rungeKuttaStepInterpolator;
        double order = (double) getOrder();
        Double.isNaN(order);
        this.exp = -1.0d / order;
        setSafety(0.9d);
        setMinReduction(0.2d);
        setMaxGrowth(10.0d);
    }

    protected EmbeddedRungeKuttaIntegrator(String str, boolean z, double[] dArr, double[][] dArr2, double[] dArr3, RungeKuttaStepInterpolator rungeKuttaStepInterpolator, double d, double d2, double[] dArr4, double[] dArr5) {
        super(str, d, d2, dArr4, dArr5);
        this.fsal = z;
        this.f8091c = dArr;
        this.f8089a = dArr2;
        this.f8090b = dArr3;
        this.prototype = rungeKuttaStepInterpolator;
        double order = (double) getOrder();
        Double.isNaN(order);
        this.exp = -1.0d / order;
        setSafety(0.9d);
        setMinReduction(0.2d);
        setMaxGrowth(10.0d);
    }

    public double getSafety() {
        return this.safety;
    }

    public void setSafety(double d) {
        this.safety = d;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v0, resolved type: double[][]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void integrate(org.apache.commons.math3.ode.ExpandableStatefulODE r31, double r32) throws org.apache.commons.math3.exception.NumberIsTooSmallException, org.apache.commons.math3.exception.DimensionMismatchException, org.apache.commons.math3.exception.MaxCountExceededException, org.apache.commons.math3.exception.NoBracketingException {
        /*
            r30 = this;
            r10 = r30
            r11 = r31
            r30.sanityChecks(r31, r32)
            r30.setEquations(r31)
            double r0 = r31.getTime()
            r12 = 1
            r13 = 0
            int r2 = (r32 > r0 ? 1 : (r32 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0016
            r14 = 1
            goto L_0x0017
        L_0x0016:
            r14 = 0
        L_0x0017:
            double[] r15 = r31.getCompleteState()
            java.lang.Object r0 = r15.clone()
            r9 = r0
            double[] r9 = (double[]) r9
            double[] r0 = r10.f8091c
            int r0 = r0.length
            int r8 = r0 + 1
            int r0 = r9.length
            int[] r0 = new int[]{r8, r0}
            java.lang.Class<double> r1 = double.class
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r1, r0)
            r16 = r0
            double[][] r16 = (double[][]) r16
            java.lang.Object r0 = r15.clone()
            r7 = r0
            double[] r7 = (double[]) r7
            int r0 = r9.length
            double[] r6 = new double[r0]
            org.apache.commons.math3.ode.nonstiff.RungeKuttaStepInterpolator r0 = r10.prototype
            org.apache.commons.math3.ode.sampling.StepInterpolator r0 = r0.copy()
            r5 = r0
            org.apache.commons.math3.ode.nonstiff.RungeKuttaStepInterpolator r5 = (org.apache.commons.math3.ode.nonstiff.RungeKuttaStepInterpolator) r5
            org.apache.commons.math3.ode.EquationsMapper r17 = r31.getPrimaryMapper()
            org.apache.commons.math3.ode.EquationsMapper[] r18 = r31.getSecondaryMappers()
            r0 = r5
            r1 = r30
            r2 = r7
            r3 = r16
            r4 = r14
            r12 = r5
            r5 = r17
            r20 = r6
            r6 = r18
            r0.reinitialize(r1, r2, r3, r4, r5, r6)
            double r0 = r31.getTime()
            r12.storeTime(r0)
            double r0 = r31.getTime()
            r10.stepStart = r0
            r17 = 0
            double r1 = r31.getTime()
            r0 = r30
            r3 = r15
            r4 = r32
            r0.initIntegration(r1, r3, r4)
            r10.isLastStep = r13
            r0 = 1
        L_0x0080:
            r12.shift()
            r1 = 4621819117588971520(0x4024000000000000, double:10.0)
            r6 = r0
            r4 = r1
        L_0x0087:
            r21 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r0 = (r4 > r21 ? 1 : (r4 == r21 ? 0 : -1))
            if (r0 < 0) goto L_0x01eb
            if (r6 != 0) goto L_0x0093
            boolean r0 = r10.fsal
            if (r0 != 0) goto L_0x009a
        L_0x0093:
            double r0 = r10.stepStart
            r2 = r16[r13]
            r10.computeDerivatives(r0, r9, r2)
        L_0x009a:
            if (r6 == 0) goto L_0x00f6
            int r0 = r10.mainSetDimension
            double[] r3 = new double[r0]
            double[] r0 = r10.vecAbsoluteTolerance
            if (r0 != 0) goto L_0x00ba
            r0 = 0
        L_0x00a5:
            int r1 = r3.length
            if (r0 >= r1) goto L_0x00d5
            double r1 = r10.scalAbsoluteTolerance
            double r4 = r10.scalRelativeTolerance
            r17 = r9[r0]
            double r17 = org.apache.commons.math3.util.FastMath.abs(r17)
            double r4 = r4 * r17
            double r1 = r1 + r4
            r3[r0] = r1
            int r0 = r0 + 1
            goto L_0x00a5
        L_0x00ba:
            r0 = 0
        L_0x00bb:
            int r1 = r3.length
            if (r0 >= r1) goto L_0x00d5
            double[] r1 = r10.vecAbsoluteTolerance
            r4 = r1[r0]
            double[] r1 = r10.vecRelativeTolerance
            r17 = r1[r0]
            r1 = r9[r0]
            double r1 = org.apache.commons.math3.util.FastMath.abs(r1)
            double r17 = r17 * r1
            double r4 = r4 + r17
            r3[r0] = r4
            int r0 = r0 + 1
            goto L_0x00bb
        L_0x00d5:
            int r2 = r30.getOrder()
            double r4 = r10.stepStart
            r17 = r16[r13]
            r19 = 1
            r18 = r16[r19]
            r0 = r30
            r1 = r14
            r6 = r9
            r23 = r7
            r7 = r17
            r13 = r8
            r8 = r23
            r11 = r9
            r9 = r18
            double r0 = r0.initializeStep(r1, r2, r3, r4, r6, r7, r8, r9)
            r6 = r0
            r8 = 0
            goto L_0x00ff
        L_0x00f6:
            r23 = r7
            r13 = r8
            r11 = r9
            r19 = 1
            r8 = r6
            r6 = r17
        L_0x00ff:
            r10.stepSize = r6
            if (r14 == 0) goto L_0x0113
            double r0 = r10.stepStart
            double r2 = r10.stepSize
            double r0 = r0 + r2
            int r2 = (r0 > r32 ? 1 : (r0 == r32 ? 0 : -1))
            if (r2 < 0) goto L_0x0122
            double r0 = r10.stepStart
            double r0 = r32 - r0
            r10.stepSize = r0
            goto L_0x0122
        L_0x0113:
            double r0 = r10.stepStart
            double r2 = r10.stepSize
            double r0 = r0 + r2
            int r2 = (r0 > r32 ? 1 : (r0 == r32 ? 0 : -1))
            if (r2 > 0) goto L_0x0122
            double r0 = r10.stepStart
            double r0 = r32 - r0
            r10.stepSize = r0
        L_0x0122:
            r0 = 1
        L_0x0123:
            if (r0 >= r13) goto L_0x0175
            r1 = 0
        L_0x0126:
            int r2 = r15.length
            if (r1 >= r2) goto L_0x015a
            double[][] r2 = r10.f8089a
            int r3 = r0 + -1
            r2 = r2[r3]
            r4 = 0
            r17 = r2[r4]
            r2 = r16[r4]
            r4 = r2[r1]
            double r17 = r17 * r4
            r2 = 1
        L_0x0139:
            if (r2 >= r0) goto L_0x014c
            double[][] r4 = r10.f8089a
            r4 = r4[r3]
            r24 = r4[r2]
            r4 = r16[r2]
            r26 = r4[r1]
            double r24 = r24 * r26
            double r17 = r17 + r24
            int r2 = r2 + 1
            goto L_0x0139
        L_0x014c:
            r2 = r11[r1]
            double r4 = r10.stepSize
            double r4 = r4 * r17
            double r2 = r2 + r4
            r9 = r23
            r9[r1] = r2
            int r1 = r1 + 1
            goto L_0x0126
        L_0x015a:
            r9 = r23
            double r1 = r10.stepStart
            double[] r3 = r10.f8091c
            int r4 = r0 + -1
            r4 = r3[r4]
            r17 = r6
            double r6 = r10.stepSize
            double r4 = r4 * r6
            double r1 = r1 + r4
            r3 = r16[r0]
            r10.computeDerivatives(r1, r9, r3)
            int r0 = r0 + 1
            r6 = r17
            goto L_0x0123
        L_0x0175:
            r17 = r6
            r9 = r23
            r0 = 0
        L_0x017a:
            int r1 = r15.length
            if (r0 >= r1) goto L_0x01a5
            double[] r1 = r10.f8090b
            r2 = 0
            r3 = r1[r2]
            r1 = r16[r2]
            r5 = r1[r0]
            double r3 = r3 * r5
            r1 = 1
        L_0x0189:
            if (r1 >= r13) goto L_0x0199
            double[] r2 = r10.f8090b
            r5 = r2[r1]
            r2 = r16[r1]
            r24 = r2[r0]
            double r5 = r5 * r24
            double r3 = r3 + r5
            int r1 = r1 + 1
            goto L_0x0189
        L_0x0199:
            r1 = r11[r0]
            double r5 = r10.stepSize
            double r5 = r5 * r3
            double r1 = r1 + r5
            r9[r0] = r1
            int r0 = r0 + 1
            goto L_0x017a
        L_0x01a5:
            double r4 = r10.stepSize
            r0 = r30
            r1 = r16
            r2 = r11
            r3 = r9
            double r4 = r0.estimateError(r1, r2, r3, r4)
            int r0 = (r4 > r21 ? 1 : (r4 == r21 ? 0 : -1))
            if (r0 < 0) goto L_0x01df
            double r0 = r10.maxGrowth
            double r2 = r10.minReduction
            double r6 = r10.safety
            r21 = r8
            r23 = r9
            double r8 = r10.exp
            double r8 = org.apache.commons.math3.util.FastMath.pow(r4, r8)
            double r6 = r6 * r8
            double r2 = org.apache.commons.math3.util.FastMath.max(r2, r6)
            double r0 = org.apache.commons.math3.util.FastMath.min(r0, r2)
            double r2 = r10.stepSize
            double r2 = r2 * r0
            r7 = 0
            double r17 = r10.filterStep(r2, r14, r7)
            r9 = r11
            r8 = r13
            r6 = r21
            r7 = r23
            goto L_0x01e6
        L_0x01df:
            r21 = r8
            r7 = r9
            r9 = r11
            r8 = r13
            r6 = r21
        L_0x01e6:
            r13 = 0
            r11 = r31
            goto L_0x0087
        L_0x01eb:
            r23 = r7
            r13 = r8
            r11 = r9
            r7 = 0
            r19 = 1
            double r0 = r10.stepStart
            double r2 = r10.stepSize
            double r0 = r0 + r2
            r12.storeTime(r0)
            int r0 = r15.length
            r8 = r23
            java.lang.System.arraycopy(r8, r7, r11, r7, r0)
            int r0 = r13 + -1
            r0 = r16[r0]
            int r1 = r15.length
            r9 = r20
            java.lang.System.arraycopy(r0, r7, r9, r7, r1)
            r0 = r30
            r1 = r12
            r2 = r11
            r3 = r9
            r28 = r4
            r4 = r32
            double r0 = r0.acceptStep(r1, r2, r3, r4)
            r10.stepStart = r0
            int r0 = r11.length
            java.lang.System.arraycopy(r11, r7, r8, r7, r0)
            boolean r0 = r10.isLastStep
            if (r0 != 0) goto L_0x0281
            double r0 = r10.stepStart
            r12.storeTime(r0)
            boolean r0 = r10.fsal
            if (r0 == 0) goto L_0x0230
            r0 = r16[r7]
            int r1 = r15.length
            java.lang.System.arraycopy(r9, r7, r0, r7, r1)
        L_0x0230:
            double r0 = r10.maxGrowth
            double r2 = r10.minReduction
            double r4 = r10.safety
            r23 = r8
            double r7 = r10.exp
            r21 = r11
            r20 = r12
            r11 = r28
            double r7 = org.apache.commons.math3.util.FastMath.pow(r11, r7)
            double r4 = r4 * r7
            double r2 = org.apache.commons.math3.util.FastMath.max(r2, r4)
            double r0 = org.apache.commons.math3.util.FastMath.min(r0, r2)
            double r2 = r10.stepSize
            double r2 = r2 * r0
            double r0 = r10.stepStart
            double r0 = r0 + r2
            if (r14 == 0) goto L_0x025c
            int r4 = (r0 > r32 ? 1 : (r0 == r32 ? 0 : -1))
            if (r4 < 0) goto L_0x0262
            goto L_0x0260
        L_0x025c:
            int r4 = (r0 > r32 ? 1 : (r0 == r32 ? 0 : -1))
            if (r4 > 0) goto L_0x0262
        L_0x0260:
            r0 = 1
            goto L_0x0263
        L_0x0262:
            r0 = 0
        L_0x0263:
            double r0 = r10.filterStep(r2, r14, r0)
            double r2 = r10.stepStart
            double r2 = r2 + r0
            if (r14 == 0) goto L_0x0271
            int r4 = (r2 > r32 ? 1 : (r2 == r32 ? 0 : -1))
            if (r4 < 0) goto L_0x0277
            goto L_0x0275
        L_0x0271:
            int r4 = (r2 > r32 ? 1 : (r2 == r32 ? 0 : -1))
            if (r4 > 0) goto L_0x0277
        L_0x0275:
            r2 = 1
            goto L_0x0278
        L_0x0277:
            r2 = 0
        L_0x0278:
            if (r2 == 0) goto L_0x027e
            double r0 = r10.stepStart
            double r0 = r32 - r0
        L_0x027e:
            r17 = r0
            goto L_0x0287
        L_0x0281:
            r23 = r8
            r21 = r11
            r20 = r12
        L_0x0287:
            boolean r0 = r10.isLastStep
            if (r0 == 0) goto L_0x029b
            double r0 = r10.stepStart
            r2 = r31
            r3 = r21
            r2.setTime(r0)
            r2.setCompleteState(r3)
            r30.resetInternalState()
            return
        L_0x029b:
            r11 = r31
            r0 = r6
            r8 = r13
            r12 = r20
            r7 = r23
            r13 = 0
            r20 = r9
            r9 = r21
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.ode.nonstiff.EmbeddedRungeKuttaIntegrator.integrate(org.apache.commons.math3.ode.ExpandableStatefulODE, double):void");
    }

    public double getMinReduction() {
        return this.minReduction;
    }

    public void setMinReduction(double d) {
        this.minReduction = d;
    }

    public double getMaxGrowth() {
        return this.maxGrowth;
    }

    public void setMaxGrowth(double d) {
        this.maxGrowth = d;
    }
}
