package org.apache.commons.math3.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import org.apache.commons.math3.Field;
import org.apache.commons.math3.FieldElement;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.util.LocalizedFormats;

public class BigReal implements FieldElement<BigReal>, Comparable<BigReal>, Serializable {
    public static final BigReal ONE = new BigReal(BigDecimal.ONE);
    public static final BigReal ZERO = new BigReal(BigDecimal.ZERO);
    private static final long serialVersionUID = 4984534880991310382L;

    /* renamed from: d */
    private final BigDecimal f8192d;
    private RoundingMode roundingMode = RoundingMode.HALF_UP;
    private int scale = 64;

    public BigReal(BigDecimal bigDecimal) {
        this.f8192d = bigDecimal;
    }

    public BigReal(BigInteger bigInteger) {
        this.f8192d = new BigDecimal(bigInteger);
    }

    public BigReal(BigInteger bigInteger, int i) {
        this.f8192d = new BigDecimal(bigInteger, i);
    }

    public BigReal(BigInteger bigInteger, int i, MathContext mathContext) {
        this.f8192d = new BigDecimal(bigInteger, i, mathContext);
    }

    public BigReal(BigInteger bigInteger, MathContext mathContext) {
        this.f8192d = new BigDecimal(bigInteger, mathContext);
    }

    public BigReal(char[] cArr) {
        this.f8192d = new BigDecimal(cArr);
    }

    public BigReal(char[] cArr, int i, int i2) {
        this.f8192d = new BigDecimal(cArr, i, i2);
    }

    public BigReal(char[] cArr, int i, int i2, MathContext mathContext) {
        this.f8192d = new BigDecimal(cArr, i, i2, mathContext);
    }

    public BigReal(char[] cArr, MathContext mathContext) {
        this.f8192d = new BigDecimal(cArr, mathContext);
    }

    public BigReal(double d) {
        this.f8192d = new BigDecimal(d);
    }

    public BigReal(double d, MathContext mathContext) {
        this.f8192d = new BigDecimal(d, mathContext);
    }

    public BigReal(int i) {
        this.f8192d = new BigDecimal(i);
    }

    public BigReal(int i, MathContext mathContext) {
        this.f8192d = new BigDecimal(i, mathContext);
    }

    public BigReal(long j) {
        this.f8192d = new BigDecimal(j);
    }

    public BigReal(long j, MathContext mathContext) {
        this.f8192d = new BigDecimal(j, mathContext);
    }

    public BigReal(String str) {
        this.f8192d = new BigDecimal(str);
    }

    public BigReal(String str, MathContext mathContext) {
        this.f8192d = new BigDecimal(str, mathContext);
    }

    public RoundingMode getRoundingMode() {
        return this.roundingMode;
    }

    public void setRoundingMode(RoundingMode roundingMode2) {
        this.roundingMode = roundingMode2;
    }

    public int getScale() {
        return this.scale;
    }

    public void setScale(int i) {
        this.scale = i;
    }

    public BigReal add(BigReal bigReal) {
        return new BigReal(this.f8192d.add(bigReal.f8192d));
    }

    public BigReal subtract(BigReal bigReal) {
        return new BigReal(this.f8192d.subtract(bigReal.f8192d));
    }

    public BigReal negate() {
        return new BigReal(this.f8192d.negate());
    }

    public BigReal divide(BigReal bigReal) throws MathArithmeticException {
        try {
            return new BigReal(this.f8192d.divide(bigReal.f8192d, this.scale, this.roundingMode));
        } catch (ArithmeticException unused) {
            throw new MathArithmeticException(LocalizedFormats.ZERO_NOT_ALLOWED, new Object[0]);
        }
    }

    public BigReal reciprocal() throws MathArithmeticException {
        try {
            return new BigReal(BigDecimal.ONE.divide(this.f8192d, this.scale, this.roundingMode));
        } catch (ArithmeticException unused) {
            throw new MathArithmeticException(LocalizedFormats.ZERO_NOT_ALLOWED, new Object[0]);
        }
    }

    public BigReal multiply(BigReal bigReal) {
        return new BigReal(this.f8192d.multiply(bigReal.f8192d));
    }

    public BigReal multiply(int i) {
        return new BigReal(this.f8192d.multiply(new BigDecimal(i)));
    }

    public int compareTo(BigReal bigReal) {
        return this.f8192d.compareTo(bigReal.f8192d);
    }

    public double doubleValue() {
        return this.f8192d.doubleValue();
    }

    public BigDecimal bigDecimalValue() {
        return this.f8192d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof BigReal) {
            return this.f8192d.equals(((BigReal) obj).f8192d);
        }
        return false;
    }

    public int hashCode() {
        return this.f8192d.hashCode();
    }

    public Field<BigReal> getField() {
        return BigRealField.getInstance();
    }
}
