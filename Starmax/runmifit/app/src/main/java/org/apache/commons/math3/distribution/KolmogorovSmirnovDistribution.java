package org.apache.commons.math3.distribution;

import java.io.Serializable;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.fraction.BigFraction;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.FieldMatrix;
import org.apache.commons.math3.util.FastMath;

public class KolmogorovSmirnovDistribution implements Serializable {
    private static final long serialVersionUID = -4670676796862967187L;

    /* renamed from: n */
    private int f7979n;

    public KolmogorovSmirnovDistribution(int i) throws NotStrictlyPositiveException {
        if (i > 0) {
            this.f7979n = i;
            return;
        }
        throw new NotStrictlyPositiveException(LocalizedFormats.NOT_POSITIVE_NUMBER_OF_SAMPLES, Integer.valueOf(i));
    }

    public double cdf(double d) throws MathArithmeticException {
        return cdf(d, false);
    }

    public double cdfExact(double d) throws MathArithmeticException {
        return cdf(d, true);
    }

    public double cdf(double d, boolean z) throws MathArithmeticException {
        double d2 = (double) this.f7979n;
        double d3 = 1.0d;
        Double.isNaN(d2);
        double d4 = 1.0d / d2;
        double d5 = 0.5d * d4;
        if (d <= d5) {
            return 0.0d;
        }
        if (d5 < d && d <= d4) {
            double d6 = (d * 2.0d) - d4;
            for (int i = 1; i <= this.f7979n; i++) {
                double d7 = (double) i;
                Double.isNaN(d7);
                d3 *= d7 * d6;
            }
            return d3;
        } else if (1.0d - d4 <= d && d < 1.0d) {
            return 1.0d - (FastMath.pow(1.0d - d, this.f7979n) * 2.0d);
        } else {
            if (1.0d <= d) {
                return 1.0d;
            }
            return z ? exactK(d) : roundedK(d);
        }
    }

    private double exactK(double d) throws MathArithmeticException {
        double d2 = (double) this.f7979n;
        Double.isNaN(d2);
        FieldMatrix<BigFraction> power = createH(d).power(this.f7979n);
        int ceil = ((int) FastMath.ceil(d2 * d)) - 1;
        BigFraction entry = power.getEntry(ceil, ceil);
        for (int i = 1; i <= this.f7979n; i++) {
            entry = entry.multiply(i).divide(this.f7979n);
        }
        return entry.bigDecimalValue(20, 4).doubleValue();
    }

    private double roundedK(double d) throws MathArithmeticException {
        double d2 = (double) this.f7979n;
        Double.isNaN(d2);
        int ceil = (int) FastMath.ceil(d2 * d);
        FieldMatrix<BigFraction> createH = createH(d);
        int rowDimension = createH.getRowDimension();
        Array2DRowRealMatrix array2DRowRealMatrix = new Array2DRowRealMatrix(rowDimension, rowDimension);
        for (int i = 0; i < rowDimension; i++) {
            for (int i2 = 0; i2 < rowDimension; i2++) {
                array2DRowRealMatrix.setEntry(i, i2, createH.getEntry(i, i2).doubleValue());
            }
        }
        int i3 = 1;
        int i4 = ceil - 1;
        double entry = array2DRowRealMatrix.power(this.f7979n).getEntry(i4, i4);
        while (true) {
            int i5 = this.f7979n;
            if (i3 > i5) {
                return entry;
            }
            double d3 = (double) i3;
            double d4 = (double) i5;
            Double.isNaN(d3);
            Double.isNaN(d4);
            entry *= d3 / d4;
            i3++;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(20:2|3|4|5|8|(4:10|(3:12|(2:14|46)(2:15|45)|16)|44|17)|43|18|(1:20)|47|21|(1:23)|48|24|(1:26)|27|(3:29|(2:30|(3:32|(3:34|(1:36)|52)(1:51)|37)(1:50))|38)|49|39|40) */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0044, code lost:
        r7 = new org.apache.commons.math3.fraction.BigFraction(r14, 1.0E-5d, 10000);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0035 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.commons.math3.linear.FieldMatrix<org.apache.commons.math3.fraction.BigFraction> createH(double r14) throws org.apache.commons.math3.exception.NumberIsTooLargeException, org.apache.commons.math3.fraction.FractionConversionException {
        /*
            r13 = this;
            int r0 = r13.f7979n
            double r0 = (double) r0
            java.lang.Double.isNaN(r0)
            double r0 = r0 * r14
            double r0 = org.apache.commons.math3.util.FastMath.ceil(r0)
            int r0 = (int) r0
            int r1 = r0 * 2
            r2 = 1
            int r1 = r1 - r2
            double r3 = (double) r0
            int r0 = r13.f7979n
            double r5 = (double) r0
            java.lang.Double.isNaN(r5)
            double r5 = r5 * r14
            java.lang.Double.isNaN(r3)
            double r14 = r3 - r5
            r3 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r0 = 0
            int r5 = (r14 > r3 ? 1 : (r14 == r3 ? 0 : -1))
            if (r5 >= 0) goto L_0x0108
            org.apache.commons.math3.fraction.BigFraction r3 = new org.apache.commons.math3.fraction.BigFraction     // Catch:{ FractionConversionException -> 0x0035 }
            r10 = 4307583784117748259(0x3bc79ca10c924223, double:1.0E-20)
            r12 = 10000(0x2710, float:1.4013E-41)
            r7 = r3
            r8 = r14
            r7.<init>(r8, r10, r12)     // Catch:{ FractionConversionException -> 0x0035 }
            goto L_0x0052
        L_0x0035:
            org.apache.commons.math3.fraction.BigFraction r3 = new org.apache.commons.math3.fraction.BigFraction     // Catch:{ FractionConversionException -> 0x0044 }
            r10 = 4457293557087583675(0x3ddb7cdfd9d7bdbb, double:1.0E-10)
            r12 = 10000(0x2710, float:1.4013E-41)
            r7 = r3
            r8 = r14
            r7.<init>(r8, r10, r12)     // Catch:{ FractionConversionException -> 0x0044 }
            goto L_0x0052
        L_0x0044:
            org.apache.commons.math3.fraction.BigFraction r3 = new org.apache.commons.math3.fraction.BigFraction
            r10 = 4532020583610935537(0x3ee4f8b588e368f1, double:1.0E-5)
            r12 = 10000(0x2710, float:1.4013E-41)
            r7 = r3
            r8 = r14
            r7.<init>(r8, r10, r12)
        L_0x0052:
            int[] r14 = new int[]{r1, r1}
            java.lang.Class<org.apache.commons.math3.fraction.BigFraction> r15 = org.apache.commons.math3.fraction.BigFraction.class
            java.lang.Object r14 = java.lang.reflect.Array.newInstance(r15, r14)
            org.apache.commons.math3.fraction.BigFraction[][] r14 = (org.apache.commons.math3.fraction.BigFraction[][]) r14
            r15 = 0
        L_0x005f:
            if (r15 >= r1) goto L_0x007c
            r4 = 0
        L_0x0062:
            if (r4 >= r1) goto L_0x0079
            int r5 = r15 - r4
            int r5 = r5 + r2
            if (r5 >= 0) goto L_0x0070
            r5 = r14[r15]
            org.apache.commons.math3.fraction.BigFraction r6 = org.apache.commons.math3.fraction.BigFraction.ZERO
            r5[r4] = r6
            goto L_0x0076
        L_0x0070:
            r5 = r14[r15]
            org.apache.commons.math3.fraction.BigFraction r6 = org.apache.commons.math3.fraction.BigFraction.ONE
            r5[r4] = r6
        L_0x0076:
            int r4 = r4 + 1
            goto L_0x0062
        L_0x0079:
            int r15 = r15 + 1
            goto L_0x005f
        L_0x007c:
            org.apache.commons.math3.fraction.BigFraction[] r15 = new org.apache.commons.math3.fraction.BigFraction[r1]
            r15[r0] = r3
            r4 = 1
        L_0x0081:
            if (r4 >= r1) goto L_0x0090
            int r5 = r4 + -1
            r5 = r15[r5]
            org.apache.commons.math3.fraction.BigFraction r5 = r3.multiply(r5)
            r15[r4] = r5
            int r4 = r4 + 1
            goto L_0x0081
        L_0x0090:
            r4 = 0
        L_0x0091:
            if (r4 >= r1) goto L_0x00b7
            r5 = r14[r4]
            r6 = r14[r4]
            r6 = r6[r0]
            r7 = r15[r4]
            org.apache.commons.math3.fraction.BigFraction r6 = r6.subtract(r7)
            r5[r0] = r6
            int r5 = r1 + -1
            r6 = r14[r5]
            r5 = r14[r5]
            r5 = r5[r4]
            int r7 = r1 - r4
            int r7 = r7 - r2
            r7 = r15[r7]
            org.apache.commons.math3.fraction.BigFraction r5 = r5.subtract(r7)
            r6[r4] = r5
            int r4 = r4 + 1
            goto L_0x0091
        L_0x00b7:
            org.apache.commons.math3.fraction.BigFraction r15 = org.apache.commons.math3.fraction.BigFraction.ONE_HALF
            int r15 = r3.compareTo(r15)
            r4 = 2
            if (r15 != r2) goto L_0x00da
            int r15 = r1 + -1
            r5 = r14[r15]
            r15 = r14[r15]
            r15 = r15[r0]
            org.apache.commons.math3.fraction.BigFraction r3 = r3.multiply(r4)
            org.apache.commons.math3.fraction.BigFraction r3 = r3.subtract(r2)
            org.apache.commons.math3.fraction.BigFraction r3 = r3.pow(r1)
            org.apache.commons.math3.fraction.BigFraction r15 = r15.add(r3)
            r5[r0] = r15
        L_0x00da:
            r15 = 0
        L_0x00db:
            if (r15 >= r1) goto L_0x00fe
            r3 = 0
        L_0x00de:
            int r5 = r15 + 1
            if (r3 >= r5) goto L_0x00fc
            int r5 = r15 - r3
            int r5 = r5 + r2
            if (r5 <= 0) goto L_0x00f9
            r6 = 2
        L_0x00e8:
            if (r6 > r5) goto L_0x00f9
            r7 = r14[r15]
            r8 = r14[r15]
            r8 = r8[r3]
            org.apache.commons.math3.fraction.BigFraction r8 = r8.divide(r6)
            r7[r3] = r8
            int r6 = r6 + 1
            goto L_0x00e8
        L_0x00f9:
            int r3 = r3 + 1
            goto L_0x00de
        L_0x00fc:
            r15 = r5
            goto L_0x00db
        L_0x00fe:
            org.apache.commons.math3.linear.Array2DRowFieldMatrix r15 = new org.apache.commons.math3.linear.Array2DRowFieldMatrix
            org.apache.commons.math3.fraction.BigFractionField r0 = org.apache.commons.math3.fraction.BigFractionField.getInstance()
            r15.<init>(r0, r14)
            return r15
        L_0x0108:
            org.apache.commons.math3.exception.NumberIsTooLargeException r1 = new org.apache.commons.math3.exception.NumberIsTooLargeException
            java.lang.Double r14 = java.lang.Double.valueOf(r14)
            java.lang.Double r15 = java.lang.Double.valueOf(r3)
            r1.<init>(r14, r15, r0)
            goto L_0x0117
        L_0x0116:
            throw r1
        L_0x0117:
            goto L_0x0116
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.distribution.KolmogorovSmirnovDistribution.createH(double):org.apache.commons.math3.linear.FieldMatrix");
    }
}
