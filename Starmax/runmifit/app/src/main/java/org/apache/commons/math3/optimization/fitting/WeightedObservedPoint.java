package org.apache.commons.math3.optimization.fitting;

import java.io.Serializable;

@Deprecated
public class WeightedObservedPoint implements Serializable {
    private static final long serialVersionUID = 5306874947404636157L;
    private final double weight;

    /* renamed from: x */
    private final double f8126x;

    /* renamed from: y */
    private final double f8127y;

    public WeightedObservedPoint(double d, double d2, double d3) {
        this.weight = d;
        this.f8126x = d2;
        this.f8127y = d3;
    }

    public double getWeight() {
        return this.weight;
    }

    public double getX() {
        return this.f8126x;
    }

    public double getY() {
        return this.f8127y;
    }
}
