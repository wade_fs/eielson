package org.apache.commons.math3.random;

import java.io.Serializable;
import org.apache.commons.math3.util.FastMath;

public class MersenneTwister extends BitsStreamGenerator implements Serializable {

    /* renamed from: M */
    private static final int f8138M = 397;
    private static final int[] MAG01 = {0, -1727483681};

    /* renamed from: N */
    private static final int f8139N = 624;
    private static final long serialVersionUID = 8661194735290153518L;

    /* renamed from: mt */
    private int[] f8140mt = new int[f8139N];
    private int mti;

    public MersenneTwister() {
        setSeed(System.currentTimeMillis() + ((long) System.identityHashCode(this)));
    }

    public MersenneTwister(int i) {
        setSeed(i);
    }

    public MersenneTwister(int[] iArr) {
        setSeed(iArr);
    }

    public MersenneTwister(long j) {
        setSeed(j);
    }

    public void setSeed(int i) {
        long j = (long) i;
        this.f8140mt[0] = (int) j;
        int i2 = 1;
        while (true) {
            this.mti = i2;
            int i3 = this.mti;
            if (i3 < f8139N) {
                j = (((j ^ (j >> 30)) * 1812433253) + ((long) i3)) & 4294967295L;
                this.f8140mt[i3] = (int) j;
                i2 = i3 + 1;
            } else {
                clear();
                return;
            }
        }
    }

    public void setSeed(int[] iArr) {
        int[] iArr2 = iArr;
        if (iArr2 == null) {
            setSeed(System.currentTimeMillis() + ((long) System.identityHashCode(this)));
            return;
        }
        setSeed(19650218);
        int i = 1;
        int i2 = 0;
        for (int max = FastMath.max((int) f8139N, iArr2.length); max != 0; max--) {
            int[] iArr3 = this.f8140mt;
            long j = (iArr3[i] < 0 ? 2147483648L : 0) | (((long) iArr3[i]) & 2147483647L);
            int[] iArr4 = this.f8140mt;
            int i3 = i - 1;
            long j2 = (((long) iArr4[i3]) & 2147483647L) | (iArr4[i3] < 0 ? 2147483648L : 0);
            int[] iArr5 = this.f8140mt;
            iArr5[i] = (int) (((((j2 ^ (j2 >> 30)) * 1664525) ^ j) + ((long) iArr2[i2]) + ((long) i2)) & 4294967295L);
            i++;
            i2++;
            if (i >= f8139N) {
                iArr5[0] = iArr5[623];
                i = 1;
            }
            if (i2 >= iArr2.length) {
                i2 = 0;
            }
        }
        int i4 = 623;
        while (i4 != 0) {
            int[] iArr6 = this.f8140mt;
            long j3 = (((long) iArr6[i]) & 2147483647L) | (iArr6[i] < 0 ? 2147483648L : 0);
            int[] iArr7 = this.f8140mt;
            int i5 = i - 1;
            long j4 = (((long) iArr7[i5]) & 2147483647L) | (iArr7[i5] < 0 ? 2147483648L : 0);
            int[] iArr8 = this.f8140mt;
            iArr8[i] = (int) (((j3 ^ ((j4 ^ (j4 >> 30)) * 1566083941)) - ((long) i)) & 4294967295L);
            int i6 = i + 1;
            if (i6 >= f8139N) {
                iArr8[0] = iArr8[623];
                i6 = 1;
            }
            i4--;
            i = i6;
        }
        this.f8140mt[0] = Integer.MIN_VALUE;
        clear();
    }

    public void setSeed(long j) {
        setSeed(new int[]{(int) (j >>> 32), (int) (j & 4294967295L)});
    }

    /* access modifiers changed from: protected */
    public int next(int i) {
        int i2;
        if (this.mti >= f8139N) {
            int i3 = this.f8140mt[0];
            int i4 = 0;
            while (true) {
                i2 = 227;
                if (i4 >= 227) {
                    break;
                }
                int[] iArr = this.f8140mt;
                int i5 = i4 + 1;
                int i6 = iArr[i5];
                int i7 = (i3 & Integer.MIN_VALUE) | (Integer.MAX_VALUE & i6);
                iArr[i4] = MAG01[i7 & 1] ^ (iArr[i4 + f8138M] ^ (i7 >>> 1));
                i4 = i5;
                i3 = i6;
            }
            while (i2 < 623) {
                int[] iArr2 = this.f8140mt;
                int i8 = i2 + 1;
                int i9 = iArr2[i8];
                int i10 = (i3 & Integer.MIN_VALUE) | (i9 & Integer.MAX_VALUE);
                iArr2[i2] = MAG01[i10 & 1] ^ (iArr2[i2 - 227] ^ (i10 >>> 1));
                i2 = i8;
                i3 = i9;
            }
            int[] iArr3 = this.f8140mt;
            int i11 = (i3 & Integer.MIN_VALUE) | (Integer.MAX_VALUE & iArr3[0]);
            iArr3[623] = MAG01[i11 & 1] ^ (iArr3[396] ^ (i11 >>> 1));
            this.mti = 0;
        }
        int[] iArr4 = this.f8140mt;
        int i12 = this.mti;
        this.mti = i12 + 1;
        int i13 = iArr4[i12];
        int i14 = i13 ^ (i13 >>> 11);
        int i15 = i14 ^ ((i14 << 7) & -1658038656);
        int i16 = i15 ^ ((i15 << 15) & -272236544);
        return (i16 ^ (i16 >>> 18)) >>> (32 - i);
    }
}
