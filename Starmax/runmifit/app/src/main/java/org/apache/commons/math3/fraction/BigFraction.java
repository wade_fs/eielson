package org.apache.commons.math3.fraction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.apache.commons.math3.FieldElement;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.exception.ZeroException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.ArithmeticUtils;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathUtils;

public class BigFraction extends Number implements FieldElement<BigFraction>, Comparable<BigFraction>, Serializable {
    public static final BigFraction FOUR_FIFTHS = new BigFraction(4, 5);
    public static final BigFraction MINUS_ONE = new BigFraction(-1);
    public static final BigFraction ONE = new BigFraction(1);
    public static final BigFraction ONE_FIFTH = new BigFraction(1, 5);
    public static final BigFraction ONE_HALF = new BigFraction(1, 2);
    private static final BigInteger ONE_HUNDRED = BigInteger.valueOf(100);
    public static final BigFraction ONE_QUARTER = new BigFraction(1, 4);
    public static final BigFraction ONE_THIRD = new BigFraction(1, 3);
    public static final BigFraction THREE_FIFTHS = new BigFraction(3, 5);
    public static final BigFraction THREE_QUARTERS = new BigFraction(3, 4);
    public static final BigFraction TWO = new BigFraction(2);
    public static final BigFraction TWO_FIFTHS = new BigFraction(2, 5);
    public static final BigFraction TWO_QUARTERS = new BigFraction(2, 4);
    public static final BigFraction TWO_THIRDS = new BigFraction(2, 3);
    public static final BigFraction ZERO = new BigFraction(0);
    private static final long serialVersionUID = -5630213147331578515L;
    private final BigInteger denominator;
    private final BigInteger numerator;

    public BigFraction(BigInteger bigInteger) {
        this(bigInteger, BigInteger.ONE);
    }

    public BigFraction(BigInteger bigInteger, BigInteger bigInteger2) {
        MathUtils.checkNotNull(bigInteger, LocalizedFormats.NUMERATOR, new Object[0]);
        MathUtils.checkNotNull(bigInteger2, LocalizedFormats.DENOMINATOR, new Object[0]);
        if (BigInteger.ZERO.equals(bigInteger2)) {
            throw new ZeroException(LocalizedFormats.ZERO_DENOMINATOR, new Object[0]);
        } else if (BigInteger.ZERO.equals(bigInteger)) {
            this.numerator = BigInteger.ZERO;
            this.denominator = BigInteger.ONE;
        } else {
            BigInteger gcd = bigInteger.gcd(bigInteger2);
            if (BigInteger.ONE.compareTo(gcd) < 0) {
                bigInteger = bigInteger.divide(gcd);
                bigInteger2 = bigInteger2.divide(gcd);
            }
            if (BigInteger.ZERO.compareTo(bigInteger2) > 0) {
                bigInteger = bigInteger.negate();
                bigInteger2 = bigInteger2.negate();
            }
            this.numerator = bigInteger;
            this.denominator = bigInteger2;
        }
    }

    public BigFraction(double d) throws MathIllegalArgumentException {
        if (Double.isNaN(d)) {
            throw new MathIllegalArgumentException(LocalizedFormats.NAN_VALUE_CONVERSION, new Object[0]);
        } else if (!Double.isInfinite(d)) {
            long doubleToLongBits = Double.doubleToLongBits(d);
            long j = Long.MIN_VALUE & doubleToLongBits;
            long j2 = 9218868437227405312L & doubleToLongBits;
            long j3 = doubleToLongBits & 4503599627370495L;
            j3 = j2 != 0 ? j3 | 4503599627370496L : j3;
            j3 = j != 0 ? -j3 : j3;
            int i = ((int) (j2 >> 52)) - 1075;
            while ((9007199254740990L & j3) != 0 && (1 & j3) == 0) {
                j3 >>= 1;
                i++;
            }
            if (i < 0) {
                this.numerator = BigInteger.valueOf(j3);
                this.denominator = BigInteger.ZERO.flipBit(-i);
                return;
            }
            this.numerator = BigInteger.valueOf(j3).multiply(BigInteger.ZERO.flipBit(i));
            this.denominator = BigInteger.ONE;
        } else {
            throw new MathIllegalArgumentException(LocalizedFormats.INFINITE_VALUE_CONVERSION, new Object[0]);
        }
    }

    public BigFraction(double d, double d2, int i) throws FractionConversionException {
        this(d, d2, Integer.MAX_VALUE, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00aa, code lost:
        if (r37 != 0.0d) goto L_0x00de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00b3, code lost:
        if (org.apache.commons.math3.util.FastMath.abs(r16) >= ((long) r1)) goto L_0x00de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b5, code lost:
        r12 = r32;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00e8, code lost:
        throw new org.apache.commons.math3.fraction.FractionConversionException(r35, r5, r7);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private BigFraction(double r35, double r37, int r39, int r40) throws org.apache.commons.math3.fraction.FractionConversionException {
        /*
            r34 = this;
            r0 = r34
            r2 = r35
            r1 = r39
            r4 = r40
            r34.<init>()
            double r5 = org.apache.commons.math3.util.FastMath.floor(r35)
            long r5 = (long) r5
            long r7 = org.apache.commons.math3.util.FastMath.abs(r5)
            r9 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 > 0) goto L_0x00e9
            double r7 = (double) r5
            java.lang.Double.isNaN(r7)
            double r7 = r7 - r2
            double r7 = org.apache.commons.math3.util.FastMath.abs(r7)
            int r11 = (r7 > r37 ? 1 : (r7 == r37 ? 0 : -1))
            if (r11 >= 0) goto L_0x0033
            java.math.BigInteger r1 = java.math.BigInteger.valueOf(r5)
            r0.numerator = r1
            java.math.BigInteger r1 = java.math.BigInteger.ONE
            r0.denominator = r1
            return
        L_0x0033:
            r7 = 0
            r11 = 0
            r12 = 1
            r18 = r7
            r14 = r12
            r16 = r14
            r20 = 0
            r7 = r5
            r12 = r7
            r5 = r2
        L_0x0042:
            r21 = 1
            int r11 = r11 + 1
            r22 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r9 = (double) r7
            java.lang.Double.isNaN(r9)
            double r9 = r5 - r9
            double r22 = r22 / r9
            double r9 = org.apache.commons.math3.util.FastMath.floor(r22)
            long r9 = (long) r9
            long r26 = r9 * r12
            r28 = r5
            long r5 = r26 + r14
            long r26 = r9 * r16
            r30 = r7
            long r7 = r26 + r18
            r24 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r26 = (r5 > r24 ? 1 : (r5 == r24 ? 0 : -1))
            if (r26 > 0) goto L_0x00a4
            int r26 = (r7 > r24 ? 1 : (r7 == r24 ? 0 : -1))
            if (r26 <= 0) goto L_0x006d
            goto L_0x00a4
        L_0x006d:
            r26 = r9
            double r9 = (double) r5
            r32 = r12
            double r12 = (double) r7
            java.lang.Double.isNaN(r9)
            java.lang.Double.isNaN(r12)
            double r9 = r9 / r12
            if (r11 >= r4) goto L_0x0094
            double r9 = r9 - r2
            double r9 = org.apache.commons.math3.util.FastMath.abs(r9)
            int r12 = (r9 > r37 ? 1 : (r9 == r37 ? 0 : -1))
            if (r12 <= 0) goto L_0x0094
            long r9 = (long) r1
            int r12 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r12 >= 0) goto L_0x0094
            r12 = r5
            r18 = r16
            r28 = r22
            r14 = r32
            r16 = r7
            goto L_0x009a
        L_0x0094:
            r26 = r30
            r12 = r32
            r20 = 1
        L_0x009a:
            if (r20 == 0) goto L_0x009d
            goto L_0x00b7
        L_0x009d:
            r9 = r24
            r7 = r26
            r5 = r28
            goto L_0x0042
        L_0x00a4:
            r32 = r12
            r9 = 0
            int r12 = (r37 > r9 ? 1 : (r37 == r9 ? 0 : -1))
            if (r12 != 0) goto L_0x00de
            long r9 = org.apache.commons.math3.util.FastMath.abs(r16)
            long r12 = (long) r1
            int r14 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
            if (r14 >= 0) goto L_0x00de
            r12 = r32
        L_0x00b7:
            if (r11 >= r4) goto L_0x00d8
            long r1 = (long) r1
            int r3 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r3 >= 0) goto L_0x00cb
            java.math.BigInteger r1 = java.math.BigInteger.valueOf(r5)
            r0.numerator = r1
            java.math.BigInteger r1 = java.math.BigInteger.valueOf(r7)
            r0.denominator = r1
            goto L_0x00d7
        L_0x00cb:
            java.math.BigInteger r1 = java.math.BigInteger.valueOf(r12)
            r0.numerator = r1
            java.math.BigInteger r1 = java.math.BigInteger.valueOf(r16)
            r0.denominator = r1
        L_0x00d7:
            return
        L_0x00d8:
            org.apache.commons.math3.fraction.FractionConversionException r1 = new org.apache.commons.math3.fraction.FractionConversionException
            r1.<init>(r2, r4)
            throw r1
        L_0x00de:
            org.apache.commons.math3.fraction.FractionConversionException r9 = new org.apache.commons.math3.fraction.FractionConversionException
            r1 = r9
            r2 = r35
            r4 = r5
            r6 = r7
            r1.<init>(r2, r4, r6)
            throw r9
        L_0x00e9:
            org.apache.commons.math3.fraction.FractionConversionException r8 = new org.apache.commons.math3.fraction.FractionConversionException
            r9 = 1
            r1 = r8
            r2 = r35
            r4 = r5
            r6 = r9
            r1.<init>(r2, r4, r6)
            goto L_0x00f7
        L_0x00f6:
            throw r8
        L_0x00f7:
            goto L_0x00f6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.fraction.BigFraction.<init>(double, double, int, int):void");
    }

    public BigFraction(double d, int i) throws FractionConversionException {
        this(d, 0.0d, i, 100);
    }

    public BigFraction(int i) {
        this(BigInteger.valueOf((long) i), BigInteger.ONE);
    }

    public BigFraction(int i, int i2) {
        this(BigInteger.valueOf((long) i), BigInteger.valueOf((long) i2));
    }

    public BigFraction(long j) {
        this(BigInteger.valueOf(j), BigInteger.ONE);
    }

    public BigFraction(long j, long j2) {
        this(BigInteger.valueOf(j), BigInteger.valueOf(j2));
    }

    public static BigFraction getReducedFraction(int i, int i2) {
        if (i == 0) {
            return ZERO;
        }
        return new BigFraction(i, i2);
    }

    public BigFraction abs() {
        return BigInteger.ZERO.compareTo(this.numerator) <= 0 ? this : negate();
    }

    public BigFraction add(BigInteger bigInteger) throws NullArgumentException {
        MathUtils.checkNotNull(bigInteger);
        return new BigFraction(this.numerator.add(this.denominator.multiply(bigInteger)), this.denominator);
    }

    public BigFraction add(int i) {
        return add(BigInteger.valueOf((long) i));
    }

    public BigFraction add(long j) {
        return add(BigInteger.valueOf(j));
    }

    public BigFraction add(BigFraction bigFraction) {
        BigInteger bigInteger;
        BigInteger bigInteger2;
        if (bigFraction == null) {
            throw new NullArgumentException(LocalizedFormats.FRACTION, new Object[0]);
        } else if (ZERO.equals(bigFraction)) {
            return this;
        } else {
            if (this.denominator.equals(bigFraction.denominator)) {
                bigInteger = this.numerator.add(bigFraction.numerator);
                bigInteger2 = this.denominator;
            } else {
                BigInteger add = this.numerator.multiply(bigFraction.denominator).add(bigFraction.numerator.multiply(this.denominator));
                bigInteger2 = this.denominator.multiply(bigFraction.denominator);
                bigInteger = add;
            }
            return new BigFraction(bigInteger, bigInteger2);
        }
    }

    public BigDecimal bigDecimalValue() {
        return new BigDecimal(this.numerator).divide(new BigDecimal(this.denominator));
    }

    public BigDecimal bigDecimalValue(int i) {
        return new BigDecimal(this.numerator).divide(new BigDecimal(this.denominator), i);
    }

    public BigDecimal bigDecimalValue(int i, int i2) {
        return new BigDecimal(this.numerator).divide(new BigDecimal(this.denominator), i, i2);
    }

    public int compareTo(BigFraction bigFraction) {
        return this.numerator.multiply(bigFraction.denominator).compareTo(this.denominator.multiply(bigFraction.numerator));
    }

    public BigFraction divide(BigInteger bigInteger) {
        if (bigInteger == null) {
            throw new NullArgumentException(LocalizedFormats.FRACTION, new Object[0]);
        } else if (!BigInteger.ZERO.equals(bigInteger)) {
            return new BigFraction(this.numerator, this.denominator.multiply(bigInteger));
        } else {
            throw new MathArithmeticException(LocalizedFormats.ZERO_DENOMINATOR, new Object[0]);
        }
    }

    public BigFraction divide(int i) {
        return divide(BigInteger.valueOf((long) i));
    }

    public BigFraction divide(long j) {
        return divide(BigInteger.valueOf(j));
    }

    public BigFraction divide(BigFraction bigFraction) {
        if (bigFraction == null) {
            throw new NullArgumentException(LocalizedFormats.FRACTION, new Object[0]);
        } else if (!BigInteger.ZERO.equals(bigFraction.numerator)) {
            return multiply(bigFraction.reciprocal());
        } else {
            throw new MathArithmeticException(LocalizedFormats.ZERO_DENOMINATOR, new Object[0]);
        }
    }

    public double doubleValue() {
        double doubleValue = this.numerator.doubleValue() / this.denominator.doubleValue();
        if (!Double.isNaN(doubleValue)) {
            return doubleValue;
        }
        int max = FastMath.max(this.numerator.bitLength(), this.denominator.bitLength()) - FastMath.getExponent(Double.MAX_VALUE);
        return this.numerator.shiftRight(max).doubleValue() / this.denominator.shiftRight(max).doubleValue();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof BigFraction) {
            BigFraction reduce = ((BigFraction) obj).reduce();
            BigFraction reduce2 = reduce();
            if (!reduce2.numerator.equals(reduce.numerator) || !reduce2.denominator.equals(reduce.denominator)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public float floatValue() {
        float floatValue = this.numerator.floatValue() / this.denominator.floatValue();
        if (!Double.isNaN((double) floatValue)) {
            return floatValue;
        }
        int max = FastMath.max(this.numerator.bitLength(), this.denominator.bitLength()) - FastMath.getExponent(Float.MAX_VALUE);
        return this.numerator.shiftRight(max).floatValue() / this.denominator.shiftRight(max).floatValue();
    }

    public BigInteger getDenominator() {
        return this.denominator;
    }

    public int getDenominatorAsInt() {
        return this.denominator.intValue();
    }

    public long getDenominatorAsLong() {
        return this.denominator.longValue();
    }

    public BigInteger getNumerator() {
        return this.numerator;
    }

    public int getNumeratorAsInt() {
        return this.numerator.intValue();
    }

    public long getNumeratorAsLong() {
        return this.numerator.longValue();
    }

    public int hashCode() {
        return ((this.numerator.hashCode() + 629) * 37) + this.denominator.hashCode();
    }

    public int intValue() {
        return this.numerator.divide(this.denominator).intValue();
    }

    public long longValue() {
        return this.numerator.divide(this.denominator).longValue();
    }

    public BigFraction multiply(BigInteger bigInteger) {
        if (bigInteger != null) {
            return new BigFraction(bigInteger.multiply(this.numerator), this.denominator);
        }
        throw new NullArgumentException();
    }

    public BigFraction multiply(int i) {
        return multiply(BigInteger.valueOf((long) i));
    }

    public BigFraction multiply(long j) {
        return multiply(BigInteger.valueOf(j));
    }

    public BigFraction multiply(BigFraction bigFraction) {
        if (bigFraction == null) {
            throw new NullArgumentException(LocalizedFormats.FRACTION, new Object[0]);
        } else if (this.numerator.equals(BigInteger.ZERO) || bigFraction.numerator.equals(BigInteger.ZERO)) {
            return ZERO;
        } else {
            return new BigFraction(this.numerator.multiply(bigFraction.numerator), this.denominator.multiply(bigFraction.denominator));
        }
    }

    public BigFraction negate() {
        return new BigFraction(this.numerator.negate(), this.denominator);
    }

    public double percentageValue() {
        return multiply(ONE_HUNDRED).doubleValue();
    }

    public BigFraction pow(int i) {
        if (i >= 0) {
            return new BigFraction(this.numerator.pow(i), this.denominator.pow(i));
        }
        int i2 = -i;
        return new BigFraction(this.denominator.pow(i2), this.numerator.pow(i2));
    }

    public BigFraction pow(long j) {
        if (j >= 0) {
            return new BigFraction(ArithmeticUtils.pow(this.numerator, j), ArithmeticUtils.pow(this.denominator, j));
        }
        long j2 = -j;
        return new BigFraction(ArithmeticUtils.pow(this.denominator, j2), ArithmeticUtils.pow(this.numerator, j2));
    }

    public BigFraction pow(BigInteger bigInteger) {
        if (bigInteger.compareTo(BigInteger.ZERO) >= 0) {
            return new BigFraction(ArithmeticUtils.pow(this.numerator, bigInteger), ArithmeticUtils.pow(this.denominator, bigInteger));
        }
        BigInteger negate = bigInteger.negate();
        return new BigFraction(ArithmeticUtils.pow(this.denominator, negate), ArithmeticUtils.pow(this.numerator, negate));
    }

    public double pow(double d) {
        return FastMath.pow(this.numerator.doubleValue(), d) / FastMath.pow(this.denominator.doubleValue(), d);
    }

    public BigFraction reciprocal() {
        return new BigFraction(this.denominator, this.numerator);
    }

    public BigFraction reduce() {
        BigInteger gcd = this.numerator.gcd(this.denominator);
        return new BigFraction(this.numerator.divide(gcd), this.denominator.divide(gcd));
    }

    public BigFraction subtract(BigInteger bigInteger) {
        if (bigInteger != null) {
            return new BigFraction(this.numerator.subtract(this.denominator.multiply(bigInteger)), this.denominator);
        }
        throw new NullArgumentException();
    }

    public BigFraction subtract(int i) {
        return subtract(BigInteger.valueOf((long) i));
    }

    public BigFraction subtract(long j) {
        return subtract(BigInteger.valueOf(j));
    }

    public BigFraction subtract(BigFraction bigFraction) {
        BigInteger bigInteger;
        BigInteger bigInteger2;
        if (bigFraction == null) {
            throw new NullArgumentException(LocalizedFormats.FRACTION, new Object[0]);
        } else if (ZERO.equals(bigFraction)) {
            return this;
        } else {
            if (this.denominator.equals(bigFraction.denominator)) {
                bigInteger = this.numerator.subtract(bigFraction.numerator);
                bigInteger2 = this.denominator;
            } else {
                BigInteger subtract = this.numerator.multiply(bigFraction.denominator).subtract(bigFraction.numerator.multiply(this.denominator));
                bigInteger2 = this.denominator.multiply(bigFraction.denominator);
                bigInteger = subtract;
            }
            return new BigFraction(bigInteger, bigInteger2);
        }
    }

    public String toString() {
        if (BigInteger.ONE.equals(this.denominator)) {
            return this.numerator.toString();
        }
        if (BigInteger.ZERO.equals(this.numerator)) {
            return "0";
        }
        return this.numerator + " / " + this.denominator;
    }

    public BigFractionField getField() {
        return BigFractionField.getInstance();
    }
}
