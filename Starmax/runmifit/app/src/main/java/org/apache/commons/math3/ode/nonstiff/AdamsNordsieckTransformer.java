package org.apache.commons.math3.ode.nonstiff;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.math3.FieldElement;
import org.apache.commons.math3.fraction.BigFraction;
import org.apache.commons.math3.linear.Array2DRowFieldMatrix;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayFieldVector;
import org.apache.commons.math3.linear.FieldDecompositionSolver;
import org.apache.commons.math3.linear.FieldLUDecomposition;
import org.apache.commons.math3.linear.FieldMatrix;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.QRDecomposition;

public class AdamsNordsieckTransformer {
    private static final Map<Integer, AdamsNordsieckTransformer> CACHE = new HashMap();

    /* renamed from: c1 */
    private final double[] f8070c1;
    private final Array2DRowRealMatrix update;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.FieldElement[], boolean):void
     arg types: [org.apache.commons.math3.fraction.BigFraction[], int]
     candidates:
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(int, org.apache.commons.math3.FieldElement):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.Field, int):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.Field, org.apache.commons.math3.FieldElement[]):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.linear.ArrayFieldVector, org.apache.commons.math3.linear.ArrayFieldVector):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.linear.ArrayFieldVector, boolean):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.linear.ArrayFieldVector, org.apache.commons.math3.FieldElement[]):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.linear.FieldVector, org.apache.commons.math3.linear.FieldVector):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.linear.FieldVector, org.apache.commons.math3.FieldElement[]):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.FieldElement[], org.apache.commons.math3.linear.ArrayFieldVector):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.FieldElement[], org.apache.commons.math3.linear.FieldVector):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.FieldElement[], org.apache.commons.math3.FieldElement[]):void
      org.apache.commons.math3.linear.ArrayFieldVector.<init>(org.apache.commons.math3.FieldElement[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowFieldMatrix.<init>(org.apache.commons.math3.FieldElement[][], boolean):void
     arg types: [org.apache.commons.math3.fraction.BigFraction[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowFieldMatrix.<init>(org.apache.commons.math3.Field, org.apache.commons.math3.FieldElement[]):void
      org.apache.commons.math3.linear.Array2DRowFieldMatrix.<init>(org.apache.commons.math3.Field, org.apache.commons.math3.FieldElement[][]):void
      org.apache.commons.math3.linear.Array2DRowFieldMatrix.<init>(org.apache.commons.math3.FieldElement[][], boolean):void */
    private AdamsNordsieckTransformer(int i) {
        FieldMatrix<BigFraction> buildP = buildP(i);
        FieldDecompositionSolver solver = new FieldLUDecomposition(buildP).getSolver();
        BigFraction[] bigFractionArr = new BigFraction[i];
        Arrays.fill(bigFractionArr, BigFraction.ONE);
        BigFraction[] bigFractionArr2 = (BigFraction[]) solver.solve(new ArrayFieldVector((FieldElement[]) bigFractionArr, false)).toArray();
        BigFraction[][] bigFractionArr3 = (BigFraction[][]) buildP.getData();
        for (int length = bigFractionArr3.length - 1; length > 0; length--) {
            bigFractionArr3[length] = bigFractionArr3[length - 1];
        }
        bigFractionArr3[0] = new BigFraction[i];
        Arrays.fill(bigFractionArr3[0], BigFraction.ZERO);
        this.update = MatrixUtils.bigFractionMatrixToRealMatrix(solver.solve(new Array2DRowFieldMatrix((FieldElement[][]) bigFractionArr3, false)));
        this.f8070c1 = new double[i];
        for (int i2 = 0; i2 < i; i2++) {
            this.f8070c1[i2] = bigFractionArr2[i2].doubleValue();
        }
    }

    public static AdamsNordsieckTransformer getInstance(int i) {
        AdamsNordsieckTransformer adamsNordsieckTransformer;
        synchronized (CACHE) {
            adamsNordsieckTransformer = CACHE.get(Integer.valueOf(i));
            if (adamsNordsieckTransformer == null) {
                adamsNordsieckTransformer = new AdamsNordsieckTransformer(i);
                CACHE.put(Integer.valueOf(i), adamsNordsieckTransformer);
            }
        }
        return adamsNordsieckTransformer;
    }

    public int getNSteps() {
        return this.f8070c1.length;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowFieldMatrix.<init>(org.apache.commons.math3.FieldElement[][], boolean):void
     arg types: [org.apache.commons.math3.fraction.BigFraction[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowFieldMatrix.<init>(org.apache.commons.math3.Field, org.apache.commons.math3.FieldElement[]):void
      org.apache.commons.math3.linear.Array2DRowFieldMatrix.<init>(org.apache.commons.math3.Field, org.apache.commons.math3.FieldElement[][]):void
      org.apache.commons.math3.linear.Array2DRowFieldMatrix.<init>(org.apache.commons.math3.FieldElement[][], boolean):void */
    private FieldMatrix<BigFraction> buildP(int i) {
        BigFraction[][] bigFractionArr = (BigFraction[][]) Array.newInstance(BigFraction.class, i, i);
        int i2 = 0;
        while (i2 < bigFractionArr.length) {
            BigFraction[] bigFractionArr2 = bigFractionArr[i2];
            i2++;
            int i3 = -i2;
            int i4 = i3;
            for (int i5 = 0; i5 < bigFractionArr2.length; i5++) {
                bigFractionArr2[i5] = new BigFraction((i5 + 2) * i4);
                i4 *= i3;
            }
        }
        return new Array2DRowFieldMatrix((FieldElement[][]) bigFractionArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void
     arg types: [double[][], int]
     candidates:
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(int, int):void
      org.apache.commons.math3.linear.Array2DRowRealMatrix.<init>(double[][], boolean):void */
    public Array2DRowRealMatrix initializeHighOrderDerivatives(double d, double[] dArr, double[][] dArr2, double[][] dArr3) {
        double[][] dArr4 = dArr2;
        Class<double> cls = double.class;
        int i = 1;
        double[][] dArr5 = (double[][]) Array.newInstance((Class<?>) cls, (dArr4.length - 1) * 2, this.f8070c1.length);
        char c = 0;
        double[][] dArr6 = (double[][]) Array.newInstance((Class<?>) cls, (dArr4.length - 1) * 2, dArr4[0].length);
        double[] dArr7 = dArr4[0];
        double[] dArr8 = dArr3[0];
        int i2 = 1;
        while (i2 < dArr4.length) {
            double d2 = dArr[i2] - dArr[c];
            double d3 = d2 / d;
            int i3 = i2 * 2;
            int i4 = i3 - 2;
            double[] dArr9 = dArr5[i4];
            int i5 = i3 - i;
            double[] dArr10 = dArr5[i5];
            double d4 = 1.0d / d;
            for (int i6 = 0; i6 < dArr9.length; i6++) {
                d4 *= d3;
                dArr9[i6] = d2 * d4;
                double d5 = (double) (i6 + 2);
                Double.isNaN(d5);
                dArr10[i6] = d5 * d4;
            }
            double[] dArr11 = dArr4[i2];
            double[] dArr12 = dArr3[i2];
            double[] dArr13 = dArr6[i4];
            double[] dArr14 = dArr6[i5];
            for (int i7 = 0; i7 < dArr11.length; i7++) {
                dArr13[i7] = (dArr11[i7] - dArr7[i7]) - (dArr8[i7] * d2);
                dArr14[i7] = dArr12[i7] - dArr8[i7];
            }
            i2++;
            i = 1;
            c = 0;
        }
        return new Array2DRowRealMatrix(new QRDecomposition(new Array2DRowRealMatrix(dArr5, false)).getSolver().solve(new Array2DRowRealMatrix(dArr6, false)).getData(), false);
    }

    public Array2DRowRealMatrix updateHighOrderDerivativesPhase1(Array2DRowRealMatrix array2DRowRealMatrix) {
        return this.update.multiply(array2DRowRealMatrix);
    }

    public void updateHighOrderDerivativesPhase2(double[] dArr, double[] dArr2, Array2DRowRealMatrix array2DRowRealMatrix) {
        double[][] dataRef = array2DRowRealMatrix.getDataRef();
        for (int i = 0; i < dataRef.length; i++) {
            double[] dArr3 = dataRef[i];
            double d = this.f8070c1[i];
            for (int i2 = 0; i2 < dArr3.length; i2++) {
                dArr3[i2] = dArr3[i2] + ((dArr[i2] - dArr2[i2]) * d);
            }
        }
    }
}
