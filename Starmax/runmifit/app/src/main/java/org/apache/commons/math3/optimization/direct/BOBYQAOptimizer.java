package org.apache.commons.math3.optimization.direct;

import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.optimization.GoalType;
import org.apache.commons.math3.optimization.MultivariateOptimizer;
import org.apache.commons.math3.optimization.PointValuePair;
import org.apache.commons.math3.util.FastMath;

@Deprecated
public class BOBYQAOptimizer extends BaseAbstractMultivariateSimpleBoundsOptimizer<MultivariateFunction> implements MultivariateOptimizer {
    public static final double DEFAULT_INITIAL_RADIUS = 10.0d;
    public static final double DEFAULT_STOPPING_RADIUS = 1.0E-8d;
    private static final double HALF = 0.5d;
    public static final int MINIMUM_PROBLEM_DIMENSION = 2;
    private static final double MINUS_ONE = -1.0d;
    private static final double ONE = 1.0d;
    private static final double ONE_OVER_A_THOUSAND = 0.001d;
    private static final double ONE_OVER_EIGHT = 0.125d;
    private static final double ONE_OVER_FOUR = 0.25d;
    private static final double ONE_OVER_TEN = 0.1d;
    private static final double SIXTEEN = 16.0d;
    private static final double TEN = 10.0d;
    private static final double TWO = 2.0d;
    private static final double TWO_HUNDRED_FIFTY = 250.0d;
    private static final double ZERO = 0.0d;
    private ArrayRealVector alternativeNewPoint;
    private Array2DRowRealMatrix bMatrix;
    private double[] boundDifference;
    private ArrayRealVector currentBest;
    private ArrayRealVector fAtInterpolationPoints;
    private ArrayRealVector gradientAtTrustRegionCenter;
    private double initialTrustRegionRadius;
    private Array2DRowRealMatrix interpolationPoints;
    private boolean isMinimize;
    private ArrayRealVector lagrangeValuesAtNewPoint;
    private ArrayRealVector lowerDifference;
    private ArrayRealVector modelSecondDerivativesParameters;
    private ArrayRealVector modelSecondDerivativesValues;
    private ArrayRealVector newPoint;
    private final int numberOfInterpolationPoints;
    private ArrayRealVector originShift;
    private final double stoppingTrustRegionRadius;
    private ArrayRealVector trialStepPoint;
    private int trustRegionCenterInterpolationPointIndex;
    private ArrayRealVector trustRegionCenterOffset;
    private ArrayRealVector upperDifference;
    private Array2DRowRealMatrix zMatrix;

    private static void printMethod() {
    }

    private static void printState(int i) {
    }

    public BOBYQAOptimizer(int i) {
        this(i, 10.0d, 1.0E-8d);
    }

    public BOBYQAOptimizer(int i, double d, double d2) {
        super(null);
        this.numberOfInterpolationPoints = i;
        this.initialTrustRegionRadius = d;
        this.stoppingTrustRegionRadius = d2;
    }

    /* access modifiers changed from: protected */
    public PointValuePair doOptimize() {
        double[] lowerBound = getLowerBound();
        double[] upperBound = getUpperBound();
        setup(lowerBound, upperBound);
        this.isMinimize = getGoalType() == GoalType.MINIMIZE;
        this.currentBest = new ArrayRealVector(getStartPoint());
        double bobyqa = bobyqa(lowerBound, upperBound);
        double[] dataRef = this.currentBest.getDataRef();
        if (!this.isMinimize) {
            bobyqa = -bobyqa;
        }
        return new PointValuePair(dataRef, bobyqa);
    }

    private double bobyqa(double[] dArr, double[] dArr2) {
        printMethod();
        int dimension = this.currentBest.getDimension();
        for (int i = 0; i < dimension; i++) {
            double d = this.boundDifference[i];
            this.lowerDifference.setEntry(i, dArr[i] - this.currentBest.getEntry(i));
            this.upperDifference.setEntry(i, dArr2[i] - this.currentBest.getEntry(i));
            if (this.lowerDifference.getEntry(i) >= (-this.initialTrustRegionRadius)) {
                if (this.lowerDifference.getEntry(i) >= 0.0d) {
                    this.currentBest.setEntry(i, dArr[i]);
                    this.lowerDifference.setEntry(i, 0.0d);
                    this.upperDifference.setEntry(i, d);
                } else {
                    this.currentBest.setEntry(i, dArr[i] + this.initialTrustRegionRadius);
                    this.lowerDifference.setEntry(i, -this.initialTrustRegionRadius);
                    this.upperDifference.setEntry(i, FastMath.max(dArr2[i] - this.currentBest.getEntry(i), this.initialTrustRegionRadius));
                }
            } else if (this.upperDifference.getEntry(i) <= this.initialTrustRegionRadius) {
                if (this.upperDifference.getEntry(i) <= 0.0d) {
                    this.currentBest.setEntry(i, dArr2[i]);
                    this.lowerDifference.setEntry(i, -d);
                    this.upperDifference.setEntry(i, 0.0d);
                } else {
                    this.currentBest.setEntry(i, dArr2[i] - this.initialTrustRegionRadius);
                    this.lowerDifference.setEntry(i, FastMath.min(dArr[i] - this.currentBest.getEntry(i), -this.initialTrustRegionRadius));
                    this.upperDifference.setEntry(i, this.initialTrustRegionRadius);
                }
            }
        }
        return bobyqb(dArr, dArr2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x03f7  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x04ad  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x04b3  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0543  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0921  */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x0947  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x0987  */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x09d9  */
    /* JADX WARNING: Removed duplicated region for block: B:298:0x0a15  */
    /* JADX WARNING: Removed duplicated region for block: B:342:0x0bfa  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x0d1d  */
    /* JADX WARNING: Removed duplicated region for block: B:374:0x0d3e  */
    /* JADX WARNING: Removed duplicated region for block: B:420:0x0f7f  */
    /* JADX WARNING: Removed duplicated region for block: B:422:0x0f89  */
    /* JADX WARNING: Removed duplicated region for block: B:423:0x0fa6  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x037d  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x03da  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x03df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private double bobyqb(double[] r93, double[] r94) {
        /*
            r92 = this;
            r8 = r92
            printMethod()
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.currentBest
            int r9 = r0.getDimension()
            int r10 = r8.numberOfInterpolationPoints
            int r0 = r9 + 1
            int r11 = r10 - r0
            int r0 = r0 * r9
            int r12 = r0 / 2
            org.apache.commons.math3.linear.ArrayRealVector r13 = new org.apache.commons.math3.linear.ArrayRealVector
            r13.<init>(r9)
            org.apache.commons.math3.linear.ArrayRealVector r14 = new org.apache.commons.math3.linear.ArrayRealVector
            r14.<init>(r10)
            org.apache.commons.math3.linear.ArrayRealVector r15 = new org.apache.commons.math3.linear.ArrayRealVector
            r15.<init>(r10)
            r7 = 0
            r8.trustRegionCenterInterpolationPointIndex = r7
            r92.prelim(r93, r94)
            r0 = 0
            r1 = 0
        L_0x002d:
            if (r0 >= r9) goto L_0x0048
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.trustRegionCenterOffset
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.interpolationPoints
            int r5 = r8.trustRegionCenterInterpolationPointIndex
            double r4 = r4.getEntry(r5, r0)
            r3.setEntry(r0, r4)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.trustRegionCenterOffset
            double r3 = r3.getEntry(r0)
            double r3 = r3 * r3
            double r1 = r1 + r3
            int r0 = r0 + 1
            goto L_0x002d
        L_0x0048:
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.fAtInterpolationPoints
            double r3 = r0.getEntry(r7)
            int r0 = r92.getEvaluations()
            double r5 = r8.initialTrustRegionRadius
            r21 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            r7 = 60
            r36 = r0
            r28 = r1
            r30 = r3
            r1 = r5
            r37 = r1
            r45 = r21
            r47 = r45
            r0 = 20
            r3 = 0
            r5 = 0
            r6 = 0
            r32 = 0
            r34 = 0
            r39 = 0
            r41 = 0
            r43 = 0
            r49 = 0
            r51 = 0
            r53 = 0
            r55 = 0
            r57 = 0
        L_0x007f:
            r58 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            r60 = r12
            r12 = 20
            if (r0 == r12) goto L_0x0aea
            if (r0 == r7) goto L_0x0ad1
            r12 = 90
            if (r0 == r12) goto L_0x0a99
            r62 = 4591870180066957722(0x3fb999999999999a, double:0.1)
            r12 = 210(0xd2, float:2.94E-43)
            if (r0 == r12) goto L_0x0109
            r12 = 230(0xe6, float:3.22E-43)
            if (r0 == r12) goto L_0x0102
            r12 = 360(0x168, float:5.04E-43)
            if (r0 == r12) goto L_0x00f0
            r12 = 650(0x28a, float:9.11E-43)
            if (r0 == r12) goto L_0x00d6
            r12 = 680(0x2a8, float:9.53E-43)
            if (r0 == r12) goto L_0x00be
            r1 = 720(0x2d0, float:1.009E-42)
            if (r0 != r1) goto L_0x00ae
            r18 = 0
            goto L_0x0a2d
        L_0x00ae:
            org.apache.commons.math3.exception.MathIllegalStateException r0 = new org.apache.commons.math3.exception.MathIllegalStateException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.SIMPLE_MESSAGE
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r3 = "bobyqb"
            r12 = 0
            r2[r12] = r3
            r0.<init>(r1, r2)
            throw r0
        L_0x00be:
            r64 = r3
            r7 = r5
            r26 = r11
            r66 = r13
            r4 = r14
            r67 = r15
            r11 = 680(0x2a8, float:9.53E-43)
            r18 = 0
            r19 = 210(0xd2, float:2.94E-43)
            r13 = r1
            r15 = r10
            r2 = r43
            r10 = r60
            goto L_0x09d0
        L_0x00d6:
            r64 = r3
            r7 = r5
            r12 = r10
            r26 = r11
            r66 = r13
            r4 = r14
            r67 = r15
            r88 = r43
            r10 = r60
            r11 = 650(0x28a, float:9.11E-43)
            r18 = 0
            r19 = 210(0xd2, float:2.94E-43)
            r13 = r1
            r0 = r37
            goto L_0x091a
        L_0x00f0:
            r66 = r1
            r64 = r3
            r12 = r10
            r26 = r13
            r72 = r14
            r71 = r15
            r69 = r37
            r19 = 210(0xd2, float:2.94E-43)
            r13 = r11
            goto L_0x0375
        L_0x0102:
            r64 = r3
            r7 = 230(0xe6, float:3.22E-43)
            r19 = 210(0xd2, float:2.94E-43)
            goto L_0x013d
        L_0x0109:
            r12 = 0
            r19 = 210(0xd2, float:2.94E-43)
            printState(r19)
            double[] r0 = r8.altmov(r6, r3)
            r21 = r0[r12]
            r12 = 1
            r45 = r0[r12]
            r0 = 0
        L_0x0119:
            if (r0 >= r9) goto L_0x0137
            org.apache.commons.math3.linear.ArrayRealVector r12 = r8.trialStepPoint
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.newPoint
            double r47 = r7.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.trustRegionCenterOffset
            double r51 = r7.getEntry(r0)
            r64 = r3
            double r3 = r47 - r51
            r12.setEntry(r0, r3)
            int r0 = r0 + 1
            r3 = r64
            r7 = 60
            goto L_0x0119
        L_0x0137:
            r64 = r3
            r47 = r21
            r7 = 230(0xe6, float:3.22E-43)
        L_0x013d:
            printState(r7)
            r0 = 0
        L_0x0141:
            if (r0 >= r10) goto L_0x019b
            r66 = r1
            r26 = r13
            r1 = 0
            r3 = 0
            r12 = 0
            r21 = 0
        L_0x014e:
            if (r3 >= r9) goto L_0x0183
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.interpolationPoints
            double r51 = r4.getEntry(r0, r3)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.trialStepPoint
            double r68 = r4.getEntry(r3)
            double r51 = r51 * r68
            double r12 = r12 + r51
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.interpolationPoints
            double r51 = r4.getEntry(r0, r3)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.trustRegionCenterOffset
            double r68 = r4.getEntry(r3)
            double r51 = r51 * r68
            double r21 = r21 + r51
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.bMatrix
            double r51 = r4.getEntry(r0, r3)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.trialStepPoint
            double r68 = r4.getEntry(r3)
            double r51 = r51 * r68
            double r1 = r1 + r51
            int r3 = r3 + 1
            goto L_0x014e
        L_0x0183:
            double r3 = r12 * r58
            double r3 = r3 + r21
            double r3 = r3 * r12
            r15.setEntry(r0, r3)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.lagrangeValuesAtNewPoint
            r3.setEntry(r0, r1)
            r14.setEntry(r0, r12)
            int r0 = r0 + 1
            r13 = r26
            r1 = r66
            goto L_0x0141
        L_0x019b:
            r66 = r1
            r26 = r13
            r0 = 0
            r1 = 0
        L_0x01a2:
            if (r0 >= r11) goto L_0x01e3
            r3 = 0
            r12 = 0
        L_0x01a7:
            if (r3 >= r10) goto L_0x01ba
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.zMatrix
            double r21 = r4.getEntry(r3, r0)
            double r51 = r15.getEntry(r3)
            double r21 = r21 * r51
            double r12 = r12 + r21
            int r3 = r3 + 1
            goto L_0x01a7
        L_0x01ba:
            double r3 = r12 * r12
            double r1 = r1 - r3
            r3 = 0
        L_0x01be:
            if (r3 >= r10) goto L_0x01dc
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.lagrangeValuesAtNewPoint
            double r21 = r4.getEntry(r3)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r7 = r8.zMatrix
            double r51 = r7.getEntry(r3, r0)
            double r51 = r51 * r12
            r69 = r1
            double r1 = r21 + r51
            r4.setEntry(r3, r1)
            int r3 = r3 + 1
            r1 = r69
            r7 = 230(0xe6, float:3.22E-43)
            goto L_0x01be
        L_0x01dc:
            r69 = r1
            int r0 = r0 + 1
            r7 = 230(0xe6, float:3.22E-43)
            goto L_0x01a2
        L_0x01e3:
            r0 = 0
            r3 = 0
            r12 = 0
            r21 = 0
        L_0x01ea:
            if (r0 >= r9) goto L_0x0268
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.trialStepPoint
            double r51 = r7.getEntry(r0)
            double r51 = r51 * r51
            double r21 = r21 + r51
            r7 = 0
            r51 = 0
        L_0x01f9:
            if (r7 >= r10) goto L_0x0210
            double r69 = r15.getEntry(r7)
            r71 = r15
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.bMatrix
            double r72 = r15.getEntry(r7, r0)
            double r69 = r69 * r72
            double r51 = r51 + r69
            int r7 = r7 + 1
            r15 = r71
            goto L_0x01f9
        L_0x0210:
            r71 = r15
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.trialStepPoint
            double r69 = r7.getEntry(r0)
            double r69 = r69 * r51
            double r12 = r12 + r69
            int r7 = r10 + r0
            r69 = r10
            r70 = r11
            r10 = r51
            r15 = 0
        L_0x0225:
            if (r15 >= r9) goto L_0x023e
            r72 = r14
            org.apache.commons.math3.linear.Array2DRowRealMatrix r14 = r8.bMatrix
            double r51 = r14.getEntry(r7, r15)
            org.apache.commons.math3.linear.ArrayRealVector r14 = r8.trialStepPoint
            double r73 = r14.getEntry(r15)
            double r51 = r51 * r73
            double r10 = r10 + r51
            int r15 = r15 + 1
            r14 = r72
            goto L_0x0225
        L_0x023e:
            r72 = r14
            org.apache.commons.math3.linear.ArrayRealVector r14 = r8.lagrangeValuesAtNewPoint
            r14.setEntry(r7, r10)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.trialStepPoint
            double r14 = r7.getEntry(r0)
            double r10 = r10 * r14
            double r12 = r12 + r10
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.trialStepPoint
            double r10 = r7.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.trustRegionCenterOffset
            double r14 = r7.getEntry(r0)
            double r10 = r10 * r14
            double r3 = r3 + r10
            int r0 = r0 + 1
            r10 = r69
            r11 = r70
            r15 = r71
            r14 = r72
            goto L_0x01ea
        L_0x0268:
            r69 = r10
            r70 = r11
            r72 = r14
            r71 = r15
            double r10 = r3 * r3
            double r14 = r28 + r3
            double r14 = r14 + r3
            double r3 = r21 * r58
            double r14 = r14 + r3
            double r14 = r14 * r21
            double r10 = r10 + r14
            double r10 = r10 + r1
            double r51 = r10 - r12
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.lagrangeValuesAtNewPoint
            int r1 = r8.trustRegionCenterInterpolationPointIndex
            double r2 = r0.getEntry(r1)
            r10 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r2 = r2 + r10
            r0.setEntry(r1, r2)
            if (r5 != 0) goto L_0x02e7
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.lagrangeValuesAtNewPoint
            double r0 = r0.getEntry(r6)
            double r0 = r0 * r0
            double r2 = r47 * r51
            double r49 = r0 + r2
            int r0 = (r49 > r45 ? 1 : (r49 == r45 ? 0 : -1))
            if (r0 >= 0) goto L_0x02df
            r0 = 0
            int r2 = (r45 > r0 ? 1 : (r45 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x02df
            r0 = 0
        L_0x02a5:
            if (r0 >= r9) goto L_0x02c7
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.newPoint
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.alternativeNewPoint
            double r2 = r2.getEntry(r0)
            r1.setEntry(r0, r2)
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.trialStepPoint
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.newPoint
            double r2 = r2.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.trustRegionCenterOffset
            double r10 = r4.getEntry(r0)
            double r2 = r2 - r10
            r1.setEntry(r0, r2)
            int r0 = r0 + 1
            goto L_0x02a5
        L_0x02c7:
            r13 = r26
            r12 = r60
            r3 = r64
            r1 = r66
            r10 = r69
            r11 = r70
            r15 = r71
            r14 = r72
            r0 = 230(0xe6, float:3.22E-43)
            r7 = 60
            r45 = 0
            goto L_0x007f
        L_0x02df:
            r12 = r69
            r13 = r70
            r69 = r37
            goto L_0x0375
        L_0x02e7:
            r10 = r37
            double r37 = r10 * r10
            r12 = r69
            r0 = 0
            r1 = 0
            r3 = 0
            r6 = 0
        L_0x02f3:
            if (r0 >= r12) goto L_0x036f
            int r7 = r8.trustRegionCenterInterpolationPointIndex
            if (r0 != r7) goto L_0x02ff
            r13 = r70
            r69 = r10
            goto L_0x0368
        L_0x02ff:
            r13 = r70
            r7 = 0
            r14 = 0
        L_0x0304:
            if (r7 >= r13) goto L_0x0317
            r32 = r6
            org.apache.commons.math3.linear.Array2DRowRealMatrix r6 = r8.zMatrix
            double r53 = r6.getEntry(r0, r7)
            double r53 = r53 * r53
            double r14 = r14 + r53
            int r7 = r7 + 1
            r6 = r32
            goto L_0x0304
        L_0x0317:
            r32 = r6
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.lagrangeValuesAtNewPoint
            double r6 = r6.getEntry(r0)
            double r14 = r14 * r51
            double r6 = r6 * r6
            double r6 = r6 + r14
            r14 = 0
            r53 = 0
        L_0x0327:
            if (r14 >= r9) goto L_0x033e
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.interpolationPoints
            double r69 = r15.getEntry(r0, r14)
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.trustRegionCenterOffset
            double r73 = r15.getEntry(r14)
            double r69 = r69 - r73
            double r69 = r69 * r69
            double r53 = r53 + r69
            int r14 = r14 + 1
            goto L_0x0327
        L_0x033e:
            double r14 = r53 / r37
            r69 = r10
            r10 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r14 = r14 * r14
            double r10 = org.apache.commons.math3.util.FastMath.max(r10, r14)
            double r14 = r10 * r6
            int r33 = (r14 > r1 ? 1 : (r14 == r1 ? 0 : -1))
            if (r33 <= 0) goto L_0x0355
            r32 = r0
            r49 = r6
            goto L_0x0356
        L_0x0355:
            r14 = r1
        L_0x0356:
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.lagrangeValuesAtNewPoint
            double r1 = r1.getEntry(r0)
            double r1 = r1 * r1
            double r10 = r10 * r1
            double r1 = org.apache.commons.math3.util.FastMath.max(r3, r10)
            r3 = r1
            r1 = r14
            r6 = r32
        L_0x0368:
            int r0 = r0 + 1
            r10 = r69
            r70 = r13
            goto L_0x02f3
        L_0x036f:
            r32 = r6
            r13 = r70
            r69 = r10
        L_0x0375:
            r0 = 360(0x168, float:5.04E-43)
            printState(r0)
            r0 = 0
        L_0x037b:
            if (r0 >= r9) goto L_0x03cc
            r1 = r93[r0]
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.originShift
            double r3 = r3.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.newPoint
            double r10 = r7.getEntry(r0)
            double r3 = r3 + r10
            double r1 = org.apache.commons.math3.util.FastMath.max(r1, r3)
            r3 = r94[r0]
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.currentBest
            double r1 = org.apache.commons.math3.util.FastMath.min(r1, r3)
            r7.setEntry(r0, r1)
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.newPoint
            double r1 = r1.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.lowerDifference
            double r3 = r3.getEntry(r0)
            int r7 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r7 != 0) goto L_0x03b2
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.currentBest
            r2 = r93[r0]
            r1.setEntry(r0, r2)
        L_0x03b2:
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.newPoint
            double r1 = r1.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.upperDifference
            double r3 = r3.getEntry(r0)
            int r7 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r7 != 0) goto L_0x03c9
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.currentBest
            r2 = r94[r0]
            r1.setEntry(r0, r2)
        L_0x03c9:
            int r0 = r0 + 1
            goto L_0x037b
        L_0x03cc:
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.currentBest
            double[] r0 = r0.toArray()
            double r0 = r8.computeObjectiveValue(r0)
            boolean r2 = r8.isMinimize
            if (r2 != 0) goto L_0x03db
            double r0 = -r0
        L_0x03db:
            r10 = r0
            r0 = -1
            if (r5 != r0) goto L_0x03f7
            r0 = 720(0x2d0, float:1.009E-42)
            r30 = r10
            r32 = r30
            r10 = r12
            r11 = r13
            r13 = r26
            r12 = r60
            r3 = r64
            r1 = r66
            r37 = r69
            r15 = r71
            r14 = r72
            goto L_0x0fc2
        L_0x03f7:
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.fAtInterpolationPoints
            int r1 = r8.trustRegionCenterInterpolationPointIndex
            double r14 = r0.getEntry(r1)
            r0 = 0
            r1 = 0
            r3 = 0
        L_0x0403:
            if (r0 >= r9) goto L_0x0440
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.trialStepPoint
            double r32 = r4.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.gradientAtTrustRegionCenter
            double r34 = r4.getEntry(r0)
            double r32 = r32 * r34
            double r1 = r1 + r32
            r4 = r3
            r2 = r1
            r1 = 0
        L_0x0418:
            if (r1 > r0) goto L_0x043b
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.trialStepPoint
            double r32 = r7.getEntry(r1)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.trialStepPoint
            double r34 = r7.getEntry(r0)
            double r32 = r32 * r34
            if (r1 != r0) goto L_0x042c
            double r32 = r32 * r58
        L_0x042c:
            org.apache.commons.math3.linear.ArrayRealVector r7 = r8.modelSecondDerivativesValues
            double r34 = r7.getEntry(r4)
            double r34 = r34 * r32
            double r2 = r2 + r34
            int r4 = r4 + 1
            int r1 = r1 + 1
            goto L_0x0418
        L_0x043b:
            int r0 = r0 + 1
            r1 = r2
            r3 = r4
            goto L_0x0403
        L_0x0440:
            r32 = r1
            r0 = 0
        L_0x0443:
            if (r0 >= r12) goto L_0x045c
            r7 = r72
            double r1 = r7.getEntry(r0)
            double r1 = r1 * r1
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.modelSecondDerivativesParameters
            double r3 = r3.getEntry(r0)
            double r3 = r3 * r58
            double r3 = r3 * r1
            double r32 = r32 + r3
            int r0 = r0 + 1
            goto L_0x0443
        L_0x045c:
            r7 = r72
            double r0 = r10 - r14
            double r34 = r0 - r32
            double r37 = org.apache.commons.math3.util.FastMath.abs(r34)
            r3 = r43
            int r2 = (r3 > r66 ? 1 : (r3 == r66 ? 0 : -1))
            if (r2 <= 0) goto L_0x0472
            int r2 = r92.getEvaluations()
            r36 = r2
        L_0x0472:
            if (r5 <= 0) goto L_0x0563
            r16 = 0
            int r2 = (r32 > r16 ? 1 : (r32 == r16 ? 0 : -1))
            if (r2 >= 0) goto L_0x0550
            double r0 = r0 / r32
            r43 = r5
            r2 = r6
            double r5 = r69 * r58
            int r44 = (r0 > r62 ? 1 : (r0 == r62 ? 0 : -1))
            if (r44 > 0) goto L_0x048c
            double r5 = org.apache.commons.math3.util.FastMath.min(r5, r3)
        L_0x0489:
            r72 = r0
            goto L_0x04a5
        L_0x048c:
            r55 = 4604480259023595110(0x3fe6666666666666, double:0.7)
            int r44 = (r0 > r55 ? 1 : (r0 == r55 ? 0 : -1))
            if (r44 > 0) goto L_0x049a
            double r5 = org.apache.commons.math3.util.FastMath.max(r5, r3)
            goto L_0x0489
        L_0x049a:
            r55 = 4611686018427387904(0x4000000000000000, double:2.0)
            r72 = r0
            double r0 = r3 * r55
            double r0 = org.apache.commons.math3.util.FastMath.max(r5, r0)
            r5 = r0
        L_0x04a5:
            r0 = 4609434218613702656(0x3ff8000000000000, double:1.5)
            double r0 = r0 * r66
            int r44 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r44 > 0) goto L_0x04af
            r5 = r66
        L_0x04af:
            int r0 = (r10 > r14 ? 1 : (r10 == r14 ? 0 : -1))
            if (r0 >= 0) goto L_0x0543
            double r0 = r5 * r5
            r44 = r2
            r74 = r3
            r3 = r16
            r69 = r49
            r76 = r53
            r2 = 0
            r55 = 0
            r53 = r3
        L_0x04c4:
            if (r2 >= r12) goto L_0x052f
            r78 = r5
            r76 = r16
            r5 = 0
        L_0x04cb:
            if (r5 >= r13) goto L_0x04da
            org.apache.commons.math3.linear.Array2DRowRealMatrix r6 = r8.zMatrix
            double r80 = r6.getEntry(r2, r5)
            double r80 = r80 * r80
            double r76 = r76 + r80
            int r5 = r5 + 1
            goto L_0x04cb
        L_0x04da:
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.lagrangeValuesAtNewPoint
            double r5 = r5.getEntry(r2)
            double r76 = r76 * r51
            double r5 = r5 * r5
            double r76 = r76 + r5
            r80 = r16
            r5 = 0
        L_0x04e9:
            if (r5 >= r9) goto L_0x0500
            org.apache.commons.math3.linear.Array2DRowRealMatrix r6 = r8.interpolationPoints
            double r82 = r6.getEntry(r2, r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.newPoint
            double r84 = r6.getEntry(r5)
            double r82 = r82 - r84
            double r82 = r82 * r82
            double r80 = r80 + r82
            int r5 = r5 + 1
            goto L_0x04e9
        L_0x0500:
            double r5 = r80 / r0
            r82 = r0
            r0 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r5 = r5 * r5
            double r0 = org.apache.commons.math3.util.FastMath.max(r0, r5)
            double r5 = r0 * r76
            int r56 = (r5 > r53 ? 1 : (r5 == r53 ? 0 : -1))
            if (r56 <= 0) goto L_0x0518
            r55 = r2
            r53 = r5
            r69 = r76
        L_0x0518:
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.lagrangeValuesAtNewPoint
            double r5 = r5.getEntry(r2)
            double r5 = r5 * r5
            double r0 = r0 * r5
            double r3 = org.apache.commons.math3.util.FastMath.max(r3, r0)
            int r2 = r2 + 1
            r5 = r78
            r76 = r80
            r0 = r82
            goto L_0x04c4
        L_0x052f:
            r78 = r5
            double r3 = r3 * r58
            int r0 = (r53 > r3 ? 1 : (r53 == r3 ? 0 : -1))
            if (r0 > 0) goto L_0x053a
            r6 = r44
            goto L_0x053e
        L_0x053a:
            r6 = r55
            r49 = r69
        L_0x053e:
            r55 = r72
            r53 = r76
            goto L_0x054d
        L_0x0543:
            r44 = r2
            r74 = r3
            r78 = r5
            r6 = r44
            r55 = r72
        L_0x054d:
            r18 = 0
            goto L_0x056f
        L_0x0550:
            org.apache.commons.math3.exception.MathIllegalStateException r0 = new org.apache.commons.math3.exception.MathIllegalStateException
            org.apache.commons.math3.exception.util.LocalizedFormats r1 = org.apache.commons.math3.exception.util.LocalizedFormats.TRUST_REGION_STEP_FAILED
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.Double r3 = java.lang.Double.valueOf(r32)
            r18 = 0
            r2[r18] = r3
            r0.<init>(r1, r2)
            throw r0
        L_0x0563:
            r74 = r3
            r43 = r5
            r44 = r6
            r16 = 0
            r18 = 0
            r78 = r69
        L_0x056f:
            r0 = r92
            r3 = r66
            r1 = r51
            r86 = r3
            r88 = r74
            r3 = r49
            r72 = r7
            r7 = r43
            r90 = r14
            r14 = r16
            r16 = r90
            r5 = r6
            r0.update(r1, r3, r5)
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.modelSecondDerivativesParameters
            double r0 = r0.getEntry(r6)
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.modelSecondDerivativesParameters
            r2.setEntry(r6, r14)
            r2 = 0
            r3 = 0
        L_0x0596:
            if (r2 >= r9) goto L_0x05c8
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.interpolationPoints
            double r4 = r4.getEntry(r6, r2)
            double r4 = r4 * r0
            r14 = r3
            r3 = 0
        L_0x05a2:
            if (r3 > r2) goto L_0x05c0
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.modelSecondDerivativesValues
            double r66 = r15.getEntry(r14)
            r69 = r0
            org.apache.commons.math3.linear.Array2DRowRealMatrix r0 = r8.interpolationPoints
            double r0 = r0.getEntry(r6, r3)
            double r0 = r0 * r4
            double r0 = r66 + r0
            r15.setEntry(r14, r0)
            int r14 = r14 + 1
            int r3 = r3 + 1
            r0 = r69
            goto L_0x05a2
        L_0x05c0:
            r69 = r0
            int r2 = r2 + 1
            r3 = r14
            r14 = 0
            goto L_0x0596
        L_0x05c8:
            r0 = 0
        L_0x05c9:
            if (r0 >= r13) goto L_0x05ef
            org.apache.commons.math3.linear.Array2DRowRealMatrix r1 = r8.zMatrix
            double r1 = r1.getEntry(r6, r0)
            double r1 = r1 * r34
            r3 = 0
        L_0x05d4:
            if (r3 >= r12) goto L_0x05ec
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.modelSecondDerivativesParameters
            double r14 = r4.getEntry(r3)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r5 = r8.zMatrix
            double r66 = r5.getEntry(r3, r0)
            double r66 = r66 * r1
            double r14 = r14 + r66
            r4.setEntry(r3, r14)
            int r3 = r3 + 1
            goto L_0x05d4
        L_0x05ec:
            int r0 = r0 + 1
            goto L_0x05c9
        L_0x05ef:
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.fAtInterpolationPoints
            r0.setEntry(r6, r10)
            r0 = 0
        L_0x05f5:
            if (r0 >= r9) goto L_0x0610
            org.apache.commons.math3.linear.Array2DRowRealMatrix r1 = r8.interpolationPoints
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.newPoint
            double r2 = r2.getEntry(r0)
            r1.setEntry(r6, r0, r2)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r1 = r8.bMatrix
            double r1 = r1.getEntry(r6, r0)
            r14 = r26
            r14.setEntry(r0, r1)
            int r0 = r0 + 1
            goto L_0x05f5
        L_0x0610:
            r14 = r26
            r0 = 0
        L_0x0613:
            if (r0 >= r12) goto L_0x0660
            r1 = 0
            r2 = 0
        L_0x0618:
            if (r1 >= r13) goto L_0x062c
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.zMatrix
            double r4 = r4.getEntry(r6, r1)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.zMatrix
            double r66 = r15.getEntry(r0, r1)
            double r4 = r4 * r66
            double r2 = r2 + r4
            int r1 = r1 + 1
            goto L_0x0618
        L_0x062c:
            r1 = 0
            r4 = 0
        L_0x062f:
            if (r1 >= r9) goto L_0x0644
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.interpolationPoints
            double r66 = r15.getEntry(r0, r1)
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.trustRegionCenterOffset
            double r69 = r15.getEntry(r1)
            double r66 = r66 * r69
            double r4 = r4 + r66
            int r1 = r1 + 1
            goto L_0x062f
        L_0x0644:
            double r2 = r2 * r4
            r1 = 0
        L_0x0647:
            if (r1 >= r9) goto L_0x065d
            double r4 = r14.getEntry(r1)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.interpolationPoints
            double r66 = r15.getEntry(r0, r1)
            double r66 = r66 * r2
            double r4 = r4 + r66
            r14.setEntry(r1, r4)
            int r1 = r1 + 1
            goto L_0x0647
        L_0x065d:
            int r0 = r0 + 1
            goto L_0x0613
        L_0x0660:
            r0 = 0
        L_0x0661:
            if (r0 >= r9) goto L_0x0676
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.gradientAtTrustRegionCenter
            double r2 = r1.getEntry(r0)
            double r4 = r14.getEntry(r0)
            double r4 = r4 * r34
            double r2 = r2 + r4
            r1.setEntry(r0, r2)
            int r0 = r0 + 1
            goto L_0x0661
        L_0x0676:
            int r0 = (r10 > r16 ? 1 : (r10 == r16 ? 0 : -1))
            if (r0 >= 0) goto L_0x072d
            r8.trustRegionCenterInterpolationPointIndex = r6
            r0 = 0
            r1 = 0
            r3 = 0
        L_0x0680:
            if (r0 >= r9) goto L_0x06e2
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.trustRegionCenterOffset
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.newPoint
            r15 = r6
            double r5 = r5.getEntry(r0)
            r4.setEntry(r0, r5)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.trustRegionCenterOffset
            double r4 = r4.getEntry(r0)
            double r4 = r4 * r4
            double r1 = r1 + r4
            r4 = r3
            r3 = 0
        L_0x0699:
            if (r3 > r0) goto L_0x06db
            if (r3 >= r0) goto L_0x06b9
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.gradientAtTrustRegionCenter
            double r28 = r5.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.modelSecondDerivativesValues
            double r34 = r6.getEntry(r4)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trialStepPoint
            double r66 = r6.getEntry(r3)
            double r34 = r34 * r66
            r66 = r1
            double r1 = r28 + r34
            r5.setEntry(r0, r1)
            goto L_0x06bb
        L_0x06b9:
            r66 = r1
        L_0x06bb:
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.gradientAtTrustRegionCenter
            double r5 = r1.getEntry(r3)
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.modelSecondDerivativesValues
            double r28 = r2.getEntry(r4)
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.trialStepPoint
            double r34 = r2.getEntry(r0)
            double r28 = r28 * r34
            double r5 = r5 + r28
            r1.setEntry(r3, r5)
            int r4 = r4 + 1
            int r3 = r3 + 1
            r1 = r66
            goto L_0x0699
        L_0x06db:
            r66 = r1
            int r0 = r0 + 1
            r3 = r4
            r6 = r15
            goto L_0x0680
        L_0x06e2:
            r15 = r6
            r0 = 0
        L_0x06e4:
            if (r0 >= r12) goto L_0x0728
            r3 = 0
            r4 = 0
        L_0x06e9:
            if (r3 >= r9) goto L_0x06fe
            org.apache.commons.math3.linear.Array2DRowRealMatrix r6 = r8.interpolationPoints
            double r28 = r6.getEntry(r0, r3)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trialStepPoint
            double r34 = r6.getEntry(r3)
            double r28 = r28 * r34
            double r4 = r4 + r28
            int r3 = r3 + 1
            goto L_0x06e9
        L_0x06fe:
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.modelSecondDerivativesParameters
            double r28 = r3.getEntry(r0)
            double r4 = r4 * r28
            r3 = 0
        L_0x0707:
            if (r3 >= r9) goto L_0x0723
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.gradientAtTrustRegionCenter
            double r28 = r6.getEntry(r3)
            r34 = r1
            org.apache.commons.math3.linear.Array2DRowRealMatrix r1 = r8.interpolationPoints
            double r1 = r1.getEntry(r0, r3)
            double r1 = r1 * r4
            double r1 = r28 + r1
            r6.setEntry(r3, r1)
            int r3 = r3 + 1
            r1 = r34
            goto L_0x0707
        L_0x0723:
            r34 = r1
            int r0 = r0 + 1
            goto L_0x06e4
        L_0x0728:
            r34 = r1
            r28 = r34
            goto L_0x072e
        L_0x072d:
            r15 = r6
        L_0x072e:
            if (r7 <= 0) goto L_0x08c1
            r0 = 0
        L_0x0731:
            if (r0 >= r12) goto L_0x0751
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.lagrangeValuesAtNewPoint
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.fAtInterpolationPoints
            double r2 = r2.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.fAtInterpolationPoints
            int r5 = r8.trustRegionCenterInterpolationPointIndex
            double r4 = r4.getEntry(r5)
            double r2 = r2 - r4
            r1.setEntry(r0, r2)
            r5 = r71
            r1 = 0
            r5.setEntry(r0, r1)
            int r0 = r0 + 1
            goto L_0x0731
        L_0x0751:
            r5 = r71
            r0 = 0
        L_0x0754:
            if (r0 >= r13) goto L_0x078c
            r1 = 0
            r2 = 0
        L_0x0759:
            if (r1 >= r12) goto L_0x076e
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.zMatrix
            double r34 = r4.getEntry(r1, r0)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.lagrangeValuesAtNewPoint
            double r66 = r4.getEntry(r1)
            double r34 = r34 * r66
            double r2 = r2 + r34
            int r1 = r1 + 1
            goto L_0x0759
        L_0x076e:
            r1 = 0
        L_0x076f:
            if (r1 >= r12) goto L_0x0789
            double r34 = r5.getEntry(r1)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.zMatrix
            double r66 = r4.getEntry(r1, r0)
            double r66 = r66 * r2
            r69 = r2
            double r2 = r34 + r66
            r5.setEntry(r1, r2)
            int r1 = r1 + 1
            r2 = r69
            goto L_0x076f
        L_0x0789:
            int r0 = r0 + 1
            goto L_0x0754
        L_0x078c:
            r0 = 0
        L_0x078d:
            if (r0 >= r12) goto L_0x07c4
            r1 = 0
            r2 = 0
        L_0x0792:
            if (r1 >= r9) goto L_0x07a7
            org.apache.commons.math3.linear.Array2DRowRealMatrix r4 = r8.interpolationPoints
            double r34 = r4.getEntry(r0, r1)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.trustRegionCenterOffset
            double r66 = r4.getEntry(r1)
            double r34 = r34 * r66
            double r2 = r2 + r34
            int r1 = r1 + 1
            goto L_0x0792
        L_0x07a7:
            r26 = r13
            r66 = r14
            double r13 = r5.getEntry(r0)
            r4 = r72
            r4.setEntry(r0, r13)
            double r13 = r5.getEntry(r0)
            double r2 = r2 * r13
            r5.setEntry(r0, r2)
            int r0 = r0 + 1
            r13 = r26
            r14 = r66
            goto L_0x078d
        L_0x07c4:
            r26 = r13
            r66 = r14
            r4 = r72
            r0 = 0
            r1 = 0
            r13 = 0
        L_0x07cf:
            if (r0 >= r9) goto L_0x086b
            r71 = r10
            r3 = 0
            r10 = 0
        L_0x07d6:
            if (r3 >= r12) goto L_0x07f9
            org.apache.commons.math3.linear.Array2DRowRealMatrix r6 = r8.bMatrix
            double r34 = r6.getEntry(r3, r0)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.lagrangeValuesAtNewPoint
            double r69 = r6.getEntry(r3)
            double r34 = r34 * r69
            org.apache.commons.math3.linear.Array2DRowRealMatrix r6 = r8.interpolationPoints
            double r69 = r6.getEntry(r3, r0)
            double r73 = r5.getEntry(r3)
            double r69 = r69 * r73
            double r34 = r34 + r69
            double r10 = r10 + r34
            int r3 = r3 + 1
            goto L_0x07d6
        L_0x07f9:
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.trustRegionCenterOffset
            double r34 = r3.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.lowerDifference
            double r69 = r3.getEntry(r0)
            int r3 = (r34 > r69 ? 1 : (r34 == r69 ? 0 : -1))
            if (r3 != 0) goto L_0x0824
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.gradientAtTrustRegionCenter
            r67 = r5
            double r5 = r3.getEntry(r0)
            r73 = r4
            r3 = 0
            double r5 = org.apache.commons.math3.util.FastMath.min(r3, r5)
            double r5 = r5 * r5
            double r1 = r1 + r5
            double r5 = org.apache.commons.math3.util.FastMath.min(r3, r10)
            double r5 = r5 * r5
            double r13 = r13 + r5
            goto L_0x085a
        L_0x0824:
            r73 = r4
            r67 = r5
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.trustRegionCenterOffset
            double r3 = r3.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.upperDifference
            double r5 = r5.getEntry(r0)
            int r34 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r34 != 0) goto L_0x084e
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.gradientAtTrustRegionCenter
            double r3 = r3.getEntry(r0)
            r5 = 0
            double r3 = org.apache.commons.math3.util.FastMath.max(r5, r3)
            double r3 = r3 * r3
            double r1 = r1 + r3
            double r3 = org.apache.commons.math3.util.FastMath.max(r5, r10)
            double r3 = r3 * r3
            goto L_0x0859
        L_0x084e:
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.gradientAtTrustRegionCenter
            double r3 = r3.getEntry(r0)
            double r3 = r3 * r3
            double r1 = r1 + r3
            double r3 = r10 * r10
        L_0x0859:
            double r13 = r13 + r3
        L_0x085a:
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.lagrangeValuesAtNewPoint
            int r4 = r12 + r0
            r3.setEntry(r4, r10)
            int r0 = r0 + 1
            r5 = r67
            r10 = r71
            r4 = r73
            goto L_0x07cf
        L_0x086b:
            r73 = r4
            r67 = r5
            r71 = r10
            int r0 = r57 + 1
            r3 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r13 = r13 * r3
            int r3 = (r1 > r13 ? 1 : (r1 == r13 ? 0 : -1))
            if (r3 >= 0) goto L_0x087c
            r0 = 0
        L_0x087c:
            r1 = 3
            r10 = r60
            if (r0 < r1) goto L_0x08bc
            int r1 = org.apache.commons.math3.util.FastMath.max(r12, r10)
            r2 = r0
            r0 = 0
        L_0x0887:
            if (r0 >= r1) goto L_0x08b7
            if (r0 >= r9) goto L_0x0898
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.gradientAtTrustRegionCenter
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.lagrangeValuesAtNewPoint
            int r4 = r12 + r0
            double r3 = r3.getEntry(r4)
            r2.setEntry(r0, r3)
        L_0x0898:
            if (r0 >= r12) goto L_0x08a6
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.modelSecondDerivativesParameters
            r4 = r73
            double r5 = r4.getEntry(r0)
            r2.setEntry(r0, r5)
            goto L_0x08a8
        L_0x08a6:
            r4 = r73
        L_0x08a8:
            if (r0 >= r10) goto L_0x08b1
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.modelSecondDerivativesValues
            r5 = 0
            r2.setEntry(r0, r5)
        L_0x08b1:
            int r0 = r0 + 1
            r73 = r4
            r2 = 0
            goto L_0x0887
        L_0x08b7:
            r4 = r73
            r57 = r2
            goto L_0x08cd
        L_0x08bc:
            r4 = r73
            r57 = r0
            goto L_0x08cd
        L_0x08c1:
            r26 = r13
            r66 = r14
            r67 = r71
            r4 = r72
            r71 = r10
            r10 = r60
        L_0x08cd:
            if (r7 != 0) goto L_0x08f3
        L_0x08cf:
            r14 = r4
            r5 = r7
            r6 = r15
            r11 = r26
            r34 = r41
            r3 = r64
            r13 = r66
            r15 = r67
            r32 = r71
            r1 = r86
            r43 = r88
            r0 = 60
            r7 = 60
            r41 = r39
            r39 = r37
            r37 = r78
            r90 = r12
            r12 = r10
            r10 = r90
            goto L_0x007f
        L_0x08f3:
            double r32 = r32 * r62
            double r0 = r16 + r32
            int r2 = (r71 > r0 ? 1 : (r71 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x08fc
            goto L_0x08cf
        L_0x08fc:
            r0 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r0 = r0 * r78
            r2 = 4621819117588971520(0x4024000000000000, double:10.0)
            r13 = r86
            double r2 = r2 * r13
            double r0 = r0 * r0
            double r2 = r2 * r2
            double r53 = org.apache.commons.math3.util.FastMath.max(r0, r2)
            r34 = r41
            r32 = r71
            r0 = r78
            r11 = 650(0x28a, float:9.11E-43)
            r41 = r39
            r39 = r37
        L_0x091a:
            printState(r11)
            r2 = 0
            r6 = -1
        L_0x091f:
            if (r2 >= r12) goto L_0x0945
            r3 = 0
            r15 = 0
        L_0x0924:
            if (r3 >= r9) goto L_0x093b
            org.apache.commons.math3.linear.Array2DRowRealMatrix r5 = r8.interpolationPoints
            double r37 = r5.getEntry(r2, r3)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.trustRegionCenterOffset
            double r60 = r5.getEntry(r3)
            double r37 = r37 - r60
            double r37 = r37 * r37
            double r15 = r15 + r37
            int r3 = r3 + 1
            goto L_0x0924
        L_0x093b:
            int r3 = (r15 > r53 ? 1 : (r15 == r53 ? 0 : -1))
            if (r3 <= 0) goto L_0x0942
            r6 = r2
            r53 = r15
        L_0x0942:
            int r2 = r2 + 1
            goto L_0x091f
        L_0x0945:
            if (r6 < 0) goto L_0x0987
            double r2 = org.apache.commons.math3.util.FastMath.sqrt(r53)
            r5 = -1
            if (r7 != r5) goto L_0x0961
            double r0 = r0 * r62
            r15 = r12
            double r11 = r2 * r58
            double r0 = org.apache.commons.math3.util.FastMath.min(r0, r11)
            r11 = 4609434218613702656(0x3ff8000000000000, double:1.5)
            double r11 = r11 * r13
            int r5 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r5 > 0) goto L_0x0962
            r0 = r13
            goto L_0x0962
        L_0x0961:
            r15 = r12
        L_0x0962:
            double r2 = r2 * r62
            double r2 = org.apache.commons.math3.util.FastMath.min(r2, r0)
            double r2 = org.apache.commons.math3.util.FastMath.max(r2, r13)
            double r21 = r2 * r2
            r37 = r0
            r12 = r10
            r10 = r15
            r11 = r26
            r15 = r67
            r43 = r88
            r0 = 90
            r5 = 0
            r7 = 60
            r90 = r13
            r14 = r4
            r3 = r2
            r1 = r90
            r13 = r66
            goto L_0x007f
        L_0x0987:
            r15 = r12
            r2 = -1
            if (r7 != r2) goto L_0x099d
            r37 = r0
            r5 = r7
            r12 = r10
            r1 = r13
            r10 = r15
            r11 = r26
            r13 = r66
            r15 = r67
            r43 = r88
            r0 = 680(0x2a8, float:9.53E-43)
            goto L_0x0a26
        L_0x099d:
            r2 = 0
            int r5 = (r55 > r2 ? 1 : (r55 == r2 ? 0 : -1))
            if (r5 <= 0) goto L_0x09b5
            r37 = r0
            r5 = r7
            r12 = r10
            r1 = r13
            r10 = r15
            r11 = r26
            r13 = r66
            r15 = r67
            r43 = r88
        L_0x09b1:
            r0 = 60
            goto L_0x0a26
        L_0x09b5:
            r2 = r88
            double r11 = org.apache.commons.math3.util.FastMath.max(r0, r2)
            int r5 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            r37 = r0
            if (r5 <= 0) goto L_0x09ce
            r43 = r2
            r5 = r7
            r12 = r10
            r1 = r13
            r10 = r15
            r11 = r26
            r13 = r66
            r15 = r67
            goto L_0x09b1
        L_0x09ce:
            r11 = 680(0x2a8, float:9.53E-43)
        L_0x09d0:
            printState(r11)
            double r0 = r8.stoppingTrustRegionRadius
            int r5 = (r13 > r0 ? 1 : (r13 == r0 ? 0 : -1))
            if (r5 <= 0) goto L_0x0a15
            double r11 = r13 * r58
            double r55 = r13 / r0
            r16 = 4625196817309499392(0x4030000000000000, double:16.0)
            int r5 = (r55 > r16 ? 1 : (r55 == r16 ? 0 : -1))
            if (r5 > 0) goto L_0x09e4
            goto L_0x09f8
        L_0x09e4:
            r0 = 4643000109586448384(0x406f400000000000, double:250.0)
            int r5 = (r55 > r0 ? 1 : (r55 == r0 ? 0 : -1))
            if (r5 > 0) goto L_0x09f6
            double r0 = org.apache.commons.math3.util.FastMath.sqrt(r55)
            double r13 = r8.stoppingTrustRegionRadius
            double r0 = r0 * r13
            goto L_0x09f8
        L_0x09f6:
            double r0 = r13 * r62
        L_0x09f8:
            double r37 = org.apache.commons.math3.util.FastMath.max(r11, r0)
            int r36 = r92.getEvaluations()
            r43 = r2
            r14 = r4
            r12 = r10
            r10 = r15
            r11 = r26
            r3 = r64
            r13 = r66
            r15 = r67
            r5 = 0
            r7 = 60
            r1 = r0
            r0 = 60
            goto L_0x007f
        L_0x0a15:
            r11 = -1
            if (r7 != r11) goto L_0x0a2d
            r0 = 360(0x168, float:5.04E-43)
            r43 = r2
            r5 = r7
            r12 = r10
            r1 = r13
            r10 = r15
            r11 = r26
            r13 = r66
            r15 = r67
        L_0x0a26:
            r7 = 60
            r14 = r4
        L_0x0a29:
            r3 = r64
            goto L_0x007f
        L_0x0a2d:
            r0 = 720(0x2d0, float:1.009E-42)
            printState(r0)
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.fAtInterpolationPoints
            int r1 = r8.trustRegionCenterInterpolationPointIndex
            double r0 = r0.getEntry(r1)
            int r2 = (r0 > r30 ? 1 : (r0 == r30 ? 0 : -1))
            if (r2 > 0) goto L_0x0a98
            r0 = 0
        L_0x0a3f:
            if (r0 >= r9) goto L_0x0a90
            r1 = r93[r0]
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.originShift
            double r3 = r3.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.trustRegionCenterOffset
            double r5 = r5.getEntry(r0)
            double r3 = r3 + r5
            double r1 = org.apache.commons.math3.util.FastMath.max(r1, r3)
            r3 = r94[r0]
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.currentBest
            double r1 = org.apache.commons.math3.util.FastMath.min(r1, r3)
            r5.setEntry(r0, r1)
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.trustRegionCenterOffset
            double r1 = r1.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.lowerDifference
            double r3 = r3.getEntry(r0)
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x0a76
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.currentBest
            r2 = r93[r0]
            r1.setEntry(r0, r2)
        L_0x0a76:
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.trustRegionCenterOffset
            double r1 = r1.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.upperDifference
            double r3 = r3.getEntry(r0)
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x0a8d
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.currentBest
            r2 = r94[r0]
            r1.setEntry(r0, r2)
        L_0x0a8d:
            int r0 = r0 + 1
            goto L_0x0a3f
        L_0x0a90:
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.fAtInterpolationPoints
            int r1 = r8.trustRegionCenterInterpolationPointIndex
            double r32 = r0.getEntry(r1)
        L_0x0a98:
            return r32
        L_0x0a99:
            r64 = r3
            r7 = r5
            r26 = r11
            r66 = r13
            r4 = r14
            r67 = r15
            r18 = 0
            r19 = 210(0xd2, float:2.94E-43)
            r13 = r1
            r15 = r10
            r2 = r43
            r12 = r4
            r17 = r6
            r7 = r15
            r69 = r37
            r62 = r41
            r42 = r60
            r4 = r66
            r16 = 210(0xd2, float:2.94E-43)
            r19 = 90
            r20 = 20
            r23 = 650(0x28a, float:9.11E-43)
            r24 = 230(0xe6, float:3.22E-43)
            r25 = -1
            r27 = 60
            r37 = 0
            r60 = r21
            r40 = r39
            r22 = r67
            r21 = 680(0x2a8, float:9.53E-43)
            goto L_0x0d30
        L_0x0ad1:
            r64 = r3
            r7 = r5
            r26 = r11
            r66 = r13
            r4 = r14
            r67 = r15
            r69 = r37
            r18 = 0
            r19 = 210(0xd2, float:2.94E-43)
            r13 = r1
            r15 = r10
            r10 = r60
        L_0x0ae5:
            r11 = r15
        L_0x0ae6:
            r12 = 60
            goto L_0x0b9a
        L_0x0aea:
            r64 = r3
            r7 = r5
            r26 = r11
            r66 = r13
            r4 = r14
            r67 = r15
            r69 = r37
            r11 = -1
            r18 = 0
            r19 = 210(0xd2, float:2.94E-43)
            r13 = r1
            r15 = r10
            r10 = r60
            printState(r12)
            int r0 = r8.trustRegionCenterInterpolationPointIndex
            if (r0 == 0) goto L_0x0ae5
            r0 = 0
            r1 = 0
        L_0x0b08:
            if (r0 >= r9) goto L_0x0b51
            r2 = r1
            r1 = 0
        L_0x0b0c:
            if (r1 > r0) goto L_0x0b4a
            if (r1 >= r0) goto L_0x0b29
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.gradientAtTrustRegionCenter
            double r16 = r3.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.modelSecondDerivativesValues
            double r21 = r5.getEntry(r2)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.trustRegionCenterOffset
            double r37 = r5.getEntry(r1)
            double r21 = r21 * r37
            double r11 = r16 + r21
            r3.setEntry(r0, r11)
        L_0x0b29:
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.gradientAtTrustRegionCenter
            double r11 = r3.getEntry(r1)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.modelSecondDerivativesValues
            double r16 = r5.getEntry(r2)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.trustRegionCenterOffset
            double r21 = r5.getEntry(r0)
            double r16 = r16 * r21
            double r11 = r11 + r16
            r3.setEntry(r1, r11)
            int r2 = r2 + 1
            int r1 = r1 + 1
            r11 = -1
            r12 = 20
            goto L_0x0b0c
        L_0x0b4a:
            int r0 = r0 + 1
            r1 = r2
            r11 = -1
            r12 = 20
            goto L_0x0b08
        L_0x0b51:
            int r0 = r92.getEvaluations()
            r11 = r15
            if (r0 <= r11) goto L_0x0ae6
            r0 = 0
        L_0x0b59:
            if (r0 >= r11) goto L_0x0ae6
            r1 = 0
            r2 = 0
        L_0x0b5e:
            if (r1 >= r9) goto L_0x0b72
            org.apache.commons.math3.linear.Array2DRowRealMatrix r5 = r8.interpolationPoints
            double r15 = r5.getEntry(r0, r1)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.trustRegionCenterOffset
            double r21 = r5.getEntry(r1)
            double r15 = r15 * r21
            double r2 = r2 + r15
            int r1 = r1 + 1
            goto L_0x0b5e
        L_0x0b72:
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.modelSecondDerivativesParameters
            double r15 = r1.getEntry(r0)
            double r2 = r2 * r15
            r1 = 0
        L_0x0b7b:
            if (r1 >= r9) goto L_0x0b97
            org.apache.commons.math3.linear.ArrayRealVector r5 = r8.gradientAtTrustRegionCenter
            double r15 = r5.getEntry(r1)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r12 = r8.interpolationPoints
            double r21 = r12.getEntry(r0, r1)
            double r21 = r21 * r2
            r37 = r2
            double r2 = r15 + r21
            r5.setEntry(r1, r2)
            int r1 = r1 + 1
            r2 = r37
            goto L_0x0b7b
        L_0x0b97:
            int r0 = r0 + 1
            goto L_0x0b59
        L_0x0b9a:
            printState(r12)
            org.apache.commons.math3.linear.ArrayRealVector r3 = new org.apache.commons.math3.linear.ArrayRealVector
            r3.<init>(r9)
            org.apache.commons.math3.linear.ArrayRealVector r5 = new org.apache.commons.math3.linear.ArrayRealVector
            r5.<init>(r9)
            org.apache.commons.math3.linear.ArrayRealVector r15 = new org.apache.commons.math3.linear.ArrayRealVector
            r15.<init>(r9)
            org.apache.commons.math3.linear.ArrayRealVector r1 = new org.apache.commons.math3.linear.ArrayRealVector
            r1.<init>(r9)
            org.apache.commons.math3.linear.ArrayRealVector r2 = new org.apache.commons.math3.linear.ArrayRealVector
            r2.<init>(r9)
            r0 = r92
            r16 = r1
            r17 = r2
            r1 = r69
            r21 = r4
            r4 = r5
            r22 = r67
            r5 = r15
            r15 = r6
            r6 = r16
            r18 = r7
            r12 = r21
            r16 = 210(0xd2, float:2.94E-43)
            r19 = 90
            r20 = 20
            r21 = 680(0x2a8, float:9.53E-43)
            r23 = 650(0x28a, float:9.11E-43)
            r24 = 230(0xe6, float:3.22E-43)
            r25 = -1
            r27 = 60
            r37 = 0
            r7 = r17
            double[] r0 = r0.trsbox(r1, r3, r4, r5, r6, r7)
            r1 = r0[r37]
            r3 = 1
            r3 = r0[r3]
            double r5 = org.apache.commons.math3.util.FastMath.sqrt(r1)
            r60 = r1
            r0 = r69
            double r5 = org.apache.commons.math3.util.FastMath.min(r0, r5)
            double r62 = r13 * r58
            int r2 = (r5 > r62 ? 1 : (r5 == r62 ? 0 : -1))
            if (r2 >= 0) goto L_0x0d1d
            r17 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r17 = r17 * r13
            double r53 = r17 * r17
            int r2 = r92.getEvaluations()
            int r7 = r36 + 2
            if (r2 > r7) goto L_0x0c22
            r37 = r0
            r43 = r5
            r1 = r13
            r6 = r15
            r15 = r22
            r21 = r60
            r3 = r64
            r13 = r66
            r0 = 650(0x28a, float:9.11E-43)
            r5 = -1
            r7 = 60
            r14 = r12
            r12 = r10
            r10 = r11
            r11 = r26
            goto L_0x007f
        L_0x0c22:
            r69 = r0
            r2 = r10
            r7 = r11
            r0 = r39
            r38 = r5
            r5 = r41
            double r10 = org.apache.commons.math3.util.FastMath.max(r0, r5)
            r40 = r0
            r0 = r34
            double r10 = org.apache.commons.math3.util.FastMath.max(r10, r0)
            r17 = 4593671619917905920(0x3fc0000000000000, double:0.125)
            double r17 = r17 * r13
            double r17 = r17 * r13
            r34 = 0
            int r42 = (r3 > r34 ? 1 : (r3 == r34 ? 0 : -1))
            if (r42 <= 0) goto L_0x0c6d
            double r17 = r17 * r3
            int r3 = (r10 > r17 ? 1 : (r10 == r17 ? 0 : -1))
            if (r3 <= 0) goto L_0x0c6d
            r34 = r0
            r10 = r7
            r11 = r26
            r43 = r38
            r39 = r40
            r3 = r64
            r37 = r69
            r0 = 650(0x28a, float:9.11E-43)
            r7 = 60
            r41 = r5
            r6 = r15
            r15 = r22
            r21 = r60
            r5 = -1
            r90 = r12
            r12 = r2
            r1 = r13
            r13 = r66
            r14 = r90
            goto L_0x007f
        L_0x0c6d:
            double r10 = r10 / r13
            r3 = 0
        L_0x0c6f:
            if (r3 >= r9) goto L_0x0cf6
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.newPoint
            double r17 = r4.getEntry(r3)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r8.lowerDifference
            double r34 = r4.getEntry(r3)
            int r4 = (r17 > r34 ? 1 : (r17 == r34 ? 0 : -1))
            if (r4 != 0) goto L_0x0c8a
            r4 = r66
            double r17 = r4.getEntry(r3)
            r34 = r0
            goto L_0x0c90
        L_0x0c8a:
            r4 = r66
            r34 = r0
            r17 = r10
        L_0x0c90:
            org.apache.commons.math3.linear.ArrayRealVector r0 = r8.newPoint
            double r0 = r0.getEntry(r3)
            r42 = r2
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.upperDifference
            double r62 = r2.getEntry(r3)
            int r2 = (r0 > r62 ? 1 : (r0 == r62 ? 0 : -1))
            if (r2 != 0) goto L_0x0ca8
            double r0 = r4.getEntry(r3)
            double r0 = -r0
            goto L_0x0caa
        L_0x0ca8:
            r0 = r17
        L_0x0caa:
            int r2 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r2 >= 0) goto L_0x0ce4
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.modelSecondDerivativesValues
            int r17 = r3 * r3
            int r17 = r3 + r17
            r62 = r5
            int r5 = r17 / 2
            double r5 = r2.getEntry(r5)
            r2 = 0
        L_0x0cbd:
            if (r2 >= r7) goto L_0x0cd8
            r17 = r15
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.interpolationPoints
            double r66 = r15.getEntry(r2, r3)
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.modelSecondDerivativesParameters
            double r71 = r15.getEntry(r2)
            double r66 = r66 * r66
            double r71 = r71 * r66
            double r5 = r5 + r71
            int r2 = r2 + 1
            r15 = r17
            goto L_0x0cbd
        L_0x0cd8:
            r17 = r15
            double r5 = r5 * r58
            double r5 = r5 * r13
            double r0 = r0 + r5
            int r2 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r2 >= 0) goto L_0x0ce8
            goto L_0x0d00
        L_0x0ce4:
            r62 = r5
            r17 = r15
        L_0x0ce8:
            int r3 = r3 + 1
            r66 = r4
            r15 = r17
            r0 = r34
            r2 = r42
            r5 = r62
            goto L_0x0c6f
        L_0x0cf6:
            r34 = r0
            r42 = r2
            r62 = r5
            r17 = r15
            r4 = r66
        L_0x0d00:
            r10 = r7
            r1 = r13
            r6 = r17
            r15 = r22
            r11 = r26
            r43 = r38
            r39 = r40
            r21 = r60
            r37 = r69
            r0 = 680(0x2a8, float:9.53E-43)
            r5 = -1
            r7 = 60
            r13 = r4
            r14 = r12
            r12 = r42
            r41 = r62
            goto L_0x0a29
        L_0x0d1d:
            r69 = r0
            r7 = r11
            r17 = r15
            r62 = r41
            r4 = r66
            r42 = r10
            r40 = r39
            r38 = r5
            int r5 = r18 + 1
            r2 = r38
        L_0x0d30:
            printState(r19)
            r0 = 4562254508917369340(0x3f50624dd2f1a9fc, double:0.001)
            double r0 = r0 * r28
            int r6 = (r60 > r0 ? 1 : (r60 == r0 ? 0 : -1))
            if (r6 > 0) goto L_0x0f7f
            r0 = 4598175219545276416(0x3fd0000000000000, double:0.25)
            double r0 = r0 * r28
            r6 = 0
            r10 = 0
        L_0x0d45:
            if (r6 >= r7) goto L_0x0de8
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.modelSecondDerivativesParameters
            double r38 = r15.getEntry(r6)
            double r10 = r10 + r38
            r38 = -4620693217682128896(0xbfe0000000000000, double:-0.5)
            double r38 = r38 * r28
            r15 = 0
            r90 = r2
            r2 = r38
            r38 = r90
        L_0x0d5a:
            if (r15 >= r9) goto L_0x0d76
            r66 = r10
            org.apache.commons.math3.linear.Array2DRowRealMatrix r10 = r8.interpolationPoints
            double r10 = r10.getEntry(r6, r15)
            r86 = r13
            org.apache.commons.math3.linear.ArrayRealVector r13 = r8.trustRegionCenterOffset
            double r13 = r13.getEntry(r15)
            double r10 = r10 * r13
            double r2 = r2 + r10
            int r15 = r15 + 1
            r10 = r66
            r13 = r86
            goto L_0x0d5a
        L_0x0d76:
            r66 = r10
            r86 = r13
            r12.setEntry(r6, r2)
            double r10 = r2 * r58
            double r10 = r0 - r10
            r13 = 0
        L_0x0d82:
            if (r13 >= r9) goto L_0x0dde
            org.apache.commons.math3.linear.Array2DRowRealMatrix r14 = r8.bMatrix
            double r14 = r14.getEntry(r6, r13)
            r4.setEntry(r13, r14)
            org.apache.commons.math3.linear.ArrayRealVector r14 = r8.lagrangeValuesAtNewPoint
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.interpolationPoints
            double r71 = r15.getEntry(r6, r13)
            double r71 = r71 * r2
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.trustRegionCenterOffset
            double r73 = r15.getEntry(r13)
            double r73 = r73 * r10
            r75 = r2
            double r2 = r71 + r73
            r14.setEntry(r13, r2)
            int r2 = r7 + r13
            r3 = 0
        L_0x0da9:
            if (r3 > r13) goto L_0x0dd7
            org.apache.commons.math3.linear.Array2DRowRealMatrix r14 = r8.bMatrix
            double r71 = r14.getEntry(r2, r3)
            double r73 = r4.getEntry(r13)
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.lagrangeValuesAtNewPoint
            double r77 = r15.getEntry(r3)
            double r73 = r73 * r77
            double r71 = r71 + r73
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.lagrangeValuesAtNewPoint
            double r73 = r15.getEntry(r13)
            double r77 = r4.getEntry(r3)
            double r73 = r73 * r77
            r77 = r10
            double r10 = r71 + r73
            r14.setEntry(r2, r3, r10)
            int r3 = r3 + 1
            r10 = r77
            goto L_0x0da9
        L_0x0dd7:
            r77 = r10
            int r13 = r13 + 1
            r2 = r75
            goto L_0x0d82
        L_0x0dde:
            int r6 = r6 + 1
            r2 = r38
            r10 = r66
            r13 = r86
            goto L_0x0d45
        L_0x0de8:
            r38 = r2
            r86 = r13
            r2 = r26
            r3 = 0
        L_0x0def:
            if (r3 >= r2) goto L_0x0ead
            r6 = 0
            r13 = 0
            r28 = 0
        L_0x0df6:
            if (r6 >= r7) goto L_0x0e28
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.zMatrix
            double r66 = r15.getEntry(r6, r3)
            double r13 = r13 + r66
            org.apache.commons.math3.linear.ArrayRealVector r15 = r8.lagrangeValuesAtNewPoint
            double r66 = r12.getEntry(r6)
            r26 = r2
            org.apache.commons.math3.linear.Array2DRowRealMatrix r2 = r8.zMatrix
            double r71 = r2.getEntry(r6, r3)
            r73 = r12
            r74 = r13
            double r12 = r66 * r71
            r15.setEntry(r6, r12)
            org.apache.commons.math3.linear.ArrayRealVector r2 = r8.lagrangeValuesAtNewPoint
            double r12 = r2.getEntry(r6)
            double r28 = r28 + r12
            int r6 = r6 + 1
            r2 = r26
            r12 = r73
            r13 = r74
            goto L_0x0df6
        L_0x0e28:
            r26 = r2
            r73 = r12
            r2 = 0
        L_0x0e2d:
            if (r2 >= r9) goto L_0x0e7e
            double r66 = r0 * r13
            double r71 = r28 * r58
            double r66 = r66 - r71
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r71 = r6.getEntry(r2)
            double r66 = r66 * r71
            r6 = 0
            r90 = r0
            r0 = r66
            r66 = r90
        L_0x0e44:
            if (r6 >= r7) goto L_0x0e59
            org.apache.commons.math3.linear.ArrayRealVector r12 = r8.lagrangeValuesAtNewPoint
            double r71 = r12.getEntry(r6)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r12 = r8.interpolationPoints
            double r74 = r12.getEntry(r6, r2)
            double r71 = r71 * r74
            double r0 = r0 + r71
            int r6 = r6 + 1
            goto L_0x0e44
        L_0x0e59:
            r4.setEntry(r2, r0)
            r6 = 0
        L_0x0e5d:
            if (r6 >= r7) goto L_0x0e79
            org.apache.commons.math3.linear.Array2DRowRealMatrix r12 = r8.bMatrix
            double r71 = r12.getEntry(r6, r2)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r8.zMatrix
            double r74 = r15.getEntry(r6, r3)
            double r74 = r74 * r0
            r76 = r0
            double r0 = r71 + r74
            r12.setEntry(r6, r2, r0)
            int r6 = r6 + 1
            r0 = r76
            goto L_0x0e5d
        L_0x0e79:
            int r2 = r2 + 1
            r0 = r66
            goto L_0x0e2d
        L_0x0e7e:
            r66 = r0
            r0 = 0
        L_0x0e81:
            if (r0 >= r9) goto L_0x0ea3
            int r1 = r0 + r7
            double r12 = r4.getEntry(r0)
            r2 = 0
        L_0x0e8a:
            if (r2 > r0) goto L_0x0ea0
            org.apache.commons.math3.linear.Array2DRowRealMatrix r6 = r8.bMatrix
            double r14 = r6.getEntry(r1, r2)
            double r28 = r4.getEntry(r2)
            double r28 = r28 * r12
            double r14 = r14 + r28
            r6.setEntry(r1, r2, r14)
            int r2 = r2 + 1
            goto L_0x0e8a
        L_0x0ea0:
            int r0 = r0 + 1
            goto L_0x0e81
        L_0x0ea3:
            int r3 = r3 + 1
            r2 = r26
            r0 = r66
            r12 = r73
            goto L_0x0def
        L_0x0ead:
            r26 = r2
            r73 = r12
            r0 = 0
            r1 = 0
        L_0x0eb3:
            if (r0 >= r9) goto L_0x0f2d
            r2 = -4620693217682128896(0xbfe0000000000000, double:-0.5)
            double r2 = r2 * r10
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r12 = r6.getEntry(r0)
            double r2 = r2 * r12
            r4.setEntry(r0, r2)
            r2 = 0
        L_0x0ec5:
            if (r2 >= r7) goto L_0x0ef0
            double r12 = r4.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.modelSecondDerivativesParameters
            double r14 = r3.getEntry(r2)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r3 = r8.interpolationPoints
            double r28 = r3.getEntry(r2, r0)
            double r14 = r14 * r28
            double r12 = r12 + r14
            r4.setEntry(r0, r12)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r3 = r8.interpolationPoints
            double r12 = r3.getEntry(r2, r0)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r14 = r6.getEntry(r0)
            double r12 = r12 - r14
            r3.setEntry(r2, r0, r12)
            int r2 = r2 + 1
            goto L_0x0ec5
        L_0x0ef0:
            r2 = r1
            r1 = 0
        L_0x0ef2:
            if (r1 > r0) goto L_0x0f29
            org.apache.commons.math3.linear.ArrayRealVector r3 = r8.modelSecondDerivativesValues
            double r12 = r3.getEntry(r2)
            double r14 = r4.getEntry(r1)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r28 = r6.getEntry(r0)
            double r14 = r14 * r28
            double r12 = r12 + r14
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r14 = r6.getEntry(r1)
            double r28 = r4.getEntry(r0)
            double r14 = r14 * r28
            double r12 = r12 + r14
            r3.setEntry(r2, r12)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r3 = r8.bMatrix
            int r6 = r7 + r1
            int r12 = r7 + r0
            double r12 = r3.getEntry(r12, r1)
            r3.setEntry(r6, r0, r12)
            int r2 = r2 + 1
            int r1 = r1 + 1
            goto L_0x0ef2
        L_0x0f29:
            int r0 = r0 + 1
            r1 = r2
            goto L_0x0eb3
        L_0x0f2d:
            r0 = 0
        L_0x0f2e:
            if (r0 >= r9) goto L_0x0f7a
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.originShift
            double r2 = r1.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r10 = r6.getEntry(r0)
            double r2 = r2 + r10
            r1.setEntry(r0, r2)
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.newPoint
            double r2 = r1.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r10 = r6.getEntry(r0)
            double r2 = r2 - r10
            r1.setEntry(r0, r2)
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.lowerDifference
            double r2 = r1.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r10 = r6.getEntry(r0)
            double r2 = r2 - r10
            r1.setEntry(r0, r2)
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.upperDifference
            double r2 = r1.getEntry(r0)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r8.trustRegionCenterOffset
            double r10 = r6.getEntry(r0)
            double r2 = r2 - r10
            r1.setEntry(r0, r2)
            org.apache.commons.math3.linear.ArrayRealVector r1 = r8.trustRegionCenterOffset
            r2 = 0
            r1.setEntry(r0, r2)
            int r0 = r0 + 1
            goto L_0x0f2e
        L_0x0f7a:
            r2 = 0
            r28 = r2
            goto L_0x0f87
        L_0x0f7f:
            r38 = r2
            r73 = r12
            r86 = r13
            r2 = 0
        L_0x0f87:
            if (r5 != 0) goto L_0x0fa6
            r13 = r4
            r10 = r7
            r6 = r17
            r15 = r22
            r11 = r26
            r43 = r38
            r39 = r40
            r12 = r42
            r21 = r60
            r41 = r62
            r3 = r64
            r37 = r69
            r14 = r73
            r1 = r86
            r0 = 210(0xd2, float:2.94E-43)
            goto L_0x0fc2
        L_0x0fa6:
            r13 = r4
            r10 = r7
            r6 = r17
            r15 = r22
            r11 = r26
            r43 = r38
            r39 = r40
            r12 = r42
            r21 = r60
            r41 = r62
            r3 = r64
            r37 = r69
            r14 = r73
            r1 = r86
            r0 = 230(0xe6, float:3.22E-43)
        L_0x0fc2:
            r7 = 60
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optimization.direct.BOBYQAOptimizer.bobyqb(double[], double[]):double");
    }

    /* JADX WARNING: Removed duplicated region for block: B:150:0x0508  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0534 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private double[] altmov(int r52, double r53) {
        /*
            r51 = this;
            r0 = r51
            r1 = r52
            printMethod()
            org.apache.commons.math3.linear.ArrayRealVector r2 = r0.currentBest
            int r2 = r2.getDimension()
            int r3 = r0.numberOfInterpolationPoints
            org.apache.commons.math3.linear.ArrayRealVector r4 = new org.apache.commons.math3.linear.ArrayRealVector
            r4.<init>(r2)
            org.apache.commons.math3.linear.ArrayRealVector r5 = new org.apache.commons.math3.linear.ArrayRealVector
            r5.<init>(r3)
            org.apache.commons.math3.linear.ArrayRealVector r6 = new org.apache.commons.math3.linear.ArrayRealVector
            r6.<init>(r2)
            org.apache.commons.math3.linear.ArrayRealVector r7 = new org.apache.commons.math3.linear.ArrayRealVector
            r7.<init>(r2)
            r9 = 0
        L_0x0024:
            r10 = 0
            if (r9 >= r3) goto L_0x002e
            r5.setEntry(r9, r10)
            int r9 = r9 + 1
            goto L_0x0024
        L_0x002e:
            int r9 = r3 - r2
            r12 = 1
            int r9 = r9 - r12
            r13 = 0
        L_0x0033:
            if (r13 >= r9) goto L_0x005b
            org.apache.commons.math3.linear.Array2DRowRealMatrix r14 = r0.zMatrix
            double r14 = r14.getEntry(r1, r13)
            r8 = 0
        L_0x003c:
            if (r8 >= r3) goto L_0x0055
            double r16 = r5.getEntry(r8)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r12 = r0.zMatrix
            double r19 = r12.getEntry(r8, r13)
            double r19 = r19 * r14
            double r10 = r16 + r19
            r5.setEntry(r8, r10)
            int r8 = r8 + 1
            r10 = 0
            r12 = 1
            goto L_0x003c
        L_0x0055:
            int r13 = r13 + 1
            r10 = 0
            r12 = 1
            goto L_0x0033
        L_0x005b:
            double r8 = r5.getEntry(r1)
            r10 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r12 = r8 * r10
            r14 = 0
        L_0x0064:
            if (r14 >= r2) goto L_0x0074
            org.apache.commons.math3.linear.Array2DRowRealMatrix r15 = r0.bMatrix
            double r10 = r15.getEntry(r1, r14)
            r4.setEntry(r14, r10)
            int r14 = r14 + 1
            r10 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            goto L_0x0064
        L_0x0074:
            r10 = 0
        L_0x0075:
            if (r10 >= r3) goto L_0x00bb
            r11 = 0
            r14 = 0
        L_0x007a:
            if (r11 >= r2) goto L_0x0093
            r19 = r7
            org.apache.commons.math3.linear.Array2DRowRealMatrix r7 = r0.interpolationPoints
            double r23 = r7.getEntry(r10, r11)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trustRegionCenterOffset
            double r25 = r7.getEntry(r11)
            double r23 = r23 * r25
            double r14 = r14 + r23
            int r11 = r11 + 1
            r7 = r19
            goto L_0x007a
        L_0x0093:
            r19 = r7
            double r23 = r5.getEntry(r10)
            double r14 = r14 * r23
            r7 = 0
        L_0x009c:
            if (r7 >= r2) goto L_0x00b6
            double r23 = r4.getEntry(r7)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r11 = r0.interpolationPoints
            double r25 = r11.getEntry(r10, r7)
            double r25 = r25 * r14
            r27 = r14
            double r14 = r23 + r25
            r4.setEntry(r7, r14)
            int r7 = r7 + 1
            r14 = r27
            goto L_0x009c
        L_0x00b6:
            int r10 = r10 + 1
            r7 = r19
            goto L_0x0075
        L_0x00bb:
            r19 = r7
            r10 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            r27 = r8
            r25 = r10
            r7 = 0
            r10 = 0
            r11 = 0
            r14 = 0
            r23 = 0
        L_0x00ca:
            if (r7 >= r3) goto L_0x02a1
            int r8 = r0.trustRegionCenterInterpolationPointIndex
            if (r7 != r8) goto L_0x00da
            r36 = r3
            r37 = r4
            r20 = r5
            r33 = r6
            goto L_0x0295
        L_0x00da:
            r8 = 0
            r25 = 0
            r31 = 0
        L_0x00df:
            if (r8 >= r2) goto L_0x00fe
            org.apache.commons.math3.linear.Array2DRowRealMatrix r9 = r0.interpolationPoints
            double r33 = r9.getEntry(r7, r8)
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trustRegionCenterOffset
            double r35 = r9.getEntry(r8)
            double r33 = r33 - r35
            double r35 = r4.getEntry(r8)
            double r35 = r35 * r33
            double r31 = r31 + r35
            double r33 = r33 * r33
            double r25 = r25 + r33
            int r8 = r8 + 1
            goto L_0x00df
        L_0x00fe:
            double r8 = org.apache.commons.math3.util.FastMath.sqrt(r25)
            double r8 = r53 / r8
            r20 = r5
            r33 = r6
            double r5 = -r8
            r36 = r3
            r37 = r4
            r34 = r5
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r3 = org.apache.commons.math3.util.FastMath.min(r5, r8)
            r5 = 0
            r6 = 0
            r38 = 0
        L_0x0119:
            if (r5 >= r2) goto L_0x01f1
            r39 = r6
            org.apache.commons.math3.linear.Array2DRowRealMatrix r6 = r0.interpolationPoints
            double r40 = r6.getEntry(r7, r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r42 = r6.getEntry(r5)
            double r40 = r40 - r42
            r21 = 0
            int r6 = (r40 > r21 ? 1 : (r40 == r21 ? 0 : -1))
            if (r6 <= 0) goto L_0x018c
            double r42 = r34 * r40
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.lowerDifference
            double r44 = r6.getEntry(r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r46 = r6.getEntry(r5)
            double r44 = r44 - r46
            int r6 = (r42 > r44 ? 1 : (r42 == r44 ? 0 : -1))
            if (r6 >= 0) goto L_0x015b
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.lowerDifference
            double r34 = r6.getEntry(r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r42 = r6.getEntry(r5)
            double r34 = r34 - r42
            double r34 = r34 / r40
            int r6 = -r5
            r18 = 1
            int r6 = r6 + -1
            goto L_0x015d
        L_0x015b:
            r6 = r39
        L_0x015d:
            double r42 = r8 * r40
            r39 = r6
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.upperDifference
            double r44 = r6.getEntry(r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r46 = r6.getEntry(r5)
            double r44 = r44 - r46
            int r6 = (r42 > r44 ? 1 : (r42 == r44 ? 0 : -1))
            if (r6 <= 0) goto L_0x01eb
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.upperDifference
            double r8 = r6.getEntry(r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r42 = r6.getEntry(r5)
            double r8 = r8 - r42
            double r8 = r8 / r40
            double r8 = org.apache.commons.math3.util.FastMath.max(r3, r8)
            int r6 = r5 + 1
        L_0x0189:
            r38 = r6
            goto L_0x01eb
        L_0x018c:
            r21 = 0
            int r6 = (r40 > r21 ? 1 : (r40 == r21 ? 0 : -1))
            if (r6 >= 0) goto L_0x01eb
            double r42 = r34 * r40
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.upperDifference
            double r44 = r6.getEntry(r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r46 = r6.getEntry(r5)
            double r44 = r44 - r46
            int r6 = (r42 > r44 ? 1 : (r42 == r44 ? 0 : -1))
            if (r6 <= 0) goto L_0x01b9
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.upperDifference
            double r34 = r6.getEntry(r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r42 = r6.getEntry(r5)
            double r34 = r34 - r42
            double r34 = r34 / r40
            int r6 = r5 + 1
            goto L_0x01bb
        L_0x01b9:
            r6 = r39
        L_0x01bb:
            double r42 = r8 * r40
            r39 = r6
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.lowerDifference
            double r44 = r6.getEntry(r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r46 = r6.getEntry(r5)
            double r44 = r44 - r46
            int r6 = (r42 > r44 ? 1 : (r42 == r44 ? 0 : -1))
            if (r6 >= 0) goto L_0x01eb
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.lowerDifference
            double r8 = r6.getEntry(r5)
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.trustRegionCenterOffset
            double r42 = r6.getEntry(r5)
            double r8 = r8 - r42
            double r8 = r8 / r40
            double r8 = org.apache.commons.math3.util.FastMath.max(r3, r8)
            int r6 = -r5
            r18 = 1
            int r6 = r6 + -1
            goto L_0x0189
        L_0x01eb:
            r6 = r39
            int r5 = r5 + 1
            goto L_0x0119
        L_0x01f1:
            r39 = r6
            if (r7 != r1) goto L_0x023f
            r3 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r5 = r31 - r3
            double r3 = r34 * r5
            double r40 = r31 - r3
            double r40 = r40 * r34
            double r42 = r8 * r5
            double r44 = r31 - r42
            double r44 = r44 * r8
            double r46 = org.apache.commons.math3.util.FastMath.abs(r44)
            double r48 = org.apache.commons.math3.util.FastMath.abs(r40)
            int r50 = (r46 > r48 ? 1 : (r46 == r48 ? 0 : -1))
            if (r50 <= 0) goto L_0x0216
            r34 = r8
            r8 = r38
            goto L_0x021a
        L_0x0216:
            r8 = r39
            r44 = r40
        L_0x021a:
            r16 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r31 = r31 * r16
            double r3 = r31 - r3
            double r38 = r31 - r42
            double r3 = r3 * r38
            r21 = 0
            int r9 = (r3 > r21 ? 1 : (r3 == r21 ? 0 : -1))
            if (r9 >= 0) goto L_0x0274
            double r3 = r31 * r31
            double r3 = r3 / r5
            double r38 = org.apache.commons.math3.util.FastMath.abs(r3)
            double r40 = org.apache.commons.math3.util.FastMath.abs(r44)
            int r9 = (r38 > r40 ? 1 : (r38 == r40 ? 0 : -1))
            if (r9 <= 0) goto L_0x0274
            double r34 = r31 / r5
            r44 = r3
            r8 = 0
            goto L_0x0274
        L_0x023f:
            r3 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r5 = r3 - r34
            double r5 = r5 * r34
            double r40 = r3 - r8
            double r3 = r8 * r40
            double r40 = org.apache.commons.math3.util.FastMath.abs(r3)
            double r42 = org.apache.commons.math3.util.FastMath.abs(r5)
            int r44 = (r40 > r42 ? 1 : (r40 == r42 ? 0 : -1))
            if (r44 <= 0) goto L_0x0258
            r34 = r8
            goto L_0x025b
        L_0x0258:
            r3 = r5
            r38 = r39
        L_0x025b:
            r5 = 4598175219545276416(0x3fd0000000000000, double:0.25)
            r16 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            int r39 = (r8 > r16 ? 1 : (r8 == r16 ? 0 : -1))
            if (r39 <= 0) goto L_0x0270
            double r8 = org.apache.commons.math3.util.FastMath.abs(r3)
            int r39 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r39 >= 0) goto L_0x0270
            r3 = r5
            r8 = 0
            r34 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            goto L_0x0272
        L_0x0270:
            r8 = r38
        L_0x0272:
            double r44 = r3 * r31
        L_0x0274:
            r3 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r3 = r3 - r34
            double r3 = r3 * r34
            double r3 = r3 * r25
            double r44 = r44 * r44
            double r5 = r12 * r3
            double r5 = r5 * r3
            double r5 = r44 + r5
            double r44 = r44 * r5
            int r3 = (r44 > r23 ? 1 : (r44 == r23 ? 0 : -1))
            if (r3 <= 0) goto L_0x0293
            r10 = r7
            r11 = r8
            r14 = r34
            r25 = r14
            r23 = r44
            goto L_0x0295
        L_0x0293:
            r25 = r34
        L_0x0295:
            int r7 = r7 + 1
            r5 = r20
            r6 = r33
            r3 = r36
            r4 = r37
            goto L_0x00ca
        L_0x02a1:
            r36 = r3
            r37 = r4
            r20 = r5
            r33 = r6
            r1 = 0
        L_0x02aa:
            if (r1 >= r2) goto L_0x02de
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.trustRegionCenterOffset
            double r3 = r3.getEntry(r1)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r5 = r0.interpolationPoints
            double r5 = r5.getEntry(r10, r1)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trustRegionCenterOffset
            double r7 = r7.getEntry(r1)
            double r5 = r5 - r7
            double r5 = r5 * r14
            double r3 = r3 + r5
            org.apache.commons.math3.linear.ArrayRealVector r5 = r0.newPoint
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.lowerDifference
            double r6 = r6.getEntry(r1)
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.upperDifference
            double r8 = r8.getEntry(r1)
            double r3 = org.apache.commons.math3.util.FastMath.min(r8, r3)
            double r3 = org.apache.commons.math3.util.FastMath.max(r6, r3)
            r5.setEntry(r1, r3)
            int r1 = r1 + 1
            goto L_0x02aa
        L_0x02de:
            if (r11 >= 0) goto L_0x02ef
            org.apache.commons.math3.linear.ArrayRealVector r1 = r0.newPoint
            int r3 = -r11
            r4 = 1
            int r3 = r3 - r4
            org.apache.commons.math3.linear.ArrayRealVector r5 = r0.lowerDifference
            double r5 = r5.getEntry(r3)
            r1.setEntry(r3, r5)
            goto L_0x02f0
        L_0x02ef:
            r4 = 1
        L_0x02f0:
            if (r11 <= 0) goto L_0x02fe
            org.apache.commons.math3.linear.ArrayRealVector r1 = r0.newPoint
            int r11 = r11 - r4
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.upperDifference
            double r3 = r3.getEntry(r11)
            r1.setEntry(r11, r3)
        L_0x02fe:
            double r3 = r53 + r53
            r1 = 0
            r5 = 0
        L_0x0303:
            r7 = 0
            r8 = 0
        L_0x0306:
            if (r7 >= r2) goto L_0x0354
            r10 = r37
            double r11 = r10.getEntry(r7)
            r13 = r33
            r14 = 0
            r13.setEntry(r7, r14)
            org.apache.commons.math3.linear.ArrayRealVector r14 = r0.trustRegionCenterOffset
            double r14 = r14.getEntry(r7)
            r23 = r5
            org.apache.commons.math3.linear.ArrayRealVector r5 = r0.lowerDifference
            double r5 = r5.getEntry(r7)
            double r14 = r14 - r5
            double r5 = org.apache.commons.math3.util.FastMath.min(r14, r11)
            r14 = 0
            int r21 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r21 > 0) goto L_0x0345
            org.apache.commons.math3.linear.ArrayRealVector r5 = r0.trustRegionCenterOffset
            double r5 = r5.getEntry(r7)
            org.apache.commons.math3.linear.ArrayRealVector r14 = r0.upperDifference
            double r14 = r14.getEntry(r7)
            double r5 = r5 - r14
            double r5 = org.apache.commons.math3.util.FastMath.max(r5, r11)
            r14 = 0
            int r31 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r31 >= 0) goto L_0x034b
        L_0x0345:
            r13.setEntry(r7, r3)
            double r11 = r11 * r11
            double r8 = r8 + r11
        L_0x034b:
            int r7 = r7 + 1
            r37 = r10
            r33 = r13
            r5 = r23
            goto L_0x0306
        L_0x0354:
            r23 = r5
            r13 = r33
            r10 = r37
            r5 = 2
            r6 = 0
            int r11 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r11 != 0) goto L_0x036a
            double[] r1 = new double[r5]
            r2 = 0
            r1[r2] = r27
            r2 = 1
            r1[r2] = r6
            return r1
        L_0x036a:
            double r11 = r53 * r53
            double r11 = r11 - r6
            int r14 = (r11 > r6 ? 1 : (r11 == r6 ? 0 : -1))
            if (r14 <= 0) goto L_0x03d0
            double r11 = r11 / r8
            double r6 = org.apache.commons.math3.util.FastMath.sqrt(r11)
            r8 = 0
        L_0x0377:
            if (r8 >= r2) goto L_0x03d2
            double r11 = r13.getEntry(r8)
            int r9 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r9 != 0) goto L_0x03cd
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trustRegionCenterOffset
            double r11 = r9.getEntry(r8)
            double r14 = r10.getEntry(r8)
            double r14 = r14 * r6
            double r11 = r11 - r14
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.lowerDifference
            double r14 = r9.getEntry(r8)
            int r9 = (r11 > r14 ? 1 : (r11 == r14 ? 0 : -1))
            if (r9 > 0) goto L_0x03ac
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.lowerDifference
            double r11 = r9.getEntry(r8)
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trustRegionCenterOffset
            double r14 = r9.getEntry(r8)
            double r11 = r11 - r14
            r13.setEntry(r8, r11)
            r13.getEntry(r8)
            goto L_0x03cd
        L_0x03ac:
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.upperDifference
            double r14 = r9.getEntry(r8)
            int r9 = (r11 > r14 ? 1 : (r11 == r14 ? 0 : -1))
            if (r9 < 0) goto L_0x03ca
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.upperDifference
            double r11 = r9.getEntry(r8)
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trustRegionCenterOffset
            double r14 = r9.getEntry(r8)
            double r11 = r11 - r14
            r13.setEntry(r8, r11)
            r13.getEntry(r8)
            goto L_0x03cd
        L_0x03ca:
            r10.getEntry(r8)
        L_0x03cd:
            int r8 = r8 + 1
            goto L_0x0377
        L_0x03d0:
            r6 = r25
        L_0x03d2:
            r8 = 0
            r11 = 0
        L_0x03d5:
            if (r8 >= r2) goto L_0x045a
            double r14 = r10.getEntry(r8)
            double r25 = r13.getEntry(r8)
            int r9 = (r25 > r3 ? 1 : (r25 == r3 ? 0 : -1))
            if (r9 != 0) goto L_0x0415
            r25 = r3
            double r3 = -r6
            double r3 = r3 * r14
            r13.setEntry(r8, r3)
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.upperDifference
            double r3 = r3.getEntry(r8)
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trustRegionCenterOffset
            double r31 = r9.getEntry(r8)
            double r33 = r13.getEntry(r8)
            r37 = r6
            double r5 = r31 + r33
            double r3 = org.apache.commons.math3.util.FastMath.min(r3, r5)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r0.alternativeNewPoint
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.lowerDifference
            double r6 = r6.getEntry(r8)
            double r3 = org.apache.commons.math3.util.FastMath.max(r6, r3)
            r5.setEntry(r8, r3)
            r21 = 0
            goto L_0x044a
        L_0x0415:
            r25 = r3
            r37 = r6
            double r3 = r13.getEntry(r8)
            r21 = 0
            int r5 = (r3 > r21 ? 1 : (r3 == r21 ? 0 : -1))
            if (r5 != 0) goto L_0x042f
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.alternativeNewPoint
            org.apache.commons.math3.linear.ArrayRealVector r4 = r0.trustRegionCenterOffset
            double r4 = r4.getEntry(r8)
            r3.setEntry(r8, r4)
            goto L_0x044a
        L_0x042f:
            int r3 = (r14 > r21 ? 1 : (r14 == r21 ? 0 : -1))
            if (r3 <= 0) goto L_0x043f
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.alternativeNewPoint
            org.apache.commons.math3.linear.ArrayRealVector r4 = r0.lowerDifference
            double r4 = r4.getEntry(r8)
            r3.setEntry(r8, r4)
            goto L_0x044a
        L_0x043f:
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.alternativeNewPoint
            org.apache.commons.math3.linear.ArrayRealVector r4 = r0.upperDifference
            double r4 = r4.getEntry(r8)
            r3.setEntry(r8, r4)
        L_0x044a:
            double r3 = r13.getEntry(r8)
            double r14 = r14 * r3
            double r11 = r11 + r14
            int r8 = r8 + 1
            r3 = r25
            r6 = r37
            r5 = 2
            goto L_0x03d5
        L_0x045a:
            r25 = r3
            r37 = r6
            r21 = 0
            r5 = r21
            r3 = r36
            r4 = 0
        L_0x0465:
            if (r4 >= r3) goto L_0x048a
            r8 = r21
            r7 = 0
        L_0x046a:
            if (r7 >= r2) goto L_0x047c
            org.apache.commons.math3.linear.Array2DRowRealMatrix r14 = r0.interpolationPoints
            double r14 = r14.getEntry(r4, r7)
            double r31 = r13.getEntry(r7)
            double r14 = r14 * r31
            double r8 = r8 + r14
            int r7 = r7 + 1
            goto L_0x046a
        L_0x047c:
            r7 = r20
            double r14 = r7.getEntry(r4)
            double r14 = r14 * r8
            double r14 = r14 * r8
            double r5 = r5 + r14
            int r4 = r4 + 1
            goto L_0x0465
        L_0x048a:
            r7 = r20
            r4 = 1
            if (r1 != r4) goto L_0x0490
            double r5 = -r5
        L_0x0490:
            double r8 = -r11
            int r4 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r4 <= 0) goto L_0x04f6
            r14 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r14 = org.apache.commons.math3.util.FastMath.sqrt(r14)
            r29 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r14 = r14 + r29
            double r14 = r14 * r8
            int r4 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r4 >= 0) goto L_0x04ed
            double r8 = r8 / r5
            r4 = 0
        L_0x04a7:
            if (r4 >= r2) goto L_0x04e0
            org.apache.commons.math3.linear.ArrayRealVector r5 = r0.trustRegionCenterOffset
            double r5 = r5.getEntry(r4)
            double r14 = r13.getEntry(r4)
            double r14 = r14 * r8
            double r5 = r5 + r14
            org.apache.commons.math3.linear.ArrayRealVector r14 = r0.alternativeNewPoint
            org.apache.commons.math3.linear.ArrayRealVector r15 = r0.lowerDifference
            r20 = r2
            r36 = r3
            double r2 = r15.getEntry(r4)
            org.apache.commons.math3.linear.ArrayRealVector r15 = r0.upperDifference
            r31 = r1
            double r0 = r15.getEntry(r4)
            double r0 = org.apache.commons.math3.util.FastMath.min(r0, r5)
            double r0 = org.apache.commons.math3.util.FastMath.max(r2, r0)
            r14.setEntry(r4, r0)
            int r4 = r4 + 1
            r0 = r51
            r2 = r20
            r1 = r31
            r3 = r36
            goto L_0x04a7
        L_0x04e0:
            r31 = r1
            r20 = r2
            r36 = r3
            r0 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r11 = r11 * r0
            double r11 = r11 * r8
            goto L_0x0503
        L_0x04ed:
            r31 = r1
            r20 = r2
            r36 = r3
            r0 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            goto L_0x0500
        L_0x04f6:
            r31 = r1
            r20 = r2
            r36 = r3
            r0 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            r29 = 4607182418800017408(0x3ff0000000000000, double:1.0)
        L_0x0500:
            double r5 = r5 * r0
            double r11 = r11 + r5
        L_0x0503:
            double r11 = r11 * r11
            r5 = r11
            if (r31 != 0) goto L_0x0534
            r2 = r20
            r3 = 0
        L_0x050b:
            if (r3 >= r2) goto L_0x0525
            double r8 = r10.getEntry(r3)
            double r8 = -r8
            r10.setEntry(r3, r8)
            r4 = r51
            org.apache.commons.math3.linear.ArrayRealVector r8 = r4.alternativeNewPoint
            double r8 = r8.getEntry(r3)
            r11 = r19
            r11.setEntry(r3, r8)
            int r3 = r3 + 1
            goto L_0x050b
        L_0x0525:
            r0 = r51
            r20 = r7
            r33 = r13
            r3 = r25
            r25 = r37
            r1 = 1
            r37 = r10
            goto L_0x0303
        L_0x0534:
            r4 = r51
            r11 = r19
            r2 = r20
            int r0 = (r23 > r5 ? 1 : (r23 == r5 ? 0 : -1))
            if (r0 <= 0) goto L_0x054f
            r0 = 0
        L_0x053f:
            if (r0 >= r2) goto L_0x054d
            org.apache.commons.math3.linear.ArrayRealVector r1 = r4.alternativeNewPoint
            double r5 = r11.getEntry(r0)
            r1.setEntry(r0, r5)
            int r0 = r0 + 1
            goto L_0x053f
        L_0x054d:
            r5 = r23
        L_0x054f:
            r0 = 2
            double[] r0 = new double[r0]
            r1 = 0
            r0[r1] = r27
            r1 = 1
            r0[r1] = r5
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optimization.direct.BOBYQAOptimizer.altmov(int, double):double[]");
    }

    private void prelim(double[] dArr, double[] dArr2) {
        double d;
        double d2;
        int i;
        double d3;
        double d4;
        int i2;
        long j;
        int i3;
        int i4;
        double d5;
        int i5;
        double d6;
        int i6;
        printMethod();
        int dimension = this.currentBest.getDimension();
        int i7 = this.numberOfInterpolationPoints;
        int rowDimension = this.bMatrix.getRowDimension();
        double d7 = this.initialTrustRegionRadius;
        double d8 = d7 * d7;
        double d9 = 1.0d / d8;
        int i8 = dimension + 1;
        for (int i9 = 0; i9 < dimension; i9++) {
            this.originShift.setEntry(i9, this.currentBest.getEntry(i9));
            for (int i10 = 0; i10 < i7; i10++) {
                this.interpolationPoints.setEntry(i10, i9, 0.0d);
            }
            for (int i11 = 0; i11 < rowDimension; i11++) {
                this.bMatrix.setEntry(i11, i9, 0.0d);
            }
        }
        int i12 = (dimension * i8) / 2;
        for (int i13 = 0; i13 < i12; i13++) {
            this.modelSecondDerivativesValues.setEntry(i13, 0.0d);
        }
        for (int i14 = 0; i14 < i7; i14++) {
            this.modelSecondDerivativesParameters.setEntry(i14, 0.0d);
            int i15 = i7 - i8;
            for (int i16 = 0; i16 < i15; i16++) {
                this.zMatrix.setEntry(i14, i16, 0.0d);
            }
        }
        double d10 = Double.NaN;
        int i17 = 0;
        int i18 = 0;
        while (true) {
            int evaluations = getEvaluations();
            int i19 = evaluations - dimension;
            int i20 = evaluations - 1;
            int i21 = i19 - 1;
            int i22 = dimension * 2;
            if (evaluations <= i22) {
                if (evaluations < 1 || evaluations > dimension) {
                    int i23 = i17;
                    i6 = i19;
                    if (evaluations > dimension) {
                        int i24 = i6;
                        double entry = this.interpolationPoints.getEntry(i24, i21);
                        int i25 = i18;
                        d = d9;
                        double d11 = -this.initialTrustRegionRadius;
                        if (this.lowerDifference.getEntry(i21) == 0.0d) {
                            i = i24;
                            d11 = FastMath.min(this.initialTrustRegionRadius * TWO, this.upperDifference.getEntry(i21));
                        } else {
                            i = i24;
                        }
                        if (this.upperDifference.getEntry(i21) == 0.0d) {
                            d11 = FastMath.max(this.initialTrustRegionRadius * -2.0d, this.lowerDifference.getEntry(i21));
                        }
                        this.interpolationPoints.setEntry(evaluations, i21, d11);
                        d3 = entry;
                        double d12 = d11;
                        i18 = i25;
                        i2 = i23;
                        d2 = d8;
                        d4 = d12;
                    } else {
                        d = d9;
                        i2 = i23;
                        d3 = 0.0d;
                    }
                } else {
                    i6 = i19;
                    d3 = this.initialTrustRegionRadius;
                    int i26 = i17;
                    if (this.upperDifference.getEntry(i20) == 0.0d) {
                        d3 = -d3;
                    }
                    this.interpolationPoints.setEntry(evaluations, i20, d3);
                    d = d9;
                    i2 = i26;
                }
                d2 = d8;
                d4 = 0.0d;
            } else {
                d = d9;
                i = i19;
                int i27 = (evaluations - i8) / dimension;
                i18 = (evaluations - (i27 * dimension)) - dimension;
                int i28 = i27 + i18;
                if (i28 > dimension) {
                    int i29 = i18;
                    i18 = i28 - dimension;
                    i28 = i29;
                }
                int i30 = i2 - 1;
                int i31 = i18 - 1;
                Array2DRowRealMatrix array2DRowRealMatrix = this.interpolationPoints;
                d2 = d8;
                array2DRowRealMatrix.setEntry(evaluations, i30, array2DRowRealMatrix.getEntry(i2, i30));
                Array2DRowRealMatrix array2DRowRealMatrix2 = this.interpolationPoints;
                array2DRowRealMatrix2.setEntry(evaluations, i31, array2DRowRealMatrix2.getEntry(i18, i31));
                d4 = 0.0d;
                d3 = 0.0d;
            }
            int i32 = 0;
            while (i32 < dimension) {
                int i33 = i2;
                double d13 = d4;
                int i34 = dimension;
                int i35 = i7;
                this.currentBest.setEntry(i32, FastMath.min(FastMath.max(dArr[i32], this.originShift.getEntry(i32) + this.interpolationPoints.getEntry(evaluations, i32)), dArr2[i32]));
                if (this.interpolationPoints.getEntry(evaluations, i32) == this.lowerDifference.getEntry(i32)) {
                    this.currentBest.setEntry(i32, dArr[i32]);
                }
                if (this.interpolationPoints.getEntry(evaluations, i32) == this.upperDifference.getEntry(i32)) {
                    this.currentBest.setEntry(i32, dArr2[i32]);
                }
                i32++;
                dimension = i34;
                i2 = i33;
                d4 = d13;
                i7 = i35;
            }
            int i36 = i7;
            int i37 = i2;
            double d14 = d4;
            int i38 = dimension;
            double computeObjectiveValue = computeObjectiveValue(this.currentBest.toArray());
            if (!this.isMinimize) {
                computeObjectiveValue = -computeObjectiveValue;
            }
            int evaluations2 = getEvaluations();
            this.fAtInterpolationPoints.setEntry(evaluations, computeObjectiveValue);
            if (evaluations2 == 1) {
                this.trustRegionCenterInterpolationPointIndex = 0;
                d10 = computeObjectiveValue;
            } else if (computeObjectiveValue < this.fAtInterpolationPoints.getEntry(this.trustRegionCenterInterpolationPointIndex)) {
                this.trustRegionCenterInterpolationPointIndex = evaluations;
            }
            if (evaluations2 > i22 + 1) {
                i4 = i38;
                j = 0;
                d5 = d;
                this.zMatrix.setEntry(0, i21, d5);
                this.zMatrix.setEntry(evaluations, i21, d5);
                double d15 = -d5;
                i3 = i37;
                this.zMatrix.setEntry(i3, i21, d15);
                this.zMatrix.setEntry(i18, i21, d15);
                int i39 = i3 - 1;
                this.modelSecondDerivativesValues.setEntry((((i3 * i39) / 2) + i18) - 1, (((d10 - this.fAtInterpolationPoints.getEntry(i3)) - this.fAtInterpolationPoints.getEntry(i18)) + computeObjectiveValue) / (this.interpolationPoints.getEntry(evaluations, i39) * this.interpolationPoints.getEntry(evaluations, i18 - 1)));
            } else if (evaluations2 < 2 || evaluations2 > i8) {
                int i40 = i36;
                if (evaluations2 >= i38 + 2) {
                    double d16 = (computeObjectiveValue - d10) / d14;
                    double d17 = d14 - d3;
                    this.modelSecondDerivativesValues.setEntry((((i + 1) * i) / 2) - 1, ((d16 - this.gradientAtTrustRegionCenter.getEntry(i21)) * TWO) / d17);
                    ArrayRealVector arrayRealVector = this.gradientAtTrustRegionCenter;
                    arrayRealVector.setEntry(i21, ((arrayRealVector.getEntry(i21) * d14) - (d16 * d3)) / d17);
                    double d18 = d3 * d14;
                    j = 0;
                    if (d18 < 0.0d) {
                        i5 = i;
                        if (computeObjectiveValue < this.fAtInterpolationPoints.getEntry(i5)) {
                            ArrayRealVector arrayRealVector2 = this.fAtInterpolationPoints;
                            i36 = i40;
                            i4 = i38;
                            arrayRealVector2.setEntry(evaluations, arrayRealVector2.getEntry(i5));
                            this.fAtInterpolationPoints.setEntry(i5, computeObjectiveValue);
                            if (this.trustRegionCenterInterpolationPointIndex == evaluations) {
                                this.trustRegionCenterInterpolationPointIndex = i5;
                            }
                            d6 = d14;
                            this.interpolationPoints.setEntry(i5, i21, d6);
                            this.interpolationPoints.setEntry(evaluations, i21, d3);
                            this.bMatrix.setEntry(0, i21, (-(d3 + d6)) / d18);
                            this.bMatrix.setEntry(evaluations, i21, -0.5d / this.interpolationPoints.getEntry(i5, i21));
                            Array2DRowRealMatrix array2DRowRealMatrix3 = this.bMatrix;
                            array2DRowRealMatrix3.setEntry(i5, i21, (-array2DRowRealMatrix3.getEntry(0, i21)) - this.bMatrix.getEntry(evaluations, i21));
                            this.zMatrix.setEntry(0, i21, FastMath.sqrt(TWO) / d18);
                            this.zMatrix.setEntry(evaluations, i21, FastMath.sqrt(0.5d) / d2);
                            Array2DRowRealMatrix array2DRowRealMatrix4 = this.zMatrix;
                            array2DRowRealMatrix4.setEntry(i5, i21, (-array2DRowRealMatrix4.getEntry(0, i21)) - this.zMatrix.getEntry(evaluations, i21));
                        } else {
                            i36 = i40;
                            i4 = i38;
                        }
                    } else {
                        i36 = i40;
                        i4 = i38;
                        i5 = i;
                    }
                    d6 = d14;
                    this.bMatrix.setEntry(0, i21, (-(d3 + d6)) / d18);
                    this.bMatrix.setEntry(evaluations, i21, -0.5d / this.interpolationPoints.getEntry(i5, i21));
                    Array2DRowRealMatrix array2DRowRealMatrix32 = this.bMatrix;
                    array2DRowRealMatrix32.setEntry(i5, i21, (-array2DRowRealMatrix32.getEntry(0, i21)) - this.bMatrix.getEntry(evaluations, i21));
                    this.zMatrix.setEntry(0, i21, FastMath.sqrt(TWO) / d18);
                    this.zMatrix.setEntry(evaluations, i21, FastMath.sqrt(0.5d) / d2);
                    Array2DRowRealMatrix array2DRowRealMatrix42 = this.zMatrix;
                    array2DRowRealMatrix42.setEntry(i5, i21, (-array2DRowRealMatrix42.getEntry(0, i21)) - this.zMatrix.getEntry(evaluations, i21));
                } else {
                    i36 = i40;
                    i4 = i38;
                    j = 0;
                }
                i3 = i37;
                d5 = d;
            } else {
                this.gradientAtTrustRegionCenter.setEntry(i20, (computeObjectiveValue - d10) / d3);
                int i41 = i36;
                if (i41 < evaluations2 + i38) {
                    double d19 = 1.0d / d3;
                    this.bMatrix.setEntry(0, i20, -d19);
                    this.bMatrix.setEntry(evaluations, i20, d19);
                    this.bMatrix.setEntry(i41 + i20, i20, -0.5d * d2);
                }
                i36 = i41;
                i4 = i38;
                i3 = i37;
                d5 = d;
                j = 0;
            }
            i7 = i36;
            if (getEvaluations() < i7) {
                d9 = d5;
                dimension = i4;
                i17 = i3;
                d8 = d2;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:218:0x07e3  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x0804  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private double[] trsbox(double r78, org.apache.commons.math3.linear.ArrayRealVector r80, org.apache.commons.math3.linear.ArrayRealVector r81, org.apache.commons.math3.linear.ArrayRealVector r82, org.apache.commons.math3.linear.ArrayRealVector r83, org.apache.commons.math3.linear.ArrayRealVector r84) {
        /*
            r77 = this;
            r0 = r77
            r1 = r80
            r2 = r81
            r3 = r82
            r4 = r83
            r5 = r84
            printMethod()
            org.apache.commons.math3.linear.ArrayRealVector r6 = r0.currentBest
            int r6 = r6.getDimension()
            int r7 = r0.numberOfInterpolationPoints
            r9 = 0
            r10 = 0
        L_0x0019:
            r11 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r13 = 0
            if (r9 >= r6) goto L_0x007a
            r2.setEntry(r9, r13)
            org.apache.commons.math3.linear.ArrayRealVector r15 = r0.trustRegionCenterOffset
            double r15 = r15.getEntry(r9)
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.lowerDifference
            double r19 = r8.getEntry(r9)
            int r8 = (r15 > r19 ? 1 : (r15 == r19 ? 0 : -1))
            if (r8 > 0) goto L_0x0040
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.gradientAtTrustRegionCenter
            double r15 = r8.getEntry(r9)
            int r8 = (r15 > r13 ? 1 : (r15 == r13 ? 0 : -1))
            if (r8 < 0) goto L_0x005f
            r2.setEntry(r9, r11)
            goto L_0x005f
        L_0x0040:
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.trustRegionCenterOffset
            double r11 = r8.getEntry(r9)
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.upperDifference
            double r15 = r8.getEntry(r9)
            int r8 = (r11 > r15 ? 1 : (r11 == r15 ? 0 : -1))
            if (r8 < 0) goto L_0x005f
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.gradientAtTrustRegionCenter
            double r11 = r8.getEntry(r9)
            int r8 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r8 > 0) goto L_0x005f
            r11 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r2.setEntry(r9, r11)
        L_0x005f:
            double r11 = r2.getEntry(r9)
            int r8 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r8 == 0) goto L_0x0069
            int r10 = r10 + 1
        L_0x0069:
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.trialStepPoint
            r8.setEntry(r9, r13)
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.gradientAtTrustRegionCenter
            double r11 = r8.getEntry(r9)
            r1.setEntry(r9, r11)
            int r9 = r9 + 1
            goto L_0x0019
        L_0x007a:
            double r8 = r78 * r78
            r11 = 90
            r12 = 50
            r13 = 30
            r14 = 20
            r15 = 190(0xbe, float:2.66E-43)
            r24 = r10
            r25 = 0
            r26 = 0
            r28 = 0
            r30 = 0
            r32 = 0
            r34 = 0
            r36 = 0
            r38 = 0
            r40 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r42 = 0
            r43 = 0
            r45 = 0
            r47 = 0
            r49 = 0
            r51 = -1
            r52 = 0
            r54 = 0
            r55 = 0
            r9 = r8
            r8 = 20
        L_0x00af:
            if (r8 == r14) goto L_0x087c
            if (r8 == r13) goto L_0x085e
            r57 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            if (r8 == r12) goto L_0x05db
            if (r8 == r11) goto L_0x05c4
            r13 = 100
            if (r8 == r13) goto L_0x05a7
            r13 = 120(0x78, float:1.68E-43)
            if (r8 == r13) goto L_0x0449
            r13 = 150(0x96, float:2.1E-43)
            r14 = 1
            if (r8 == r13) goto L_0x01c8
            if (r8 == r15) goto L_0x01c1
            r13 = 210(0xd2, float:2.94E-43)
            if (r8 != r13) goto L_0x01b2
            printState(r13)
            r8 = 0
            r14 = 0
        L_0x00d1:
            if (r8 >= r6) goto L_0x0129
            r57 = r14
            r13 = 0
            r4.setEntry(r8, r13)
            r14 = r57
            r13 = 0
        L_0x00dd:
            if (r13 > r8) goto L_0x011e
            if (r13 >= r8) goto L_0x00fb
            double r57 = r4.getEntry(r8)
            org.apache.commons.math3.linear.ArrayRealVector r11 = r0.modelSecondDerivativesValues
            double r59 = r11.getEntry(r14)
            double r61 = r3.getEntry(r13)
            double r59 = r59 * r61
            r61 = r13
            double r12 = r57 + r59
            r4.setEntry(r8, r12)
            r12 = r61
            goto L_0x00fc
        L_0x00fb:
            r12 = r13
        L_0x00fc:
            double r57 = r4.getEntry(r12)
            org.apache.commons.math3.linear.ArrayRealVector r13 = r0.modelSecondDerivativesValues
            double r59 = r13.getEntry(r14)
            double r61 = r3.getEntry(r8)
            double r59 = r59 * r61
            r61 = r9
            double r9 = r57 + r59
            r4.setEntry(r12, r9)
            int r14 = r14 + 1
            int r13 = r12 + 1
            r9 = r61
            r11 = 90
            r12 = 50
            goto L_0x00dd
        L_0x011e:
            r61 = r9
            int r8 = r8 + 1
            r11 = 90
            r12 = 50
            r13 = 210(0xd2, float:2.94E-43)
            goto L_0x00d1
        L_0x0129:
            r61 = r9
            org.apache.commons.math3.linear.Array2DRowRealMatrix r8 = r0.interpolationPoints
            org.apache.commons.math3.linear.RealVector r8 = r8.operate(r3)
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.modelSecondDerivativesParameters
            org.apache.commons.math3.linear.RealVector r8 = r8.ebeMultiply(r9)
            r9 = 0
        L_0x0138:
            if (r9 >= r7) goto L_0x0164
            org.apache.commons.math3.linear.ArrayRealVector r10 = r0.modelSecondDerivativesParameters
            double r12 = r10.getEntry(r9)
            r21 = 0
            int r10 = (r12 > r21 ? 1 : (r12 == r21 ? 0 : -1))
            if (r10 == 0) goto L_0x0161
            r10 = 0
        L_0x0147:
            if (r10 >= r6) goto L_0x0161
            double r12 = r4.getEntry(r10)
            double r57 = r8.getEntry(r9)
            org.apache.commons.math3.linear.Array2DRowRealMatrix r14 = r0.interpolationPoints
            double r59 = r14.getEntry(r9, r10)
            double r57 = r57 * r59
            double r12 = r12 + r57
            r4.setEntry(r10, r12)
            int r10 = r10 + 1
            goto L_0x0147
        L_0x0161:
            int r9 = r9 + 1
            goto L_0x0138
        L_0x0164:
            r9 = r40
            r12 = 0
            int r8 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
            if (r8 == 0) goto L_0x017c
            r40 = r9
            r9 = r61
            r8 = 50
        L_0x0172:
            r11 = 90
            r12 = 50
            r13 = 30
            r14 = 20
            goto L_0x00af
        L_0x017c:
            r8 = r25
            r12 = r42
            if (r8 <= r12) goto L_0x018f
            r13 = 150(0x96, float:2.1E-43)
            r25 = r8
            r40 = r9
            r42 = r12
            r9 = r61
            r8 = 150(0x96, float:2.1E-43)
            goto L_0x0172
        L_0x018f:
            r13 = 0
        L_0x0190:
            if (r13 >= r6) goto L_0x01a0
            r40 = r12
            double r11 = r4.getEntry(r13)
            r5.setEntry(r13, r11)
            int r13 = r13 + 1
            r12 = r40
            goto L_0x0190
        L_0x01a0:
            r40 = r12
            r25 = r8
            r42 = r40
            r8 = 120(0x78, float:1.68E-43)
            r11 = 90
            r12 = 50
            r13 = 30
            r14 = 20
            goto L_0x0900
        L_0x01b2:
            org.apache.commons.math3.exception.MathIllegalStateException r1 = new org.apache.commons.math3.exception.MathIllegalStateException
            org.apache.commons.math3.exception.util.LocalizedFormats r2 = org.apache.commons.math3.exception.util.LocalizedFormats.SIMPLE_MESSAGE
            java.lang.Object[] r3 = new java.lang.Object[r14]
            java.lang.String r4 = "trsbox"
            r5 = 0
            r3[r5] = r4
            r1.<init>(r2, r3)
            throw r1
        L_0x01c1:
            r12 = r2
            r69 = r40
        L_0x01c4:
            r2 = 190(0xbe, float:2.66E-43)
            goto L_0x03c8
        L_0x01c8:
            r61 = r9
            r8 = r25
            r9 = r40
            r40 = r42
            r11 = 150(0x96, float:2.1E-43)
            printState(r11)
            r11 = 0
            r12 = 0
            r41 = 0
            r59 = 0
        L_0x01dc:
            if (r11 >= r6) goto L_0x0214
            double r63 = r2.getEntry(r11)
            r21 = 0
            int r65 = (r63 > r21 ? 1 : (r63 == r21 ? 0 : -1))
            if (r65 != 0) goto L_0x0210
            double r63 = r3.getEntry(r11)
            double r65 = r4.getEntry(r11)
            double r63 = r63 * r65
            double r12 = r12 + r63
            org.apache.commons.math3.linear.ArrayRealVector r14 = r0.trialStepPoint
            double r64 = r14.getEntry(r11)
            double r66 = r4.getEntry(r11)
            double r64 = r64 * r66
            double r41 = r41 + r64
            org.apache.commons.math3.linear.ArrayRealVector r14 = r0.trialStepPoint
            double r64 = r14.getEntry(r11)
            double r66 = r5.getEntry(r11)
            double r64 = r64 * r66
            double r59 = r59 + r64
        L_0x0210:
            int r11 = r11 + 1
            r14 = 1
            goto L_0x01dc
        L_0x0214:
            r64 = 4625478292286210048(0x4031000000000000, double:17.0)
            double r64 = r64 * r36
            r66 = 4614162998222441677(0x4008cccccccccccd, double:3.1)
            r11 = r7
            r14 = r8
            double r7 = r64 + r66
            int r7 = (int) r7
            r65 = r43
            r67 = r49
            r8 = 0
            r15 = -1
            r43 = 0
            r49 = r45
            r45 = 0
        L_0x022e:
            if (r8 >= r7) goto L_0x0277
            r69 = r9
            double r9 = (double) r8
            java.lang.Double.isNaN(r9)
            double r9 = r9 * r36
            double r2 = (double) r7
            java.lang.Double.isNaN(r2)
            double r67 = r9 / r2
            double r2 = r67 + r67
            double r9 = r67 * r67
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r9 = r9 + r17
            double r2 = r2 / r9
            double r9 = r67 * r59
            double r9 = r9 - r41
            double r9 = r9 - r41
            double r9 = r9 * r67
            double r9 = r9 + r12
            double r71 = r67 * r30
            double r71 = r71 - r47
            double r73 = r2 * r57
            double r73 = r73 * r9
            double r71 = r71 - r73
            double r2 = r2 * r71
            int r9 = (r2 > r43 ? 1 : (r2 == r43 ? 0 : -1))
            if (r9 <= 0) goto L_0x0266
            r43 = r2
            r15 = r8
            r65 = r45
            goto L_0x026c
        L_0x0266:
            int r9 = r15 + 1
            if (r8 != r9) goto L_0x026c
            r49 = r2
        L_0x026c:
            int r8 = r8 + 1
            r45 = r2
            r9 = r69
            r2 = r81
            r3 = r82
            goto L_0x022e
        L_0x0277:
            r69 = r9
            if (r15 >= 0) goto L_0x028e
            r2 = r81
            r3 = r82
            r7 = r11
            r25 = r14
            r42 = r40
            r45 = r49
            r9 = r61
            r43 = r65
            r49 = r67
            goto L_0x0482
        L_0x028e:
            if (r15 >= r7) goto L_0x02a9
            double r2 = r49 - r65
            double r43 = r43 + r43
            double r43 = r43 - r65
            double r43 = r43 - r49
            double r2 = r2 / r43
            double r8 = (double) r15
            double r2 = r2 * r57
            java.lang.Double.isNaN(r8)
            double r8 = r8 + r2
            double r8 = r8 * r36
            double r2 = (double) r7
            java.lang.Double.isNaN(r2)
            double r8 = r8 / r2
            goto L_0x02ab
        L_0x02a9:
            r8 = r67
        L_0x02ab:
            double r2 = r8 * r8
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r43 = r17 - r2
            double r2 = r2 + r17
            double r43 = r43 / r2
            double r45 = r8 + r8
            double r45 = r45 / r2
            double r59 = r59 * r8
            double r59 = r59 - r41
            double r59 = r59 - r41
            double r59 = r59 * r8
            double r12 = r12 + r59
            double r2 = r8 * r30
            double r2 = r2 - r47
            double r57 = r57 * r45
            double r57 = r57 * r12
            double r2 = r2 - r57
            double r2 = r2 * r45
            r12 = 0
            int r10 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1))
            if (r10 > 0) goto L_0x02f6
            r2 = r81
            r3 = r82
            r7 = r11
            r25 = r14
            r42 = r40
            r45 = r49
            r43 = r65
            r40 = r69
            r11 = 90
            r12 = 50
            r13 = 30
            r14 = 20
            r15 = 190(0xbe, float:2.66E-43)
            r49 = r8
            r9 = r61
            r8 = 190(0xbe, float:2.66E-43)
            goto L_0x00af
        L_0x02f6:
            r10 = 0
            r26 = 0
            r30 = 0
        L_0x02fb:
            if (r10 >= r6) goto L_0x036a
            double r12 = r1.getEntry(r10)
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r41 = r43 - r17
            double r57 = r5.getEntry(r10)
            double r41 = r41 * r57
            double r12 = r12 + r41
            double r41 = r4.getEntry(r10)
            double r41 = r41 * r45
            double r12 = r12 + r41
            r1.setEntry(r10, r12)
            r12 = r81
            double r41 = r12.getEntry(r10)
            r21 = 0
            int r13 = (r41 > r21 ? 1 : (r41 == r21 ? 0 : -1))
            if (r13 != 0) goto L_0x0352
            org.apache.commons.math3.linear.ArrayRealVector r13 = r0.trialStepPoint
            double r41 = r13.getEntry(r10)
            double r41 = r41 * r43
            r59 = r8
            r8 = r82
            double r57 = r8.getEntry(r10)
            double r57 = r57 * r45
            double r8 = r41 + r57
            r13.setEntry(r10, r8)
            org.apache.commons.math3.linear.ArrayRealVector r8 = r0.trialStepPoint
            double r8 = r8.getEntry(r10)
            double r41 = r1.getEntry(r10)
            double r8 = r8 * r41
            double r30 = r30 + r8
            double r8 = r1.getEntry(r10)
            double r8 = r8 * r8
            double r26 = r26 + r8
            goto L_0x0354
        L_0x0352:
            r59 = r8
        L_0x0354:
            double r8 = r5.getEntry(r10)
            double r8 = r8 * r43
            double r41 = r4.getEntry(r10)
            double r41 = r41 * r45
            double r8 = r8 + r41
            r5.setEntry(r10, r8)
            int r10 = r10 + 1
            r8 = r59
            goto L_0x02fb
        L_0x036a:
            r12 = r81
            r59 = r8
            double r32 = r32 + r2
            r8 = r51
            if (r8 < 0) goto L_0x03a1
            if (r15 != r7) goto L_0x03a1
            r10 = r24
            int r24 = r10 + 1
            r2 = r52
            r12.setEntry(r8, r2)
            r51 = r8
            r7 = r11
            r2 = r12
            r25 = r14
            r42 = r40
            r45 = r49
            r49 = r59
            r9 = r61
            r43 = r65
            r40 = r69
            r8 = 100
            r11 = 90
            r12 = 50
            r13 = 30
            r14 = 20
            r15 = 190(0xbe, float:2.66E-43)
            r3 = r82
            goto L_0x00af
        L_0x03a1:
            r10 = r24
            r41 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
            double r41 = r41 * r32
            int r7 = (r2 > r41 ? 1 : (r2 == r41 ? 0 : -1))
            if (r7 <= 0) goto L_0x01c4
            r3 = r82
            r51 = r8
            r24 = r10
            r7 = r11
            r2 = r12
            r25 = r14
            r42 = r40
            r45 = r49
            r49 = r59
            r9 = r61
            r43 = r65
            r40 = r69
            r8 = 120(0x78, float:1.68E-43)
            goto L_0x0486
        L_0x03c8:
            printState(r2)
            r1 = 0
            r21 = 0
        L_0x03ce:
            if (r1 >= r6) goto L_0x043f
            org.apache.commons.math3.linear.ArrayRealVector r2 = r0.trustRegionCenterOffset
            double r2 = r2.getEntry(r1)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r0.trialStepPoint
            double r4 = r4.getEntry(r1)
            double r2 = r2 + r4
            org.apache.commons.math3.linear.ArrayRealVector r4 = r0.upperDifference
            double r4 = r4.getEntry(r1)
            double r2 = org.apache.commons.math3.util.FastMath.min(r2, r4)
            org.apache.commons.math3.linear.ArrayRealVector r4 = r0.newPoint
            org.apache.commons.math3.linear.ArrayRealVector r5 = r0.lowerDifference
            double r7 = r5.getEntry(r1)
            double r2 = org.apache.commons.math3.util.FastMath.max(r2, r7)
            r4.setEntry(r1, r2)
            double r2 = r12.getEntry(r1)
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            int r7 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r7 != 0) goto L_0x040b
            org.apache.commons.math3.linear.ArrayRealVector r2 = r0.newPoint
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.lowerDifference
            double r3 = r3.getEntry(r1)
            r2.setEntry(r1, r3)
        L_0x040b:
            double r2 = r12.getEntry(r1)
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r7 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r7 != 0) goto L_0x0420
            org.apache.commons.math3.linear.ArrayRealVector r2 = r0.newPoint
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.upperDifference
            double r3 = r3.getEntry(r1)
            r2.setEntry(r1, r3)
        L_0x0420:
            org.apache.commons.math3.linear.ArrayRealVector r2 = r0.trialStepPoint
            org.apache.commons.math3.linear.ArrayRealVector r3 = r0.newPoint
            double r3 = r3.getEntry(r1)
            org.apache.commons.math3.linear.ArrayRealVector r5 = r0.trustRegionCenterOffset
            double r7 = r5.getEntry(r1)
            double r3 = r3 - r7
            r2.setEntry(r1, r3)
            org.apache.commons.math3.linear.ArrayRealVector r2 = r0.trialStepPoint
            double r2 = r2.getEntry(r1)
            double r2 = r2 * r2
            double r21 = r21 + r2
            int r1 = r1 + 1
            goto L_0x03ce
        L_0x043f:
            r1 = 2
            double[] r1 = new double[r1]
            r3 = 0
            r1[r3] = r21
            r2 = 1
            r1[r2] = r69
            return r1
        L_0x0449:
            r12 = r2
            r11 = r7
            r61 = r9
            r10 = r24
            r14 = r25
            r69 = r40
            r40 = r42
            r8 = r51
            r2 = 190(0xbe, float:2.66E-43)
            r3 = 0
            r7 = 120(0x78, float:1.68E-43)
            printState(r7)
            int r9 = r14 + 1
            double r13 = r26 * r28
            double r41 = r30 * r30
            double r13 = r13 - r41
            r41 = 4547007122018943789(0x3f1a36e2eb1c432d, double:1.0E-4)
            double r41 = r41 * r32
            double r41 = r41 * r32
            int r15 = (r13 > r41 ? 1 : (r13 == r41 ? 0 : -1))
            if (r15 > 0) goto L_0x0492
            r3 = r82
            r51 = r8
            r25 = r9
            r24 = r10
            r7 = r11
            r2 = r12
        L_0x047e:
            r42 = r40
            r9 = r61
        L_0x0482:
            r40 = r69
            r8 = 190(0xbe, float:2.66E-43)
        L_0x0486:
            r11 = 90
            r12 = 50
            r13 = 30
            r14 = 20
            r15 = 190(0xbe, float:2.66E-43)
            goto L_0x00af
        L_0x0492:
            double r13 = org.apache.commons.math3.util.FastMath.sqrt(r13)
            r8 = 0
        L_0x0497:
            if (r8 >= r6) goto L_0x04c8
            double r36 = r12.getEntry(r8)
            r2 = 0
            int r15 = (r36 > r2 ? 1 : (r36 == r2 ? 0 : -1))
            if (r15 != 0) goto L_0x04bd
            org.apache.commons.math3.linear.ArrayRealVector r15 = r0.trialStepPoint
            double r21 = r15.getEntry(r8)
            double r21 = r21 * r30
            double r36 = r1.getEntry(r8)
            double r36 = r36 * r28
            double r21 = r21 - r36
            double r2 = r21 / r13
            r15 = r82
            r15.setEntry(r8, r2)
            r2 = 0
            goto L_0x04c2
        L_0x04bd:
            r15 = r82
            r15.setEntry(r8, r2)
        L_0x04c2:
            int r8 = r8 + 1
            r2 = 190(0xbe, float:2.66E-43)
            r3 = 0
            goto L_0x0497
        L_0x04c8:
            r15 = r82
            r2 = 0
            double r13 = -r13
            r8 = 0
            r36 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r51 = -1
        L_0x04d2:
            if (r8 >= r6) goto L_0x059a
            double r21 = r12.getEntry(r8)
            int r24 = (r21 > r2 ? 1 : (r21 == r2 ? 0 : -1))
            if (r24 != 0) goto L_0x0592
            org.apache.commons.math3.linear.ArrayRealVector r2 = r0.trustRegionCenterOffset
            double r2 = r2.getEntry(r8)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trialStepPoint
            double r41 = r7.getEntry(r8)
            double r2 = r2 + r41
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.lowerDifference
            double r41 = r7.getEntry(r8)
            double r2 = r2 - r41
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.upperDifference
            double r41 = r7.getEntry(r8)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trustRegionCenterOffset
            double r47 = r7.getEntry(r8)
            double r41 = r41 - r47
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trialStepPoint
            double r47 = r7.getEntry(r8)
            double r41 = r41 - r47
            r21 = 0
            int r7 = (r2 > r21 ? 1 : (r2 == r21 ? 0 : -1))
            if (r7 > 0) goto L_0x0517
            int r24 = r10 + 1
            r2 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r12.setEntry(r8, r2)
            goto L_0x059c
        L_0x0517:
            int r7 = (r41 > r21 ? 1 : (r41 == r21 ? 0 : -1))
            if (r7 > 0) goto L_0x0524
            int r24 = r10 + 1
            r2 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r12.setEntry(r8, r2)
            goto L_0x059c
        L_0x0524:
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trialStepPoint
            double r47 = r7.getEntry(r8)
            double r57 = r15.getEntry(r8)
            double r47 = r47 * r47
            double r57 = r57 * r57
            double r47 = r47 + r57
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trustRegionCenterOffset
            double r57 = r7.getEntry(r8)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.lowerDifference
            double r59 = r7.getEntry(r8)
            double r57 = r57 - r59
            double r57 = r57 * r57
            double r57 = r47 - r57
            r21 = 0
            int r7 = (r57 > r21 ? 1 : (r57 == r21 ? 0 : -1))
            if (r7 <= 0) goto L_0x0562
            double r57 = org.apache.commons.math3.util.FastMath.sqrt(r57)
            double r59 = r15.getEntry(r8)
            double r57 = r57 - r59
            double r59 = r36 * r57
            int r7 = (r59 > r2 ? 1 : (r59 == r2 ? 0 : -1))
            if (r7 <= 0) goto L_0x0562
            double r36 = r2 / r57
            r51 = r8
            r52 = -4616189618054758400(0xbff0000000000000, double:-1.0)
        L_0x0562:
            org.apache.commons.math3.linear.ArrayRealVector r2 = r0.upperDifference
            double r2 = r2.getEntry(r8)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trustRegionCenterOffset
            double r57 = r7.getEntry(r8)
            double r2 = r2 - r57
            double r2 = r2 * r2
            double r47 = r47 - r2
            r2 = 0
            int r7 = (r47 > r2 ? 1 : (r47 == r2 ? 0 : -1))
            if (r7 <= 0) goto L_0x0592
            double r2 = org.apache.commons.math3.util.FastMath.sqrt(r47)
            double r47 = r15.getEntry(r8)
            double r2 = r2 + r47
            double r47 = r36 * r2
            int r7 = (r47 > r41 ? 1 : (r47 == r41 ? 0 : -1))
            if (r7 <= 0) goto L_0x0592
            double r41 = r41 / r2
            r51 = r8
            r36 = r41
            r52 = 4607182418800017408(0x3ff0000000000000, double:1.0)
        L_0x0592:
            int r8 = r8 + 1
            r2 = 0
            r7 = 120(0x78, float:1.68E-43)
            goto L_0x04d2
        L_0x059a:
            r24 = r10
        L_0x059c:
            r25 = r9
            r7 = r11
            r2 = r12
            r47 = r13
            r3 = r15
            r42 = r40
            goto L_0x0856
        L_0x05a7:
            r12 = r2
            r15 = r3
            r11 = r7
            r61 = r9
            r14 = r25
            r69 = r40
            r40 = r42
            r8 = r51
            r42 = r14
            r23 = r24
            r2 = r54
            r3 = 90
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r7 = 100
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            goto L_0x07da
        L_0x05c4:
            r12 = r2
            r15 = r3
            r11 = r7
            r61 = r9
            r14 = r25
            r40 = r42
            r8 = r51
            r23 = r24
            r2 = r54
            r3 = 90
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            goto L_0x07cf
        L_0x05db:
            r12 = r2
            r15 = r3
            r11 = r7
            r61 = r9
            r10 = r24
            r14 = r25
            r69 = r40
            r40 = r42
            r8 = r51
            r2 = 50
            printState(r2)
            r24 = r61
            r3 = 0
            r41 = 0
            r59 = 0
        L_0x05f6:
            if (r3 >= r6) goto L_0x0629
            double r65 = r12.getEntry(r3)
            r21 = 0
            int r7 = (r65 > r21 ? 1 : (r65 == r21 ? 0 : -1))
            if (r7 != 0) goto L_0x0626
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trialStepPoint
            double r65 = r7.getEntry(r3)
            double r65 = r65 * r65
            double r24 = r24 - r65
            double r65 = r15.getEntry(r3)
            org.apache.commons.math3.linear.ArrayRealVector r7 = r0.trialStepPoint
            double r67 = r7.getEntry(r3)
            double r65 = r65 * r67
            double r41 = r41 + r65
            double r65 = r15.getEntry(r3)
            double r67 = r4.getEntry(r3)
            double r65 = r65 * r67
            double r59 = r59 + r65
        L_0x0626:
            int r3 = r3 + 1
            goto L_0x05f6
        L_0x0629:
            r21 = 0
            int r3 = (r24 > r21 ? 1 : (r24 == r21 ? 0 : -1))
            if (r3 > 0) goto L_0x0642
            r51 = r8
            r24 = r10
            r7 = r11
            r2 = r12
            r25 = r14
            r3 = r15
            r42 = r40
            r9 = r61
            r40 = r69
            r8 = 90
            goto L_0x0486
        L_0x0642:
            double r7 = r34 * r24
            double r65 = r41 * r41
            double r7 = r7 + r65
            double r7 = org.apache.commons.math3.util.FastMath.sqrt(r7)
            int r3 = (r41 > r21 ? 1 : (r41 == r21 ? 0 : -1))
            if (r3 >= 0) goto L_0x0655
            double r7 = r7 - r41
            double r7 = r7 / r34
            goto L_0x0659
        L_0x0655:
            double r7 = r7 + r41
            double r7 = r24 / r7
        L_0x0659:
            int r3 = (r59 > r21 ? 1 : (r59 == r21 ? 0 : -1))
            if (r3 <= 0) goto L_0x0664
            double r2 = r26 / r59
            double r2 = org.apache.commons.math3.util.FastMath.min(r7, r2)
            goto L_0x0665
        L_0x0664:
            r2 = r7
        L_0x0665:
            r41 = r2
            r2 = 0
            r3 = -1
        L_0x0669:
            if (r2 >= r6) goto L_0x06b0
            double r65 = r15.getEntry(r2)
            int r9 = (r65 > r21 ? 1 : (r65 == r21 ? 0 : -1))
            if (r9 == 0) goto L_0x06ab
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trustRegionCenterOffset
            double r65 = r9.getEntry(r2)
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trialStepPoint
            double r67 = r9.getEntry(r2)
            double r65 = r65 + r67
            double r67 = r15.getEntry(r2)
            int r9 = (r67 > r21 ? 1 : (r67 == r21 ? 0 : -1))
            if (r9 <= 0) goto L_0x0696
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.upperDifference
            double r67 = r9.getEntry(r2)
            double r67 = r67 - r65
            double r65 = r15.getEntry(r2)
            goto L_0x06a2
        L_0x0696:
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.lowerDifference
            double r67 = r9.getEntry(r2)
            double r67 = r67 - r65
            double r65 = r15.getEntry(r2)
        L_0x06a2:
            double r67 = r67 / r65
            int r9 = (r67 > r41 ? 1 : (r67 == r41 ? 0 : -1))
            if (r9 >= 0) goto L_0x06ab
            r3 = r2
            r41 = r67
        L_0x06ab:
            int r2 = r2 + 1
            r21 = 0
            goto L_0x0669
        L_0x06b0:
            int r2 = (r41 > r21 ? 1 : (r41 == r21 ? 0 : -1))
            if (r2 <= 0) goto L_0x0731
            int r2 = r14 + 1
            double r13 = r59 / r34
            r9 = -1
            if (r3 != r9) goto L_0x06cf
            int r23 = (r13 > r21 ? 1 : (r13 == r21 ? 0 : -1))
            if (r23 <= 0) goto L_0x06cf
            r23 = r10
            r9 = r69
            double r9 = org.apache.commons.math3.util.FastMath.min(r9, r13)
            r19 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            int r51 = (r9 > r19 ? 1 : (r9 == r19 ? 0 : -1))
            if (r51 != 0) goto L_0x06d3
            r9 = r13
            goto L_0x06d3
        L_0x06cf:
            r23 = r10
            r9 = r69
        L_0x06d3:
            r13 = 0
            r55 = 0
        L_0x06d6:
            if (r13 >= r6) goto L_0x0715
            double r65 = r1.getEntry(r13)
            double r67 = r4.getEntry(r13)
            double r67 = r67 * r41
            double r4 = r65 + r67
            r1.setEntry(r13, r4)
            double r4 = r12.getEntry(r13)
            r21 = 0
            int r14 = (r4 > r21 ? 1 : (r4 == r21 ? 0 : -1))
            if (r14 != 0) goto L_0x06f9
            double r4 = r1.getEntry(r13)
            double r4 = r4 * r4
            double r55 = r55 + r4
        L_0x06f9:
            org.apache.commons.math3.linear.ArrayRealVector r4 = r0.trialStepPoint
            double r65 = r4.getEntry(r13)
            double r67 = r15.getEntry(r13)
            double r67 = r67 * r41
            r69 = r9
            double r9 = r65 + r67
            r4.setEntry(r13, r9)
            int r13 = r13 + 1
            r4 = r83
            r5 = r84
            r9 = r69
            goto L_0x06d6
        L_0x0715:
            r69 = r9
            double r57 = r57 * r41
            double r57 = r57 * r59
            double r4 = r26 - r57
            double r4 = r4 * r41
            r9 = 0
            double r13 = org.apache.commons.math3.util.FastMath.max(r4, r9)
            double r32 = r32 + r13
            r4 = r9
            r9 = r13
            r14 = r2
            r75 = r26
            r26 = r55
            r55 = r75
            goto L_0x0738
        L_0x0731:
            r23 = r10
            r4 = r21
            r9 = r69
            r9 = r4
        L_0x0738:
            if (r3 < 0) goto L_0x0786
            int r2 = r23 + 1
            r7 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r12.setEntry(r3, r7)
            double r9 = r15.getEntry(r3)
            int r13 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            r9 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            if (r13 >= 0) goto L_0x074e
            r12.setEntry(r3, r9)
        L_0x074e:
            org.apache.commons.math3.linear.ArrayRealVector r13 = r0.trialStepPoint
            double r17 = r13.getEntry(r3)
            double r17 = r17 * r17
            double r17 = r61 - r17
            int r13 = (r17 > r4 ? 1 : (r17 == r4 ? 0 : -1))
            if (r13 > 0) goto L_0x076f
            r4 = r83
            r5 = r84
            r24 = r2
            r51 = r3
            r7 = r11
            r2 = r12
            r25 = r14
            r3 = r15
            r9 = r17
            r42 = r40
            goto L_0x0482
        L_0x076f:
            r4 = r83
            r5 = r84
            r24 = r2
            r51 = r3
            r7 = r11
            r2 = r12
            r25 = r14
            r3 = r15
            r9 = r17
            r42 = r40
            r40 = r69
            r8 = 20
            goto L_0x0486
        L_0x0786:
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r2 = (r41 > r7 ? 1 : (r41 == r7 ? 0 : -1))
            if (r2 >= 0) goto L_0x07ca
            r2 = r54
            if (r14 != r2) goto L_0x07a3
        L_0x0792:
            r4 = r83
            r5 = r84
            r54 = r2
            r51 = r3
            r7 = r11
            r2 = r12
            r25 = r14
            r3 = r15
            r24 = r23
            goto L_0x047e
        L_0x07a3:
            r7 = 4576918229304087675(0x3f847ae147ae147b, double:0.01)
            double r7 = r7 * r32
            int r13 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r13 > 0) goto L_0x07af
            goto L_0x0792
        L_0x07af:
            double r38 = r26 / r55
            r4 = r83
            r5 = r84
            r54 = r2
            r51 = r3
            r7 = r11
            r2 = r12
            r25 = r14
            r3 = r15
            r24 = r23
            r42 = r40
            r9 = r61
            r40 = r69
            r8 = 30
            goto L_0x0486
        L_0x07ca:
            r2 = r54
            r8 = r3
            r3 = 90
        L_0x07cf:
            printState(r3)
            r51 = r8
            r42 = r14
            r7 = 100
            r69 = 0
        L_0x07da:
            printState(r7)
            int r8 = r6 + -1
            r13 = r23
            if (r13 < r8) goto L_0x0804
            r4 = r83
            r5 = r84
            r54 = r2
            r7 = r11
            r2 = r12
            r24 = r13
            r3 = r15
            r25 = r42
            r9 = r61
            r8 = 190(0xbe, float:2.66E-43)
            r11 = 90
            r12 = 50
            r13 = 30
            r14 = 20
            r15 = 190(0xbe, float:2.66E-43)
            r42 = r40
            r40 = r69
            goto L_0x00af
        L_0x0804:
            r8 = 0
            r26 = 0
            r28 = 0
            r30 = 0
        L_0x080b:
            if (r8 >= r6) goto L_0x0849
            double r9 = r12.getEntry(r8)
            r19 = 0
            int r14 = (r9 > r19 ? 1 : (r9 == r19 ? 0 : -1))
            if (r14 != 0) goto L_0x0841
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trialStepPoint
            double r9 = r9.getEntry(r8)
            double r9 = r9 * r9
            double r28 = r28 + r9
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trialStepPoint
            double r9 = r9.getEntry(r8)
            double r19 = r1.getEntry(r8)
            double r9 = r9 * r19
            double r30 = r30 + r9
            double r9 = r1.getEntry(r8)
            double r9 = r9 * r9
            double r26 = r26 + r9
            org.apache.commons.math3.linear.ArrayRealVector r9 = r0.trialStepPoint
            double r9 = r9.getEntry(r8)
            r15.setEntry(r8, r9)
            goto L_0x0846
        L_0x0841:
            r9 = 0
            r15.setEntry(r8, r9)
        L_0x0846:
            int r8 = r8 + 1
            goto L_0x080b
        L_0x0849:
            r4 = r83
            r5 = r84
            r54 = r2
            r7 = r11
            r2 = r12
            r24 = r13
            r3 = r15
            r25 = r42
        L_0x0856:
            r9 = r61
            r40 = r69
            r8 = 210(0xd2, float:2.94E-43)
            goto L_0x0486
        L_0x085e:
            r12 = r2
            r15 = r3
            r11 = r7
            r61 = r9
            r13 = r24
            r14 = r25
            r9 = r40
            r40 = r42
            r8 = r51
            r2 = r54
            r3 = 90
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r7 = 100
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r19 = 20
            r20 = 30
            goto L_0x089e
        L_0x087c:
            r12 = r2
            r15 = r3
            r11 = r7
            r61 = r9
            r13 = r24
            r14 = r25
            r9 = r40
            r40 = r42
            r8 = r51
            r2 = r54
            r3 = 90
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r7 = 100
            r17 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r19 = 20
            printState(r19)
            r20 = 30
            r38 = 0
        L_0x089e:
            printState(r20)
            r3 = 0
            r34 = 0
        L_0x08a4:
            if (r3 >= r6) goto L_0x08dd
            double r41 = r12.getEntry(r3)
            r4 = 0
            int r21 = (r41 > r4 ? 1 : (r41 == r4 ? 0 : -1))
            if (r21 == 0) goto L_0x08b4
            r15.setEntry(r3, r4)
            goto L_0x08d0
        L_0x08b4:
            int r23 = (r38 > r4 ? 1 : (r38 == r4 ? 0 : -1))
            if (r23 != 0) goto L_0x08c1
            double r4 = r1.getEntry(r3)
            double r4 = -r4
            r15.setEntry(r3, r4)
            goto L_0x08d0
        L_0x08c1:
            double r4 = r15.getEntry(r3)
            double r4 = r4 * r38
            double r41 = r1.getEntry(r3)
            double r4 = r4 - r41
            r15.setEntry(r3, r4)
        L_0x08d0:
            double r4 = r15.getEntry(r3)
            double r4 = r4 * r4
            double r34 = r34 + r4
            int r3 = r3 + 1
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            goto L_0x08a4
        L_0x08dd:
            r3 = 0
            int r5 = (r34 > r3 ? 1 : (r34 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x0906
            r4 = r83
            r5 = r84
            r54 = r2
        L_0x08e9:
            r51 = r8
            r7 = r11
            r2 = r12
            r24 = r13
            r25 = r14
            r3 = r15
            r42 = r40
            r8 = 190(0xbe, float:2.66E-43)
        L_0x08f6:
            r11 = 90
            r12 = 50
            r13 = 30
            r14 = 20
            r15 = 190(0xbe, float:2.66E-43)
        L_0x0900:
            r40 = r9
            r9 = r61
            goto L_0x00af
        L_0x0906:
            int r5 = (r38 > r3 ? 1 : (r38 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x0912
            int r2 = r14 + r6
            int r2 = r2 - r13
            r54 = r2
            r26 = r34
            goto L_0x0914
        L_0x0912:
            r54 = r2
        L_0x0914:
            double r21 = r26 * r61
            r41 = 4547007122018943789(0x3f1a36e2eb1c432d, double:1.0E-4)
            double r41 = r41 * r32
            double r41 = r41 * r32
            int r2 = (r21 > r41 ? 1 : (r21 == r41 ? 0 : -1))
            if (r2 > 0) goto L_0x0928
            r4 = r83
            r5 = r84
            goto L_0x08e9
        L_0x0928:
            r4 = r83
            r5 = r84
            r51 = r8
            r7 = r11
            r2 = r12
            r24 = r13
            r25 = r14
            r3 = r15
            r42 = r40
            r8 = 210(0xd2, float:2.94E-43)
            goto L_0x08f6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.math3.optimization.direct.BOBYQAOptimizer.trsbox(double, org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector, org.apache.commons.math3.linear.ArrayRealVector):double[]");
    }

    private void update(double d, double d2, int i) {
        double d3;
        int i2 = i;
        printMethod();
        int dimension = this.currentBest.getDimension();
        int i3 = this.numberOfInterpolationPoints;
        int i4 = 1;
        int i5 = (i3 - dimension) - 1;
        ArrayRealVector arrayRealVector = new ArrayRealVector(i3 + dimension);
        int i6 = 0;
        int i7 = 0;
        double d4 = 0.0d;
        while (i7 < i3) {
            double d5 = d4;
            for (int i8 = 0; i8 < i5; i8++) {
                d5 = FastMath.max(d5, FastMath.abs(this.zMatrix.getEntry(i7, i8)));
            }
            i7++;
            d4 = d5;
        }
        double d6 = d4 * 1.0E-20d;
        while (i4 < i5) {
            if (FastMath.abs(this.zMatrix.getEntry(i2, i4)) > d6) {
                double entry = this.zMatrix.getEntry(i2, 0);
                double entry2 = this.zMatrix.getEntry(i2, i4);
                double sqrt = FastMath.sqrt((entry * entry) + (entry2 * entry2));
                double entry3 = this.zMatrix.getEntry(i2, 0) / sqrt;
                double entry4 = this.zMatrix.getEntry(i2, i4) / sqrt;
                int i9 = 0;
                while (i9 < i3) {
                    double entry5 = (this.zMatrix.getEntry(i9, 0) * entry3) + (this.zMatrix.getEntry(i9, i4) * entry4);
                    Array2DRowRealMatrix array2DRowRealMatrix = this.zMatrix;
                    array2DRowRealMatrix.setEntry(i9, i4, (array2DRowRealMatrix.getEntry(i9, i4) * entry3) - (this.zMatrix.getEntry(i9, 0) * entry4));
                    this.zMatrix.setEntry(i9, 0, entry5);
                    i9++;
                    d6 = d6;
                }
            }
            this.zMatrix.setEntry(i2, i4, 0.0d);
            i4++;
            d6 = d6;
        }
        for (int i10 = 0; i10 < i3; i10++) {
            arrayRealVector.setEntry(i10, this.zMatrix.getEntry(i2, 0) * this.zMatrix.getEntry(i10, 0));
        }
        double entry6 = arrayRealVector.getEntry(i2);
        double entry7 = this.lagrangeValuesAtNewPoint.getEntry(i2);
        ArrayRealVector arrayRealVector2 = this.lagrangeValuesAtNewPoint;
        arrayRealVector2.setEntry(i2, arrayRealVector2.getEntry(i2) - 1.0d);
        double sqrt2 = FastMath.sqrt(d2);
        double d7 = entry7 / sqrt2;
        double entry8 = this.zMatrix.getEntry(i2, 0) / sqrt2;
        int i11 = 0;
        while (i11 < i3) {
            Array2DRowRealMatrix array2DRowRealMatrix2 = this.zMatrix;
            i6 = 0;
            array2DRowRealMatrix2.setEntry(i11, 0, (array2DRowRealMatrix2.getEntry(i11, i6) * d7) - (this.lagrangeValuesAtNewPoint.getEntry(i11) * entry8));
            i11++;
            d7 = d7;
        }
        int i12 = 0;
        while (i12 < dimension) {
            int i13 = i3 + i12;
            arrayRealVector.setEntry(i13, this.bMatrix.getEntry(i2, i12));
            double entry9 = ((this.lagrangeValuesAtNewPoint.getEntry(i13) * entry6) - (arrayRealVector.getEntry(i13) * entry7)) / d2;
            int i14 = i12;
            double entry10 = (((-d) * arrayRealVector.getEntry(i13)) - (this.lagrangeValuesAtNewPoint.getEntry(i13) * entry7)) / d2;
            int i15 = 0;
            while (i15 <= i13) {
                int i16 = dimension;
                Array2DRowRealMatrix array2DRowRealMatrix3 = this.bMatrix;
                double d8 = entry6;
                int i17 = i14;
                ArrayRealVector arrayRealVector3 = arrayRealVector;
                array2DRowRealMatrix3.setEntry(i15, i17, array2DRowRealMatrix3.getEntry(i15, i17) + (this.lagrangeValuesAtNewPoint.getEntry(i15) * entry9) + (arrayRealVector.getEntry(i15) * entry10));
                if (i15 >= i3) {
                    Array2DRowRealMatrix array2DRowRealMatrix4 = this.bMatrix;
                    d3 = entry7;
                    array2DRowRealMatrix4.setEntry(i13, i15 - i3, array2DRowRealMatrix4.getEntry(i15, i17));
                } else {
                    d3 = entry7;
                }
                i15++;
                arrayRealVector = arrayRealVector3;
                dimension = i16;
                entry7 = d3;
                i14 = i17;
                entry6 = d8;
            }
            i12 = i14 + 1;
            i2 = i;
            entry6 = entry6;
        }
    }

    private void setup(double[] dArr, double[] dArr2) {
        printMethod();
        int length = getStartPoint().length;
        if (length >= 2) {
            int i = length + 2;
            int i2 = length + 1;
            int[] iArr = {i, (i * i2) / 2};
            int i3 = this.numberOfInterpolationPoints;
            if (i3 < iArr[0] || i3 > iArr[1]) {
                throw new OutOfRangeException(LocalizedFormats.NUMBER_OF_INTERPOLATION_POINTS, Integer.valueOf(this.numberOfInterpolationPoints), Integer.valueOf(iArr[0]), Integer.valueOf(iArr[1]));
            }
            this.boundDifference = new double[length];
            double d = this.initialTrustRegionRadius * TWO;
            double d2 = Double.POSITIVE_INFINITY;
            for (int i4 = 0; i4 < length; i4++) {
                double[] dArr3 = this.boundDifference;
                dArr3[i4] = dArr2[i4] - dArr[i4];
                d2 = FastMath.min(d2, dArr3[i4]);
            }
            if (d2 < d) {
                this.initialTrustRegionRadius = d2 / 3.0d;
            }
            this.bMatrix = new Array2DRowRealMatrix(this.numberOfInterpolationPoints + length, length);
            int i5 = this.numberOfInterpolationPoints;
            this.zMatrix = new Array2DRowRealMatrix(i5, (i5 - length) - 1);
            this.interpolationPoints = new Array2DRowRealMatrix(this.numberOfInterpolationPoints, length);
            this.originShift = new ArrayRealVector(length);
            this.fAtInterpolationPoints = new ArrayRealVector(this.numberOfInterpolationPoints);
            this.trustRegionCenterOffset = new ArrayRealVector(length);
            this.gradientAtTrustRegionCenter = new ArrayRealVector(length);
            this.lowerDifference = new ArrayRealVector(length);
            this.upperDifference = new ArrayRealVector(length);
            this.modelSecondDerivativesParameters = new ArrayRealVector(this.numberOfInterpolationPoints);
            this.newPoint = new ArrayRealVector(length);
            this.alternativeNewPoint = new ArrayRealVector(length);
            this.trialStepPoint = new ArrayRealVector(length);
            this.lagrangeValuesAtNewPoint = new ArrayRealVector(this.numberOfInterpolationPoints + length);
            this.modelSecondDerivativesValues = new ArrayRealVector((length * i2) / 2);
            return;
        }
        throw new NumberIsTooSmallException(Integer.valueOf(length), 2, true);
    }

    /* access modifiers changed from: private */
    public static String caller(int i) {
        StackTraceElement stackTraceElement = new Throwable().getStackTrace()[i];
        return stackTraceElement.getMethodName() + " (at line " + stackTraceElement.getLineNumber() + ")";
    }

    private static class PathIsExploredException extends RuntimeException {
        private static final String PATH_IS_EXPLORED = "If this exception is thrown, just remove it from the code";
        private static final long serialVersionUID = 745350979634801853L;

        PathIsExploredException() {
            super("If this exception is thrown, just remove it from the code " + BOBYQAOptimizer.caller(3));
        }
    }
}
