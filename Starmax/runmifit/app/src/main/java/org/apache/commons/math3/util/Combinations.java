package org.apache.commons.math3.util;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MathInternalError;
import org.apache.commons.math3.exception.OutOfRangeException;

public class Combinations implements Iterable<int[]> {
    private final IterationOrder iterationOrder;

    /* renamed from: k */
    private final int f8193k;

    /* renamed from: n */
    private final int f8194n;

    private enum IterationOrder {
        LEXICOGRAPHIC
    }

    public Combinations(int i, int i2) {
        this(i, i2, IterationOrder.LEXICOGRAPHIC);
    }

    private Combinations(int i, int i2, IterationOrder iterationOrder2) {
        CombinatoricsUtils.checkBinomial(i, i2);
        this.f8194n = i;
        this.f8193k = i2;
        this.iterationOrder = iterationOrder2;
    }

    public int getN() {
        return this.f8194n;
    }

    public int getK() {
        return this.f8193k;
    }

    public Iterator<int[]> iterator() {
        int i = this.f8193k;
        if (i == 0 || i == this.f8194n) {
            return new SingletonIterator(MathArrays.natural(this.f8193k));
        }
        if (C36161.f8195xf3d2afa[this.iterationOrder.ordinal()] == 1) {
            return new LexicographicIterator(this.f8194n, this.f8193k);
        }
        throw new MathInternalError();
    }

    /* renamed from: org.apache.commons.math3.util.Combinations$1 */
    static /* synthetic */ class C36161 {

        /* renamed from: $SwitchMap$org$apache$commons$math3$util$Combinations$IterationOrder */
        static final /* synthetic */ int[] f8195xf3d2afa = new int[IterationOrder.values().length];

        static {
            try {
                f8195xf3d2afa[IterationOrder.LEXICOGRAPHIC.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public Comparator<int[]> comparator() {
        return new LexicographicComparator(this.f8194n, this.f8193k);
    }

    private static class LexicographicIterator implements Iterator<int[]> {

        /* renamed from: c */
        private final int[] f8198c;

        /* renamed from: j */
        private int f8199j;

        /* renamed from: k */
        private final int f8200k;
        private boolean more = true;

        public LexicographicIterator(int i, int i2) {
            this.f8200k = i2;
            this.f8198c = new int[(i2 + 3)];
            if (i2 == 0 || i2 >= i) {
                this.more = false;
                return;
            }
            for (int i3 = 1; i3 <= i2; i3++) {
                this.f8198c[i3] = i3 - 1;
            }
            int[] iArr = this.f8198c;
            iArr[i2 + 1] = i;
            iArr[i2 + 2] = 0;
            this.f8199j = i2;
        }

        public boolean hasNext() {
            return this.more;
        }

        public int[] next() {
            if (this.more) {
                int i = this.f8200k;
                int[] iArr = new int[i];
                System.arraycopy(this.f8198c, 1, iArr, 0, i);
                int i2 = this.f8199j;
                if (i2 > 0) {
                    this.f8198c[i2] = i2;
                    this.f8199j = i2 - 1;
                    return iArr;
                }
                int[] iArr2 = this.f8198c;
                if (iArr2[1] + 1 < iArr2[2]) {
                    iArr2[1] = iArr2[1] + 1;
                    return iArr;
                }
                this.f8199j = 2;
                boolean z = false;
                int i3 = 0;
                while (!z) {
                    int[] iArr3 = this.f8198c;
                    int i4 = this.f8199j;
                    iArr3[i4 - 1] = i4 - 2;
                    int i5 = iArr3[i4] + 1;
                    if (i5 == iArr3[i4 + 1]) {
                        this.f8199j = i4 + 1;
                        i3 = i5;
                    } else {
                        i3 = i5;
                        z = true;
                    }
                }
                int i6 = this.f8199j;
                if (i6 > this.f8200k) {
                    this.more = false;
                    return iArr;
                }
                this.f8198c[i6] = i3;
                this.f8199j = i6 - 1;
                return iArr;
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static class SingletonIterator implements Iterator<int[]> {
        private boolean more = true;
        private final int[] singleton;

        public SingletonIterator(int[] iArr) {
            this.singleton = iArr;
        }

        public boolean hasNext() {
            return this.more;
        }

        public int[] next() {
            if (this.more) {
                this.more = false;
                return this.singleton;
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static class LexicographicComparator implements Comparator<int[]>, Serializable {
        private static final long serialVersionUID = 20130906;

        /* renamed from: k */
        private final int f8196k;

        /* renamed from: n */
        private final int f8197n;

        public LexicographicComparator(int i, int i2) {
            this.f8197n = i;
            this.f8196k = i2;
        }

        public int compare(int[] iArr, int[] iArr2) {
            int length = iArr.length;
            int i = this.f8196k;
            if (length != i) {
                throw new DimensionMismatchException(iArr.length, i);
            } else if (iArr2.length == i) {
                int[] copyOf = MathArrays.copyOf(iArr);
                Arrays.sort(copyOf);
                int[] copyOf2 = MathArrays.copyOf(iArr2);
                Arrays.sort(copyOf2);
                long lexNorm = lexNorm(copyOf);
                long lexNorm2 = lexNorm(copyOf2);
                if (lexNorm < lexNorm2) {
                    return -1;
                }
                return lexNorm > lexNorm2 ? 1 : 0;
            } else {
                throw new DimensionMismatchException(iArr2.length, i);
            }
        }

        private long lexNorm(int[] iArr) {
            int i;
            long j = 0;
            for (int i2 = 0; i2 < iArr.length; i2++) {
                int i3 = iArr[i2];
                if (i3 < 0 || i3 >= (i = this.f8197n)) {
                    throw new OutOfRangeException(Integer.valueOf(i3), 0, Integer.valueOf(this.f8197n - 1));
                }
                j += (long) (iArr[i2] * ArithmeticUtils.pow(i, i2));
            }
            return j;
        }
    }
}
