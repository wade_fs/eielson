package org.apache.commons.math3.p067ml.clustering;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.MathIllegalStateException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.p067ml.clustering.Clusterable;
import org.apache.commons.math3.p067ml.distance.DistanceMeasure;
import org.apache.commons.math3.p067ml.distance.EuclideanDistance;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.MathUtils;

/* renamed from: org.apache.commons.math3.ml.clustering.FuzzyKMeansClusterer */
public class FuzzyKMeansClusterer<T extends Clusterable> extends Clusterer<T> {
    private static final double DEFAULT_EPSILON = 0.001d;
    private List<CentroidCluster<T>> clusters;
    private final double epsilon;
    private final double fuzziness;

    /* renamed from: k */
    private final int f8058k;
    private final int maxIterations;
    private double[][] membershipMatrix;
    private List<T> points;
    private final RandomGenerator random;

    public FuzzyKMeansClusterer(int i, double d) throws NumberIsTooSmallException {
        this(i, d, -1, new EuclideanDistance());
    }

    public FuzzyKMeansClusterer(int i, double d, int i2, DistanceMeasure distanceMeasure) throws NumberIsTooSmallException {
        this(i, d, i2, distanceMeasure, DEFAULT_EPSILON, new JDKRandomGenerator());
    }

    public FuzzyKMeansClusterer(int i, double d, int i2, DistanceMeasure distanceMeasure, double d2, RandomGenerator randomGenerator) throws NumberIsTooSmallException {
        super(distanceMeasure);
        if (d > 1.0d) {
            this.f8058k = i;
            this.fuzziness = d;
            this.maxIterations = i2;
            this.epsilon = d2;
            this.random = randomGenerator;
            this.membershipMatrix = null;
            this.points = null;
            this.clusters = null;
            return;
        }
        throw new NumberIsTooSmallException(Double.valueOf(d), Double.valueOf(1.0d), false);
    }

    public int getK() {
        return this.f8058k;
    }

    public double getFuzziness() {
        return this.fuzziness;
    }

    public int getMaxIterations() {
        return this.maxIterations;
    }

    public double getEpsilon() {
        return this.epsilon;
    }

    public RandomGenerator getRandomGenerator() {
        return this.random;
    }

    public RealMatrix getMembershipMatrix() {
        double[][] dArr = this.membershipMatrix;
        if (dArr != null) {
            return MatrixUtils.createRealMatrix(dArr);
        }
        throw new MathIllegalStateException();
    }

    public List<T> getDataPoints() {
        return this.points;
    }

    public List<CentroidCluster<T>> getClusters() {
        return this.clusters;
    }

    public double getObjectiveFunctionValue() {
        List<T> list = this.points;
        if (list == null || this.clusters == null) {
            throw new MathIllegalStateException();
        }
        double d = 0.0d;
        int i = 0;
        for (T t : list) {
            int i2 = 0;
            for (CentroidCluster<T> centroidCluster : this.clusters) {
                double distance = distance(t, centroidCluster.getCenter());
                d += distance * distance * FastMath.pow(this.membershipMatrix[i][i2], this.fuzziness);
                i2++;
            }
            i++;
        }
        return d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<double>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    public List<CentroidCluster<T>> cluster(Collection<T> collection) throws MathIllegalArgumentException {
        Class<double> cls = double.class;
        MathUtils.checkNotNull(collection);
        int size = collection.size();
        int i = 0;
        if (size >= this.f8058k) {
            this.points = Collections.unmodifiableList(new ArrayList(collection));
            this.clusters = new ArrayList();
            this.membershipMatrix = (double[][]) Array.newInstance((Class<?>) cls, size, this.f8058k);
            double[][] dArr = (double[][]) Array.newInstance((Class<?>) cls, size, this.f8058k);
            if (size == 0) {
                return this.clusters;
            }
            initializeMembershipMatrix();
            int length = ((Clusterable) this.points.get(0)).getPoint().length;
            for (int i2 = 0; i2 < this.f8058k; i2++) {
                this.clusters.add(new CentroidCluster(new DoublePoint(new double[length])));
            }
            int i3 = this.maxIterations;
            if (i3 < 0) {
                i3 = Integer.MAX_VALUE;
            }
            do {
                saveMembershipMatrix(dArr);
                updateClusterCenters();
                updateMembershipMatrix();
                if (calculateMaxMembershipChange(dArr) <= this.epsilon) {
                    break;
                }
                i++;
            } while (i < i3);
            return this.clusters;
        }
        throw new NumberIsTooSmallException(Integer.valueOf(size), Integer.valueOf(this.f8058k), false);
    }

    private void updateClusterCenters() {
        ArrayList arrayList = new ArrayList(this.f8058k);
        int i = 0;
        for (CentroidCluster<T> centroidCluster : this.clusters) {
            double[] dArr = new double[centroidCluster.getCenter().getPoint().length];
            double d = 0.0d;
            int i2 = 0;
            for (T t : this.points) {
                double pow = FastMath.pow(this.membershipMatrix[i2][i], this.fuzziness);
                double[] point = t.getPoint();
                for (int i3 = 0; i3 < dArr.length; i3++) {
                    dArr[i3] = dArr[i3] + (point[i3] * pow);
                }
                d += pow;
                i2++;
            }
            MathArrays.scaleInPlace(1.0d / d, dArr);
            arrayList.add(new CentroidCluster(new DoublePoint(dArr)));
            i++;
        }
        this.clusters.clear();
        this.clusters = arrayList;
    }

    private void updateMembershipMatrix() {
        int i = 0;
        while (i < this.points.size()) {
            Clusterable clusterable = (Clusterable) this.points.get(i);
            int i2 = 0;
            int i3 = -1;
            double d = 0.0d;
            while (i2 < this.clusters.size()) {
                double abs = FastMath.abs(distance(clusterable, this.clusters.get(i2).getCenter()));
                double d2 = 0.0d;
                for (CentroidCluster<T> centroidCluster : this.clusters) {
                    d2 += FastMath.pow(abs / FastMath.abs(distance(clusterable, centroidCluster.getCenter())), 2.0d / (this.fuzziness - 1.0d));
                    i = i;
                }
                int i4 = i;
                double[][] dArr = this.membershipMatrix;
                dArr[i4][i2] = 1.0d / d2;
                if (dArr[i4][i2] > d) {
                    i3 = i2;
                    d = dArr[i4][i2];
                }
                i2++;
                i = i4;
            }
            this.clusters.get(i3).addPoint(clusterable);
            i++;
        }
    }

    private void initializeMembershipMatrix() {
        for (int i = 0; i < this.points.size(); i++) {
            for (int i2 = 0; i2 < this.f8058k; i2++) {
                this.membershipMatrix[i][i2] = this.random.nextDouble();
            }
            double[][] dArr = this.membershipMatrix;
            dArr[i] = MathArrays.normalizeArray(dArr[i], 1.0d);
        }
    }

    private double calculateMaxMembershipChange(double[][] dArr) {
        double d = 0.0d;
        int i = 0;
        while (i < this.points.size()) {
            double d2 = d;
            for (int i2 = 0; i2 < this.clusters.size(); i2++) {
                d2 = FastMath.max(FastMath.abs(this.membershipMatrix[i][i2] - dArr[i][i2]), d2);
            }
            i++;
            d = d2;
        }
        return d;
    }

    private void saveMembershipMatrix(double[][] dArr) {
        for (int i = 0; i < this.points.size(); i++) {
            System.arraycopy(this.membershipMatrix[i], 0, dArr[i], 0, this.clusters.size());
        }
    }
}
