package org.apache.commons.math3.p067ml.neuralnet.sofm.util;

import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.util.FastMath;

/* renamed from: org.apache.commons.math3.ml.neuralnet.sofm.util.ExponentialDecayFunction */
public class ExponentialDecayFunction {

    /* renamed from: a */
    private final double f8061a;
    private final double oneOverB;

    public ExponentialDecayFunction(double d, double d2, long j) {
        if (d <= 0.0d) {
            throw new NotStrictlyPositiveException(Double.valueOf(d));
        } else if (d2 <= 0.0d) {
            throw new NotStrictlyPositiveException(Double.valueOf(d2));
        } else if (d2 >= d) {
            throw new NumberIsTooLargeException(Double.valueOf(d2), Double.valueOf(d), false);
        } else if (j > 0) {
            this.f8061a = d;
            double d3 = (double) j;
            Double.isNaN(d3);
            this.oneOverB = (-FastMath.log(d2 / d)) / d3;
        } else {
            throw new NotStrictlyPositiveException(Long.valueOf(j));
        }
    }

    public double value(long j) {
        double d = this.f8061a;
        double d2 = (double) (-j);
        double d3 = this.oneOverB;
        Double.isNaN(d2);
        return d * FastMath.exp(d2 * d3);
    }
}
