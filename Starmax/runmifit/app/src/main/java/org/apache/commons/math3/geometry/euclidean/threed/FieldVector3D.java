package org.apache.commons.math3.geometry.euclidean.threed;

import com.google.api.client.http.HttpStatusCodes;
import java.io.Serializable;
import java.text.NumberFormat;
import org.apache.commons.math3.RealFieldElement;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;

public class FieldVector3D<T extends RealFieldElement<T>> implements Serializable {
    private static final long serialVersionUID = 20130224;

    /* renamed from: x */
    private final T f8002x;

    /* renamed from: y */
    private final T f8003y;

    /* renamed from: z */
    private final T f8004z;

    public FieldVector3D(T t, T t2, T t3) {
        this.f8002x = t;
        this.f8003y = t2;
        this.f8004z = t3;
    }

    public FieldVector3D(T[] tArr) throws DimensionMismatchException {
        if (tArr.length == 3) {
            this.f8002x = tArr[0];
            this.f8003y = tArr[1];
            this.f8004z = tArr[2];
            return;
        }
        throw new DimensionMismatchException(tArr.length, 3);
    }

    public FieldVector3D(RealFieldElement realFieldElement, RealFieldElement realFieldElement2) {
        RealFieldElement realFieldElement3 = (RealFieldElement) realFieldElement2.cos();
        this.f8002x = (RealFieldElement) ((RealFieldElement) realFieldElement.cos()).multiply(realFieldElement3);
        this.f8003y = (RealFieldElement) ((RealFieldElement) realFieldElement.sin()).multiply(realFieldElement3);
        this.f8004z = (RealFieldElement) realFieldElement2.sin();
    }

    public FieldVector3D(RealFieldElement realFieldElement, FieldVector3D fieldVector3D) {
        this.f8002x = (RealFieldElement) realFieldElement.multiply(fieldVector3D.f8002x);
        this.f8003y = (RealFieldElement) realFieldElement.multiply(fieldVector3D.f8003y);
        this.f8004z = (RealFieldElement) realFieldElement.multiply(fieldVector3D.f8004z);
    }

    public FieldVector3D(RealFieldElement realFieldElement, Vector3D vector3D) {
        this.f8002x = (RealFieldElement) realFieldElement.multiply(vector3D.getX());
        this.f8003y = (RealFieldElement) realFieldElement.multiply(vector3D.getY());
        this.f8004z = (RealFieldElement) realFieldElement.multiply(vector3D.getZ());
    }

    public FieldVector3D(double d, FieldVector3D fieldVector3D) {
        this.f8002x = (RealFieldElement) fieldVector3D.f8002x.multiply(d);
        this.f8003y = (RealFieldElement) fieldVector3D.f8003y.multiply(d);
        this.f8004z = (RealFieldElement) fieldVector3D.f8004z.multiply(d);
    }

    public FieldVector3D(T t, FieldVector3D<T> fieldVector3D, T t2, FieldVector3D<T> fieldVector3D2) {
        this.f8002x = (RealFieldElement) t.linearCombination(t, fieldVector3D.getX(), t2, fieldVector3D2.getX());
        this.f8003y = (RealFieldElement) t.linearCombination(t, fieldVector3D.getY(), t2, fieldVector3D2.getY());
        this.f8004z = (RealFieldElement) t.linearCombination(t, fieldVector3D.getZ(), t2, fieldVector3D2.getZ());
    }

    public FieldVector3D(T t, Vector3D vector3D, T t2, Vector3D vector3D2) {
        this.f8002x = (RealFieldElement) t.linearCombination(vector3D.getX(), t, vector3D2.getX(), t2);
        T t3 = t;
        T t4 = t;
        T t5 = t2;
        this.f8003y = (RealFieldElement) t3.linearCombination(vector3D.getY(), t4, vector3D2.getY(), t5);
        this.f8004z = (RealFieldElement) t3.linearCombination(vector3D.getZ(), t4, vector3D2.getZ(), t5);
    }

    public FieldVector3D(double d, FieldVector3D<T> fieldVector3D, double d2, FieldVector3D<T> fieldVector3D2) {
        T x = fieldVector3D.getX();
        double d3 = d;
        double d4 = d2;
        this.f8002x = (RealFieldElement) x.linearCombination(d3, fieldVector3D.getX(), d4, fieldVector3D2.getX());
        this.f8003y = (RealFieldElement) x.linearCombination(d3, fieldVector3D.getY(), d4, fieldVector3D2.getY());
        this.f8004z = (RealFieldElement) x.linearCombination(d3, fieldVector3D.getZ(), d4, fieldVector3D2.getZ());
    }

    public FieldVector3D(T t, FieldVector3D<T> fieldVector3D, T t2, FieldVector3D<T> fieldVector3D2, T t3, FieldVector3D<T> fieldVector3D3) {
        T t4 = t;
        this.f8002x = (RealFieldElement) t.linearCombination(t4, fieldVector3D.getX(), t2, fieldVector3D2.getX(), t3, fieldVector3D3.getX());
        T t5 = t;
        T t6 = t2;
        T t7 = t3;
        this.f8003y = (RealFieldElement) t4.linearCombination(t5, fieldVector3D.getY(), t6, fieldVector3D2.getY(), t7, fieldVector3D3.getY());
        this.f8004z = (RealFieldElement) t4.linearCombination(t5, fieldVector3D.getZ(), t6, fieldVector3D2.getZ(), t7, fieldVector3D3.getZ());
    }

    public FieldVector3D(T t, Vector3D vector3D, T t2, Vector3D vector3D2, T t3, Vector3D vector3D3) {
        this.f8002x = (RealFieldElement) t.linearCombination(vector3D.getX(), t, vector3D2.getX(), t2, vector3D3.getX(), t3);
        T t4 = t;
        T t5 = t;
        T t6 = t2;
        T t7 = t3;
        this.f8003y = (RealFieldElement) t4.linearCombination(vector3D.getY(), t5, vector3D2.getY(), t6, vector3D3.getY(), t7);
        this.f8004z = (RealFieldElement) t4.linearCombination(vector3D.getZ(), t5, vector3D2.getZ(), t6, vector3D3.getZ(), t7);
    }

    public FieldVector3D(double d, FieldVector3D<T> fieldVector3D, double d2, FieldVector3D<T> fieldVector3D2, double d3, FieldVector3D<T> fieldVector3D3) {
        T x = fieldVector3D.getX();
        double d4 = d;
        double d5 = d2;
        double d6 = d3;
        this.f8002x = (RealFieldElement) x.linearCombination(d4, fieldVector3D.getX(), d5, fieldVector3D2.getX(), d6, fieldVector3D3.getX());
        this.f8003y = (RealFieldElement) x.linearCombination(d4, fieldVector3D.getY(), d5, fieldVector3D2.getY(), d6, fieldVector3D3.getY());
        this.f8004z = (RealFieldElement) x.linearCombination(d4, fieldVector3D.getZ(), d5, fieldVector3D2.getZ(), d6, fieldVector3D3.getZ());
    }

    public FieldVector3D(T t, FieldVector3D<T> fieldVector3D, T t2, FieldVector3D<T> fieldVector3D2, T t3, FieldVector3D<T> fieldVector3D3, T t4, FieldVector3D<T> fieldVector3D4) {
        T t5 = t;
        this.f8002x = (RealFieldElement) t.linearCombination(t5, fieldVector3D.getX(), t2, fieldVector3D2.getX(), t3, fieldVector3D3.getX(), t4, fieldVector3D4.getX());
        T t6 = t;
        T t7 = t2;
        T t8 = t3;
        T t9 = t4;
        this.f8003y = (RealFieldElement) t5.linearCombination(t6, fieldVector3D.getY(), t7, fieldVector3D2.getY(), t8, fieldVector3D3.getY(), t9, fieldVector3D4.getY());
        this.f8004z = (RealFieldElement) t5.linearCombination(t6, fieldVector3D.getZ(), t7, fieldVector3D2.getZ(), t8, fieldVector3D3.getZ(), t9, fieldVector3D4.getZ());
    }

    public FieldVector3D(T t, Vector3D vector3D, T t2, Vector3D vector3D2, T t3, Vector3D vector3D3, T t4, Vector3D vector3D4) {
        this.f8002x = (RealFieldElement) t.linearCombination(vector3D.getX(), t, vector3D2.getX(), t2, vector3D3.getX(), t3, vector3D4.getX(), t4);
        T t5 = t;
        T t6 = t;
        T t7 = t2;
        T t8 = t3;
        T t9 = t4;
        this.f8003y = (RealFieldElement) t5.linearCombination(vector3D.getY(), t6, vector3D2.getY(), t7, vector3D3.getY(), t8, vector3D4.getY(), t9);
        this.f8004z = (RealFieldElement) t5.linearCombination(vector3D.getZ(), t6, vector3D2.getZ(), t7, vector3D3.getZ(), t8, vector3D4.getZ(), t9);
    }

    public FieldVector3D(double d, FieldVector3D<T> fieldVector3D, double d2, FieldVector3D<T> fieldVector3D2, double d3, FieldVector3D<T> fieldVector3D3, double d4, FieldVector3D<T> fieldVector3D4) {
        T x = fieldVector3D.getX();
        double d5 = d;
        double d6 = d2;
        double d7 = d3;
        double d8 = d4;
        this.f8002x = (RealFieldElement) x.linearCombination(d5, fieldVector3D.getX(), d6, fieldVector3D2.getX(), d7, fieldVector3D3.getX(), d8, fieldVector3D4.getX());
        this.f8003y = (RealFieldElement) x.linearCombination(d5, fieldVector3D.getY(), d6, fieldVector3D2.getY(), d7, fieldVector3D3.getY(), d8, fieldVector3D4.getY());
        this.f8004z = (RealFieldElement) x.linearCombination(d5, fieldVector3D.getZ(), d6, fieldVector3D2.getZ(), d7, fieldVector3D3.getZ(), d8, fieldVector3D4.getZ());
    }

    public T getX() {
        return this.f8002x;
    }

    public T getY() {
        return this.f8003y;
    }

    public T getZ() {
        return this.f8004z;
    }

    public T[] toArray() {
        T[] tArr = (RealFieldElement[]) MathArrays.buildArray(this.f8002x.getField(), 3);
        tArr[0] = this.f8002x;
        tArr[1] = this.f8003y;
        tArr[2] = this.f8004z;
        return tArr;
    }

    public Vector3D toVector3D() {
        return new Vector3D(this.f8002x.getReal(), this.f8003y.getReal(), this.f8004z.getReal());
    }

    public T getNorm1() {
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) this.f8002x.abs()).add(this.f8003y.abs())).add(this.f8004z.abs());
    }

    public T getNorm() {
        T t = this.f8002x;
        T t2 = this.f8003y;
        T t3 = this.f8004z;
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) t.multiply(t)).add(t2.multiply(t2))).add(t3.multiply(t3))).sqrt();
    }

    public T getNormSq() {
        T t = this.f8002x;
        T t2 = this.f8003y;
        T t3 = this.f8004z;
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) t.multiply(t)).add(t2.multiply(t2))).add(t3.multiply(t3));
    }

    public T getNormInf() {
        T t = (RealFieldElement) this.f8002x.abs();
        T t2 = (RealFieldElement) this.f8003y.abs();
        T t3 = (RealFieldElement) this.f8004z.abs();
        return t.getReal() <= t2.getReal() ? t2.getReal() <= t3.getReal() ? t3 : t2 : t.getReal() <= t3.getReal() ? t3 : t;
    }

    public T getAlpha() {
        return (RealFieldElement) this.f8003y.atan2(this.f8002x);
    }

    public T getDelta() {
        return (RealFieldElement) ((RealFieldElement) this.f8004z.divide(getNorm())).asin();
    }

    public FieldVector3D<T> add(FieldVector3D<T> fieldVector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.add(fieldVector3D.f8002x), (RealFieldElement) this.f8003y.add(fieldVector3D.f8003y), (RealFieldElement) this.f8004z.add(fieldVector3D.f8004z));
    }

    public FieldVector3D<T> add(Vector3D vector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.add(vector3D.getX()), (RealFieldElement) this.f8003y.add(vector3D.getY()), (RealFieldElement) this.f8004z.add(vector3D.getZ()));
    }

    public FieldVector3D<T> add(T t, FieldVector3D<T> fieldVector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.getField().getOne(), this, t, fieldVector3D);
    }

    public FieldVector3D<T> add(T t, Vector3D vector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.add(t.multiply(vector3D.getX())), (RealFieldElement) this.f8003y.add(t.multiply(vector3D.getY())), (RealFieldElement) this.f8004z.add(t.multiply(vector3D.getZ())));
    }

    public FieldVector3D<T> add(double d, FieldVector3D<T> fieldVector3D) {
        return new FieldVector3D(1.0d, this, d, fieldVector3D);
    }

    public FieldVector3D<T> add(double d, Vector3D vector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.add(vector3D.getX() * d), (RealFieldElement) this.f8003y.add(vector3D.getY() * d), (RealFieldElement) this.f8004z.add(d * vector3D.getZ()));
    }

    public FieldVector3D<T> subtract(FieldVector3D<T> fieldVector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.subtract(fieldVector3D.f8002x), (RealFieldElement) this.f8003y.subtract(fieldVector3D.f8003y), (RealFieldElement) this.f8004z.subtract(fieldVector3D.f8004z));
    }

    public FieldVector3D<T> subtract(Vector3D vector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.subtract(vector3D.getX()), (RealFieldElement) this.f8003y.subtract(vector3D.getY()), (RealFieldElement) this.f8004z.subtract(vector3D.getZ()));
    }

    public FieldVector3D<T> subtract(T t, FieldVector3D<T> fieldVector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.getField().getOne(), this, (RealFieldElement) t.negate(), fieldVector3D);
    }

    public FieldVector3D<T> subtract(T t, Vector3D vector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.subtract(t.multiply(vector3D.getX())), (RealFieldElement) this.f8003y.subtract(t.multiply(vector3D.getY())), (RealFieldElement) this.f8004z.subtract(t.multiply(vector3D.getZ())));
    }

    public FieldVector3D<T> subtract(double d, FieldVector3D<T> fieldVector3D) {
        return new FieldVector3D(1.0d, this, -d, fieldVector3D);
    }

    public FieldVector3D<T> subtract(double d, Vector3D vector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.subtract(vector3D.getX() * d), (RealFieldElement) this.f8003y.subtract(vector3D.getY() * d), (RealFieldElement) this.f8004z.subtract(d * vector3D.getZ()));
    }

    public FieldVector3D<T> normalize() throws MathArithmeticException {
        RealFieldElement norm = getNorm();
        if (norm.getReal() != 0.0d) {
            return scalarMultiply((RealFieldElement) norm.reciprocal());
        }
        throw new MathArithmeticException(LocalizedFormats.CANNOT_NORMALIZE_A_ZERO_NORM_VECTOR, new Object[0]);
    }

    public FieldVector3D<T> orthogonal() throws MathArithmeticException {
        double real = getNorm().getReal() * 0.6d;
        if (real == 0.0d) {
            throw new MathArithmeticException(LocalizedFormats.ZERO_NORM, new Object[0]);
        } else if (FastMath.abs(this.f8002x.getReal()) <= real) {
            T t = this.f8003y;
            T t2 = this.f8004z;
            RealFieldElement realFieldElement = (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) t.multiply(t)).add(t2.multiply(t2))).sqrt()).reciprocal();
            return new FieldVector3D<>((RealFieldElement) realFieldElement.getField().getZero(), (RealFieldElement) realFieldElement.multiply(this.f8004z), (RealFieldElement) ((RealFieldElement) realFieldElement.multiply(this.f8003y)).negate());
        } else if (FastMath.abs(this.f8003y.getReal()) <= real) {
            T t3 = this.f8002x;
            T t4 = this.f8004z;
            RealFieldElement realFieldElement2 = (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) t3.multiply(t3)).add(t4.multiply(t4))).sqrt()).reciprocal();
            return new FieldVector3D<>((RealFieldElement) ((RealFieldElement) realFieldElement2.multiply(this.f8004z)).negate(), (RealFieldElement) realFieldElement2.getField().getZero(), (RealFieldElement) realFieldElement2.multiply(this.f8002x));
        } else {
            T t5 = this.f8002x;
            T t6 = this.f8003y;
            RealFieldElement realFieldElement3 = (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) t5.multiply(t5)).add(t6.multiply(t6))).sqrt()).reciprocal();
            return new FieldVector3D<>((RealFieldElement) realFieldElement3.multiply(this.f8003y), (RealFieldElement) ((RealFieldElement) realFieldElement3.multiply(this.f8002x)).negate(), (RealFieldElement) realFieldElement3.getField().getZero());
        }
    }

    public static <T extends RealFieldElement<T>> T angle(FieldVector3D<T> fieldVector3D, FieldVector3D<T> fieldVector3D2) throws MathArithmeticException {
        RealFieldElement realFieldElement = (RealFieldElement) fieldVector3D.getNorm().multiply(fieldVector3D2.getNorm());
        if (realFieldElement.getReal() != 0.0d) {
            T dotProduct = dotProduct(fieldVector3D, fieldVector3D2);
            double real = realFieldElement.getReal() * 0.9999d;
            if (dotProduct.getReal() >= (-real) && dotProduct.getReal() <= real) {
                return (RealFieldElement) ((RealFieldElement) dotProduct.divide(realFieldElement)).acos();
            }
            FieldVector3D<T> crossProduct = crossProduct(fieldVector3D, fieldVector3D2);
            if (dotProduct.getReal() >= 0.0d) {
                return (RealFieldElement) ((RealFieldElement) crossProduct.getNorm().divide(realFieldElement)).asin();
            }
            return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) crossProduct.getNorm().divide(realFieldElement)).asin()).subtract(3.141592653589793d)).negate();
        }
        throw new MathArithmeticException(LocalizedFormats.ZERO_NORM, new Object[0]);
    }

    public static <T extends RealFieldElement<T>> T angle(FieldVector3D<T> fieldVector3D, Vector3D vector3D) throws MathArithmeticException {
        RealFieldElement realFieldElement = (RealFieldElement) fieldVector3D.getNorm().multiply(vector3D.getNorm());
        if (realFieldElement.getReal() != 0.0d) {
            T dotProduct = dotProduct(fieldVector3D, vector3D);
            double real = realFieldElement.getReal() * 0.9999d;
            if (dotProduct.getReal() >= (-real) && dotProduct.getReal() <= real) {
                return (RealFieldElement) ((RealFieldElement) dotProduct.divide(realFieldElement)).acos();
            }
            FieldVector3D<T> crossProduct = crossProduct(fieldVector3D, vector3D);
            if (dotProduct.getReal() >= 0.0d) {
                return (RealFieldElement) ((RealFieldElement) crossProduct.getNorm().divide(realFieldElement)).asin();
            }
            return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) crossProduct.getNorm().divide(realFieldElement)).asin()).subtract(3.141592653589793d)).negate();
        }
        throw new MathArithmeticException(LocalizedFormats.ZERO_NORM, new Object[0]);
    }

    public static <T extends RealFieldElement<T>> T angle(Vector3D vector3D, FieldVector3D<T> fieldVector3D) throws MathArithmeticException {
        return angle(fieldVector3D, vector3D);
    }

    public FieldVector3D<T> negate() {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.negate(), (RealFieldElement) this.f8003y.negate(), (RealFieldElement) this.f8004z.negate());
    }

    public FieldVector3D<T> scalarMultiply(T t) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.multiply(t), (RealFieldElement) this.f8003y.multiply(t), (RealFieldElement) this.f8004z.multiply(t));
    }

    public FieldVector3D<T> scalarMultiply(double d) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.multiply(d), (RealFieldElement) this.f8003y.multiply(d), (RealFieldElement) this.f8004z.multiply(d));
    }

    public boolean isNaN() {
        return Double.isNaN(this.f8002x.getReal()) || Double.isNaN(this.f8003y.getReal()) || Double.isNaN(this.f8004z.getReal());
    }

    public boolean isInfinite() {
        return !isNaN() && (Double.isInfinite(this.f8002x.getReal()) || Double.isInfinite(this.f8003y.getReal()) || Double.isInfinite(this.f8004z.getReal()));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FieldVector3D)) {
            return false;
        }
        FieldVector3D fieldVector3D = (FieldVector3D) obj;
        if (fieldVector3D.isNaN()) {
            return isNaN();
        }
        if (!this.f8002x.equals(fieldVector3D.f8002x) || !this.f8003y.equals(fieldVector3D.f8003y) || !this.f8004z.equals(fieldVector3D.f8004z)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (isNaN()) {
            return HttpStatusCodes.STATUS_CODE_CONFLICT;
        }
        return ((this.f8002x.hashCode() * 107) + (this.f8003y.hashCode() * 83) + this.f8004z.hashCode()) * 311;
    }

    public T dotProduct(FieldVector3D<T> fieldVector3D) {
        T t = this.f8002x;
        return (RealFieldElement) t.linearCombination(t, fieldVector3D.f8002x, this.f8003y, fieldVector3D.f8003y, this.f8004z, fieldVector3D.f8004z);
    }

    public T dotProduct(Vector3D vector3D) {
        return (RealFieldElement) this.f8002x.linearCombination(vector3D.getX(), this.f8002x, vector3D.getY(), this.f8003y, vector3D.getZ(), this.f8004z);
    }

    public FieldVector3D<T> crossProduct(FieldVector3D<T> fieldVector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.linearCombination(this.f8003y, fieldVector3D.f8004z, this.f8004z.negate(), fieldVector3D.f8003y), (RealFieldElement) this.f8003y.linearCombination(this.f8004z, fieldVector3D.f8002x, this.f8002x.negate(), fieldVector3D.f8004z), (RealFieldElement) this.f8004z.linearCombination(this.f8002x, fieldVector3D.f8003y, this.f8003y.negate(), fieldVector3D.f8002x));
    }

    public FieldVector3D<T> crossProduct(Vector3D vector3D) {
        return new FieldVector3D<>((RealFieldElement) this.f8002x.linearCombination(vector3D.getZ(), this.f8003y, -vector3D.getY(), this.f8004z), (RealFieldElement) this.f8003y.linearCombination(vector3D.getX(), this.f8004z, -vector3D.getZ(), this.f8002x), (RealFieldElement) this.f8004z.linearCombination(vector3D.getY(), this.f8002x, -vector3D.getX(), this.f8003y));
    }

    public T distance1(FieldVector3D<T> fieldVector3D) {
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) fieldVector3D.f8002x.subtract(this.f8002x)).abs()).add((RealFieldElement) ((RealFieldElement) fieldVector3D.f8003y.subtract(this.f8003y)).abs())).add((RealFieldElement) ((RealFieldElement) fieldVector3D.f8004z.subtract(this.f8004z)).abs());
    }

    public T distance1(Vector3D vector3D) {
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) this.f8002x.subtract(vector3D.getX())).abs()).add((RealFieldElement) ((RealFieldElement) this.f8003y.subtract(vector3D.getY())).abs())).add((RealFieldElement) ((RealFieldElement) this.f8004z.subtract(vector3D.getZ())).abs());
    }

    public T distance(FieldVector3D<T> fieldVector3D) {
        RealFieldElement realFieldElement = (RealFieldElement) fieldVector3D.f8002x.subtract(this.f8002x);
        RealFieldElement realFieldElement2 = (RealFieldElement) fieldVector3D.f8003y.subtract(this.f8003y);
        RealFieldElement realFieldElement3 = (RealFieldElement) fieldVector3D.f8004z.subtract(this.f8004z);
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) realFieldElement.multiply(realFieldElement)).add(realFieldElement2.multiply(realFieldElement2))).add(realFieldElement3.multiply(realFieldElement3))).sqrt();
    }

    public T distance(Vector3D vector3D) {
        RealFieldElement realFieldElement = (RealFieldElement) this.f8002x.subtract(vector3D.getX());
        RealFieldElement realFieldElement2 = (RealFieldElement) this.f8003y.subtract(vector3D.getY());
        RealFieldElement realFieldElement3 = (RealFieldElement) this.f8004z.subtract(vector3D.getZ());
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) ((RealFieldElement) realFieldElement.multiply(realFieldElement)).add(realFieldElement2.multiply(realFieldElement2))).add(realFieldElement3.multiply(realFieldElement3))).sqrt();
    }

    public T distanceInf(FieldVector3D<T> fieldVector3D) {
        T t = (RealFieldElement) ((RealFieldElement) fieldVector3D.f8002x.subtract(this.f8002x)).abs();
        T t2 = (RealFieldElement) ((RealFieldElement) fieldVector3D.f8003y.subtract(this.f8003y)).abs();
        T t3 = (RealFieldElement) ((RealFieldElement) fieldVector3D.f8004z.subtract(this.f8004z)).abs();
        return t.getReal() <= t2.getReal() ? t2.getReal() <= t3.getReal() ? t3 : t2 : t.getReal() <= t3.getReal() ? t3 : t;
    }

    public T distanceInf(Vector3D vector3D) {
        T t = (RealFieldElement) ((RealFieldElement) this.f8002x.subtract(vector3D.getX())).abs();
        T t2 = (RealFieldElement) ((RealFieldElement) this.f8003y.subtract(vector3D.getY())).abs();
        T t3 = (RealFieldElement) ((RealFieldElement) this.f8004z.subtract(vector3D.getZ())).abs();
        return t.getReal() <= t2.getReal() ? t2.getReal() <= t3.getReal() ? t3 : t2 : t.getReal() <= t3.getReal() ? t3 : t;
    }

    public T distanceSq(FieldVector3D<T> fieldVector3D) {
        RealFieldElement realFieldElement = (RealFieldElement) fieldVector3D.f8002x.subtract(this.f8002x);
        RealFieldElement realFieldElement2 = (RealFieldElement) fieldVector3D.f8003y.subtract(this.f8003y);
        RealFieldElement realFieldElement3 = (RealFieldElement) fieldVector3D.f8004z.subtract(this.f8004z);
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) realFieldElement.multiply(realFieldElement)).add(realFieldElement2.multiply(realFieldElement2))).add(realFieldElement3.multiply(realFieldElement3));
    }

    public T distanceSq(Vector3D vector3D) {
        RealFieldElement realFieldElement = (RealFieldElement) this.f8002x.subtract(vector3D.getX());
        RealFieldElement realFieldElement2 = (RealFieldElement) this.f8003y.subtract(vector3D.getY());
        RealFieldElement realFieldElement3 = (RealFieldElement) this.f8004z.subtract(vector3D.getZ());
        return (RealFieldElement) ((RealFieldElement) ((RealFieldElement) realFieldElement.multiply(realFieldElement)).add(realFieldElement2.multiply(realFieldElement2))).add(realFieldElement3.multiply(realFieldElement3));
    }

    public static <T extends RealFieldElement<T>> T dotProduct(FieldVector3D fieldVector3D, FieldVector3D fieldVector3D2) {
        return fieldVector3D.dotProduct(fieldVector3D2);
    }

    public static <T extends RealFieldElement<T>> T dotProduct(FieldVector3D fieldVector3D, Vector3D vector3D) {
        return fieldVector3D.dotProduct(vector3D);
    }

    public static <T extends RealFieldElement<T>> T dotProduct(Vector3D vector3D, FieldVector3D fieldVector3D) {
        return fieldVector3D.dotProduct(vector3D);
    }

    public static <T extends RealFieldElement<T>> FieldVector3D<T> crossProduct(FieldVector3D fieldVector3D, FieldVector3D fieldVector3D2) {
        return fieldVector3D.crossProduct(fieldVector3D2);
    }

    public static <T extends RealFieldElement<T>> FieldVector3D<T> crossProduct(FieldVector3D fieldVector3D, Vector3D vector3D) {
        return fieldVector3D.crossProduct(vector3D);
    }

    public static <T extends RealFieldElement<T>> FieldVector3D<T> crossProduct(Vector3D vector3D, FieldVector3D fieldVector3D) {
        return new FieldVector3D<>((RealFieldElement) fieldVector3D.f8002x.linearCombination(vector3D.getY(), fieldVector3D.f8004z, -vector3D.getZ(), fieldVector3D.f8003y), (RealFieldElement) fieldVector3D.f8003y.linearCombination(vector3D.getZ(), fieldVector3D.f8002x, -vector3D.getX(), fieldVector3D.f8004z), (RealFieldElement) fieldVector3D.f8004z.linearCombination(vector3D.getX(), fieldVector3D.f8003y, -vector3D.getY(), fieldVector3D.f8002x));
    }

    public static <T extends RealFieldElement<T>> T distance1(FieldVector3D<T> fieldVector3D, FieldVector3D<T> fieldVector3D2) {
        return fieldVector3D.distance1(fieldVector3D2);
    }

    public static <T extends RealFieldElement<T>> T distance1(FieldVector3D<T> fieldVector3D, Vector3D vector3D) {
        return fieldVector3D.distance1(vector3D);
    }

    public static <T extends RealFieldElement<T>> T distance1(Vector3D vector3D, FieldVector3D<T> fieldVector3D) {
        return fieldVector3D.distance1(vector3D);
    }

    public static <T extends RealFieldElement<T>> T distance(FieldVector3D<T> fieldVector3D, FieldVector3D<T> fieldVector3D2) {
        return fieldVector3D.distance(fieldVector3D2);
    }

    public static <T extends RealFieldElement<T>> T distance(FieldVector3D<T> fieldVector3D, Vector3D vector3D) {
        return fieldVector3D.distance(vector3D);
    }

    public static <T extends RealFieldElement<T>> T distance(Vector3D vector3D, FieldVector3D<T> fieldVector3D) {
        return fieldVector3D.distance(vector3D);
    }

    public static <T extends RealFieldElement<T>> T distanceInf(FieldVector3D<T> fieldVector3D, FieldVector3D<T> fieldVector3D2) {
        return fieldVector3D.distanceInf(fieldVector3D2);
    }

    public static <T extends RealFieldElement<T>> T distanceInf(FieldVector3D<T> fieldVector3D, Vector3D vector3D) {
        return fieldVector3D.distanceInf(vector3D);
    }

    public static <T extends RealFieldElement<T>> T distanceInf(Vector3D vector3D, FieldVector3D<T> fieldVector3D) {
        return fieldVector3D.distanceInf(vector3D);
    }

    public static <T extends RealFieldElement<T>> T distanceSq(FieldVector3D<T> fieldVector3D, FieldVector3D<T> fieldVector3D2) {
        return fieldVector3D.distanceSq(fieldVector3D2);
    }

    public static <T extends RealFieldElement<T>> T distanceSq(FieldVector3D<T> fieldVector3D, Vector3D vector3D) {
        return fieldVector3D.distanceSq(vector3D);
    }

    public static <T extends RealFieldElement<T>> T distanceSq(Vector3D vector3D, FieldVector3D<T> fieldVector3D) {
        return fieldVector3D.distanceSq(vector3D);
    }

    public String toString() {
        return Vector3DFormat.getInstance().format(toVector3D());
    }

    public String toString(NumberFormat numberFormat) {
        return new Vector3DFormat(numberFormat).format(toVector3D());
    }
}
