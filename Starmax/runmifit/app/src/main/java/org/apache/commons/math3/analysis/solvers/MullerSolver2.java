package org.apache.commons.math3.analysis.solvers;

import org.apache.commons.math3.exception.NoBracketingException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.util.FastMath;

public class MullerSolver2 extends AbstractUnivariateSolver {
    private static final double DEFAULT_ABSOLUTE_ACCURACY = 1.0E-6d;

    public MullerSolver2() {
        this(1.0E-6d);
    }

    public MullerSolver2(double d) {
        super(d);
    }

    public MullerSolver2(double d, double d2) {
        super(d, d2);
    }

    /* access modifiers changed from: protected */
    public double doSolve() throws TooManyEvaluationsException, NumberIsTooLargeException, NoBracketingException {
        double d;
        double d2;
        double d3;
        MullerSolver2 mullerSolver2 = this;
        double min = getMin();
        double max = getMax();
        mullerSolver2.verifyInterval(min, max);
        double relativeAccuracy = getRelativeAccuracy();
        double absoluteAccuracy = getAbsoluteAccuracy();
        double functionValueAccuracy = getFunctionValueAccuracy();
        double computeObjectiveValue = mullerSolver2.computeObjectiveValue(min);
        if (FastMath.abs(computeObjectiveValue) < functionValueAccuracy) {
            return min;
        }
        double computeObjectiveValue2 = mullerSolver2.computeObjectiveValue(max);
        if (FastMath.abs(computeObjectiveValue2) < functionValueAccuracy) {
            return max;
        }
        if (computeObjectiveValue * computeObjectiveValue2 <= 0.0d) {
            double d4 = computeObjectiveValue2;
            double d5 = (min + max) * 0.5d;
            double d6 = computeObjectiveValue;
            double d7 = Double.POSITIVE_INFINITY;
            double d8 = d4;
            double d9 = max;
            double computeObjectiveValue3 = mullerSolver2.computeObjectiveValue(d5);
            double d10 = min;
            while (true) {
                double d11 = d5 - d9;
                double d12 = d11 / (d9 - d10);
                double d13 = d12 + 1.0d;
                double d14 = ((computeObjectiveValue3 - (d13 * d8)) + (d12 * d6)) * d12;
                double d15 = ((((d12 * 2.0d) + 1.0d) * computeObjectiveValue3) - ((d13 * d13) * d8)) + (d12 * d12 * d6);
                double d16 = d13 * computeObjectiveValue3;
                double d17 = d15 * d15;
                double d18 = d17 - ((d14 * 4.0d) * d16);
                if (d18 >= 0.0d) {
                    d = d15 + FastMath.sqrt(d18);
                    double sqrt = d15 - FastMath.sqrt(d18);
                    if (FastMath.abs(d) <= FastMath.abs(sqrt)) {
                        d = sqrt;
                    }
                } else {
                    d = FastMath.sqrt(d17 - d18);
                }
                if (d != 0.0d) {
                    double d19 = d5 - (((d16 * 2.0d) * d11) / d);
                    while (true) {
                        if (d19 != d9 && d19 != d5) {
                            break;
                        }
                        d19 += absoluteAccuracy;
                    }
                    d2 = d9;
                    d3 = d19;
                } else {
                    d2 = d9;
                    d3 = min + (FastMath.random() * (max - min));
                    d7 = Double.POSITIVE_INFINITY;
                }
                double computeObjectiveValue4 = mullerSolver2.computeObjectiveValue(d3);
                if (FastMath.abs(d3 - d7) <= FastMath.max(relativeAccuracy * FastMath.abs(d3), absoluteAccuracy) || FastMath.abs(computeObjectiveValue4) <= functionValueAccuracy) {
                    return d3;
                }
                mullerSolver2 = this;
                d7 = d3;
                d9 = d5;
                d5 = d7;
                double d20 = computeObjectiveValue4;
                d10 = d2;
                d6 = d8;
                d8 = computeObjectiveValue3;
                computeObjectiveValue3 = d20;
            }
            return d3;
        }
        throw new NoBracketingException(min, max, computeObjectiveValue, computeObjectiveValue2);
    }
}
