package org.apache.commons.math3.stat.descriptive.moment;

import java.io.Serializable;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.util.MathUtils;

class FourthMoment extends ThirdMoment implements Serializable {
    private static final long serialVersionUID = 4763990447117157611L;

    /* renamed from: m4 */
    private double f8176m4;

    public FourthMoment() {
        this.f8176m4 = Double.NaN;
    }

    public FourthMoment(FourthMoment fourthMoment) throws NullArgumentException {
        copy(fourthMoment, this);
    }

    public void increment(double d) {
        if (this.f8175n < 1) {
            this.f8176m4 = 0.0d;
            this.f8178m3 = 0.0d;
            this.f8177m2 = 0.0d;
            this.f8174m1 = 0.0d;
        }
        double d2 = this.f8178m3;
        double d3 = this.f8177m2;
        super.increment(d);
        double d4 = (double) this.f8175n;
        double d5 = (this.f8176m4 - ((this.nDev * 4.0d) * d2)) + (this.nDevSq * 6.0d * d3);
        Double.isNaN(d4);
        Double.isNaN(d4);
        Double.isNaN(d4);
        double d6 = d4 - 1.0d;
        Double.isNaN(d4);
        this.f8176m4 = d5 + (((d4 * d4) - (3.0d * d6)) * this.nDevSq * this.nDevSq * d6 * d4);
    }

    public double getResult() {
        return this.f8176m4;
    }

    public void clear() {
        super.clear();
        this.f8176m4 = Double.NaN;
    }

    public FourthMoment copy() {
        FourthMoment fourthMoment = new FourthMoment();
        copy(this, fourthMoment);
        return fourthMoment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.math3.stat.descriptive.moment.ThirdMoment.copy(org.apache.commons.math3.stat.descriptive.moment.ThirdMoment, org.apache.commons.math3.stat.descriptive.moment.ThirdMoment):void
     arg types: [org.apache.commons.math3.stat.descriptive.moment.FourthMoment, org.apache.commons.math3.stat.descriptive.moment.FourthMoment]
     candidates:
      org.apache.commons.math3.stat.descriptive.moment.SecondMoment.copy(org.apache.commons.math3.stat.descriptive.moment.SecondMoment, org.apache.commons.math3.stat.descriptive.moment.SecondMoment):void
      org.apache.commons.math3.stat.descriptive.moment.FirstMoment.copy(org.apache.commons.math3.stat.descriptive.moment.FirstMoment, org.apache.commons.math3.stat.descriptive.moment.FirstMoment):void
      org.apache.commons.math3.stat.descriptive.moment.ThirdMoment.copy(org.apache.commons.math3.stat.descriptive.moment.ThirdMoment, org.apache.commons.math3.stat.descriptive.moment.ThirdMoment):void */
    public static void copy(FourthMoment fourthMoment, FourthMoment fourthMoment2) throws NullArgumentException {
        MathUtils.checkNotNull(fourthMoment);
        MathUtils.checkNotNull(fourthMoment2);
        ThirdMoment.copy((ThirdMoment) super, (ThirdMoment) super);
        fourthMoment2.f8176m4 = fourthMoment.f8176m4;
    }
}
