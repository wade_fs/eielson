package org.greenrobot.greendao.identityscope;

public interface IdentityScope<K, T> {
    void clear();

    boolean detach(K k, T t);

    T get(K k);

    T getNoLock(K k);

    void lock();

    void put(K k, T t);

    void putNoLock(K k, T t);

    void remove(Iterable iterable);

    void remove(Object obj);

    void reserveRoom(int i);

    void unlock();
}
