package org.greenrobot.greendao.generator;

import java.io.PrintStream;

public class ToOne {
    private final Property[] fkProperties;
    private String name;
    private final String[] resolvedKeyJavaType;
    private final boolean[] resolvedKeyUseEquals;
    private final Schema schema;
    private final Entity sourceEntity;
    private final Entity targetEntity;
    private final boolean useFkProperty;

    public ToOne(Schema schema2, Entity entity, Entity entity2, Property[] propertyArr, boolean z) {
        this.schema = schema2;
        this.sourceEntity = entity;
        this.targetEntity = entity2;
        this.fkProperties = propertyArr;
        this.useFkProperty = z;
        this.resolvedKeyJavaType = new String[propertyArr.length];
        this.resolvedKeyUseEquals = new boolean[propertyArr.length];
    }

    public Entity getSourceEntity() {
        return this.sourceEntity;
    }

    public Entity getTargetEntity() {
        return this.targetEntity;
    }

    public Property[] getFkProperties() {
        return this.fkProperties;
    }

    public String[] getResolvedKeyJavaType() {
        return this.resolvedKeyJavaType;
    }

    public boolean[] getResolvedKeyUseEquals() {
        return this.resolvedKeyUseEquals;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public boolean isUseFkProperty() {
        return this.useFkProperty;
    }

    /* access modifiers changed from: package-private */
    public void init2ndPass() {
        if (this.name == null) {
            char[] charArray = this.targetEntity.getClassName().toCharArray();
            charArray[0] = Character.toLowerCase(charArray[0]);
            this.name = new String(charArray);
        }
    }

    /* access modifiers changed from: package-private */
    public void init3ndPass() {
        Property pkProperty = this.targetEntity.getPkProperty();
        Property[] propertyArr = this.fkProperties;
        if (propertyArr.length != 1 || pkProperty == null) {
            throw new RuntimeException("Currently only single FK columns are supported: " + this);
        }
        Property property = propertyArr[0];
        PropertyType propertyType = property.getPropertyType();
        if (propertyType == null) {
            propertyType = pkProperty.getPropertyType();
            property.setPropertyType(propertyType);
            property.init2ndPass();
            property.init3ndPass();
        } else if (propertyType != pkProperty.getPropertyType()) {
            PrintStream printStream = System.err;
            printStream.println("Warning to-one property type does not match target key type: " + this);
        }
        this.resolvedKeyJavaType[0] = this.schema.mapToJavaTypeNullable(propertyType);
        this.resolvedKeyUseEquals[0] = checkUseEquals(propertyType);
    }

    /* access modifiers changed from: protected */
    public boolean checkUseEquals(PropertyType propertyType) {
        switch (propertyType) {
            case Byte:
            case Short:
            case Int:
            case Long:
            case Boolean:
            case Float:
                return true;
            default:
                return false;
        }
    }

    public String toString() {
        Entity entity = this.sourceEntity;
        String str = null;
        String className = entity != null ? entity.getClassName() : null;
        Entity entity2 = this.targetEntity;
        if (entity2 != null) {
            str = entity2.getClassName();
        }
        return "ToOne '" + this.name + "' from " + className + " to " + str;
    }
}
