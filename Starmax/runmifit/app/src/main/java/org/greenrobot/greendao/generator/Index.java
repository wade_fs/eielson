package org.greenrobot.greendao.generator;

public class Index extends PropertyOrderList {
    private String name;
    private boolean nonDefaultName;
    private boolean unique;

    public String getName() {
        return this.name;
    }

    public Index setName(String str) {
        this.name = str;
        this.nonDefaultName = str != null;
        return this;
    }

    public Index makeUnique() {
        this.unique = true;
        return this;
    }

    public boolean isUnique() {
        return this.unique;
    }

    public boolean isNonDefaultName() {
        return this.nonDefaultName;
    }

    /* access modifiers changed from: package-private */
    public void setDefaultName(String str) {
        this.name = str;
        this.nonDefaultName = false;
    }
}
