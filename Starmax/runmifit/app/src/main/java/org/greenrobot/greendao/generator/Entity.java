package org.greenrobot.greendao.generator;

import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.greenrobot.greendao.generator.Property;

public class Entity {
    private Boolean active;
    private final Collection<String> additionalImportsDao = new TreeSet();
    private final Collection<String> additionalImportsEntity = new TreeSet();
    private final String className;
    private String classNameDao;
    private String classNameTest;
    private String codeBeforeClass;
    private boolean constructors = true;
    private final List<ContentProvider> contentProviders = new ArrayList();
    private String dbName;
    private Boolean hasKeepSections;
    private final List<ToManyBase> incomingToManyRelations = new ArrayList();
    private final List<Index> indexes = new ArrayList();
    private final List<String> interfacesToImplement = new ArrayList();
    private String javaDoc;
    private String javaPackage;
    private String javaPackageDao;
    private String javaPackageTest;
    private final List<Index> multiIndexes = new ArrayList();
    private boolean nonDefaultDbName;
    private Property pkProperty;
    private String pkType;
    private final List<Property> properties = new ArrayList();
    private List<Property> propertiesColumns;
    private final List<Property> propertiesNonPk = new ArrayList();
    private final List<Property> propertiesPk = new ArrayList();
    private final Set<String> propertyNames = new HashSet();
    private boolean protobuf;
    private final Schema schema;
    private boolean skipCreationInDb;
    private boolean skipGeneration;
    private boolean skipGenerationTest;
    private String superclass;
    private final List<ToManyBase> toManyRelations = new ArrayList();
    private final List<ToOne> toOneRelations = new ArrayList();

    Entity(Schema schema2, String str) {
        this.schema = schema2;
        this.className = str;
    }

    public Property.PropertyBuilder addBooleanProperty(String str) {
        return addProperty(PropertyType.Boolean, str);
    }

    public Property.PropertyBuilder addByteProperty(String str) {
        return addProperty(PropertyType.Byte, str);
    }

    public Property.PropertyBuilder addShortProperty(String str) {
        return addProperty(PropertyType.Short, str);
    }

    public Property.PropertyBuilder addIntProperty(String str) {
        return addProperty(PropertyType.Int, str);
    }

    public Property.PropertyBuilder addLongProperty(String str) {
        return addProperty(PropertyType.Long, str);
    }

    public Property.PropertyBuilder addFloatProperty(String str) {
        return addProperty(PropertyType.Float, str);
    }

    public Property.PropertyBuilder addDoubleProperty(String str) {
        return addProperty(PropertyType.Double, str);
    }

    public Property.PropertyBuilder addByteArrayProperty(String str) {
        return addProperty(PropertyType.ByteArray, str);
    }

    public Property.PropertyBuilder addStringProperty(String str) {
        return addProperty(PropertyType.String, str);
    }

    public Property.PropertyBuilder addDateProperty(String str) {
        return addProperty(PropertyType.Date, str);
    }

    public Property.PropertyBuilder addProperty(PropertyType propertyType, String str) {
        if (this.propertyNames.add(str)) {
            Property.PropertyBuilder propertyBuilder = new Property.PropertyBuilder(this.schema, this, propertyType, str);
            this.properties.add(propertyBuilder.getProperty());
            return propertyBuilder;
        }
        throw new RuntimeException("Property already defined: " + str);
    }

    public Property.PropertyBuilder addIdProperty() {
        Property.PropertyBuilder addLongProperty = addLongProperty(Config.FEED_LIST_ITEM_CUSTOM_ID);
        addLongProperty.dbName("_id").primaryKey();
        return addLongProperty;
    }

    public ToMany addToMany(Entity entity, Property property) {
        return addToMany((Property[]) null, entity, new Property[]{property});
    }

    public ToMany addToMany(Entity entity, Property property, String str) {
        ToMany addToMany = addToMany(entity, property);
        addToMany.setName(str);
        return addToMany;
    }

    public ToMany addToMany(Property property, Entity entity, Property property2) {
        return addToMany(new Property[]{property}, entity, new Property[]{property2});
    }

    public ToMany addToMany(Property[] propertyArr, Entity entity, Property[] propertyArr2) {
        if (!this.protobuf) {
            ToMany toMany = new ToMany(this.schema, this, propertyArr, entity, propertyArr2);
            this.toManyRelations.add(toMany);
            entity.incomingToManyRelations.add(toMany);
            return toMany;
        }
        throw new IllegalStateException("Protobuf entities do not support relations, currently");
    }

    public ToManyWithJoinEntity addToMany(Entity entity, Entity entity2, Property property, Property property2) {
        ToManyWithJoinEntity toManyWithJoinEntity = new ToManyWithJoinEntity(this.schema, this, entity, entity2, property, property2);
        this.toManyRelations.add(toManyWithJoinEntity);
        entity.incomingToManyRelations.add(toManyWithJoinEntity);
        return toManyWithJoinEntity;
    }

    public ToOne addToOne(Entity entity, Property property) {
        if (!this.protobuf) {
            ToOne toOne = new ToOne(this.schema, this, entity, new Property[]{property}, true);
            this.toOneRelations.add(toOne);
            return toOne;
        }
        throw new IllegalStateException("Protobuf entities do not support realtions, currently");
    }

    public ToOne addToOne(Entity entity, Property property, String str) {
        ToOne addToOne = addToOne(entity, property);
        addToOne.setName(str);
        return addToOne;
    }

    public ToOne addToOneWithoutProperty(String str, Entity entity, String str2) {
        return addToOneWithoutProperty(str, entity, str2, false, false);
    }

    public ToOne addToOneWithoutProperty(String str, Entity entity, String str2, boolean z, boolean z2) {
        Property.PropertyBuilder propertyBuilder = new Property.PropertyBuilder(this.schema, this, null, str);
        if (z) {
            propertyBuilder.notNull();
        }
        if (z2) {
            propertyBuilder.unique();
        }
        propertyBuilder.dbName(str2);
        ToOne toOne = new ToOne(this.schema, this, entity, new Property[]{propertyBuilder.getProperty()}, false);
        toOne.setName(str);
        this.toOneRelations.add(toOne);
        return toOne;
    }

    /* access modifiers changed from: protected */
    public void addIncomingToMany(ToMany toMany) {
        this.incomingToManyRelations.add(toMany);
    }

    public ContentProvider addContentProvider() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(this);
        ContentProvider contentProvider = new ContentProvider(this.schema, arrayList);
        this.contentProviders.add(contentProvider);
        return contentProvider;
    }

    public Entity addIndex(Index index) {
        this.indexes.add(index);
        return this;
    }

    public Entity addImport(String str) {
        this.additionalImportsEntity.add(str);
        return this;
    }

    /* access modifiers changed from: package-private */
    public Entity useProtobuf() {
        this.protobuf = true;
        return this;
    }

    public boolean isProtobuf() {
        return this.protobuf;
    }

    public Schema getSchema() {
        return this.schema;
    }

    public String getDbName() {
        return this.dbName;
    }

    @Deprecated
    public void setTableName(String str) {
        setDbName(str);
    }

    public void setDbName(String str) {
        this.dbName = str;
        this.nonDefaultDbName = str != null;
    }

    public String getClassName() {
        return this.className;
    }

    public List<Property> getProperties() {
        return this.properties;
    }

    public List<Property> getPropertiesColumns() {
        return this.propertiesColumns;
    }

    public String getJavaPackage() {
        return this.javaPackage;
    }

    public void setJavaPackage(String str) {
        this.javaPackage = str;
    }

    public String getJavaPackageDao() {
        return this.javaPackageDao;
    }

    public void setJavaPackageDao(String str) {
        this.javaPackageDao = str;
    }

    public String getClassNameDao() {
        return this.classNameDao;
    }

    public void setClassNameDao(String str) {
        this.classNameDao = str;
    }

    public String getClassNameTest() {
        return this.classNameTest;
    }

    public void setClassNameTest(String str) {
        this.classNameTest = str;
    }

    public String getJavaPackageTest() {
        return this.javaPackageTest;
    }

    public void setJavaPackageTest(String str) {
        this.javaPackageTest = str;
    }

    public List<Property> getPropertiesPk() {
        return this.propertiesPk;
    }

    public List<Property> getPropertiesNonPk() {
        return this.propertiesNonPk;
    }

    public Property getPkProperty() {
        return this.pkProperty;
    }

    public List<Index> getIndexes() {
        return this.indexes;
    }

    public String getPkType() {
        return this.pkType;
    }

    public boolean isConstructors() {
        return this.constructors;
    }

    public void setConstructors(boolean z) {
        this.constructors = z;
    }

    public boolean isSkipGeneration() {
        return this.skipGeneration;
    }

    public void setSkipGeneration(boolean z) {
        this.skipGeneration = z;
    }

    @Deprecated
    public void setSkipTableCreation(boolean z) {
        setSkipCreationInDb(z);
    }

    public void setSkipCreationInDb(boolean z) {
        this.skipCreationInDb = z;
    }

    public boolean isSkipCreationInDb() {
        return this.skipCreationInDb;
    }

    public boolean isSkipGenerationTest() {
        return this.skipGenerationTest;
    }

    public void setSkipGenerationTest(boolean z) {
        this.skipGenerationTest = z;
    }

    public List<ToOne> getToOneRelations() {
        return this.toOneRelations;
    }

    public List<ToManyBase> getToManyRelations() {
        return this.toManyRelations;
    }

    public List<ToManyBase> getIncomingToManyRelations() {
        return this.incomingToManyRelations;
    }

    public void setActive(Boolean bool) {
        this.active = bool;
    }

    public Boolean getActive() {
        return this.active;
    }

    public Boolean getHasKeepSections() {
        return this.hasKeepSections;
    }

    public Collection<String> getAdditionalImportsEntity() {
        return this.additionalImportsEntity;
    }

    public Collection<String> getAdditionalImportsDao() {
        return this.additionalImportsDao;
    }

    public void setHasKeepSections(Boolean bool) {
        this.hasKeepSections = bool;
    }

    public List<String> getInterfacesToImplement() {
        return this.interfacesToImplement;
    }

    public List<ContentProvider> getContentProviders() {
        return this.contentProviders;
    }

    public void implementsInterface(String... strArr) {
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str = strArr[i];
            if (!this.interfacesToImplement.contains(str)) {
                this.interfacesToImplement.add(str);
                i++;
            } else {
                throw new RuntimeException("Interface defined more than once: " + str);
            }
        }
    }

    public void implementsSerializable() {
        this.interfacesToImplement.add("java.io.Serializable");
    }

    public String getSuperclass() {
        return this.superclass;
    }

    public void setSuperclass(String str) {
        this.superclass = str;
    }

    public String getJavaDoc() {
        return this.javaDoc;
    }

    public void setJavaDoc(String str) {
        this.javaDoc = DaoUtil.checkConvertToJavaDoc(str, "");
    }

    public String getCodeBeforeClass() {
        return this.codeBeforeClass;
    }

    public void setCodeBeforeClass(String str) {
        this.codeBeforeClass = str;
    }

    /* access modifiers changed from: package-private */
    public void init2ndPass() {
        init2ndPassNamesWithDefaults();
        boolean z = false;
        for (int i = 0; i < this.properties.size(); i++) {
            Property property = this.properties.get(i);
            property.setOrdinal(i);
            property.init2ndPass();
            if (property.isPrimaryKey()) {
                this.propertiesPk.add(property);
            } else {
                this.propertiesNonPk.add(property);
            }
        }
        for (int i2 = 0; i2 < this.indexes.size(); i2++) {
            Index index = this.indexes.get(i2);
            int size = index.getProperties().size();
            if (size == 1) {
                index.getProperties().get(0).setIndex(index);
            } else if (size > 1) {
                this.multiIndexes.add(index);
            }
        }
        if (this.propertiesPk.size() == 1) {
            this.pkProperty = this.propertiesPk.get(0);
            this.pkType = this.schema.mapToJavaTypeNullable(this.pkProperty.getPropertyType());
        } else {
            this.pkType = "Void";
        }
        this.propertiesColumns = new ArrayList(this.properties);
        for (ToOne toOne : this.toOneRelations) {
            toOne.init2ndPass();
            Property[] fkProperties = toOne.getFkProperties();
            for (Property property2 : fkProperties) {
                if (!this.propertiesColumns.contains(property2)) {
                    this.propertiesColumns.add(property2);
                }
            }
        }
        for (ToManyBase toManyBase : this.toManyRelations) {
            toManyBase.init2ndPass();
        }
        if (this.active == null) {
            this.active = Boolean.valueOf(this.schema.isUseActiveEntitiesByDefault());
        }
        boolean booleanValue = this.active.booleanValue();
        if (!this.toOneRelations.isEmpty() || !this.toManyRelations.isEmpty()) {
            z = true;
        }
        this.active = Boolean.valueOf(z | booleanValue);
        if (this.hasKeepSections == null) {
            this.hasKeepSections = Boolean.valueOf(this.schema.isHasKeepSectionsByDefault());
        }
        init2ndPassIndexNamesWithDefaults();
        for (ContentProvider contentProvider : this.contentProviders) {
            contentProvider.init2ndPass();
        }
    }

    /* access modifiers changed from: protected */
    public void init2ndPassNamesWithDefaults() {
        if (this.dbName == null) {
            this.dbName = DaoUtil.dbName(this.className);
            this.nonDefaultDbName = false;
        }
        if (this.classNameDao == null) {
            this.classNameDao = this.className + "Dao";
        }
        if (this.classNameTest == null) {
            this.classNameTest = this.className + "Test";
        }
        if (this.javaPackage == null) {
            this.javaPackage = this.schema.getDefaultJavaPackage();
        }
        if (this.javaPackageDao == null) {
            this.javaPackageDao = this.schema.getDefaultJavaPackageDao();
            if (this.javaPackageDao == null) {
                this.javaPackageDao = this.javaPackage;
            }
        }
        if (this.javaPackageTest == null) {
            this.javaPackageTest = this.schema.getDefaultJavaPackageTest();
            if (this.javaPackageTest == null) {
                this.javaPackageTest = this.javaPackage;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void init2ndPassIndexNamesWithDefaults() {
        for (int i = 0; i < this.indexes.size(); i++) {
            Index index = this.indexes.get(i);
            if (index.getName() == null) {
                List<Property> properties2 = index.getProperties();
                String str = "IDX_" + getDbName();
                for (int i2 = 0; i2 < properties2.size(); i2++) {
                    str = str + "_" + properties2.get(i2).getDbName();
                    if ("DESC".equalsIgnoreCase(index.getPropertiesOrder().get(i2))) {
                        str = str + "_DESC";
                    }
                }
                index.setDefaultName(str);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void init3rdPass() {
        for (Property property : this.properties) {
            property.init3ndPass();
        }
        init3rdPassRelations();
        init3rdPassAdditionalImports();
    }

    private void init3rdPassRelations() {
        HashSet hashSet = new HashSet();
        for (ToOne toOne : this.toOneRelations) {
            toOne.init3ndPass();
            if (!hashSet.add(toOne.getName().toLowerCase())) {
                throw new RuntimeException("Duplicate name for " + toOne);
            }
        }
        HashSet hashSet2 = new HashSet();
        for (ToManyBase toManyBase : this.toManyRelations) {
            toManyBase.init3rdPass();
            if (toManyBase instanceof ToMany) {
                Entity targetEntity = toManyBase.getTargetEntity();
                Property[] targetProperties = ((ToMany) toManyBase).getTargetProperties();
                for (Property property : targetProperties) {
                    if (!targetEntity.propertiesColumns.contains(property)) {
                        targetEntity.propertiesColumns.add(property);
                    }
                }
            }
            if (!hashSet2.add(toManyBase.getName().toLowerCase())) {
                throw new RuntimeException("Duplicate name for " + toManyBase);
            }
        }
    }

    private void init3rdPassAdditionalImports() {
        String packageFromFullyQualified;
        if (this.active.booleanValue() && !this.javaPackage.equals(this.javaPackageDao)) {
            Collection<String> collection = this.additionalImportsEntity;
            collection.add(this.javaPackageDao + FileUtil.HIDDEN_PREFIX + this.classNameDao);
        }
        for (ToOne toOne : this.toOneRelations) {
            Entity targetEntity = toOne.getTargetEntity();
            checkAdditionalImportsEntityTargetEntity(targetEntity);
            checkAdditionalImportsDaoTargetEntity(targetEntity);
        }
        for (ToManyBase toManyBase : this.toManyRelations) {
            checkAdditionalImportsEntityTargetEntity(toManyBase.getTargetEntity());
        }
        for (ToManyBase toManyBase2 : this.incomingToManyRelations) {
            if (toManyBase2 instanceof ToManyWithJoinEntity) {
                checkAdditionalImportsDaoTargetEntity(((ToManyWithJoinEntity) toManyBase2).getJoinEntity());
            }
        }
        for (Property property : this.properties) {
            String customType = property.getCustomType();
            if (customType != null) {
                String packageFromFullyQualified2 = DaoUtil.getPackageFromFullyQualified(customType);
                if (packageFromFullyQualified2 != null && !packageFromFullyQualified2.equals(this.javaPackage)) {
                    this.additionalImportsEntity.add(customType);
                }
                if (packageFromFullyQualified2 != null && !packageFromFullyQualified2.equals(this.javaPackageDao)) {
                    this.additionalImportsDao.add(customType);
                }
            }
            String converter = property.getConverter();
            if (!(converter == null || (packageFromFullyQualified = DaoUtil.getPackageFromFullyQualified(converter)) == null || packageFromFullyQualified.equals(this.javaPackageDao))) {
                this.additionalImportsDao.add(converter);
            }
        }
    }

    private void checkAdditionalImportsEntityTargetEntity(Entity entity) {
        if (!entity.getJavaPackage().equals(this.javaPackage)) {
            Collection<String> collection = this.additionalImportsEntity;
            collection.add(entity.getJavaPackage() + FileUtil.HIDDEN_PREFIX + entity.getClassName());
        }
        if (!entity.getJavaPackageDao().equals(this.javaPackage)) {
            Collection<String> collection2 = this.additionalImportsEntity;
            collection2.add(entity.getJavaPackageDao() + FileUtil.HIDDEN_PREFIX + entity.getClassNameDao());
        }
    }

    private void checkAdditionalImportsDaoTargetEntity(Entity entity) {
        if (!entity.getJavaPackage().equals(this.javaPackageDao)) {
            Collection<String> collection = this.additionalImportsDao;
            collection.add(entity.getJavaPackage() + FileUtil.HIDDEN_PREFIX + entity.getClassName());
        }
    }

    public void validatePropertyExists(Property property) {
        if (!this.properties.contains(property)) {
            throw new RuntimeException("Property " + property + " does not exist in " + this);
        }
    }

    public List<Index> getMultiIndexes() {
        return this.multiIndexes;
    }

    public boolean isNonDefaultDbName() {
        return this.nonDefaultDbName;
    }

    public String toString() {
        return "Entity " + this.className + " (package: " + this.javaPackage + ")";
    }
}
