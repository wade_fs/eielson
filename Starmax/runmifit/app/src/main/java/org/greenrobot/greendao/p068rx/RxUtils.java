package org.greenrobot.greendao.p068rx;

import java.util.concurrent.Callable;
import p042rx.Observable;
import p042rx.functions.Func0;

/* renamed from: org.greenrobot.greendao.rx.RxUtils */
class RxUtils {
    RxUtils() {
    }

    static <T> Observable<T> fromCallable(final Callable<T> callable) {
        return Observable.defer(new Func0<Observable<T>>() {
            /* class org.greenrobot.greendao.p068rx.RxUtils.C36611 */

            public Observable<T> call() {
                try {
                    return Observable.just(callable.call());
                } catch (Exception e) {
                    return Observable.error(e);
                }
            }
        });
    }
}
