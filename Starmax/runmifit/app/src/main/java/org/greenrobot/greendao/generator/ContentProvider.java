package org.greenrobot.greendao.generator;

import java.util.List;

public class ContentProvider {
    private String authority;
    private String basePath;
    private String className;
    private final List<Entity> entities;
    private String javaPackage;
    private boolean readOnly;
    private Schema schema;

    public ContentProvider(Schema schema2, List<Entity> list) {
        this.schema = schema2;
        this.entities = list;
    }

    public String getAuthority() {
        return this.authority;
    }

    public void setAuthority(String str) {
        this.authority = str;
    }

    public String getBasePath() {
        return this.basePath;
    }

    public void setBasePath(String str) {
        this.basePath = str;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String str) {
        this.className = str;
    }

    public String getJavaPackage() {
        return this.javaPackage;
    }

    public void setJavaPackage(String str) {
        this.javaPackage = str;
    }

    public boolean isReadOnly() {
        return this.readOnly;
    }

    public void readOnly() {
        this.readOnly = true;
    }

    public List<Entity> getEntities() {
        return this.entities;
    }

    public void init2ndPass() {
        if (this.authority == null) {
            this.authority = this.schema.getDefaultJavaPackage() + ".provider";
        }
        if (this.basePath == null) {
            this.basePath = "";
        }
        if (this.className == null) {
            this.className = this.entities.get(0).getClassName() + "ContentProvider";
        }
        if (this.javaPackage == null) {
            this.javaPackage = this.schema.getDefaultJavaPackage();
        }
    }
}
