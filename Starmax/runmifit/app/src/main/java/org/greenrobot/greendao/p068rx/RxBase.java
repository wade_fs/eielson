package org.greenrobot.greendao.p068rx;

import java.util.concurrent.Callable;
import p042rx.Observable;
import p042rx.Scheduler;

/* renamed from: org.greenrobot.greendao.rx.RxBase */
class RxBase {
    protected final Scheduler scheduler;

    RxBase() {
        this.scheduler = null;
    }

    RxBase(Scheduler scheduler2) {
        this.scheduler = scheduler2;
    }

    public Scheduler getScheduler() {
        return this.scheduler;
    }

    /* access modifiers changed from: protected */
    public <R> Observable<R> wrap(Callable callable) {
        return wrap(RxUtils.fromCallable(callable));
    }

    /* access modifiers changed from: protected */
    public <R> Observable<R> wrap(Observable observable) {
        Scheduler scheduler2 = this.scheduler;
        return scheduler2 != null ? observable.subscribeOn(scheduler2) : observable;
    }
}
