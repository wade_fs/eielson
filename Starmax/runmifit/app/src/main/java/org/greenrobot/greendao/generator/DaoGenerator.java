package org.greenrobot.greendao.generator;

import com.tamic.novate.util.FileUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateNotFoundException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DaoGenerator {
    private Pattern patternKeepFields = compilePattern("FIELDS");
    private Pattern patternKeepIncludes = compilePattern("INCLUDES");
    private Pattern patternKeepMethods = compilePattern("METHODS");
    private Template templateContentProvider;
    private Template templateDao;
    private Template templateDaoMaster;
    private Template templateDaoSession;
    private Template templateDaoUnitTest;
    private Template templateEntity;

    public DaoGenerator() throws IOException {
        System.out.println("greenDAO Generator");
        System.out.println("Copyright 2011-2016 Markus Junginger, greenrobot.de. Licensed under GPL V3.");
        System.out.println("This program comes with ABSOLUTELY NO WARRANTY");
        Configuration configuration = getConfiguration("dao.ftl");
        this.templateDao = configuration.getTemplate("dao.ftl");
        this.templateDaoMaster = configuration.getTemplate("dao-master.ftl");
        this.templateDaoSession = configuration.getTemplate("dao-session.ftl");
        this.templateEntity = configuration.getTemplate("entity.ftl");
        this.templateDaoUnitTest = configuration.getTemplate("dao-unit-test.ftl");
        this.templateContentProvider = configuration.getTemplate("content-provider.ftl");
    }

    private Configuration getConfiguration(String str) throws IOException {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_23);
        configuration.setClassForTemplateLoading(getClass(), "/");
        try {
            configuration.getTemplate(str);
        } catch (TemplateNotFoundException e) {
            File file = new File("src/main/resources/");
            if (!file.exists()) {
                file = new File("DaoGenerator/src/main/resources/");
            }
            if (!file.exists() || !new File(file, str).exists()) {
                throw e;
            }
            configuration.setDirectoryForTemplateLoading(file);
            configuration.getTemplate(str);
        }
        return configuration;
    }

    private Pattern compilePattern(String str) {
        return Pattern.compile(".*^\\s*?//\\s*?KEEP " + str + ".*?\n(.*?)^\\s*// KEEP " + str + " END.*?\n", 40);
    }

    public void generateAll(Schema schema, String str) throws Exception {
        generateAll(schema, str, null, null);
    }

    public void generateAll(Schema schema, String str, String str2, String str3) throws Exception {
        String str4 = str2;
        String str5 = str3;
        long currentTimeMillis = System.currentTimeMillis();
        File fileForceExists = toFileForceExists(str);
        File fileForceExists2 = str4 != null ? toFileForceExists(str4) : fileForceExists;
        File fileForceExists3 = str5 != null ? toFileForceExists(str5) : null;
        schema.init2ndPass();
        schema.init3rdPass();
        PrintStream printStream = System.out;
        printStream.println("Processing schema version " + schema.getVersion() + "...");
        List<Entity> entities = schema.getEntities();
        for (Entity entity : entities) {
            generate(this.templateDao, fileForceExists, entity.getJavaPackageDao(), entity.getClassNameDao(), schema, entity);
            if (!entity.isProtobuf() && !entity.isSkipGeneration()) {
                generate(this.templateEntity, fileForceExists2, entity.getJavaPackage(), entity.getClassName(), schema, entity);
            }
            if (fileForceExists3 != null && !entity.isSkipGenerationTest()) {
                String javaPackageTest = entity.getJavaPackageTest();
                String classNameTest = entity.getClassNameTest();
                File javaFilename = toJavaFilename(fileForceExists3, javaPackageTest, classNameTest);
                if (!javaFilename.exists()) {
                    generate(this.templateDaoUnitTest, fileForceExists3, javaPackageTest, classNameTest, schema, entity);
                } else {
                    PrintStream printStream2 = System.out;
                    printStream2.println("Skipped " + javaFilename.getCanonicalPath());
                }
            }
            for (ContentProvider contentProvider : entity.getContentProviders()) {
                HashMap hashMap = new HashMap();
                hashMap.put("contentProvider", contentProvider);
                Template template = this.templateContentProvider;
                String javaPackage = entity.getJavaPackage();
                generate(template, fileForceExists, javaPackage, entity.getClassName() + "ContentProvider", schema, entity, hashMap);
            }
        }
        Template template2 = this.templateDaoMaster;
        String defaultJavaPackageDao = schema.getDefaultJavaPackageDao();
        Schema schema2 = schema;
        generate(template2, fileForceExists, defaultJavaPackageDao, schema.getPrefix() + "DaoMaster", schema2, null);
        Template template3 = this.templateDaoSession;
        String defaultJavaPackageDao2 = schema.getDefaultJavaPackageDao();
        generate(template3, fileForceExists, defaultJavaPackageDao2, schema.getPrefix() + "DaoSession", schema2, null);
        PrintStream printStream3 = System.out;
        printStream3.println("Processed " + entities.size() + " entities in " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
    }

    /* access modifiers changed from: protected */
    public File toFileForceExists(String str) throws IOException {
        File file = new File(str);
        if (file.exists()) {
            return file;
        }
        throw new IOException(str + " does not exist. This check is to prevent accidental file generation into a wrong path.");
    }

    private void generate(Template template, File file, String str, String str2, Schema schema, Entity entity) throws Exception {
        generate(template, file, str, str2, schema, entity, null);
    }

    private void generate(Template template, File file, String str, String str2, Schema schema, Entity entity, Map<String, Object> map) throws Exception {
        FileWriter fileWriter;
        HashMap hashMap = new HashMap();
        hashMap.put("schema", schema);
        hashMap.put("entity", entity);
        if (map != null) {
            hashMap.putAll(map);
        }
        try {
            File javaFilename = toJavaFilename(file, str, str2);
            javaFilename.getParentFile().mkdirs();
            if (entity != null && entity.getHasKeepSections().booleanValue()) {
                checkKeepSections(javaFilename, hashMap);
            }
            fileWriter = new FileWriter(javaFilename);
            template.process(hashMap, fileWriter);
            fileWriter.flush();
            PrintStream printStream = System.out;
            printStream.println("Written " + javaFilename.getCanonicalPath());
            fileWriter.close();
        } catch (Exception e) {
            PrintStream printStream2 = System.err;
            printStream2.println("Data map for template: " + hashMap);
            PrintStream printStream3 = System.err;
            printStream3.println("Error while generating " + str + FileUtil.HIDDEN_PREFIX + str2 + " (" + file.getCanonicalPath() + ")");
            throw e;
        } catch (Throwable th) {
            fileWriter.close();
            throw th;
        }
    }

    private void checkKeepSections(File file, Map<String, Object> map) {
        if (file.exists()) {
            try {
                String str = new String(DaoUtil.readAllBytes(file));
                Matcher matcher = this.patternKeepIncludes.matcher(str);
                if (matcher.matches()) {
                    map.put("keepIncludes", matcher.group(1));
                }
                Matcher matcher2 = this.patternKeepFields.matcher(str);
                if (matcher2.matches()) {
                    map.put("keepFields", matcher2.group(1));
                }
                Matcher matcher3 = this.patternKeepMethods.matcher(str);
                if (matcher3.matches()) {
                    map.put("keepMethods", matcher3.group(1));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: protected */
    public File toJavaFilename(File file, String str, String str2) {
        File file2 = new File(file, str.replace('.', '/'));
        return new File(file2, str2 + ".java");
    }
}
