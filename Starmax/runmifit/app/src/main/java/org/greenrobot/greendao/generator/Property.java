package org.greenrobot.greendao.generator;

public class Property {
    /* access modifiers changed from: private */
    public String codeBeforeField;
    /* access modifiers changed from: private */
    public String codeBeforeGetter;
    /* access modifiers changed from: private */
    public String codeBeforeSetter;
    private String constraints;
    /* access modifiers changed from: private */
    public String converter;
    /* access modifiers changed from: private */
    public String converterClassName;
    /* access modifiers changed from: private */
    public String customType;
    /* access modifiers changed from: private */
    public String customTypeClassName;
    /* access modifiers changed from: private */
    public String dbName;
    /* access modifiers changed from: private */
    public String dbType;
    /* access modifiers changed from: private */
    public final Entity entity;
    private Index index;
    /* access modifiers changed from: private */
    public String javaDocField;
    /* access modifiers changed from: private */
    public String javaDocGetter;
    /* access modifiers changed from: private */
    public String javaDocSetter;
    private String javaType;
    /* access modifiers changed from: private */
    public boolean nonDefaultDbName;
    /* access modifiers changed from: private */
    public boolean nonPrimitiveType;
    /* access modifiers changed from: private */
    public boolean notNull;
    private int ordinal;
    /* access modifiers changed from: private */
    public boolean pkAsc;
    /* access modifiers changed from: private */
    public boolean pkAutoincrement;
    /* access modifiers changed from: private */
    public boolean pkDesc;
    /* access modifiers changed from: private */
    public boolean primaryKey;
    private final String propertyName;
    /* access modifiers changed from: private */
    public PropertyType propertyType;
    private final Schema schema;
    /* access modifiers changed from: private */
    public boolean unique;

    /* access modifiers changed from: package-private */
    public void init3ndPass() {
    }

    public static class PropertyBuilder {
        private final Property property;

        public PropertyBuilder(Schema schema, Entity entity, PropertyType propertyType, String str) {
            this.property = new Property(schema, entity, propertyType, str);
        }

        @Deprecated
        public PropertyBuilder columnName(String str) {
            return dbName(str);
        }

        public PropertyBuilder dbName(String str) {
            String unused = this.property.dbName = str;
            boolean unused2 = this.property.nonDefaultDbName = str != null;
            return this;
        }

        @Deprecated
        public PropertyBuilder columnType(String str) {
            return dbType(str);
        }

        public PropertyBuilder dbType(String str) {
            String unused = this.property.dbType = str;
            return this;
        }

        public PropertyBuilder primaryKey() {
            boolean unused = this.property.primaryKey = true;
            return this;
        }

        public PropertyBuilder primaryKeyAsc() {
            boolean unused = this.property.primaryKey = true;
            boolean unused2 = this.property.pkAsc = true;
            return this;
        }

        public PropertyBuilder primaryKeyDesc() {
            boolean unused = this.property.primaryKey = true;
            boolean unused2 = this.property.pkDesc = true;
            return this;
        }

        public PropertyBuilder autoincrement() {
            if (!this.property.primaryKey || this.property.propertyType != PropertyType.Long) {
                throw new RuntimeException("AUTOINCREMENT is only available to primary key properties of type long/Long");
            }
            boolean unused = this.property.pkAutoincrement = true;
            return this;
        }

        public PropertyBuilder unique() {
            boolean unused = this.property.unique = true;
            return this;
        }

        public PropertyBuilder notNull() {
            boolean unused = this.property.notNull = true;
            return this;
        }

        public PropertyBuilder nonPrimitiveType() {
            if (this.property.propertyType.isScalar()) {
                boolean unused = this.property.nonPrimitiveType = true;
                return this;
            }
            throw new RuntimeException("Type is already non-primitive");
        }

        public PropertyBuilder index() {
            Index index = new Index();
            index.addProperty(this.property);
            this.property.entity.addIndex(index);
            return this;
        }

        public PropertyBuilder indexAsc(String str, boolean z) {
            Index index = new Index();
            index.addPropertyAsc(this.property);
            if (z) {
                index.makeUnique();
            }
            index.setName(str);
            this.property.entity.addIndex(index);
            return this;
        }

        public PropertyBuilder indexDesc(String str, boolean z) {
            Index index = new Index();
            index.addPropertyDesc(this.property);
            if (z) {
                index.makeUnique();
            }
            index.setName(str);
            this.property.entity.addIndex(index);
            return this;
        }

        public PropertyBuilder customType(String str, String str2) {
            String unused = this.property.customType = str;
            String unused2 = this.property.customTypeClassName = DaoUtil.getClassnameFromFullyQualified(str);
            String unused3 = this.property.converter = str2;
            String unused4 = this.property.converterClassName = DaoUtil.getClassnameFromFullyQualified(str2);
            return this;
        }

        public PropertyBuilder codeBeforeField(String str) {
            String unused = this.property.codeBeforeField = str;
            return this;
        }

        public PropertyBuilder codeBeforeGetter(String str) {
            String unused = this.property.codeBeforeGetter = str;
            return this;
        }

        public PropertyBuilder codeBeforeSetter(String str) {
            String unused = this.property.codeBeforeSetter = str;
            return this;
        }

        public PropertyBuilder codeBeforeGetterAndSetter(String str) {
            String unused = this.property.codeBeforeGetter = str;
            String unused2 = this.property.codeBeforeSetter = str;
            return this;
        }

        public PropertyBuilder javaDocField(String str) {
            String unused = this.property.javaDocField = checkConvertToJavaDoc(str);
            return this;
        }

        private String checkConvertToJavaDoc(String str) {
            return DaoUtil.checkConvertToJavaDoc(str, "    ");
        }

        public PropertyBuilder javaDocGetter(String str) {
            String unused = this.property.javaDocGetter = checkConvertToJavaDoc(str);
            return this;
        }

        public PropertyBuilder javaDocSetter(String str) {
            String unused = this.property.javaDocSetter = checkConvertToJavaDoc(str);
            return this;
        }

        public PropertyBuilder javaDocGetterAndSetter(String str) {
            String checkConvertToJavaDoc = checkConvertToJavaDoc(str);
            String unused = this.property.javaDocGetter = checkConvertToJavaDoc;
            String unused2 = this.property.javaDocSetter = checkConvertToJavaDoc;
            return this;
        }

        public Property getProperty() {
            return this.property;
        }
    }

    public Property(Schema schema2, Entity entity2, PropertyType propertyType2, String str) {
        this.schema = schema2;
        this.entity = entity2;
        this.propertyName = str;
        this.propertyType = propertyType2;
    }

    public String getPropertyName() {
        return this.propertyName;
    }

    public PropertyType getPropertyType() {
        return this.propertyType;
    }

    public void setPropertyType(PropertyType propertyType2) {
        this.propertyType = propertyType2;
    }

    public String getDbName() {
        return this.dbName;
    }

    public boolean isNonDefaultDbName() {
        return this.nonDefaultDbName;
    }

    public String getDbType() {
        return this.dbType;
    }

    public boolean isPrimaryKey() {
        return this.primaryKey;
    }

    public boolean isPkAsc() {
        return this.pkAsc;
    }

    public boolean isPkDesc() {
        return this.pkDesc;
    }

    public boolean isAutoincrement() {
        return this.pkAutoincrement;
    }

    public String getConstraints() {
        return this.constraints;
    }

    public boolean isUnique() {
        return this.unique;
    }

    public boolean isNotNull() {
        return this.notNull;
    }

    public boolean isNonPrimitiveType() {
        return this.nonPrimitiveType || !this.propertyType.isScalar();
    }

    public String getJavaType() {
        return this.javaType;
    }

    public String getJavaTypeInEntity() {
        String str = this.customTypeClassName;
        if (str != null) {
            return str;
        }
        return this.javaType;
    }

    public int getOrdinal() {
        return this.ordinal;
    }

    /* access modifiers changed from: package-private */
    public void setOrdinal(int i) {
        this.ordinal = i;
    }

    public String getCustomType() {
        return this.customType;
    }

    public String getCustomTypeClassName() {
        return this.customTypeClassName;
    }

    public String getConverter() {
        return this.converter;
    }

    public String getConverterClassName() {
        return this.converterClassName;
    }

    public String getCodeBeforeField() {
        return this.codeBeforeField;
    }

    public String getCodeBeforeGetter() {
        return this.codeBeforeGetter;
    }

    public String getCodeBeforeSetter() {
        return this.codeBeforeSetter;
    }

    public String getJavaDocField() {
        return this.javaDocField;
    }

    public String getJavaDocGetter() {
        return this.javaDocGetter;
    }

    public String getJavaDocSetter() {
        return this.javaDocSetter;
    }

    public String getDatabaseValueExpression() {
        return getDatabaseValueExpression(this.propertyName);
    }

    public String getDatabaseValueExpressionNotNull() {
        return getDatabaseValueExpression("entity.get" + DaoUtil.capFirst(this.propertyName) + "()");
    }

    public String getDatabaseValueExpression(String str) {
        StringBuilder sb = new StringBuilder();
        if (this.customType != null) {
            sb.append(this.propertyName);
            sb.append("Converter.convertToDatabaseValue(");
        }
        sb.append(str);
        if (this.customType != null) {
            sb.append(')');
        }
        if (this.propertyType == PropertyType.Boolean) {
            sb.append(" ? 1L: 0L");
        } else if (this.propertyType == PropertyType.Date) {
            sb.append(".getTime()");
        }
        return sb.toString();
    }

    public String getEntityValueExpression(String str) {
        StringBuilder sb = new StringBuilder();
        if (this.customType != null) {
            sb.append(this.propertyName);
            sb.append("Converter.convertToEntityProperty(");
        }
        if (this.propertyType == PropertyType.Byte) {
            sb.append("(byte) ");
        } else if (this.propertyType == PropertyType.Date) {
            sb.append("new java.util.Date(");
        }
        sb.append(str);
        if (this.propertyType == PropertyType.Boolean) {
            sb.append(" != 0");
        } else if (this.propertyType == PropertyType.Date) {
            sb.append(")");
        }
        if (this.customType != null) {
            sb.append(')');
        }
        return sb.toString();
    }

    public Entity getEntity() {
        return this.entity;
    }

    public Index getIndex() {
        return this.index;
    }

    public void setIndex(Index index2) {
        this.index = index2;
    }

    /* access modifiers changed from: package-private */
    public void init2ndPass() {
        initConstraint();
        if (this.dbType == null) {
            this.dbType = this.schema.mapToDbType(this.propertyType);
        }
        if (this.dbName == null) {
            this.dbName = DaoUtil.dbName(this.propertyName);
            this.nonDefaultDbName = false;
        } else if (this.primaryKey && this.propertyType == PropertyType.Long && this.dbName.equals("_id")) {
            this.nonDefaultDbName = false;
        }
        if (!this.notNull || this.nonPrimitiveType) {
            this.javaType = this.schema.mapToJavaTypeNullable(this.propertyType);
        } else {
            this.javaType = this.schema.mapToJavaTypeNotNull(this.propertyType);
        }
    }

    private void initConstraint() {
        StringBuilder sb = new StringBuilder();
        if (this.primaryKey) {
            sb.append("PRIMARY KEY");
            if (this.pkAsc) {
                sb.append(" ASC");
            }
            if (this.pkDesc) {
                sb.append(" DESC");
            }
            if (this.pkAutoincrement) {
                sb.append(" AUTOINCREMENT");
            }
        }
        if (this.notNull || (this.primaryKey && this.propertyType == PropertyType.String)) {
            sb.append(" NOT NULL");
        }
        if (this.unique) {
            sb.append(" UNIQUE");
        }
        String trim = sb.toString().trim();
        if (sb.length() > 0) {
            this.constraints = trim;
        }
    }

    public String toString() {
        return "Property " + this.propertyName + " of " + this.entity.getClassName();
    }
}
