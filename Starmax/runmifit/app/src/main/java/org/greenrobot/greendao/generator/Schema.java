package org.greenrobot.greendao.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Schema {
    public static final String DEFAULT_NAME = "default";
    private final String defaultJavaPackage;
    private String defaultJavaPackageDao;
    private String defaultJavaPackageTest;
    private final List<Entity> entities;
    private boolean hasKeepSectionsByDefault;
    private final String name;
    private final String prefix;
    private Map<PropertyType, String> propertyToDbType;
    private Map<PropertyType, String> propertyToJavaTypeNotNull;
    private Map<PropertyType, String> propertyToJavaTypeNullable;
    private boolean useActiveEntitiesByDefault;
    private final int version;

    public Schema(String str, int i, String str2) {
        this.name = str;
        this.prefix = str.equals(DEFAULT_NAME) ? "" : DaoUtil.capFirst(str);
        this.version = i;
        this.defaultJavaPackage = str2;
        this.entities = new ArrayList();
        initTypeMappings();
    }

    public Schema(int i, String str) {
        this(DEFAULT_NAME, i, str);
    }

    public void enableKeepSectionsByDefault() {
        this.hasKeepSectionsByDefault = true;
    }

    public void enableActiveEntitiesByDefault() {
        this.useActiveEntitiesByDefault = true;
    }

    private void initTypeMappings() {
        this.propertyToDbType = new HashMap();
        this.propertyToDbType.put(PropertyType.Boolean, "INTEGER");
        this.propertyToDbType.put(PropertyType.Byte, "INTEGER");
        this.propertyToDbType.put(PropertyType.Short, "INTEGER");
        this.propertyToDbType.put(PropertyType.Int, "INTEGER");
        this.propertyToDbType.put(PropertyType.Long, "INTEGER");
        this.propertyToDbType.put(PropertyType.Float, "REAL");
        this.propertyToDbType.put(PropertyType.Double, "REAL");
        this.propertyToDbType.put(PropertyType.String, "TEXT");
        this.propertyToDbType.put(PropertyType.ByteArray, "BLOB");
        this.propertyToDbType.put(PropertyType.Date, "INTEGER");
        this.propertyToJavaTypeNotNull = new HashMap();
        this.propertyToJavaTypeNotNull.put(PropertyType.Boolean, "boolean");
        this.propertyToJavaTypeNotNull.put(PropertyType.Byte, "byte");
        this.propertyToJavaTypeNotNull.put(PropertyType.Short, "short");
        this.propertyToJavaTypeNotNull.put(PropertyType.Int, "int");
        this.propertyToJavaTypeNotNull.put(PropertyType.Long, "long");
        this.propertyToJavaTypeNotNull.put(PropertyType.Float, "float");
        this.propertyToJavaTypeNotNull.put(PropertyType.Double, "double");
        this.propertyToJavaTypeNotNull.put(PropertyType.String, "String");
        this.propertyToJavaTypeNotNull.put(PropertyType.ByteArray, "byte[]");
        this.propertyToJavaTypeNotNull.put(PropertyType.Date, "java.util.Date");
        this.propertyToJavaTypeNullable = new HashMap();
        this.propertyToJavaTypeNullable.put(PropertyType.Boolean, "Boolean");
        this.propertyToJavaTypeNullable.put(PropertyType.Byte, "Byte");
        this.propertyToJavaTypeNullable.put(PropertyType.Short, "Short");
        this.propertyToJavaTypeNullable.put(PropertyType.Int, "Integer");
        this.propertyToJavaTypeNullable.put(PropertyType.Long, "Long");
        this.propertyToJavaTypeNullable.put(PropertyType.Float, "Float");
        this.propertyToJavaTypeNullable.put(PropertyType.Double, "Double");
        this.propertyToJavaTypeNullable.put(PropertyType.String, "String");
        this.propertyToJavaTypeNullable.put(PropertyType.ByteArray, "byte[]");
        this.propertyToJavaTypeNullable.put(PropertyType.Date, "java.util.Date");
    }

    public Entity addEntity(String str) {
        Entity entity = new Entity(this, str);
        this.entities.add(entity);
        return entity;
    }

    public Entity addProtobufEntity(String str) {
        Entity addEntity = addEntity(str);
        addEntity.useProtobuf();
        return addEntity;
    }

    public String mapToDbType(PropertyType propertyType) {
        return mapType(this.propertyToDbType, propertyType);
    }

    public String mapToJavaTypeNullable(PropertyType propertyType) {
        return mapType(this.propertyToJavaTypeNullable, propertyType);
    }

    public String mapToJavaTypeNotNull(PropertyType propertyType) {
        return mapType(this.propertyToJavaTypeNotNull, propertyType);
    }

    private String mapType(Map<PropertyType, String> map, PropertyType propertyType) {
        String str = map.get(propertyType);
        if (str != null) {
            return str;
        }
        throw new IllegalStateException("No mapping for " + propertyType);
    }

    public int getVersion() {
        return this.version;
    }

    public String getDefaultJavaPackage() {
        return this.defaultJavaPackage;
    }

    public String getDefaultJavaPackageDao() {
        return this.defaultJavaPackageDao;
    }

    public void setDefaultJavaPackageDao(String str) {
        this.defaultJavaPackageDao = str;
    }

    public String getDefaultJavaPackageTest() {
        return this.defaultJavaPackageTest;
    }

    public void setDefaultJavaPackageTest(String str) {
        this.defaultJavaPackageTest = str;
    }

    public List<Entity> getEntities() {
        return this.entities;
    }

    public boolean isHasKeepSectionsByDefault() {
        return this.hasKeepSectionsByDefault;
    }

    public boolean isUseActiveEntitiesByDefault() {
        return this.useActiveEntitiesByDefault;
    }

    public String getName() {
        return this.name;
    }

    public String getPrefix() {
        return this.prefix;
    }

    /* access modifiers changed from: package-private */
    public void init2ndPass() {
        if (this.defaultJavaPackageDao == null) {
            this.defaultJavaPackageDao = this.defaultJavaPackage;
        }
        if (this.defaultJavaPackageTest == null) {
            this.defaultJavaPackageTest = this.defaultJavaPackageDao;
        }
        for (Entity entity : this.entities) {
            entity.init2ndPass();
        }
    }

    /* access modifiers changed from: package-private */
    public void init3rdPass() {
        for (Entity entity : this.entities) {
            entity.init3rdPass();
        }
    }
}
