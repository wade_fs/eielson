package org.greenrobot.greendao.query;

import android.database.Cursor;
import java.util.Date;
import org.greenrobot.greendao.AbstractDao;

public class CursorQuery<T> extends AbstractQueryWithLimit<T> {
    private final QueryData<T> queryData;

    public /* bridge */ /* synthetic */ void setLimit(int i) {
        super.setLimit(i);
    }

    public /* bridge */ /* synthetic */ void setOffset(int i) {
        super.setOffset(i);
    }

    private static final class QueryData<T2> extends AbstractQueryData<T2, CursorQuery<T2>> {
        private final int limitPosition;
        private final int offsetPosition;

        QueryData(AbstractDao abstractDao, String str, String[] strArr, int i, int i2) {
            super(abstractDao, str, strArr);
            this.limitPosition = i;
            this.offsetPosition = i2;
        }

        /* access modifiers changed from: protected */
        public CursorQuery<T2> createQuery() {
            return new CursorQuery(this, this.dao, this.sql, (String[]) this.initialValues.clone(), this.limitPosition, this.offsetPosition);
        }
    }

    public static <T2> CursorQuery<T2> internalCreate(AbstractDao<T2, ?> abstractDao, String str, Object[] objArr) {
        return create(abstractDao, str, objArr, -1, -1);
    }

    static <T2> CursorQuery<T2> create(AbstractDao<T2, ?> abstractDao, String str, Object[] objArr, int i, int i2) {
        return (CursorQuery) new QueryData(abstractDao, str, toStringArray(objArr), i, i2).forCurrentThread();
    }

    private CursorQuery(QueryData<T> queryData2, AbstractDao<T, ?> abstractDao, String str, String[] strArr, int i, int i2) {
        super(abstractDao, str, strArr, i, i2);
        this.queryData = queryData2;
    }

    public CursorQuery forCurrentThread() {
        return (CursorQuery) this.queryData.forCurrentThread(this);
    }

    public Cursor query() {
        checkThread();
        return this.dao.getDatabase().rawQuery(this.sql, this.parameters);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.greenrobot.greendao.query.AbstractQueryWithLimit.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQueryWithLimit<T>
     arg types: [int, java.lang.Object]
     candidates:
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.AbstractQueryWithLimit.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.AbstractQuery<T>
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery<T>
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.AbstractQuery<T>
      org.greenrobot.greendao.query.AbstractQueryWithLimit.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQueryWithLimit<T> */
    public CursorQuery<T> setParameter(int i, Object obj) {
        return (CursorQuery) super.setParameter(i, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.AbstractQuery<T>
     arg types: [int, java.util.Date]
     candidates:
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQueryWithLimit
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.AbstractQueryWithLimit.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.AbstractQueryWithLimit.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQueryWithLimit<T>
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.AbstractQuery<T>
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery<T>
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.AbstractQuery<T> */
    public CursorQuery<T> setParameter(int i, Date date) {
        return (CursorQuery) super.setParameter(i, date);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.AbstractQuery<T>
     arg types: [int, java.lang.Boolean]
     candidates:
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQueryWithLimit
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.CursorQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.CursorQuery<T>
      org.greenrobot.greendao.query.AbstractQueryWithLimit.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery
      org.greenrobot.greendao.query.AbstractQueryWithLimit.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQueryWithLimit<T>
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.lang.Object):org.greenrobot.greendao.query.AbstractQuery<T>
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.util.Date):org.greenrobot.greendao.query.AbstractQuery<T>
      org.greenrobot.greendao.query.AbstractQuery.setParameter(int, java.lang.Boolean):org.greenrobot.greendao.query.AbstractQuery<T> */
    public CursorQuery<T> setParameter(int i, Boolean bool) {
        return (CursorQuery) super.setParameter(i, bool);
    }
}
