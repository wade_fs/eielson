package org.greenrobot.greendao.generator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DaoUtil {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    public static String dbName(String str) {
        StringBuilder sb = new StringBuilder(str);
        int i = 1;
        while (i < sb.length()) {
            boolean isUpperCase = Character.isUpperCase(sb.charAt(i - 1));
            if (Character.isUpperCase(sb.charAt(i)) && !isUpperCase) {
                sb.insert(i, '_');
                i++;
            }
            i++;
        }
        return sb.toString().toUpperCase();
    }

    public static String getClassnameFromFullyQualified(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        return lastIndexOf != -1 ? str.substring(lastIndexOf + 1) : str;
    }

    public static String capFirst(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(Character.toUpperCase(str.charAt(0)));
        sb.append(str.length() > 1 ? str.substring(1) : "");
        return sb.toString();
    }

    public static String getPackageFromFullyQualified(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf != -1) {
            return str.substring(0, lastIndexOf);
        }
        return null;
    }

    public static byte[] readAllBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        copyAllBytes(inputStream, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] readAllBytes(File file) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        try {
            return readAllBytes(fileInputStream);
        } finally {
            fileInputStream.close();
        }
    }

    public static byte[] readAllBytes(String str) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(str);
        try {
            return readAllBytes(fileInputStream);
        } finally {
            fileInputStream.close();
        }
    }

    public static int copyAllBytes(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[4096];
        int i = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return i;
            }
            outputStream.write(bArr, 0, read);
            i += read;
        }
    }

    public static String checkConvertToJavaDoc(String str, String str2) {
        if (str == null || str.trim().startsWith("/**")) {
            return str;
        }
        String replace = str.replace("\n", "\n" + str2 + " * ");
        return str2 + "/**\n" + str2 + " * " + replace + "\n" + str2 + " */";
    }
}
