package no.nordicsemi.android.other;

import android.os.Handler;
import android.os.Looper;

/* renamed from: no.nordicsemi.android.other.WritePeriodController */
public class WritePeriodController {
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static long lastWriteTime;

    public static void updateWriteTime() {
        lastWriteTime = System.currentTimeMillis();
    }

    public static void deviceResponse(final Runnable runnable) {
        long currentTimeMillis = System.currentTimeMillis() - lastWriteTime;
        if (currentTimeMillis < 10) {
            handler.postDelayed(new Runnable() {
                /* class no.nordicsemi.android.other.WritePeriodController.C34251 */

                public void run() {
                    runnable.run();
                }
            }, 10 - currentTimeMillis);
        } else {
            runnable.run();
        }
    }
}
