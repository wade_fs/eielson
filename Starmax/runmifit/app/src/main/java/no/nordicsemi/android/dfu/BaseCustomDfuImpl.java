package no.nordicsemi.android.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import java.io.IOException;
import java.util.UUID;
import java.util.zip.CRC32;
import no.nordicsemi.android.dfu.BaseDfuImpl;
import no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException;
import no.nordicsemi.android.dfu.internal.exception.DfuException;
import no.nordicsemi.android.dfu.internal.exception.HexFileValidationException;
import no.nordicsemi.android.dfu.internal.exception.UploadAbortedException;
import no.nordicsemi.android.other.WritePeriodController;

/* renamed from: no.nordicsemi.android.dfu.BaseCustomDfuImpl */
abstract class BaseCustomDfuImpl extends BaseDfuImpl {
    protected boolean mFirmwareUploadInProgress;
    /* access modifiers changed from: private */
    public boolean mInitPacketInProgress;
    protected int mPacketsBeforeNotification;
    protected int mPacketsSentSinceNotification;
    protected boolean mRemoteErrorOccurred;

    /* access modifiers changed from: protected */
    public abstract UUID getControlPointCharacteristicUUID();

    /* access modifiers changed from: protected */
    public abstract UUID getDfuServiceUUID();

    /* access modifiers changed from: protected */
    public abstract UUID getPacketCharacteristicUUID();

    /* renamed from: no.nordicsemi.android.dfu.BaseCustomDfuImpl$BaseCustomBluetoothCallback */
    protected class BaseCustomBluetoothCallback extends BaseDfuImpl.BaseBluetoothGattCallback {
        /* access modifiers changed from: protected */
        public void onPacketCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        }

        protected BaseCustomBluetoothCallback() {
            super();
        }

        public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            boolean z = true;
            if (i == 0) {
                if (!bluetoothGattCharacteristic.getUuid().equals(BaseCustomDfuImpl.this.getPacketCharacteristicUUID())) {
                    BaseCustomDfuImpl.this.mService.sendLogBroadcast(5, "Data written to " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + parse(bluetoothGattCharacteristic));
                    BaseCustomDfuImpl.this.mRequestCompleted = true;
                } else if (BaseCustomDfuImpl.this.mInitPacketInProgress) {
                    BaseCustomDfuImpl.this.mService.sendLogBroadcast(5, "Data written to " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + parse(bluetoothGattCharacteristic));
                    boolean unused = BaseCustomDfuImpl.this.mInitPacketInProgress = false;
                } else if (BaseCustomDfuImpl.this.mFirmwareUploadInProgress) {
                    BaseCustomDfuImpl.this.mProgressInfo.addBytesSent(bluetoothGattCharacteristic.getValue().length);
                    BaseCustomDfuImpl.this.mPacketsSentSinceNotification++;
                    if (BaseCustomDfuImpl.this.mPacketsBeforeNotification <= 0 || BaseCustomDfuImpl.this.mPacketsSentSinceNotification < BaseCustomDfuImpl.this.mPacketsBeforeNotification) {
                        z = false;
                    }
                    boolean isComplete = BaseCustomDfuImpl.this.mProgressInfo.isComplete();
                    boolean isObjectComplete = BaseCustomDfuImpl.this.mProgressInfo.isObjectComplete();
                    if (!z) {
                        if (isComplete || isObjectComplete) {
                            BaseCustomDfuImpl baseCustomDfuImpl = BaseCustomDfuImpl.this;
                            baseCustomDfuImpl.mFirmwareUploadInProgress = false;
                            baseCustomDfuImpl.notifyLock();
                            return;
                        }
                        try {
                            BaseCustomDfuImpl.this.waitIfPaused();
                            if (!BaseCustomDfuImpl.this.mAborted && BaseCustomDfuImpl.this.mError == 0 && !BaseCustomDfuImpl.this.mRemoteErrorOccurred) {
                                if (!BaseCustomDfuImpl.this.mResetRequestSent) {
                                    int availableObjectSizeIsBytes = BaseCustomDfuImpl.this.mProgressInfo.getAvailableObjectSizeIsBytes();
                                    byte[] bArr = BaseCustomDfuImpl.this.mBuffer;
                                    if (availableObjectSizeIsBytes < bArr.length) {
                                        bArr = new byte[availableObjectSizeIsBytes];
                                    }
                                    int read = BaseCustomDfuImpl.this.mFirmwareStream.read(bArr);
                                    WritePeriodController.updateWriteTime();
                                    BaseCustomDfuImpl.this.writePacket(bluetoothGatt, bluetoothGattCharacteristic, bArr, read);
                                    return;
                                }
                            }
                            BaseCustomDfuImpl.this.mFirmwareUploadInProgress = false;
                            BaseCustomDfuImpl.this.mService.sendLogBroadcast(15, "Upload terminated");
                            BaseCustomDfuImpl.this.notifyLock();
                            return;
                        } catch (HexFileValidationException unused2) {
                            BaseCustomDfuImpl.this.loge("Invalid HEX file");
                            BaseCustomDfuImpl.this.mError = 4099;
                        } catch (IOException e) {
                            BaseCustomDfuImpl.this.loge("Error while reading the input stream", e);
                            BaseCustomDfuImpl.this.mError = 4100;
                        }
                    } else {
                        return;
                    }
                } else {
                    onPacketCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
                }
            } else if (BaseCustomDfuImpl.this.mResetRequestSent) {
                BaseCustomDfuImpl.this.mRequestCompleted = true;
            } else {
                BaseCustomDfuImpl.this.loge("Characteristic write error: " + i);
                BaseCustomDfuImpl.this.mError = i | 16384;
            }
            BaseCustomDfuImpl.this.notifyLock();
        }

        /* access modifiers changed from: protected */
        public void handlePacketReceiptNotification(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            if (!BaseCustomDfuImpl.this.mFirmwareUploadInProgress) {
                handleNotification(bluetoothGatt, bluetoothGattCharacteristic);
                return;
            }
            BluetoothGattCharacteristic characteristic = bluetoothGatt.getService(BaseCustomDfuImpl.this.getDfuServiceUUID()).getCharacteristic(BaseCustomDfuImpl.this.getPacketCharacteristicUUID());
            try {
                BaseCustomDfuImpl.this.mPacketsSentSinceNotification = 0;
                BaseCustomDfuImpl.this.waitIfPaused();
                if (!BaseCustomDfuImpl.this.mAborted && BaseCustomDfuImpl.this.mError == 0 && !BaseCustomDfuImpl.this.mRemoteErrorOccurred) {
                    if (!BaseCustomDfuImpl.this.mResetRequestSent) {
                        boolean isComplete = BaseCustomDfuImpl.this.mProgressInfo.isComplete();
                        boolean isObjectComplete = BaseCustomDfuImpl.this.mProgressInfo.isObjectComplete();
                        if (!isComplete) {
                            if (!isObjectComplete) {
                                int availableObjectSizeIsBytes = BaseCustomDfuImpl.this.mProgressInfo.getAvailableObjectSizeIsBytes();
                                byte[] bArr = BaseCustomDfuImpl.this.mBuffer;
                                if (availableObjectSizeIsBytes < bArr.length) {
                                    bArr = new byte[availableObjectSizeIsBytes];
                                }
                                BaseCustomDfuImpl.this.writePacket(bluetoothGatt, characteristic, bArr, BaseCustomDfuImpl.this.mFirmwareStream.read(bArr));
                                return;
                            }
                        }
                        BaseCustomDfuImpl.this.mFirmwareUploadInProgress = false;
                        BaseCustomDfuImpl.this.notifyLock();
                        return;
                    }
                }
                BaseCustomDfuImpl.this.mFirmwareUploadInProgress = false;
                BaseCustomDfuImpl.this.mService.sendLogBroadcast(15, "Upload terminated");
            } catch (HexFileValidationException unused) {
                BaseCustomDfuImpl.this.loge("Invalid HEX file");
                BaseCustomDfuImpl.this.mError = 4099;
            } catch (IOException e) {
                BaseCustomDfuImpl.this.loge("Error while reading the input stream", e);
                BaseCustomDfuImpl.this.mError = 4100;
            }
        }

        /* access modifiers changed from: protected */
        public void handleNotification(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            DfuBaseService dfuBaseService = BaseCustomDfuImpl.this.mService;
            dfuBaseService.sendLogBroadcast(5, "Notification received from " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + parse(bluetoothGattCharacteristic));
            BaseCustomDfuImpl.this.mReceivedData = bluetoothGattCharacteristic.getValue();
            BaseCustomDfuImpl.this.mFirmwareUploadInProgress = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0050, code lost:
        if (r8 <= 65535) goto L_0x0054;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    BaseCustomDfuImpl(android.content.Intent r8, no.nordicsemi.android.dfu.DfuBaseService r9) {
        /*
            r7 = this;
            r7.<init>(r8, r9)
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_PRN_ENABLED"
            boolean r1 = r8.hasExtra(r0)
            r2 = 65535(0xffff, float:9.1834E-41)
            r3 = 1
            r4 = 23
            r5 = 0
            r6 = 12
            if (r1 == 0) goto L_0x0030
            int r9 = android.os.Build.VERSION.SDK_INT
            if (r9 >= r4) goto L_0x0019
            goto L_0x001a
        L_0x0019:
            r3 = 0
        L_0x001a:
            boolean r9 = r8.getBooleanExtra(r0, r3)
            java.lang.String r0 = "no.nordicsemi.android.dfu.extra.EXTRA_PRN_VALUE"
            int r8 = r8.getIntExtra(r0, r6)
            if (r8 < 0) goto L_0x0028
            if (r8 <= r2) goto L_0x002a
        L_0x0028:
            r8 = 12
        L_0x002a:
            if (r9 != 0) goto L_0x002d
            r8 = 0
        L_0x002d:
            r7.mPacketsBeforeNotification = r8
            goto L_0x0059
        L_0x0030:
            android.content.SharedPreferences r8 = android.preference.PreferenceManager.getDefaultSharedPreferences(r9)
            int r9 = android.os.Build.VERSION.SDK_INT
            if (r9 >= r4) goto L_0x0039
            goto L_0x003a
        L_0x0039:
            r3 = 0
        L_0x003a:
            java.lang.String r9 = "settings_packet_receipt_notification_enabled"
            boolean r9 = r8.getBoolean(r9, r3)
            java.lang.String r0 = java.lang.String.valueOf(r6)
            java.lang.String r1 = "settings_number_of_packets"
            java.lang.String r8 = r8.getString(r1, r0)
            int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ NumberFormatException -> 0x0052 }
            if (r8 < 0) goto L_0x0052
            if (r8 <= r2) goto L_0x0054
        L_0x0052:
            r8 = 12
        L_0x0054:
            if (r9 != 0) goto L_0x0057
            r8 = 0
        L_0x0057:
            r7.mPacketsBeforeNotification = r8
        L_0x0059:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.BaseCustomDfuImpl.<init>(android.content.Intent, no.nordicsemi.android.dfu.DfuBaseService):void");
    }

    /* access modifiers changed from: protected */
    public void writeInitData(BluetoothGattCharacteristic bluetoothGattCharacteristic, CRC32 crc32) throws DfuException, DeviceDisconnectedException, UploadAbortedException {
        try {
            byte[] bArr = this.mBuffer;
            while (true) {
                int read = this.mInitPacketStream.read(bArr, 0, bArr.length);
                if (read != -1) {
                    writeInitPacket(bluetoothGattCharacteristic, bArr, read);
                    if (crc32 != null) {
                        crc32.update(bArr, 0, read);
                    }
                } else {
                    return;
                }
            }
        } catch (IOException e) {
            loge("Error while reading Init packet file", e);
            throw new DfuException("Error while reading Init packet file", 4098);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void writeInitPacket(android.bluetooth.BluetoothGattCharacteristic r4, byte[] r5, int r6) throws no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException, no.nordicsemi.android.dfu.internal.exception.DfuException, no.nordicsemi.android.dfu.internal.exception.UploadAbortedException {
        /*
            r3 = this;
            boolean r0 = r3.mAborted
            if (r0 != 0) goto L_0x00b6
            int r0 = r5.length
            r1 = 0
            if (r0 == r6) goto L_0x000e
            byte[] r0 = new byte[r6]
            java.lang.System.arraycopy(r5, r1, r0, r1, r6)
            r5 = r0
        L_0x000e:
            r6 = 0
            r3.mReceivedData = r6
            r3.mError = r1
            r6 = 1
            r3.mInitPacketInProgress = r6
            r4.setWriteType(r6)
            r4.setValue(r5)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Sending init packet (Value = "
            r0.append(r2)
            java.lang.String r5 = r3.parse(r5)
            r0.append(r5)
            java.lang.String r5 = ")"
            r0.append(r5)
            java.lang.String r5 = r0.toString()
            r3.logi(r5)
            no.nordicsemi.android.dfu.DfuBaseService r5 = r3.mService
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Writing to characteristic "
            r0.append(r2)
            java.util.UUID r2 = r4.getUuid()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.sendLogBroadcast(r6, r0)
            no.nordicsemi.android.dfu.DfuBaseService r5 = r3.mService
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r0 = "gatt.writeCharacteristic("
            r6.append(r0)
            java.util.UUID r0 = r4.getUuid()
            r6.append(r0)
            java.lang.String r0 = ")"
            r6.append(r0)
            java.lang.String r6 = r6.toString()
            r5.sendLogBroadcast(r1, r6)
            android.bluetooth.BluetoothGatt r5 = r3.mGatt
            r5.writeCharacteristic(r4)
            java.lang.Object r4 = r3.mLock     // Catch:{ InterruptedException -> 0x0095 }
            monitor-enter(r4)     // Catch:{ InterruptedException -> 0x0095 }
        L_0x007a:
            boolean r5 = r3.mInitPacketInProgress     // Catch:{ all -> 0x0092 }
            if (r5 == 0) goto L_0x0086
            boolean r5 = r3.mConnected     // Catch:{ all -> 0x0092 }
            if (r5 == 0) goto L_0x0086
            int r5 = r3.mError     // Catch:{ all -> 0x0092 }
            if (r5 == 0) goto L_0x008a
        L_0x0086:
            boolean r5 = r3.mPaused     // Catch:{ all -> 0x0092 }
            if (r5 == 0) goto L_0x0090
        L_0x008a:
            java.lang.Object r5 = r3.mLock     // Catch:{ all -> 0x0092 }
            r5.wait()     // Catch:{ all -> 0x0092 }
            goto L_0x007a
        L_0x0090:
            monitor-exit(r4)     // Catch:{ all -> 0x0092 }
            goto L_0x009b
        L_0x0092:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0092 }
            throw r5     // Catch:{ InterruptedException -> 0x0095 }
        L_0x0095:
            r4 = move-exception
            java.lang.String r5 = "Sleeping interrupted"
            r3.loge(r5, r4)
        L_0x009b:
            int r4 = r3.mError
            if (r4 != 0) goto L_0x00ac
            boolean r4 = r3.mConnected
            if (r4 == 0) goto L_0x00a4
            return
        L_0x00a4:
            no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException r4 = new no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException
            java.lang.String r5 = "Unable to write Init DFU Parameters: device disconnected"
            r4.<init>(r5)
            throw r4
        L_0x00ac:
            no.nordicsemi.android.dfu.internal.exception.DfuException r4 = new no.nordicsemi.android.dfu.internal.exception.DfuException
            int r5 = r3.mError
            java.lang.String r6 = "Unable to write Init DFU Parameters"
            r4.<init>(r6, r5)
            throw r4
        L_0x00b6:
            no.nordicsemi.android.dfu.internal.exception.UploadAbortedException r4 = new no.nordicsemi.android.dfu.internal.exception.UploadAbortedException
            r4.<init>()
            goto L_0x00bd
        L_0x00bc:
            throw r4
        L_0x00bd:
            goto L_0x00bc
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.BaseCustomDfuImpl.writeInitPacket(android.bluetooth.BluetoothGattCharacteristic, byte[], int):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void uploadFirmwareImage(android.bluetooth.BluetoothGattCharacteristic r8) throws no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException, no.nordicsemi.android.dfu.internal.exception.DfuException, no.nordicsemi.android.dfu.internal.exception.UploadAbortedException {
        /*
            r7 = this;
            boolean r0 = r7.mAborted
            if (r0 != 0) goto L_0x00a2
            r0 = 0
            r7.mReceivedData = r0
            r0 = 0
            r7.mError = r0
            r1 = 1
            r7.mFirmwareUploadInProgress = r1
            r7.mPacketsSentSinceNotification = r0
            byte[] r0 = r7.mBuffer
            r2 = 4100(0x1004, float:5.745E-42)
            java.io.InputStream r3 = r7.mFirmwareStream     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            int r3 = r3.read(r0)     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r7.mService     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            r5.<init>()     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            java.lang.String r6 = "Sending firmware to characteristic "
            r5.append(r6)     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            java.util.UUID r6 = r8.getUuid()     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            r5.append(r6)     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            java.lang.String r6 = "..."
            r5.append(r6)     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            java.lang.String r5 = r5.toString()     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            r4.sendLogBroadcast(r1, r5)     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            android.bluetooth.BluetoothGatt r1 = r7.mGatt     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            r7.writePacket(r1, r8, r0, r3)     // Catch:{ HexFileValidationException -> 0x0098, IOException -> 0x0090, NullPointerException -> 0x0080 }
            java.lang.Object r8 = r7.mLock     // Catch:{ InterruptedException -> 0x005f }
            monitor-enter(r8)     // Catch:{ InterruptedException -> 0x005f }
        L_0x0040:
            boolean r0 = r7.mFirmwareUploadInProgress     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0050
            byte[] r0 = r7.mReceivedData     // Catch:{ all -> 0x005c }
            if (r0 != 0) goto L_0x0050
            boolean r0 = r7.mConnected     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0050
            int r0 = r7.mError     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x0054
        L_0x0050:
            boolean r0 = r7.mPaused     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x005a
        L_0x0054:
            java.lang.Object r0 = r7.mLock     // Catch:{ all -> 0x005c }
            r0.wait()     // Catch:{ all -> 0x005c }
            goto L_0x0040
        L_0x005a:
            monitor-exit(r8)     // Catch:{ all -> 0x005c }
            goto L_0x0065
        L_0x005c:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x005c }
            throw r0     // Catch:{ InterruptedException -> 0x005f }
        L_0x005f:
            r8 = move-exception
            java.lang.String r0 = "Sleeping interrupted"
            r7.loge(r0, r8)
        L_0x0065:
            int r8 = r7.mError
            if (r8 != 0) goto L_0x0076
            boolean r8 = r7.mConnected
            if (r8 == 0) goto L_0x006e
            return
        L_0x006e:
            no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException r8 = new no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException
            java.lang.String r0 = "Uploading Firmware Image failed: device disconnected"
            r8.<init>(r0)
            throw r8
        L_0x0076:
            no.nordicsemi.android.dfu.internal.exception.DfuException r8 = new no.nordicsemi.android.dfu.internal.exception.DfuException
            int r0 = r7.mError
            java.lang.String r1 = "Uploading Firmware Image failed"
            r8.<init>(r1, r0)
            throw r8
        L_0x0080:
            r8 = move-exception
            java.lang.String r8 = r8.getMessage()
            r7.loge(r8)
            no.nordicsemi.android.dfu.internal.exception.DfuException r8 = new no.nordicsemi.android.dfu.internal.exception.DfuException
            java.lang.String r0 = "Error while reading file"
            r8.<init>(r0, r2)
            throw r8
        L_0x0090:
            no.nordicsemi.android.dfu.internal.exception.DfuException r8 = new no.nordicsemi.android.dfu.internal.exception.DfuException
            java.lang.String r0 = "Error while reading file"
            r8.<init>(r0, r2)
            throw r8
        L_0x0098:
            no.nordicsemi.android.dfu.internal.exception.DfuException r8 = new no.nordicsemi.android.dfu.internal.exception.DfuException
            r0 = 4099(0x1003, float:5.744E-42)
            java.lang.String r1 = "HEX file not valid"
            r8.<init>(r1, r0)
            throw r8
        L_0x00a2:
            no.nordicsemi.android.dfu.internal.exception.UploadAbortedException r8 = new no.nordicsemi.android.dfu.internal.exception.UploadAbortedException
            r8.<init>()
            goto L_0x00a9
        L_0x00a8:
            throw r8
        L_0x00a9:
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.BaseCustomDfuImpl.uploadFirmwareImage(android.bluetooth.BluetoothGattCharacteristic):void");
    }

    /* access modifiers changed from: private */
    public void writePacket(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr, int i) {
        if (i > 0) {
            if (bArr.length != i) {
                byte[] bArr2 = new byte[i];
                System.arraycopy(bArr, 0, bArr2, 0, i);
                bArr = bArr2;
            }
            bluetoothGattCharacteristic.setWriteType(1);
            bluetoothGattCharacteristic.setValue(bArr);
            bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize(Intent intent, boolean z) {
        boolean z2;
        boolean z3 = false;
        boolean booleanExtra = intent.getBooleanExtra(DfuBaseService.EXTRA_KEEP_BOND, false);
        this.mService.refreshDeviceCache(this.mGatt, z || !booleanExtra);
        this.mService.close(this.mGatt);
        if (this.mGatt.getDevice().getBondState() == 12) {
            boolean booleanExtra2 = intent.getBooleanExtra(DfuBaseService.EXTRA_RESTORE_BOND, false);
            if (booleanExtra2 || !booleanExtra) {
                removeBond();
                this.mService.waitFor(2000);
                z2 = true;
            } else {
                z2 = false;
            }
            if (!booleanExtra2 || (this.mFileType & 4) <= 0) {
                z3 = z2;
            } else {
                createBond();
            }
        }
        if (this.mProgressInfo.isLastPart()) {
            if (!z3) {
                this.mService.waitFor(1400);
            }
            this.mProgressInfo.setProgress(-6);
            return;
        }
        logi("Starting service that will upload application");
        Intent intent2 = new Intent();
        intent2.fillIn(intent, 24);
        intent2.putExtra(DfuBaseService.EXTRA_FILE_MIME_TYPE, DfuBaseService.MIME_TYPE_ZIP);
        intent2.putExtra(DfuBaseService.EXTRA_FILE_TYPE, 4);
        intent2.putExtra(DfuBaseService.EXTRA_PART_CURRENT, this.mProgressInfo.getCurrentPart() + 1);
        intent2.putExtra(DfuBaseService.EXTRA_PARTS_TOTAL, this.mProgressInfo.getTotalParts());
        restartService(intent2, true);
    }
}
