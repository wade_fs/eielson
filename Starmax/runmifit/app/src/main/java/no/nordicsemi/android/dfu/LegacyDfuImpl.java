package no.nordicsemi.android.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import java.util.UUID;
import no.nordicsemi.android.dfu.BaseCustomDfuImpl;
import no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException;
import no.nordicsemi.android.dfu.internal.exception.DfuException;
import no.nordicsemi.android.dfu.internal.exception.UnknownResponseException;
import no.nordicsemi.android.dfu.internal.exception.UploadAbortedException;

/* renamed from: no.nordicsemi.android.dfu.LegacyDfuImpl */
class LegacyDfuImpl extends BaseCustomDfuImpl {
    protected static final UUID DEFAULT_DFU_CONTROL_POINT_UUID = new UUID(23300500811742L, 1523193452336828707L);
    protected static final UUID DEFAULT_DFU_PACKET_UUID = new UUID(23304795779038L, 1523193452336828707L);
    protected static final UUID DEFAULT_DFU_SERVICE_UUID = new UUID(23296205844446L, 1523193452336828707L);
    protected static final UUID DEFAULT_DFU_VERSION_UUID = new UUID(23313385713630L, 1523193452336828707L);
    protected static UUID DFU_CONTROL_POINT_UUID = DEFAULT_DFU_CONTROL_POINT_UUID;
    protected static UUID DFU_PACKET_UUID = DEFAULT_DFU_PACKET_UUID;
    protected static UUID DFU_SERVICE_UUID = DEFAULT_DFU_SERVICE_UUID;
    private static final int DFU_STATUS_SUCCESS = 1;
    protected static UUID DFU_VERSION_UUID = DEFAULT_DFU_VERSION_UUID;
    private static final byte[] OP_CODE_ACTIVATE_AND_RESET = {5};
    private static final int OP_CODE_ACTIVATE_AND_RESET_KEY = 5;
    private static final byte[] OP_CODE_INIT_DFU_PARAMS = {2};
    private static final byte[] OP_CODE_INIT_DFU_PARAMS_COMPLETE = {2, 1};
    private static final int OP_CODE_INIT_DFU_PARAMS_KEY = 2;
    private static final byte[] OP_CODE_INIT_DFU_PARAMS_START = {2, 0};
    private static final int OP_CODE_PACKET_RECEIPT_NOTIF_KEY = 17;
    private static final byte[] OP_CODE_PACKET_RECEIPT_NOTIF_REQ = {8, 0, 0};
    private static final int OP_CODE_PACKET_RECEIPT_NOTIF_REQ_KEY = 8;
    private static final byte[] OP_CODE_RECEIVE_FIRMWARE_IMAGE = {3};
    private static final int OP_CODE_RECEIVE_FIRMWARE_IMAGE_KEY = 3;
    private static final byte[] OP_CODE_RESET = {6};
    private static final int OP_CODE_RESET_KEY = 6;
    private static final int OP_CODE_RESPONSE_CODE_KEY = 16;
    private static final byte[] OP_CODE_START_DFU = {1, 0};
    private static final int OP_CODE_START_DFU_KEY = 1;
    private static final byte[] OP_CODE_VALIDATE = {4};
    private static final int OP_CODE_VALIDATE_KEY = 4;
    private final LegacyBluetoothCallback mBluetoothCallback = new LegacyBluetoothCallback();
    private BluetoothGattCharacteristic mControlPointCharacteristic;
    /* access modifiers changed from: private */
    public boolean mImageSizeInProgress;
    private BluetoothGattCharacteristic mPacketCharacteristic;

    /* renamed from: no.nordicsemi.android.dfu.LegacyDfuImpl$LegacyBluetoothCallback */
    protected class LegacyBluetoothCallback extends BaseCustomDfuImpl.BaseCustomBluetoothCallback {
        protected LegacyBluetoothCallback() {
            super();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: no.nordicsemi.android.dfu.LegacyDfuImpl.access$002(no.nordicsemi.android.dfu.LegacyDfuImpl, boolean):boolean
         arg types: [no.nordicsemi.android.dfu.LegacyDfuImpl, int]
         candidates:
          no.nordicsemi.android.dfu.BaseCustomDfuImpl.access$002(no.nordicsemi.android.dfu.BaseCustomDfuImpl, boolean):boolean
          no.nordicsemi.android.dfu.LegacyDfuImpl.access$002(no.nordicsemi.android.dfu.LegacyDfuImpl, boolean):boolean */
        /* access modifiers changed from: protected */
        public void onPacketCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            if (LegacyDfuImpl.this.mImageSizeInProgress) {
                DfuBaseService dfuBaseService = LegacyDfuImpl.this.mService;
                dfuBaseService.sendLogBroadcast(5, "Data written to " + bluetoothGattCharacteristic.getUuid() + ", value (0x): " + parse(bluetoothGattCharacteristic));
                boolean unused = LegacyDfuImpl.this.mImageSizeInProgress = false;
            }
        }

        public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            if (bluetoothGattCharacteristic.getIntValue(17, 0).intValue() == 17) {
                LegacyDfuImpl.this.mProgressInfo.setBytesReceived(bluetoothGattCharacteristic.getIntValue(20, 1).intValue());
                handlePacketReceiptNotification(bluetoothGatt, bluetoothGattCharacteristic);
            } else if (!LegacyDfuImpl.this.mRemoteErrorOccurred) {
                if (bluetoothGattCharacteristic.getIntValue(17, 2).intValue() != 1) {
                    LegacyDfuImpl.this.mRemoteErrorOccurred = true;
                }
                handleNotification(bluetoothGatt, bluetoothGattCharacteristic);
            }
            LegacyDfuImpl.this.notifyLock();
        }
    }

    LegacyDfuImpl(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    public boolean isClientCompatible(Intent intent, BluetoothGatt bluetoothGatt) {
        BluetoothGattService service = bluetoothGatt.getService(DFU_SERVICE_UUID);
        if (service == null) {
            return false;
        }
        this.mControlPointCharacteristic = service.getCharacteristic(DFU_CONTROL_POINT_UUID);
        this.mPacketCharacteristic = service.getCharacteristic(DFU_PACKET_UUID);
        if (this.mControlPointCharacteristic == null || this.mPacketCharacteristic == null) {
            return false;
        }
        return true;
    }

    public BaseCustomDfuImpl.BaseCustomBluetoothCallback getGattCallback() {
        return this.mBluetoothCallback;
    }

    /* access modifiers changed from: protected */
    public UUID getControlPointCharacteristicUUID() {
        return DFU_CONTROL_POINT_UUID;
    }

    /* access modifiers changed from: protected */
    public UUID getPacketCharacteristicUUID() {
        return DFU_PACKET_UUID;
    }

    /* access modifiers changed from: protected */
    public UUID getDfuServiceUUID() {
        return DFU_SERVICE_UUID;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0633, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0634, code lost:
        r2 = r0;
        loge("Disconnected while sending data");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x063a, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0640, code lost:
        throw new no.nordicsemi.android.dfu.internal.exception.RemoteDfuException("Starting DFU failed", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x0641, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x0642, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0643, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0645, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0646, code lost:
        r3 = r0;
        r2 = "Sending Reset command (Op Code = 6)";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0649, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x064a, code lost:
        r3 = r0;
        r2 = "Sending Reset command (Op Code = 6)";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x064e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x064f, code lost:
        r19 = r4;
        r20 = "Sending Reset command (Op Code = 6)";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0653, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x06af, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x06b0, code lost:
        r2 = "Sending Reset command (Op Code = 6)";
        r3 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x06dd, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x06de, code lost:
        r2 = "Sending Reset command (Op Code = 6)";
        r3 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01c0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02e3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x02e4, code lost:
        r3 = r0;
        r4 = r19;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02eb, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x02ec, code lost:
        r3 = r0;
        r4 = r19;
        r2 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x02f3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x02f4, code lost:
        r4 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x02fa, code lost:
        if (r4.getErrorNumber() == 3) goto L_0x02fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x02fd, code lost:
        if (r14 == 4) goto L_0x02ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x02ff, code lost:
        r1.mRemoteErrorOccurred = false;
        logw("DFU target does not support DFU v.2");
        r1.mService.sendLogBroadcast(15, "DFU target does not support DFU v.2");
        r1.mService.sendLogBroadcast(1, "Switching to DFU v.1");
        logi("Resending Start DFU command (Op Code = 1)");
        writeOpCode(r1.mControlPointCharacteristic, no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_START_DFU);
        r1.mService.sendLogBroadcast(10, "DFU Start sent (Op Code = 1)");
        logi("Sending application image size to DFU Packet: " + r1.mImageSizeInBytes + " bytes");
        writeImageSize(r1.mPacketCharacteristic, r1.mImageSizeInBytes);
        r4 = r1.mService;
        r4.sendLogBroadcast(10, "Firmware image size sent (" + r1.mImageSizeInBytes + " bytes)");
        r4 = readNotificationResponse();
        r8 = getStatusCode(r4, 1);
        r11 = r1.mService;
        r11.sendLogBroadcast(10, "Response received (Op Code = " + ((int) r4[1]) + ", Status = " + r8 + ")");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0399, code lost:
        if (r8 == 2) goto L_0x039b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x039b, code lost:
        resetAndRestart(r10, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x039e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x03a0, code lost:
        if (r8 == 1) goto L_0x03a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x03a2, code lost:
        r3 = false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x03a7 A[Catch:{ DeviceDisconnectedException -> 0x0633, RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x045b A[Catch:{ DeviceDisconnectedException -> 0x0633, RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x046d A[Catch:{ DeviceDisconnectedException -> 0x0633, RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x053b A[Catch:{ DeviceDisconnectedException -> 0x0633, RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x062b A[Catch:{ DeviceDisconnectedException -> 0x0633, RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x0645 A[ExcHandler: UnknownResponseException (r0v5 'e' no.nordicsemi.android.dfu.internal.exception.UnknownResponseException A[CUSTOM_DECLARE]), Splitter:B:9:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0649 A[ExcHandler: UploadAbortedException (r0v4 'e' no.nordicsemi.android.dfu.internal.exception.UploadAbortedException A[CUSTOM_DECLARE]), Splitter:B:9:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x064e A[ExcHandler: RemoteDfuException (e no.nordicsemi.android.dfu.internal.exception.RemoteDfuException), Splitter:B:7:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01dd A[Catch:{ DeviceDisconnectedException -> 0x0633, RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x02e2 A[Catch:{ DeviceDisconnectedException -> 0x0633, RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x02e3 A[ExcHandler: UnknownResponseException (r0v9 'e' no.nordicsemi.android.dfu.internal.exception.UnknownResponseException A[CUSTOM_DECLARE]), PHI: r20 10  PHI: (r20v6 java.lang.String) = (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String) binds: [B:122:0x04cc, B:124:0x04dd, B:64:0x01d6, B:87:0x02f5, B:41:0x00f7, B:42:?, B:44:0x0119] A[DONT_GENERATE, DONT_INLINE], Splitter:B:41:0x00f7] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x02eb A[ExcHandler: UploadAbortedException (r0v8 'e' no.nordicsemi.android.dfu.internal.exception.UploadAbortedException A[CUSTOM_DECLARE]), PHI: r20 10  PHI: (r20v5 java.lang.String) = (r20v4 java.lang.String), (r20v4 java.lang.String), (r20v8 java.lang.String), (r20v8 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String), (r20v12 java.lang.String) binds: [B:122:0x04cc, B:124:0x04dd, B:64:0x01d6, B:87:0x02f5, B:41:0x00f7, B:42:?, B:44:0x0119] A[DONT_GENERATE, DONT_INLINE], Splitter:B:41:0x00f7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void performDfu(android.content.Intent r23) throws no.nordicsemi.android.dfu.internal.exception.DfuException, no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException, no.nordicsemi.android.dfu.internal.exception.UploadAbortedException {
        /*
            r22 = this;
            r1 = r22
            r2 = r23
            java.lang.String r3 = "Starting DFU failed"
            java.lang.String r4 = "Reset request sent"
            java.lang.String r5 = "Sending Reset command (Op Code = 6)"
            java.lang.String r6 = ", Status = "
            java.lang.String r7 = "Response received (Op Code = "
            java.lang.String r8 = "b, "
            java.lang.String r9 = ")"
            java.lang.String r10 = "Legacy DFU bootloader found"
            r1.logw(r10)
            no.nordicsemi.android.dfu.DfuProgressInfo r10 = r1.mProgressInfo
            r11 = -2
            r10.setProgress(r11)
            no.nordicsemi.android.dfu.DfuBaseService r10 = r1.mService
            r11 = 1000(0x3e8, float:1.401E-42)
            r10.waitFor(r11)
            android.bluetooth.BluetoothGatt r10 = r1.mGatt
            java.util.UUID r11 = no.nordicsemi.android.dfu.LegacyDfuImpl.DFU_SERVICE_UUID
            android.bluetooth.BluetoothGattService r11 = r10.getService(r11)
            java.util.UUID r12 = no.nordicsemi.android.dfu.LegacyDfuImpl.DFU_VERSION_UUID
            android.bluetooth.BluetoothGattCharacteristic r11 = r11.getCharacteristic(r12)
            int r11 = r1.readVersion(r11)
            r12 = 5
            r13 = 20
            if (r11 < r12) goto L_0x0062
            java.io.InputStream r14 = r1.mInitPacketStream
            if (r14 != 0) goto L_0x0062
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Init packet not set for the DFU Bootloader version "
            r2.append(r3)
            r2.append(r11)
            java.lang.String r2 = r2.toString()
            r1.logw(r2)
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService
            java.lang.String r3 = "The Init packet is required by this version DFU Bootloader"
            r2.sendLogBroadcast(r13, r3)
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService
            r3 = 4107(0x100b, float:5.755E-42)
            r2.terminateConnection(r10, r3)
            return
        L_0x0062:
            r15 = 1
            r12 = 10
            android.bluetooth.BluetoothGattCharacteristic r14 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x06dd, UnknownResponseException -> 0x06af, RemoteDfuException -> 0x064e }
            r1.enableCCCD(r14, r15)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x064e }
            no.nordicsemi.android.dfu.DfuBaseService r14 = r1.mService     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x064e }
            java.lang.String r15 = "Notifications enabled"
            r14.sendLogBroadcast(r12, r15)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x064e }
            no.nordicsemi.android.dfu.DfuBaseService r14 = r1.mService     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x064e }
            r15 = 1000(0x3e8, float:1.401E-42)
            r14.waitFor(r15)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x064e }
            int r14 = r1.mFileType     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x064e }
            r15 = r14 & 1
            if (r15 <= 0) goto L_0x0089
            int r15 = r1.mImageSizeInBytes     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            goto L_0x008a
        L_0x0081:
            r0 = move-exception
            r2 = r0
            r19 = r4
            r20 = r5
            goto L_0x0654
        L_0x0089:
            r15 = 0
        L_0x008a:
            r17 = r14 & 2
            if (r17 <= 0) goto L_0x0091
            int r12 = r1.mImageSizeInBytes     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            goto L_0x0092
        L_0x0091:
            r12 = 0
        L_0x0092:
            r18 = r14 & 4
            if (r18 <= 0) goto L_0x009b
            int r13 = r1.mImageSizeInBytes     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            r19 = r12
            goto L_0x009e
        L_0x009b:
            r19 = r12
            r13 = 0
        L_0x009e:
            java.io.InputStream r12 = r1.mFirmwareStream     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x064e }
            boolean r12 = r12 instanceof no.nordicsemi.android.dfu.internal.ArchiveInputStream     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x064e }
            if (r12 == 0) goto L_0x00e2
            java.io.InputStream r12 = r1.mFirmwareStream     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            no.nordicsemi.android.dfu.internal.ArchiveInputStream r12 = (no.nordicsemi.android.dfu.internal.ArchiveInputStream) r12     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            boolean r13 = r12.isSecureDfuRequired()     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            if (r13 == 0) goto L_0x00d5
            java.lang.String r2 = "Secure DFU is required to upload selected firmware"
            r1.loge(r2)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            java.lang.String r3 = "The device does not support given firmware."
            r6 = 20
            r2.sendLogBroadcast(r6, r3)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            r1.logi(r5)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            android.bluetooth.BluetoothGattCharacteristic r2 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            byte[] r3 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_RESET     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            r1.writeOpCode(r2, r3)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            r3 = 10
            r2.sendLogBroadcast(r3, r4)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            r3 = 4099(0x1003, float:5.744E-42)
            r2.terminateConnection(r10, r3)     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            return
        L_0x00d5:
            int r15 = r12.softDeviceImageSize()     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            int r13 = r12.bootloaderImageSize()     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            int r12 = r12.applicationImageSize()     // Catch:{ UploadAbortedException -> 0x0649, UnknownResponseException -> 0x0645, RemoteDfuException -> 0x0081 }
            goto L_0x00e5
        L_0x00e2:
            r12 = r13
            r13 = r19
        L_0x00e5:
            r19 = r4
            byte[] r20 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_START_DFU     // Catch:{ RemoteDfuException -> 0x01d0, UploadAbortedException -> 0x01c9, UnknownResponseException -> 0x01c2 }
            byte r4 = (byte) r14     // Catch:{ RemoteDfuException -> 0x01d0, UploadAbortedException -> 0x01c9, UnknownResponseException -> 0x01c2 }
            r16 = 1
            r20[r16] = r4     // Catch:{ RemoteDfuException -> 0x01d0, UploadAbortedException -> 0x01c9, UnknownResponseException -> 0x01c2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x01d0, UploadAbortedException -> 0x01c9, UnknownResponseException -> 0x01c2 }
            r4.<init>()     // Catch:{ RemoteDfuException -> 0x01d0, UploadAbortedException -> 0x01c9, UnknownResponseException -> 0x01c2 }
            r20 = r5
            java.lang.String r5 = "Sending Start DFU command (Op Code = 1, Upload Mode = "
            r4.append(r5)     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r14)     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r9)     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = r4.toString()     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r1.logi(r4)     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mControlPointCharacteristic     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            byte[] r5 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_START_DFU     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r1.writeOpCode(r4, r5)     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.<init>()     // Catch:{ RemoteDfuException -> 0x01c0, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r21 = r11
            java.lang.String r11 = "DFU Start sent (Op Code = 1, Upload Mode = "
            r5.append(r11)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r14)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r9)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = r5.toString()     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r11 = 10
            r4.sendLogBroadcast(r11, r5)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.<init>()     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = "Sending image size array to DFU Packet ("
            r4.append(r5)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r15)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r8)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r13)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r8)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r12)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = "b)"
            r4.append(r5)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = r4.toString()     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r1.logi(r4)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mPacketCharacteristic     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r1.writeImageSize(r4, r15, r13, r12)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.<init>()     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r11 = "Firmware image size sent ("
            r5.append(r11)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r15)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r8)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r13)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r8)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r12)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r11 = "b)"
            r5.append(r11)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = r5.toString()     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r11 = 10
            r4.sendLogBroadcast(r11, r5)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            byte[] r4 = r22.readNotificationResponse()     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5 = 1
            int r11 = r1.getStatusCode(r4, r5)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuBaseService r12 = r1.mService     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.<init>()     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r7)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r16 = 1
            byte r4 = r4[r16]     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r4)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = " Status = "
            r5.append(r4)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r11)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r9)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = r5.toString()     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5 = 10
            r12.sendLogBroadcast(r5, r4)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4 = 2
            if (r11 != r4) goto L_0x01b3
            r1.resetAndRestart(r10, r2)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            return
        L_0x01b3:
            r4 = 1
            if (r11 != r4) goto L_0x01b8
            goto L_0x02d8
        L_0x01b8:
            no.nordicsemi.android.dfu.internal.exception.RemoteDfuException r4 = new no.nordicsemi.android.dfu.internal.exception.RemoteDfuException     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.<init>(r3, r11)     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            throw r4     // Catch:{ RemoteDfuException -> 0x01be, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
        L_0x01be:
            r0 = move-exception
            goto L_0x01d5
        L_0x01c0:
            r0 = move-exception
            goto L_0x01d3
        L_0x01c2:
            r0 = move-exception
            r3 = r0
            r2 = r5
            r4 = r19
            goto L_0x06b2
        L_0x01c9:
            r0 = move-exception
            r3 = r0
            r2 = r5
            r4 = r19
            goto L_0x06e0
        L_0x01d0:
            r0 = move-exception
            r20 = r5
        L_0x01d3:
            r21 = r11
        L_0x01d5:
            r4 = r0
            int r5 = r4.getErrorNumber()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r11 = 3
            if (r5 != r11) goto L_0x02e2
            if (r18 <= 0) goto L_0x02e1
            r5 = r14 & 3
            if (r5 <= 0) goto L_0x02e1
            r5 = 0
            r1.mRemoteErrorOccurred = r5     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = "DFU target does not support (SD/BL)+App update"
            r1.logw(r4)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5 = 15
            java.lang.String r11 = "DFU target does not support (SD/BL)+App update"
            r4.sendLogBroadcast(r5, r11)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r14 = r14 & -5
            r1.mFileType = r14     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            byte[] r4 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_START_DFU     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            byte r5 = (byte) r14     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r11 = 1
            r4[r11] = r5     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuProgressInfo r4 = r1.mProgressInfo     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5 = 2
            r4.setTotalPart(r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.io.InputStream r4 = r1.mFirmwareStream     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.internal.ArchiveInputStream r4 = (no.nordicsemi.android.dfu.internal.ArchiveInputStream) r4     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.setContentType(r14)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = "Sending only SD/BL"
            r11 = 1
            r4.sendLogBroadcast(r11, r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.<init>()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = "Resending Start DFU command (Op Code = 1, Upload Mode = "
            r4.append(r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r14)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r9)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = r4.toString()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r1.logi(r4)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mControlPointCharacteristic     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            byte[] r5 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_START_DFU     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r1.writeOpCode(r4, r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.<init>()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r11 = "DFU Start sent (Op Code = 1, Upload Mode = "
            r5.append(r11)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r14)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r9)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = r5.toString()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r11 = 10
            r4.sendLogBroadcast(r11, r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.<init>()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = "Sending image size array to DFU Packet: ["
            r4.append(r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r15)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r8)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r13)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.append(r8)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5 = 0
            r4.append(r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = "b]"
            r4.append(r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = r4.toString()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r1.logi(r4)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mPacketCharacteristic     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5 = 0
            r1.writeImageSize(r4, r15, r13, r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.<init>()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r11 = "Firmware image size sent ["
            r5.append(r11)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r15)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r8)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r13)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5.append(r8)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r8 = 0
            r5.append(r8)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r8 = "b]"
            r5.append(r8)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r5 = r5.toString()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r8 = 10
            r4.sendLogBroadcast(r8, r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            byte[] r4 = r22.readNotificationResponse()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5 = 1
            int r8 = r1.getStatusCode(r4, r5)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            no.nordicsemi.android.dfu.DfuBaseService r11 = r1.mService     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r12.<init>()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r12.append(r7)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            byte r4 = r4[r5]     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r12.append(r4)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = " Status = "
            r12.append(r4)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r12.append(r8)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r12.append(r9)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            java.lang.String r4 = r12.toString()     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r5 = 10
            r11.sendLogBroadcast(r5, r4)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4 = 2
            if (r8 != r4) goto L_0x02d5
            r1.resetAndRestart(r10, r2)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            return
        L_0x02d5:
            r4 = 1
            if (r8 != r4) goto L_0x02db
        L_0x02d8:
            r3 = 1
            goto L_0x03a3
        L_0x02db:
            no.nordicsemi.android.dfu.internal.exception.RemoteDfuException r4 = new no.nordicsemi.android.dfu.internal.exception.RemoteDfuException     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            r4.<init>(r3, r8)     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
            throw r4     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
        L_0x02e1:
            throw r4     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
        L_0x02e2:
            throw r4     // Catch:{ RemoteDfuException -> 0x02f3, UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3 }
        L_0x02e3:
            r0 = move-exception
            r3 = r0
            r4 = r19
            r2 = r20
            goto L_0x06b2
        L_0x02eb:
            r0 = move-exception
            r3 = r0
            r4 = r19
            r2 = r20
            goto L_0x06e0
        L_0x02f3:
            r0 = move-exception
            r4 = r0
            int r5 = r4.getErrorNumber()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8 = 3
            if (r5 != r8) goto L_0x0642
            r5 = 4
            if (r14 != r5) goto L_0x0641
            r5 = 0
            r1.mRemoteErrorOccurred = r5     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = "DFU target does not support DFU v.2"
            r1.logw(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 15
            java.lang.String r8 = "DFU target does not support DFU v.2"
            r4.sendLogBroadcast(r5, r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Switching to DFU v.1"
            r8 = 1
            r4.sendLogBroadcast(r8, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = "Resending Start DFU command (Op Code = 1)"
            r1.logi(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r5 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_START_DFU     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeOpCode(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "DFU Start sent (Op Code = 1)"
            r8 = 10
            r4.sendLogBroadcast(r8, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Sending application image size to DFU Packet: "
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            int r5 = r1.mImageSizeInBytes     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = " bytes"
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.logi(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mPacketCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            int r5 = r1.mImageSizeInBytes     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeImageSize(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r8 = "Firmware image size sent ("
            r5.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            int r8 = r1.mImageSizeInBytes     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r8 = " bytes)"
            r5.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = r5.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8 = 10
            r4.sendLogBroadcast(r8, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r4 = r22.readNotificationResponse()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 1
            int r8 = r1.getStatusCode(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r11 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r12.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r12.append(r7)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte r4 = r4[r5]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r12.append(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r12.append(r6)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r12.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r12.append(r9)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = r12.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 10
            r11.sendLogBroadcast(r5, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4 = 2
            if (r8 != r4) goto L_0x039f
            r1.resetAndRestart(r10, r2)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            return
        L_0x039f:
            r4 = 1
            if (r8 != r4) goto L_0x063b
            r3 = 0
        L_0x03a3:
            java.io.InputStream r4 = r1.mInitPacketStream     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            if (r4 == 0) goto L_0x0459
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Writing Initialize DFU Parameters..."
            r8 = 10
            r4.sendLogBroadcast(r8, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            if (r3 == 0) goto L_0x03f5
            java.lang.String r4 = "Sending the Initialize DFU Parameters START (Op Code = 2, Value = 0)"
            r1.logi(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r5 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_INIT_DFU_PARAMS_START     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeOpCode(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Sending "
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            int r5 = r1.mInitPacketSizeInBytes     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = " bytes of init packet"
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.logi(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mPacketCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 0
            r1.writeInitData(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = "Sending the Initialize DFU Parameters COMPLETE (Op Code = 2, Value = 1)"
            r1.logi(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r5 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_INIT_DFU_PARAMS_COMPLETE     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeOpCode(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r4 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Initialize DFU Parameters completed"
            r8 = 10
            r4.sendLogBroadcast(r8, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            goto L_0x0422
        L_0x03f5:
            java.lang.String r4 = "Sending the Initialize DFU Parameters (Op Code = 2)"
            r1.logi(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r5 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_INIT_DFU_PARAMS     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeOpCode(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Sending "
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            int r5 = r1.mInitPacketSizeInBytes     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = " bytes of init packet"
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.logi(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r4 = r1.mPacketCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 0
            r1.writeInitData(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x0422:
            byte[] r4 = r22.readNotificationResponse()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 2
            int r8 = r1.getStatusCode(r4, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r5 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r11.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r11.append(r7)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r12 = 1
            byte r4 = r4[r12]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r11.append(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r11.append(r6)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r11.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r11.append(r9)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = r11.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r11 = 10
            r5.sendLogBroadcast(r11, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4 = 1
            if (r8 != r4) goto L_0x0451
            goto L_0x0459
        L_0x0451:
            no.nordicsemi.android.dfu.internal.exception.RemoteDfuException r2 = new no.nordicsemi.android.dfu.internal.exception.RemoteDfuException     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = "Device returned error after sending init packet"
            r2.<init>(r3, r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            throw r2     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x0459:
            if (r3 != 0) goto L_0x0469
            int r3 = r1.mPacketsBeforeNotification     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            if (r3 <= 0) goto L_0x0466
            int r3 = r1.mPacketsBeforeNotification     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4 = 10
            if (r3 > r4) goto L_0x0466
            goto L_0x0469
        L_0x0466:
            r12 = 10
            goto L_0x046b
        L_0x0469:
            int r12 = r1.mPacketsBeforeNotification     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x046b:
            if (r12 <= 0) goto L_0x04ab
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r3.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = "Sending the number of packets before notifications (Op Code = 8, Value = "
            r3.append(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r3.append(r12)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r3.append(r9)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = r3.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.logi(r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r3 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_PACKET_RECEIPT_NOTIF_REQ     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.setNumberOfPackets(r3, r12)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r3 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r4 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_PACKET_RECEIPT_NOTIF_REQ     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeOpCode(r3, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r3 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Packet Receipt Notif Req (Op Code = 8) sent (Value = "
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.append(r12)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.append(r9)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 10
            r3.sendLogBroadcast(r5, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x04ab:
            java.lang.String r3 = "Sending Receive Firmware Image request (Op Code = 3)"
            r1.logi(r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r3 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r4 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_RECEIVE_FIRMWARE_IMAGE     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeOpCode(r3, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r3 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = "Receive Firmware Image request sent"
            r5 = 10
            r3.sendLogBroadcast(r5, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            long r3 = android.os.SystemClock.elapsedRealtime()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuProgressInfo r5 = r1.mProgressInfo     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8 = 0
            r5.setBytesSent(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Uploading firmware..."
            r1.logi(r5)     // Catch:{ DeviceDisconnectedException -> 0x0633 }
            no.nordicsemi.android.dfu.DfuBaseService r5 = r1.mService     // Catch:{ DeviceDisconnectedException -> 0x0633 }
            java.lang.String r8 = "Uploading firmware..."
            r11 = 10
            r5.sendLogBroadcast(r11, r8)     // Catch:{ DeviceDisconnectedException -> 0x0633 }
            android.bluetooth.BluetoothGattCharacteristic r5 = r1.mPacketCharacteristic     // Catch:{ DeviceDisconnectedException -> 0x0633 }
            r1.uploadFirmwareImage(r5)     // Catch:{ DeviceDisconnectedException -> 0x0633 }
            long r11 = android.os.SystemClock.elapsedRealtime()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r5 = r22.readNotificationResponse()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8 = 3
            int r8 = r1.getStatusCode(r5, r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r13.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r13.append(r7)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14 = 0
            byte r15 = r5[r14]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r13.append(r15)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r14 = ", Req Op Code = "
            r13.append(r14)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14 = 1
            byte r15 = r5[r14]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r13.append(r15)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r13.append(r6)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14 = 2
            byte r15 = r5[r14]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r13.append(r15)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r13.append(r9)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r13 = r13.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.logi(r13)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r13 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14.append(r7)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r15 = 1
            byte r5 = r5[r15]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14.append(r6)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14.append(r9)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = r14.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r14 = 10
            r13.sendLogBroadcast(r14, r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 1
            if (r8 != r5) goto L_0x062b
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r8 = "Transfer of "
            r5.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuProgressInfo r8 = r1.mProgressInfo     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            int r8 = r8.getBytesSent()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r8 = " bytes has taken "
            r5.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            long r11 = r11 - r3
            r5.append(r11)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = " ms"
            r5.append(r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = r5.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.logi(r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r3 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = "Upload completed in "
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4.append(r11)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = " ms"
            r4.append(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = r4.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5 = 10
            r3.sendLogBroadcast(r5, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = "Sending Validate request (Op Code = 4)"
            r1.logi(r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r3 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r4 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_VALIDATE     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeOpCode(r3, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r3 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = "Validate request sent"
            r5 = 10
            r3.sendLogBroadcast(r5, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r3 = r22.readNotificationResponse()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4 = 4
            int r4 = r1.getStatusCode(r3, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.append(r7)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8 = 0
            byte r11 = r3[r8]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.append(r11)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r8 = ", Req Op Code = "
            r5.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8 = 1
            byte r11 = r3[r8]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.append(r11)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.append(r6)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8 = 2
            byte r8 = r3[r8]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.append(r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r5.append(r9)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r5 = r5.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.logi(r5)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r5 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8.<init>()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8.append(r7)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r7 = 1
            byte r3 = r3[r7]     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8.append(r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8.append(r6)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8.append(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r8.append(r9)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = r8.toString()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r6 = 10
            r5.sendLogBroadcast(r6, r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r3 = 1
            if (r4 != r3) goto L_0x0623
            no.nordicsemi.android.dfu.DfuProgressInfo r3 = r1.mProgressInfo     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r4 = -5
            r3.setProgress(r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = "Sending Activate and Reset request (Op Code = 5)"
            r1.logi(r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            android.bluetooth.BluetoothGattCharacteristic r3 = r1.mControlPointCharacteristic     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            byte[] r4 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_ACTIVATE_AND_RESET     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r1.writeOpCode(r3, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r3 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = "Activate and Reset request sent"
            r5 = 10
            r3.sendLogBroadcast(r5, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r3 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r3.waitUntilDisconnected()     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            no.nordicsemi.android.dfu.DfuBaseService r3 = r1.mService     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r4 = "Disconnected by the remote device"
            r5 = 5
            r3.sendLogBroadcast(r5, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r3 = r21
            if (r3 != r5) goto L_0x061d
            r3 = 1
            goto L_0x061e
        L_0x061d:
            r3 = 0
        L_0x061e:
            r1.finalize(r2, r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            goto L_0x06dc
        L_0x0623:
            no.nordicsemi.android.dfu.internal.exception.RemoteDfuException r2 = new no.nordicsemi.android.dfu.internal.exception.RemoteDfuException     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = "Device returned validation error"
            r2.<init>(r3, r4)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            throw r2     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x062b:
            no.nordicsemi.android.dfu.internal.exception.RemoteDfuException r2 = new no.nordicsemi.android.dfu.internal.exception.RemoteDfuException     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            java.lang.String r3 = "Device returned error after sending file"
            r2.<init>(r3, r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            throw r2     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x0633:
            r0 = move-exception
            r2 = r0
            java.lang.String r3 = "Disconnected while sending data"
            r1.loge(r3)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            throw r2     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x063b:
            no.nordicsemi.android.dfu.internal.exception.RemoteDfuException r2 = new no.nordicsemi.android.dfu.internal.exception.RemoteDfuException     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            r2.<init>(r3, r8)     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
            throw r2     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x0641:
            throw r4     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x0642:
            throw r4     // Catch:{ UploadAbortedException -> 0x02eb, UnknownResponseException -> 0x02e3, RemoteDfuException -> 0x0643 }
        L_0x0643:
            r0 = move-exception
            goto L_0x0653
        L_0x0645:
            r0 = move-exception
            r3 = r0
            r2 = r5
            goto L_0x06b2
        L_0x0649:
            r0 = move-exception
            r3 = r0
            r2 = r5
            goto L_0x06e0
        L_0x064e:
            r0 = move-exception
            r19 = r4
            r20 = r5
        L_0x0653:
            r2 = r0
        L_0x0654:
            int r3 = r2.getErrorNumber()
            r3 = r3 | 256(0x100, float:3.59E-43)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r2 = r2.getMessage()
            r4.append(r2)
            java.lang.String r2 = ": "
            r4.append(r2)
            java.lang.String r2 = no.nordicsemi.android.error.LegacyDfuError.parse(r3)
            r4.append(r2)
            java.lang.String r2 = r4.toString()
            r1.loge(r2)
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService
            java.util.Locale r4 = java.util.Locale.US
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.String r6 = no.nordicsemi.android.error.LegacyDfuError.parse(r3)
            r7 = 0
            r5[r7] = r6
            java.lang.String r6 = "Remote DFU error: %s"
            java.lang.String r4 = java.lang.String.format(r4, r6, r5)
            r5 = 20
            r2.sendLogBroadcast(r5, r4)
            r2 = r20
            r1.logi(r2)
            android.bluetooth.BluetoothGattCharacteristic r2 = r1.mControlPointCharacteristic
            byte[] r4 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_RESET
            r1.writeOpCode(r2, r4)
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService
            r4 = r19
            r5 = 10
            r2.sendLogBroadcast(r5, r4)
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService
            r3 = r3 | 8192(0x2000, float:1.14794E-41)
            r2.terminateConnection(r10, r3)
            goto L_0x06dc
        L_0x06af:
            r0 = move-exception
            r2 = r5
            r3 = r0
        L_0x06b2:
            java.lang.String r5 = r3.getMessage()
            r1.loge(r5)
            no.nordicsemi.android.dfu.DfuBaseService r5 = r1.mService
            java.lang.String r3 = r3.getMessage()
            r6 = 20
            r5.sendLogBroadcast(r6, r3)
            r1.logi(r2)
            android.bluetooth.BluetoothGattCharacteristic r2 = r1.mControlPointCharacteristic
            byte[] r3 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_RESET
            r1.writeOpCode(r2, r3)
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService
            r3 = 10
            r2.sendLogBroadcast(r3, r4)
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService
            r3 = 4104(0x1008, float:5.751E-42)
            r2.terminateConnection(r10, r3)
        L_0x06dc:
            return
        L_0x06dd:
            r0 = move-exception
            r2 = r5
            r3 = r0
        L_0x06e0:
            r1.logi(r2)
            r2 = 0
            r1.mAborted = r2
            android.bluetooth.BluetoothGattCharacteristic r2 = r1.mControlPointCharacteristic
            byte[] r5 = no.nordicsemi.android.dfu.LegacyDfuImpl.OP_CODE_RESET
            r1.writeOpCode(r2, r5)
            no.nordicsemi.android.dfu.DfuBaseService r2 = r1.mService
            r5 = 10
            r2.sendLogBroadcast(r5, r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.LegacyDfuImpl.performDfu(android.content.Intent):void");
    }

    private void setNumberOfPackets(byte[] bArr, int i) {
        bArr[1] = (byte) (i & 255);
        bArr[2] = (byte) ((i >> 8) & 255);
    }

    private int getStatusCode(byte[] bArr, int i) throws UnknownResponseException {
        if (bArr != null && bArr.length == 3 && bArr[0] == 16 && bArr[1] == i && bArr[2] >= 1 && bArr[2] <= 6) {
            return bArr[2];
        }
        throw new UnknownResponseException("Invalid response received", bArr, 16, i);
    }

    private int readVersion(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        if (bluetoothGattCharacteristic != null) {
            return bluetoothGattCharacteristic.getIntValue(18, 0).intValue();
        }
        return 0;
    }

    private void writeOpCode(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr) throws DeviceDisconnectedException, DfuException, UploadAbortedException {
        boolean z = false;
        if (bArr[0] == 6 || bArr[0] == 5) {
            z = true;
        }
        writeOpCode(bluetoothGattCharacteristic, bArr, z);
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void writeImageSize(android.bluetooth.BluetoothGattCharacteristic r5, int r6) throws no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException, no.nordicsemi.android.dfu.internal.exception.DfuException, no.nordicsemi.android.dfu.internal.exception.UploadAbortedException {
        /*
            r4 = this;
            r0 = 0
            r4.mReceivedData = r0
            r0 = 0
            r4.mError = r0
            r1 = 1
            r4.mImageSizeInProgress = r1
            r5.setWriteType(r1)
            r2 = 4
            byte[] r2 = new byte[r2]
            r5.setValue(r2)
            r2 = 20
            r5.setValue(r6, r2, r0)
            no.nordicsemi.android.dfu.DfuBaseService r6 = r4.mService
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Writing to characteristic "
            r2.append(r3)
            java.util.UUID r3 = r5.getUuid()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r6.sendLogBroadcast(r1, r2)
            no.nordicsemi.android.dfu.DfuBaseService r6 = r4.mService
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "gatt.writeCharacteristic("
            r1.append(r2)
            java.util.UUID r2 = r5.getUuid()
            r1.append(r2)
            java.lang.String r2 = ")"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r6.sendLogBroadcast(r0, r1)
            android.bluetooth.BluetoothGatt r6 = r4.mGatt
            r6.writeCharacteristic(r5)
            java.lang.Object r5 = r4.mLock     // Catch:{ InterruptedException -> 0x0077 }
            monitor-enter(r5)     // Catch:{ InterruptedException -> 0x0077 }
        L_0x0058:
            boolean r6 = r4.mImageSizeInProgress     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x0068
            boolean r6 = r4.mConnected     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x0068
            int r6 = r4.mError     // Catch:{ all -> 0x0074 }
            if (r6 != 0) goto L_0x0068
            boolean r6 = r4.mAborted     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x006c
        L_0x0068:
            boolean r6 = r4.mPaused     // Catch:{ all -> 0x0074 }
            if (r6 == 0) goto L_0x0072
        L_0x006c:
            java.lang.Object r6 = r4.mLock     // Catch:{ all -> 0x0074 }
            r6.wait()     // Catch:{ all -> 0x0074 }
            goto L_0x0058
        L_0x0072:
            monitor-exit(r5)     // Catch:{ all -> 0x0074 }
            goto L_0x007d
        L_0x0074:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0074 }
            throw r6     // Catch:{ InterruptedException -> 0x0077 }
        L_0x0077:
            r5 = move-exception
            java.lang.String r6 = "Sleeping interrupted"
            r4.loge(r6, r5)
        L_0x007d:
            boolean r5 = r4.mAborted
            if (r5 != 0) goto L_0x009c
            int r5 = r4.mError
            if (r5 != 0) goto L_0x0092
            boolean r5 = r4.mConnected
            if (r5 == 0) goto L_0x008a
            return
        L_0x008a:
            no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException r5 = new no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException
            java.lang.String r6 = "Unable to write Image Size: device disconnected"
            r5.<init>(r6)
            throw r5
        L_0x0092:
            no.nordicsemi.android.dfu.internal.exception.DfuException r5 = new no.nordicsemi.android.dfu.internal.exception.DfuException
            int r6 = r4.mError
            java.lang.String r0 = "Unable to write Image Size"
            r5.<init>(r0, r6)
            throw r5
        L_0x009c:
            no.nordicsemi.android.dfu.internal.exception.UploadAbortedException r5 = new no.nordicsemi.android.dfu.internal.exception.UploadAbortedException
            r5.<init>()
            goto L_0x00a3
        L_0x00a2:
            throw r5
        L_0x00a3:
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.LegacyDfuImpl.writeImageSize(android.bluetooth.BluetoothGattCharacteristic, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void writeImageSize(android.bluetooth.BluetoothGattCharacteristic r4, int r5, int r6, int r7) throws no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException, no.nordicsemi.android.dfu.internal.exception.DfuException, no.nordicsemi.android.dfu.internal.exception.UploadAbortedException {
        /*
            r3 = this;
            r0 = 0
            r3.mReceivedData = r0
            r0 = 0
            r3.mError = r0
            r1 = 1
            r3.mImageSizeInProgress = r1
            r4.setWriteType(r1)
            r2 = 12
            byte[] r2 = new byte[r2]
            r4.setValue(r2)
            r2 = 20
            r4.setValue(r5, r2, r0)
            r5 = 4
            r4.setValue(r6, r2, r5)
            r5 = 8
            r4.setValue(r7, r2, r5)
            no.nordicsemi.android.dfu.DfuBaseService r5 = r3.mService
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Writing to characteristic "
            r6.append(r7)
            java.util.UUID r7 = r4.getUuid()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.sendLogBroadcast(r1, r6)
            no.nordicsemi.android.dfu.DfuBaseService r5 = r3.mService
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "gatt.writeCharacteristic("
            r6.append(r7)
            java.util.UUID r7 = r4.getUuid()
            r6.append(r7)
            java.lang.String r7 = ")"
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.sendLogBroadcast(r0, r6)
            android.bluetooth.BluetoothGatt r5 = r3.mGatt
            r5.writeCharacteristic(r4)
            java.lang.Object r4 = r3.mLock     // Catch:{ InterruptedException -> 0x0081 }
            monitor-enter(r4)     // Catch:{ InterruptedException -> 0x0081 }
        L_0x0062:
            boolean r5 = r3.mImageSizeInProgress     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x0072
            boolean r5 = r3.mConnected     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x0072
            int r5 = r3.mError     // Catch:{ all -> 0x007e }
            if (r5 != 0) goto L_0x0072
            boolean r5 = r3.mAborted     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x0076
        L_0x0072:
            boolean r5 = r3.mPaused     // Catch:{ all -> 0x007e }
            if (r5 == 0) goto L_0x007c
        L_0x0076:
            java.lang.Object r5 = r3.mLock     // Catch:{ all -> 0x007e }
            r5.wait()     // Catch:{ all -> 0x007e }
            goto L_0x0062
        L_0x007c:
            monitor-exit(r4)     // Catch:{ all -> 0x007e }
            goto L_0x0087
        L_0x007e:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x007e }
            throw r5     // Catch:{ InterruptedException -> 0x0081 }
        L_0x0081:
            r4 = move-exception
            java.lang.String r5 = "Sleeping interrupted"
            r3.loge(r5, r4)
        L_0x0087:
            boolean r4 = r3.mAborted
            if (r4 != 0) goto L_0x00a6
            int r4 = r3.mError
            if (r4 != 0) goto L_0x009c
            boolean r4 = r3.mConnected
            if (r4 == 0) goto L_0x0094
            return
        L_0x0094:
            no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException r4 = new no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException
            java.lang.String r5 = "Unable to write Image Sizes: device disconnected"
            r4.<init>(r5)
            throw r4
        L_0x009c:
            no.nordicsemi.android.dfu.internal.exception.DfuException r4 = new no.nordicsemi.android.dfu.internal.exception.DfuException
            int r5 = r3.mError
            java.lang.String r6 = "Unable to write Image Sizes"
            r4.<init>(r6, r5)
            throw r4
        L_0x00a6:
            no.nordicsemi.android.dfu.internal.exception.UploadAbortedException r4 = new no.nordicsemi.android.dfu.internal.exception.UploadAbortedException
            r4.<init>()
            goto L_0x00ad
        L_0x00ac:
            throw r4
        L_0x00ad:
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.LegacyDfuImpl.writeImageSize(android.bluetooth.BluetoothGattCharacteristic, int, int, int):void");
    }

    private void resetAndRestart(BluetoothGatt bluetoothGatt, Intent intent) throws DfuException, DeviceDisconnectedException, UploadAbortedException {
        this.mService.sendLogBroadcast(15, "Last upload interrupted. Restarting device...");
        this.mProgressInfo.setProgress(-5);
        logi("Sending Reset command (Op Code = 6)");
        writeOpCode(this.mControlPointCharacteristic, OP_CODE_RESET);
        this.mService.sendLogBroadcast(10, "Reset request sent");
        this.mService.waitUntilDisconnected();
        this.mService.sendLogBroadcast(5, "Disconnected by the remote device");
        BluetoothGattService service = bluetoothGatt.getService(GENERIC_ATTRIBUTE_SERVICE_UUID);
        this.mService.refreshDeviceCache(bluetoothGatt, !((service == null || service.getCharacteristic(SERVICE_CHANGED_UUID) == null) ? false : true));
        this.mService.close(bluetoothGatt);
        logi("Restarting the service");
        Intent intent2 = new Intent();
        intent2.fillIn(intent, 24);
        restartService(intent2, false);
    }
}
