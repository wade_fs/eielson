package no.nordicsemi.android.dfu.internal;

import android.util.Log;
import com.google.gson.Gson;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import no.nordicsemi.android.dfu.internal.manifest.Manifest;
import no.nordicsemi.android.dfu.internal.manifest.ManifestFile;

/* renamed from: no.nordicsemi.android.dfu.internal.ArchiveInputStream */
public class ArchiveInputStream extends ZipInputStream {
    private static final String APPLICATION_BIN = "application.bin";
    private static final String APPLICATION_HEX = "application.hex";
    private static final String APPLICATION_INIT = "application.dat";
    private static final String BOOTLOADER_BIN = "bootloader.bin";
    private static final String BOOTLOADER_HEX = "bootloader.hex";
    private static final String MANIFEST = "manifest.json";
    private static final String SOFTDEVICE_BIN = "softdevice.bin";
    private static final String SOFTDEVICE_HEX = "softdevice.hex";
    private static final String SYSTEM_INIT = "system.dat";
    private static final String TAG = "DfuArchiveInputStream";
    private byte[] applicationBytes;
    private byte[] applicationInitBytes;
    private int applicationSize;
    private byte[] bootloaderBytes;
    private int bootloaderSize;
    private int bytesRead = 0;
    private int bytesReadFromCurrentSource = 0;
    private int bytesReadFromMarkedSource;
    private CRC32 crc32 = new CRC32();
    private byte[] currentSource;
    private Map<String, byte[]> entries = new HashMap();
    private Manifest manifest;
    private byte[] markedSource;
    private byte[] softDeviceAndBootloaderBytes;
    private byte[] softDeviceBytes;
    private int softDeviceSize;
    private byte[] systemInitBytes;
    private int type;

    public boolean markSupported() {
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:79:0x021a A[Catch:{ all -> 0x028e }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x022a A[Catch:{ all -> 0x028e }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0253 A[Catch:{ all -> 0x028e }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0263 A[Catch:{ all -> 0x028e }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0286 A[SYNTHETIC, Splitter:B:96:0x0286] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:76:0x020a=Splitter:B:76:0x020a, B:10:0x002e=Splitter:B:10:0x002e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ArchiveInputStream(java.io.InputStream r5, int r6, int r7) throws java.io.IOException {
        /*
            r4 = this;
            r4.<init>(r5)
            java.util.zip.CRC32 r5 = new java.util.zip.CRC32
            r5.<init>()
            r4.crc32 = r5
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            r4.entries = r5
            r5 = 0
            r4.bytesRead = r5
            r4.bytesReadFromCurrentSource = r5
            r4.parseZip(r6)     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.Manifest r6 = r4.manifest     // Catch:{ all -> 0x028e }
            r0 = 1
            if (r6 == 0) goto L_0x01c4
            no.nordicsemi.android.dfu.internal.manifest.Manifest r6 = r4.manifest     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.FileInfo r6 = r6.getApplicationInfo()     // Catch:{ all -> 0x028e }
            java.lang.String r1 = " not found."
            if (r6 == 0) goto L_0x007d
            if (r7 == 0) goto L_0x002e
            r6 = r7 & 4
            if (r6 <= 0) goto L_0x007d
        L_0x002e:
            no.nordicsemi.android.dfu.internal.manifest.Manifest r6 = r4.manifest     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.FileInfo r6 = r6.getApplicationInfo()     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r2 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r3 = r6.getBinFileName()     // Catch:{ all -> 0x028e }
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x028e }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x028e }
            r4.applicationBytes = r2     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r2 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r3 = r6.getDatFileName()     // Catch:{ all -> 0x028e }
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x028e }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x028e }
            r4.applicationInitBytes = r2     // Catch:{ all -> 0x028e }
            byte[] r2 = r4.applicationBytes     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x005f
            byte[] r6 = r4.applicationBytes     // Catch:{ all -> 0x028e }
            int r6 = r6.length     // Catch:{ all -> 0x028e }
            r4.applicationSize = r6     // Catch:{ all -> 0x028e }
            byte[] r6 = r4.applicationBytes     // Catch:{ all -> 0x028e }
            r4.currentSource = r6     // Catch:{ all -> 0x028e }
            r6 = 1
            goto L_0x007e
        L_0x005f:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x028e }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x028e }
            r7.<init>()     // Catch:{ all -> 0x028e }
            java.lang.String r0 = "Application file "
            r7.append(r0)     // Catch:{ all -> 0x028e }
            java.lang.String r6 = r6.getBinFileName()     // Catch:{ all -> 0x028e }
            r7.append(r6)     // Catch:{ all -> 0x028e }
            r7.append(r1)     // Catch:{ all -> 0x028e }
            java.lang.String r6 = r7.toString()     // Catch:{ all -> 0x028e }
            r5.<init>(r6)     // Catch:{ all -> 0x028e }
            throw r5     // Catch:{ all -> 0x028e }
        L_0x007d:
            r6 = 0
        L_0x007e:
            no.nordicsemi.android.dfu.internal.manifest.Manifest r2 = r4.manifest     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.FileInfo r2 = r2.getBootloaderInfo()     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x00e7
            if (r7 == 0) goto L_0x008c
            r2 = r7 & 2
            if (r2 <= 0) goto L_0x00e7
        L_0x008c:
            byte[] r6 = r4.systemInitBytes     // Catch:{ all -> 0x028e }
            if (r6 != 0) goto L_0x00df
            no.nordicsemi.android.dfu.internal.manifest.Manifest r6 = r4.manifest     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.FileInfo r6 = r6.getBootloaderInfo()     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r2 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r3 = r6.getBinFileName()     // Catch:{ all -> 0x028e }
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x028e }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x028e }
            r4.bootloaderBytes = r2     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r2 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r3 = r6.getDatFileName()     // Catch:{ all -> 0x028e }
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x028e }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x028e }
            r4.systemInitBytes = r2     // Catch:{ all -> 0x028e }
            byte[] r2 = r4.bootloaderBytes     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x00c1
            byte[] r6 = r4.bootloaderBytes     // Catch:{ all -> 0x028e }
            int r6 = r6.length     // Catch:{ all -> 0x028e }
            r4.bootloaderSize = r6     // Catch:{ all -> 0x028e }
            byte[] r6 = r4.bootloaderBytes     // Catch:{ all -> 0x028e }
            r4.currentSource = r6     // Catch:{ all -> 0x028e }
            r6 = 1
            goto L_0x00e7
        L_0x00c1:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x028e }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x028e }
            r7.<init>()     // Catch:{ all -> 0x028e }
            java.lang.String r0 = "Bootloader file "
            r7.append(r0)     // Catch:{ all -> 0x028e }
            java.lang.String r6 = r6.getBinFileName()     // Catch:{ all -> 0x028e }
            r7.append(r6)     // Catch:{ all -> 0x028e }
            r7.append(r1)     // Catch:{ all -> 0x028e }
            java.lang.String r6 = r7.toString()     // Catch:{ all -> 0x028e }
            r5.<init>(r6)     // Catch:{ all -> 0x028e }
            throw r5     // Catch:{ all -> 0x028e }
        L_0x00df:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x028e }
            java.lang.String r6 = "Manifest: softdevice and bootloader specified. Use softdevice_bootloader instead."
            r5.<init>(r6)     // Catch:{ all -> 0x028e }
            throw r5     // Catch:{ all -> 0x028e }
        L_0x00e7:
            no.nordicsemi.android.dfu.internal.manifest.Manifest r2 = r4.manifest     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.FileInfo r2 = r2.getSoftdeviceInfo()     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x0144
            if (r7 == 0) goto L_0x00f5
            r2 = r7 & 1
            if (r2 <= 0) goto L_0x0144
        L_0x00f5:
            no.nordicsemi.android.dfu.internal.manifest.Manifest r6 = r4.manifest     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.FileInfo r6 = r6.getSoftdeviceInfo()     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r2 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r3 = r6.getBinFileName()     // Catch:{ all -> 0x028e }
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x028e }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x028e }
            r4.softDeviceBytes = r2     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r2 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r3 = r6.getDatFileName()     // Catch:{ all -> 0x028e }
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x028e }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x028e }
            r4.systemInitBytes = r2     // Catch:{ all -> 0x028e }
            byte[] r2 = r4.softDeviceBytes     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x0126
            byte[] r6 = r4.softDeviceBytes     // Catch:{ all -> 0x028e }
            int r6 = r6.length     // Catch:{ all -> 0x028e }
            r4.softDeviceSize = r6     // Catch:{ all -> 0x028e }
            byte[] r6 = r4.softDeviceBytes     // Catch:{ all -> 0x028e }
            r4.currentSource = r6     // Catch:{ all -> 0x028e }
            r6 = 1
            goto L_0x0144
        L_0x0126:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x028e }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x028e }
            r7.<init>()     // Catch:{ all -> 0x028e }
            java.lang.String r0 = "SoftDevice file "
            r7.append(r0)     // Catch:{ all -> 0x028e }
            java.lang.String r6 = r6.getBinFileName()     // Catch:{ all -> 0x028e }
            r7.append(r6)     // Catch:{ all -> 0x028e }
            r7.append(r1)     // Catch:{ all -> 0x028e }
            java.lang.String r6 = r7.toString()     // Catch:{ all -> 0x028e }
            r5.<init>(r6)     // Catch:{ all -> 0x028e }
            throw r5     // Catch:{ all -> 0x028e }
        L_0x0144:
            no.nordicsemi.android.dfu.internal.manifest.Manifest r2 = r4.manifest     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.SoftDeviceBootloaderFileInfo r2 = r2.getSoftdeviceBootloaderInfo()     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x01b8
            if (r7 == 0) goto L_0x0156
            r2 = r7 & 1
            if (r2 <= 0) goto L_0x01b8
            r7 = r7 & 2
            if (r7 <= 0) goto L_0x01b8
        L_0x0156:
            byte[] r6 = r4.systemInitBytes     // Catch:{ all -> 0x028e }
            if (r6 != 0) goto L_0x01b0
            no.nordicsemi.android.dfu.internal.manifest.Manifest r6 = r4.manifest     // Catch:{ all -> 0x028e }
            no.nordicsemi.android.dfu.internal.manifest.SoftDeviceBootloaderFileInfo r6 = r6.getSoftdeviceBootloaderInfo()     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r7 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r2 = r6.getBinFileName()     // Catch:{ all -> 0x028e }
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x028e }
            byte[] r7 = (byte[]) r7     // Catch:{ all -> 0x028e }
            r4.softDeviceAndBootloaderBytes = r7     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r7 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r2 = r6.getDatFileName()     // Catch:{ all -> 0x028e }
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x028e }
            byte[] r7 = (byte[]) r7     // Catch:{ all -> 0x028e }
            r4.systemInitBytes = r7     // Catch:{ all -> 0x028e }
            byte[] r7 = r4.softDeviceAndBootloaderBytes     // Catch:{ all -> 0x028e }
            if (r7 == 0) goto L_0x0192
            int r7 = r6.getSoftdeviceSize()     // Catch:{ all -> 0x028e }
            r4.softDeviceSize = r7     // Catch:{ all -> 0x028e }
            int r6 = r6.getBootloaderSize()     // Catch:{ all -> 0x028e }
            r4.bootloaderSize = r6     // Catch:{ all -> 0x028e }
            byte[] r6 = r4.softDeviceAndBootloaderBytes     // Catch:{ all -> 0x028e }
            r4.currentSource = r6     // Catch:{ all -> 0x028e }
            r6 = 1
            goto L_0x01b8
        L_0x0192:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x028e }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x028e }
            r7.<init>()     // Catch:{ all -> 0x028e }
            java.lang.String r0 = "File "
            r7.append(r0)     // Catch:{ all -> 0x028e }
            java.lang.String r6 = r6.getBinFileName()     // Catch:{ all -> 0x028e }
            r7.append(r6)     // Catch:{ all -> 0x028e }
            r7.append(r1)     // Catch:{ all -> 0x028e }
            java.lang.String r6 = r7.toString()     // Catch:{ all -> 0x028e }
            r5.<init>(r6)     // Catch:{ all -> 0x028e }
            throw r5     // Catch:{ all -> 0x028e }
        L_0x01b0:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x028e }
            java.lang.String r6 = "Manifest: The softdevice_bootloader may not be used together with softdevice or bootloader."
            r5.<init>(r6)     // Catch:{ all -> 0x028e }
            throw r5     // Catch:{ all -> 0x028e }
        L_0x01b8:
            if (r6 == 0) goto L_0x01bc
            goto L_0x0279
        L_0x01bc:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x028e }
            java.lang.String r6 = "Manifest file must specify at least one file."
            r5.<init>(r6)     // Catch:{ all -> 0x028e }
            throw r5     // Catch:{ all -> 0x028e }
        L_0x01c4:
            if (r7 == 0) goto L_0x01ca
            r6 = r7 & 4
            if (r6 <= 0) goto L_0x0201
        L_0x01ca:
            java.util.Map<java.lang.String, byte[]> r6 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r1 = "application.hex"
            java.lang.Object r6 = r6.get(r1)     // Catch:{ all -> 0x028e }
            byte[] r6 = (byte[]) r6     // Catch:{ all -> 0x028e }
            r4.applicationBytes = r6     // Catch:{ all -> 0x028e }
            byte[] r6 = r4.applicationBytes     // Catch:{ all -> 0x028e }
            if (r6 != 0) goto L_0x01e6
            java.util.Map<java.lang.String, byte[]> r6 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r1 = "application.bin"
            java.lang.Object r6 = r6.get(r1)     // Catch:{ all -> 0x028e }
            byte[] r6 = (byte[]) r6     // Catch:{ all -> 0x028e }
            r4.applicationBytes = r6     // Catch:{ all -> 0x028e }
        L_0x01e6:
            byte[] r6 = r4.applicationBytes     // Catch:{ all -> 0x028e }
            if (r6 == 0) goto L_0x0201
            byte[] r6 = r4.applicationBytes     // Catch:{ all -> 0x028e }
            int r6 = r6.length     // Catch:{ all -> 0x028e }
            r4.applicationSize = r6     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r6 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r1 = "application.dat"
            java.lang.Object r6 = r6.get(r1)     // Catch:{ all -> 0x028e }
            byte[] r6 = (byte[]) r6     // Catch:{ all -> 0x028e }
            r4.applicationInitBytes = r6     // Catch:{ all -> 0x028e }
            byte[] r6 = r4.applicationBytes     // Catch:{ all -> 0x028e }
            r4.currentSource = r6     // Catch:{ all -> 0x028e }
            r6 = 1
            goto L_0x0202
        L_0x0201:
            r6 = 0
        L_0x0202:
            java.lang.String r1 = "system.dat"
            if (r7 == 0) goto L_0x020a
            r2 = r7 & 2
            if (r2 <= 0) goto L_0x023e
        L_0x020a:
            java.util.Map<java.lang.String, byte[]> r2 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r3 = "bootloader.hex"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x028e }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x028e }
            r4.bootloaderBytes = r2     // Catch:{ all -> 0x028e }
            byte[] r2 = r4.bootloaderBytes     // Catch:{ all -> 0x028e }
            if (r2 != 0) goto L_0x0226
            java.util.Map<java.lang.String, byte[]> r2 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r3 = "bootloader.bin"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ all -> 0x028e }
            byte[] r2 = (byte[]) r2     // Catch:{ all -> 0x028e }
            r4.bootloaderBytes = r2     // Catch:{ all -> 0x028e }
        L_0x0226:
            byte[] r2 = r4.bootloaderBytes     // Catch:{ all -> 0x028e }
            if (r2 == 0) goto L_0x023e
            byte[] r6 = r4.bootloaderBytes     // Catch:{ all -> 0x028e }
            int r6 = r6.length     // Catch:{ all -> 0x028e }
            r4.bootloaderSize = r6     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r6 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.Object r6 = r6.get(r1)     // Catch:{ all -> 0x028e }
            byte[] r6 = (byte[]) r6     // Catch:{ all -> 0x028e }
            r4.systemInitBytes = r6     // Catch:{ all -> 0x028e }
            byte[] r6 = r4.bootloaderBytes     // Catch:{ all -> 0x028e }
            r4.currentSource = r6     // Catch:{ all -> 0x028e }
            r6 = 1
        L_0x023e:
            if (r7 == 0) goto L_0x0243
            r7 = r7 & r0
            if (r7 <= 0) goto L_0x0277
        L_0x0243:
            java.util.Map<java.lang.String, byte[]> r7 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r2 = "softdevice.hex"
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x028e }
            byte[] r7 = (byte[]) r7     // Catch:{ all -> 0x028e }
            r4.softDeviceBytes = r7     // Catch:{ all -> 0x028e }
            byte[] r7 = r4.softDeviceBytes     // Catch:{ all -> 0x028e }
            if (r7 != 0) goto L_0x025f
            java.util.Map<java.lang.String, byte[]> r7 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.String r2 = "softdevice.bin"
            java.lang.Object r7 = r7.get(r2)     // Catch:{ all -> 0x028e }
            byte[] r7 = (byte[]) r7     // Catch:{ all -> 0x028e }
            r4.softDeviceBytes = r7     // Catch:{ all -> 0x028e }
        L_0x025f:
            byte[] r7 = r4.softDeviceBytes     // Catch:{ all -> 0x028e }
            if (r7 == 0) goto L_0x0277
            byte[] r6 = r4.softDeviceBytes     // Catch:{ all -> 0x028e }
            int r6 = r6.length     // Catch:{ all -> 0x028e }
            r4.softDeviceSize = r6     // Catch:{ all -> 0x028e }
            java.util.Map<java.lang.String, byte[]> r6 = r4.entries     // Catch:{ all -> 0x028e }
            java.lang.Object r6 = r6.get(r1)     // Catch:{ all -> 0x028e }
            byte[] r6 = (byte[]) r6     // Catch:{ all -> 0x028e }
            r4.systemInitBytes = r6     // Catch:{ all -> 0x028e }
            byte[] r6 = r4.softDeviceBytes     // Catch:{ all -> 0x028e }
            r4.currentSource = r6     // Catch:{ all -> 0x028e }
            r6 = 1
        L_0x0277:
            if (r6 == 0) goto L_0x0286
        L_0x0279:
            r4.mark(r5)     // Catch:{ all -> 0x028e }
            int r5 = r4.getContentType()
            r4.type = r5
            super.close()
            return
        L_0x0286:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x028e }
            java.lang.String r6 = "The ZIP file must contain an Application, a Soft Device and/or a Bootloader."
            r5.<init>(r6)     // Catch:{ all -> 0x028e }
            throw r5     // Catch:{ all -> 0x028e }
        L_0x028e:
            r5 = move-exception
            int r6 = r4.getContentType()
            r4.type = r6
            super.close()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.internal.ArchiveInputStream.<init>(java.io.InputStream, int, int):void");
    }

    private void parseZip(int i) throws IOException {
        byte[] bArr = new byte[1024];
        String str = null;
        while (true) {
            ZipEntry nextEntry = getNextEntry();
            if (nextEntry == null) {
                break;
            }
            String name = nextEntry.getName();
            if (nextEntry.isDirectory()) {
                Log.w(TAG, "A directory found in the ZIP: " + name + "!");
            } else {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = super.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                if (name.toLowerCase(Locale.US).endsWith("hex")) {
                    HexInputStream hexInputStream = new HexInputStream(byteArray, i);
                    byteArray = new byte[hexInputStream.available()];
                    hexInputStream.read(byteArray);
                    hexInputStream.close();
                }
                if (MANIFEST.equals(name)) {
                    str = new String(byteArray, "UTF-8");
                } else {
                    this.entries.put(name, byteArray);
                }
            }
        }
        if (this.entries.isEmpty()) {
            throw new FileNotFoundException("No files found in the ZIP. Check if the URI provided is valid and the ZIP contains required files on root level, not in a directory.");
        } else if (str != null) {
            this.manifest = ((ManifestFile) new Gson().fromJson(str, ManifestFile.class)).getManifest();
            if (this.manifest == null) {
                Log.w(TAG, "Manifest failed to be parsed. Did you add \n-keep class no.nordicsemi.android.dfu.** { *; }\nto your proguard rules?");
            }
        } else {
            Log.w(TAG, "Manifest not found in the ZIP. It is recommended to use a distribution file created with: https://github.com/NordicSemiconductor/pc-nrfutil/ (for Legacy DFU use version 0.5.x)");
        }
    }

    public void close() throws IOException {
        this.softDeviceBytes = null;
        this.bootloaderBytes = null;
        this.softDeviceBytes = null;
        this.softDeviceAndBootloaderBytes = null;
        this.applicationSize = 0;
        this.bootloaderSize = 0;
        this.softDeviceSize = 0;
        this.currentSource = null;
        this.bytesReadFromCurrentSource = 0;
        this.bytesRead = 0;
        super.close();
    }

    public int read(byte[] bArr) throws IOException {
        int length = this.currentSource.length - this.bytesReadFromCurrentSource;
        if (bArr.length <= length) {
            length = bArr.length;
        }
        System.arraycopy(this.currentSource, this.bytesReadFromCurrentSource, bArr, 0, length);
        this.bytesReadFromCurrentSource += length;
        if (bArr.length > length) {
            if (startNextFile() == null) {
                this.bytesRead += length;
                this.crc32.update(bArr, 0, length);
                return length;
            }
            int length2 = this.currentSource.length;
            if (bArr.length - length <= length2) {
                length2 = bArr.length - length;
            }
            System.arraycopy(this.currentSource, 0, bArr, length, length2);
            this.bytesReadFromCurrentSource += length2;
            length += length2;
        }
        this.bytesRead += length;
        this.crc32.update(bArr, 0, length);
        return length;
    }

    public void mark(int i) {
        this.markedSource = this.currentSource;
        this.bytesReadFromMarkedSource = this.bytesReadFromCurrentSource;
    }

    public void reset() throws IOException {
        byte[] bArr;
        this.currentSource = this.markedSource;
        int i = this.bytesReadFromMarkedSource;
        this.bytesReadFromCurrentSource = i;
        this.bytesRead = i;
        this.crc32.reset();
        if (this.currentSource == this.bootloaderBytes && (bArr = this.softDeviceBytes) != null) {
            this.crc32.update(bArr);
            this.bytesRead += this.softDeviceSize;
        }
        this.crc32.update(this.currentSource, 0, this.bytesReadFromCurrentSource);
    }

    public int getBytesRead() {
        return this.bytesRead;
    }

    public long getCrc32() {
        return this.crc32.getValue();
    }

    public int getContentType() {
        this.type = 0;
        if (this.softDeviceAndBootloaderBytes != null) {
            this.type |= 3;
        }
        if (this.softDeviceSize > 0) {
            this.type |= 1;
        }
        if (this.bootloaderSize > 0) {
            this.type |= 2;
        }
        if (this.applicationSize > 0) {
            this.type |= 4;
        }
        return this.type;
    }

    public int setContentType(int i) {
        byte[] bArr;
        this.type = i;
        int i2 = i & 4;
        if (i2 > 0 && this.applicationBytes == null) {
            this.type &= -5;
        }
        int i3 = i & 3;
        if (i3 == 3) {
            if (this.softDeviceBytes == null && this.softDeviceAndBootloaderBytes == null) {
                this.type &= -2;
            }
            if (this.bootloaderBytes == null && this.softDeviceAndBootloaderBytes == null) {
                this.type &= -2;
            }
        } else if (this.softDeviceAndBootloaderBytes != null) {
            this.type &= -4;
        }
        if (i3 > 0 && (bArr = this.softDeviceAndBootloaderBytes) != null) {
            this.currentSource = bArr;
        } else if ((i & 1) > 0) {
            this.currentSource = this.softDeviceBytes;
        } else if ((i & 2) > 0) {
            this.currentSource = this.bootloaderBytes;
        } else if (i2 > 0) {
            this.currentSource = this.applicationBytes;
        }
        this.bytesReadFromCurrentSource = 0;
        try {
            mark(0);
            reset();
        } catch (IOException unused) {
        }
        return this.type;
    }

    private byte[] startNextFile() {
        byte[] bArr;
        if (this.currentSource != this.softDeviceBytes || (bArr = this.bootloaderBytes) == null || (this.type & 2) <= 0) {
            byte[] bArr2 = this.currentSource;
            byte[] bArr3 = this.applicationBytes;
            if (bArr2 == bArr3 || bArr3 == null || (this.type & 4) <= 0) {
                bArr = null;
                this.currentSource = null;
            } else {
                this.currentSource = bArr3;
                bArr = bArr3;
            }
        } else {
            this.currentSource = bArr;
        }
        this.bytesReadFromCurrentSource = 0;
        return bArr;
    }

    public int available() {
        int softDeviceImageSize;
        int i;
        byte[] bArr = this.softDeviceAndBootloaderBytes;
        if (bArr == null || this.softDeviceSize != 0 || this.bootloaderSize != 0 || (this.type & 3) <= 0) {
            softDeviceImageSize = softDeviceImageSize() + bootloaderImageSize() + applicationImageSize();
            i = this.bytesRead;
        } else {
            softDeviceImageSize = bArr.length + applicationImageSize();
            i = this.bytesRead;
        }
        return softDeviceImageSize - i;
    }

    public int softDeviceImageSize() {
        if ((this.type & 1) > 0) {
            return this.softDeviceSize;
        }
        return 0;
    }

    public int bootloaderImageSize() {
        if ((this.type & 2) > 0) {
            return this.bootloaderSize;
        }
        return 0;
    }

    public int applicationImageSize() {
        if ((this.type & 4) > 0) {
            return this.applicationSize;
        }
        return 0;
    }

    public byte[] getSystemInit() {
        return this.systemInitBytes;
    }

    public byte[] getApplicationInit() {
        return this.applicationInitBytes;
    }

    public boolean isSecureDfuRequired() {
        Manifest manifest2 = this.manifest;
        return manifest2 != null && manifest2.isSecureDfuRequired();
    }
}
