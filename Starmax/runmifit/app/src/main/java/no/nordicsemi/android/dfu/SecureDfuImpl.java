package no.nordicsemi.android.dfu;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import com.baidu.mapapi.UIMsg;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.UUID;
import no.nordicsemi.android.dfu.BaseCustomDfuImpl;
import no.nordicsemi.android.dfu.BaseDfuImpl;
import no.nordicsemi.android.dfu.internal.ArchiveInputStream;
import no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException;
import no.nordicsemi.android.dfu.internal.exception.DfuException;
import no.nordicsemi.android.dfu.internal.exception.RemoteDfuException;
import no.nordicsemi.android.dfu.internal.exception.RemoteDfuExtendedErrorException;
import no.nordicsemi.android.dfu.internal.exception.UnknownResponseException;
import no.nordicsemi.android.dfu.internal.exception.UploadAbortedException;
import no.nordicsemi.android.error.SecureDfuError;

/* renamed from: no.nordicsemi.android.dfu.SecureDfuImpl */
class SecureDfuImpl extends BaseCustomDfuImpl {
    protected static final UUID DEFAULT_DFU_CONTROL_POINT_UUID = new UUID(-8157989241631715488L, -6937650605005804976L);
    protected static final UUID DEFAULT_DFU_PACKET_UUID = new UUID(-8157989237336748192L, -6937650605005804976L);
    protected static final UUID DEFAULT_DFU_SERVICE_UUID = new UUID(279658205548544L, -9223371485494954757L);
    protected static UUID DFU_CONTROL_POINT_UUID = DEFAULT_DFU_CONTROL_POINT_UUID;
    protected static UUID DFU_PACKET_UUID = DEFAULT_DFU_PACKET_UUID;
    protected static UUID DFU_SERVICE_UUID = DEFAULT_DFU_SERVICE_UUID;
    private static final int DFU_STATUS_SUCCESS = 1;
    private static final int MAX_ATTEMPTS = 3;
    private static final int OBJECT_COMMAND = 1;
    private static final int OBJECT_DATA = 2;
    private static final byte[] OP_CODE_CALCULATE_CHECKSUM = {3};
    private static final int OP_CODE_CALCULATE_CHECKSUM_KEY = 3;
    private static final byte[] OP_CODE_CREATE_COMMAND = {1, 1, 0, 0, 0, 0};
    private static final byte[] OP_CODE_CREATE_DATA = {1, 2, 0, 0, 0, 0};
    private static final int OP_CODE_CREATE_KEY = 1;
    private static final byte[] OP_CODE_EXECUTE = {4};
    private static final int OP_CODE_EXECUTE_KEY = 4;
    private static final byte[] OP_CODE_PACKET_RECEIPT_NOTIF_REQ = {2, 0, 0};
    private static final int OP_CODE_PACKET_RECEIPT_NOTIF_REQ_KEY = 2;
    private static final int OP_CODE_RESPONSE_CODE_KEY = 96;
    private static final byte[] OP_CODE_SELECT_OBJECT = {6, 0};
    private static final int OP_CODE_SELECT_OBJECT_KEY = 6;
    private final SecureBluetoothCallback mBluetoothCallback = new SecureBluetoothCallback();
    private BluetoothGattCharacteristic mControlPointCharacteristic;
    private BluetoothGattCharacteristic mPacketCharacteristic;

    /* renamed from: no.nordicsemi.android.dfu.SecureDfuImpl$SecureBluetoothCallback */
    protected class SecureBluetoothCallback extends BaseCustomDfuImpl.BaseCustomBluetoothCallback {
        protected SecureBluetoothCallback() {
            super();
        }

        public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            if (bluetoothGattCharacteristic.getValue() == null || bluetoothGattCharacteristic.getValue().length < 3) {
                SecureDfuImpl secureDfuImpl = SecureDfuImpl.this;
                secureDfuImpl.loge("Empty response: " + parse(bluetoothGattCharacteristic));
                SecureDfuImpl secureDfuImpl2 = SecureDfuImpl.this;
                secureDfuImpl2.mError = 4104;
                secureDfuImpl2.notifyLock();
                return;
            }
            if (bluetoothGattCharacteristic.getIntValue(17, 0).intValue() != 96) {
                SecureDfuImpl secureDfuImpl3 = SecureDfuImpl.this;
                secureDfuImpl3.loge("Invalid response: " + parse(bluetoothGattCharacteristic));
                SecureDfuImpl.this.mError = 4104;
            } else if (bluetoothGattCharacteristic.getIntValue(17, 1).intValue() == 3) {
                int intValue = bluetoothGattCharacteristic.getIntValue(20, 3).intValue();
                if (((int) (((ArchiveInputStream) SecureDfuImpl.this.mFirmwareStream).getCrc32() & 4294967295L)) == bluetoothGattCharacteristic.getIntValue(20, 7).intValue()) {
                    SecureDfuImpl.this.mProgressInfo.setBytesReceived(intValue);
                } else if (SecureDfuImpl.this.mFirmwareUploadInProgress) {
                    SecureDfuImpl secureDfuImpl4 = SecureDfuImpl.this;
                    secureDfuImpl4.mFirmwareUploadInProgress = false;
                    secureDfuImpl4.notifyLock();
                    return;
                }
                handlePacketReceiptNotification(bluetoothGatt, bluetoothGattCharacteristic);
            } else if (!SecureDfuImpl.this.mRemoteErrorOccurred) {
                if (bluetoothGattCharacteristic.getIntValue(17, 2).intValue() != 1) {
                    SecureDfuImpl.this.mRemoteErrorOccurred = true;
                }
                handleNotification(bluetoothGatt, bluetoothGattCharacteristic);
            }
            SecureDfuImpl.this.notifyLock();
        }
    }

    SecureDfuImpl(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    public boolean isClientCompatible(Intent intent, BluetoothGatt bluetoothGatt) {
        BluetoothGattService service = bluetoothGatt.getService(DFU_SERVICE_UUID);
        if (service == null) {
            return false;
        }
        this.mControlPointCharacteristic = service.getCharacteristic(DFU_CONTROL_POINT_UUID);
        this.mPacketCharacteristic = service.getCharacteristic(DFU_PACKET_UUID);
        if (this.mControlPointCharacteristic == null || this.mPacketCharacteristic == null) {
            return false;
        }
        return true;
    }

    public boolean initialize(Intent intent, BluetoothGatt bluetoothGatt, int i, InputStream inputStream, InputStream inputStream2) throws DfuException, DeviceDisconnectedException, UploadAbortedException {
        if (inputStream2 != null) {
            return super.initialize(intent, bluetoothGatt, i, inputStream, inputStream2);
        }
        this.mService.sendLogBroadcast(20, "The Init packet is required by this version DFU Bootloader");
        this.mService.terminateConnection(bluetoothGatt, 4107);
        return false;
    }

    public BaseDfuImpl.BaseBluetoothGattCallback getGattCallback() {
        return this.mBluetoothCallback;
    }

    /* access modifiers changed from: protected */
    public UUID getControlPointCharacteristicUUID() {
        return DFU_CONTROL_POINT_UUID;
    }

    /* access modifiers changed from: protected */
    public UUID getPacketCharacteristicUUID() {
        return DFU_PACKET_UUID;
    }

    /* access modifiers changed from: protected */
    public UUID getDfuServiceUUID() {
        return DFU_SERVICE_UUID;
    }

    public void performDfu(Intent intent) throws DfuException, DeviceDisconnectedException, UploadAbortedException {
        logw("Secure DFU bootloader found");
        this.mProgressInfo.setProgress(-2);
        this.mService.waitFor(1000);
        BluetoothGatt bluetoothGatt = this.mGatt;
        if (intent.hasExtra(DfuBaseService.EXTRA_MTU) && Build.VERSION.SDK_INT >= 21) {
            int intExtra = intent.getIntExtra(DfuBaseService.EXTRA_MTU, UIMsg.m_AppUI.MSG_CITY_SUP_DOM);
            logi("Requesting MTU = " + intExtra);
            requestMtu(intExtra);
        }
        try {
            enableCCCD(this.mControlPointCharacteristic, 1);
            this.mService.sendLogBroadcast(10, "Notifications enabled");
            this.mService.waitFor(1000);
            sendInitPacket(bluetoothGatt);
            sendFirmware(bluetoothGatt);
            this.mProgressInfo.setProgress(-5);
            this.mService.waitUntilDisconnected();
            this.mService.sendLogBroadcast(5, "Disconnected by the remote device");
            finalize(intent, false);
        } catch (UploadAbortedException e) {
            throw e;
        } catch (UnknownResponseException e2) {
            loge(e2.getMessage());
            this.mService.sendLogBroadcast(20, e2.getMessage());
            this.mService.terminateConnection(bluetoothGatt, 4104);
        } catch (RemoteDfuException e3) {
            int errorNumber = e3.getErrorNumber() | 512;
            loge(e3.getMessage() + ": " + SecureDfuError.parse(errorNumber));
            this.mService.sendLogBroadcast(20, String.format(Locale.US, "Remote DFU error: %s", SecureDfuError.parse(errorNumber)));
            if (e3 instanceof RemoteDfuExtendedErrorException) {
                RemoteDfuExtendedErrorException remoteDfuExtendedErrorException = (RemoteDfuExtendedErrorException) e3;
                int extendedErrorNumber = remoteDfuExtendedErrorException.getExtendedErrorNumber() | 1024;
                loge("Extended Error details: " + SecureDfuError.parseExtendedError(extendedErrorNumber));
                DfuBaseService dfuBaseService = this.mService;
                dfuBaseService.sendLogBroadcast(20, "Details: " + SecureDfuError.parseExtendedError(extendedErrorNumber) + " (Code = " + remoteDfuExtendedErrorException.getExtendedErrorNumber() + ")");
                this.mService.terminateConnection(bluetoothGatt, extendedErrorNumber | 8192);
                return;
            }
            this.mService.terminateConnection(bluetoothGatt, errorNumber | 8192);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0118  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void sendInitPacket(android.bluetooth.BluetoothGatt r20) throws no.nordicsemi.android.dfu.internal.exception.RemoteDfuException, no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException, no.nordicsemi.android.dfu.internal.exception.DfuException, no.nordicsemi.android.dfu.internal.exception.UploadAbortedException, no.nordicsemi.android.dfu.internal.exception.UnknownResponseException {
        /*
            r19 = this;
            r1 = r19
            r2 = r20
            java.util.zip.CRC32 r3 = new java.util.zip.CRC32
            r3.<init>()
            java.lang.String r0 = "Setting object to Command (Op Code = 6, Type = 1)"
            r1.logi(r0)
            r4 = 1
            no.nordicsemi.android.dfu.SecureDfuImpl$ObjectInfo r5 = r1.selectObject(r4)
            java.util.Locale r0 = java.util.Locale.US
            r6 = 3
            java.lang.Object[] r7 = new java.lang.Object[r6]
            int r8 = r5.maxSize
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r9 = 0
            r7[r9] = r8
            int r8 = r5.offset
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r7[r4] = r8
            int r8 = r5.CRC32
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r10 = 2
            r7[r10] = r8
            java.lang.String r8 = "Command object info received (Max size = %d, Offset = %d, CRC = %08X)"
            java.lang.String r0 = java.lang.String.format(r0, r8, r7)
            r1.logi(r0)
            no.nordicsemi.android.dfu.DfuBaseService r0 = r1.mService
            java.util.Locale r7 = java.util.Locale.US
            java.lang.Object[] r11 = new java.lang.Object[r6]
            int r12 = r5.maxSize
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r11[r9] = r12
            int r12 = r5.offset
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r11[r4] = r12
            int r12 = r5.CRC32
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            r11[r10] = r12
            java.lang.String r7 = java.lang.String.format(r7, r8, r11)
            r8 = 10
            r0.sendLogBroadcast(r8, r7)
            int r0 = r1.mInitPacketSizeInBytes
            int r0 = r5.maxSize
            int r0 = r5.offset
            r7 = 4100(0x1004, float:5.745E-42)
            java.lang.String r11 = "Error while resetting the init packet stream"
            r12 = 4294967295(0xffffffff, double:2.1219957905E-314)
            if (r0 <= 0) goto L_0x0114
            int r0 = r5.offset
            int r14 = r1.mInitPacketSizeInBytes
            if (r0 > r14) goto L_0x0114
            int r0 = r5.offset     // Catch:{ IOException -> 0x00df }
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x00df }
            java.io.InputStream r14 = r1.mInitPacketStream     // Catch:{ IOException -> 0x00df }
            r14.read(r0)     // Catch:{ IOException -> 0x00df }
            r3.update(r0)     // Catch:{ IOException -> 0x00df }
            long r14 = r3.getValue()     // Catch:{ IOException -> 0x00df }
            long r14 = r14 & r12
            int r0 = (int) r14     // Catch:{ IOException -> 0x00df }
            int r14 = r5.CRC32     // Catch:{ IOException -> 0x00df }
            if (r14 != r0) goto L_0x00d4
            java.lang.String r0 = "Init packet CRC is the same"
            r1.logi(r0)     // Catch:{ IOException -> 0x00df }
            int r0 = r5.offset     // Catch:{ IOException -> 0x00df }
            int r14 = r1.mInitPacketSizeInBytes     // Catch:{ IOException -> 0x00df }
            if (r0 != r14) goto L_0x00ad
            java.lang.String r0 = "-> Whole Init packet was sent before"
            r1.logi(r0)     // Catch:{ IOException -> 0x00df }
            no.nordicsemi.android.dfu.DfuBaseService r0 = r1.mService     // Catch:{ IOException -> 0x00a9 }
            java.lang.String r14 = "Received CRC match Init packet"
            r0.sendLogBroadcast(r8, r14)     // Catch:{ IOException -> 0x00a9 }
            r0 = 0
            r14 = 1
            goto L_0x0116
        L_0x00a9:
            r0 = move-exception
            r14 = 0
            r15 = 1
            goto L_0x00e2
        L_0x00ad:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00df }
            r0.<init>()     // Catch:{ IOException -> 0x00df }
            java.lang.String r14 = "-> "
            r0.append(r14)     // Catch:{ IOException -> 0x00df }
            int r14 = r5.offset     // Catch:{ IOException -> 0x00df }
            r0.append(r14)     // Catch:{ IOException -> 0x00df }
            java.lang.String r14 = " bytes of Init packet were sent before"
            r0.append(r14)     // Catch:{ IOException -> 0x00df }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00df }
            r1.logi(r0)     // Catch:{ IOException -> 0x00df }
            no.nordicsemi.android.dfu.DfuBaseService r0 = r1.mService     // Catch:{ IOException -> 0x00d1 }
            java.lang.String r14 = "Resuming sending Init packet..."
            r0.sendLogBroadcast(r8, r14)     // Catch:{ IOException -> 0x00d1 }
            r0 = 1
            goto L_0x0115
        L_0x00d1:
            r0 = move-exception
            r14 = 1
            goto L_0x00e1
        L_0x00d4:
            java.io.InputStream r0 = r1.mInitPacketStream     // Catch:{ IOException -> 0x00df }
            r0.reset()     // Catch:{ IOException -> 0x00df }
            r3.reset()     // Catch:{ IOException -> 0x00df }
            r5.offset = r9     // Catch:{ IOException -> 0x00df }
            goto L_0x0114
        L_0x00df:
            r0 = move-exception
            r14 = 0
        L_0x00e1:
            r15 = 0
        L_0x00e2:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r12 = "Error while reading "
            r10.append(r12)
            int r12 = r5.offset
            r10.append(r12)
            java.lang.String r12 = " bytes from the init packet stream"
            r10.append(r12)
            java.lang.String r10 = r10.toString()
            r1.loge(r10, r0)
            java.io.InputStream r0 = r1.mInitPacketStream     // Catch:{ IOException -> 0x010a }
            r0.reset()     // Catch:{ IOException -> 0x010a }
            r3.reset()     // Catch:{ IOException -> 0x010a }
            r5.offset = r9     // Catch:{ IOException -> 0x010a }
            r0 = r14
            r14 = r15
            goto L_0x0116
        L_0x010a:
            r0 = move-exception
            r1.loge(r11, r0)
            no.nordicsemi.android.dfu.DfuBaseService r0 = r1.mService
            r0.terminateConnection(r2, r7)
            return
        L_0x0114:
            r0 = 0
        L_0x0115:
            r14 = 0
        L_0x0116:
            if (r14 != 0) goto L_0x0251
            r1.setPacketReceiptNotifications(r9)
            no.nordicsemi.android.dfu.DfuBaseService r10 = r1.mService
            java.lang.String r12 = "Packet Receipt Notif disabled (Op Code = 2, Value = 0)"
            r10.sendLogBroadcast(r8, r12)
            r10 = r0
            r0 = 1
        L_0x0124:
            if (r0 > r6) goto L_0x0251
            java.lang.String r12 = ")"
            if (r10 != 0) goto L_0x014f
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r13 = "Creating Init packet object (Op Code = 1, Type = 1, Size = "
            r10.append(r13)
            int r13 = r1.mInitPacketSizeInBytes
            r10.append(r13)
            r10.append(r12)
            java.lang.String r10 = r10.toString()
            r1.logi(r10)
            int r10 = r1.mInitPacketSizeInBytes
            r1.writeCreateRequest(r4, r10)
            no.nordicsemi.android.dfu.DfuBaseService r10 = r1.mService
            java.lang.String r13 = "Command object created"
            r10.sendLogBroadcast(r8, r13)
        L_0x014f:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r13 = "Sending "
            r10.append(r13)
            int r13 = r1.mInitPacketSizeInBytes
            int r14 = r5.offset
            int r13 = r13 - r14
            r10.append(r13)
            java.lang.String r13 = " bytes of init packet..."
            r10.append(r13)
            java.lang.String r10 = r10.toString()
            r1.logi(r10)
            android.bluetooth.BluetoothGattCharacteristic r10 = r1.mPacketCharacteristic
            r1.writeInitData(r10, r3)
            long r13 = r3.getValue()
            r16 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r13 = r13 & r16
            int r10 = (int) r13
            no.nordicsemi.android.dfu.DfuBaseService r13 = r1.mService
            java.util.Locale r14 = java.util.Locale.US
            java.lang.Object[] r15 = new java.lang.Object[r4]
            java.lang.Integer r18 = java.lang.Integer.valueOf(r10)
            r15[r9] = r18
            java.lang.String r7 = "Command object sent (CRC = %08X)"
            java.lang.String r7 = java.lang.String.format(r14, r7, r15)
            r13.sendLogBroadcast(r8, r7)
            java.lang.String r7 = "Sending Calculate Checksum command (Op Code = 3)"
            r1.logi(r7)
            no.nordicsemi.android.dfu.SecureDfuImpl$ObjectChecksum r7 = r19.readChecksum()
            no.nordicsemi.android.dfu.DfuBaseService r13 = r1.mService
            java.util.Locale r14 = java.util.Locale.US
            r15 = 2
            java.lang.Object[] r6 = new java.lang.Object[r15]
            int r15 = r7.offset
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)
            r6[r9] = r15
            int r15 = r7.CRC32
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)
            r6[r4] = r15
            java.lang.String r15 = "Checksum received (Offset = %d, CRC = %08X)"
            java.lang.String r6 = java.lang.String.format(r14, r15, r6)
            r13.sendLogBroadcast(r8, r6)
            java.util.Locale r6 = java.util.Locale.US
            r13 = 2
            java.lang.Object[] r14 = new java.lang.Object[r13]
            int r13 = r7.offset
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            r14[r9] = r13
            int r13 = r7.CRC32
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            r14[r4] = r13
            java.lang.String r6 = java.lang.String.format(r6, r15, r14)
            r1.logi(r6)
            int r6 = r7.CRC32
            if (r10 != r6) goto L_0x01dd
            goto L_0x0251
        L_0x01dd:
            r6 = 3
            if (r0 >= r6) goto L_0x023d
            int r0 = r0 + 1
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r10 = "CRC does not match! Retrying...("
            r7.append(r10)
            r7.append(r0)
            java.lang.String r13 = "/"
            r7.append(r13)
            r7.append(r6)
            r7.append(r12)
            java.lang.String r7 = r7.toString()
            r1.logi(r7)
            no.nordicsemi.android.dfu.DfuBaseService r7 = r1.mService
            r14 = 15
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r15.append(r10)
            r15.append(r0)
            r15.append(r13)
            r15.append(r6)
            r15.append(r12)
            java.lang.String r10 = r15.toString()
            r7.sendLogBroadcast(r14, r10)
            r5.offset = r9     // Catch:{ IOException -> 0x0231 }
            r5.CRC32 = r9     // Catch:{ IOException -> 0x0231 }
            java.io.InputStream r7 = r1.mInitPacketStream     // Catch:{ IOException -> 0x0231 }
            r7.reset()     // Catch:{ IOException -> 0x0231 }
            r3.reset()     // Catch:{ IOException -> 0x0231 }
            r7 = 4100(0x1004, float:5.745E-42)
            r10 = 0
            goto L_0x0124
        L_0x0231:
            r0 = move-exception
            r1.loge(r11, r0)
            no.nordicsemi.android.dfu.DfuBaseService r0 = r1.mService
            r3 = 4100(0x1004, float:5.745E-42)
            r0.terminateConnection(r2, r3)
            return
        L_0x023d:
            java.lang.String r0 = "CRC does not match!"
            r1.loge(r0)
            no.nordicsemi.android.dfu.DfuBaseService r3 = r1.mService
            r4 = 20
            r3.sendLogBroadcast(r4, r0)
            no.nordicsemi.android.dfu.DfuBaseService r0 = r1.mService
            r3 = 4109(0x100d, float:5.758E-42)
            r0.terminateConnection(r2, r3)
            return
        L_0x0251:
            java.lang.String r0 = "Executing init packet (Op Code = 4)"
            r1.logi(r0)
            r19.writeExecute()
            no.nordicsemi.android.dfu.DfuBaseService r0 = r1.mService
            java.lang.String r2 = "Command object executed"
            r0.sendLogBroadcast(r8, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.SecureDfuImpl.sendInitPacket(android.bluetooth.BluetoothGatt):void");
    }

    private void sendFirmware(BluetoothGatt bluetoothGatt) throws RemoteDfuException, DeviceDisconnectedException, DfuException, UploadAbortedException, UnknownResponseException {
        int i;
        boolean z;
        long j;
        long j2;
        boolean z2;
        String str;
        BluetoothGatt bluetoothGatt2 = bluetoothGatt;
        int i2 = this.mPacketsBeforeNotification;
        String str2 = "Packet Receipt Notif Req (Op Code = 2) sent (Value = ";
        if (i2 > 0) {
            setPacketReceiptNotifications(i2);
            this.mService.sendLogBroadcast(10, str2 + i2 + ")");
        }
        logi("Setting object to Data (Op Code = 6, Type = 2)");
        ObjectInfo selectObject = selectObject(2);
        logi(String.format(Locale.US, "Data object info received (Max size = %d, Offset = %d, CRC = %08X)", Integer.valueOf(selectObject.maxSize), Integer.valueOf(selectObject.offset), Integer.valueOf(selectObject.CRC32)));
        this.mService.sendLogBroadcast(10, String.format(Locale.US, "Data object info received (Max size = %d, Offset = %d, CRC = %08X)", Integer.valueOf(selectObject.maxSize), Integer.valueOf(selectObject.offset), Integer.valueOf(selectObject.CRC32)));
        this.mProgressInfo.setMaxObjectSizeInBytes(selectObject.maxSize);
        int i3 = ((this.mImageSizeInBytes + selectObject.maxSize) - 1) / selectObject.maxSize;
        if (selectObject.offset > 0) {
            try {
                i = selectObject.offset / selectObject.maxSize;
                int i4 = selectObject.maxSize * i;
                int i5 = selectObject.offset - i4;
                if (i5 == 0) {
                    i4 -= selectObject.maxSize;
                    i5 = selectObject.maxSize;
                }
                if (i4 > 0) {
                    this.mFirmwareStream.read(new byte[i4]);
                    this.mFirmwareStream.mark(selectObject.maxSize);
                }
                this.mFirmwareStream.read(new byte[i5]);
                if (((int) (((ArchiveInputStream) this.mFirmwareStream).getCrc32() & 4294967295L)) == selectObject.CRC32) {
                    logi(selectObject.offset + " bytes of data sent before, CRC match");
                    this.mService.sendLogBroadcast(10, selectObject.offset + " bytes of data sent before, CRC match");
                    this.mProgressInfo.setBytesSent(selectObject.offset);
                    this.mProgressInfo.setBytesReceived(selectObject.offset);
                    if (i5 != selectObject.maxSize || selectObject.offset >= this.mImageSizeInBytes) {
                        z = true;
                    } else {
                        logi("Executing data object (Op Code = 4)");
                        writeExecute();
                        this.mService.sendLogBroadcast(10, "Data object executed");
                    }
                } else {
                    logi(selectObject.offset + " bytes sent before, CRC does not match");
                    this.mService.sendLogBroadcast(15, selectObject.offset + " bytes sent before, CRC does not match");
                    this.mProgressInfo.setBytesSent(i4);
                    this.mProgressInfo.setBytesReceived(i4);
                    selectObject.offset = selectObject.offset - i5;
                    selectObject.CRC32 = 0;
                    this.mFirmwareStream.reset();
                    logi("Resuming from byte " + selectObject.offset + "...");
                    this.mService.sendLogBroadcast(10, "Resuming from byte " + selectObject.offset + "...");
                }
                z = false;
            } catch (IOException e) {
                loge("Error while reading firmware stream", e);
                this.mService.terminateConnection(bluetoothGatt2, 4100);
                return;
            }
        } else {
            this.mProgressInfo.setBytesSent(0);
            z = false;
            i = 0;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (selectObject.offset < this.mImageSizeInBytes) {
            boolean z3 = z;
            int i6 = 1;
            while (this.mProgressInfo.getAvailableObjectSizeIsBytes() > 0) {
                if (!z3) {
                    int availableObjectSizeIsBytes = this.mProgressInfo.getAvailableObjectSizeIsBytes();
                    StringBuilder sb = new StringBuilder();
                    z2 = z3;
                    sb.append("Creating Data object (Op Code = 1, Type = 2, Size = ");
                    sb.append(availableObjectSizeIsBytes);
                    sb.append(") (");
                    int i7 = i + 1;
                    sb.append(i7);
                    j2 = elapsedRealtime;
                    sb.append("/");
                    sb.append(i3);
                    sb.append(")");
                    logi(sb.toString());
                    writeCreateRequest(2, availableObjectSizeIsBytes);
                    this.mService.sendLogBroadcast(10, "Data object (" + i7 + "/" + i3 + ") created");
                    this.mService.sendLogBroadcast(10, "Uploading firmware...");
                } else {
                    j2 = elapsedRealtime;
                    this.mService.sendLogBroadcast(10, "Resuming uploading firmware...");
                    z2 = false;
                }
                try {
                    logi("Uploading firmware...");
                    uploadFirmwareImage(this.mPacketCharacteristic);
                    logi("Sending Calculate Checksum command (Op Code = 3)");
                    ObjectChecksum readChecksum = readChecksum();
                    logi(String.format(Locale.US, "Checksum received (Offset = %d, CRC = %08X)", Integer.valueOf(readChecksum.offset), Integer.valueOf(readChecksum.CRC32)));
                    this.mService.sendLogBroadcast(10, String.format(Locale.US, "Checksum received (Offset = %d, CRC = %08X)", Integer.valueOf(readChecksum.offset), Integer.valueOf(readChecksum.CRC32)));
                    int bytesSent = this.mProgressInfo.getBytesSent() - readChecksum.offset;
                    if (bytesSent > 0) {
                        logw(bytesSent + " bytes were lost!");
                        this.mService.sendLogBroadcast(15, bytesSent + " bytes were lost");
                        try {
                            this.mFirmwareStream.reset();
                            this.mFirmwareStream.read(new byte[(selectObject.maxSize - bytesSent)]);
                            this.mProgressInfo.setBytesSent(readChecksum.offset);
                            this.mPacketsBeforeNotification = 1;
                            setPacketReceiptNotifications(1);
                            this.mService.sendLogBroadcast(10, str2 + 1 + ")");
                        } catch (IOException e2) {
                            loge("Error while reading firmware stream", e2);
                            this.mService.terminateConnection(bluetoothGatt2, 4100);
                            return;
                        }
                    }
                    int crc32 = (int) (((ArchiveInputStream) this.mFirmwareStream).getCrc32() & 4294967295L);
                    if (crc32 != readChecksum.CRC32) {
                        str = str2;
                        String format = String.format(Locale.US, "CRC does not match! Expected %08X but found %08X.", Integer.valueOf(crc32), Integer.valueOf(readChecksum.CRC32));
                        if (i6 < 3) {
                            i6++;
                            String str3 = format + String.format(Locale.US, " Retrying...(%d/%d)", Integer.valueOf(i6), 3);
                            logi(str3);
                            this.mService.sendLogBroadcast(15, str3);
                            try {
                                this.mFirmwareStream.reset();
                                this.mProgressInfo.setBytesSent(((ArchiveInputStream) this.mFirmwareStream).getBytesRead());
                            } catch (IOException e3) {
                                loge("Error while resetting the firmware stream", e3);
                                this.mService.terminateConnection(bluetoothGatt2, 4100);
                                return;
                            }
                        } else {
                            loge(format);
                            this.mService.sendLogBroadcast(20, format);
                            this.mService.terminateConnection(bluetoothGatt2, 4109);
                            return;
                        }
                    } else if (bytesSent > 0) {
                        elapsedRealtime = j2;
                        z3 = true;
                    } else {
                        logi("Executing data object (Op Code = 4)");
                        writeExecute(this.mProgressInfo.isComplete());
                        this.mService.sendLogBroadcast(10, "Data object executed");
                        i++;
                        this.mFirmwareStream.mark(0);
                        str = str2;
                        i6 = 1;
                    }
                    z3 = z2;
                    elapsedRealtime = j2;
                    str2 = str;
                } catch (DeviceDisconnectedException e4) {
                    loge("Disconnected while sending data");
                    throw e4;
                }
            }
            j = elapsedRealtime;
        } else {
            j = elapsedRealtime;
            logi("Executing data object (Op Code = 4)");
            writeExecute(true);
            this.mService.sendLogBroadcast(10, "Data object executed");
        }
        long elapsedRealtime2 = SystemClock.elapsedRealtime();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Transfer of ");
        sb2.append(this.mProgressInfo.getBytesSent() - selectObject.offset);
        sb2.append(" bytes has taken ");
        long j3 = elapsedRealtime2 - j;
        sb2.append(j3);
        sb2.append(" ms");
        logi(sb2.toString());
        this.mService.sendLogBroadcast(10, "Upload completed in " + j3 + " ms");
    }

    private int getStatusCode(byte[] bArr, int i) throws UnknownResponseException {
        if (bArr != null && bArr.length >= 3 && bArr[0] == 96 && bArr[1] == i && (bArr[2] == 1 || bArr[2] == 2 || bArr[2] == 3 || bArr[2] == 4 || bArr[2] == 5 || bArr[2] == 7 || bArr[2] == 8 || bArr[2] == 10 || bArr[2] == 11)) {
            return bArr[2];
        }
        throw new UnknownResponseException("Invalid response received", bArr, 96, i);
    }

    private void setNumberOfPackets(byte[] bArr, int i) {
        bArr[1] = (byte) (i & 255);
        bArr[2] = (byte) ((i >> 8) & 255);
    }

    private void setObjectSize(byte[] bArr, int i) {
        bArr[2] = (byte) (i & 255);
        bArr[3] = (byte) ((i >> 8) & 255);
        bArr[4] = (byte) ((i >> 16) & 255);
        bArr[5] = (byte) ((i >> 24) & 255);
    }

    private void setPacketReceiptNotifications(int i) throws DfuException, DeviceDisconnectedException, UploadAbortedException, UnknownResponseException, RemoteDfuException {
        if (this.mConnected) {
            logi("Sending the number of packets before notifications (Op Code = 2, Value = " + i + ")");
            setNumberOfPackets(OP_CODE_PACKET_RECEIPT_NOTIF_REQ, i);
            writeOpCode(this.mControlPointCharacteristic, OP_CODE_PACKET_RECEIPT_NOTIF_REQ);
            byte[] readNotificationResponse = readNotificationResponse();
            int statusCode = getStatusCode(readNotificationResponse, 2);
            if (statusCode == 11) {
                throw new RemoteDfuExtendedErrorException("Sending the number of packets failed", readNotificationResponse[3]);
            } else if (statusCode != 1) {
                throw new RemoteDfuException("Sending the number of packets failed", statusCode);
            }
        } else {
            throw new DeviceDisconnectedException("Unable to read Checksum: device disconnected");
        }
    }

    private void writeOpCode(BluetoothGattCharacteristic bluetoothGattCharacteristic, byte[] bArr) throws DeviceDisconnectedException, DfuException, UploadAbortedException {
        writeOpCode(bluetoothGattCharacteristic, bArr, false);
    }

    private void writeCreateRequest(int i, int i2) throws DeviceDisconnectedException, DfuException, UploadAbortedException, RemoteDfuException, UnknownResponseException {
        if (this.mConnected) {
            byte[] bArr = i == 1 ? OP_CODE_CREATE_COMMAND : OP_CODE_CREATE_DATA;
            setObjectSize(bArr, i2);
            writeOpCode(this.mControlPointCharacteristic, bArr);
            byte[] readNotificationResponse = readNotificationResponse();
            int statusCode = getStatusCode(readNotificationResponse, 1);
            if (statusCode == 11) {
                throw new RemoteDfuExtendedErrorException("Creating Command object failed", readNotificationResponse[3]);
            } else if (statusCode != 1) {
                throw new RemoteDfuException("Creating Command object failed", statusCode);
            }
        } else {
            throw new DeviceDisconnectedException("Unable to create object: device disconnected");
        }
    }

    private ObjectInfo selectObject(int i) throws DeviceDisconnectedException, DfuException, UploadAbortedException, RemoteDfuException, UnknownResponseException {
        if (this.mConnected) {
            byte[] bArr = OP_CODE_SELECT_OBJECT;
            bArr[1] = (byte) i;
            writeOpCode(this.mControlPointCharacteristic, bArr);
            byte[] readNotificationResponse = readNotificationResponse();
            int statusCode = getStatusCode(readNotificationResponse, 6);
            if (statusCode == 11) {
                throw new RemoteDfuExtendedErrorException("Selecting object failed", readNotificationResponse[3]);
            } else if (statusCode == 1) {
                ObjectInfo objectInfo = new ObjectInfo();
                objectInfo.maxSize = this.mControlPointCharacteristic.getIntValue(20, 3).intValue();
                objectInfo.offset = this.mControlPointCharacteristic.getIntValue(20, 7).intValue();
                objectInfo.CRC32 = this.mControlPointCharacteristic.getIntValue(20, 11).intValue();
                return objectInfo;
            } else {
                throw new RemoteDfuException("Selecting object failed", statusCode);
            }
        } else {
            throw new DeviceDisconnectedException("Unable to read object info: device disconnected");
        }
    }

    private ObjectChecksum readChecksum() throws DeviceDisconnectedException, DfuException, UploadAbortedException, RemoteDfuException, UnknownResponseException {
        if (this.mConnected) {
            writeOpCode(this.mControlPointCharacteristic, OP_CODE_CALCULATE_CHECKSUM);
            byte[] readNotificationResponse = readNotificationResponse();
            int statusCode = getStatusCode(readNotificationResponse, 3);
            if (statusCode == 11) {
                throw new RemoteDfuExtendedErrorException("Receiving Checksum failed", readNotificationResponse[3]);
            } else if (statusCode == 1) {
                ObjectChecksum objectChecksum = new ObjectChecksum();
                objectChecksum.offset = this.mControlPointCharacteristic.getIntValue(20, 3).intValue();
                objectChecksum.CRC32 = this.mControlPointCharacteristic.getIntValue(20, 7).intValue();
                return objectChecksum;
            } else {
                throw new RemoteDfuException("Receiving Checksum failed", statusCode);
            }
        } else {
            throw new DeviceDisconnectedException("Unable to read Checksum: device disconnected");
        }
    }

    private void writeExecute() throws DfuException, DeviceDisconnectedException, UploadAbortedException, UnknownResponseException, RemoteDfuException {
        if (this.mConnected) {
            writeOpCode(this.mControlPointCharacteristic, OP_CODE_EXECUTE);
            byte[] readNotificationResponse = readNotificationResponse();
            int statusCode = getStatusCode(readNotificationResponse, 4);
            if (statusCode == 11) {
                throw new RemoteDfuExtendedErrorException("Executing object failed", readNotificationResponse[3]);
            } else if (statusCode != 1) {
                throw new RemoteDfuException("Executing object failed", statusCode);
            }
        } else {
            throw new DeviceDisconnectedException("Unable to read Checksum: device disconnected");
        }
    }

    private void writeExecute(boolean z) throws DfuException, DeviceDisconnectedException, UploadAbortedException, UnknownResponseException, RemoteDfuException {
        try {
            writeExecute();
        } catch (RemoteDfuException e) {
            if (!z || e.getErrorNumber() != 5) {
                throw e;
            }
            logw(e.getMessage() + ": " + SecureDfuError.parse(UIMsg.m_AppUI.MSG_CITY_SUP_DOM));
            if (this.mFileType == 1) {
                logw("Are you sure your new SoftDevice is API compatible with the updated one? If not, update the bootloader as well");
            }
            this.mService.sendLogBroadcast(15, String.format(Locale.US, "Remote DFU error: %s. SD busy? Retrying...", SecureDfuError.parse(UIMsg.m_AppUI.MSG_CITY_SUP_DOM)));
            logi("SD busy? Retrying...");
            logi("Executing data object (Op Code = 4)");
            writeExecute();
        }
    }

    /* renamed from: no.nordicsemi.android.dfu.SecureDfuImpl$ObjectInfo */
    private class ObjectInfo extends ObjectChecksum {
        protected int maxSize;

        private ObjectInfo() {
            super();
        }
    }

    /* renamed from: no.nordicsemi.android.dfu.SecureDfuImpl$ObjectChecksum */
    private class ObjectChecksum {
        protected int CRC32;
        protected int offset;

        private ObjectChecksum() {
        }
    }
}
