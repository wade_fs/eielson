package no.nordicsemi.android.other;

import android.os.Environment;
import android.text.TextUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/* renamed from: no.nordicsemi.android.other.NodicLog */
public class NodicLog {
    public static final String APP_ROOT_PATH = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/veryfit2.2");
    private static final String FILE_TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSZ";
    public static final String LOG_PATH = (APP_ROOT_PATH + "/log");
    public static final String OTALog = "ota.log";
    public static final String OTAPath = (LOG_PATH + "/Ota");

    /* renamed from: e */
    public static void m8143e(String str, String str2) {
    }

    /* renamed from: i */
    public static void m8144i(String str, String str2) {
    }

    private static void writeToFile(String str, String str2) {
        writeLogInfotoFile(OTAPath, "    [" + getLogTimeString() + "]    " + "    [" + str + "]    " + str2, OTALog);
    }

    private static synchronized String getLogTimeString() {
        String format;
        Class<NodicLog> cls = NodicLog.class;
        synchronized (cls) {
            Date time = Calendar.getInstance().getTime();
            synchronized (cls) {
                format = new SimpleDateFormat(FILE_TIMESTAMP_PATTERN, Locale.getDefault()).format(time);
            }
            return format;
        }
        return format;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    public static void writeLogInfotoFile(String str, String str2, String str3) {
        File file;
        if (TextUtils.isEmpty(str)) {
            file = new File(APP_ROOT_PATH);
        } else {
            file = new File(str);
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(file, str3 + ".txt");
        BufferedWriter bufferedWriter = null;
        try {
            BufferedWriter bufferedWriter2 = new BufferedWriter(new FileWriter(file2, true));
            try {
                bufferedWriter2.write(str2);
                bufferedWriter2.newLine();
                bufferedWriter2.flush();
                closeWrite(bufferedWriter2);
            } catch (IOException unused) {
                bufferedWriter = bufferedWriter2;
                closeWrite(bufferedWriter);
            } catch (Throwable th) {
                th = th;
                bufferedWriter = bufferedWriter2;
                closeWrite(bufferedWriter);
                throw th;
            }
        } catch (IOException unused2) {
            closeWrite(bufferedWriter);
        } catch (Throwable th2) {
            th = th2;
            closeWrite(bufferedWriter);
            throw th;
        }
    }

    public static void closeWrite(BufferedWriter bufferedWriter) {
        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
