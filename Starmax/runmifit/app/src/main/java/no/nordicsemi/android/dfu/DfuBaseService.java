package no.nordicsemi.android.dfu;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.internal.view.SupportMenu;
import android.util.Log;
import com.google.android.gms.common.util.CrashUtils;
import com.runmifit.android.app.Constants;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Locale;
import no.nordicsemi.android.dfu.DfuProgressInfo;
import no.nordicsemi.android.dfu.internal.ArchiveInputStream;
import no.nordicsemi.android.dfu.internal.HexInputStream;
import no.nordicsemi.android.other.WritePeriodController;

/* renamed from: no.nordicsemi.android.dfu.DfuBaseService */
public abstract class DfuBaseService extends IntentService implements DfuProgressInfo.ProgressListener {
    public static final int ACTION_ABORT = 2;
    public static final int ACTION_PAUSE = 0;
    public static final int ACTION_RESUME = 1;
    public static final String BROADCAST_ACTION = "no.nordicsemi.android.dfu.broadcast.BROADCAST_ACTION";
    public static final String BROADCAST_ERROR = "no.nordicsemi.android.dfu.broadcast.BROADCAST_ERROR";
    public static final String BROADCAST_LOG = "no.nordicsemi.android.dfu.broadcast.BROADCAST_LOG";
    public static final String BROADCAST_PROGRESS = "no.nordicsemi.android.dfu.broadcast.BROADCAST_PROGRESS";
    static boolean DEBUG = false;
    public static final int ERROR_BLUETOOTH_DISABLED = 4106;
    public static final int ERROR_CONNECTION_MASK = 16384;
    public static final int ERROR_CONNECTION_STATE_MASK = 32768;
    public static final int ERROR_CRC_ERROR = 4109;
    public static final int ERROR_DEVICE_DISCONNECTED = 4096;
    public static final int ERROR_DEVICE_NOT_BONDED = 4110;
    public static final int ERROR_FILE_ERROR = 4098;
    public static final int ERROR_FILE_INVALID = 4099;
    public static final int ERROR_FILE_IO_EXCEPTION = 4100;
    public static final int ERROR_FILE_NOT_FOUND = 4097;
    public static final int ERROR_FILE_SIZE_INVALID = 4108;
    public static final int ERROR_FILE_TYPE_UNSUPPORTED = 4105;
    public static final int ERROR_INIT_PACKET_REQUIRED = 4107;
    public static final int ERROR_INVALID_RESPONSE = 4104;
    public static final int ERROR_MASK = 4096;
    public static final int ERROR_REMOTE_MASK = 8192;
    public static final int ERROR_REMOTE_TYPE_LEGACY = 256;
    public static final int ERROR_REMOTE_TYPE_SECURE = 512;
    public static final int ERROR_REMOTE_TYPE_SECURE_BUTTONLESS = 2048;
    public static final int ERROR_REMOTE_TYPE_SECURE_EXTENDED = 1024;
    public static final int ERROR_SERVICE_DISCOVERY_NOT_STARTED = 4101;
    public static final int ERROR_SERVICE_NOT_FOUND = 4102;
    public static final int ERROR_TYPE_COMMUNICATION = 2;
    public static final int ERROR_TYPE_COMMUNICATION_STATE = 1;
    public static final int ERROR_TYPE_DFU_REMOTE = 3;
    public static final int ERROR_TYPE_OTHER = 0;
    public static final String EXTRA_ACTION = "no.nordicsemi.android.dfu.extra.EXTRA_ACTION";
    private static final String EXTRA_ATTEMPT = "no.nordicsemi.android.dfu.extra.EXTRA_ATTEMPT";
    public static final String EXTRA_AVG_SPEED_B_PER_MS = "no.nordicsemi.android.dfu.extra.EXTRA_AVG_SPEED_B_PER_MS";
    public static final String EXTRA_CUSTOM_UUIDS_FOR_BUTTONLESS_DFU_WITHOUT_BOND_SHARING = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_BUTTONLESS_DFU_WITHOUT_BOND_SHARING";
    public static final String EXTRA_CUSTOM_UUIDS_FOR_BUTTONLESS_DFU_WITH_BOND_SHARING = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_BUTTONLESS_DFU_WITH_BOND_SHARING";
    public static final String EXTRA_CUSTOM_UUIDS_FOR_EXPERIMENTAL_BUTTONLESS_DFU = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_EXPERIMENTAL_BUTTONLESS_DFU";
    public static final String EXTRA_CUSTOM_UUIDS_FOR_LEGACY_DFU = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_LEGACY_DFU";
    public static final String EXTRA_CUSTOM_UUIDS_FOR_SECURE_DFU = "no.nordicsemi.android.dfu.extra.EXTRA_CUSTOM_UUIDS_FOR_SECURE_DFU";
    public static final String EXTRA_DATA = "no.nordicsemi.android.dfu.extra.EXTRA_DATA";
    public static final String EXTRA_DEVICE_ADDRESS = "no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS";
    public static final String EXTRA_DEVICE_NAME = "no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_NAME";
    public static final String EXTRA_DISABLE_NOTIFICATION = "no.nordicsemi.android.dfu.extra.EXTRA_DISABLE_NOTIFICATION";
    public static final String EXTRA_ERROR_TYPE = "no.nordicsemi.android.dfu.extra.EXTRA_ERROR_TYPE";
    public static final String EXTRA_FILE_MIME_TYPE = "no.nordicsemi.android.dfu.extra.EXTRA_MIME_TYPE";
    public static final String EXTRA_FILE_PATH = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_PATH";
    public static final String EXTRA_FILE_RES_ID = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_RES_ID";
    public static final String EXTRA_FILE_TYPE = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_TYPE";
    public static final String EXTRA_FILE_URI = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_URI";
    public static final String EXTRA_FORCE_DFU = "no.nordicsemi.android.dfu.extra.EXTRA_FORCE_DFU";
    public static final String EXTRA_FOREGROUND_SERVICE = "no.nordicsemi.android.dfu.extra.EXTRA_FOREGROUND_SERVICE";
    public static final String EXTRA_INIT_FILE_PATH = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_PATH";
    public static final String EXTRA_INIT_FILE_RES_ID = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_RES_ID";
    public static final String EXTRA_INIT_FILE_URI = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_URI";
    public static final String EXTRA_KEEP_BOND = "no.nordicsemi.android.dfu.extra.EXTRA_KEEP_BOND";
    public static final String EXTRA_LOG_LEVEL = "no.nordicsemi.android.dfu.extra.EXTRA_LOG_LEVEL";
    public static final String EXTRA_LOG_MESSAGE = "no.nordicsemi.android.dfu.extra.EXTRA_LOG_INFO";
    public static final String EXTRA_MTU = "no.nordicsemi.android.dfu.extra.EXTRA_MTU";
    public static final String EXTRA_PACKET_RECEIPT_NOTIFICATIONS_ENABLED = "no.nordicsemi.android.dfu.extra.EXTRA_PRN_ENABLED";
    public static final String EXTRA_PACKET_RECEIPT_NOTIFICATIONS_VALUE = "no.nordicsemi.android.dfu.extra.EXTRA_PRN_VALUE";
    public static final String EXTRA_PARTS_TOTAL = "no.nordicsemi.android.dfu.extra.EXTRA_PARTS_TOTAL";
    public static final String EXTRA_PART_CURRENT = "no.nordicsemi.android.dfu.extra.EXTRA_PART_CURRENT";
    public static final String EXTRA_PROGRESS = "no.nordicsemi.android.dfu.extra.EXTRA_PROGRESS";
    public static final String EXTRA_RESTORE_BOND = "no.nordicsemi.android.dfu.extra.EXTRA_RESTORE_BOND";
    public static final String EXTRA_SPEED_B_PER_MS = "no.nordicsemi.android.dfu.extra.EXTRA_SPEED_B_PER_MS";
    public static final String EXTRA_UNSAFE_EXPERIMENTAL_BUTTONLESS_DFU = "no.nordicsemi.android.dfu.extra.EXTRA_UNSAFE_EXPERIMENTAL_BUTTONLESS_DFU";
    public static final int LOG_LEVEL_APPLICATION = 10;
    public static final int LOG_LEVEL_DEBUG = 0;
    public static final int LOG_LEVEL_ERROR = 20;
    public static final int LOG_LEVEL_INFO = 5;
    public static final int LOG_LEVEL_VERBOSE = 1;
    public static final int LOG_LEVEL_WARNING = 15;
    public static final String MIME_TYPE_OCTET_STREAM = "application/octet-stream";
    public static final String MIME_TYPE_ZIP = "application/zip";
    public static final String NOTIFICATION_CHANNEL_DFU = "dfu";
    public static final int NOTIFICATION_ID = 283;
    public static final int PROGRESS_ABORTED = -7;
    public static final int PROGRESS_COMPLETED = -6;
    public static final int PROGRESS_CONNECTING = -1;
    public static final int PROGRESS_DISCONNECTING = -5;
    public static final int PROGRESS_ENABLING_DFU_MODE = -3;
    public static final int PROGRESS_STARTING = -2;
    public static final int PROGRESS_VALIDATING = -4;
    protected static final int STATE_CLOSED = -5;
    protected static final int STATE_CONNECTED = -2;
    protected static final int STATE_CONNECTED_AND_READY = -3;
    protected static final int STATE_CONNECTING = -1;
    protected static final int STATE_DISCONNECTED = 0;
    protected static final int STATE_DISCONNECTING = -4;
    private static final String TAG = "DfuBaseService";
    public static final int TYPE_APPLICATION = 4;
    public static final int TYPE_AUTO = 0;
    public static final int TYPE_BOOTLOADER = 2;
    public static final int TYPE_SOFT_DEVICE = 1;
    /* access modifiers changed from: private */
    public final Object discoverServiceLock = new Object();
    /* access modifiers changed from: private */
    public boolean mAborted;
    private BluetoothAdapter mBluetoothAdapter;
    private final BroadcastReceiver mBondStateBroadcastReceiver = new BroadcastReceiver() {
        /* class no.nordicsemi.android.dfu.DfuBaseService.C34122 */

        public void onReceive(Context context, Intent intent) {
            int intExtra;
            if (((BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE")).getAddress().equals(DfuBaseService.this.mDeviceAddress) && (intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1)) != 11 && DfuBaseService.this.mDfuServiceImpl != null) {
                DfuBaseService.this.mDfuServiceImpl.onBondStateChanged(intExtra);
            }
        }
    };
    protected int mConnectionState;
    private final BroadcastReceiver mConnectionStateBroadcastReceiver = new BroadcastReceiver() {
        /* class no.nordicsemi.android.dfu.DfuBaseService.C34133 */

        public void onReceive(Context context, Intent intent) {
            if (((BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE")).getAddress().equals(DfuBaseService.this.mDeviceAddress)) {
                String action = intent.getAction();
                DfuBaseService dfuBaseService = DfuBaseService.this;
                dfuBaseService.logi("Action received: " + action);
                DfuBaseService dfuBaseService2 = DfuBaseService.this;
                dfuBaseService2.sendLogBroadcast(0, "[Broadcast] Action received: " + action);
            }
        }
    };
    /* access modifiers changed from: private */
    public String mDeviceAddress;
    private String mDeviceName;
    private final BroadcastReceiver mDfuActionReceiver = new BroadcastReceiver() {
        /* class no.nordicsemi.android.dfu.DfuBaseService.C34111 */

        public void onReceive(Context context, Intent intent) {
            int intExtra = intent.getIntExtra(DfuBaseService.EXTRA_ACTION, 0);
            DfuBaseService dfuBaseService = DfuBaseService.this;
            dfuBaseService.logi("User action received: " + intExtra);
            if (intExtra == 0) {
                DfuBaseService.this.sendLogBroadcast(15, "[Broadcast] Pause action received");
                if (DfuBaseService.this.mDfuServiceImpl != null) {
                    DfuBaseService.this.mDfuServiceImpl.pause();
                }
            } else if (intExtra == 1) {
                DfuBaseService.this.sendLogBroadcast(15, "[Broadcast] Resume action received");
                if (DfuBaseService.this.mDfuServiceImpl != null) {
                    DfuBaseService.this.mDfuServiceImpl.resume();
                }
            } else if (intExtra == 2) {
                DfuBaseService.this.sendLogBroadcast(15, "[Broadcast] Abort action received");
                DfuBaseService.this.loge("[Broadcast] Abort action received");
                boolean unused = DfuBaseService.this.mAborted = true;
                if (DfuBaseService.this.mDfuServiceImpl != null) {
                    DfuBaseService.this.mDfuServiceImpl.abort();
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public DfuCallback mDfuServiceImpl;
    private boolean mDisableNotification;
    /* access modifiers changed from: private */
    public int mError;
    private InputStream mFirmwareInputStream;
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        /* class no.nordicsemi.android.dfu.DfuBaseService.C34144 */

        public void onConnectionStateChange(final BluetoothGatt bluetoothGatt, int i, int i2) {
            DfuBaseService dfuBaseService = DfuBaseService.this;
            dfuBaseService.loge("!!!!!!status=" + i + ",newState=" + i2);
            if (i != 0) {
                if (i == 8 || i == 19) {
                    DfuBaseService dfuBaseService2 = DfuBaseService.this;
                    dfuBaseService2.logw("Target device disconnected with status: " + i);
                } else {
                    DfuBaseService dfuBaseService3 = DfuBaseService.this;
                    dfuBaseService3.loge("Connection state change error: " + i + " newState: " + i2);
                }
                if (i2 == 0) {
                    DfuBaseService dfuBaseService4 = DfuBaseService.this;
                    dfuBaseService4.mConnectionState = 0;
                    if (dfuBaseService4.mDfuServiceImpl != null) {
                        DfuBaseService.this.mDfuServiceImpl.getGattCallback().onDisconnected();
                    }
                }
                int unused = DfuBaseService.this.mError = i | 32768;
            } else if (i2 == 2) {
                DfuBaseService.this.logi("Connected to GATT server");
                DfuBaseService dfuBaseService5 = DfuBaseService.this;
                dfuBaseService5.sendLogBroadcast(5, "Connected to " + DfuBaseService.this.mDeviceAddress);
                DfuBaseService.this.mConnectionState = -2;
                if (bluetoothGatt.getDevice().getBondState() == 12) {
                    DfuBaseService.this.logi("Waiting 1600 ms for a possible Service Changed indication...");
                    DfuBaseService.this.waitFor(1600);
                }
                DfuBaseService.this.sendLogBroadcast(1, "Discovering services...");
                DfuBaseService.this.sendLogBroadcast(0, "gatt.discoverServices()");
                DfuBaseService.this.logi("Waiting 2000 ms for discoverServices");
                final boolean[] zArr = new boolean[1];
                DfuBaseService.this.mainThreadHandler.postDelayed(new Runnable() {
                    /* class no.nordicsemi.android.dfu.DfuBaseService.C34144.C34151 */

                    public void run() {
                        zArr[0] = bluetoothGatt.discoverServices();
                        synchronized (DfuBaseService.this.discoverServiceLock) {
                            DfuBaseService.this.discoverServiceLock.notify();
                        }
                        DfuBaseService dfuBaseService = DfuBaseService.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Attempting to start service discovery... ");
                        sb.append(zArr[0] ? "succeed" : "failed");
                        dfuBaseService.logi(sb.toString());
                    }
                }, 2000);
                synchronized (DfuBaseService.this.discoverServiceLock) {
                    try {
                        DfuBaseService.this.discoverServiceLock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!zArr[0]) {
                    int unused2 = DfuBaseService.this.mError = 4101;
                } else {
                    return;
                }
            } else if (i2 == 0) {
                DfuBaseService.this.logi("Disconnected from GATT server");
                DfuBaseService dfuBaseService6 = DfuBaseService.this;
                dfuBaseService6.mConnectionState = 0;
                if (dfuBaseService6.mDfuServiceImpl != null) {
                    DfuBaseService.this.mDfuServiceImpl.getGattCallback().onDisconnected();
                }
            }
            synchronized (DfuBaseService.this.mLock) {
                DfuBaseService.this.mLock.notifyAll();
            }
        }

        public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
            if (i == 0) {
                DfuBaseService.this.logi("Services discovered");
                DfuBaseService.this.mConnectionState = -3;
            } else {
                DfuBaseService dfuBaseService = DfuBaseService.this;
                dfuBaseService.loge("Service discovery error: " + i);
                int unused = DfuBaseService.this.mError = i | 16384;
            }
            synchronized (DfuBaseService.this.mLock) {
                DfuBaseService.this.mLock.notifyAll();
            }
        }

        public void onCharacteristicWrite(final BluetoothGatt bluetoothGatt, final BluetoothGattCharacteristic bluetoothGattCharacteristic, final int i) {
            WritePeriodController.deviceResponse(new Runnable() {
                /* class no.nordicsemi.android.dfu.DfuBaseService.C34144.C34162 */

                public void run() {
                    if (DfuBaseService.this.mDfuServiceImpl != null) {
                        DfuBaseService.this.mDfuServiceImpl.getGattCallback().onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
                    }
                }
            });
        }

        public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
            if (DfuBaseService.this.mDfuServiceImpl != null) {
                DfuBaseService.this.mDfuServiceImpl.getGattCallback().onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
            }
        }

        public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            if (DfuBaseService.this.mDfuServiceImpl != null) {
                DfuBaseService.this.mDfuServiceImpl.getGattCallback().onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
            }
        }

        public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
            if (DfuBaseService.this.mDfuServiceImpl != null) {
                DfuBaseService.this.mDfuServiceImpl.getGattCallback().onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
            }
        }

        public void onDescriptorRead(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
            if (DfuBaseService.this.mDfuServiceImpl != null) {
                DfuBaseService.this.mDfuServiceImpl.getGattCallback().onDescriptorRead(bluetoothGatt, bluetoothGattDescriptor, i);
            }
        }

        public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
            if (DfuBaseService.this.mDfuServiceImpl != null) {
                DfuBaseService.this.mDfuServiceImpl.getGattCallback().onMtuChanged(bluetoothGatt, i, i2);
            }
        }

        public void onPhyUpdate(BluetoothGatt bluetoothGatt, int i, int i2, int i3) {
            if (DfuBaseService.this.mDfuServiceImpl != null) {
                DfuBaseService.this.mDfuServiceImpl.getGattCallback().onPhyUpdate(bluetoothGatt, i, i2, i3);
            }
        }
    };
    private InputStream mInitFileInputStream;
    private long mLastNotificationTime;
    private int mLastProgress = -1;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    DfuProgressInfo mProgressInfo;
    /* access modifiers changed from: private */
    public Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    /* access modifiers changed from: protected */
    public abstract Class<? extends Activity> getNotificationTarget();

    /* access modifiers changed from: protected */
    public boolean isDebug() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void updateErrorNotification(NotificationCompat.Builder builder) {
    }

    /* access modifiers changed from: protected */
    public void updateForegroundNotification(NotificationCompat.Builder builder) {
    }

    public DfuBaseService() {
        super(TAG);
    }

    private static IntentFilter makeDfuActionIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BROADCAST_ACTION);
        return intentFilter;
    }

    public void onCreate() {
        super.onCreate();
        DEBUG = isDebug();
        logi("DFU service created. Version: 1.7.0");
        initialize();
        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this);
        IntentFilter makeDfuActionIntentFilter = makeDfuActionIntentFilter();
        instance.registerReceiver(this.mDfuActionReceiver, makeDfuActionIntentFilter);
        registerReceiver(this.mDfuActionReceiver, makeDfuActionIntentFilter);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        intentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED");
        intentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        registerReceiver(this.mConnectionStateBroadcastReceiver, intentFilter);
        registerReceiver(this.mBondStateBroadcastReceiver, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
    }

    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        ((NotificationManager) getSystemService("notification")).cancel(NOTIFICATION_ID);
        stopSelf();
    }

    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.mDfuActionReceiver);
        unregisterReceiver(this.mDfuActionReceiver);
        unregisterReceiver(this.mConnectionStateBroadcastReceiver);
        unregisterReceiver(this.mBondStateBroadcastReceiver);
        try {
            if (this.mFirmwareInputStream != null) {
                this.mFirmwareInputStream.close();
            }
            if (this.mInitFileInputStream != null) {
                this.mInitFileInputStream.close();
            }
        } catch (IOException unused) {
        } catch (Throwable th) {
            this.mFirmwareInputStream = null;
            this.mInitFileInputStream = null;
            throw th;
        }
        this.mFirmwareInputStream = null;
        this.mInitFileInputStream = null;
        logi("DFU service destroyed");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:186:0x0383 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:215:0x03d4 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v6, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v9, resolved type: no.nordicsemi.android.dfu.DfuService} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v10, resolved type: no.nordicsemi.android.dfu.DfuService} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v11, resolved type: no.nordicsemi.android.dfu.DfuService} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v12, resolved type: no.nordicsemi.android.dfu.DfuService} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v14, resolved type: no.nordicsemi.android.dfu.DfuService} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v15, resolved type: no.nordicsemi.android.dfu.DfuService} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v13, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v16, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v24, resolved type: java.io.InputStream} */
    /* JADX WARN: Type inference failed for: r14v8 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x03bd, code lost:
        if (r14 != 0) goto L_0x03bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x044b, code lost:
        if (r14 != null) goto L_0x03bf;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:228:0x0452 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x03de A[Catch:{ UploadAbortedException -> 0x0452, DeviceDisconnectedException -> 0x0432, DfuException -> 0x03d1, all -> 0x044f }] */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x0401 A[Catch:{ UploadAbortedException -> 0x0452, DeviceDisconnectedException -> 0x0432, DfuException -> 0x03d1, all -> 0x044f }] */
    /* JADX WARNING: Removed duplicated region for block: B:222:0x0431 A[Catch:{ UploadAbortedException -> 0x0452, DeviceDisconnectedException -> 0x0432, DfuException -> 0x03d1, all -> 0x044f }] */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x0465 A[SYNTHETIC, Splitter:B:231:0x0465] */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x046a  */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x0470 A[SYNTHETIC, Splitter:B:236:0x0470] */
    /* JADX WARNING: Removed duplicated region for block: B:282:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onHandleIntent(android.content.Intent r22) {
        /*
            r21 = this;
            r1 = r21
            r8 = r22
            java.lang.String r2 = "no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_ADDRESS"
            java.lang.String r2 = r8.getStringExtra(r2)
            java.lang.String r3 = "no.nordicsemi.android.dfu.extra.EXTRA_DEVICE_NAME"
            java.lang.String r3 = r8.getStringExtra(r3)
            r9 = 0
            java.lang.String r4 = "no.nordicsemi.android.dfu.extra.EXTRA_DISABLE_NOTIFICATION"
            boolean r10 = r8.getBooleanExtra(r4, r9)
            r11 = 1
            java.lang.String r4 = "no.nordicsemi.android.dfu.extra.EXTRA_FOREGROUND_SERVICE"
            boolean r12 = r8.getBooleanExtra(r4, r11)
            java.lang.String r4 = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_PATH"
            java.lang.String r4 = r8.getStringExtra(r4)
            java.lang.String r5 = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_URI"
            android.os.Parcelable r5 = r8.getParcelableExtra(r5)
            android.net.Uri r5 = (android.net.Uri) r5
            java.lang.String r6 = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_RES_ID"
            int r6 = r8.getIntExtra(r6, r9)
            java.lang.String r7 = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_PATH"
            java.lang.String r7 = r8.getStringExtra(r7)
            java.lang.String r13 = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_URI"
            android.os.Parcelable r13 = r8.getParcelableExtra(r13)
            android.net.Uri r13 = (android.net.Uri) r13
            java.lang.String r14 = "no.nordicsemi.android.dfu.extra.EXTRA_INIT_FILE_RES_ID"
            int r14 = r8.getIntExtra(r14, r9)
            java.lang.String r15 = "no.nordicsemi.android.dfu.extra.EXTRA_FILE_TYPE"
            int r15 = r8.getIntExtra(r15, r9)
            if (r4 == 0) goto L_0x0061
            if (r15 != 0) goto L_0x0061
            java.util.Locale r15 = java.util.Locale.US
            java.lang.String r15 = r4.toLowerCase(r15)
            java.lang.String r9 = "zip"
            boolean r9 = r15.endsWith(r9)
            if (r9 == 0) goto L_0x0060
            r15 = 0
            goto L_0x0061
        L_0x0060:
            r15 = 4
        L_0x0061:
            java.lang.String r9 = "no.nordicsemi.android.dfu.extra.EXTRA_MIME_TYPE"
            java.lang.String r9 = r8.getStringExtra(r9)
            java.lang.String r11 = "application/zip"
            java.lang.String r8 = "application/octet-stream"
            if (r9 == 0) goto L_0x006e
            goto L_0x0073
        L_0x006e:
            if (r15 != 0) goto L_0x0072
            r9 = r11
            goto L_0x0073
        L_0x0072:
            r9 = r8
        L_0x0073:
            r16 = r15 & -8
            r17 = r14
            if (r16 > 0) goto L_0x0513
            boolean r16 = r11.equals(r9)
            if (r16 != 0) goto L_0x0087
            boolean r16 = r8.equals(r9)
            if (r16 != 0) goto L_0x0087
            goto L_0x0513
        L_0x0087:
            boolean r8 = r8.equals(r9)
            r14 = 2
            if (r8 == 0) goto L_0x00a6
            r8 = 1
            if (r15 == r8) goto L_0x00a6
            if (r15 == r14) goto L_0x00a6
            r8 = 4
            if (r15 == r8) goto L_0x00a6
            java.lang.String r2 = "Unable to determine file type"
            r1.logw(r2)
            r3 = 15
            r1.sendLogBroadcast(r3, r2)
            r2 = 4105(0x1009, float:5.752E-42)
            r1.report(r2)
            return
        L_0x00a6:
            if (r10 != 0) goto L_0x00b7
            java.lang.Class r8 = r21.getNotificationTarget()
            if (r8 == 0) goto L_0x00af
            goto L_0x00b7
        L_0x00af:
            java.lang.NullPointerException r2 = new java.lang.NullPointerException
            java.lang.String r3 = "getNotificationTarget() must not return null if notifications are enabled"
            r2.<init>(r3)
            throw r2
        L_0x00b7:
            if (r12 != 0) goto L_0x00c4
            int r8 = android.os.Build.VERSION.SDK_INT
            r14 = 26
            if (r8 < r14) goto L_0x00c4
            java.lang.String r8 = "Foreground service disabled. Android Oreo or newer may kill a background service few moments after user closes the application.\nConsider enabling foreground service using DfuServiceInitiator#setForeground(boolean)"
            r1.logw(r8)
        L_0x00c4:
            no.nordicsemi.android.dfu.UuidHelper.assignCustomUuids(r22)
            r1.mDeviceAddress = r2
            r1.mDeviceName = r3
            r1.mDisableNotification = r10
            r3 = 0
            r1.mConnectionState = r3
            r1.mError = r3
            android.content.SharedPreferences r3 = android.preference.PreferenceManager.getDefaultSharedPreferences(r21)
            r8 = 4096(0x1000, float:5.74E-42)
            java.lang.String r14 = java.lang.String.valueOf(r8)
            java.lang.String r8 = "settings_mbr_size"
            java.lang.String r3 = r3.getString(r8, r14)
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ NumberFormatException -> 0x00ec }
            if (r3 >= 0) goto L_0x00ea
            r8 = 0
            goto L_0x00ee
        L_0x00ea:
            r8 = r3
            goto L_0x00ee
        L_0x00ec:
            r8 = 4096(0x1000, float:5.74E-42)
        L_0x00ee:
            if (r12 == 0) goto L_0x00f3
            r21.startForeground()
        L_0x00f3:
            java.lang.String r3 = "DFU service started"
            r14 = 1
            r1.sendLogBroadcast(r14, r3)
            java.io.InputStream r3 = r1.mFirmwareInputStream
            java.io.InputStream r14 = r1.mInitFileInputStream
            if (r3 != 0) goto L_0x0102
            r18 = 1
            goto L_0x0104
        L_0x0102:
            r18 = 0
        L_0x0104:
            r19 = r14
            if (r18 == 0) goto L_0x0173
            java.lang.String r14 = "Opening file..."
            r20 = r3
            r3 = 1
            r1.sendLogBroadcast(r3, r14)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            if (r5 == 0) goto L_0x0117
            java.io.InputStream r3 = r1.openInputStream(r5, r9, r8, r15)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            goto L_0x0127
        L_0x0117:
            if (r4 == 0) goto L_0x011e
            java.io.InputStream r3 = r1.openInputStream(r4, r9, r8, r15)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            goto L_0x0127
        L_0x011e:
            if (r6 <= 0) goto L_0x0125
            java.io.InputStream r3 = r1.openInputStream(r6, r9, r8, r15)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            goto L_0x0127
        L_0x0125:
            r3 = r20
        L_0x0127:
            if (r13 == 0) goto L_0x0132
            android.content.ContentResolver r4 = r21.getContentResolver()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            java.io.InputStream r14 = r4.openInputStream(r13)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            goto L_0x0149
        L_0x0132:
            if (r7 == 0) goto L_0x013a
            java.io.FileInputStream r14 = new java.io.FileInputStream     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r14.<init>(r7)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            goto L_0x0149
        L_0x013a:
            if (r17 <= 0) goto L_0x0147
            android.content.res.Resources r4 = r21.getResources()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r5 = r17
            java.io.InputStream r14 = r4.openRawResource(r5)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            goto L_0x0149
        L_0x0147:
            r14 = r19
        L_0x0149:
            int r4 = r3.available()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r5 = 4
            int r4 = r4 % r5
            if (r4 != 0) goto L_0x0153
            r6 = r3
            goto L_0x0179
        L_0x0153:
            no.nordicsemi.android.dfu.internal.exception.SizeValidationException r2 = new no.nordicsemi.android.dfu.internal.exception.SizeValidationException     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            java.lang.String r3 = "The new firmware is not word-aligned."
            r2.<init>(r3)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            throw r2     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
        L_0x015b:
            r0 = move-exception
            r2 = r0
            goto L_0x050d
        L_0x015f:
            r0 = move-exception
            r2 = r0
            goto L_0x0474
        L_0x0163:
            r0 = move-exception
            r2 = r0
            goto L_0x049e
        L_0x0167:
            r0 = move-exception
            r2 = r0
            goto L_0x04c8
        L_0x016b:
            r0 = move-exception
            r2 = r0
            goto L_0x04df
        L_0x016f:
            r0 = move-exception
            r2 = r0
            goto L_0x04f6
        L_0x0173:
            r20 = r3
            r14 = r19
            r6 = r20
        L_0x0179:
            boolean r3 = r11.equals(r9)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            if (r3 == 0) goto L_0x01f4
            r3 = r6
            no.nordicsemi.android.dfu.internal.ArchiveInputStream r3 = (no.nordicsemi.android.dfu.internal.ArchiveInputStream) r3     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            if (r15 != 0) goto L_0x0189
            int r4 = r3.getContentType()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            goto L_0x018d
        L_0x0189:
            int r4 = r3.setContentType(r15)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
        L_0x018d:
            r5 = r4 & 4
            if (r5 <= 0) goto L_0x01a2
            int r5 = r3.applicationImageSize()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r7 = 4
            int r5 = r5 % r7
            if (r5 != 0) goto L_0x019a
            goto L_0x01a2
        L_0x019a:
            no.nordicsemi.android.dfu.internal.exception.SizeValidationException r2 = new no.nordicsemi.android.dfu.internal.exception.SizeValidationException     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            java.lang.String r3 = "Application firmware is not word-aligned."
            r2.<init>(r3)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            throw r2     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
        L_0x01a2:
            r5 = r4 & 2
            if (r5 <= 0) goto L_0x01b7
            int r5 = r3.bootloaderImageSize()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r7 = 4
            int r5 = r5 % r7
            if (r5 != 0) goto L_0x01af
            goto L_0x01b7
        L_0x01af:
            no.nordicsemi.android.dfu.internal.exception.SizeValidationException r2 = new no.nordicsemi.android.dfu.internal.exception.SizeValidationException     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            java.lang.String r3 = "Bootloader firmware is not word-aligned."
            r2.<init>(r3)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            throw r2     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
        L_0x01b7:
            r5 = r4 & 1
            if (r5 <= 0) goto L_0x01cc
            int r5 = r3.softDeviceImageSize()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r7 = 4
            int r5 = r5 % r7
            if (r5 != 0) goto L_0x01c4
            goto L_0x01cc
        L_0x01c4:
            no.nordicsemi.android.dfu.internal.exception.SizeValidationException r2 = new no.nordicsemi.android.dfu.internal.exception.SizeValidationException     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            java.lang.String r3 = "Soft Device firmware is not word-aligned."
            r2.<init>(r3)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            throw r2     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
        L_0x01cc:
            r5 = 4
            if (r4 != r5) goto L_0x01df
            byte[] r5 = r3.getApplicationInit()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            if (r5 == 0) goto L_0x01f1
            java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            byte[] r3 = r3.getApplicationInit()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r5.<init>(r3)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            goto L_0x01ee
        L_0x01df:
            byte[] r5 = r3.getSystemInit()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            if (r5 == 0) goto L_0x01f1
            java.io.ByteArrayInputStream r5 = new java.io.ByteArrayInputStream     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            byte[] r3 = r3.getSystemInit()     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r5.<init>(r3)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
        L_0x01ee:
            r7 = r5
            r5 = r4
            goto L_0x01f6
        L_0x01f1:
            r5 = r4
            r7 = r14
            goto L_0x01f6
        L_0x01f4:
            r7 = r14
            r5 = r15
        L_0x01f6:
            r1.mFirmwareInputStream = r6     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r1.mInitFileInputStream = r7     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            r3 = 5
            java.lang.String r4 = "Firmware file opened successfully"
            r1.sendLogBroadcast(r3, r4)     // Catch:{ SecurityException -> 0x016f, FileNotFoundException -> 0x016b, SizeValidationException -> 0x0167, IOException -> 0x0163, Exception -> 0x015f }
            if (r18 != 0) goto L_0x020c
            r3 = 1000(0x3e8, float:1.401E-42)
            r1.waitFor(r3)     // Catch:{ all -> 0x015b }
            r3 = 1000(0x3e8, float:1.401E-42)
            r1.waitFor(r3)     // Catch:{ all -> 0x015b }
        L_0x020c:
            no.nordicsemi.android.dfu.DfuProgressInfo r3 = new no.nordicsemi.android.dfu.DfuProgressInfo     // Catch:{ all -> 0x015b }
            r3.<init>(r1)     // Catch:{ all -> 0x015b }
            r1.mProgressInfo = r3     // Catch:{ all -> 0x015b }
            boolean r3 = r1.mAborted     // Catch:{ all -> 0x015b }
            r8 = -7
            java.lang.String r9 = "Upload aborted"
            if (r3 == 0) goto L_0x022d
            r1.logw(r9)     // Catch:{ all -> 0x015b }
            r2 = 15
            r1.sendLogBroadcast(r2, r9)     // Catch:{ all -> 0x015b }
            no.nordicsemi.android.dfu.DfuProgressInfo r2 = r1.mProgressInfo     // Catch:{ all -> 0x015b }
            r2.setProgress(r8)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x022c
            r1.stopForeground(r10)
        L_0x022c:
            return
        L_0x022d:
            java.lang.String r3 = "Connecting to DFU target..."
            r4 = 1
            r1.sendLogBroadcast(r4, r3)     // Catch:{ all -> 0x015b }
            no.nordicsemi.android.dfu.DfuProgressInfo r3 = r1.mProgressInfo     // Catch:{ all -> 0x015b }
            r4 = -1
            r3.setProgress(r4)     // Catch:{ all -> 0x015b }
            android.bluetooth.BluetoothGatt r11 = r1.connect(r2)     // Catch:{ all -> 0x015b }
            if (r11 != 0) goto L_0x0256
            java.lang.String r2 = "Bluetooth adapter disabled"
            r1.loge(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = "Bluetooth adapter disabled"
            r3 = 20
            r1.sendLogBroadcast(r3, r2)     // Catch:{ all -> 0x015b }
            r2 = 4106(0x100a, float:5.754E-42)
            r1.report(r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x0255
            r1.stopForeground(r10)
        L_0x0255:
            return
        L_0x0256:
            int r3 = r1.mConnectionState     // Catch:{ all -> 0x015b }
            if (r3 != 0) goto L_0x0299
            int r3 = r1.mError     // Catch:{ all -> 0x015b }
            r4 = 32901(0x8085, float:4.6104E-41)
            if (r3 != r4) goto L_0x0282
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x015b }
            r3.<init>()     // Catch:{ all -> 0x015b }
            java.lang.String r4 = "Device not reachable. Check if the device with address "
            r3.append(r4)     // Catch:{ all -> 0x015b }
            r3.append(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = " is in range, is advertising and is connectable"
            r3.append(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x015b }
            r1.loge(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = "Error 133: Connection timeout"
            r3 = 20
            r1.sendLogBroadcast(r3, r2)     // Catch:{ all -> 0x015b }
            goto L_0x028e
        L_0x0282:
            java.lang.String r2 = "Device got disconnected before service discovery finished"
            r1.loge(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = "Disconnected"
            r3 = 20
            r1.sendLogBroadcast(r3, r2)     // Catch:{ all -> 0x015b }
        L_0x028e:
            r2 = 4096(0x1000, float:5.74E-42)
            r1.terminateConnection(r11, r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x0298
            r1.stopForeground(r10)
        L_0x0298:
            return
        L_0x0299:
            int r2 = r1.mError     // Catch:{ all -> 0x015b }
            java.lang.String r3 = "no.nordicsemi.android.dfu.extra.EXTRA_ATTEMPT"
            if (r2 <= 0) goto L_0x035b
            int r2 = r1.mError     // Catch:{ all -> 0x015b }
            r4 = 32768(0x8000, float:4.5918E-41)
            r2 = r2 & r4
            if (r2 <= 0) goto L_0x02e0
            int r2 = r1.mError     // Catch:{ all -> 0x015b }
            r4 = -32769(0xffffffffffff7fff, float:NaN)
            r2 = r2 & r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x015b }
            r4.<init>()     // Catch:{ all -> 0x015b }
            java.lang.String r5 = "An error occurred while connecting to the device:"
            r4.append(r5)     // Catch:{ all -> 0x015b }
            r4.append(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x015b }
            r1.loge(r4)     // Catch:{ all -> 0x015b }
            java.util.Locale r4 = java.util.Locale.US     // Catch:{ all -> 0x015b }
            java.lang.String r5 = "Connection failed (0x%02X): %s"
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x015b }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x015b }
            r8 = 0
            r6[r8] = r7     // Catch:{ all -> 0x015b }
            java.lang.String r2 = no.nordicsemi.android.error.GattError.parseConnectionError(r2)     // Catch:{ all -> 0x015b }
            r7 = 1
            r6[r7] = r2     // Catch:{ all -> 0x015b }
            java.lang.String r2 = java.lang.String.format(r4, r5, r6)     // Catch:{ all -> 0x015b }
            r4 = 20
            r1.sendLogBroadcast(r4, r2)     // Catch:{ all -> 0x015b }
            goto L_0x0316
        L_0x02e0:
            int r2 = r1.mError     // Catch:{ all -> 0x015b }
            r2 = r2 & -16385(0xffffffffffffbfff, float:NaN)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x015b }
            r4.<init>()     // Catch:{ all -> 0x015b }
            java.lang.String r5 = "An error occurred during discovering services:"
            r4.append(r5)     // Catch:{ all -> 0x015b }
            r4.append(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x015b }
            r1.loge(r4)     // Catch:{ all -> 0x015b }
            java.util.Locale r4 = java.util.Locale.US     // Catch:{ all -> 0x015b }
            java.lang.String r5 = "Connection failed (0x%02X): %s"
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x015b }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x015b }
            r8 = 0
            r6[r8] = r7     // Catch:{ all -> 0x015b }
            java.lang.String r2 = no.nordicsemi.android.error.GattError.parse(r2)     // Catch:{ all -> 0x015b }
            r7 = 1
            r6[r7] = r2     // Catch:{ all -> 0x015b }
            java.lang.String r2 = java.lang.String.format(r4, r5, r6)     // Catch:{ all -> 0x015b }
            r4 = 20
            r1.sendLogBroadcast(r4, r2)     // Catch:{ all -> 0x015b }
        L_0x0316:
            r13 = r22
            r2 = 0
            int r2 = r13.getIntExtra(r3, r2)     // Catch:{ all -> 0x015b }
            if (r2 != 0) goto L_0x0350
            java.lang.String r2 = "Retrying..."
            r4 = 15
            r1.sendLogBroadcast(r4, r2)     // Catch:{ all -> 0x015b }
            int r2 = r1.mConnectionState     // Catch:{ all -> 0x015b }
            if (r2 == 0) goto L_0x032d
            r1.disconnect(r11)     // Catch:{ all -> 0x015b }
        L_0x032d:
            r2 = 1
            r1.refreshDeviceCache(r11, r2)     // Catch:{ all -> 0x015b }
            r1.close(r11)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = "Restarting the service"
            r1.logi(r2)     // Catch:{ all -> 0x015b }
            android.content.Intent r2 = new android.content.Intent     // Catch:{ all -> 0x015b }
            r2.<init>()     // Catch:{ all -> 0x015b }
            r4 = 24
            r2.fillIn(r13, r4)     // Catch:{ all -> 0x015b }
            r4 = 1
            r2.putExtra(r3, r4)     // Catch:{ all -> 0x015b }
            r1.startService(r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x034f
            r1.stopForeground(r10)
        L_0x034f:
            return
        L_0x0350:
            int r2 = r1.mError     // Catch:{ all -> 0x015b }
            r1.terminateConnection(r11, r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x035a
            r1.stopForeground(r10)
        L_0x035a:
            return
        L_0x035b:
            r13 = r22
            boolean r2 = r1.mAborted     // Catch:{ all -> 0x015b }
            if (r2 == 0) goto L_0x0378
            r1.logw(r9)     // Catch:{ all -> 0x015b }
            r2 = 15
            r1.sendLogBroadcast(r2, r9)     // Catch:{ all -> 0x015b }
            r2 = 0
            r1.terminateConnection(r11, r2)     // Catch:{ all -> 0x015b }
            no.nordicsemi.android.dfu.DfuProgressInfo r2 = r1.mProgressInfo     // Catch:{ all -> 0x015b }
            r2.setProgress(r8)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x0377
            r1.stopForeground(r10)
        L_0x0377:
            return
        L_0x0378:
            r2 = 5
            java.lang.String r4 = "Services discovered"
            r1.sendLogBroadcast(r2, r4)     // Catch:{ all -> 0x015b }
            r2 = 0
            r13.putExtra(r3, r2)     // Catch:{ all -> 0x015b }
            r2 = 0
            no.nordicsemi.android.dfu.DfuServiceProvider r3 = new no.nordicsemi.android.dfu.DfuServiceProvider     // Catch:{ UploadAbortedException -> 0x0452, DeviceDisconnectedException -> 0x0432, DfuException -> 0x03d1 }
            r3.<init>()     // Catch:{ UploadAbortedException -> 0x0452, DeviceDisconnectedException -> 0x0432, DfuException -> 0x03d1 }
            r1.mDfuServiceImpl = r3     // Catch:{ UploadAbortedException -> 0x0452, DeviceDisconnectedException -> 0x0432, DfuException -> 0x03d1 }
            no.nordicsemi.android.dfu.DfuService r14 = r3.getServiceImpl(r13, r1, r11)     // Catch:{ UploadAbortedException -> 0x0452, DeviceDisconnectedException -> 0x0432, DfuException -> 0x03d1 }
            r1.mDfuServiceImpl = r14     // Catch:{ UploadAbortedException -> 0x03c9, DeviceDisconnectedException -> 0x03c6, DfuException -> 0x03c4 }
            if (r14 != 0) goto L_0x03b0
            java.lang.String r2 = "DfuBaseService"
            java.lang.String r3 = "DFU Service not found."
            android.util.Log.w(r2, r3)     // Catch:{ UploadAbortedException -> 0x03c9, DeviceDisconnectedException -> 0x03c6, DfuException -> 0x03c4 }
            java.lang.String r2 = "DFU Service not found"
            r3 = 15
            r1.sendLogBroadcast(r3, r2)     // Catch:{ UploadAbortedException -> 0x03c9, DeviceDisconnectedException -> 0x03c6, DfuException -> 0x03c4 }
            r2 = 4102(0x1006, float:5.748E-42)
            r1.terminateConnection(r11, r2)     // Catch:{ UploadAbortedException -> 0x03c9, DeviceDisconnectedException -> 0x03c6, DfuException -> 0x03c4 }
            if (r14 == 0) goto L_0x03aa
            r14.release()     // Catch:{ all -> 0x015b }
        L_0x03aa:
            if (r12 == 0) goto L_0x03af
            r1.stopForeground(r10)
        L_0x03af:
            return
        L_0x03b0:
            r2 = r14
            r3 = r22
            r4 = r11
            boolean r2 = r2.initialize(r3, r4, r5, r6, r7)     // Catch:{ UploadAbortedException -> 0x03c9, DeviceDisconnectedException -> 0x03c6, DfuException -> 0x03c4 }
            if (r2 == 0) goto L_0x03bd
            r14.performDfu(r13)     // Catch:{ UploadAbortedException -> 0x03c9, DeviceDisconnectedException -> 0x03c6, DfuException -> 0x03c4 }
        L_0x03bd:
            if (r14 == 0) goto L_0x0468
        L_0x03bf:
            r14.release()     // Catch:{ all -> 0x015b }
            goto L_0x0468
        L_0x03c4:
            r0 = move-exception
            goto L_0x03d3
        L_0x03c6:
            r0 = move-exception
            goto L_0x0434
        L_0x03c9:
            r2 = r14
            goto L_0x0452
        L_0x03cc:
            r0 = move-exception
            r14 = r2
        L_0x03ce:
            r2 = r0
            goto L_0x046e
        L_0x03d1:
            r0 = move-exception
            r14 = r2
        L_0x03d3:
            r2 = r0
            int r3 = r2.getErrorNumber()     // Catch:{ all -> 0x044f }
            r4 = 32768(0x8000, float:4.5918E-41)
            r4 = r4 & r3
            if (r4 <= 0) goto L_0x0401
            r4 = -32769(0xffffffffffff7fff, float:NaN)
            r3 = r3 & r4
            java.util.Locale r4 = java.util.Locale.US     // Catch:{ all -> 0x044f }
            java.lang.String r5 = "Error (0x%02X): %s"
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x044f }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x044f }
            r8 = 0
            r6[r8] = r7     // Catch:{ all -> 0x044f }
            java.lang.String r3 = no.nordicsemi.android.error.GattError.parseConnectionError(r3)     // Catch:{ all -> 0x044f }
            r7 = 1
            r6[r7] = r3     // Catch:{ all -> 0x044f }
            java.lang.String r3 = java.lang.String.format(r4, r5, r6)     // Catch:{ all -> 0x044f }
            r4 = 20
            r1.sendLogBroadcast(r4, r3)     // Catch:{ all -> 0x044f }
            goto L_0x0421
        L_0x0401:
            r3 = r3 & -16385(0xffffffffffffbfff, float:NaN)
            java.util.Locale r4 = java.util.Locale.US     // Catch:{ all -> 0x044f }
            java.lang.String r5 = "Error (0x%02X): %s"
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x044f }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x044f }
            r8 = 0
            r6[r8] = r7     // Catch:{ all -> 0x044f }
            java.lang.String r3 = no.nordicsemi.android.error.GattError.parse(r3)     // Catch:{ all -> 0x044f }
            r7 = 1
            r6[r7] = r3     // Catch:{ all -> 0x044f }
            java.lang.String r3 = java.lang.String.format(r4, r5, r6)     // Catch:{ all -> 0x044f }
            r4 = 20
            r1.sendLogBroadcast(r4, r3)     // Catch:{ all -> 0x044f }
        L_0x0421:
            java.lang.String r3 = r2.getMessage()     // Catch:{ all -> 0x044f }
            r1.loge(r3)     // Catch:{ all -> 0x044f }
            int r2 = r2.getErrorNumber()     // Catch:{ all -> 0x044f }
            r1.terminateConnection(r11, r2)     // Catch:{ all -> 0x044f }
            if (r14 == 0) goto L_0x0468
            goto L_0x03bf
        L_0x0432:
            r0 = move-exception
            r14 = r2
        L_0x0434:
            r2 = r0
            java.lang.String r3 = "Device has disconnected"
            r4 = 20
            r1.sendLogBroadcast(r4, r3)     // Catch:{ all -> 0x044f }
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x044f }
            r1.loge(r2)     // Catch:{ all -> 0x044f }
            r1.close(r11)     // Catch:{ all -> 0x044f }
            r2 = 4096(0x1000, float:5.74E-42)
            r1.report(r2)     // Catch:{ all -> 0x044f }
            if (r14 == 0) goto L_0x0468
            goto L_0x03bf
        L_0x044f:
            r0 = move-exception
            goto L_0x03ce
        L_0x0452:
            r1.logw(r9)     // Catch:{ all -> 0x03cc }
            r3 = 15
            r1.sendLogBroadcast(r3, r9)     // Catch:{ all -> 0x03cc }
            r3 = 0
            r1.terminateConnection(r11, r3)     // Catch:{ all -> 0x03cc }
            no.nordicsemi.android.dfu.DfuProgressInfo r3 = r1.mProgressInfo     // Catch:{ all -> 0x03cc }
            r3.setProgress(r8)     // Catch:{ all -> 0x03cc }
            if (r2 == 0) goto L_0x0468
            r2.release()     // Catch:{ all -> 0x015b }
        L_0x0468:
            if (r12 == 0) goto L_0x046d
            r1.stopForeground(r10)
        L_0x046d:
            return
        L_0x046e:
            if (r14 == 0) goto L_0x0473
            r14.release()     // Catch:{ all -> 0x015b }
        L_0x0473:
            throw r2     // Catch:{ all -> 0x015b }
        L_0x0474:
            java.lang.String r3 = "An exception occurred while opening files. Did you set the firmware file?"
            r1.loge(r3, r2)     // Catch:{ all -> 0x015b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x015b }
            r3.<init>()     // Catch:{ all -> 0x015b }
            java.lang.String r4 = "Opening file failed: "
            r3.append(r4)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = r2.getLocalizedMessage()     // Catch:{ all -> 0x015b }
            r3.append(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x015b }
            r3 = 20
            r1.sendLogBroadcast(r3, r2)     // Catch:{ all -> 0x015b }
            r2 = 4098(0x1002, float:5.743E-42)
            r1.report(r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x049d
            r1.stopForeground(r10)
        L_0x049d:
            return
        L_0x049e:
            java.lang.String r3 = "An exception occurred while calculating file size"
            r1.loge(r3, r2)     // Catch:{ all -> 0x015b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x015b }
            r3.<init>()     // Catch:{ all -> 0x015b }
            java.lang.String r4 = "Opening file failed: "
            r3.append(r4)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = r2.getLocalizedMessage()     // Catch:{ all -> 0x015b }
            r3.append(r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x015b }
            r3 = 20
            r1.sendLogBroadcast(r3, r2)     // Catch:{ all -> 0x015b }
            r2 = 4098(0x1002, float:5.743E-42)
            r1.report(r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x04c7
            r1.stopForeground(r10)
        L_0x04c7:
            return
        L_0x04c8:
            java.lang.String r3 = "Firmware not word-aligned"
            r1.loge(r3, r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = "Opening file failed: Firmware size must be word-aligned"
            r3 = 20
            r1.sendLogBroadcast(r3, r2)     // Catch:{ all -> 0x015b }
            r2 = 4108(0x100c, float:5.757E-42)
            r1.report(r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x04de
            r1.stopForeground(r10)
        L_0x04de:
            return
        L_0x04df:
            java.lang.String r3 = "An exception occurred while opening file"
            r1.loge(r3, r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = "Opening file failed: File not found"
            r3 = 20
            r1.sendLogBroadcast(r3, r2)     // Catch:{ all -> 0x015b }
            r2 = 4097(0x1001, float:5.741E-42)
            r1.report(r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x04f5
            r1.stopForeground(r10)
        L_0x04f5:
            return
        L_0x04f6:
            java.lang.String r3 = "A security exception occurred while opening file"
            r1.loge(r3, r2)     // Catch:{ all -> 0x015b }
            java.lang.String r2 = "Opening file failed: Permission required"
            r3 = 20
            r1.sendLogBroadcast(r3, r2)     // Catch:{ all -> 0x015b }
            r2 = 4097(0x1001, float:5.741E-42)
            r1.report(r2)     // Catch:{ all -> 0x015b }
            if (r12 == 0) goto L_0x050c
            r1.stopForeground(r10)
        L_0x050c:
            return
        L_0x050d:
            if (r12 == 0) goto L_0x0512
            r1.stopForeground(r10)
        L_0x0512:
            throw r2
        L_0x0513:
            java.lang.String r2 = "File type or file mime-type not supported"
            r1.logw(r2)
            r3 = 15
            r1.sendLogBroadcast(r3, r2)
            r2 = 4105(0x1009, float:5.752E-42)
            r1.report(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.DfuBaseService.onHandleIntent(android.content.Intent):void");
    }

    private InputStream openInputStream(String str, String str2, int i, int i2) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(str);
        if (MIME_TYPE_ZIP.equals(str2)) {
            return new ArchiveInputStream(fileInputStream, i, i2);
        }
        return str.toLowerCase(Locale.US).endsWith("hex") ? new HexInputStream(fileInputStream, i) : fileInputStream;
    }

    private InputStream openInputStream(Uri uri, String str, int i, int i2) throws IOException {
        InputStream openInputStream = getContentResolver().openInputStream(uri);
        if (MIME_TYPE_ZIP.equals(str)) {
            return new ArchiveInputStream(openInputStream, i, i2);
        }
        Cursor query = getContentResolver().query(uri, new String[]{"_display_name"}, null, null, null);
        try {
            if (query.moveToNext() && query.getString(0).toLowerCase(Locale.US).endsWith("hex")) {
                return new HexInputStream(openInputStream, i);
            }
            query.close();
            return openInputStream;
        } finally {
            query.close();
        }
    }

    private InputStream openInputStream(int i, String str, int i2, int i3) throws IOException {
        InputStream openRawResource = getResources().openRawResource(i);
        if (MIME_TYPE_ZIP.equals(str)) {
            return new ArchiveInputStream(openRawResource, i2, i3);
        }
        openRawResource.mark(2);
        int read = openRawResource.read();
        openRawResource.reset();
        return read == 58 ? new HexInputStream(openRawResource, i2) : openRawResource;
    }

    /* access modifiers changed from: protected */
    public BluetoothGatt connect(String str) {
        if (!this.mBluetoothAdapter.isEnabled()) {
            return null;
        }
        this.mConnectionState = -1;
        logi("Connecting to the device...");
        BluetoothDevice remoteDevice = this.mBluetoothAdapter.getRemoteDevice(str);
        sendLogBroadcast(0, "gatt = device.connectGatt(autoConnect = false)");
        BluetoothGatt connectGatt = remoteDevice.connectGatt(this, false, this.mGattCallback);
        try {
            synchronized (this.mLock) {
                while (true) {
                    if ((this.mConnectionState == -1 || this.mConnectionState == -2) && this.mError == 0) {
                        this.mLock.wait();
                    }
                }
            }
        } catch (InterruptedException e) {
            loge("Sleeping interrupted", e);
        }
        return connectGatt;
    }

    /* access modifiers changed from: protected */
    public void terminateConnection(BluetoothGatt bluetoothGatt, int i) {
        if (this.mConnectionState != 0) {
            disconnect(bluetoothGatt);
        }
        refreshDeviceCache(bluetoothGatt, false);
        close(bluetoothGatt);
        waitFor(Constants.DEFAULT_WEIGHT_KG);
        if (i != 0) {
            report(i);
        }
    }

    /* access modifiers changed from: protected */
    public void disconnect(BluetoothGatt bluetoothGatt) {
        if (this.mConnectionState != 0) {
            sendLogBroadcast(1, "Disconnecting...");
            this.mProgressInfo.setProgress(-5);
            this.mConnectionState = -4;
            logi("Disconnecting from the device...");
            sendLogBroadcast(0, "gatt.disconnect()");
            bluetoothGatt.disconnect();
            waitUntilDisconnected();
            sendLogBroadcast(5, "Disconnected");
        }
    }

    /* access modifiers changed from: protected */
    public void waitUntilDisconnected() {
        try {
            synchronized (this.mLock) {
                while (this.mConnectionState != 0 && this.mError == 0) {
                    this.mLock.wait();
                }
            }
        } catch (InterruptedException e) {
            loge("Sleeping interrupted", e);
        }
    }

    /* access modifiers changed from: protected */
    public void waitFor(int i) {
        synchronized (this.mLock) {
            try {
                sendLogBroadcast(0, "wait(" + i + ")");
                this.mLock.wait((long) i);
            } catch (InterruptedException e) {
                loge("Sleeping interrupted", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void close(BluetoothGatt bluetoothGatt) {
        logi("Cleaning up...");
        sendLogBroadcast(0, "gatt.close()");
        bluetoothGatt.close();
        this.mConnectionState = -5;
    }

    /* access modifiers changed from: protected */
    public void refreshDeviceCache(BluetoothGatt bluetoothGatt, boolean z) {
        if (z || bluetoothGatt.getDevice().getBondState() == 10) {
            sendLogBroadcast(0, "gatt.refresh() (hidden)");
            try {
                Method method = bluetoothGatt.getClass().getMethod("refresh", new Class[0]);
                if (method != null) {
                    boolean booleanValue = ((Boolean) method.invoke(bluetoothGatt, new Object[0])).booleanValue();
                    logi("Refreshing result: " + booleanValue);
                }
            } catch (Exception e) {
                loge("An exception occurred while refreshing device", e);
                sendLogBroadcast(15, "Refreshing failed");
            }
        }
    }

    public void updateProgressNotification() {
        String str;
        DfuProgressInfo dfuProgressInfo = this.mProgressInfo;
        int progress = dfuProgressInfo.getProgress();
        if (this.mLastProgress != progress) {
            this.mLastProgress = progress;
            sendProgressBroadcast(dfuProgressInfo);
            if (!this.mDisableNotification) {
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (elapsedRealtime - this.mLastNotificationTime >= 250 || -6 == progress || -7 == progress) {
                    this.mLastNotificationTime = elapsedRealtime;
                    String str2 = this.mDeviceAddress;
                    String str3 = this.mDeviceName;
                    if (str3 == null) {
                        str3 = getString(C3418R.string.dfu_unknown_name);
                    }
                    NotificationCompat.Builder onlyAlertOnce = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_DFU).setSmallIcon(17301640).setOnlyAlertOnce(true);
                    onlyAlertOnce.setColor(-7829368);
                    switch (progress) {
                        case -7:
                            onlyAlertOnce.setOngoing(false).setContentTitle(getString(C3418R.string.dfu_status_aborted)).setSmallIcon(17301641).setContentText(getString(C3418R.string.dfu_status_aborted_msg)).setAutoCancel(true);
                            break;
                        case -6:
                            onlyAlertOnce.setOngoing(false).setContentTitle(getString(C3418R.string.dfu_status_completed)).setSmallIcon(17301641).setContentText(getString(C3418R.string.dfu_status_completed_msg)).setAutoCancel(true).setColor(-16730086);
                            break;
                        case -5:
                            onlyAlertOnce.setOngoing(true).setContentTitle(getString(C3418R.string.dfu_status_disconnecting)).setContentText(getString(C3418R.string.dfu_status_disconnecting_msg, new Object[]{str3})).setProgress(100, 0, true);
                            break;
                        case -4:
                            onlyAlertOnce.setOngoing(true).setContentTitle(getString(C3418R.string.dfu_status_validating)).setContentText(getString(C3418R.string.dfu_status_validating_msg)).setProgress(100, 0, true);
                            break;
                        case -3:
                            onlyAlertOnce.setOngoing(true).setContentTitle(getString(C3418R.string.dfu_status_switching_to_dfu)).setContentText(getString(C3418R.string.dfu_status_switching_to_dfu_msg)).setProgress(100, 0, true);
                            break;
                        case -2:
                            onlyAlertOnce.setOngoing(true).setContentTitle(getString(C3418R.string.dfu_status_starting)).setContentText(getString(C3418R.string.dfu_status_starting_msg)).setProgress(100, 0, true);
                            break;
                        case -1:
                            onlyAlertOnce.setOngoing(true).setContentTitle(getString(C3418R.string.dfu_status_connecting)).setContentText(getString(C3418R.string.dfu_status_connecting_msg, new Object[]{str3})).setProgress(100, 0, true);
                            break;
                        default:
                            if (dfuProgressInfo.getTotalParts() == 1) {
                                str = getString(C3418R.string.dfu_status_uploading);
                            } else {
                                str = getString(C3418R.string.dfu_status_uploading_part, new Object[]{Integer.valueOf(dfuProgressInfo.getCurrentPart()), Integer.valueOf(dfuProgressInfo.getTotalParts())});
                            }
                            onlyAlertOnce.setOngoing(true).setContentTitle(str).setContentText(getString(C3418R.string.dfu_status_uploading_msg, new Object[]{str3})).setProgress(100, progress, false);
                            break;
                    }
                    Intent intent = new Intent(this, getNotificationTarget());
                    intent.addFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
                    intent.putExtra(EXTRA_DEVICE_ADDRESS, str2);
                    intent.putExtra(EXTRA_DEVICE_NAME, str3);
                    intent.putExtra(EXTRA_PROGRESS, progress);
                    onlyAlertOnce.setContentIntent(PendingIntent.getActivity(this, 0, intent, 134217728));
                    updateProgressNotification(onlyAlertOnce, progress);
                    ((NotificationManager) getSystemService("notification")).notify(NOTIFICATION_ID, onlyAlertOnce.build());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateProgressNotification(NotificationCompat.Builder builder, int i) {
        if (i != -7 && i != -6) {
            Intent intent = new Intent(BROADCAST_ACTION);
            intent.putExtra(EXTRA_ACTION, 2);
            builder.addAction(C3418R.C3420drawable.ic_action_notify_cancel, getString(C3418R.string.dfu_action_abort), PendingIntent.getBroadcast(this, 1, intent, 134217728));
        }
    }

    private void report(int i) {
        sendErrorBroadcast(i);
        if (!this.mDisableNotification) {
            String str = this.mDeviceAddress;
            String str2 = this.mDeviceName;
            if (str2 == null) {
                str2 = getString(C3418R.string.dfu_unknown_name);
            }
            NotificationCompat.Builder autoCancel = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_DFU).setSmallIcon(17301640).setOnlyAlertOnce(true).setColor(SupportMenu.CATEGORY_MASK).setOngoing(false).setContentTitle(getString(C3418R.string.dfu_status_error)).setSmallIcon(17301641).setContentText(getString(C3418R.string.dfu_status_error_msg)).setAutoCancel(true);
            Intent intent = new Intent(this, getNotificationTarget());
            intent.addFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
            intent.putExtra(EXTRA_DEVICE_ADDRESS, str);
            intent.putExtra(EXTRA_DEVICE_NAME, str2);
            intent.putExtra(EXTRA_PROGRESS, i);
            autoCancel.setContentIntent(PendingIntent.getActivity(this, 0, intent, 134217728));
            updateErrorNotification(autoCancel);
            ((NotificationManager) getSystemService("notification")).notify(NOTIFICATION_ID, autoCancel.build());
        }
    }

    private void startForeground() {
        NotificationCompat.Builder ongoing = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_DFU).setSmallIcon(17301640).setContentTitle(getString(C3418R.string.dfu_status_foreground_title)).setContentText(getString(C3418R.string.dfu_status_foreground_content)).setColor(-7829368).setPriority(-1).setOngoing(true);
        Class<? extends Activity> notificationTarget = getNotificationTarget();
        if (notificationTarget != null) {
            Intent intent = new Intent(this, notificationTarget);
            intent.addFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
            intent.putExtra(EXTRA_DEVICE_ADDRESS, this.mDeviceAddress);
            intent.putExtra(EXTRA_DEVICE_NAME, this.mDeviceName);
            ongoing.setContentIntent(PendingIntent.getActivity(this, 0, intent, 134217728));
        } else {
            logw("getNotificationTarget() should not return null if the service is to be started as a foreground service");
        }
        updateForegroundNotification(ongoing);
        startForeground(NOTIFICATION_ID, ongoing.build());
    }

    private void sendProgressBroadcast(DfuProgressInfo dfuProgressInfo) {
        Intent intent = new Intent(BROADCAST_PROGRESS);
        intent.putExtra(EXTRA_DATA, dfuProgressInfo.getProgress());
        intent.putExtra(EXTRA_DEVICE_ADDRESS, this.mDeviceAddress);
        intent.putExtra(EXTRA_PART_CURRENT, dfuProgressInfo.getCurrentPart());
        intent.putExtra(EXTRA_PARTS_TOTAL, dfuProgressInfo.getTotalParts());
        intent.putExtra(EXTRA_SPEED_B_PER_MS, dfuProgressInfo.getSpeed());
        intent.putExtra(EXTRA_AVG_SPEED_B_PER_MS, dfuProgressInfo.getAverageSpeed());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendErrorBroadcast(int i) {
        Intent intent = new Intent(BROADCAST_ERROR);
        if ((i & 16384) > 0) {
            intent.putExtra(EXTRA_DATA, i & -16385);
            intent.putExtra(EXTRA_ERROR_TYPE, 2);
        } else if ((32768 & i) > 0) {
            intent.putExtra(EXTRA_DATA, i & -32769);
            intent.putExtra(EXTRA_ERROR_TYPE, 1);
        } else if ((i & 8192) > 0) {
            intent.putExtra(EXTRA_DATA, i & -8193);
            intent.putExtra(EXTRA_ERROR_TYPE, 3);
        } else {
            intent.putExtra(EXTRA_DATA, i);
            intent.putExtra(EXTRA_ERROR_TYPE, 0);
        }
        intent.putExtra(EXTRA_DEVICE_ADDRESS, this.mDeviceAddress);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /* access modifiers changed from: package-private */
    public void sendLogBroadcast(int i, String str) {
        Intent intent = new Intent(BROADCAST_LOG);
        intent.putExtra(EXTRA_LOG_MESSAGE, "[DFU] " + str);
        intent.putExtra(EXTRA_LOG_LEVEL, i);
        intent.putExtra(EXTRA_DEVICE_ADDRESS, this.mDeviceAddress);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private boolean initialize() {
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService("bluetooth");
        if (bluetoothManager == null) {
            loge("Unable to initialize BluetoothManager.");
            return false;
        }
        this.mBluetoothAdapter = bluetoothManager.getAdapter();
        if (this.mBluetoothAdapter != null) {
            return true;
        }
        loge("Unable to obtain a BluetoothAdapter.");
        return false;
    }

    /* access modifiers changed from: private */
    public void loge(String str) {
        Log.e(TAG, str);
    }

    private void loge(String str, Throwable th) {
        Log.e(TAG, str, th);
    }

    /* access modifiers changed from: private */
    public void logw(String str) {
        if (DEBUG) {
            Log.w(TAG, str);
        }
    }

    /* access modifiers changed from: private */
    public void logi(String str) {
        if (DEBUG) {
            Log.i(TAG, str);
        }
    }
}
