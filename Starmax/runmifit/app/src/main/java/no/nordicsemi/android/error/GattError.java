package no.nordicsemi.android.error;

import com.tencent.bugly.beta.tinker.TinkerReport;

/* renamed from: no.nordicsemi.android.error.GattError */
public class GattError {
    public static String parseConnectionError(int i) {
        if (i == 0) {
            return "SUCCESS";
        }
        if (i == 1) {
            return "GATT CONN L2C FAILURE";
        }
        if (i == 8) {
            return "GATT CONN TIMEOUT";
        }
        if (i == 19) {
            return "GATT CONN TERMINATE PEER USER";
        }
        if (i == 22) {
            return "GATT CONN TERMINATE LOCAL HOST";
        }
        if (i == 34) {
            return "GATT CONN LMP TIMEOUT";
        }
        if (i == 62) {
            return "GATT CONN FAIL ESTABLISH";
        }
        if (i == 133) {
            return "GATT ERROR";
        }
        if (i == 256) {
            return "GATT CONN CANCEL ";
        }
        return "UNKNOWN (" + i + ")";
    }

    public static String parse(int i) {
        if (i == 34) {
            return "GATT CONN LMP TIMEOUT";
        }
        if (i == 257) {
            return "TOO MANY OPEN CONNECTIONS";
        }
        if (i == 58) {
            return "GATT CONTROLLER BUSY";
        }
        if (i == 59) {
            return "GATT UNACCEPT CONN INTERVAL";
        }
        switch (i) {
            case 1:
                return "GATT INVALID HANDLE";
            case 2:
                return "GATT READ NOT PERMIT";
            case 3:
                return "GATT WRITE NOT PERMIT";
            case 4:
                return "GATT INVALID PDU";
            case 5:
                return "GATT INSUF AUTHENTICATION";
            case 6:
                return "GATT REQ NOT SUPPORTED";
            case 7:
                return "GATT INVALID OFFSET";
            case 8:
                return "GATT INSUF AUTHORIZATION";
            case 9:
                return "GATT PREPARE Q FULL";
            case 10:
                return "GATT NOT FOUND";
            case 11:
                return "GATT NOT LONG";
            case 12:
                return "GATT INSUF KEY SIZE";
            case 13:
                return "GATT INVALID ATTR LEN";
            case 14:
                return "GATT ERR UNLIKELY";
            case 15:
                return "GATT INSUF ENCRYPTION";
            case 16:
                return "GATT UNSUPPORT GRP TYPE";
            case 17:
                return "GATT INSUF RESOURCE";
            default:
                switch (i) {
                    case 128:
                        return "GATT NO RESOURCES";
                    case 129:
                        return "GATT INTERNAL ERROR";
                    case FMParserConstants.f7459IN:
                        return "GATT WRONG STATE";
                    case FMParserConstants.f7456AS:
                        return "GATT DB FULL";
                    case 132:
                        return "GATT BUSY";
                    case FMParserConstants.f7457ID:
                        return "GATT ERROR";
                    case FMParserConstants.OPEN_MISPLACED_INTERPOLATION:
                        return "GATT CMD STARTED";
                    case FMParserConstants.NON_ESCAPED_ID_START_CHAR:
                        return "GATT ILLEGAL PARAMETER";
                    case FMParserConstants.ESCAPED_ID_CHAR:
                        return "GATT PENDING";
                    case FMParserConstants.ID_START_CHAR:
                        return "GATT AUTH FAIL";
                    case FMParserConstants.ASCII_DIGIT:
                        return "GATT MORE";
                    case FMParserConstants.DIRECTIVE_END:
                        return "GATT INVALID CFG";
                    case FMParserConstants.EMPTY_DIRECTIVE_END:
                        return "GATT SERVICE STARTED";
                    case FMParserConstants.NATURAL_GT:
                        return "GATT ENCRYPTED NO MITM";
                    case FMParserConstants.NATURAL_GTE:
                        return "GATT NOT ENCRYPTED";
                    case FMParserConstants.TERMINATING_WHITESPACE:
                        return "GATT CONGESTED";
                    default:
                        switch (i) {
                            case TinkerReport.KEY_LOADED_EXCEPTION_DEX_CHECK:
                                return "GATT CCCD CFG ERROR";
                            case TinkerReport.KEY_LOADED_EXCEPTION_RESOURCE:
                                return "GATT PROCEDURE IN PROGRESS";
                            case 255:
                                return "GATT VALUE OUT OF RANGE";
                            default:
                                switch (i) {
                                    case 4096:
                                        return "DFU DEVICE DISCONNECTED";
                                    case 4097:
                                        return "DFU FILE NOT FOUND";
                                    case 4098:
                                        return "DFU FILE ERROR";
                                    case 4099:
                                        return "DFU NOT A VALID HEX FILE";
                                    case 4100:
                                        return "DFU IO EXCEPTION";
                                    case 4101:
                                        return "DFU SERVICE DISCOVERY NOT STARTED";
                                    case 4102:
                                        return "DFU CHARACTERISTICS NOT FOUND";
                                    default:
                                        switch (i) {
                                            case 4104:
                                                return "DFU INVALID RESPONSE";
                                            case 4105:
                                                return "DFU FILE TYPE NOT SUPPORTED";
                                            case 4106:
                                                return "BLUETOOTH ADAPTER DISABLED";
                                            case 4107:
                                            case 4108:
                                                return "DFU INIT PACKET REQUIRED";
                                            case 4109:
                                                return "DFU CRC ERROR";
                                            case 4110:
                                                return "DFU DEVICE NOT BONDED";
                                            default:
                                                return "UNKNOWN (" + i + ")";
                                        }
                                }
                        }
                }
        }
    }

    public static String parseDfuRemoteError(int i) {
        int i2 = i & 3840;
        if (i2 == 256) {
            return LegacyDfuError.parse(i);
        }
        if (i2 == 512) {
            return SecureDfuError.parse(i);
        }
        if (i2 == 1024) {
            return SecureDfuError.parseExtendedError(i);
        }
        if (i2 == 2048) {
            return SecureDfuError.parseButtonlessError(i);
        }
        return "UNKNOWN (" + i + ")";
    }
}
