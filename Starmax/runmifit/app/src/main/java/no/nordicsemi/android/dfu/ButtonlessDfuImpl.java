package no.nordicsemi.android.dfu;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import no.nordicsemi.android.dfu.internal.exception.UnknownResponseException;

/* renamed from: no.nordicsemi.android.dfu.ButtonlessDfuImpl */
abstract class ButtonlessDfuImpl extends BaseButtonlessDfuImpl {
    private static final int DFU_STATUS_SUCCESS = 1;
    private static final byte[] OP_CODE_ENTER_BOOTLOADER = {1};
    private static final int OP_CODE_ENTER_BOOTLOADER_KEY = 1;
    private static final int OP_CODE_RESPONSE_CODE_KEY = 32;

    /* access modifiers changed from: protected */
    public abstract BluetoothGattCharacteristic getButtonlessDfuCharacteristic();

    /* access modifiers changed from: protected */
    public abstract int getResponseType();

    /* access modifiers changed from: protected */
    public abstract boolean shouldScanForBootloader();

    ButtonlessDfuImpl(Intent intent, DfuBaseService dfuBaseService) {
        super(intent, dfuBaseService);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:9|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r5 = r12.mReceivedData;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0079 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void performDfu(android.content.Intent r13) throws no.nordicsemi.android.dfu.internal.exception.DfuException, no.nordicsemi.android.dfu.internal.exception.DeviceDisconnectedException, no.nordicsemi.android.dfu.internal.exception.UploadAbortedException {
        /*
            r12 = this;
            java.lang.String r0 = ")"
            java.lang.String r1 = ", Status = "
            java.lang.String r2 = "Response received (Op Code = "
            no.nordicsemi.android.dfu.DfuProgressInfo r3 = r12.mProgressInfo
            r4 = -2
            r3.setProgress(r4)
            no.nordicsemi.android.dfu.DfuBaseService r3 = r12.mService
            r4 = 1000(0x3e8, float:1.401E-42)
            r3.waitFor(r4)
            android.bluetooth.BluetoothGatt r3 = r12.mGatt
            no.nordicsemi.android.dfu.DfuBaseService r5 = r12.mService
            r6 = 15
            java.lang.String r7 = "Application with buttonless update found"
            r5.sendLogBroadcast(r6, r7)
            no.nordicsemi.android.dfu.DfuBaseService r5 = r12.mService
            r6 = 1
            java.lang.String r7 = "Jumping to the DFU Bootloader..."
            r5.sendLogBroadcast(r6, r7)
            android.bluetooth.BluetoothGattCharacteristic r5 = r12.getButtonlessDfuCharacteristic()
            int r7 = r12.getResponseType()
            int r8 = r12.getResponseType()
            r12.enableCCCD(r5, r8)
            no.nordicsemi.android.dfu.DfuBaseService r8 = r12.mService
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r10 = 2
            if (r7 != r10) goto L_0x0042
            java.lang.String r7 = "Indications"
            goto L_0x0044
        L_0x0042:
            java.lang.String r7 = "Notifications"
        L_0x0044:
            r9.append(r7)
            java.lang.String r7 = " enabled"
            r9.append(r7)
            java.lang.String r7 = r9.toString()
            r9 = 10
            r8.sendLogBroadcast(r9, r7)
            no.nordicsemi.android.dfu.DfuBaseService r7 = r12.mService
            r7.waitFor(r4)
            r4 = 0
            r7 = 20
            no.nordicsemi.android.dfu.DfuProgressInfo r8 = r12.mProgressInfo     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r10 = -3
            r8.setProgress(r10)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            java.lang.String r8 = "Sending Enter Bootloader (Op Code = 1)"
            r12.logi(r8)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            byte[] r8 = no.nordicsemi.android.dfu.ButtonlessDfuImpl.OP_CODE_ENTER_BOOTLOADER     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r12.writeOpCode(r5, r8, r6)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            no.nordicsemi.android.dfu.DfuBaseService r5 = r12.mService     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            java.lang.String r8 = "Enter bootloader sent (Op Code = 1)"
            r5.sendLogBroadcast(r9, r8)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            byte[] r5 = r12.readNotificationResponse()     // Catch:{ DeviceDisconnectedException -> 0x0079 }
            goto L_0x007b
        L_0x0079:
            byte[] r5 = r12.mReceivedData     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
        L_0x007b:
            if (r5 == 0) goto L_0x00cd
            int r8 = r12.getStatusCode(r5, r6)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r10.<init>()     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r10.append(r2)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            byte r11 = r5[r6]     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r10.append(r11)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r10.append(r1)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r10.append(r8)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r10.append(r0)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            java.lang.String r10 = r10.toString()     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r12.logi(r10)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            no.nordicsemi.android.dfu.DfuBaseService r10 = r12.mService     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r11.<init>()     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r11.append(r2)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            byte r2 = r5[r6]     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r11.append(r2)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r11.append(r1)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r11.append(r8)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r11.append(r0)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            java.lang.String r0 = r11.toString()     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r10.sendLogBroadcast(r9, r0)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            if (r8 != r6) goto L_0x00c5
            no.nordicsemi.android.dfu.DfuBaseService r0 = r12.mService     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r0.waitUntilDisconnected()     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            goto L_0x00d2
        L_0x00c5:
            no.nordicsemi.android.dfu.internal.exception.RemoteDfuException r13 = new no.nordicsemi.android.dfu.internal.exception.RemoteDfuException     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            java.lang.String r0 = "Device returned error after sending Enter Bootloader"
            r13.<init>(r0, r8)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            throw r13     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
        L_0x00cd:
            java.lang.String r0 = "Device disconnected before receiving notification"
            r12.logi(r0)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
        L_0x00d2:
            no.nordicsemi.android.dfu.DfuBaseService r0 = r12.mService     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r1 = 5
            java.lang.String r2 = "Disconnected by the remote device"
            r0.sendLogBroadcast(r1, r2)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            boolean r0 = r12.shouldScanForBootloader()     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            r12.finalize(r13, r4, r0)     // Catch:{ UnknownResponseException -> 0x010d, RemoteDfuException -> 0x00e2 }
            goto L_0x0125
        L_0x00e2:
            r13 = move-exception
            int r0 = r13.getErrorNumber()
            r0 = r0 | 2048(0x800, float:2.87E-42)
            java.lang.String r13 = r13.getMessage()
            r12.loge(r13)
            no.nordicsemi.android.dfu.DfuBaseService r13 = r12.mService
            java.util.Locale r1 = java.util.Locale.US
            java.lang.Object[] r2 = new java.lang.Object[r6]
            java.lang.String r5 = no.nordicsemi.android.error.SecureDfuError.parseButtonlessError(r0)
            r2[r4] = r5
            java.lang.String r4 = "Remote DFU error: %s"
            java.lang.String r1 = java.lang.String.format(r1, r4, r2)
            r13.sendLogBroadcast(r7, r1)
            no.nordicsemi.android.dfu.DfuBaseService r13 = r12.mService
            r0 = r0 | 8192(0x2000, float:1.14794E-41)
            r13.terminateConnection(r3, r0)
            goto L_0x0125
        L_0x010d:
            r13 = move-exception
            java.lang.String r0 = r13.getMessage()
            r12.loge(r0)
            no.nordicsemi.android.dfu.DfuBaseService r0 = r12.mService
            java.lang.String r13 = r13.getMessage()
            r0.sendLogBroadcast(r7, r13)
            no.nordicsemi.android.dfu.DfuBaseService r13 = r12.mService
            r0 = 4104(0x1008, float:5.751E-42)
            r13.terminateConnection(r3, r0)
        L_0x0125:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: no.nordicsemi.android.dfu.ButtonlessDfuImpl.performDfu(android.content.Intent):void");
    }

    private int getStatusCode(byte[] bArr, int i) throws UnknownResponseException {
        if (bArr != null && bArr.length >= 3 && bArr[0] == 32 && bArr[1] == i && (bArr[2] == 1 || bArr[2] == 2 || bArr[2] == 4)) {
            return bArr[2];
        }
        throw new UnknownResponseException("Invalid response received", bArr, 32, i);
    }
}
