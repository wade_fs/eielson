package freemarker.cache;

import freemarker.template.MalformedTemplateNameException;

public class _CacheAPI {
    public static String toAbsoluteName(TemplateNameFormat templateNameFormat, String str, String str2) throws MalformedTemplateNameException {
        return templateNameFormat.toAbsoluteName(str, str2);
    }
}
