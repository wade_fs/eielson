package freemarker.cache;

import java.io.IOException;

public abstract class TemplateLookupStrategy {
    public static final TemplateLookupStrategy DEFAULT_2_3_0 = new Default020300();

    public abstract TemplateLookupResult lookup(TemplateLookupContext templateLookupContext) throws IOException;

    private static class Default020300 extends TemplateLookupStrategy {
        public String toString() {
            return "TemplateLookupStrategy.DEFAULT_2_3_0";
        }

        private Default020300() {
        }

        public TemplateLookupResult lookup(TemplateLookupContext templateLookupContext) throws IOException {
            return templateLookupContext.lookupWithLocalizedThenAcquisitionStrategy(templateLookupContext.getTemplateName(), templateLookupContext.getTemplateLocale());
        }
    }
}
