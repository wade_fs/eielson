package freemarker.cache;

import java.io.IOException;
import java.util.Locale;

public abstract class TemplateLookupContext {
    private final Object customLookupCondition;
    private final Locale templateLocale;
    private final String templateName;

    public abstract TemplateLookupResult lookupWithAcquisitionStrategy(String str) throws IOException;

    public abstract TemplateLookupResult lookupWithLocalizedThenAcquisitionStrategy(String str, Locale locale) throws IOException;

    TemplateLookupContext(String str, Locale locale, Object obj) {
        this.templateName = str;
        this.templateLocale = locale;
        this.customLookupCondition = obj;
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public Locale getTemplateLocale() {
        return this.templateLocale;
    }

    public Object getCustomLookupCondition() {
        return this.customLookupCondition;
    }

    public TemplateLookupResult createNegativeLookupResult() {
        return TemplateLookupResult.createNegativeResult();
    }
}
