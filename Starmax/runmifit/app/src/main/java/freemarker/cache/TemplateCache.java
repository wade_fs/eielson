package freemarker.cache;

import com.baidu.mobstat.Config;
import freemarker.cache.MultiTemplateLoader;
import freemarker.core.Environment;
import freemarker.log.Logger;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.NullArgumentException;
import freemarker.template.utility.StringUtil;
import freemarker.template.utility.UndeclaredThrowableException;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class TemplateCache {
    private static final char ASTERISK = '*';
    private static final String ASTERISKSTR = "*";
    public static final long DEFAULT_TEMPLATE_UPDATE_DELAY_MILLIS = 5000;
    private static final Method INIT_CAUSE = getInitCauseMethod();
    private static final String LOCALE_PART_SEPARATOR = "_";
    private static final Logger LOG = Logger.getLogger("freemarker.cache");
    private static final char SLASH = '/';
    static /* synthetic */ Class class$java$lang$Throwable;
    private Configuration config;
    private final boolean isStorageConcurrent;
    /* access modifiers changed from: private */
    public boolean localizedLookup;
    private final CacheStorage storage;
    private final TemplateLoader templateLoader;
    private final TemplateLookupStrategy templateLookupStrategy;
    private final TemplateNameFormat templateNameFormat;
    private long updateDelay;

    public TemplateCache() {
        this(_TemplateAPI.createDefaultTemplateLoader(Configuration.VERSION_2_3_0));
    }

    public TemplateCache(TemplateLoader templateLoader2) {
        this(templateLoader2, (Configuration) null);
    }

    public TemplateCache(TemplateLoader templateLoader2, CacheStorage cacheStorage) {
        this(templateLoader2, cacheStorage, null);
    }

    public TemplateCache(TemplateLoader templateLoader2, Configuration configuration) {
        this(templateLoader2, _TemplateAPI.createDefaultCacheStorage(Configuration.VERSION_2_3_0), configuration);
    }

    public TemplateCache(TemplateLoader templateLoader2, CacheStorage cacheStorage, Configuration configuration) {
        this(templateLoader2, cacheStorage, _TemplateAPI.getDefaultTemplateLookupStrategy(Configuration.VERSION_2_3_0), _TemplateAPI.getDefaultTemplateNameFormat(Configuration.VERSION_2_3_0), configuration);
    }

    public TemplateCache(TemplateLoader templateLoader2, CacheStorage cacheStorage, TemplateLookupStrategy templateLookupStrategy2, TemplateNameFormat templateNameFormat2, Configuration configuration) {
        this.updateDelay = 5000;
        boolean z = true;
        this.localizedLookup = true;
        this.templateLoader = templateLoader2;
        NullArgumentException.check(Configuration.CACHE_STORAGE_KEY_CAMEL_CASE, cacheStorage);
        this.storage = cacheStorage;
        this.isStorageConcurrent = (!(cacheStorage instanceof ConcurrentCacheStorage) || !((ConcurrentCacheStorage) cacheStorage).isConcurrent()) ? false : z;
        NullArgumentException.check(Configuration.TEMPLATE_LOOKUP_STRATEGY_KEY_CAMEL_CASE, templateLookupStrategy2);
        this.templateLookupStrategy = templateLookupStrategy2;
        NullArgumentException.check(Configuration.TEMPLATE_NAME_FORMAT_KEY_CAMEL_CASE, templateNameFormat2);
        this.templateNameFormat = templateNameFormat2;
        this.config = configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.config = configuration;
        clear();
    }

    public TemplateLoader getTemplateLoader() {
        return this.templateLoader;
    }

    public CacheStorage getCacheStorage() {
        return this.storage;
    }

    public TemplateLookupStrategy getTemplateLookupStrategy() {
        return this.templateLookupStrategy;
    }

    public TemplateNameFormat getTemplateNameFormat() {
        return this.templateNameFormat;
    }

    public MaybeMissingTemplate getTemplate(String str, Locale locale, Object obj, String str2, boolean z) throws IOException {
        NullArgumentException.check(Config.FEED_LIST_NAME, str);
        NullArgumentException.check("locale", locale);
        NullArgumentException.check("encoding", str2);
        try {
            String normalizeAbsoluteName = this.templateNameFormat.normalizeAbsoluteName(str);
            TemplateLoader templateLoader2 = this.templateLoader;
            if (templateLoader2 == null) {
                return new MaybeMissingTemplate(normalizeAbsoluteName, "The TemplateLoader was null.");
            }
            Template template = getTemplate(templateLoader2, normalizeAbsoluteName, locale, obj, str2, z);
            return template != null ? new MaybeMissingTemplate(template) : new MaybeMissingTemplate(normalizeAbsoluteName, (String) null);
        } catch (MalformedTemplateNameException e) {
            if (this.templateNameFormat == TemplateNameFormat.DEFAULT_2_3_0 && this.config.getIncompatibleImprovements().intValue() < _TemplateAPI.VERSION_INT_2_4_0) {
                return new MaybeMissingTemplate((String) null, e);
            }
            throw e;
        }
    }

    public Template getTemplate(String str, Locale locale, String str2, boolean z) throws IOException {
        return getTemplate(str, locale, null, str2, z).getTemplate();
    }

    protected static TemplateLoader createLegacyDefaultTemplateLoader() {
        return _TemplateAPI.createDefaultTemplateLoader(Configuration.VERSION_2_3_0);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:208:0x033d */
    /* JADX WARN: Type inference failed for: r12v0 */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r12v1 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v52 */
    /* JADX WARN: Type inference failed for: r12v23 */
    /* JADX WARN: Type inference failed for: r2v58 */
    /* JADX WARN: Type inference failed for: r12v24 */
    /* JADX WARN: Type inference failed for: r12v26 */
    /* JADX WARN: Type inference failed for: r2v72 */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01ef, code lost:
        r12 = null;
        r2 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01f3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x01f4, code lost:
        r13 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x021a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x021b, code lost:
        r2 = r11;
        r13 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x021f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0220, code lost:
        r13 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0292, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0293, code lost:
        r1 = r8;
        r2 = r11;
        r12 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0298, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0299, code lost:
        r1 = r8;
        r2 = r11;
        r12 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x02e8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x02e9, code lost:
        r2 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x02fb, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x02fc, code lost:
        r2 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x02fe, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0301, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x0302, code lost:
        r2 = r11;
        r6 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0321, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x0322, code lost:
        r2 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x0324, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x0325, code lost:
        r2 = r11;
        r13 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x0329, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x032a, code lost:
        r13 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c1, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c2, code lost:
        r15 = r4;
        r2 = r11;
        r13 = r1;
        r12 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01bd, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01be, code lost:
        r12 = r2;
        r2 = r11;
        r13 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01c3, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01c4, code lost:
        r13 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01d7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01d8, code lost:
        r6 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01e7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01e8, code lost:
        r2 = r11;
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01ec, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01ed, code lost:
        r2 = r11;
        r13 = r1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:15:0x0055, B:107:0x01fe] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01f3 A[ExcHandler: RuntimeException (e java.lang.RuntimeException), Splitter:B:15:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x02e8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:141:0x0267] */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x0301 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:120:0x0234] */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x0321 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:116:0x0227] */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x033d A[SYNTHETIC, Splitter:B:208:0x033d] */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x0349 A[Catch:{ RuntimeException -> 0x0341, IOException -> 0x0333, all -> 0x032e, all -> 0x034d }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01d7 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:47:0x00d7] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01e7 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:15:0x0055] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:38:0x00a2=Splitter:B:38:0x00a2, B:43:0x00c8=Splitter:B:43:0x00c8} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private freemarker.template.Template getTemplate(freemarker.cache.TemplateLoader r20, java.lang.String r21, java.util.Locale r22, java.lang.Object r23, java.lang.String r24, boolean r25) throws java.io.IOException {
        /*
            r19 = this;
            r10 = r19
            r11 = r20
            r0 = r21
            r7 = r22
            r8 = r23
            freemarker.log.Logger r1 = freemarker.cache.TemplateCache.LOG
            boolean r9 = r1.isDebugEnabled()
            if (r9 == 0) goto L_0x0024
            r1 = r19
            r2 = r21
            r3 = r22
            r4 = r23
            r5 = r24
            r6 = r25
            java.lang.String r1 = r1.buildDebugName(r2, r3, r4, r5, r6)
            r13 = r1
            goto L_0x0025
        L_0x0024:
            r13 = 0
        L_0x0025:
            freemarker.cache.TemplateCache$TemplateKey r14 = new freemarker.cache.TemplateCache$TemplateKey
            r1 = r14
            r2 = r21
            r3 = r22
            r4 = r23
            r5 = r24
            r6 = r25
            r1.<init>(r2, r3, r4, r5, r6)
            boolean r1 = r10.isStorageConcurrent
            if (r1 == 0) goto L_0x0042
            freemarker.cache.CacheStorage r1 = r10.storage
            java.lang.Object r1 = r1.get(r14)
            freemarker.cache.TemplateCache$CachedTemplate r1 = (freemarker.cache.TemplateCache.CachedTemplate) r1
            goto L_0x004f
        L_0x0042:
            freemarker.cache.CacheStorage r1 = r10.storage
            monitor-enter(r1)
            freemarker.cache.CacheStorage r2 = r10.storage     // Catch:{ all -> 0x035f }
            java.lang.Object r2 = r2.get(r14)     // Catch:{ all -> 0x035f }
            freemarker.cache.TemplateCache$CachedTemplate r2 = (freemarker.cache.TemplateCache.CachedTemplate) r2     // Catch:{ all -> 0x035f }
            monitor-exit(r1)     // Catch:{ all -> 0x035f }
            r1 = r2
        L_0x004f:
            long r2 = java.lang.System.currentTimeMillis()
            if (r1 == 0) goto L_0x01f9
            long r4 = r1.lastChecked     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            long r4 = r2 - r4
            r17 = r13
            long r12 = r10.updateDelay     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            int r18 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
            if (r18 >= 0) goto L_0x00cb
            if (r9 == 0) goto L_0x007b
            freemarker.log.Logger r0 = freemarker.cache.TemplateCache.LOG     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            r2.<init>()     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            r12 = r17
            r2.append(r12)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            java.lang.String r3 = " cached copy not yet stale; using cached."
            r2.append(r3)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            java.lang.String r2 = r2.toString()     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            r0.debug(r2)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
        L_0x007b:
            java.lang.Object r0 = r1.templateOrException     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            boolean r2 = r0 instanceof freemarker.template.Template     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            if (r2 != 0) goto L_0x00c8
            if (r0 != 0) goto L_0x0084
            goto L_0x00c8
        L_0x0084:
            boolean r2 = r0 instanceof java.lang.RuntimeException     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            if (r2 != 0) goto L_0x009b
            boolean r2 = r0 instanceof java.io.IOException     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            if (r2 == 0) goto L_0x00a1
            r2 = r0
            java.io.IOException r2 = (java.io.IOException) r2     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x0094, all -> 0x01e7 }
            r10.throwLoadFailedException(r2)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x0094, all -> 0x01e7 }
            r4 = 1
            goto L_0x00a2
        L_0x0094:
            r0 = move-exception
            r2 = r11
            r13 = r14
            r12 = 0
            r15 = 1
            goto L_0x033b
        L_0x009b:
            r2 = r0
            java.lang.RuntimeException r2 = (java.lang.RuntimeException) r2     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            r10.throwLoadFailedException(r2)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
        L_0x00a1:
            r4 = 0
        L_0x00a2:
            freemarker.core.BugException r2 = new freemarker.core.BugException     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            r3.<init>()     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            java.lang.String r5 = "t is "
            r3.append(r5)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            java.lang.String r0 = r0.getName()     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            r3.append(r0)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            java.lang.String r0 = r3.toString()     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            r2.<init>(r0)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
            throw r2     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x00c1, all -> 0x01e7 }
        L_0x00c1:
            r0 = move-exception
            r15 = r4
            r2 = r11
            r13 = r14
            r12 = 0
            goto L_0x033b
        L_0x00c8:
            freemarker.template.Template r0 = (freemarker.template.Template) r0     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            return r0
        L_0x00cb:
            r12 = r17
            freemarker.cache.TemplateCache$CachedTemplate r1 = r1.cloneCachedTemplate()     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            r1.lastChecked = r2     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            freemarker.cache.TemplateLookupResult r2 = r10.lookupTemplate(r0, r7, r8)     // Catch:{ RuntimeException -> 0x01f3, IOException -> 0x01ec, all -> 0x01e7 }
            boolean r3 = r2.isPositive()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            if (r3 != 0) goto L_0x0109
            if (r9 == 0) goto L_0x00f5
            freemarker.log.Logger r0 = freemarker.cache.TemplateCache.LOG     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r3.<init>()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r3.append(r12)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r4 = " no source found."
            r3.append(r4)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r0.debug(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
        L_0x00f5:
            r3 = 0
            r10.storeNegativeLookup(r14, r1, r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            if (r2 == 0) goto L_0x0108
            boolean r0 = r2.isPositive()
            if (r0 == 0) goto L_0x0108
            java.lang.Object r0 = r2.getTemplateSource()
            r11.closeTemplateSource(r0)
        L_0x0108:
            return r3
        L_0x0109:
            java.lang.Object r3 = r2.getTemplateSource()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            long r5 = r11.getLastModified(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            long r7 = r1.lastModified     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            int r4 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r4 != 0) goto L_0x0119
            r4 = 1
            goto L_0x011a
        L_0x0119:
            r4 = 0
        L_0x011a:
            java.lang.Object r7 = r1.source     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            boolean r7 = r3.equals(r7)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            if (r4 == 0) goto L_0x015b
            if (r7 == 0) goto L_0x015b
            if (r9 == 0) goto L_0x0144
            freemarker.log.Logger r0 = freemarker.cache.TemplateCache.LOG     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r4.<init>()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r4.append(r12)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r5 = ": using cached since "
            r4.append(r5)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r4.append(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r3 = " hasn't changed."
            r4.append(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r3 = r4.toString()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r0.debug(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
        L_0x0144:
            r10.storeCached(r14, r1)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.Object r0 = r1.templateOrException     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            freemarker.template.Template r0 = (freemarker.template.Template) r0     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            if (r2 == 0) goto L_0x015a
            boolean r1 = r2.isPositive()
            if (r1 == 0) goto L_0x015a
            java.lang.Object r1 = r2.getTemplateSource()
            r11.closeTemplateSource(r1)
        L_0x015a:
            return r0
        L_0x015b:
            if (r9 == 0) goto L_0x01ca
            if (r7 != 0) goto L_0x0190
            freemarker.log.Logger r4 = freemarker.cache.TemplateCache.LOG     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r8.<init>()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r13 = "Updating source because: sourceEquals="
            r8.append(r13)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r8.append(r7)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r7 = ", newlyFoundSource="
            r8.append(r7)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r3 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r8.append(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r3 = ", cached.source="
            r8.append(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.Object r3 = r1.source     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r3 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r8.append(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r3 = r8.toString()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r4.debug(r3)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            goto L_0x01ca
        L_0x0190:
            if (r4 != 0) goto L_0x01ca
            freemarker.log.Logger r3 = freemarker.cache.TemplateCache.LOG     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.StringBuffer r7 = new java.lang.StringBuffer     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r7.<init>()     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r8 = "Updating source because: lastModifiedNotChanged="
            r7.append(r8)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r7.append(r4)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            java.lang.String r4 = ", cached.lastModified="
            r7.append(r4)     // Catch:{ RuntimeException -> 0x01e1, IOException -> 0x01dc, all -> 0x01d7 }
            r16 = r14
            long r13 = r1.lastModified     // Catch:{ RuntimeException -> 0x01c3, IOException -> 0x01bd, all -> 0x01d7 }
            r7.append(r13)     // Catch:{ RuntimeException -> 0x01c3, IOException -> 0x01bd, all -> 0x01d7 }
            java.lang.String r4 = " != source.lastModified="
            r7.append(r4)     // Catch:{ RuntimeException -> 0x01c3, IOException -> 0x01bd, all -> 0x01d7 }
            r7.append(r5)     // Catch:{ RuntimeException -> 0x01c3, IOException -> 0x01bd, all -> 0x01d7 }
            java.lang.String r4 = r7.toString()     // Catch:{ RuntimeException -> 0x01c3, IOException -> 0x01bd, all -> 0x01d7 }
            r3.debug(r4)     // Catch:{ RuntimeException -> 0x01c3, IOException -> 0x01bd, all -> 0x01d7 }
            goto L_0x01cc
        L_0x01bd:
            r0 = move-exception
            r12 = r2
            r2 = r11
            r13 = r16
            goto L_0x01f0
        L_0x01c3:
            r0 = move-exception
            r12 = r2
            r2 = r11
            r13 = r16
            goto L_0x0347
        L_0x01ca:
            r16 = r14
        L_0x01cc:
            r7 = r23
            r8 = r1
            r14 = r2
            r1 = r5
            r13 = r16
            r6 = r22
            goto L_0x0267
        L_0x01d7:
            r0 = move-exception
            r6 = r2
        L_0x01d9:
            r2 = r11
            goto L_0x034f
        L_0x01dc:
            r0 = move-exception
            r12 = r2
            r2 = r11
            r13 = r14
            goto L_0x01f0
        L_0x01e1:
            r0 = move-exception
            r12 = r2
            r2 = r11
            r13 = r14
            goto L_0x0347
        L_0x01e7:
            r0 = move-exception
            r2 = r11
            r6 = 0
            goto L_0x034f
        L_0x01ec:
            r0 = move-exception
            r2 = r11
            r13 = r14
        L_0x01ef:
            r12 = 0
        L_0x01f0:
            r15 = 0
            goto L_0x033b
        L_0x01f3:
            r0 = move-exception
            r2 = r11
            r13 = r14
        L_0x01f6:
            r12 = 0
            goto L_0x0347
        L_0x01f9:
            r12 = r13
            r16 = r14
            if (r9 == 0) goto L_0x0224
            freemarker.log.Logger r4 = freemarker.cache.TemplateCache.LOG     // Catch:{ RuntimeException -> 0x021f, IOException -> 0x021a, all -> 0x01e7 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ RuntimeException -> 0x021f, IOException -> 0x021a, all -> 0x01e7 }
            r5.<init>()     // Catch:{ RuntimeException -> 0x021f, IOException -> 0x021a, all -> 0x01e7 }
            java.lang.String r7 = "Couldn't find template in cache for "
            r5.append(r7)     // Catch:{ RuntimeException -> 0x021f, IOException -> 0x021a, all -> 0x01e7 }
            r5.append(r12)     // Catch:{ RuntimeException -> 0x021f, IOException -> 0x021a, all -> 0x01e7 }
            java.lang.String r7 = "; will try to load it."
            r5.append(r7)     // Catch:{ RuntimeException -> 0x021f, IOException -> 0x021a, all -> 0x01e7 }
            java.lang.String r5 = r5.toString()     // Catch:{ RuntimeException -> 0x021f, IOException -> 0x021a, all -> 0x01e7 }
            r4.debug(r5)     // Catch:{ RuntimeException -> 0x021f, IOException -> 0x021a, all -> 0x01e7 }
            goto L_0x0224
        L_0x021a:
            r0 = move-exception
            r2 = r11
            r13 = r16
            goto L_0x01ef
        L_0x021f:
            r0 = move-exception
            r2 = r11
            r13 = r16
            goto L_0x01f6
        L_0x0224:
            freemarker.cache.TemplateCache$CachedTemplate r4 = new freemarker.cache.TemplateCache$CachedTemplate     // Catch:{ RuntimeException -> 0x0341, IOException -> 0x0333, all -> 0x032e }
            r5 = 0
            r4.<init>()     // Catch:{ RuntimeException -> 0x0329, IOException -> 0x0324, all -> 0x0321 }
            r4.lastChecked = r2     // Catch:{ RuntimeException -> 0x031b, IOException -> 0x0315, all -> 0x0321 }
            r6 = r22
            r7 = r23
            freemarker.cache.TemplateLookupResult r1 = r10.lookupTemplate(r0, r6, r7)     // Catch:{ RuntimeException -> 0x031b, IOException -> 0x0315, all -> 0x0321 }
            boolean r2 = r1.isPositive()     // Catch:{ RuntimeException -> 0x030e, IOException -> 0x0306, all -> 0x0301 }
            if (r2 != 0) goto L_0x025d
            r13 = r16
            r10.storeNegativeLookup(r13, r4, r5)     // Catch:{ RuntimeException -> 0x0257, IOException -> 0x0252, all -> 0x024f }
            if (r1 == 0) goto L_0x024e
            boolean r0 = r1.isPositive()
            if (r0 == 0) goto L_0x024e
            java.lang.Object r0 = r1.getTemplateSource()
            r11.closeTemplateSource(r0)
        L_0x024e:
            return r5
        L_0x024f:
            r0 = move-exception
            r6 = r1
            goto L_0x01d9
        L_0x0252:
            r0 = move-exception
            r12 = r1
            r1 = r4
            r2 = r11
            goto L_0x01f0
        L_0x0257:
            r0 = move-exception
            r12 = r1
            r1 = r4
            r2 = r11
            goto L_0x0347
        L_0x025d:
            r13 = r16
            r2 = -9223372036854775808
            r4.lastModified = r2     // Catch:{ RuntimeException -> 0x02fe, IOException -> 0x02fb, all -> 0x0301 }
            r14 = r1
            r8 = r4
            r1 = -9223372036854775808
        L_0x0267:
            java.lang.Object r3 = r14.getTemplateSource()     // Catch:{ RuntimeException -> 0x02f4, IOException -> 0x02ed, all -> 0x02e8 }
            r8.source = r3     // Catch:{ RuntimeException -> 0x02f4, IOException -> 0x02ed, all -> 0x02e8 }
            if (r9 == 0) goto L_0x029e
            freemarker.log.Logger r4 = freemarker.cache.TemplateCache.LOG     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            r5.<init>()     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            java.lang.String r9 = "Loading template for "
            r5.append(r9)     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            r5.append(r12)     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            java.lang.String r9 = " from "
            r5.append(r9)     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            java.lang.String r9 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r3)     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            r5.append(r9)     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            java.lang.String r5 = r5.toString()     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            r4.debug(r5)     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
            goto L_0x029e
        L_0x0292:
            r0 = move-exception
            r1 = r8
            r2 = r11
            r12 = r14
            goto L_0x01f0
        L_0x0298:
            r0 = move-exception
            r1 = r8
            r2 = r11
            r12 = r14
            goto L_0x0347
        L_0x029e:
            r4 = -9223372036854775808
            int r9 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r9 != 0) goto L_0x02a8
            long r1 = r11.getLastModified(r3)     // Catch:{ RuntimeException -> 0x0298, IOException -> 0x0292, all -> 0x02e8 }
        L_0x02a8:
            r4 = r1
            java.lang.String r9 = r14.getTemplateSourceName()     // Catch:{ RuntimeException -> 0x02f4, IOException -> 0x02ed, all -> 0x02e8 }
            r1 = r19
            r2 = r20
            r11 = r4
            r4 = r21
            r5 = r9
            r6 = r22
            r7 = r23
            r9 = r8
            r8 = r24
            r15 = r9
            r9 = r25
            freemarker.template.Template r0 = r1.loadTemplate(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ RuntimeException -> 0x02e4, IOException -> 0x02e0, all -> 0x02dc }
            r15.templateOrException = r0     // Catch:{ RuntimeException -> 0x02e4, IOException -> 0x02e0, all -> 0x02dc }
            r15.lastModified = r11     // Catch:{ RuntimeException -> 0x02e4, IOException -> 0x02e0, all -> 0x02dc }
            r10.storeCached(r13, r15)     // Catch:{ RuntimeException -> 0x02e4, IOException -> 0x02e0, all -> 0x02dc }
            if (r14 == 0) goto L_0x02db
            boolean r1 = r14.isPositive()
            if (r1 == 0) goto L_0x02db
            java.lang.Object r1 = r14.getTemplateSource()
            r2 = r20
            r2.closeTemplateSource(r1)
        L_0x02db:
            return r0
        L_0x02dc:
            r0 = move-exception
            r2 = r20
            goto L_0x02ea
        L_0x02e0:
            r0 = move-exception
            r2 = r20
            goto L_0x02f0
        L_0x02e4:
            r0 = move-exception
            r2 = r20
            goto L_0x02f7
        L_0x02e8:
            r0 = move-exception
            r2 = r11
        L_0x02ea:
            r6 = r14
            goto L_0x034f
        L_0x02ed:
            r0 = move-exception
            r15 = r8
            r2 = r11
        L_0x02f0:
            r12 = r14
            r1 = r15
            goto L_0x01f0
        L_0x02f4:
            r0 = move-exception
            r15 = r8
            r2 = r11
        L_0x02f7:
            r12 = r14
            r1 = r15
            goto L_0x0347
        L_0x02fb:
            r0 = move-exception
            r2 = r11
            goto L_0x030a
        L_0x02fe:
            r0 = move-exception
            r2 = r11
            goto L_0x0312
        L_0x0301:
            r0 = move-exception
            r2 = r11
            r6 = r1
            goto L_0x034f
        L_0x0306:
            r0 = move-exception
            r2 = r11
            r13 = r16
        L_0x030a:
            r12 = r1
            r1 = r4
            goto L_0x01f0
        L_0x030e:
            r0 = move-exception
            r2 = r11
            r13 = r16
        L_0x0312:
            r12 = r1
            r1 = r4
            goto L_0x0347
        L_0x0315:
            r0 = move-exception
            r2 = r11
            r13 = r16
            r1 = r4
            goto L_0x0338
        L_0x031b:
            r0 = move-exception
            r2 = r11
            r13 = r16
            r1 = r4
            goto L_0x0346
        L_0x0321:
            r0 = move-exception
            r2 = r11
            goto L_0x0331
        L_0x0324:
            r0 = move-exception
            r2 = r11
            r13 = r16
            goto L_0x0338
        L_0x0329:
            r0 = move-exception
            r2 = r11
            r13 = r16
            goto L_0x0346
        L_0x032e:
            r0 = move-exception
            r2 = r11
            r5 = 0
        L_0x0331:
            r6 = r5
            goto L_0x034f
        L_0x0333:
            r0 = move-exception
            r2 = r11
            r13 = r16
            r5 = 0
        L_0x0338:
            r12 = r5
            goto L_0x01f0
        L_0x033b:
            if (r15 != 0) goto L_0x0340
            r10.storeNegativeLookup(r13, r1, r0)     // Catch:{ all -> 0x034d }
        L_0x0340:
            throw r0     // Catch:{ all -> 0x034d }
        L_0x0341:
            r0 = move-exception
            r2 = r11
            r13 = r16
            r5 = 0
        L_0x0346:
            r12 = r5
        L_0x0347:
            if (r1 == 0) goto L_0x034c
            r10.storeNegativeLookup(r13, r1, r0)     // Catch:{ all -> 0x034d }
        L_0x034c:
            throw r0     // Catch:{ all -> 0x034d }
        L_0x034d:
            r0 = move-exception
            r6 = r12
        L_0x034f:
            if (r6 == 0) goto L_0x035e
            boolean r1 = r6.isPositive()
            if (r1 == 0) goto L_0x035e
            java.lang.Object r1 = r6.getTemplateSource()
            r2.closeTemplateSource(r1)
        L_0x035e:
            throw r0
        L_0x035f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x035f }
            goto L_0x0363
        L_0x0362:
            throw r0
        L_0x0363:
            goto L_0x0362
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.cache.TemplateCache.getTemplate(freemarker.cache.TemplateLoader, java.lang.String, java.util.Locale, java.lang.Object, java.lang.String, boolean):freemarker.template.Template");
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static final Method getInitCauseMethod() {
        Class cls;
        Class cls2;
        try {
            if (class$java$lang$Throwable == null) {
                cls = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls;
            } else {
                cls = class$java$lang$Throwable;
            }
            Class[] clsArr = new Class[1];
            if (class$java$lang$Throwable == null) {
                cls2 = class$("java.lang.Throwable");
                class$java$lang$Throwable = cls2;
            } else {
                cls2 = class$java$lang$Throwable;
            }
            clsArr[0] = cls2;
            return cls.getMethod("initCause", clsArr);
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    private void throwLoadFailedException(Exception exc) throws IOException {
        if (INIT_CAUSE != null) {
            IOException iOException = new IOException("There was an error loading the template on an earlier attempt; it's attached as a cause");
            try {
                INIT_CAUSE.invoke(iOException, exc);
                throw iOException;
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e2) {
                throw new UndeclaredThrowableException(e2);
            }
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("There was an error loading the template on an earlier attempt: ");
            stringBuffer.append(exc.getClass().getName());
            stringBuffer.append(": ");
            stringBuffer.append(exc.getMessage());
            throw new IOException(stringBuffer.toString());
        }
    }

    private void storeNegativeLookup(TemplateKey templateKey, CachedTemplate cachedTemplate, Exception exc) {
        cachedTemplate.templateOrException = exc;
        cachedTemplate.source = null;
        cachedTemplate.lastModified = 0;
        storeCached(templateKey, cachedTemplate);
    }

    private void storeCached(TemplateKey templateKey, CachedTemplate cachedTemplate) {
        if (this.isStorageConcurrent) {
            this.storage.put(templateKey, cachedTemplate);
            return;
        }
        synchronized (this.storage) {
            this.storage.put(templateKey, cachedTemplate);
        }
    }

    /* JADX INFO: finally extract failed */
    private Template loadTemplate(TemplateLoader templateLoader2, Object obj, String str, String str2, Locale locale, Object obj2, String str3, boolean z) throws IOException {
        Template template;
        Reader reader;
        if (z) {
            try {
                reader = templateLoader2.getReader(obj, str3);
                template = new Template(str, str2, reader, this.config, str3);
                reader.close();
            } catch (Template.WrongEncodingException e) {
                String templateSpecifiedEncoding = e.getTemplateSpecifiedEncoding();
                if (LOG.isDebugEnabled()) {
                    Logger logger = LOG;
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Initial encoding \"");
                    stringBuffer.append(str3);
                    stringBuffer.append("\" was incorrect, re-reading with \"");
                    stringBuffer.append(templateSpecifiedEncoding);
                    stringBuffer.append("\". Template: ");
                    stringBuffer.append(str2);
                    logger.debug(stringBuffer.toString());
                }
                Reader reader2 = templateLoader2.getReader(obj, templateSpecifiedEncoding);
                try {
                    Template template2 = new Template(str, str2, reader2, this.config, templateSpecifiedEncoding);
                    reader2.close();
                    template = template2;
                    str3 = templateSpecifiedEncoding;
                } catch (Throwable th) {
                    reader2.close();
                    throw th;
                }
            } catch (Throwable th2) {
                reader.close();
                throw th2;
            }
        } else {
            StringWriter stringWriter = new StringWriter();
            char[] cArr = new char[4096];
            Reader reader3 = templateLoader2.getReader(obj, str3);
            while (true) {
                try {
                    int read = reader3.read(cArr);
                    if (read > 0) {
                        stringWriter.write(cArr, 0, read);
                    } else if (read < 0) {
                        break;
                    }
                } catch (Throwable th3) {
                    reader3.close();
                    throw th3;
                }
            }
            reader3.close();
            template = Template.getPlainTextTemplate(str, str2, stringWriter.toString(), this.config);
        }
        template.setLocale(locale);
        template.setCustomLookupCondition(obj2);
        template.setEncoding(str3);
        return template;
    }

    public long getDelay() {
        long j;
        synchronized (this) {
            j = this.updateDelay;
        }
        return j;
    }

    public void setDelay(long j) {
        synchronized (this) {
            this.updateDelay = j;
        }
    }

    public boolean getLocalizedLookup() {
        boolean z;
        synchronized (this) {
            z = this.localizedLookup;
        }
        return z;
    }

    public void setLocalizedLookup(boolean z) {
        synchronized (this) {
            if (this.localizedLookup != z) {
                this.localizedLookup = z;
                clear();
            }
        }
    }

    public void clear() {
        synchronized (this.storage) {
            this.storage.clear();
            if (this.templateLoader instanceof StatefulTemplateLoader) {
                ((StatefulTemplateLoader) this.templateLoader).resetState();
            }
        }
    }

    public void removeTemplate(String str, Locale locale, String str2, boolean z) throws IOException {
        removeTemplate(str, locale, null, str2, z);
    }

    public void removeTemplate(String str, Locale locale, Object obj, String str2, boolean z) throws IOException {
        if (str == null) {
            throw new IllegalArgumentException("Argument \"name\" can't be null");
        } else if (locale == null) {
            throw new IllegalArgumentException("Argument \"locale\" can't be null");
        } else if (str2 != null) {
            String normalizeAbsoluteName = this.templateNameFormat.normalizeAbsoluteName(str);
            if (normalizeAbsoluteName != null && this.templateLoader != null) {
                String buildDebugName = LOG.isDebugEnabled() ? buildDebugName(normalizeAbsoluteName, locale, obj, str2, z) : null;
                TemplateKey templateKey = new TemplateKey(normalizeAbsoluteName, locale, obj, str2, z);
                if (this.isStorageConcurrent) {
                    this.storage.remove(templateKey);
                } else {
                    synchronized (this.storage) {
                        this.storage.remove(templateKey);
                    }
                }
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(buildDebugName);
                stringBuffer.append(" was removed from the cache, if it was there");
                logger.debug(stringBuffer.toString());
            }
        } else {
            throw new IllegalArgumentException("Argument \"encoding\" can't be null");
        }
    }

    private String buildDebugName(String str, Locale locale, Object obj, String str2, boolean z) {
        String str3;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(StringUtil.jQuoteNoXSS(str));
        stringBuffer.append("(");
        stringBuffer.append(StringUtil.jQuoteNoXSS(locale));
        if (obj != null) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(", cond=");
            stringBuffer2.append(StringUtil.jQuoteNoXSS(obj));
            str3 = stringBuffer2.toString();
        } else {
            str3 = "";
        }
        stringBuffer.append(str3);
        stringBuffer.append(", ");
        stringBuffer.append(str2);
        stringBuffer.append(z ? ", parsed)" : ", unparsed]");
        return stringBuffer.toString();
    }

    public static String getFullTemplatePath(Environment environment, String str, String str2) {
        try {
            return environment.toFullTemplateName(str, str2);
        } catch (MalformedTemplateNameException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private TemplateLookupResult lookupTemplate(String str, Locale locale, Object obj) throws IOException {
        TemplateLookupResult lookup = this.templateLookupStrategy.lookup(new TemplateCacheTemplateLookupContext(str, locale, obj));
        if (lookup != null) {
            return lookup;
        }
        throw new NullPointerException("Lookup result shouldn't be null");
    }

    /* access modifiers changed from: private */
    public TemplateLookupResult lookupTemplateWithAcquisitionStrategy(String str) throws IOException {
        if (str.indexOf(42) == -1) {
            return TemplateLookupResult.from(str, findTemplateSource(str));
        }
        StringTokenizer stringTokenizer = new StringTokenizer(str, "/");
        ArrayList arrayList = new ArrayList();
        int i = -1;
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            if (nextToken.equals(ASTERISKSTR)) {
                if (i != -1) {
                    arrayList.remove(i);
                }
                i = arrayList.size();
            }
            arrayList.add(nextToken);
        }
        if (i == -1) {
            return TemplateLookupResult.from(str, findTemplateSource(str));
        }
        String concatPath = concatPath(arrayList, 0, i);
        String concatPath2 = concatPath(arrayList, i + 1, arrayList.size());
        if (concatPath2.endsWith("/")) {
            concatPath2 = concatPath2.substring(0, concatPath2.length() - 1);
        }
        StringBuffer stringBuffer = new StringBuffer(str.length());
        stringBuffer.append(concatPath);
        int length = concatPath.length();
        while (true) {
            stringBuffer.append(concatPath2);
            String stringBuffer2 = stringBuffer.toString();
            Object findTemplateSource = findTemplateSource(stringBuffer2);
            if (findTemplateSource != null) {
                return TemplateLookupResult.from(stringBuffer2, findTemplateSource);
            }
            if (length == 0) {
                return TemplateLookupResult.createNegativeResult();
            }
            length = concatPath.lastIndexOf(47, length - 2) + 1;
            stringBuffer.setLength(length);
        }
    }

    private Object findTemplateSource(String str) throws IOException {
        Object findTemplateSource = this.templateLoader.findTemplateSource(str);
        if (LOG.isDebugEnabled()) {
            Logger logger = LOG;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("TemplateLoader.findTemplateSource(");
            stringBuffer.append(StringUtil.jQuote(str));
            stringBuffer.append("): ");
            stringBuffer.append(findTemplateSource == null ? "Not found" : "Found");
            logger.debug(stringBuffer.toString());
        }
        return modifyForConfIcI(findTemplateSource);
    }

    private Object modifyForConfIcI(Object obj) {
        if (obj == null) {
            return null;
        }
        if (this.config.getIncompatibleImprovements().intValue() < _TemplateAPI.VERSION_INT_2_3_21) {
            return obj;
        }
        if (obj instanceof URLTemplateSource) {
            URLTemplateSource uRLTemplateSource = (URLTemplateSource) obj;
            if (uRLTemplateSource.getUseCaches() == null) {
                uRLTemplateSource.setUseCaches(false);
            }
        } else if (obj instanceof MultiTemplateLoader.MultiSource) {
            modifyForConfIcI(((MultiTemplateLoader.MultiSource) obj).getWrappedSource());
        }
        return obj;
    }

    private String concatPath(List list, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer((i2 - i) * 16);
        while (i < i2) {
            stringBuffer.append(list.get(i));
            stringBuffer.append((char) SLASH);
            i++;
        }
        return stringBuffer.toString();
    }

    private static final class TemplateKey {
        private final Object customLookupCondition;
        private final String encoding;
        private final Locale locale;
        private final String name;
        private final boolean parse;

        TemplateKey(String str, Locale locale2, Object obj, String str2, boolean z) {
            this.name = str;
            this.locale = locale2;
            this.customLookupCondition = obj;
            this.encoding = str2;
            this.parse = z;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof TemplateKey)) {
                return false;
            }
            TemplateKey templateKey = (TemplateKey) obj;
            if (this.parse != templateKey.parse || !this.name.equals(templateKey.name) || !this.locale.equals(templateKey.locale) || !nullSafeEquals(this.customLookupCondition, templateKey.customLookupCondition) || !this.encoding.equals(templateKey.encoding)) {
                return false;
            }
            return true;
        }

        private boolean nullSafeEquals(Object obj, Object obj2) {
            if (obj == null) {
                return obj2 == null;
            }
            if (obj2 != null) {
                return obj.equals(obj2);
            }
            return false;
        }

        public int hashCode() {
            int hashCode = (this.name.hashCode() ^ this.locale.hashCode()) ^ this.encoding.hashCode();
            Object obj = this.customLookupCondition;
            return (hashCode ^ (obj != null ? obj.hashCode() : 0)) ^ Boolean.valueOf(!this.parse).hashCode();
        }
    }

    private static final class CachedTemplate implements Cloneable, Serializable {
        private static final long serialVersionUID = 1;
        long lastChecked;
        long lastModified;
        Object source;
        Object templateOrException;

        private CachedTemplate() {
        }

        public CachedTemplate cloneCachedTemplate() {
            try {
                return (CachedTemplate) super.clone();
            } catch (CloneNotSupportedException e) {
                throw new UndeclaredThrowableException(e);
            }
        }
    }

    private class TemplateCacheTemplateLookupContext extends TemplateLookupContext {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        TemplateCacheTemplateLookupContext(String str, Locale locale, Object obj) {
            super(str, !TemplateCache.this.localizedLookup ? null : locale, obj);
        }

        public TemplateLookupResult lookupWithAcquisitionStrategy(String str) throws IOException {
            if (!str.startsWith("/")) {
                return TemplateCache.this.lookupTemplateWithAcquisitionStrategy(str);
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Non-normalized name, starts with \"/\": ");
            stringBuffer.append(str);
            throw new IllegalArgumentException(stringBuffer.toString());
        }

        public TemplateLookupResult lookupWithLocalizedThenAcquisitionStrategy(String str, Locale locale) throws IOException {
            String str2;
            String str3;
            if (locale == null) {
                return lookupWithAcquisitionStrategy(str);
            }
            int lastIndexOf = str.lastIndexOf(46);
            if (lastIndexOf == -1) {
                str2 = str;
            } else {
                str2 = str.substring(0, lastIndexOf);
            }
            if (lastIndexOf == -1) {
                str3 = "";
            } else {
                str3 = str.substring(lastIndexOf);
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(TemplateCache.LOCALE_PART_SEPARATOR);
            stringBuffer.append(locale.toString());
            String stringBuffer2 = stringBuffer.toString();
            StringBuffer stringBuffer3 = new StringBuffer(str.length() + stringBuffer2.length());
            stringBuffer3.append(str2);
            while (true) {
                stringBuffer3.setLength(str2.length());
                stringBuffer3.append(stringBuffer2);
                stringBuffer3.append(str3);
                TemplateLookupResult lookupWithAcquisitionStrategy = lookupWithAcquisitionStrategy(stringBuffer3.toString());
                if (lookupWithAcquisitionStrategy.isPositive()) {
                    return lookupWithAcquisitionStrategy;
                }
                int lastIndexOf2 = stringBuffer2.lastIndexOf(95);
                if (lastIndexOf2 == -1) {
                    return createNegativeLookupResult();
                }
                stringBuffer2 = stringBuffer2.substring(0, lastIndexOf2);
            }
        }
    }

    public static final class MaybeMissingTemplate {
        private final MalformedTemplateNameException missingTemplateCauseException;
        private final String missingTemplateNormalizedName;
        private final String missingTemplateReason;
        private final Template template;

        private MaybeMissingTemplate(Template template2) {
            this.template = template2;
            this.missingTemplateNormalizedName = null;
            this.missingTemplateReason = null;
            this.missingTemplateCauseException = null;
        }

        private MaybeMissingTemplate(String str, MalformedTemplateNameException malformedTemplateNameException) {
            this.template = null;
            this.missingTemplateNormalizedName = str;
            this.missingTemplateReason = null;
            this.missingTemplateCauseException = malformedTemplateNameException;
        }

        private MaybeMissingTemplate(String str, String str2) {
            this.template = null;
            this.missingTemplateNormalizedName = str;
            this.missingTemplateReason = str2;
            this.missingTemplateCauseException = null;
        }

        public Template getTemplate() {
            return this.template;
        }

        public String getMissingTemplateReason() {
            String str = this.missingTemplateReason;
            if (str != null) {
                return str;
            }
            MalformedTemplateNameException malformedTemplateNameException = this.missingTemplateCauseException;
            if (malformedTemplateNameException != null) {
                return malformedTemplateNameException.getMalformednessDescription();
            }
            return null;
        }

        public String getMissingTemplateNormalizedName() {
            return this.missingTemplateNormalizedName;
        }
    }
}
