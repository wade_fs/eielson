package freemarker.cache;

import com.baidu.mobstat.Config;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.utility.StringUtil;

public abstract class TemplateNameFormat {
    public static final TemplateNameFormat DEFAULT_2_3_0 = new Default020300();
    public static final TemplateNameFormat DEFAULT_2_4_0 = new Default020400();

    /* access modifiers changed from: package-private */
    public abstract String normalizeAbsoluteName(String str) throws MalformedTemplateNameException;

    /* access modifiers changed from: package-private */
    public abstract String toAbsoluteName(String str, String str2) throws MalformedTemplateNameException;

    private TemplateNameFormat() {
    }

    private static final class Default020300 extends TemplateNameFormat {
        public String toString() {
            return "TemplateNameFormat.DEFAULT_2_3_0";
        }

        private Default020300() {
            super();
        }

        /* access modifiers changed from: package-private */
        public String toAbsoluteName(String str, String str2) {
            if (str == null || str2.indexOf("://") > 0) {
                return str2;
            }
            if (str2.startsWith("/")) {
                int indexOf = str.indexOf("://");
                if (indexOf <= 0) {
                    return str2.substring(1);
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(str.substring(0, indexOf + 2));
                stringBuffer.append(str2);
                return stringBuffer.toString();
            }
            if (!str.endsWith("/")) {
                str = str.substring(0, str.lastIndexOf("/") + 1);
            }
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(str);
            stringBuffer2.append(str2);
            return stringBuffer2.toString();
        }

        /* access modifiers changed from: package-private */
        public String normalizeAbsoluteName(String str) throws MalformedTemplateNameException {
            TemplateNameFormat.checkNameHasNoNullCharacter(str);
            String str2 = str;
            while (true) {
                int indexOf = str2.indexOf("/../");
                if (indexOf == 0) {
                    throw TemplateNameFormat.newRootLeavingException(str);
                } else if (indexOf != -1) {
                    int lastIndexOf = str2.lastIndexOf(47, indexOf - 1);
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(str2.substring(0, lastIndexOf + 1));
                    stringBuffer.append(str2.substring(indexOf + 4));
                    str2 = stringBuffer.toString();
                } else if (!str2.startsWith("../")) {
                    while (true) {
                        int indexOf2 = str2.indexOf("/./");
                        if (indexOf2 == -1) {
                            break;
                        }
                        StringBuffer stringBuffer2 = new StringBuffer();
                        stringBuffer2.append(str2.substring(0, indexOf2));
                        stringBuffer2.append(str2.substring((indexOf2 + 3) - 1));
                        str2 = stringBuffer2.toString();
                    }
                    if (str2.startsWith("./")) {
                        str2 = str2.substring(2);
                    }
                    if (str2.length() <= 1 || str2.charAt(0) != '/') {
                        return str2;
                    }
                    return str2.substring(1);
                } else {
                    throw TemplateNameFormat.newRootLeavingException(str);
                }
            }
        }
    }

    private static final class Default020400 extends TemplateNameFormat {
        public String toString() {
            return "TemplateNameFormat.DEFAULT_2_4_0";
        }

        private Default020400() {
            super();
        }

        /* access modifiers changed from: package-private */
        public String toAbsoluteName(String str, String str2) {
            if (str == null || findSchemeSectionEnd(str2) != 0) {
                return str2;
            }
            if (str2.startsWith("/")) {
                String substring = str2.substring(1);
                int findSchemeSectionEnd = findSchemeSectionEnd(str);
                if (findSchemeSectionEnd == 0) {
                    return substring;
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(str.substring(0, findSchemeSectionEnd));
                stringBuffer.append(substring);
                return stringBuffer.toString();
            }
            if (!str.endsWith("/")) {
                int lastIndexOf = str.lastIndexOf("/") + 1;
                if (lastIndexOf == 0) {
                    lastIndexOf = findSchemeSectionEnd(str);
                }
                str = str.substring(0, lastIndexOf);
            }
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(str);
            stringBuffer2.append(str2);
            return stringBuffer2.toString();
        }

        /* access modifiers changed from: package-private */
        public String normalizeAbsoluteName(String str) throws MalformedTemplateNameException {
            String str2;
            String str3;
            TemplateNameFormat.checkNameHasNoNullCharacter(str);
            if (str.indexOf(92) == -1) {
                int findSchemeSectionEnd = findSchemeSectionEnd(str);
                if (findSchemeSectionEnd == 0) {
                    str2 = null;
                    str3 = str;
                } else {
                    str2 = str.substring(0, findSchemeSectionEnd);
                    str3 = str.substring(findSchemeSectionEnd);
                }
                if (str3.indexOf(58) == -1) {
                    String removeRedundantStarSteps = removeRedundantStarSteps(resolveDotDotSteps(removeDotSteps(removeRedundantSlashes(str3)), str));
                    if (str2 == null) {
                        return removeRedundantStarSteps;
                    }
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(str2);
                    stringBuffer.append(removeRedundantStarSteps);
                    return stringBuffer.toString();
                }
                throw new MalformedTemplateNameException(str, "The ':' character can only be used after the scheme name (if there's any), not in the path part");
            }
            throw new MalformedTemplateNameException(str, "Backslash (\"\\\") is not allowed in template names. Use slash (\"/\") instead.");
        }

        private int findSchemeSectionEnd(String str) {
            int indexOf = str.indexOf(Config.TRACE_TODAY_VISIT_SPLIT);
            if (indexOf == -1 || str.lastIndexOf(47, indexOf - 1) != -1) {
                return 0;
            }
            int i = indexOf + 2;
            return (i < str.length() && str.charAt(indexOf + 1) == '/' && str.charAt(i) == '/') ? indexOf + 3 : indexOf + 1;
        }

        private String removeRedundantSlashes(String str) {
            String replace;
            while (true) {
                replace = StringUtil.replace(str, "//", "/");
                if (str == replace) {
                    break;
                }
                str = replace;
            }
            return replace.startsWith("/") ? replace.substring(1) : replace;
        }

        private String removeDotSteps(String str) {
            boolean z;
            int length = str.length() - 1;
            while (true) {
                int lastIndexOf = str.lastIndexOf(46, length);
                if (lastIndexOf < 0) {
                    return str;
                }
                int i = lastIndexOf - 1;
                if (lastIndexOf == 0 || str.charAt(i) == '/') {
                    int i2 = lastIndexOf + 1;
                    if (i2 == str.length()) {
                        z = false;
                    } else if (str.charAt(i2) == '/') {
                        z = true;
                    }
                    if (z) {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append(str.substring(0, lastIndexOf));
                        stringBuffer.append(str.substring(lastIndexOf + 2));
                        str = stringBuffer.toString();
                    } else {
                        str = str.substring(0, str.length() - 1);
                    }
                }
                length = i;
            }
        }

        private String resolveDotDotSteps(String str, String str2) throws MalformedTemplateNameException {
            boolean z;
            int i = 0;
            while (true) {
                int indexOf = str.indexOf("..", i);
                if (indexOf < 0) {
                    return str;
                }
                if (indexOf != 0) {
                    if (str.charAt(indexOf - 1) == '/') {
                        int i2 = indexOf + 2;
                        if (i2 == str.length()) {
                            z = false;
                        } else if (str.charAt(i2) == '/') {
                            z = true;
                        }
                        int i3 = indexOf - 2;
                        boolean z2 = false;
                        while (i3 != -1) {
                            int lastIndexOf = str.lastIndexOf(47, i3);
                            if (lastIndexOf == -1) {
                                if (i3 == 0 && str.charAt(0) == '*') {
                                    throw TemplateNameFormat.newRootLeavingException(str2);
                                }
                            } else if (str.charAt(lastIndexOf + 1) == '*' && str.charAt(lastIndexOf + 2) == '/') {
                                i3 = lastIndexOf - 1;
                                z2 = true;
                            }
                            StringBuffer stringBuffer = new StringBuffer();
                            int i4 = lastIndexOf + 1;
                            stringBuffer.append(str.substring(0, i4));
                            stringBuffer.append(z2 ? "*/" : "");
                            stringBuffer.append(str.substring(indexOf + (z ? 3 : 2)));
                            str = stringBuffer.toString();
                            i = i4;
                        }
                        throw TemplateNameFormat.newRootLeavingException(str2);
                    }
                    i = indexOf + 3;
                } else {
                    throw TemplateNameFormat.newRootLeavingException(str2);
                }
            }
        }

        private String removeRedundantStarSteps(String str) {
            String str2;
            int i;
            while (true) {
                int indexOf = str.indexOf("*/*");
                if (indexOf == -1) {
                    break;
                }
                if ((indexOf == 0 || str.charAt(indexOf - 1) == '/') && ((i = indexOf + 3) == str.length() || str.charAt(i) == '/')) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(str.substring(0, indexOf));
                    stringBuffer.append(str.substring(indexOf + 2));
                    str2 = stringBuffer.toString();
                } else {
                    str2 = str;
                }
                if (str == str2) {
                    str = str2;
                    break;
                }
                str = str2;
            }
            if (!str.startsWith("*")) {
                return str;
            }
            if (str.length() == 1) {
                return "";
            }
            return str.charAt(1) == '/' ? str.substring(2) : str;
        }
    }

    /* access modifiers changed from: private */
    public static void checkNameHasNoNullCharacter(String str) throws MalformedTemplateNameException {
        if (str.indexOf(0) != -1) {
            throw new MalformedTemplateNameException(str, "Null character (\\u0000) in the name; possible attack attempt");
        }
    }

    /* access modifiers changed from: private */
    public static MalformedTemplateNameException newRootLeavingException(String str) {
        return new MalformedTemplateNameException(str, "Backing out from the root directory is not allowed");
    }
}
