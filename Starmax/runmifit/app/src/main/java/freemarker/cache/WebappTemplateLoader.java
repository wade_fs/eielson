package freemarker.cache;

import freemarker.log.Logger;
import freemarker.template.utility.CollectionUtils;
import freemarker.template.utility.StringUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import javax.servlet.ServletContext;

public class WebappTemplateLoader implements TemplateLoader {
    private static final Logger LOG = Logger.getLogger("freemarker.cache");
    private boolean attemptFileAccess;
    private final ServletContext servletContext;
    private final String subdirPath;
    private Boolean urlConnectionUsesCaches;

    public WebappTemplateLoader(ServletContext servletContext2) {
        this(servletContext2, "/");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public WebappTemplateLoader(ServletContext servletContext2, String str) {
        this.attemptFileAccess = true;
        if (servletContext2 == null) {
            throw new IllegalArgumentException("servletContext == null");
        } else if (str != null) {
            String replace = str.replace('\\', '/');
            if (!replace.endsWith("/")) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(replace);
                stringBuffer.append("/");
                replace = stringBuffer.toString();
            }
            if (!replace.startsWith("/")) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("/");
                stringBuffer2.append(replace);
                replace = stringBuffer2.toString();
            }
            this.subdirPath = replace;
            this.servletContext = servletContext2;
        } else {
            throw new IllegalArgumentException("path == null");
        }
    }

    public Object findTemplateSource(String str) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.subdirPath);
        stringBuffer.append(str);
        String stringBuffer2 = stringBuffer.toString();
        if (this.attemptFileAccess) {
            try {
                String realPath = this.servletContext.getRealPath(stringBuffer2);
                if (realPath != null) {
                    File file = new File(realPath);
                    if (file.canRead() && file.isFile()) {
                        return file;
                    }
                }
            } catch (SecurityException unused) {
            }
        }
        try {
            URL resource = this.servletContext.getResource(stringBuffer2);
            if (resource == null) {
                return null;
            }
            return new URLTemplateSource(resource, getURLConnectionUsesCaches());
        } catch (MalformedURLException e) {
            Logger logger = LOG;
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append("Could not retrieve resource ");
            stringBuffer3.append(StringUtil.jQuoteNoXSS(stringBuffer2));
            logger.warn(stringBuffer3.toString(), e);
            return null;
        }
    }

    public long getLastModified(Object obj) {
        if (obj instanceof File) {
            return ((File) obj).lastModified();
        }
        return ((URLTemplateSource) obj).lastModified();
    }

    public Reader getReader(Object obj, String str) throws IOException {
        if (obj instanceof File) {
            return new InputStreamReader(new FileInputStream((File) obj), str);
        }
        return new InputStreamReader(((URLTemplateSource) obj).getInputStream(), str);
    }

    public void closeTemplateSource(Object obj) throws IOException {
        if (!(obj instanceof File)) {
            ((URLTemplateSource) obj).close();
        }
    }

    public Boolean getURLConnectionUsesCaches() {
        return this.urlConnectionUsesCaches;
    }

    public void setURLConnectionUsesCaches(Boolean bool) {
        this.urlConnectionUsesCaches = bool;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TemplateLoaderUtils.getClassNameForToString(this));
        stringBuffer.append("(subdirPath=");
        stringBuffer.append(StringUtil.jQuote(this.subdirPath));
        stringBuffer.append(", servletContext={contextPath=");
        stringBuffer.append(StringUtil.jQuote(getContextPath()));
        stringBuffer.append(", displayName=");
        stringBuffer.append(StringUtil.jQuote(this.servletContext.getServletContextName()));
        stringBuffer.append("})");
        return stringBuffer.toString();
    }

    private String getContextPath() {
        try {
            return (String) this.servletContext.getClass().getMethod("getContextPath", CollectionUtils.EMPTY_CLASS_ARRAY).invoke(this.servletContext, CollectionUtils.EMPTY_OBJECT_ARRAY);
        } catch (Throwable unused) {
            return "[can't query before Serlvet 2.5]";
        }
    }

    public boolean getAttemptFileAccess() {
        return this.attemptFileAccess;
    }

    public void setAttemptFileAccess(boolean z) {
        this.attemptFileAccess = z;
    }
}
