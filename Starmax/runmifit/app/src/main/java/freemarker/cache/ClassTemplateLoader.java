package freemarker.cache;

import freemarker.template.utility.NullArgumentException;
import freemarker.template.utility.StringUtil;
import java.net.URL;

public class ClassTemplateLoader extends URLTemplateLoader {
    private final String basePackagePath;
    private final ClassLoader classLoader;
    private final Class resourceLoaderClass;

    public ClassTemplateLoader() {
        this(null, true, null, "/");
    }

    public ClassTemplateLoader(Class cls) {
        this(cls, "");
    }

    public ClassTemplateLoader(Class cls, String str) {
        this(cls, false, null, str);
    }

    public ClassTemplateLoader(ClassLoader classLoader2, String str) {
        this(null, true, classLoader2, str);
    }

    private ClassTemplateLoader(Class cls, boolean z, ClassLoader classLoader2, String str) {
        if (!z) {
            NullArgumentException.check("resourceLoaderClass", cls);
        }
        NullArgumentException.check("basePackagePath", str);
        if (classLoader2 != null) {
            cls = null;
        } else if (cls == null) {
            cls = getClass();
        }
        this.resourceLoaderClass = cls;
        if (this.resourceLoaderClass == null && classLoader2 == null) {
            throw new NullArgumentException("classLoader");
        }
        this.classLoader = classLoader2;
        String canonicalizePrefix = canonicalizePrefix(str);
        if (this.classLoader != null && canonicalizePrefix.startsWith("/")) {
            canonicalizePrefix = canonicalizePrefix.substring(1);
        }
        this.basePackagePath = canonicalizePrefix;
    }

    /* access modifiers changed from: protected */
    public URL getURL(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.basePackagePath);
        stringBuffer.append(str);
        String stringBuffer2 = stringBuffer.toString();
        if (this.basePackagePath.equals("/") && !isSchemeless(stringBuffer2)) {
            return null;
        }
        Class cls = this.resourceLoaderClass;
        if (cls != null) {
            return cls.getResource(stringBuffer2);
        }
        return this.classLoader.getResource(stringBuffer2);
    }

    private static boolean isSchemeless(String str) {
        int length = str.length();
        for (int i = (length <= 0 || str.charAt(0) != '/') ? 0 : 1; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '/') {
                return true;
            }
            if (charAt == ':') {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TemplateLoaderUtils.getClassNameForToString(this));
        stringBuffer.append("(");
        if (this.resourceLoaderClass != null) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("resourceLoaderClass=");
            stringBuffer2.append(this.resourceLoaderClass.getName());
            str = stringBuffer2.toString();
        } else {
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append("classLoader=");
            stringBuffer3.append(StringUtil.jQuote(this.classLoader));
            str = stringBuffer3.toString();
        }
        stringBuffer.append(str);
        stringBuffer.append(", basePackagePath");
        stringBuffer.append("=");
        stringBuffer.append(StringUtil.jQuote(this.basePackagePath));
        String str2 = "";
        if (this.resourceLoaderClass != null && !this.basePackagePath.startsWith("/")) {
            str2 = " /* relatively to resourceLoaderClass pkg */";
        }
        stringBuffer.append(str2);
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    public Class getResourceLoaderClass() {
        return this.resourceLoaderClass;
    }

    public ClassLoader getClassLoader() {
        return this.classLoader;
    }

    public String getBasePackagePath() {
        return this.basePackagePath;
    }
}
