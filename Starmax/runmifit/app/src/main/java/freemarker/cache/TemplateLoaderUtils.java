package freemarker.cache;

final class TemplateLoaderUtils {
    static /* synthetic */ Class class$freemarker$cache$TemplateLoader;
    static /* synthetic */ Class class$freemarker$template$Configuration;

    private TemplateLoaderUtils() {
    }

    public static String getClassNameForToString(TemplateLoader templateLoader) {
        Class<?> cls = templateLoader.getClass();
        Package packageR = cls.getPackage();
        Class cls2 = class$freemarker$template$Configuration;
        if (cls2 == null) {
            cls2 = class$("freemarker.template.Configuration");
            class$freemarker$template$Configuration = cls2;
        }
        if (packageR != cls2.getPackage()) {
            Class cls3 = class$freemarker$cache$TemplateLoader;
            if (cls3 == null) {
                cls3 = class$("freemarker.cache.TemplateLoader");
                class$freemarker$cache$TemplateLoader = cls3;
            }
            if (packageR != cls3.getPackage()) {
                return cls.getName();
            }
        }
        return getSimpleName(cls);
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static String getSimpleName(Class cls) {
        String name = cls.getName();
        int lastIndexOf = name.lastIndexOf(46);
        return lastIndexOf < 0 ? name : name.substring(lastIndexOf + 1);
    }
}
