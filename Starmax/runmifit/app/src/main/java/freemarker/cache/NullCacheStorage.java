package freemarker.cache;

public class NullCacheStorage implements ConcurrentCacheStorage, CacheStorageWithGetSize {
    public static final NullCacheStorage INSTANCE = new NullCacheStorage();

    public void clear() {
    }

    public Object get(Object obj) {
        return null;
    }

    public int getSize() {
        return 0;
    }

    public boolean isConcurrent() {
        return true;
    }

    public void put(Object obj, Object obj2) {
    }

    public void remove(Object obj) {
    }
}
