package freemarker.cache;

import freemarker.template.utility.NullArgumentException;

public abstract class TemplateLookupResult {
    /* access modifiers changed from: package-private */
    public abstract Object getTemplateSource();

    public abstract String getTemplateSourceName();

    public abstract boolean isPositive();

    static TemplateLookupResult createNegativeResult() {
        return NegativeTemplateLookupResult.INSTANCE;
    }

    static TemplateLookupResult from(String str, Object obj) {
        if (obj != null) {
            return new PositiveTemplateLookupResult(str, obj);
        }
        return createNegativeResult();
    }

    private TemplateLookupResult() {
    }

    private static final class PositiveTemplateLookupResult extends TemplateLookupResult {
        private final Object templateSource;
        private final String templateSourceName;

        public boolean isPositive() {
            return true;
        }

        private PositiveTemplateLookupResult(String str, Object obj) {
            super();
            NullArgumentException.check("templateName", str);
            NullArgumentException.check("templateSource", obj);
            if (!(obj instanceof TemplateLookupResult)) {
                this.templateSourceName = str;
                this.templateSource = obj;
                return;
            }
            throw new IllegalArgumentException();
        }

        public String getTemplateSourceName() {
            return this.templateSourceName;
        }

        /* access modifiers changed from: package-private */
        public Object getTemplateSource() {
            return this.templateSource;
        }
    }

    private static final class NegativeTemplateLookupResult extends TemplateLookupResult {
        /* access modifiers changed from: private */
        public static final NegativeTemplateLookupResult INSTANCE = new NegativeTemplateLookupResult();

        /* access modifiers changed from: package-private */
        public Object getTemplateSource() {
            return null;
        }

        public String getTemplateSourceName() {
            return null;
        }

        public boolean isPositive() {
            return false;
        }

        private NegativeTemplateLookupResult() {
            super();
        }
    }
}
