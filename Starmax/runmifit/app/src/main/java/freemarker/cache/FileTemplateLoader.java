package freemarker.cache;

import freemarker.log.Logger;
import freemarker.template.utility.SecurityUtilities;
import freemarker.template.utility.StringUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;

public class FileTemplateLoader implements TemplateLoader {
    private static final int CASE_CHECH_CACHE_HARD_SIZE = 50;
    private static final int CASE_CHECK_CACHE__SOFT_SIZE = 1000;
    private static final boolean EMULATE_CASE_SENSITIVE_FILE_SYSTEM_DEFAULT;
    private static final Logger LOG = Logger.getLogger("freemarker.cache");
    /* access modifiers changed from: private */
    public static final boolean SEP_IS_SLASH;
    public static String SYSTEM_PROPERTY_NAME_EMULATE_CASE_SENSITIVE_FILE_SYSTEM = "org.freemarker.emulateCaseSensitiveFileSystem";
    public final File baseDir;
    /* access modifiers changed from: private */
    public final String canonicalBasePath;
    private MruCacheStorage correctCasePaths;
    /* access modifiers changed from: private */
    public boolean emulateCaseSensitiveFileSystem;

    public void closeTemplateSource(Object obj) {
    }

    static {
        boolean z;
        boolean z2 = false;
        try {
            z = StringUtil.getYesNo(SecurityUtilities.getSystemProperty(SYSTEM_PROPERTY_NAME_EMULATE_CASE_SENSITIVE_FILE_SYSTEM, "false"));
        } catch (Exception unused) {
            z = false;
        }
        EMULATE_CASE_SENSITIVE_FILE_SYSTEM_DEFAULT = z;
        if (File.separatorChar == '/') {
            z2 = true;
        }
        SEP_IS_SLASH = z2;
    }

    public FileTemplateLoader() throws IOException {
        this(new File(SecurityUtilities.getSystemProperty("user.dir")));
    }

    public FileTemplateLoader(File file) throws IOException {
        this(file, false);
    }

    public FileTemplateLoader(final File file, final boolean z) throws IOException {
        try {
            Object[] objArr = (Object[]) AccessController.doPrivileged(new PrivilegedExceptionAction() {
                /* class freemarker.cache.FileTemplateLoader.C32701 */

                public Object run() throws IOException {
                    if (!file.exists()) {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append(file);
                        stringBuffer.append(" does not exist.");
                        throw new FileNotFoundException(stringBuffer.toString());
                    } else if (file.isDirectory()) {
                        Object[] objArr = new Object[2];
                        if (z) {
                            objArr[0] = file;
                            objArr[1] = null;
                        } else {
                            objArr[0] = file.getCanonicalFile();
                            String path = ((File) objArr[0]).getPath();
                            if (!path.endsWith(File.separator)) {
                                StringBuffer stringBuffer2 = new StringBuffer();
                                stringBuffer2.append(path);
                                stringBuffer2.append(File.separatorChar);
                                path = stringBuffer2.toString();
                            }
                            objArr[1] = path;
                        }
                        return objArr;
                    } else {
                        StringBuffer stringBuffer3 = new StringBuffer();
                        stringBuffer3.append(file);
                        stringBuffer3.append(" is not a directory.");
                        throw new IOException(stringBuffer3.toString());
                    }
                }
            });
            this.baseDir = (File) objArr[0];
            this.canonicalBasePath = (String) objArr[1];
            setEmulateCaseSensitiveFileSystem(getEmulateCaseSensitiveFileSystemDefault());
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }

    public Object findTemplateSource(final String str) throws IOException {
        try {
            return AccessController.doPrivileged(new PrivilegedExceptionAction() {
                /* class freemarker.cache.FileTemplateLoader.C32712 */

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
                 arg types: [int, char]
                 candidates:
                  ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
                  ClspMth{java.lang.String.replace(char, char):java.lang.String} */
                public Object run() throws IOException {
                    String str;
                    File file = FileTemplateLoader.this.baseDir;
                    if (FileTemplateLoader.SEP_IS_SLASH) {
                        str = str;
                    } else {
                        str = str.replace('/', File.separatorChar);
                    }
                    File file2 = new File(file, str);
                    if (!file2.isFile()) {
                        return null;
                    }
                    if (FileTemplateLoader.this.canonicalBasePath != null) {
                        String canonicalPath = file2.getCanonicalPath();
                        if (!canonicalPath.startsWith(FileTemplateLoader.this.canonicalBasePath)) {
                            StringBuffer stringBuffer = new StringBuffer();
                            stringBuffer.append(file2.getAbsolutePath());
                            stringBuffer.append(" resolves to ");
                            stringBuffer.append(canonicalPath);
                            stringBuffer.append(" which ");
                            stringBuffer.append(" doesn't start with ");
                            stringBuffer.append(FileTemplateLoader.this.canonicalBasePath);
                            throw new SecurityException(stringBuffer.toString());
                        }
                    }
                    if (!FileTemplateLoader.this.emulateCaseSensitiveFileSystem || FileTemplateLoader.this.isNameCaseCorrect(file2)) {
                        return file2;
                    }
                    return null;
                }
            });
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }

    public long getLastModified(final Object obj) {
        return ((Long) AccessController.doPrivileged(new PrivilegedAction() {
            /* class freemarker.cache.FileTemplateLoader.C32723 */

            public Object run() {
                return new Long(((File) obj).lastModified());
            }
        })).longValue();
    }

    public Reader getReader(final Object obj, final String str) throws IOException {
        try {
            return (Reader) AccessController.doPrivileged(new PrivilegedExceptionAction() {
                /* class freemarker.cache.FileTemplateLoader.C32734 */

                public Object run() throws IOException {
                    Object obj = obj;
                    if (obj instanceof File) {
                        return new InputStreamReader(new FileInputStream((File) obj), str);
                    }
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("templateSource wasn't a File, but a: ");
                    stringBuffer.append(obj.getClass().getName());
                    throw new IllegalArgumentException(stringBuffer.toString());
                }
            });
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }

    /* access modifiers changed from: private */
    public boolean isNameCaseCorrect(File file) throws IOException {
        String path = file.getPath();
        if (this.correctCasePaths.get(path) != null) {
            return true;
        }
        File parentFile = file.getParentFile();
        if (parentFile != null) {
            if (!this.baseDir.equals(parentFile) && !isNameCaseCorrect(parentFile)) {
                return false;
            }
            String[] list = parentFile.list();
            if (list != null) {
                String name = file.getName();
                boolean z = false;
                int i = 0;
                while (!z && i < list.length) {
                    if (name.equals(list[i])) {
                        z = true;
                    }
                    i++;
                }
                if (!z) {
                    for (String str : list) {
                        if (name.equalsIgnoreCase(str)) {
                            if (LOG.isDebugEnabled()) {
                                Logger logger = LOG;
                                StringBuffer stringBuffer = new StringBuffer();
                                stringBuffer.append("Emulating file-not-found because of letter case differences to the real file, for: ");
                                stringBuffer.append(path);
                                logger.debug(stringBuffer.toString());
                            }
                            return false;
                        }
                    }
                }
            }
        }
        this.correctCasePaths.put(path, Boolean.TRUE);
        return true;
    }

    public File getBaseDirectory() {
        return this.baseDir;
    }

    public void setEmulateCaseSensitiveFileSystem(boolean z) {
        if (!z) {
            this.correctCasePaths = null;
        } else if (this.correctCasePaths == null) {
            this.correctCasePaths = new MruCacheStorage(50, 1000);
        }
        this.emulateCaseSensitiveFileSystem = z;
    }

    public boolean getEmulateCaseSensitiveFileSystem() {
        return this.emulateCaseSensitiveFileSystem;
    }

    /* access modifiers changed from: protected */
    public boolean getEmulateCaseSensitiveFileSystemDefault() {
        return EMULATE_CASE_SENSITIVE_FILE_SYSTEM_DEFAULT;
    }

    public String toString() {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TemplateLoaderUtils.getClassNameForToString(this));
        stringBuffer.append("(");
        stringBuffer.append("baseDir=\"");
        stringBuffer.append(this.baseDir);
        stringBuffer.append("\"");
        String str2 = "";
        if (this.canonicalBasePath != null) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(", canonicalBasePath=\"");
            stringBuffer2.append(this.canonicalBasePath);
            stringBuffer2.append("\"");
            str = stringBuffer2.toString();
        } else {
            str = str2;
        }
        stringBuffer.append(str);
        if (this.emulateCaseSensitiveFileSystem) {
            str2 = ", emulateCaseSensitiveFileSystem=true";
        }
        stringBuffer.append(str2);
        stringBuffer.append(")");
        return stringBuffer.toString();
    }
}
