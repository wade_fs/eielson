package freemarker.template;

import freemarker.core._DelayedShortClassName;
import freemarker.core._TemplateModelException;
import freemarker.ext.util.WrapperTemplateModel;
import freemarker.template.utility.ObjectWrapperWithAPISupport;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

public class DefaultNonListCollectionAdapter extends WrappingTemplateModel implements TemplateCollectionModelEx, AdapterTemplateModel, WrapperTemplateModel, TemplateModelWithAPISupport, Serializable {
    private final Collection collection;

    public static DefaultNonListCollectionAdapter adapt(Collection collection2, ObjectWrapperWithAPISupport objectWrapperWithAPISupport) {
        return new DefaultNonListCollectionAdapter(collection2, objectWrapperWithAPISupport);
    }

    private DefaultNonListCollectionAdapter(Collection collection2, ObjectWrapperWithAPISupport objectWrapperWithAPISupport) {
        super(objectWrapperWithAPISupport);
        this.collection = collection2;
    }

    public TemplateModelIterator iterator() throws TemplateModelException {
        return new IteratorAdapter(this.collection.iterator());
    }

    public int size() {
        return this.collection.size();
    }

    public boolean isEmpty() {
        return this.collection.isEmpty();
    }

    public Object getWrappedObject() {
        return this.collection;
    }

    public Object getAdaptedObject(Class cls) {
        return getWrappedObject();
    }

    private class IteratorAdapter implements TemplateModelIterator {
        private final Iterator iterator;

        IteratorAdapter(Iterator it) {
            this.iterator = it;
        }

        public TemplateModel next() throws TemplateModelException {
            if (this.iterator.hasNext()) {
                Object next = this.iterator.next();
                return next instanceof TemplateModel ? (TemplateModel) next : DefaultNonListCollectionAdapter.this.wrap(next);
            }
            throw new TemplateModelException("The collection has no more items.");
        }

        public boolean hasNext() throws TemplateModelException {
            return this.iterator.hasNext();
        }
    }

    public boolean contains(TemplateModel templateModel) throws TemplateModelException {
        Object unwrap = ((ObjectWrapperAndUnwrapper) getObjectWrapper()).unwrap(templateModel);
        try {
            return this.collection.contains(unwrap);
        } catch (ClassCastException e) {
            Object[] objArr = new Object[3];
            objArr[0] = "Failed to check if the collection contains the item. Probably the item's Java type, ";
            objArr[1] = unwrap != null ? new _DelayedShortClassName(unwrap.getClass()) : "Null";
            objArr[2] = ", doesn't match the type of (some of) the collection items; see cause exception.";
            throw new _TemplateModelException(e, objArr);
        }
    }

    public TemplateModel getAPI() throws TemplateModelException {
        return ((ObjectWrapperWithAPISupport) getObjectWrapper()).wrapAsAPI(this.collection);
    }
}
