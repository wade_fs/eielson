package freemarker.template;

import freemarker.ext.util.WrapperTemplateModel;
import java.io.Serializable;
import java.lang.reflect.Array;

public abstract class DefaultArrayAdapter extends WrappingTemplateModel implements TemplateSequenceModel, AdapterTemplateModel, WrapperTemplateModel, Serializable {
    public static DefaultArrayAdapter adapt(Object obj, ObjectWrapperAndUnwrapper objectWrapperAndUnwrapper) {
        Class<?> componentType = obj.getClass().getComponentType();
        if (componentType == null) {
            throw new IllegalArgumentException("Not an array");
        } else if (!componentType.isPrimitive()) {
            return new ObjectArrayAdapter((Object[]) obj, objectWrapperAndUnwrapper);
        } else {
            if (componentType == Integer.TYPE) {
                return new IntArrayAdapter((int[]) obj, objectWrapperAndUnwrapper);
            }
            if (componentType == Double.TYPE) {
                return new DoubleArrayAdapter((double[]) obj, objectWrapperAndUnwrapper);
            }
            if (componentType == Long.TYPE) {
                return new LongArrayAdapter((long[]) obj, objectWrapperAndUnwrapper);
            }
            if (componentType == Boolean.TYPE) {
                return new BooleanArrayAdapter((boolean[]) obj, objectWrapperAndUnwrapper);
            }
            if (componentType == Float.TYPE) {
                return new FloatArrayAdapter((float[]) obj, objectWrapperAndUnwrapper);
            }
            if (componentType == Character.TYPE) {
                return new CharArrayAdapter((char[]) obj, objectWrapperAndUnwrapper);
            }
            if (componentType == Short.TYPE) {
                return new ShortArrayAdapter((short[]) obj, objectWrapperAndUnwrapper);
            }
            if (componentType == Byte.TYPE) {
                return new ByteArrayAdapter((byte[]) obj, objectWrapperAndUnwrapper);
            }
            return new GenericPrimitiveArrayAdapter(obj, objectWrapperAndUnwrapper);
        }
    }

    private DefaultArrayAdapter(ObjectWrapper objectWrapper) {
        super(objectWrapper);
    }

    public final Object getAdaptedObject(Class cls) {
        return getWrappedObject();
    }

    private static class ObjectArrayAdapter extends DefaultArrayAdapter {
        private final Object[] array;

        private ObjectArrayAdapter(Object[] objArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = objArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                Object[] objArr = this.array;
                if (i < objArr.length) {
                    return wrap(objArr[i]);
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class ByteArrayAdapter extends DefaultArrayAdapter {
        private final byte[] array;

        private ByteArrayAdapter(byte[] bArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = bArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                byte[] bArr = this.array;
                if (i < bArr.length) {
                    return wrap(new Byte(bArr[i]));
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class ShortArrayAdapter extends DefaultArrayAdapter {
        private final short[] array;

        private ShortArrayAdapter(short[] sArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = sArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                short[] sArr = this.array;
                if (i < sArr.length) {
                    return wrap(new Short(sArr[i]));
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class IntArrayAdapter extends DefaultArrayAdapter {
        private final int[] array;

        private IntArrayAdapter(int[] iArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = iArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                int[] iArr = this.array;
                if (i < iArr.length) {
                    return wrap(new Integer(iArr[i]));
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class LongArrayAdapter extends DefaultArrayAdapter {
        private final long[] array;

        private LongArrayAdapter(long[] jArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = jArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                long[] jArr = this.array;
                if (i < jArr.length) {
                    return wrap(new Long(jArr[i]));
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class FloatArrayAdapter extends DefaultArrayAdapter {
        private final float[] array;

        private FloatArrayAdapter(float[] fArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = fArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                float[] fArr = this.array;
                if (i < fArr.length) {
                    return wrap(new Float(fArr[i]));
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class DoubleArrayAdapter extends DefaultArrayAdapter {
        private final double[] array;

        private DoubleArrayAdapter(double[] dArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = dArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                double[] dArr = this.array;
                if (i < dArr.length) {
                    return wrap(new Double(dArr[i]));
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class CharArrayAdapter extends DefaultArrayAdapter {
        private final char[] array;

        private CharArrayAdapter(char[] cArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = cArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                char[] cArr = this.array;
                if (i < cArr.length) {
                    return wrap(new Character(cArr[i]));
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class BooleanArrayAdapter extends DefaultArrayAdapter {
        private final boolean[] array;

        private BooleanArrayAdapter(boolean[] zArr, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = zArr;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i >= 0) {
                boolean[] zArr = this.array;
                if (i < zArr.length) {
                    return wrap(new Boolean(zArr[i]));
                }
            }
            return null;
        }

        public int size() throws TemplateModelException {
            return this.array.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }

    private static class GenericPrimitiveArrayAdapter extends DefaultArrayAdapter {
        private final Object array;
        private final int length;

        private GenericPrimitiveArrayAdapter(Object obj, ObjectWrapper objectWrapper) {
            super(objectWrapper);
            this.array = obj;
            this.length = Array.getLength(obj);
        }

        public TemplateModel get(int i) throws TemplateModelException {
            if (i < 0 || i >= this.length) {
                return null;
            }
            return wrap(Array.get(this.array, i));
        }

        public int size() throws TemplateModelException {
            return this.length;
        }

        public Object getWrappedObject() {
            return this.array;
        }
    }
}
