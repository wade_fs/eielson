package freemarker.template;

import java.io.IOException;

public class MalformedTemplateNameException extends IOException {
    private final String malformednessDescription;
    private final String templateName;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MalformedTemplateNameException(java.lang.String r3, java.lang.String r4) {
        /*
            r2 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "Malformed template name, "
            r0.append(r1)
            java.lang.String r1 = freemarker.template.utility.StringUtil.jQuote(r3)
            r0.append(r1)
            java.lang.String r1 = ": "
            r0.append(r1)
            r0.append(r4)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.templateName = r3
            r2.malformednessDescription = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.template.MalformedTemplateNameException.<init>(java.lang.String, java.lang.String):void");
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public String getMalformednessDescription() {
        return this.malformednessDescription;
    }
}
