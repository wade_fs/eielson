package freemarker.template.utility;

import freemarker.template.SimpleNumber;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import freemarker.template.TemplateSequenceModel;
import java.io.Serializable;

public class Constants {
    public static final TemplateCollectionModel EMPTY_COLLECTION = new EmptyCollectionModel();
    public static final TemplateHashModelEx EMPTY_HASH = new EmptyHashModel();
    public static final TemplateModelIterator EMPTY_ITERATOR = new EmptyIteratorModel();
    public static final TemplateSequenceModel EMPTY_SEQUENCE = new EmptySequenceModel();
    public static final TemplateScalarModel EMPTY_STRING = ((TemplateScalarModel) TemplateScalarModel.EMPTY_STRING);
    public static final TemplateBooleanModel FALSE = TemplateBooleanModel.FALSE;
    public static final TemplateNumberModel MINUS_ONE = new SimpleNumber(-1);
    public static final TemplateNumberModel ONE = new SimpleNumber(1);
    public static final TemplateBooleanModel TRUE = TemplateBooleanModel.TRUE;
    public static final TemplateNumberModel ZERO = new SimpleNumber(0);

    private static class EmptyIteratorModel implements TemplateModelIterator, Serializable {
        public boolean hasNext() throws TemplateModelException {
            return false;
        }

        private EmptyIteratorModel() {
        }

        public TemplateModel next() throws TemplateModelException {
            throw new TemplateModelException("The collection has no more elements.");
        }
    }

    private static class EmptyCollectionModel implements TemplateCollectionModel, Serializable {
        private EmptyCollectionModel() {
        }

        public TemplateModelIterator iterator() throws TemplateModelException {
            return Constants.EMPTY_ITERATOR;
        }
    }

    private static class EmptySequenceModel implements TemplateSequenceModel, Serializable {
        public TemplateModel get(int i) throws TemplateModelException {
            return null;
        }

        public int size() throws TemplateModelException {
            return 0;
        }

        private EmptySequenceModel() {
        }
    }

    private static class EmptyHashModel implements TemplateHashModelEx, Serializable {
        public TemplateModel get(String str) throws TemplateModelException {
            return null;
        }

        public boolean isEmpty() throws TemplateModelException {
            return true;
        }

        public int size() throws TemplateModelException {
            return 0;
        }

        private EmptyHashModel() {
        }

        public TemplateCollectionModel keys() throws TemplateModelException {
            return Constants.EMPTY_COLLECTION;
        }

        public TemplateCollectionModel values() throws TemplateModelException {
            return Constants.EMPTY_COLLECTION;
        }
    }
}
