package freemarker.template;

import freemarker.ext.util.WrapperTemplateModel;
import freemarker.template.utility.ObjectWrapperWithAPISupport;
import freemarker.template.utility.RichObjectWrapper;
import java.io.Serializable;
import java.util.AbstractSequentialList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class DefaultListAdapter extends WrappingTemplateModel implements TemplateSequenceModel, AdapterTemplateModel, WrapperTemplateModel, TemplateModelWithAPISupport, Serializable {
    protected final List list;

    public static DefaultListAdapter adapt(List list2, RichObjectWrapper richObjectWrapper) {
        return list2 instanceof AbstractSequentialList ? new DefaultListAdapterWithCollectionSupport(list2, richObjectWrapper) : new DefaultListAdapter(list2, richObjectWrapper);
    }

    private DefaultListAdapter(List list2, RichObjectWrapper richObjectWrapper) {
        super(richObjectWrapper);
        this.list = list2;
    }

    public TemplateModel get(int i) throws TemplateModelException {
        if (i < 0 || i >= this.list.size()) {
            return null;
        }
        return wrap(this.list.get(i));
    }

    public int size() throws TemplateModelException {
        return this.list.size();
    }

    public Object getAdaptedObject(Class cls) {
        return getWrappedObject();
    }

    public Object getWrappedObject() {
        return this.list;
    }

    private static class DefaultListAdapterWithCollectionSupport extends DefaultListAdapter implements TemplateCollectionModel {
        private DefaultListAdapterWithCollectionSupport(List list, RichObjectWrapper richObjectWrapper) {
            super(list, richObjectWrapper);
        }

        public TemplateModelIterator iterator() throws TemplateModelException {
            return new IteratorAdapter(this.list.iterator(), getObjectWrapper());
        }
    }

    private static class IteratorAdapter implements TemplateModelIterator {

        /* renamed from: it */
        private final Iterator f7497it;
        private final ObjectWrapper wrapper;

        private IteratorAdapter(Iterator it, ObjectWrapper objectWrapper) {
            this.f7497it = it;
            this.wrapper = objectWrapper;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Exception):void
         arg types: [java.lang.String, java.util.NoSuchElementException]
         candidates:
          freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Throwable):void
          freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Exception):void */
        public TemplateModel next() throws TemplateModelException {
            try {
                return this.wrapper.wrap(this.f7497it.next());
            } catch (NoSuchElementException e) {
                throw new TemplateModelException("The collection has no more items.", (Exception) e);
            }
        }

        public boolean hasNext() throws TemplateModelException {
            return this.f7497it.hasNext();
        }
    }

    public TemplateModel getAPI() throws TemplateModelException {
        return ((ObjectWrapperWithAPISupport) getObjectWrapper()).wrapAsAPI(this.list);
    }
}
