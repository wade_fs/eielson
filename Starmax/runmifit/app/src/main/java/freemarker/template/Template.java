package freemarker.template;

import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import freemarker.core.Configurable;
import freemarker.core.Environment;
import freemarker.core.FMParser;
import freemarker.core.LibraryLoad;
import freemarker.core.Macro;
import freemarker.core.ParseException;
import freemarker.core.TemplateElement;
import freemarker.core.TextBlock;
import freemarker.core.TokenMgrError;
import freemarker.debug.impl.DebuggerService;
import java.io.BufferedReader;
import java.io.FilterReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.tree.TreePath;

public class Template extends Configurable {
    public static final String DEFAULT_NAMESPACE_PREFIX = "D";
    public static final String NO_NS_PREFIX = "N";
    private int actualNamingConvention;
    private int actualTagSyntax;
    private Object customLookupCondition;
    private String defaultNS;
    private String encoding;
    private List imports;
    /* access modifiers changed from: private */
    public final ArrayList lines;
    private Map macros;
    private final String name;
    private Map namespaceURIToPrefixLookup;
    private transient FMParser parser;
    private Map prefixToNamespaceURILookup;
    private TemplateElement rootElement;
    private final String sourceName;
    private Version templateLanguageVersion;

    private Template(String str, String str2, Configuration configuration, boolean z) {
        super(toNonNull(configuration));
        this.macros = new HashMap();
        this.imports = new Vector();
        this.lines = new ArrayList();
        this.prefixToNamespaceURILookup = new HashMap();
        this.namespaceURIToPrefixLookup = new HashMap();
        this.name = str;
        this.sourceName = str2;
        this.templateLanguageVersion = normalizeTemplateLanguageVersion(toNonNull(configuration).getIncompatibleImprovements());
    }

    private static Configuration toNonNull(Configuration configuration) {
        return configuration != null ? configuration : Configuration.getDefaultConfiguration();
    }

    public Template(String str, Reader reader, Configuration configuration) throws IOException {
        this(str, (String) null, reader, configuration);
    }

    public Template(String str, String str2, Configuration configuration) throws IOException {
        this(str, new StringReader(str2), configuration);
    }

    public Template(String str, Reader reader, Configuration configuration, String str2) throws IOException {
        this(str, null, reader, configuration, str2);
    }

    public Template(String str, String str2, Reader reader, Configuration configuration) throws IOException {
        this(str, str2, reader, configuration, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.Template.<init>(java.lang.String, java.lang.String, freemarker.template.Configuration, boolean):void
     arg types: [java.lang.String, java.lang.String, freemarker.template.Configuration, int]
     candidates:
      freemarker.template.Template.<init>(java.lang.String, java.io.Reader, freemarker.template.Configuration, java.lang.String):void
      freemarker.template.Template.<init>(java.lang.String, java.lang.String, java.io.Reader, freemarker.template.Configuration):void
      freemarker.template.Template.<init>(java.lang.String, java.lang.String, freemarker.template.Configuration, boolean):void */
    public Template(String str, String str2, LineTableBuilder lineTableBuilder, Configuration configuration, String str3) throws IOException {
        this(str, str2, configuration, true);
        this.encoding = str3;
        try {
            LineTableBuilder lineTableBuilder2 = new LineTableBuilder(!(lineTableBuilder instanceof BufferedReader) ? new BufferedReader(lineTableBuilder, 4096) : lineTableBuilder);
            try {
                Configuration configuration2 = getConfiguration();
                this.parser = new FMParser(this, lineTableBuilder2, configuration2.getStrictSyntaxMode(), configuration2.getWhitespaceStripping(), configuration2.getTagSyntax(), configuration2.getNamingConvention(), configuration2.getIncompatibleImprovements().intValue());
                this.rootElement = this.parser.Root();
                this.actualTagSyntax = this.parser._getLastTagSyntax();
                this.actualNamingConvention = this.parser._getLastNamingConvention();
                this.parser = null;
                lineTableBuilder2.close();
                lineTableBuilder2.throwFailure();
                DebuggerService.registerTemplate(this);
                this.namespaceURIToPrefixLookup = Collections.unmodifiableMap(this.namespaceURIToPrefixLookup);
                this.prefixToNamespaceURILookup = Collections.unmodifiableMap(this.prefixToNamespaceURILookup);
            } catch (TokenMgrError e) {
                throw e.toParseException(this);
            } catch (ParseException e2) {
                lineTableBuilder = lineTableBuilder2;
                e = e2;
                try {
                    e.setTemplateName(getSourceName());
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    lineTableBuilder.close();
                    throw th;
                }
            } catch (Throwable th2) {
                lineTableBuilder = lineTableBuilder2;
                th = th2;
                lineTableBuilder.close();
                throw th;
            }
        } catch (ParseException e3) {
            e = e3;
            e.setTemplateName(getSourceName());
            throw e;
        }
    }

    public Template(String str, Reader reader) throws IOException {
        this(str, reader, (Configuration) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.Template.<init>(java.lang.String, java.lang.String, freemarker.template.Configuration, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], freemarker.template.Configuration, int]
     candidates:
      freemarker.template.Template.<init>(java.lang.String, java.io.Reader, freemarker.template.Configuration, java.lang.String):void
      freemarker.template.Template.<init>(java.lang.String, java.lang.String, java.io.Reader, freemarker.template.Configuration):void
      freemarker.template.Template.<init>(java.lang.String, java.lang.String, freemarker.template.Configuration, boolean):void */
    Template(String str, TemplateElement templateElement, Configuration configuration) {
        this(str, (String) null, configuration, true);
        this.rootElement = templateElement;
        DebuggerService.registerTemplate(this);
    }

    public static Template getPlainTextTemplate(String str, String str2, Configuration configuration) {
        return getPlainTextTemplate(str, null, str2, configuration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.Template.<init>(java.lang.String, java.lang.String, freemarker.template.Configuration, boolean):void
     arg types: [java.lang.String, java.lang.String, freemarker.template.Configuration, int]
     candidates:
      freemarker.template.Template.<init>(java.lang.String, java.io.Reader, freemarker.template.Configuration, java.lang.String):void
      freemarker.template.Template.<init>(java.lang.String, java.lang.String, java.io.Reader, freemarker.template.Configuration):void
      freemarker.template.Template.<init>(java.lang.String, java.lang.String, freemarker.template.Configuration, boolean):void */
    public static Template getPlainTextTemplate(String str, String str2, String str3, Configuration configuration) {
        Template template = new Template(str, str2, configuration, true);
        template.rootElement = new TextBlock(str3);
        template.actualTagSyntax = configuration.getTagSyntax();
        DebuggerService.registerTemplate(template);
        return template;
    }

    private static Version normalizeTemplateLanguageVersion(Version version) {
        _TemplateAPI.checkVersionNotNullAndSupported(version);
        int intValue = version.intValue();
        if (intValue < _TemplateAPI.VERSION_INT_2_3_19) {
            return Configuration.VERSION_2_3_0;
        }
        return intValue > _TemplateAPI.VERSION_INT_2_3_21 ? Configuration.VERSION_2_3_21 : version;
    }

    public void process(Object obj, Writer writer) throws TemplateException, IOException {
        createProcessingEnvironment(obj, writer, null).process();
    }

    public void process(Object obj, Writer writer, ObjectWrapper objectWrapper, TemplateNodeModel templateNodeModel) throws TemplateException, IOException {
        Environment createProcessingEnvironment = createProcessingEnvironment(obj, writer, objectWrapper);
        if (templateNodeModel != null) {
            createProcessingEnvironment.setCurrentVisitorNode(templateNodeModel);
        }
        createProcessingEnvironment.process();
    }

    public void process(Object obj, Writer writer, ObjectWrapper objectWrapper) throws TemplateException, IOException {
        createProcessingEnvironment(obj, writer, objectWrapper).process();
    }

    public Environment createProcessingEnvironment(Object obj, Writer writer, ObjectWrapper objectWrapper) throws TemplateException, IOException {
        TemplateHashModel templateHashModel;
        if (obj instanceof TemplateHashModel) {
            templateHashModel = (TemplateHashModel) obj;
        } else {
            if (objectWrapper == null) {
                objectWrapper = getObjectWrapper();
            }
            if (obj == null) {
                templateHashModel = new SimpleHash(objectWrapper);
            } else {
                Object wrap = objectWrapper.wrap(obj);
                if (wrap instanceof TemplateHashModel) {
                    templateHashModel = (TemplateHashModel) wrap;
                } else if (wrap == null) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(objectWrapper.getClass().getName());
                    stringBuffer.append(" converted ");
                    stringBuffer.append(obj.getClass().getName());
                    stringBuffer.append(" to null.");
                    throw new IllegalArgumentException(stringBuffer.toString());
                } else {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append(objectWrapper.getClass().getName());
                    stringBuffer2.append(" didn't convert ");
                    stringBuffer2.append(obj.getClass().getName());
                    stringBuffer2.append(" to a TemplateHashModel. Generally, you want to use a Map<String, Object> or a ");
                    stringBuffer2.append("JavaBean as the root-map (aka. data-model) parameter. The Map key-s or JavaBean ");
                    stringBuffer2.append("property names will be the variable names in the template.");
                    throw new IllegalArgumentException(stringBuffer2.toString());
                }
            }
        }
        return new Environment(this, templateHashModel, writer);
    }

    public Environment createProcessingEnvironment(Object obj, Writer writer) throws TemplateException, IOException {
        return createProcessingEnvironment(obj, writer, null);
    }

    public String toString() {
        StringWriter stringWriter = new StringWriter();
        try {
            dump(stringWriter);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public String getName() {
        return this.name;
    }

    public String getSourceName() {
        String str = this.sourceName;
        return str != null ? str : getName();
    }

    public Configuration getConfiguration() {
        return (Configuration) getParent();
    }

    /* access modifiers changed from: package-private */
    public Version getTemplateLanguageVersion() {
        return this.templateLanguageVersion;
    }

    public void setEncoding(String str) {
        this.encoding = str;
    }

    public String getEncoding() {
        return this.encoding;
    }

    public Object getCustomLookupCondition() {
        return this.customLookupCondition;
    }

    public void setCustomLookupCondition(Object obj) {
        this.customLookupCondition = obj;
    }

    public int getActualTagSyntax() {
        return this.actualTagSyntax;
    }

    public int getActualNamingConvention() {
        return this.actualNamingConvention;
    }

    public void dump(PrintStream printStream) {
        printStream.print(this.rootElement.getCanonicalForm());
    }

    public void dump(Writer writer) throws IOException {
        writer.write(this.rootElement.getCanonicalForm());
    }

    public void addMacro(Macro macro) {
        this.macros.put(macro.getName(), macro);
    }

    public void addImport(LibraryLoad libraryLoad) {
        this.imports.add(libraryLoad);
    }

    public String getSource(int i, int i2, int i3, int i4) {
        if (i2 < 1 || i4 < 1) {
            return null;
        }
        int i5 = i - 1;
        int i6 = i3 - 1;
        int i7 = i4 - 1;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i8 = i2 - 1; i8 <= i7; i8++) {
            if (i8 < this.lines.size()) {
                stringBuffer.append(this.lines.get(i8));
            }
        }
        stringBuffer.delete(0, i5);
        stringBuffer.delete(stringBuffer.length() - ((this.lines.get(i7).toString().length() - i6) - 1), stringBuffer.length());
        return stringBuffer.toString();
    }

    private class LineTableBuilder extends FilterReader {
        boolean closed;
        private IOException failure;
        int lastChar;
        private final StringBuffer lineBuf = new StringBuffer();

        LineTableBuilder(Reader reader) {
            super(reader);
        }

        public void throwFailure() throws IOException {
            IOException iOException = this.failure;
            if (iOException != null) {
                throw iOException;
            }
        }

        public int read() throws IOException {
            try {
                int read = this.in.read();
                handleChar(read);
                return read;
            } catch (IOException e) {
                throw rememberException(e);
            }
        }

        private IOException rememberException(IOException iOException) throws IOException {
            if (!this.closed) {
                this.failure = iOException;
            }
            return iOException;
        }

        public int read(char[] cArr, int i, int i2) throws IOException {
            try {
                int read = this.in.read(cArr, i, i2);
                for (int i3 = i; i3 < i + read; i3++) {
                    handleChar(cArr[i3]);
                }
                return read;
            } catch (IOException e) {
                throw rememberException(e);
            }
        }

        public void close() throws IOException {
            if (this.lineBuf.length() > 0) {
                Template.this.lines.add(this.lineBuf.toString());
                this.lineBuf.setLength(0);
            }
            super.close();
            this.closed = true;
        }

        private void handleChar(int i) {
            if (i == 10 || i == 13) {
                if (this.lastChar == 13 && i == 10) {
                    int size = Template.this.lines.size() - 1;
                    ArrayList access$000 = Template.this.lines;
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append((String) Template.this.lines.get(size));
                    stringBuffer.append(10);
                    access$000.set(size, stringBuffer.toString());
                } else {
                    this.lineBuf.append((char) i);
                    Template.this.lines.add(this.lineBuf.toString());
                    this.lineBuf.setLength(0);
                }
            } else if (i == 9) {
                int length = 8 - (this.lineBuf.length() % 8);
                for (int i2 = 0; i2 < length; i2++) {
                    this.lineBuf.append(' ');
                }
            } else {
                this.lineBuf.append((char) i);
            }
            this.lastChar = i;
        }
    }

    public TemplateElement getRootTreeNode() {
        return this.rootElement;
    }

    public Map getMacros() {
        return this.macros;
    }

    public List getImports() {
        return this.imports;
    }

    public void addPrefixNSMapping(String str, String str2) {
        if (str2.length() == 0) {
            throw new IllegalArgumentException("Cannot map empty string URI");
        } else if (str.length() == 0) {
            throw new IllegalArgumentException("Cannot map empty string prefix");
        } else if (str.equals(NO_NS_PREFIX)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("The prefix: ");
            stringBuffer.append(str);
            stringBuffer.append(" cannot be registered, it's reserved for special internal use.");
            throw new IllegalArgumentException(stringBuffer.toString());
        } else if (this.prefixToNamespaceURILookup.containsKey(str)) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("The prefix: '");
            stringBuffer2.append(str);
            stringBuffer2.append("' was repeated. This is illegal.");
            throw new IllegalArgumentException(stringBuffer2.toString());
        } else if (this.namespaceURIToPrefixLookup.containsKey(str2)) {
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append("The namespace URI: ");
            stringBuffer3.append(str2);
            stringBuffer3.append(" cannot be mapped to 2 different prefixes.");
            throw new IllegalArgumentException(stringBuffer3.toString());
        } else if (str.equals(DEFAULT_NAMESPACE_PREFIX)) {
            this.defaultNS = str2;
        } else {
            this.prefixToNamespaceURILookup.put(str, str2);
            this.namespaceURIToPrefixLookup.put(str2, str);
        }
    }

    public String getDefaultNS() {
        return this.defaultNS;
    }

    public String getNamespaceForPrefix(String str) {
        if (!str.equals("")) {
            return (String) this.prefixToNamespaceURILookup.get(str);
        }
        String str2 = this.defaultNS;
        if (str2 == null) {
            return "";
        }
        return str2;
    }

    public String getPrefixForNamespace(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            if (this.defaultNS == null) {
                return "";
            }
            return NO_NS_PREFIX;
        } else if (str.equals(this.defaultNS)) {
            return "";
        } else {
            return (String) this.namespaceURIToPrefixLookup.get(str);
        }
    }

    public String getPrefixedName(String str, String str2) {
        if (str2 == null || str2.length() == 0) {
            if (this.defaultNS == null) {
                return str;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("N:");
            stringBuffer.append(str);
            return stringBuffer.toString();
        } else if (str2.equals(this.defaultNS)) {
            return str;
        } else {
            String prefixForNamespace = getPrefixForNamespace(str2);
            if (prefixForNamespace == null) {
                return null;
            }
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(prefixForNamespace);
            stringBuffer2.append(Config.TRACE_TODAY_VISIT_SPLIT);
            stringBuffer2.append(str);
            return stringBuffer2.toString();
        }
    }

    public TreePath containingElements(int i, int i2) {
        ArrayList arrayList = new ArrayList();
        TemplateElement templateElement = this.rootElement;
        loop0:
        while (templateElement.contains(i, i2)) {
            arrayList.add(templateElement);
            Enumeration children = templateElement.children();
            while (children.hasMoreElements()) {
                TemplateElement templateElement2 = (TemplateElement) children.nextElement();
                if (templateElement2.contains(i, i2)) {
                    templateElement = templateElement2;
                }
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return new TreePath(arrayList.toArray());
    }

    public static class WrongEncodingException extends ParseException {
        private static final long serialVersionUID = 1;
        private final String constructorSpecifiedEncoding;
        public String specifiedEncoding;

        public WrongEncodingException(String str) {
            this(str, null);
        }

        public WrongEncodingException(String str, String str2) {
            this.specifiedEncoding = str;
            this.constructorSpecifiedEncoding = str2;
        }

        public String getMessage() {
            String str;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Encoding specified inside the template (");
            stringBuffer.append(this.specifiedEncoding);
            stringBuffer.append(") doesn't match the encoding specified for the Template constructor");
            if (this.constructorSpecifiedEncoding != null) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append(" (");
                stringBuffer2.append(this.constructorSpecifiedEncoding);
                stringBuffer2.append(").");
                str = stringBuffer2.toString();
            } else {
                str = FileUtil.HIDDEN_PREFIX;
            }
            stringBuffer.append(str);
            return stringBuffer.toString();
        }

        public String getTemplateSpecifiedEncoding() {
            return this.specifiedEncoding;
        }

        public String getConstructorSpecifiedEncoding() {
            return this.constructorSpecifiedEncoding;
        }
    }
}
