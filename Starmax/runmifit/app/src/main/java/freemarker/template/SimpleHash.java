package freemarker.template;

import freemarker.ext.beans.BeansWrapper;
import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class SimpleHash extends WrappingTemplateModel implements TemplateHashModelEx, Serializable {
    private final Map map;
    private boolean putFailed;
    private Map unwrappedMap;

    public SimpleHash() {
        this((ObjectWrapper) null);
    }

    public SimpleHash(Map map2) {
        this(map2, null);
    }

    public SimpleHash(ObjectWrapper objectWrapper) {
        super(objectWrapper);
        this.map = new HashMap();
    }

    public SimpleHash(Map map2, ObjectWrapper objectWrapper) {
        super(objectWrapper);
        Map map3;
        try {
            map3 = copyMap(map2);
        } catch (ConcurrentModificationException unused) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException unused2) {
            }
            synchronized (map2) {
                map3 = copyMap(map2);
            }
        }
        this.map = map3;
    }

    /* access modifiers changed from: protected */
    public Map copyMap(Map map2) {
        if (map2 instanceof HashMap) {
            return (Map) ((HashMap) map2).clone();
        }
        if (!(map2 instanceof SortedMap)) {
            return new HashMap(map2);
        }
        if (map2 instanceof TreeMap) {
            return (Map) ((TreeMap) map2).clone();
        }
        return new TreeMap((SortedMap) map2);
    }

    public void put(String str, Object obj) {
        this.map.put(str, obj);
        this.unwrappedMap = null;
    }

    public void put(String str, boolean z) {
        put(str, z ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE);
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r3v6, types: [java.lang.Character, java.lang.Object] */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        if (r0 != false) goto L_0x005c;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0067  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public freemarker.template.TemplateModel get(java.lang.String r8) throws freemarker.template.TemplateModelException {
        /*
            r7 = this;
            r0 = 2
            r1 = 0
            r2 = 1
            java.util.Map r3 = r7.map     // Catch:{ ClassCastException -> 0x0096, NullPointerException -> 0x0082 }
            java.lang.Object r3 = r3.get(r8)     // Catch:{ ClassCastException -> 0x0096, NullPointerException -> 0x0082 }
            if (r3 != 0) goto L_0x0069
            int r4 = r8.length()
            r5 = 0
            if (r4 != r2) goto L_0x005a
            java.util.Map r4 = r7.map
            boolean r4 = r4 instanceof java.util.SortedMap
            if (r4 != 0) goto L_0x005a
            java.lang.Character r3 = new java.lang.Character
            char r4 = r8.charAt(r1)
            r3.<init>(r4)
            java.util.Map r4 = r7.map     // Catch:{ ClassCastException -> 0x0046, NullPointerException -> 0x0032 }
            java.lang.Object r4 = r4.get(r3)     // Catch:{ ClassCastException -> 0x0046, NullPointerException -> 0x0032 }
            if (r4 != 0) goto L_0x005c
            java.util.Map r6 = r7.map     // Catch:{ ClassCastException -> 0x0046, NullPointerException -> 0x0032 }
            boolean r0 = r6.containsKey(r3)     // Catch:{ ClassCastException -> 0x0046, NullPointerException -> 0x0032 }
            if (r0 == 0) goto L_0x005b
            goto L_0x005c
        L_0x0032:
            r3 = move-exception
            freemarker.core._TemplateModelException r4 = new freemarker.core._TemplateModelException
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r5 = "NullPointerException while getting Map entry with Character key "
            r0[r1] = r5
            freemarker.core._DelayedJQuote r1 = new freemarker.core._DelayedJQuote
            r1.<init>(r8)
            r0[r2] = r1
            r4.<init>(r3, r0)
            throw r4
        L_0x0046:
            r3 = move-exception
            freemarker.core._TemplateModelException r4 = new freemarker.core._TemplateModelException
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r5 = "ClassCastException while getting Map entry with Character key "
            r0[r1] = r5
            freemarker.core._DelayedJQuote r1 = new freemarker.core._DelayedJQuote
            r1.<init>(r8)
            r0[r2] = r1
            r4.<init>(r3, r0)
            throw r4
        L_0x005a:
            r4 = r3
        L_0x005b:
            r3 = r5
        L_0x005c:
            if (r3 != 0) goto L_0x0067
            java.util.Map r0 = r7.map
            boolean r0 = r0.containsKey(r8)
            if (r0 != 0) goto L_0x006a
            return r5
        L_0x0067:
            r8 = r3
            goto L_0x006a
        L_0x0069:
            r4 = r3
        L_0x006a:
            boolean r0 = r4 instanceof freemarker.template.TemplateModel
            if (r0 == 0) goto L_0x0071
            freemarker.template.TemplateModel r4 = (freemarker.template.TemplateModel) r4
            return r4
        L_0x0071:
            freemarker.template.TemplateModel r0 = r7.wrap(r4)
            boolean r1 = r7.putFailed
            if (r1 != 0) goto L_0x0081
            java.util.Map r1 = r7.map     // Catch:{ Exception -> 0x007f }
            r1.put(r8, r0)     // Catch:{ Exception -> 0x007f }
            goto L_0x0081
        L_0x007f:
            r7.putFailed = r2
        L_0x0081:
            return r0
        L_0x0082:
            r3 = move-exception
            freemarker.core._TemplateModelException r4 = new freemarker.core._TemplateModelException
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r5 = "NullPointerException while getting Map entry with String key "
            r0[r1] = r5
            freemarker.core._DelayedJQuote r1 = new freemarker.core._DelayedJQuote
            r1.<init>(r8)
            r0[r2] = r1
            r4.<init>(r3, r0)
            throw r4
        L_0x0096:
            r3 = move-exception
            freemarker.core._TemplateModelException r4 = new freemarker.core._TemplateModelException
            java.lang.Object[] r0 = new java.lang.Object[r0]
            java.lang.String r5 = "ClassCastException while getting Map entry with String key "
            r0[r1] = r5
            freemarker.core._DelayedJQuote r1 = new freemarker.core._DelayedJQuote
            r1.<init>(r8)
            r0[r2] = r1
            r4.<init>(r3, r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.template.SimpleHash.get(java.lang.String):freemarker.template.TemplateModel");
    }

    public boolean containsKey(String str) {
        return this.map.containsKey(str);
    }

    public void remove(String str) {
        this.map.remove(str);
    }

    public void putAll(Map map2) {
        for (Map.Entry entry : map2.entrySet()) {
            put((String) entry.getKey(), entry.getValue());
        }
    }

    public Map toMap() throws TemplateModelException {
        if (this.unwrappedMap == null) {
            Class<?> cls = this.map.getClass();
            try {
                Map map2 = (Map) cls.newInstance();
                BeansWrapper defaultInstance = BeansWrapper.getDefaultInstance();
                for (Map.Entry entry : this.map.entrySet()) {
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    if (value instanceof TemplateModel) {
                        value = defaultInstance.unwrap((TemplateModel) value);
                    }
                    map2.put(key, value);
                }
                this.unwrappedMap = map2;
            } catch (Exception e) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Error instantiating map of type ");
                stringBuffer.append(cls.getName());
                stringBuffer.append("\n");
                stringBuffer.append(e.getMessage());
                throw new TemplateModelException(stringBuffer.toString());
            }
        }
        return this.unwrappedMap;
    }

    public String toString() {
        return this.map.toString();
    }

    public int size() {
        return this.map.size();
    }

    public boolean isEmpty() {
        Map map2 = this.map;
        return map2 == null || map2.isEmpty();
    }

    public TemplateCollectionModel keys() {
        return new SimpleCollection(this.map.keySet(), getObjectWrapper());
    }

    public TemplateCollectionModel values() {
        return new SimpleCollection(this.map.values(), getObjectWrapper());
    }

    public SimpleHash synchronizedWrapper() {
        return new SynchronizedHash();
    }

    private class SynchronizedHash extends SimpleHash {
        private SynchronizedHash() {
        }

        public boolean isEmpty() {
            boolean isEmpty;
            synchronized (SimpleHash.this) {
                isEmpty = SimpleHash.this.isEmpty();
            }
            return isEmpty;
        }

        public void put(String str, Object obj) {
            synchronized (SimpleHash.this) {
                SimpleHash.this.put(str, obj);
            }
        }

        public TemplateModel get(String str) throws TemplateModelException {
            TemplateModel templateModel;
            synchronized (SimpleHash.this) {
                templateModel = SimpleHash.this.get(str);
            }
            return templateModel;
        }

        public void remove(String str) {
            synchronized (SimpleHash.this) {
                SimpleHash.this.remove(str);
            }
        }

        public int size() {
            int size;
            synchronized (SimpleHash.this) {
                size = SimpleHash.this.size();
            }
            return size;
        }

        public TemplateCollectionModel keys() {
            TemplateCollectionModel keys;
            synchronized (SimpleHash.this) {
                keys = SimpleHash.this.keys();
            }
            return keys;
        }

        public TemplateCollectionModel values() {
            TemplateCollectionModel values;
            synchronized (SimpleHash.this) {
                values = SimpleHash.this.values();
            }
            return values;
        }

        public Map toMap() throws TemplateModelException {
            Map map;
            synchronized (SimpleHash.this) {
                map = SimpleHash.this.toMap();
            }
            return map;
        }
    }
}
