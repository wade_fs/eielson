package freemarker.template;

import freemarker.ext.util.WrapperTemplateModel;
import java.io.Serializable;
import java.util.Iterator;

public class DefaultIteratorAdapter extends WrappingTemplateModel implements TemplateCollectionModel, AdapterTemplateModel, WrapperTemplateModel, Serializable {
    /* access modifiers changed from: private */
    public final Iterator iterator;
    /* access modifiers changed from: private */
    public boolean iteratorOwned;

    public static DefaultIteratorAdapter adapt(Iterator it, ObjectWrapper objectWrapper) {
        return new DefaultIteratorAdapter(it, objectWrapper);
    }

    private DefaultIteratorAdapter(Iterator it, ObjectWrapper objectWrapper) {
        super(objectWrapper);
        this.iterator = it;
    }

    public Object getWrappedObject() {
        return this.iterator;
    }

    public Object getAdaptedObject(Class cls) {
        return getWrappedObject();
    }

    public TemplateModelIterator iterator() throws TemplateModelException {
        return new SimpleTemplateModelIterator();
    }

    private class SimpleTemplateModelIterator implements TemplateModelIterator {
        private boolean iteratorOwnedByMe;

        private SimpleTemplateModelIterator() {
        }

        public TemplateModel next() throws TemplateModelException {
            if (!this.iteratorOwnedByMe) {
                takeIteratorOwnership();
            }
            if (DefaultIteratorAdapter.this.iterator.hasNext()) {
                Object next = DefaultIteratorAdapter.this.iterator.next();
                return next instanceof TemplateModel ? (TemplateModel) next : DefaultIteratorAdapter.this.wrap(next);
            }
            throw new TemplateModelException("The collection has no more items.");
        }

        public boolean hasNext() throws TemplateModelException {
            if (!this.iteratorOwnedByMe) {
                takeIteratorOwnership();
            }
            return DefaultIteratorAdapter.this.iterator.hasNext();
        }

        private void takeIteratorOwnership() throws TemplateModelException {
            if (!DefaultIteratorAdapter.this.iteratorOwned) {
                boolean unused = DefaultIteratorAdapter.this.iteratorOwned = true;
                this.iteratorOwnedByMe = true;
                return;
            }
            throw new TemplateModelException("This collection value wraps a java.util.Iterator, thus it can be listed only once.");
        }
    }
}
