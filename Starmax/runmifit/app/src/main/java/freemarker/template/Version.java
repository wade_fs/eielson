package freemarker.template;

import com.tamic.novate.util.FileUtil;
import freemarker.template.utility.StringUtil;
import java.io.Serializable;
import java.util.Date;

public final class Version implements Serializable {
    private final Date buildDate;
    private String calculatedStringValue;
    private final String extraInfo;
    private final Boolean gaeCompliant;
    private int hashCode;
    private final int intValue;
    private final int major;
    private final int micro;
    private final int minor;
    private final String originalStringValue;

    public static int intValueFor(int i, int i2, int i3) {
        return (i * 1000000) + (i2 * 1000) + i3;
    }

    private boolean isNumber(char c) {
        return c >= '0' && c <= '9';
    }

    public Version(String str) {
        this(str, (Boolean) null, (Date) null);
    }

    public Version(String str, Boolean bool, Date date) {
        String str2;
        char charAt;
        String trim = str.trim();
        this.originalStringValue = trim;
        int[] iArr = new int[3];
        int i = 0;
        int i2 = 0;
        while (true) {
            if (i >= trim.length()) {
                str2 = null;
                break;
            }
            char charAt2 = trim.charAt(i);
            if (isNumber(charAt2)) {
                iArr[i2] = (iArr[i2] * 10) + (charAt2 - '0');
            } else if (i != 0) {
                if (charAt2 != '.') {
                    str2 = trim.substring(i);
                    break;
                }
                int i3 = i + 1;
                char charAt3 = i3 >= trim.length() ? 0 : trim.charAt(i3);
                if (charAt3 == '.') {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("The version number string ");
                    stringBuffer.append(StringUtil.jQuote(trim));
                    stringBuffer.append(" contains multiple dots after a number.");
                    throw new IllegalArgumentException(stringBuffer.toString());
                } else if (i2 == 2 || !isNumber(charAt3)) {
                    str2 = trim.substring(i);
                } else {
                    i2++;
                }
            } else {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("The version number string ");
                stringBuffer2.append(StringUtil.jQuote(trim));
                stringBuffer2.append(" doesn't start with a number.");
                throw new IllegalArgumentException(stringBuffer2.toString());
            }
            i++;
        }
        if (str2 != null && ((charAt = str2.charAt(0)) == '.' || charAt == '-' || charAt == '_')) {
            str2 = str2.substring(1);
            if (str2.length() == 0) {
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("The version number string ");
                stringBuffer3.append(StringUtil.jQuote(trim));
                stringBuffer3.append(" has an extra info section opened with \"");
                stringBuffer3.append(charAt);
                stringBuffer3.append("\", but it's empty.");
                throw new IllegalArgumentException(stringBuffer3.toString());
            }
        }
        this.extraInfo = str2;
        this.major = iArr[0];
        this.minor = iArr[1];
        this.micro = iArr[2];
        this.intValue = calculateIntValue();
        this.gaeCompliant = bool;
        this.buildDate = date;
    }

    public Version(int i, int i2, int i3) {
        this(i, i2, i3, null, null, null);
    }

    public Version(int i, int i2, int i3, String str, Boolean bool, Date date) {
        this.major = i;
        this.minor = i2;
        this.micro = i3;
        this.extraInfo = str;
        this.gaeCompliant = bool;
        this.buildDate = date;
        this.intValue = calculateIntValue();
        this.originalStringValue = null;
    }

    private int calculateIntValue() {
        return intValueFor(this.major, this.minor, this.micro);
    }

    private String getStringValue() {
        String str;
        String str2 = this.originalStringValue;
        if (str2 != null) {
            return str2;
        }
        synchronized (this) {
            if (this.calculatedStringValue == null) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(this.major);
                stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                stringBuffer.append(this.minor);
                stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                stringBuffer.append(this.micro);
                this.calculatedStringValue = stringBuffer.toString();
                if (this.extraInfo != null) {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append(this.calculatedStringValue);
                    stringBuffer2.append("-");
                    stringBuffer2.append(this.extraInfo);
                    this.calculatedStringValue = stringBuffer2.toString();
                }
            }
            str = this.calculatedStringValue;
        }
        return str;
    }

    public String toString() {
        return getStringValue();
    }

    public int getMajor() {
        return this.major;
    }

    public int getMinor() {
        return this.minor;
    }

    public int getMicro() {
        return this.micro;
    }

    public String getExtraInfo() {
        return this.extraInfo;
    }

    public Boolean isGAECompliant() {
        return this.gaeCompliant;
    }

    public Date getBuildDate() {
        return this.buildDate;
    }

    public int intValue() {
        return this.intValue;
    }

    public int hashCode() {
        int i;
        int i2 = this.hashCode;
        if (i2 != 0) {
            return i2;
        }
        synchronized (this) {
            if (this.hashCode == 0) {
                int i3 = 0;
                int hashCode2 = ((((this.buildDate == null ? 0 : this.buildDate.hashCode()) + 31) * 31) + (this.extraInfo == null ? 0 : this.extraInfo.hashCode())) * 31;
                if (this.gaeCompliant != null) {
                    i3 = this.gaeCompliant.hashCode();
                }
                int i4 = ((hashCode2 + i3) * 31) + this.intValue;
                if (i4 == 0) {
                    i4 = -1;
                }
                this.hashCode = i4;
            }
            i = this.hashCode;
        }
        return i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Version version = (Version) obj;
        if (this.intValue != version.intValue || version.hashCode() != hashCode()) {
            return false;
        }
        Date date = this.buildDate;
        if (date == null) {
            if (version.buildDate != null) {
                return false;
            }
        } else if (!date.equals(version.buildDate)) {
            return false;
        }
        String str = this.extraInfo;
        if (str == null) {
            if (version.extraInfo != null) {
                return false;
            }
        } else if (!str.equals(version.extraInfo)) {
            return false;
        }
        Boolean bool = this.gaeCompliant;
        if (bool == null) {
            if (version.gaeCompliant != null) {
                return false;
            }
        } else if (!bool.equals(version.gaeCompliant)) {
            return false;
        }
        return true;
    }
}
