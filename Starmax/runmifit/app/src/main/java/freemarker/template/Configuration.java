package freemarker.template;

import com.baidu.mobstat.Config;
import com.google.android.gms.dynamite.ProviderConstants;
import com.runmifit.android.util.ChangeCharset;
import com.tamic.novate.util.FileUtil;
import freemarker.cache.CacheStorage;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.SoftCacheStorage;
import freemarker.cache.TemplateCache;
import freemarker.cache.TemplateLoader;
import freemarker.cache.TemplateLookupStrategy;
import freemarker.cache.TemplateNameFormat;
import freemarker.core.BugException;
import freemarker.core.Configurable;
import freemarker.core.Environment;
import freemarker.core._ConcurrentMapFactory;
import freemarker.core._CoreAPI;
import freemarker.core._SortedArraySet;
import freemarker.core._UnmodifiableCompositeSet;
import freemarker.log.Logger;
import freemarker.template.utility.CaptureOutput;
import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.HtmlEscape;
import freemarker.template.utility.NormalizeNewlines;
import freemarker.template.utility.NullArgumentException;
import freemarker.template.utility.SecurityUtilities;
import freemarker.template.utility.StandardCompress;
import freemarker.template.utility.StringUtil;
import freemarker.template.utility.XmlEscape;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class Configuration extends Configurable implements Cloneable {
    public static final int ANGLE_BRACKET_TAG_SYNTAX = 1;
    public static final int AUTO_DETECT_NAMING_CONVENTION = 10;
    public static final int AUTO_DETECT_TAG_SYNTAX = 0;
    public static final String AUTO_IMPORT_KEY = "auto_import";
    public static final String AUTO_IMPORT_KEY_CAMEL_CASE = "autoImport";
    public static final String AUTO_IMPORT_KEY_SNAKE_CASE = "auto_import";
    public static final String AUTO_INCLUDE_KEY = "auto_include";
    public static final String AUTO_INCLUDE_KEY_CAMEL_CASE = "autoInclude";
    public static final String AUTO_INCLUDE_KEY_SNAKE_CASE = "auto_include";
    private static final Logger CACHE_LOG = Logger.getLogger("freemarker.cache");
    public static final String CACHE_STORAGE_KEY = "cache_storage";
    public static final String CACHE_STORAGE_KEY_CAMEL_CASE = "cacheStorage";
    public static final String CACHE_STORAGE_KEY_SNAKE_CASE = "cache_storage";
    public static final int CAMEL_CASE_NAMING_CONVENTION = 12;
    private static final String DEFAULT = "default";
    public static final String DEFAULT_ENCODING_KEY = "default_encoding";
    public static final String DEFAULT_ENCODING_KEY_CAMEL_CASE = "defaultEncoding";
    public static final String DEFAULT_ENCODING_KEY_SNAKE_CASE = "default_encoding";
    public static final String DEFAULT_INCOMPATIBLE_ENHANCEMENTS = DEFAULT_INCOMPATIBLE_IMPROVEMENTS.toString();
    public static final Version DEFAULT_INCOMPATIBLE_IMPROVEMENTS = VERSION_2_3_0;
    private static final boolean FM_24_DETECTED;
    private static final String FM_24_DETECTION_CLASS_NAME = "freemarker.core._2_4_OrLaterMarker";
    public static final String INCOMPATIBLE_ENHANCEMENTS = "incompatible_enhancements";
    public static final String INCOMPATIBLE_IMPROVEMENTS = "incompatible_improvements";
    public static final String INCOMPATIBLE_IMPROVEMENTS_KEY = "incompatible_improvements";
    public static final String INCOMPATIBLE_IMPROVEMENTS_KEY_CAMEL_CASE = "incompatibleImprovements";
    public static final String INCOMPATIBLE_IMPROVEMENTS_KEY_SNAKE_CASE = "incompatible_improvements";
    public static final int LEGACY_NAMING_CONVENTION = 11;
    public static final String LOCALIZED_LOOKUP_KEY = "localized_lookup";
    public static final String LOCALIZED_LOOKUP_KEY_CAMEL_CASE = "localizedLookup";
    public static final String LOCALIZED_LOOKUP_KEY_SNAKE_CASE = "localized_lookup";
    public static final String NAMING_CONVENTION_KEY = "naming_convention";
    public static final String NAMING_CONVENTION_KEY_CAMEL_CASE = "namingConvention";
    public static final String NAMING_CONVENTION_KEY_SNAKE_CASE = "naming_convention";
    public static final int PARSED_DEFAULT_INCOMPATIBLE_ENHANCEMENTS = DEFAULT_INCOMPATIBLE_IMPROVEMENTS.intValue();
    private static final String[] SETTING_NAMES_CAMEL_CASE = {AUTO_IMPORT_KEY_CAMEL_CASE, AUTO_INCLUDE_KEY_CAMEL_CASE, CACHE_STORAGE_KEY_CAMEL_CASE, DEFAULT_ENCODING_KEY_CAMEL_CASE, INCOMPATIBLE_IMPROVEMENTS_KEY_CAMEL_CASE, LOCALIZED_LOOKUP_KEY_CAMEL_CASE, NAMING_CONVENTION_KEY_CAMEL_CASE, STRICT_SYNTAX_KEY_CAMEL_CASE, TAG_SYNTAX_KEY_CAMEL_CASE, TEMPLATE_LOADER_KEY_CAMEL_CASE, TEMPLATE_LOOKUP_STRATEGY_KEY_CAMEL_CASE, TEMPLATE_NAME_FORMAT_KEY_CAMEL_CASE, TEMPLATE_UPDATE_DELAY_KEY_CAMEL_CASE, WHITESPACE_STRIPPING_KEY_CAMEL_CASE};
    private static final String[] SETTING_NAMES_SNAKE_CASE = {"auto_import", "auto_include", "cache_storage", "default_encoding", "incompatible_improvements", "localized_lookup", "naming_convention", "strict_syntax", "tag_syntax", "template_loader", "template_lookup_strategy", "template_name_format", "template_update_delay", "whitespace_stripping"};
    public static final int SQUARE_BRACKET_TAG_SYNTAX = 2;
    public static final String STRICT_SYNTAX_KEY = "strict_syntax";
    public static final String STRICT_SYNTAX_KEY_CAMEL_CASE = "strictSyntax";
    public static final String STRICT_SYNTAX_KEY_SNAKE_CASE = "strict_syntax";
    public static final String TAG_SYNTAX_KEY = "tag_syntax";
    public static final String TAG_SYNTAX_KEY_CAMEL_CASE = "tagSyntax";
    public static final String TAG_SYNTAX_KEY_SNAKE_CASE = "tag_syntax";
    public static final String TEMPLATE_LOADER_KEY = "template_loader";
    public static final String TEMPLATE_LOADER_KEY_CAMEL_CASE = "templateLoader";
    public static final String TEMPLATE_LOADER_KEY_SNAKE_CASE = "template_loader";
    public static final String TEMPLATE_LOOKUP_STRATEGY_KEY = "template_lookup_strategy";
    public static final String TEMPLATE_LOOKUP_STRATEGY_KEY_CAMEL_CASE = "templateLookupStrategy";
    public static final String TEMPLATE_LOOKUP_STRATEGY_KEY_SNAKE_CASE = "template_lookup_strategy";
    public static final String TEMPLATE_NAME_FORMAT_KEY = "template_name_format";
    public static final String TEMPLATE_NAME_FORMAT_KEY_CAMEL_CASE = "templateNameFormat";
    public static final String TEMPLATE_NAME_FORMAT_KEY_SNAKE_CASE = "template_name_format";
    public static final String TEMPLATE_UPDATE_DELAY_KEY = "template_update_delay";
    public static final String TEMPLATE_UPDATE_DELAY_KEY_CAMEL_CASE = "templateUpdateDelay";
    public static final String TEMPLATE_UPDATE_DELAY_KEY_SNAKE_CASE = "template_update_delay";
    private static final Version VERSION;
    public static final Version VERSION_2_3_0 = new Version(2, 3, 0);
    public static final Version VERSION_2_3_19 = new Version(2, 3, 19);
    public static final Version VERSION_2_3_20 = new Version(2, 3, 20);
    public static final Version VERSION_2_3_21 = new Version(2, 3, 21);
    public static final Version VERSION_2_3_22 = new Version(2, 3, 22);
    public static final Version VERSION_2_3_23 = new Version(2, 3, 23);
    private static final String VERSION_PROPERTIES_PATH = "freemarker/version.properties";
    public static final String WHITESPACE_STRIPPING_KEY = "whitespace_stripping";
    public static final String WHITESPACE_STRIPPING_KEY_CAMEL_CASE = "whitespaceStripping";
    public static final String WHITESPACE_STRIPPING_KEY_SNAKE_CASE = "whitespace_stripping";
    static /* synthetic */ Class class$freemarker$cache$CacheStorage;
    static /* synthetic */ Class class$freemarker$cache$TemplateLoader;
    static /* synthetic */ Class class$freemarker$cache$TemplateLookupStrategy;
    static /* synthetic */ Class class$freemarker$template$Configuration;
    static /* synthetic */ Class class$java$lang$String;
    private static Configuration defaultConfig;
    private static final Object defaultConfigLock = new Object();
    private Map autoImportNsToTmpMap;
    private ArrayList autoImports;
    private ArrayList autoIncludes;
    private TemplateCache cache;
    private boolean cacheStorageExplicitlySet;
    private String defaultEncoding;
    private Version incompatibleImprovements;
    private Map localeToCharsetMap;
    private volatile boolean localizedLookup;
    private boolean logTemplateExceptionsExplicitlySet;
    private int namingConvention;
    private boolean objectWrapperExplicitlySet;
    private HashMap rewrappableSharedVariables;
    private HashMap sharedVariables;
    private boolean strictSyntax;
    private int tagSyntax;
    private boolean templateExceptionHandlerExplicitlySet;
    private boolean templateLoaderExplicitlySet;
    private boolean templateLookupStrategyExplicitlySet;
    private boolean templateNameFormatExplicitlySet;
    private boolean whitespaceStripping;

    private static class LegacyDefaultFileTemplateLoader extends FileTemplateLoader {
    }

    static final boolean getDefaultLogTemplateExceptions(Version version) {
        return true;
    }

    static {
        Class cls;
        InputStream resourceAsStream;
        Date date;
        boolean z = false;
        try {
            Properties properties = new Properties();
            if (class$freemarker$template$Configuration == null) {
                cls = class$("freemarker.template.Configuration");
                class$freemarker$template$Configuration = cls;
            } else {
                cls = class$freemarker$template$Configuration;
            }
            resourceAsStream = cls.getClassLoader().getResourceAsStream(VERSION_PROPERTIES_PATH);
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
                resourceAsStream.close();
                String requiredVersionProperty = getRequiredVersionProperty(properties, ProviderConstants.API_COLNAME_FEATURE_VERSION);
                String requiredVersionProperty2 = getRequiredVersionProperty(properties, "buildTimestamp");
                if (requiredVersionProperty2.endsWith("Z")) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(requiredVersionProperty2.substring(0, requiredVersionProperty2.length() - 1));
                    stringBuffer.append("+0000");
                    requiredVersionProperty2 = stringBuffer.toString();
                }
                try {
                    date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).parse(requiredVersionProperty2);
                } catch (ParseException unused) {
                    date = null;
                }
                VERSION = new Version(requiredVersionProperty, Boolean.valueOf(getRequiredVersionProperty(properties, "isGAECompliant")), date);
                try {
                    Class.forName(FM_24_DETECTION_CLASS_NAME);
                } catch (ClassNotFoundException unused2) {
                } catch (LinkageError unused3) {
                }
                z = true;
                FM_24_DETECTED = z;
                return;
            }
            throw new RuntimeException("Version file is missing.");
        } catch (IOException e) {
            throw new RuntimeException("Failed to load and parse freemarker/version.properties", e);
        } catch (Throwable th) {
            resourceAsStream.close();
            throw th;
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public Configuration() {
        this(DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
    }

    public Configuration(Version version) {
        super(version);
        this.strictSyntax = true;
        this.localizedLookup = true;
        this.whitespaceStripping = true;
        this.tagSyntax = 1;
        this.namingConvention = 10;
        this.sharedVariables = new HashMap();
        this.rewrappableSharedVariables = null;
        this.defaultEncoding = SecurityUtilities.getSystemProperty("file.encoding", "utf-8");
        this.localeToCharsetMap = _ConcurrentMapFactory.newThreadSafeMap();
        this.autoImports = new ArrayList();
        this.autoIncludes = new ArrayList();
        this.autoImportNsToTmpMap = new HashMap();
        checkFreeMarkerVersionClash();
        NullArgumentException.check(INCOMPATIBLE_IMPROVEMENTS_KEY_CAMEL_CASE, version);
        this.incompatibleImprovements = version;
        createTemplateCache();
        loadBuiltInSharedVariables();
    }

    private static void checkFreeMarkerVersionClash() {
        if (FM_24_DETECTED) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Clashing FreeMarker versions (");
            stringBuffer.append(VERSION);
            stringBuffer.append(" and some post-2.3.x) detected: ");
            stringBuffer.append("found post-2.3.x class ");
            stringBuffer.append(FM_24_DETECTION_CLASS_NAME);
            stringBuffer.append(". You probably have two different ");
            stringBuffer.append("freemarker.jar-s in the classpath.");
            throw new RuntimeException(stringBuffer.toString());
        }
    }

    private void createTemplateCache() {
        this.cache = new TemplateCache(getDefaultTemplateLoader(), getDefaultCacheStorage(), getDefaultTemplateLookupStrategy(), getDefaultTemplateNameFormat(), this);
        this.cache.clear();
        this.cache.setDelay(5000);
    }

    private void recreateTemplateCacheWith(TemplateLoader templateLoader, CacheStorage cacheStorage, TemplateLookupStrategy templateLookupStrategy, TemplateNameFormat templateNameFormat) {
        TemplateCache templateCache = this.cache;
        this.cache = new TemplateCache(templateLoader, cacheStorage, templateLookupStrategy, templateNameFormat, this);
        this.cache.clear();
        this.cache.setDelay(templateCache.getDelay());
        this.cache.setLocalizedLookup(this.localizedLookup);
    }

    private TemplateLoader getDefaultTemplateLoader() {
        return createDefaultTemplateLoader(getIncompatibleImprovements(), getTemplateLoader());
    }

    static TemplateLoader createDefaultTemplateLoader(Version version) {
        return createDefaultTemplateLoader(version, null);
    }

    private static TemplateLoader createDefaultTemplateLoader(Version version, TemplateLoader templateLoader) {
        if (version.intValue() < _TemplateAPI.VERSION_INT_2_3_21) {
            if (templateLoader instanceof LegacyDefaultFileTemplateLoader) {
                return templateLoader;
            }
            try {
                return new LegacyDefaultFileTemplateLoader();
            } catch (Exception e) {
                CACHE_LOG.warn("Couldn't create legacy default TemplateLoader which accesses the current directory. (Use new Configuration(Configuration.VERSION_2_3_21) or higher to avoid this.)", e);
            }
        }
        return null;
    }

    private TemplateLookupStrategy getDefaultTemplateLookupStrategy() {
        return getDefaultTemplateLookupStrategy(getIncompatibleImprovements());
    }

    static TemplateLookupStrategy getDefaultTemplateLookupStrategy(Version version) {
        return TemplateLookupStrategy.DEFAULT_2_3_0;
    }

    private TemplateNameFormat getDefaultTemplateNameFormat() {
        return getDefaultTemplateNameFormat(getIncompatibleImprovements());
    }

    static TemplateNameFormat getDefaultTemplateNameFormat(Version version) {
        return TemplateNameFormat.DEFAULT_2_3_0;
    }

    private CacheStorage getDefaultCacheStorage() {
        return createDefaultCacheStorage(getIncompatibleImprovements(), getCacheStorage());
    }

    static CacheStorage createDefaultCacheStorage(Version version, CacheStorage cacheStorage) {
        if (cacheStorage instanceof DefaultSoftCacheStorage) {
            return cacheStorage;
        }
        return new DefaultSoftCacheStorage();
    }

    static CacheStorage createDefaultCacheStorage(Version version) {
        return createDefaultCacheStorage(version, null);
    }

    private static class DefaultSoftCacheStorage extends SoftCacheStorage {
        private DefaultSoftCacheStorage() {
        }
    }

    private TemplateExceptionHandler getDefaultTemplateExceptionHandler() {
        return getDefaultTemplateExceptionHandler(getIncompatibleImprovements());
    }

    private boolean getDefaultLogTemplateExceptions() {
        return getDefaultLogTemplateExceptions(getIncompatibleImprovements());
    }

    private ObjectWrapper getDefaultObjectWrapper() {
        return getDefaultObjectWrapper(getIncompatibleImprovements());
    }

    static final TemplateExceptionHandler getDefaultTemplateExceptionHandler(Version version) {
        return TemplateExceptionHandler.DEBUG_HANDLER;
    }

    public Object clone() {
        try {
            Configuration configuration = (Configuration) super.clone();
            configuration.sharedVariables = new HashMap(this.sharedVariables);
            configuration.localeToCharsetMap = new HashMap(this.localeToCharsetMap);
            configuration.autoImportNsToTmpMap = new HashMap(this.autoImportNsToTmpMap);
            configuration.autoImports = (ArrayList) this.autoImports.clone();
            configuration.autoIncludes = (ArrayList) this.autoIncludes.clone();
            configuration.recreateTemplateCacheWith(this.cache.getTemplateLoader(), this.cache.getCacheStorage(), this.cache.getTemplateLookupStrategy(), this.cache.getTemplateNameFormat());
            return configuration;
        } catch (CloneNotSupportedException e) {
            throw new BugException(e.getMessage());
        }
    }

    private void loadBuiltInSharedVariables() {
        this.sharedVariables.put("capture_output", new CaptureOutput());
        this.sharedVariables.put("compress", StandardCompress.INSTANCE);
        this.sharedVariables.put("html_escape", new HtmlEscape());
        this.sharedVariables.put("normalize_newlines", new NormalizeNewlines());
        this.sharedVariables.put("xml_escape", new XmlEscape());
    }

    public void loadBuiltInEncodingMap() {
        this.localeToCharsetMap.clear();
        this.localeToCharsetMap.put("ar", "ISO-8859-6");
        this.localeToCharsetMap.put("be", "ISO-8859-5");
        this.localeToCharsetMap.put("bg", "ISO-8859-5");
        this.localeToCharsetMap.put("ca", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("cs", "ISO-8859-2");
        this.localeToCharsetMap.put("da", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("de", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("el", "ISO-8859-7");
        this.localeToCharsetMap.put("en", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("es", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("et", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("fi", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("fr", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("hr", "ISO-8859-2");
        this.localeToCharsetMap.put("hu", "ISO-8859-2");
        this.localeToCharsetMap.put("is", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("it", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("iw", "ISO-8859-8");
        this.localeToCharsetMap.put("ja", "Shift_JIS");
        this.localeToCharsetMap.put("ko", "EUC-KR");
        this.localeToCharsetMap.put("lt", "ISO-8859-2");
        this.localeToCharsetMap.put("lv", "ISO-8859-2");
        this.localeToCharsetMap.put("mk", "ISO-8859-5");
        this.localeToCharsetMap.put("nl", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("no", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put(Config.PROCESS_LABEL, "ISO-8859-2");
        this.localeToCharsetMap.put(Config.PLATFORM_TYPE, ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("ro", "ISO-8859-2");
        this.localeToCharsetMap.put("ru", "ISO-8859-5");
        this.localeToCharsetMap.put("sh", "ISO-8859-5");
        this.localeToCharsetMap.put("sk", "ISO-8859-2");
        this.localeToCharsetMap.put("sl", "ISO-8859-2");
        this.localeToCharsetMap.put(Config.SEQUENCE_INDEX, "ISO-8859-2");
        this.localeToCharsetMap.put("sr", "ISO-8859-5");
        this.localeToCharsetMap.put("sv", ChangeCharset.ISO_8859_1);
        this.localeToCharsetMap.put("tr", "ISO-8859-9");
        this.localeToCharsetMap.put("uk", "ISO-8859-5");
        this.localeToCharsetMap.put("zh", "GB2312");
        this.localeToCharsetMap.put("zh_TW", "Big5");
    }

    public void clearEncodingMap() {
        this.localeToCharsetMap.clear();
    }

    public static Configuration getDefaultConfiguration() {
        Configuration configuration;
        synchronized (defaultConfigLock) {
            if (defaultConfig == null) {
                defaultConfig = new Configuration();
            }
            configuration = defaultConfig;
        }
        return configuration;
    }

    public static void setDefaultConfiguration(Configuration configuration) {
        synchronized (defaultConfigLock) {
            defaultConfig = configuration;
        }
    }

    public void setTemplateLoader(TemplateLoader templateLoader) {
        synchronized (this) {
            if (this.cache.getTemplateLoader() != templateLoader) {
                recreateTemplateCacheWith(templateLoader, this.cache.getCacheStorage(), this.cache.getTemplateLookupStrategy(), this.cache.getTemplateNameFormat());
            }
            this.templateLoaderExplicitlySet = true;
        }
    }

    public void unsetTemplateLoader() {
        if (this.templateLoaderExplicitlySet) {
            setTemplateLoader(getDefaultTemplateLoader());
            this.templateLoaderExplicitlySet = false;
        }
    }

    public boolean isTemplateLoaderExplicitlySet() {
        return this.templateLoaderExplicitlySet;
    }

    public TemplateLoader getTemplateLoader() {
        TemplateCache templateCache = this.cache;
        if (templateCache == null) {
            return null;
        }
        return templateCache.getTemplateLoader();
    }

    public void setTemplateLookupStrategy(TemplateLookupStrategy templateLookupStrategy) {
        if (this.cache.getTemplateLookupStrategy() != templateLookupStrategy) {
            recreateTemplateCacheWith(this.cache.getTemplateLoader(), this.cache.getCacheStorage(), templateLookupStrategy, this.cache.getTemplateNameFormat());
        }
        this.templateLookupStrategyExplicitlySet = true;
    }

    public void unsetTemplateLookupStrategy() {
        if (this.templateLookupStrategyExplicitlySet) {
            setTemplateLookupStrategy(getDefaultTemplateLookupStrategy());
            this.templateLookupStrategyExplicitlySet = false;
        }
    }

    public boolean isTemplateLookupStrategyExplicitlySet() {
        return this.templateLookupStrategyExplicitlySet;
    }

    public TemplateLookupStrategy getTemplateLookupStrategy() {
        TemplateCache templateCache = this.cache;
        if (templateCache == null) {
            return null;
        }
        return templateCache.getTemplateLookupStrategy();
    }

    public void setTemplateNameFormat(TemplateNameFormat templateNameFormat) {
        if (this.cache.getTemplateNameFormat() != templateNameFormat) {
            recreateTemplateCacheWith(this.cache.getTemplateLoader(), this.cache.getCacheStorage(), this.cache.getTemplateLookupStrategy(), templateNameFormat);
        }
        this.templateNameFormatExplicitlySet = true;
    }

    public void unsetTemplateNameFormat() {
        if (this.templateNameFormatExplicitlySet) {
            setTemplateNameFormat(getDefaultTemplateNameFormat());
            this.templateNameFormatExplicitlySet = false;
        }
    }

    public boolean isTemplateNameFormatExplicitlySet() {
        return this.templateNameFormatExplicitlySet;
    }

    public TemplateNameFormat getTemplateNameFormat() {
        TemplateCache templateCache = this.cache;
        if (templateCache == null) {
            return null;
        }
        return templateCache.getTemplateNameFormat();
    }

    public void setCacheStorage(CacheStorage cacheStorage) {
        synchronized (this) {
            if (getCacheStorage() != cacheStorage) {
                recreateTemplateCacheWith(this.cache.getTemplateLoader(), cacheStorage, this.cache.getTemplateLookupStrategy(), this.cache.getTemplateNameFormat());
            }
            this.cacheStorageExplicitlySet = true;
        }
    }

    public void unsetCacheStorage() {
        if (this.cacheStorageExplicitlySet) {
            setCacheStorage(getDefaultCacheStorage());
            this.cacheStorageExplicitlySet = false;
        }
    }

    public boolean isCacheStorageExplicitlySet() {
        return this.cacheStorageExplicitlySet;
    }

    public CacheStorage getCacheStorage() {
        synchronized (this) {
            if (this.cache == null) {
                return null;
            }
            CacheStorage cacheStorage = this.cache.getCacheStorage();
            return cacheStorage;
        }
    }

    public void setDirectoryForTemplateLoading(File file) throws IOException {
        TemplateLoader templateLoader = getTemplateLoader();
        if (!(templateLoader instanceof FileTemplateLoader) || !((FileTemplateLoader) templateLoader).baseDir.getCanonicalPath().equals(file.getCanonicalPath())) {
            setTemplateLoader(new FileTemplateLoader(file));
        }
    }

    public void setServletContextForTemplateLoading(Object obj, String str) {
        Class[] clsArr;
        Object[] objArr;
        Class cls;
        try {
            Class forName = ClassUtil.forName("freemarker.cache.WebappTemplateLoader");
            Class forName2 = ClassUtil.forName("javax.servlet.ServletContext");
            if (str == null) {
                clsArr = new Class[]{forName2};
                objArr = new Object[]{obj};
            } else {
                Class[] clsArr2 = new Class[2];
                clsArr2[0] = forName2;
                if (class$java$lang$String == null) {
                    cls = class$("java.lang.String");
                    class$java$lang$String = cls;
                } else {
                    cls = class$java$lang$String;
                }
                clsArr2[1] = cls;
                objArr = new Object[]{obj, str};
                clsArr = clsArr2;
            }
            setTemplateLoader((TemplateLoader) forName.getConstructor(clsArr).newInstance(objArr));
        } catch (Exception e) {
            throw new BugException(e);
        }
    }

    public void setClassForTemplateLoading(Class cls, String str) {
        setTemplateLoader(new ClassTemplateLoader(cls, str));
    }

    public void setClassLoaderForTemplateLoading(ClassLoader classLoader, String str) {
        setTemplateLoader(new ClassTemplateLoader(classLoader, str));
    }

    public void setTemplateUpdateDelay(int i) {
        this.cache.setDelay(((long) i) * 1000);
    }

    public void setTemplateUpdateDelayMilliseconds(long j) {
        this.cache.setDelay(j);
    }

    public long getTemplateUpdateDelayMilliseconds() {
        return this.cache.getDelay();
    }

    public void setStrictSyntaxMode(boolean z) {
        this.strictSyntax = z;
    }

    public void setObjectWrapper(ObjectWrapper objectWrapper) {
        ObjectWrapper objectWrapper2 = getObjectWrapper();
        super.setObjectWrapper(objectWrapper);
        this.objectWrapperExplicitlySet = true;
        if (objectWrapper != objectWrapper2) {
            try {
                setSharedVariablesFromRewrappableSharedVariables();
            } catch (TemplateModelException e) {
                throw new RuntimeException("Failed to re-wrap earliearly set shared variables with the newly set object wrapper", e);
            }
        }
    }

    public void unsetObjectWrapper() {
        if (this.objectWrapperExplicitlySet) {
            setObjectWrapper(getDefaultObjectWrapper());
            this.objectWrapperExplicitlySet = false;
        }
    }

    public boolean isObjectWrapperExplicitlySet() {
        return this.objectWrapperExplicitlySet;
    }

    public void setTemplateExceptionHandler(TemplateExceptionHandler templateExceptionHandler) {
        super.setTemplateExceptionHandler(templateExceptionHandler);
        this.templateExceptionHandlerExplicitlySet = true;
    }

    public void unsetTemplateExceptionHandler() {
        if (this.templateExceptionHandlerExplicitlySet) {
            setTemplateExceptionHandler(getDefaultTemplateExceptionHandler());
            this.templateExceptionHandlerExplicitlySet = false;
        }
    }

    public boolean isTemplateExceptionHandlerExplicitlySet() {
        return this.templateExceptionHandlerExplicitlySet;
    }

    public void setLogTemplateExceptions(boolean z) {
        super.setLogTemplateExceptions(z);
        this.logTemplateExceptionsExplicitlySet = true;
    }

    public void unsetLogTemplateExceptions() {
        if (this.logTemplateExceptionsExplicitlySet) {
            setLogTemplateExceptions(getDefaultLogTemplateExceptions());
            this.logTemplateExceptionsExplicitlySet = false;
        }
    }

    public boolean isLogTemplateExceptionsExplicitlySet() {
        return this.logTemplateExceptionsExplicitlySet;
    }

    public boolean getStrictSyntaxMode() {
        return this.strictSyntax;
    }

    public void setIncompatibleImprovements(Version version) {
        _TemplateAPI.checkVersionNotNullAndSupported(version);
        if (!this.incompatibleImprovements.equals(version)) {
            this.incompatibleImprovements = version;
            if (!this.templateLoaderExplicitlySet) {
                this.templateLoaderExplicitlySet = true;
                unsetTemplateLoader();
            }
            if (!this.templateLookupStrategyExplicitlySet) {
                this.templateLookupStrategyExplicitlySet = true;
                unsetTemplateLookupStrategy();
            }
            if (!this.templateNameFormatExplicitlySet) {
                this.templateNameFormatExplicitlySet = true;
                unsetTemplateNameFormat();
            }
            if (!this.cacheStorageExplicitlySet) {
                this.cacheStorageExplicitlySet = true;
                unsetCacheStorage();
            }
            if (!this.templateExceptionHandlerExplicitlySet) {
                this.templateExceptionHandlerExplicitlySet = true;
                unsetTemplateExceptionHandler();
            }
            if (!this.logTemplateExceptionsExplicitlySet) {
                this.logTemplateExceptionsExplicitlySet = true;
                unsetLogTemplateExceptions();
            }
            if (!this.objectWrapperExplicitlySet) {
                this.objectWrapperExplicitlySet = true;
                unsetObjectWrapper();
            }
        }
    }

    public Version getIncompatibleImprovements() {
        return this.incompatibleImprovements;
    }

    public void setIncompatibleEnhancements(String str) {
        setIncompatibleImprovements(new Version(str));
    }

    public String getIncompatibleEnhancements() {
        return this.incompatibleImprovements.toString();
    }

    public int getParsedIncompatibleEnhancements() {
        return getIncompatibleImprovements().intValue();
    }

    public void setWhitespaceStripping(boolean z) {
        this.whitespaceStripping = z;
    }

    public boolean getWhitespaceStripping() {
        return this.whitespaceStripping;
    }

    public void setTagSyntax(int i) {
        if (i == 0 || i == 2 || i == 1) {
            this.tagSyntax = i;
            return;
        }
        throw new IllegalArgumentException("\"tag_syntax\" can only be set to one of these: Configuration.AUTO_DETECT_TAG_SYNTAX, Configuration.ANGLE_BRACKET_SYNTAX, or Configuration.SQAUARE_BRACKET_SYNTAX");
    }

    public int getTagSyntax() {
        return this.tagSyntax;
    }

    public void setNamingConvention(int i) {
        if (i == 10 || i == 11 || i == 12) {
            this.namingConvention = i;
            return;
        }
        throw new IllegalArgumentException("\"naming_convention\" can only be set to one of these: Configuration.AUTO_DETECT_NAMING_CONVENTION, or Configuration.LEGACY_NAMING_CONVENTIONor Configuration.CAMEL_CASE_NAMING_CONVENTION");
    }

    public int getNamingConvention() {
        return this.namingConvention;
    }

    public Template getTemplate(String str) throws TemplateNotFoundException, MalformedTemplateNameException, freemarker.core.ParseException, IOException {
        return getTemplate(str, null, null, null, true, false);
    }

    public Template getTemplate(String str, Locale locale) throws TemplateNotFoundException, MalformedTemplateNameException, freemarker.core.ParseException, IOException {
        return getTemplate(str, locale, null, null, true, false);
    }

    public Template getTemplate(String str, String str2) throws TemplateNotFoundException, MalformedTemplateNameException, freemarker.core.ParseException, IOException {
        return getTemplate(str, null, null, str2, true, false);
    }

    public Template getTemplate(String str, Locale locale, String str2) throws TemplateNotFoundException, MalformedTemplateNameException, freemarker.core.ParseException, IOException {
        return getTemplate(str, locale, null, str2, true, false);
    }

    public Template getTemplate(String str, Locale locale, String str2, boolean z) throws TemplateNotFoundException, MalformedTemplateNameException, freemarker.core.ParseException, IOException {
        return getTemplate(str, locale, null, str2, z, false);
    }

    public Template getTemplate(String str, Locale locale, String str2, boolean z, boolean z2) throws TemplateNotFoundException, MalformedTemplateNameException, freemarker.core.ParseException, IOException {
        return getTemplate(str, locale, null, str2, z, z2);
    }

    public Template getTemplate(String str, Locale locale, Object obj, String str2, boolean z, boolean z2) throws TemplateNotFoundException, MalformedTemplateNameException, freemarker.core.ParseException, IOException {
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        if (locale == null) {
            locale = getLocale();
        }
        Locale locale2 = locale;
        if (str2 == null) {
            str2 = getEncoding(locale2);
        }
        TemplateCache.MaybeMissingTemplate template = this.cache.getTemplate(str, locale2, obj, str2, z);
        Template template2 = template.getTemplate();
        if (template2 != null) {
            return template2;
        }
        if (z2) {
            return null;
        }
        TemplateLoader templateLoader = getTemplateLoader();
        if (templateLoader == null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Don't know where to load template ");
            stringBuffer.append(StringUtil.jQuote(str));
            stringBuffer.append(" from because the \"template_loader\" FreeMarker ");
            stringBuffer.append("setting wasn't set (Configuration.setTemplateLoader), so it's null.");
            str3 = stringBuffer.toString();
        } else {
            String missingTemplateNormalizedName = template.getMissingTemplateNormalizedName();
            String missingTemplateReason = template.getMissingTemplateReason();
            TemplateLookupStrategy templateLookupStrategy = getTemplateLookupStrategy();
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Template not found for name ");
            stringBuffer2.append(StringUtil.jQuote(str));
            String str8 = "";
            if (missingTemplateNormalizedName == null || str == null || removeInitialSlash(str).equals(missingTemplateNormalizedName)) {
                str4 = str8;
            } else {
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append(" (normalized: ");
                stringBuffer3.append(StringUtil.jQuote(missingTemplateNormalizedName));
                stringBuffer3.append(")");
                str4 = stringBuffer3.toString();
            }
            stringBuffer2.append(str4);
            if (obj != null) {
                StringBuffer stringBuffer4 = new StringBuffer();
                stringBuffer4.append(" and custom lookup condition ");
                stringBuffer4.append(StringUtil.jQuote(obj));
                str5 = stringBuffer4.toString();
            } else {
                str5 = str8;
            }
            stringBuffer2.append(str5);
            stringBuffer2.append(FileUtil.HIDDEN_PREFIX);
            if (missingTemplateReason != null) {
                StringBuffer stringBuffer5 = new StringBuffer();
                stringBuffer5.append("\nReason given: ");
                stringBuffer5.append(ensureSentenceIsClosed(missingTemplateReason));
                str6 = stringBuffer5.toString();
            } else {
                str6 = str8;
            }
            stringBuffer2.append(str6);
            stringBuffer2.append("\nThe name was interpreted by this TemplateLoader: ");
            stringBuffer2.append(StringUtil.tryToString(templateLoader));
            stringBuffer2.append(FileUtil.HIDDEN_PREFIX);
            if (!isKnownNonConfusingLookupStrategy(templateLookupStrategy)) {
                StringBuffer stringBuffer6 = new StringBuffer();
                stringBuffer6.append("\n(Before that, the name was possibly changed by this lookup strategy: ");
                stringBuffer6.append(StringUtil.tryToString(templateLookupStrategy));
                stringBuffer6.append(".)");
                str7 = stringBuffer6.toString();
            } else {
                str7 = str8;
            }
            stringBuffer2.append(str7);
            stringBuffer2.append(!this.templateLoaderExplicitlySet ? "\nWarning: The \"template_loader\" FreeMarker setting wasn't set (Configuration.setTemplateLoader), and using the default value is most certainly not intended and dangerous, and can be the cause of this error." : str8);
            if (missingTemplateReason == null && str.indexOf(92) != -1) {
                str8 = "\nWarning: The name contains backslash (\"\\\") instead of slash (\"/\"); template names should use slash only.";
            }
            stringBuffer2.append(str8);
            str3 = stringBuffer2.toString();
        }
        String missingTemplateNormalizedName2 = template.getMissingTemplateNormalizedName();
        if (missingTemplateNormalizedName2 != null) {
            str = missingTemplateNormalizedName2;
        }
        throw new TemplateNotFoundException(str, obj, str3);
    }

    private boolean isKnownNonConfusingLookupStrategy(TemplateLookupStrategy templateLookupStrategy) {
        return templateLookupStrategy == TemplateLookupStrategy.DEFAULT_2_3_0;
    }

    private String removeInitialSlash(String str) {
        return str.startsWith("/") ? str.substring(1) : str;
    }

    private String ensureSentenceIsClosed(String str) {
        char charAt;
        if (str == null || str.length() == 0 || (charAt = str.charAt(str.length() - 1)) == '.' || charAt == '!' || charAt == '?') {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        stringBuffer.append(FileUtil.HIDDEN_PREFIX);
        return stringBuffer.toString();
    }

    public void setDefaultEncoding(String str) {
        this.defaultEncoding = str;
    }

    public String getDefaultEncoding() {
        return this.defaultEncoding;
    }

    public String getEncoding(Locale locale) {
        if (this.localeToCharsetMap.isEmpty()) {
            return this.defaultEncoding;
        }
        String str = (String) this.localeToCharsetMap.get(locale.toString());
        if (str == null) {
            if (locale.getVariant().length() > 0) {
                String str2 = (String) this.localeToCharsetMap.get(new Locale(locale.getLanguage(), locale.getCountry()).toString());
                if (str2 != null) {
                    this.localeToCharsetMap.put(locale.toString(), str2);
                }
            }
            str = (String) this.localeToCharsetMap.get(locale.getLanguage());
            if (str != null) {
                this.localeToCharsetMap.put(locale.toString(), str);
            }
        }
        return str != null ? str : this.defaultEncoding;
    }

    public void setEncoding(Locale locale, String str) {
        this.localeToCharsetMap.put(locale.toString(), str);
    }

    public void setSharedVariable(String str, TemplateModel templateModel) {
        HashMap hashMap;
        if (this.sharedVariables.put(str, templateModel) != null && (hashMap = this.rewrappableSharedVariables) != null) {
            hashMap.remove(str);
        }
    }

    public Set getSharedVariableNames() {
        return new HashSet(this.sharedVariables.keySet());
    }

    public void setSharedVariable(String str, Object obj) throws TemplateModelException {
        setSharedVariable(str, getObjectWrapper().wrap(obj));
    }

    public void setSharedVaribles(Map map) throws TemplateModelException {
        this.rewrappableSharedVariables = new HashMap(map);
        this.sharedVariables.clear();
        setSharedVariablesFromRewrappableSharedVariables();
    }

    private void setSharedVariablesFromRewrappableSharedVariables() throws TemplateModelException {
        TemplateModel templateModel;
        HashMap hashMap = this.rewrappableSharedVariables;
        if (hashMap != null) {
            for (Map.Entry entry : hashMap.entrySet()) {
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                if (value instanceof TemplateModel) {
                    templateModel = (TemplateModel) value;
                } else {
                    templateModel = getObjectWrapper().wrap(value);
                }
                this.sharedVariables.put(str, templateModel);
            }
        }
    }

    public void setAllSharedVariables(TemplateHashModelEx templateHashModelEx) throws TemplateModelException {
        TemplateModelIterator it = templateHashModelEx.keys().iterator();
        TemplateModelIterator it2 = templateHashModelEx.values().iterator();
        while (it.hasNext()) {
            setSharedVariable(((TemplateScalarModel) it.next()).getAsString(), it2.next());
        }
    }

    public TemplateModel getSharedVariable(String str) {
        return (TemplateModel) this.sharedVariables.get(str);
    }

    public void clearSharedVariables() {
        this.sharedVariables.clear();
        loadBuiltInSharedVariables();
    }

    public void clearTemplateCache() {
        this.cache.clear();
    }

    public void removeTemplateFromCache(String str) throws IOException {
        Locale locale = getLocale();
        removeTemplateFromCache(str, locale, getEncoding(locale), true);
    }

    public void removeTemplateFromCache(String str, Locale locale) throws IOException {
        removeTemplateFromCache(str, locale, getEncoding(locale), true);
    }

    public void removeTemplateFromCache(String str, String str2) throws IOException {
        removeTemplateFromCache(str, getLocale(), str2, true);
    }

    public void removeTemplateFromCache(String str, Locale locale, String str2) throws IOException {
        removeTemplateFromCache(str, locale, str2, true);
    }

    public void removeTemplateFromCache(String str, Locale locale, String str2, boolean z) throws IOException {
        this.cache.removeTemplate(str, locale, str2, z);
    }

    public boolean getLocalizedLookup() {
        return this.cache.getLocalizedLookup();
    }

    public void setLocalizedLookup(boolean z) {
        this.localizedLookup = z;
        this.cache.setLocalizedLookup(z);
    }

    /* JADX WARNING: Removed duplicated region for block: B:211:0x035f  */
    /* JADX WARNING: Removed duplicated region for block: B:221:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setSetting(java.lang.String r8, java.lang.String r9) throws freemarker.template.TemplateException {
        /*
            r7 = this;
            java.lang.String r0 = "TemplateUpdateInterval"
            boolean r0 = r0.equalsIgnoreCase(r8)     // Catch:{ Exception -> 0x0363 }
            java.lang.String r1 = "default_encoding"
            java.lang.String r2 = "template_update_delay"
            if (r0 == 0) goto L_0x000e
            r8 = r2
            goto L_0x0017
        L_0x000e:
            java.lang.String r0 = "DefaultEncoding"
            boolean r0 = r0.equalsIgnoreCase(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0017
            r8 = r1
        L_0x0017:
            boolean r0 = r1.equals(r8)     // Catch:{ Exception -> 0x0363 }
            r1 = 1
            r3 = 0
            if (r0 != 0) goto L_0x0359
            java.lang.String r0 = "defaultEncoding"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0029
            goto L_0x0359
        L_0x0029:
            java.lang.String r0 = "localized_lookup"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0351
            java.lang.String r0 = "localizedLookup"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x003b
            goto L_0x0351
        L_0x003b:
            java.lang.String r0 = "strict_syntax"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0349
            java.lang.String r0 = "strictSyntax"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x004d
            goto L_0x0349
        L_0x004d:
            java.lang.String r0 = "whitespace_stripping"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0341
            java.lang.String r0 = "whitespaceStripping"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x005f
            goto L_0x0341
        L_0x005f:
            java.lang.String r0 = "cache_storage"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            java.lang.String r4 = "default"
            if (r0 != 0) goto L_0x02b0
            java.lang.String r0 = "cacheStorage"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0073
            goto L_0x02b0
        L_0x0073:
            boolean r0 = r2.equals(r8)     // Catch:{ Exception -> 0x0363 }
            r2 = 2
            if (r0 != 0) goto L_0x0241
            java.lang.String r0 = "templateUpdateDelay"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0084
            goto L_0x0241
        L_0x0084:
            java.lang.String r0 = "auto_include"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0238
            java.lang.String r0 = "autoInclude"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0096
            goto L_0x0238
        L_0x0096:
            java.lang.String r0 = "auto_import"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x022f
            java.lang.String r0 = "autoImport"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x00a8
            goto L_0x022f
        L_0x00a8:
            java.lang.String r0 = "tag_syntax"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            java.lang.String r5 = "autoDetect"
            java.lang.String r6 = "auto_detect"
            if (r0 != 0) goto L_0x01ec
            java.lang.String r0 = "tagSyntax"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x00be
            goto L_0x01ec
        L_0x00be:
            java.lang.String r0 = "naming_convention"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x01ac
            java.lang.String r0 = "namingConvention"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x00d0
            goto L_0x01ac
        L_0x00d0:
            java.lang.String r0 = "incompatible_improvements"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x01a2
            java.lang.String r0 = "incompatibleImprovements"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x00e2
            goto L_0x01a2
        L_0x00e2:
            java.lang.String r0 = "incompatible_enhancements"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x00ef
            r7.setIncompatibleEnhancements(r9)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x00ef:
            java.lang.String r0 = "template_loader"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0179
            java.lang.String r0 = "templateLoader"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0101
            goto L_0x0179
        L_0x0101:
            java.lang.String r0 = "template_lookup_strategy"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0150
            java.lang.String r0 = "templateLookupStrategy"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0112
            goto L_0x0150
        L_0x0112:
            java.lang.String r0 = "template_name_format"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0122
            java.lang.String r0 = "templateNameFormat"
            boolean r0 = r0.equals(r8)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x035d
        L_0x0122:
            boolean r0 = r9.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x012d
            r7.unsetTemplateNameFormat()     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x012d:
            java.lang.String r0 = "default_2_3_0"
            boolean r0 = r9.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x013c
            freemarker.cache.TemplateNameFormat r0 = freemarker.cache.TemplateNameFormat.DEFAULT_2_3_0     // Catch:{ Exception -> 0x0363 }
            r7.setTemplateNameFormat(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x013c:
            java.lang.String r0 = "default_2_4_0"
            boolean r0 = r9.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x014b
            freemarker.cache.TemplateNameFormat r0 = freemarker.cache.TemplateNameFormat.DEFAULT_2_4_0     // Catch:{ Exception -> 0x0363 }
            r7.setTemplateNameFormat(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x014b:
            freemarker.template.TemplateException r0 = r7.invalidSettingValueException(r8, r9)     // Catch:{ Exception -> 0x0363 }
            throw r0     // Catch:{ Exception -> 0x0363 }
        L_0x0150:
            boolean r0 = r9.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x015b
            r7.unsetTemplateLookupStrategy()     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x015b:
            java.lang.Class r0 = freemarker.template.Configuration.class$freemarker$cache$TemplateLookupStrategy     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0168
            java.lang.String r0 = "freemarker.cache.TemplateLookupStrategy"
            java.lang.Class r0 = class$(r0)     // Catch:{ Exception -> 0x0363 }
            freemarker.template.Configuration.class$freemarker$cache$TemplateLookupStrategy = r0     // Catch:{ Exception -> 0x0363 }
            goto L_0x016a
        L_0x0168:
            java.lang.Class r0 = freemarker.template.Configuration.class$freemarker$cache$TemplateLookupStrategy     // Catch:{ Exception -> 0x0363 }
        L_0x016a:
            freemarker.core._SettingEvaluationEnvironment r1 = freemarker.core._SettingEvaluationEnvironment.getCurrent()     // Catch:{ Exception -> 0x0363 }
            java.lang.Object r0 = freemarker.core._ObjectBuilderSettingEvaluator.eval(r9, r0, r1)     // Catch:{ Exception -> 0x0363 }
            freemarker.cache.TemplateLookupStrategy r0 = (freemarker.cache.TemplateLookupStrategy) r0     // Catch:{ Exception -> 0x0363 }
            r7.setTemplateLookupStrategy(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0179:
            boolean r0 = r9.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0184
            r7.unsetTemplateLoader()     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0184:
            java.lang.Class r0 = freemarker.template.Configuration.class$freemarker$cache$TemplateLoader     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0191
            java.lang.String r0 = "freemarker.cache.TemplateLoader"
            java.lang.Class r0 = class$(r0)     // Catch:{ Exception -> 0x0363 }
            freemarker.template.Configuration.class$freemarker$cache$TemplateLoader = r0     // Catch:{ Exception -> 0x0363 }
            goto L_0x0193
        L_0x0191:
            java.lang.Class r0 = freemarker.template.Configuration.class$freemarker$cache$TemplateLoader     // Catch:{ Exception -> 0x0363 }
        L_0x0193:
            freemarker.core._SettingEvaluationEnvironment r1 = freemarker.core._SettingEvaluationEnvironment.getCurrent()     // Catch:{ Exception -> 0x0363 }
            java.lang.Object r0 = freemarker.core._ObjectBuilderSettingEvaluator.eval(r9, r0, r1)     // Catch:{ Exception -> 0x0363 }
            freemarker.cache.TemplateLoader r0 = (freemarker.cache.TemplateLoader) r0     // Catch:{ Exception -> 0x0363 }
            r7.setTemplateLoader(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x01a2:
            freemarker.template.Version r0 = new freemarker.template.Version     // Catch:{ Exception -> 0x0363 }
            r0.<init>(r9)     // Catch:{ Exception -> 0x0363 }
            r7.setIncompatibleImprovements(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x01ac:
            boolean r0 = r6.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x01e5
            boolean r0 = r5.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x01b9
            goto L_0x01e5
        L_0x01b9:
            java.lang.String r0 = "legacy"
            boolean r0 = r0.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x01c8
            r0 = 11
            r7.setNamingConvention(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x01c8:
            java.lang.String r0 = "camel_case"
            boolean r0 = r0.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x01de
            java.lang.String r0 = "camelCase"
            boolean r0 = r0.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x01d9
            goto L_0x01de
        L_0x01d9:
            freemarker.template.TemplateException r0 = r7.invalidSettingValueException(r8, r9)     // Catch:{ Exception -> 0x0363 }
            throw r0     // Catch:{ Exception -> 0x0363 }
        L_0x01de:
            r0 = 12
            r7.setNamingConvention(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x01e5:
            r0 = 10
            r7.setNamingConvention(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x01ec:
            boolean r0 = r6.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x022a
            boolean r0 = r5.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x01f9
            goto L_0x022a
        L_0x01f9:
            java.lang.String r0 = "angle_bracket"
            boolean r0 = r0.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0225
            java.lang.String r0 = "angleBracket"
            boolean r0 = r0.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x020a
            goto L_0x0225
        L_0x020a:
            java.lang.String r0 = "square_bracket"
            boolean r0 = r0.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0220
            java.lang.String r0 = "squareBracket"
            boolean r0 = r0.equals(r9)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x021b
            goto L_0x0220
        L_0x021b:
            freemarker.template.TemplateException r0 = r7.invalidSettingValueException(r8, r9)     // Catch:{ Exception -> 0x0363 }
            throw r0     // Catch:{ Exception -> 0x0363 }
        L_0x0220:
            r7.setTagSyntax(r2)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0225:
            r7.setTagSyntax(r1)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x022a:
            r7.setTagSyntax(r3)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x022f:
            java.util.HashMap r0 = r7.parseAsImportList(r9)     // Catch:{ Exception -> 0x0363 }
            r7.setAutoImports(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0238:
            java.util.ArrayList r0 = r7.parseAsList(r9)     // Catch:{ Exception -> 0x0363 }
            r7.setAutoIncludes(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0241:
            java.lang.String r0 = "ms"
            boolean r0 = r9.endsWith(r0)     // Catch:{ Exception -> 0x0363 }
            r4 = 1000(0x3e8, double:4.94E-321)
            if (r0 == 0) goto L_0x025b
            r4 = 1
            int r0 = r9.length()     // Catch:{ Exception -> 0x0363 }
            int r0 = r0 - r2
            java.lang.String r0 = r9.substring(r3, r0)     // Catch:{ Exception -> 0x0363 }
            java.lang.String r0 = r7.rightTrim(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x02a4
        L_0x025b:
            java.lang.String r0 = "s"
            boolean r0 = r9.endsWith(r0)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x0271
            int r0 = r9.length()     // Catch:{ Exception -> 0x0363 }
            int r0 = r0 - r1
            java.lang.String r0 = r9.substring(r3, r0)     // Catch:{ Exception -> 0x0363 }
            java.lang.String r0 = r7.rightTrim(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x02a4
        L_0x0271:
            java.lang.String r0 = "m"
            boolean r0 = r9.endsWith(r0)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x028a
            r4 = 60000(0xea60, double:2.9644E-319)
            int r0 = r9.length()     // Catch:{ Exception -> 0x0363 }
            int r0 = r0 - r1
            java.lang.String r0 = r9.substring(r3, r0)     // Catch:{ Exception -> 0x0363 }
            java.lang.String r0 = r7.rightTrim(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x02a4
        L_0x028a:
            java.lang.String r0 = "h"
            boolean r0 = r9.endsWith(r0)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x02a3
            r4 = 3600000(0x36ee80, double:1.7786363E-317)
            int r0 = r9.length()     // Catch:{ Exception -> 0x0363 }
            int r0 = r0 - r1
            java.lang.String r0 = r9.substring(r3, r0)     // Catch:{ Exception -> 0x0363 }
            java.lang.String r0 = r7.rightTrim(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x02a4
        L_0x02a3:
            r0 = r9
        L_0x02a4:
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0363 }
            long r0 = (long) r0     // Catch:{ Exception -> 0x0363 }
            long r0 = r0 * r4
            r7.setTemplateUpdateDelayMilliseconds(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x02b0:
            boolean r0 = r9.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x0363 }
            if (r0 == 0) goto L_0x02b9
            r7.unsetCacheStorage()     // Catch:{ Exception -> 0x0363 }
        L_0x02b9:
            r0 = 46
            int r0 = r9.indexOf(r0)     // Catch:{ Exception -> 0x0363 }
            r1 = -1
            if (r0 != r1) goto L_0x0324
            r0 = 2147483647(0x7fffffff, float:NaN)
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x0363 }
            java.util.Map r0 = freemarker.template.utility.StringUtil.parseNameValuePairList(r9, r0)     // Catch:{ Exception -> 0x0363 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ Exception -> 0x0363 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x0363 }
            r1 = 0
            r2 = 0
        L_0x02d7:
            boolean r4 = r0.hasNext()     // Catch:{ Exception -> 0x0363 }
            if (r4 == 0) goto L_0x0311
            java.lang.Object r4 = r0.next()     // Catch:{ Exception -> 0x0363 }
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4     // Catch:{ Exception -> 0x0363 }
            java.lang.Object r5 = r4.getKey()     // Catch:{ Exception -> 0x0363 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ Exception -> 0x0363 }
            java.lang.Object r4 = r4.getValue()     // Catch:{ NumberFormatException -> 0x030c }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ NumberFormatException -> 0x030c }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ NumberFormatException -> 0x030c }
            java.lang.String r6 = "soft"
            boolean r6 = r6.equalsIgnoreCase(r5)     // Catch:{ Exception -> 0x0363 }
            if (r6 == 0) goto L_0x02fd
            r1 = r4
            goto L_0x02d7
        L_0x02fd:
            java.lang.String r2 = "strong"
            boolean r2 = r2.equalsIgnoreCase(r5)     // Catch:{ Exception -> 0x0363 }
            if (r2 == 0) goto L_0x0307
            r2 = r4
            goto L_0x02d7
        L_0x0307:
            freemarker.template.TemplateException r0 = r7.invalidSettingValueException(r8, r9)     // Catch:{ Exception -> 0x0363 }
            throw r0     // Catch:{ Exception -> 0x0363 }
        L_0x030c:
            freemarker.template.TemplateException r0 = r7.invalidSettingValueException(r8, r9)     // Catch:{ Exception -> 0x0363 }
            throw r0     // Catch:{ Exception -> 0x0363 }
        L_0x0311:
            if (r1 != 0) goto L_0x031b
            if (r2 == 0) goto L_0x0316
            goto L_0x031b
        L_0x0316:
            freemarker.template.TemplateException r0 = r7.invalidSettingValueException(r8, r9)     // Catch:{ Exception -> 0x0363 }
            throw r0     // Catch:{ Exception -> 0x0363 }
        L_0x031b:
            freemarker.cache.MruCacheStorage r0 = new freemarker.cache.MruCacheStorage     // Catch:{ Exception -> 0x0363 }
            r0.<init>(r2, r1)     // Catch:{ Exception -> 0x0363 }
            r7.setCacheStorage(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0324:
            java.lang.Class r0 = freemarker.template.Configuration.class$freemarker$cache$CacheStorage     // Catch:{ Exception -> 0x0363 }
            if (r0 != 0) goto L_0x0331
            java.lang.String r0 = "freemarker.cache.CacheStorage"
            java.lang.Class r0 = class$(r0)     // Catch:{ Exception -> 0x0363 }
            freemarker.template.Configuration.class$freemarker$cache$CacheStorage = r0     // Catch:{ Exception -> 0x0363 }
            goto L_0x0333
        L_0x0331:
            java.lang.Class r0 = freemarker.template.Configuration.class$freemarker$cache$CacheStorage     // Catch:{ Exception -> 0x0363 }
        L_0x0333:
            freemarker.core._SettingEvaluationEnvironment r1 = freemarker.core._SettingEvaluationEnvironment.getCurrent()     // Catch:{ Exception -> 0x0363 }
            java.lang.Object r0 = freemarker.core._ObjectBuilderSettingEvaluator.eval(r9, r0, r1)     // Catch:{ Exception -> 0x0363 }
            freemarker.cache.CacheStorage r0 = (freemarker.cache.CacheStorage) r0     // Catch:{ Exception -> 0x0363 }
            r7.setCacheStorage(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0341:
            boolean r0 = freemarker.template.utility.StringUtil.getYesNo(r9)     // Catch:{ Exception -> 0x0363 }
            r7.setWhitespaceStripping(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0349:
            boolean r0 = freemarker.template.utility.StringUtil.getYesNo(r9)     // Catch:{ Exception -> 0x0363 }
            r7.setStrictSyntaxMode(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0351:
            boolean r0 = freemarker.template.utility.StringUtil.getYesNo(r9)     // Catch:{ Exception -> 0x0363 }
            r7.setLocalizedLookup(r0)     // Catch:{ Exception -> 0x0363 }
            goto L_0x035c
        L_0x0359:
            r7.setDefaultEncoding(r9)     // Catch:{ Exception -> 0x0363 }
        L_0x035c:
            r1 = 0
        L_0x035d:
            if (r1 == 0) goto L_0x0362
            super.setSetting(r8, r9)
        L_0x0362:
            return
        L_0x0363:
            r0 = move-exception
            freemarker.template.TemplateException r8 = r7.settingValueAssignmentException(r8, r9, r0)
            goto L_0x036a
        L_0x0369:
            throw r8
        L_0x036a:
            goto L_0x0369
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.template.Configuration.setSetting(java.lang.String, java.lang.String):void");
    }

    private String rightTrim(String str) {
        int length = str.length();
        while (length > 0 && Character.isWhitespace(str.charAt(length - 1))) {
            length--;
        }
        return str.substring(0, length);
    }

    /* access modifiers changed from: package-private */
    public Set getSettingNames(boolean z) {
        return new _UnmodifiableCompositeSet(_CoreAPI.getConfigurableSettingNames(super, z), new _SortedArraySet(z ? SETTING_NAMES_CAMEL_CASE : SETTING_NAMES_SNAKE_CASE));
    }

    /* access modifiers changed from: protected */
    public String getCorrectedNameForUnknownSetting(String str) {
        if ("encoding".equals(str) || "charset".equals(str) || "default_charset".equals(str)) {
            return "default_encoding";
        }
        if ("defaultCharset".equals(str)) {
            return DEFAULT_ENCODING_KEY_CAMEL_CASE;
        }
        return super.getCorrectedNameForUnknownSetting(str);
    }

    public void addAutoImport(String str, String str2) {
        synchronized (this) {
            this.autoImports.remove(str);
            this.autoImports.add(str);
            this.autoImportNsToTmpMap.put(str, str2);
        }
    }

    public void removeAutoImport(String str) {
        synchronized (this) {
            this.autoImports.remove(str);
            this.autoImportNsToTmpMap.remove(str);
        }
    }

    public void setAutoImports(Map map) {
        synchronized (this) {
            this.autoImports = new ArrayList(map.keySet());
            if (map instanceof HashMap) {
                this.autoImportNsToTmpMap = (Map) ((HashMap) map).clone();
            } else if (map instanceof SortedMap) {
                this.autoImportNsToTmpMap = new TreeMap(map);
            } else {
                this.autoImportNsToTmpMap = new HashMap(map);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void doAutoImportsAndIncludes(Environment environment) throws TemplateException, IOException {
        for (int i = 0; i < this.autoImports.size(); i++) {
            String str = (String) this.autoImports.get(i);
            environment.importLib((String) this.autoImportNsToTmpMap.get(str), str);
        }
        for (int i2 = 0; i2 < this.autoIncludes.size(); i2++) {
            environment.include(getTemplate((String) this.autoIncludes.get(i2), environment.getLocale()));
        }
    }

    public void addAutoInclude(String str) {
        synchronized (this) {
            this.autoIncludes.remove(str);
            this.autoIncludes.add(str);
        }
    }

    public void setAutoIncludes(List list) {
        synchronized (this) {
            this.autoIncludes.clear();
            for (Object obj : list) {
                if (obj instanceof String) {
                    this.autoIncludes.add(obj);
                } else {
                    throw new IllegalArgumentException("List items must be String-s.");
                }
            }
        }
    }

    public void removeAutoInclude(String str) {
        synchronized (this) {
            this.autoIncludes.remove(str);
        }
    }

    public static String getVersionNumber() {
        return VERSION.toString();
    }

    public static Version getVersion() {
        return VERSION;
    }

    public static ObjectWrapper getDefaultObjectWrapper(Version version) {
        if (version.intValue() < _TemplateAPI.VERSION_INT_2_3_21) {
            return ObjectWrapper.DEFAULT_WRAPPER;
        }
        return new DefaultObjectWrapperBuilder(version).build();
    }

    public Set getSupportedBuiltInNames() {
        return _CoreAPI.getSupportedBuiltInNames();
    }

    public Set getSupportedBuiltInDirectiveNames() {
        return _CoreAPI.BUILT_IN_DIRECTIVE_NAMES;
    }

    private static String getRequiredVersionProperty(Properties properties, String str) {
        String property = properties.getProperty(str);
        if (property != null) {
            return property;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Version file is corrupt: \"");
        stringBuffer.append(str);
        stringBuffer.append("\" property is missing.");
        throw new RuntimeException(stringBuffer.toString());
    }
}
