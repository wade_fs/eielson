package freemarker.template.utility;

import com.baidu.mobstat.Config;
import com.google.common.base.Ascii;
import freemarker.core.BugException;
import freemarker.core.Environment;
import freemarker.core.ParseException;
import freemarker.template.Version;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.StringTokenizer;

public class StringUtil {
    private static final char[] ESCAPES = createEscapes();
    private static final int ESC_BACKSLASH = 3;
    private static final int ESC_HEXA = 1;
    private static final int NO_ESC = 0;

    public static boolean isFTLIdentifierStart(char c) {
        return c < 170 ? (c >= 97 && c <= 122) || (c >= 64 && c <= 90) || c == 36 || c == 95 : c < 43000 ? c < 11631 ? c < 8488 ? c < 8336 ? c < 216 ? c < 186 ? c == 170 || c == 181 : c == 186 || (c >= 192 && c <= 214) : c < 8305 ? (c >= 216 && c <= 246) || (c >= 248 && c <= 8191) : c == 8305 || c == 8319 : c < 8469 ? c < 8455 ? (c >= 8336 && c <= 8348) || c == 8450 : c == 8455 || (c >= 8458 && c <= 8467) : c < 8484 ? c == 8469 || (c >= 8473 && c <= 8477) : c == 8484 || c == 8486 : c < 11312 ? c < 8517 ? c < 8495 ? c == 8488 || (c >= 8490 && c <= 8493) : (c >= 8495 && c <= 8505) || (c >= 8508 && c <= 8511) : c < 8579 ? (c >= 8517 && c <= 8521) || c == 8526 : (c >= 8579 && c <= 8580) || (c >= 11264 && c <= 11310) : c < 11520 ? c < 11499 ? (c >= 11312 && c <= 11358) || (c >= 11360 && c <= 11492) : (c >= 11499 && c <= 11502) || (c >= 11506 && c <= 11507) : c < 11565 ? (c >= 11520 && c <= 11557) || c == 11559 : c == 11565 || (c >= 11568 && c <= 11623) : c < 12784 ? c < 11728 ? c < 11696 ? c < 11680 ? c == 11631 || (c >= 11648 && c <= 11670) : (c >= 11680 && c <= 11686) || (c >= 11688 && c <= 11694) : c < 11712 ? (c >= 11696 && c <= 11702) || (c >= 11704 && c <= 11710) : (c >= 11712 && c <= 11718) || (c >= 11720 && c <= 11726) : c < 12337 ? c < 11823 ? (c >= 11728 && c <= 11734) || (c >= 11736 && c <= 11742) : c == 11823 || (c >= 12293 && c <= 12294) : c < 12352 ? (c >= 12337 && c <= 12341) || (c >= 12347 && c <= 12348) : (c >= 12352 && c <= 12687) || (c >= 12704 && c <= 12730) : c < 42623 ? c < 42192 ? c < 13312 ? (c >= 12784 && c <= 12799) || (c >= 13056 && c <= 13183) : (c >= 13312 && c <= 19893) || (c >= 19968 && c <= 42124) : c < 42512 ? (c >= 42192 && c <= 42237) || (c >= 42240 && c <= 42508) : (c >= 42512 && c <= 42539) || (c >= 42560 && c <= 42606) : c < 42891 ? c < 42775 ? (c >= 42623 && c <= 42647) || (c >= 42656 && c <= 42725) : (c >= 42775 && c <= 42783) || (c >= 42786 && c <= 42888) : c < 42912 ? (c >= 42891 && c <= 42894) || (c >= 42896 && c <= 42899) : c >= 42912 && c <= 42922 : c < 43808 ? c < 43588 ? c < 43259 ? c < 43072 ? c < 43015 ? (c >= 43000 && c <= 43009) || (c >= 43011 && c <= 43013) : (c >= 43015 && c <= 43018) || (c >= 43020 && c <= 43042) : c < 43216 ? (c >= 43072 && c <= 43123) || (c >= 43138 && c <= 43187) : (c >= 43216 && c <= 43225) || (c >= 43250 && c <= 43255) : c < 43396 ? c < 43312 ? c == 43259 || (c >= 43264 && c <= 43301) : (c >= 43312 && c <= 43334) || (c >= 43360 && c <= 43388) : c < 43520 ? (c >= 43396 && c <= 43442) || (c >= 43471 && c <= 43481) : (c >= 43520 && c <= 43560) || (c >= 43584 && c <= 43586) : c < 43712 ? c < 43648 ? c < 43616 ? (c >= 43588 && c <= 43595) || (c >= 43600 && c <= 43609) : (c >= 43616 && c <= 43638) || c == 43642 : c < 43701 ? (c >= 43648 && c <= 43695) || c == 43697 : (c >= 43701 && c <= 43702) || (c >= 43705 && c <= 43709) : c < 43762 ? c < 43739 ? c == 43712 || c == 43714 : (c >= 43739 && c <= 43741) || (c >= 43744 && c <= 43754) : c < 43785 ? (c >= 43762 && c <= 43764) || (c >= 43777 && c <= 43782) : (c >= 43785 && c <= 43790) || (c >= 43793 && c <= 43798) : c < 64326 ? c < 64275 ? c < 44032 ? c < 43968 ? (c >= 43808 && c <= 43814) || (c >= 43816 && c <= 43822) : (c >= 43968 && c <= 44002) || (c >= 44016 && c <= 44025) : c < 55243 ? (c >= 44032 && c <= 55203) || (c >= 55216 && c <= 55238) : (c >= 55243 && c <= 55291) || (c >= 63744 && c <= 64262) : c < 64312 ? c < 64287 ? (c >= 64275 && c <= 64279) || c == 64285 : (c >= 64287 && c <= 64296) || (c >= 64298 && c <= 64310) : c < 64320 ? (c >= 64312 && c <= 64316) || c == 64318 : (c >= 64320 && c <= 64321) || (c >= 64323 && c <= 64324) : c < 65313 ? c < 65008 ? c < 64848 ? (c >= 64326 && c <= 64433) || (c >= 64467 && c <= 64829) : (c >= 64848 && c <= 64911) || (c >= 64914 && c <= 64967) : c < 65142 ? (c >= 65008 && c <= 65019) || (c >= 65136 && c <= 65140) : (c >= 65142 && c <= 65276) || (c >= 65296 && c <= 65305) : c < 65482 ? c < 65382 ? (c >= 65313 && c <= 65338) || (c >= 65345 && c <= 65370) : (c >= 65382 && c <= 65470) || (c >= 65474 && c <= 65479) : c < 65498 ? (c >= 65482 && c <= 65487) || (c >= 65490 && c <= 65495) : c >= 65498 && c <= 65500;
    }

    private static boolean isInterpolationStart(char c) {
        return c == '$' || c == '#';
    }

    private static boolean safeInURL(char c, boolean z) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || ((c >= '0' && c <= '9') || c == '_' || c == '-' || c == '.' || c == '!' || c == '~' || ((c >= '\'' && c <= '*') || (z && c == '/')));
    }

    private static char toHexDigit(int i) {
        return (char) (i < 10 ? i + 48 : (i - 10) + 65);
    }

    public static String HTMLEnc(String str) {
        return XMLEncNA(str);
    }

    public static String XMLEnc(String str) {
        return XMLOrXHTMLEnc(str, "&apos;");
    }

    public static String XHTMLEnc(String str) {
        return XMLOrXHTMLEnc(str, "&#39;");
    }

    private static String XMLOrXHTMLEnc(String str, String str2) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '<' || charAt == '>' || charAt == '&' || charAt == '\"' || charAt == '\'') {
                StringBuffer stringBuffer = new StringBuffer(str.substring(0, i));
                if (charAt == '\"') {
                    stringBuffer.append("&quot;");
                } else if (charAt == '<') {
                    stringBuffer.append("&lt;");
                } else if (charAt == '>') {
                    stringBuffer.append("&gt;");
                } else if (charAt == '&') {
                    stringBuffer.append("&amp;");
                } else if (charAt == '\'') {
                    stringBuffer.append(str2);
                }
                int i2 = i + 1;
                int i3 = i2;
                while (i2 < length) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == '<' || charAt2 == '>' || charAt2 == '&' || charAt2 == '\"' || charAt2 == '\'') {
                        stringBuffer.append(str.substring(i3, i2));
                        if (charAt2 == '\"') {
                            stringBuffer.append("&quot;");
                        } else if (charAt2 == '<') {
                            stringBuffer.append("&lt;");
                        } else if (charAt2 == '>') {
                            stringBuffer.append("&gt;");
                        } else if (charAt2 == '&') {
                            stringBuffer.append("&amp;");
                        } else if (charAt2 == '\'') {
                            stringBuffer.append(str2);
                        }
                        i3 = i2 + 1;
                    }
                    i2++;
                }
                if (i3 < length) {
                    stringBuffer.append(str.substring(i3));
                }
                return stringBuffer.toString();
            }
        }
        return str;
    }

    public static String XMLEncNA(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '<' || charAt == '>' || charAt == '&' || charAt == '\"') {
                StringBuffer stringBuffer = new StringBuffer(str.substring(0, i));
                if (charAt == '\"') {
                    stringBuffer.append("&quot;");
                } else if (charAt == '&') {
                    stringBuffer.append("&amp;");
                } else if (charAt == '<') {
                    stringBuffer.append("&lt;");
                } else if (charAt == '>') {
                    stringBuffer.append("&gt;");
                }
                int i2 = i + 1;
                int i3 = i2;
                while (i2 < length) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == '<' || charAt2 == '>' || charAt2 == '&' || charAt2 == '\"') {
                        stringBuffer.append(str.substring(i3, i2));
                        if (charAt2 == '\"') {
                            stringBuffer.append("&quot;");
                        } else if (charAt2 == '&') {
                            stringBuffer.append("&amp;");
                        } else if (charAt2 == '<') {
                            stringBuffer.append("&lt;");
                        } else if (charAt2 == '>') {
                            stringBuffer.append("&gt;");
                        }
                        i3 = i2 + 1;
                    }
                    i2++;
                }
                if (i3 < length) {
                    stringBuffer.append(str.substring(i3));
                }
                return stringBuffer.toString();
            }
        }
        return str;
    }

    public static String XMLEncQAttr(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '<' || charAt == '&' || charAt == '\"') {
                StringBuffer stringBuffer = new StringBuffer(str.substring(0, i));
                if (charAt == '\"') {
                    stringBuffer.append("&quot;");
                } else if (charAt == '&') {
                    stringBuffer.append("&amp;");
                } else if (charAt == '<') {
                    stringBuffer.append("&lt;");
                }
                int i2 = i + 1;
                int i3 = i2;
                while (i2 < length) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == '<' || charAt2 == '&' || charAt2 == '\"') {
                        stringBuffer.append(str.substring(i3, i2));
                        if (charAt2 == '\"') {
                            stringBuffer.append("&quot;");
                        } else if (charAt2 == '&') {
                            stringBuffer.append("&amp;");
                        } else if (charAt2 == '<') {
                            stringBuffer.append("&lt;");
                        }
                        i3 = i2 + 1;
                    }
                    i2++;
                }
                if (i3 < length) {
                    stringBuffer.append(str.substring(i3));
                }
                return stringBuffer.toString();
            }
        }
        return str;
    }

    public static String XMLEncNQG(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '<' || ((charAt == '>' && i > 1 && str.charAt(i - 1) == ']' && str.charAt(i - 2) == ']') || charAt == '&')) {
                StringBuffer stringBuffer = new StringBuffer(str.substring(0, i));
                if (charAt == '&') {
                    stringBuffer.append("&amp;");
                } else if (charAt == '<') {
                    stringBuffer.append("&lt;");
                } else if (charAt == '>') {
                    stringBuffer.append("&gt;");
                } else {
                    throw new BugException();
                }
                int i2 = i + 1;
                int i3 = i2;
                while (i2 < length) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == '<' || ((charAt2 == '>' && i2 > 1 && str.charAt(i2 - 1) == ']' && str.charAt(i2 - 2) == ']') || charAt2 == '&')) {
                        stringBuffer.append(str.substring(i3, i2));
                        if (charAt2 == '&') {
                            stringBuffer.append("&amp;");
                        } else if (charAt2 == '<') {
                            stringBuffer.append("&lt;");
                        } else if (charAt2 == '>') {
                            stringBuffer.append("&gt;");
                        } else {
                            throw new BugException();
                        }
                        i3 = i2 + 1;
                    }
                    i2++;
                }
                if (i3 < length) {
                    stringBuffer.append(str.substring(i3));
                }
                return stringBuffer.toString();
            }
        }
        return str;
    }

    public static String RTFEnc(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '\\' || charAt == '{' || charAt == '}') {
                StringBuffer stringBuffer = new StringBuffer(str.substring(0, i));
                if (charAt == '\\') {
                    stringBuffer.append("\\\\");
                } else if (charAt == '{') {
                    stringBuffer.append("\\{");
                } else if (charAt == '}') {
                    stringBuffer.append("\\}");
                }
                int i2 = i + 1;
                int i3 = i2;
                while (i2 < length) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == '\\' || charAt2 == '{' || charAt2 == '}') {
                        stringBuffer.append(str.substring(i3, i2));
                        if (charAt2 == '\\') {
                            stringBuffer.append("\\\\");
                        } else if (charAt2 == '{') {
                            stringBuffer.append("\\{");
                        } else if (charAt2 == '}') {
                            stringBuffer.append("\\}");
                        }
                        i3 = i2 + 1;
                    }
                    i2++;
                }
                if (i3 < length) {
                    stringBuffer.append(str.substring(i3));
                }
                return stringBuffer.toString();
            }
        }
        return str;
    }

    public static String URLEnc(String str, String str2) throws UnsupportedEncodingException {
        return URLEnc(str, str2, false);
    }

    public static String URLPathEnc(String str, String str2) throws UnsupportedEncodingException {
        return URLEnc(str, str2, true);
    }

    private static String URLEnc(String str, String str2, boolean z) throws UnsupportedEncodingException {
        int length = str.length();
        int i = 0;
        while (i < length && safeInURL(str.charAt(i), z)) {
            i++;
        }
        if (i == length) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer((length / 3) + length + 2);
        stringBuffer.append(str.substring(0, i));
        int i2 = i + 1;
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (safeInURL(charAt, z)) {
                if (i != -1) {
                    byte[] bytes = str.substring(i, i2).getBytes(str2);
                    for (byte b : bytes) {
                        stringBuffer.append('%');
                        byte b2 = b & Ascii.f4512SI;
                        int i3 = (b >> 4) & 15;
                        stringBuffer.append((char) (i3 < 10 ? i3 + 48 : (i3 - 10) + 65));
                        stringBuffer.append((char) (b2 < 10 ? b2 + 48 : (b2 - 10) + 65));
                    }
                    i = -1;
                }
                stringBuffer.append(charAt);
            } else if (i == -1) {
                i = i2;
            }
            i2++;
        }
        if (i != -1) {
            byte[] bytes2 = str.substring(i, i2).getBytes(str2);
            for (byte b3 : bytes2) {
                stringBuffer.append('%');
                byte b4 = b3 & Ascii.f4512SI;
                int i4 = (b3 >> 4) & 15;
                stringBuffer.append((char) (i4 < 10 ? i4 + 48 : (i4 - 10) + 65));
                stringBuffer.append((char) (b4 < 10 ? b4 + 48 : (b4 - 10) + 65));
            }
        }
        return stringBuffer.toString();
    }

    private static char[] createEscapes() {
        char[] cArr = new char[93];
        for (int i = 0; i < 32; i++) {
            cArr[i] = 1;
        }
        cArr[92] = '\\';
        cArr[39] = '\'';
        cArr[34] = '\"';
        cArr[60] = 'l';
        cArr[62] = 'g';
        cArr[38] = 'a';
        cArr[8] = 'b';
        cArr[9] = 't';
        cArr[10] = 'n';
        cArr[12] = 'f';
        cArr[13] = 'r';
        return cArr;
    }

    public static String FTLStringLiteralEnc(String str, char c) {
        return FTLStringLiteralEnc(str, c, false);
    }

    public static String FTLStringLiteralEnc(String str) {
        return FTLStringLiteralEnc(str, 0, false);
    }

    private static String FTLStringLiteralEnc(String str, char c, boolean z) {
        int length = str.length();
        char c2 = '\'';
        if (c == 0) {
            c2 = 0;
        } else if (c != '\"') {
            if (c == '\'') {
                c2 = '\"';
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Unsupported quotation character: ");
                stringBuffer.append(c);
                throw new IllegalArgumentException(stringBuffer.toString());
            }
        }
        int length2 = ESCAPES.length;
        StringBuffer stringBuffer2 = null;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            char c3 = '{';
            if (charAt < length2) {
                c3 = ESCAPES[charAt];
            } else if (charAt != '{' || i <= 0 || !isInterpolationStart(str.charAt(i - 1))) {
                c3 = 0;
            }
            if (c3 != 0 && c3 != c2) {
                if (stringBuffer2 == null) {
                    stringBuffer2 = new StringBuffer(str.length() + 4 + (z ? 2 : 0));
                    if (z) {
                        stringBuffer2.append(c);
                    }
                    stringBuffer2.append(str.substring(0, i));
                }
                if (c3 == 1) {
                    stringBuffer2.append("\\x00");
                    int i2 = (charAt >> 4) & 15;
                    char c4 = (char) (charAt & 15);
                    stringBuffer2.append((char) (i2 < 10 ? i2 + 48 : (i2 - 10) + 65));
                    stringBuffer2.append((char) (c4 < 10 ? c4 + '0' : (c4 - 10) + 65));
                } else {
                    stringBuffer2.append('\\');
                    stringBuffer2.append(c3);
                }
            } else if (stringBuffer2 != null) {
                stringBuffer2.append(charAt);
            }
        }
        if (stringBuffer2 != null) {
            if (z) {
                stringBuffer2.append(c);
            }
            return stringBuffer2.toString();
        } else if (!z) {
            return str;
        } else {
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append(c);
            stringBuffer3.append(str);
            stringBuffer3.append(c);
            return stringBuffer3.toString();
        }
    }

    public static String FTLStringLiteralDec(String str) throws ParseException {
        int i;
        int i2;
        int i3;
        int indexOf = str.indexOf(92);
        if (indexOf == -1) {
            return str;
        }
        int length = str.length() - 1;
        StringBuffer stringBuffer = new StringBuffer(length);
        int i4 = indexOf;
        int i5 = 0;
        do {
            stringBuffer.append(str.substring(i5, i4));
            if (i4 < length) {
                char charAt = str.charAt(i4 + 1);
                if (charAt == '\"') {
                    stringBuffer.append('\"');
                } else if (charAt == '\'') {
                    stringBuffer.append('\'');
                } else if (charAt == '\\') {
                    stringBuffer.append('\\');
                } else if (charAt == 'l') {
                    stringBuffer.append('<');
                } else if (charAt == 'n') {
                    stringBuffer.append(10);
                } else if (charAt == 'r') {
                    stringBuffer.append(13);
                } else if (charAt == 't') {
                    stringBuffer.append(9);
                } else if (charAt == 'x') {
                    int i6 = i4 + 2;
                    int i7 = i6 + 3;
                    if (length <= i7) {
                        i7 = length;
                    }
                    int i8 = i6;
                    int i9 = 0;
                    while (i8 <= i7) {
                        char charAt2 = str.charAt(i8);
                        if (charAt2 < '0' || charAt2 > '9') {
                            if (charAt2 < 'a' || charAt2 > 'f') {
                                if (charAt2 < 'A' || charAt2 > 'F') {
                                    break;
                                }
                                i = i9 << 4;
                                i2 = charAt2 - 'A';
                            } else {
                                i = i9 << 4;
                                i2 = charAt2 - 'a';
                            }
                            i3 = i2 + 10;
                        } else {
                            i = i9 << 4;
                            i3 = charAt2 - '0';
                        }
                        i9 = i + i3;
                        i8++;
                    }
                    if (i6 < i8) {
                        stringBuffer.append((char) i9);
                        i5 = i8;
                        i4 = str.indexOf(92, i5);
                    } else {
                        throw new ParseException("Invalid \\x escape in a string literal", 0, 0);
                    }
                } else if (charAt == '{') {
                    stringBuffer.append('{');
                } else if (charAt == 'a') {
                    stringBuffer.append('&');
                } else if (charAt == 'b') {
                    stringBuffer.append(8);
                } else if (charAt == 'f') {
                    stringBuffer.append(12);
                } else if (charAt == 'g') {
                    stringBuffer.append('>');
                } else {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("Invalid escape sequence (\\");
                    stringBuffer2.append(charAt);
                    stringBuffer2.append(") in a string literal");
                    throw new ParseException(stringBuffer2.toString(), 0, 0);
                }
                i5 = i4 + 2;
                i4 = str.indexOf(92, i5);
            } else {
                throw new ParseException("The last character of string literal is backslash", 0, 0);
            }
        } while (i4 != -1);
        stringBuffer.append(str.substring(i5));
        return stringBuffer.toString();
    }

    public static Locale deduceLocale(String str) {
        if (str == null) {
            return null;
        }
        Locale.getDefault();
        if (str.length() > 0 && str.charAt(0) == '\"') {
            str = str.substring(1, str.length() - 1);
        }
        StringTokenizer stringTokenizer = new StringTokenizer(str, ",_ ");
        String str2 = "";
        String nextToken = stringTokenizer.hasMoreTokens() ? stringTokenizer.nextToken() : str2;
        if (stringTokenizer.hasMoreTokens()) {
            str2 = stringTokenizer.nextToken();
        }
        if (!stringTokenizer.hasMoreTokens()) {
            return new Locale(nextToken, str2);
        }
        return new Locale(nextToken, str2, stringTokenizer.nextToken());
    }

    public static String capitalize(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, " \t\r\n", true);
        StringBuffer stringBuffer = new StringBuffer(str.length());
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            stringBuffer.append(nextToken.substring(0, 1).toUpperCase());
            stringBuffer.append(nextToken.substring(1).toLowerCase());
        }
        return stringBuffer.toString();
    }

    public static boolean getYesNo(String str) {
        if (str.startsWith("\"")) {
            str = str.substring(1, str.length() - 1);
        }
        if (str.equalsIgnoreCase("n") || str.equalsIgnoreCase("no") || str.equalsIgnoreCase("f") || str.equalsIgnoreCase("false")) {
            return false;
        }
        if (str.equalsIgnoreCase("y") || str.equalsIgnoreCase("yes") || str.equalsIgnoreCase("t") || str.equalsIgnoreCase("true")) {
            return true;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Illegal boolean value: ");
        stringBuffer.append(str);
        throw new IllegalArgumentException(stringBuffer.toString());
    }

    public static String[] split(String str, char c) {
        int length = str.length();
        int i = 0;
        int i2 = 0;
        int i3 = 1;
        while (true) {
            int indexOf = str.indexOf(c, i2);
            if (indexOf == -1) {
                break;
            }
            i3++;
            i2 = indexOf + 1;
        }
        String[] strArr = new String[i3];
        int i4 = 0;
        while (i <= length) {
            int indexOf2 = str.indexOf(c, i);
            if (indexOf2 == -1) {
                indexOf2 = length;
            }
            strArr[i4] = str.substring(i, indexOf2);
            i = indexOf2 + 1;
            i4++;
        }
        return strArr;
    }

    public static String[] split(String str, String str2, boolean z) {
        String lowerCase = z ? str2.toLowerCase() : str2;
        String lowerCase2 = z ? str.toLowerCase() : str;
        int length = str.length();
        int length2 = str2.length();
        if (length2 != 0) {
            int i = 0;
            int i2 = 0;
            int i3 = 1;
            while (true) {
                int indexOf = lowerCase2.indexOf(lowerCase, i2);
                if (indexOf == -1) {
                    break;
                }
                i3++;
                i2 = indexOf + length2;
            }
            String[] strArr = new String[i3];
            int i4 = 0;
            while (i <= length) {
                int indexOf2 = lowerCase2.indexOf(lowerCase, i);
                if (indexOf2 == -1) {
                    indexOf2 = length;
                }
                strArr[i4] = str.substring(i, indexOf2);
                i = indexOf2 + length2;
                i4++;
            }
            return strArr;
        }
        throw new IllegalArgumentException("The separator string has 0 length");
    }

    public static String replace(String str, String str2, String str3) {
        return replace(str, str2, str3, false, false);
    }

    public static String replace(String str, String str2, String str3, boolean z, boolean z2) {
        int length = str2.length();
        int i = 0;
        if (length == 0) {
            int length2 = str3.length();
            if (length2 == 0) {
                return str;
            }
            if (z2) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(str3);
                stringBuffer.append(str);
                return stringBuffer.toString();
            }
            int length3 = str.length();
            StringBuffer stringBuffer2 = new StringBuffer(((length3 + 1) * length2) + length3);
            stringBuffer2.append(str3);
            while (i < length3) {
                stringBuffer2.append(str.charAt(i));
                stringBuffer2.append(str3);
                i++;
            }
            return stringBuffer2.toString();
        }
        if (z) {
            str2 = str2.toLowerCase();
        }
        String lowerCase = z ? str.toLowerCase() : str;
        int indexOf = lowerCase.indexOf(str2);
        if (indexOf == -1) {
            return str;
        }
        StringBuffer stringBuffer3 = new StringBuffer(str.length() + (Math.max(str3.length() - length, 0) * 3));
        do {
            stringBuffer3.append(str.substring(i, indexOf));
            stringBuffer3.append(str3);
            i = indexOf + length;
            indexOf = lowerCase.indexOf(str2, i);
            if (indexOf == -1) {
                break;
            }
        } while (!z2);
        stringBuffer3.append(str.substring(i));
        return stringBuffer3.toString();
    }

    public static String chomp(String str) {
        if (str.endsWith("\r\n")) {
            return str.substring(0, str.length() - 2);
        }
        if (str.endsWith("\r") || str.endsWith("\n")) {
            return str.substring(0, str.length() - 1);
        }
        return str;
    }

    public static String emptyToNull(String str) {
        if (str == null) {
            return null;
        }
        if (str.length() == 0) {
            return null;
        }
        return str;
    }

    public static String jQuote(Object obj) {
        return jQuote(obj != null ? obj.toString() : null);
    }

    public static String jQuote(String str) {
        if (str == null) {
            return "null";
        }
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length + 4);
        stringBuffer.append('\"');
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '\"') {
                stringBuffer.append("\\\"");
            } else if (charAt == '\\') {
                stringBuffer.append("\\\\");
            } else if (charAt >= ' ') {
                stringBuffer.append(charAt);
            } else if (charAt == 10) {
                stringBuffer.append("\\n");
            } else if (charAt == 13) {
                stringBuffer.append("\\r");
            } else if (charAt == 12) {
                stringBuffer.append("\\f");
            } else if (charAt == 8) {
                stringBuffer.append("\\b");
            } else if (charAt == 9) {
                stringBuffer.append("\\t");
            } else {
                stringBuffer.append("\\u00");
                stringBuffer.append(toHexDigit(charAt / 16));
                stringBuffer.append(toHexDigit(charAt & 15));
            }
        }
        stringBuffer.append('\"');
        return stringBuffer.toString();
    }

    public static String jQuoteNoXSS(Object obj) {
        return jQuoteNoXSS(obj != null ? obj.toString() : null);
    }

    public static String jQuoteNoXSS(String str) {
        if (str == null) {
            return "null";
        }
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length + 4);
        stringBuffer.append('\"');
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '\"') {
                stringBuffer.append("\\\"");
            } else if (charAt == '\\') {
                stringBuffer.append("\\\\");
            } else if (charAt == '<') {
                stringBuffer.append("\\u003C");
            } else if (charAt >= ' ') {
                stringBuffer.append(charAt);
            } else if (charAt == 10) {
                stringBuffer.append("\\n");
            } else if (charAt == 13) {
                stringBuffer.append("\\r");
            } else if (charAt == 12) {
                stringBuffer.append("\\f");
            } else if (charAt == 8) {
                stringBuffer.append("\\b");
            } else if (charAt == 9) {
                stringBuffer.append("\\t");
            } else {
                stringBuffer.append("\\u00");
                stringBuffer.append(toHexDigit(charAt / 16));
                stringBuffer.append(toHexDigit(charAt & 15));
            }
        }
        stringBuffer.append('\"');
        return stringBuffer.toString();
    }

    public static String ftlQuote(String str) {
        char c = '\"';
        if (str.indexOf(34) != -1 && str.indexOf(39) == -1) {
            c = '\'';
        }
        return FTLStringLiteralEnc(str, c, true);
    }

    public static boolean isFTLIdentifierPart(char c) {
        return isFTLIdentifierStart(c) || (c >= '0' && c <= '9');
    }

    public static String javaStringEnc(String str) {
        int length = str.length();
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt == '\"' || charAt == '\\' || charAt < ' ') {
                StringBuffer stringBuffer = new StringBuffer(length + 4);
                stringBuffer.append(str.substring(0, i));
                while (true) {
                    if (charAt == '\"') {
                        stringBuffer.append("\\\"");
                    } else if (charAt == '\\') {
                        stringBuffer.append("\\\\");
                    } else if (charAt >= ' ') {
                        stringBuffer.append(charAt);
                    } else if (charAt == 10) {
                        stringBuffer.append("\\n");
                    } else if (charAt == 13) {
                        stringBuffer.append("\\r");
                    } else if (charAt == 12) {
                        stringBuffer.append("\\f");
                    } else if (charAt == 8) {
                        stringBuffer.append("\\b");
                    } else if (charAt == 9) {
                        stringBuffer.append("\\t");
                    } else {
                        stringBuffer.append("\\u00");
                        int i2 = charAt / 16;
                        stringBuffer.append((char) (i2 < 10 ? i2 + 48 : (i2 - 10) + 97));
                        char c = charAt & 15;
                        stringBuffer.append((char) (c < 10 ? c + '0' : (c - 10) + 97));
                    }
                    i++;
                    if (i >= length) {
                        return stringBuffer.toString();
                    }
                    charAt = str.charAt(i);
                }
            } else {
                i++;
            }
        }
        return str;
    }

    public static String javaScriptStringEnc(String str) {
        return jsStringEnc(str, false);
    }

    public static String jsonStringEnc(String str) {
        return jsStringEnc(str, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0067, code lost:
        if (r15 != false) goto L_0x0069;
     */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String jsStringEnc(java.lang.String r14, boolean r15) {
        /*
            java.lang.String r0 = "s"
            freemarker.template.utility.NullArgumentException.check(r0, r14)
            int r0 = r14.length()
            r1 = 0
            r2 = 0
            r3 = r2
            r2 = 0
        L_0x000d:
            if (r2 >= r0) goto L_0x0147
            char r4 = r14.charAt(r2)
            r5 = 127(0x7f, float:1.78E-43)
            r6 = 62
            r7 = 92
            if (r4 <= r6) goto L_0x001f
            if (r4 >= r5) goto L_0x001f
            if (r4 != r7) goto L_0x013e
        L_0x001f:
            r8 = 32
            if (r4 == r8) goto L_0x013e
            r9 = 160(0xa0, float:2.24E-43)
            r10 = 8232(0x2028, float:1.1535E-41)
            if (r4 < r9) goto L_0x002b
            if (r4 < r10) goto L_0x013e
        L_0x002b:
            r9 = 31
            r11 = 3
            r12 = 1
            if (r4 > r9) goto L_0x005c
            r5 = 10
            if (r4 != r5) goto L_0x0039
            r5 = 110(0x6e, float:1.54E-43)
            goto L_0x00ce
        L_0x0039:
            r5 = 13
            if (r4 != r5) goto L_0x0041
            r5 = 114(0x72, float:1.6E-43)
            goto L_0x00ce
        L_0x0041:
            r5 = 12
            if (r4 != r5) goto L_0x0049
            r5 = 102(0x66, float:1.43E-43)
            goto L_0x00ce
        L_0x0049:
            r5 = 8
            if (r4 != r5) goto L_0x0051
            r5 = 98
            goto L_0x00ce
        L_0x0051:
            r5 = 9
            if (r4 != r5) goto L_0x0059
            r5 = 116(0x74, float:1.63E-43)
            goto L_0x00ce
        L_0x0059:
            r5 = 1
            goto L_0x00ce
        L_0x005c:
            r9 = 34
            if (r4 != r9) goto L_0x0063
        L_0x0060:
            r5 = 3
            goto L_0x00ce
        L_0x0063:
            r9 = 39
            if (r4 != r9) goto L_0x006d
            if (r15 == 0) goto L_0x006a
        L_0x0069:
            r11 = 0
        L_0x006a:
            r5 = r11
            goto L_0x00ce
        L_0x006d:
            if (r4 != r7) goto L_0x0070
            goto L_0x0060
        L_0x0070:
            r9 = 47
            r13 = 60
            if (r4 != r9) goto L_0x0081
            if (r2 == 0) goto L_0x0060
            int r9 = r2 + -1
            char r9 = r14.charAt(r9)
            if (r9 != r13) goto L_0x0081
            goto L_0x0060
        L_0x0081:
            if (r4 != r6) goto L_0x00aa
            if (r2 != 0) goto L_0x0087
        L_0x0085:
            r5 = 1
            goto L_0x00a4
        L_0x0087:
            int r5 = r2 + -1
            char r5 = r14.charAt(r5)
            r6 = 93
            if (r5 == r6) goto L_0x0098
            r6 = 45
            if (r5 != r6) goto L_0x0096
            goto L_0x0098
        L_0x0096:
            r5 = 0
            goto L_0x00a4
        L_0x0098:
            if (r2 != r12) goto L_0x009b
            goto L_0x0085
        L_0x009b:
            int r6 = r2 + -2
            char r6 = r14.charAt(r6)
            if (r6 != r5) goto L_0x0096
            goto L_0x0085
        L_0x00a4:
            if (r5 == 0) goto L_0x0069
            if (r15 == 0) goto L_0x006a
            r11 = 1
            goto L_0x006a
        L_0x00aa:
            if (r4 != r13) goto L_0x00c0
            int r5 = r0 + -1
            if (r2 != r5) goto L_0x00b1
            goto L_0x0059
        L_0x00b1:
            int r5 = r2 + 1
            char r5 = r14.charAt(r5)
            r6 = 33
            if (r5 == r6) goto L_0x0059
            r6 = 63
            if (r5 != r6) goto L_0x00cd
            goto L_0x0059
        L_0x00c0:
            if (r4 < r5) goto L_0x00c6
            r5 = 159(0x9f, float:2.23E-43)
            if (r4 <= r5) goto L_0x0059
        L_0x00c6:
            if (r4 == r10) goto L_0x0059
            r5 = 8233(0x2029, float:1.1537E-41)
            if (r4 != r5) goto L_0x00cd
            goto L_0x0059
        L_0x00cd:
            r5 = 0
        L_0x00ce:
            if (r5 == 0) goto L_0x013e
            if (r3 != 0) goto L_0x00e0
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            int r6 = r0 + 6
            r3.<init>(r6)
            java.lang.String r6 = r14.substring(r1, r2)
            r3.append(r6)
        L_0x00e0:
            r3.append(r7)
            if (r5 <= r8) goto L_0x00ea
            char r4 = (char) r5
            r3.append(r4)
            goto L_0x0143
        L_0x00ea:
            if (r5 != r12) goto L_0x013a
            if (r15 != 0) goto L_0x010a
            r5 = 256(0x100, float:3.59E-43)
            if (r4 >= r5) goto L_0x010a
            r5 = 120(0x78, float:1.68E-43)
            r3.append(r5)
            int r5 = r4 >> 4
            char r5 = toHexDigit(r5)
            r3.append(r5)
            r4 = r4 & 15
            char r4 = toHexDigit(r4)
            r3.append(r4)
            goto L_0x0143
        L_0x010a:
            r5 = 117(0x75, float:1.64E-43)
            r3.append(r5)
            int r5 = r4 >> 12
            r5 = r5 & 15
            char r5 = toHexDigit(r5)
            r3.append(r5)
            int r5 = r4 >> 8
            r5 = r5 & 15
            char r5 = toHexDigit(r5)
            r3.append(r5)
            int r5 = r4 >> 4
            r5 = r5 & 15
            char r5 = toHexDigit(r5)
            r3.append(r5)
            r4 = r4 & 15
            char r4 = toHexDigit(r4)
            r3.append(r4)
            goto L_0x0143
        L_0x013a:
            r3.append(r4)
            goto L_0x0143
        L_0x013e:
            if (r3 == 0) goto L_0x0143
            r3.append(r4)
        L_0x0143:
            int r2 = r2 + 1
            goto L_0x000d
        L_0x0147:
            if (r3 != 0) goto L_0x014a
            goto L_0x014e
        L_0x014a:
            java.lang.String r14 = r3.toString()
        L_0x014e:
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.template.utility.StringUtil.jsStringEnc(java.lang.String, boolean):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x011b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map parseNameValuePairList(java.lang.String r12, java.lang.String r13) throws java.text.ParseException {
        /*
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            int r1 = r12.length()
            r2 = 32
            r3 = 0
        L_0x000c:
            if (r3 >= r1) goto L_0x001c
            char r2 = r12.charAt(r3)
            boolean r4 = java.lang.Character.isWhitespace(r2)
            if (r4 != 0) goto L_0x0019
            goto L_0x001c
        L_0x0019:
            int r3 = r3 + 1
            goto L_0x000c
        L_0x001c:
            if (r3 != r1) goto L_0x001f
            return r0
        L_0x001f:
            r4 = r2
            r2 = r3
        L_0x0021:
            r5 = 95
            if (r2 >= r1) goto L_0x0035
            char r4 = r12.charAt(r2)
            boolean r6 = java.lang.Character.isLetterOrDigit(r4)
            if (r6 != 0) goto L_0x0032
            if (r4 == r5) goto L_0x0032
            goto L_0x0035
        L_0x0032:
            int r2 = r2 + 1
            goto L_0x0021
        L_0x0035:
            java.lang.String r6 = " at position "
            java.lang.String r7 = "."
            if (r3 == r2) goto L_0x0178
            java.lang.String r8 = r12.substring(r3, r2)
        L_0x003f:
            if (r2 >= r1) goto L_0x004f
            char r4 = r12.charAt(r2)
            boolean r9 = java.lang.Character.isWhitespace(r4)
            if (r9 != 0) goto L_0x004c
            goto L_0x004f
        L_0x004c:
            int r2 = r2 + 1
            goto L_0x003f
        L_0x004f:
            if (r2 != r1) goto L_0x0071
            if (r13 == 0) goto L_0x0057
        L_0x0053:
            r5 = r2
            r2 = r13
            goto L_0x0111
        L_0x0057:
            java.text.ParseException r12 = new java.text.ParseException
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            java.lang.String r0 = "Expecting \":\", but reached the end of the string  at position "
            r13.append(r0)
            r13.append(r2)
            r13.append(r7)
            java.lang.String r13 = r13.toString()
            r12.<init>(r13, r2)
            throw r12
        L_0x0071:
            r9 = 58
            r10 = 44
            if (r4 == r9) goto L_0x00a6
            if (r13 == 0) goto L_0x007e
            if (r4 != r10) goto L_0x007e
            int r2 = r2 + 1
            goto L_0x0053
        L_0x007e:
            java.text.ParseException r12 = new java.text.ParseException
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            java.lang.String r0 = "Expecting \":\" here, but found "
            r13.append(r0)
            java.lang.String r0 = java.lang.String.valueOf(r4)
            java.lang.String r0 = jQuote(r0)
            r13.append(r0)
            r13.append(r6)
            r13.append(r2)
            r13.append(r7)
            java.lang.String r13 = r13.toString()
            r12.<init>(r13, r2)
            throw r12
        L_0x00a6:
            int r2 = r2 + 1
            if (r2 >= r1) goto L_0x00b4
            char r4 = r12.charAt(r2)
            boolean r9 = java.lang.Character.isWhitespace(r4)
            if (r9 != 0) goto L_0x00a6
        L_0x00b4:
            if (r2 == r1) goto L_0x015e
            r9 = r4
            r4 = r2
        L_0x00b8:
            if (r4 >= r1) goto L_0x00ca
            char r9 = r12.charAt(r4)
            boolean r11 = java.lang.Character.isLetterOrDigit(r9)
            if (r11 != 0) goto L_0x00c7
            if (r9 == r5) goto L_0x00c7
            goto L_0x00ca
        L_0x00c7:
            int r4 = r4 + 1
            goto L_0x00b8
        L_0x00ca:
            if (r2 == r4) goto L_0x0136
            java.lang.String r2 = r12.substring(r2, r4)
        L_0x00d0:
            if (r4 >= r1) goto L_0x00e0
            char r9 = r12.charAt(r4)
            boolean r5 = java.lang.Character.isWhitespace(r9)
            if (r5 != 0) goto L_0x00dd
            goto L_0x00e0
        L_0x00dd:
            int r4 = r4 + 1
            goto L_0x00d0
        L_0x00e0:
            if (r4 >= r1) goto L_0x010f
            if (r9 != r10) goto L_0x00e7
            int r4 = r4 + 1
            goto L_0x010f
        L_0x00e7:
            java.text.ParseException r12 = new java.text.ParseException
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            java.lang.String r0 = "Excpecting \",\" or the end of the string here, but found "
            r13.append(r0)
            java.lang.String r0 = java.lang.String.valueOf(r9)
            java.lang.String r0 = jQuote(r0)
            r13.append(r0)
            r13.append(r6)
            r13.append(r4)
            r13.append(r7)
            java.lang.String r13 = r13.toString()
            r12.<init>(r13, r4)
            throw r12
        L_0x010f:
            r5 = r4
            r4 = r9
        L_0x0111:
            java.lang.Object r2 = r0.put(r8, r2)
            if (r2 != 0) goto L_0x011b
            r2 = r4
            r3 = r5
            goto L_0x000c
        L_0x011b:
            java.text.ParseException r12 = new java.text.ParseException
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            java.lang.String r0 = "Dublicated key: "
            r13.append(r0)
            java.lang.String r0 = jQuote(r8)
            r13.append(r0)
            java.lang.String r13 = r13.toString()
            r12.<init>(r13, r3)
            throw r12
        L_0x0136:
            java.text.ParseException r12 = new java.text.ParseException
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            java.lang.String r0 = "Expecting letter, digit or \"_\" here, (the first character of the value) but found "
            r13.append(r0)
            java.lang.String r0 = java.lang.String.valueOf(r9)
            java.lang.String r0 = jQuote(r0)
            r13.append(r0)
            r13.append(r6)
            r13.append(r4)
            r13.append(r7)
            java.lang.String r13 = r13.toString()
            r12.<init>(r13, r4)
            throw r12
        L_0x015e:
            java.text.ParseException r12 = new java.text.ParseException
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            java.lang.String r0 = "Expecting the value of the key here, but reached the end of the string  at position "
            r13.append(r0)
            r13.append(r2)
            r13.append(r7)
            java.lang.String r13 = r13.toString()
            r12.<init>(r13, r2)
            throw r12
        L_0x0178:
            java.text.ParseException r12 = new java.text.ParseException
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            java.lang.String r0 = "Expecting letter, digit or \"_\" here, (the first character of the key) but found "
            r13.append(r0)
            java.lang.String r0 = java.lang.String.valueOf(r4)
            java.lang.String r0 = jQuote(r0)
            r13.append(r0)
            r13.append(r6)
            r13.append(r2)
            r13.append(r7)
            java.lang.String r13 = r13.toString()
            r12.<init>(r13, r2)
            goto L_0x01a1
        L_0x01a0:
            throw r12
        L_0x01a1:
            goto L_0x01a0
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.template.utility.StringUtil.parseNameValuePairList(java.lang.String, java.lang.String):java.util.Map");
    }

    public static boolean isXMLID(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (i == 0 && (charAt == '-' || charAt == '.' || Character.isDigit(charAt))) {
                return false;
            }
            if (!Character.isLetterOrDigit(charAt) && charAt != ':' && charAt != '_' && charAt != '-' && charAt != '.') {
                return false;
            }
        }
        return true;
    }

    public static boolean matchesName(String str, String str2, String str3, Environment environment) {
        String defaultNS = environment.getDefaultNS();
        if (defaultNS == null || !defaultNS.equals(str3)) {
            if (!"".equals(str3)) {
                String prefixForNamespace = environment.getPrefixForNamespace(str3);
                if (prefixForNamespace == null) {
                    return false;
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(prefixForNamespace);
                stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
                stringBuffer.append(str2);
                return str.equals(stringBuffer.toString());
            } else if (defaultNS != null) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("N:");
                stringBuffer2.append(str2);
                return str.equals(stringBuffer2.toString());
            } else if (str.equals(str2)) {
                return true;
            } else {
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("N:");
                stringBuffer3.append(str2);
                if (str.equals(stringBuffer3.toString())) {
                    return true;
                }
                return false;
            }
        } else if (str.equals(str2)) {
            return true;
        } else {
            StringBuffer stringBuffer4 = new StringBuffer();
            stringBuffer4.append("D:");
            stringBuffer4.append(str2);
            if (str.equals(stringBuffer4.toString())) {
                return true;
            }
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.utility.StringUtil.leftPad(java.lang.String, int, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      freemarker.template.utility.StringUtil.leftPad(java.lang.String, int, java.lang.String):java.lang.String
      freemarker.template.utility.StringUtil.leftPad(java.lang.String, int, char):java.lang.String */
    public static String leftPad(String str, int i) {
        return leftPad(str, i, ' ');
    }

    public static String leftPad(String str, int i, char c) {
        int length = str.length();
        if (i <= length) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer(i);
        int i2 = i - length;
        for (int i3 = 0; i3 < i2; i3++) {
            stringBuffer.append(c);
        }
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    public static String leftPad(String str, int i, String str2) {
        int length = str.length();
        if (i <= length) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer(i);
        int i2 = i - length;
        int length2 = str2.length();
        if (length2 != 0) {
            int i3 = i2 / length2;
            for (int i4 = 0; i4 < i3; i4++) {
                stringBuffer.append(str2);
            }
            int i5 = i2 % length2;
            for (int i6 = 0; i6 < i5; i6++) {
                stringBuffer.append(str2.charAt(i6));
            }
            stringBuffer.append(str);
            return stringBuffer.toString();
        }
        throw new IllegalArgumentException("The \"filling\" argument can't be 0 length string.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.utility.StringUtil.rightPad(java.lang.String, int, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      freemarker.template.utility.StringUtil.rightPad(java.lang.String, int, java.lang.String):java.lang.String
      freemarker.template.utility.StringUtil.rightPad(java.lang.String, int, char):java.lang.String */
    public static String rightPad(String str, int i) {
        return rightPad(str, i, ' ');
    }

    public static String rightPad(String str, int i, char c) {
        int length = str.length();
        if (i <= length) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer(i);
        stringBuffer.append(str);
        int i2 = i - length;
        for (int i3 = 0; i3 < i2; i3++) {
            stringBuffer.append(c);
        }
        return stringBuffer.toString();
    }

    public static String rightPad(String str, int i, String str2) {
        int length = str.length();
        if (i <= length) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer(i);
        stringBuffer.append(str);
        int i2 = i - length;
        int length2 = str2.length();
        if (length2 != 0) {
            int i3 = length % length2;
            int i4 = length2 - i3 <= i2 ? length2 : i3 + i2;
            for (int i5 = i3; i5 < i4; i5++) {
                stringBuffer.append(str2.charAt(i5));
            }
            int i6 = i2 - (i4 - i3);
            int i7 = i6 / length2;
            for (int i8 = 0; i8 < i7; i8++) {
                stringBuffer.append(str2);
            }
            int i9 = i6 % length2;
            for (int i10 = 0; i10 < i9; i10++) {
                stringBuffer.append(str2.charAt(i10));
            }
            return stringBuffer.toString();
        }
        throw new IllegalArgumentException("The \"filling\" argument can't be 0 length string.");
    }

    public static int versionStringToInt(String str) {
        return new Version(str).intValue();
    }

    public static String tryToString(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj.toString();
        } catch (Throwable th) {
            return failedToStringSubstitute(obj, th);
        }
    }

    private static String failedToStringSubstitute(Object obj, Throwable th) {
        String str;
        try {
            str = th.toString();
        } catch (Throwable unused) {
            str = ClassUtil.getShortClassNameOfObject(th);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        stringBuffer.append(ClassUtil.getShortClassNameOfObject(obj));
        stringBuffer.append(".toString() failed: ");
        stringBuffer.append(str);
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    public static String toUpperABC(int i) {
        return toABC(i, 'A');
    }

    public static String toLowerABC(int i) {
        return toABC(i, 'a');
    }

    private static String toABC(int i, char c) {
        int i2 = 1;
        if (i >= 1) {
            int i3 = 1;
            while (true) {
                int i4 = i2 * 26;
                int i5 = i3 + i4;
                if (i5 > i) {
                    break;
                }
                i2 = i4;
                i3 = i5;
            }
            StringBuffer stringBuffer = new StringBuffer();
            while (i2 != 0) {
                int i6 = (i - i3) / i2;
                stringBuffer.append((char) (c + i6));
                i3 += i6 * i2;
                i2 /= 26;
            }
            return stringBuffer.toString();
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("Can't convert 0 or negative numbers to latin-number: ");
        stringBuffer2.append(i);
        throw new IllegalArgumentException(stringBuffer2.toString());
    }

    public static char[] trim(char[] cArr) {
        if (cArr.length == 0) {
            return cArr;
        }
        int length = cArr.length;
        int i = 0;
        while (i < length && cArr[i] <= ' ') {
            i++;
        }
        while (i < length && cArr[length - 1] <= ' ') {
            length--;
        }
        if (i == 0 && length == cArr.length) {
            return cArr;
        }
        if (i == length) {
            return CollectionUtils.EMPTY_CHAR_ARRAY;
        }
        int i2 = length - i;
        char[] cArr2 = new char[i2];
        System.arraycopy(cArr, i, cArr2, 0, i2);
        return cArr2;
    }

    public static boolean isTrimmableToEmpty(char[] cArr) {
        return isTrimmableToEmpty(cArr, 0, cArr.length);
    }

    public static boolean isTrimmableToEmpty(char[] cArr, int i) {
        return isTrimmableToEmpty(cArr, i, cArr.length);
    }

    public static boolean isTrimmableToEmpty(char[] cArr, int i, int i2) {
        while (i < i2) {
            if (cArr[i] > ' ') {
                return false;
            }
            i++;
        }
        return true;
    }
}
