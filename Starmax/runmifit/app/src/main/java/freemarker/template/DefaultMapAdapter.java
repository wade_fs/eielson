package freemarker.template;

import freemarker.core._DelayedJQuote;
import freemarker.core._TemplateModelException;
import freemarker.ext.util.WrapperTemplateModel;
import freemarker.template.utility.ObjectWrapperWithAPISupport;
import java.io.Serializable;
import java.util.Map;
import java.util.SortedMap;

public class DefaultMapAdapter extends WrappingTemplateModel implements TemplateHashModelEx, AdapterTemplateModel, WrapperTemplateModel, TemplateModelWithAPISupport, Serializable {
    private final Map map;

    public static DefaultMapAdapter adapt(Map map2, ObjectWrapperWithAPISupport objectWrapperWithAPISupport) {
        return new DefaultMapAdapter(map2, objectWrapperWithAPISupport);
    }

    private DefaultMapAdapter(Map map2, ObjectWrapper objectWrapper) {
        super(objectWrapper);
        this.map = map2;
    }

    public TemplateModel get(String str) throws TemplateModelException {
        try {
            Object obj = this.map.get(str);
            if (obj == null) {
                if (str.length() != 1 || (this.map instanceof SortedMap)) {
                    TemplateModel wrap = wrap(null);
                    if (wrap == null || !this.map.containsKey(str)) {
                        return null;
                    }
                    return wrap;
                }
                Character ch = new Character(str.charAt(0));
                try {
                    Object obj2 = this.map.get(ch);
                    if (obj2 == null) {
                        TemplateModel wrap2 = wrap(null);
                        if (wrap2 == null || (!this.map.containsKey(str) && !this.map.containsKey(ch))) {
                            return null;
                        }
                        return wrap2;
                    }
                    obj = obj2;
                } catch (ClassCastException e) {
                    throw new _TemplateModelException(e, new Object[]{"Class casting exception while getting Map entry with Character key ", new _DelayedJQuote(ch)});
                } catch (NullPointerException e2) {
                    throw new _TemplateModelException(e2, new Object[]{"NullPointerException while getting Map entry with Character key ", new _DelayedJQuote(ch)});
                }
            }
            return wrap(obj);
        } catch (ClassCastException e3) {
            throw new _TemplateModelException(e3, new Object[]{"ClassCastException while getting Map entry with String key ", new _DelayedJQuote(str)});
        } catch (NullPointerException e4) {
            throw new _TemplateModelException(e4, new Object[]{"NullPointerException while getting Map entry with String key ", new _DelayedJQuote(str)});
        }
    }

    public boolean isEmpty() {
        return this.map.isEmpty();
    }

    public int size() {
        return this.map.size();
    }

    public TemplateCollectionModel keys() {
        return new SimpleCollection(this.map.keySet(), getObjectWrapper());
    }

    public TemplateCollectionModel values() {
        return new SimpleCollection(this.map.values(), getObjectWrapper());
    }

    public Object getAdaptedObject(Class cls) {
        return this.map;
    }

    public Object getWrappedObject() {
        return this.map;
    }

    public TemplateModel getAPI() throws TemplateModelException {
        return ((ObjectWrapperWithAPISupport) getObjectWrapper()).wrapAsAPI(this.map);
    }
}
