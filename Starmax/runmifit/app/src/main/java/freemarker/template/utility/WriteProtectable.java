package freemarker.template.utility;

public interface WriteProtectable {
    boolean isWriteProtected();

    void writeProtect();
}
