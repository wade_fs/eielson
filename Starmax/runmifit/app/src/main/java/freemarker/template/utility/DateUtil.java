package freemarker.template.utility;

import com.tamic.novate.util.FileUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtil {
    public static final int ACCURACY_HOURS = 4;
    public static final int ACCURACY_MILLISECONDS = 7;
    public static final int ACCURACY_MILLISECONDS_FORCED = 8;
    public static final int ACCURACY_MINUTES = 5;
    public static final int ACCURACY_SECONDS = 6;
    private static final String MSG_YEAR_0_NOT_ALLOWED = "Year 0 is not allowed in XML schema dates. BC 1 is -1, AD 1 is 1.";
    private static final Pattern PATTERN_ISO8601_BASIC_DATE = Pattern.compile(REGEX_ISO8601_BASIC_DATE_BASE);
    private static final Pattern PATTERN_ISO8601_BASIC_DATE_TIME = Pattern.compile("(-?[0-9]{4,}?)([0-9]{2})([0-9]{2})T([0-9]{2})(?:([0-9]{2})(?:([0-9]{2})(?:[\\.,]([0-9]+))?)?)?(Z|(?:[-+][0-9]{2}(?:[0-9]{2})?))?");
    private static final Pattern PATTERN_ISO8601_BASIC_TIME = Pattern.compile("([0-9]{2})(?:([0-9]{2})(?:([0-9]{2})(?:[\\.,]([0-9]+))?)?)?(Z|(?:[-+][0-9]{2}(?:[0-9]{2})?))?");
    private static final Pattern PATTERN_ISO8601_EXTENDED_DATE = Pattern.compile(REGEX_ISO8601_EXTENDED_DATE_BASE);
    private static final Pattern PATTERN_ISO8601_EXTENDED_DATE_TIME = Pattern.compile("(-?[0-9]{4,})-([0-9]{2})-([0-9]{2})T([0-9]{2})(?::([0-9]{2})(?::([0-9]{2})(?:[\\.,]([0-9]+))?)?)?(Z|(?:[-+][0-9]{2}(?::[0-9]{2})?))?");
    private static final Pattern PATTERN_ISO8601_EXTENDED_TIME = Pattern.compile("([0-9]{2})(?::([0-9]{2})(?::([0-9]{2})(?:[\\.,]([0-9]+))?)?)?(Z|(?:[-+][0-9]{2}(?::[0-9]{2})?))?");
    private static final Pattern PATTERN_XS_DATE = Pattern.compile("(-?[0-9]+)-([0-9]{2})-([0-9]{2})(Z|(?:[-+][0-9]{2}:[0-9]{2}))?");
    private static final Pattern PATTERN_XS_DATE_TIME = Pattern.compile("(-?[0-9]+)-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})(?:\\.([0-9]+))?(Z|(?:[-+][0-9]{2}:[0-9]{2}))?");
    private static final Pattern PATTERN_XS_TIME = Pattern.compile("([0-9]{2}):([0-9]{2}):([0-9]{2})(?:\\.([0-9]+))?(Z|(?:[-+][0-9]{2}:[0-9]{2}))?");
    private static final Pattern PATTERN_XS_TIME_ZONE = Pattern.compile(REGEX_XS_TIME_ZONE);
    private static final String REGEX_ISO8601_BASIC_DATE_BASE = "(-?[0-9]{4,}?)([0-9]{2})([0-9]{2})";
    private static final String REGEX_ISO8601_BASIC_OPTIONAL_TIME_ZONE = "(Z|(?:[-+][0-9]{2}(?:[0-9]{2})?))?";
    private static final String REGEX_ISO8601_BASIC_TIME_BASE = "([0-9]{2})(?:([0-9]{2})(?:([0-9]{2})(?:[\\.,]([0-9]+))?)?)?";
    private static final String REGEX_ISO8601_BASIC_TIME_ZONE = "Z|(?:[-+][0-9]{2}(?:[0-9]{2})?)";
    private static final String REGEX_ISO8601_EXTENDED_DATE_BASE = "(-?[0-9]{4,})-([0-9]{2})-([0-9]{2})";
    private static final String REGEX_ISO8601_EXTENDED_OPTIONAL_TIME_ZONE = "(Z|(?:[-+][0-9]{2}(?::[0-9]{2})?))?";
    private static final String REGEX_ISO8601_EXTENDED_TIME_BASE = "([0-9]{2})(?::([0-9]{2})(?::([0-9]{2})(?:[\\.,]([0-9]+))?)?)?";
    private static final String REGEX_ISO8601_EXTENDED_TIME_ZONE = "Z|(?:[-+][0-9]{2}(?::[0-9]{2})?)";
    private static final String REGEX_XS_DATE_BASE = "(-?[0-9]+)-([0-9]{2})-([0-9]{2})";
    private static final String REGEX_XS_OPTIONAL_TIME_ZONE = "(Z|(?:[-+][0-9]{2}:[0-9]{2}))?";
    private static final String REGEX_XS_TIME_BASE = "([0-9]{2}):([0-9]{2}):([0-9]{2})(?:\\.([0-9]+))?";
    private static final String REGEX_XS_TIME_ZONE = "Z|(?:[-+][0-9]{2}:[0-9]{2})";
    public static final TimeZone UTC = TimeZone.getTimeZone("UTC");

    public interface CalendarFieldsToDateConverter {
        Date calculate(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z, TimeZone timeZone);
    }

    public interface DateToISO8601CalendarFactory {
        GregorianCalendar get(TimeZone timeZone, Date date);
    }

    private DateUtil() {
    }

    public static TimeZone getTimeZone(String str) throws UnrecognizedTimeZoneException {
        if (!isGMTish(str)) {
            TimeZone timeZone = TimeZone.getTimeZone(str);
            if (!isGMTish(timeZone.getID())) {
                return timeZone;
            }
            throw new UnrecognizedTimeZoneException(str);
        } else if (str.equalsIgnoreCase("UTC")) {
            return UTC;
        } else {
            return TimeZone.getTimeZone(str);
        }
    }

    private static boolean isGMTish(String str) {
        if (str.length() < 3) {
            return false;
        }
        char charAt = str.charAt(0);
        char charAt2 = str.charAt(1);
        char charAt3 = str.charAt(2);
        if (((charAt != 'G' && charAt != 'g') || ((charAt2 != 'M' && charAt2 != 'm') || (charAt3 != 'T' && charAt3 != 't'))) && (((charAt != 'U' && charAt != 'u') || ((charAt2 != 'T' && charAt2 != 't') || (charAt3 != 'C' && charAt3 != 'c'))) && ((charAt != 'U' && charAt != 'u') || ((charAt2 != 'T' && charAt2 != 't') || charAt3 != '1')))) {
            return false;
        }
        if (str.length() == 3) {
            return true;
        }
        String substring = str.substring(3);
        if (substring.startsWith("+")) {
            if (substring.equals("+0") || substring.equals("+00") || substring.equals("+00:00")) {
                return true;
            }
            return false;
        } else if (substring.equals("-0") || substring.equals("-00") || substring.equals("-00:00")) {
            return true;
        } else {
            return false;
        }
    }

    public static String dateToISO8601String(Date date, boolean z, boolean z2, boolean z3, int i, TimeZone timeZone, DateToISO8601CalendarFactory dateToISO8601CalendarFactory) {
        return dateToString(date, z, z2, z3, i, timeZone, false, dateToISO8601CalendarFactory);
    }

    public static String dateToXSString(Date date, boolean z, boolean z2, boolean z3, int i, TimeZone timeZone, DateToISO8601CalendarFactory dateToISO8601CalendarFactory) {
        return dateToString(date, z, z2, z3, i, timeZone, true, dateToISO8601CalendarFactory);
    }

    private static String dateToString(Date date, boolean z, boolean z2, boolean z3, int i, TimeZone timeZone, boolean z4, DateToISO8601CalendarFactory dateToISO8601CalendarFactory) {
        TimeZone timeZone2;
        DateToISO8601CalendarFactory dateToISO8601CalendarFactory2;
        Date date2;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = i;
        if (z4 || z2 || !z3) {
            if (timeZone == null) {
                dateToISO8601CalendarFactory2 = dateToISO8601CalendarFactory;
                timeZone2 = UTC;
                date2 = date;
            } else {
                date2 = date;
                timeZone2 = timeZone;
                dateToISO8601CalendarFactory2 = dateToISO8601CalendarFactory;
            }
            GregorianCalendar gregorianCalendar = dateToISO8601CalendarFactory2.get(timeZone2, date2);
            if (!z2) {
                i2 = (z4 ? 6 : 0) + 10;
            } else {
                i2 = !z ? 18 : 29;
            }
            char[] cArr = new char[i2];
            int i7 = 3;
            char c = '-';
            boolean z5 = true;
            if (z) {
                int i8 = gregorianCalendar.get(1);
                if (i8 > 0 && gregorianCalendar.get(0) == 0) {
                    i8 = (-i8) + (z4 ^ true ? 1 : 0);
                }
                int i9 = 4;
                if (i8 < 0 || i8 >= 9999) {
                    String valueOf = String.valueOf(i8);
                    char[] cArr2 = new char[((i2 - 4) + valueOf.length())];
                    int i10 = 0;
                    i9 = 0;
                    while (i10 < valueOf.length()) {
                        cArr2[i9] = valueOf.charAt(i10);
                        i10++;
                        i9++;
                    }
                    cArr = cArr2;
                } else {
                    cArr[0] = (char) ((i8 / 1000) + 48);
                    cArr[1] = (char) (((i8 % 1000) / 100) + 48);
                    cArr[2] = (char) (((i8 % 100) / 10) + 48);
                    cArr[3] = (char) ((i8 % 10) + 48);
                }
                cArr[i9] = '-';
                int append00 = append00(cArr, i9 + 1, gregorianCalendar.get(2) + 1);
                cArr[append00] = '-';
                int append002 = append00(cArr, append00 + 1, gregorianCalendar.get(5));
                if (z2) {
                    i3 = append002 + 1;
                    cArr[append002] = 'T';
                } else {
                    i3 = append002;
                }
            } else {
                i3 = 0;
            }
            if (z2) {
                i3 = append00(cArr, i3, gregorianCalendar.get(11));
                if (i6 >= 5) {
                    cArr[i3] = ':';
                    i3 = append00(cArr, i3 + 1, gregorianCalendar.get(12));
                    if (i6 >= 6) {
                        cArr[i3] = ':';
                        i3 = append00(cArr, i3 + 1, gregorianCalendar.get(13));
                        if (i6 >= 7) {
                            int i11 = gregorianCalendar.get(14);
                            if (i6 != 8) {
                                i7 = 0;
                            }
                            if (!(i11 == 0 && i7 == 0)) {
                                if (i11 <= 999) {
                                    int i12 = i3 + 1;
                                    cArr[i3] = '.';
                                    while (true) {
                                        i5 = i12 + 1;
                                        cArr[i12] = (char) ((i11 / 100) + 48);
                                        i7--;
                                        i11 = (i11 % 100) * 10;
                                        if (i11 == 0 && i7 <= 0) {
                                            break;
                                        }
                                        i12 = i5;
                                    }
                                    i3 = i5;
                                } else {
                                    throw new RuntimeException("Calendar.MILLISECOND > 999");
                                }
                            }
                        }
                    }
                }
            }
            if (z3) {
                if (timeZone2 == UTC) {
                    i4 = i3 + 1;
                    cArr[i3] = 'Z';
                } else {
                    int offset = timeZone2.getOffset(date.getTime());
                    if (offset < 0) {
                        offset = -offset;
                        z5 = false;
                    }
                    int i13 = offset / 1000;
                    int i14 = i13 % 60;
                    int i15 = i13 / 60;
                    int i16 = i15 % 60;
                    int i17 = i15 / 60;
                    if (i14 == 0 && i16 == 0 && i17 == 0) {
                        i4 = i3 + 1;
                        cArr[i3] = 'Z';
                    } else {
                        int i18 = i3 + 1;
                        if (z5) {
                            c = '+';
                        }
                        cArr[i3] = c;
                        int append003 = append00(cArr, i18, i17);
                        cArr[append003] = ':';
                        i3 = append00(cArr, append003 + 1, i16);
                        if (i14 != 0) {
                            cArr[i3] = ':';
                            i3 = append00(cArr, i3 + 1, i14);
                        }
                    }
                }
                i3 = i4;
            }
            return new String(cArr, 0, i3);
        }
        throw new IllegalArgumentException("ISO 8601:2004 doesn't specify any formats where the offset is shown but the time isn't.");
    }

    private static int append00(char[] cArr, int i, int i2) {
        int i3 = i + 1;
        cArr[i] = (char) ((i2 / 10) + 48);
        int i4 = i3 + 1;
        cArr[i3] = (char) ((i2 % 10) + 48);
        return i4;
    }

    public static Date parseXSDate(String str, TimeZone timeZone, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        Matcher matcher = PATTERN_XS_DATE.matcher(str);
        if (matcher.matches()) {
            return parseDate_parseMatcher(matcher, timeZone, true, calendarFieldsToDateConverter);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("The value didn't match the expected pattern: ");
        stringBuffer.append(PATTERN_XS_DATE);
        throw new DateParseException(stringBuffer.toString());
    }

    public static Date parseISO8601Date(String str, TimeZone timeZone, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        Matcher matcher = PATTERN_ISO8601_EXTENDED_DATE.matcher(str);
        if (!matcher.matches()) {
            matcher = PATTERN_ISO8601_BASIC_DATE.matcher(str);
            if (!matcher.matches()) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("The value didn't match the expected pattern: ");
                stringBuffer.append(PATTERN_ISO8601_EXTENDED_DATE);
                stringBuffer.append(" or ");
                stringBuffer.append(PATTERN_ISO8601_BASIC_DATE);
                throw new DateParseException(stringBuffer.toString());
            }
        }
        return parseDate_parseMatcher(matcher, timeZone, false, calendarFieldsToDateConverter);
    }

    private static Date parseDate_parseMatcher(Matcher matcher, TimeZone timeZone, boolean z, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        int i;
        int i2;
        NullArgumentException.check("defaultTZ", timeZone);
        try {
            int groupToInt = groupToInt(matcher.group(1), "year", Integer.MIN_VALUE, Integer.MAX_VALUE);
            if (groupToInt <= 0) {
                int i3 = (-groupToInt) + (z ^ true ? 1 : 0);
                if (i3 != 0) {
                    i = i3;
                    i2 = 0;
                } else {
                    throw new DateParseException(MSG_YEAR_0_NOT_ALLOWED);
                }
            } else {
                i = groupToInt;
                i2 = 1;
            }
            int groupToInt2 = groupToInt(matcher.group(2), "month", 1, 12) - 1;
            int groupToInt3 = groupToInt(matcher.group(3), "day-of-month", 1, 31);
            if (z) {
                timeZone = parseMatchingTimeZone(matcher.group(4), timeZone);
            }
            return calendarFieldsToDateConverter.calculate(i2, i, groupToInt2, groupToInt3, 0, 0, 0, 0, false, timeZone);
        } catch (IllegalArgumentException unused) {
            throw new DateParseException("Date calculation faliure. Probably the date is formally correct, but refers to an unexistent date (like February 30).");
        }
    }

    public static Date parseXSTime(String str, TimeZone timeZone, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        Matcher matcher = PATTERN_XS_TIME.matcher(str);
        if (matcher.matches()) {
            return parseTime_parseMatcher(matcher, timeZone, calendarFieldsToDateConverter);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("The value didn't match the expected pattern: ");
        stringBuffer.append(PATTERN_XS_TIME);
        throw new DateParseException(stringBuffer.toString());
    }

    public static Date parseISO8601Time(String str, TimeZone timeZone, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        Matcher matcher = PATTERN_ISO8601_EXTENDED_TIME.matcher(str);
        if (!matcher.matches()) {
            matcher = PATTERN_ISO8601_BASIC_TIME.matcher(str);
            if (!matcher.matches()) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("The value didn't match the expected pattern: ");
                stringBuffer.append(PATTERN_ISO8601_EXTENDED_TIME);
                stringBuffer.append(" or ");
                stringBuffer.append(PATTERN_ISO8601_BASIC_TIME);
                throw new DateParseException(stringBuffer.toString());
            }
        }
        return parseTime_parseMatcher(matcher, timeZone, calendarFieldsToDateConverter);
    }

    private static Date parseTime_parseMatcher(Matcher matcher, TimeZone timeZone, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        int i;
        boolean z;
        int i2;
        Matcher matcher2 = matcher;
        TimeZone timeZone2 = timeZone;
        NullArgumentException.check("defaultTZ", timeZone2);
        try {
            int groupToInt = groupToInt(matcher2.group(1), "hour-of-day", 0, 24);
            if (groupToInt == 24) {
                z = true;
                i = 0;
            } else {
                i = groupToInt;
                z = false;
            }
            String group = matcher2.group(2);
            int groupToInt2 = group != null ? groupToInt(group, "minute", 0, 59) : 0;
            String group2 = matcher2.group(3);
            int groupToInt3 = group2 != null ? groupToInt(group2, "second", 0, 60) : 0;
            int groupToMillisecond = groupToMillisecond(matcher2.group(4));
            TimeZone parseMatchingTimeZone = parseMatchingTimeZone(matcher2.group(5), timeZone2);
            if (!z) {
                i2 = 1;
            } else if (groupToInt2 == 0 && groupToInt3 == 0 && groupToMillisecond == 0) {
                i2 = 2;
            } else {
                throw new DateParseException("Hour 24 is only allowed in the case of midnight.");
            }
            return calendarFieldsToDateConverter.calculate(1, 1970, 0, i2, i, groupToInt2, groupToInt3, groupToMillisecond, false, parseMatchingTimeZone);
        } catch (IllegalArgumentException unused) {
            throw new DateParseException("Unexpected time calculation faliure.");
        }
    }

    public static Date parseXSDateTime(String str, TimeZone timeZone, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        Matcher matcher = PATTERN_XS_DATE_TIME.matcher(str);
        if (matcher.matches()) {
            return parseDateTime_parseMatcher(matcher, timeZone, true, calendarFieldsToDateConverter);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("The value didn't match the expected pattern: ");
        stringBuffer.append(PATTERN_XS_DATE_TIME);
        throw new DateParseException(stringBuffer.toString());
    }

    public static Date parseISO8601DateTime(String str, TimeZone timeZone, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        Matcher matcher = PATTERN_ISO8601_EXTENDED_DATE_TIME.matcher(str);
        if (!matcher.matches()) {
            matcher = PATTERN_ISO8601_BASIC_DATE_TIME.matcher(str);
            if (!matcher.matches()) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("The value (");
                stringBuffer.append(str);
                stringBuffer.append(") didn't match the expected pattern: ");
                stringBuffer.append(PATTERN_ISO8601_EXTENDED_DATE_TIME);
                stringBuffer.append(" or ");
                stringBuffer.append(PATTERN_ISO8601_BASIC_DATE_TIME);
                throw new DateParseException(stringBuffer.toString());
            }
        }
        return parseDateTime_parseMatcher(matcher, timeZone, false, calendarFieldsToDateConverter);
    }

    private static Date parseDateTime_parseMatcher(Matcher matcher, TimeZone timeZone, boolean z, CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateParseException {
        int i;
        int i2;
        boolean z2;
        NullArgumentException.check("defaultTZ", timeZone);
        try {
            int groupToInt = groupToInt(matcher.group(1), "year", Integer.MIN_VALUE, Integer.MAX_VALUE);
            if (groupToInt <= 0) {
                int i3 = (-groupToInt) + (z ^ true ? 1 : 0);
                if (i3 != 0) {
                    i = i3;
                    i2 = 0;
                } else {
                    throw new DateParseException(MSG_YEAR_0_NOT_ALLOWED);
                }
            } else {
                i = groupToInt;
                i2 = 1;
            }
            int groupToInt2 = groupToInt(matcher.group(2), "month", 1, 12) - 1;
            int groupToInt3 = groupToInt(matcher.group(3), "day-of-month", 1, 31);
            int groupToInt4 = groupToInt(matcher.group(4), "hour-of-day", 0, 24);
            if (groupToInt4 == 24) {
                groupToInt4 = 0;
                z2 = true;
            } else {
                z2 = false;
            }
            String group = matcher.group(5);
            int groupToInt5 = group != null ? groupToInt(group, "minute", 0, 59) : 0;
            String group2 = matcher.group(6);
            int groupToInt6 = group2 != null ? groupToInt(group2, "second", 0, 60) : 0;
            int groupToMillisecond = groupToMillisecond(matcher.group(7));
            TimeZone parseMatchingTimeZone = parseMatchingTimeZone(matcher.group(8), timeZone);
            if (z2) {
                if (groupToInt5 != 0 || groupToInt6 != 0 || groupToMillisecond != 0) {
                    throw new DateParseException("Hour 24 is only allowed in the case of midnight.");
                }
            }
            return calendarFieldsToDateConverter.calculate(i2, i, groupToInt2, groupToInt3, groupToInt4, groupToInt5, groupToInt6, groupToMillisecond, z2, parseMatchingTimeZone);
        } catch (IllegalArgumentException unused) {
            throw new DateParseException("Date-time calculation faliure. Probably the date-time is formally correct, but refers to an unexistent date-time (like February 30).");
        }
    }

    public static TimeZone parseXSTimeZone(String str) throws DateParseException {
        if (PATTERN_XS_TIME_ZONE.matcher(str).matches()) {
            return parseMatchingTimeZone(str, null);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("The time zone offset didn't match the expected pattern: ");
        stringBuffer.append(PATTERN_XS_TIME_ZONE);
        throw new DateParseException(stringBuffer.toString());
    }

    private static int groupToInt(String str, String str2, int i, int i2) throws DateParseException {
        boolean z;
        if (str != null) {
            int i3 = 0;
            if (str.startsWith("-")) {
                z = true;
                i3 = 1;
            } else {
                z = false;
            }
            while (i3 < str.length() - 1 && str.charAt(i3) == '0') {
                i3++;
            }
            if (i3 != 0) {
                str = str.substring(i3);
            }
            try {
                int parseInt = Integer.parseInt(str);
                if (z) {
                    parseInt = -parseInt;
                }
                if (parseInt < i) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("The ");
                    stringBuffer.append(str2);
                    stringBuffer.append(" part ");
                    stringBuffer.append("must be at least ");
                    stringBuffer.append(i);
                    stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                    throw new DateParseException(stringBuffer.toString());
                } else if (parseInt <= i2) {
                    return parseInt;
                } else {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("The ");
                    stringBuffer2.append(str2);
                    stringBuffer2.append(" part ");
                    stringBuffer2.append("can't be more than ");
                    stringBuffer2.append(i2);
                    stringBuffer2.append(FileUtil.HIDDEN_PREFIX);
                    throw new DateParseException(stringBuffer2.toString());
                }
            } catch (NumberFormatException unused) {
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("The ");
                stringBuffer3.append(str2);
                stringBuffer3.append(" part ");
                stringBuffer3.append("is a malformed integer.");
                throw new DateParseException(stringBuffer3.toString());
            }
        } else {
            StringBuffer stringBuffer4 = new StringBuffer();
            stringBuffer4.append("The ");
            stringBuffer4.append(str2);
            stringBuffer4.append(" part ");
            stringBuffer4.append("is missing.");
            throw new DateParseException(stringBuffer4.toString());
        }
    }

    private static TimeZone parseMatchingTimeZone(String str, TimeZone timeZone) throws DateParseException {
        if (str == null) {
            return timeZone;
        }
        if (str.equals("Z")) {
            return UTC;
        }
        StringBuffer stringBuffer = new StringBuffer(9);
        stringBuffer.append("GMT");
        stringBuffer.append(str.charAt(0));
        int i = 3;
        String substring = str.substring(1, 3);
        groupToInt(substring, "offset-hours", 0, 23);
        stringBuffer.append(substring);
        if (str.length() > 3) {
            if (str.charAt(3) == ':') {
                i = 4;
            }
            String substring2 = str.substring(i, i + 2);
            groupToInt(substring2, "offset-minutes", 0, 59);
            stringBuffer.append(':');
            stringBuffer.append(substring2);
        }
        return TimeZone.getTimeZone(stringBuffer.toString());
    }

    private static int groupToMillisecond(String str) throws DateParseException {
        if (str == null) {
            return 0;
        }
        if (str.length() > 3) {
            str = str.substring(0, 3);
        }
        int groupToInt = groupToInt(str, "partial-seconds", 0, Integer.MAX_VALUE);
        if (str.length() == 1) {
            return groupToInt * 100;
        }
        return str.length() == 2 ? groupToInt * 10 : groupToInt;
    }

    public static final class TrivialDateToISO8601CalendarFactory implements DateToISO8601CalendarFactory {
        private GregorianCalendar calendar;
        private TimeZone lastlySetTimeZone;

        public GregorianCalendar get(TimeZone timeZone, Date date) {
            GregorianCalendar gregorianCalendar = this.calendar;
            if (gregorianCalendar == null) {
                this.calendar = new GregorianCalendar(timeZone, Locale.US);
                this.calendar.setGregorianChange(new Date(Long.MIN_VALUE));
            } else if (this.lastlySetTimeZone != timeZone) {
                gregorianCalendar.setTimeZone(timeZone);
                this.lastlySetTimeZone = timeZone;
            }
            this.calendar.setTime(date);
            return this.calendar;
        }
    }

    public static final class TrivialCalendarFieldsToDateConverter implements CalendarFieldsToDateConverter {
        private GregorianCalendar calendar;
        private TimeZone lastlySetTimeZone;

        public Date calculate(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z, TimeZone timeZone) {
            GregorianCalendar gregorianCalendar = this.calendar;
            if (gregorianCalendar == null) {
                this.calendar = new GregorianCalendar(timeZone, Locale.US);
                this.calendar.setLenient(false);
                this.calendar.setGregorianChange(new Date(Long.MIN_VALUE));
            } else if (this.lastlySetTimeZone != timeZone) {
                gregorianCalendar.setTimeZone(timeZone);
                this.lastlySetTimeZone = timeZone;
            }
            this.calendar.set(0, i);
            this.calendar.set(1, i2);
            this.calendar.set(2, i3);
            this.calendar.set(5, i4);
            this.calendar.set(11, i5);
            this.calendar.set(12, i6);
            this.calendar.set(13, i7);
            this.calendar.set(14, i8);
            if (z) {
                this.calendar.add(5, 1);
            }
            return this.calendar.getTime();
        }
    }

    public static final class DateParseException extends ParseException {
        public DateParseException(String str) {
            super(str, 0);
        }
    }
}
