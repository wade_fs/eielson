package freemarker.template;

public interface TemplateHashModel extends TemplateModel {
    TemplateModel get(String str) throws TemplateModelException;

    boolean isEmpty() throws TemplateModelException;
}
