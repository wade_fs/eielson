package freemarker.template;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.BeansWrapperConfiguration;
import freemarker.ext.beans._BeansAPI;
import java.lang.ref.ReferenceQueue;
import java.util.WeakHashMap;

public class DefaultObjectWrapperBuilder extends DefaultObjectWrapperConfiguration {
    private static final WeakHashMap INSTANCE_CACHE = new WeakHashMap();
    private static final ReferenceQueue INSTANCE_CACHE_REF_QUEUE = new ReferenceQueue();

    public DefaultObjectWrapperBuilder(Version version) {
        super(version);
    }

    static void clearInstanceCache() {
        synchronized (INSTANCE_CACHE) {
            INSTANCE_CACHE.clear();
        }
    }

    public DefaultObjectWrapper build() {
        return (DefaultObjectWrapper) _BeansAPI.getBeansWrapperSubclassSingleton(this, INSTANCE_CACHE, INSTANCE_CACHE_REF_QUEUE, DefaultObjectWrapperFactory.INSTANCE);
    }

    private static class DefaultObjectWrapperFactory implements _BeansAPI._BeansWrapperSubclassFactory {
        /* access modifiers changed from: private */
        public static final DefaultObjectWrapperFactory INSTANCE = new DefaultObjectWrapperFactory();

        private DefaultObjectWrapperFactory() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: freemarker.template.DefaultObjectWrapper.<init>(freemarker.ext.beans.BeansWrapperConfiguration, boolean):void
         arg types: [freemarker.ext.beans.BeansWrapperConfiguration, int]
         candidates:
          freemarker.template.DefaultObjectWrapper.<init>(freemarker.template.DefaultObjectWrapperConfiguration, boolean):void
          freemarker.template.DefaultObjectWrapper.<init>(freemarker.ext.beans.BeansWrapperConfiguration, boolean):void */
        public BeansWrapper create(BeansWrapperConfiguration beansWrapperConfiguration) {
            return new DefaultObjectWrapper(beansWrapperConfiguration, true);
        }
    }
}
