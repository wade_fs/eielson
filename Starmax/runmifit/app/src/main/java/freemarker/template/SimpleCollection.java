package freemarker.template;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

public class SimpleCollection extends WrappingTemplateModel implements TemplateCollectionModel, Serializable {
    private final Collection collection;
    private final Iterator iterator;
    /* access modifiers changed from: private */
    public boolean iteratorOwned;

    public SimpleCollection(Iterator it) {
        this.iterator = it;
        this.collection = null;
    }

    public SimpleCollection(Collection collection2) {
        this.collection = collection2;
        this.iterator = null;
    }

    public SimpleCollection(Iterator it, ObjectWrapper objectWrapper) {
        super(objectWrapper);
        this.iterator = it;
        this.collection = null;
    }

    public SimpleCollection(Collection collection2, ObjectWrapper objectWrapper) {
        super(objectWrapper);
        this.collection = collection2;
        this.iterator = null;
    }

    public TemplateModelIterator iterator() {
        SimpleTemplateModelIterator simpleTemplateModelIterator;
        Iterator it = this.iterator;
        if (it != null) {
            return new SimpleTemplateModelIterator(it, false);
        }
        synchronized (this.collection) {
            simpleTemplateModelIterator = new SimpleTemplateModelIterator(this.collection.iterator(), true);
        }
        return simpleTemplateModelIterator;
    }

    private class SimpleTemplateModelIterator implements TemplateModelIterator {
        private final Iterator iterator;
        private boolean iteratorOwnedByMe;

        SimpleTemplateModelIterator(Iterator it, boolean z) {
            this.iterator = it;
            this.iteratorOwnedByMe = z;
        }

        public TemplateModel next() throws TemplateModelException {
            if (!this.iteratorOwnedByMe) {
                takeIteratorOwnership();
            }
            if (this.iterator.hasNext()) {
                Object next = this.iterator.next();
                return next instanceof TemplateModel ? (TemplateModel) next : SimpleCollection.this.wrap(next);
            }
            throw new TemplateModelException("The collection has no more items.");
        }

        public boolean hasNext() throws TemplateModelException {
            if (!this.iteratorOwnedByMe) {
                takeIteratorOwnership();
            }
            return this.iterator.hasNext();
        }

        private void takeIteratorOwnership() throws TemplateModelException {
            synchronized (SimpleCollection.this) {
                if (!SimpleCollection.this.iteratorOwned) {
                    boolean unused = SimpleCollection.this.iteratorOwned = true;
                    this.iteratorOwnedByMe = true;
                } else {
                    throw new TemplateModelException("This collection value wraps a java.util.Iterator, thus it can be listed only once.");
                }
            }
        }
    }
}
