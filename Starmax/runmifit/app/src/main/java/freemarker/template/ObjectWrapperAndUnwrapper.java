package freemarker.template;

public interface ObjectWrapperAndUnwrapper extends ObjectWrapper {
    public static final Object CANT_UNWRAP_TO_TARGET_CLASS = new Object();

    Object tryUnwrapTo(TemplateModel templateModel, Class cls) throws TemplateModelException;

    Object unwrap(TemplateModel templateModel) throws TemplateModelException;
}
