package freemarker.template.utility;

import freemarker.template.EmptyMap;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Collections12 {
    public static final Map EMPTY_MAP = new EmptyMap();

    private Collections12() {
    }

    public static Map singletonMap(Object obj, Object obj2) {
        return Collections.singletonMap(obj, obj2);
    }

    public static List singletonList(Object obj) {
        return Collections.singletonList(obj);
    }
}
