package freemarker.template;

import freemarker.ext.beans.BeansWrapperConfiguration;

public abstract class DefaultObjectWrapperConfiguration extends BeansWrapperConfiguration {
    private boolean forceLegacyNonListCollections;
    private boolean useAdaptersForContainers;

    protected DefaultObjectWrapperConfiguration(Version version) {
        super(DefaultObjectWrapper.normalizeIncompatibleImprovementsVersion(version), true);
        this.useAdaptersForContainers = getIncompatibleImprovements().intValue() >= _TemplateAPI.VERSION_INT_2_3_22;
        this.forceLegacyNonListCollections = true;
    }

    public boolean getUseAdaptersForContainers() {
        return this.useAdaptersForContainers;
    }

    public void setUseAdaptersForContainers(boolean z) {
        this.useAdaptersForContainers = z;
    }

    public boolean getForceLegacyNonListCollections() {
        return this.forceLegacyNonListCollections;
    }

    public void setForceLegacyNonListCollections(boolean z) {
        this.forceLegacyNonListCollections = z;
    }

    public int hashCode() {
        int i = 1231;
        int hashCode = ((super.hashCode() * 31) + (this.useAdaptersForContainers ? 1231 : 1237)) * 31;
        if (!this.forceLegacyNonListCollections) {
            i = 1237;
        }
        return hashCode + i;
    }

    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        DefaultObjectWrapperConfiguration defaultObjectWrapperConfiguration = (DefaultObjectWrapperConfiguration) obj;
        if (this.useAdaptersForContainers == defaultObjectWrapperConfiguration.getUseAdaptersForContainers() && this.forceLegacyNonListCollections == defaultObjectWrapperConfiguration.forceLegacyNonListCollections) {
            return true;
        }
        return false;
    }
}
