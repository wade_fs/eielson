package freemarker.template;

public interface TemplateCollectionModelEx extends TemplateCollectionModel {
    boolean contains(TemplateModel templateModel) throws TemplateModelException;

    boolean isEmpty() throws TemplateModelException;

    int size() throws TemplateModelException;
}
