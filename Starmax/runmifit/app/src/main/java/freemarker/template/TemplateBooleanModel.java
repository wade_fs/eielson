package freemarker.template;

public interface TemplateBooleanModel extends TemplateModel {
    public static final TemplateBooleanModel FALSE = new FalseTemplateBooleanModel();
    public static final TemplateBooleanModel TRUE = new TrueTemplateBooleanModel();

    boolean getAsBoolean() throws TemplateModelException;
}
