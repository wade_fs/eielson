package freemarker.template.utility;

public interface ObjectFactory {
    Object createObject() throws Exception;
}
