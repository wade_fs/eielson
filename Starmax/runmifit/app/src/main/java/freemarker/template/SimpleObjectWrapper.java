package freemarker.template;

public class SimpleObjectWrapper extends DefaultObjectWrapper {
    static final SimpleObjectWrapper instance = new SimpleObjectWrapper();

    public SimpleObjectWrapper() {
    }

    public SimpleObjectWrapper(Version version) {
        super(version);
    }

    /* access modifiers changed from: protected */
    public TemplateModel handleUnknownType(Object obj) throws TemplateModelException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("SimpleObjectWrapper deliberately won't wrap this type: ");
        stringBuffer.append(obj.getClass().getName());
        throw new TemplateModelException(stringBuffer.toString());
    }

    public TemplateHashModel wrapAsAPI(Object obj) throws TemplateModelException {
        throw new TemplateModelException("SimpleObjectWrapper deliberately doesn't allow ?api.");
    }
}
