package freemarker.template;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.BeansWrapperConfiguration;
import freemarker.ext.dom.NodeModel;
import java.lang.reflect.Array;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Node;

public class DefaultObjectWrapper extends BeansWrapper {
    private static final Class JYTHON_OBJ_CLASS;
    private static final ObjectWrapper JYTHON_WRAPPER;
    static final DefaultObjectWrapper instance = new DefaultObjectWrapper();
    private boolean forceLegacyNonListCollections;
    private boolean useAdaptersForContainers;

    static {
        ObjectWrapper objectWrapper;
        Class<?> cls = null;
        try {
            Class<?> cls2 = Class.forName("org.python.core.PyObject");
            objectWrapper = (ObjectWrapper) Class.forName("freemarker.ext.jython.JythonWrapper").getField("INSTANCE").get(null);
            cls = cls2;
        } catch (Throwable unused) {
        }
        JYTHON_OBJ_CLASS = cls;
        JYTHON_WRAPPER = objectWrapper;
        objectWrapper = null;
        JYTHON_OBJ_CLASS = cls;
        JYTHON_WRAPPER = objectWrapper;
    }

    public DefaultObjectWrapper() {
        this(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.DefaultObjectWrapper.<init>(freemarker.template.DefaultObjectWrapperConfiguration, boolean):void
     arg types: [freemarker.template.DefaultObjectWrapper$1, int]
     candidates:
      freemarker.template.DefaultObjectWrapper.<init>(freemarker.ext.beans.BeansWrapperConfiguration, boolean):void
      freemarker.template.DefaultObjectWrapper.<init>(freemarker.template.DefaultObjectWrapperConfiguration, boolean):void */
    public DefaultObjectWrapper(Version version) {
        this((DefaultObjectWrapperConfiguration) new DefaultObjectWrapperConfiguration(version) {
            /* class freemarker.template.DefaultObjectWrapper.C33751 */
        }, false);
    }

    protected DefaultObjectWrapper(BeansWrapperConfiguration beansWrapperConfiguration, boolean z) {
        super(beansWrapperConfiguration, z, false);
        DefaultObjectWrapperConfiguration defaultObjectWrapperConfiguration;
        if (beansWrapperConfiguration instanceof DefaultObjectWrapperConfiguration) {
            defaultObjectWrapperConfiguration = (DefaultObjectWrapperConfiguration) beansWrapperConfiguration;
        } else {
            defaultObjectWrapperConfiguration = new DefaultObjectWrapperConfiguration(beansWrapperConfiguration.getIncompatibleImprovements()) {
                /* class freemarker.template.DefaultObjectWrapper.C33762 */
            };
        }
        this.useAdaptersForContainers = defaultObjectWrapperConfiguration.getUseAdaptersForContainers();
        this.forceLegacyNonListCollections = defaultObjectWrapperConfiguration.getForceLegacyNonListCollections();
        finalizeConstruction(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.DefaultObjectWrapper.<init>(freemarker.ext.beans.BeansWrapperConfiguration, boolean):void
     arg types: [freemarker.template.DefaultObjectWrapperConfiguration, boolean]
     candidates:
      freemarker.template.DefaultObjectWrapper.<init>(freemarker.template.DefaultObjectWrapperConfiguration, boolean):void
      freemarker.template.DefaultObjectWrapper.<init>(freemarker.ext.beans.BeansWrapperConfiguration, boolean):void */
    protected DefaultObjectWrapper(DefaultObjectWrapperConfiguration defaultObjectWrapperConfiguration, boolean z) {
        this((BeansWrapperConfiguration) defaultObjectWrapperConfiguration, z);
    }

    public TemplateModel wrap(Object obj) throws TemplateModelException {
        if (obj == null) {
            return super.wrap(null);
        }
        if (obj instanceof TemplateModel) {
            return (TemplateModel) obj;
        }
        if (obj instanceof String) {
            return new SimpleScalar((String) obj);
        }
        if (obj instanceof Number) {
            return new SimpleNumber((Number) obj);
        }
        if (!(obj instanceof Date)) {
            if (obj.getClass().isArray()) {
                if (this.useAdaptersForContainers) {
                    return DefaultArrayAdapter.adapt(obj, this);
                }
                obj = convertArray(obj);
            }
            if (obj instanceof Collection) {
                if (!this.useAdaptersForContainers) {
                    return new SimpleSequence((Collection) obj, this);
                }
                if (obj instanceof List) {
                    return DefaultListAdapter.adapt((List) obj, this);
                }
                if (this.forceLegacyNonListCollections) {
                    return new SimpleSequence((Collection) obj, this);
                }
                return DefaultNonListCollectionAdapter.adapt((Collection) obj, this);
            } else if (obj instanceof Map) {
                return this.useAdaptersForContainers ? DefaultMapAdapter.adapt((Map) obj, this) : new SimpleHash((Map) obj, this);
            } else {
                if (obj instanceof Boolean) {
                    return obj.equals(Boolean.TRUE) ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
                }
                if (obj instanceof Iterator) {
                    return this.useAdaptersForContainers ? DefaultIteratorAdapter.adapt((Iterator) obj, this) : new SimpleCollection((Iterator) obj, this);
                }
                return handleUnknownType(obj);
            }
        } else if (obj instanceof java.sql.Date) {
            return new SimpleDate((java.sql.Date) obj);
        } else {
            if (obj instanceof Time) {
                return new SimpleDate((Time) obj);
            }
            if (obj instanceof Timestamp) {
                return new SimpleDate((Timestamp) obj);
            }
            return new SimpleDate((Date) obj, getDefaultDateType());
        }
    }

    /* access modifiers changed from: protected */
    public TemplateModel handleUnknownType(Object obj) throws TemplateModelException {
        if (obj instanceof Node) {
            return wrapDomNode(obj);
        }
        if (JYTHON_WRAPPER == null || !JYTHON_OBJ_CLASS.isInstance(obj)) {
            return super.wrap(obj);
        }
        return JYTHON_WRAPPER.wrap(obj);
    }

    public TemplateModel wrapDomNode(Object obj) {
        return NodeModel.wrap((Node) obj);
    }

    /* access modifiers changed from: protected */
    public Object convertArray(Object obj) {
        int length = Array.getLength(obj);
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            arrayList.add(Array.get(obj, i));
        }
        return arrayList;
    }

    public boolean getUseAdaptersForContainers() {
        return this.useAdaptersForContainers;
    }

    public void setUseAdaptersForContainers(boolean z) {
        checkModifiable();
        this.useAdaptersForContainers = z;
    }

    public boolean getForceLegacyNonListCollections() {
        return this.forceLegacyNonListCollections;
    }

    public void setForceLegacyNonListCollections(boolean z) {
        checkModifiable();
        this.forceLegacyNonListCollections = z;
    }

    protected static Version normalizeIncompatibleImprovementsVersion(Version version) {
        _TemplateAPI.checkVersionNotNullAndSupported(version);
        Version normalizeIncompatibleImprovementsVersion = BeansWrapper.normalizeIncompatibleImprovementsVersion(version);
        return (version.intValue() < _TemplateAPI.VERSION_INT_2_3_22 || normalizeIncompatibleImprovementsVersion.intValue() >= _TemplateAPI.VERSION_INT_2_3_22) ? normalizeIncompatibleImprovementsVersion : Configuration.VERSION_2_3_22;
    }

    /* access modifiers changed from: protected */
    public String toPropertiesString() {
        int indexOf;
        String propertiesString = super.toPropertiesString();
        if (propertiesString.startsWith("simpleMapWrapper") && (indexOf = propertiesString.indexOf(44)) != -1) {
            propertiesString = propertiesString.substring(indexOf + 1).trim();
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("useAdaptersForContainers=");
        stringBuffer.append(this.useAdaptersForContainers);
        stringBuffer.append(", forceLegacyNonListCollections=");
        stringBuffer.append(this.forceLegacyNonListCollections);
        stringBuffer.append(", ");
        stringBuffer.append(propertiesString);
        return stringBuffer.toString();
    }
}
