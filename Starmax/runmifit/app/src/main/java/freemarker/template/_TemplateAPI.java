package freemarker.template;

import freemarker.cache.CacheStorage;
import freemarker.cache.TemplateLoader;
import freemarker.cache.TemplateLookupStrategy;
import freemarker.cache.TemplateNameFormat;
import freemarker.core.TemplateObject;
import freemarker.template.utility.NullArgumentException;
import java.util.Set;

public class _TemplateAPI {
    public static final int VERSION_INT_2_3_0 = Configuration.VERSION_2_3_0.intValue();
    public static final int VERSION_INT_2_3_19 = Configuration.VERSION_2_3_19.intValue();
    public static final int VERSION_INT_2_3_20 = Configuration.VERSION_2_3_20.intValue();
    public static final int VERSION_INT_2_3_21 = Configuration.VERSION_2_3_21.intValue();
    public static final int VERSION_INT_2_3_22 = Configuration.VERSION_2_3_22.intValue();
    public static final int VERSION_INT_2_3_23 = Configuration.VERSION_2_3_23.intValue();
    public static final int VERSION_INT_2_4_0 = Version.intValueFor(2, 4, 0);

    public static void checkVersionNotNullAndSupported(Version version) {
        NullArgumentException.check(Configuration.INCOMPATIBLE_IMPROVEMENTS_KEY_CAMEL_CASE, version);
        int intValue = version.intValue();
        if (intValue > Configuration.getVersion().intValue()) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("The FreeMarker version requested by \"incompatibleImprovements\" was ");
            stringBuffer.append(version);
            stringBuffer.append(", but the installed FreeMarker version is only ");
            stringBuffer.append(Configuration.getVersion());
            stringBuffer.append(". You may need to upgrade FreeMarker in your project.");
            throw new IllegalArgumentException(stringBuffer.toString());
        } else if (intValue < VERSION_INT_2_3_0) {
            throw new IllegalArgumentException("\"incompatibleImprovements\" must be at least 2.3.0.");
        }
    }

    public static int getTemplateLanguageVersionAsInt(TemplateObject templateObject) {
        return getTemplateLanguageVersionAsInt(templateObject.getTemplate());
    }

    public static int getTemplateLanguageVersionAsInt(Template template) {
        return template.getTemplateLanguageVersion().intValue();
    }

    public static void DefaultObjectWrapperFactory_clearInstanceCache() {
        DefaultObjectWrapperBuilder.clearInstanceCache();
    }

    public static TemplateExceptionHandler getDefaultTemplateExceptionHandler(Version version) {
        return Configuration.getDefaultTemplateExceptionHandler(version);
    }

    public static boolean getDefaultLogTemplateExceptions(Version version) {
        return Configuration.getDefaultLogTemplateExceptions(version);
    }

    public static TemplateLoader createDefaultTemplateLoader(Version version) {
        return Configuration.createDefaultTemplateLoader(version);
    }

    public static CacheStorage createDefaultCacheStorage(Version version) {
        return Configuration.createDefaultCacheStorage(version);
    }

    public static TemplateLookupStrategy getDefaultTemplateLookupStrategy(Version version) {
        return Configuration.getDefaultTemplateLookupStrategy(version);
    }

    public static TemplateNameFormat getDefaultTemplateNameFormat(Version version) {
        return Configuration.getDefaultTemplateNameFormat(version);
    }

    public static Set getConfigurationSettingNames(Configuration configuration, boolean z) {
        return configuration.getSettingNames(z);
    }
}
