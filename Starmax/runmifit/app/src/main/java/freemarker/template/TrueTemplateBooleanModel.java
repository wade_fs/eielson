package freemarker.template;

final class TrueTemplateBooleanModel implements SerializableTemplateBooleanModel {
    public boolean getAsBoolean() {
        return true;
    }

    TrueTemplateBooleanModel() {
    }

    private Object readResolve() {
        return TRUE;
    }
}
