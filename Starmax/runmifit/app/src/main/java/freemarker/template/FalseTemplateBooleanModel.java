package freemarker.template;

final class FalseTemplateBooleanModel implements SerializableTemplateBooleanModel {
    public boolean getAsBoolean() {
        return false;
    }

    FalseTemplateBooleanModel() {
    }

    private Object readResolve() {
        return FALSE;
    }
}
