package freemarker.template.utility;

import java.math.BigDecimal;
import java.math.BigInteger;

public class NumberUtil {
    private static final BigDecimal BIG_DECIMAL_INT_MAX = BigDecimal.valueOf(2147483647L);
    private static final BigDecimal BIG_DECIMAL_INT_MIN = BigDecimal.valueOf(-2147483648L);
    private static final BigInteger BIG_INTEGER_INT_MAX = BIG_DECIMAL_INT_MAX.toBigInteger();
    private static final BigInteger BIG_INTEGER_INT_MIN = BIG_DECIMAL_INT_MIN.toBigInteger();
    static /* synthetic */ Class class$java$lang$Integer;

    private NumberUtil() {
    }

    public static boolean isInfinite(Number number) {
        if (number instanceof Double) {
            return ((Double) number).isInfinite();
        }
        if (number instanceof Float) {
            return ((Float) number).isInfinite();
        }
        if (isNonFPNumberOfSupportedClass(number)) {
            return false;
        }
        throw new UnsupportedNumberClassException(number.getClass());
    }

    public static boolean isNaN(Number number) {
        if (number instanceof Double) {
            return ((Double) number).isNaN();
        }
        if (number instanceof Float) {
            return ((Float) number).isNaN();
        }
        if (isNonFPNumberOfSupportedClass(number)) {
            return false;
        }
        throw new UnsupportedNumberClassException(number.getClass());
    }

    public static int getSignum(Number number) throws ArithmeticException {
        if (number instanceof Integer) {
            int intValue = ((Integer) number).intValue();
            if (intValue > 0) {
                return 1;
            }
            if (intValue == 0) {
                return 0;
            }
            return -1;
        } else if (number instanceof BigDecimal) {
            return ((BigDecimal) number).signum();
        } else {
            if (number instanceof Double) {
                double doubleValue = ((Double) number).doubleValue();
                if (doubleValue > 0.0d) {
                    return 1;
                }
                if (doubleValue == 0.0d) {
                    return 0;
                }
                if (doubleValue < 0.0d) {
                    return -1;
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("The signum of ");
                stringBuffer.append(doubleValue);
                stringBuffer.append(" is not defined.");
                throw new ArithmeticException(stringBuffer.toString());
            } else if (number instanceof Float) {
                float floatValue = ((Float) number).floatValue();
                if (floatValue > 0.0f) {
                    return 1;
                }
                if (floatValue == 0.0f) {
                    return 0;
                }
                if (floatValue < 0.0f) {
                    return -1;
                }
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("The signum of ");
                stringBuffer2.append(floatValue);
                stringBuffer2.append(" is not defined.");
                throw new ArithmeticException(stringBuffer2.toString());
            } else if (number instanceof Long) {
                long longValue = ((Long) number).longValue();
                if (longValue > 0) {
                    return 1;
                }
                if (longValue == 0) {
                    return 0;
                }
                return -1;
            } else if (number instanceof Short) {
                short shortValue = ((Short) number).shortValue();
                if (shortValue > 0) {
                    return 1;
                }
                if (shortValue == 0) {
                    return 0;
                }
                return -1;
            } else if (number instanceof Byte) {
                byte byteValue = ((Byte) number).byteValue();
                if (byteValue > 0) {
                    return 1;
                }
                if (byteValue == 0) {
                    return 0;
                }
                return -1;
            } else if (number instanceof BigInteger) {
                return ((BigInteger) number).signum();
            } else {
                throw new UnsupportedNumberClassException(number.getClass());
            }
        }
    }

    public static boolean isIntegerBigDecimal(BigDecimal bigDecimal) {
        if (bigDecimal.scale() <= 0 || bigDecimal.setScale(0, 1).compareTo(bigDecimal) == 0) {
            return true;
        }
        return false;
    }

    private static boolean isNonFPNumberOfSupportedClass(Number number) {
        return (number instanceof Integer) || (number instanceof BigDecimal) || (number instanceof Long) || (number instanceof Short) || (number instanceof Byte) || (number instanceof BigInteger);
    }

    public static int toIntExact(Number number) {
        if ((number instanceof Integer) || (number instanceof Short) || (number instanceof Byte)) {
            return number.intValue();
        }
        if (number instanceof Long) {
            long longValue = number.longValue();
            int i = (int) longValue;
            if (longValue == ((long) i)) {
                return i;
            }
            Class cls = class$java$lang$Integer;
            if (cls == null) {
                cls = class$("java.lang.Integer");
                class$java$lang$Integer = cls;
            }
            throw newLossyConverionException(number, cls);
        } else if ((number instanceof Double) || (number instanceof Float)) {
            double doubleValue = number.doubleValue();
            if (doubleValue % 1.0d == 0.0d && doubleValue >= -2.147483648E9d && doubleValue <= 2.147483647E9d) {
                return (int) doubleValue;
            }
            Class cls2 = class$java$lang$Integer;
            if (cls2 == null) {
                cls2 = class$("java.lang.Integer");
                class$java$lang$Integer = cls2;
            }
            throw newLossyConverionException(number, cls2);
        } else if (number instanceof BigDecimal) {
            BigDecimal bigDecimal = (BigDecimal) number;
            if (isIntegerBigDecimal(bigDecimal) && bigDecimal.compareTo(BIG_DECIMAL_INT_MAX) <= 0 && bigDecimal.compareTo(BIG_DECIMAL_INT_MIN) >= 0) {
                return bigDecimal.intValue();
            }
            Class cls3 = class$java$lang$Integer;
            if (cls3 == null) {
                cls3 = class$("java.lang.Integer");
                class$java$lang$Integer = cls3;
            }
            throw newLossyConverionException(number, cls3);
        } else if (number instanceof BigInteger) {
            BigInteger bigInteger = (BigInteger) number;
            if (bigInteger.compareTo(BIG_INTEGER_INT_MAX) <= 0 && bigInteger.compareTo(BIG_INTEGER_INT_MIN) >= 0) {
                return bigInteger.intValue();
            }
            Class cls4 = class$java$lang$Integer;
            if (cls4 == null) {
                cls4 = class$("java.lang.Integer");
                class$java$lang$Integer = cls4;
            }
            throw newLossyConverionException(number, cls4);
        } else {
            throw new UnsupportedNumberClassException(number.getClass());
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static ArithmeticException newLossyConverionException(Number number, Class cls) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Can't convert ");
        stringBuffer.append(number);
        stringBuffer.append(" to type ");
        stringBuffer.append(ClassUtil.getShortClassName(cls));
        stringBuffer.append(" without loss.");
        return new ArithmeticException(stringBuffer.toString());
    }
}
