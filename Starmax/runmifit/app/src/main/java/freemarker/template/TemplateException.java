package freemarker.template;

import freemarker.core.Environment;
import freemarker.core.Expression;
import freemarker.core.TemplateElement;
import freemarker.core.TemplateObject;
import freemarker.core._CoreAPI;
import freemarker.core._ErrorDescriptionBuilder;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

public class TemplateException extends Exception {
    private static final String FTL_INSTRUCTION_STACK_TRACE_TITLE = "FTL stack trace (\"~\" means nesting-related):";
    private static final int FTL_STACK_TOP_FEW_MAX_LINES = 6;
    private final transient Expression blamedExpression;
    private String blamedExpressionString;
    private boolean blamedExpressionStringCalculated;
    private Integer columnNumber;
    private String description;
    private transient _ErrorDescriptionBuilder descriptionBuilder;
    private Integer endColumnNumber;
    private Integer endLineNumber;
    private final transient Environment env;
    private transient TemplateElement[] ftlInstructionStackSnapshot;
    private Integer lineNumber;
    private transient Object lock;
    private transient String message;
    private transient ThreadLocal messageWasAlreadyPrintedForThisTrace;
    private transient String messageWithoutStackTop;
    private boolean positionsCalculated;
    private String renderedFtlInstructionStackSnapshot;
    private String renderedFtlInstructionStackSnapshotTop;
    private String templateName;
    private String templateSourceName;

    private interface StackTraceWriter {
        void print(Object obj);

        void printStandardStackTrace(Throwable th);

        void println();

        void println(Object obj);
    }

    public TemplateException(Environment environment) {
        this((String) null, (Exception) null, environment);
    }

    public TemplateException(String str, Environment environment) {
        this(str, (Exception) null, environment);
    }

    public TemplateException(Exception exc, Environment environment) {
        this((String) null, super, environment);
    }

    public TemplateException(Throwable th, Environment environment) {
        this((String) null, th, environment);
    }

    public TemplateException(String str, Exception exc, Environment environment) {
        this(str, exc, environment, null, null);
    }

    public TemplateException(String str, Throwable th, Environment environment) {
        this(str, th, environment, null, null);
    }

    protected TemplateException(Throwable th, Environment environment, Expression expression, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        this(null, th, environment, expression, _errordescriptionbuilder);
    }

    private TemplateException(String str, Throwable th, Environment environment, Expression expression, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(th);
        this.lock = new Object();
        environment = environment == null ? Environment.getCurrentEnvironment() : environment;
        this.env = environment;
        this.blamedExpression = expression;
        this.descriptionBuilder = _errordescriptionbuilder;
        this.description = str;
        if (environment != null) {
            this.ftlInstructionStackSnapshot = _CoreAPI.getInstructionStackSnapshot(environment);
        }
    }

    private void renderMessages() {
        String description2 = getDescription();
        if (description2 != null && description2.length() != 0) {
            this.messageWithoutStackTop = description2;
        } else if (getCause() != null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("No error description was specified for this error; low-level message: ");
            stringBuffer.append(getCause().getClass().getName());
            stringBuffer.append(": ");
            stringBuffer.append(getCause().getMessage());
            this.messageWithoutStackTop = stringBuffer.toString();
        } else {
            this.messageWithoutStackTop = "[No error description was available.]";
        }
        String fTLInstructionStackTopFew = getFTLInstructionStackTopFew();
        if (fTLInstructionStackTopFew != null) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(this.messageWithoutStackTop);
            stringBuffer2.append("\n\n");
            stringBuffer2.append(_CoreAPI.ERROR_MESSAGE_HR);
            stringBuffer2.append("\n");
            stringBuffer2.append(FTL_INSTRUCTION_STACK_TRACE_TITLE);
            stringBuffer2.append("\n");
            stringBuffer2.append(fTLInstructionStackTopFew);
            stringBuffer2.append(_CoreAPI.ERROR_MESSAGE_HR);
            this.message = stringBuffer2.toString();
            this.messageWithoutStackTop = this.message.substring(0, this.messageWithoutStackTop.length());
            return;
        }
        this.message = this.messageWithoutStackTop;
    }

    private void calculatePosition() {
        synchronized (this.lock) {
            if (!this.positionsCalculated) {
                String str = null;
                TemplateObject templateObject = this.blamedExpression != null ? this.blamedExpression : (this.ftlInstructionStackSnapshot == null || this.ftlInstructionStackSnapshot.length == 0) ? null : this.ftlInstructionStackSnapshot[0];
                if (templateObject != null && templateObject.getBeginLine() > 0) {
                    Template template = templateObject.getTemplate();
                    this.templateName = template != null ? template.getName() : null;
                    if (template != null) {
                        str = template.getSourceName();
                    }
                    this.templateSourceName = str;
                    this.lineNumber = new Integer(templateObject.getBeginLine());
                    this.columnNumber = new Integer(templateObject.getBeginColumn());
                    this.endLineNumber = new Integer(templateObject.getEndLine());
                    this.endColumnNumber = new Integer(templateObject.getEndColumn());
                }
                this.positionsCalculated = true;
                deleteFTLInstructionStackSnapshotIfNotNeeded();
            }
        }
    }

    public Exception getCauseException() {
        if (getCause() instanceof Exception) {
            return (Exception) getCause();
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Wrapped to Exception: ");
        stringBuffer.append(getCause());
        return new Exception(stringBuffer.toString(), getCause());
    }

    public String getFTLInstructionStack() {
        synchronized (this.lock) {
            if (this.ftlInstructionStackSnapshot == null) {
                if (this.renderedFtlInstructionStackSnapshot == null) {
                    return null;
                }
            }
            if (this.renderedFtlInstructionStackSnapshot == null) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                _CoreAPI.outputInstructionStack(this.ftlInstructionStackSnapshot, false, printWriter);
                printWriter.close();
                if (this.renderedFtlInstructionStackSnapshot == null) {
                    this.renderedFtlInstructionStackSnapshot = stringWriter.toString();
                    deleteFTLInstructionStackSnapshotIfNotNeeded();
                }
            }
            String str = this.renderedFtlInstructionStackSnapshot;
            return str;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003e, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getFTLInstructionStackTopFew() {
        /*
            r5 = this;
            java.lang.Object r0 = r5.lock
            monitor-enter(r0)
            freemarker.core.TemplateElement[] r1 = r5.ftlInstructionStackSnapshot     // Catch:{ all -> 0x003f }
            r2 = 0
            if (r1 != 0) goto L_0x000f
            java.lang.String r1 = r5.renderedFtlInstructionStackSnapshotTop     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x000d
            goto L_0x000f
        L_0x000d:
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return r2
        L_0x000f:
            java.lang.String r1 = r5.renderedFtlInstructionStackSnapshotTop     // Catch:{ all -> 0x003f }
            if (r1 != 0) goto L_0x0033
            freemarker.core.TemplateElement[] r1 = r5.ftlInstructionStackSnapshot     // Catch:{ all -> 0x003f }
            int r1 = r1.length     // Catch:{ all -> 0x003f }
            if (r1 != 0) goto L_0x001b
            java.lang.String r1 = ""
            goto L_0x002a
        L_0x001b:
            java.io.StringWriter r1 = new java.io.StringWriter     // Catch:{ all -> 0x003f }
            r1.<init>()     // Catch:{ all -> 0x003f }
            freemarker.core.TemplateElement[] r3 = r5.ftlInstructionStackSnapshot     // Catch:{ all -> 0x003f }
            r4 = 1
            freemarker.core._CoreAPI.outputInstructionStack(r3, r4, r1)     // Catch:{ all -> 0x003f }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x003f }
        L_0x002a:
            java.lang.String r3 = r5.renderedFtlInstructionStackSnapshotTop     // Catch:{ all -> 0x003f }
            if (r3 != 0) goto L_0x0033
            r5.renderedFtlInstructionStackSnapshotTop = r1     // Catch:{ all -> 0x003f }
            r5.deleteFTLInstructionStackSnapshotIfNotNeeded()     // Catch:{ all -> 0x003f }
        L_0x0033:
            java.lang.String r1 = r5.renderedFtlInstructionStackSnapshotTop     // Catch:{ all -> 0x003f }
            int r1 = r1.length()     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x003d
            java.lang.String r2 = r5.renderedFtlInstructionStackSnapshotTop     // Catch:{ all -> 0x003f }
        L_0x003d:
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return r2
        L_0x003f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.template.TemplateException.getFTLInstructionStackTopFew():java.lang.String");
    }

    private void deleteFTLInstructionStackSnapshotIfNotNeeded() {
        if (this.renderedFtlInstructionStackSnapshot != null && this.renderedFtlInstructionStackSnapshotTop != null) {
            if (this.positionsCalculated || this.blamedExpression != null) {
                this.ftlInstructionStackSnapshot = null;
            }
        }
    }

    private String getDescription() {
        String str;
        synchronized (this.lock) {
            if (this.description == null && this.descriptionBuilder != null) {
                this.description = this.descriptionBuilder.toString(getFailingInstruction(), this.env != null ? this.env.getShowErrorTips() : true);
                this.descriptionBuilder = null;
            }
            str = this.description;
        }
        return str;
    }

    private TemplateElement getFailingInstruction() {
        TemplateElement[] templateElementArr = this.ftlInstructionStackSnapshot;
        if (templateElementArr == null || templateElementArr.length <= 0) {
            return null;
        }
        return templateElementArr[0];
    }

    public Environment getEnvironment() {
        return this.env;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.TemplateException.printStackTrace(java.io.PrintStream, boolean, boolean, boolean):void
     arg types: [java.io.PrintStream, int, int, int]
     candidates:
      freemarker.template.TemplateException.printStackTrace(freemarker.template.TemplateException$StackTraceWriter, boolean, boolean, boolean):void
      freemarker.template.TemplateException.printStackTrace(java.io.PrintWriter, boolean, boolean, boolean):void
      freemarker.template.TemplateException.printStackTrace(java.io.PrintStream, boolean, boolean, boolean):void */
    public void printStackTrace(PrintStream printStream) {
        printStackTrace(printStream, true, true, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.TemplateException.printStackTrace(java.io.PrintWriter, boolean, boolean, boolean):void
     arg types: [java.io.PrintWriter, int, int, int]
     candidates:
      freemarker.template.TemplateException.printStackTrace(freemarker.template.TemplateException$StackTraceWriter, boolean, boolean, boolean):void
      freemarker.template.TemplateException.printStackTrace(java.io.PrintStream, boolean, boolean, boolean):void
      freemarker.template.TemplateException.printStackTrace(java.io.PrintWriter, boolean, boolean, boolean):void */
    public void printStackTrace(PrintWriter printWriter) {
        printStackTrace(printWriter, true, true, true);
    }

    public void printStackTrace(PrintWriter printWriter, boolean z, boolean z2, boolean z3) {
        synchronized (printWriter) {
            printStackTrace(new PrintWriterStackTraceWriter(printWriter), z, z2, z3);
        }
    }

    public void printStackTrace(PrintStream printStream, boolean z, boolean z2, boolean z3) {
        synchronized (printStream) {
            printStackTrace(new PrintStreamStackTraceWriter(printStream), z, z2, z3);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:(3:2|3|4)|(2:8|(1:10)(1:11))|(3:(6:14|46|22|23|24|25)(1:34)|35|(3:39|40|(1:42)))|43|44) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00aa */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void printStackTrace(freemarker.template.TemplateException.StackTraceWriter r2, boolean r3, boolean r4, boolean r5) {
        /*
            r1 = this;
            monitor-enter(r2)
            if (r3 == 0) goto L_0x000c
            java.lang.String r3 = "FreeMarker template error:"
            r2.println(r3)     // Catch:{ all -> 0x0009 }
            goto L_0x000c
        L_0x0009:
            r3 = move-exception
            goto L_0x00ac
        L_0x000c:
            if (r4 == 0) goto L_0x0033
            java.lang.String r3 = r1.getFTLInstructionStack()     // Catch:{ all -> 0x0009 }
            if (r3 == 0) goto L_0x0031
            java.lang.String r0 = r1.getMessageWithoutStackTop()     // Catch:{ all -> 0x0009 }
            r2.println(r0)     // Catch:{ all -> 0x0009 }
            r2.println()     // Catch:{ all -> 0x0009 }
            java.lang.String r0 = "----"
            r2.println(r0)     // Catch:{ all -> 0x0009 }
            java.lang.String r0 = "FTL stack trace (\"~\" means nesting-related):"
            r2.println(r0)     // Catch:{ all -> 0x0009 }
            r2.print(r3)     // Catch:{ all -> 0x0009 }
            java.lang.String r3 = "----"
            r2.println(r3)     // Catch:{ all -> 0x0009 }
            goto L_0x0033
        L_0x0031:
            r4 = 0
            r5 = 1
        L_0x0033:
            if (r5 == 0) goto L_0x00aa
            if (r4 == 0) goto L_0x0071
            r2.println()     // Catch:{ all -> 0x0009 }
            java.lang.String r3 = "Java stack trace (for programmers):"
            r2.println(r3)     // Catch:{ all -> 0x0009 }
            java.lang.String r3 = "----"
            r2.println(r3)     // Catch:{ all -> 0x0009 }
            java.lang.Object r3 = r1.lock     // Catch:{ all -> 0x0009 }
            monitor-enter(r3)     // Catch:{ all -> 0x0009 }
            java.lang.ThreadLocal r4 = r1.messageWasAlreadyPrintedForThisTrace     // Catch:{ all -> 0x006e }
            if (r4 != 0) goto L_0x0052
            java.lang.ThreadLocal r4 = new java.lang.ThreadLocal     // Catch:{ all -> 0x006e }
            r4.<init>()     // Catch:{ all -> 0x006e }
            r1.messageWasAlreadyPrintedForThisTrace = r4     // Catch:{ all -> 0x006e }
        L_0x0052:
            java.lang.ThreadLocal r4 = r1.messageWasAlreadyPrintedForThisTrace     // Catch:{ all -> 0x006e }
            java.lang.Boolean r5 = java.lang.Boolean.TRUE     // Catch:{ all -> 0x006e }
            r4.set(r5)     // Catch:{ all -> 0x006e }
            monitor-exit(r3)     // Catch:{ all -> 0x006e }
            r2.printStandardStackTrace(r1)     // Catch:{ all -> 0x0065 }
            java.lang.ThreadLocal r3 = r1.messageWasAlreadyPrintedForThisTrace     // Catch:{ all -> 0x0009 }
            java.lang.Boolean r4 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x0009 }
            r3.set(r4)     // Catch:{ all -> 0x0009 }
            goto L_0x0074
        L_0x0065:
            r3 = move-exception
            java.lang.ThreadLocal r4 = r1.messageWasAlreadyPrintedForThisTrace     // Catch:{ all -> 0x0009 }
            java.lang.Boolean r5 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x0009 }
            r4.set(r5)     // Catch:{ all -> 0x0009 }
            throw r3     // Catch:{ all -> 0x0009 }
        L_0x006e:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x006e }
            throw r4     // Catch:{ all -> 0x0009 }
        L_0x0071:
            r2.printStandardStackTrace(r1)     // Catch:{ all -> 0x0009 }
        L_0x0074:
            java.lang.Throwable r3 = r1.getCause()     // Catch:{ all -> 0x0009 }
            if (r3 == 0) goto L_0x00aa
            java.lang.Throwable r3 = r1.getCause()     // Catch:{ all -> 0x0009 }
            java.lang.Throwable r3 = r3.getCause()     // Catch:{ all -> 0x0009 }
            if (r3 != 0) goto L_0x00aa
            java.lang.Throwable r3 = r1.getCause()     // Catch:{ all -> 0x00aa }
            java.lang.Class r3 = r3.getClass()     // Catch:{ all -> 0x00aa }
            java.lang.String r4 = "getRootCause"
            java.lang.Class[] r5 = freemarker.template.utility.CollectionUtils.EMPTY_CLASS_ARRAY     // Catch:{ all -> 0x00aa }
            java.lang.reflect.Method r3 = r3.getMethod(r4, r5)     // Catch:{ all -> 0x00aa }
            java.lang.Throwable r4 = r1.getCause()     // Catch:{ all -> 0x00aa }
            java.lang.Object[] r5 = freemarker.template.utility.CollectionUtils.EMPTY_OBJECT_ARRAY     // Catch:{ all -> 0x00aa }
            java.lang.Object r3 = r3.invoke(r4, r5)     // Catch:{ all -> 0x00aa }
            java.lang.Throwable r3 = (java.lang.Throwable) r3     // Catch:{ all -> 0x00aa }
            if (r3 == 0) goto L_0x00aa
            java.lang.String r4 = "ServletException root cause: "
            r2.println(r4)     // Catch:{ all -> 0x00aa }
            r2.printStandardStackTrace(r3)     // Catch:{ all -> 0x00aa }
        L_0x00aa:
            monitor-exit(r2)     // Catch:{ all -> 0x0009 }
            return
        L_0x00ac:
            monitor-exit(r2)     // Catch:{ all -> 0x0009 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.template.TemplateException.printStackTrace(freemarker.template.TemplateException$StackTraceWriter, boolean, boolean, boolean):void");
    }

    public void printStandardStackTrace(PrintStream printStream) {
        super.printStackTrace(printStream);
    }

    public void printStandardStackTrace(PrintWriter printWriter) {
        super.printStackTrace(printWriter);
    }

    public String getMessage() {
        String str;
        ThreadLocal threadLocal = this.messageWasAlreadyPrintedForThisTrace;
        if (threadLocal != null && threadLocal.get() == Boolean.TRUE) {
            return "[... Exception message was already printed; see it above ...]";
        }
        synchronized (this.lock) {
            if (this.message == null) {
                renderMessages();
            }
            str = this.message;
        }
        return str;
    }

    public String getMessageWithoutStackTop() {
        String str;
        synchronized (this.lock) {
            if (this.messageWithoutStackTop == null) {
                renderMessages();
            }
            str = this.messageWithoutStackTop;
        }
        return str;
    }

    public Integer getLineNumber() {
        Integer num;
        synchronized (this.lock) {
            if (!this.positionsCalculated) {
                calculatePosition();
            }
            num = this.lineNumber;
        }
        return num;
    }

    public String getTemplateName() {
        String str;
        synchronized (this.lock) {
            if (!this.positionsCalculated) {
                calculatePosition();
            }
            str = this.templateName;
        }
        return str;
    }

    public String getTemplateSourceName() {
        String str;
        synchronized (this.lock) {
            if (!this.positionsCalculated) {
                calculatePosition();
            }
            str = this.templateSourceName;
        }
        return str;
    }

    public Integer getColumnNumber() {
        Integer num;
        synchronized (this.lock) {
            if (!this.positionsCalculated) {
                calculatePosition();
            }
            num = this.columnNumber;
        }
        return num;
    }

    public Integer getEndLineNumber() {
        Integer num;
        synchronized (this.lock) {
            if (!this.positionsCalculated) {
                calculatePosition();
            }
            num = this.endLineNumber;
        }
        return num;
    }

    public Integer getEndColumnNumber() {
        Integer num;
        synchronized (this.lock) {
            if (!this.positionsCalculated) {
                calculatePosition();
            }
            num = this.endColumnNumber;
        }
        return num;
    }

    public String getBlamedExpressionString() {
        String str;
        synchronized (this.lock) {
            if (!this.blamedExpressionStringCalculated) {
                if (this.blamedExpression != null) {
                    this.blamedExpressionString = this.blamedExpression.getCanonicalForm();
                }
                this.blamedExpressionStringCalculated = true;
            }
            str = this.blamedExpressionString;
        }
        return str;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException, ClassNotFoundException {
        getFTLInstructionStack();
        getFTLInstructionStackTopFew();
        getDescription();
        calculatePosition();
        getBlamedExpressionString();
        objectOutputStream.defaultWriteObject();
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        this.lock = new Object();
        objectInputStream.defaultReadObject();
    }

    private static class PrintStreamStackTraceWriter implements StackTraceWriter {
        private final PrintStream out;

        PrintStreamStackTraceWriter(PrintStream printStream) {
            this.out = printStream;
        }

        public void print(Object obj) {
            this.out.print(obj);
        }

        public void println(Object obj) {
            this.out.println(obj);
        }

        public void println() {
            this.out.println();
        }

        public void printStandardStackTrace(Throwable th) {
            if (th instanceof TemplateException) {
                ((TemplateException) th).printStandardStackTrace(this.out);
            } else {
                th.printStackTrace(this.out);
            }
        }
    }

    private static class PrintWriterStackTraceWriter implements StackTraceWriter {
        private final PrintWriter out;

        PrintWriterStackTraceWriter(PrintWriter printWriter) {
            this.out = printWriter;
        }

        public void print(Object obj) {
            this.out.print(obj);
        }

        public void println(Object obj) {
            this.out.println(obj);
        }

        public void println() {
            this.out.println();
        }

        public void printStandardStackTrace(Throwable th) {
            if (th instanceof TemplateException) {
                ((TemplateException) th).printStandardStackTrace(this.out);
            } else {
                th.printStackTrace(this.out);
            }
        }
    }
}
