package freemarker.core;

import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class UnexpectedTypeException extends TemplateException {
    public UnexpectedTypeException(Environment environment, String str) {
        super(str, environment);
    }

    UnexpectedTypeException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(null, environment, null, _errordescriptionbuilder);
    }

    UnexpectedTypeException(Expression expression, TemplateModel templateModel, String str, Class[] clsArr, Environment environment) throws InvalidReferenceException {
        super(null, environment, expression, newDesciptionBuilder(expression, null, templateModel, str, clsArr, environment));
    }

    UnexpectedTypeException(Expression expression, TemplateModel templateModel, String str, Class[] clsArr, String str2, Environment environment) throws InvalidReferenceException {
        super(null, environment, expression, newDesciptionBuilder(expression, null, templateModel, str, clsArr, environment).tip(str2));
    }

    UnexpectedTypeException(Expression expression, TemplateModel templateModel, String str, Class[] clsArr, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(null, environment, expression, newDesciptionBuilder(expression, null, templateModel, str, clsArr, environment).tips(strArr));
    }

    UnexpectedTypeException(String str, TemplateModel templateModel, String str2, Class[] clsArr, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(null, environment, null, newDesciptionBuilder(null, str, templateModel, str2, clsArr, environment).tips(strArr));
    }

    private static _ErrorDescriptionBuilder newDesciptionBuilder(Expression expression, String str, TemplateModel templateModel, String str2, Class[] clsArr, Environment environment) throws InvalidReferenceException {
        Object[] explainTypeError;
        if (templateModel != null) {
            _ErrorDescriptionBuilder showBlamer = new _ErrorDescriptionBuilder(unexpectedTypeErrorDescription(str2, expression, str, templateModel)).blame(expression).showBlamer(true);
            if ((templateModel instanceof _UnexpectedTypeErrorExplainerTemplateModel) && (explainTypeError = ((_UnexpectedTypeErrorExplainerTemplateModel) templateModel).explainTypeError(clsArr)) != null) {
                showBlamer.tip(explainTypeError);
            }
            return showBlamer;
        }
        throw InvalidReferenceException.getInstance(expression, environment);
    }

    private static Object[] unexpectedTypeErrorDescription(String str, Expression expression, String str2, TemplateModel templateModel) {
        String str3;
        Object[] objArr = new Object[7];
        objArr[0] = "Expected ";
        objArr[1] = new _DelayedAOrAn(str);
        objArr[2] = ", but ";
        if (str2 == null) {
            str3 = expression != null ? "this" : "the expression";
        } else {
            str3 = new Object[]{"assignment target variable ", new _DelayedJQuote(str2)};
        }
        objArr[3] = str3;
        objArr[4] = " has evaluated to ";
        objArr[5] = new _DelayedAOrAn(new _DelayedFTLTypeDescription(templateModel));
        objArr[6] = str2 == null ? Config.TRACE_TODAY_VISIT_SPLIT : FileUtil.HIDDEN_PREFIX;
        return objArr;
    }
}
