package freemarker.core;

import com.baidu.mobstat.Config;

public class _DelayedOrdinal extends _DelayedConversionToString {
    public _DelayedOrdinal(Object obj) {
        super(obj);
    }

    /* access modifiers changed from: protected */
    public String doConversion(Object obj) {
        if (obj instanceof Number) {
            long longValue = ((Number) obj).longValue();
            long j = longValue % 10;
            if (j == 1 && longValue % 100 != 11) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(longValue);
                stringBuffer.append(Config.STAT_SDK_TYPE);
                return stringBuffer.toString();
            } else if (j == 2 && longValue % 100 != 12) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append(longValue);
                stringBuffer2.append("nd");
                return stringBuffer2.toString();
            } else if (j != 3 || longValue % 100 == 13) {
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append(longValue);
                stringBuffer3.append("th");
                return stringBuffer3.toString();
            } else {
                StringBuffer stringBuffer4 = new StringBuffer();
                stringBuffer4.append(longValue);
                stringBuffer4.append("rd");
                return stringBuffer4.toString();
            }
        } else {
            StringBuffer stringBuffer5 = new StringBuffer();
            stringBuffer5.append("");
            stringBuffer5.append(obj);
            return stringBuffer5.toString();
        }
    }
}
