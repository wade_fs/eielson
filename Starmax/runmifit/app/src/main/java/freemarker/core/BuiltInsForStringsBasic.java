package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.template.ObjectWrapper;
import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.StringUtil;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;

class BuiltInsForStringsBasic {

    static class cap_firstBI extends BuiltInForString {
        cap_firstBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) {
            int length = str.length();
            int i = 0;
            while (i < length && Character.isWhitespace(str.charAt(i))) {
                i++;
            }
            if (i < length) {
                StringBuffer stringBuffer = new StringBuffer(str);
                stringBuffer.setCharAt(i, Character.toUpperCase(str.charAt(i)));
                str = stringBuffer.toString();
            }
            return new SimpleScalar(str);
        }
    }

    static class capitalizeBI extends BuiltInForString {
        capitalizeBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) {
            return new SimpleScalar(StringUtil.capitalize(str));
        }
    }

    static class chop_linebreakBI extends BuiltInForString {
        chop_linebreakBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) {
            return new SimpleScalar(StringUtil.chomp(str));
        }
    }

    static class containsBI extends BuiltIn {
        containsBI() {
        }

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private final String f7438s;

            private BIMethod(String str) {
                this.f7438s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                containsBI.this.checkMethodArgCount(list, 1);
                return this.f7438s.indexOf(containsBI.this.getStringMethodArg(list, 0)) != -1 ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            return new BIMethod(this.target.evalAndCoerceToString(environment, "For sequences/collections (lists and such) use \"?seq_contains\" instead."));
        }
    }

    static class ends_withBI extends BuiltInForString {
        ends_withBI() {
        }

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7439s;

            private BIMethod(String str) {
                this.f7439s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                ends_withBI.this.checkMethodArgCount(list, 1);
                return this.f7439s.endsWith(ends_withBI.this.getStringMethodArg(list, 0)) ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            return new BIMethod(str);
        }
    }

    static class ensure_ends_withBI extends BuiltInForString {
        ensure_ends_withBI() {
        }

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7440s;

            private BIMethod(String str) {
                this.f7440s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                String str;
                ensure_ends_withBI.this.checkMethodArgCount(list, 1);
                String stringMethodArg = ensure_ends_withBI.this.getStringMethodArg(list, 0);
                if (this.f7440s.endsWith(stringMethodArg)) {
                    str = this.f7440s;
                } else {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(this.f7440s);
                    stringBuffer.append(stringMethodArg);
                    str = stringBuffer.toString();
                }
                return new SimpleScalar(str);
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            return new BIMethod(str);
        }
    }

    static class ensure_starts_withBI extends BuiltInForString {
        ensure_starts_withBI() {
        }

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7441s;

            private BIMethod(String str) {
                this.f7441s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                boolean z;
                String str;
                ensure_starts_withBI.this.checkMethodArgCount(list, 1, 3);
                String stringMethodArg = ensure_starts_withBI.this.getStringMethodArg(list, 0);
                if (list.size() > 1) {
                    String stringMethodArg2 = ensure_starts_withBI.this.getStringMethodArg(list, 1);
                    long parseFlagString = list.size() > 2 ? RegexpHelper.parseFlagString(ensure_starts_withBI.this.getStringMethodArg(list, 2)) : 4294967296L;
                    if ((4294967296L & parseFlagString) == 0) {
                        RegexpHelper.checkOnlyHasNonRegexpFlags(ensure_starts_withBI.this.key, parseFlagString, true);
                        if ((RegexpHelper.RE_FLAG_CASE_INSENSITIVE & parseFlagString) == 0) {
                            z = this.f7441s.startsWith(stringMethodArg);
                        } else {
                            z = this.f7441s.toLowerCase().startsWith(stringMethodArg.toLowerCase());
                        }
                    } else {
                        z = RegexpHelper.getPattern(stringMethodArg, (int) parseFlagString).matcher(this.f7441s).lookingAt();
                    }
                    stringMethodArg = stringMethodArg2;
                } else {
                    z = this.f7441s.startsWith(stringMethodArg);
                }
                if (z) {
                    str = this.f7441s;
                } else {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(stringMethodArg);
                    stringBuffer.append(this.f7441s);
                    str = stringBuffer.toString();
                }
                return new SimpleScalar(str);
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            return new BIMethod(str);
        }
    }

    static class index_ofBI extends BuiltIn {
        /* access modifiers changed from: private */
        public final boolean findLast;

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private final String f7442s;

            private BIMethod(String str) {
                this.f7442s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                int size = list.size();
                index_ofBI.this.checkMethodArgCount(size, 1, 2);
                String stringMethodArg = index_ofBI.this.getStringMethodArg(list, 0);
                if (size > 1) {
                    int intValue = index_ofBI.this.getNumberMethodArg(list, 1).intValue();
                    return new SimpleNumber(index_ofBI.this.findLast ? this.f7442s.lastIndexOf(stringMethodArg, intValue) : this.f7442s.indexOf(stringMethodArg, intValue));
                }
                return new SimpleNumber(index_ofBI.this.findLast ? this.f7442s.lastIndexOf(stringMethodArg) : this.f7442s.indexOf(stringMethodArg));
            }
        }

        index_ofBI(boolean z) {
            this.findLast = z;
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            return new BIMethod(this.target.evalAndCoerceToString(environment, "For sequences/collections (lists and such) use \"?seq_index_of\" instead."));
        }
    }

    static class keep_afterBI extends BuiltInForString {
        keep_afterBI() {
        }

        class KeepAfterMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7443s;

            KeepAfterMethod(String str) {
                this.f7443s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                int i;
                int size = list.size();
                keep_afterBI.this.checkMethodArgCount(size, 1, 2);
                String stringMethodArg = keep_afterBI.this.getStringMethodArg(list, 0);
                long parseFlagString = size > 1 ? RegexpHelper.parseFlagString(keep_afterBI.this.getStringMethodArg(list, 1)) : 0;
                if ((4294967296L & parseFlagString) == 0) {
                    RegexpHelper.checkOnlyHasNonRegexpFlags(keep_afterBI.this.key, parseFlagString, true);
                    if ((parseFlagString & RegexpHelper.RE_FLAG_CASE_INSENSITIVE) == 0) {
                        i = this.f7443s.indexOf(stringMethodArg);
                    } else {
                        i = this.f7443s.toLowerCase().indexOf(stringMethodArg.toLowerCase());
                    }
                    if (i >= 0) {
                        i += stringMethodArg.length();
                    }
                } else {
                    Matcher matcher = RegexpHelper.getPattern(stringMethodArg, (int) parseFlagString).matcher(this.f7443s);
                    i = matcher.find() ? matcher.end() : -1;
                }
                return i == -1 ? SimpleScalar.EMPTY_STRING : new SimpleScalar(this.f7443s.substring(i));
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateModelException {
            return new KeepAfterMethod(str);
        }
    }

    static class keep_after_lastBI extends BuiltInForString {
        keep_after_lastBI() {
        }

        class KeepAfterMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7444s;

            KeepAfterMethod(String str) {
                this.f7444s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                int i;
                int size = list.size();
                keep_after_lastBI.this.checkMethodArgCount(size, 1, 2);
                String stringMethodArg = keep_after_lastBI.this.getStringMethodArg(list, 0);
                long parseFlagString = size > 1 ? RegexpHelper.parseFlagString(keep_after_lastBI.this.getStringMethodArg(list, 1)) : 0;
                if ((4294967296L & parseFlagString) == 0) {
                    RegexpHelper.checkOnlyHasNonRegexpFlags(keep_after_lastBI.this.key, parseFlagString, true);
                    if ((parseFlagString & RegexpHelper.RE_FLAG_CASE_INSENSITIVE) == 0) {
                        i = this.f7444s.lastIndexOf(stringMethodArg);
                    } else {
                        i = this.f7444s.toLowerCase().lastIndexOf(stringMethodArg.toLowerCase());
                    }
                    if (i >= 0) {
                        i += stringMethodArg.length();
                    }
                } else if (stringMethodArg.length() == 0) {
                    i = this.f7444s.length();
                } else {
                    Matcher matcher = RegexpHelper.getPattern(stringMethodArg, (int) parseFlagString).matcher(this.f7444s);
                    if (matcher.find()) {
                        int end = matcher.end();
                        while (matcher.find(matcher.start() + 1)) {
                            end = matcher.end();
                        }
                        i = end;
                    } else {
                        i = -1;
                    }
                }
                return i == -1 ? SimpleScalar.EMPTY_STRING : new SimpleScalar(this.f7444s.substring(i));
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateModelException {
            return new KeepAfterMethod(str);
        }
    }

    static class keep_beforeBI extends BuiltInForString {
        keep_beforeBI() {
        }

        class KeepUntilMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7445s;

            KeepUntilMethod(String str) {
                this.f7445s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                int i;
                int size = list.size();
                keep_beforeBI.this.checkMethodArgCount(size, 1, 2);
                String stringMethodArg = keep_beforeBI.this.getStringMethodArg(list, 0);
                long parseFlagString = size > 1 ? RegexpHelper.parseFlagString(keep_beforeBI.this.getStringMethodArg(list, 1)) : 0;
                if ((4294967296L & parseFlagString) == 0) {
                    RegexpHelper.checkOnlyHasNonRegexpFlags(keep_beforeBI.this.key, parseFlagString, true);
                    i = (parseFlagString & RegexpHelper.RE_FLAG_CASE_INSENSITIVE) == 0 ? this.f7445s.indexOf(stringMethodArg) : this.f7445s.toLowerCase().indexOf(stringMethodArg.toLowerCase());
                } else {
                    Matcher matcher = RegexpHelper.getPattern(stringMethodArg, (int) parseFlagString).matcher(this.f7445s);
                    i = matcher.find() ? matcher.start() : -1;
                }
                return i == -1 ? new SimpleScalar(this.f7445s) : new SimpleScalar(this.f7445s.substring(0, i));
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateModelException {
            return new KeepUntilMethod(str);
        }
    }

    static class keep_before_lastBI extends BuiltInForString {
        keep_before_lastBI() {
        }

        class KeepUntilMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7446s;

            KeepUntilMethod(String str) {
                this.f7446s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                int i;
                int size = list.size();
                keep_before_lastBI.this.checkMethodArgCount(size, 1, 2);
                String stringMethodArg = keep_before_lastBI.this.getStringMethodArg(list, 0);
                long parseFlagString = size > 1 ? RegexpHelper.parseFlagString(keep_before_lastBI.this.getStringMethodArg(list, 1)) : 0;
                if ((4294967296L & parseFlagString) == 0) {
                    RegexpHelper.checkOnlyHasNonRegexpFlags(keep_before_lastBI.this.key, parseFlagString, true);
                    i = (parseFlagString & RegexpHelper.RE_FLAG_CASE_INSENSITIVE) == 0 ? this.f7446s.lastIndexOf(stringMethodArg) : this.f7446s.toLowerCase().lastIndexOf(stringMethodArg.toLowerCase());
                } else if (stringMethodArg.length() == 0) {
                    i = this.f7446s.length();
                } else {
                    Matcher matcher = RegexpHelper.getPattern(stringMethodArg, (int) parseFlagString).matcher(this.f7446s);
                    if (matcher.find()) {
                        int start = matcher.start();
                        while (matcher.find(start + 1)) {
                            start = matcher.start();
                        }
                        i = start;
                    } else {
                        i = -1;
                    }
                }
                return i == -1 ? new SimpleScalar(this.f7446s) : new SimpleScalar(this.f7446s.substring(0, i));
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateModelException {
            return new KeepUntilMethod(str);
        }
    }

    static class lengthBI extends BuiltInForString {
        lengthBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            return new SimpleNumber(str.length());
        }
    }

    static class lower_caseBI extends BuiltInForString {
        lower_caseBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) {
            return new SimpleScalar(str.toLowerCase(environment.getLocale()));
        }
    }

    static class padBI extends BuiltInForString {
        /* access modifiers changed from: private */
        public final boolean leftPadder;

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private final String f7447s;

            private BIMethod(String str) {
                this.f7447s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                String str;
                int size = list.size();
                padBI.this.checkMethodArgCount(size, 1, 2);
                int intValue = padBI.this.getNumberMethodArg(list, 0).intValue();
                if (size > 1) {
                    String stringMethodArg = padBI.this.getStringMethodArg(list, 1);
                    try {
                        if (padBI.this.leftPadder) {
                            str = StringUtil.leftPad(this.f7447s, intValue, stringMethodArg);
                        } else {
                            str = StringUtil.rightPad(this.f7447s, intValue, stringMethodArg);
                        }
                        return new SimpleScalar(str);
                    } catch (IllegalArgumentException e) {
                        if (stringMethodArg.length() == 0) {
                            throw new _TemplateModelException(new Object[]{"?", padBI.this.key, "(...) argument #2 can't be a 0-length string."});
                        }
                        throw new _TemplateModelException(e, new Object[]{"?", padBI.this.key, "(...) failed: ", e});
                    }
                } else {
                    return new SimpleScalar(padBI.this.leftPadder ? StringUtil.leftPad(this.f7447s, intValue) : StringUtil.rightPad(this.f7447s, intValue));
                }
            }
        }

        padBI(boolean z) {
            this.leftPadder = z;
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            return new BIMethod(str);
        }
    }

    static class remove_beginningBI extends BuiltInForString {
        remove_beginningBI() {
        }

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7448s;

            private BIMethod(String str) {
                this.f7448s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                remove_beginningBI.this.checkMethodArgCount(list, 1);
                String stringMethodArg = remove_beginningBI.this.getStringMethodArg(list, 0);
                return new SimpleScalar(this.f7448s.startsWith(stringMethodArg) ? this.f7448s.substring(stringMethodArg.length()) : this.f7448s);
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            return new BIMethod(str);
        }
    }

    static class remove_endingBI extends BuiltInForString {
        remove_endingBI() {
        }

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7449s;

            private BIMethod(String str) {
                this.f7449s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                String str;
                remove_endingBI.this.checkMethodArgCount(list, 1);
                String stringMethodArg = remove_endingBI.this.getStringMethodArg(list, 0);
                if (this.f7449s.endsWith(stringMethodArg)) {
                    String str2 = this.f7449s;
                    str = str2.substring(0, str2.length() - stringMethodArg.length());
                } else {
                    str = this.f7449s;
                }
                return new SimpleScalar(str);
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            return new BIMethod(str);
        }
    }

    static class split_BI extends BuiltInForString {
        split_BI() {
        }

        class SplitMethod implements TemplateMethodModel {

            /* renamed from: s */
            private String f7450s;

            SplitMethod(String str) {
                this.f7450s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                String[] strArr;
                int size = list.size();
                split_BI.this.checkMethodArgCount(size, 1, 2);
                boolean z = false;
                String str = (String) list.get(0);
                long parseFlagString = size > 1 ? RegexpHelper.parseFlagString((String) list.get(1)) : 0;
                if ((4294967296L & parseFlagString) == 0) {
                    RegexpHelper.checkNonRegexpFlags("split", parseFlagString);
                    String str2 = this.f7450s;
                    if ((parseFlagString & RegexpHelper.RE_FLAG_CASE_INSENSITIVE) != 0) {
                        z = true;
                    }
                    strArr = StringUtil.split(str2, str, z);
                } else {
                    strArr = RegexpHelper.getPattern(str, (int) parseFlagString).split(this.f7450s);
                }
                return ObjectWrapper.DEFAULT_WRAPPER.wrap(strArr);
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateModelException {
            return new SplitMethod(str);
        }
    }

    static class starts_withBI extends BuiltInForString {
        starts_withBI() {
        }

        private class BIMethod implements TemplateMethodModelEx {

            /* renamed from: s */
            private String f7451s;

            private BIMethod(String str) {
                this.f7451s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                starts_withBI.this.checkMethodArgCount(list, 1);
                return this.f7451s.startsWith(starts_withBI.this.getStringMethodArg(list, 0)) ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            return new BIMethod(str);
        }
    }

    static class substringBI extends BuiltInForString {
        substringBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(final String str, Environment environment) throws TemplateException {
            return new TemplateMethodModelEx() {
                /* class freemarker.core.BuiltInsForStringsBasic.substringBI.C32841 */

                public Object exec(List list) throws TemplateModelException {
                    int size = list.size();
                    substringBI.this.checkMethodArgCount(size, 1, 2);
                    int intValue = substringBI.this.getNumberMethodArg(list, 0).intValue();
                    int length = str.length();
                    if (intValue < 0) {
                        throw newIndexLessThan0Exception(0, intValue);
                    } else if (intValue > length) {
                        throw newIndexGreaterThanLengthException(0, intValue, length);
                    } else if (size <= 1) {
                        return new SimpleScalar(str.substring(intValue));
                    } else {
                        int intValue2 = substringBI.this.getNumberMethodArg(list, 1).intValue();
                        if (intValue2 < 0) {
                            throw newIndexLessThan0Exception(1, intValue2);
                        } else if (intValue2 > length) {
                            throw newIndexGreaterThanLengthException(1, intValue2, length);
                        } else if (intValue <= intValue2) {
                            return new SimpleScalar(str.substring(intValue, intValue2));
                        } else {
                            StringBuffer stringBuffer = new StringBuffer();
                            stringBuffer.append("?");
                            stringBuffer.append(substringBI.this.key);
                            throw MessageUtil.newMethodArgsInvalidValueException(stringBuffer.toString(), new Object[]{"The begin index argument, ", new Integer(intValue), ", shouldn't be greater than the end index argument, ", new Integer(intValue2), FileUtil.HIDDEN_PREFIX});
                        }
                    }
                }

                private TemplateModelException newIndexGreaterThanLengthException(int i, int i2, int i3) throws TemplateModelException {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("?");
                    stringBuffer.append(substringBI.this.key);
                    return MessageUtil.newMethodArgInvalidValueException(stringBuffer.toString(), i, new Object[]{"The index mustn't be greater than the length of the string, ", new Integer(i3), ", but it was ", new Integer(i2), FileUtil.HIDDEN_PREFIX});
                }

                private TemplateModelException newIndexLessThan0Exception(int i, int i2) throws TemplateModelException {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("?");
                    stringBuffer.append(substringBI.this.key);
                    return MessageUtil.newMethodArgInvalidValueException(stringBuffer.toString(), i, new Object[]{"The index must be at least 0, but was ", new Integer(i2), FileUtil.HIDDEN_PREFIX});
                }
            };
        }
    }

    static class trimBI extends BuiltInForString {
        trimBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) {
            return new SimpleScalar(str.trim());
        }
    }

    static class uncap_firstBI extends BuiltInForString {
        uncap_firstBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) {
            int length = str.length();
            int i = 0;
            while (i < length && Character.isWhitespace(str.charAt(i))) {
                i++;
            }
            if (i < length) {
                StringBuffer stringBuffer = new StringBuffer(str);
                stringBuffer.setCharAt(i, Character.toLowerCase(str.charAt(i)));
                str = stringBuffer.toString();
            }
            return new SimpleScalar(str);
        }
    }

    static class upper_caseBI extends BuiltInForString {
        upper_caseBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) {
            return new SimpleScalar(str.toUpperCase(environment.getLocale()));
        }
    }

    static class word_listBI extends BuiltInForString {
        word_listBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) {
            SimpleSequence simpleSequence = new SimpleSequence();
            StringTokenizer stringTokenizer = new StringTokenizer(str);
            while (stringTokenizer.hasMoreTokens()) {
                simpleSequence.add(stringTokenizer.nextToken());
            }
            return simpleSequence;
        }
    }

    private BuiltInsForStringsBasic() {
    }
}
