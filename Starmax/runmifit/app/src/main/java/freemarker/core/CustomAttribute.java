package freemarker.core;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class CustomAttribute {
    public static final int SCOPE_CONFIGURATION = 2;
    public static final int SCOPE_ENVIRONMENT = 0;
    public static final int SCOPE_TEMPLATE = 1;
    private final Object key = new Object();
    private final int scope;

    /* access modifiers changed from: protected */
    public Object create() {
        return null;
    }

    public CustomAttribute(int i) {
        if (i == 0 || i == 1 || i == 2) {
            this.scope = i;
            return;
        }
        throw new IllegalArgumentException();
    }

    public final Object get(Environment environment) {
        return getScopeConfigurable(environment).getCustomAttribute(this.key, this);
    }

    public final Object get() {
        return getScopeConfigurable(getRequiredCurrentEnvironment()).getCustomAttribute(this.key, this);
    }

    public final Object get(Template template) {
        if (this.scope == 1) {
            return template.getCustomAttribute(this.key, this);
        }
        throw new UnsupportedOperationException("This is not a template-scope attribute");
    }

    public final Object get(Configuration configuration) {
        if (this.scope == 2) {
            return configuration.getCustomAttribute(this.key, this);
        }
        throw new UnsupportedOperationException("This is not a template-scope attribute");
    }

    public final void set(Object obj, Environment environment) {
        getScopeConfigurable(environment).setCustomAttribute(this.key, obj);
    }

    public final void set(Object obj) {
        getScopeConfigurable(getRequiredCurrentEnvironment()).setCustomAttribute(this.key, obj);
    }

    public final void set(Object obj, Template template) {
        if (this.scope == 1) {
            template.setCustomAttribute(this.key, obj);
            return;
        }
        throw new UnsupportedOperationException("This is not a template-scope attribute");
    }

    public final void set(Object obj, Configuration configuration) {
        if (this.scope == 2) {
            configuration.setCustomAttribute(this.key, obj);
            return;
        }
        throw new UnsupportedOperationException("This is not a configuration-scope attribute");
    }

    private Environment getRequiredCurrentEnvironment() {
        Environment currentEnvironment = Environment.getCurrentEnvironment();
        if (currentEnvironment != null) {
            return currentEnvironment;
        }
        throw new IllegalStateException("No current environment");
    }

    private Configurable getScopeConfigurable(Environment environment) throws Error {
        int i = this.scope;
        if (i == 0) {
            return environment;
        }
        if (i == 1) {
            return environment.getParent();
        }
        if (i == 2) {
            return environment.getParent().getParent();
        }
        throw new BugException();
    }
}
