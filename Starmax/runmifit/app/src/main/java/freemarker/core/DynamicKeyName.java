package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.core.Expression;
import freemarker.template.ObjectWrapper;
import freemarker.template.SimpleScalar;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import freemarker.template.TemplateSequenceModel;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.Constants;
import java.util.Collections;

final class DynamicKeyName extends Expression {
    private static Class[] NUMERICAL_KEY_LHO_EXPECTED_TYPES = new Class[(NonStringException.STRING_COERCABLE_TYPES.length + 1)];
    static /* synthetic */ Class class$freemarker$core$Range;
    static /* synthetic */ Class class$freemarker$template$TemplateNumberModel;
    static /* synthetic */ Class class$freemarker$template$TemplateScalarModel;
    static /* synthetic */ Class class$freemarker$template$TemplateSequenceModel;
    private final Expression keyExpression;
    private final Expression target;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "...[...]";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    DynamicKeyName(Expression expression, Expression expression2) {
        this.target = super;
        this.keyExpression = super;
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        TemplateModel eval = this.target.eval(environment);
        if (eval != null) {
            TemplateModel eval2 = this.keyExpression.eval(environment);
            if (eval2 == null) {
                if (environment.isClassicCompatible()) {
                    eval2 = TemplateScalarModel.EMPTY_STRING;
                } else {
                    this.keyExpression.assertNonNull(null, environment);
                }
            }
            TemplateModel templateModel = eval2;
            if (templateModel instanceof TemplateNumberModel) {
                return dealWithNumericalKey(eval, this.keyExpression.modelToNumber(templateModel, environment).intValue(), environment);
            }
            if (templateModel instanceof TemplateScalarModel) {
                return dealWithStringKey(eval, EvalUtil.modelToString((TemplateScalarModel) templateModel, this.keyExpression, environment), environment);
            }
            if (templateModel instanceof RangeModel) {
                return dealWithRangeKey(eval, (RangeModel) templateModel, environment);
            }
            Expression expression = this.keyExpression;
            Class[] clsArr = new Class[3];
            Class cls = class$freemarker$template$TemplateNumberModel;
            if (cls == null) {
                cls = class$("freemarker.template.TemplateNumberModel");
                class$freemarker$template$TemplateNumberModel = cls;
            }
            clsArr[0] = cls;
            Class cls2 = class$freemarker$template$TemplateScalarModel;
            if (cls2 == null) {
                cls2 = class$("freemarker.template.TemplateScalarModel");
                class$freemarker$template$TemplateScalarModel = cls2;
            }
            clsArr[1] = cls2;
            Class cls3 = class$freemarker$core$Range;
            if (cls3 == null) {
                cls3 = class$("freemarker.core.Range");
                class$freemarker$core$Range = cls3;
            }
            clsArr[2] = cls3;
            throw new UnexpectedTypeException(super, templateModel, "number, range, or string", clsArr, environment);
        } else if (environment.isClassicCompatible()) {
            return null;
        } else {
            throw InvalidReferenceException.getInstance(this.target, environment);
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    static {
        Class[] clsArr = NUMERICAL_KEY_LHO_EXPECTED_TYPES;
        Class cls = class$freemarker$template$TemplateSequenceModel;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateSequenceModel");
            class$freemarker$template$TemplateSequenceModel = cls;
        }
        int i = 0;
        clsArr[0] = cls;
        while (i < NonStringException.STRING_COERCABLE_TYPES.length) {
            int i2 = i + 1;
            NUMERICAL_KEY_LHO_EXPECTED_TYPES[i2] = NonStringException.STRING_COERCABLE_TYPES[i];
            i = i2;
        }
    }

    private TemplateModel dealWithNumericalKey(TemplateModel templateModel, int i, Environment environment) throws TemplateException {
        String evalAndCoerceToString;
        int i2;
        if (templateModel instanceof TemplateSequenceModel) {
            TemplateSequenceModel templateSequenceModel = (TemplateSequenceModel) templateModel;
            try {
                i2 = templateSequenceModel.size();
            } catch (Exception unused) {
                i2 = Integer.MAX_VALUE;
            }
            if (i < i2) {
                return templateSequenceModel.get(i);
            }
            return null;
        }
        try {
            evalAndCoerceToString = this.target.evalAndCoerceToString(environment);
            return new SimpleScalar(evalAndCoerceToString.substring(i, i + 1));
        } catch (IndexOutOfBoundsException e) {
            if (i < 0) {
                throw new _MiscTemplateException(new Object[]{"Negative index not allowed: ", new Integer(i)});
            } else if (i >= evalAndCoerceToString.length()) {
                throw new _MiscTemplateException(new Object[]{"String index out of range: The index was ", new Integer(i), " (0-based), but the length of the string is only ", new Integer(evalAndCoerceToString.length()), FileUtil.HIDDEN_PREFIX});
            } else {
                throw new RuntimeException("Can't explain exception", e);
            }
        } catch (NonStringException unused2) {
            throw new UnexpectedTypeException(this.target, templateModel, "sequence or string or something automatically convertible to string (number, date or boolean)", NUMERICAL_KEY_LHO_EXPECTED_TYPES, templateModel instanceof TemplateHashModel ? "You had a numberical value inside the []. Currently that's only supported for sequences (lists) and strings. To get a Map item with a non-string key, use myMap?api.get(myKey)." : null, environment);
        }
    }

    private TemplateModel dealWithStringKey(TemplateModel templateModel, String str, Environment environment) throws TemplateException {
        if (templateModel instanceof TemplateHashModel) {
            return ((TemplateHashModel) templateModel).get(str);
        }
        throw new NonHashException(this.target, templateModel, environment);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r7v0 */
    /* JADX WARN: Type inference failed for: r7v1, types: [int] */
    /* JADX WARN: Type inference failed for: r7v3, types: [boolean] */
    /* JADX WARN: Type inference failed for: r7v4 */
    /* JADX WARN: Type inference failed for: r7v6, types: [boolean] */
    /* JADX WARN: Type inference failed for: r7v7 */
    /* JADX WARN: Type inference failed for: r7v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private freemarker.template.TemplateModel dealWithRangeKey(freemarker.template.TemplateModel r24, freemarker.core.RangeModel r25, freemarker.core.Environment r26) throws freemarker.core.UnexpectedTypeException, freemarker.core.InvalidReferenceException, freemarker.template.TemplateException {
        /*
            r23 = this;
            r0 = r23
            r1 = r24
            r6 = r26
            boolean r2 = r1 instanceof freemarker.template.TemplateSequenceModel
            if (r2 == 0) goto L_0x000f
            freemarker.template.TemplateSequenceModel r1 = (freemarker.template.TemplateSequenceModel) r1
            r2 = r1
            r1 = 0
            goto L_0x0016
        L_0x000f:
            freemarker.core.Expression r1 = r0.target     // Catch:{ NonStringException -> 0x0187 }
            java.lang.String r1 = r1.evalAndCoerceToString(r6)     // Catch:{ NonStringException -> 0x0187 }
            r2 = 0
        L_0x0016:
            int r4 = r25.size()
            boolean r5 = r25.isRightUnbounded()
            boolean r6 = r25.isRightAdaptive()
            r7 = 0
            r8 = 1
            if (r5 != 0) goto L_0x0030
            if (r4 != 0) goto L_0x0030
            if (r2 == 0) goto L_0x002b
            r7 = 1
        L_0x002b:
            freemarker.template.TemplateModel r1 = r0.emptyResult(r7)
            return r1
        L_0x0030:
            int r9 = r25.getBegining()
            java.lang.String r10 = ") isn't allowed for a range used for slicing."
            r11 = 3
            r12 = 2
            if (r9 < 0) goto L_0x0170
            if (r1 == 0) goto L_0x0041
            int r13 = r1.length()
            goto L_0x0045
        L_0x0041:
            int r13 = r2.size()
        L_0x0045:
            int r14 = r25.getStep()
            r15 = 7
            java.lang.String r16 = " "
            r17 = 6
            r18 = 5
            java.lang.String r19 = " has only "
            java.lang.String r20 = "string"
            java.lang.String r21 = "sequence"
            java.lang.String r22 = " is out of bounds, because the sliced "
            r3 = 4
            if (r6 == 0) goto L_0x0060
            if (r14 != r8) goto L_0x0060
            if (r9 <= r13) goto L_0x00a2
            goto L_0x0062
        L_0x0060:
            if (r9 < r13) goto L_0x00a2
        L_0x0062:
            freemarker.core._MiscTemplateException r2 = new freemarker.core._MiscTemplateException
            freemarker.core.Expression r4 = r0.keyExpression
            r5 = 10
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.String r6 = "Range start index "
            r5[r7] = r6
            java.lang.Integer r6 = new java.lang.Integer
            r6.<init>(r9)
            r5[r8] = r6
            r5[r12] = r22
            if (r1 == 0) goto L_0x007a
            goto L_0x007c
        L_0x007a:
            r20 = r21
        L_0x007c:
            r5[r11] = r20
            r5[r3] = r19
            java.lang.Integer r3 = new java.lang.Integer
            r3.<init>(r13)
            r5[r18] = r3
            r5[r17] = r16
            if (r1 == 0) goto L_0x008e
            java.lang.String r1 = "character(s)"
            goto L_0x0090
        L_0x008e:
            java.lang.String r1 = "element(s)"
        L_0x0090:
            r5[r15] = r1
            r1 = 8
            java.lang.String r3 = ". "
            r5[r1] = r3
            r1 = 9
            java.lang.String r3 = "(Note that indices are 0-based)."
            r5[r1] = r3
            r2.<init>(r4, r5)
            throw r2
        L_0x00a2:
            if (r5 != 0) goto L_0x010b
            int r5 = r4 + -1
            int r5 = r5 * r14
            int r5 = r5 + r9
            if (r5 >= 0) goto L_0x00c7
            if (r6 == 0) goto L_0x00b0
            int r4 = r9 + 1
            goto L_0x010d
        L_0x00b0:
            freemarker.core._MiscTemplateException r1 = new freemarker.core._MiscTemplateException
            freemarker.core.Expression r2 = r0.keyExpression
            java.lang.Object[] r3 = new java.lang.Object[r11]
            java.lang.String r4 = "Negative range end index ("
            r3[r7] = r4
            java.lang.Integer r4 = new java.lang.Integer
            r4.<init>(r5)
            r3[r8] = r4
            r3[r12] = r10
            r1.<init>(r2, r3)
            throw r1
        L_0x00c7:
            if (r5 < r13) goto L_0x010d
            if (r6 != 0) goto L_0x0105
            freemarker.core._MiscTemplateException r2 = new freemarker.core._MiscTemplateException
            freemarker.core.Expression r4 = r0.keyExpression
            r6 = 9
            java.lang.Object[] r6 = new java.lang.Object[r6]
            java.lang.String r9 = "Range end index "
            r6[r7] = r9
            java.lang.Integer r7 = new java.lang.Integer
            r7.<init>(r5)
            r6[r8] = r7
            r6[r12] = r22
            if (r1 == 0) goto L_0x00e3
            goto L_0x00e5
        L_0x00e3:
            r20 = r21
        L_0x00e5:
            r6[r11] = r20
            r6[r3] = r19
            java.lang.Integer r3 = new java.lang.Integer
            r3.<init>(r13)
            r6[r18] = r3
            r6[r17] = r16
            if (r1 == 0) goto L_0x00f7
            java.lang.String r1 = "character(s)"
            goto L_0x00f9
        L_0x00f7:
            java.lang.String r1 = "element(s)"
        L_0x00f9:
            r6[r15] = r1
            r1 = 8
            java.lang.String r3 = ". (Note that indices are 0-based)."
            r6[r1] = r3
            r2.<init>(r4, r6)
            throw r2
        L_0x0105:
            int r13 = r13 - r9
            int r4 = java.lang.Math.abs(r13)
            goto L_0x010d
        L_0x010b:
            int r4 = r13 - r9
        L_0x010d:
            if (r4 != 0) goto L_0x0117
            if (r2 == 0) goto L_0x0112
            r7 = 1
        L_0x0112:
            freemarker.template.TemplateModel r1 = r0.emptyResult(r7)
            return r1
        L_0x0117:
            if (r2 == 0) goto L_0x0132
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>(r4)
        L_0x011e:
            if (r7 >= r4) goto L_0x012b
            freemarker.template.TemplateModel r3 = r2.get(r9)
            r1.add(r3)
            int r9 = r9 + r14
            int r7 = r7 + 1
            goto L_0x011e
        L_0x012b:
            freemarker.template.SimpleSequence r2 = new freemarker.template.SimpleSequence
            r3 = 0
            r2.<init>(r1, r3)
            return r2
        L_0x0132:
            if (r14 >= 0) goto L_0x0164
            if (r4 <= r8) goto L_0x0164
            boolean r2 = r25.isAffactedByStringSlicingBug()
            if (r2 == 0) goto L_0x0140
            if (r4 != r12) goto L_0x0140
            r2 = r9
            goto L_0x0166
        L_0x0140:
            freemarker.core._MiscTemplateException r1 = new freemarker.core._MiscTemplateException
            freemarker.core.Expression r2 = r0.keyExpression
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.String r5 = "Decreasing ranges aren't allowed for slicing strings (as it would give reversed text). The index range was: first = "
            r3[r7] = r5
            java.lang.Integer r5 = new java.lang.Integer
            r5.<init>(r9)
            r3[r8] = r5
            java.lang.String r5 = ", last = "
            r3[r12] = r5
            java.lang.Integer r5 = new java.lang.Integer
            int r4 = r4 - r8
            int r4 = r4 * r14
            int r9 = r9 + r4
            r5.<init>(r9)
            r3[r11] = r5
            r1.<init>(r2, r3)
            throw r1
        L_0x0164:
            int r2 = r9 + r4
        L_0x0166:
            freemarker.template.SimpleScalar r3 = new freemarker.template.SimpleScalar
            java.lang.String r1 = r1.substring(r9, r2)
            r3.<init>(r1)
            return r3
        L_0x0170:
            freemarker.core._MiscTemplateException r1 = new freemarker.core._MiscTemplateException
            freemarker.core.Expression r2 = r0.keyExpression
            java.lang.Object[] r3 = new java.lang.Object[r11]
            java.lang.String r4 = "Negative range start index ("
            r3[r7] = r4
            java.lang.Integer r4 = new java.lang.Integer
            r4.<init>(r9)
            r3[r8] = r4
            r3[r12] = r10
            r1.<init>(r2, r3)
            throw r1
        L_0x0187:
            freemarker.core.UnexpectedTypeException r7 = new freemarker.core.UnexpectedTypeException
            freemarker.core.Expression r2 = r0.target
            freemarker.template.TemplateModel r3 = r2.eval(r6)
            java.lang.Class[] r5 = freemarker.core.DynamicKeyName.NUMERICAL_KEY_LHO_EXPECTED_TYPES
            java.lang.String r4 = "sequence or string or something automatically convertible to string (number, date or boolean)"
            r1 = r7
            r6 = r26
            r1.<init>(r2, r3, r4, r5, r6)
            goto L_0x019b
        L_0x019a:
            throw r7
        L_0x019b:
            goto L_0x019a
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.DynamicKeyName.dealWithRangeKey(freemarker.template.TemplateModel, freemarker.core.RangeModel, freemarker.core.Environment):freemarker.template.TemplateModel");
    }

    private TemplateModel emptyResult(boolean z) {
        if (z) {
            return _TemplateAPI.getTemplateLanguageVersionAsInt(this) < _TemplateAPI.VERSION_INT_2_3_21 ? new SimpleSequence(Collections.EMPTY_LIST, (ObjectWrapper) null) : Constants.EMPTY_SEQUENCE;
        }
        return TemplateScalarModel.EMPTY_STRING;
    }

    public String getCanonicalForm() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.target.getCanonicalForm());
        stringBuffer.append("[");
        stringBuffer.append(this.keyExpression.getCanonicalForm());
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public boolean isLiteral() {
        return this.constantValue != null || (this.target.isLiteral() && this.keyExpression.isLiteral());
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        return i == 0 ? this.target : this.keyExpression;
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        return i == 0 ? ParameterRole.LEFT_HAND_OPERAND : ParameterRole.ENCLOSED_OPERAND;
    }

    /* access modifiers changed from: protected */
    public Expression deepCloneWithIdentifierReplaced_inner(String str, Expression expression, Expression.ReplacemenetState replacemenetState) {
        return new DynamicKeyName(this.target.deepCloneWithIdentifierReplaced(str, super, replacemenetState), this.keyExpression.deepCloneWithIdentifierReplaced(str, super, replacemenetState));
    }
}
