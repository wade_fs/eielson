package freemarker.core;

import java.util.AbstractSet;

public abstract class _UnmodifiableSet extends AbstractSet {
    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean remove(Object obj) {
        if (!contains(obj)) {
            return false;
        }
        throw new UnsupportedOperationException();
    }

    public void clear() {
        if (!isEmpty()) {
            throw new UnsupportedOperationException();
        }
    }
}
