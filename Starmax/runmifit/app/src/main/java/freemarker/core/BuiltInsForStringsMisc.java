package freemarker.core;

import freemarker.template.Configuration;
import freemarker.template.SimpleNumber;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import java.io.StringReader;

class BuiltInsForStringsMisc {

    static class booleanBI extends BuiltInForString {
        booleanBI() {
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
        /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public freemarker.template.TemplateModel calculateResult(java.lang.String r6, freemarker.core.Environment r7) throws freemarker.template.TemplateException {
            /*
                r5 = this;
                java.lang.String r0 = "true"
                boolean r0 = r6.equals(r0)
                r1 = 0
                r2 = 1
                if (r0 == 0) goto L_0x000c
            L_0x000a:
                r1 = 1
                goto L_0x002a
            L_0x000c:
                java.lang.String r0 = "false"
                boolean r0 = r6.equals(r0)
                if (r0 == 0) goto L_0x0015
                goto L_0x002a
            L_0x0015:
                java.lang.String r0 = r7.getTrueStringValue()
                boolean r0 = r6.equals(r0)
                if (r0 == 0) goto L_0x0020
                goto L_0x000a
            L_0x0020:
                java.lang.String r0 = r7.getFalseStringValue()
                boolean r0 = r6.equals(r0)
                if (r0 == 0) goto L_0x0032
            L_0x002a:
                if (r1 == 0) goto L_0x002f
                freemarker.template.TemplateBooleanModel r6 = freemarker.template.TemplateBooleanModel.TRUE
                goto L_0x0031
            L_0x002f:
                freemarker.template.TemplateBooleanModel r6 = freemarker.template.TemplateBooleanModel.FALSE
            L_0x0031:
                return r6
            L_0x0032:
                freemarker.core._MiscTemplateException r0 = new freemarker.core._MiscTemplateException
                r3 = 2
                java.lang.Object[] r3 = new java.lang.Object[r3]
                java.lang.String r4 = "Can't convert this string to boolean: "
                r3[r1] = r4
                freemarker.core._DelayedJQuote r1 = new freemarker.core._DelayedJQuote
                r1.<init>(r6)
                r3[r2] = r1
                r0.<init>(r5, r7, r3)
                goto L_0x0047
            L_0x0046:
                throw r0
            L_0x0047:
                goto L_0x0046
            */
            throw new UnsupportedOperationException("Method not decompiled: freemarker.core.BuiltInsForStringsMisc.booleanBI.calculateResult(java.lang.String, freemarker.core.Environment):freemarker.template.TemplateModel");
        }
    }

    static class evalBI extends BuiltInForString {
        evalBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            Environment environment2 = environment;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("(");
            stringBuffer.append(str);
            stringBuffer.append(")");
            FMParserTokenManager fMParserTokenManager = new FMParserTokenManager(new SimpleCharStream(new StringReader(stringBuffer.toString()), -1000000000, 1, str.length() + 2));
            Configuration configuration = environment.getConfiguration();
            fMParserTokenManager.incompatibleImprovements = configuration.getIncompatibleImprovements().intValue();
            fMParserTokenManager.SwitchTo(2);
            int namingConvention = configuration.getNamingConvention();
            fMParserTokenManager.initialNamingConvention = namingConvention;
            fMParserTokenManager.namingConvention = namingConvention;
            FMParser fMParser = new FMParser(fMParserTokenManager);
            fMParser.setTemplate(getTemplate());
            try {
                try {
                    return fMParser.Expression().eval(environment2);
                } catch (TemplateException e) {
                    throw new _MiscTemplateException(this, environment2, new Object[]{"Failed to \"?", this.key, "\" string with this error:\n\n", "---begin-message---\n", new _DelayedGetMessageWithoutStackTop(e), "\n---end-message---", "\n\nThe failing expression:"});
                }
            } catch (TokenMgrError e2) {
                throw e2.toParseException(getTemplate());
            } catch (ParseException e3) {
                throw new _MiscTemplateException(this, environment2, new Object[]{"Failed to \"?", this.key, "\" string with this error:\n\n", "---begin-message---\n", new _DelayedGetMessage(e3), "\n---end-message---", "\n\nThe failing expression:"});
            }
        }
    }

    static class numberBI extends BuiltInForString {
        numberBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateException {
            try {
                return new SimpleNumber(environment.getArithmeticEngine().toNumber(str));
            } catch (NumberFormatException unused) {
                throw NonNumericalException.newMalformedNumberException(this, str, environment);
            }
        }
    }

    private BuiltInsForStringsMisc() {
    }
}
