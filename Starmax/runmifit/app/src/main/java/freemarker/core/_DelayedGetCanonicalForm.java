package freemarker.core;

public class _DelayedGetCanonicalForm extends _DelayedConversionToString {
    public _DelayedGetCanonicalForm(TemplateObject templateObject) {
        super(templateObject);
    }

    /* access modifiers changed from: protected */
    public String doConversion(Object obj) {
        try {
            return ((TemplateObject) obj).getCanonicalForm();
        } catch (Exception unused) {
            return "{Error getting canonical form}";
        }
    }
}
