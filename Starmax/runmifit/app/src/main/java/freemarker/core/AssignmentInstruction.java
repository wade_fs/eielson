package freemarker.core;

import freemarker.template.TemplateException;
import java.io.IOException;

final class AssignmentInstruction extends TemplateElement {
    private Expression namespaceExp;
    private int scope;

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    AssignmentInstruction(int i) {
        this.scope = i;
        setRegulatedChildBufferCapacity(1);
    }

    /* access modifiers changed from: package-private */
    public void addAssignment(Assignment assignment) {
        addRegulatedChild(super);
    }

    /* access modifiers changed from: package-private */
    public void setNamespaceExp(Expression expression) {
        this.namespaceExp = expression;
        int regulatedChildCount = getRegulatedChildCount();
        for (int i = 0; i < regulatedChildCount; i++) {
            ((Assignment) getRegulatedChild(i)).setNamespaceExp(expression);
        }
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        int regulatedChildCount = getRegulatedChildCount();
        for (int i = 0; i < regulatedChildCount; i++) {
            environment.visit((Assignment) getRegulatedChild(i));
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(Assignment.getDirectiveName(this.scope));
        if (z) {
            stringBuffer.append(' ');
            int regulatedChildCount = getRegulatedChildCount();
            for (int i = 0; i < regulatedChildCount; i++) {
                if (i != 0) {
                    stringBuffer.append(", ");
                }
                stringBuffer.append(((Assignment) getRegulatedChild(i)).getCanonicalForm());
            }
        } else {
            stringBuffer.append("-container");
        }
        if (this.namespaceExp != null) {
            stringBuffer.append(" in ");
            stringBuffer.append(this.namespaceExp.getCanonicalForm());
        }
        if (z) {
            stringBuffer.append(">");
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return new Integer(this.scope);
        }
        if (i != 1) {
            return null;
        }
        return this.namespaceExp;
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.VARIABLE_SCOPE;
        }
        if (i != 1) {
            return null;
        }
        return ParameterRole.NAMESPACE;
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return Assignment.getDirectiveName(this.scope);
    }

    public TemplateElement postParseCleanup(boolean z) throws ParseException {
        super.postParseCleanup(z);
        return super;
    }
}
