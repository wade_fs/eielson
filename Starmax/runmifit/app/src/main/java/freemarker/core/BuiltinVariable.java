package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.core.Expression;
import freemarker.core.Macro;
import freemarker.template.Configuration;
import freemarker.template.SimpleDate;
import freemarker.template.SimpleScalar;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.StringUtil;
import java.util.Arrays;
import java.util.Date;

final class BuiltinVariable extends Expression {
    static final String CURRENT_NODE = "current_node";
    static final String CURRENT_NODE_CC = "currentNode";
    static final String CURRENT_TEMPLATE_NAME = "current_template_name";
    static final String CURRENT_TEMPLATE_NAME_CC = "currentTemplateName";
    static final String DATA_MODEL = "data_model";
    static final String DATA_MODEL_CC = "dataModel";
    static final String ERROR = "error";
    static final String GLOBALS = "globals";
    static final String LANG = "lang";
    static final String LOCALE = "locale";
    static final String LOCALE_OBJECT = "locale_object";
    static final String LOCALE_OBJECT_CC = "localeObject";
    static final String LOCALS = "locals";
    static final String MAIN = "main";
    static final String MAIN_TEMPLATE_NAME = "main_template_name";
    static final String MAIN_TEMPLATE_NAME_CC = "mainTemplateName";
    static final String NAMESPACE = "namespace";
    static final String NODE = "node";
    static final String NOW = "now";
    static final String OUTPUT_ENCODING = "output_encoding";
    static final String OUTPUT_ENCODING_CC = "outputEncoding";
    static final String PASS = "pass";
    static final String[] SPEC_VAR_NAMES = {CURRENT_NODE_CC, CURRENT_TEMPLATE_NAME_CC, CURRENT_NODE, CURRENT_TEMPLATE_NAME, DATA_MODEL_CC, DATA_MODEL, ERROR, GLOBALS, LANG, "locale", LOCALE_OBJECT_CC, LOCALE_OBJECT, LOCALS, MAIN, MAIN_TEMPLATE_NAME_CC, MAIN_TEMPLATE_NAME, "namespace", NODE, NOW, "outputEncoding", "output_encoding", PASS, TEMPLATE_NAME_CC, TEMPLATE_NAME, "urlEscapingCharset", "url_escaping_charset", VARS, "version"};
    static final String TEMPLATE_NAME = "template_name";
    static final String TEMPLATE_NAME_CC = "templateName";
    static final String URL_ESCAPING_CHARSET = "url_escaping_charset";
    static final String URL_ESCAPING_CHARSET_CC = "urlEscapingCharset";
    static final String VARS = "vars";
    static final String VERSION = "version";
    private final String name;

    /* access modifiers changed from: protected */
    public Expression deepCloneWithIdentifierReplaced_inner(String str, Expression expression, Expression.ReplacemenetState replacemenetState) {
        return super;
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isLiteral() {
        return false;
    }

    BuiltinVariable(Token token, FMParserTokenManager fMParserTokenManager) throws ParseException {
        String str = token.image;
        if (Arrays.binarySearch(SPEC_VAR_NAMES, str) < 0) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Unknown special variable name: ");
            stringBuffer.append(StringUtil.jQuote(str));
            stringBuffer.append(FileUtil.HIDDEN_PREFIX);
            int i = fMParserTokenManager.namingConvention;
            i = i == 10 ? 11 : i;
            stringBuffer.append(" The allowed special variable names are: ");
            int i2 = 0;
            boolean z = true;
            while (true) {
                String[] strArr = SPEC_VAR_NAMES;
                if (i2 < strArr.length) {
                    String str2 = strArr[i2];
                    int identifierNamingConvention = _CoreStringUtils.getIdentifierNamingConvention(str2);
                    if (i == 12) {
                        i2 = identifierNamingConvention == 11 ? i2 + 1 : i2;
                    } else if (identifierNamingConvention == 12) {
                    }
                    if (z) {
                        z = false;
                    } else {
                        stringBuffer.append(", ");
                    }
                    stringBuffer.append(str2);
                } else {
                    throw new ParseException(stringBuffer.toString(), (Template) null, token);
                }
            }
        } else {
            this.name = str.intern();
        }
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        String str = this.name;
        if (str == "namespace") {
            return environment.getCurrentNamespace();
        }
        if (str == MAIN) {
            return environment.getMainNamespace();
        }
        if (str == GLOBALS) {
            return environment.getGlobalVariables();
        }
        if (str == LOCALS) {
            Macro.Context currentMacroContext = environment.getCurrentMacroContext();
            if (currentMacroContext == null) {
                return null;
            }
            return currentMacroContext.getLocals();
        } else if (str == DATA_MODEL || str == DATA_MODEL_CC) {
            return environment.getDataModel();
        } else {
            if (str == VARS) {
                return new VarsHash(environment);
            }
            if (str == "locale") {
                return new SimpleScalar(environment.getLocale().toString());
            }
            if (str == LOCALE_OBJECT || str == LOCALE_OBJECT_CC) {
                return environment.getObjectWrapper().wrap(environment.getLocale());
            }
            if (str == LANG) {
                return new SimpleScalar(environment.getLocale().getLanguage());
            }
            if (str == CURRENT_NODE || str == NODE || str == CURRENT_NODE_CC) {
                return environment.getCurrentVisitorNode();
            }
            if (str == TEMPLATE_NAME || str == TEMPLATE_NAME_CC) {
                if (environment.getConfiguration().getIncompatibleImprovements().intValue() >= _TemplateAPI.VERSION_INT_2_3_23) {
                    return new SimpleScalar(environment.getTemplate230().getName());
                }
                return new SimpleScalar(environment.getTemplate().getName());
            } else if (str == MAIN_TEMPLATE_NAME || str == MAIN_TEMPLATE_NAME_CC) {
                return SimpleScalar.newInstanceOrNull(environment.getMainTemplate().getName());
            } else {
                if (str == CURRENT_TEMPLATE_NAME || str == CURRENT_TEMPLATE_NAME_CC) {
                    return SimpleScalar.newInstanceOrNull(environment.getCurrentTemplate().getName());
                }
                if (str == PASS) {
                    return Macro.DO_NOTHING_MACRO;
                }
                if (str == "version") {
                    return new SimpleScalar(Configuration.getVersionNumber());
                }
                if (str == "output_encoding" || str == "outputEncoding") {
                    return SimpleScalar.newInstanceOrNull(environment.getOutputEncoding());
                }
                if (str == "url_escaping_charset" || str == "urlEscapingCharset") {
                    return SimpleScalar.newInstanceOrNull(environment.getURLEscapingCharset());
                }
                if (str == ERROR) {
                    return new SimpleScalar(environment.getCurrentRecoveredErrorMessage());
                }
                if (str == NOW) {
                    return new SimpleDate(new Date(), 3);
                }
                throw new _MiscTemplateException(super, new Object[]{"Invalid built-in variable: ", str});
            }
        }
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(FileUtil.HIDDEN_PREFIX);
        stringBuffer.append(this.name);
        return stringBuffer.toString();
    }

    public String getCanonicalForm() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(FileUtil.HIDDEN_PREFIX);
        stringBuffer.append(this.name);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return getCanonicalForm();
    }

    static class VarsHash implements TemplateHashModel {
        Environment env;

        public boolean isEmpty() {
            return false;
        }

        VarsHash(Environment environment) {
            this.env = environment;
        }

        public TemplateModel get(String str) throws TemplateModelException {
            return this.env.getVariable(str);
        }
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        throw new IndexOutOfBoundsException();
    }
}
