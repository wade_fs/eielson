package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.template.EmptyMap;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateTransformModel;
import freemarker.template.utility.ObjectFactory;
import freemarker.template.utility.StringUtil;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.math3.geometry.VectorFormat;

final class UnifiedCall extends TemplateElement implements DirectiveCallPlace {
    private List bodyParameterNames;
    private CustomDataHolder customDataHolder;
    boolean legacySyntax;
    private Expression nameExp;
    private Map namedArgs;
    private List positionalArgs;
    private volatile transient SoftReference sortedNamedArgsCache;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "@";
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return true;
    }

    UnifiedCall(Expression expression, Map map, TemplateElement templateElement, List list) {
        this.nameExp = expression;
        this.namedArgs = map;
        setNestedBlock(super);
        this.bodyParameterNames = list;
    }

    UnifiedCall(Expression expression, List list, TemplateElement templateElement, List list2) {
        this.nameExp = expression;
        this.positionalArgs = list;
        setNestedBlock(templateElement == TextBlock.EMPTY_BLOCK ? null : templateElement);
        this.bodyParameterNames = list2;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        Map map;
        TemplateModel eval = this.nameExp.eval(environment);
        if (eval != Macro.DO_NOTHING_MACRO) {
            if (eval instanceof Macro) {
                Macro macro = (Macro) eval;
                if (!macro.isFunction() || this.legacySyntax) {
                    environment.invoke(macro, this.namedArgs, this.positionalArgs, this.bodyParameterNames, getNestedBlock());
                    return;
                }
                throw new _MiscTemplateException(environment, new Object[]{"Routine ", new _DelayedJQuote(macro.getName()), " is a function, not a directive. Functions can only be called from expressions, like in ${f()}, ${x + f()} or ", "<@someDirective someParam=f() />", FileUtil.HIDDEN_PREFIX});
            }
            boolean z = eval instanceof TemplateDirectiveModel;
            if (z || (eval instanceof TemplateTransformModel)) {
                Map map2 = this.namedArgs;
                if (map2 == null || map2.isEmpty()) {
                    map = EmptyMap.instance;
                } else {
                    map = new HashMap();
                    for (Map.Entry entry : this.namedArgs.entrySet()) {
                        map.put((String) entry.getKey(), ((Expression) entry.getValue()).eval(environment));
                    }
                }
                if (z) {
                    environment.visit(getNestedBlock(), (TemplateDirectiveModel) eval, map, this.bodyParameterNames);
                } else {
                    environment.visitAndTransform(getNestedBlock(), (TemplateTransformModel) eval, map);
                }
            } else if (eval == null) {
                throw InvalidReferenceException.getInstance(this.nameExp, environment);
            } else {
                throw new NonUserDefinedDirectiveLikeException(this.nameExp, eval, environment);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append('@');
        MessageUtil.appendExpressionAsUntearable(stringBuffer, this.nameExp);
        boolean z2 = true;
        if (stringBuffer.charAt(stringBuffer.length() - 1) != ')') {
            z2 = false;
        }
        if (this.positionalArgs != null) {
            for (int i = 0; i < this.positionalArgs.size(); i++) {
                Expression expression = (Expression) this.positionalArgs.get(i);
                if (i != 0) {
                    stringBuffer.append(',');
                }
                stringBuffer.append(' ');
                stringBuffer.append(expression.getCanonicalForm());
            }
        } else {
            List sortedNamedArgs = getSortedNamedArgs();
            for (int i2 = 0; i2 < sortedNamedArgs.size(); i2++) {
                Map.Entry entry = (Map.Entry) sortedNamedArgs.get(i2);
                stringBuffer.append(' ');
                stringBuffer.append(_CoreStringUtils.toFTLTopLevelIdentifierReference((String) entry.getKey()));
                stringBuffer.append('=');
                MessageUtil.appendExpressionAsUntearable(stringBuffer, (Expression) entry.getValue());
            }
        }
        List list = this.bodyParameterNames;
        if (list != null && !list.isEmpty()) {
            stringBuffer.append(VectorFormat.DEFAULT_SEPARATOR);
            for (int i3 = 0; i3 < this.bodyParameterNames.size(); i3++) {
                if (i3 != 0) {
                    stringBuffer.append(", ");
                }
                stringBuffer.append(_CoreStringUtils.toFTLTopLevelIdentifierReference((String) this.bodyParameterNames.get(i3)));
            }
        }
        if (z) {
            if (getNestedBlock() == null) {
                stringBuffer.append("/>");
            } else {
                stringBuffer.append('>');
                stringBuffer.append(getNestedBlock().getCanonicalForm());
                stringBuffer.append("</@");
                if (!z2) {
                    Expression expression2 = this.nameExp;
                    if ((expression2 instanceof Identifier) || ((expression2 instanceof Dot) && ((Dot) expression2).onlyHasIdentifiers())) {
                        stringBuffer.append(this.nameExp.getCanonicalForm());
                    }
                }
                stringBuffer.append('>');
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        List list = this.positionalArgs;
        int i = 0;
        int size = (list != null ? list.size() : 0) + 1;
        Map map = this.namedArgs;
        int size2 = size + (map != null ? map.size() * 2 : 0);
        List list2 = this.bodyParameterNames;
        if (list2 != null) {
            i = list2.size();
        }
        return size2 + i;
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.nameExp;
        }
        List list = this.positionalArgs;
        int i2 = 0;
        int size = list != null ? list.size() : 0;
        int i3 = i - 1;
        if (i3 < size) {
            return this.positionalArgs.get(i3);
        }
        int i4 = size + 1;
        Map map = this.namedArgs;
        int i5 = i - i4;
        int size2 = (map != null ? map.size() : 0) * 2;
        if (i5 < size2) {
            Map.Entry entry = (Map.Entry) getSortedNamedArgs().get(i5 / 2);
            return i5 % 2 == 0 ? entry.getKey() : entry.getValue();
        }
        int i6 = i4 + size2;
        List list2 = this.bodyParameterNames;
        if (list2 != null) {
            i2 = list2.size();
        }
        int i7 = i - i6;
        if (i7 < i2) {
            return this.bodyParameterNames.get(i7);
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.CALLEE;
        }
        List list = this.positionalArgs;
        int i2 = 0;
        int size = list != null ? list.size() : 0;
        if (i - 1 < size) {
            return ParameterRole.ARGUMENT_VALUE;
        }
        int i3 = size + 1;
        Map map = this.namedArgs;
        int i4 = i - i3;
        int size2 = (map != null ? map.size() : 0) * 2;
        if (i4 < size2) {
            return i4 % 2 == 0 ? ParameterRole.ARGUMENT_NAME : ParameterRole.ARGUMENT_VALUE;
        }
        int i5 = i3 + size2;
        List list2 = this.bodyParameterNames;
        if (list2 != null) {
            i2 = list2.size();
        }
        if (i - i5 < i2) {
            return ParameterRole.TARGET_LOOP_VARIABLE;
        }
        throw new IndexOutOfBoundsException();
    }

    private List getSortedNamedArgs() {
        List list;
        SoftReference softReference = this.sortedNamedArgsCache;
        if (softReference != null && (list = (List) softReference.get()) != null) {
            return list;
        }
        List sortMapOfExpressions = MiscUtil.sortMapOfExpressions(this.namedArgs);
        this.sortedNamedArgsCache = new SoftReference(sortMapOfExpressions);
        return sortMapOfExpressions;
    }

    public Object getOrCreateCustomData(Object obj, ObjectFactory objectFactory) throws CallPlaceCustomDataInitializationException {
        CustomDataHolder customDataHolder2 = this.customDataHolder;
        if (customDataHolder2 == null) {
            synchronized (this) {
                customDataHolder2 = this.customDataHolder;
                if (customDataHolder2 == null || customDataHolder2.providerIdentity != obj) {
                    if (customDataHolder2 == null) {
                        try {
                            Class.forName("java.util.concurrent.atomic.AtomicInteger");
                        } catch (ClassNotFoundException e) {
                            throw new CallPlaceCustomDataInitializationException("Feature requires at least Java 5", e);
                        }
                    }
                    customDataHolder2 = createNewCustomData(obj, objectFactory);
                    this.customDataHolder = customDataHolder2;
                }
            }
        }
        if (customDataHolder2.providerIdentity != obj) {
            synchronized (this) {
                CustomDataHolder customDataHolder3 = this.customDataHolder;
                if (customDataHolder3 == null || customDataHolder3.providerIdentity != obj) {
                    CustomDataHolder createNewCustomData = createNewCustomData(obj, objectFactory);
                    this.customDataHolder = createNewCustomData;
                    customDataHolder3 = createNewCustomData;
                }
            }
        }
        return customDataHolder2.customData;
    }

    private CustomDataHolder createNewCustomData(Object obj, ObjectFactory objectFactory) throws CallPlaceCustomDataInitializationException {
        try {
            Object createObject = objectFactory.createObject();
            if (createObject != null) {
                return new CustomDataHolder(obj, createObject);
            }
            throw new NullPointerException("ObjectFactory.createObject() has returned null");
        } catch (Exception e) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Failed to initialize custom data for provider identity ");
            stringBuffer.append(StringUtil.tryToString(obj));
            stringBuffer.append(" via factory ");
            stringBuffer.append(StringUtil.tryToString(objectFactory));
            throw new CallPlaceCustomDataInitializationException(stringBuffer.toString(), e);
        }
    }

    public boolean isNestedOutputCacheable() {
        if (getNestedBlock() == null) {
            return true;
        }
        return getNestedBlock().isOutputCacheable();
    }

    private static class CustomDataHolder {
        /* access modifiers changed from: private */
        public final Object customData;
        /* access modifiers changed from: private */
        public final Object providerIdentity;

        public CustomDataHolder(Object obj, Object obj2) {
            this.providerIdentity = obj;
            this.customData = obj2;
        }
    }
}
