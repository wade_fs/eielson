package freemarker.core;

import freemarker.cache._CacheAPI;
import freemarker.core.BodyInstruction;
import freemarker.core.IteratorBlock;
import freemarker.core.Macro;
import freemarker.core.ReturnInstruction;
import freemarker.ext.beans.BeansWrapper;
import freemarker.log.Logger;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.SimpleHash;
import freemarker.template.SimpleSequence;
import freemarker.template.Template;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import freemarker.template.TemplateNodeModel;
import freemarker.template.TemplateScalarModel;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.TemplateTransformModel;
import freemarker.template.TransformControl;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.DateUtil;
import freemarker.template.utility.NullWriter;
import freemarker.template.utility.UndeclaredThrowableException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.Collator;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import org.greenrobot.greendao.generator.Schema;

public final class Environment extends Configurable {
    private static final Logger ATTEMPT_LOGGER = Logger.getLogger("freemarker.runtime.attempt");
    private static final int CACHED_TDFS_DEF_SYS_TZ_OFFS = 8;
    private static final int CACHED_TDFS_LENGTH = 16;
    private static final int CACHED_TDFS_SQL_D_T_TZ_OFFS = 8;
    private static final int CACHED_TDFS_ZONELESS_INPUT_OFFS = 4;
    private static final DecimalFormat C_NUMBER_FORMAT = new DecimalFormat("0.################", new DecimalFormatSymbols(Locale.US));
    private static final Writer EMPTY_BODY_WRITER = new Writer() {
        /* class freemarker.core.Environment.C32945 */

        public void close() {
        }

        public void flush() {
        }

        public void write(char[] cArr, int i, int i2) throws IOException {
            if (i2 > 0) {
                throw new IOException("This transform does not allow nested content.");
            }
        }
    };
    private static final Map JAVA_NUMBER_FORMATS = new HashMap();
    private static final Logger LOG = Logger.getLogger("freemarker.runtime");
    private static final TemplateModel[] NO_OUT_ARGS = new TemplateModel[0];
    private static final int TERSE_MODE_INSTRUCTION_STACK_TRACE_LIMIT = 10;
    static /* synthetic */ Class class$java$sql$Date;
    static /* synthetic */ Class class$java$sql$Time;
    static /* synthetic */ Class class$java$sql$Timestamp;
    static /* synthetic */ Class class$java$util$Date;
    private static final ThreadLocal threadEnv = new ThreadLocal();
    private NumberFormat cNumberFormat;
    private Collator cachedCollator;
    private ISOTemplateDateFormatFactory cachedISOTemplateDateFormatFactory;
    private JavaTemplateDateFormatFactory cachedJavaTemplateDateFormatFactory;
    private NumberFormat cachedNumberFormat;
    private Map cachedNumberFormats;
    private ISOTemplateDateFormatFactory cachedSQLDTISOTemplateDateFormatFactory;
    private JavaTemplateDateFormatFactory cachedSQLDTJavaTemplateDateFormatFactory;
    private XSTemplateDateFormatFactory cachedSQLDTXSTemplateDateFormatFactory;
    private Boolean cachedSQLDateAndTimeTimeZoneSameAsNormal;
    private TemplateDateFormat[] cachedTemplateDateFormats;
    private String cachedURLEscapingCharset;
    private boolean cachedURLEscapingCharsetSet;
    private XSTemplateDateFormatFactory cachedXSTemplateDateFormatFactory;
    private Macro.Context currentMacroContext;
    private Namespace currentNamespace;
    private String currentNodeNS;
    private String currentNodeName;
    private TemplateNodeModel currentVisitorNode;
    private boolean fastInvalidReferenceExceptions;
    /* access modifiers changed from: private */
    public Namespace globalNamespace = new Namespace(null);
    private boolean inAttemptBlock;
    private final ArrayList instructionStack = new ArrayList();
    private DateUtil.DateToISO8601CalendarFactory isoBuiltInCalendarFactory;
    private TemplateModel lastReturnValue;
    private Throwable lastThrowable;
    private Configurable legacyParent;
    private HashMap loadedLibs;
    private ArrayList localContextStack;
    private HashMap macroToNamespaceLookup = new HashMap();
    private final Namespace mainNamespace;
    private int nodeNamespaceIndex;
    private TemplateSequenceModel nodeNamespaces;
    /* access modifiers changed from: private */
    public Writer out;
    private final ArrayList recoveredErrorStack = new ArrayList();
    /* access modifiers changed from: private */
    public final TemplateHashModel rootDataModel;

    private int getCachedTemplateDateFormatIndex(int i, boolean z, boolean z2) {
        int i2 = 0;
        int i3 = i + (z ? 4 : 0);
        if (z2) {
            i2 = 8;
        }
        return i3 + i2;
    }

    static {
        C_NUMBER_FORMAT.setGroupingUsed(false);
        C_NUMBER_FORMAT.setDecimalSeparatorAlwaysShown(false);
    }

    public static Environment getCurrentEnvironment() {
        return (Environment) threadEnv.get();
    }

    static void setCurrentEnvironment(Environment environment) {
        threadEnv.set(environment);
    }

    public Environment(Template template, TemplateHashModel templateHashModel, Writer writer) {
        super(super);
        Namespace namespace = new Namespace(template);
        this.mainNamespace = namespace;
        this.currentNamespace = namespace;
        this.out = writer;
        this.rootDataModel = templateHashModel;
        importMacros(template);
    }

    public Template getTemplate() {
        return (Template) getParent();
    }

    /* access modifiers changed from: package-private */
    public Template getTemplate230() {
        Template template = (Template) this.legacyParent;
        return template != null ? template : getTemplate();
    }

    public Template getMainTemplate() {
        return this.mainNamespace.getTemplate();
    }

    public Template getCurrentTemplate() {
        int size = this.instructionStack.size();
        return size == 0 ? getMainTemplate() : ((TemplateObject) this.instructionStack.get(size - 1)).getTemplate();
    }

    public DirectiveCallPlace getCurrentDirectiveCallPlace() {
        int size = this.instructionStack.size();
        if (size == 0) {
            return null;
        }
        TemplateElement templateElement = (TemplateElement) this.instructionStack.get(size - 1);
        if (templateElement instanceof UnifiedCall) {
            return (UnifiedCall) templateElement;
        }
        if ((templateElement instanceof Macro) && size > 1) {
            int i = size - 2;
            if (this.instructionStack.get(i) instanceof UnifiedCall) {
                return (UnifiedCall) this.instructionStack.get(i);
            }
        }
        return null;
    }

    private void clearCachedValues() {
        this.cachedNumberFormats = null;
        this.cachedNumberFormat = null;
        this.cachedTemplateDateFormats = null;
        this.cachedSQLDTXSTemplateDateFormatFactory = null;
        this.cachedXSTemplateDateFormatFactory = null;
        this.cachedSQLDTISOTemplateDateFormatFactory = null;
        this.cachedISOTemplateDateFormatFactory = null;
        this.cachedSQLDTJavaTemplateDateFormatFactory = null;
        this.cachedJavaTemplateDateFormatFactory = null;
        this.cachedCollator = null;
        this.cachedURLEscapingCharset = null;
        this.cachedURLEscapingCharsetSet = false;
    }

    public void process() throws TemplateException, IOException {
        Object obj = threadEnv.get();
        threadEnv.set(this);
        try {
            clearCachedValues();
            doAutoImportsAndIncludes(this);
            visit(getTemplate().getRootTreeNode());
            if (getAutoFlush()) {
                this.out.flush();
            }
            clearCachedValues();
            threadEnv.set(obj);
        } catch (Throwable th) {
            threadEnv.set(obj);
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void visit(TemplateElement templateElement) throws TemplateException, IOException {
        pushElement(templateElement);
        try {
            templateElement.accept(this);
        } catch (TemplateException e) {
            handleTemplateException(e);
        } catch (Throwable th) {
            popElement();
            throw th;
        }
        popElement();
    }

    /* access modifiers changed from: package-private */
    public void visitByHiddingParent(TemplateElement templateElement) throws TemplateException, IOException {
        TemplateElement replaceTopElement = replaceTopElement(templateElement);
        try {
            templateElement.accept(this);
        } catch (TemplateException e) {
            handleTemplateException(e);
        } catch (Throwable th) {
            replaceTopElement(replaceTopElement);
            throw th;
        }
        replaceTopElement(replaceTopElement);
    }

    private TemplateElement replaceTopElement(TemplateElement templateElement) {
        ArrayList arrayList = this.instructionStack;
        return (TemplateElement) arrayList.set(arrayList.size() - 1, templateElement);
    }

    public void visit(TemplateElement templateElement, TemplateDirectiveModel templateDirectiveModel, Map map, final List list) throws TemplateException, IOException {
        final TemplateModel[] templateModelArr;
        NestedElementTemplateDirectiveBody nestedElementTemplateDirectiveBody = null;
        if (templateElement != null) {
            nestedElementTemplateDirectiveBody = new NestedElementTemplateDirectiveBody(templateElement);
        }
        if (list == null || list.isEmpty()) {
            templateModelArr = NO_OUT_ARGS;
        } else {
            templateModelArr = new TemplateModel[list.size()];
        }
        if (templateModelArr.length > 0) {
            pushLocalContext(new LocalContext() {
                /* class freemarker.core.Environment.C32901 */

                public TemplateModel getLocalVariable(String str) {
                    int indexOf = list.indexOf(str);
                    if (indexOf != -1) {
                        return templateModelArr[indexOf];
                    }
                    return null;
                }

                public Collection getLocalVariableNames() {
                    return list;
                }
            });
        }
        try {
            templateDirectiveModel.execute(this, map, templateModelArr, nestedElementTemplateDirectiveBody);
        } finally {
            if (templateModelArr.length > 0) {
                popLocalContext();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void visitAndTransform(TemplateElement templateElement, TemplateTransformModel templateTransformModel, Map map) throws TemplateException, IOException {
        try {
            Writer writer = templateTransformModel.getWriter(this.out, map);
            if (writer == null) {
                writer = EMPTY_BODY_WRITER;
            }
            TransformControl transformControl = writer instanceof TransformControl ? (TransformControl) writer : null;
            Writer writer2 = this.out;
            this.out = writer;
            if (transformControl != null) {
                try {
                    if (transformControl.onStart() != 0) {
                    }
                    this.out = writer2;
                } catch (TemplateException e) {
                    throw e;
                } catch (IOException e2) {
                    throw e2;
                } catch (RuntimeException e3) {
                    throw e3;
                } catch (Error e4) {
                    throw e4;
                } catch (Throwable th) {
                    try {
                        throw new UndeclaredThrowableException(th);
                    } catch (Throwable th2) {
                        this.out = writer2;
                        writer.close();
                        throw th2;
                    }
                }
                writer.close();
            }
            do {
                if (templateElement != null) {
                    visitByHiddingParent(templateElement);
                }
                if (transformControl == null) {
                    break;
                }
            } while (transformControl.afterBody() == 0);
            this.out = writer2;
            writer.close();
        } catch (TemplateException e5) {
            handleTemplateException(e5);
        }
    }

    /* access modifiers changed from: package-private */
    public void visitAttemptRecover(TemplateElement templateElement, RecoveryBlock recoveryBlock) throws TemplateException, IOException {
        TemplateException templateException;
        Writer writer = this.out;
        StringWriter stringWriter = new StringWriter();
        this.out = stringWriter;
        boolean fastInvalidReferenceExceptions2 = setFastInvalidReferenceExceptions(false);
        boolean z = this.inAttemptBlock;
        try {
            this.inAttemptBlock = true;
            visitByHiddingParent(templateElement);
            this.inAttemptBlock = z;
            setFastInvalidReferenceExceptions(fastInvalidReferenceExceptions2);
            this.out = writer;
            templateException = null;
        } catch (TemplateException e) {
            this.inAttemptBlock = z;
            setFastInvalidReferenceExceptions(fastInvalidReferenceExceptions2);
            this.out = writer;
            templateException = e;
        } catch (Throwable th) {
            this.inAttemptBlock = z;
            setFastInvalidReferenceExceptions(fastInvalidReferenceExceptions2);
            this.out = writer;
            throw th;
        }
        if (templateException != null) {
            if (ATTEMPT_LOGGER.isDebugEnabled()) {
                Logger logger = ATTEMPT_LOGGER;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Error in attempt block ");
                stringBuffer.append(templateElement.getStartLocationQuoted());
                logger.debug(stringBuffer.toString(), templateException);
            }
            try {
                this.recoveredErrorStack.add(templateException);
                visit(recoveryBlock);
            } finally {
                ArrayList arrayList = this.recoveredErrorStack;
                arrayList.remove(arrayList.size() - 1);
            }
        } else {
            this.out.write(stringWriter.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public String getCurrentRecoveredErrorMessage() throws TemplateException {
        if (!this.recoveredErrorStack.isEmpty()) {
            ArrayList arrayList = this.recoveredErrorStack;
            return ((Throwable) arrayList.get(arrayList.size() - 1)).getMessage();
        }
        throw new _MiscTemplateException(this, ".error is not available outside of a #recover block");
    }

    public boolean isInAttemptBlock() {
        return this.inAttemptBlock;
    }

    /* access modifiers changed from: package-private */
    public void invokeNestedContent(BodyInstruction.Context context) throws TemplateException, IOException {
        Macro.Context currentMacroContext2 = getCurrentMacroContext();
        ArrayList arrayList = this.localContextStack;
        TemplateElement templateElement = currentMacroContext2.nestedContent;
        if (templateElement != null) {
            this.currentMacroContext = currentMacroContext2.prevMacroContext;
            this.currentNamespace = currentMacroContext2.nestedContentNamespace;
            boolean isIcI2322OrLater = isIcI2322OrLater();
            Configurable parent = getParent();
            if (isIcI2322OrLater) {
                setParent(this.currentNamespace.getTemplate());
            } else {
                this.legacyParent = this.currentNamespace.getTemplate();
            }
            this.localContextStack = currentMacroContext2.prevLocalContextStack;
            if (currentMacroContext2.nestedContentParameterNames != null) {
                pushLocalContext(context);
            }
            try {
                visit(templateElement);
                if (!isIcI2322OrLater) {
                    this.legacyParent = super;
                }
            } finally {
                if (currentMacroContext2.nestedContentParameterNames != null) {
                    popLocalContext();
                }
                this.currentMacroContext = currentMacroContext2;
                this.currentNamespace = getMacroNamespace(currentMacroContext2.getMacro());
                if (isIcI2322OrLater) {
                    setParent(super);
                } else {
                    this.legacyParent = super;
                }
                this.localContextStack = arrayList;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean visitIteratorBlock(IteratorBlock.IterationContext iterationContext) throws TemplateException, IOException {
        pushLocalContext(iterationContext);
        try {
            return iterationContext.accept(this);
        } catch (TemplateException e) {
            handleTemplateException(e);
            return true;
        } finally {
            popLocalContext();
        }
    }

    /* access modifiers changed from: package-private */
    public void invokeNodeHandlerFor(TemplateNodeModel templateNodeModel, TemplateSequenceModel templateSequenceModel) throws TemplateException, IOException {
        if (this.nodeNamespaces == null) {
            SimpleSequence simpleSequence = new SimpleSequence(1);
            simpleSequence.add(this.currentNamespace);
            this.nodeNamespaces = simpleSequence;
        }
        int i = this.nodeNamespaceIndex;
        String str = this.currentNodeName;
        String str2 = this.currentNodeNS;
        TemplateSequenceModel templateSequenceModel2 = this.nodeNamespaces;
        TemplateNodeModel templateNodeModel2 = this.currentVisitorNode;
        this.currentVisitorNode = templateNodeModel;
        if (templateSequenceModel != null) {
            this.nodeNamespaces = templateSequenceModel;
        }
        try {
            TemplateModel nodeProcessor = getNodeProcessor(templateNodeModel);
            if (nodeProcessor instanceof Macro) {
                invoke((Macro) nodeProcessor, null, null, null, null);
            } else if (nodeProcessor instanceof TemplateTransformModel) {
                visitAndTransform(null, (TemplateTransformModel) nodeProcessor, null);
            } else {
                String nodeType = templateNodeModel.getNodeType();
                if (nodeType == null) {
                    throw new _MiscTemplateException(this, noNodeHandlerDefinedDescription(templateNodeModel, templateNodeModel.getNodeNamespace(), Schema.DEFAULT_NAME));
                } else if (nodeType.equals("text") && (templateNodeModel instanceof TemplateScalarModel)) {
                    this.out.write(((TemplateScalarModel) templateNodeModel).getAsString());
                } else if (nodeType.equals("document")) {
                    recurse(templateNodeModel, templateSequenceModel);
                } else if (!nodeType.equals("pi") && !nodeType.equals("comment")) {
                    if (!nodeType.equals("document_type")) {
                        throw new _MiscTemplateException(this, noNodeHandlerDefinedDescription(templateNodeModel, templateNodeModel.getNodeNamespace(), nodeType));
                    }
                }
            }
        } finally {
            this.currentVisitorNode = templateNodeModel2;
            this.nodeNamespaceIndex = i;
            this.currentNodeName = str;
            this.currentNodeNS = str2;
            this.nodeNamespaces = templateSequenceModel2;
        }
    }

    private Object[] noNodeHandlerDefinedDescription(TemplateNodeModel templateNodeModel, String str, String str2) throws TemplateModelException {
        String str3 = "";
        if (str != null) {
            str3 = str.length() > 0 ? " and namespace " : " and no namespace";
        } else {
            str = str3;
        }
        return new Object[]{"No macro or directive is defined for node named ", new _DelayedJQuote(templateNodeModel.getNodeName()), str3, str, ", and there is no fallback handler called @", str2, " either."};
    }

    /* access modifiers changed from: package-private */
    public void fallback() throws TemplateException, IOException {
        TemplateModel nodeProcessor = getNodeProcessor(this.currentNodeName, this.currentNodeNS, this.nodeNamespaceIndex);
        if (nodeProcessor instanceof Macro) {
            invoke((Macro) nodeProcessor, null, null, null, null);
        } else if (nodeProcessor instanceof TemplateTransformModel) {
            visitAndTransform(null, (TemplateTransformModel) nodeProcessor, null);
        }
    }

    /* access modifiers changed from: package-private */
    public void invoke(Macro macro, Map map, List list, List list2, TemplateElement templateElement) throws TemplateException, IOException {
        Macro.Context context;
        ArrayList arrayList;
        Namespace namespace;
        if (macro != Macro.DO_NOTHING_MACRO) {
            pushElement(macro);
            try {
                macro.getClass();
                Macro.Context context2 = new Macro.Context(this, templateElement, list2);
                setMacroContextLocalsFromArguments(context2, macro, map, list);
                context = this.currentMacroContext;
                this.currentMacroContext = context2;
                arrayList = this.localContextStack;
                this.localContextStack = null;
                namespace = this.currentNamespace;
                this.currentNamespace = (Namespace) this.macroToNamespaceLookup.get(macro);
                try {
                    context2.runMacro(this);
                    this.currentMacroContext = context;
                    this.localContextStack = arrayList;
                } catch (ReturnInstruction.Return unused) {
                    this.currentMacroContext = context;
                    this.localContextStack = arrayList;
                } catch (TemplateException e) {
                    handleTemplateException(e);
                    this.currentMacroContext = context;
                    this.localContextStack = arrayList;
                }
                this.currentNamespace = namespace;
                popElement();
            } catch (Throwable th) {
                popElement();
                throw th;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r13v0 */
    /* JADX WARN: Type inference failed for: r13v1, types: [freemarker.template.SimpleSequence] */
    /* JADX WARN: Type inference failed for: r13v3 */
    /* JADX WARN: Type inference failed for: r13v4, types: [freemarker.template.SimpleHash] */
    /* JADX WARN: Type inference failed for: r13v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setMacroContextLocalsFromArguments(freemarker.core.Macro.Context r18, freemarker.core.Macro r19, java.util.Map r20, java.util.List r21) throws freemarker.template.TemplateException, freemarker.core._MiscTemplateException {
        /*
            r17 = this;
            r1 = r17
            r0 = r18
            r2 = r21
            java.lang.String r3 = r19.getCatchAll()
            java.lang.String r4 = "."
            r6 = 3
            r7 = 2
            r8 = 5
            java.lang.String r9 = "Function "
            java.lang.String r10 = "Macro "
            r11 = 1
            r12 = 0
            r13 = 0
            if (r20 == 0) goto L_0x0089
            if (r3 == 0) goto L_0x0025
            freemarker.template.SimpleHash r2 = new freemarker.template.SimpleHash
            freemarker.template.ObjectWrapper r13 = (freemarker.template.ObjectWrapper) r13
            r2.<init>(r13)
            r0.setLocalVar(r3, r2)
            r13 = r2
        L_0x0025:
            java.util.Set r2 = r20.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x002d:
            boolean r14 = r2.hasNext()
            if (r14 == 0) goto L_0x0103
            java.lang.Object r14 = r2.next()
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14
            java.lang.Object r15 = r14.getKey()
            java.lang.String r15 = (java.lang.String) r15
            r5 = r19
            boolean r16 = r5.hasArgNamed(r15)
            if (r16 != 0) goto L_0x0075
            if (r3 == 0) goto L_0x004a
            goto L_0x0075
        L_0x004a:
            freemarker.core._MiscTemplateException r0 = new freemarker.core._MiscTemplateException
            java.lang.Object[] r2 = new java.lang.Object[r8]
            boolean r3 = r19.isFunction()
            if (r3 == 0) goto L_0x0055
            goto L_0x0056
        L_0x0055:
            r9 = r10
        L_0x0056:
            r2[r12] = r9
            freemarker.core._DelayedJQuote r3 = new freemarker.core._DelayedJQuote
            java.lang.String r5 = r19.getName()
            r3.<init>(r5)
            r2[r11] = r3
            java.lang.String r3 = " has no parameter with name "
            r2[r7] = r3
            freemarker.core._DelayedJQuote r3 = new freemarker.core._DelayedJQuote
            r3.<init>(r15)
            r2[r6] = r3
            r3 = 4
            r2[r3] = r4
            r0.<init>(r1, r2)
            throw r0
        L_0x0075:
            java.lang.Object r14 = r14.getValue()
            freemarker.core.Expression r14 = (freemarker.core.Expression) r14
            freemarker.template.TemplateModel r14 = r14.eval(r1)
            if (r16 == 0) goto L_0x0085
            r0.setLocalVar(r15, r14)
            goto L_0x002d
        L_0x0085:
            r13.put(r15, r14)
            goto L_0x002d
        L_0x0089:
            r5 = r19
            if (r2 == 0) goto L_0x0103
            if (r3 == 0) goto L_0x009a
            freemarker.template.SimpleSequence r14 = new freemarker.template.SimpleSequence
            freemarker.template.ObjectWrapper r13 = (freemarker.template.ObjectWrapper) r13
            r14.<init>(r13)
            r0.setLocalVar(r3, r14)
            r13 = r14
        L_0x009a:
            java.lang.String[] r14 = r19.getArgumentNamesInternal()
            int r15 = r21.size()
            int r8 = r14.length
            if (r8 >= r15) goto L_0x00e1
            if (r3 != 0) goto L_0x00e1
            freemarker.core._MiscTemplateException r0 = new freemarker.core._MiscTemplateException
            r2 = 7
            java.lang.Object[] r2 = new java.lang.Object[r2]
            boolean r3 = r19.isFunction()
            if (r3 == 0) goto L_0x00b3
            goto L_0x00b4
        L_0x00b3:
            r9 = r10
        L_0x00b4:
            r2[r12] = r9
            freemarker.core._DelayedJQuote r3 = new freemarker.core._DelayedJQuote
            java.lang.String r5 = r19.getName()
            r3.<init>(r5)
            r2[r11] = r3
            java.lang.String r3 = " only accepts "
            r2[r7] = r3
            freemarker.core._DelayedToString r3 = new freemarker.core._DelayedToString
            int r5 = r14.length
            r3.<init>(r5)
            r2[r6] = r3
            java.lang.String r3 = " parameters, but got "
            r5 = 4
            r2[r5] = r3
            freemarker.core._DelayedToString r3 = new freemarker.core._DelayedToString
            r3.<init>(r15)
            r5 = 5
            r2[r5] = r3
            r3 = 6
            r2[r3] = r4
            r0.<init>(r1, r2)
            throw r0
        L_0x00e1:
            if (r12 >= r15) goto L_0x0103
            java.lang.Object r3 = r2.get(r12)
            freemarker.core.Expression r3 = (freemarker.core.Expression) r3
            freemarker.template.TemplateModel r3 = r3.eval(r1)
            int r4 = r14.length     // Catch:{ RuntimeException -> 0x00fc }
            if (r12 >= r4) goto L_0x00f6
            r4 = r14[r12]     // Catch:{ RuntimeException -> 0x00fc }
            r0.setLocalVar(r4, r3)     // Catch:{ RuntimeException -> 0x00fc }
            goto L_0x00f9
        L_0x00f6:
            r13.add(r3)     // Catch:{ RuntimeException -> 0x00fc }
        L_0x00f9:
            int r12 = r12 + 1
            goto L_0x00e1
        L_0x00fc:
            r0 = move-exception
            freemarker.core._MiscTemplateException r2 = new freemarker.core._MiscTemplateException
            r2.<init>(r0, r1)
            throw r2
        L_0x0103:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.Environment.setMacroContextLocalsFromArguments(freemarker.core.Macro$Context, freemarker.core.Macro, java.util.Map, java.util.List):void");
    }

    /* access modifiers changed from: package-private */
    public void visitMacroDef(Macro macro) {
        this.macroToNamespaceLookup.put(macro, this.currentNamespace);
        this.currentNamespace.put(macro.getName(), macro);
    }

    /* access modifiers changed from: package-private */
    public Namespace getMacroNamespace(Macro macro) {
        return (Namespace) this.macroToNamespaceLookup.get(macro);
    }

    /* access modifiers changed from: package-private */
    public void recurse(TemplateNodeModel templateNodeModel, TemplateSequenceModel templateSequenceModel) throws TemplateException, IOException {
        if (templateNodeModel == null && (templateNodeModel = getCurrentVisitorNode()) == null) {
            throw new _TemplateModelException("The target node of recursion is missing or null.");
        }
        TemplateSequenceModel childNodes = templateNodeModel.getChildNodes();
        if (childNodes != null) {
            for (int i = 0; i < childNodes.size(); i++) {
                TemplateNodeModel templateNodeModel2 = (TemplateNodeModel) childNodes.get(i);
                if (templateNodeModel2 != null) {
                    invokeNodeHandlerFor(templateNodeModel2, templateSequenceModel);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Macro.Context getCurrentMacroContext() {
        return this.currentMacroContext;
    }

    private void handleTemplateException(TemplateException templateException) throws TemplateException {
        if (this.lastThrowable != templateException) {
            this.lastThrowable = templateException;
            if (LOG.isErrorEnabled() && (isInAttemptBlock() || getLogTemplateExceptions())) {
                LOG.error("Error executing FreeMarker template", templateException);
            }
            if (!(templateException instanceof StopException)) {
                getTemplateExceptionHandler().handleTemplateException(templateException, this, this.out);
                return;
            }
            throw templateException;
        }
        throw templateException;
    }

    public void setTemplateExceptionHandler(TemplateExceptionHandler templateExceptionHandler) {
        super.setTemplateExceptionHandler(templateExceptionHandler);
        this.lastThrowable = null;
    }

    public void setLocale(Locale locale) {
        Locale locale2 = getLocale();
        super.setLocale(locale);
        if (!locale.equals(locale2)) {
            this.cachedNumberFormats = null;
            this.cachedNumberFormat = null;
            if (this.cachedTemplateDateFormats != null) {
                for (int i = 0; i < 16; i++) {
                    TemplateDateFormat templateDateFormat = this.cachedTemplateDateFormats[i];
                    if (templateDateFormat != null && templateDateFormat.isLocaleBound()) {
                        this.cachedTemplateDateFormats[i] = null;
                    }
                }
            }
            XSTemplateDateFormatFactory xSTemplateDateFormatFactory = this.cachedXSTemplateDateFormatFactory;
            if (xSTemplateDateFormatFactory != null && xSTemplateDateFormatFactory.isLocaleBound()) {
                this.cachedXSTemplateDateFormatFactory = null;
            }
            XSTemplateDateFormatFactory xSTemplateDateFormatFactory2 = this.cachedSQLDTXSTemplateDateFormatFactory;
            if (xSTemplateDateFormatFactory2 != null && xSTemplateDateFormatFactory2.isLocaleBound()) {
                this.cachedSQLDTXSTemplateDateFormatFactory = null;
            }
            ISOTemplateDateFormatFactory iSOTemplateDateFormatFactory = this.cachedISOTemplateDateFormatFactory;
            if (iSOTemplateDateFormatFactory != null && iSOTemplateDateFormatFactory.isLocaleBound()) {
                this.cachedISOTemplateDateFormatFactory = null;
            }
            ISOTemplateDateFormatFactory iSOTemplateDateFormatFactory2 = this.cachedSQLDTISOTemplateDateFormatFactory;
            if (iSOTemplateDateFormatFactory2 != null && iSOTemplateDateFormatFactory2.isLocaleBound()) {
                this.cachedSQLDTISOTemplateDateFormatFactory = null;
            }
            JavaTemplateDateFormatFactory javaTemplateDateFormatFactory = this.cachedJavaTemplateDateFormatFactory;
            if (javaTemplateDateFormatFactory != null && javaTemplateDateFormatFactory.isLocaleBound()) {
                this.cachedJavaTemplateDateFormatFactory = null;
            }
            JavaTemplateDateFormatFactory javaTemplateDateFormatFactory2 = this.cachedSQLDTJavaTemplateDateFormatFactory;
            if (javaTemplateDateFormatFactory2 != null && javaTemplateDateFormatFactory2.isLocaleBound()) {
                this.cachedSQLDTJavaTemplateDateFormatFactory = null;
            }
            this.cachedCollator = null;
        }
    }

    public void setTimeZone(TimeZone timeZone) {
        TimeZone timeZone2 = getTimeZone();
        super.setTimeZone(timeZone);
        if (!timeZone.equals(timeZone2)) {
            if (this.cachedTemplateDateFormats != null) {
                for (int i = 0; i < 8; i++) {
                    this.cachedTemplateDateFormats[i] = null;
                }
            }
            this.cachedXSTemplateDateFormatFactory = null;
            this.cachedISOTemplateDateFormatFactory = null;
            this.cachedJavaTemplateDateFormatFactory = null;
            this.cachedSQLDateAndTimeTimeZoneSameAsNormal = null;
        }
    }

    public void setSQLDateAndTimeTimeZone(TimeZone timeZone) {
        TimeZone sQLDateAndTimeTimeZone = getSQLDateAndTimeTimeZone();
        super.setSQLDateAndTimeTimeZone(timeZone);
        if (!nullSafeEquals(timeZone, sQLDateAndTimeTimeZone)) {
            if (this.cachedTemplateDateFormats != null) {
                for (int i = 8; i < 16; i++) {
                    this.cachedTemplateDateFormats[i] = null;
                }
            }
            this.cachedSQLDTXSTemplateDateFormatFactory = null;
            this.cachedSQLDTISOTemplateDateFormatFactory = null;
            this.cachedSQLDTJavaTemplateDateFormatFactory = null;
            this.cachedSQLDateAndTimeTimeZoneSameAsNormal = null;
        }
    }

    private static boolean nullSafeEquals(Object obj, Object obj2) {
        if (obj == obj2) {
            return true;
        }
        if (obj == null || obj2 == null) {
            return false;
        }
        return obj.equals(obj2);
    }

    /* access modifiers changed from: package-private */
    public boolean isSQLDateAndTimeTimeZoneSameAsNormal() {
        if (this.cachedSQLDateAndTimeTimeZoneSameAsNormal == null) {
            this.cachedSQLDateAndTimeTimeZoneSameAsNormal = Boolean.valueOf(getSQLDateAndTimeTimeZone() == null || getSQLDateAndTimeTimeZone().equals(getTimeZone()));
        }
        return this.cachedSQLDateAndTimeTimeZoneSameAsNormal.booleanValue();
    }

    public void setURLEscapingCharset(String str) {
        this.cachedURLEscapingCharsetSet = false;
        super.setURLEscapingCharset(str);
    }

    public void setOutputEncoding(String str) {
        this.cachedURLEscapingCharsetSet = false;
        super.setOutputEncoding(str);
    }

    /* access modifiers changed from: package-private */
    public String getEffectiveURLEscapingCharset() {
        if (!this.cachedURLEscapingCharsetSet) {
            this.cachedURLEscapingCharset = getURLEscapingCharset();
            if (this.cachedURLEscapingCharset == null) {
                this.cachedURLEscapingCharset = getOutputEncoding();
            }
            this.cachedURLEscapingCharsetSet = true;
        }
        return this.cachedURLEscapingCharset;
    }

    /* access modifiers changed from: package-private */
    public Collator getCollator() {
        if (this.cachedCollator == null) {
            this.cachedCollator = Collator.getInstance(getLocale());
        }
        return this.cachedCollator;
    }

    public boolean applyEqualsOperator(TemplateModel templateModel, TemplateModel templateModel2) throws TemplateException {
        return EvalUtil.compare(templateModel, 1, templateModel2, this);
    }

    public boolean applyEqualsOperatorLenient(TemplateModel templateModel, TemplateModel templateModel2) throws TemplateException {
        return EvalUtil.compareLenient(templateModel, 1, templateModel2, this);
    }

    public boolean applyLessThanOperator(TemplateModel templateModel, TemplateModel templateModel2) throws TemplateException {
        return EvalUtil.compare(templateModel, 3, templateModel2, this);
    }

    public boolean applyLessThanOrEqualsOperator(TemplateModel templateModel, TemplateModel templateModel2) throws TemplateException {
        return EvalUtil.compare(templateModel, 5, templateModel2, this);
    }

    public boolean applyGreaterThanOperator(TemplateModel templateModel, TemplateModel templateModel2) throws TemplateException {
        return EvalUtil.compare(templateModel, 4, templateModel2, this);
    }

    public boolean applyWithGreaterThanOrEqualsOperator(TemplateModel templateModel, TemplateModel templateModel2) throws TemplateException {
        return EvalUtil.compare(templateModel, 6, templateModel2, this);
    }

    public void setOut(Writer writer) {
        this.out = writer;
    }

    public Writer getOut() {
        return this.out;
    }

    /* access modifiers changed from: package-private */
    public String formatNumber(Number number) {
        if (this.cachedNumberFormat == null) {
            this.cachedNumberFormat = getNumberFormatObject(getNumberFormat());
        }
        return this.cachedNumberFormat.format(number);
    }

    public void setNumberFormat(String str) {
        super.setNumberFormat(str);
        this.cachedNumberFormat = null;
    }

    public void setTimeFormat(String str) {
        String timeFormat = getTimeFormat();
        super.setTimeFormat(str);
        if (!str.equals(timeFormat) && this.cachedTemplateDateFormats != null) {
            for (int i = 0; i < 16; i += 4) {
                this.cachedTemplateDateFormats[i + 1] = null;
            }
        }
    }

    public void setDateFormat(String str) {
        String dateFormat = getDateFormat();
        super.setDateFormat(str);
        if (!str.equals(dateFormat) && this.cachedTemplateDateFormats != null) {
            for (int i = 0; i < 16; i += 4) {
                this.cachedTemplateDateFormats[i + 2] = null;
            }
        }
    }

    public void setDateTimeFormat(String str) {
        String dateTimeFormat = getDateTimeFormat();
        super.setDateTimeFormat(str);
        if (!str.equals(dateTimeFormat) && this.cachedTemplateDateFormats != null) {
            for (int i = 0; i < 16; i += 4) {
                this.cachedTemplateDateFormats[i + 3] = null;
            }
        }
    }

    public Configuration getConfiguration() {
        return getTemplate().getConfiguration();
    }

    /* access modifiers changed from: package-private */
    public TemplateModel getLastReturnValue() {
        return this.lastReturnValue;
    }

    /* access modifiers changed from: package-private */
    public void setLastReturnValue(TemplateModel templateModel) {
        this.lastReturnValue = templateModel;
    }

    /* access modifiers changed from: package-private */
    public void clearLastReturnValue() {
        this.lastReturnValue = null;
    }

    /* access modifiers changed from: package-private */
    public NumberFormat getNumberFormatObject(String str) {
        NumberFormat numberFormat;
        NumberFormat decimalFormat;
        if (this.cachedNumberFormats == null) {
            this.cachedNumberFormats = new HashMap();
        }
        NumberFormat numberFormat2 = (NumberFormat) this.cachedNumberFormats.get(str);
        if (numberFormat2 != null) {
            return numberFormat2;
        }
        synchronized (JAVA_NUMBER_FORMATS) {
            Locale locale = getLocale();
            NumberFormatKey numberFormatKey = new NumberFormatKey(str, locale);
            numberFormat = (NumberFormat) JAVA_NUMBER_FORMATS.get(numberFormatKey);
            if (numberFormat == null) {
                if ("number".equals(str)) {
                    decimalFormat = NumberFormat.getNumberInstance(locale);
                } else if ("currency".equals(str)) {
                    decimalFormat = NumberFormat.getCurrencyInstance(locale);
                } else if ("percent".equals(str)) {
                    decimalFormat = NumberFormat.getPercentInstance(locale);
                } else if ("computer".equals(str)) {
                    decimalFormat = getCNumberFormat();
                } else {
                    decimalFormat = new DecimalFormat(str, new DecimalFormatSymbols(getLocale()));
                }
                numberFormat = decimalFormat;
                JAVA_NUMBER_FORMATS.put(numberFormatKey, numberFormat);
            }
        }
        NumberFormat numberFormat3 = (NumberFormat) numberFormat.clone();
        this.cachedNumberFormats.put(str, numberFormat3);
        return numberFormat3;
    }

    /* access modifiers changed from: package-private */
    public String formatDate(TemplateDateModel templateDateModel, Expression expression) throws TemplateModelException {
        try {
            boolean isSQLDateOrTimeClass = isSQLDateOrTimeClass(EvalUtil.modelToDate(templateDateModel, expression).getClass());
            return getTemplateDateFormat(templateDateModel.getDateType(), isSQLDateOrTimeClass, shouldUseSQLDTTimeZone(isSQLDateOrTimeClass), expression).format(templateDateModel);
        } catch (UnknownDateTypeFormattingUnsupportedException e) {
            throw MessageUtil.newCantFormatUnknownTypeDateException(expression, e);
        } catch (UnformattableDateException e2) {
            throw MessageUtil.newCantFormatDateException(expression, e2);
        }
    }

    /* access modifiers changed from: package-private */
    public String formatDate(TemplateDateModel templateDateModel, String str, Expression expression) throws TemplateModelException {
        boolean isSQLDateOrTimeClass = isSQLDateOrTimeClass(EvalUtil.modelToDate(templateDateModel, expression).getClass());
        try {
            return getTemplateDateFormat(templateDateModel.getDateType(), isSQLDateOrTimeClass, shouldUseSQLDTTimeZone(isSQLDateOrTimeClass), str, null).format(templateDateModel);
        } catch (UnknownDateTypeFormattingUnsupportedException e) {
            throw MessageUtil.newCantFormatUnknownTypeDateException(expression, e);
        } catch (UnformattableDateException e2) {
            throw MessageUtil.newCantFormatDateException(expression, e2);
        }
    }

    /* access modifiers changed from: package-private */
    public TemplateDateFormat getTemplateDateFormat(int i, Class cls, Expression expression) throws TemplateModelException {
        try {
            boolean isSQLDateOrTimeClass = isSQLDateOrTimeClass(cls);
            return getTemplateDateFormat(i, isSQLDateOrTimeClass, shouldUseSQLDTTimeZone(isSQLDateOrTimeClass), expression);
        } catch (UnknownDateTypeFormattingUnsupportedException e) {
            throw MessageUtil.newCantFormatUnknownTypeDateException(expression, e);
        }
    }

    private TemplateDateFormat getTemplateDateFormat(int i, boolean z, boolean z2, Expression expression) throws TemplateModelException, UnknownDateTypeFormattingUnsupportedException {
        String str;
        String str2;
        if (i != 0) {
            int cachedTemplateDateFormatIndex = getCachedTemplateDateFormatIndex(i, z, z2);
            TemplateDateFormat[] templateDateFormatArr = this.cachedTemplateDateFormats;
            if (templateDateFormatArr == null) {
                templateDateFormatArr = new TemplateDateFormat[16];
                this.cachedTemplateDateFormats = templateDateFormatArr;
            }
            TemplateDateFormat templateDateFormat = templateDateFormatArr[cachedTemplateDateFormatIndex];
            if (templateDateFormat != null) {
                return templateDateFormat;
            }
            if (i == 1) {
                str2 = getTimeFormat();
                str = "time_format";
            } else if (i == 2) {
                str2 = getDateFormat();
                str = "date_format";
            } else if (i == 3) {
                str2 = getDateTimeFormat();
                str = "datetime_format";
            } else {
                throw new _TemplateModelException(new Object[]{"Invalid date type enum: ", new Integer(i)});
            }
            TemplateDateFormat templateDateFormat2 = getTemplateDateFormat(i, z, z2, str2, str);
            templateDateFormatArr[cachedTemplateDateFormatIndex] = templateDateFormat2;
            return templateDateFormat2;
        }
        throw MessageUtil.newCantFormatUnknownTypeDateException(expression, null);
    }

    /* access modifiers changed from: package-private */
    public TemplateDateFormat getTemplateDateFormat(int i, Class cls, String str, Expression expression) throws TemplateModelException {
        try {
            boolean isSQLDateOrTimeClass = isSQLDateOrTimeClass(cls);
            return getTemplateDateFormat(i, isSQLDateOrTimeClass, shouldUseSQLDTTimeZone(isSQLDateOrTimeClass), str, null);
        } catch (UnknownDateTypeFormattingUnsupportedException e) {
            throw MessageUtil.newCantFormatUnknownTypeDateException(expression, e);
        }
    }

    private TemplateDateFormat getTemplateDateFormat(int i, boolean z, boolean z2, String str, String str2) throws TemplateModelException, UnknownDateTypeFormattingUnsupportedException {
        TemplateDateFormatFactory templateDateFormatFactory;
        int length = str.length();
        TimeZone sQLDateAndTimeTimeZone = z2 ? getSQLDateAndTimeTimeZone() : getTimeZone();
        if (length > 1 && str.charAt(0) == 'x' && str.charAt(1) == 's') {
            templateDateFormatFactory = z2 ? this.cachedSQLDTXSTemplateDateFormatFactory : this.cachedXSTemplateDateFormatFactory;
            if (templateDateFormatFactory == null) {
                templateDateFormatFactory = new XSTemplateDateFormatFactory(sQLDateAndTimeTimeZone);
                if (z2) {
                    this.cachedSQLDTXSTemplateDateFormatFactory = (XSTemplateDateFormatFactory) templateDateFormatFactory;
                } else {
                    this.cachedXSTemplateDateFormatFactory = (XSTemplateDateFormatFactory) templateDateFormatFactory;
                }
            }
        } else if (length > 2 && str.charAt(0) == 'i' && str.charAt(1) == 's' && str.charAt(2) == 'o') {
            templateDateFormatFactory = z2 ? this.cachedSQLDTISOTemplateDateFormatFactory : this.cachedISOTemplateDateFormatFactory;
            if (templateDateFormatFactory == null) {
                templateDateFormatFactory = new ISOTemplateDateFormatFactory(sQLDateAndTimeTimeZone);
                if (z2) {
                    this.cachedSQLDTISOTemplateDateFormatFactory = (ISOTemplateDateFormatFactory) templateDateFormatFactory;
                } else {
                    this.cachedISOTemplateDateFormatFactory = (ISOTemplateDateFormatFactory) templateDateFormatFactory;
                }
            }
        } else {
            templateDateFormatFactory = z2 ? this.cachedSQLDTJavaTemplateDateFormatFactory : this.cachedJavaTemplateDateFormatFactory;
            if (templateDateFormatFactory == null) {
                templateDateFormatFactory = new JavaTemplateDateFormatFactory(sQLDateAndTimeTimeZone, getLocale());
                if (z2) {
                    this.cachedSQLDTJavaTemplateDateFormatFactory = (JavaTemplateDateFormatFactory) templateDateFormatFactory;
                } else {
                    this.cachedJavaTemplateDateFormatFactory = (JavaTemplateDateFormatFactory) templateDateFormatFactory;
                }
            }
        }
        try {
            return templateDateFormatFactory.get(i, z, str);
        } catch (ParseException e) {
            Throwable cause = e.getCause();
            Object[] objArr = new Object[4];
            objArr[0] = str2 == null ? "Malformed date/time format descriptor: " : new Object[]{"The value of the \"", str2, "\" FreeMarker configuration setting is a malformed date/time format descriptor: "};
            objArr[1] = new _DelayedJQuote(str);
            objArr[2] = ". Reason given: ";
            objArr[3] = e.getMessage();
            throw new _TemplateModelException(cause, objArr);
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean shouldUseSQLDTTZ(Class cls) {
        Class cls2 = class$java$util$Date;
        if (cls2 == null) {
            cls2 = class$("java.util.Date");
            class$java$util$Date = cls2;
        }
        return cls != cls2 && !isSQLDateAndTimeTimeZoneSameAsNormal() && isSQLDateOrTimeClass(cls);
    }

    private boolean shouldUseSQLDTTimeZone(boolean z) {
        return z && !isSQLDateAndTimeTimeZoneSameAsNormal();
    }

    private static boolean isSQLDateOrTimeClass(Class cls) {
        Class cls2 = class$java$util$Date;
        if (cls2 == null) {
            cls2 = class$("java.util.Date");
            class$java$util$Date = cls2;
        }
        if (cls != cls2) {
            Class cls3 = class$java$sql$Date;
            if (cls3 == null) {
                cls3 = class$("java.sql.Date");
                class$java$sql$Date = cls3;
            }
            if (cls != cls3) {
                Class cls4 = class$java$sql$Time;
                if (cls4 == null) {
                    cls4 = class$("java.sql.Time");
                    class$java$sql$Time = cls4;
                }
                if (cls != cls4) {
                    Class cls5 = class$java$sql$Timestamp;
                    if (cls5 == null) {
                        cls5 = class$("java.sql.Timestamp");
                        class$java$sql$Timestamp = cls5;
                    }
                    if (cls != cls5) {
                        Class cls6 = class$java$sql$Date;
                        if (cls6 == null) {
                            cls6 = class$("java.sql.Date");
                            class$java$sql$Date = cls6;
                        }
                        if (!cls6.isAssignableFrom(cls)) {
                            Class cls7 = class$java$sql$Time;
                            if (cls7 == null) {
                                cls7 = class$("java.sql.Time");
                                class$java$sql$Time = cls7;
                            }
                            if (cls7.isAssignableFrom(cls)) {
                                return true;
                            }
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public DateUtil.DateToISO8601CalendarFactory getISOBuiltInCalendarFactory() {
        if (this.isoBuiltInCalendarFactory == null) {
            this.isoBuiltInCalendarFactory = new DateUtil.TrivialDateToISO8601CalendarFactory();
        }
        return this.isoBuiltInCalendarFactory;
    }

    public NumberFormat getCNumberFormat() {
        if (this.cNumberFormat == null) {
            this.cNumberFormat = (DecimalFormat) C_NUMBER_FORMAT.clone();
        }
        return this.cNumberFormat;
    }

    /* access modifiers changed from: package-private */
    public TemplateTransformModel getTransform(Expression expression) throws TemplateException {
        TemplateModel eval = expression.eval(this);
        if (eval instanceof TemplateTransformModel) {
            return (TemplateTransformModel) eval;
        }
        if (expression instanceof Identifier) {
            TemplateModel sharedVariable = getConfiguration().getSharedVariable(expression.toString());
            if (sharedVariable instanceof TemplateTransformModel) {
                return (TemplateTransformModel) sharedVariable;
            }
        }
        return null;
    }

    public TemplateModel getLocalVariable(String str) throws TemplateModelException {
        ArrayList arrayList = this.localContextStack;
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                TemplateModel localVariable = ((LocalContext) this.localContextStack.get(size)).getLocalVariable(str);
                if (localVariable != null) {
                    return localVariable;
                }
            }
        }
        Macro.Context context = this.currentMacroContext;
        if (context == null) {
            return null;
        }
        return context.getLocalVariable(str);
    }

    public TemplateModel getVariable(String str) throws TemplateModelException {
        TemplateModel localVariable = getLocalVariable(str);
        if (localVariable == null) {
            localVariable = this.currentNamespace.get(str);
        }
        return localVariable == null ? getGlobalVariable(str) : localVariable;
    }

    public TemplateModel getGlobalVariable(String str) throws TemplateModelException {
        TemplateModel templateModel = this.globalNamespace.get(str);
        if (templateModel == null) {
            templateModel = this.rootDataModel.get(str);
        }
        return templateModel == null ? getConfiguration().getSharedVariable(str) : templateModel;
    }

    public void setGlobalVariable(String str, TemplateModel templateModel) {
        this.globalNamespace.put(str, templateModel);
    }

    public void setVariable(String str, TemplateModel templateModel) {
        this.currentNamespace.put(str, templateModel);
    }

    public void setLocalVariable(String str, TemplateModel templateModel) {
        Macro.Context context = this.currentMacroContext;
        if (context != null) {
            context.setLocalVar(str, templateModel);
            return;
        }
        throw new IllegalStateException("Not executing macro body");
    }

    public Set getKnownVariableNames() throws TemplateModelException {
        Set sharedVariableNames = getConfiguration().getSharedVariableNames();
        TemplateHashModel templateHashModel = this.rootDataModel;
        if (templateHashModel instanceof TemplateHashModelEx) {
            TemplateModelIterator it = ((TemplateHashModelEx) templateHashModel).keys().iterator();
            while (it.hasNext()) {
                sharedVariableNames.add(((TemplateScalarModel) it.next()).getAsString());
            }
        }
        TemplateModelIterator it2 = this.globalNamespace.keys().iterator();
        while (it2.hasNext()) {
            sharedVariableNames.add(((TemplateScalarModel) it2.next()).getAsString());
        }
        TemplateModelIterator it3 = this.currentNamespace.keys().iterator();
        while (it3.hasNext()) {
            sharedVariableNames.add(((TemplateScalarModel) it3.next()).getAsString());
        }
        Macro.Context context = this.currentMacroContext;
        if (context != null) {
            sharedVariableNames.addAll(context.getLocalVariableNames());
        }
        ArrayList arrayList = this.localContextStack;
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                sharedVariableNames.addAll(((LocalContext) this.localContextStack.get(size)).getLocalVariableNames());
            }
        }
        return sharedVariableNames;
    }

    public void outputInstructionStack(PrintWriter printWriter) {
        outputInstructionStack(getInstructionStackSnapshot(), false, printWriter);
        printWriter.flush();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x002a A[Catch:{ IOException -> 0x00d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0072 A[Catch:{ IOException -> 0x00d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0087 A[Catch:{ IOException -> 0x00d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00b7 A[Catch:{ IOException -> 0x00d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void outputInstructionStack(freemarker.core.TemplateElement[] r13, boolean r14, java.io.Writer r15) {
        /*
            boolean r0 = r15 instanceof java.io.PrintWriter
            if (r0 == 0) goto L_0x0006
            r0 = r15
            goto L_0x0007
        L_0x0006:
            r0 = 0
        L_0x0007:
            java.io.PrintWriter r0 = (java.io.PrintWriter) r0
            java.io.PrintWriter r0 = (java.io.PrintWriter) r0
            r1 = 10
            if (r13 == 0) goto L_0x00c1
            int r2 = r13.length     // Catch:{ IOException -> 0x00d0 }
            r3 = 9
            if (r14 == 0) goto L_0x001a
            if (r2 > r1) goto L_0x0017
            goto L_0x001a
        L_0x0017:
            r4 = 9
            goto L_0x001b
        L_0x001a:
            r4 = r2
        L_0x001b:
            r5 = 0
            r6 = 1
            if (r14 == 0) goto L_0x0023
            if (r4 >= r2) goto L_0x0023
            r14 = 1
            goto L_0x0024
        L_0x0023:
            r14 = 0
        L_0x0024:
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
        L_0x0028:
            if (r7 >= r2) goto L_0x0070
            r11 = r13[r7]     // Catch:{ IOException -> 0x00d0 }
            if (r7 <= 0) goto L_0x0032
            boolean r12 = r11 instanceof freemarker.core.BodyInstruction     // Catch:{ IOException -> 0x00d0 }
            if (r12 != 0) goto L_0x003c
        L_0x0032:
            if (r7 <= r6) goto L_0x003e
            int r12 = r7 + -1
            r12 = r13[r12]     // Catch:{ IOException -> 0x00d0 }
            boolean r12 = r12 instanceof freemarker.core.BodyInstruction     // Catch:{ IOException -> 0x00d0 }
            if (r12 == 0) goto L_0x003e
        L_0x003c:
            r12 = 1
            goto L_0x003f
        L_0x003e:
            r12 = 0
        L_0x003f:
            if (r10 >= r4) goto L_0x006b
            if (r12 == 0) goto L_0x0049
            if (r14 != 0) goto L_0x0046
            goto L_0x0049
        L_0x0046:
            int r9 = r9 + 1
            goto L_0x006d
        L_0x0049:
            if (r7 != 0) goto L_0x004e
            java.lang.String r12 = "\t- Failed at: "
            goto L_0x0055
        L_0x004e:
            if (r12 == 0) goto L_0x0053
            java.lang.String r12 = "\t~ Reached through: "
            goto L_0x0055
        L_0x0053:
            java.lang.String r12 = "\t- Reached through: "
        L_0x0055:
            r15.write(r12)     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r11 = instructionStackItemToString(r11)     // Catch:{ IOException -> 0x00d0 }
            r15.write(r11)     // Catch:{ IOException -> 0x00d0 }
            if (r0 == 0) goto L_0x0065
            r0.println()     // Catch:{ IOException -> 0x00d0 }
            goto L_0x0068
        L_0x0065:
            r15.write(r1)     // Catch:{ IOException -> 0x00d0 }
        L_0x0068:
            int r10 = r10 + 1
            goto L_0x006d
        L_0x006b:
            int r8 = r8 + 1
        L_0x006d:
            int r7 = r7 + 1
            goto L_0x0028
        L_0x0070:
            if (r8 <= 0) goto L_0x0085
            java.lang.String r13 = "\t... (Had "
            r15.write(r13)     // Catch:{ IOException -> 0x00d0 }
            int r8 = r8 + r9
            java.lang.String r13 = java.lang.String.valueOf(r8)     // Catch:{ IOException -> 0x00d0 }
            r15.write(r13)     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r13 = " more, hidden for tersenes)"
            r15.write(r13)     // Catch:{ IOException -> 0x00d0 }
            r5 = 1
        L_0x0085:
            if (r9 <= 0) goto L_0x00b5
            if (r5 == 0) goto L_0x008f
            r13 = 32
            r15.write(r13)     // Catch:{ IOException -> 0x00d0 }
            goto L_0x0092
        L_0x008f:
            r15.write(r3)     // Catch:{ IOException -> 0x00d0 }
        L_0x0092:
            java.lang.StringBuffer r13 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x00d0 }
            r13.<init>()     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r14 = "(Hidden "
            r13.append(r14)     // Catch:{ IOException -> 0x00d0 }
            r13.append(r9)     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r14 = " \"~\" lines for terseness)"
            r13.append(r14)     // Catch:{ IOException -> 0x00d0 }
            java.lang.String r13 = r13.toString()     // Catch:{ IOException -> 0x00d0 }
            r15.write(r13)     // Catch:{ IOException -> 0x00d0 }
            if (r0 == 0) goto L_0x00b1
            r0.println()     // Catch:{ IOException -> 0x00d0 }
            goto L_0x00b4
        L_0x00b1:
            r15.write(r1)     // Catch:{ IOException -> 0x00d0 }
        L_0x00b4:
            r5 = 1
        L_0x00b5:
            if (r5 == 0) goto L_0x00d8
            if (r0 == 0) goto L_0x00bd
            r0.println()     // Catch:{ IOException -> 0x00d0 }
            goto L_0x00d8
        L_0x00bd:
            r15.write(r1)     // Catch:{ IOException -> 0x00d0 }
            goto L_0x00d8
        L_0x00c1:
            java.lang.String r13 = "(The stack was empty)"
            r15.write(r13)     // Catch:{ IOException -> 0x00d0 }
            if (r0 == 0) goto L_0x00cc
            r0.println()     // Catch:{ IOException -> 0x00d0 }
            goto L_0x00d8
        L_0x00cc:
            r15.write(r1)     // Catch:{ IOException -> 0x00d0 }
            goto L_0x00d8
        L_0x00d0:
            r13 = move-exception
            freemarker.log.Logger r14 = freemarker.core.Environment.LOG
            java.lang.String r15 = "Failed to print FTL stack trace"
            r14.error(r15, r13)
        L_0x00d8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.Environment.outputInstructionStack(freemarker.core.TemplateElement[], boolean, java.io.Writer):void");
    }

    /* access modifiers changed from: package-private */
    public TemplateElement[] getInstructionStackSnapshot() {
        int size = this.instructionStack.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            TemplateElement templateElement = (TemplateElement) this.instructionStack.get(i2);
            if (i2 == size || templateElement.isShownInStackTrace()) {
                i++;
            }
        }
        if (i == 0) {
            return null;
        }
        TemplateElement[] templateElementArr = new TemplateElement[i];
        int i3 = i - 1;
        for (int i4 = 0; i4 < size; i4++) {
            TemplateElement templateElement2 = (TemplateElement) this.instructionStack.get(i4);
            if (i4 == size || templateElement2.isShownInStackTrace()) {
                templateElementArr[i3] = templateElement2;
                i3--;
            }
        }
        return templateElementArr;
    }

    static String instructionStackItemToString(TemplateElement templateElement) {
        StringBuffer stringBuffer = new StringBuffer();
        appendInstructionStackItem(templateElement, stringBuffer);
        return stringBuffer.toString();
    }

    static void appendInstructionStackItem(TemplateElement templateElement, StringBuffer stringBuffer) {
        stringBuffer.append(MessageUtil.shorten(templateElement.getDescription(), 40));
        stringBuffer.append("  [");
        Macro enclosingMacro = getEnclosingMacro(templateElement);
        if (enclosingMacro != null) {
            stringBuffer.append(MessageUtil.formatLocationForEvaluationError(enclosingMacro, templateElement.beginLine, templateElement.beginColumn));
        } else {
            stringBuffer.append(MessageUtil.formatLocationForEvaluationError(templateElement.getTemplate(), templateElement.beginLine, templateElement.beginColumn));
        }
        stringBuffer.append("]");
    }

    private static Macro getEnclosingMacro(TemplateElement templateElement) {
        while (templateElement != null) {
            if (templateElement instanceof Macro) {
                return (Macro) templateElement;
            }
            templateElement = templateElement.getParentElement();
        }
        return null;
    }

    private void pushLocalContext(LocalContext localContext) {
        if (this.localContextStack == null) {
            this.localContextStack = new ArrayList();
        }
        this.localContextStack.add(localContext);
    }

    private void popLocalContext() {
        ArrayList arrayList = this.localContextStack;
        arrayList.remove(arrayList.size() - 1);
    }

    /* access modifiers changed from: package-private */
    public ArrayList getLocalContextStack() {
        return this.localContextStack;
    }

    public Namespace getNamespace(String str) {
        if (str.startsWith("/")) {
            str = str.substring(1);
        }
        HashMap hashMap = this.loadedLibs;
        if (hashMap != null) {
            return (Namespace) hashMap.get(str);
        }
        return null;
    }

    public Namespace getMainNamespace() {
        return this.mainNamespace;
    }

    public Namespace getCurrentNamespace() {
        return this.currentNamespace;
    }

    public Namespace getGlobalNamespace() {
        return this.globalNamespace;
    }

    public TemplateHashModel getDataModel() {
        final C32912 r0 = new TemplateHashModel() {
            /* class freemarker.core.Environment.C32912 */

            public boolean isEmpty() {
                return false;
            }

            public TemplateModel get(String str) throws TemplateModelException {
                TemplateModel templateModel = Environment.this.rootDataModel.get(str);
                return templateModel == null ? Environment.this.getConfiguration().getSharedVariable(str) : templateModel;
            }
        };
        return this.rootDataModel instanceof TemplateHashModelEx ? new TemplateHashModelEx() {
            /* class freemarker.core.Environment.C32923 */

            public boolean isEmpty() throws TemplateModelException {
                return r0.isEmpty();
            }

            public TemplateModel get(String str) throws TemplateModelException {
                return r0.get(str);
            }

            public TemplateCollectionModel values() throws TemplateModelException {
                return ((TemplateHashModelEx) Environment.this.rootDataModel).values();
            }

            public TemplateCollectionModel keys() throws TemplateModelException {
                return ((TemplateHashModelEx) Environment.this.rootDataModel).keys();
            }

            public int size() throws TemplateModelException {
                return ((TemplateHashModelEx) Environment.this.rootDataModel).size();
            }
        } : r0;
    }

    public TemplateHashModel getGlobalVariables() {
        return new TemplateHashModel() {
            /* class freemarker.core.Environment.C32934 */

            public boolean isEmpty() {
                return false;
            }

            public TemplateModel get(String str) throws TemplateModelException {
                TemplateModel templateModel = Environment.this.globalNamespace.get(str);
                if (templateModel == null) {
                    templateModel = Environment.this.rootDataModel.get(str);
                }
                return templateModel == null ? Environment.this.getConfiguration().getSharedVariable(str) : templateModel;
            }
        };
    }

    private void pushElement(TemplateElement templateElement) {
        this.instructionStack.add(templateElement);
    }

    private void popElement() {
        ArrayList arrayList = this.instructionStack;
        arrayList.remove(arrayList.size() - 1);
    }

    /* access modifiers changed from: package-private */
    public void replaceElementStackTop(TemplateElement templateElement) {
        ArrayList arrayList = this.instructionStack;
        arrayList.set(arrayList.size() - 1, templateElement);
    }

    public TemplateNodeModel getCurrentVisitorNode() {
        return this.currentVisitorNode;
    }

    public void setCurrentVisitorNode(TemplateNodeModel templateNodeModel) {
        this.currentVisitorNode = templateNodeModel;
    }

    /* access modifiers changed from: package-private */
    public TemplateModel getNodeProcessor(TemplateNodeModel templateNodeModel) throws TemplateException {
        String nodeName = templateNodeModel.getNodeName();
        if (nodeName != null) {
            TemplateModel nodeProcessor = getNodeProcessor(nodeName, templateNodeModel.getNodeNamespace(), 0);
            if (nodeProcessor != null) {
                return nodeProcessor;
            }
            String nodeType = templateNodeModel.getNodeType();
            if (nodeType == null) {
                nodeType = Schema.DEFAULT_NAME;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("@");
            stringBuffer.append(nodeType);
            return getNodeProcessor(stringBuffer.toString(), (String) null, 0);
        }
        throw new _MiscTemplateException(this, "Node name is null.");
    }

    private TemplateModel getNodeProcessor(String str, String str2, int i) throws TemplateException {
        TemplateModel templateModel = null;
        while (i < this.nodeNamespaces.size()) {
            try {
                templateModel = getNodeProcessor((Namespace) this.nodeNamespaces.get(i), str, str2);
                if (templateModel != null) {
                    break;
                }
                i++;
            } catch (ClassCastException unused) {
                throw new _MiscTemplateException(this, "A \"using\" clause should contain a sequence of namespaces or strings that indicate the location of importable macro libraries.");
            }
        }
        if (templateModel != null) {
            this.nodeNamespaceIndex = i + 1;
            this.currentNodeName = str;
            this.currentNodeNS = str2;
        }
        return templateModel;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0065, code lost:
        if ((r2 instanceof freemarker.template.TemplateTransformModel) == false) goto L_0x0067;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private freemarker.template.TemplateModel getNodeProcessor(freemarker.core.Environment.Namespace r5, java.lang.String r6, java.lang.String r7) throws freemarker.template.TemplateException {
        /*
            r4 = this;
            r0 = 0
            if (r7 != 0) goto L_0x0012
            freemarker.template.TemplateModel r5 = r5.get(r6)
            boolean r6 = r5 instanceof freemarker.core.Macro
            if (r6 != 0) goto L_0x00a3
            boolean r6 = r5 instanceof freemarker.template.TemplateTransformModel
            if (r6 != 0) goto L_0x00a3
        L_0x000f:
            r5 = r0
            goto L_0x00a3
        L_0x0012:
            freemarker.template.Template r1 = r5.getTemplate()
            java.lang.String r2 = r1.getPrefixForNamespace(r7)
            if (r2 != 0) goto L_0x001d
            return r0
        L_0x001d:
            int r3 = r2.length()
            if (r3 <= 0) goto L_0x0044
            java.lang.StringBuffer r7 = new java.lang.StringBuffer
            r7.<init>()
            r7.append(r2)
            java.lang.String r1 = ":"
            r7.append(r1)
            r7.append(r6)
            java.lang.String r6 = r7.toString()
            freemarker.template.TemplateModel r5 = r5.get(r6)
            boolean r6 = r5 instanceof freemarker.core.Macro
            if (r6 != 0) goto L_0x00a3
            boolean r6 = r5 instanceof freemarker.template.TemplateTransformModel
            if (r6 != 0) goto L_0x00a3
            goto L_0x000f
        L_0x0044:
            int r2 = r7.length()
            if (r2 != 0) goto L_0x0067
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r3 = "N:"
            r2.append(r3)
            r2.append(r6)
            java.lang.String r2 = r2.toString()
            freemarker.template.TemplateModel r2 = r5.get(r2)
            boolean r3 = r2 instanceof freemarker.core.Macro
            if (r3 != 0) goto L_0x0068
            boolean r3 = r2 instanceof freemarker.template.TemplateTransformModel
            if (r3 != 0) goto L_0x0068
        L_0x0067:
            r2 = r0
        L_0x0068:
            java.lang.String r1 = r1.getDefaultNS()
            boolean r7 = r7.equals(r1)
            if (r7 == 0) goto L_0x0091
            java.lang.StringBuffer r7 = new java.lang.StringBuffer
            r7.<init>()
            java.lang.String r1 = "D:"
            r7.append(r1)
            r7.append(r6)
            java.lang.String r7 = r7.toString()
            freemarker.template.TemplateModel r7 = r5.get(r7)
            boolean r1 = r7 instanceof freemarker.core.Macro
            if (r1 != 0) goto L_0x0092
            boolean r1 = r7 instanceof freemarker.template.TemplateTransformModel
            if (r1 != 0) goto L_0x0092
            r7 = r0
            goto L_0x0092
        L_0x0091:
            r7 = r2
        L_0x0092:
            if (r7 != 0) goto L_0x00a2
            freemarker.template.TemplateModel r5 = r5.get(r6)
            boolean r6 = r5 instanceof freemarker.core.Macro
            if (r6 != 0) goto L_0x00a3
            boolean r6 = r5 instanceof freemarker.template.TemplateTransformModel
            if (r6 != 0) goto L_0x00a3
            goto L_0x000f
        L_0x00a2:
            r5 = r7
        L_0x00a3:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.Environment.getNodeProcessor(freemarker.core.Environment$Namespace, java.lang.String, java.lang.String):freemarker.template.TemplateModel");
    }

    public void include(String str, String str2, boolean z) throws IOException, TemplateException {
        include(getTemplateForInclusion(str, str2, z));
    }

    public Template getTemplateForInclusion(String str, String str2, boolean z) throws IOException {
        return getTemplateForInclusion(str, str2, z, false);
    }

    public Template getTemplateForInclusion(String str, String str2, boolean z, boolean z2) throws IOException {
        Template template = getTemplate();
        if (str2 == null && (str2 = template.getEncoding()) == null) {
            str2 = getConfiguration().getEncoding(getLocale());
        }
        return getConfiguration().getTemplate(str, getLocale(), template.getCustomLookupCondition(), str2, z, z2);
    }

    public void include(Template template) throws TemplateException, IOException {
        boolean isIcI2322OrLater = isIcI2322OrLater();
        Template template2 = getTemplate();
        if (isIcI2322OrLater) {
            setParent(super);
        } else {
            this.legacyParent = super;
        }
        importMacros(template);
        try {
            visit(template.getRootTreeNode());
            if (!isIcI2322OrLater) {
                this.legacyParent = super;
            }
        } finally {
            if (isIcI2322OrLater) {
                setParent(super);
            } else {
                this.legacyParent = super;
            }
        }
    }

    public Namespace importLib(String str, String str2) throws IOException, TemplateException {
        return importLib(getTemplateForImporting(str), str2);
    }

    public Template getTemplateForImporting(String str) throws IOException {
        return getTemplateForInclusion(str, null, true);
    }

    public Namespace importLib(Template template, String str) throws IOException, TemplateException {
        if (this.loadedLibs == null) {
            this.loadedLibs = new HashMap();
        }
        String name = template.getName();
        Namespace namespace = (Namespace) this.loadedLibs.get(name);
        if (namespace == null) {
            Namespace namespace2 = new Namespace(template);
            if (str != null) {
                this.currentNamespace.put(str, namespace2);
                if (this.currentNamespace == this.mainNamespace) {
                    this.globalNamespace.put(str, namespace2);
                }
            }
            Namespace namespace3 = this.currentNamespace;
            this.currentNamespace = namespace2;
            this.loadedLibs.put(name, this.currentNamespace);
            Writer writer = this.out;
            this.out = NullWriter.INSTANCE;
            try {
                include(template);
            } finally {
                this.out = writer;
                this.currentNamespace = namespace3;
            }
        } else if (str != null) {
            setVariable(str, namespace);
        }
        return (Namespace) this.loadedLibs.get(name);
    }

    public String toFullTemplateName(String str, String str2) throws MalformedTemplateNameException {
        if (isClassicCompatible()) {
            return str2;
        }
        return _CacheAPI.toAbsoluteName(getConfiguration().getTemplateNameFormat(), str, str2);
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public String renderElementToString(TemplateElement templateElement) throws IOException, TemplateException {
        Writer writer = this.out;
        try {
            StringWriter stringWriter = new StringWriter();
            this.out = stringWriter;
            visit(templateElement);
            String stringWriter2 = stringWriter.toString();
            this.out = writer;
            return stringWriter2;
        } catch (Throwable th) {
            this.out = writer;
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public void importMacros(Template template) {
        for (Macro macro : template.getMacros().values()) {
            visitMacroDef(macro);
        }
    }

    public String getNamespaceForPrefix(String str) {
        return this.currentNamespace.getTemplate().getNamespaceForPrefix(str);
    }

    public String getPrefixForNamespace(String str) {
        return this.currentNamespace.getTemplate().getPrefixForNamespace(str);
    }

    public String getDefaultNS() {
        return this.currentNamespace.getTemplate().getDefaultNS();
    }

    public Object __getitem__(String str) throws TemplateModelException {
        return BeansWrapper.getDefaultInstance().unwrap(getVariable(str));
    }

    public void __setitem__(String str, Object obj) throws TemplateException {
        setGlobalVariable(str, getObjectWrapper().wrap(obj));
    }

    final class NestedElementTemplateDirectiveBody implements TemplateDirectiveBody {
        private final TemplateElement element;

        private NestedElementTemplateDirectiveBody(TemplateElement templateElement) {
            this.element = templateElement;
        }

        public void render(Writer writer) throws TemplateException, IOException {
            Writer access$300 = Environment.this.out;
            Writer unused = Environment.this.out = writer;
            try {
                Environment.this.visit(this.element);
            } finally {
                Writer unused2 = Environment.this.out = access$300;
            }
        }

        public TemplateElement getElement() {
            return this.element;
        }
    }

    private static final class NumberFormatKey {
        private final Locale locale;
        private final String pattern;

        NumberFormatKey(String str, Locale locale2) {
            this.pattern = str;
            this.locale = locale2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof NumberFormatKey)) {
                return false;
            }
            NumberFormatKey numberFormatKey = (NumberFormatKey) obj;
            if (!numberFormatKey.pattern.equals(this.pattern) || !numberFormatKey.locale.equals(this.locale)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.pattern.hashCode() ^ this.locale.hashCode();
        }
    }

    public class Namespace extends SimpleHash {
        private final Template template;

        Namespace() {
            this.template = Environment.this.getTemplate();
        }

        Namespace(Template template2) {
            this.template = template2;
        }

        public Template getTemplate() {
            Template template2 = this.template;
            return template2 == null ? Environment.this.getTemplate() : template2;
        }
    }

    private boolean isIcI2322OrLater() {
        return getConfiguration().getIncompatibleImprovements().intValue() < _TemplateAPI.VERSION_INT_2_3_22;
    }

    /* access modifiers changed from: package-private */
    public boolean getFastInvalidReferenceExceptions() {
        return this.fastInvalidReferenceExceptions;
    }

    /* access modifiers changed from: package-private */
    public boolean setFastInvalidReferenceExceptions(boolean z) {
        boolean z2 = this.fastInvalidReferenceExceptions;
        this.fastInvalidReferenceExceptions = z;
        return z2;
    }
}
