package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.ext.beans.BeanModel;
import freemarker.ext.beans.OverloadedMethodsModel;
import freemarker.ext.beans.SimpleMethodModel;
import freemarker.ext.beans._BeansAPI;
import freemarker.template.SimpleDate;
import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateCollectionModelEx;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelWithAPISupport;
import freemarker.template.TemplateNodeModel;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.TemplateTransformModel;
import freemarker.template._TemplateAPI;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

class BuiltInsForMultipleTypes {
    static /* synthetic */ Class class$freemarker$core$Macro;
    static /* synthetic */ Class class$freemarker$template$TemplateBooleanModel;
    static /* synthetic */ Class class$freemarker$template$TemplateCollectionModelEx;
    static /* synthetic */ Class class$freemarker$template$TemplateDateModel;
    static /* synthetic */ Class class$freemarker$template$TemplateHashModelEx;
    static /* synthetic */ Class class$freemarker$template$TemplateNumberModel;
    static /* synthetic */ Class class$freemarker$template$TemplateScalarModel;
    static /* synthetic */ Class class$freemarker$template$TemplateSequenceModel;
    static /* synthetic */ Class class$java$util$Date;

    static class cBI extends AbstractCBI implements ICIChainMember {
        private final BIBeforeICE2d3d21 prevICIObj = new BIBeforeICE2d3d21();

        cBI() {
        }

        static class BIBeforeICE2d3d21 extends AbstractCBI {
            BIBeforeICE2d3d21() {
            }

            /* access modifiers changed from: protected */
            public TemplateModel formatNumber(Environment environment, TemplateModel templateModel) throws TemplateModelException {
                Number modelToNumber = EvalUtil.modelToNumber((TemplateNumberModel) templateModel, this.target);
                if ((modelToNumber instanceof Integer) || (modelToNumber instanceof Long)) {
                    return new SimpleScalar(modelToNumber.toString());
                }
                return new SimpleScalar(environment.getCNumberFormat().format(modelToNumber));
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            Class cls;
            Class cls2;
            TemplateModel eval = this.target.eval(environment);
            if (eval instanceof TemplateNumberModel) {
                return formatNumber(environment, eval);
            }
            if (eval instanceof TemplateBooleanModel) {
                return new SimpleScalar(((TemplateBooleanModel) eval).getAsBoolean() ? "true" : "false");
            }
            Expression expression = this.target;
            Class[] clsArr = new Class[2];
            if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel == null) {
                cls = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateNumberModel");
                BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel = cls;
            } else {
                cls = BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel;
            }
            clsArr[0] = cls;
            if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel == null) {
                cls2 = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateBooleanModel");
                BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel = cls2;
            } else {
                cls2 = BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel;
            }
            clsArr[1] = cls2;
            throw new UnexpectedTypeException(expression, eval, "number or boolean", clsArr, environment);
        }

        /* access modifiers changed from: protected */
        public TemplateModel formatNumber(Environment environment, TemplateModel templateModel) throws TemplateModelException {
            Number modelToNumber = EvalUtil.modelToNumber((TemplateNumberModel) templateModel, this.target);
            if ((modelToNumber instanceof Integer) || (modelToNumber instanceof Long)) {
                return new SimpleScalar(modelToNumber.toString());
            }
            if (modelToNumber instanceof Double) {
                double doubleValue = modelToNumber.doubleValue();
                if (doubleValue == Double.POSITIVE_INFINITY) {
                    return new SimpleScalar("INF");
                }
                if (doubleValue == Double.NEGATIVE_INFINITY) {
                    return new SimpleScalar("-INF");
                }
                if (Double.isNaN(doubleValue)) {
                    return new SimpleScalar("NaN");
                }
            } else if (modelToNumber instanceof Float) {
                float floatValue = modelToNumber.floatValue();
                if (floatValue == Float.POSITIVE_INFINITY) {
                    return new SimpleScalar("INF");
                }
                if (floatValue == Float.NEGATIVE_INFINITY) {
                    return new SimpleScalar("-INF");
                }
                if (Float.isNaN(floatValue)) {
                    return new SimpleScalar("NaN");
                }
            }
            return new SimpleScalar(environment.getCNumberFormat().format(modelToNumber));
        }

        public int getMinimumICIVersion() {
            return _TemplateAPI.VERSION_INT_2_3_21;
        }

        public Object getPreviousICIChainMember() {
            return this.prevICIObj;
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    static class dateBI extends BuiltIn {
        /* access modifiers changed from: private */
        public final int dateType;

        private class DateParser implements TemplateDateModel, TemplateMethodModel, TemplateHashModel {
            private Date cachedValue;
            private final TemplateDateFormat defaultFormat;
            private final Environment env;
            private final String text;

            public boolean isEmpty() {
                return false;
            }

            DateParser(String str, Environment environment) throws TemplateModelException {
                Class cls;
                this.text = str;
                this.env = environment;
                int access$000 = dateBI.this.dateType;
                if (BuiltInsForMultipleTypes.class$java$util$Date == null) {
                    cls = BuiltInsForMultipleTypes.class$("java.util.Date");
                    BuiltInsForMultipleTypes.class$java$util$Date = cls;
                } else {
                    cls = BuiltInsForMultipleTypes.class$java$util$Date;
                }
                this.defaultFormat = environment.getTemplateDateFormat(access$000, cls, dateBI.this.target);
            }

            public Object exec(List list) throws TemplateModelException {
                dateBI.this.checkMethodArgCount(list, 1);
                return get((String) list.get(0));
            }

            public TemplateModel get(String str) throws TemplateModelException {
                Class cls;
                Environment environment = this.env;
                int access$000 = dateBI.this.dateType;
                if (BuiltInsForMultipleTypes.class$java$util$Date == null) {
                    cls = BuiltInsForMultipleTypes.class$("java.util.Date");
                    BuiltInsForMultipleTypes.class$java$util$Date = cls;
                } else {
                    cls = BuiltInsForMultipleTypes.class$java$util$Date;
                }
                return new SimpleDate(parse(environment.getTemplateDateFormat(access$000, cls, str, dateBI.this.target)), dateBI.this.dateType);
            }

            public Date getAsDate() throws TemplateModelException {
                if (this.cachedValue == null) {
                    this.cachedValue = parse(this.defaultFormat);
                }
                return this.cachedValue;
            }

            public int getDateType() {
                return dateBI.this.dateType;
            }

            private Date parse(TemplateDateFormat templateDateFormat) throws TemplateModelException {
                try {
                    return templateDateFormat.parse(this.text);
                } catch (ParseException e) {
                    Object[] objArr = new Object[8];
                    objArr[0] = "The string doesn't match the expected date/time/date-time format. The string to parse was: ";
                    objArr[1] = new _DelayedJQuote(this.text);
                    objArr[2] = ". ";
                    objArr[3] = "The expected format was: ";
                    objArr[4] = new _DelayedJQuote(templateDateFormat.getDescription());
                    objArr[5] = FileUtil.HIDDEN_PREFIX;
                    String str = "";
                    objArr[6] = e.getMessage() != null ? "\nThe nested reason given follows:\n" : str;
                    if (e.getMessage() != null) {
                        str = e.getMessage();
                    }
                    objArr[7] = str;
                    throw new _TemplateModelException(e, objArr);
                }
            }
        }

        dateBI(int i) {
            this.dateType = i;
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            if (!(eval instanceof TemplateDateModel)) {
                return new DateParser(this.target.evalAndCoerceToString(environment), environment);
            }
            TemplateDateModel templateDateModel = (TemplateDateModel) eval;
            int dateType2 = templateDateModel.getDateType();
            if (this.dateType == dateType2) {
                return eval;
            }
            if (dateType2 == 0 || dateType2 == 3) {
                return new SimpleDate(templateDateModel.getAsDate(), this.dateType);
            }
            throw new _MiscTemplateException(this, new Object[]{"Cannot convert ", TemplateDateModel.TYPE_NAMES.get(dateType2), " to ", TemplateDateModel.TYPE_NAMES.get(this.dateType)});
        }
    }

    static class apiBI extends BuiltIn {
        apiBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            if (environment.isAPIBuiltinEnabled()) {
                TemplateModel eval = this.target.eval(environment);
                if (eval instanceof TemplateModelWithAPISupport) {
                    return ((TemplateModelWithAPISupport) eval).getAPI();
                }
                this.target.assertNonNull(eval, environment);
                throw new APINotSupportedTemplateException(environment, this.target, eval);
            }
            throw new _MiscTemplateException(this, new Object[]{"Can't use ?api, because the \"", "api_builtin_enabled", "\" configuration setting is false. Think twice before you set it to true though. Especially, it shouldn't abussed for modifying Map-s and Collection-s."});
        }
    }

    static class has_apiBI extends BuiltIn {
        has_apiBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateModelWithAPISupport ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_booleanBI extends BuiltIn {
        is_booleanBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateBooleanModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_collectionBI extends BuiltIn {
        is_collectionBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateCollectionModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_collection_exBI extends BuiltIn {
        is_collection_exBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateCollectionModelEx ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_dateLikeBI extends BuiltIn {
        is_dateLikeBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateDateModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_dateOfTypeBI extends BuiltIn {
        private final int dateType;

        is_dateOfTypeBI(int i) {
            this.dateType = i;
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return (!(eval instanceof TemplateDateModel) || ((TemplateDateModel) eval).getDateType() != this.dateType) ? TemplateBooleanModel.FALSE : TemplateBooleanModel.TRUE;
        }
    }

    static class is_directiveBI extends BuiltIn {
        is_directiveBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return ((eval instanceof TemplateTransformModel) || (eval instanceof Macro) || (eval instanceof TemplateDirectiveModel)) ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_enumerableBI extends BuiltIn {
        is_enumerableBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return (((eval instanceof TemplateSequenceModel) || (eval instanceof TemplateCollectionModel)) && (_TemplateAPI.getTemplateLanguageVersionAsInt(this) < _TemplateAPI.VERSION_INT_2_3_21 || (!(eval instanceof SimpleMethodModel) && !(eval instanceof OverloadedMethodsModel)))) ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_hash_exBI extends BuiltIn {
        is_hash_exBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateHashModelEx ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_hashBI extends BuiltIn {
        is_hashBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateHashModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_indexableBI extends BuiltIn {
        is_indexableBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateSequenceModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_macroBI extends BuiltIn {
        is_macroBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof Macro ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_methodBI extends BuiltIn {
        is_methodBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateMethodModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_nodeBI extends BuiltIn {
        is_nodeBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateNodeModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_numberBI extends BuiltIn {
        is_numberBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateNumberModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_sequenceBI extends BuiltIn {
        is_sequenceBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateSequenceModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_stringBI extends BuiltIn {
        is_stringBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateScalarModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_transformBI extends BuiltIn {
        is_transformBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            this.target.assertNonNull(eval, environment);
            return eval instanceof TemplateTransformModel ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class namespaceBI extends BuiltIn {
        namespaceBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            Class cls;
            TemplateModel eval = this.target.eval(environment);
            if (eval instanceof Macro) {
                return environment.getMacroNamespace((Macro) eval);
            }
            Expression expression = this.target;
            Class[] clsArr = new Class[1];
            if (BuiltInsForMultipleTypes.class$freemarker$core$Macro == null) {
                cls = BuiltInsForMultipleTypes.class$("freemarker.core.Macro");
                BuiltInsForMultipleTypes.class$freemarker$core$Macro = cls;
            } else {
                cls = BuiltInsForMultipleTypes.class$freemarker$core$Macro;
            }
            clsArr[0] = cls;
            throw new UnexpectedTypeException(expression, eval, "macro or function", clsArr, environment);
        }
    }

    static class sizeBI extends BuiltIn {
        sizeBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            int i;
            Class cls;
            Class cls2;
            Class cls3;
            TemplateModel eval = this.target.eval(environment);
            if (eval instanceof TemplateSequenceModel) {
                i = ((TemplateSequenceModel) eval).size();
            } else if (eval instanceof TemplateCollectionModelEx) {
                i = ((TemplateCollectionModelEx) eval).size();
            } else if (eval instanceof TemplateHashModelEx) {
                i = ((TemplateHashModelEx) eval).size();
            } else {
                Expression expression = this.target;
                Class[] clsArr = new Class[3];
                if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateHashModelEx == null) {
                    cls = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateHashModelEx");
                    BuiltInsForMultipleTypes.class$freemarker$template$TemplateHashModelEx = cls;
                } else {
                    cls = BuiltInsForMultipleTypes.class$freemarker$template$TemplateHashModelEx;
                }
                clsArr[0] = cls;
                if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateSequenceModel == null) {
                    cls2 = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateSequenceModel");
                    BuiltInsForMultipleTypes.class$freemarker$template$TemplateSequenceModel = cls2;
                } else {
                    cls2 = BuiltInsForMultipleTypes.class$freemarker$template$TemplateSequenceModel;
                }
                clsArr[1] = cls2;
                if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateCollectionModelEx == null) {
                    cls3 = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateCollectionModelEx");
                    BuiltInsForMultipleTypes.class$freemarker$template$TemplateCollectionModelEx = cls3;
                } else {
                    cls3 = BuiltInsForMultipleTypes.class$freemarker$template$TemplateCollectionModelEx;
                }
                clsArr[2] = cls3;
                throw new UnexpectedTypeException(expression, eval, "extended-hash or sequence or extended collection", clsArr, environment);
            }
            return new SimpleNumber(i);
        }
    }

    static class stringBI extends BuiltIn {
        stringBI() {
        }

        private class BooleanFormatter implements TemplateScalarModel, TemplateMethodModel {
            private final TemplateBooleanModel bool;
            private final Environment env;

            BooleanFormatter(TemplateBooleanModel templateBooleanModel, Environment environment) {
                this.bool = templateBooleanModel;
                this.env = environment;
            }

            public Object exec(List list) throws TemplateModelException {
                stringBI.this.checkMethodArgCount(list, 2);
                return new SimpleScalar((String) list.get(this.bool.getAsBoolean() ^ true ? 1 : 0));
            }

            public String getAsString() throws TemplateModelException {
                TemplateBooleanModel templateBooleanModel = this.bool;
                if (templateBooleanModel instanceof TemplateScalarModel) {
                    return ((TemplateScalarModel) templateBooleanModel).getAsString();
                }
                try {
                    return this.env.formatBoolean(templateBooleanModel.getAsBoolean(), true);
                } catch (TemplateException e) {
                    throw new TemplateModelException((Exception) e);
                }
            }
        }

        private class DateFormatter implements TemplateScalarModel, TemplateHashModel, TemplateMethodModel {
            private String cachedValue;
            private final TemplateDateModel dateModel;
            private final TemplateDateFormat defaultFormat;
            private final Environment env;

            public boolean isEmpty() {
                return false;
            }

            DateFormatter(TemplateDateModel templateDateModel, Environment environment) throws TemplateModelException {
                TemplateDateFormat templateDateFormat;
                this.dateModel = templateDateModel;
                this.env = environment;
                int dateType = templateDateModel.getDateType();
                if (dateType == 0) {
                    templateDateFormat = null;
                } else {
                    templateDateFormat = environment.getTemplateDateFormat(dateType, EvalUtil.modelToDate(templateDateModel, stringBI.this.target).getClass(), stringBI.this.target);
                }
                this.defaultFormat = templateDateFormat;
            }

            public Object exec(List list) throws TemplateModelException {
                stringBI.this.checkMethodArgCount(list, 1);
                return get((String) list.get(0));
            }

            public TemplateModel get(String str) throws TemplateModelException {
                return new SimpleScalar(this.env.formatDate(this.dateModel, str, stringBI.this.target));
            }

            public String getAsString() throws TemplateModelException {
                if (this.cachedValue == null) {
                    try {
                        if (this.defaultFormat != null) {
                            this.cachedValue = this.defaultFormat.format(this.dateModel);
                        } else if (this.dateModel.getDateType() == 0) {
                            throw MessageUtil.newCantFormatUnknownTypeDateException(stringBI.this.target, null);
                        } else {
                            throw new BugException();
                        }
                    } catch (UnformattableDateException e) {
                        throw MessageUtil.newCantFormatDateException(stringBI.this.target, e);
                    }
                }
                return this.cachedValue;
            }
        }

        private class NumberFormatter implements TemplateScalarModel, TemplateHashModel, TemplateMethodModel {
            private String cachedValue;
            private final NumberFormat defaultFormat;
            private final Environment env;
            private final Number number;

            public boolean isEmpty() {
                return false;
            }

            NumberFormatter(Number number2, Environment environment) {
                this.number = number2;
                this.env = environment;
                this.defaultFormat = environment.getNumberFormatObject(environment.getNumberFormat());
            }

            public Object exec(List list) throws TemplateModelException {
                stringBI.this.checkMethodArgCount(list, 1);
                return get((String) list.get(0));
            }

            public TemplateModel get(String str) {
                return new SimpleScalar(this.env.getNumberFormatObject(str).format(this.number));
            }

            public String getAsString() {
                if (this.cachedValue == null) {
                    this.cachedValue = this.defaultFormat.format(this.number);
                }
                return this.cachedValue;
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            Class cls;
            Class cls2;
            Class cls3;
            Class cls4;
            TemplateModel eval = this.target.eval(environment);
            if (eval instanceof TemplateNumberModel) {
                return new NumberFormatter(EvalUtil.modelToNumber((TemplateNumberModel) eval, this.target), environment);
            }
            if (eval instanceof TemplateDateModel) {
                return new DateFormatter((TemplateDateModel) eval, environment);
            }
            if (eval instanceof SimpleScalar) {
                return eval;
            }
            if (eval instanceof TemplateBooleanModel) {
                return new BooleanFormatter((TemplateBooleanModel) eval, environment);
            }
            if (eval instanceof TemplateScalarModel) {
                return new SimpleScalar(((TemplateScalarModel) eval).getAsString());
            }
            if (environment.isClassicCompatible() && (eval instanceof BeanModel)) {
                return new SimpleScalar(_BeansAPI.getAsClassicCompatibleString((BeanModel) eval));
            }
            Expression expression = this.target;
            Class[] clsArr = new Class[4];
            if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel == null) {
                cls = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateNumberModel");
                BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel = cls;
            } else {
                cls = BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel;
            }
            clsArr[0] = cls;
            if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateDateModel == null) {
                cls2 = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateDateModel");
                BuiltInsForMultipleTypes.class$freemarker$template$TemplateDateModel = cls2;
            } else {
                cls2 = BuiltInsForMultipleTypes.class$freemarker$template$TemplateDateModel;
            }
            clsArr[1] = cls2;
            if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel == null) {
                cls3 = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateBooleanModel");
                BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel = cls3;
            } else {
                cls3 = BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel;
            }
            clsArr[2] = cls3;
            if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateScalarModel == null) {
                cls4 = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateScalarModel");
                BuiltInsForMultipleTypes.class$freemarker$template$TemplateScalarModel = cls4;
            } else {
                cls4 = BuiltInsForMultipleTypes.class$freemarker$template$TemplateScalarModel;
            }
            clsArr[3] = cls4;
            throw new UnexpectedTypeException(expression, eval, "number, date, boolean or string", clsArr, environment);
        }
    }

    private BuiltInsForMultipleTypes() {
    }

    static abstract class AbstractCBI extends BuiltIn {
        /* access modifiers changed from: protected */
        public abstract TemplateModel formatNumber(Environment environment, TemplateModel templateModel) throws TemplateModelException;

        AbstractCBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            Class cls;
            Class cls2;
            TemplateModel eval = this.target.eval(environment);
            if (eval instanceof TemplateNumberModel) {
                return formatNumber(environment, eval);
            }
            if (eval instanceof TemplateBooleanModel) {
                return new SimpleScalar(((TemplateBooleanModel) eval).getAsBoolean() ? "true" : "false");
            }
            Expression expression = this.target;
            Class[] clsArr = new Class[2];
            if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel == null) {
                cls = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateNumberModel");
                BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel = cls;
            } else {
                cls = BuiltInsForMultipleTypes.class$freemarker$template$TemplateNumberModel;
            }
            clsArr[0] = cls;
            if (BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel == null) {
                cls2 = BuiltInsForMultipleTypes.class$("freemarker.template.TemplateBooleanModel");
                BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel = cls2;
            } else {
                cls2 = BuiltInsForMultipleTypes.class$freemarker$template$TemplateBooleanModel;
            }
            clsArr[1] = cls2;
            throw new UnexpectedTypeException(expression, eval, "number or boolean", clsArr, environment);
        }
    }
}
