package freemarker.core;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.ObjectWrapper;
import freemarker.template.SimpleHash;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template._TemplateAPI;

class APINotSupportedTemplateException extends TemplateException {
    APINotSupportedTemplateException(Environment environment, Expression expression, TemplateModel templateModel) {
        super(null, environment, expression, buildDescription(environment, expression, templateModel));
    }

    protected static _ErrorDescriptionBuilder buildDescription(Environment environment, Expression expression, TemplateModel templateModel) {
        _ErrorDescriptionBuilder blame = new _ErrorDescriptionBuilder(new Object[]{"The value doesn't support ?api. See requirements in the FreeMarker Manual. (FTL type: ", new _DelayedFTLTypeDescription(templateModel), ", TemplateModel class: ", new _DelayedShortClassName(templateModel.getClass()), ", ObjectWapper: ", new _DelayedToString(environment.getObjectWrapper()), ")"}).blame(expression);
        if (expression.isLiteral()) {
            blame.tip("Only adapted Java objects can possibly have API, not values created inside templates.");
        } else {
            ObjectWrapper objectWrapper = environment.getObjectWrapper();
            if ((objectWrapper instanceof DefaultObjectWrapper) && ((templateModel instanceof SimpleHash) || (templateModel instanceof SimpleSequence))) {
                DefaultObjectWrapper defaultObjectWrapper = (DefaultObjectWrapper) objectWrapper;
                if (!defaultObjectWrapper.getUseAdaptersForContainers()) {
                    blame.tip(new Object[]{"In the FreeMarker configuration, \"", "object_wrapper", "\" is a DefaultObjectWrapper with its \"useAdaptersForContainers\" property set to false. Setting it to true might solves this problem."});
                    if (defaultObjectWrapper.getIncompatibleImprovements().intValue() < _TemplateAPI.VERSION_INT_2_3_22) {
                        blame.tip("Setting DefaultObjectWrapper's \"incompatibleImprovements\" to 2.3.22 or higher will change the default value of \"useAdaptersForContainers\" to true.");
                    }
                } else if ((templateModel instanceof SimpleSequence) && defaultObjectWrapper.getForceLegacyNonListCollections()) {
                    blame.tip(new Object[]{"In the FreeMarker configuration, \"", "object_wrapper", "\" is a DefaultObjectWrapper with its \"forceLegacyNonListCollections\" property set to true. If you are trying to access the API of a non-List Collection, setting the \"forceLegacyNonListCollections\" property to false might solves this problem."});
                }
            }
        }
        return blame;
    }
}
