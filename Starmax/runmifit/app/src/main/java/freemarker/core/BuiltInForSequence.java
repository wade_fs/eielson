package freemarker.core;

import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;

abstract class BuiltInForSequence extends BuiltIn {
    /* access modifiers changed from: package-private */
    public abstract TemplateModel calculateResult(TemplateSequenceModel templateSequenceModel) throws TemplateModelException;

    BuiltInForSequence() {
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        TemplateModel eval = this.target.eval(environment);
        if (eval instanceof TemplateSequenceModel) {
            return calculateResult((TemplateSequenceModel) eval);
        }
        throw new NonSequenceException(this.target, eval, environment);
    }
}
