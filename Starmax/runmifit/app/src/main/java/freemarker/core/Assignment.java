package freemarker.core;

import freemarker.core.Environment;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;

final class Assignment extends TemplateElement {
    static final int GLOBAL = 3;
    static final int LOCAL = 2;
    static final int NAMESPACE = 1;
    private static final Number ONE = new Integer(1);
    private static final int OPERATOR_TYPE_EQUALS = 65536;
    private static final int OPERATOR_TYPE_MINUS_MINUS = 65539;
    private static final int OPERATOR_TYPE_PLUS_EQUALS = 65537;
    private static final int OPERATOR_TYPE_PLUS_PLUS = 65538;
    private Expression namespaceExp;
    private final int operatorType;
    private final int scope;
    private final Expression valueExp;
    private final String variableName;

    static String getDirectiveName(int i) {
        return i == 2 ? "#local" : i == 3 ? "#global" : i == 1 ? "#assign" : "#{unknown_assignment_type}";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 5;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    Assignment(String str, int i, Expression expression, int i2) {
        this.scope = i2;
        this.variableName = str;
        if (i == 97) {
            this.operatorType = 65536;
        } else {
            switch (i) {
                case 100:
                    this.operatorType = OPERATOR_TYPE_PLUS_EQUALS;
                    break;
                case 101:
                    this.operatorType = 0;
                    break;
                case 102:
                    this.operatorType = 1;
                    break;
                case 103:
                    this.operatorType = 2;
                    break;
                case 104:
                    this.operatorType = 3;
                    break;
                case 105:
                    this.operatorType = OPERATOR_TYPE_PLUS_PLUS;
                    break;
                case 106:
                    this.operatorType = OPERATOR_TYPE_MINUS_MINUS;
                    break;
                default:
                    throw new BugException();
            }
        }
        this.valueExp = expression;
    }

    /* access modifiers changed from: package-private */
    public void setNamespaceExp(Expression expression) {
        if (this.scope == 1 || expression == null) {
            this.namespaceExp = expression;
            return;
        }
        throw new BugException();
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException {
        Environment.Namespace namespace;
        TemplateModel templateModel;
        TemplateModel templateModel2;
        Expression expression = this.namespaceExp;
        if (expression == null) {
            int i = this.scope;
            if (i == 1) {
                namespace = environment.getCurrentNamespace();
            } else if (i == 2) {
                namespace = null;
            } else if (i == 3) {
                namespace = environment.getGlobalNamespace();
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Unexpected scope type: ");
                stringBuffer.append(this.scope);
                throw new BugException(stringBuffer.toString());
            }
        } else {
            TemplateModel eval = expression.eval(environment);
            try {
                namespace = (Environment.Namespace) eval;
                if (namespace == null) {
                    throw InvalidReferenceException.getInstance(this.namespaceExp, environment);
                }
            } catch (ClassCastException unused) {
                throw new NonNamespaceException(this.namespaceExp, eval, environment);
            }
        }
        if (this.operatorType == 65536) {
            templateModel = this.valueExp.eval(environment);
            if (templateModel == null) {
                if (environment.isClassicCompatible()) {
                    templateModel = TemplateScalarModel.EMPTY_STRING;
                } else {
                    throw InvalidReferenceException.getInstance(this.valueExp, environment);
                }
            }
        } else {
            if (namespace == null) {
                templateModel2 = environment.getLocalVariable(this.variableName);
            } else {
                templateModel2 = namespace.get(this.variableName);
            }
            if (this.operatorType == OPERATOR_TYPE_PLUS_EQUALS) {
                if (templateModel2 == null) {
                    if (environment.isClassicCompatible()) {
                        templateModel2 = TemplateScalarModel.EMPTY_STRING;
                    } else {
                        throw InvalidReferenceException.getInstance(this.variableName, getOperatorTypeAsString(), environment);
                    }
                }
                TemplateModel templateModel3 = templateModel2;
                TemplateModel eval2 = this.valueExp.eval(environment);
                if (eval2 == null) {
                    if (environment.isClassicCompatible()) {
                        eval2 = TemplateScalarModel.EMPTY_STRING;
                    } else {
                        throw InvalidReferenceException.getInstance(this.valueExp, environment);
                    }
                }
                templateModel = AddConcatExpression._eval(environment, this.namespaceExp, null, templateModel3, this.valueExp, eval2);
            } else if (templateModel2 instanceof TemplateNumberModel) {
                Number modelToNumber = EvalUtil.modelToNumber((TemplateNumberModel) templateModel2, null);
                int i2 = this.operatorType;
                if (i2 == OPERATOR_TYPE_PLUS_PLUS) {
                    templateModel = AddConcatExpression._evalOnNumbers(environment, getParentElement(), modelToNumber, ONE);
                } else if (i2 == OPERATOR_TYPE_MINUS_MINUS) {
                    templateModel = ArithmeticExpression._eval(environment, getParentElement(), modelToNumber, 0, ONE);
                } else {
                    templateModel = ArithmeticExpression._eval(environment, this, modelToNumber, this.operatorType, this.valueExp.evalToNumber(environment));
                }
            } else if (templateModel2 == null) {
                throw InvalidReferenceException.getInstance(this.variableName, getOperatorTypeAsString(), environment);
            } else {
                throw new NonNumericalException(this.variableName, templateModel2, (String[]) null, environment);
            }
        }
        if (namespace == null) {
            environment.setLocalVariable(this.variableName, templateModel);
        } else {
            namespace.put(this.variableName, templateModel);
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        String nodeTypeSymbol = getParentElement() instanceof AssignmentInstruction ? null : getNodeTypeSymbol();
        if (nodeTypeSymbol != null) {
            if (z) {
                stringBuffer.append("<");
            }
            stringBuffer.append(nodeTypeSymbol);
            stringBuffer.append(' ');
        }
        stringBuffer.append(_CoreStringUtils.toFTLTopLevelTragetIdentifier(this.variableName));
        if (this.valueExp != null) {
            stringBuffer.append(' ');
        }
        stringBuffer.append(getOperatorTypeAsString());
        if (this.valueExp != null) {
            stringBuffer.append(' ');
            stringBuffer.append(this.valueExp.getCanonicalForm());
        }
        if (nodeTypeSymbol != null) {
            if (this.namespaceExp != null) {
                stringBuffer.append(" in ");
                stringBuffer.append(this.namespaceExp.getCanonicalForm());
            }
            if (z) {
                stringBuffer.append(">");
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return getDirectiveName(this.scope);
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.variableName;
        }
        if (i == 1) {
            return getOperatorTypeAsString();
        }
        if (i == 2) {
            return this.valueExp;
        }
        if (i == 3) {
            return new Integer(this.scope);
        }
        if (i == 4) {
            return this.namespaceExp;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.ASSIGNMENT_TARGET;
        }
        if (i == 1) {
            return ParameterRole.ASSIGNMENT_OPERATOR;
        }
        if (i == 2) {
            return ParameterRole.ASSIGNMENT_SOURCE;
        }
        if (i == 3) {
            return ParameterRole.VARIABLE_SCOPE;
        }
        if (i == 4) {
            return ParameterRole.NAMESPACE;
        }
        throw new IndexOutOfBoundsException();
    }

    private String getOperatorTypeAsString() {
        int i = this.operatorType;
        if (i == 65536) {
            return "=";
        }
        if (i == OPERATOR_TYPE_PLUS_EQUALS) {
            return "+=";
        }
        if (i == OPERATOR_TYPE_PLUS_PLUS) {
            return "++";
        }
        if (i == OPERATOR_TYPE_MINUS_MINUS) {
            return "--";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(ArithmeticExpression.getOperatorSymbol(this.operatorType));
        stringBuffer.append("=");
        return stringBuffer.toString();
    }
}
