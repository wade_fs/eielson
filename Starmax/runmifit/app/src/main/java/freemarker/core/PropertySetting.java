package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.StringUtil;
import java.util.Arrays;

final class PropertySetting extends TemplateElement {
    static final String[] SETTING_NAMES = {Configurable.BOOLEAN_FORMAT_KEY_CAMEL_CASE, "boolean_format", Configurable.CLASSIC_COMPATIBLE_KEY_CAMEL_CASE, "classic_compatible", Configurable.DATE_FORMAT_KEY_CAMEL_CASE, "date_format", Configurable.DATETIME_FORMAT_KEY_CAMEL_CASE, "datetime_format", "locale", Configurable.NUMBER_FORMAT_KEY_CAMEL_CASE, "number_format", Configurable.OUTPUT_ENCODING_KEY_CAMEL_CASE, "output_encoding", Configurable.SQL_DATE_AND_TIME_TIME_ZONE_KEY_CAMEL_CASE, "sql_date_and_time_time_zone", Configurable.TIME_FORMAT_KEY_CAMEL_CASE, Configurable.TIME_ZONE_KEY_CAMEL_CASE, "time_format", "time_zone", Configurable.URL_ESCAPING_CHARSET_KEY_CAMEL_CASE, "url_escaping_charset"};
    private final String key;
    private final Expression value;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#setting";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    PropertySetting(Token token, FMParserTokenManager fMParserTokenManager, Expression expression, Configuration configuration) throws ParseException {
        String str = token.image;
        if (Arrays.binarySearch(SETTING_NAMES, str) < 0) {
            StringBuffer stringBuffer = new StringBuffer();
            boolean z = true;
            if (!_TemplateAPI.getConfigurationSettingNames(configuration, true).contains(str) && !_TemplateAPI.getConfigurationSettingNames(configuration, false).contains(str)) {
                stringBuffer.append("Unknown setting name: ");
                stringBuffer.append(StringUtil.jQuote(str));
                stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                stringBuffer.append(" The allowed setting names are: ");
                int i = fMParserTokenManager.namingConvention;
                i = i == 10 ? 11 : i;
                int i2 = 0;
                while (true) {
                    String[] strArr = SETTING_NAMES;
                    if (i2 >= strArr.length) {
                        break;
                    }
                    int identifierNamingConvention = _CoreStringUtils.getIdentifierNamingConvention(strArr[i2]);
                    if (i == 12) {
                        i2 = identifierNamingConvention == 11 ? i2 + 1 : i2;
                    } else if (identifierNamingConvention == 12) {
                    }
                    if (z) {
                        z = false;
                    } else {
                        stringBuffer.append(", ");
                    }
                    stringBuffer.append(SETTING_NAMES[i2]);
                }
            } else {
                stringBuffer.append("The setting name is recognized, but changing this setting from inside a template isn't supported.");
            }
            throw new ParseException(stringBuffer.toString(), (Template) null, token);
        }
        this.key = str;
        this.value = expression;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException {
        String str;
        TemplateModel eval = this.value.eval(environment);
        if (eval instanceof TemplateScalarModel) {
            str = ((TemplateScalarModel) eval).getAsString();
        } else if (eval instanceof TemplateBooleanModel) {
            str = ((TemplateBooleanModel) eval).getAsBoolean() ? "true" : "false";
        } else if (eval instanceof TemplateNumberModel) {
            str = ((TemplateNumberModel) eval).getAsNumber().toString();
        } else {
            str = this.value.evalAndCoerceToString(environment);
        }
        environment.setSetting(this.key, str);
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(' ');
        stringBuffer.append(_CoreStringUtils.toFTLTopLevelTragetIdentifier(this.key));
        stringBuffer.append('=');
        stringBuffer.append(this.value.getCanonicalForm());
        if (z) {
            stringBuffer.append("/>");
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.key;
        }
        if (i == 1) {
            return this.value;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.ITEM_KEY;
        }
        if (i == 1) {
            return ParameterRole.ITEM_VALUE;
        }
        throw new IndexOutOfBoundsException();
    }
}
