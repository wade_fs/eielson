package freemarker.core;

import java.text.ParseException;
import java.util.TimeZone;

class ISOTemplateDateFormatFactory extends ISOLikeTemplateDateFormatFactory {
    public ISOTemplateDateFormatFactory(TimeZone timeZone) {
        super(timeZone);
    }

    public TemplateDateFormat get(int i, boolean z, String str) throws ParseException, UnknownDateTypeFormattingUnsupportedException {
        return new ISOTemplateDateFormat(str, 3, i, z, getTimeZone(), super);
    }
}
