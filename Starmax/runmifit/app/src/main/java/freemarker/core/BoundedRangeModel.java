package freemarker.core;

final class BoundedRangeModel extends RangeModel {
    private final boolean affectedByStringSlicingBug;
    private final boolean rightAdaptive;
    private final int size;
    private final int step;

    /* access modifiers changed from: package-private */
    public boolean isRightUnbounded() {
        return false;
    }

    BoundedRangeModel(int i, int i2, boolean z, boolean z2) {
        super(i);
        this.step = i <= i2 ? 1 : -1;
        this.size = Math.abs(i2 - i) + (z ? 1 : 0);
        this.rightAdaptive = z2;
        this.affectedByStringSlicingBug = z;
    }

    public int size() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    public int getStep() {
        return this.step;
    }

    /* access modifiers changed from: package-private */
    public boolean isRightAdaptive() {
        return this.rightAdaptive;
    }

    /* access modifiers changed from: package-private */
    public boolean isAffactedByStringSlicingBug() {
        return this.affectedByStringSlicingBug;
    }
}
