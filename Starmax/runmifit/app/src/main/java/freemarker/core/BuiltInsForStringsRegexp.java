package freemarker.core;

import freemarker.template.SimpleScalar;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import freemarker.template.TemplateScalarModel;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.utility.StringUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class BuiltInsForStringsRegexp {
    static /* synthetic */ Class class$freemarker$core$BuiltInsForStringsRegexp$RegexMatchModel;

    /* renamed from: class$freemarker$core$BuiltInsForStringsRegexp$RegexMatchModel$MatchWithGroups */
    static /* synthetic */ Class f7452x3d3c93a7;

    static class groupsBI extends BuiltIn {
        groupsBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            Class cls;
            Class cls2;
            TemplateModel eval = this.target.eval(environment);
            assertNonNull(eval, environment);
            if (eval instanceof RegexMatchModel) {
                return ((RegexMatchModel) eval).getGroups();
            }
            if (eval instanceof RegexMatchModel.MatchWithGroups) {
                return ((RegexMatchModel.MatchWithGroups) eval).groupsSeq;
            }
            Expression expression = this.target;
            Class[] clsArr = new Class[2];
            if (BuiltInsForStringsRegexp.class$freemarker$core$BuiltInsForStringsRegexp$RegexMatchModel == null) {
                cls = BuiltInsForStringsRegexp.class$("freemarker.core.BuiltInsForStringsRegexp$RegexMatchModel");
                BuiltInsForStringsRegexp.class$freemarker$core$BuiltInsForStringsRegexp$RegexMatchModel = cls;
            } else {
                cls = BuiltInsForStringsRegexp.class$freemarker$core$BuiltInsForStringsRegexp$RegexMatchModel;
            }
            clsArr[0] = cls;
            if (BuiltInsForStringsRegexp.f7452x3d3c93a7 == null) {
                cls2 = BuiltInsForStringsRegexp.class$("freemarker.core.BuiltInsForStringsRegexp$RegexMatchModel$MatchWithGroups");
                BuiltInsForStringsRegexp.f7452x3d3c93a7 = cls2;
            } else {
                cls2 = BuiltInsForStringsRegexp.f7452x3d3c93a7;
            }
            clsArr[1] = cls2;
            throw new UnexpectedTypeException(expression, eval, "regular expression matcher", clsArr, environment);
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    static class matchesBI extends BuiltInForString {
        matchesBI() {
        }

        class MatcherBuilder implements TemplateMethodModel {
            String matchString;

            MatcherBuilder(String str) throws TemplateModelException {
                this.matchString = str;
            }

            public Object exec(List list) throws TemplateModelException {
                int size = list.size();
                matchesBI.this.checkMethodArgCount(size, 1, 2);
                String str = (String) list.get(0);
                long parseFlagString = size > 1 ? RegexpHelper.parseFlagString((String) list.get(1)) : 0;
                if ((8589934592L & parseFlagString) != 0) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("?");
                    stringBuffer.append(matchesBI.this.key);
                    stringBuffer.append(" doesn't support the \"f\" flag.");
                    RegexpHelper.logFlagWarning(stringBuffer.toString());
                }
                return new RegexMatchModel(RegexpHelper.getPattern(str, (int) parseFlagString), this.matchString);
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateModelException {
            return new MatcherBuilder(str);
        }
    }

    static class replace_reBI extends BuiltInForString {
        replace_reBI() {
        }

        class ReplaceMethod implements TemplateMethodModel {

            /* renamed from: s */
            private String f7453s;

            ReplaceMethod(String str) {
                this.f7453s = str;
            }

            public Object exec(List list) throws TemplateModelException {
                String str;
                int size = list.size();
                replace_reBI.this.checkMethodArgCount(size, 2, 3);
                boolean z = false;
                String str2 = (String) list.get(0);
                String str3 = (String) list.get(1);
                long parseFlagString = size > 2 ? RegexpHelper.parseFlagString((String) list.get(2)) : 0;
                if ((4294967296L & parseFlagString) == 0) {
                    RegexpHelper.checkNonRegexpFlags("replace", parseFlagString);
                    String str4 = this.f7453s;
                    boolean z2 = (RegexpHelper.RE_FLAG_CASE_INSENSITIVE & parseFlagString) != 0;
                    if ((parseFlagString & 8589934592L) != 0) {
                        z = true;
                    }
                    str = StringUtil.replace(str4, str2, str3, z2, z);
                } else {
                    Matcher matcher = RegexpHelper.getPattern(str2, (int) parseFlagString).matcher(this.f7453s);
                    if ((parseFlagString & 8589934592L) != 0) {
                        str = matcher.replaceFirst(str3);
                    } else {
                        str = matcher.replaceAll(str3);
                    }
                }
                return new SimpleScalar(str);
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(String str, Environment environment) throws TemplateModelException {
            return new ReplaceMethod(str);
        }
    }

    static class RegexMatchModel implements TemplateBooleanModel, TemplateCollectionModel, TemplateSequenceModel {
        private TemplateSequenceModel entireInputMatchGroups;
        private Boolean entireInputMatched;
        private Matcher firedEntireInputMatcher;
        final String input;
        /* access modifiers changed from: private */
        public ArrayList matchingInputParts;
        final Pattern pattern;

        static class MatchWithGroups implements TemplateScalarModel {
            final SimpleSequence groupsSeq;
            final String matchedInputPart;

            MatchWithGroups(String str, Matcher matcher) {
                this.matchedInputPart = str.substring(matcher.start(), matcher.end());
                int groupCount = matcher.groupCount() + 1;
                this.groupsSeq = new SimpleSequence(groupCount);
                for (int i = 0; i < groupCount; i++) {
                    this.groupsSeq.add(matcher.group(i));
                }
            }

            public String getAsString() {
                return this.matchedInputPart;
            }
        }

        RegexMatchModel(Pattern pattern2, String str) {
            this.pattern = pattern2;
            this.input = str;
        }

        public TemplateModel get(int i) throws TemplateModelException {
            ArrayList arrayList = this.matchingInputParts;
            if (arrayList == null) {
                arrayList = getMatchingInputPartsAndStoreResults();
            }
            return (TemplateModel) arrayList.get(i);
        }

        public boolean getAsBoolean() {
            Boolean bool = this.entireInputMatched;
            return bool != null ? bool.booleanValue() : isEntrieInputMatchesAndStoreResults();
        }

        /* access modifiers changed from: package-private */
        public TemplateModel getGroups() {
            TemplateSequenceModel templateSequenceModel = this.entireInputMatchGroups;
            if (templateSequenceModel != null) {
                return templateSequenceModel;
            }
            final Matcher matcher = this.firedEntireInputMatcher;
            if (matcher == null) {
                isEntrieInputMatchesAndStoreResults();
                matcher = this.firedEntireInputMatcher;
            }
            C32851 r1 = new TemplateSequenceModel() {
                /* class freemarker.core.BuiltInsForStringsRegexp.RegexMatchModel.C32851 */

                public TemplateModel get(int i) throws TemplateModelException {
                    try {
                        return new SimpleScalar(matcher.group(i));
                    } catch (Exception e) {
                        throw new _TemplateModelException(e, "Failed to read match group");
                    }
                }

                public int size() throws TemplateModelException {
                    try {
                        return matcher.groupCount() + 1;
                    } catch (Exception e) {
                        throw new _TemplateModelException(e, "Failed to get match group count");
                    }
                }
            };
            this.entireInputMatchGroups = r1;
            return r1;
        }

        private ArrayList getMatchingInputPartsAndStoreResults() throws TemplateModelException {
            ArrayList arrayList = new ArrayList();
            Matcher matcher = this.pattern.matcher(this.input);
            while (matcher.find()) {
                arrayList.add(new MatchWithGroups(this.input, matcher));
            }
            this.matchingInputParts = arrayList;
            return arrayList;
        }

        private boolean isEntrieInputMatchesAndStoreResults() {
            Matcher matcher = this.pattern.matcher(this.input);
            boolean matches = matcher.matches();
            this.firedEntireInputMatcher = matcher;
            this.entireInputMatched = Boolean.valueOf(matches);
            return matches;
        }

        public TemplateModelIterator iterator() {
            final ArrayList arrayList = this.matchingInputParts;
            if (arrayList != null) {
                return new TemplateModelIterator() {
                    /* class freemarker.core.BuiltInsForStringsRegexp.RegexMatchModel.C32873 */
                    private int nextIdx = 0;

                    public boolean hasNext() {
                        return this.nextIdx < arrayList.size();
                    }

                    public TemplateModel next() throws TemplateModelException {
                        try {
                            ArrayList arrayList = arrayList;
                            int i = this.nextIdx;
                            this.nextIdx = i + 1;
                            return (TemplateModel) arrayList.get(i);
                        } catch (IndexOutOfBoundsException e) {
                            throw new _TemplateModelException(e, "There were no more matches");
                        }
                    }
                };
            }
            final Matcher matcher = this.pattern.matcher(this.input);
            return new TemplateModelIterator() {
                /* class freemarker.core.BuiltInsForStringsRegexp.RegexMatchModel.C32862 */
                boolean hasFindInfo = matcher.find();
                private int nextIdx = 0;

                public boolean hasNext() {
                    ArrayList access$000 = RegexMatchModel.this.matchingInputParts;
                    if (access$000 == null) {
                        return this.hasFindInfo;
                    }
                    return this.nextIdx < access$000.size();
                }

                public TemplateModel next() throws TemplateModelException {
                    ArrayList access$000 = RegexMatchModel.this.matchingInputParts;
                    if (access$000 != null) {
                        try {
                            int i = this.nextIdx;
                            this.nextIdx = i + 1;
                            return (TemplateModel) access$000.get(i);
                        } catch (IndexOutOfBoundsException e) {
                            throw new _TemplateModelException(e, "There were no more matches");
                        }
                    } else if (this.hasFindInfo) {
                        MatchWithGroups matchWithGroups = new MatchWithGroups(RegexMatchModel.this.input, matcher);
                        this.nextIdx++;
                        this.hasFindInfo = matcher.find();
                        return matchWithGroups;
                    } else {
                        throw new _TemplateModelException("There were no more matches");
                    }
                }
            };
        }

        public int size() throws TemplateModelException {
            ArrayList arrayList = this.matchingInputParts;
            if (arrayList == null) {
                arrayList = getMatchingInputPartsAndStoreResults();
            }
            return arrayList.size();
        }
    }

    private BuiltInsForStringsRegexp() {
    }
}
