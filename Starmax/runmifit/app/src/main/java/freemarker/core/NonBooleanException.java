package freemarker.core;

import freemarker.template.TemplateModel;

public class NonBooleanException extends UnexpectedTypeException {
    private static final Class[] EXPECTED_TYPES;
    static /* synthetic */ Class class$freemarker$template$TemplateBooleanModel;

    static {
        Class[] clsArr = new Class[1];
        Class cls = class$freemarker$template$TemplateBooleanModel;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateBooleanModel");
            class$freemarker$template$TemplateBooleanModel = cls;
        }
        clsArr[0] = cls;
        EXPECTED_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonBooleanException(Environment environment) {
        super(environment, "Expecting boolean value here");
    }

    public NonBooleanException(String str, Environment environment) {
        super(environment, str);
    }

    NonBooleanException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(environment, _errordescriptionbuilder);
    }

    NonBooleanException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "boolean", EXPECTED_TYPES, environment);
    }

    NonBooleanException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "boolean", EXPECTED_TYPES, str, environment);
    }

    NonBooleanException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "boolean", EXPECTED_TYPES, strArr, environment);
    }
}
