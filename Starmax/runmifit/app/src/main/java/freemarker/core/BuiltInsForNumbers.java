package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.template.SimpleDate;
import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.utility.NumberUtil;
import freemarker.template.utility.StringUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

class BuiltInsForNumbers {
    private static final BigDecimal BIG_DECIMAL_LONG_MAX = BigDecimal.valueOf(Long.MAX_VALUE);
    private static final BigDecimal BIG_DECIMAL_LONG_MIN = BigDecimal.valueOf(Long.MIN_VALUE);
    /* access modifiers changed from: private */
    public static final BigDecimal BIG_DECIMAL_ONE = new BigDecimal("1");
    private static final BigInteger BIG_INTEGER_LONG_MAX = BigInteger.valueOf(Long.MAX_VALUE);
    private static final BigInteger BIG_INTEGER_LONG_MIN = BigInteger.valueOf(Long.MIN_VALUE);

    private static abstract class abcBI extends BuiltInForNumber {
        /* access modifiers changed from: protected */
        public abstract String toABC(int i);

        private abcBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) throws TemplateModelException {
            try {
                int intExact = NumberUtil.toIntExact(number);
                if (intExact > 0) {
                    return new SimpleScalar(toABC(intExact));
                }
                throw new _TemplateModelException(this.target, new Object[]{"The left side operand of to ?", this.key, " must be at least 1, but was ", new Integer(intExact), FileUtil.HIDDEN_PREFIX});
            } catch (ArithmeticException e) {
                throw new _TemplateModelException(this.target, new Object[]{"The left side operand value isn't compatible with ?", this.key, ": ", e.getMessage()});
            }
        }
    }

    static class lower_abcBI extends abcBI {
        lower_abcBI() {
            super();
        }

        /* access modifiers changed from: protected */
        public String toABC(int i) {
            return StringUtil.toLowerABC(i);
        }
    }

    static class upper_abcBI extends abcBI {
        upper_abcBI() {
            super();
        }

        /* access modifiers changed from: protected */
        public String toABC(int i) {
            return StringUtil.toUpperABC(i);
        }
    }

    static class absBI extends BuiltInForNumber {
        absBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) throws TemplateModelException {
            if (number instanceof Integer) {
                int intValue = ((Integer) number).intValue();
                return intValue < 0 ? new SimpleNumber(-intValue) : templateModel;
            } else if (number instanceof BigDecimal) {
                BigDecimal bigDecimal = (BigDecimal) number;
                return bigDecimal.signum() < 0 ? new SimpleNumber(bigDecimal.negate()) : templateModel;
            } else if (number instanceof Double) {
                double doubleValue = ((Double) number).doubleValue();
                return doubleValue < 0.0d ? new SimpleNumber(-doubleValue) : templateModel;
            } else if (number instanceof Float) {
                float floatValue = ((Float) number).floatValue();
                return floatValue < 0.0f ? new SimpleNumber(-floatValue) : templateModel;
            } else if (number instanceof Long) {
                long longValue = ((Long) number).longValue();
                return longValue < 0 ? new SimpleNumber(-longValue) : templateModel;
            } else if (number instanceof Short) {
                short shortValue = ((Short) number).shortValue();
                return shortValue < 0 ? new SimpleNumber(-shortValue) : templateModel;
            } else if (number instanceof Byte) {
                byte byteValue = ((Byte) number).byteValue();
                return byteValue < 0 ? new SimpleNumber(-byteValue) : templateModel;
            } else if (number instanceof BigInteger) {
                BigInteger bigInteger = (BigInteger) number;
                return bigInteger.signum() < 0 ? new SimpleNumber(bigInteger.negate()) : templateModel;
            } else {
                throw new _TemplateModelException(new Object[]{"Unsupported number class: ", number.getClass()});
            }
        }
    }

    static class byteBI extends BuiltInForNumber {
        byteBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) {
            if (number instanceof Byte) {
                return templateModel;
            }
            return new SimpleNumber(new Byte(number.byteValue()));
        }
    }

    static class ceilingBI extends BuiltInForNumber {
        ceilingBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) {
            return new SimpleNumber(new BigDecimal(number.doubleValue()).divide(BuiltInsForNumbers.BIG_DECIMAL_ONE, 0, 2));
        }
    }

    static class doubleBI extends BuiltInForNumber {
        doubleBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) {
            if (number instanceof Double) {
                return templateModel;
            }
            return new SimpleNumber(number.doubleValue());
        }
    }

    static class floatBI extends BuiltInForNumber {
        floatBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) {
            if (number instanceof Float) {
                return templateModel;
            }
            return new SimpleNumber(number.floatValue());
        }
    }

    static class floorBI extends BuiltInForNumber {
        floorBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) {
            return new SimpleNumber(new BigDecimal(number.doubleValue()).divide(BuiltInsForNumbers.BIG_DECIMAL_ONE, 0, 3));
        }
    }

    static class intBI extends BuiltInForNumber {
        intBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) {
            if (number instanceof Integer) {
                return templateModel;
            }
            return new SimpleNumber(number.intValue());
        }
    }

    static class is_infiniteBI extends BuiltInForNumber {
        is_infiniteBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) throws TemplateModelException {
            return NumberUtil.isInfinite(number) ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class is_nanBI extends BuiltInForNumber {
        is_nanBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) throws TemplateModelException {
            return NumberUtil.isNaN(number) ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class longBI extends BuiltIn {
        longBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            if (!(eval instanceof TemplateNumberModel) && (eval instanceof TemplateDateModel)) {
                return new SimpleNumber(EvalUtil.modelToDate((TemplateDateModel) eval, this.target).getTime());
            }
            Number modelToNumber = this.target.modelToNumber(eval, environment);
            if (modelToNumber instanceof Long) {
                return eval;
            }
            return new SimpleNumber(modelToNumber.longValue());
        }
    }

    static class number_to_dateBI extends BuiltInForNumber {
        private final int dateType;

        number_to_dateBI(int i) {
            this.dateType = i;
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) throws TemplateModelException {
            return new SimpleDate(new Date(BuiltInsForNumbers.safeToLong(number)), this.dateType);
        }
    }

    static class roundBI extends BuiltInForNumber {
        private static final BigDecimal half = new BigDecimal("0.5");

        roundBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) {
            return new SimpleNumber(new BigDecimal(number.doubleValue()).add(half).divide(BuiltInsForNumbers.BIG_DECIMAL_ONE, 0, 3));
        }
    }

    static class shortBI extends BuiltInForNumber {
        shortBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(Number number, TemplateModel templateModel) {
            if (number instanceof Short) {
                return templateModel;
            }
            return new SimpleNumber(new Short(number.shortValue()));
        }
    }

    /* access modifiers changed from: private */
    public static final long safeToLong(Number number) throws TemplateModelException {
        if (number instanceof Double) {
            double round = (double) Math.round(((Double) number).doubleValue());
            if (round <= 9.223372036854776E18d && round >= -9.223372036854776E18d) {
                return (long) round;
            }
            throw new _TemplateModelException(new Object[]{"Number doesn't fit into a 64 bit signed integer (long): ", new Double(round)});
        } else if (number instanceof Float) {
            float round2 = (float) Math.round(((Float) number).floatValue());
            if (round2 <= 9.223372E18f && round2 >= -9.223372E18f) {
                return (long) round2;
            }
            throw new _TemplateModelException(new Object[]{"Number doesn't fit into a 64 bit signed integer (long): ", new Float(round2)});
        } else if (number instanceof BigDecimal) {
            BigDecimal scale = ((BigDecimal) number).setScale(0, 4);
            if (scale.compareTo(BIG_DECIMAL_LONG_MAX) <= 0 && scale.compareTo(BIG_DECIMAL_LONG_MIN) >= 0) {
                return scale.longValue();
            }
            throw new _TemplateModelException(new Object[]{"Number doesn't fit into a 64 bit signed integer (long): ", scale});
        } else if (number instanceof BigInteger) {
            BigInteger bigInteger = (BigInteger) number;
            if (bigInteger.compareTo(BIG_INTEGER_LONG_MAX) <= 0 && bigInteger.compareTo(BIG_INTEGER_LONG_MIN) >= 0) {
                return bigInteger.longValue();
            }
            throw new _TemplateModelException(new Object[]{"Number doesn't fit into a 64 bit signed integer (long): ", bigInteger});
        } else if ((number instanceof Long) || (number instanceof Integer) || (number instanceof Byte) || (number instanceof Short)) {
            return number.longValue();
        } else {
            throw new _TemplateModelException(new Object[]{"Unsupported number type: ", number.getClass()});
        }
    }

    private BuiltInsForNumbers() {
    }
}
