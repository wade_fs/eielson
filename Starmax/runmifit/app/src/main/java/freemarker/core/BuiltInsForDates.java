package freemarker.core;

import freemarker.template.SimpleDate;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.DateUtil;
import java.sql.Time;
import java.util.Date;
import java.util.TimeZone;

class BuiltInsForDates {
    static /* synthetic */ Class class$java$util$TimeZone;

    static class dateType_if_unknownBI extends BuiltIn {
        private final int dateType;

        /* access modifiers changed from: protected */
        public TemplateModel calculateResult(Date date, int i, Environment environment) throws TemplateException {
            return null;
        }

        dateType_if_unknownBI(int i) {
            this.dateType = i;
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            TemplateModel eval = this.target.eval(environment);
            if (eval instanceof TemplateDateModel) {
                TemplateDateModel templateDateModel = (TemplateDateModel) eval;
                if (templateDateModel.getDateType() != 0) {
                    return templateDateModel;
                }
                return new SimpleDate(EvalUtil.modelToDate(templateDateModel, this.target), this.dateType);
            }
            throw BuiltInForDate.newNonDateException(environment, eval, this.target);
        }
    }

    static class iso_BI extends AbstractISOBI {

        class Result implements TemplateMethodModelEx {
            private final Date date;
            private final int dateType;
            private final Environment env;

            Result(Date date2, int i, Environment environment) {
                this.date = date2;
                this.dateType = i;
                this.env = environment;
            }

            /* JADX WARNING: Removed duplicated region for block: B:17:0x0048  */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x004a  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x004f  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x0051  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object exec(java.util.List r12) throws freemarker.template.TemplateModelException {
                /*
                    r11 = this;
                    freemarker.core.BuiltInsForDates$iso_BI r0 = freemarker.core.BuiltInsForDates.iso_BI.this
                    r1 = 1
                    r0.checkMethodArgCount(r12, r1)
                    r0 = 0
                    java.lang.Object r12 = r12.get(r0)
                    freemarker.template.TemplateModel r12 = (freemarker.template.TemplateModel) r12
                    boolean r2 = r12 instanceof freemarker.template.AdapterTemplateModel
                    r3 = 2
                    if (r2 == 0) goto L_0x0030
                    r2 = r12
                    freemarker.template.AdapterTemplateModel r2 = (freemarker.template.AdapterTemplateModel) r2
                    java.lang.Class r4 = freemarker.core.BuiltInsForDates.class$java$util$TimeZone
                    if (r4 != 0) goto L_0x0022
                    java.lang.String r4 = "java.util.TimeZone"
                    java.lang.Class r4 = freemarker.core.BuiltInsForDates.class$(r4)
                    freemarker.core.BuiltInsForDates.class$java$util$TimeZone = r4
                    goto L_0x0024
                L_0x0022:
                    java.lang.Class r4 = freemarker.core.BuiltInsForDates.class$java$util$TimeZone
                L_0x0024:
                    java.lang.Object r2 = r2.getAdaptedObject(r4)
                    boolean r4 = r2 instanceof java.util.TimeZone
                    if (r4 == 0) goto L_0x0030
                    java.util.TimeZone r2 = (java.util.TimeZone) r2
                L_0x002e:
                    r9 = r2
                    goto L_0x0040
                L_0x0030:
                    boolean r2 = r12 instanceof freemarker.template.TemplateScalarModel
                    if (r2 == 0) goto L_0x008f
                    freemarker.template.TemplateScalarModel r12 = (freemarker.template.TemplateScalarModel) r12
                    r2 = 0
                    java.lang.String r12 = freemarker.core.EvalUtil.modelToString(r12, r2, r2)
                    java.util.TimeZone r2 = freemarker.template.utility.DateUtil.getTimeZone(r12)     // Catch:{ UnrecognizedTimeZoneException -> 0x0070 }
                    goto L_0x002e
                L_0x0040:
                    freemarker.template.SimpleScalar r12 = new freemarker.template.SimpleScalar
                    java.util.Date r4 = r11.date
                    int r2 = r11.dateType
                    if (r2 == r1) goto L_0x004a
                    r5 = 1
                    goto L_0x004b
                L_0x004a:
                    r5 = 0
                L_0x004b:
                    int r2 = r11.dateType
                    if (r2 == r3) goto L_0x0051
                    r6 = 1
                    goto L_0x0052
                L_0x0051:
                    r6 = 0
                L_0x0052:
                    freemarker.core.BuiltInsForDates$iso_BI r0 = freemarker.core.BuiltInsForDates.iso_BI.this
                    java.util.Date r1 = r11.date
                    int r2 = r11.dateType
                    freemarker.core.Environment r3 = r11.env
                    boolean r7 = r0.shouldShowOffset(r1, r2, r3)
                    freemarker.core.BuiltInsForDates$iso_BI r0 = freemarker.core.BuiltInsForDates.iso_BI.this
                    int r8 = r0.accuracy
                    freemarker.core.Environment r0 = r11.env
                    freemarker.template.utility.DateUtil$DateToISO8601CalendarFactory r10 = r0.getISOBuiltInCalendarFactory()
                    java.lang.String r0 = freemarker.template.utility.DateUtil.dateToISO8601String(r4, r5, r6, r7, r8, r9, r10)
                    r12.<init>(r0)
                    return r12
                L_0x0070:
                    freemarker.core._TemplateModelException r2 = new freemarker.core._TemplateModelException
                    r4 = 4
                    java.lang.Object[] r4 = new java.lang.Object[r4]
                    java.lang.String r5 = "The time zone string specified for ?"
                    r4[r0] = r5
                    freemarker.core.BuiltInsForDates$iso_BI r0 = freemarker.core.BuiltInsForDates.iso_BI.this
                    java.lang.String r0 = r0.key
                    r4[r1] = r0
                    java.lang.String r0 = "(...) is not recognized as a valid time zone name: "
                    r4[r3] = r0
                    r0 = 3
                    freemarker.core._DelayedJQuote r1 = new freemarker.core._DelayedJQuote
                    r1.<init>(r12)
                    r4[r0] = r1
                    r2.<init>(r4)
                    throw r2
                L_0x008f:
                    java.lang.StringBuffer r1 = new java.lang.StringBuffer
                    r1.<init>()
                    java.lang.String r2 = "?"
                    r1.append(r2)
                    freemarker.core.BuiltInsForDates$iso_BI r2 = freemarker.core.BuiltInsForDates.iso_BI.this
                    java.lang.String r2 = r2.key
                    r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    java.lang.String r2 = "string or java.util.TimeZone"
                    freemarker.template.TemplateModelException r12 = freemarker.core.MessageUtil.newMethodArgUnexpectedTypeException(r1, r0, r2, r12)
                    goto L_0x00ac
                L_0x00ab:
                    throw r12
                L_0x00ac:
                    goto L_0x00ab
                */
                throw new UnsupportedOperationException("Method not decompiled: freemarker.core.BuiltInsForDates.iso_BI.Result.exec(java.util.List):java.lang.Object");
            }
        }

        iso_BI(Boolean bool, int i) {
            super(bool, i);
        }

        /* access modifiers changed from: protected */
        public TemplateModel calculateResult(Date date, int i, Environment environment) throws TemplateException {
            checkDateTypeNotUnknown(i);
            return new Result(date, i, environment);
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    static class iso_utc_or_local_BI extends AbstractISOBI {
        private final boolean useUTC;

        iso_utc_or_local_BI(Boolean bool, int i, boolean z) {
            super(bool, i);
            this.useUTC = z;
        }

        /* access modifiers changed from: protected */
        public TemplateModel calculateResult(Date date, int i, Environment environment) throws TemplateException {
            TimeZone timeZone;
            checkDateTypeNotUnknown(i);
            boolean z = i != 1;
            boolean z2 = i != 2;
            boolean shouldShowOffset = shouldShowOffset(date, i, environment);
            int i2 = this.accuracy;
            if (this.useUTC) {
                timeZone = DateUtil.UTC;
            } else if (environment.shouldUseSQLDTTZ(date.getClass())) {
                timeZone = environment.getSQLDateAndTimeTimeZone();
            } else {
                timeZone = environment.getTimeZone();
            }
            return new SimpleScalar(DateUtil.dateToISO8601String(date, z, z2, shouldShowOffset, i2, timeZone, environment.getISOBuiltInCalendarFactory()));
        }
    }

    private BuiltInsForDates() {
    }

    static abstract class AbstractISOBI extends BuiltInForDate {
        protected final int accuracy;
        protected final Boolean showOffset;

        protected AbstractISOBI(Boolean bool, int i) {
            this.showOffset = bool;
            this.accuracy = i;
        }

        /* access modifiers changed from: protected */
        public void checkDateTypeNotUnknown(int i) throws TemplateException {
            if (i == 0) {
                throw new _MiscTemplateException(new _ErrorDescriptionBuilder(new Object[]{"The value of the following has unknown date type, but ?", this.key, " needs a value where it's known if it's a date (no time part), time, or date-time value:"}).blame(this.target).tip("Use ?date, ?time, or ?datetime to tell FreeMarker the exact type."));
            }
        }

        /* access modifiers changed from: protected */
        public boolean shouldShowOffset(Date date, int i, Environment environment) {
            if (i == 2) {
                return false;
            }
            Boolean bool = this.showOffset;
            if (bool != null) {
                return bool.booleanValue();
            }
            if (!(date instanceof Time) || _TemplateAPI.getTemplateLanguageVersionAsInt(this) < _TemplateAPI.VERSION_INT_2_3_21) {
                return true;
            }
            return false;
        }
    }
}
