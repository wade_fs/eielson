package freemarker.core;

import freemarker.template.TemplateModel;

public class NonNumericalException extends UnexpectedTypeException {
    private static final Class[] EXPECTED_TYPES;
    static /* synthetic */ Class class$freemarker$template$TemplateNumberModel;

    static {
        Class[] clsArr = new Class[1];
        Class cls = class$freemarker$template$TemplateNumberModel;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateNumberModel");
            class$freemarker$template$TemplateNumberModel = cls;
        }
        clsArr[0] = cls;
        EXPECTED_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonNumericalException(Environment environment) {
        super(environment, "Expecting numerical value here");
    }

    public NonNumericalException(String str, Environment environment) {
        super(environment, str);
    }

    NonNumericalException(_ErrorDescriptionBuilder _errordescriptionbuilder, Environment environment) {
        super(environment, _errordescriptionbuilder);
    }

    NonNumericalException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "number", EXPECTED_TYPES, environment);
    }

    NonNumericalException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "number", EXPECTED_TYPES, str, environment);
    }

    NonNumericalException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "number", EXPECTED_TYPES, strArr, environment);
    }

    NonNumericalException(String str, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(str, templateModel, "number", EXPECTED_TYPES, strArr, environment);
    }

    static NonNumericalException newMalformedNumberException(Expression expression, String str, Environment environment) {
        return new NonNumericalException(new _ErrorDescriptionBuilder(new Object[]{"Can't convert this string to number: ", new _DelayedJQuote(str)}).blame(expression), environment);
    }
}
