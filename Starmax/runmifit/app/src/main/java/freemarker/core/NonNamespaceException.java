package freemarker.core;

import com.google.android.gms.common.server.FavaDiagnosticsEntity;
import freemarker.template.TemplateModel;

class NonNamespaceException extends UnexpectedTypeException {
    private static final Class[] EXPECTED_TYPES;
    static /* synthetic */ Class class$freemarker$core$Environment$Namespace;

    static {
        Class[] clsArr = new Class[1];
        Class cls = class$freemarker$core$Environment$Namespace;
        if (cls == null) {
            cls = class$("freemarker.core.Environment$Namespace");
            class$freemarker$core$Environment$Namespace = cls;
        }
        clsArr[0] = cls;
        EXPECTED_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonNamespaceException(Environment environment) {
        super(environment, "Expecting namespace value here");
    }

    public NonNamespaceException(String str, Environment environment) {
        super(environment, str);
    }

    NonNamespaceException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(environment, _errordescriptionbuilder);
    }

    NonNamespaceException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, FavaDiagnosticsEntity.EXTRA_NAMESPACE, EXPECTED_TYPES, environment);
    }

    NonNamespaceException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, FavaDiagnosticsEntity.EXTRA_NAMESPACE, EXPECTED_TYPES, str, environment);
    }

    NonNamespaceException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, FavaDiagnosticsEntity.EXTRA_NAMESPACE, EXPECTED_TYPES, strArr, environment);
    }
}
