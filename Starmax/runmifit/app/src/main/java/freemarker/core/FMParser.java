package freemarker.core;

import com.baidu.mobstat.Config;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateScalarModel;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.StringUtil;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

public class FMParser implements FMParserConstants {
    private static final int ITERATOR_BLOCK_KIND_FOREACH = 1;
    private static final int ITERATOR_BLOCK_KIND_ITEMS = 2;
    private static final int ITERATOR_BLOCK_KIND_LIST = 0;
    private static final int ITERATOR_BLOCK_KIND_USER_DIRECTIVE = 3;
    static /* synthetic */ Class class$freemarker$core$SpecialBuiltIn;
    private static int[] jj_la1_0;
    private static int[] jj_la1_1;
    private static int[] jj_la1_2;
    private static int[] jj_la1_3;
    private static int[] jj_la1_4;
    private int breakableDirectiveNesting;
    private LinkedList escapes;
    private boolean inFunction;
    private boolean inMacro;
    private int incompatibleImprovements;
    private List iteratorBlockContexts;
    private final JJCalls[] jj_2_rtns;
    private int jj_endpos;
    private Vector jj_expentries;
    private int[] jj_expentry;
    private int jj_gc;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private int jj_la;
    private final int[] jj_la1;
    private Token jj_lastpos;
    private int[] jj_lasttokens;
    private final LookaheadSuccess jj_ls;
    public Token jj_nt;
    private int jj_ntk;
    private boolean jj_rescan;
    private Token jj_scanpos;
    private boolean jj_semLA;
    public boolean lookingAhead;
    private int mixedContentNesting;
    private int parentListAndForeachFlags;
    private boolean stripText;
    private boolean stripWhitespace;
    private Template template;
    public Token token;
    public FMParserTokenManager token_source;

    public final void disable_tracing() {
    }

    public final void enable_tracing() {
    }

    private static class ParserIteratorBlockContext {
        /* access modifiers changed from: private */
        public int kind;
        /* access modifiers changed from: private */
        public String loopVarName;

        private ParserIteratorBlockContext() {
        }
    }

    public static FMParser createExpressionParser(String str) {
        FMParserTokenManager fMParserTokenManager = new FMParserTokenManager(new SimpleCharStream(new StringReader(str), 1, 1, str.length()));
        fMParserTokenManager.SwitchTo(2);
        FMParser fMParser = new FMParser(fMParserTokenManager);
        fMParserTokenManager.setParser(fMParser);
        return fMParser;
    }

    public FMParser(Template template2, Reader reader, boolean z, boolean z2) {
        this(reader);
        setTemplate(template2);
        this.token_source.setParser(this);
        this.token_source.strictEscapeSyntax = z;
        this.stripWhitespace = z2;
    }

    public FMParser(Template template2, Reader reader, boolean z, boolean z2, int i) {
        this(template2, reader, z, z2, i, Configuration.PARSED_DEFAULT_INCOMPATIBLE_ENHANCEMENTS);
    }

    public FMParser(Template template2, Reader reader, boolean z, boolean z2, int i, int i2) {
        this(template2, reader, z, z2, i, 10, i2);
    }

    public FMParser(Template template2, Reader reader, boolean z, boolean z2, int i, int i2, int i3) {
        this(template2, reader, z, z2);
        if (i == 0) {
            this.token_source.autodetectTagSyntax = true;
        } else if (i == 1) {
            this.token_source.squBracTagSyntax = false;
        } else if (i == 2) {
            this.token_source.squBracTagSyntax = true;
        } else {
            throw new IllegalArgumentException("Illegal argument for tagSyntax");
        }
        switch (i2) {
            case 10:
            case 11:
            case 12:
                FMParserTokenManager fMParserTokenManager = this.token_source;
                fMParserTokenManager.initialNamingConvention = i2;
                fMParserTokenManager.namingConvention = i2;
                fMParserTokenManager.incompatibleImprovements = i3;
                this.incompatibleImprovements = i3;
                return;
            default:
                throw new IllegalArgumentException("Illegal argument for namingConvention");
        }
    }

    public FMParser(String str) {
        this(null, new StringReader(str), true, true);
    }

    /* access modifiers changed from: package-private */
    public void setTemplate(Template template2) {
        this.template = template2;
    }

    /* access modifiers changed from: package-private */
    public Template getTemplate() {
        return this.template;
    }

    public int _getLastTagSyntax() {
        return this.token_source.squBracTagSyntax ? 2 : 1;
    }

    public int _getLastNamingConvention() {
        return this.token_source.namingConvention;
    }

    private void notStringLiteral(Expression expression, String str) throws ParseException {
        if (expression instanceof StringLiteral) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Found string literal: ");
            stringBuffer.append(expression);
            stringBuffer.append(". Expecting: ");
            stringBuffer.append(str);
            throw new ParseException(stringBuffer.toString(), expression);
        }
    }

    private void notNumberLiteral(Expression expression, String str) throws ParseException {
        if (expression instanceof NumberLiteral) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Found number literal: ");
            stringBuffer.append(expression.getCanonicalForm());
            stringBuffer.append(". Expecting ");
            stringBuffer.append(str);
            throw new ParseException(stringBuffer.toString(), expression);
        }
    }

    private void notBooleanLiteral(Expression expression, String str) throws ParseException {
        if (expression instanceof BooleanLiteral) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Found: ");
            stringBuffer.append(expression.getCanonicalForm());
            stringBuffer.append(". Expecting ");
            stringBuffer.append(str);
            throw new ParseException(stringBuffer.toString(), expression);
        }
    }

    private void notHashLiteral(Expression expression, String str) throws ParseException {
        if (expression instanceof HashLiteral) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Found hash literal: ");
            stringBuffer.append(expression.getCanonicalForm());
            stringBuffer.append(". Expecting ");
            stringBuffer.append(str);
            throw new ParseException(stringBuffer.toString(), expression);
        }
    }

    private void notListLiteral(Expression expression, String str) throws ParseException {
        if (expression instanceof ListLiteral) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Found list literal: ");
            stringBuffer.append(expression.getCanonicalForm());
            stringBuffer.append(". Expecting ");
            stringBuffer.append(str);
            throw new ParseException(stringBuffer.toString(), expression);
        }
    }

    private void numberLiteralOnly(Expression expression) throws ParseException {
        notStringLiteral(expression, "number");
        notListLiteral(expression, "number");
        notHashLiteral(expression, "number");
        notBooleanLiteral(expression, "number");
    }

    private void stringLiteralOnly(Expression expression) throws ParseException {
        notNumberLiteral(expression, "string");
        notListLiteral(expression, "string");
        notHashLiteral(expression, "string");
        notBooleanLiteral(expression, "string");
    }

    private void booleanLiteralOnly(Expression expression) throws ParseException {
        notStringLiteral(expression, "boolean (true/false)");
        notListLiteral(expression, "boolean (true/false)");
        notHashLiteral(expression, "boolean (true/false)");
        notNumberLiteral(expression, "boolean (true/false)");
    }

    private Expression escapedExpression(Expression expression) {
        return !this.escapes.isEmpty() ? ((EscapeBlock) this.escapes.getFirst()).doEscape(expression) : expression;
    }

    private boolean getBoolean(Expression expression) throws ParseException {
        try {
            TemplateModel eval = expression.eval(null);
            if (eval instanceof TemplateBooleanModel) {
                try {
                    return ((TemplateBooleanModel) eval).getAsBoolean();
                } catch (TemplateModelException unused) {
                }
            }
            if (eval instanceof TemplateScalarModel) {
                try {
                    return StringUtil.getYesNo(((TemplateScalarModel) eval).getAsString());
                } catch (Exception e) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(e.getMessage());
                    stringBuffer.append("\nExpecting boolean (true/false), found: ");
                    stringBuffer.append(expression.getCanonicalForm());
                    throw new ParseException(stringBuffer.toString(), expression);
                }
            } else {
                throw new ParseException("Expecting boolean (true/false) parameter", expression);
            }
        } catch (Exception e2) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(e2.getMessage());
            stringBuffer2.append("\nCould not evaluate expression: ");
            stringBuffer2.append(expression.getCanonicalForm());
            throw new ParseException(stringBuffer2.toString(), expression, e2);
        }
    }

    private ParserIteratorBlockContext pushIteratorBlockContext() {
        if (this.iteratorBlockContexts == null) {
            this.iteratorBlockContexts = new ArrayList(4);
        }
        ParserIteratorBlockContext parserIteratorBlockContext = new ParserIteratorBlockContext();
        this.iteratorBlockContexts.add(parserIteratorBlockContext);
        return parserIteratorBlockContext;
    }

    private void popIteratorBlockContext() {
        List list = this.iteratorBlockContexts;
        list.remove(list.size() - 1);
    }

    private ParserIteratorBlockContext peekIteratorBlockContext() {
        List list = this.iteratorBlockContexts;
        int size = list != null ? list.size() : 0;
        if (size != 0) {
            return (ParserIteratorBlockContext) this.iteratorBlockContexts.get(size - 1);
        }
        return null;
    }

    private void checkLoopVariableBuiltInLHO(String str, Expression expression, Token token2) throws ParseException {
        List list = this.iteratorBlockContexts;
        int size = list != null ? list.size() : 0;
        while (true) {
            size--;
            if (size >= 0) {
                ParserIteratorBlockContext parserIteratorBlockContext = (ParserIteratorBlockContext) this.iteratorBlockContexts.get(size);
                if (str.equals(parserIteratorBlockContext.loopVarName)) {
                    if (parserIteratorBlockContext.kind == 3) {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("The left hand operand of ?");
                        stringBuffer.append(token2.image);
                        stringBuffer.append(" can't be the loop variable of an user defined directive: ");
                        stringBuffer.append(str);
                        throw new ParseException(stringBuffer.toString(), expression);
                    }
                    return;
                }
            } else {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("The left hand operand of ?");
                stringBuffer2.append(token2.image);
                stringBuffer2.append(" must be a loop variable, ");
                stringBuffer2.append("but there's no loop variable in scope with this name: ");
                stringBuffer2.append(str);
                throw new ParseException(stringBuffer2.toString(), expression);
            }
        }
    }

    private String forEachDirectiveSymbol() {
        return this.token_source.namingConvention == 12 ? "#forEach" : "#foreach";
    }

    public final Expression Expression() throws ParseException {
        return OrExpression();
    }

    public final Expression PrimaryExpression() throws ParseException {
        Expression expression;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 124) {
            expression = ListLiteral();
        } else if (i == 126) {
            expression = Parenthesis();
        } else if (i == 128) {
            expression = HashLiteral();
        } else if (i != 133) {
            switch (i) {
                case 85:
                case 86:
                    expression = StringLiteral(true);
                    break;
                case 87:
                case 88:
                    expression = BooleanLiteral();
                    break;
                case 89:
                case 90:
                    expression = NumberLiteral();
                    break;
                case 91:
                    expression = BuiltinVariable();
                    break;
                default:
                    this.jj_la1[0] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        } else {
            expression = Identifier();
        }
        while (jj_2_1(Integer.MAX_VALUE)) {
            expression = AddSubExpression(expression);
        }
        return expression;
    }

    public final Expression Parenthesis() throws ParseException {
        Token jj_consume_token = jj_consume_token(FMParserConstants.OPEN_PAREN);
        Expression Expression = Expression();
        Token jj_consume_token2 = jj_consume_token(FMParserConstants.CLOSE_PAREN);
        ParentheticalExpression parentheticalExpression = new ParentheticalExpression(Expression);
        parentheticalExpression.setLocation(this.template, jj_consume_token, jj_consume_token2);
        return parentheticalExpression;
    }

    public final Expression UnaryExpression() throws ParseException {
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 111 || i == 112) {
            return UnaryPlusMinusExpression();
        }
        if (i == 120) {
            return NotExpression();
        }
        if (!(i == 124 || i == 126 || i == 128 || i == 133)) {
            switch (i) {
                case 85:
                case 86:
                case 87:
                case 88:
                case 89:
                case 90:
                case 91:
                    break;
                default:
                    this.jj_la1[1] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        }
        return PrimaryExpression();
    }

    public final Expression NotExpression() throws ParseException {
        int i;
        ArrayList arrayList = new ArrayList();
        do {
            arrayList.add(jj_consume_token(120));
            i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
                continue;
            }
        } while (i == 120);
        this.jj_la1[2] = this.jj_gen;
        Expression PrimaryExpression = PrimaryExpression();
        int i2 = 0;
        NotExpression notExpression = null;
        while (i2 < arrayList.size()) {
            notExpression = new NotExpression(PrimaryExpression);
            notExpression.setLocation(this.template, (Token) arrayList.get((arrayList.size() - i2) - 1), PrimaryExpression);
            i2++;
            PrimaryExpression = notExpression;
        }
        return notExpression;
    }

    public final Expression UnaryPlusMinusExpression() throws ParseException {
        boolean z;
        Token token2;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 111) {
            token2 = jj_consume_token(111);
            z = false;
        } else if (i == 112) {
            token2 = jj_consume_token(112);
            z = true;
        } else {
            this.jj_la1[3] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        Expression PrimaryExpression = PrimaryExpression();
        UnaryPlusMinusExpression unaryPlusMinusExpression = new UnaryPlusMinusExpression(PrimaryExpression, z);
        unaryPlusMinusExpression.setLocation(this.template, token2, PrimaryExpression);
        return unaryPlusMinusExpression;
    }

    public final Expression AdditiveExpression() throws ParseException {
        boolean z;
        Expression expression;
        Expression MultiplicativeExpression = MultiplicativeExpression();
        while (jj_2_2(Integer.MAX_VALUE)) {
            int i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
            }
            if (i == 111) {
                jj_consume_token(111);
                z = true;
            } else if (i == 112) {
                jj_consume_token(112);
                z = false;
            } else {
                this.jj_la1[4] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
            }
            Expression MultiplicativeExpression2 = MultiplicativeExpression();
            if (z) {
                expression = new AddConcatExpression(MultiplicativeExpression, MultiplicativeExpression2);
            } else {
                numberLiteralOnly(MultiplicativeExpression);
                numberLiteralOnly(MultiplicativeExpression2);
                expression = new ArithmeticExpression(MultiplicativeExpression, MultiplicativeExpression2, 0);
            }
            expression.setLocation(this.template, MultiplicativeExpression, MultiplicativeExpression2);
            MultiplicativeExpression = expression;
        }
        return MultiplicativeExpression;
    }

    public final Expression MultiplicativeExpression() throws ParseException {
        int i;
        Expression UnaryExpression = UnaryExpression();
        while (jj_2_3(Integer.MAX_VALUE)) {
            int i2 = this.jj_ntk;
            if (i2 == -1) {
                i2 = jj_ntk();
            }
            if (i2 == 113) {
                jj_consume_token(113);
                i = 1;
            } else if (i2 == 116) {
                jj_consume_token(116);
                i = 2;
            } else if (i2 == 117) {
                jj_consume_token(117);
                i = 3;
            } else {
                this.jj_la1[5] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
            }
            Expression UnaryExpression2 = UnaryExpression();
            numberLiteralOnly(UnaryExpression);
            numberLiteralOnly(UnaryExpression2);
            ArithmeticExpression arithmeticExpression = new ArithmeticExpression(UnaryExpression, UnaryExpression2, i);
            arithmeticExpression.setLocation(this.template, UnaryExpression, UnaryExpression2);
            UnaryExpression = arithmeticExpression;
        }
        return UnaryExpression;
    }

    public final Expression EqualityExpression() throws ParseException {
        Token token2;
        Expression RelationalExpression = RelationalExpression();
        if (!jj_2_4(Integer.MAX_VALUE)) {
            return RelationalExpression;
        }
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        switch (i) {
            case 97:
                token2 = jj_consume_token(97);
                break;
            case 98:
                token2 = jj_consume_token(98);
                break;
            case 99:
                token2 = jj_consume_token(99);
                break;
            default:
                this.jj_la1[6] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
        Expression RelationalExpression2 = RelationalExpression();
        notHashLiteral(RelationalExpression, "scalar");
        notHashLiteral(RelationalExpression2, "scalar");
        notListLiteral(RelationalExpression, "scalar");
        notListLiteral(RelationalExpression2, "scalar");
        ComparisonExpression comparisonExpression = new ComparisonExpression(RelationalExpression, RelationalExpression2, token2.image);
        comparisonExpression.setLocation(this.template, RelationalExpression, RelationalExpression2);
        return comparisonExpression;
    }

    public final Expression RelationalExpression() throws ParseException {
        Token token2;
        Expression RangeExpression = RangeExpression();
        if (!jj_2_5(Integer.MAX_VALUE)) {
            return RangeExpression;
        }
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 141) {
            token2 = jj_consume_token(FMParserConstants.NATURAL_GT);
        } else if (i != 142) {
            switch (i) {
                case 107:
                    token2 = jj_consume_token(107);
                    break;
                case 108:
                    token2 = jj_consume_token(108);
                    break;
                case 109:
                    token2 = jj_consume_token(109);
                    break;
                case 110:
                    token2 = jj_consume_token(110);
                    break;
                default:
                    this.jj_la1[7] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        } else {
            token2 = jj_consume_token(FMParserConstants.NATURAL_GTE);
        }
        Expression RangeExpression2 = RangeExpression();
        notHashLiteral(RangeExpression, "scalar");
        notHashLiteral(RangeExpression2, "scalar");
        notListLiteral(RangeExpression, "scalar");
        notListLiteral(RangeExpression2, "scalar");
        notStringLiteral(RangeExpression, "number");
        notStringLiteral(RangeExpression2, "number");
        ComparisonExpression comparisonExpression = new ComparisonExpression(RangeExpression, RangeExpression2, token2.image);
        comparisonExpression.setLocation(this.template, RangeExpression, RangeExpression2);
        return comparisonExpression;
    }

    public final Expression RangeExpression() throws ParseException {
        int i;
        Token token2;
        int i2;
        Expression AdditiveExpression = AdditiveExpression();
        int i3 = this.jj_ntk;
        if (i3 == -1) {
            i3 = jj_ntk();
        }
        switch (i3) {
            case 92:
            case 93:
            case 94:
                int i4 = this.jj_ntk;
                if (i4 == -1) {
                    i4 = jj_ntk();
                }
                Expression expression = null;
                switch (i4) {
                    case 92:
                        token2 = jj_consume_token(92);
                        i = 2;
                        if (jj_2_6(Integer.MAX_VALUE)) {
                            expression = AdditiveExpression();
                            i = 0;
                            break;
                        }
                        break;
                    case 93:
                    case 94:
                        int i5 = this.jj_ntk;
                        if (i5 == -1) {
                            i5 = jj_ntk();
                        }
                        if (i5 == 93) {
                            jj_consume_token(93);
                            i2 = 1;
                        } else if (i5 == 94) {
                            jj_consume_token(94);
                            i2 = 3;
                        } else {
                            this.jj_la1[8] = this.jj_gen;
                            jj_consume_token(-1);
                            throw new ParseException();
                        }
                        i = i2;
                        token2 = null;
                        expression = AdditiveExpression();
                        break;
                    default:
                        this.jj_la1[9] = this.jj_gen;
                        jj_consume_token(-1);
                        throw new ParseException();
                }
                numberLiteralOnly(AdditiveExpression);
                if (expression != null) {
                    numberLiteralOnly(expression);
                }
                Range range = new Range(AdditiveExpression, expression, i);
                if (expression != null) {
                    range.setLocation(this.template, AdditiveExpression, expression);
                } else {
                    range.setLocation(this.template, AdditiveExpression, token2);
                }
                return range;
            default:
                this.jj_la1[10] = this.jj_gen;
                return AdditiveExpression;
        }
    }

    public final Expression AndExpression() throws ParseException {
        Expression EqualityExpression = EqualityExpression();
        while (jj_2_7(Integer.MAX_VALUE)) {
            jj_consume_token(118);
            Expression EqualityExpression2 = EqualityExpression();
            booleanLiteralOnly(EqualityExpression);
            booleanLiteralOnly(EqualityExpression2);
            AndExpression andExpression = new AndExpression(EqualityExpression, EqualityExpression2);
            andExpression.setLocation(this.template, EqualityExpression, EqualityExpression2);
            EqualityExpression = andExpression;
        }
        return EqualityExpression;
    }

    public final Expression OrExpression() throws ParseException {
        Expression AndExpression = AndExpression();
        while (jj_2_8(Integer.MAX_VALUE)) {
            jj_consume_token(119);
            Expression AndExpression2 = AndExpression();
            booleanLiteralOnly(AndExpression);
            booleanLiteralOnly(AndExpression2);
            OrExpression orExpression = new OrExpression(AndExpression, AndExpression2);
            orExpression.setLocation(this.template, AndExpression, AndExpression2);
            AndExpression = orExpression;
        }
        return AndExpression;
    }

    public final ListLiteral ListLiteral() throws ParseException {
        new ArrayList();
        Token jj_consume_token = jj_consume_token(124);
        ArrayList PositionalArgs = PositionalArgs();
        Token jj_consume_token2 = jj_consume_token(FMParserConstants.CLOSE_BRACKET);
        ListLiteral listLiteral = new ListLiteral(PositionalArgs);
        listLiteral.setLocation(this.template, jj_consume_token, jj_consume_token2);
        return listLiteral;
    }

    public final Expression NumberLiteral() throws ParseException {
        Token token2;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 89) {
            token2 = jj_consume_token(89);
        } else if (i == 90) {
            token2 = jj_consume_token(90);
        } else {
            this.jj_la1[11] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        NumberLiteral numberLiteral = new NumberLiteral(this.template.getArithmeticEngine().toNumber(token2.image));
        numberLiteral.setLocation(this.template, token2, token2);
        return numberLiteral;
    }

    public final Identifier Identifier() throws ParseException {
        Token jj_consume_token = jj_consume_token(FMParserConstants.f7457ID);
        Identifier identifier = new Identifier(jj_consume_token.image);
        identifier.setLocation(this.template, jj_consume_token, jj_consume_token);
        return identifier;
    }

    public final Expression IdentifierOrStringLiteral() throws ParseException {
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 85 || i == 86) {
            return StringLiteral(false);
        }
        if (i == 133) {
            return Identifier();
        }
        this.jj_la1[12] = this.jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
    }

    public final BuiltinVariable BuiltinVariable() throws ParseException {
        Token jj_consume_token = jj_consume_token(91);
        Token jj_consume_token2 = jj_consume_token(FMParserConstants.f7457ID);
        this.token_source.checkNamingConvention(jj_consume_token2);
        BuiltinVariable builtinVariable = new BuiltinVariable(jj_consume_token2, this.token_source);
        builtinVariable.setLocation(this.template, jj_consume_token, jj_consume_token2);
        return builtinVariable;
    }

    public final Expression AddSubExpression(Expression expression) throws ParseException {
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 91) {
            return DotVariable(expression);
        }
        if (i != 120) {
            if (i == 124) {
                return DynamicKey(expression);
            }
            if (i == 126) {
                return MethodArgs(expression);
            }
            if (i != 144) {
                if (i == 95) {
                    return BuiltIn(expression);
                }
                if (i == 96) {
                    return Exists(expression);
                }
                this.jj_la1[13] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
            }
        }
        return DefaultTo(expression);
    }

    public final Expression DefaultTo(Expression expression) throws ParseException {
        Token token2;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        Expression expression2 = null;
        if (i == 120) {
            token2 = jj_consume_token(120);
            if (jj_2_9(Integer.MAX_VALUE)) {
                expression2 = Expression();
            }
        } else if (i == 144) {
            token2 = jj_consume_token(FMParserConstants.TERMINATING_EXCLAM);
        } else {
            this.jj_la1[14] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        DefaultToExpression defaultToExpression = new DefaultToExpression(expression, expression2);
        if (expression2 == null) {
            defaultToExpression.setLocation(this.template, expression, token2);
        } else {
            defaultToExpression.setLocation(this.template, expression, expression2);
        }
        return defaultToExpression;
    }

    public final Expression Exists(Expression expression) throws ParseException {
        Token jj_consume_token = jj_consume_token(96);
        ExistsExpression existsExpression = new ExistsExpression(expression);
        existsExpression.setLocation(this.template, expression, jj_consume_token);
        return existsExpression;
    }

    public final Expression BuiltIn(Expression expression) throws ParseException {
        jj_consume_token(95);
        Token jj_consume_token = jj_consume_token(FMParserConstants.f7457ID);
        this.token_source.checkNamingConvention(jj_consume_token);
        BuiltIn newBuiltIn = BuiltIn.newBuiltIn(this.incompatibleImprovements, expression, jj_consume_token, this.token_source);
        newBuiltIn.setLocation(this.template, expression, jj_consume_token);
        if (!(newBuiltIn instanceof SpecialBuiltIn)) {
            return newBuiltIn;
        }
        if (newBuiltIn instanceof BuiltInForLoopVariable) {
            if (expression instanceof Identifier) {
                String name = ((Identifier) expression).getName();
                checkLoopVariableBuiltInLHO(name, expression, jj_consume_token);
                ((BuiltInForLoopVariable) newBuiltIn).bindToLoopVariable(name);
                return newBuiltIn;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Expression used as the left hand operand of ?");
            stringBuffer.append(jj_consume_token.image);
            stringBuffer.append(" must be a simple loop variable name.");
            throw new ParseException(stringBuffer.toString(), expression);
        } else if (newBuiltIn instanceof BuiltInWithParseTimeParameters) {
            Token jj_consume_token2 = jj_consume_token(FMParserConstants.OPEN_PAREN);
            ArrayList PositionalArgs = PositionalArgs();
            Token jj_consume_token3 = jj_consume_token(FMParserConstants.CLOSE_PAREN);
            newBuiltIn.setLocation(this.template, expression, jj_consume_token3);
            ((BuiltInWithParseTimeParameters) newBuiltIn).bindToParameters(PositionalArgs, jj_consume_token2, jj_consume_token3);
            return newBuiltIn;
        } else {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Unhandled ");
            Class cls = class$freemarker$core$SpecialBuiltIn;
            if (cls == null) {
                cls = class$("freemarker.core.SpecialBuiltIn");
                class$freemarker$core$SpecialBuiltIn = cls;
            }
            stringBuffer2.append(cls.getName());
            stringBuffer2.append(" subclass: ");
            stringBuffer2.append(newBuiltIn.getClass());
            throw new AssertionError(stringBuffer2.toString());
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public final Expression DotVariable(Expression expression) throws ParseException {
        Token token2;
        jj_consume_token(91);
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (!(i == 87 || i == 88)) {
            if (i == 113) {
                token2 = jj_consume_token(113);
            } else if (i != 114) {
                switch (i) {
                    case 107:
                    case 108:
                    case 109:
                    case 110:
                        break;
                    default:
                        switch (i) {
                            case FMParserConstants.f7459IN /*130*/:
                            case FMParserConstants.f7456AS /*131*/:
                            case 132:
                                break;
                            case FMParserConstants.f7457ID /*133*/:
                                token2 = jj_consume_token(FMParserConstants.f7457ID);
                                break;
                            default:
                                this.jj_la1[16] = this.jj_gen;
                                jj_consume_token(-1);
                                throw new ParseException();
                        }
                }
            } else {
                token2 = jj_consume_token(114);
            }
            notListLiteral(expression, "hash");
            notStringLiteral(expression, "hash");
            notBooleanLiteral(expression, "hash");
            Dot dot = new Dot(expression, token2.image);
            dot.setLocation(this.template, expression, token2);
            return dot;
        }
        int i2 = this.jj_ntk;
        if (i2 == -1) {
            i2 = jj_ntk();
        }
        if (i2 == 87) {
            token2 = jj_consume_token(87);
        } else if (i2 != 88) {
            switch (i2) {
                case 107:
                    token2 = jj_consume_token(107);
                    break;
                case 108:
                    token2 = jj_consume_token(108);
                    break;
                case 109:
                    token2 = jj_consume_token(109);
                    break;
                case 110:
                    token2 = jj_consume_token(110);
                    break;
                default:
                    switch (i2) {
                        case FMParserConstants.f7459IN /*130*/:
                            token2 = jj_consume_token(FMParserConstants.f7459IN);
                            break;
                        case FMParserConstants.f7456AS /*131*/:
                            token2 = jj_consume_token(FMParserConstants.f7456AS);
                            break;
                        case 132:
                            token2 = jj_consume_token(132);
                            break;
                        default:
                            this.jj_la1[15] = this.jj_gen;
                            jj_consume_token(-1);
                            throw new ParseException();
                    }
            }
        } else {
            token2 = jj_consume_token(88);
        }
        if (!Character.isLetter(token2.image.charAt(0))) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(token2.image);
            stringBuffer.append(" is not a valid identifier.");
            throw new ParseException(stringBuffer.toString(), this.template, token2);
        }
        notListLiteral(expression, "hash");
        notStringLiteral(expression, "hash");
        notBooleanLiteral(expression, "hash");
        Dot dot2 = new Dot(expression, token2.image);
        dot2.setLocation(this.template, expression, token2);
        return dot2;
    }

    public final Expression DynamicKey(Expression expression) throws ParseException {
        jj_consume_token(124);
        Expression Expression = Expression();
        Token jj_consume_token = jj_consume_token(FMParserConstants.CLOSE_BRACKET);
        notBooleanLiteral(expression, "list or hash");
        notNumberLiteral(expression, "list or hash");
        DynamicKeyName dynamicKeyName = new DynamicKeyName(expression, Expression);
        dynamicKeyName.setLocation(this.template, expression, jj_consume_token);
        return dynamicKeyName;
    }

    public final MethodCall MethodArgs(Expression expression) throws ParseException {
        new ArrayList();
        jj_consume_token(FMParserConstants.OPEN_PAREN);
        ArrayList PositionalArgs = PositionalArgs();
        Token jj_consume_token = jj_consume_token(FMParserConstants.CLOSE_PAREN);
        PositionalArgs.trimToSize();
        MethodCall methodCall = new MethodCall(expression, PositionalArgs);
        methodCall.setLocation(this.template, expression, jj_consume_token);
        return methodCall;
    }

    public final StringLiteral StringLiteral(boolean z) throws ParseException {
        boolean z2;
        Token token2;
        String str;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 85) {
            token2 = jj_consume_token(85);
            z2 = false;
        } else if (i == 86) {
            token2 = jj_consume_token(86);
            z2 = true;
        } else {
            this.jj_la1[17] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        String str2 = token2.image;
        String substring = str2.substring(1, str2.length() - 1);
        if (z2) {
            str = substring.substring(1);
        } else {
            try {
                str = StringUtil.FTLStringLiteralDec(substring);
            } catch (ParseException e) {
                e.lineNumber = token2.beginLine;
                e.columnNumber = token2.beginColumn;
                e.endLineNumber = token2.endLine;
                e.endColumnNumber = token2.endColumn;
                throw e;
            }
        }
        StringLiteral stringLiteral = new StringLiteral(str);
        stringLiteral.setLocation(this.template, token2, token2);
        if (z && !z2 && (token2.image.indexOf("${") >= 0 || token2.image.indexOf("#{") >= 0)) {
            stringLiteral.parseValue(this.token_source);
        }
        return stringLiteral;
    }

    public final Expression BooleanLiteral() throws ParseException {
        BooleanLiteral booleanLiteral;
        Token token2;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 87) {
            token2 = jj_consume_token(87);
            booleanLiteral = new BooleanLiteral(false);
        } else if (i == 88) {
            token2 = jj_consume_token(88);
            booleanLiteral = new BooleanLiteral(true);
        } else {
            this.jj_la1[18] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        booleanLiteral.setLocation(this.template, token2, token2);
        return booleanLiteral;
    }

    public final HashLiteral HashLiteral() throws ParseException {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Token jj_consume_token = jj_consume_token(128);
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (!(i == 111 || i == 112 || i == 120 || i == 124 || i == 126 || i == 128 || i == 133)) {
            switch (i) {
                case 85:
                case 86:
                case 87:
                case 88:
                case 89:
                case 90:
                case 91:
                    break;
                default:
                    this.jj_la1[22] = this.jj_gen;
                    Token jj_consume_token2 = jj_consume_token(129);
                    HashLiteral hashLiteral = new HashLiteral(arrayList, arrayList2);
                    hashLiteral.setLocation(this.template, jj_consume_token, jj_consume_token2);
                    return hashLiteral;
            }
        }
        Expression Expression = Expression();
        int i2 = this.jj_ntk;
        if (i2 == -1) {
            i2 = jj_ntk();
        }
        if (i2 == 121) {
            jj_consume_token(121);
        } else if (i2 == 123) {
            jj_consume_token(123);
        } else {
            this.jj_la1[19] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        Expression Expression2 = Expression();
        stringLiteralOnly(Expression);
        arrayList.add(Expression);
        arrayList2.add(Expression2);
        while (true) {
            int i3 = this.jj_ntk;
            if (i3 == -1) {
                i3 = jj_ntk();
            }
            if (i3 != 121) {
                this.jj_la1[20] = this.jj_gen;
            } else {
                jj_consume_token(121);
                Expression Expression3 = Expression();
                int i4 = this.jj_ntk;
                if (i4 == -1) {
                    i4 = jj_ntk();
                }
                if (i4 == 121) {
                    jj_consume_token(121);
                } else if (i4 == 123) {
                    jj_consume_token(123);
                } else {
                    this.jj_la1[21] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
                }
                Expression Expression4 = Expression();
                stringLiteralOnly(Expression3);
                arrayList.add(Expression3);
                arrayList2.add(Expression4);
            }
        }
        Token jj_consume_token22 = jj_consume_token(129);
        HashLiteral hashLiteral2 = new HashLiteral(arrayList, arrayList2);
        hashLiteral2.setLocation(this.template, jj_consume_token, jj_consume_token22);
        return hashLiteral2;
    }

    public final DollarVariable StringOutput() throws ParseException {
        Token jj_consume_token = jj_consume_token(75);
        Expression Expression = Expression();
        notHashLiteral(Expression, "string or something automatically convertible to string (number, date or boolean)");
        notListLiteral(Expression, "string or something automatically convertible to string (number, date or boolean)");
        Token jj_consume_token2 = jj_consume_token(129);
        DollarVariable dollarVariable = new DollarVariable(Expression, escapedExpression(Expression));
        dollarVariable.setLocation(this.template, jj_consume_token, jj_consume_token2);
        return dollarVariable;
    }

    public final NumericalOutput NumericalOutput() throws ParseException {
        Token token2;
        NumericalOutput numericalOutput;
        Token jj_consume_token = jj_consume_token(76);
        Expression Expression = Expression();
        numberLiteralOnly(Expression);
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i != 122) {
            this.jj_la1[23] = this.jj_gen;
            token2 = null;
        } else {
            jj_consume_token(122);
            token2 = jj_consume_token(FMParserConstants.f7457ID);
        }
        Token jj_consume_token2 = jj_consume_token(129);
        if (token2 != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(token2.image, "mM", true);
            int i2 = -1;
            int i3 = -1;
            while (true) {
                char c = '-';
                while (stringTokenizer.hasMoreTokens()) {
                    String nextToken = stringTokenizer.nextToken();
                    if (c != '-') {
                        if (c != 'M') {
                            if (c != 'm') {
                                throw new ParseException("Invalid formatting string", this.template, token2);
                            } else if (i3 == -1) {
                                try {
                                    i3 = Integer.parseInt(nextToken);
                                } catch (ParseException unused) {
                                    StringBuffer stringBuffer = new StringBuffer();
                                    stringBuffer.append("Invalid format specifier ");
                                    stringBuffer.append(token2.image);
                                    throw new ParseException(stringBuffer.toString(), this.template, token2);
                                } catch (NumberFormatException unused2) {
                                    StringBuffer stringBuffer2 = new StringBuffer();
                                    stringBuffer2.append("Invalid number in the format specifier ");
                                    stringBuffer2.append(token2.image);
                                    throw new ParseException(stringBuffer2.toString(), this.template, token2);
                                }
                            } else {
                                throw new ParseException("Invalid formatting string", this.template, token2);
                            }
                        } else if (i2 == -1) {
                            i2 = Integer.parseInt(nextToken);
                        } else {
                            throw new ParseException("Invalid formatting string", this.template, token2);
                        }
                    } else if (nextToken.equals(Config.MODEL)) {
                        c = 'm';
                    } else if (nextToken.equals("M")) {
                        c = 'M';
                    } else {
                        throw new ParseException();
                    }
                }
                if (i2 == -1) {
                    if (i3 != -1) {
                        i2 = i3;
                    } else {
                        throw new ParseException("Invalid format specification, at least one of m and M must be specified!", this.template, token2);
                    }
                } else if (i3 == -1) {
                    i3 = 0;
                }
                if (i3 > i2) {
                    throw new ParseException("Invalid format specification, min cannot be greater than max!", this.template, token2);
                } else if (i3 > 50 || i2 > 50) {
                    throw new ParseException("Cannot specify more than 50 fraction digits", this.template, token2);
                } else {
                    numericalOutput = new NumericalOutput(Expression, i3, i2);
                }
            }
        } else {
            numericalOutput = new NumericalOutput(Expression);
        }
        numericalOutput.setLocation(this.template, jj_consume_token, jj_consume_token2);
        return numericalOutput;
    }

    /* renamed from: If */
    public final TemplateElement mo36036If() throws ParseException {
        Token jj_consume_token = jj_consume_token(8);
        Expression Expression = Expression();
        jj_consume_token(FMParserConstants.DIRECTIVE_END);
        TemplateElement OptionalBlock = OptionalBlock();
        ConditionalBlock conditionalBlock = new ConditionalBlock(Expression, OptionalBlock, 0);
        conditionalBlock.setLocation(this.template, jj_consume_token, OptionalBlock);
        IfBlock ifBlock = new IfBlock(conditionalBlock);
        while (true) {
            int i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
            }
            if (i != 9) {
                break;
            }
            Token jj_consume_token2 = jj_consume_token(9);
            Expression Expression2 = Expression();
            LooseDirectiveEnd();
            TemplateElement OptionalBlock2 = OptionalBlock();
            ConditionalBlock conditionalBlock2 = new ConditionalBlock(Expression2, OptionalBlock2, 2);
            conditionalBlock2.setLocation(this.template, jj_consume_token2, OptionalBlock2);
            ifBlock.addBlock(conditionalBlock2);
        }
        this.jj_la1[24] = this.jj_gen;
        int i2 = this.jj_ntk;
        if (i2 == -1) {
            i2 = jj_ntk();
        }
        if (i2 != 48) {
            this.jj_la1[25] = this.jj_gen;
        } else {
            Token jj_consume_token3 = jj_consume_token(48);
            TemplateElement OptionalBlock3 = OptionalBlock();
            ConditionalBlock conditionalBlock3 = new ConditionalBlock(null, OptionalBlock3, 1);
            conditionalBlock3.setLocation(this.template, jj_consume_token3, OptionalBlock3);
            ifBlock.addBlock(conditionalBlock3);
        }
        ifBlock.setLocation(this.template, jj_consume_token, jj_consume_token(33));
        return ifBlock;
    }

    public final AttemptBlock Attempt() throws ParseException {
        Token token2;
        Token jj_consume_token = jj_consume_token(6);
        TemplateElement OptionalBlock = OptionalBlock();
        RecoveryBlock Recover = Recover();
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 37) {
            token2 = jj_consume_token(37);
        } else if (i == 38) {
            token2 = jj_consume_token(38);
        } else {
            this.jj_la1[26] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        AttemptBlock attemptBlock = new AttemptBlock(OptionalBlock, Recover);
        attemptBlock.setLocation(this.template, jj_consume_token, token2);
        return attemptBlock;
    }

    public final RecoveryBlock Recover() throws ParseException {
        Token jj_consume_token = jj_consume_token(7);
        TemplateElement OptionalBlock = OptionalBlock();
        RecoveryBlock recoveryBlock = new RecoveryBlock(OptionalBlock);
        recoveryBlock.setLocation(this.template, jj_consume_token, OptionalBlock);
        return recoveryBlock;
    }

    public final TemplateElement List() throws ParseException {
        Token token2;
        ElseOfList elseOfList;
        Token jj_consume_token = jj_consume_token(10);
        Expression Expression = Expression();
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        String str = null;
        if (i != 131) {
            this.jj_la1[27] = this.jj_gen;
            token2 = null;
        } else {
            jj_consume_token(FMParserConstants.f7456AS);
            token2 = jj_consume_token(FMParserConstants.f7457ID);
        }
        jj_consume_token(FMParserConstants.DIRECTIVE_END);
        ParserIteratorBlockContext pushIteratorBlockContext = pushIteratorBlockContext();
        if (token2 != null) {
            String unused = pushIteratorBlockContext.loopVarName = token2.image;
            this.breakableDirectiveNesting++;
        }
        TemplateElement OptionalBlock = OptionalBlock();
        if (token2 != null) {
            this.breakableDirectiveNesting--;
        } else if (pushIteratorBlockContext.kind != 2) {
            throw new ParseException("#list must have either \"as loopVar\" parameter or nested #items that belongs to it.", this.template, jj_consume_token);
        }
        popIteratorBlockContext();
        int i2 = this.jj_ntk;
        if (i2 == -1) {
            i2 = jj_ntk();
        }
        if (i2 != 48) {
            this.jj_la1[28] = this.jj_gen;
            elseOfList = null;
        } else {
            elseOfList = ElseOfList();
        }
        Token jj_consume_token2 = jj_consume_token(34);
        if (token2 != null) {
            str = token2.image;
        }
        IteratorBlock iteratorBlock = new IteratorBlock(Expression, str, OptionalBlock, false);
        iteratorBlock.setLocation(this.template, jj_consume_token, jj_consume_token2);
        if (elseOfList == null) {
            return iteratorBlock;
        }
        ListElseContainer listElseContainer = new ListElseContainer(iteratorBlock, elseOfList);
        listElseContainer.setLocation(this.template, jj_consume_token, jj_consume_token2);
        return listElseContainer;
    }

    public final ElseOfList ElseOfList() throws ParseException {
        Token jj_consume_token = jj_consume_token(48);
        TemplateElement OptionalBlock = OptionalBlock();
        ElseOfList elseOfList = new ElseOfList(OptionalBlock);
        elseOfList.setLocation(this.template, jj_consume_token, OptionalBlock);
        return elseOfList;
    }

    public final IteratorBlock ForEach() throws ParseException {
        Token jj_consume_token = jj_consume_token(13);
        Token jj_consume_token2 = jj_consume_token(FMParserConstants.f7457ID);
        jj_consume_token(FMParserConstants.f7459IN);
        Expression Expression = Expression();
        jj_consume_token(FMParserConstants.DIRECTIVE_END);
        ParserIteratorBlockContext pushIteratorBlockContext = pushIteratorBlockContext();
        String unused = pushIteratorBlockContext.loopVarName = jj_consume_token2.image;
        int unused2 = pushIteratorBlockContext.kind = 1;
        this.breakableDirectiveNesting++;
        TemplateElement OptionalBlock = OptionalBlock();
        Token jj_consume_token3 = jj_consume_token(39);
        this.breakableDirectiveNesting--;
        popIteratorBlockContext();
        IteratorBlock iteratorBlock = new IteratorBlock(Expression, jj_consume_token2.image, OptionalBlock, true);
        iteratorBlock.setLocation(this.template, jj_consume_token, jj_consume_token3);
        return iteratorBlock;
    }

    public final Items Items() throws ParseException {
        String str;
        Token jj_consume_token = jj_consume_token(11);
        Token jj_consume_token2 = jj_consume_token(FMParserConstants.f7457ID);
        jj_consume_token(FMParserConstants.DIRECTIVE_END);
        ParserIteratorBlockContext peekIteratorBlockContext = peekIteratorBlockContext();
        if (peekIteratorBlockContext == null) {
            throw new ParseException("#items must be inside a #list block.", this.template, jj_consume_token);
        } else if (peekIteratorBlockContext.loopVarName != null) {
            if (peekIteratorBlockContext.kind != 1) {
                str = peekIteratorBlockContext.kind == 2 ? "Can't nest #items into each other that belong to the same #list." : "The parent #list of the #items must not have \"as loopVar\" parameter.";
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(forEachDirectiveSymbol());
                stringBuffer.append(" doesn't support nested #items.");
                str = stringBuffer.toString();
            }
            throw new ParseException(str, this.template, jj_consume_token);
        } else {
            int unused = peekIteratorBlockContext.kind = 2;
            String unused2 = peekIteratorBlockContext.loopVarName = jj_consume_token2.image;
            this.breakableDirectiveNesting++;
            TemplateElement OptionalBlock = OptionalBlock();
            Token jj_consume_token3 = jj_consume_token(35);
            this.breakableDirectiveNesting--;
            String unused3 = peekIteratorBlockContext.loopVarName = null;
            Items items = new Items(jj_consume_token2.image, OptionalBlock);
            items.setLocation(this.template, jj_consume_token, jj_consume_token3);
            return items;
        }
    }

    public final Sep Sep() throws ParseException {
        Token token2;
        Token jj_consume_token = jj_consume_token(12);
        if (peekIteratorBlockContext() != null) {
            TemplateElement OptionalBlock = OptionalBlock();
            int i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
            }
            if (i != 36) {
                this.jj_la1[29] = this.jj_gen;
                token2 = null;
            } else {
                token2 = jj_consume_token(36);
            }
            Sep sep = new Sep(OptionalBlock);
            if (token2 != null) {
                sep.setLocation(this.template, jj_consume_token, token2);
            } else {
                sep.setLocation(this.template, jj_consume_token, OptionalBlock);
            }
            return sep;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("#sep must be inside a #list (or ");
        stringBuffer.append(forEachDirectiveSymbol());
        stringBuffer.append(") block.");
        throw new ParseException(stringBuffer.toString(), this.template, jj_consume_token);
    }

    public final VisitNode Visit() throws ParseException {
        Expression expression;
        Token jj_consume_token = jj_consume_token(24);
        Expression Expression = Expression();
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i != 132) {
            this.jj_la1[30] = this.jj_gen;
            expression = null;
        } else {
            jj_consume_token(132);
            expression = Expression();
        }
        Token LooseDirectiveEnd = LooseDirectiveEnd();
        VisitNode visitNode = new VisitNode(Expression, expression);
        visitNode.setLocation(this.template, jj_consume_token, LooseDirectiveEnd);
        return visitNode;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final freemarker.core.RecurseNode Recurse() throws freemarker.core.ParseException {
        /*
            r6 = this;
            int r0 = r6.jj_ntk
            r1 = -1
            if (r0 != r1) goto L_0x0009
            int r0 = r6.jj_ntk()
        L_0x0009:
            r2 = 60
            r3 = 0
            if (r0 == r2) goto L_0x007d
            r2 = 61
            if (r0 != r2) goto L_0x006c
            freemarker.core.Token r0 = r6.jj_consume_token(r2)
            int r2 = r6.jj_ntk
            if (r2 != r1) goto L_0x001e
            int r2 = r6.jj_ntk()
        L_0x001e:
            r4 = 111(0x6f, float:1.56E-43)
            if (r2 == r4) goto L_0x0047
            r4 = 112(0x70, float:1.57E-43)
            if (r2 == r4) goto L_0x0047
            r4 = 120(0x78, float:1.68E-43)
            if (r2 == r4) goto L_0x0047
            r4 = 124(0x7c, float:1.74E-43)
            if (r2 == r4) goto L_0x0047
            r4 = 126(0x7e, float:1.77E-43)
            if (r2 == r4) goto L_0x0047
            r4 = 128(0x80, float:1.794E-43)
            if (r2 == r4) goto L_0x0047
            r4 = 133(0x85, float:1.86E-43)
            if (r2 == r4) goto L_0x0047
            switch(r2) {
                case 85: goto L_0x0047;
                case 86: goto L_0x0047;
                case 87: goto L_0x0047;
                case 88: goto L_0x0047;
                case 89: goto L_0x0047;
                case 90: goto L_0x0047;
                case 91: goto L_0x0047;
                default: goto L_0x003d;
            }
        L_0x003d:
            int[] r2 = r6.jj_la1
            r4 = 31
            int r5 = r6.jj_gen
            r2[r4] = r5
            r2 = r3
            goto L_0x004b
        L_0x0047:
            freemarker.core.Expression r2 = r6.Expression()
        L_0x004b:
            int r4 = r6.jj_ntk
            if (r4 != r1) goto L_0x0053
            int r4 = r6.jj_ntk()
        L_0x0053:
            r1 = 132(0x84, float:1.85E-43)
            if (r4 == r1) goto L_0x0060
            int[] r1 = r6.jj_la1
            r4 = 32
            int r5 = r6.jj_gen
            r1[r4] = r5
            goto L_0x0067
        L_0x0060:
            r6.jj_consume_token(r1)
            freemarker.core.Expression r3 = r6.Expression()
        L_0x0067:
            freemarker.core.Token r1 = r6.LooseDirectiveEnd()
            goto L_0x0083
        L_0x006c:
            int[] r0 = r6.jj_la1
            r2 = 33
            int r3 = r6.jj_gen
            r0[r2] = r3
            r6.jj_consume_token(r1)
            freemarker.core.ParseException r0 = new freemarker.core.ParseException
            r0.<init>()
            throw r0
        L_0x007d:
            freemarker.core.Token r0 = r6.jj_consume_token(r2)
            r1 = r3
            r2 = r1
        L_0x0083:
            if (r1 != 0) goto L_0x0086
            r1 = r0
        L_0x0086:
            freemarker.core.RecurseNode r4 = new freemarker.core.RecurseNode
            r4.<init>(r2, r3)
            freemarker.template.Template r2 = r6.template
            r4.setLocation(r2, r0, r1)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.FMParser.Recurse():freemarker.core.RecurseNode");
    }

    public final FallbackInstruction FallBack() throws ParseException {
        Token jj_consume_token = jj_consume_token(62);
        if (this.inMacro) {
            FallbackInstruction fallbackInstruction = new FallbackInstruction();
            fallbackInstruction.setLocation(this.template, jj_consume_token, jj_consume_token);
            return fallbackInstruction;
        }
        throw new ParseException("Cannot fall back outside a macro.", this.template, jj_consume_token);
    }

    public final BreakInstruction Break() throws ParseException {
        Token jj_consume_token = jj_consume_token(49);
        if (this.breakableDirectiveNesting >= 1) {
            BreakInstruction breakInstruction = new BreakInstruction();
            breakInstruction.setLocation(this.template, jj_consume_token, jj_consume_token);
            return breakInstruction;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(jj_consume_token.image);
        stringBuffer.append(" must be nested inside a directive that supports it: ");
        stringBuffer.append(" #list with \"as\", #items, #switch (or the deprecated ");
        stringBuffer.append(forEachDirectiveSymbol());
        stringBuffer.append(")");
        throw new ParseException(stringBuffer.toString(), this.template, jj_consume_token);
    }

    public final ReturnInstruction Return() throws ParseException {
        Token token2;
        Expression expression;
        Token token3;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 26) {
            token3 = jj_consume_token(26);
            expression = Expression();
            token2 = LooseDirectiveEnd();
        } else if (i == 50) {
            token3 = jj_consume_token(50);
            expression = null;
            token2 = token3;
        } else {
            this.jj_la1[34] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        if (this.inMacro) {
            if (expression != null) {
                throw new ParseException("A macro cannot return a value", this.template, token3);
            }
        } else if (this.inFunction) {
            if (expression == null) {
                throw new ParseException("A function must return a value", this.template, token3);
            }
        } else if (expression == null) {
            throw new ParseException("A return instruction can only occur inside a macro or function", this.template, token3);
        }
        ReturnInstruction returnInstruction = new ReturnInstruction(expression);
        returnInstruction.setLocation(this.template, token3, token2);
        return returnInstruction;
    }

    public final StopInstruction Stop() throws ParseException {
        Expression expression;
        Token token2;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 25) {
            token2 = jj_consume_token(25);
            expression = Expression();
            LooseDirectiveEnd();
        } else if (i == 51) {
            token2 = jj_consume_token(51);
            expression = null;
        } else {
            this.jj_la1[35] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        StopInstruction stopInstruction = new StopInstruction(expression);
        stopInstruction.setLocation(this.template, token2, token2);
        return stopInstruction;
    }

    public final TemplateElement Nested() throws ParseException {
        BodyInstruction bodyInstruction;
        Token token2;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 58) {
            token2 = jj_consume_token(58);
            bodyInstruction = new BodyInstruction(null);
            bodyInstruction.setLocation(this.template, token2, token2);
        } else if (i == 59) {
            token2 = jj_consume_token(59);
            ArrayList PositionalArgs = PositionalArgs();
            Token LooseDirectiveEnd = LooseDirectiveEnd();
            bodyInstruction = new BodyInstruction(PositionalArgs);
            bodyInstruction.setLocation(this.template, token2, LooseDirectiveEnd);
        } else {
            this.jj_la1[36] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        if (this.inMacro) {
            return bodyInstruction;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Cannot use a ");
        stringBuffer.append(token2.image);
        stringBuffer.append(" instruction outside a macro.");
        throw new ParseException(stringBuffer.toString(), this.template, token2);
    }

    public final TemplateElement Flush() throws ParseException {
        Token jj_consume_token = jj_consume_token(52);
        FlushInstruction flushInstruction = new FlushInstruction();
        flushInstruction.setLocation(this.template, jj_consume_token, jj_consume_token);
        return flushInstruction;
    }

    public final TemplateElement Trim() throws ParseException {
        TrimInstruction trimInstruction;
        Token token2;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        switch (i) {
            case 53:
                token2 = jj_consume_token(53);
                trimInstruction = new TrimInstruction(true, true);
                break;
            case 54:
                token2 = jj_consume_token(54);
                trimInstruction = new TrimInstruction(true, false);
                break;
            case 55:
                token2 = jj_consume_token(55);
                trimInstruction = new TrimInstruction(false, true);
                break;
            case 56:
                token2 = jj_consume_token(56);
                trimInstruction = new TrimInstruction(false, false);
                break;
            default:
                this.jj_la1[37] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
        trimInstruction.setLocation(this.template, token2, token2);
        return trimInstruction;
    }

    /* JADX WARNING: Removed duplicated region for block: B:145:0x02a0  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x02b8  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x02cb  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x02f0  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01b7  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01ce  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final freemarker.core.TemplateElement Assign() throws freemarker.core.ParseException {
        /*
            r16 = this;
            r0 = r16
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            int r2 = r0.jj_ntk
            r3 = -1
            if (r2 != r3) goto L_0x0010
            int r2 = r16.jj_ntk()
        L_0x0010:
            r4 = 2
            r5 = 3
            r6 = 1
            switch(r2) {
                case 16: goto L_0x004c;
                case 17: goto L_0x0043;
                case 18: goto L_0x0027;
                default: goto L_0x0016;
            }
        L_0x0016:
            int[] r1 = r0.jj_la1
            r2 = 38
            int r4 = r0.jj_gen
            r1[r2] = r4
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x0027:
            r2 = 18
            freemarker.core.Token r2 = r0.jj_consume_token(r2)
            boolean r7 = r0.inMacro
            if (r7 != 0) goto L_0x0040
            boolean r7 = r0.inFunction
            if (r7 == 0) goto L_0x0036
            goto L_0x0040
        L_0x0036:
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            freemarker.template.Template r3 = r0.template
            java.lang.String r4 = "Local variable assigned outside a macro."
            r1.<init>(r4, r3, r2)
            throw r1
        L_0x0040:
            r7 = r2
            r2 = 2
            goto L_0x0054
        L_0x0043:
            r2 = 17
            freemarker.core.Token r2 = r0.jj_consume_token(r2)
            r7 = r2
            r2 = 3
            goto L_0x0054
        L_0x004c:
            r2 = 16
            freemarker.core.Token r2 = r0.jj_consume_token(r2)
            r7 = r2
            r2 = 1
        L_0x0054:
            freemarker.core.Expression r8 = r16.IdentifierOrStringLiteral()
            boolean r9 = r8 instanceof freemarker.core.StringLiteral
            if (r9 == 0) goto L_0x0064
            r9 = r8
            freemarker.core.StringLiteral r9 = (freemarker.core.StringLiteral) r9
            java.lang.String r9 = r9.getAsString()
            goto L_0x006b
        L_0x0064:
            r9 = r8
            freemarker.core.Identifier r9 = (freemarker.core.Identifier) r9
            java.lang.String r9 = r9.getName()
        L_0x006b:
            int r10 = r0.jj_ntk
            if (r10 != r3) goto L_0x0073
            int r10 = r16.jj_ntk()
        L_0x0073:
            java.lang.String r11 = "Cannot assign to namespace here."
            r12 = 41
            r14 = 130(0x82, float:1.82E-43)
            r15 = 97
            if (r10 == r15) goto L_0x0121
            r13 = 139(0x8b, float:1.95E-43)
            if (r10 == r14) goto L_0x0097
            if (r10 == r13) goto L_0x0097
            switch(r10) {
                case 100: goto L_0x0121;
                case 101: goto L_0x0121;
                case 102: goto L_0x0121;
                case 103: goto L_0x0121;
                case 104: goto L_0x0121;
                case 105: goto L_0x0121;
                case 106: goto L_0x0121;
                default: goto L_0x0086;
            }
        L_0x0086:
            int[] r1 = r0.jj_la1
            r2 = 49
            int r4 = r0.jj_gen
            r1[r2] = r4
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x0097:
            int r1 = r0.jj_ntk
            if (r1 != r3) goto L_0x009f
            int r1 = r16.jj_ntk()
        L_0x009f:
            if (r1 == r14) goto L_0x00ab
            int[] r1 = r0.jj_la1
            r8 = 47
            int r10 = r0.jj_gen
            r1[r8] = r10
            r8 = 0
            goto L_0x00b5
        L_0x00ab:
            freemarker.core.Token r1 = r0.jj_consume_token(r14)
            freemarker.core.Expression r8 = r16.Expression()
            if (r2 != r6) goto L_0x0119
        L_0x00b5:
            r0.jj_consume_token(r13)
            freemarker.core.TemplateElement r1 = r16.OptionalBlock()
            int r10 = r0.jj_ntk
            if (r10 != r3) goto L_0x00c4
            int r10 = r16.jj_ntk()
        L_0x00c4:
            switch(r10) {
                case 40: goto L_0x00fc;
                case 41: goto L_0x00eb;
                case 42: goto L_0x00d8;
                default: goto L_0x00c7;
            }
        L_0x00c7:
            int[] r1 = r0.jj_la1
            r2 = 48
            int r4 = r0.jj_gen
            r1[r2] = r4
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x00d8:
            r3 = 42
            freemarker.core.Token r3 = r0.jj_consume_token(r3)
            if (r2 != r6) goto L_0x00e1
            goto L_0x0104
        L_0x00e1:
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            freemarker.template.Template r2 = r0.template
            java.lang.String r4 = "Mismatched assignment tags."
            r1.<init>(r4, r2, r3)
            throw r1
        L_0x00eb:
            freemarker.core.Token r3 = r0.jj_consume_token(r12)
            if (r2 != r5) goto L_0x00f2
            goto L_0x0104
        L_0x00f2:
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            freemarker.template.Template r2 = r0.template
            java.lang.String r4 = "Mismatched assignment tags"
            r1.<init>(r4, r2, r3)
            throw r1
        L_0x00fc:
            r3 = 40
            freemarker.core.Token r3 = r0.jj_consume_token(r3)
            if (r2 != r4) goto L_0x010f
        L_0x0104:
            freemarker.core.BlockAssignment r4 = new freemarker.core.BlockAssignment
            r4.<init>(r1, r9, r2, r8)
            freemarker.template.Template r1 = r0.template
            r4.setLocation(r1, r7, r3)
            return r4
        L_0x010f:
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            freemarker.template.Template r2 = r0.template
            java.lang.String r4 = "Mismatched assignment tags."
            r1.<init>(r4, r2, r3)
            throw r1
        L_0x0119:
            freemarker.core.ParseException r2 = new freemarker.core.ParseException
            freemarker.template.Template r3 = r0.template
            r2.<init>(r11, r3, r1)
            throw r2
        L_0x0121:
            int r4 = r0.jj_ntk
            if (r4 != r3) goto L_0x0129
            int r4 = r16.jj_ntk()
        L_0x0129:
            r5 = 102(0x66, float:1.43E-43)
            r10 = 101(0x65, float:1.42E-43)
            r13 = 100
            r6 = 106(0x6a, float:1.49E-43)
            r14 = 105(0x69, float:1.47E-43)
            if (r4 == r15) goto L_0x016f
            switch(r4) {
                case 100: goto L_0x016f;
                case 101: goto L_0x016f;
                case 102: goto L_0x016f;
                case 103: goto L_0x016f;
                case 104: goto L_0x016f;
                case 105: goto L_0x0147;
                case 106: goto L_0x0147;
                default: goto L_0x0138;
            }
        L_0x0138:
            int[] r1 = r0.jj_la1
            int r2 = r0.jj_gen
            r1[r12] = r2
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x0147:
            int r4 = r0.jj_ntk
            if (r4 != r3) goto L_0x014f
            int r4 = r16.jj_ntk()
        L_0x014f:
            if (r4 == r14) goto L_0x0168
            if (r4 != r6) goto L_0x0157
            r0.jj_consume_token(r6)
            goto L_0x016b
        L_0x0157:
            int[] r1 = r0.jj_la1
            r2 = 40
            int r4 = r0.jj_gen
            r1[r2] = r4
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x0168:
            r0.jj_consume_token(r14)
        L_0x016b:
            freemarker.core.Token r4 = r0.token
            r12 = 0
            goto L_0x01ae
        L_0x016f:
            int r4 = r0.jj_ntk
            if (r4 != r3) goto L_0x0177
            int r4 = r16.jj_ntk()
        L_0x0177:
            if (r4 == r15) goto L_0x01a5
            switch(r4) {
                case 100: goto L_0x01a1;
                case 101: goto L_0x019d;
                case 102: goto L_0x0199;
                case 103: goto L_0x0193;
                case 104: goto L_0x018d;
                default: goto L_0x017c;
            }
        L_0x017c:
            int[] r1 = r0.jj_la1
            r2 = 39
            int r4 = r0.jj_gen
            r1[r2] = r4
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x018d:
            r4 = 104(0x68, float:1.46E-43)
            r0.jj_consume_token(r4)
            goto L_0x01a8
        L_0x0193:
            r4 = 103(0x67, float:1.44E-43)
            r0.jj_consume_token(r4)
            goto L_0x01a8
        L_0x0199:
            r0.jj_consume_token(r5)
            goto L_0x01a8
        L_0x019d:
            r0.jj_consume_token(r10)
            goto L_0x01a8
        L_0x01a1:
            r0.jj_consume_token(r13)
            goto L_0x01a8
        L_0x01a5:
            r0.jj_consume_token(r15)
        L_0x01a8:
            freemarker.core.Token r4 = r0.token
            freemarker.core.Expression r12 = r16.Expression()
        L_0x01ae:
            freemarker.core.Assignment r13 = new freemarker.core.Assignment
            int r10 = r4.kind
            r13.<init>(r9, r10, r12, r2)
            if (r12 == 0) goto L_0x01bd
            freemarker.template.Template r4 = r0.template
            r13.setLocation(r4, r8, r12)
            goto L_0x01c2
        L_0x01bd:
            freemarker.template.Template r9 = r0.template
            r13.setLocation(r9, r8, r4)
        L_0x01c2:
            r1.add(r13)
        L_0x01c5:
            r4 = 2147483647(0x7fffffff, float:NaN)
            boolean r4 = r0.jj_2_10(r4)
            if (r4 == 0) goto L_0x02b4
            int r4 = r0.jj_ntk
            if (r4 != r3) goto L_0x01d6
            int r4 = r16.jj_ntk()
        L_0x01d6:
            r8 = 121(0x79, float:1.7E-43)
            if (r4 == r8) goto L_0x01e3
            int[] r4 = r0.jj_la1
            r8 = 42
            int r9 = r0.jj_gen
            r4[r8] = r9
            goto L_0x01e8
        L_0x01e3:
            r4 = 121(0x79, float:1.7E-43)
            r0.jj_consume_token(r4)
        L_0x01e8:
            freemarker.core.Expression r4 = r16.IdentifierOrStringLiteral()
            boolean r8 = r4 instanceof freemarker.core.StringLiteral
            if (r8 == 0) goto L_0x01f8
            r8 = r4
            freemarker.core.StringLiteral r8 = (freemarker.core.StringLiteral) r8
            java.lang.String r8 = r8.getAsString()
            goto L_0x01ff
        L_0x01f8:
            r8 = r4
            freemarker.core.Identifier r8 = (freemarker.core.Identifier) r8
            java.lang.String r8 = r8.getName()
        L_0x01ff:
            int r9 = r0.jj_ntk
            if (r9 != r3) goto L_0x0207
            int r9 = r16.jj_ntk()
        L_0x0207:
            if (r9 == r15) goto L_0x024a
            switch(r9) {
                case 100: goto L_0x024a;
                case 101: goto L_0x024a;
                case 102: goto L_0x024a;
                case 103: goto L_0x024a;
                case 104: goto L_0x024a;
                case 105: goto L_0x021d;
                case 106: goto L_0x021d;
                default: goto L_0x020c;
            }
        L_0x020c:
            int[] r1 = r0.jj_la1
            r2 = 45
            int r4 = r0.jj_gen
            r1[r2] = r4
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x021d:
            int r9 = r0.jj_ntk
            if (r9 != r3) goto L_0x0225
            int r9 = r16.jj_ntk()
        L_0x0225:
            if (r9 == r14) goto L_0x023e
            if (r9 != r6) goto L_0x022d
            r0.jj_consume_token(r6)
            goto L_0x0241
        L_0x022d:
            int[] r1 = r0.jj_la1
            r2 = 44
            int r4 = r0.jj_gen
            r1[r2] = r4
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x023e:
            r0.jj_consume_token(r14)
        L_0x0241:
            freemarker.core.Token r9 = r0.token
            r12 = r9
            r9 = 101(0x65, float:1.42E-43)
            r10 = 100
            r13 = 0
            goto L_0x0297
        L_0x024a:
            int r9 = r0.jj_ntk
            if (r9 != r3) goto L_0x0252
            int r9 = r16.jj_ntk()
        L_0x0252:
            if (r9 == r15) goto L_0x028a
            switch(r9) {
                case 100: goto L_0x0282;
                case 101: goto L_0x027a;
                case 102: goto L_0x0274;
                case 103: goto L_0x026e;
                case 104: goto L_0x0268;
                default: goto L_0x0257;
            }
        L_0x0257:
            int[] r1 = r0.jj_la1
            r2 = 43
            int r4 = r0.jj_gen
            r1[r2] = r4
            r0.jj_consume_token(r3)
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            r1.<init>()
            throw r1
        L_0x0268:
            r9 = 104(0x68, float:1.46E-43)
            r0.jj_consume_token(r9)
            goto L_0x0277
        L_0x026e:
            r9 = 103(0x67, float:1.44E-43)
            r0.jj_consume_token(r9)
            goto L_0x0277
        L_0x0274:
            r0.jj_consume_token(r5)
        L_0x0277:
            r9 = 101(0x65, float:1.42E-43)
            goto L_0x027f
        L_0x027a:
            r9 = 101(0x65, float:1.42E-43)
            r0.jj_consume_token(r9)
        L_0x027f:
            r10 = 100
            goto L_0x0291
        L_0x0282:
            r9 = 101(0x65, float:1.42E-43)
            r10 = 100
            r0.jj_consume_token(r10)
            goto L_0x0291
        L_0x028a:
            r9 = 101(0x65, float:1.42E-43)
            r10 = 100
            r0.jj_consume_token(r15)
        L_0x0291:
            freemarker.core.Token r12 = r0.token
            freemarker.core.Expression r13 = r16.Expression()
        L_0x0297:
            freemarker.core.Assignment r5 = new freemarker.core.Assignment
            int r6 = r12.kind
            r5.<init>(r8, r6, r13, r2)
            if (r13 == 0) goto L_0x02a6
            freemarker.template.Template r6 = r0.template
            r5.setLocation(r6, r4, r13)
            goto L_0x02ab
        L_0x02a6:
            freemarker.template.Template r6 = r0.template
            r5.setLocation(r6, r4, r12)
        L_0x02ab:
            r1.add(r5)
            r5 = 102(0x66, float:1.43E-43)
            r6 = 106(0x6a, float:1.49E-43)
            goto L_0x01c5
        L_0x02b4:
            int r4 = r0.jj_ntk
            if (r4 != r3) goto L_0x02bc
            int r4 = r16.jj_ntk()
        L_0x02bc:
            r3 = 130(0x82, float:1.82E-43)
            if (r4 == r3) goto L_0x02cb
            int[] r3 = r0.jj_la1
            r4 = 46
            int r5 = r0.jj_gen
            r3[r4] = r5
            r4 = 1
            r13 = 0
            goto L_0x02d6
        L_0x02cb:
            freemarker.core.Token r3 = r0.jj_consume_token(r3)
            freemarker.core.Expression r13 = r16.Expression()
            r4 = 1
            if (r2 != r4) goto L_0x0311
        L_0x02d6:
            freemarker.core.Token r3 = r16.LooseDirectiveEnd()
            int r5 = r1.size()
            if (r5 != r4) goto L_0x02f0
            r2 = 0
            java.lang.Object r1 = r1.get(r2)
            freemarker.core.Assignment r1 = (freemarker.core.Assignment) r1
            r1.setNamespaceExp(r13)
            freemarker.template.Template r2 = r0.template
            r1.setLocation(r2, r7, r3)
            return r1
        L_0x02f0:
            freemarker.core.AssignmentInstruction r4 = new freemarker.core.AssignmentInstruction
            r4.<init>(r2)
            r2 = 0
        L_0x02f6:
            int r5 = r1.size()
            if (r2 >= r5) goto L_0x0308
            java.lang.Object r5 = r1.get(r2)
            freemarker.core.Assignment r5 = (freemarker.core.Assignment) r5
            r4.addAssignment(r5)
            int r2 = r2 + 1
            goto L_0x02f6
        L_0x0308:
            r4.setNamespaceExp(r13)
            freemarker.template.Template r1 = r0.template
            r4.setLocation(r1, r7, r3)
            return r4
        L_0x0311:
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            freemarker.template.Template r2 = r0.template
            r1.<init>(r11, r2, r3)
            goto L_0x031a
        L_0x0319:
            throw r1
        L_0x031a:
            goto L_0x0319
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.FMParser.Assign():freemarker.core.TemplateElement");
    }

    public final Include Include() throws ParseException {
        Token jj_consume_token = jj_consume_token(19);
        Expression Expression = Expression();
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i != 122) {
            this.jj_la1[50] = this.jj_gen;
        } else {
            jj_consume_token(122);
        }
        Object obj = null;
        Expression expression = null;
        Expression expression2 = null;
        Expression expression3 = null;
        while (true) {
            int i2 = this.jj_ntk;
            if (i2 == -1) {
                i2 = jj_ntk();
            }
            if (i2 != 133) {
                this.jj_la1[51] = this.jj_gen;
                Token LooseDirectiveEnd = LooseDirectiveEnd();
                Include include = new Include(this.template, Expression, expression, expression2, expression3);
                include.setLocation(this.template, jj_consume_token, LooseDirectiveEnd);
                return include;
            }
            Token jj_consume_token2 = jj_consume_token(FMParserConstants.f7457ID);
            jj_consume_token(97);
            Expression Expression2 = Expression();
            String str = jj_consume_token2.image;
            if (str.equalsIgnoreCase("parse")) {
                expression2 = Expression2;
            } else if (str.equalsIgnoreCase("encoding")) {
                expression = Expression2;
            } else if (str.equalsIgnoreCase("ignore_missing") || str.equals("ignoreMissing")) {
                this.token_source.checkNamingConvention(jj_consume_token2);
                expression3 = Expression2;
            } else {
                if (str.equals("ignoreMissing")) {
                    obj = "ignore_missing";
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Unsupported named #include parameter: \"");
                stringBuffer.append(str);
                stringBuffer.append("\". Supported parameters are: ");
                stringBuffer.append("\"parse\", \"encoding\", \"ignore_missing\".");
                stringBuffer.append(obj == null ? "" : " Supporting camelCase parameter names is planned for FreeMarker 2.4.0; check if an update is available, and if it indeed supports camel case.");
                throw new ParseException(stringBuffer.toString(), this.template, jj_consume_token2);
            }
        }
    }

    public final LibraryLoad Import() throws ParseException {
        Token jj_consume_token = jj_consume_token(20);
        Expression Expression = Expression();
        jj_consume_token(FMParserConstants.f7456AS);
        Token jj_consume_token2 = jj_consume_token(FMParserConstants.f7457ID);
        Token LooseDirectiveEnd = LooseDirectiveEnd();
        LibraryLoad libraryLoad = new LibraryLoad(this.template, Expression, jj_consume_token2.image);
        libraryLoad.setLocation(this.template, jj_consume_token, LooseDirectiveEnd);
        this.template.addImport(libraryLoad);
        return libraryLoad;
    }

    public final Macro Macro() throws ParseException {
        Token token2;
        boolean z;
        String str;
        int i;
        Token token3;
        Expression expression;
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        ArrayList arrayList2 = new ArrayList();
        int i2 = this.jj_ntk;
        int i3 = -1;
        if (i2 == -1) {
            i2 = jj_ntk();
        }
        boolean z2 = false;
        if (i2 == 21) {
            token2 = jj_consume_token(21);
            z = true;
        } else if (i2 == 22) {
            token2 = jj_consume_token(22);
            z = false;
        } else {
            this.jj_la1[52] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        if (this.inMacro || this.inFunction) {
            throw new ParseException("Macros cannot be nested.", this.template, token2);
        }
        if (z) {
            this.inFunction = true;
        } else {
            this.inMacro = true;
        }
        Expression IdentifierOrStringLiteral = IdentifierOrStringLiteral();
        if (IdentifierOrStringLiteral instanceof StringLiteral) {
            str = ((StringLiteral) IdentifierOrStringLiteral).getAsString();
        } else {
            str = ((Identifier) IdentifierOrStringLiteral).getName();
        }
        int i4 = this.jj_ntk;
        if (i4 == -1) {
            i4 = jj_ntk();
        }
        if (i4 != 126) {
            this.jj_la1[53] = this.jj_gen;
        } else {
            jj_consume_token(FMParserConstants.OPEN_PAREN);
        }
        String str2 = null;
        boolean z3 = false;
        boolean z4 = false;
        while (true) {
            int i5 = this.jj_ntk;
            if (i5 == i3) {
                i5 = jj_ntk();
            }
            if (i5 != 133) {
                this.jj_la1[54] = this.jj_gen;
                int i6 = this.jj_ntk;
                if (i6 == i3) {
                    i6 = jj_ntk();
                }
                if (i6 != 127) {
                    this.jj_la1[58] = this.jj_gen;
                } else {
                    jj_consume_token(FMParserConstants.CLOSE_PAREN);
                }
                jj_consume_token(FMParserConstants.DIRECTIVE_END);
                List list = this.iteratorBlockContexts;
                this.iteratorBlockContexts = null;
                if (this.incompatibleImprovements >= _TemplateAPI.VERSION_INT_2_3_23) {
                    i = this.breakableDirectiveNesting;
                    this.breakableDirectiveNesting = z2 ? 1 : 0;
                } else {
                    i = 0;
                }
                TemplateElement OptionalBlock = OptionalBlock();
                int i7 = this.jj_ntk;
                if (i7 == i3) {
                    i7 = jj_ntk();
                }
                if (i7 == 43) {
                    token3 = jj_consume_token(43);
                    if (!z) {
                        throw new ParseException("Expected macro end tag here.", this.template, token2);
                    }
                } else if (i7 == 44) {
                    token3 = jj_consume_token(44);
                    if (z) {
                        throw new ParseException("Expected function end tag here.", this.template, token2);
                    }
                } else {
                    this.jj_la1[59] = this.jj_gen;
                    jj_consume_token(i3);
                    throw new ParseException();
                }
                Token token4 = token3;
                this.iteratorBlockContexts = list;
                if (this.incompatibleImprovements >= _TemplateAPI.VERSION_INT_2_3_23) {
                    this.breakableDirectiveNesting = i;
                }
                this.inFunction = z2;
                this.inMacro = z2;
                Macro macro = new Macro(str, arrayList, hashMap, str2, z, OptionalBlock);
                macro.setLocation(this.template, token2, token4);
                this.template.addMacro(macro);
                return macro;
            }
            Token jj_consume_token = jj_consume_token(FMParserConstants.f7457ID);
            int i8 = this.jj_ntk;
            if (i8 == i3) {
                i8 = jj_ntk();
            }
            if (i8 != 115) {
                this.jj_la1[55] = this.jj_gen;
            } else {
                jj_consume_token(115);
                z3 = true;
            }
            int i9 = this.jj_ntk;
            if (i9 == i3) {
                i9 = jj_ntk();
            }
            if (i9 != 97) {
                this.jj_la1[56] = this.jj_gen;
                expression = null;
            } else {
                jj_consume_token(97);
                expression = Expression();
                arrayList2.add(jj_consume_token.image);
                z4 = true;
            }
            int i10 = this.jj_ntk;
            if (i10 == i3) {
                i10 = jj_ntk();
            }
            if (i10 != 121) {
                this.jj_la1[57] = this.jj_gen;
            } else {
                jj_consume_token(121);
            }
            if (str2 == null) {
                if (!z3) {
                    arrayList.add(jj_consume_token.image);
                    if (!z4 || expression != null) {
                        hashMap.put(jj_consume_token.image, expression);
                    } else {
                        throw new ParseException("In a macro declaration, parameters without a default value must all occur before the parameters with default values.", this.template, jj_consume_token);
                    }
                } else if (expression == null) {
                    str2 = jj_consume_token.image;
                } else {
                    throw new ParseException("\"Catch-all\" macro parameter may not have a default value.", this.template, jj_consume_token);
                }
                i3 = -1;
                z2 = false;
            } else {
                throw new ParseException("There may only be one \"catch-all\" parameter in a macro declaration, and it must be the last parameter.", this.template, jj_consume_token);
            }
        }
    }

    public final CompressedBlock Compress() throws ParseException {
        Token jj_consume_token = jj_consume_token(29);
        TemplateElement OptionalBlock = OptionalBlock();
        Token jj_consume_token2 = jj_consume_token(45);
        CompressedBlock compressedBlock = new CompressedBlock(OptionalBlock);
        compressedBlock.setLocation(this.template, jj_consume_token, jj_consume_token2);
        return compressedBlock;
    }

    public final TemplateElement UnifiedMacroTransform() throws ParseException {
        ArrayList arrayList;
        HashMap hashMap;
        ArrayList arrayList2;
        Token token2;
        UnifiedCall unifiedCall;
        int i;
        List list;
        Token jj_consume_token = jj_consume_token(67);
        Expression Expression = Expression();
        TemplateElement templateElement = null;
        Expression expression = ((Expression instanceof Identifier) || ((Expression instanceof Dot) && ((Dot) Expression).onlyHasIdentifiers())) ? Expression : null;
        int i2 = this.jj_ntk;
        if (i2 == -1) {
            i2 = jj_ntk();
        }
        if (i2 != 143) {
            this.jj_la1[60] = this.jj_gen;
        } else {
            jj_consume_token(FMParserConstants.TERMINATING_WHITESPACE);
        }
        if (jj_2_11(Integer.MAX_VALUE)) {
            hashMap = NamedArgs();
            arrayList = null;
        } else {
            arrayList = PositionalArgs();
            hashMap = null;
        }
        int i3 = this.jj_ntk;
        if (i3 == -1) {
            i3 = jj_ntk();
        }
        if (i3 != 122) {
            this.jj_la1[66] = this.jj_gen;
            arrayList2 = null;
        } else {
            jj_consume_token(122);
            arrayList2 = new ArrayList(4);
            int i4 = this.jj_ntk;
            if (i4 == -1) {
                i4 = jj_ntk();
            }
            if (i4 == 133 || i4 == 143) {
                int i5 = this.jj_ntk;
                if (i5 == -1) {
                    i5 = jj_ntk();
                }
                if (i5 != 143) {
                    this.jj_la1[61] = this.jj_gen;
                } else {
                    jj_consume_token(FMParserConstants.TERMINATING_WHITESPACE);
                }
                arrayList2.add(jj_consume_token(FMParserConstants.f7457ID).image);
                while (true) {
                    int i6 = this.jj_ntk;
                    if (i6 == -1) {
                        i6 = jj_ntk();
                    }
                    if (i6 != 121 && i6 != 143) {
                        break;
                    }
                    int i7 = this.jj_ntk;
                    if (i7 == -1) {
                        i7 = jj_ntk();
                    }
                    if (i7 != 143) {
                        this.jj_la1[63] = this.jj_gen;
                    } else {
                        jj_consume_token(FMParserConstants.TERMINATING_WHITESPACE);
                    }
                    jj_consume_token(121);
                    int i8 = this.jj_ntk;
                    if (i8 == -1) {
                        i8 = jj_ntk();
                    }
                    if (i8 != 143) {
                        this.jj_la1[64] = this.jj_gen;
                    } else {
                        jj_consume_token(FMParserConstants.TERMINATING_WHITESPACE);
                    }
                    arrayList2.add(jj_consume_token(FMParserConstants.f7457ID).image);
                }
                this.jj_la1[62] = this.jj_gen;
            } else {
                this.jj_la1[65] = this.jj_gen;
            }
        }
        int i9 = this.jj_ntk;
        if (i9 == -1) {
            i9 = jj_ntk();
        }
        if (i9 == 139) {
            jj_consume_token(FMParserConstants.DIRECTIVE_END);
            if (arrayList2 == null || (list = this.iteratorBlockContexts) == null || list.isEmpty()) {
                i = 0;
            } else {
                int size = this.iteratorBlockContexts.size();
                int size2 = arrayList2.size();
                i = 0;
                for (int i10 = 0; i10 < size2; i10++) {
                    String str = (String) arrayList2.get(i10);
                    int i11 = size - 1;
                    while (true) {
                        if (i11 < 0) {
                            break;
                        }
                        ParserIteratorBlockContext parserIteratorBlockContext = (ParserIteratorBlockContext) this.iteratorBlockContexts.get(i11);
                        if (parserIteratorBlockContext.loopVarName == null || !parserIteratorBlockContext.loopVarName.equals(str)) {
                            i11--;
                        } else if (parserIteratorBlockContext.kind != 3) {
                            ParserIteratorBlockContext pushIteratorBlockContext = pushIteratorBlockContext();
                            String unused = pushIteratorBlockContext.loopVarName = str;
                            int unused2 = pushIteratorBlockContext.kind = 3;
                            i++;
                        }
                    }
                }
            }
            templateElement = OptionalBlock();
            Token jj_consume_token2 = jj_consume_token(68);
            for (int i12 = 0; i12 < i; i12++) {
                popIteratorBlockContext();
            }
            String trim = jj_consume_token2.image.substring(3, jj_consume_token2.image.length() - 1).trim();
            if (trim.length() > 0) {
                if (expression != null) {
                    String canonicalForm = expression.getCanonicalForm();
                    if (!trim.equals(canonicalForm)) {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("Expecting </@> or </@");
                        stringBuffer.append(canonicalForm);
                        stringBuffer.append(">");
                        throw new ParseException(stringBuffer.toString(), this.template, jj_consume_token2);
                    }
                } else {
                    throw new ParseException("Expecting </@>", this.template, jj_consume_token2);
                }
            }
            token2 = jj_consume_token2;
        } else if (i9 == 140) {
            token2 = jj_consume_token(FMParserConstants.EMPTY_DIRECTIVE_END);
        } else {
            this.jj_la1[67] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        if (arrayList == null) {
            unifiedCall = new UnifiedCall(Expression, hashMap, templateElement, arrayList2);
        }
        unifiedCall.setLocation(this.template, jj_consume_token, token2);
        return unifiedCall;
    }

    public final TemplateElement Call() throws ParseException {
        HashMap hashMap;
        ArrayList arrayList;
        UnifiedCall unifiedCall;
        Token jj_consume_token = jj_consume_token(27);
        String str = jj_consume_token(FMParserConstants.f7457ID).image;
        if (jj_2_13(Integer.MAX_VALUE)) {
            hashMap = NamedArgs();
            arrayList = null;
        } else {
            if (jj_2_12(Integer.MAX_VALUE)) {
                jj_consume_token(FMParserConstants.OPEN_PAREN);
            }
            arrayList = PositionalArgs();
            int i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
            }
            if (i != 127) {
                this.jj_la1[68] = this.jj_gen;
            } else {
                jj_consume_token(FMParserConstants.CLOSE_PAREN);
            }
            hashMap = null;
        }
        Token LooseDirectiveEnd = LooseDirectiveEnd();
        if (arrayList != null) {
            unifiedCall = new UnifiedCall(new Identifier(str), arrayList, (TemplateElement) null, (List) null);
        } else {
            unifiedCall = new UnifiedCall(new Identifier(str), hashMap, (TemplateElement) null, (List) null);
        }
        unifiedCall.legacySyntax = true;
        unifiedCall.setLocation(this.template, jj_consume_token, LooseDirectiveEnd);
        return unifiedCall;
    }

    public final HashMap NamedArgs() throws ParseException {
        int i;
        HashMap hashMap = new HashMap();
        do {
            Token jj_consume_token = jj_consume_token(FMParserConstants.f7457ID);
            jj_consume_token(97);
            this.token_source.SwitchTo(4);
            this.token_source.inInvocation = true;
            hashMap.put(jj_consume_token.image, Expression());
            i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
                continue;
            }
        } while (i == 133);
        this.jj_la1[69] = this.jj_gen;
        this.token_source.inInvocation = false;
        return hashMap;
    }

    public final ArrayList PositionalArgs() throws ParseException {
        ArrayList arrayList = new ArrayList();
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (!(i == 111 || i == 112 || i == 120 || i == 124 || i == 126 || i == 128 || i == 133)) {
            switch (i) {
                case 85:
                case 86:
                case 87:
                case 88:
                case 89:
                case 90:
                case 91:
                    break;
                default:
                    this.jj_la1[72] = this.jj_gen;
                    break;
            }
            return arrayList;
        }
        arrayList.add(Expression());
        while (true) {
            int i2 = this.jj_ntk;
            if (i2 == -1) {
                i2 = jj_ntk();
            }
            if (!(i2 == 111 || i2 == 112 || i2 == 120 || i2 == 121 || i2 == 124 || i2 == 126 || i2 == 128 || i2 == 133)) {
                switch (i2) {
                    case 85:
                    case 86:
                    case 87:
                    case 88:
                    case 89:
                    case 90:
                    case 91:
                        break;
                    default:
                        this.jj_la1[70] = this.jj_gen;
                        return arrayList;
                }
            }
            int i3 = this.jj_ntk;
            if (i3 == -1) {
                i3 = jj_ntk();
            }
            if (i3 != 121) {
                this.jj_la1[71] = this.jj_gen;
            } else {
                jj_consume_token(121);
            }
            arrayList.add(Expression());
        }
    }

    public final Comment Comment() throws ParseException {
        Token token2;
        StringBuffer stringBuffer = new StringBuffer();
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 30) {
            token2 = jj_consume_token(30);
        } else if (i == 31) {
            token2 = jj_consume_token(31);
        } else {
            this.jj_la1[73] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        Token UnparsedContent = UnparsedContent(token2, stringBuffer);
        Comment comment = new Comment(stringBuffer.toString());
        comment.setLocation(this.template, token2, UnparsedContent);
        return comment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.core.TextBlock.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      freemarker.core.TextBlock.<init>(char[], boolean):void
      freemarker.core.TextBlock.<init>(java.lang.String, boolean):void */
    public final TextBlock NoParse() throws ParseException {
        StringBuffer stringBuffer = new StringBuffer();
        Token jj_consume_token = jj_consume_token(32);
        Token UnparsedContent = UnparsedContent(jj_consume_token, stringBuffer);
        TextBlock textBlock = new TextBlock(stringBuffer.toString(), true);
        textBlock.setLocation(this.template, jj_consume_token, UnparsedContent);
        return textBlock;
    }

    public final TransformBlock Transform() throws ParseException {
        Token token2;
        Token jj_consume_token = jj_consume_token(23);
        Expression Expression = Expression();
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i != 122) {
            this.jj_la1[74] = this.jj_gen;
        } else {
            jj_consume_token(122);
        }
        TemplateElement templateElement = null;
        HashMap hashMap = null;
        while (true) {
            int i2 = this.jj_ntk;
            if (i2 == -1) {
                i2 = jj_ntk();
            }
            if (i2 != 133) {
                break;
            }
            Token jj_consume_token2 = jj_consume_token(FMParserConstants.f7457ID);
            jj_consume_token(97);
            Expression Expression2 = Expression();
            if (hashMap == null) {
                hashMap = new HashMap();
            }
            hashMap.put(jj_consume_token2.image, Expression2);
        }
        this.jj_la1[75] = this.jj_gen;
        int i3 = this.jj_ntk;
        if (i3 == -1) {
            i3 = jj_ntk();
        }
        if (i3 == 139) {
            jj_consume_token(FMParserConstants.DIRECTIVE_END);
            templateElement = OptionalBlock();
            token2 = jj_consume_token(46);
        } else if (i3 == 140) {
            token2 = jj_consume_token(FMParserConstants.EMPTY_DIRECTIVE_END);
        } else {
            this.jj_la1[76] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        TransformBlock transformBlock = new TransformBlock(Expression, hashMap, templateElement);
        transformBlock.setLocation(this.template, jj_consume_token, token2);
        return transformBlock;
    }

    public final SwitchBlock Switch() throws ParseException {
        Token jj_consume_token = jj_consume_token(14);
        Expression Expression = Expression();
        jj_consume_token(FMParserConstants.DIRECTIVE_END);
        this.breakableDirectiveNesting++;
        SwitchBlock switchBlock = new SwitchBlock(Expression);
        boolean z = false;
        while (jj_2_14(2)) {
            Case Case = Case();
            if (Case.condition == null) {
                if (!z) {
                    z = true;
                } else {
                    throw new ParseException("You can only have one default case in a switch statement", this.template, jj_consume_token);
                }
            }
            switchBlock.addCase(Case);
        }
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i != 72) {
            this.jj_la1[77] = this.jj_gen;
        } else {
            jj_consume_token(72);
        }
        Token jj_consume_token2 = jj_consume_token(47);
        this.breakableDirectiveNesting--;
        switchBlock.setLocation(this.template, jj_consume_token, jj_consume_token2);
        return switchBlock;
    }

    public final Case Case() throws ParseException {
        Expression expression;
        Token token2;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i != 72) {
            this.jj_la1[78] = this.jj_gen;
        } else {
            jj_consume_token(72);
        }
        int i2 = this.jj_ntk;
        if (i2 == -1) {
            i2 = jj_ntk();
        }
        if (i2 == 15) {
            token2 = jj_consume_token(15);
            expression = Expression();
            jj_consume_token(FMParserConstants.DIRECTIVE_END);
        } else if (i2 == 57) {
            token2 = jj_consume_token(57);
            expression = null;
        } else {
            this.jj_la1[79] = this.jj_gen;
            jj_consume_token(-1);
            throw new ParseException();
        }
        TemplateElement OptionalBlock = OptionalBlock();
        Case caseR = new Case(expression, OptionalBlock);
        caseR.setLocation(this.template, token2, OptionalBlock);
        return caseR;
    }

    public final EscapeBlock Escape() throws ParseException {
        Token jj_consume_token = jj_consume_token(63);
        Token jj_consume_token2 = jj_consume_token(FMParserConstants.f7457ID);
        jj_consume_token(FMParserConstants.f7456AS);
        Expression Expression = Expression();
        jj_consume_token(FMParserConstants.DIRECTIVE_END);
        EscapeBlock escapeBlock = new EscapeBlock(jj_consume_token2.image, Expression, escapedExpression(Expression));
        this.escapes.addFirst(escapeBlock);
        escapeBlock.setContent(OptionalBlock());
        this.escapes.removeFirst();
        escapeBlock.setLocation(this.template, jj_consume_token, jj_consume_token(64));
        return escapeBlock;
    }

    public final NoEscapeBlock NoEscape() throws ParseException {
        Token jj_consume_token = jj_consume_token(65);
        if (!this.escapes.isEmpty()) {
            Object removeFirst = this.escapes.removeFirst();
            TemplateElement OptionalBlock = OptionalBlock();
            Token jj_consume_token2 = jj_consume_token(66);
            this.escapes.addFirst(removeFirst);
            NoEscapeBlock noEscapeBlock = new NoEscapeBlock(OptionalBlock);
            noEscapeBlock.setLocation(this.template, jj_consume_token, jj_consume_token2);
            return noEscapeBlock;
        }
        throw new ParseException("#noescape with no matching #escape encountered.", this.template, jj_consume_token);
    }

    public final Token LooseDirectiveEnd() throws ParseException {
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (i == 139) {
            return jj_consume_token(FMParserConstants.DIRECTIVE_END);
        }
        if (i == 140) {
            return jj_consume_token(FMParserConstants.EMPTY_DIRECTIVE_END);
        }
        this.jj_la1[80] = this.jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
    }

    public final PropertySetting Setting() throws ParseException {
        Token jj_consume_token = jj_consume_token(28);
        Token jj_consume_token2 = jj_consume_token(FMParserConstants.f7457ID);
        jj_consume_token(97);
        Expression Expression = Expression();
        Token LooseDirectiveEnd = LooseDirectiveEnd();
        this.token_source.checkNamingConvention(jj_consume_token2);
        PropertySetting propertySetting = new PropertySetting(jj_consume_token2, this.token_source, Expression, this.template.getConfiguration());
        propertySetting.setLocation(this.template, jj_consume_token, LooseDirectiveEnd);
        return propertySetting;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final freemarker.core.TemplateElement FreemarkerDirective() throws freemarker.core.ParseException {
        /*
            r4 = this;
            int r0 = r4.jj_ntk
            r1 = -1
            if (r0 != r1) goto L_0x0009
            int r0 = r4.jj_ntk()
        L_0x0009:
            r2 = 6
            if (r0 == r2) goto L_0x00cb
            r2 = 8
            if (r0 == r2) goto L_0x00c6
            r2 = 65
            if (r0 == r2) goto L_0x00c1
            r2 = 67
            if (r0 == r2) goto L_0x00bc
            switch(r0) {
                case 10: goto L_0x00b7;
                case 11: goto L_0x00b2;
                case 12: goto L_0x00ad;
                case 13: goto L_0x00a8;
                case 14: goto L_0x00a3;
                default: goto L_0x001b;
            }
        L_0x001b:
            switch(r0) {
                case 16: goto L_0x009e;
                case 17: goto L_0x009e;
                case 18: goto L_0x009e;
                case 19: goto L_0x0099;
                case 20: goto L_0x0094;
                case 21: goto L_0x008f;
                case 22: goto L_0x008f;
                case 23: goto L_0x008a;
                case 24: goto L_0x0085;
                case 25: goto L_0x0080;
                case 26: goto L_0x007b;
                case 27: goto L_0x0076;
                case 28: goto L_0x0071;
                case 29: goto L_0x006b;
                case 30: goto L_0x0065;
                case 31: goto L_0x0065;
                case 32: goto L_0x005f;
                default: goto L_0x001e;
            }
        L_0x001e:
            switch(r0) {
                case 49: goto L_0x0059;
                case 50: goto L_0x007b;
                case 51: goto L_0x0080;
                case 52: goto L_0x0053;
                case 53: goto L_0x004d;
                case 54: goto L_0x004d;
                case 55: goto L_0x004d;
                case 56: goto L_0x004d;
                default: goto L_0x0021;
            }
        L_0x0021:
            switch(r0) {
                case 58: goto L_0x0047;
                case 59: goto L_0x0047;
                case 60: goto L_0x0041;
                case 61: goto L_0x0041;
                case 62: goto L_0x003b;
                case 63: goto L_0x0035;
                default: goto L_0x0024;
            }
        L_0x0024:
            int[] r0 = r4.jj_la1
            r2 = 81
            int r3 = r4.jj_gen
            r0[r2] = r3
            r4.jj_consume_token(r1)
            freemarker.core.ParseException r0 = new freemarker.core.ParseException
            r0.<init>()
            throw r0
        L_0x0035:
            freemarker.core.EscapeBlock r0 = r4.Escape()
            goto L_0x00cf
        L_0x003b:
            freemarker.core.FallbackInstruction r0 = r4.FallBack()
            goto L_0x00cf
        L_0x0041:
            freemarker.core.RecurseNode r0 = r4.Recurse()
            goto L_0x00cf
        L_0x0047:
            freemarker.core.TemplateElement r0 = r4.Nested()
            goto L_0x00cf
        L_0x004d:
            freemarker.core.TemplateElement r0 = r4.Trim()
            goto L_0x00cf
        L_0x0053:
            freemarker.core.TemplateElement r0 = r4.Flush()
            goto L_0x00cf
        L_0x0059:
            freemarker.core.BreakInstruction r0 = r4.Break()
            goto L_0x00cf
        L_0x005f:
            freemarker.core.TextBlock r0 = r4.NoParse()
            goto L_0x00cf
        L_0x0065:
            freemarker.core.Comment r0 = r4.Comment()
            goto L_0x00cf
        L_0x006b:
            freemarker.core.CompressedBlock r0 = r4.Compress()
            goto L_0x00cf
        L_0x0071:
            freemarker.core.PropertySetting r0 = r4.Setting()
            goto L_0x00cf
        L_0x0076:
            freemarker.core.TemplateElement r0 = r4.Call()
            goto L_0x00cf
        L_0x007b:
            freemarker.core.ReturnInstruction r0 = r4.Return()
            goto L_0x00cf
        L_0x0080:
            freemarker.core.StopInstruction r0 = r4.Stop()
            goto L_0x00cf
        L_0x0085:
            freemarker.core.VisitNode r0 = r4.Visit()
            goto L_0x00cf
        L_0x008a:
            freemarker.core.TransformBlock r0 = r4.Transform()
            goto L_0x00cf
        L_0x008f:
            freemarker.core.Macro r0 = r4.Macro()
            goto L_0x00cf
        L_0x0094:
            freemarker.core.LibraryLoad r0 = r4.Import()
            goto L_0x00cf
        L_0x0099:
            freemarker.core.Include r0 = r4.Include()
            goto L_0x00cf
        L_0x009e:
            freemarker.core.TemplateElement r0 = r4.Assign()
            goto L_0x00cf
        L_0x00a3:
            freemarker.core.SwitchBlock r0 = r4.Switch()
            goto L_0x00cf
        L_0x00a8:
            freemarker.core.IteratorBlock r0 = r4.ForEach()
            goto L_0x00cf
        L_0x00ad:
            freemarker.core.Sep r0 = r4.Sep()
            goto L_0x00cf
        L_0x00b2:
            freemarker.core.Items r0 = r4.Items()
            goto L_0x00cf
        L_0x00b7:
            freemarker.core.TemplateElement r0 = r4.List()
            goto L_0x00cf
        L_0x00bc:
            freemarker.core.TemplateElement r0 = r4.UnifiedMacroTransform()
            goto L_0x00cf
        L_0x00c1:
            freemarker.core.NoEscapeBlock r0 = r4.NoEscape()
            goto L_0x00cf
        L_0x00c6:
            freemarker.core.TemplateElement r0 = r4.mo36036If()
            goto L_0x00cf
        L_0x00cb:
            freemarker.core.AttemptBlock r0 = r4.Attempt()
        L_0x00cf:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.FMParser.FreemarkerDirective():freemarker.core.TemplateElement");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.core.TextBlock.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      freemarker.core.TextBlock.<init>(char[], boolean):void
      freemarker.core.TextBlock.<init>(java.lang.String, boolean):void */
    public final TextBlock PCData() throws ParseException {
        StringBuffer stringBuffer = new StringBuffer();
        Token token2 = null;
        Token token3 = null;
        Token token4 = null;
        do {
            int i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
            }
            switch (i) {
                case 72:
                    token4 = token2;
                    token2 = jj_consume_token(72);
                    break;
                case 73:
                    token2 = jj_consume_token(73);
                    break;
                case 74:
                    token2 = jj_consume_token(74);
                    break;
                default:
                    this.jj_la1[82] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
            stringBuffer.append(token2.image);
            if (token3 == null) {
                token3 = token2;
            }
            if (token4 != null) {
                token4.next = null;
            }
        } while (jj_2_15(Integer.MAX_VALUE));
        if (this.stripText && this.mixedContentNesting == 1) {
            return TextBlock.EMPTY_BLOCK;
        }
        TextBlock textBlock = new TextBlock(stringBuffer.toString(), false);
        textBlock.setLocation(this.template, token3, token2);
        return textBlock;
    }

    public final Token UnparsedContent(Token token2, StringBuffer stringBuffer) throws ParseException {
        Token token3;
        while (true) {
            int i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
            }
            switch (i) {
                case FMParserConstants.TERSE_COMMENT_END /*145*/:
                    token3 = jj_consume_token(FMParserConstants.TERSE_COMMENT_END);
                    break;
                case FMParserConstants.MAYBE_END /*146*/:
                    token3 = jj_consume_token(FMParserConstants.MAYBE_END);
                    break;
                case FMParserConstants.KEEP_GOING /*147*/:
                    token3 = jj_consume_token(FMParserConstants.KEEP_GOING);
                    break;
                case FMParserConstants.LONE_LESS_THAN_OR_DASH /*148*/:
                    token3 = jj_consume_token(FMParserConstants.LONE_LESS_THAN_OR_DASH);
                    break;
                default:
                    this.jj_la1[83] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
            stringBuffer.append(token3.image);
            int i2 = this.jj_ntk;
            if (i2 == -1) {
                i2 = jj_ntk();
                continue;
            }
            switch (i2) {
                case FMParserConstants.TERSE_COMMENT_END /*145*/:
                case FMParserConstants.MAYBE_END /*146*/:
                case FMParserConstants.KEEP_GOING /*147*/:
                case FMParserConstants.LONE_LESS_THAN_OR_DASH /*148*/:
                    break;
                default:
                    this.jj_la1[84] = this.jj_gen;
                    stringBuffer.setLength(stringBuffer.length() - token3.image.length());
                    if (token3.image.endsWith(";") || _TemplateAPI.getTemplateLanguageVersionAsInt(this.template) < _TemplateAPI.VERSION_INT_2_3_21) {
                        return token3;
                    }
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("Unclosed \"");
                    stringBuffer2.append(token2.image);
                    stringBuffer2.append("\"");
                    throw new ParseException(stringBuffer2.toString(), this.template, token2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0053 A[FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x000c A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final freemarker.core.MixedContent MixedContent() throws freemarker.core.ParseException {
        /*
            r9 = this;
            freemarker.core.MixedContent r0 = new freemarker.core.MixedContent
            r0.<init>()
            int r1 = r9.mixedContentNesting
            int r1 = r1 + 1
            r9.mixedContentNesting = r1
            r1 = 0
        L_0x000c:
            int r2 = r9.jj_ntk
            r3 = -1
            if (r2 != r3) goto L_0x0015
            int r2 = r9.jj_ntk()
        L_0x0015:
            r4 = 67
            r5 = 65
            r6 = 8
            r7 = 6
            if (r2 == r7) goto L_0x0053
            if (r2 == r6) goto L_0x0053
            if (r2 == r5) goto L_0x0053
            if (r2 == r4) goto L_0x0053
            switch(r2) {
                case 10: goto L_0x0053;
                case 11: goto L_0x0053;
                case 12: goto L_0x0053;
                case 13: goto L_0x0053;
                case 14: goto L_0x0053;
                default: goto L_0x0027;
            }
        L_0x0027:
            switch(r2) {
                case 16: goto L_0x0053;
                case 17: goto L_0x0053;
                case 18: goto L_0x0053;
                case 19: goto L_0x0053;
                case 20: goto L_0x0053;
                case 21: goto L_0x0053;
                case 22: goto L_0x0053;
                case 23: goto L_0x0053;
                case 24: goto L_0x0053;
                case 25: goto L_0x0053;
                case 26: goto L_0x0053;
                case 27: goto L_0x0053;
                case 28: goto L_0x0053;
                case 29: goto L_0x0053;
                case 30: goto L_0x0053;
                case 31: goto L_0x0053;
                case 32: goto L_0x0053;
                default: goto L_0x002a;
            }
        L_0x002a:
            switch(r2) {
                case 49: goto L_0x0053;
                case 50: goto L_0x0053;
                case 51: goto L_0x0053;
                case 52: goto L_0x0053;
                case 53: goto L_0x0053;
                case 54: goto L_0x0053;
                case 55: goto L_0x0053;
                case 56: goto L_0x0053;
                default: goto L_0x002d;
            }
        L_0x002d:
            switch(r2) {
                case 58: goto L_0x0053;
                case 59: goto L_0x0053;
                case 60: goto L_0x0053;
                case 61: goto L_0x0053;
                case 62: goto L_0x0053;
                case 63: goto L_0x0053;
                default: goto L_0x0030;
            }
        L_0x0030:
            switch(r2) {
                case 72: goto L_0x004e;
                case 73: goto L_0x004e;
                case 74: goto L_0x004e;
                case 75: goto L_0x0049;
                case 76: goto L_0x0044;
                default: goto L_0x0033;
            }
        L_0x0033:
            int[] r0 = r9.jj_la1
            r1 = 85
            int r2 = r9.jj_gen
            r0[r1] = r2
            r9.jj_consume_token(r3)
            freemarker.core.ParseException r0 = new freemarker.core.ParseException
            r0.<init>()
            throw r0
        L_0x0044:
            freemarker.core.NumericalOutput r2 = r9.NumericalOutput()
            goto L_0x0057
        L_0x0049:
            freemarker.core.DollarVariable r2 = r9.StringOutput()
            goto L_0x0057
        L_0x004e:
            freemarker.core.TextBlock r2 = r9.PCData()
            goto L_0x0057
        L_0x0053:
            freemarker.core.TemplateElement r2 = r9.FreemarkerDirective()
        L_0x0057:
            if (r1 != 0) goto L_0x005a
            r1 = r2
        L_0x005a:
            r0.addElement(r2)
            int r8 = r9.jj_ntk
            if (r8 != r3) goto L_0x0065
            int r8 = r9.jj_ntk()
        L_0x0065:
            if (r8 == r7) goto L_0x000c
            if (r8 == r6) goto L_0x000c
            if (r8 == r5) goto L_0x000c
            if (r8 == r4) goto L_0x000c
            switch(r8) {
                case 10: goto L_0x000c;
                case 11: goto L_0x000c;
                case 12: goto L_0x000c;
                case 13: goto L_0x000c;
                case 14: goto L_0x000c;
                default: goto L_0x0070;
            }
        L_0x0070:
            switch(r8) {
                case 16: goto L_0x000c;
                case 17: goto L_0x000c;
                case 18: goto L_0x000c;
                case 19: goto L_0x000c;
                case 20: goto L_0x000c;
                case 21: goto L_0x000c;
                case 22: goto L_0x000c;
                case 23: goto L_0x000c;
                case 24: goto L_0x000c;
                case 25: goto L_0x000c;
                case 26: goto L_0x000c;
                case 27: goto L_0x000c;
                case 28: goto L_0x000c;
                case 29: goto L_0x000c;
                case 30: goto L_0x000c;
                case 31: goto L_0x000c;
                case 32: goto L_0x000c;
                default: goto L_0x0073;
            }
        L_0x0073:
            switch(r8) {
                case 49: goto L_0x000c;
                case 50: goto L_0x000c;
                case 51: goto L_0x000c;
                case 52: goto L_0x000c;
                case 53: goto L_0x000c;
                case 54: goto L_0x000c;
                case 55: goto L_0x000c;
                case 56: goto L_0x000c;
                default: goto L_0x0076;
            }
        L_0x0076:
            switch(r8) {
                case 58: goto L_0x000c;
                case 59: goto L_0x000c;
                case 60: goto L_0x000c;
                case 61: goto L_0x000c;
                case 62: goto L_0x000c;
                case 63: goto L_0x000c;
                default: goto L_0x0079;
            }
        L_0x0079:
            switch(r8) {
                case 72: goto L_0x000c;
                case 73: goto L_0x000c;
                case 74: goto L_0x000c;
                case 75: goto L_0x000c;
                case 76: goto L_0x000c;
                default: goto L_0x007c;
            }
        L_0x007c:
            int[] r3 = r9.jj_la1
            r4 = 86
            int r5 = r9.jj_gen
            r3[r4] = r5
            int r3 = r9.mixedContentNesting
            int r3 = r3 + -1
            r9.mixedContentNesting = r3
            freemarker.template.Template r3 = r9.template
            r0.setLocation(r3, r1, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.FMParser.MixedContent():freemarker.core.MixedContent");
    }

    public final TemplateElement FreeMarkerText() throws ParseException {
        TemplateElement templateElement;
        MixedContent mixedContent = new MixedContent();
        TemplateElement templateElement2 = null;
        while (true) {
            int i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
            }
            switch (i) {
                case 72:
                case 73:
                case 74:
                    templateElement = PCData();
                    break;
                case 75:
                    templateElement = StringOutput();
                    break;
                case 76:
                    templateElement = NumericalOutput();
                    break;
                default:
                    this.jj_la1[87] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
            if (templateElement2 == null) {
                templateElement2 = templateElement;
            }
            mixedContent.addElement(templateElement);
            int i2 = this.jj_ntk;
            if (i2 == -1) {
                i2 = jj_ntk();
                continue;
            }
            switch (i2) {
                case 72:
                case 73:
                case 74:
                case 75:
                case 76:
                    break;
                default:
                    this.jj_la1[88] = this.jj_gen;
                    mixedContent.setLocation(this.template, templateElement2, templateElement);
                    return mixedContent;
            }
        }
    }

    public final TemplateElement OptionalBlock() throws ParseException {
        TextBlock textBlock = TextBlock.EMPTY_BLOCK;
        int i = this.jj_ntk;
        if (i == -1) {
            i = jj_ntk();
        }
        if (!(i == 6 || i == 8 || i == 65 || i == 67)) {
            switch (i) {
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                    break;
                default:
                    switch (i) {
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                        case 28:
                        case 29:
                        case 30:
                        case 31:
                        case 32:
                            break;
                        default:
                            switch (i) {
                                case 49:
                                case 50:
                                case 51:
                                case 52:
                                case 53:
                                case 54:
                                case 55:
                                case 56:
                                    break;
                                default:
                                    switch (i) {
                                        case 58:
                                        case 59:
                                        case 60:
                                        case 61:
                                        case 62:
                                        case 63:
                                            break;
                                        default:
                                            switch (i) {
                                                case 72:
                                                case 73:
                                                case 74:
                                                case 75:
                                                case 76:
                                                    break;
                                                default:
                                                    this.jj_la1[89] = this.jj_gen;
                                                    return textBlock;
                                            }
                                    }
                            }
                    }
            }
        }
        return MixedContent();
    }

    /* JADX WARNING: Removed duplicated region for block: B:125:0x0041 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0083  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void HeaderElement() throws freemarker.core.ParseException {
        /*
            r10 = this;
            int r0 = r10.jj_ntk
            r1 = -1
            if (r0 != r1) goto L_0x0009
            int r0 = r10.jj_ntk()
        L_0x0009:
            r2 = 72
            if (r0 == r2) goto L_0x0016
            int[] r0 = r10.jj_la1
            r2 = 90
            int r3 = r10.jj_gen
            r0[r2] = r3
            goto L_0x0019
        L_0x0016:
            r10.jj_consume_token(r2)
        L_0x0019:
            int r0 = r10.jj_ntk
            if (r0 != r1) goto L_0x0021
            int r0 = r10.jj_ntk()
        L_0x0021:
            r2 = 69
            if (r0 == r2) goto L_0x003e
            r2 = 70
            if (r0 != r2) goto L_0x002d
            r10.jj_consume_token(r2)
            goto L_0x0058
        L_0x002d:
            int[] r0 = r10.jj_la1
            r2 = 92
            int r3 = r10.jj_gen
            r0[r2] = r3
            r10.jj_consume_token(r1)
            freemarker.core.ParseException r0 = new freemarker.core.ParseException
            r0.<init>()
            throw r0
        L_0x003e:
            r10.jj_consume_token(r2)
        L_0x0041:
            int r0 = r10.jj_ntk
            if (r0 != r1) goto L_0x0049
            int r0 = r10.jj_ntk()
        L_0x0049:
            r2 = 133(0x85, float:1.86E-43)
            if (r0 == r2) goto L_0x0059
            int[] r0 = r10.jj_la1
            r1 = 91
            int r2 = r10.jj_gen
            r0[r1] = r2
            r10.LooseDirectiveEnd()
        L_0x0058:
            return
        L_0x0059:
            freemarker.core.Token r0 = r10.jj_consume_token(r2)
            r2 = 97
            r10.jj_consume_token(r2)
            freemarker.core.Expression r2 = r10.Expression()
            freemarker.core.FMParserTokenManager r3 = r10.token_source
            r3.checkNamingConvention(r0)
            java.lang.String r3 = r0.image
            r4 = 0
            freemarker.template.TemplateModel r5 = r2.eval(r4)     // Catch:{ Exception -> 0x01ee }
            boolean r6 = r5 instanceof freemarker.template.TemplateScalarModel
            if (r6 == 0) goto L_0x007e
            r6 = r2
            freemarker.template.TemplateScalarModel r6 = (freemarker.template.TemplateScalarModel) r6     // Catch:{ TemplateModelException -> 0x007e }
            java.lang.String r6 = r6.getAsString()     // Catch:{ TemplateModelException -> 0x007e }
            goto L_0x007f
        L_0x007e:
            r6 = r4
        L_0x007f:
            freemarker.template.Template r7 = r10.template
            if (r7 == 0) goto L_0x0041
            java.lang.String r7 = "encoding"
            boolean r8 = r3.equalsIgnoreCase(r7)
            if (r8 == 0) goto L_0x00aa
            if (r6 == 0) goto L_0x00a2
            freemarker.template.Template r0 = r10.template
            java.lang.String r0 = r0.getEncoding()
            if (r0 == 0) goto L_0x0041
            boolean r2 = r0.equalsIgnoreCase(r6)
            if (r2 == 0) goto L_0x009c
            goto L_0x0041
        L_0x009c:
            freemarker.template.Template$WrongEncodingException r1 = new freemarker.template.Template$WrongEncodingException
            r1.<init>(r6, r0)
            throw r1
        L_0x00a2:
            freemarker.core.ParseException r0 = new freemarker.core.ParseException
            java.lang.String r1 = "Expecting an encoding string."
            r0.<init>(r1, r2)
            throw r0
        L_0x00aa:
            java.lang.String r6 = "STRIP_WHITESPACE"
            boolean r6 = r3.equalsIgnoreCase(r6)
            if (r6 != 0) goto L_0x01e6
            java.lang.String r6 = "stripWhitespace"
            boolean r6 = r3.equals(r6)
            if (r6 == 0) goto L_0x00bc
            goto L_0x01e6
        L_0x00bc:
            java.lang.String r6 = "STRIP_TEXT"
            boolean r6 = r3.equalsIgnoreCase(r6)
            if (r6 != 0) goto L_0x01de
            java.lang.String r6 = "stripText"
            boolean r6 = r3.equals(r6)
            if (r6 == 0) goto L_0x00ce
            goto L_0x01de
        L_0x00ce:
            java.lang.String r6 = "STRICT_SYNTAX"
            boolean r6 = r3.equalsIgnoreCase(r6)
            if (r6 != 0) goto L_0x01d4
            java.lang.String r6 = "strictSyntax"
            boolean r6 = r3.equals(r6)
            if (r6 == 0) goto L_0x00e0
            goto L_0x01d4
        L_0x00e0:
            java.lang.String r6 = "ns_prefixes"
            boolean r8 = r3.equalsIgnoreCase(r6)
            if (r8 != 0) goto L_0x0187
            java.lang.String r8 = "nsPrefixes"
            boolean r9 = r3.equals(r8)
            if (r9 == 0) goto L_0x00f2
            goto L_0x0187
        L_0x00f2:
            java.lang.String r9 = "attributes"
            boolean r9 = r3.equalsIgnoreCase(r9)
            if (r9 == 0) goto L_0x0131
            boolean r0 = r5 instanceof freemarker.template.TemplateHashModelEx
            if (r0 == 0) goto L_0x0129
            freemarker.template.TemplateHashModelEx r5 = (freemarker.template.TemplateHashModelEx) r5
            freemarker.template.TemplateCollectionModel r0 = r5.keys()     // Catch:{ TemplateModelException -> 0x0126 }
            freemarker.template.TemplateModelIterator r0 = r0.iterator()     // Catch:{ TemplateModelException -> 0x0126 }
        L_0x0108:
            boolean r2 = r0.hasNext()     // Catch:{ TemplateModelException -> 0x0126 }
            if (r2 == 0) goto L_0x0041
            freemarker.template.TemplateModel r2 = r0.next()     // Catch:{ TemplateModelException -> 0x0126 }
            freemarker.template.TemplateScalarModel r2 = (freemarker.template.TemplateScalarModel) r2     // Catch:{ TemplateModelException -> 0x0126 }
            java.lang.String r2 = r2.getAsString()     // Catch:{ TemplateModelException -> 0x0126 }
            freemarker.template.TemplateModel r3 = r5.get(r2)     // Catch:{ TemplateModelException -> 0x0126 }
            java.lang.Object r3 = freemarker.template.utility.DeepUnwrap.unwrap(r3)     // Catch:{ TemplateModelException -> 0x0126 }
            freemarker.template.Template r4 = r10.template     // Catch:{ TemplateModelException -> 0x0126 }
            r4.setCustomAttribute(r2, r3)     // Catch:{ TemplateModelException -> 0x0126 }
            goto L_0x0108
        L_0x0126:
            goto L_0x0041
        L_0x0129:
            freemarker.core.ParseException r0 = new freemarker.core.ParseException
            java.lang.String r1 = "Expecting a hash of attribute names to values."
            r0.<init>(r1, r2)
            throw r0
        L_0x0131:
            java.lang.String r1 = "charset"
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x013b
            r4 = r7
            goto L_0x014e
        L_0x013b:
            java.lang.String r1 = "xmlns"
            boolean r1 = r3.equals(r1)
            if (r1 == 0) goto L_0x014e
            freemarker.core.FMParserTokenManager r1 = r10.token_source
            int r1 = r1.namingConvention
            r2 = 12
            if (r1 != r2) goto L_0x014d
            r4 = r8
            goto L_0x014e
        L_0x014d:
            r4 = r6
        L_0x014e:
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r3 = "Unknown FTL header parameter: "
            r2.append(r3)
            java.lang.String r3 = r0.image
            r2.append(r3)
            if (r4 != 0) goto L_0x0164
            java.lang.String r3 = ""
            goto L_0x017a
        L_0x0164:
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r5 = ". Did you mean "
            r3.append(r5)
            r3.append(r4)
            java.lang.String r4 = "?"
            r3.append(r4)
            java.lang.String r3 = r3.toString()
        L_0x017a:
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            freemarker.template.Template r3 = r10.template
            r1.<init>(r2, r3, r0)
            throw r1
        L_0x0187:
            boolean r0 = r5 instanceof freemarker.template.TemplateHashModelEx
            if (r0 == 0) goto L_0x01cc
            freemarker.template.TemplateHashModelEx r5 = (freemarker.template.TemplateHashModelEx) r5
            freemarker.template.TemplateCollectionModel r0 = r5.keys()     // Catch:{ TemplateModelException -> 0x0126 }
            freemarker.template.TemplateModelIterator r0 = r0.iterator()     // Catch:{ TemplateModelException -> 0x0126 }
        L_0x0195:
            boolean r3 = r0.hasNext()     // Catch:{ TemplateModelException -> 0x0126 }
            if (r3 == 0) goto L_0x0041
            freemarker.template.TemplateModel r3 = r0.next()     // Catch:{ TemplateModelException -> 0x0126 }
            freemarker.template.TemplateScalarModel r3 = (freemarker.template.TemplateScalarModel) r3     // Catch:{ TemplateModelException -> 0x0126 }
            java.lang.String r3 = r3.getAsString()     // Catch:{ TemplateModelException -> 0x0126 }
            freemarker.template.TemplateModel r4 = r5.get(r3)     // Catch:{ TemplateModelException -> 0x0126 }
            boolean r6 = r4 instanceof freemarker.template.TemplateScalarModel     // Catch:{ TemplateModelException -> 0x0126 }
            if (r6 == 0) goto L_0x01c4
            freemarker.template.TemplateScalarModel r4 = (freemarker.template.TemplateScalarModel) r4     // Catch:{ TemplateModelException -> 0x0126 }
            java.lang.String r4 = r4.getAsString()     // Catch:{ TemplateModelException -> 0x0126 }
            freemarker.template.Template r6 = r10.template     // Catch:{ IllegalArgumentException -> 0x01b9 }
            r6.addPrefixNSMapping(r3, r4)     // Catch:{ IllegalArgumentException -> 0x01b9 }
            goto L_0x0195
        L_0x01b9:
            r0 = move-exception
            freemarker.core.ParseException r3 = new freemarker.core.ParseException     // Catch:{ TemplateModelException -> 0x0126 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ TemplateModelException -> 0x0126 }
            r3.<init>(r0, r2)     // Catch:{ TemplateModelException -> 0x0126 }
            throw r3     // Catch:{ TemplateModelException -> 0x0126 }
        L_0x01c4:
            freemarker.core.ParseException r0 = new freemarker.core.ParseException     // Catch:{ TemplateModelException -> 0x0126 }
            java.lang.String r3 = "Non-string value in prefix to namespace hash."
            r0.<init>(r3, r2)     // Catch:{ TemplateModelException -> 0x0126 }
            throw r0     // Catch:{ TemplateModelException -> 0x0126 }
        L_0x01cc:
            freemarker.core.ParseException r0 = new freemarker.core.ParseException
            java.lang.String r1 = "Expecting a hash of prefixes to namespace URI's."
            r0.<init>(r1, r2)
            throw r0
        L_0x01d4:
            freemarker.core.FMParserTokenManager r0 = r10.token_source
            boolean r2 = r10.getBoolean(r2)
            r0.strictEscapeSyntax = r2
            goto L_0x0041
        L_0x01de:
            boolean r0 = r10.getBoolean(r2)
            r10.stripText = r0
            goto L_0x0041
        L_0x01e6:
            boolean r0 = r10.getBoolean(r2)
            r10.stripWhitespace = r0
            goto L_0x0041
        L_0x01ee:
            r0 = move-exception
            freemarker.core.ParseException r1 = new freemarker.core.ParseException
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "Could not evaluate expression: "
            r3.append(r4)
            java.lang.String r4 = r2.getCanonicalForm()
            r3.append(r4)
            java.lang.String r4 = " \nUnderlying cause: "
            r3.append(r4)
            java.lang.String r4 = r0.getMessage()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.<init>(r3, r2, r0)
            goto L_0x0217
        L_0x0216:
            throw r1
        L_0x0217:
            goto L_0x0216
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.FMParser.HeaderElement():void");
    }

    public final Map ParamList() throws ParseException {
        int i;
        HashMap hashMap = new HashMap();
        do {
            Identifier Identifier = Identifier();
            jj_consume_token(97);
            hashMap.put(Identifier.toString(), Expression());
            int i2 = this.jj_ntk;
            if (i2 == -1) {
                i2 = jj_ntk();
            }
            if (i2 != 121) {
                this.jj_la1[93] = this.jj_gen;
            } else {
                jj_consume_token(121);
            }
            i = this.jj_ntk;
            if (i == -1) {
                i = jj_ntk();
            }
        } while (i == 133);
        this.jj_la1[94] = this.jj_gen;
        return hashMap;
    }

    public final TemplateElement Root() throws ParseException {
        if (jj_2_16(Integer.MAX_VALUE)) {
            HeaderElement();
        }
        TemplateElement OptionalBlock = OptionalBlock();
        jj_consume_token(0);
        OptionalBlock.setFieldsForRootElement();
        TemplateElement postParseCleanup = OptionalBlock.postParseCleanup(this.stripWhitespace);
        postParseCleanup.setFieldsForRootElement();
        return postParseCleanup;
    }

    private final boolean jj_2_1(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_1();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(0, i);
        }
    }

    private final boolean jj_2_2(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return !jj_3_2();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(1, i);
        }
    }

    private final boolean jj_2_3(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_3();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(2, i);
        }
    }

    private final boolean jj_2_4(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_4();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(3, i);
        }
    }

    private final boolean jj_2_5(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_5();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(4, i);
        }
    }

    private final boolean jj_2_6(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_6();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(5, i);
        }
    }

    private final boolean jj_2_7(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_7();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(6, i);
        }
    }

    private final boolean jj_2_8(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_8();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(7, i);
        }
    }

    private final boolean jj_2_9(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_9();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(8, i);
        }
    }

    private final boolean jj_2_10(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_10();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(9, i);
        }
    }

    private final boolean jj_2_11(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_11();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(10, i);
        }
    }

    private final boolean jj_2_12(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_12();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(11, i);
        }
    }

    private final boolean jj_2_13(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_13();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(12, i);
        }
    }

    private final boolean jj_2_14(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_14();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(13, i);
        }
    }

    private final boolean jj_2_15(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_15();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(14, i);
        }
    }

    private final boolean jj_2_16(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            return true ^ jj_3_16();
        } catch (LookaheadSuccess unused) {
            return true;
        } finally {
            jj_save(15, i);
        }
    }

    private final boolean jj_3R_174() {
        return jj_scan_token(17);
    }

    private final boolean jj_3R_42() {
        return jj_scan_token(113);
    }

    private final boolean jj_3_3() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(113)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(116)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(117);
    }

    private final boolean jj_3R_105() {
        return jj_3R_146();
    }

    private final boolean jj_3R_173() {
        return jj_scan_token(16);
    }

    private final boolean jj_3_11() {
        if (!jj_scan_token(FMParserConstants.f7457ID) && !jj_scan_token(97)) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_104() {
        return jj_3R_145();
    }

    private final boolean jj_3R_103() {
        return jj_3R_144();
    }

    private final boolean jj_3R_102() {
        return jj_3R_143();
    }

    private final boolean jj_3R_143() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_173()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_174()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_175();
    }

    private final boolean jj_3R_101() {
        return jj_3R_142();
    }

    private final boolean jj_3R_33() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_42()) {
            this.jj_scanpos = token2;
            if (jj_3R_43()) {
                this.jj_scanpos = token2;
                if (jj_3R_44()) {
                    return true;
                }
            }
        }
        if (jj_3R_32()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_100() {
        return jj_3R_141();
    }

    private final boolean jj_3R_99() {
        return jj_3R_140();
    }

    private final boolean jj_3R_141() {
        return jj_scan_token(10);
    }

    private final boolean jj_3R_171() {
        if (!jj_scan_token(FMParserConstants.OPEN_PAREN) && !jj_3R_89() && !jj_scan_token(FMParserConstants.CLOSE_PAREN)) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_26() {
        Token token2;
        if (jj_3R_32()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_33());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_84() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_99()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_100()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_101()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_102()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_103()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_104()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_105()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_106()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_107()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_108()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_109()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_110()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_111()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_112()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_113()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_114()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_115()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_116()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_117()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_118()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_119()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_120()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_121()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_122()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_123()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_124()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_125()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_126()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_127();
    }

    private final boolean jj_3R_183() {
        return jj_scan_token(56);
    }

    private final boolean jj_3R_182() {
        return jj_scan_token(55);
    }

    private final boolean jj_3R_148() {
        return jj_scan_token(67);
    }

    private final boolean jj_3R_181() {
        return jj_scan_token(54);
    }

    private final boolean jj_3R_180() {
        return jj_scan_token(53);
    }

    private final boolean jj_3R_161() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_180()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_181()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_182()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_183();
    }

    private final boolean jj_3R_35() {
        return jj_scan_token(112);
    }

    private final boolean jj_3_16() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(72)) {
            this.jj_scanpos = token2;
        }
        Token token3 = this.jj_scanpos;
        if (!jj_scan_token(70)) {
            return false;
        }
        this.jj_scanpos = token3;
        return jj_scan_token(69);
    }

    private final boolean jj_3R_34() {
        return jj_scan_token(111);
    }

    private final boolean jj_3R_156() {
        return jj_scan_token(28);
    }

    private final boolean jj_3_2() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(111)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(112);
    }

    private final boolean jj_3R_133() {
        if (jj_scan_token(95) || jj_scan_token(FMParserConstants.f7457ID)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (!jj_3R_171()) {
            return false;
        }
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_147() {
        return jj_scan_token(29);
    }

    private final boolean jj_3R_160() {
        return jj_scan_token(52);
    }

    private final boolean jj_3R_27() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_34()) {
            this.jj_scanpos = token2;
            if (jj_3R_35()) {
                return true;
            }
        }
        if (jj_3R_26()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_168() {
        return jj_scan_token(6);
    }

    private final boolean jj_3R_23() {
        Token token2;
        if (jj_3R_26()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_27());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3_9() {
        return jj_3R_24();
    }

    private final boolean jj_3R_135() {
        return jj_scan_token(96);
    }

    private final boolean jj_3R_185() {
        return jj_scan_token(59);
    }

    private final boolean jj_3R_54() {
        return jj_scan_token(112);
    }

    private final boolean jj_3R_187() {
        return jj_3R_24();
    }

    private final boolean jj_3R_48() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(111)) {
            this.jj_scanpos = token2;
            if (jj_3R_54()) {
                return true;
            }
        }
        if (jj_3R_50()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_184() {
        return jj_scan_token(58);
    }

    private final boolean jj_3R_164() {
        return jj_scan_token(65);
    }

    private final boolean jj_3R_162() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_184()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_185();
    }

    private final boolean jj_3R_172() {
        if (jj_scan_token(120)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (!jj_3R_187()) {
            return false;
        }
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_134() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(FMParserConstants.TERMINATING_EXCLAM)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_172();
    }

    private final boolean jj_3R_55() {
        return jj_scan_token(120);
    }

    private final boolean jj_3R_179() {
        return jj_scan_token(25);
    }

    private final boolean jj_3R_140() {
        return jj_scan_token(8);
    }

    private final boolean jj_3R_49() {
        Token token2;
        if (jj_3R_55()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_55());
        this.jj_scanpos = token2;
        if (jj_3R_50()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_95() {
        return jj_3R_135();
    }

    private final boolean jj_3R_159() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(51)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_179();
    }

    private final boolean jj_3R_94() {
        return jj_3R_134();
    }

    private final boolean jj_3R_93() {
        return jj_3R_133();
    }

    private final boolean jj_3R_92() {
        return jj_3R_132();
    }

    private final boolean jj_3R_91() {
        return jj_3R_131();
    }

    private final boolean jj_3R_41() {
        return jj_3R_50();
    }

    private final boolean jj_3R_163() {
        return jj_scan_token(63);
    }

    private final boolean jj_3R_90() {
        return jj_3R_130();
    }

    private final boolean jj_3R_40() {
        return jj_3R_49();
    }

    private final boolean jj_3R_39() {
        return jj_3R_48();
    }

    private final boolean jj_3R_79() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_90()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_91()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_92()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_93()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_94()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_95();
    }

    private final boolean jj_3R_32() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_39()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_40()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_41();
    }

    private final boolean jj_3R_30() {
        return jj_scan_token(57);
    }

    private final boolean jj_3R_29() {
        if (!jj_scan_token(15) && !jj_3R_24()) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_178() {
        return jj_scan_token(26);
    }

    private final boolean jj_3R_177() {
        return jj_scan_token(50);
    }

    private final boolean jj_3R_25() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(72)) {
            this.jj_scanpos = token2;
        }
        Token token3 = this.jj_scanpos;
        if (jj_3R_29()) {
            this.jj_scanpos = token3;
            if (jj_3R_30()) {
                return true;
            }
        }
        if (jj_3R_31()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_158() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_177()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_178();
    }

    private final boolean jj_3R_78() {
        if (!jj_scan_token(91) && !jj_scan_token(FMParserConstants.f7457ID)) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_77() {
        if (!jj_scan_token(FMParserConstants.OPEN_PAREN) && !jj_3R_24() && !jj_scan_token(FMParserConstants.CLOSE_PAREN)) {
            return false;
        }
        return true;
    }

    private final boolean jj_3_1() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(91)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(124)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(FMParserConstants.OPEN_PAREN)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(95)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(120)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(FMParserConstants.TERMINATING_EXCLAM)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(96);
    }

    private final boolean jj_3R_176() {
        return jj_scan_token(21);
    }

    private final boolean jj_3R_64() {
        return jj_3R_79();
    }

    private final boolean jj_3R_146() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(22)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_176();
    }

    private final boolean jj_3R_63() {
        return jj_3R_78();
    }

    private final boolean jj_3R_62() {
        return jj_3R_77();
    }

    private final boolean jj_3_14() {
        return jj_3R_25();
    }

    private final boolean jj_3R_157() {
        return jj_scan_token(49);
    }

    private final boolean jj_3R_61() {
        return jj_3R_76();
    }

    private final boolean jj_3R_60() {
        return jj_3R_75();
    }

    private final boolean jj_3R_59() {
        return jj_3R_74();
    }

    private final boolean jj_3R_58() {
        return jj_3R_73();
    }

    private final boolean jj_3R_76() {
        return jj_scan_token(FMParserConstants.f7457ID);
    }

    private final boolean jj_3R_57() {
        return jj_3R_72();
    }

    private final boolean jj_3R_56() {
        return jj_3R_71();
    }

    private final boolean jj_3R_155() {
        return jj_scan_token(14);
    }

    private final boolean jj_3R_50() {
        Token token2;
        Token token3 = this.jj_scanpos;
        if (jj_3R_56()) {
            this.jj_scanpos = token3;
            if (jj_3R_57()) {
                this.jj_scanpos = token3;
                if (jj_3R_58()) {
                    this.jj_scanpos = token3;
                    if (jj_3R_59()) {
                        this.jj_scanpos = token3;
                        if (jj_3R_60()) {
                            this.jj_scanpos = token3;
                            if (jj_3R_61()) {
                                this.jj_scanpos = token3;
                                if (jj_3R_62()) {
                                    this.jj_scanpos = token3;
                                    if (jj_3R_63()) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_64());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_167() {
        return jj_scan_token(62);
    }

    private final boolean jj_3R_83() {
        return jj_scan_token(76);
    }

    private final boolean jj_3R_145() {
        return jj_scan_token(20);
    }

    private final boolean jj_3R_71() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(89)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(90);
    }

    private final boolean jj_3R_24() {
        return jj_3R_28();
    }

    private final boolean jj_3R_186() {
        return jj_scan_token(61);
    }

    private final boolean jj_3R_38() {
        return jj_3R_47();
    }

    private final boolean jj_3R_75() {
        if (!jj_scan_token(124) && !jj_3R_89() && !jj_scan_token(FMParserConstants.CLOSE_BRACKET)) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_82() {
        return jj_scan_token(75);
    }

    private final boolean jj_3R_31() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_38()) {
            return false;
        }
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_166() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(60)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_186();
    }

    private final boolean jj_3R_154() {
        return jj_scan_token(23);
    }

    private final boolean jj_3_8() {
        return jj_scan_token(119);
    }

    private final boolean jj_3R_37() {
        if (!jj_scan_token(119) && !jj_3R_36()) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_128() {
        if (jj_scan_token(121) || jj_3R_24()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(121)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(123)) {
                return true;
            }
        }
        if (jj_3R_24()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_153() {
        return jj_scan_token(32);
    }

    private final boolean jj_3R_28() {
        Token token2;
        if (jj_3R_36()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_37());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_165() {
        return jj_scan_token(24);
    }

    private final boolean jj_3R_144() {
        return jj_scan_token(19);
    }

    private final boolean jj_3_7() {
        return jj_scan_token(118);
    }

    private final boolean jj_3R_85() {
        Token token2;
        if (jj_3R_24()) {
            return true;
        }
        Token token3 = this.jj_scanpos;
        if (jj_scan_token(121)) {
            this.jj_scanpos = token3;
            if (jj_scan_token(123)) {
                return true;
            }
        }
        if (jj_3R_24()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_128());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_72() {
        if (jj_scan_token(128)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_85()) {
            this.jj_scanpos = token2;
        }
        if (jj_scan_token(129)) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_46() {
        if (!jj_scan_token(118) && !jj_3R_45()) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_152() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(30)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(31);
    }

    private final boolean jj_3R_36() {
        Token token2;
        if (jj_3R_45()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_46());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_169() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(121)) {
            this.jj_scanpos = token2;
        }
        return jj_3R_24();
    }

    private final boolean jj_3R_88() {
        return jj_scan_token(88);
    }

    private final boolean jj_3R_70() {
        return jj_3R_84();
    }

    private final boolean jj_3R_87() {
        return jj_scan_token(87);
    }

    private final boolean jj_3R_69() {
        return jj_3R_83();
    }

    private final boolean jj_3R_150() {
        return jj_scan_token(12);
    }

    private final boolean jj_3_6() {
        return jj_3R_23();
    }

    private final boolean jj_3R_129() {
        Token token2;
        if (jj_3R_24()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_169());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_68() {
        return jj_3R_82();
    }

    private final boolean jj_3R_67() {
        return jj_3R_81();
    }

    private final boolean jj_3R_74() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_87()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_88();
    }

    private final boolean jj_3R_89() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_129()) {
            return false;
        }
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_53() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_67()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_68()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_3R_69()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_70();
    }

    private final boolean jj_3R_138() {
        return jj_3R_23();
    }

    private final boolean jj_3R_47() {
        Token token2;
        if (jj_3R_53()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_53());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_137() {
        return jj_scan_token(94);
    }

    private final boolean jj_3R_136() {
        return jj_scan_token(93);
    }

    private final boolean jj_3R_97() {
        if (jj_scan_token(92)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (!jj_3R_138()) {
            return false;
        }
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_96() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_136()) {
            this.jj_scanpos = token2;
            if (jj_3R_137()) {
                return true;
            }
        }
        if (jj_3R_23()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_86() {
        return jj_scan_token(86);
    }

    private final boolean jj_3R_80() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_96()) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_97();
    }

    private final boolean jj_3R_73() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(85)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_3R_86();
    }

    private final boolean jj_3_12() {
        return jj_scan_token(FMParserConstants.OPEN_PAREN);
    }

    private final boolean jj_3R_65() {
        if (jj_3R_23()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (!jj_3R_80()) {
            return false;
        }
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_149() {
        return jj_scan_token(11);
    }

    private final boolean jj_3_13() {
        if (!jj_scan_token(FMParserConstants.f7457ID) && !jj_scan_token(97)) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_132() {
        if (!jj_scan_token(FMParserConstants.OPEN_PAREN) && !jj_3R_89() && !jj_scan_token(FMParserConstants.CLOSE_PAREN)) {
            return false;
        }
        return true;
    }

    private final boolean jj_3_15() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(72)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(73)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(74);
    }

    private final boolean jj_3R_139() {
        return jj_scan_token(72);
    }

    private final boolean jj_3R_151() {
        return jj_scan_token(27);
    }

    private final boolean jj_3_5() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(FMParserConstants.NATURAL_GTE)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(110)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(FMParserConstants.NATURAL_GT)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(109)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(108)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(108)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(107);
    }

    private final boolean jj_3R_98() {
        Token token2 = this.jj_scanpos;
        if (!jj_3R_139()) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(73)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(74);
    }

    private final boolean jj_3_10() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(121)) {
            this.jj_scanpos = token2;
        }
        Token token3 = this.jj_scanpos;
        if (jj_scan_token(FMParserConstants.f7457ID)) {
            this.jj_scanpos = token3;
            if (jj_scan_token(85)) {
                return true;
            }
        }
        Token token4 = this.jj_scanpos;
        if (!jj_scan_token(97)) {
            return false;
        }
        this.jj_scanpos = token4;
        if (!jj_scan_token(100)) {
            return false;
        }
        this.jj_scanpos = token4;
        if (!jj_scan_token(101)) {
            return false;
        }
        this.jj_scanpos = token4;
        if (!jj_scan_token(102)) {
            return false;
        }
        this.jj_scanpos = token4;
        if (!jj_scan_token(103)) {
            return false;
        }
        this.jj_scanpos = token4;
        if (!jj_scan_token(104)) {
            return false;
        }
        this.jj_scanpos = token4;
        if (!jj_scan_token(105)) {
            return false;
        }
        this.jj_scanpos = token4;
        if (jj_scan_token(106)) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_81() {
        Token token2;
        if (jj_3R_98()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_98());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_142() {
        return jj_scan_token(13);
    }

    private final boolean jj_3R_66() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(FMParserConstants.NATURAL_GTE)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(110)) {
                this.jj_scanpos = token2;
                if (jj_scan_token(FMParserConstants.NATURAL_GT)) {
                    this.jj_scanpos = token2;
                    if (jj_scan_token(109)) {
                        this.jj_scanpos = token2;
                        if (jj_scan_token(108)) {
                            this.jj_scanpos = token2;
                            if (jj_scan_token(107)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        if (jj_3R_65()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_131() {
        if (!jj_scan_token(124) && !jj_3R_24() && !jj_scan_token(FMParserConstants.CLOSE_BRACKET)) {
            return false;
        }
        return true;
    }

    private final boolean jj_3R_51() {
        if (jj_3R_65()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (!jj_3R_66()) {
            return false;
        }
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_127() {
        return jj_3R_168();
    }

    private final boolean jj_3R_126() {
        return jj_3R_167();
    }

    private final boolean jj_3R_125() {
        return jj_3R_166();
    }

    private final boolean jj_3R_124() {
        return jj_3R_165();
    }

    private final boolean jj_3R_123() {
        return jj_3R_164();
    }

    private final boolean jj_3R_122() {
        return jj_3R_163();
    }

    private final boolean jj_3_4() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(99)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(97)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(98);
    }

    private final boolean jj_3R_121() {
        return jj_3R_162();
    }

    private final boolean jj_3R_120() {
        return jj_3R_161();
    }

    private final boolean jj_3R_119() {
        return jj_3R_160();
    }

    private final boolean jj_3R_118() {
        return jj_3R_159();
    }

    private final boolean jj_3R_117() {
        return jj_3R_158();
    }

    private final boolean jj_3R_52() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(99)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(97)) {
                this.jj_scanpos = token2;
                if (jj_scan_token(98)) {
                    return true;
                }
            }
        }
        if (jj_3R_51()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_116() {
        return jj_3R_157();
    }

    private final boolean jj_3R_115() {
        return jj_3R_156();
    }

    private final boolean jj_3R_114() {
        return jj_3R_155();
    }

    private final boolean jj_3R_45() {
        if (jj_3R_51()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (!jj_3R_52()) {
            return false;
        }
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_170() {
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(107)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(108)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(109)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(110)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(87)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(88)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(FMParserConstants.f7459IN)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(FMParserConstants.f7456AS)) {
            return false;
        }
        this.jj_scanpos = token2;
        return jj_scan_token(132);
    }

    private final boolean jj_3R_113() {
        return jj_3R_154();
    }

    private final boolean jj_3R_112() {
        return jj_3R_153();
    }

    private final boolean jj_3R_111() {
        return jj_3R_152();
    }

    private final boolean jj_3R_110() {
        return jj_3R_151();
    }

    private final boolean jj_3R_130() {
        if (jj_scan_token(91)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (!jj_scan_token(FMParserConstants.f7457ID)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(113)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (!jj_scan_token(114)) {
            return false;
        }
        this.jj_scanpos = token2;
        if (jj_3R_170()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_109() {
        return jj_3R_150();
    }

    private final boolean jj_3R_108() {
        return jj_3R_149();
    }

    private final boolean jj_3R_44() {
        return jj_scan_token(117);
    }

    private final boolean jj_3R_107() {
        return jj_3R_148();
    }

    private final boolean jj_3R_175() {
        return jj_scan_token(18);
    }

    private final boolean jj_3R_43() {
        return jj_scan_token(116);
    }

    private final boolean jj_3R_106() {
        return jj_3R_147();
    }

    static {
        jj_la1_0();
        jj_la1_1();
        jj_la1_2();
        jj_la1_3();
        jj_la1_4();
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 512, 0, 0, 0, 0, 0, 0, 0, 0, 0, 67108864, MediaHttpDownloader.MAXIMUM_CHUNK_SIZE, 0, 0, 458752, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6291456, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1073741824, 0, 0, 0, 0, 0, 32768, 0, -33472, 0, 0, 0, -33472, -33472, 0, 0, -33472, 0, 0, 0, 0, 0};
    }

    private static void jj_la1_1() {
        jj_la1_1 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65536, 96, 0, 65536, 16, 0, 0, 0, 805306368, 262144, 524288, 201326592, 31457280, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1792, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6144, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, MediaHttpDownloader.MAXIMUM_CHUNK_SIZE, 0, -33685503, 0, 0, 0, -33685503, -33685503, 0, 0, -33685503, 0, 0, 0, 0, 0};
    }

    private static void jj_la1_2() {
        jj_la1_2 = new int[]{266338304, 266338304, 0, 0, 0, 0, 0, 0, 1610612736, 1879048192, 1879048192, 100663296, 6291456, -2013265920, 0, 25165824, 25165824, 6291456, 25165824, 0, 0, 0, 266338304, 0, 0, 0, 0, 0, 0, 0, 0, 266338304, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 266338304, 0, 266338304, 0, 0, 0, 0, 256, 256, 0, 0, 10, 1792, 0, 0, 7946, 7946, 7936, 7936, 7946, 256, 0, 96, 0, 0};
    }

    private static void jj_la1_3() {
        jj_la1_3 = new int[]{1342177280, 1359052800, 16777216, 98304, 98304, 3276800, 14, 30720, 0, 0, 0, 0, 0, 1358954497, 16777216, 30720, 423936, 0, 0, 167772160, MediaHttpDownloader.MAXIMUM_CHUNK_SIZE, 167772160, 1359052800, 67108864, 0, 0, 0, 0, 0, 0, 0, 1359052800, 0, 0, 0, 0, 0, 0, 0, 498, 1536, 2034, MediaHttpDownloader.MAXIMUM_CHUNK_SIZE, 498, 1536, 2034, 0, 0, 0, 2034, 67108864, 0, 0, 1073741824, 0, 524288, 2, MediaHttpDownloader.MAXIMUM_CHUNK_SIZE, Integer.MIN_VALUE, 0, 0, 0, MediaHttpDownloader.MAXIMUM_CHUNK_SIZE, 0, 0, 0, 67108864, 0, Integer.MIN_VALUE, 0, 1392607232, MediaHttpDownloader.MAXIMUM_CHUNK_SIZE, 1359052800, 0, 67108864, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, MediaHttpDownloader.MAXIMUM_CHUNK_SIZE, 0};
    }

    private static void jj_la1_4() {
        jj_la1_4 = new int[]{33, 33, 0, 0, 0, 0, 0, 24576, 0, 0, 0, 0, 32, 65536, 65536, 28, 60, 0, 0, 0, 0, 0, 33, 0, 0, 0, 0, 8, 0, 0, 16, 33, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 0, 2052, 0, 32, 0, 0, 32, 0, 0, 0, 0, 0, 32768, 32768, 32768, 32768, 32768, 32800, 0, 6144, 0, 32, 33, 0, 33, 0, 0, 32, 6144, 0, 0, 0, 6144, 0, 0, 1966080, 1966080, 0, 0, 0, 0, 0, 0, 32, 0, 0, 32};
    }

    public FMParser(InputStream inputStream) {
        this.escapes = new LinkedList();
        int i = 0;
        this.lookingAhead = false;
        this.jj_la1 = new int[95];
        this.jj_2_rtns = new JJCalls[16];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new Vector();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.jj_input_stream = new SimpleCharStream(inputStream, 1, 1);
        this.token_source = new FMParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i2 = 0; i2 < 95; i2++) {
            this.jj_la1[i2] = -1;
        }
        while (true) {
            JJCalls[] jJCallsArr = this.jj_2_rtns;
            if (i < jJCallsArr.length) {
                jJCallsArr[i] = new JJCalls();
                i++;
            } else {
                return;
            }
        }
    }

    public void ReInit(InputStream inputStream) {
        this.jj_input_stream.ReInit(inputStream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        int i = 0;
        this.jj_gen = 0;
        for (int i2 = 0; i2 < 95; i2++) {
            this.jj_la1[i2] = -1;
        }
        while (true) {
            JJCalls[] jJCallsArr = this.jj_2_rtns;
            if (i < jJCallsArr.length) {
                jJCallsArr[i] = new JJCalls();
                i++;
            } else {
                return;
            }
        }
    }

    public FMParser(Reader reader) {
        this.escapes = new LinkedList();
        int i = 0;
        this.lookingAhead = false;
        this.jj_la1 = new int[95];
        this.jj_2_rtns = new JJCalls[16];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new Vector();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.jj_input_stream = new SimpleCharStream(reader, 1, 1);
        this.token_source = new FMParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i2 = 0; i2 < 95; i2++) {
            this.jj_la1[i2] = -1;
        }
        while (true) {
            JJCalls[] jJCallsArr = this.jj_2_rtns;
            if (i < jJCallsArr.length) {
                jJCallsArr[i] = new JJCalls();
                i++;
            } else {
                return;
            }
        }
    }

    public void ReInit(Reader reader) {
        this.jj_input_stream.ReInit(reader, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        int i = 0;
        this.jj_gen = 0;
        for (int i2 = 0; i2 < 95; i2++) {
            this.jj_la1[i2] = -1;
        }
        while (true) {
            JJCalls[] jJCallsArr = this.jj_2_rtns;
            if (i < jJCallsArr.length) {
                jJCallsArr[i] = new JJCalls();
                i++;
            } else {
                return;
            }
        }
    }

    public FMParser(FMParserTokenManager fMParserTokenManager) {
        this.escapes = new LinkedList();
        int i = 0;
        this.lookingAhead = false;
        this.jj_la1 = new int[95];
        this.jj_2_rtns = new JJCalls[16];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new Vector();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.token_source = fMParserTokenManager;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i2 = 0; i2 < 95; i2++) {
            this.jj_la1[i2] = -1;
        }
        while (true) {
            JJCalls[] jJCallsArr = this.jj_2_rtns;
            if (i < jJCallsArr.length) {
                jJCallsArr[i] = new JJCalls();
                i++;
            } else {
                return;
            }
        }
    }

    public void ReInit(FMParserTokenManager fMParserTokenManager) {
        this.token_source = fMParserTokenManager;
        this.token = new Token();
        this.jj_ntk = -1;
        int i = 0;
        this.jj_gen = 0;
        for (int i2 = 0; i2 < 95; i2++) {
            this.jj_la1[i2] = -1;
        }
        while (true) {
            JJCalls[] jJCallsArr = this.jj_2_rtns;
            if (i < jJCallsArr.length) {
                jJCallsArr[i] = new JJCalls();
                i++;
            } else {
                return;
            }
        }
    }

    private final Token jj_consume_token(int i) throws ParseException {
        Token token2 = this.token;
        if (token2.next != null) {
            this.token = this.token.next;
        } else {
            Token token3 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token3.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        if (this.token.kind == i) {
            this.jj_gen++;
            int i2 = this.jj_gc + 1;
            this.jj_gc = i2;
            if (i2 > 100) {
                int i3 = 0;
                this.jj_gc = 0;
                while (true) {
                    JJCalls[] jJCallsArr = this.jj_2_rtns;
                    if (i3 >= jJCallsArr.length) {
                        break;
                    }
                    for (JJCalls jJCalls = jJCallsArr[i3]; jJCalls != null; jJCalls = jJCalls.next) {
                        if (jJCalls.gen < this.jj_gen) {
                            jJCalls.first = null;
                        }
                    }
                    i3++;
                }
            }
            return this.token;
        }
        this.token = token2;
        this.jj_kind = i;
        throw generateParseException();
    }

    private static final class LookaheadSuccess extends Error {
        private LookaheadSuccess() {
        }
    }

    private final boolean jj_scan_token(int i) {
        Token token2 = this.jj_scanpos;
        if (token2 == this.jj_lastpos) {
            this.jj_la--;
            if (token2.next == null) {
                Token token3 = this.jj_scanpos;
                Token nextToken = this.token_source.getNextToken();
                token3.next = nextToken;
                this.jj_scanpos = nextToken;
                this.jj_lastpos = nextToken;
            } else {
                Token token4 = this.jj_scanpos.next;
                this.jj_scanpos = token4;
                this.jj_lastpos = token4;
            }
        } else {
            this.jj_scanpos = token2.next;
        }
        if (this.jj_rescan) {
            Token token5 = this.token;
            int i2 = 0;
            while (token5 != null && token5 != this.jj_scanpos) {
                i2++;
                token5 = token5.next;
            }
            if (token5 != null) {
                jj_add_error_token(i, i2);
            }
        }
        if (this.jj_scanpos.kind != i) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_scanpos != this.jj_lastpos) {
            return false;
        }
        throw this.jj_ls;
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int i) {
        Token token2;
        Token token3 = this.lookingAhead ? this.jj_scanpos : this.token;
        for (int i2 = 0; i2 < i; i2++) {
            if (token3.next != null) {
                token2 = token3.next;
            } else {
                Token nextToken = this.token_source.getNextToken();
                token3.next = nextToken;
                token2 = nextToken;
            }
        }
        return token3;
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token3.next = nextToken;
            int i = nextToken.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    private void jj_add_error_token(int i, int i2) {
        if (i2 < 100) {
            int i3 = this.jj_endpos;
            if (i2 == i3 + 1) {
                int[] iArr = this.jj_lasttokens;
                this.jj_endpos = i3 + 1;
                iArr[i3] = i;
            } else if (i3 != 0) {
                this.jj_expentry = new int[i3];
                for (int i4 = 0; i4 < this.jj_endpos; i4++) {
                    this.jj_expentry[i4] = this.jj_lasttokens[i4];
                }
                Enumeration elements = this.jj_expentries.elements();
                boolean z = false;
                while (elements.hasMoreElements()) {
                    int[] iArr2 = (int[]) elements.nextElement();
                    if (iArr2.length == this.jj_expentry.length) {
                        int i5 = 0;
                        while (true) {
                            int[] iArr3 = this.jj_expentry;
                            if (i5 >= iArr3.length) {
                                z = true;
                                break;
                            } else if (iArr2[i5] != iArr3[i5]) {
                                z = false;
                                break;
                            } else {
                                i5++;
                            }
                        }
                        if (z) {
                            break;
                        }
                    }
                }
                if (!z) {
                    this.jj_expentries.addElement(this.jj_expentry);
                }
                if (i2 != 0) {
                    int[] iArr4 = this.jj_lasttokens;
                    this.jj_endpos = i2;
                    iArr4[i2 - 1] = i;
                }
            }
        }
    }

    public ParseException generateParseException() {
        this.jj_expentries.removeAllElements();
        boolean[] zArr = new boolean[149];
        for (int i = 0; i < 149; i++) {
            zArr[i] = false;
        }
        int i2 = this.jj_kind;
        if (i2 >= 0) {
            zArr[i2] = true;
            this.jj_kind = -1;
        }
        for (int i3 = 0; i3 < 95; i3++) {
            if (this.jj_la1[i3] == this.jj_gen) {
                for (int i4 = 0; i4 < 32; i4++) {
                    int i5 = 1 << i4;
                    if ((jj_la1_0[i3] & i5) != 0) {
                        zArr[i4] = true;
                    }
                    if ((jj_la1_1[i3] & i5) != 0) {
                        zArr[i4 + 32] = true;
                    }
                    if ((jj_la1_2[i3] & i5) != 0) {
                        zArr[i4 + 64] = true;
                    }
                    if ((jj_la1_3[i3] & i5) != 0) {
                        zArr[i4 + 96] = true;
                    }
                    if ((jj_la1_4[i3] & i5) != 0) {
                        zArr[i4 + 128] = true;
                    }
                }
            }
        }
        for (int i6 = 0; i6 < 149; i6++) {
            if (zArr[i6]) {
                this.jj_expentry = new int[1];
                int[] iArr = this.jj_expentry;
                iArr[0] = i6;
                this.jj_expentries.addElement(iArr);
            }
        }
        this.jj_endpos = 0;
        jj_rescan_token();
        jj_add_error_token(0, 0);
        int[][] iArr2 = new int[this.jj_expentries.size()][];
        for (int i7 = 0; i7 < this.jj_expentries.size(); i7++) {
            iArr2[i7] = (int[]) this.jj_expentries.elementAt(i7);
        }
        return new ParseException(this.token, iArr2, tokenImage);
    }

    private final void jj_rescan_token() {
        this.jj_rescan = true;
        for (int i = 0; i < 16; i++) {
            JJCalls jJCalls = this.jj_2_rtns[i];
            do {
                if (jJCalls.gen > this.jj_gen) {
                    this.jj_la = jJCalls.arg;
                    Token token2 = jJCalls.first;
                    this.jj_scanpos = token2;
                    this.jj_lastpos = token2;
                    switch (i) {
                        case 0:
                            jj_3_1();
                            break;
                        case 1:
                            jj_3_2();
                            break;
                        case 2:
                            jj_3_3();
                            break;
                        case 3:
                            jj_3_4();
                            break;
                        case 4:
                            jj_3_5();
                            break;
                        case 5:
                            jj_3_6();
                            break;
                        case 6:
                            jj_3_7();
                            break;
                        case 7:
                            jj_3_8();
                            break;
                        case 8:
                            jj_3_9();
                            break;
                        case 9:
                            jj_3_10();
                            break;
                        case 10:
                            jj_3_11();
                            break;
                        case 11:
                            jj_3_12();
                            break;
                        case 12:
                            jj_3_13();
                            break;
                        case 13:
                            jj_3_14();
                            break;
                        case 14:
                            jj_3_15();
                            break;
                        case 15:
                            jj_3_16();
                            break;
                    }
                }
                jJCalls = jJCalls.next;
            } while (jJCalls != null);
        }
        this.jj_rescan = false;
    }

    private final void jj_save(int i, int i2) {
        JJCalls jJCalls = this.jj_2_rtns[i];
        while (true) {
            if (jJCalls.gen <= this.jj_gen) {
                break;
            } else if (jJCalls.next == null) {
                JJCalls jJCalls2 = new JJCalls();
                jJCalls.next = jJCalls2;
                jJCalls = jJCalls2;
                break;
            } else {
                jJCalls = jJCalls.next;
            }
        }
        jJCalls.gen = (this.jj_gen + i2) - this.jj_la;
        jJCalls.first = this.token;
        jJCalls.arg = i2;
    }

    static final class JJCalls {
        int arg;
        Token first;
        int gen;
        JJCalls next;

        JJCalls() {
        }
    }
}
