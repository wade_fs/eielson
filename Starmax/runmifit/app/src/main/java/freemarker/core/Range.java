package freemarker.core;

import freemarker.core.Expression;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template._TemplateAPI;

final class Range extends Expression {
    static final int END_EXCLUSIVE = 1;
    static final int END_INCLUSIVE = 0;
    static final int END_SIZE_LIMITED = 3;
    static final int END_UNBOUND = 2;
    final int endType;
    final Expression lho;
    final Expression rho;

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    Range(Expression expression, Expression expression2, int i) {
        this.lho = super;
        this.rho = super;
        this.endType = i;
    }

    /* access modifiers changed from: package-private */
    public int getEndType() {
        return this.endType;
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        int intValue = this.lho.evalToNumber(environment).intValue();
        if (this.endType == 2) {
            return _TemplateAPI.getTemplateLanguageVersionAsInt(this) >= _TemplateAPI.VERSION_INT_2_3_21 ? new ListableRightUnboundedRangeModel(intValue) : new NonListableRightUnboundedRangeModel(intValue);
        }
        int intValue2 = this.rho.evalToNumber(environment).intValue();
        if (this.endType == 3) {
            intValue2 += intValue;
        }
        boolean z = true;
        boolean z2 = this.endType == 0;
        if (this.endType != 3) {
            z = false;
        }
        return new BoundedRangeModel(intValue, intValue2, z2, z);
    }

    /* access modifiers changed from: package-private */
    public boolean evalToBoolean(Environment environment) throws TemplateException {
        throw new NonBooleanException(super, new BoundedRangeModel(0, 0, false, false), environment);
    }

    public String getCanonicalForm() {
        Expression expression = this.rho;
        String canonicalForm = expression != null ? super.getCanonicalForm() : "";
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.lho.getCanonicalForm());
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(canonicalForm);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        int i = this.endType;
        if (i == 0) {
            return "..";
        }
        if (i == 1) {
            return "..<";
        }
        if (i == 2) {
            return "..";
        }
        if (i == 3) {
            return "..*";
        }
        throw new BugException(i);
    }

    /* access modifiers changed from: package-private */
    public boolean isLiteral() {
        Expression expression = this.rho;
        boolean z = expression == null || super.isLiteral();
        if (this.constantValue != null || (this.lho.isLiteral() && z)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public Expression deepCloneWithIdentifierReplaced_inner(String str, Expression expression, Expression.ReplacemenetState replacemenetState) {
        return new Range(this.lho.deepCloneWithIdentifierReplaced(str, super, replacemenetState), this.rho.deepCloneWithIdentifierReplaced(str, super, replacemenetState), this.endType);
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.lho;
        }
        if (i == 1) {
            return this.rho;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        return ParameterRole.forBinaryOperatorOperand(i);
    }
}
