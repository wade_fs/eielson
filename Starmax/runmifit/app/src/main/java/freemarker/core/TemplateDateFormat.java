package freemarker.core;

import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateModelException;
import java.text.ParseException;
import java.util.Date;

abstract class TemplateDateFormat {
    public abstract String format(TemplateDateModel templateDateModel) throws UnformattableDateException, TemplateModelException;

    public abstract String getDescription();

    public abstract boolean isLocaleBound();

    public abstract Date parse(String str) throws ParseException;

    TemplateDateFormat() {
    }
}
