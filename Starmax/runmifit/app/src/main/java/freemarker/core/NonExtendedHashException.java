package freemarker.core;

import freemarker.template.TemplateModel;

public class NonExtendedHashException extends UnexpectedTypeException {
    private static final Class[] EXPECTED_TYPES;
    static /* synthetic */ Class class$freemarker$template$TemplateHashModelEx;

    static {
        Class[] clsArr = new Class[1];
        Class cls = class$freemarker$template$TemplateHashModelEx;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateHashModelEx");
            class$freemarker$template$TemplateHashModelEx = cls;
        }
        clsArr[0] = cls;
        EXPECTED_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonExtendedHashException(Environment environment) {
        super(environment, "Expecting extended hash value here");
    }

    public NonExtendedHashException(String str, Environment environment) {
        super(environment, str);
    }

    NonExtendedHashException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(environment, _errordescriptionbuilder);
    }

    NonExtendedHashException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "extended hash", EXPECTED_TYPES, environment);
    }

    NonExtendedHashException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "extended hash", EXPECTED_TYPES, str, environment);
    }

    NonExtendedHashException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "extended hash", EXPECTED_TYPES, strArr, environment);
    }
}
