package freemarker.core;

import freemarker.template.TemplateException;
import freemarker.template.utility.StandardCompress;
import java.io.IOException;

final class CompressedBlock extends TemplateElement {
    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#compress";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    CompressedBlock(TemplateElement templateElement) {
        setNestedBlock(super);
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        if (getNestedBlock() != null) {
            environment.visitAndTransform(getNestedBlock(), StandardCompress.INSTANCE, null);
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        if (!z) {
            return getNodeTypeSymbol();
        }
        String canonicalForm = getNestedBlock() != null ? getNestedBlock().getCanonicalForm() : "";
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<");
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(">");
        stringBuffer.append(canonicalForm);
        stringBuffer.append("</");
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(">");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public boolean isIgnorable() {
        return getNestedBlock() == null || getNestedBlock().isIgnorable();
    }
}
