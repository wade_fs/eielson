package freemarker.core;

abstract class Interpolation extends TemplateElement {
    /* access modifiers changed from: protected */
    public abstract String dump(boolean z, boolean z2);

    Interpolation() {
    }

    /* access modifiers changed from: protected */
    public final String dump(boolean z) {
        return dump(z, false);
    }

    /* access modifiers changed from: package-private */
    public final String getCanonicalFormInStringLiteral() {
        return dump(true, true);
    }
}
