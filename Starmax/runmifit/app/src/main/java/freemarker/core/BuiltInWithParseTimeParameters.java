package freemarker.core;

import freemarker.core.Expression;
import java.util.List;

abstract class BuiltInWithParseTimeParameters extends SpecialBuiltIn {
    /* access modifiers changed from: package-private */
    public abstract void bindToParameters(List list, Token token, Token token2) throws ParseException;

    /* access modifiers changed from: protected */
    public abstract void cloneArguments(Expression expression, String str, Expression expression2, Expression.ReplacemenetState replacemenetState);

    /* access modifiers changed from: protected */
    public abstract Expression getArgumentParameterValue(int i);

    /* access modifiers changed from: protected */
    public abstract List getArgumentsAsList();

    /* access modifiers changed from: protected */
    public abstract int getArgumentsCount();

    BuiltInWithParseTimeParameters() {
    }

    public String getCanonicalForm() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(super.getCanonicalForm());
        stringBuffer.append("(");
        List argumentsAsList = getArgumentsAsList();
        int size = argumentsAsList.size();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                stringBuffer.append(", ");
            }
            stringBuffer.append(((Expression) argumentsAsList.get(i)).getCanonicalForm());
        }
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(super.getNodeTypeSymbol());
        stringBuffer.append("(...)");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return super.getParameterCount() + getArgumentsCount();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        int parameterCount = super.getParameterCount();
        if (i < parameterCount) {
            return super.getParameterValue(i);
        }
        return getArgumentParameterValue(i - parameterCount);
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        int parameterCount = super.getParameterCount();
        if (i < parameterCount) {
            return super.getParameterRole(i);
        }
        if (i - parameterCount < getArgumentsCount()) {
            return ParameterRole.ARGUMENT_VALUE;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: protected */
    public ParseException newArgumentCountException(String str, Token token, Token token2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?");
        stringBuffer.append(this.key);
        stringBuffer.append("(...) ");
        stringBuffer.append(str);
        stringBuffer.append(" parameters");
        return new ParseException(stringBuffer.toString(), getTemplate(), token.beginLine, token.beginColumn, token2.endLine, token2.endColumn);
    }

    /* access modifiers changed from: protected */
    public Expression deepCloneWithIdentifierReplaced_inner(String str, Expression expression, Expression.ReplacemenetState replacemenetState) {
        Expression deepCloneWithIdentifierReplaced_inner = super.deepCloneWithIdentifierReplaced_inner(str, expression, replacemenetState);
        cloneArguments(deepCloneWithIdentifierReplaced_inner, str, expression, replacemenetState);
        return deepCloneWithIdentifierReplaced_inner;
    }
}
