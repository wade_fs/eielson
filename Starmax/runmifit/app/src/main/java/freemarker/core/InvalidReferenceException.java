package freemarker.core;

import freemarker.ext.servlet.FreemarkerServlet;
import freemarker.template.TemplateException;

public class InvalidReferenceException extends TemplateException {
    static final InvalidReferenceException FAST_INSTANCE;
    private static final String[] TIP = {"If the failing expression is known to be legally refer to something that's sometimes null or missing, either specify a default value like myOptionalVar!myDefault, or use ", "<#if myOptionalVar??>", "when-present", "<#else>", "when-missing", "</#if>", ". (These only cover the last step of the expression; to cover the whole expression, use parenthesis: (myOptionalVar.foo)!myDefault, (myOptionalVar.foo)??"};
    private static final String TIP_JSP_TAGLIBS = "The \"JspTaglibs\" variable isn't a core FreeMarker feature; it's only available when templates are invoked through freemarker.ext.servlet.FreemarkerServlet (or other custom FreeMarker-JSP integration solution).";
    private static final String TIP_LAST_STEP_DOT = "It's the step after the last dot that caused this error, not those before it.";
    private static final String TIP_LAST_STEP_SQUARE_BRACKET = "It's the final [] step that caused this error, not those before it.";
    private static final String[] TIP_MISSING_ASSIGNMENT_TARGET = {"If the target variable is known to be legally null or missing sometimes, instead of something like ", "<#assign x += 1>", ", you could write ", "<#if x??>", "<#assign x += 1>", "</#if>", " or ", "<#assign x = (x!0) + 1>"};
    private static final String TIP_NO_DOLLAR = "Variable references must not start with \"$\", unless the \"$\" is really part of the variable name.";

    /* JADX INFO: finally extract failed */
    static {
        Environment currentEnvironment = Environment.getCurrentEnvironment();
        try {
            Environment.setCurrentEnvironment(null);
            FAST_INSTANCE = new InvalidReferenceException("Invalid reference. Details are unavilable, as this should have been handled by an FTL construct. If it wasn't, that's problably a bug in FreeMarker.", null);
            Environment.setCurrentEnvironment(currentEnvironment);
        } catch (Throwable th) {
            Environment.setCurrentEnvironment(currentEnvironment);
            throw th;
        }
    }

    public InvalidReferenceException(Environment environment) {
        super("Invalid reference: The expression has evaluated to null or refers to something that doesn't exist.", environment);
    }

    public InvalidReferenceException(String str, Environment environment) {
        super(str, environment);
    }

    InvalidReferenceException(_ErrorDescriptionBuilder _errordescriptionbuilder, Environment environment, Expression expression) {
        super(null, environment, expression, _errordescriptionbuilder);
    }

    static InvalidReferenceException getInstance(Expression expression, Environment environment) {
        Object[] objArr;
        if (environment != null && environment.getFastInvalidReferenceExceptions()) {
            return FAST_INSTANCE;
        }
        if (expression == null) {
            return new InvalidReferenceException(environment);
        }
        _ErrorDescriptionBuilder blame = new _ErrorDescriptionBuilder("The following has evaluated to null or missing:").blame(expression);
        if (endsWithDollarVariable(expression)) {
            blame.tips(new Object[]{TIP_NO_DOLLAR, TIP});
        } else if (expression instanceof Dot) {
            String rho = ((Dot) expression).getRHO();
            String str = null;
            if ("size".equals(rho)) {
                str = "To query the size of a collection or map use ?size, like myList?size";
            } else if ("length".equals(rho)) {
                str = "To query the length of a string use ?length, like myString?size";
            }
            if (str == null) {
                objArr = new Object[]{TIP_LAST_STEP_DOT, TIP};
            } else {
                objArr = new Object[]{TIP_LAST_STEP_DOT, str, TIP};
            }
            blame.tips(objArr);
        } else if (expression instanceof DynamicKeyName) {
            blame.tips(new Object[]{TIP_LAST_STEP_SQUARE_BRACKET, TIP});
        } else if (!(expression instanceof Identifier) || !((Identifier) expression).getName().equals(FreemarkerServlet.KEY_JSP_TAGLIBS)) {
            blame.tip((Object[]) TIP);
        } else {
            blame.tips(new Object[]{TIP_JSP_TAGLIBS, TIP});
        }
        return new InvalidReferenceException(blame, environment, expression);
    }

    static InvalidReferenceException getInstance(String str, String str2, Environment environment) {
        if (environment != null && environment.getFastInvalidReferenceExceptions()) {
            return FAST_INSTANCE;
        }
        _ErrorDescriptionBuilder _errordescriptionbuilder = new _ErrorDescriptionBuilder(new Object[]{"The target variable of the assignment, ", new _DelayedJQuote(str), ", was null or missing, but the \"", str2, "\" operator needs to get its value before assigning to it."});
        if (str.startsWith("$")) {
            _errordescriptionbuilder.tips(new Object[]{TIP_NO_DOLLAR, TIP_MISSING_ASSIGNMENT_TARGET});
        } else {
            _errordescriptionbuilder.tip((Object[]) TIP_MISSING_ASSIGNMENT_TARGET);
        }
        return new InvalidReferenceException(_errordescriptionbuilder, environment, null);
    }

    private static boolean endsWithDollarVariable(Expression expression) {
        return ((expression instanceof Identifier) && ((Identifier) expression).getName().startsWith("$")) || ((expression instanceof Dot) && ((Dot) expression).getRHO().startsWith("$"));
    }
}
