package freemarker.core;

import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import com.tencent.bugly.beta.tinker.TinkerReport;
import freemarker.cache.MruCacheStorage;
import freemarker.log.Logger;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.StringUtil;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class RegexpHelper {
    private static final Logger LOG = Logger.getLogger("freemarker.runtime");
    private static final int MAX_FLAG_WARNINGS_LOGGED = 25;
    static final long RE_FLAG_CASE_INSENSITIVE = intFlagToLong(2);
    static final long RE_FLAG_COMMENTS = intFlagToLong(4);
    static final long RE_FLAG_DOTALL = intFlagToLong(32);
    static final long RE_FLAG_FIRST_ONLY = 8589934592L;
    static final long RE_FLAG_MULTILINE = intFlagToLong(8);
    static final long RE_FLAG_REGEXP = 4294967296L;
    private static int flagWarningsCnt;
    private static final Object flagWarningsCntSync = new Object();
    private static volatile boolean flagWarningsEnabled = LOG.isWarnEnabled();
    private static final MruCacheStorage patternCache = new MruCacheStorage(50, TinkerReport.KEY_APPLIED_PACKAGE_CHECK_SIGNATURE);

    private static long intFlagToLong(int i) {
        return ((long) i) & 65535;
    }

    private RegexpHelper() {
    }

    static Pattern getPattern(String str, int i) throws TemplateModelException {
        Pattern pattern;
        PatternCacheKey patternCacheKey = new PatternCacheKey(str, i);
        synchronized (patternCache) {
            pattern = (Pattern) patternCache.get(patternCacheKey);
        }
        if (pattern != null) {
            return pattern;
        }
        try {
            Pattern compile = Pattern.compile(str, i);
            synchronized (patternCache) {
                patternCache.put(patternCacheKey, compile);
            }
            return compile;
        } catch (PatternSyntaxException e) {
            throw new _TemplateModelException(e, new Object[]{"Malformed regular expression: ", new _DelayedGetMessage(e)});
        }
    }

    private static class PatternCacheKey {
        private final int flags;
        private final int hashCode;
        private final String patternString;

        public PatternCacheKey(String str, int i) {
            this.patternString = str;
            this.flags = i;
            this.hashCode = str.hashCode() + (i * 31);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof PatternCacheKey)) {
                return false;
            }
            PatternCacheKey patternCacheKey = (PatternCacheKey) obj;
            if (patternCacheKey.flags != this.flags || !patternCacheKey.patternString.equals(this.patternString)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.hashCode;
        }
    }

    static long parseFlagString(String str) {
        long j;
        long j2 = 0;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt == 'c') {
                j = RE_FLAG_COMMENTS;
            } else if (charAt == 'f') {
                j = RE_FLAG_FIRST_ONLY;
            } else if (charAt == 'i') {
                j = RE_FLAG_CASE_INSENSITIVE;
            } else if (charAt == 'm') {
                j = RE_FLAG_MULTILINE;
            } else if (charAt == 'r') {
                j = RE_FLAG_REGEXP;
            } else if (charAt != 's') {
                if (flagWarningsEnabled) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Unrecognized regular expression flag: ");
                    stringBuffer.append(StringUtil.jQuote(String.valueOf(charAt)));
                    stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                    logFlagWarning(stringBuffer.toString());
                }
            } else {
                j = RE_FLAG_DOTALL;
            }
            j2 |= j;
        }
        return j2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0015, code lost:
        r0 = new java.lang.StringBuffer();
        r0.append(r4);
        r0.append(" This will be an error in some later FreeMarker version!");
        r4 = r0.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if ((r1 + 1) != 25) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        r0 = new java.lang.StringBuffer();
        r0.append(r4);
        r0.append(" [Will not log more regular expression flag problems until restart!]");
        r4 = r0.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003b, code lost:
        freemarker.core.RegexpHelper.LOG.warn(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void logFlagWarning(java.lang.String r4) {
        /*
            boolean r0 = freemarker.core.RegexpHelper.flagWarningsEnabled
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            java.lang.Object r0 = freemarker.core.RegexpHelper.flagWarningsCntSync
            monitor-enter(r0)
            int r1 = freemarker.core.RegexpHelper.flagWarningsCnt     // Catch:{ all -> 0x0046 }
            r2 = 25
            if (r1 >= r2) goto L_0x0041
            int r3 = freemarker.core.RegexpHelper.flagWarningsCnt     // Catch:{ all -> 0x0046 }
            int r3 = r3 + 1
            freemarker.core.RegexpHelper.flagWarningsCnt = r3     // Catch:{ all -> 0x0046 }
            monitor-exit(r0)     // Catch:{ all -> 0x0046 }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r0.append(r4)
            java.lang.String r4 = " This will be an error in some later FreeMarker version!"
            r0.append(r4)
            java.lang.String r4 = r0.toString()
            int r1 = r1 + 1
            if (r1 != r2) goto L_0x003b
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r0.append(r4)
            java.lang.String r4 = " [Will not log more regular expression flag problems until restart!]"
            r0.append(r4)
            java.lang.String r4 = r0.toString()
        L_0x003b:
            freemarker.log.Logger r0 = freemarker.core.RegexpHelper.LOG
            r0.warn(r4)
            return
        L_0x0041:
            r4 = 0
            freemarker.core.RegexpHelper.flagWarningsEnabled = r4     // Catch:{ all -> 0x0046 }
            monitor-exit(r0)     // Catch:{ all -> 0x0046 }
            return
        L_0x0046:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0046 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.RegexpHelper.logFlagWarning(java.lang.String):void");
    }

    static void checkNonRegexpFlags(String str, long j) throws _TemplateModelException {
        checkOnlyHasNonRegexpFlags(str, j, false);
    }

    static void checkOnlyHasNonRegexpFlags(String str, long j, boolean z) throws _TemplateModelException {
        String str2;
        if (z || flagWarningsEnabled) {
            if ((RE_FLAG_MULTILINE & j) != 0) {
                str2 = Config.MODEL;
            } else if ((RE_FLAG_DOTALL & j) != 0) {
                str2 = "s";
            } else if ((j & RE_FLAG_COMMENTS) != 0) {
                str2 = "c";
            } else {
                return;
            }
            Object[] objArr = {"?", str, " doesn't support the \"", str2, "\" flag without the \"r\" flag."};
            if (!z) {
                logFlagWarning(new _ErrorDescriptionBuilder(objArr).toString());
                return;
            }
            throw new _TemplateModelException(objArr);
        }
    }
}
