package freemarker.core;

import java.text.ParseException;
import java.util.TimeZone;

class XSTemplateDateFormatFactory extends ISOLikeTemplateDateFormatFactory {
    public XSTemplateDateFormatFactory(TimeZone timeZone) {
        super(timeZone);
    }

    public TemplateDateFormat get(int i, boolean z, String str) throws ParseException, UnknownDateTypeFormattingUnsupportedException {
        return new XSTemplateDateFormat(str, 2, i, z, getTimeZone(), super);
    }
}
