package freemarker.core;

import freemarker.template.utility.ClassUtil;

public class _DelayedShortClassName extends _DelayedConversionToString {
    public _DelayedShortClassName(Class cls) {
        super(cls);
    }

    /* access modifiers changed from: protected */
    public String doConversion(Object obj) {
        return ClassUtil.getShortClassName((Class) obj, true);
    }
}
