package freemarker.core;

import freemarker.template.Template;

abstract class TemplatePostProcessor {
    public abstract void postProcess(Template template) throws TemplatePostProcessorException;

    TemplatePostProcessor() {
    }
}
