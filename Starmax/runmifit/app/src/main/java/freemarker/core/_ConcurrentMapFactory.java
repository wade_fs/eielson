package freemarker.core;

import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.UndeclaredThrowableException;
import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.Map;

public class _ConcurrentMapFactory {
    private static final Class bestHashMapClass;
    private static final Constructor bestHashMapClassConstructor;
    private static final int bestHashMapClassConstructorParamCnt;
    static /* synthetic */ Class class$java$util$HashMap;
    private static final Class concurrentMapClass;

    static {
        Class cls;
        Class cls2;
        Constructor constructor;
        try {
            cls = ClassUtil.forName("java.util.concurrent.ConcurrentMap");
        } catch (ClassNotFoundException unused) {
            cls = null;
        }
        concurrentMapClass = cls;
        int i = 3;
        try {
            cls2 = ClassUtil.forName("java.util.concurrent.ConcurrentHashMap");
            constructor = cls2.getConstructor(Integer.TYPE, Float.TYPE, Integer.TYPE);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get ConcurrentHashMap constructor", e);
        } catch (ClassNotFoundException unused2) {
            Class cls3 = class$java$util$HashMap;
            if (cls3 == null) {
                cls3 = class$("java.util.HashMap");
                class$java$util$HashMap = cls3;
            }
            cls2 = cls3;
            try {
                constructor = cls2.getConstructor(Integer.TYPE, Float.TYPE);
                i = 2;
            } catch (Exception e2) {
                throw new RuntimeException("Failed to get HashMap constructor", e2);
            }
        }
        bestHashMapClass = cls2;
        bestHashMapClassConstructor = constructor;
        bestHashMapClassConstructorParamCnt = i;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static Map newMaybeConcurrentHashMap() {
        try {
            return (Map) bestHashMapClass.newInstance();
        } catch (Exception e) {
            throw new UndeclaredThrowableException(e);
        }
    }

    public static Map newMaybeConcurrentHashMap(int i, float f, int i2) {
        try {
            if (bestHashMapClassConstructorParamCnt == 3) {
                return (Map) bestHashMapClassConstructor.newInstance(new Integer(i), new Float(f), new Integer(i2));
            } else if (bestHashMapClassConstructorParamCnt == 2) {
                return (Map) bestHashMapClassConstructor.newInstance(new Integer(i), new Float(f));
            } else {
                throw new BugException();
            }
        } catch (Exception e) {
            throw new UndeclaredThrowableException(e);
        }
    }

    public static Map newThreadSafeMap() {
        Map newMaybeConcurrentHashMap = newMaybeConcurrentHashMap();
        return isConcurrent(newMaybeConcurrentHashMap) ? newMaybeConcurrentHashMap : Collections.synchronizedMap(newMaybeConcurrentHashMap);
    }

    public static boolean concurrentMapsAvailable() {
        return concurrentMapClass != null;
    }

    public static boolean isConcurrent(Map map) {
        Class cls = concurrentMapClass;
        return cls != null && cls.isInstance(map);
    }
}
