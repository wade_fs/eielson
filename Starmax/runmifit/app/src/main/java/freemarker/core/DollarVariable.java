package freemarker.core;

import freemarker.template.TemplateException;
import freemarker.template.utility.StringUtil;
import java.io.IOException;
import org.apache.commons.math3.geometry.VectorFormat;

final class DollarVariable extends Interpolation {
    private final Expression escapedExpression;
    private final Expression expression;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "${...}";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public boolean heedsOpeningWhitespace() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean heedsTrailingWhitespace() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    DollarVariable(Expression expression2, Expression expression3) {
        this.expression = expression2;
        this.escapedExpression = expression3;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        environment.getOut().write(this.escapedExpression.evalAndCoerceToString(environment));
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z, boolean z2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("${");
        String canonicalForm = this.expression.getCanonicalForm();
        if (z2) {
            canonicalForm = StringUtil.FTLStringLiteralEnc(canonicalForm, '\"');
        }
        stringBuffer.append(canonicalForm);
        stringBuffer.append(VectorFormat.DEFAULT_SUFFIX);
        if (!z && this.expression != this.escapedExpression) {
            stringBuffer.append(" auto-escaped");
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.expression;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.CONTENT;
        }
        throw new IndexOutOfBoundsException();
    }
}
