package freemarker.core;

import freemarker.template.TemplateModelException;

final class NonListableRightUnboundedRangeModel extends RightUnboundedRangeModel {
    public int size() throws TemplateModelException {
        return 0;
    }

    NonListableRightUnboundedRangeModel(int i) {
        super(i);
    }
}
