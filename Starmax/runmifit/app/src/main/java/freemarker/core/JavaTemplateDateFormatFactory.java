package freemarker.core;

import freemarker.template.TemplateModelException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

class JavaTemplateDateFormatFactory extends TemplateDateFormatFactory {
    private static final Map JAVA_DATE_FORMATS = new HashMap();
    private Map[] formatCache;
    private final Locale locale;

    public boolean isLocaleBound() {
        return true;
    }

    public JavaTemplateDateFormatFactory(TimeZone timeZone, Locale locale2) {
        super(timeZone);
        this.locale = locale2;
    }

    public TemplateDateFormat get(int i, boolean z, String str) throws ParseException, TemplateModelException, UnknownDateTypeFormattingUnsupportedException {
        Map[] mapArr = this.formatCache;
        if (mapArr == null) {
            mapArr = new Map[4];
            this.formatCache = mapArr;
        }
        Map map = mapArr[i];
        if (map == null) {
            map = new HashMap();
            mapArr[i] = map;
        }
        TemplateDateFormat templateDateFormat = (TemplateDateFormat) map.get(str);
        if (templateDateFormat != null) {
            return templateDateFormat;
        }
        JavaTemplateDateFormat javaTemplateDateFormat = new JavaTemplateDateFormat(getJavaDateFormat(i, str));
        map.put(str, javaTemplateDateFormat);
        return javaTemplateDateFormat;
    }

    private DateFormat getJavaDateFormat(int i, String str) throws UnknownDateTypeFormattingUnsupportedException, ParseException {
        DateFormat dateFormat;
        DateFormatKey dateFormatKey = new DateFormatKey(i, str, this.locale, getTimeZone());
        synchronized (JAVA_DATE_FORMATS) {
            dateFormat = (DateFormat) JAVA_DATE_FORMATS.get(dateFormatKey);
            if (dateFormat == null) {
                StringTokenizer stringTokenizer = new StringTokenizer(str, "_");
                int parseDateStyleToken = stringTokenizer.hasMoreTokens() ? parseDateStyleToken(stringTokenizer.nextToken()) : 2;
                if (parseDateStyleToken != -1) {
                    if (i == 0) {
                        throw new UnknownDateTypeFormattingUnsupportedException();
                    } else if (i == 1) {
                        dateFormat = DateFormat.getTimeInstance(parseDateStyleToken, dateFormatKey.locale);
                    } else if (i == 2) {
                        dateFormat = DateFormat.getDateInstance(parseDateStyleToken, dateFormatKey.locale);
                    } else if (i == 3) {
                        int parseDateStyleToken2 = stringTokenizer.hasMoreTokens() ? parseDateStyleToken(stringTokenizer.nextToken()) : parseDateStyleToken;
                        if (parseDateStyleToken2 != -1) {
                            dateFormat = DateFormat.getDateTimeInstance(parseDateStyleToken, parseDateStyleToken2, dateFormatKey.locale);
                        }
                    }
                }
                if (dateFormat == null) {
                    try {
                        dateFormat = new SimpleDateFormat(str, dateFormatKey.locale);
                    } catch (IllegalArgumentException e) {
                        String message = e.getMessage();
                        if (message == null) {
                            message = "Illegal SimpleDateFormat pattern";
                        }
                        throw new ParseException(message, 0);
                    }
                }
                dateFormat.setTimeZone(dateFormatKey.timeZone);
                JAVA_DATE_FORMATS.put(dateFormatKey, dateFormat);
            }
        }
        return (DateFormat) dateFormat.clone();
    }

    private static final class DateFormatKey {
        private final int dateType;
        /* access modifiers changed from: private */
        public final Locale locale;
        private final String pattern;
        /* access modifiers changed from: private */
        public final TimeZone timeZone;

        DateFormatKey(int i, String str, Locale locale2, TimeZone timeZone2) {
            this.dateType = i;
            this.pattern = str;
            this.locale = locale2;
            this.timeZone = timeZone2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof DateFormatKey)) {
                return false;
            }
            DateFormatKey dateFormatKey = (DateFormatKey) obj;
            if (this.dateType != dateFormatKey.dateType || !dateFormatKey.pattern.equals(this.pattern) || !dateFormatKey.locale.equals(this.locale) || !dateFormatKey.timeZone.equals(this.timeZone)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return ((this.dateType ^ this.pattern.hashCode()) ^ this.locale.hashCode()) ^ this.timeZone.hashCode();
        }
    }

    private int parseDateStyleToken(String str) {
        if ("short".equals(str)) {
            return 3;
        }
        if ("medium".equals(str)) {
            return 2;
        }
        if ("long".equals(str)) {
            return 1;
        }
        return "full".equals(str) ? 0 : -1;
    }
}
