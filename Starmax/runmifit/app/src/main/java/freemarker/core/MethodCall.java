package freemarker.core;

import freemarker.core.Expression;
import freemarker.template.TemplateException;
import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.utility.NullWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

final class MethodCall extends Expression {
    private final ListLiteral arguments;
    private final Expression target;

    /* access modifiers changed from: package-private */
    public TemplateModel getConstantValue() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "...(...)";
    }

    /* access modifiers changed from: package-private */
    public boolean isLiteral() {
        return false;
    }

    MethodCall(Expression expression, ArrayList arrayList) {
        this(super, new ListLiteral(arrayList));
    }

    private MethodCall(Expression expression, ListLiteral listLiteral) {
        this.target = super;
        this.arguments = listLiteral;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.TemplateException.<init>(java.lang.String, java.lang.Exception, freemarker.core.Environment):void
     arg types: [java.lang.String, java.io.IOException, freemarker.core.Environment]
     candidates:
      freemarker.template.TemplateException.<init>(java.lang.String, java.lang.Throwable, freemarker.core.Environment):void
      freemarker.template.TemplateException.<init>(java.lang.String, java.lang.Exception, freemarker.core.Environment):void */
    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        List list;
        TemplateModel eval = this.target.eval(environment);
        if (eval instanceof TemplateMethodModel) {
            TemplateMethodModel templateMethodModel = (TemplateMethodModel) eval;
            if (templateMethodModel instanceof TemplateMethodModelEx) {
                list = this.arguments.getModelList(environment);
            } else {
                list = this.arguments.getValueList(environment);
            }
            return environment.getObjectWrapper().wrap(templateMethodModel.exec(list));
        } else if (eval instanceof Macro) {
            Macro macro = (Macro) eval;
            environment.setLastReturnValue(null);
            if (macro.isFunction()) {
                Writer out = environment.getOut();
                try {
                    environment.setOut(NullWriter.INSTANCE);
                    environment.invoke(macro, null, this.arguments.items, null, null);
                    environment.setOut(out);
                    return environment.getLastReturnValue();
                } catch (IOException e) {
                    throw new TemplateException("Unexpected exception during function execution", (Exception) e, environment);
                } catch (Throwable th) {
                    environment.setOut(out);
                    throw th;
                }
            } else {
                throw new _MiscTemplateException(environment, "A macro cannot be called in an expression. (Functions can be.)");
            }
        } else {
            throw new NonMethodException(this.target, eval, environment);
        }
    }

    public String getCanonicalForm() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.target.getCanonicalForm());
        stringBuffer.append("(");
        String canonicalForm = this.arguments.getCanonicalForm();
        stringBuffer.append(canonicalForm.substring(1, canonicalForm.length() - 1));
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public Expression deepCloneWithIdentifierReplaced_inner(String str, Expression expression, Expression.ReplacemenetState replacemenetState) {
        return new MethodCall(this.target.deepCloneWithIdentifierReplaced(str, super, replacemenetState), (ListLiteral) this.arguments.deepCloneWithIdentifierReplaced(str, super, replacemenetState));
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return this.arguments.items.size() + 1;
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.target;
        }
        if (i < getParameterCount()) {
            return this.arguments.items.get(i - 1);
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.CALLEE;
        }
        if (i < getParameterCount()) {
            return ParameterRole.ARGUMENT_VALUE;
        }
        throw new IndexOutOfBoundsException();
    }
}
