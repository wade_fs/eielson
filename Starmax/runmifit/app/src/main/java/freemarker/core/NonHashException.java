package freemarker.core;

import freemarker.template.TemplateModel;

public class NonHashException extends UnexpectedTypeException {
    private static final Class[] EXPECTED_TYPES;
    static /* synthetic */ Class class$freemarker$template$TemplateHashModel;

    static {
        Class[] clsArr = new Class[1];
        Class cls = class$freemarker$template$TemplateHashModel;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateHashModel");
            class$freemarker$template$TemplateHashModel = cls;
        }
        clsArr[0] = cls;
        EXPECTED_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonHashException(Environment environment) {
        super(environment, "Expecting hash value here");
    }

    public NonHashException(String str, Environment environment) {
        super(environment, str);
    }

    NonHashException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(environment, _errordescriptionbuilder);
    }

    NonHashException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "hash", EXPECTED_TYPES, environment);
    }

    NonHashException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "hash", EXPECTED_TYPES, str, environment);
    }

    NonHashException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "hash", EXPECTED_TYPES, strArr, environment);
    }
}
