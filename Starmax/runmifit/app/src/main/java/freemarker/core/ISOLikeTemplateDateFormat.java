package freemarker.core;

import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.DateUtil;
import freemarker.template.utility.StringUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

abstract class ISOLikeTemplateDateFormat extends TemplateDateFormat {
    private static final String XS_LESS_THAN_SECONDS_ACCURACY_ERROR_MESSAGE = "Less than seconds accuracy isn't allowed by the XML Schema format";
    protected final int accuracy;
    protected final int dateType;
    private final ISOLikeTemplateDateFormatFactory factory;
    protected final Boolean forceUTC;
    protected final Boolean showZoneOffset;
    protected final TimeZone timeZone;
    protected final boolean zonelessInput;

    /* access modifiers changed from: protected */
    public abstract String format(Date date, boolean z, boolean z2, boolean z3, int i, TimeZone timeZone2, DateUtil.DateToISO8601CalendarFactory dateToISO8601CalendarFactory);

    /* access modifiers changed from: protected */
    public abstract String getDateDescription();

    /* access modifiers changed from: protected */
    public abstract String getDateTimeDescription();

    /* access modifiers changed from: protected */
    public abstract String getTimeDescription();

    public final boolean isLocaleBound() {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract boolean isXSMode();

    /* access modifiers changed from: protected */
    public abstract Date parseDate(String str, TimeZone timeZone2, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException;

    /* access modifiers changed from: protected */
    public abstract Date parseDateTime(String str, TimeZone timeZone2, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException;

    /* access modifiers changed from: protected */
    public abstract Date parseTime(String str, TimeZone timeZone2, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException;

    public ISOLikeTemplateDateFormat(String str, int i, int i2, boolean z, TimeZone timeZone2, ISOLikeTemplateDateFormatFactory iSOLikeTemplateDateFormatFactory) throws ParseException, UnknownDateTypeFormattingUnsupportedException {
        Boolean bool;
        String str2 = str;
        int i3 = i2;
        this.factory = iSOLikeTemplateDateFormatFactory;
        if (i3 != 0) {
            this.dateType = i3;
            this.zonelessInput = z;
            int length = str.length();
            Boolean bool2 = Boolean.FALSE;
            int i4 = 7;
            Boolean bool3 = null;
            boolean z2 = false;
            int i5 = i;
            while (i5 < length) {
                int i6 = i5 + 1;
                char charAt = str2.charAt(i5);
                if (charAt == '_' || charAt == ' ') {
                    i5 = i6;
                    z2 = true;
                } else if (z2) {
                    if (charAt != 'f') {
                        if (!(charAt == 'h' || charAt == 's')) {
                            if (charAt == 'u') {
                                checkForceUTCNotSet(bool2, i6);
                                bool2 = null;
                                i5 = i6;
                                z2 = false;
                            } else if (charAt != 'm') {
                                if (charAt != 'n') {
                                    StringBuffer stringBuffer = new StringBuffer();
                                    stringBuffer.append("Unexpected character, ");
                                    stringBuffer.append(StringUtil.jQuote(String.valueOf(charAt)));
                                    stringBuffer.append(". Expected the beginning of one of: h, m, s, ms, nz, fz, u");
                                    throw new ParseException(stringBuffer.toString(), i6);
                                }
                            }
                        }
                        if (i4 == 7) {
                            if (charAt != 'h') {
                                if (charAt != 'm') {
                                    if (charAt == 's') {
                                        i4 = 6;
                                    }
                                } else if (i6 < length && str2.charAt(i6) == 's') {
                                    i6++;
                                    i4 = 8;
                                } else if (!isXSMode()) {
                                    i4 = 5;
                                } else {
                                    throw new ParseException(XS_LESS_THAN_SECONDS_ACCURACY_ERROR_MESSAGE, i6);
                                }
                            } else if (!isXSMode()) {
                                i4 = 4;
                            } else {
                                throw new ParseException(XS_LESS_THAN_SECONDS_ACCURACY_ERROR_MESSAGE, i6);
                            }
                            i5 = i6;
                            z2 = false;
                        } else {
                            StringBuffer stringBuffer2 = new StringBuffer();
                            stringBuffer2.append("Character \"");
                            stringBuffer2.append(charAt);
                            stringBuffer2.append("\" is unexpected as accuracy was already specified earlier.");
                            throw new ParseException(stringBuffer2.toString(), i6);
                        }
                    } else if (i6 < length && str2.charAt(i6) == 'u') {
                        checkForceUTCNotSet(bool2, i6);
                        i6++;
                        bool2 = Boolean.TRUE;
                        i5 = i6;
                        z2 = false;
                    }
                    if (bool3 == null) {
                        if (charAt != 'f') {
                            if (charAt == 'n') {
                                if (i6 >= length || str2.charAt(i6) != 'z') {
                                    throw new ParseException("\"n\" must be followed by \"z\"", i6);
                                }
                                i6++;
                                bool = Boolean.FALSE;
                            }
                            i5 = i6;
                            z2 = false;
                        } else if (i6 >= length || str2.charAt(i6) != 'z') {
                            throw new ParseException("\"f\" must be followed by \"z\"", i6);
                        } else {
                            i6++;
                            bool = Boolean.TRUE;
                        }
                        bool3 = bool;
                        i5 = i6;
                        z2 = false;
                    } else {
                        StringBuffer stringBuffer3 = new StringBuffer();
                        stringBuffer3.append("Character \"");
                        stringBuffer3.append(charAt);
                        stringBuffer3.append("\" is unexpected as zone offset visibility was already ");
                        stringBuffer3.append("specified earlier.");
                        throw new ParseException(stringBuffer3.toString(), i6);
                    }
                } else {
                    StringBuffer stringBuffer4 = new StringBuffer();
                    stringBuffer4.append("Missing space or \"_\" before \"");
                    stringBuffer4.append(charAt);
                    stringBuffer4.append("\"");
                    throw new ParseException(stringBuffer4.toString(), i6);
                }
            }
            this.accuracy = i4;
            this.showZoneOffset = bool3;
            this.forceUTC = bool2;
            this.timeZone = timeZone2;
            return;
        }
        throw new UnknownDateTypeFormattingUnsupportedException();
    }

    private void checkForceUTCNotSet(Boolean bool, int i) throws ParseException {
        if (bool != Boolean.FALSE) {
            throw new ParseException("The UTC usage option was already set earlier.", i);
        }
    }

    public final String format(TemplateDateModel templateDateModel) throws TemplateModelException {
        boolean z;
        Date asDate = templateDateModel.getAsDate();
        boolean z2 = this.dateType != 1;
        boolean z3 = this.dateType != 2;
        Boolean bool = this.showZoneOffset;
        if (bool == null) {
            z = !this.zonelessInput;
        } else {
            z = bool.booleanValue();
        }
        int i = this.accuracy;
        Boolean bool2 = this.forceUTC;
        return format(asDate, z2, z3, z, i, (bool2 != null ? !bool2.booleanValue() : this.zonelessInput) ? this.timeZone : DateUtil.UTC, this.factory.getISOBuiltInCalendar());
    }

    public final Date parse(String str) throws ParseException {
        DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateCalculator = this.factory.getCalendarFieldsToDateCalculator();
        TimeZone timeZone2 = this.forceUTC != Boolean.FALSE ? DateUtil.UTC : this.timeZone;
        int i = this.dateType;
        if (i == 2) {
            return parseDate(str, timeZone2, calendarFieldsToDateCalculator);
        }
        if (i == 1) {
            return parseTime(str, timeZone2, calendarFieldsToDateCalculator);
        }
        if (i == 3) {
            return parseDateTime(str, timeZone2, calendarFieldsToDateCalculator);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Unexpected date type: ");
        stringBuffer.append(this.dateType);
        throw new BugException(stringBuffer.toString());
    }

    public final String getDescription() {
        int i = this.dateType;
        if (i == 1) {
            return getTimeDescription();
        }
        if (i == 2) {
            return getDateDescription();
        }
        if (i != 3) {
            return "<error: wrong format dateType>";
        }
        return getDateTimeDescription();
    }
}
