package freemarker.core;

public class _DelayedJoinWithComma extends _DelayedConversionToString {
    public _DelayedJoinWithComma(String[] strArr) {
        super(strArr);
    }

    /* access modifiers changed from: protected */
    public String doConversion(Object obj) {
        String[] strArr = (String[]) obj;
        int i = 0;
        for (int i2 = 0; i2 < strArr.length; i2++) {
            if (i2 != 0) {
                i += 2;
            }
            i += strArr[i2].length();
        }
        StringBuffer stringBuffer = new StringBuffer(i);
        for (int i3 = 0; i3 < strArr.length; i3++) {
            if (i3 != 0) {
                stringBuffer.append(", ");
            }
            stringBuffer.append(strArr[i3]);
        }
        return stringBuffer.toString();
    }
}
