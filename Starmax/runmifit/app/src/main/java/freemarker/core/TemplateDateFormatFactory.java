package freemarker.core;

import freemarker.template.TemplateModelException;
import java.text.ParseException;
import java.util.TimeZone;

abstract class TemplateDateFormatFactory {
    private final TimeZone timeZone;

    public abstract TemplateDateFormat get(int i, boolean z, String str) throws ParseException, TemplateModelException, UnknownDateTypeFormattingUnsupportedException;

    public abstract boolean isLocaleBound();

    public TemplateDateFormatFactory(TimeZone timeZone2) {
        this.timeZone = timeZone2;
    }

    public TimeZone getTimeZone() {
        return this.timeZone;
    }
}
