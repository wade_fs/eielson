package freemarker.core;

public class _DelayedGetMessage extends _DelayedConversionToString {
    public _DelayedGetMessage(Throwable th) {
        super(th);
    }

    /* access modifiers changed from: protected */
    public String doConversion(Object obj) {
        String message = ((Throwable) obj).getMessage();
        return (message == null || message.length() == 0) ? "[No exception message]" : message;
    }
}
