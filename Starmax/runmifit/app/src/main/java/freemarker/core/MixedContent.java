package freemarker.core;

import freemarker.template.TemplateException;
import java.io.IOException;

final class MixedContent extends TemplateElement {
    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#mixed_content";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isShownInStackTrace() {
        return false;
    }

    MixedContent() {
    }

    /* access modifiers changed from: package-private */
    public void addElement(TemplateElement templateElement) {
        addRegulatedChild(super);
    }

    /* access modifiers changed from: package-private */
    public void addElement(int i, TemplateElement templateElement) {
        addRegulatedChild(i, super);
    }

    /* access modifiers changed from: package-private */
    public TemplateElement postParseCleanup(boolean z) throws ParseException {
        super.postParseCleanup(z);
        return getRegulatedChildCount() == 1 ? getRegulatedChild(0) : super;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        int regulatedChildCount = getRegulatedChildCount();
        for (int i = 0; i < regulatedChildCount; i++) {
            environment.visit(getRegulatedChild(i));
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        if (z) {
            StringBuffer stringBuffer = new StringBuffer();
            int regulatedChildCount = getRegulatedChildCount();
            for (int i = 0; i < regulatedChildCount; i++) {
                stringBuffer.append(getRegulatedChild(i).getCanonicalForm());
            }
            return stringBuffer.toString();
        } else if (getParentElement() == null) {
            return "root";
        } else {
            return getNodeTypeSymbol();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isOutputCacheable() {
        int regulatedChildCount = getRegulatedChildCount();
        for (int i = 0; i < regulatedChildCount; i++) {
            if (!getRegulatedChild(i).isOutputCacheable()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public boolean isIgnorable() {
        return getRegulatedChildCount() == 0;
    }
}
