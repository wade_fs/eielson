package freemarker.core;

import freemarker.template.TemplateException;
import java.io.IOException;

final class ConditionalBlock extends TemplateElement {
    static final int TYPE_ELSE = 1;
    static final int TYPE_ELSE_IF = 2;
    static final int TYPE_IF = 0;
    final Expression condition;
    boolean isLonelyIf;
    private final int type;

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    ConditionalBlock(Expression expression, TemplateElement templateElement, int i) {
        this.condition = expression;
        setNestedBlock(super);
        this.type = i;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        Expression expression = this.condition;
        if ((expression == null || expression.evalToBoolean(environment)) && getNestedBlock() != null) {
            environment.visitByHiddingParent(getNestedBlock());
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        if (this.condition != null) {
            stringBuffer.append(' ');
            stringBuffer.append(this.condition.getCanonicalForm());
        }
        if (z) {
            stringBuffer.append(">");
            if (getNestedBlock() != null) {
                stringBuffer.append(getNestedBlock().getCanonicalForm());
            }
            if (this.isLonelyIf) {
                stringBuffer.append("</#if>");
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        int i = this.type;
        if (i == 1) {
            return "#else";
        }
        if (i == 0) {
            return "#if";
        }
        if (i == 2) {
            return "#elseif";
        }
        throw new BugException("Unknown type");
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.condition;
        }
        if (i == 1) {
            return new Integer(this.type);
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.CONDITION;
        }
        if (i == 1) {
            return ParameterRole.AST_NODE_SUBTYPE;
        }
        throw new IndexOutOfBoundsException();
    }
}
