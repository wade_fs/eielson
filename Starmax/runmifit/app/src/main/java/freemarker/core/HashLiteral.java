package freemarker.core;

import freemarker.core.Expression;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateModel;
import freemarker.template._TemplateAPI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import org.apache.commons.math3.geometry.VectorFormat;

final class HashLiteral extends Expression {
    /* access modifiers changed from: private */
    public final ArrayList keys;
    /* access modifiers changed from: private */
    public final int size;
    /* access modifiers changed from: private */
    public final ArrayList values;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "{...}";
    }

    HashLiteral(ArrayList arrayList, ArrayList arrayList2) {
        this.keys = arrayList;
        this.values = arrayList2;
        this.size = arrayList.size();
        arrayList.trimToSize();
        arrayList2.trimToSize();
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        return new SequenceHash(environment);
    }

    public String getCanonicalForm() {
        StringBuffer stringBuffer = new StringBuffer(VectorFormat.DEFAULT_PREFIX);
        for (int i = 0; i < this.size; i++) {
            stringBuffer.append(((Expression) this.keys.get(i)).getCanonicalForm());
            stringBuffer.append(": ");
            stringBuffer.append(((Expression) this.values.get(i)).getCanonicalForm());
            if (i != this.size - 1) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append(VectorFormat.DEFAULT_SUFFIX);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public boolean isLiteral() {
        if (this.constantValue != null) {
            return true;
        }
        for (int i = 0; i < this.size; i++) {
            Expression expression = (Expression) this.values.get(i);
            if (!((Expression) this.keys.get(i)).isLiteral() || !super.isLiteral()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public Expression deepCloneWithIdentifierReplaced_inner(String str, Expression expression, Expression.ReplacemenetState replacemenetState) {
        ArrayList arrayList = (ArrayList) this.keys.clone();
        ListIterator listIterator = arrayList.listIterator();
        while (listIterator.hasNext()) {
            listIterator.set(((Expression) listIterator.next()).deepCloneWithIdentifierReplaced(str, super, replacemenetState));
        }
        ArrayList arrayList2 = (ArrayList) this.values.clone();
        ListIterator listIterator2 = arrayList2.listIterator();
        while (listIterator2.hasNext()) {
            listIterator2.set(((Expression) listIterator2.next()).deepCloneWithIdentifierReplaced(str, super, replacemenetState));
        }
        return new HashLiteral(arrayList, arrayList2);
    }

    private class SequenceHash implements TemplateHashModelEx {
        private TemplateCollectionModel keyCollection;
        private HashMap map;
        private TemplateCollectionModel valueCollection;

        SequenceHash(Environment environment) throws TemplateException {
            int i = 0;
            if (_TemplateAPI.getTemplateLanguageVersionAsInt(HashLiteral.this) >= _TemplateAPI.VERSION_INT_2_3_21) {
                this.map = new LinkedHashMap();
                while (i < HashLiteral.this.size) {
                    Expression expression = (Expression) HashLiteral.this.values.get(i);
                    String evalAndCoerceToString = ((Expression) HashLiteral.this.keys.get(i)).evalAndCoerceToString(environment);
                    TemplateModel eval = expression.eval(environment);
                    if (environment == null || !environment.isClassicCompatible()) {
                        expression.assertNonNull(eval, environment);
                    }
                    this.map.put(evalAndCoerceToString, eval);
                    i++;
                }
                return;
            }
            this.map = new HashMap();
            ArrayList arrayList = new ArrayList(HashLiteral.this.size);
            ArrayList arrayList2 = new ArrayList(HashLiteral.this.size);
            while (i < HashLiteral.this.size) {
                Expression expression2 = (Expression) HashLiteral.this.values.get(i);
                String evalAndCoerceToString2 = ((Expression) HashLiteral.this.keys.get(i)).evalAndCoerceToString(environment);
                TemplateModel eval2 = expression2.eval(environment);
                if (environment == null || !environment.isClassicCompatible()) {
                    expression2.assertNonNull(eval2, environment);
                }
                this.map.put(evalAndCoerceToString2, eval2);
                arrayList.add(evalAndCoerceToString2);
                arrayList2.add(eval2);
                i++;
            }
            this.keyCollection = new CollectionAndSequence(new SimpleSequence(arrayList));
            this.valueCollection = new CollectionAndSequence(new SimpleSequence(arrayList2));
        }

        public int size() {
            return HashLiteral.this.size;
        }

        public TemplateCollectionModel keys() {
            if (this.keyCollection == null) {
                this.keyCollection = new CollectionAndSequence(new SimpleSequence(this.map.keySet()));
            }
            return this.keyCollection;
        }

        public TemplateCollectionModel values() {
            if (this.valueCollection == null) {
                this.valueCollection = new CollectionAndSequence(new SimpleSequence(this.map.values()));
            }
            return this.valueCollection;
        }

        public TemplateModel get(String str) {
            return (TemplateModel) this.map.get(str);
        }

        public boolean isEmpty() {
            return HashLiteral.this.size == 0;
        }

        public String toString() {
            return HashLiteral.this.getCanonicalForm();
        }
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return this.size * 2;
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        checkIndex(i);
        return (i % 2 == 0 ? this.keys : this.values).get(i / 2);
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        checkIndex(i);
        return i % 2 == 0 ? ParameterRole.ITEM_KEY : ParameterRole.ITEM_VALUE;
    }

    private void checkIndex(int i) {
        if (i >= this.size * 2) {
            throw new IndexOutOfBoundsException();
        }
    }
}
