package freemarker.core;

final class TrimInstruction extends TemplateElement {
    private final int TYPE_LT = 1;
    private final int TYPE_NT = 3;
    private final int TYPE_RT = 2;
    private final int TYPE_T = 0;
    final boolean left;
    final boolean right;

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) {
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public boolean isIgnorable() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isOutputCacheable() {
        return true;
    }

    TrimInstruction(boolean z, boolean z2) {
        this.left = z;
        this.right = z2;
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        if (z) {
            stringBuffer.append("/>");
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        if (this.left && this.right) {
            return "#t";
        }
        if (this.left) {
            return "#lt";
        }
        return this.right ? "#rt" : "#nt";
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        int i2;
        if (i == 0) {
            if (this.left && this.right) {
                i2 = 0;
            } else if (this.left) {
                i2 = 1;
            } else {
                i2 = this.right ? 2 : 3;
            }
            return new Integer(i2);
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.AST_NODE_SUBTYPE;
        }
        throw new IndexOutOfBoundsException();
    }
}
