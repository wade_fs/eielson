package freemarker.core;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class _ArrayIterator implements Iterator {
    private final Object[] array;
    private int nextIndex = 0;

    public _ArrayIterator(Object[] objArr) {
        this.array = objArr;
    }

    public boolean hasNext() {
        return this.nextIndex < this.array.length;
    }

    public Object next() {
        int i = this.nextIndex;
        Object[] objArr = this.array;
        if (i < objArr.length) {
            this.nextIndex = i + 1;
            return objArr[i];
        }
        throw new NoSuchElementException();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
