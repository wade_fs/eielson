package freemarker.core;

import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

abstract class BuiltInForNumber extends BuiltIn {
    /* access modifiers changed from: package-private */
    public abstract TemplateModel calculateResult(Number number, TemplateModel templateModel) throws TemplateModelException;

    BuiltInForNumber() {
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        TemplateModel eval = this.target.eval(environment);
        return calculateResult(this.target.modelToNumber(eval, environment), eval);
    }
}
