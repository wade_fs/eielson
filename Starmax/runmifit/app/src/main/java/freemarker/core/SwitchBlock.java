package freemarker.core;

import freemarker.core.BreakInstruction;
import freemarker.template.TemplateException;
import java.io.IOException;

final class SwitchBlock extends TemplateElement {
    private Case defaultCase;
    private final Expression searched;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#switch";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    SwitchBlock(Expression expression) {
        this.searched = expression;
        setRegulatedChildBufferCapacity(4);
    }

    /* access modifiers changed from: package-private */
    public void addCase(Case caseR) {
        if (caseR.condition == null) {
            this.defaultCase = caseR;
        }
        addRegulatedChild(super);
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        boolean z;
        int regulatedChildCount = getRegulatedChildCount();
        int i = 0;
        boolean z2 = false;
        while (i < regulatedChildCount) {
            try {
                Case caseR = (Case) getRegulatedChild(i);
                if (z2) {
                    z = true;
                } else {
                    z = caseR.condition != null ? EvalUtil.compare(this.searched, 1, "case==", caseR.condition, caseR.condition, environment) : false;
                }
                if (z) {
                    environment.visitByHiddingParent(super);
                    z2 = true;
                }
                i++;
            } catch (BreakInstruction.Break unused) {
                return;
            }
        }
        if (!z2 && this.defaultCase != null) {
            environment.visitByHiddingParent(this.defaultCase);
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(' ');
        stringBuffer.append(this.searched.getCanonicalForm());
        if (z) {
            stringBuffer.append('>');
            int regulatedChildCount = getRegulatedChildCount();
            for (int i = 0; i < regulatedChildCount; i++) {
                stringBuffer.append(((Case) getRegulatedChild(i)).getCanonicalForm());
            }
            stringBuffer.append("</");
            stringBuffer.append(getNodeTypeSymbol());
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.searched;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.VALUE;
        }
        throw new IndexOutOfBoundsException();
    }
}
