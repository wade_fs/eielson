package freemarker.core;

abstract class RightUnboundedRangeModel extends RangeModel {
    /* access modifiers changed from: package-private */
    public final int getStep() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public final boolean isAffactedByStringSlicingBug() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean isRightAdaptive() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean isRightUnbounded() {
        return true;
    }

    RightUnboundedRangeModel(int i) {
        super(i);
    }
}
