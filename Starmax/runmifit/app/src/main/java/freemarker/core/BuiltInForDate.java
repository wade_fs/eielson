package freemarker.core;

import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import java.util.Date;

abstract class BuiltInForDate extends BuiltIn {
    /* access modifiers changed from: protected */
    public abstract TemplateModel calculateResult(Date date, int i, Environment environment) throws TemplateException;

    BuiltInForDate() {
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        TemplateModel eval = this.target.eval(environment);
        if (eval instanceof TemplateDateModel) {
            TemplateDateModel templateDateModel = (TemplateDateModel) eval;
            return calculateResult(EvalUtil.modelToDate(templateDateModel, this.target), templateDateModel.getDateType(), environment);
        }
        throw newNonDateException(environment, eval, this.target);
    }

    static TemplateException newNonDateException(Environment environment, TemplateModel templateModel, Expression expression) throws InvalidReferenceException {
        if (templateModel == null) {
            return InvalidReferenceException.getInstance(expression, environment);
        }
        return new NonDateException(expression, templateModel, "date", environment);
    }
}
