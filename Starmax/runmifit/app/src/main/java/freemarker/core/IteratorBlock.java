package freemarker.core;

import freemarker.core.BreakInstruction;
import freemarker.template.SimpleNumber;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.utility.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

final class IteratorBlock extends TemplateElement {
    private final boolean isForEach;
    /* access modifiers changed from: private */
    public final Expression listExp;
    private final String loopVarName;

    IteratorBlock(Expression expression, String str, TemplateElement templateElement, boolean z) {
        this.listExp = expression;
        this.loopVarName = str;
        setNestedBlock(super);
        this.isForEach = z;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        acceptWithResult(environment);
    }

    /* access modifiers changed from: package-private */
    public boolean acceptWithResult(Environment environment) throws TemplateException, IOException {
        TemplateModel eval = this.listExp.eval(environment);
        if (eval == null) {
            if (environment.isClassicCompatible()) {
                eval = Constants.EMPTY_SEQUENCE;
            } else {
                this.listExp.assertNonNull(null, environment);
            }
        }
        return environment.visitIteratorBlock(new IterationContext(eval, this.loopVarName));
    }

    static IterationContext findEnclosingIterationContext(Environment environment, String str) throws _MiscTemplateException {
        ArrayList localContextStack = environment.getLocalContextStack();
        if (localContextStack == null) {
            return null;
        }
        for (int size = localContextStack.size() - 1; size >= 0; size--) {
            Object obj = localContextStack.get(size);
            if ((obj instanceof IterationContext) && (str == null || str.equals(((IterationContext) obj).getLoopVariableName()))) {
                return (IterationContext) obj;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(' ');
        if (this.isForEach) {
            stringBuffer.append(_CoreStringUtils.toFTLTopLevelIdentifierReference(this.loopVarName));
            stringBuffer.append(" in ");
            stringBuffer.append(this.listExp.getCanonicalForm());
        } else {
            stringBuffer.append(this.listExp.getCanonicalForm());
            if (this.loopVarName != null) {
                stringBuffer.append(" as ");
                stringBuffer.append(_CoreStringUtils.toFTLTopLevelIdentifierReference(this.loopVarName));
            }
        }
        if (z) {
            stringBuffer.append(">");
            if (getNestedBlock() != null) {
                stringBuffer.append(getNestedBlock().getCanonicalForm());
            }
            if (!(getParentElement() instanceof ListElseContainer)) {
                stringBuffer.append("</");
                stringBuffer.append(getNodeTypeSymbol());
                stringBuffer.append('>');
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return this.loopVarName != null ? 2 : 1;
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.listExp;
        }
        if (i == 1) {
            String str = this.loopVarName;
            if (str != null) {
                return str;
            }
            throw new IndexOutOfBoundsException();
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.LIST_SOURCE;
        }
        if (i != 1) {
            throw new IndexOutOfBoundsException();
        } else if (this.loopVarName != null) {
            return ParameterRole.TARGET_LOOP_VARIABLE;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return this.isForEach ? "#foreach" : "#list";
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return this.loopVarName != null;
    }

    class IterationContext implements LocalContext {
        private static final String LOOP_STATE_HAS_NEXT = "_has_next";
        private static final String LOOP_STATE_INDEX = "_index";
        private boolean alreadyEntered;
        private boolean hasNext;
        private int index;
        private final TemplateModel listValue;
        private Collection localVarNames = null;
        private TemplateModel loopVar;
        private String loopVarName;
        private TemplateModelIterator openedIteratorModel;

        public IterationContext(TemplateModel templateModel, String str) {
            this.listValue = templateModel;
            this.loopVarName = str;
        }

        /* access modifiers changed from: package-private */
        public boolean accept(Environment environment) throws TemplateException, IOException {
            return executeNestedBlock(environment, IteratorBlock.this.getNestedBlock());
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: package-private */
        public void loopForItemsElement(Environment environment, TemplateElement templateElement, String str) throws NonSequenceOrCollectionException, TemplateModelException, InvalidReferenceException, TemplateException, IOException {
            try {
                if (!this.alreadyEntered) {
                    this.alreadyEntered = true;
                    this.loopVarName = str;
                    executeNestedBlock(environment, templateElement);
                    this.loopVarName = null;
                    return;
                }
                throw new _MiscTemplateException(environment, "The #items directive was already entered earlier for this listing.");
            } catch (Throwable th) {
                this.loopVarName = null;
                throw th;
            }
        }

        private boolean executeNestedBlock(Environment environment, TemplateElement templateElement) throws TemplateModelException, TemplateException, IOException, NonSequenceOrCollectionException, InvalidReferenceException {
            return executeNestedBlockInner(environment, templateElement);
        }

        private boolean executeNestedBlockInner(Environment environment, TemplateElement templateElement) throws TemplateModelException, TemplateException, IOException, NonSequenceOrCollectionException, InvalidReferenceException {
            TemplateModel templateModel = this.listValue;
            if (templateModel instanceof TemplateCollectionModel) {
                TemplateCollectionModel templateCollectionModel = (TemplateCollectionModel) templateModel;
                TemplateModelIterator templateModelIterator = this.openedIteratorModel;
                if (templateModelIterator == null) {
                    templateModelIterator = templateCollectionModel.iterator();
                }
                this.hasNext = templateModelIterator.hasNext();
                boolean z = this.hasNext;
                if (!z) {
                    return z;
                }
                if (this.loopVarName != null) {
                    while (this.hasNext) {
                        try {
                            this.loopVar = templateModelIterator.next();
                            this.hasNext = templateModelIterator.hasNext();
                            if (templateElement != null) {
                                environment.visitByHiddingParent(templateElement);
                            }
                            this.index++;
                        } catch (BreakInstruction.Break unused) {
                        }
                    }
                    this.openedIteratorModel = null;
                    return z;
                }
                this.openedIteratorModel = templateModelIterator;
                if (templateElement == null) {
                    return z;
                }
                environment.visitByHiddingParent(templateElement);
                return z;
            } else if (templateModel instanceof TemplateSequenceModel) {
                TemplateSequenceModel templateSequenceModel = (TemplateSequenceModel) templateModel;
                int size = templateSequenceModel.size();
                boolean z2 = size != 0;
                if (z2) {
                    if (this.loopVarName != null) {
                        try {
                            this.index = 0;
                            while (this.index < size) {
                                this.loopVar = templateSequenceModel.get(this.index);
                                this.hasNext = size > this.index + 1;
                                if (templateElement != null) {
                                    environment.visitByHiddingParent(templateElement);
                                }
                                this.index++;
                            }
                        } catch (BreakInstruction.Break unused2) {
                        }
                    } else if (templateElement != null) {
                        environment.visitByHiddingParent(templateElement);
                    }
                }
                return z2;
            } else if (environment.isClassicCompatible()) {
                if (this.loopVarName != null) {
                    this.loopVar = this.listValue;
                    this.hasNext = false;
                }
                if (templateElement != null) {
                    try {
                        environment.visitByHiddingParent(templateElement);
                    } catch (BreakInstruction.Break unused3) {
                    }
                }
                return true;
            } else {
                throw new NonSequenceOrCollectionException(IteratorBlock.this.listExp, this.listValue, environment);
            }
        }

        /* access modifiers changed from: package-private */
        public String getLoopVariableName() {
            return this.loopVarName;
        }

        public TemplateModel getLocalVariable(String str) {
            String str2 = this.loopVarName;
            if (str2 == null || !str.startsWith(str2)) {
                return null;
            }
            int length = str.length() - str2.length();
            if (length == 0) {
                return this.loopVar;
            }
            if (length != 6) {
                if (length == 9 && str.endsWith(LOOP_STATE_HAS_NEXT)) {
                    return this.hasNext ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
                }
                return null;
            } else if (str.endsWith(LOOP_STATE_INDEX)) {
                return new SimpleNumber(this.index);
            } else {
                return null;
            }
        }

        public Collection getLocalVariableNames() {
            String str = this.loopVarName;
            if (str == null) {
                return Collections.EMPTY_LIST;
            }
            if (this.localVarNames == null) {
                this.localVarNames = new ArrayList(3);
                this.localVarNames.add(str);
                Collection collection = this.localVarNames;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(str);
                stringBuffer.append(LOOP_STATE_INDEX);
                collection.add(stringBuffer.toString());
                Collection collection2 = this.localVarNames;
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append(str);
                stringBuffer2.append(LOOP_STATE_HAS_NEXT);
                collection2.add(stringBuffer2.toString());
            }
            return this.localVarNames;
        }

        /* access modifiers changed from: package-private */
        public boolean hasNext() {
            return this.hasNext;
        }

        /* access modifiers changed from: package-private */
        public int getIndex() {
            return this.index;
        }
    }
}
