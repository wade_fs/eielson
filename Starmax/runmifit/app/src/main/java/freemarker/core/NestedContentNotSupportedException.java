package freemarker.core;

import freemarker.core.Environment;
import freemarker.core.ThreadInterruptionSupportTemplatePostProcessor;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;

class NestedContentNotSupportedException extends TemplateException {
    public static void check(TemplateDirectiveBody templateDirectiveBody) throws NestedContentNotSupportedException {
        TemplateElement element;
        if (templateDirectiveBody != null) {
            if (!(templateDirectiveBody instanceof Environment.NestedElementTemplateDirectiveBody) || ((element = ((Environment.NestedElementTemplateDirectiveBody) templateDirectiveBody).getElement()) != null && !(element instanceof ThreadInterruptionSupportTemplatePostProcessor.ThreadInterruptionCheck))) {
                throw new NestedContentNotSupportedException(Environment.getCurrentEnvironment());
            }
        }
    }

    private NestedContentNotSupportedException(Environment environment) {
        this(null, null, environment);
    }

    private NestedContentNotSupportedException(Exception exc, Environment environment) {
        this(null, exc, environment);
    }

    private NestedContentNotSupportedException(String str, Environment environment) {
        this(str, null, environment);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private NestedContentNotSupportedException(java.lang.String r4, java.lang.Exception r5, freemarker.core.Environment r6) {
        /*
            r3 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "Nested content (body) not supported."
            r0.append(r1)
            if (r4 == 0) goto L_0x0022
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = " "
            r1.append(r2)
            java.lang.String r4 = freemarker.template.utility.StringUtil.jQuote(r4)
            r1.append(r4)
            java.lang.String r4 = r1.toString()
            goto L_0x0024
        L_0x0022:
            java.lang.String r4 = ""
        L_0x0024:
            r0.append(r4)
            java.lang.String r4 = r0.toString()
            r3.<init>(r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.NestedContentNotSupportedException.<init>(java.lang.String, java.lang.Exception, freemarker.core.Environment):void");
    }
}
