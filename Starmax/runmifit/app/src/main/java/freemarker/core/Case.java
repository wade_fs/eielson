package freemarker.core;

import freemarker.template.TemplateException;
import java.io.IOException;

final class Case extends TemplateElement {
    final int TYPE_CASE = 0;
    final int TYPE_DEFAULT = 1;
    Expression condition;

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    Case(Expression expression, TemplateElement templateElement) {
        this.condition = expression;
        setNestedBlock(super);
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        if (getNestedBlock() != null) {
            environment.visitByHiddingParent(getNestedBlock());
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        if (this.condition != null) {
            stringBuffer.append(' ');
            stringBuffer.append(this.condition.getCanonicalForm());
        }
        if (z) {
            stringBuffer.append('>');
            if (getNestedBlock() != null) {
                stringBuffer.append(getNestedBlock().getCanonicalForm());
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return this.condition != null ? "#case" : "#default";
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.condition;
        }
        int i2 = 1;
        if (i == 1) {
            if (this.condition != null) {
                i2 = 0;
            }
            return new Integer(i2);
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.CONDITION;
        }
        if (i == 1) {
            return ParameterRole.AST_NODE_SUBTYPE;
        }
        throw new IndexOutOfBoundsException();
    }
}
