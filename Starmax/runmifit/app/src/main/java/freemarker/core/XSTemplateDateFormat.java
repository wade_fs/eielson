package freemarker.core;

import freemarker.template.utility.DateUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

final class XSTemplateDateFormat extends ISOLikeTemplateDateFormat {
    /* access modifiers changed from: protected */
    public String getDateDescription() {
        return "W3C XML Schema date";
    }

    /* access modifiers changed from: protected */
    public String getDateTimeDescription() {
        return "W3C XML Schema dateTime";
    }

    /* access modifiers changed from: protected */
    public String getTimeDescription() {
        return "W3C XML Schema time";
    }

    /* access modifiers changed from: protected */
    public boolean isXSMode() {
        return true;
    }

    XSTemplateDateFormat(String str, int i, int i2, boolean z, TimeZone timeZone, ISOLikeTemplateDateFormatFactory iSOLikeTemplateDateFormatFactory) throws ParseException, UnknownDateTypeFormattingUnsupportedException {
        super(str, i, i2, z, timeZone, iSOLikeTemplateDateFormatFactory);
    }

    /* access modifiers changed from: protected */
    public String format(Date date, boolean z, boolean z2, boolean z3, int i, TimeZone timeZone, DateUtil.DateToISO8601CalendarFactory dateToISO8601CalendarFactory) {
        return DateUtil.dateToXSString(date, z, z2, z3, i, timeZone, dateToISO8601CalendarFactory);
    }

    /* access modifiers changed from: protected */
    public Date parseDate(String str, TimeZone timeZone, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException {
        return DateUtil.parseXSDate(str, timeZone, calendarFieldsToDateConverter);
    }

    /* access modifiers changed from: protected */
    public Date parseTime(String str, TimeZone timeZone, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException {
        return DateUtil.parseXSTime(str, timeZone, calendarFieldsToDateConverter);
    }

    /* access modifiers changed from: protected */
    public Date parseDateTime(String str, TimeZone timeZone, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException {
        return DateUtil.parseXSDateTime(str, timeZone, calendarFieldsToDateConverter);
    }
}
