package freemarker.core;

import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNodeModel;

abstract class BuiltInForNode extends BuiltIn {
    /* access modifiers changed from: package-private */
    public abstract TemplateModel calculateResult(TemplateNodeModel templateNodeModel, Environment environment) throws TemplateModelException;

    BuiltInForNode() {
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        TemplateModel eval = this.target.eval(environment);
        if (eval instanceof TemplateNodeModel) {
            return calculateResult((TemplateNodeModel) eval, environment);
        }
        throw new NonNodeException(this.target, eval, environment);
    }
}
