package freemarker.core;

import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import freemarker.template.utility.StringUtil;

public final class _CoreStringUtils {
    public static boolean isUpperUSASCII(char c) {
        return c >= 'A' && c <= 'Z';
    }

    private _CoreStringUtils() {
    }

    public static String toFTLIdentifierReferenceAfterDot(String str) {
        return backslashEscapeIdentifier(str);
    }

    public static String toFTLTopLevelIdentifierReference(String str) {
        return backslashEscapeIdentifier(str);
    }

    public static String toFTLTopLevelTragetIdentifier(String str) {
        int i = 0;
        char c = 0;
        while (true) {
            if (i >= str.length()) {
                break;
            }
            char charAt = str.charAt(i);
            if (i == 0) {
                if (StringUtil.isFTLIdentifierStart(charAt)) {
                    continue;
                    i++;
                }
            } else if (StringUtil.isFTLIdentifierPart(charAt)) {
                continue;
                i++;
            }
            if (charAt == '@') {
                continue;
            } else if ((c == 0 || c == '\\') && (charAt == '-' || charAt == '.' || charAt == ':')) {
                c = '\\';
            }
            i++;
        }
        c = '\"';
        if (c == 0) {
            return str;
        }
        if (c == '\"') {
            return StringUtil.ftlQuote(str);
        }
        if (c == '\\') {
            return backslashEscapeIdentifier(str);
        }
        throw new BugException();
    }

    private static String backslashEscapeIdentifier(String str) {
        return StringUtil.replace(StringUtil.replace(StringUtil.replace(str, "-", "\\-"), FileUtil.HIDDEN_PREFIX, "\\."), Config.TRACE_TODAY_VISIT_SPLIT, "\\:");
    }

    public static int getIdentifierNamingConvention(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '_') {
                return 11;
            }
            if (isUpperUSASCII(charAt)) {
                return 12;
            }
        }
        return 10;
    }

    public static String camelCaseToUnderscored(String str) {
        int i = 0;
        while (i < str.length() && Character.isLowerCase(str.charAt(i))) {
            i++;
        }
        if (i == str.length()) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str.substring(0, i));
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (isUpperUSASCII(charAt)) {
                stringBuffer.append('_');
                stringBuffer.append(Character.toLowerCase(charAt));
            } else {
                stringBuffer.append(charAt);
            }
            i++;
        }
        return stringBuffer.toString();
    }
}
