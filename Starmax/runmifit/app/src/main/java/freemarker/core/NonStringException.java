package freemarker.core;

import freemarker.template.TemplateModel;

public class NonStringException extends UnexpectedTypeException {
    private static final String DEFAULT_DESCRIPTION = "Expecting string or something automatically convertible to string (number, date or boolean) value here";
    static final Class[] STRING_COERCABLE_TYPES;
    static final String STRING_COERCABLE_TYPES_DESC = "string or something automatically convertible to string (number, date or boolean)";
    static /* synthetic */ Class class$freemarker$template$TemplateBooleanModel;
    static /* synthetic */ Class class$freemarker$template$TemplateDateModel;
    static /* synthetic */ Class class$freemarker$template$TemplateNumberModel;
    static /* synthetic */ Class class$freemarker$template$TemplateScalarModel;

    static {
        Class[] clsArr = new Class[4];
        Class cls = class$freemarker$template$TemplateScalarModel;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateScalarModel");
            class$freemarker$template$TemplateScalarModel = cls;
        }
        clsArr[0] = cls;
        Class cls2 = class$freemarker$template$TemplateNumberModel;
        if (cls2 == null) {
            cls2 = class$("freemarker.template.TemplateNumberModel");
            class$freemarker$template$TemplateNumberModel = cls2;
        }
        clsArr[1] = cls2;
        Class cls3 = class$freemarker$template$TemplateDateModel;
        if (cls3 == null) {
            cls3 = class$("freemarker.template.TemplateDateModel");
            class$freemarker$template$TemplateDateModel = cls3;
        }
        clsArr[2] = cls3;
        Class cls4 = class$freemarker$template$TemplateBooleanModel;
        if (cls4 == null) {
            cls4 = class$("freemarker.template.TemplateBooleanModel");
            class$freemarker$template$TemplateBooleanModel = cls4;
        }
        clsArr[3] = cls4;
        STRING_COERCABLE_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonStringException(Environment environment) {
        super(environment, DEFAULT_DESCRIPTION);
    }

    public NonStringException(String str, Environment environment) {
        super(environment, str);
    }

    NonStringException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(environment, _errordescriptionbuilder);
    }

    NonStringException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, STRING_COERCABLE_TYPES_DESC, STRING_COERCABLE_TYPES, environment);
    }

    NonStringException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, STRING_COERCABLE_TYPES_DESC, STRING_COERCABLE_TYPES, str, environment);
    }

    NonStringException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, STRING_COERCABLE_TYPES_DESC, STRING_COERCABLE_TYPES, strArr, environment);
    }
}
