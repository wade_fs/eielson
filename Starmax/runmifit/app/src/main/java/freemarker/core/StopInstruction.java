package freemarker.core;

import freemarker.template.TemplateException;

final class StopInstruction extends TemplateElement {
    private Expression exp;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#stop";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    StopInstruction(Expression expression) {
        this.exp = expression;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException {
        Expression expression = this.exp;
        if (expression == null) {
            throw new StopException(environment);
        }
        throw new StopException(environment, expression.evalAndCoerceToString(environment));
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        if (this.exp != null) {
            stringBuffer.append(' ');
            stringBuffer.append(this.exp.getCanonicalForm());
        }
        if (z) {
            stringBuffer.append("/>");
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.exp;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.MESSAGE;
        }
        throw new IndexOutOfBoundsException();
    }
}
