package freemarker.core;

import freemarker.core.Expression;
import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import freemarker.template.TemplateSequenceModel;
import java.util.HashSet;
import java.util.Set;

final class AddConcatExpression extends Expression {
    private final Expression left;
    private final Expression right;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "+";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    AddConcatExpression(Expression expression, Expression expression2) {
        this.left = super;
        this.right = super;
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        Expression expression = this.left;
        TemplateModel eval = super.eval(environment);
        Expression expression2 = this.right;
        return _eval(environment, this, super, eval, super, super.eval(environment));
    }

    static TemplateModel _eval(Environment environment, TemplateObject templateObject, Expression expression, TemplateModel templateModel, Expression expression2, TemplateModel templateModel2) throws TemplateModelException, TemplateException, NonStringException {
        if ((templateModel instanceof TemplateNumberModel) && (templateModel2 instanceof TemplateNumberModel)) {
            return _evalOnNumbers(environment, templateObject, EvalUtil.modelToNumber((TemplateNumberModel) templateModel, super), EvalUtil.modelToNumber((TemplateNumberModel) templateModel2, super));
        }
        if ((templateModel instanceof TemplateSequenceModel) && (templateModel2 instanceof TemplateSequenceModel)) {
            return new ConcatenatedSequence((TemplateSequenceModel) templateModel, (TemplateSequenceModel) templateModel2);
        }
        try {
            String coerceModelToString = Expression.coerceModelToString(templateModel, super, environment);
            if (coerceModelToString == null) {
                coerceModelToString = "null";
            }
            String coerceModelToString2 = Expression.coerceModelToString(templateModel2, super, environment);
            if (coerceModelToString2 == null) {
                coerceModelToString2 = "null";
            }
            return new SimpleScalar(coerceModelToString.concat(coerceModelToString2));
        } catch (NonStringException e) {
            if (!(templateModel instanceof TemplateHashModel) || !(templateModel2 instanceof TemplateHashModel)) {
                throw e;
            } else if (!(templateModel instanceof TemplateHashModelEx) || !(templateModel2 instanceof TemplateHashModelEx)) {
                return new ConcatenatedHash((TemplateHashModel) templateModel, (TemplateHashModel) templateModel2);
            } else {
                TemplateHashModelEx templateHashModelEx = (TemplateHashModelEx) templateModel;
                TemplateHashModelEx templateHashModelEx2 = (TemplateHashModelEx) templateModel2;
                if (templateHashModelEx.size() == 0) {
                    return templateHashModelEx2;
                }
                if (templateHashModelEx2.size() == 0) {
                    return templateHashModelEx;
                }
                return new ConcatenatedHashEx(templateHashModelEx, templateHashModelEx2);
            }
        }
    }

    static TemplateModel _evalOnNumbers(Environment environment, TemplateObject templateObject, Number number, Number number2) throws TemplateException {
        ArithmeticEngine arithmeticEngine;
        if (environment != null) {
            arithmeticEngine = environment.getArithmeticEngine();
        } else {
            arithmeticEngine = templateObject.getTemplate().getArithmeticEngine();
        }
        return new SimpleNumber(arithmeticEngine.add(number, number2));
    }

    /* access modifiers changed from: package-private */
    public boolean isLiteral() {
        return this.constantValue != null || (this.left.isLiteral() && this.right.isLiteral());
    }

    /* access modifiers changed from: protected */
    public Expression deepCloneWithIdentifierReplaced_inner(String str, Expression expression, Expression.ReplacemenetState replacemenetState) {
        return new AddConcatExpression(this.left.deepCloneWithIdentifierReplaced(str, super, replacemenetState), this.right.deepCloneWithIdentifierReplaced(str, super, replacemenetState));
    }

    public String getCanonicalForm() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.left.getCanonicalForm());
        stringBuffer.append(" + ");
        stringBuffer.append(this.right.getCanonicalForm());
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        return i == 0 ? this.left : this.right;
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        return ParameterRole.forBinaryOperatorOperand(i);
    }

    private static final class ConcatenatedSequence implements TemplateSequenceModel {
        private final TemplateSequenceModel left;
        private final TemplateSequenceModel right;

        ConcatenatedSequence(TemplateSequenceModel templateSequenceModel, TemplateSequenceModel templateSequenceModel2) {
            this.left = templateSequenceModel;
            this.right = templateSequenceModel2;
        }

        public int size() throws TemplateModelException {
            return this.left.size() + this.right.size();
        }

        public TemplateModel get(int i) throws TemplateModelException {
            int size = this.left.size();
            return i < size ? this.left.get(i) : this.right.get(i - size);
        }
    }

    private static class ConcatenatedHash implements TemplateHashModel {
        protected final TemplateHashModel left;
        protected final TemplateHashModel right;

        ConcatenatedHash(TemplateHashModel templateHashModel, TemplateHashModel templateHashModel2) {
            this.left = templateHashModel;
            this.right = templateHashModel2;
        }

        public TemplateModel get(String str) throws TemplateModelException {
            TemplateModel templateModel = this.right.get(str);
            return templateModel != null ? templateModel : this.left.get(str);
        }

        public boolean isEmpty() throws TemplateModelException {
            return this.left.isEmpty() && this.right.isEmpty();
        }
    }

    private static final class ConcatenatedHashEx extends ConcatenatedHash implements TemplateHashModelEx {
        private CollectionAndSequence keys;
        private int size;
        private CollectionAndSequence values;

        ConcatenatedHashEx(TemplateHashModelEx templateHashModelEx, TemplateHashModelEx templateHashModelEx2) {
            super(templateHashModelEx, templateHashModelEx2);
        }

        public int size() throws TemplateModelException {
            initKeys();
            return this.size;
        }

        public TemplateCollectionModel keys() throws TemplateModelException {
            initKeys();
            return this.keys;
        }

        public TemplateCollectionModel values() throws TemplateModelException {
            initValues();
            return this.values;
        }

        private void initKeys() throws TemplateModelException {
            if (this.keys == null) {
                HashSet hashSet = new HashSet();
                SimpleSequence simpleSequence = new SimpleSequence(32);
                addKeys(hashSet, simpleSequence, (TemplateHashModelEx) this.left);
                addKeys(hashSet, simpleSequence, (TemplateHashModelEx) this.right);
                this.size = hashSet.size();
                this.keys = new CollectionAndSequence(simpleSequence);
            }
        }

        private static void addKeys(Set set, SimpleSequence simpleSequence, TemplateHashModelEx templateHashModelEx) throws TemplateModelException {
            TemplateModelIterator it = templateHashModelEx.keys().iterator();
            while (it.hasNext()) {
                TemplateScalarModel templateScalarModel = (TemplateScalarModel) it.next();
                if (set.add(templateScalarModel.getAsString())) {
                    simpleSequence.add(templateScalarModel);
                }
            }
        }

        private void initValues() throws TemplateModelException {
            if (this.values == null) {
                SimpleSequence simpleSequence = new SimpleSequence(size());
                int size2 = this.keys.size();
                for (int i = 0; i < size2; i++) {
                    simpleSequence.add(get(((TemplateScalarModel) this.keys.get(i)).getAsString()));
                }
                this.values = new CollectionAndSequence(simpleSequence);
            }
        }
    }
}
