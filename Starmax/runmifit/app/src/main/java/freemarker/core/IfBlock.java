package freemarker.core;

import freemarker.template.TemplateException;
import java.io.IOException;

final class IfBlock extends TemplateElement {
    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#if-#elseif-#else-container";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isShownInStackTrace() {
        return false;
    }

    IfBlock(ConditionalBlock conditionalBlock) {
        setRegulatedChildBufferCapacity(1);
        addBlock(conditionalBlock);
    }

    /* access modifiers changed from: package-private */
    public void addBlock(ConditionalBlock conditionalBlock) {
        addRegulatedChild(super);
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        int regulatedChildCount = getRegulatedChildCount();
        int i = 0;
        while (i < regulatedChildCount) {
            ConditionalBlock conditionalBlock = (ConditionalBlock) getRegulatedChild(i);
            Expression expression = conditionalBlock.condition;
            environment.replaceElementStackTop(super);
            if (expression != null && !expression.evalToBoolean(environment)) {
                i++;
            } else if (conditionalBlock.getNestedBlock() != null) {
                environment.visitByHiddingParent(conditionalBlock.getNestedBlock());
                return;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public TemplateElement postParseCleanup(boolean z) throws ParseException {
        if (getRegulatedChildCount() != 1) {
            return super.postParseCleanup(z);
        }
        ConditionalBlock conditionalBlock = (ConditionalBlock) getRegulatedChild(0);
        conditionalBlock.isLonelyIf = true;
        conditionalBlock.setLocation(getTemplate(), conditionalBlock, this);
        return conditionalBlock.postParseCleanup(z);
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        if (!z) {
            return getNodeTypeSymbol();
        }
        StringBuffer stringBuffer = new StringBuffer();
        int regulatedChildCount = getRegulatedChildCount();
        for (int i = 0; i < regulatedChildCount; i++) {
            stringBuffer.append(((ConditionalBlock) getRegulatedChild(i)).dump(z));
        }
        stringBuffer.append("</#if>");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        throw new IndexOutOfBoundsException();
    }
}
