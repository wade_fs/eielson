package freemarker.core;

import com.baidu.mobstat.Config;
import com.google.android.gms.common.server.FavaDiagnosticsEntity;
import com.google.android.gms.dynamite.ProviderConstants;
import com.tamic.novate.download.MimeType;
import freemarker.core.BuiltInsForDates;
import freemarker.core.BuiltInsForHashes;
import freemarker.core.BuiltInsForLoopVariables;
import freemarker.core.BuiltInsForMultipleTypes;
import freemarker.core.BuiltInsForNodes;
import freemarker.core.BuiltInsForNumbers;
import freemarker.core.BuiltInsForSequences;
import freemarker.core.BuiltInsForStringsBasic;
import freemarker.core.BuiltInsForStringsEncoding;
import freemarker.core.BuiltInsForStringsMisc;
import freemarker.core.BuiltInsForStringsRegexp;
import freemarker.core.BuiltInsWithParseTimeParameters;
import freemarker.core.ExistenceBuiltins;
import freemarker.core.Expression;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;
import freemarker.template.TemplateScalarModel;
import java.util.HashMap;
import java.util.List;
import org.greenrobot.greendao.generator.Schema;

abstract class BuiltIn extends Expression implements Cloneable {
    static final int NUMBER_OF_BIS = 252;
    static final HashMap builtins = new HashMap(379, 0.67f);
    protected String key;
    protected Expression target;

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public boolean isLiteral() {
        return false;
    }

    BuiltIn() {
    }

    static {
        putBI("abs", new BuiltInsForNumbers.absBI());
        putBI("ancestors", new BuiltInsForNodes.ancestorsBI());
        putBI(ProviderConstants.API_PATH, new BuiltInsForMultipleTypes.apiBI());
        putBI("boolean", new BuiltInsForStringsMisc.booleanBI());
        putBI("byte", new BuiltInsForNumbers.byteBI());
        putBI("c", new BuiltInsForMultipleTypes.cBI());
        putBI("cap_first", "capFirst", new BuiltInsForStringsBasic.cap_firstBI());
        putBI("capitalize", new BuiltInsForStringsBasic.capitalizeBI());
        putBI("ceiling", new BuiltInsForNumbers.ceilingBI());
        putBI("children", new BuiltInsForNodes.childrenBI());
        putBI("chop_linebreak", "chopLinebreak", new BuiltInsForStringsBasic.chop_linebreakBI());
        putBI("contains", new BuiltInsForStringsBasic.containsBI());
        putBI("date", new BuiltInsForMultipleTypes.dateBI(2));
        putBI("date_if_unknown", "dateIfUnknown", new BuiltInsForDates.dateType_if_unknownBI(2));
        putBI("datetime", new BuiltInsForMultipleTypes.dateBI(3));
        putBI("datetime_if_unknown", "datetimeIfUnknown", new BuiltInsForDates.dateType_if_unknownBI(3));
        putBI(Schema.DEFAULT_NAME, new ExistenceBuiltins.defaultBI());
        putBI("double", new BuiltInsForNumbers.doubleBI());
        putBI("ends_with", "endsWith", new BuiltInsForStringsBasic.ends_withBI());
        putBI("ensure_ends_with", "ensureEndsWith", new BuiltInsForStringsBasic.ensure_ends_withBI());
        putBI("ensure_starts_with", "ensureStartsWith", new BuiltInsForStringsBasic.ensure_starts_withBI());
        putBI("eval", new BuiltInsForStringsMisc.evalBI());
        putBI("exists", new ExistenceBuiltins.existsBI());
        putBI(Config.TRACE_VISIT_FIRST, new BuiltInsForSequences.firstBI());
        putBI("float", new BuiltInsForNumbers.floatBI());
        putBI("floor", new BuiltInsForNumbers.floorBI());
        putBI("chunk", new BuiltInsForSequences.chunkBI());
        putBI("counter", new BuiltInsForLoopVariables.counterBI());
        putBI("item_cycle", "itemCycle", new BuiltInsForLoopVariables.item_cycleBI());
        putBI("has_api", "hasApi", new BuiltInsForMultipleTypes.has_apiBI());
        putBI("has_content", "hasContent", new ExistenceBuiltins.has_contentBI());
        putBI("has_next", "hasNext", new BuiltInsForLoopVariables.has_nextBI());
        putBI(MimeType.HTML, new BuiltInsForStringsEncoding.htmlBI());
        putBI("if_exists", "ifExists", new ExistenceBuiltins.if_existsBI());
        putBI(Config.FEED_LIST_ITEM_INDEX, new BuiltInsForLoopVariables.indexBI());
        putBI("index_of", "indexOf", new BuiltInsForStringsBasic.index_ofBI(false));
        putBI("int", new BuiltInsForNumbers.intBI());
        putBI("interpret", new Interpret());
        putBI("is_boolean", "isBoolean", new BuiltInsForMultipleTypes.is_booleanBI());
        putBI("is_collection", "isCollection", new BuiltInsForMultipleTypes.is_collectionBI());
        putBI("is_collection_ex", "isCollectionEx", new BuiltInsForMultipleTypes.is_collection_exBI());
        BuiltInsForMultipleTypes.is_dateLikeBI is_datelikebi = new BuiltInsForMultipleTypes.is_dateLikeBI();
        putBI("is_date", "isDate", is_datelikebi);
        putBI("is_date_like", "isDateLike", is_datelikebi);
        putBI("is_date_only", "isDateOnly", new BuiltInsForMultipleTypes.is_dateOfTypeBI(2));
        putBI("is_even_item", "isEvenItem", new BuiltInsForLoopVariables.is_even_itemBI());
        putBI("is_first", "isFirst", new BuiltInsForLoopVariables.is_firstBI());
        putBI("is_last", "isLast", new BuiltInsForLoopVariables.is_lastBI());
        putBI("is_unknown_date_like", "isUnknownDateLike", new BuiltInsForMultipleTypes.is_dateOfTypeBI(0));
        putBI("is_datetime", "isDatetime", new BuiltInsForMultipleTypes.is_dateOfTypeBI(3));
        putBI("is_directive", "isDirective", new BuiltInsForMultipleTypes.is_directiveBI());
        putBI("is_enumerable", "isEnumerable", new BuiltInsForMultipleTypes.is_enumerableBI());
        putBI("is_hash_ex", "isHashEx", new BuiltInsForMultipleTypes.is_hash_exBI());
        putBI("is_hash", "isHash", new BuiltInsForMultipleTypes.is_hashBI());
        putBI("is_infinite", "isInfinite", new BuiltInsForNumbers.is_infiniteBI());
        putBI("is_indexable", "isIndexable", new BuiltInsForMultipleTypes.is_indexableBI());
        putBI("is_macro", "isMacro", new BuiltInsForMultipleTypes.is_macroBI());
        putBI("is_method", "isMethod", new BuiltInsForMultipleTypes.is_methodBI());
        putBI("is_nan", "isNan", new BuiltInsForNumbers.is_nanBI());
        putBI("is_node", "isNode", new BuiltInsForMultipleTypes.is_nodeBI());
        putBI("is_number", "isNumber", new BuiltInsForMultipleTypes.is_numberBI());
        putBI("is_odd_item", "isOddItem", new BuiltInsForLoopVariables.is_odd_itemBI());
        putBI("is_sequence", "isSequence", new BuiltInsForMultipleTypes.is_sequenceBI());
        putBI("is_string", "isString", new BuiltInsForMultipleTypes.is_stringBI());
        putBI("is_time", "isTime", new BuiltInsForMultipleTypes.is_dateOfTypeBI(1));
        putBI("is_transform", "isTransform", new BuiltInsForMultipleTypes.is_transformBI());
        putBI("iso_utc", "isoUtc", new BuiltInsForDates.iso_utc_or_local_BI(null, 6, true));
        putBI("iso_utc_fz", "isoUtcFZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.TRUE, 6, true));
        putBI("iso_utc_nz", "isoUtcNZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.FALSE, 6, true));
        putBI("iso_utc_ms", "isoUtcMs", new BuiltInsForDates.iso_utc_or_local_BI(null, 7, true));
        putBI("iso_utc_ms_nz", "isoUtcMsNZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.FALSE, 7, true));
        putBI("iso_utc_m", "isoUtcM", new BuiltInsForDates.iso_utc_or_local_BI(null, 5, true));
        putBI("iso_utc_m_nz", "isoUtcMNZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.FALSE, 5, true));
        putBI("iso_utc_h", "isoUtcH", new BuiltInsForDates.iso_utc_or_local_BI(null, 4, true));
        putBI("iso_utc_h_nz", "isoUtcHNZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.FALSE, 4, true));
        putBI("iso_local", "isoLocal", new BuiltInsForDates.iso_utc_or_local_BI(null, 6, false));
        putBI("iso_local_nz", "isoLocalNZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.FALSE, 6, false));
        putBI("iso_local_ms", "isoLocalMs", new BuiltInsForDates.iso_utc_or_local_BI(null, 7, false));
        putBI("iso_local_ms_nz", "isoLocalMsNZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.FALSE, 7, false));
        putBI("iso_local_m", "isoLocalM", new BuiltInsForDates.iso_utc_or_local_BI(null, 5, false));
        putBI("iso_local_m_nz", "isoLocalMNZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.FALSE, 5, false));
        putBI("iso_local_h", "isoLocalH", new BuiltInsForDates.iso_utc_or_local_BI(null, 4, false));
        putBI("iso_local_h_nz", "isoLocalHNZ", new BuiltInsForDates.iso_utc_or_local_BI(Boolean.FALSE, 4, false));
        putBI("iso", new BuiltInsForDates.iso_BI(null, 6));
        putBI("iso_nz", "isoNZ", new BuiltInsForDates.iso_BI(Boolean.FALSE, 6));
        putBI("iso_ms", "isoMs", new BuiltInsForDates.iso_BI(null, 7));
        putBI("iso_ms_nz", "isoMsNZ", new BuiltInsForDates.iso_BI(Boolean.FALSE, 7));
        putBI("iso_m", "isoM", new BuiltInsForDates.iso_BI(null, 5));
        putBI("iso_m_nz", "isoMNZ", new BuiltInsForDates.iso_BI(Boolean.FALSE, 5));
        putBI("iso_h", "isoH", new BuiltInsForDates.iso_BI(null, 4));
        putBI("iso_h_nz", "isoHNZ", new BuiltInsForDates.iso_BI(Boolean.FALSE, 4));
        putBI("j_string", "jString", new BuiltInsForStringsEncoding.j_stringBI());
        putBI("join", new BuiltInsForSequences.joinBI());
        putBI("js_string", "jsString", new BuiltInsForStringsEncoding.js_stringBI());
        putBI("json_string", "jsonString", new BuiltInsForStringsEncoding.json_stringBI());
        putBI("keep_after", "keepAfter", new BuiltInsForStringsBasic.keep_afterBI());
        putBI("keep_before", "keepBefore", new BuiltInsForStringsBasic.keep_beforeBI());
        putBI("keep_after_last", "keepAfterLast", new BuiltInsForStringsBasic.keep_after_lastBI());
        putBI("keep_before_last", "keepBeforeLast", new BuiltInsForStringsBasic.keep_before_lastBI());
        putBI("keys", new BuiltInsForHashes.keysBI());
        putBI("last_index_of", "lastIndexOf", new BuiltInsForStringsBasic.index_ofBI(true));
        putBI("last", new BuiltInsForSequences.lastBI());
        putBI("left_pad", "leftPad", new BuiltInsForStringsBasic.padBI(true));
        putBI("length", new BuiltInsForStringsBasic.lengthBI());
        putBI("long", new BuiltInsForNumbers.longBI());
        putBI("lower_abc", "lowerAbc", new BuiltInsForNumbers.lower_abcBI());
        putBI("lower_case", "lowerCase", new BuiltInsForStringsBasic.lower_caseBI());
        putBI(FavaDiagnosticsEntity.EXTRA_NAMESPACE, new BuiltInsForMultipleTypes.namespaceBI());
        putBI("new", new NewBI());
        putBI("node_name", "nodeName", new BuiltInsForNodes.node_nameBI());
        putBI("node_namespace", "nodeNamespace", new BuiltInsForNodes.node_namespaceBI());
        putBI("node_type", "nodeType", new BuiltInsForNodes.node_typeBI());
        putBI("number", new BuiltInsForStringsMisc.numberBI());
        putBI("number_to_date", "numberToDate", new BuiltInsForNumbers.number_to_dateBI(2));
        putBI("number_to_time", "numberToTime", new BuiltInsForNumbers.number_to_dateBI(1));
        putBI("number_to_datetime", "numberToDatetime", new BuiltInsForNumbers.number_to_dateBI(3));
        putBI("parent", new BuiltInsForNodes.parentBI());
        putBI("item_parity", "itemParity", new BuiltInsForLoopVariables.item_parityBI());
        putBI("item_parity_cap", "itemParityCap", new BuiltInsForLoopVariables.item_parity_capBI());
        putBI("reverse", new BuiltInsForSequences.reverseBI());
        putBI("right_pad", "rightPad", new BuiltInsForStringsBasic.padBI(false));
        putBI("root", new BuiltInsForNodes.rootBI());
        putBI("round", new BuiltInsForNumbers.roundBI());
        putBI("remove_ending", "removeEnding", new BuiltInsForStringsBasic.remove_endingBI());
        putBI("remove_beginning", "removeBeginning", new BuiltInsForStringsBasic.remove_beginningBI());
        putBI("rtf", new BuiltInsForStringsEncoding.rtfBI());
        putBI("seq_contains", "seqContains", new BuiltInsForSequences.seq_containsBI());
        putBI("seq_index_of", "seqIndexOf", new BuiltInsForSequences.seq_index_ofBI(1));
        putBI("seq_last_index_of", "seqLastIndexOf", new BuiltInsForSequences.seq_index_ofBI(-1));
        putBI("short", new BuiltInsForNumbers.shortBI());
        putBI("size", new BuiltInsForMultipleTypes.sizeBI());
        putBI("sort_by", "sortBy", new BuiltInsForSequences.sort_byBI());
        putBI("sort", new BuiltInsForSequences.sortBI());
        putBI("split", new BuiltInsForStringsBasic.split_BI());
        putBI("switch", new BuiltInsWithParseTimeParameters.switch_BI());
        putBI("starts_with", "startsWith", new BuiltInsForStringsBasic.starts_withBI());
        putBI("string", new BuiltInsForMultipleTypes.stringBI());
        putBI("substring", new BuiltInsForStringsBasic.substringBI());
        putBI("then", new BuiltInsWithParseTimeParameters.then_BI());
        putBI("time", new BuiltInsForMultipleTypes.dateBI(1));
        putBI("time_if_unknown", "timeIfUnknown", new BuiltInsForDates.dateType_if_unknownBI(1));
        putBI("trim", new BuiltInsForStringsBasic.trimBI());
        putBI("uncap_first", "uncapFirst", new BuiltInsForStringsBasic.uncap_firstBI());
        putBI("upper_abc", "upperAbc", new BuiltInsForNumbers.upper_abcBI());
        putBI("upper_case", "upperCase", new BuiltInsForStringsBasic.upper_caseBI());
        putBI("url", new BuiltInsForStringsEncoding.urlBI());
        putBI("url_path", "urlPath", new BuiltInsForStringsEncoding.urlPathBI());
        putBI("values", new BuiltInsForHashes.valuesBI());
        putBI("web_safe", "webSafe", (BuiltIn) builtins.get(MimeType.HTML));
        putBI("word_list", "wordList", new BuiltInsForStringsBasic.word_listBI());
        putBI("xhtml", new BuiltInsForStringsEncoding.xhtmlBI());
        putBI(MimeType.XML, new BuiltInsForStringsEncoding.xmlBI());
        putBI("matches", new BuiltInsForStringsRegexp.matchesBI());
        putBI("groups", new BuiltInsForStringsRegexp.groupsBI());
        putBI("replace", new BuiltInsForStringsRegexp.replace_reBI());
        if (252 < builtins.size()) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Update NUMBER_OF_BIS! Should be: ");
            stringBuffer.append(builtins.size());
            throw new AssertionError(stringBuffer.toString());
        }
    }

    private static void putBI(String str, BuiltIn builtIn) {
        builtins.put(str, builtIn);
    }

    private static void putBI(String str, String str2, BuiltIn builtIn) {
        builtins.put(str, builtIn);
        builtins.put(str2, builtIn);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: freemarker.core.BuiltIn} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static freemarker.core.BuiltIn newBuiltIn(int r8, freemarker.core.Expression r9, freemarker.core.Token r10, freemarker.core.FMParserTokenManager r11) throws freemarker.core.ParseException {
        /*
            java.lang.String r0 = r10.image
            java.util.HashMap r1 = freemarker.core.BuiltIn.builtins
            java.lang.Object r1 = r1.get(r0)
            freemarker.core.BuiltIn r1 = (freemarker.core.BuiltIn) r1
            if (r1 != 0) goto L_0x0098
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            java.lang.String r9 = "Unknown built-in: "
            r8.<init>(r9)
            java.lang.String r9 = freemarker.template.utility.StringUtil.jQuote(r0)
            r8.append(r9)
            java.lang.String r9 = ". "
            r8.append(r9)
            java.lang.String r9 = "Help (latest version): http://freemarker.org/docs/ref_builtins.html; you're using FreeMarker "
            r8.append(r9)
            freemarker.template.Version r9 = freemarker.template.Configuration.getVersion()
            r8.append(r9)
            java.lang.String r9 = ".\nThe alphabetical list of built-ins:"
            r8.append(r9)
            java.util.ArrayList r9 = new java.util.ArrayList
            java.util.HashMap r0 = freemarker.core.BuiltIn.builtins
            java.util.Set r0 = r0.keySet()
            int r0 = r0.size()
            r9.<init>(r0)
            java.util.HashMap r0 = freemarker.core.BuiltIn.builtins
            java.util.Set r0 = r0.keySet()
            r9.addAll(r0)
            java.util.Collections.sort(r9)
            int r11 = r11.namingConvention
            r0 = 11
            r1 = 10
            if (r11 == r1) goto L_0x0054
            goto L_0x0056
        L_0x0054:
            r11 = 11
        L_0x0056:
            r2 = 1
            java.util.Iterator r9 = r9.iterator()
            r3 = 0
            r4 = 0
        L_0x005d:
            boolean r5 = r9.hasNext()
            if (r5 == 0) goto L_0x008d
            java.lang.Object r5 = r9.next()
            java.lang.String r5 = (java.lang.String) r5
            int r6 = freemarker.core._CoreStringUtils.getIdentifierNamingConvention(r5)
            r7 = 12
            if (r11 != r7) goto L_0x0074
            if (r6 == r0) goto L_0x005d
            goto L_0x0076
        L_0x0074:
            if (r6 == r7) goto L_0x005d
        L_0x0076:
            if (r2 == 0) goto L_0x007a
            r2 = 0
            goto L_0x007f
        L_0x007a:
            java.lang.String r6 = ", "
            r8.append(r6)
        L_0x007f:
            char r6 = r5.charAt(r3)
            if (r6 == r4) goto L_0x0089
            r8.append(r1)
            r4 = r6
        L_0x0089:
            r8.append(r5)
            goto L_0x005d
        L_0x008d:
            freemarker.core.ParseException r9 = new freemarker.core.ParseException
            java.lang.String r8 = r8.toString()
            r11 = 0
            r9.<init>(r8, r11, r10)
            throw r9
        L_0x0098:
            boolean r10 = r1 instanceof freemarker.core.ICIChainMember
            if (r10 == 0) goto L_0x00ad
            r10 = r1
            freemarker.core.ICIChainMember r10 = (freemarker.core.ICIChainMember) r10
            int r11 = r10.getMinimumICIVersion()
            if (r8 >= r11) goto L_0x00ad
            java.lang.Object r10 = r10.getPreviousICIChainMember()
            r1 = r10
            freemarker.core.BuiltIn r1 = (freemarker.core.BuiltIn) r1
            goto L_0x0098
        L_0x00ad:
            java.lang.Object r8 = r1.clone()     // Catch:{ CloneNotSupportedException -> 0x00b8 }
            freemarker.core.BuiltIn r8 = (freemarker.core.BuiltIn) r8     // Catch:{ CloneNotSupportedException -> 0x00b8 }
            r8.key = r0
            r8.target = r9
            return r8
        L_0x00b8:
            java.lang.InternalError r8 = new java.lang.InternalError
            r8.<init>()
            goto L_0x00bf
        L_0x00be:
            throw r8
        L_0x00bf:
            goto L_0x00be
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.BuiltIn.newBuiltIn(int, freemarker.core.Expression, freemarker.core.Token, freemarker.core.FMParserTokenManager):freemarker.core.BuiltIn");
    }

    public String getCanonicalForm() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.target.getCanonicalForm());
        stringBuffer.append("?");
        stringBuffer.append(this.key);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?");
        stringBuffer.append(this.key);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public final void checkMethodArgCount(List list, int i) throws TemplateModelException {
        checkMethodArgCount(list.size(), i);
    }

    /* access modifiers changed from: protected */
    public final void checkMethodArgCount(int i, int i2) throws TemplateModelException {
        if (i != i2) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("?");
            stringBuffer.append(this.key);
            throw MessageUtil.newArgCntError(stringBuffer.toString(), i, i2);
        }
    }

    /* access modifiers changed from: protected */
    public final void checkMethodArgCount(List list, int i, int i2) throws TemplateModelException {
        checkMethodArgCount(list.size(), i, i2);
    }

    /* access modifiers changed from: protected */
    public final void checkMethodArgCount(int i, int i2, int i3) throws TemplateModelException {
        if (i < i2 || i > i3) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("?");
            stringBuffer.append(this.key);
            throw MessageUtil.newArgCntError(stringBuffer.toString(), i, i2, i3);
        }
    }

    /* access modifiers changed from: protected */
    public final String getOptStringMethodArg(List list, int i) throws TemplateModelException {
        if (list.size() > i) {
            return getStringMethodArg(list, i);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final String getStringMethodArg(List list, int i) throws TemplateModelException {
        TemplateModel templateModel = (TemplateModel) list.get(i);
        if (templateModel instanceof TemplateScalarModel) {
            return EvalUtil.modelToString((TemplateScalarModel) templateModel, null, null);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?");
        stringBuffer.append(this.key);
        throw MessageUtil.newMethodArgMustBeStringException(stringBuffer.toString(), i, templateModel);
    }

    /* access modifiers changed from: protected */
    public final Number getNumberMethodArg(List list, int i) throws TemplateModelException {
        TemplateModel templateModel = (TemplateModel) list.get(i);
        if (templateModel instanceof TemplateNumberModel) {
            return EvalUtil.modelToNumber((TemplateNumberModel) templateModel, null);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?");
        stringBuffer.append(this.key);
        throw MessageUtil.newMethodArgMustBeNumberException(stringBuffer.toString(), i, templateModel);
    }

    /* access modifiers changed from: protected */
    public final TemplateModelException newMethodArgInvalidValueException(int i, Object[] objArr) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?");
        stringBuffer.append(this.key);
        return MessageUtil.newMethodArgInvalidValueException(stringBuffer.toString(), i, objArr);
    }

    /* access modifiers changed from: protected */
    public final TemplateModelException newMethodArgsInvalidValueException(Object[] objArr) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?");
        stringBuffer.append(this.key);
        return MessageUtil.newMethodArgsInvalidValueException(stringBuffer.toString(), objArr);
    }

    /* access modifiers changed from: protected */
    public Expression deepCloneWithIdentifierReplaced_inner(String str, Expression expression, Expression.ReplacemenetState replacemenetState) {
        try {
            BuiltIn builtIn = (BuiltIn) clone();
            builtIn.target = this.target.deepCloneWithIdentifierReplaced(str, super, replacemenetState);
            return super;
        } catch (CloneNotSupportedException e) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Internal error: ");
            stringBuffer.append(e);
            throw new RuntimeException(stringBuffer.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.target;
        }
        if (i == 1) {
            return this.key;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.LEFT_HAND_OPERAND;
        }
        if (i == 1) {
            return ParameterRole.RIGHT_HAND_OPERAND;
        }
        throw new IndexOutOfBoundsException();
    }
}
