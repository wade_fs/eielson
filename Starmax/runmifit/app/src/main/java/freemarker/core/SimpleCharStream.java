package freemarker.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class SimpleCharStream {
    public static final boolean staticFlag = false;
    int available;
    protected int[] bufcolumn;
    protected char[] buffer;
    protected int[] bufline;
    public int bufpos;
    int bufsize;
    protected int column;
    protected int inBuf;
    protected Reader inputStream;
    protected int line;
    protected int maxNextCharInd;
    protected boolean prevCharIsCR;
    protected boolean prevCharIsLF;
    int tokenBegin;

    /* access modifiers changed from: protected */
    public void ExpandBuff(boolean z) {
        int i = this.bufsize;
        char[] cArr = new char[(i + 2048)];
        int[] iArr = new int[(i + 2048)];
        int[] iArr2 = new int[(i + 2048)];
        if (z) {
            try {
                System.arraycopy(this.buffer, this.tokenBegin, cArr, 0, i - this.tokenBegin);
                System.arraycopy(this.buffer, 0, cArr, this.bufsize - this.tokenBegin, this.bufpos);
                this.buffer = cArr;
                System.arraycopy(this.bufline, this.tokenBegin, iArr, 0, this.bufsize - this.tokenBegin);
                System.arraycopy(this.bufline, 0, iArr, this.bufsize - this.tokenBegin, this.bufpos);
                this.bufline = iArr;
                System.arraycopy(this.bufcolumn, this.tokenBegin, iArr2, 0, this.bufsize - this.tokenBegin);
                System.arraycopy(this.bufcolumn, 0, iArr2, this.bufsize - this.tokenBegin, this.bufpos);
                this.bufcolumn = iArr2;
                int i2 = this.bufpos + (this.bufsize - this.tokenBegin);
                this.bufpos = i2;
                this.maxNextCharInd = i2;
            } catch (Throwable th) {
                throw new Error(th.getMessage());
            }
        } else {
            System.arraycopy(this.buffer, this.tokenBegin, cArr, 0, i - this.tokenBegin);
            this.buffer = cArr;
            System.arraycopy(this.bufline, this.tokenBegin, iArr, 0, this.bufsize - this.tokenBegin);
            this.bufline = iArr;
            System.arraycopy(this.bufcolumn, this.tokenBegin, iArr2, 0, this.bufsize - this.tokenBegin);
            this.bufcolumn = iArr2;
            int i3 = this.bufpos - this.tokenBegin;
            this.bufpos = i3;
            this.maxNextCharInd = i3;
        }
        this.bufsize += 2048;
        this.available = this.bufsize;
        this.tokenBegin = 0;
    }

    /* access modifiers changed from: protected */
    public void FillBuff() throws IOException {
        int i = this.maxNextCharInd;
        int i2 = this.available;
        if (i == i2) {
            int i3 = this.bufsize;
            if (i2 == i3) {
                int i4 = this.tokenBegin;
                if (i4 > 2048) {
                    this.maxNextCharInd = 0;
                    this.bufpos = 0;
                    this.available = i4;
                } else if (i4 < 0) {
                    this.maxNextCharInd = 0;
                    this.bufpos = 0;
                } else {
                    ExpandBuff(false);
                }
            } else {
                int i5 = this.tokenBegin;
                if (i2 > i5) {
                    this.available = i3;
                } else if (i5 - i2 < 2048) {
                    ExpandBuff(true);
                } else {
                    this.available = i5;
                }
            }
        }
        try {
            int read = this.inputStream.read(this.buffer, this.maxNextCharInd, this.available - this.maxNextCharInd);
            if (read != -1) {
                this.maxNextCharInd += read;
            } else {
                this.inputStream.close();
                throw new IOException();
            }
        } catch (IOException e) {
            this.bufpos--;
            backup(0);
            if (this.tokenBegin == -1) {
                this.tokenBegin = this.bufpos;
            }
            throw e;
        }
    }

    public char BeginToken() throws IOException {
        this.tokenBegin = -1;
        char readChar = readChar();
        this.tokenBegin = this.bufpos;
        return readChar;
    }

    /* access modifiers changed from: protected */
    public void UpdateLineColumn(char c) {
        this.column++;
        if (this.prevCharIsLF) {
            this.prevCharIsLF = false;
            int i = this.line;
            this.column = 1;
            this.line = i + 1;
        } else if (this.prevCharIsCR) {
            this.prevCharIsCR = false;
            if (c == 10) {
                this.prevCharIsLF = true;
            } else {
                int i2 = this.line;
                this.column = 1;
                this.line = i2 + 1;
            }
        }
        if (c == 9) {
            this.column--;
            int i3 = this.column;
            this.column = i3 + (8 - (i3 & 7));
        } else if (c == 10) {
            this.prevCharIsLF = true;
        } else if (c == 13) {
            this.prevCharIsCR = true;
        }
        int[] iArr = this.bufline;
        int i4 = this.bufpos;
        iArr[i4] = this.line;
        this.bufcolumn[i4] = this.column;
    }

    public char readChar() throws IOException {
        int i = this.inBuf;
        if (i > 0) {
            this.inBuf = i - 1;
            int i2 = this.bufpos + 1;
            this.bufpos = i2;
            if (i2 == this.bufsize) {
                this.bufpos = 0;
            }
            return this.buffer[this.bufpos];
        }
        int i3 = this.bufpos + 1;
        this.bufpos = i3;
        if (i3 >= this.maxNextCharInd) {
            FillBuff();
        }
        char c = this.buffer[this.bufpos];
        UpdateLineColumn(c);
        return c;
    }

    public int getColumn() {
        return this.bufcolumn[this.bufpos];
    }

    public int getLine() {
        return this.bufline[this.bufpos];
    }

    public int getEndColumn() {
        return this.bufcolumn[this.bufpos];
    }

    public int getEndLine() {
        return this.bufline[this.bufpos];
    }

    public int getBeginColumn() {
        return this.bufcolumn[this.tokenBegin];
    }

    public int getBeginLine() {
        return this.bufline[this.tokenBegin];
    }

    public void backup(int i) {
        this.inBuf += i;
        int i2 = this.bufpos - i;
        this.bufpos = i2;
        if (i2 < 0) {
            this.bufpos += this.bufsize;
        }
    }

    public SimpleCharStream(Reader reader, int i, int i2, int i3) {
        this.bufpos = -1;
        this.column = 0;
        this.line = 1;
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.inputStream = reader;
        this.line = i;
        this.column = i2 - 1;
        this.bufsize = i3;
        this.available = i3;
        this.buffer = new char[i3];
        this.bufline = new int[i3];
        this.bufcolumn = new int[i3];
    }

    public SimpleCharStream(Reader reader, int i, int i2) {
        this(reader, i, i2, 4096);
    }

    public SimpleCharStream(Reader reader) {
        this(reader, 1, 1, 4096);
    }

    public void ReInit(Reader reader, int i, int i2, int i3) {
        this.inputStream = reader;
        this.line = i;
        this.column = i2 - 1;
        char[] cArr = this.buffer;
        if (cArr == null || i3 != cArr.length) {
            this.bufsize = i3;
            this.available = i3;
            this.buffer = new char[i3];
            this.bufline = new int[i3];
            this.bufcolumn = new int[i3];
        }
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.tokenBegin = 0;
        this.bufpos = -1;
    }

    public void ReInit(Reader reader, int i, int i2) {
        ReInit(reader, i, i2, 4096);
    }

    public void ReInit(Reader reader) {
        ReInit(reader, 1, 1, 4096);
    }

    public SimpleCharStream(InputStream inputStream2, int i, int i2, int i3) {
        this(new InputStreamReader(inputStream2), i, i2, 4096);
    }

    public SimpleCharStream(InputStream inputStream2, int i, int i2) {
        this(inputStream2, i, i2, 4096);
    }

    public SimpleCharStream(InputStream inputStream2) {
        this(inputStream2, 1, 1, 4096);
    }

    public void ReInit(InputStream inputStream2, int i, int i2, int i3) {
        ReInit(new InputStreamReader(inputStream2), i, i2, 4096);
    }

    public void ReInit(InputStream inputStream2) {
        ReInit(inputStream2, 1, 1, 4096);
    }

    public void ReInit(InputStream inputStream2, int i, int i2) {
        ReInit(inputStream2, i, i2, 4096);
    }

    public String GetImage() {
        int i = this.bufpos;
        int i2 = this.tokenBegin;
        if (i >= i2) {
            return new String(this.buffer, i2, (i - i2) + 1);
        }
        StringBuffer stringBuffer = new StringBuffer();
        char[] cArr = this.buffer;
        int i3 = this.tokenBegin;
        stringBuffer.append(new String(cArr, i3, this.bufsize - i3));
        stringBuffer.append(new String(this.buffer, 0, this.bufpos + 1));
        return stringBuffer.toString();
    }

    public char[] GetSuffix(int i) {
        char[] cArr = new char[i];
        int i2 = this.bufpos;
        if (i2 + 1 >= i) {
            System.arraycopy(this.buffer, (i2 - i) + 1, cArr, 0, i);
        } else {
            System.arraycopy(this.buffer, this.bufsize - ((i - i2) - 1), cArr, 0, (i - i2) - 1);
            char[] cArr2 = this.buffer;
            int i3 = this.bufpos;
            System.arraycopy(cArr2, 0, cArr, (i - i3) - 1, i3 + 1);
        }
        return cArr;
    }

    public void Done() {
        this.buffer = null;
        this.bufline = null;
        this.bufcolumn = null;
    }

    public void adjustBeginLineColumn(int i, int i2) {
        int i3;
        int i4 = this.tokenBegin;
        int i5 = this.bufpos;
        if (i5 >= i4) {
            i3 = (i5 - i4) + this.inBuf + 1;
        } else {
            i3 = this.inBuf + (this.bufsize - i4) + i5 + 1;
        }
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        while (true) {
            if (i6 >= i3) {
                break;
            }
            int[] iArr = this.bufline;
            int i9 = this.bufsize;
            int i10 = i4 % i9;
            i4++;
            int i11 = i4 % i9;
            if (iArr[i10] != iArr[i11]) {
                i7 = i10;
                break;
            }
            iArr[i10] = i;
            int[] iArr2 = this.bufcolumn;
            iArr2[i10] = i8 + i2;
            i6++;
            i8 = (iArr2[i11] + i8) - iArr2[i10];
            i7 = i10;
        }
        if (i6 < i3) {
            int i12 = i + 1;
            this.bufline[i7] = i;
            this.bufcolumn[i7] = i2 + i8;
            while (true) {
                int i13 = i6 + 1;
                if (i6 >= i3) {
                    break;
                }
                int[] iArr3 = this.bufline;
                int i14 = this.bufsize;
                i7 = i4 % i14;
                i4++;
                if (iArr3[i7] != iArr3[i4 % i14]) {
                    iArr3[i7] = i12;
                    i12++;
                } else {
                    iArr3[i7] = i12;
                }
                i6 = i13;
            }
        }
        this.line = this.bufline[i7];
        this.column = this.bufcolumn[i7];
    }
}
