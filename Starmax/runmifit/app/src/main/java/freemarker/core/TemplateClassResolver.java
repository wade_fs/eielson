package freemarker.core;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.utility.ClassUtil;

public interface TemplateClassResolver {
    public static final TemplateClassResolver ALLOWS_NOTHING_RESOLVER = new TemplateClassResolver() {
        /* class freemarker.core.TemplateClassResolver.C33033 */

        public Class resolve(String str, Environment environment, Template template) throws TemplateException {
            throw MessageUtil.newInstantiatingClassNotAllowedException(str, environment);
        }
    };
    public static final TemplateClassResolver SAFER_RESOLVER = new TemplateClassResolver() {
        /* class freemarker.core.TemplateClassResolver.C33022 */

        public Class resolve(String str, Environment environment, Template template) throws TemplateException {
            Class cls;
            Class cls2;
            if (C33044.class$freemarker$template$utility$ObjectConstructor == null) {
                cls = C33044.class$("freemarker.template.utility.ObjectConstructor");
                C33044.class$freemarker$template$utility$ObjectConstructor = cls;
            } else {
                cls = C33044.class$freemarker$template$utility$ObjectConstructor;
            }
            if (!str.equals(cls.getName())) {
                if (C33044.class$freemarker$template$utility$Execute == null) {
                    cls2 = C33044.class$("freemarker.template.utility.Execute");
                    C33044.class$freemarker$template$utility$Execute = cls2;
                } else {
                    cls2 = C33044.class$freemarker$template$utility$Execute;
                }
                if (!str.equals(cls2.getName()) && !str.equals("freemarker.template.utility.JythonRuntime")) {
                    try {
                        return ClassUtil.forName(str);
                    } catch (ClassNotFoundException e) {
                        throw new _MiscTemplateException(e, environment);
                    }
                }
            }
            throw MessageUtil.newInstantiatingClassNotAllowedException(str, environment);
        }
    };
    public static final TemplateClassResolver UNRESTRICTED_RESOLVER = new TemplateClassResolver() {
        /* class freemarker.core.TemplateClassResolver.C33011 */

        public Class resolve(String str, Environment environment, Template template) throws TemplateException {
            try {
                return ClassUtil.forName(str);
            } catch (ClassNotFoundException e) {
                throw new _MiscTemplateException(e, environment);
            }
        }
    };

    Class resolve(String str, Environment environment, Template template) throws TemplateException;

    /* renamed from: freemarker.core.TemplateClassResolver$4 */
    /* synthetic */ class C33044 {
        static /* synthetic */ Class class$freemarker$template$utility$Execute;
        static /* synthetic */ Class class$freemarker$template$utility$ObjectConstructor;

        static /* synthetic */ Class class$(String str) {
            try {
                return Class.forName(str);
            } catch (ClassNotFoundException e) {
                throw new NoClassDefFoundError().initCause(e);
            }
        }
    }
}
