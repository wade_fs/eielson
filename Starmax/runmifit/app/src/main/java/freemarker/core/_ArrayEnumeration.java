package freemarker.core;

import java.util.Enumeration;
import java.util.NoSuchElementException;

public class _ArrayEnumeration implements Enumeration {
    private final Object[] array;
    private int nextIndex = 0;
    private final int size;

    public _ArrayEnumeration(Object[] objArr, int i) {
        this.array = objArr;
        this.size = i;
    }

    public boolean hasMoreElements() {
        return this.nextIndex < this.size;
    }

    public Object nextElement() {
        int i = this.nextIndex;
        if (i < this.size) {
            Object[] objArr = this.array;
            this.nextIndex = i + 1;
            return objArr[i];
        }
        throw new NoSuchElementException();
    }
}
