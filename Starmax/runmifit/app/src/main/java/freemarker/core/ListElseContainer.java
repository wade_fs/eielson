package freemarker.core;

import freemarker.template.TemplateException;
import java.io.IOException;

class ListElseContainer extends TemplateElement {
    private final ElseOfList elsePart;
    private final IteratorBlock listPart;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#list-#else-container";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    public ListElseContainer(IteratorBlock iteratorBlock, ElseOfList elseOfList) {
        setRegulatedChildBufferCapacity(2);
        addRegulatedChild(super);
        addRegulatedChild(super);
        this.listPart = iteratorBlock;
        this.elsePart = elseOfList;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        if (!this.listPart.acceptWithResult(environment)) {
            this.elsePart.accept(environment);
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        if (!z) {
            return getNodeTypeSymbol();
        }
        StringBuffer stringBuffer = new StringBuffer();
        int regulatedChildCount = getRegulatedChildCount();
        for (int i = 0; i < regulatedChildCount; i++) {
            stringBuffer.append(getRegulatedChild(i).dump(z));
        }
        stringBuffer.append("</#list>");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        throw new IndexOutOfBoundsException();
    }
}
