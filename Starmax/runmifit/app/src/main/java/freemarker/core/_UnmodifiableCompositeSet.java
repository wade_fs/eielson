package freemarker.core;

import java.util.Iterator;
import java.util.Set;

public class _UnmodifiableCompositeSet extends _UnmodifiableSet {
    /* access modifiers changed from: private */
    public final Set set1;
    /* access modifiers changed from: private */
    public final Set set2;

    public _UnmodifiableCompositeSet(Set set, Set set3) {
        this.set1 = set;
        this.set2 = set3;
    }

    public Iterator iterator() {
        return new CompositeIterator();
    }

    public boolean contains(Object obj) {
        return this.set1.contains(obj) || this.set2.contains(obj);
    }

    public int size() {
        return this.set1.size() + this.set2.size();
    }

    private class CompositeIterator implements Iterator {
        private Iterator it1;
        private boolean it1Deplected;
        private Iterator it2;

        private CompositeIterator() {
        }

        public boolean hasNext() {
            if (!this.it1Deplected) {
                if (this.it1 == null) {
                    this.it1 = _UnmodifiableCompositeSet.this.set1.iterator();
                }
                if (this.it1.hasNext()) {
                    return true;
                }
                this.it2 = _UnmodifiableCompositeSet.this.set2.iterator();
                this.it1 = null;
                this.it1Deplected = true;
            }
            return this.it2.hasNext();
        }

        public Object next() {
            if (!this.it1Deplected) {
                if (this.it1 == null) {
                    this.it1 = _UnmodifiableCompositeSet.this.set1.iterator();
                }
                if (this.it1.hasNext()) {
                    return this.it1.next();
                }
                this.it2 = _UnmodifiableCompositeSet.this.set2.iterator();
                this.it1 = null;
                this.it1Deplected = true;
            }
            return this.it2.next();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
