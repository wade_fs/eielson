package freemarker.core;

import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;

public final class LibraryLoad extends TemplateElement {
    private Expression importedTemplateNameExp;
    private String namespace;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#import";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 2;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    LibraryLoad(Template template, Expression expression, String str) {
        this.namespace = str;
        this.importedTemplateNameExp = expression;
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        String evalAndCoerceToString = this.importedTemplateNameExp.evalAndCoerceToString(environment);
        try {
            try {
                environment.importLib(environment.getTemplateForImporting(environment.toFullTemplateName(getTemplate().getName(), evalAndCoerceToString)), this.namespace);
            } catch (IOException e) {
                throw new _MiscTemplateException(e, environment, new Object[]{"Template importing failed (for parameter value ", new _DelayedJQuote(evalAndCoerceToString), "):\n", new _DelayedGetMessage(e)});
            }
        } catch (MalformedTemplateNameException e2) {
            throw new _MiscTemplateException(e2, environment, new Object[]{"Malformed template name ", new _DelayedJQuote(e2.getTemplateName()), ":\n", e2.getMalformednessDescription()});
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(' ');
        stringBuffer.append(this.importedTemplateNameExp.getCanonicalForm());
        stringBuffer.append(" as ");
        stringBuffer.append(_CoreStringUtils.toFTLTopLevelTragetIdentifier(this.namespace));
        if (z) {
            stringBuffer.append("/>");
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.importedTemplateNameExp;
        }
        if (i == 1) {
            return this.namespace;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.TEMPLATE_NAME;
        }
        if (i == 1) {
            return ParameterRole.NAMESPACE;
        }
        throw new IndexOutOfBoundsException();
    }

    public String getTemplateName() {
        return this.importedTemplateNameExp.toString();
    }
}
