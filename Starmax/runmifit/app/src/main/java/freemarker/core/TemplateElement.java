package freemarker.core;

import freemarker.template.SimpleSequence;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNodeModel;
import freemarker.template.TemplateSequenceModel;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import javax.swing.tree.TreeNode;

public abstract class TemplateElement extends TemplateObject implements TreeNode {
    private static final int INITIAL_REGULATED_CHILD_BUFFER_CAPACITY = 6;
    private int index;
    private TemplateElement nestedBlock;
    private TemplateElement parent;
    private TemplateElement[] regulatedChildBuffer;
    private int regulatedChildCount;

    /* access modifiers changed from: package-private */
    public abstract void accept(Environment environment) throws TemplateException, IOException;

    /* access modifiers changed from: protected */
    public abstract String dump(boolean z);

    public String getNodeNamespace() {
        return null;
    }

    public String getNodeType() {
        return "element";
    }

    public TemplateNodeModel getParentNode() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean heedsOpeningWhitespace() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean heedsTrailingWhitespace() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isIgnorable() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public abstract boolean isNestedBlockRepeater();

    /* access modifiers changed from: package-private */
    public boolean isOutputCacheable() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isShownInStackTrace() {
        return true;
    }

    public final String getDescription() {
        return dump(false);
    }

    public final String getCanonicalForm() {
        return dump(true);
    }

    public TemplateSequenceModel getChildNodes() {
        if (this.regulatedChildBuffer != null) {
            SimpleSequence simpleSequence = new SimpleSequence(this.regulatedChildCount);
            for (int i = 0; i < this.regulatedChildCount; i++) {
                simpleSequence.add(this.regulatedChildBuffer[i]);
            }
            return simpleSequence;
        }
        SimpleSequence simpleSequence2 = new SimpleSequence(1);
        TemplateElement templateElement = this.nestedBlock;
        if (templateElement != null) {
            simpleSequence2.add(templateElement);
        }
        return simpleSequence2;
    }

    public String getNodeName() {
        String name = getClass().getName();
        return name.substring(name.lastIndexOf(46) + 1);
    }

    public boolean isLeaf() {
        return this.nestedBlock == null && this.regulatedChildCount == 0;
    }

    public boolean getAllowsChildren() {
        return !isLeaf();
    }

    public int getIndex(TreeNode treeNode) {
        TemplateElement templateElement = this.nestedBlock;
        if (templateElement instanceof MixedContent) {
            return templateElement.getIndex(treeNode);
        }
        if (templateElement == null) {
            for (int i = 0; i < this.regulatedChildCount; i++) {
                if (this.regulatedChildBuffer[i].equals(treeNode)) {
                    return i;
                }
            }
            return -1;
        } else if (treeNode == templateElement) {
            return 0;
        } else {
            return -1;
        }
    }

    public int getChildCount() {
        TemplateElement templateElement = this.nestedBlock;
        if (templateElement instanceof MixedContent) {
            return templateElement.getChildCount();
        }
        if (templateElement != null) {
            return 1;
        }
        return this.regulatedChildCount;
    }

    public Enumeration children() {
        TemplateElement templateElement = this.nestedBlock;
        if (templateElement instanceof MixedContent) {
            return templateElement.children();
        }
        if (templateElement != null) {
            return Collections.enumeration(Collections.singletonList(templateElement));
        }
        TemplateElement[] templateElementArr = this.regulatedChildBuffer;
        if (templateElementArr != null) {
            return new _ArrayEnumeration(templateElementArr, this.regulatedChildCount);
        }
        return Collections.enumeration(Collections.EMPTY_LIST);
    }

    public TreeNode getChildAt(int i) {
        TemplateElement templateElement = this.nestedBlock;
        if (templateElement instanceof MixedContent) {
            return templateElement.getChildAt(i);
        }
        if (templateElement != null) {
            if (i == 0) {
                return templateElement;
            }
            throw new ArrayIndexOutOfBoundsException("invalid index");
        } else if (this.regulatedChildCount != 0) {
            try {
                return this.regulatedChildBuffer[i];
            } catch (ArrayIndexOutOfBoundsException unused) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Index: ");
                stringBuffer.append(i);
                stringBuffer.append(", Size: ");
                stringBuffer.append(this.regulatedChildCount);
                throw new IndexOutOfBoundsException(stringBuffer.toString());
            }
        } else {
            throw new ArrayIndexOutOfBoundsException("Template element has no children");
        }
    }

    public void setChildAt(int i, TemplateElement templateElement) {
        TemplateElement templateElement2 = this.nestedBlock;
        if (templateElement2 instanceof MixedContent) {
            templateElement2.setChildAt(i, templateElement);
        } else if (templateElement2 == null) {
            TemplateElement[] templateElementArr = this.regulatedChildBuffer;
            if (templateElementArr != null) {
                templateElementArr[i] = templateElement;
                templateElement.index = i;
                templateElement.parent = this;
                return;
            }
            throw new IndexOutOfBoundsException("element has no children");
        } else if (i == 0) {
            this.nestedBlock = templateElement;
            templateElement.index = 0;
            templateElement.parent = this;
        } else {
            throw new IndexOutOfBoundsException("invalid index");
        }
    }

    public TreeNode getParent() {
        return this.parent;
    }

    /* access modifiers changed from: package-private */
    public final void setRegulatedChildBufferCapacity(int i) {
        int i2 = this.regulatedChildCount;
        TemplateElement[] templateElementArr = new TemplateElement[i];
        for (int i3 = 0; i3 < i2; i3++) {
            templateElementArr[i3] = this.regulatedChildBuffer[i3];
        }
        this.regulatedChildBuffer = templateElementArr;
    }

    /* access modifiers changed from: package-private */
    public final void addRegulatedChild(TemplateElement templateElement) {
        addRegulatedChild(this.regulatedChildCount, templateElement);
    }

    /* access modifiers changed from: package-private */
    public final void addRegulatedChild(int i, TemplateElement templateElement) {
        int i2 = this.regulatedChildCount;
        TemplateElement[] templateElementArr = this.regulatedChildBuffer;
        if (templateElementArr == null) {
            templateElementArr = new TemplateElement[6];
            this.regulatedChildBuffer = templateElementArr;
        } else if (i2 == templateElementArr.length) {
            setRegulatedChildBufferCapacity(i2 != 0 ? i2 * 2 : 1);
            templateElementArr = this.regulatedChildBuffer;
        }
        for (int i3 = i2; i3 > i; i3--) {
            TemplateElement templateElement2 = templateElementArr[i3 - 1];
            templateElement2.index = i3;
            templateElementArr[i3] = templateElement2;
        }
        templateElement.index = i;
        templateElement.parent = this;
        templateElementArr[i] = templateElement;
        this.regulatedChildCount = i2 + 1;
    }

    /* access modifiers changed from: package-private */
    public final int getRegulatedChildCount() {
        return this.regulatedChildCount;
    }

    /* access modifiers changed from: package-private */
    public final TemplateElement getRegulatedChild(int i) {
        return this.regulatedChildBuffer[i];
    }

    /* access modifiers changed from: package-private */
    public final int getIndex() {
        return this.index;
    }

    /* access modifiers changed from: package-private */
    public final TemplateElement getParentElement() {
        return this.parent;
    }

    /* access modifiers changed from: package-private */
    public final TemplateElement getNestedBlock() {
        return this.nestedBlock;
    }

    /* access modifiers changed from: package-private */
    public final void setNestedBlock(TemplateElement templateElement) {
        if (templateElement != null) {
            templateElement.parent = this;
            templateElement.index = 0;
        }
        this.nestedBlock = templateElement;
    }

    /* access modifiers changed from: package-private */
    public final void setFieldsForRootElement() {
        this.index = 0;
        this.parent = null;
    }

    /* access modifiers changed from: package-private */
    public TemplateElement postParseCleanup(boolean z) throws ParseException {
        int i = this.regulatedChildCount;
        if (i != 0) {
            for (int i2 = 0; i2 < i; i2++) {
                TemplateElement postParseCleanup = this.regulatedChildBuffer[i2].postParseCleanup(z);
                this.regulatedChildBuffer[i2] = postParseCleanup;
                postParseCleanup.parent = this;
                postParseCleanup.index = i2;
            }
            if (z) {
                int i3 = 0;
                while (i3 < i) {
                    if (this.regulatedChildBuffer[i3].isIgnorable()) {
                        i--;
                        int i4 = i3;
                        while (i4 < i) {
                            TemplateElement[] templateElementArr = this.regulatedChildBuffer;
                            int i5 = i4 + 1;
                            TemplateElement templateElement = templateElementArr[i5];
                            templateElementArr[i4] = templateElement;
                            templateElement.index = i4;
                            i4 = i5;
                        }
                        this.regulatedChildBuffer[i] = null;
                        this.regulatedChildCount = i;
                        i3--;
                    }
                    i3++;
                }
            }
            TemplateElement[] templateElementArr2 = this.regulatedChildBuffer;
            if (i < templateElementArr2.length && i <= (templateElementArr2.length * 3) / 4) {
                TemplateElement[] templateElementArr3 = new TemplateElement[i];
                for (int i6 = 0; i6 < i; i6++) {
                    templateElementArr3[i6] = this.regulatedChildBuffer[i6];
                }
                this.regulatedChildBuffer = templateElementArr3;
            }
        } else {
            TemplateElement templateElement2 = this.nestedBlock;
            if (templateElement2 != null) {
                this.nestedBlock = templateElement2.postParseCleanup(z);
                if (this.nestedBlock.isIgnorable()) {
                    this.nestedBlock = null;
                } else {
                    this.nestedBlock.parent = this;
                }
            }
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public TemplateElement prevTerminalNode() {
        TemplateElement previousSibling = previousSibling();
        if (previousSibling != null) {
            return previousSibling.getLastLeaf();
        }
        TemplateElement templateElement = this.parent;
        if (templateElement != null) {
            return templateElement.prevTerminalNode();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public TemplateElement nextTerminalNode() {
        TemplateElement nextSibling = nextSibling();
        if (nextSibling != null) {
            return nextSibling.getFirstLeaf();
        }
        TemplateElement templateElement = this.parent;
        if (templateElement != null) {
            return templateElement.nextTerminalNode();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public TemplateElement previousSibling() {
        int i;
        TemplateElement templateElement = this.parent;
        if (templateElement != null && (i = this.index) > 0) {
            return templateElement.regulatedChildBuffer[i - 1];
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public TemplateElement nextSibling() {
        TemplateElement templateElement = this.parent;
        if (templateElement == null) {
            return null;
        }
        int i = this.index;
        if (i + 1 < templateElement.regulatedChildCount) {
            return templateElement.regulatedChildBuffer[i + 1];
        }
        return null;
    }

    private TemplateElement getFirstChild() {
        TemplateElement templateElement = this.nestedBlock;
        if (templateElement != null) {
            return templateElement;
        }
        if (this.regulatedChildCount == 0) {
            return null;
        }
        return this.regulatedChildBuffer[0];
    }

    private TemplateElement getLastChild() {
        TemplateElement templateElement = this.nestedBlock;
        if (templateElement != null) {
            return templateElement;
        }
        int i = this.regulatedChildCount;
        if (i == 0) {
            return null;
        }
        return this.regulatedChildBuffer[i - 1];
    }

    private TemplateElement getFirstLeaf() {
        TemplateElement templateElement = this;
        while (!templateElement.isLeaf() && !(templateElement instanceof Macro) && !(templateElement instanceof BlockAssignment)) {
            templateElement = templateElement.getFirstChild();
        }
        return templateElement;
    }

    private TemplateElement getLastLeaf() {
        TemplateElement templateElement = this;
        while (!templateElement.isLeaf() && !(templateElement instanceof Macro) && !(templateElement instanceof BlockAssignment)) {
            templateElement = templateElement.getLastChild();
        }
        return templateElement;
    }
}
