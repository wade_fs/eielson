package freemarker.core;

import android.support.v4.app.NotificationCompat;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.internal.ImagesContract;
import freemarker.template.Template;
import freemarker.template.TemplateDirectiveBody;
import java.io.Writer;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import org.greenrobot.greendao.generator.Schema;

public class _CoreAPI {
    public static final Set BUILT_IN_DIRECTIVE_NAMES;
    public static final String ERROR_MESSAGE_HR = "----";

    private _CoreAPI() {
    }

    static {
        TreeSet treeSet = new TreeSet();
        treeSet.add("assign");
        treeSet.add("attempt");
        treeSet.add("break");
        treeSet.add(NotificationCompat.CATEGORY_CALL);
        treeSet.add("case");
        treeSet.add("comment");
        treeSet.add("compress");
        treeSet.add(Schema.DEFAULT_NAME);
        treeSet.add("else");
        treeSet.add("elseif");
        treeSet.add("elseIf");
        treeSet.add("escape");
        treeSet.add("fallback");
        treeSet.add("flush");
        treeSet.add("foreach");
        treeSet.add("forEach");
        treeSet.add("ftl");
        treeSet.add("function");
        treeSet.add("global");
        treeSet.add("if");
        treeSet.add("import");
        treeSet.add("include");
        treeSet.add("items");
        treeSet.add("list");
        treeSet.add(ImagesContract.LOCAL);
        treeSet.add("lt");
        treeSet.add("macro");
        treeSet.add("nested");
        treeSet.add("noescape");
        treeSet.add("noEscape");
        treeSet.add("noparse");
        treeSet.add("noParse");
        treeSet.add("nt");
        treeSet.add("recover");
        treeSet.add("recurse");
        treeSet.add("return");
        treeSet.add("rt");
        treeSet.add("sep");
        treeSet.add("setting");
        treeSet.add("stop");
        treeSet.add("switch");
        treeSet.add("t");
        treeSet.add("transform");
        treeSet.add(Config.TRACE_VISIT);
        BUILT_IN_DIRECTIVE_NAMES = Collections.unmodifiableSet(treeSet);
    }

    public static Set getSupportedBuiltInNames() {
        return Collections.unmodifiableSet(BuiltIn.builtins.keySet());
    }

    public static void appendInstructionStackItem(TemplateElement templateElement, StringBuffer stringBuffer) {
        Environment.appendInstructionStackItem(templateElement, stringBuffer);
    }

    public static TemplateElement[] getInstructionStackSnapshot(Environment environment) {
        return environment.getInstructionStackSnapshot();
    }

    public static void outputInstructionStack(TemplateElement[] templateElementArr, boolean z, Writer writer) {
        Environment.outputInstructionStack(templateElementArr, z, writer);
    }

    public static Set getConfigurableSettingNames(Configurable configurable, boolean z) {
        return configurable.getSettingNames(z);
    }

    public static final void addThreadInterruptedChecks(Template template) {
        try {
            new ThreadInterruptionSupportTemplatePostProcessor().postProcess(template);
        } catch (TemplatePostProcessorException e) {
            throw new RuntimeException("Template post-processing failed", e);
        }
    }

    public static final void checkHasNoNestedContent(TemplateDirectiveBody templateDirectiveBody) throws NestedContentNotSupportedException {
        NestedContentNotSupportedException.check(templateDirectiveBody);
    }
}
