package freemarker.core;

public class CallPlaceCustomDataInitializationException extends Exception {
    public CallPlaceCustomDataInitializationException(String str, Throwable th) {
        super(str, th);
    }
}
