package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.template.Template;
import freemarker.template.utility.SecurityUtilities;
import freemarker.template.utility.StringUtil;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ParseException extends IOException implements FMParserConstants {
    static /* synthetic */ Class class$freemarker$core$ParseException;
    private static volatile Boolean jbossToolsMode;
    public int columnNumber;
    public Token currentToken;
    private String description;
    public int endColumnNumber;
    public int endLineNumber;
    protected String eol;
    public int[][] expectedTokenSequences;
    public int lineNumber;
    private String message;
    private boolean messageAndDescriptionRendered;
    protected boolean specialConstructor;
    private String templateName;
    public String[] tokenImage;

    public ParseException(Token token, int[][] iArr, String[] strArr) {
        super("");
        this.eol = SecurityUtilities.getSystemProperty("line.separator", "\n");
        this.currentToken = token;
        this.specialConstructor = true;
        this.expectedTokenSequences = iArr;
        this.tokenImage = strArr;
        this.lineNumber = this.currentToken.next.beginLine;
        this.columnNumber = this.currentToken.next.beginColumn;
        this.endLineNumber = this.currentToken.next.endLine;
        this.endColumnNumber = this.currentToken.next.endColumn;
    }

    protected ParseException() {
        this.eol = SecurityUtilities.getSystemProperty("line.separator", "\n");
    }

    public ParseException(String str, int i, int i2) {
        this(str, null, i, i2, null);
    }

    public ParseException(String str, Template template, int i, int i2, int i3, int i4) {
        this(str, template, i, i2, i3, i4, (Throwable) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ParseException(String str, Template template, int i, int i2, int i3, int i4, Throwable th) {
        this(str, template == null ? null : template.getSourceName(), i, i2, i3, i4, th);
    }

    public ParseException(String str, Template template, int i, int i2) {
        this(str, template, i, i2, null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ParseException(String str, Template template, int i, int i2, Throwable th) {
        this(str, template == null ? null : template.getSourceName(), i, i2, 0, 0, th);
    }

    public ParseException(String str, Template template, Token token) {
        this(str, template, token, (Throwable) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ParseException(String str, Template template, Token token, Throwable th) {
        this(str, template == null ? null : template.getSourceName(), token.beginLine, token.beginColumn, token.endLine, token.endColumn, th);
    }

    public ParseException(String str, TemplateObject templateObject) {
        this(str, templateObject, (Throwable) null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ParseException(String str, TemplateObject templateObject, Throwable th) {
        this(str, templateObject.getTemplate() == null ? null : templateObject.getTemplate().getSourceName(), templateObject.beginLine, templateObject.beginColumn, templateObject.endLine, templateObject.endColumn, th);
    }

    private ParseException(String str, String str2, int i, int i2, int i3, int i4, Throwable th) {
        super(str);
        this.eol = SecurityUtilities.getSystemProperty("line.separator", "\n");
        this.description = str;
        this.templateName = str2;
        this.lineNumber = i;
        this.columnNumber = i2;
        this.endLineNumber = i3;
        this.endColumnNumber = i4;
    }

    public void setTemplateName(String str) {
        this.templateName = str;
        synchronized (this) {
            this.messageAndDescriptionRendered = false;
            this.message = null;
        }
    }

    public String getMessage() {
        String str;
        synchronized (this) {
            if (this.messageAndDescriptionRendered) {
                String str2 = this.message;
                return str2;
            }
            renderMessageAndDescription();
            synchronized (this) {
                str = this.message;
            }
            return str;
        }
    }

    private String getDescription() {
        String str;
        synchronized (this) {
            if (this.messageAndDescriptionRendered) {
                String str2 = this.description;
                return str2;
            }
            renderMessageAndDescription();
            synchronized (this) {
                str = this.description;
            }
            return str;
        }
    }

    public String getEditorMessage() {
        return getDescription();
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public int getLineNumber() {
        return this.lineNumber;
    }

    public int getColumnNumber() {
        return this.columnNumber;
    }

    public int getEndLineNumber() {
        return this.endLineNumber;
    }

    public int getEndColumnNumber() {
        return this.endColumnNumber;
    }

    private void renderMessageAndDescription() {
        String str;
        String orRenderDescription = getOrRenderDescription();
        if (!isInJBossToolsMode()) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Syntax error ");
            stringBuffer.append(MessageUtil.formatLocationForSimpleParsingError(this.templateName, this.lineNumber, this.columnNumber));
            stringBuffer.append(":\n");
            str = stringBuffer.toString();
        } else {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("[col. ");
            stringBuffer2.append(this.columnNumber);
            stringBuffer2.append("] ");
            str = stringBuffer2.toString();
        }
        StringBuffer stringBuffer3 = new StringBuffer();
        stringBuffer3.append(str);
        stringBuffer3.append(orRenderDescription);
        String stringBuffer4 = stringBuffer3.toString();
        String substring = stringBuffer4.substring(str.length());
        synchronized (this) {
            this.message = stringBuffer4;
            this.description = substring;
            this.messageAndDescriptionRendered = true;
        }
    }

    private boolean isInJBossToolsMode() {
        Class cls;
        if (jbossToolsMode == null) {
            try {
                if (class$freemarker$core$ParseException == null) {
                    cls = class$("freemarker.core.ParseException");
                    class$freemarker$core$ParseException = cls;
                } else {
                    cls = class$freemarker$core$ParseException;
                }
                jbossToolsMode = Boolean.valueOf(cls.getClassLoader().toString().indexOf("[org.jboss.ide.eclipse.freemarker:") != -1);
            } catch (Throwable unused) {
                jbossToolsMode = Boolean.FALSE;
            }
        }
        return jbossToolsMode.booleanValue();
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x000e, code lost:
        r0 = getCustomTokenErrorDescription();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0012, code lost:
        if (r0 != null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0014, code lost:
        r0 = new java.lang.StringBuffer();
        r2 = 0;
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001f, code lost:
        if (r2 >= r7.expectedTokenSequences.length) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0021, code lost:
        if (r2 == 0) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0023, code lost:
        r0.append(r7.eol);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0028, code lost:
        r0.append("    ");
        r4 = r7.expectedTokenSequences;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0032, code lost:
        if (r3 >= r4[r2].length) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0034, code lost:
        r3 = r4[r2].length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0037, code lost:
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003d, code lost:
        if (r4 >= r7.expectedTokenSequences[r2].length) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003f, code lost:
        if (r4 == 0) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0041, code lost:
        r0.append(' ');
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0046, code lost:
        r0.append(r7.tokenImage[r7.expectedTokenSequences[r2][r4]]);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0056, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0059, code lost:
        r4 = "Encountered \"";
        r5 = r7.currentToken.next;
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0061, code lost:
        if (r2 >= r3) goto L_0x00a8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0063, code lost:
        if (r2 == 0) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0065, code lost:
        r6 = new java.lang.StringBuffer();
        r6.append(r4);
        r6.append(" ");
        r4 = r6.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0078, code lost:
        if (r5.kind != 0) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007a, code lost:
        r2 = new java.lang.StringBuffer();
        r2.append(r4);
        r2.append(r7.tokenImage[0]);
        r4 = r2.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008e, code lost:
        r6 = new java.lang.StringBuffer();
        r6.append(r4);
        r6.append(add_escapes(r5.image));
        r4 = r6.toString();
        r5 = r5.next;
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a8, code lost:
        r1 = new java.lang.StringBuffer();
        r1.append(r4);
        r1.append("\", but ");
        r1 = r1.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00bd, code lost:
        if (r7.expectedTokenSequences.length != 1) goto L_0x00d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00bf, code lost:
        r2 = new java.lang.StringBuffer();
        r2.append(r1);
        r2.append("was expecting:");
        r2.append(r7.eol);
        r1 = r2.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00d6, code lost:
        r2 = new java.lang.StringBuffer();
        r2.append(r1);
        r2.append("was expecting one of:");
        r2.append(r7.eol);
        r1 = r2.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00ec, code lost:
        r2 = new java.lang.StringBuffer();
        r2.append(r1);
        r2.append((java.lang.Object) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00fc, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return r2.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000c, code lost:
        if (r7.currentToken == null) goto L_0x00fc;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getOrRenderDescription() {
        /*
            r7 = this;
            monitor-enter(r7)
            java.lang.String r0 = r7.description     // Catch:{ all -> 0x00fe }
            if (r0 == 0) goto L_0x0009
            java.lang.String r0 = r7.description     // Catch:{ all -> 0x00fe }
            monitor-exit(r7)     // Catch:{ all -> 0x00fe }
            return r0
        L_0x0009:
            monitor-exit(r7)     // Catch:{ all -> 0x00fe }
            freemarker.core.Token r0 = r7.currentToken
            if (r0 == 0) goto L_0x00fc
            java.lang.String r0 = r7.getCustomTokenErrorDescription()
            if (r0 != 0) goto L_0x00fd
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r1 = 0
            r2 = 0
            r3 = 0
        L_0x001c:
            int[][] r4 = r7.expectedTokenSequences
            int r4 = r4.length
            if (r2 >= r4) goto L_0x0059
            if (r2 == 0) goto L_0x0028
            java.lang.String r4 = r7.eol
            r0.append(r4)
        L_0x0028:
            java.lang.String r4 = "    "
            r0.append(r4)
            int[][] r4 = r7.expectedTokenSequences
            r5 = r4[r2]
            int r5 = r5.length
            if (r3 >= r5) goto L_0x0037
            r3 = r4[r2]
            int r3 = r3.length
        L_0x0037:
            r4 = 0
        L_0x0038:
            int[][] r5 = r7.expectedTokenSequences
            r5 = r5[r2]
            int r5 = r5.length
            if (r4 >= r5) goto L_0x0056
            if (r4 == 0) goto L_0x0046
            r5 = 32
            r0.append(r5)
        L_0x0046:
            java.lang.String[] r5 = r7.tokenImage
            int[][] r6 = r7.expectedTokenSequences
            r6 = r6[r2]
            r6 = r6[r4]
            r5 = r5[r6]
            r0.append(r5)
            int r4 = r4 + 1
            goto L_0x0038
        L_0x0056:
            int r2 = r2 + 1
            goto L_0x001c
        L_0x0059:
            freemarker.core.Token r2 = r7.currentToken
            freemarker.core.Token r2 = r2.next
            java.lang.String r4 = "Encountered \""
            r5 = r2
            r2 = 0
        L_0x0061:
            if (r2 >= r3) goto L_0x00a8
            if (r2 == 0) goto L_0x0076
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            r6.append(r4)
            java.lang.String r4 = " "
            r6.append(r4)
            java.lang.String r4 = r6.toString()
        L_0x0076:
            int r6 = r5.kind
            if (r6 != 0) goto L_0x008e
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            r2.append(r4)
            java.lang.String[] r3 = r7.tokenImage
            r1 = r3[r1]
            r2.append(r1)
            java.lang.String r4 = r2.toString()
            goto L_0x00a8
        L_0x008e:
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            r6.append(r4)
            java.lang.String r4 = r5.image
            java.lang.String r4 = r7.add_escapes(r4)
            r6.append(r4)
            java.lang.String r4 = r6.toString()
            freemarker.core.Token r5 = r5.next
            int r2 = r2 + 1
            goto L_0x0061
        L_0x00a8:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            r1.append(r4)
            java.lang.String r2 = "\", but "
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            int[][] r2 = r7.expectedTokenSequences
            int r2 = r2.length
            r3 = 1
            if (r2 != r3) goto L_0x00d6
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            r2.append(r1)
            java.lang.String r1 = "was expecting:"
            r2.append(r1)
            java.lang.String r1 = r7.eol
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            goto L_0x00ec
        L_0x00d6:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            r2.append(r1)
            java.lang.String r1 = "was expecting one of:"
            r2.append(r1)
            java.lang.String r1 = r7.eol
            r2.append(r1)
            java.lang.String r1 = r2.toString()
        L_0x00ec:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            r2.append(r1)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            goto L_0x00fd
        L_0x00fc:
            r0 = 0
        L_0x00fd:
            return r0
        L_0x00fe:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00fe }
            goto L_0x0102
        L_0x0101:
            throw r0
        L_0x0102:
            goto L_0x0101
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.ParseException.getOrRenderDescription():java.lang.String");
    }

    private String getCustomTokenErrorDescription() {
        String str;
        Token token = this.currentToken.next;
        int i = token.kind;
        if (i == 0) {
            HashSet hashSet = new HashSet();
            int i2 = 0;
            while (true) {
                int[][] iArr = this.expectedTokenSequences;
                if (i2 < iArr.length) {
                    int[] iArr2 = iArr[i2];
                    for (int i3 : iArr2) {
                        if (i3 == 33) {
                            hashSet.add("#if");
                        } else if (i3 == 34) {
                            hashSet.add("#list");
                        } else if (i3 == 64) {
                            hashSet.add("#escape");
                        } else if (i3 == 66) {
                            hashSet.add("#noescape");
                        } else if (i3 == 68) {
                            hashSet.add("@...");
                        } else if (i3 == 125) {
                            hashSet.add("\"[\"");
                        } else if (i3 == 127) {
                            hashSet.add("\"(\"");
                        } else if (i3 != 129) {
                            switch (i3) {
                                case 38:
                                    hashSet.add("#attempt");
                                    break;
                                case 39:
                                    hashSet.add("#foreach");
                                    break;
                                case 40:
                                    hashSet.add("#local");
                                    break;
                                case 41:
                                    hashSet.add("#global");
                                    break;
                                case 42:
                                    hashSet.add("#assign");
                                    break;
                                case 44:
                                    hashSet.add("#macro");
                                case 43:
                                    hashSet.add("#function");
                                    break;
                                case 45:
                                    hashSet.add("#compress");
                                    break;
                                case 46:
                                    hashSet.add("#transform");
                                    break;
                                case 47:
                                    hashSet.add("#switch");
                                    break;
                            }
                        } else {
                            hashSet.add("\"{\"");
                        }
                    }
                    i2++;
                } else {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Unexpected end of file reached.");
                    if (hashSet.size() == 0) {
                        str = "";
                    } else {
                        StringBuffer stringBuffer2 = new StringBuffer();
                        stringBuffer2.append(" You have an unclosed ");
                        stringBuffer2.append(concatWithOrs(hashSet));
                        stringBuffer2.append(FileUtil.HIDDEN_PREFIX);
                        str = stringBuffer2.toString();
                    }
                    stringBuffer.append(str);
                    return stringBuffer.toString();
                }
            }
        } else if (i == 48) {
            return "Unexpected directive, \"#else\". Check if you have a valid #if-#elseif-#else or #list-#else structure.";
        } else {
            if (i != 33 && i != 9) {
                return null;
            }
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append("Unexpected directive, ");
            stringBuffer3.append(StringUtil.jQuote(token));
            stringBuffer3.append(". Check if you have a valid #if-#elseif-#else structure.");
            return stringBuffer3.toString();
        }
    }

    private String concatWithOrs(Set set) {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (stringBuffer.length() != 0) {
                stringBuffer.append(" or ");
            }
            stringBuffer.append(str);
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public String add_escapes(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != 0) {
                if (charAt == '\"') {
                    stringBuffer.append("\\\"");
                } else if (charAt == '\'') {
                    stringBuffer.append("\\'");
                } else if (charAt == '\\') {
                    stringBuffer.append("\\\\");
                } else if (charAt == 12) {
                    stringBuffer.append("\\f");
                } else if (charAt != 13) {
                    switch (charAt) {
                        case 8:
                            stringBuffer.append("\\b");
                            continue;
                        case 9:
                            stringBuffer.append("\\t");
                            continue;
                        case 10:
                            stringBuffer.append("\\n");
                            continue;
                        default:
                            char charAt2 = str.charAt(i);
                            if (charAt2 < ' ' || charAt2 > '~') {
                                StringBuffer stringBuffer2 = new StringBuffer();
                                stringBuffer2.append("0000");
                                stringBuffer2.append(Integer.toString(charAt2, 16));
                                String stringBuffer3 = stringBuffer2.toString();
                                StringBuffer stringBuffer4 = new StringBuffer();
                                stringBuffer4.append("\\u");
                                stringBuffer4.append(stringBuffer3.substring(stringBuffer3.length() - 4, stringBuffer3.length()));
                                stringBuffer.append(stringBuffer4.toString());
                                break;
                            } else {
                                stringBuffer.append(charAt2);
                                continue;
                            }
                    }
                } else {
                    stringBuffer.append("\\r");
                }
            }
        }
        return stringBuffer.toString();
    }
}
