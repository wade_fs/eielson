package freemarker.core;

import freemarker.core.IteratorBlock;
import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.util.List;

class BuiltInsForLoopVariables {
    BuiltInsForLoopVariables() {
    }

    static class indexBI extends BuiltInForLoopVariable {
        indexBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(IteratorBlock.IterationContext iterationContext, Environment environment) throws TemplateException {
            return new SimpleNumber(iterationContext.getIndex());
        }
    }

    static class counterBI extends BuiltInForLoopVariable {
        counterBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(IteratorBlock.IterationContext iterationContext, Environment environment) throws TemplateException {
            return new SimpleNumber(iterationContext.getIndex() + 1);
        }
    }

    static abstract class BooleanBuiltInForLoopVariable extends BuiltInForLoopVariable {
        /* access modifiers changed from: protected */
        public abstract boolean calculateBooleanResult(IteratorBlock.IterationContext iterationContext, Environment environment);

        BooleanBuiltInForLoopVariable() {
        }

        /* access modifiers changed from: package-private */
        public final TemplateModel calculateResult(IteratorBlock.IterationContext iterationContext, Environment environment) throws TemplateException {
            return calculateBooleanResult(iterationContext, environment) ? TemplateBooleanModel.TRUE : TemplateBooleanModel.FALSE;
        }
    }

    static class has_nextBI extends BooleanBuiltInForLoopVariable {
        has_nextBI() {
        }

        /* access modifiers changed from: protected */
        public boolean calculateBooleanResult(IteratorBlock.IterationContext iterationContext, Environment environment) {
            return iterationContext.hasNext();
        }
    }

    static class is_lastBI extends BooleanBuiltInForLoopVariable {
        is_lastBI() {
        }

        /* access modifiers changed from: protected */
        public boolean calculateBooleanResult(IteratorBlock.IterationContext iterationContext, Environment environment) {
            return !iterationContext.hasNext();
        }
    }

    static class is_firstBI extends BooleanBuiltInForLoopVariable {
        is_firstBI() {
        }

        /* access modifiers changed from: protected */
        public boolean calculateBooleanResult(IteratorBlock.IterationContext iterationContext, Environment environment) {
            return iterationContext.getIndex() == 0;
        }
    }

    static class is_odd_itemBI extends BooleanBuiltInForLoopVariable {
        is_odd_itemBI() {
        }

        /* access modifiers changed from: protected */
        public boolean calculateBooleanResult(IteratorBlock.IterationContext iterationContext, Environment environment) {
            return iterationContext.getIndex() % 2 == 0;
        }
    }

    static class is_even_itemBI extends BooleanBuiltInForLoopVariable {
        is_even_itemBI() {
        }

        /* access modifiers changed from: protected */
        public boolean calculateBooleanResult(IteratorBlock.IterationContext iterationContext, Environment environment) {
            return iterationContext.getIndex() % 2 != 0;
        }
    }

    static class item_parityBI extends BuiltInForLoopVariable {
        private static final SimpleScalar EVEN = new SimpleScalar("even");
        private static final SimpleScalar ODD = new SimpleScalar("odd");

        item_parityBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(IteratorBlock.IterationContext iterationContext, Environment environment) throws TemplateException {
            return iterationContext.getIndex() % 2 == 0 ? ODD : EVEN;
        }
    }

    static class item_parity_capBI extends BuiltInForLoopVariable {
        private static final SimpleScalar EVEN = new SimpleScalar("Even");
        private static final SimpleScalar ODD = new SimpleScalar("Odd");

        item_parity_capBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(IteratorBlock.IterationContext iterationContext, Environment environment) throws TemplateException {
            return iterationContext.getIndex() % 2 == 0 ? ODD : EVEN;
        }
    }

    static class item_cycleBI extends BuiltInForLoopVariable {
        item_cycleBI() {
        }

        private class BIMethod implements TemplateMethodModelEx {
            private final IteratorBlock.IterationContext iterCtx;

            private BIMethod(IteratorBlock.IterationContext iterationContext) {
                this.iterCtx = iterationContext;
            }

            public Object exec(List list) throws TemplateModelException {
                item_cycleBI.this.checkMethodArgCount(list, 1, Integer.MAX_VALUE);
                return list.get(this.iterCtx.getIndex() % list.size());
            }
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(IteratorBlock.IterationContext iterationContext, Environment environment) throws TemplateException {
            return new BIMethod(iterationContext);
        }
    }
}
