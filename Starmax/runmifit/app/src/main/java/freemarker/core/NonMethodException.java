package freemarker.core;

import freemarker.template.TemplateModel;

public class NonMethodException extends UnexpectedTypeException {
    private static final Class[] EXPECTED_TYPES;
    static /* synthetic */ Class class$freemarker$template$TemplateMethodModel;

    static {
        Class[] clsArr = new Class[1];
        Class cls = class$freemarker$template$TemplateMethodModel;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateMethodModel");
            class$freemarker$template$TemplateMethodModel = cls;
        }
        clsArr[0] = cls;
        EXPECTED_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonMethodException(Environment environment) {
        super(environment, "Expecting method value here");
    }

    public NonMethodException(String str, Environment environment) {
        super(environment, str);
    }

    NonMethodException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(environment, _errordescriptionbuilder);
    }

    NonMethodException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "method", EXPECTED_TYPES, environment);
    }

    NonMethodException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "method", EXPECTED_TYPES, str, environment);
    }

    NonMethodException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "method", EXPECTED_TYPES, strArr, environment);
    }
}
