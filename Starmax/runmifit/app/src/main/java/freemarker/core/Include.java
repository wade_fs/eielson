package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateScalarModel;
import freemarker.template.utility.StringUtil;
import java.io.IOException;

final class Include extends TemplateElement {
    private final String encoding;
    private final Expression encodingExp;
    private final Expression ignoreMissingExp;
    private final Boolean ignoreMissingExpPrecalcedValue;
    private final Expression includedTemplateNameExp;
    private final Boolean parse;
    private final Expression parseExp;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#include";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 3;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    Include(Template template, Expression expression, Expression expression2, Expression expression3, Expression expression4) throws ParseException {
        this.includedTemplateNameExp = expression;
        this.encodingExp = expression2;
        if (expression2 == null) {
            this.encoding = null;
        } else if (expression2.isLiteral()) {
            try {
                TemplateModel eval = expression2.eval(null);
                if (eval instanceof TemplateScalarModel) {
                    this.encoding = ((TemplateScalarModel) eval).getAsString();
                } else {
                    throw new ParseException("Expected a string as the value of the \"encoding\" argument", expression2);
                }
            } catch (TemplateException e) {
                throw new BugException(e);
            }
        } else {
            this.encoding = null;
        }
        this.parseExp = expression3;
        if (expression3 == null) {
            this.parse = Boolean.TRUE;
        } else if (expression3.isLiteral()) {
            try {
                if (expression3 instanceof StringLiteral) {
                    this.parse = Boolean.valueOf(StringUtil.getYesNo(expression3.evalAndCoerceToString(null)));
                } else {
                    this.parse = Boolean.valueOf(expression3.evalToBoolean(template.getConfiguration()));
                }
            } catch (NonBooleanException e2) {
                throw new ParseException("Expected a boolean or string as the value of the parse attribute", expression3, e2);
            } catch (TemplateException e3) {
                throw new BugException(e3);
            }
        } else {
            this.parse = null;
        }
        this.ignoreMissingExp = expression4;
        if (expression4 == null || !expression4.isLiteral()) {
            this.ignoreMissingExpPrecalcedValue = null;
            return;
        }
        try {
            this.ignoreMissingExpPrecalcedValue = Boolean.valueOf(expression4.evalToBoolean(template.getConfiguration()));
        } catch (NonBooleanException e4) {
            throw new ParseException("Expected a boolean as the value of the \"ignore_missing\" attribute", expression4, e4);
        } catch (TemplateException e5) {
            throw new BugException(e5);
        }
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        boolean z;
        boolean z2;
        String evalAndCoerceToString = this.includedTemplateNameExp.evalAndCoerceToString(environment);
        try {
            String fullTemplateName = environment.toFullTemplateName(getTemplate().getName(), evalAndCoerceToString);
            String str = this.encoding;
            if (str == null) {
                Expression expression = this.encodingExp;
                str = expression != null ? expression.evalAndCoerceToString(environment) : null;
            }
            Boolean bool = this.parse;
            if (bool != null) {
                z = bool.booleanValue();
            } else {
                TemplateModel eval = this.parseExp.eval(environment);
                if (eval instanceof TemplateScalarModel) {
                    Expression expression2 = this.parseExp;
                    z = getYesNo(expression2, EvalUtil.modelToString((TemplateScalarModel) eval, expression2, environment));
                } else {
                    z = this.parseExp.modelToBoolean(eval, environment);
                }
            }
            Boolean bool2 = this.ignoreMissingExpPrecalcedValue;
            if (bool2 != null) {
                z2 = bool2.booleanValue();
            } else {
                Expression expression3 = this.ignoreMissingExp;
                z2 = expression3 != null ? expression3.evalToBoolean(environment) : false;
            }
            try {
                Template templateForInclusion = environment.getTemplateForInclusion(fullTemplateName, str, z, z2);
                if (templateForInclusion != null) {
                    environment.include(templateForInclusion);
                }
            } catch (IOException e) {
                throw new _MiscTemplateException(e, environment, new Object[]{"Template inclusion failed (for parameter value ", new _DelayedJQuote(evalAndCoerceToString), "):\n", new _DelayedGetMessage(e)});
            }
        } catch (MalformedTemplateNameException e2) {
            throw new _MiscTemplateException(e2, environment, new Object[]{"Malformed template name ", new _DelayedJQuote(e2.getTemplateName()), ":\n", e2.getMalformednessDescription()});
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(' ');
        stringBuffer.append(this.includedTemplateNameExp.getCanonicalForm());
        if (this.encodingExp != null) {
            stringBuffer.append(" encoding=");
            stringBuffer.append(this.encodingExp.getCanonicalForm());
        }
        if (this.parseExp != null) {
            stringBuffer.append(" parse=");
            stringBuffer.append(this.parseExp.getCanonicalForm());
        }
        if (this.ignoreMissingExp != null) {
            stringBuffer.append(" ignore_missing=");
            stringBuffer.append(this.ignoreMissingExp.getCanonicalForm());
        }
        if (z) {
            stringBuffer.append("/>");
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        if (i == 0) {
            return this.includedTemplateNameExp;
        }
        if (i == 1) {
            return this.parseExp;
        }
        if (i == 2) {
            return this.encodingExp;
        }
        if (i == 3) {
            return this.ignoreMissingExp;
        }
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.TEMPLATE_NAME;
        }
        if (i == 1) {
            return ParameterRole.PARSE_PARAMETER;
        }
        if (i == 2) {
            return ParameterRole.ENCODING_PARAMETER;
        }
        if (i == 3) {
            return ParameterRole.IGNORE_MISSING_PARAMETER;
        }
        throw new IndexOutOfBoundsException();
    }

    private boolean getYesNo(Expression expression, String str) throws TemplateException {
        try {
            return StringUtil.getYesNo(str);
        } catch (IllegalArgumentException unused) {
            throw new _MiscTemplateException(expression, new Object[]{"Value must be boolean (or one of these strings: \"n\", \"no\", \"f\", \"false\", \"y\", \"yes\", \"t\", \"true\"), but it was ", new _DelayedJQuote(str), FileUtil.HIDDEN_PREFIX});
        }
    }
}
