package freemarker.core;

import freemarker.template.utility.DateUtil;
import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

final class ISOTemplateDateFormat extends ISOLikeTemplateDateFormat {
    /* access modifiers changed from: protected */
    public String getDateDescription() {
        return "ISO 8601 (subset) date";
    }

    /* access modifiers changed from: protected */
    public String getDateTimeDescription() {
        return "ISO 8601 (subset) date-time";
    }

    /* access modifiers changed from: protected */
    public String getTimeDescription() {
        return "ISO 8601 (subset) time";
    }

    /* access modifiers changed from: protected */
    public boolean isXSMode() {
        return false;
    }

    ISOTemplateDateFormat(String str, int i, int i2, boolean z, TimeZone timeZone, ISOLikeTemplateDateFormatFactory iSOLikeTemplateDateFormatFactory) throws ParseException, UnknownDateTypeFormattingUnsupportedException {
        super(str, i, i2, z, timeZone, iSOLikeTemplateDateFormatFactory);
    }

    /* access modifiers changed from: protected */
    public String format(Date date, boolean z, boolean z2, boolean z3, int i, TimeZone timeZone, DateUtil.DateToISO8601CalendarFactory dateToISO8601CalendarFactory) {
        return DateUtil.dateToISO8601String(date, z, z2, z2 && z3, i, timeZone, dateToISO8601CalendarFactory);
    }

    /* access modifiers changed from: protected */
    public Date parseDate(String str, TimeZone timeZone, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException {
        return DateUtil.parseISO8601Date(str, timeZone, calendarFieldsToDateConverter);
    }

    /* access modifiers changed from: protected */
    public Date parseTime(String str, TimeZone timeZone, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException {
        return DateUtil.parseISO8601Time(str, timeZone, calendarFieldsToDateConverter);
    }

    /* access modifiers changed from: protected */
    public Date parseDateTime(String str, TimeZone timeZone, DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter) throws DateUtil.DateParseException {
        return DateUtil.parseISO8601DateTime(str, timeZone, calendarFieldsToDateConverter);
    }
}
