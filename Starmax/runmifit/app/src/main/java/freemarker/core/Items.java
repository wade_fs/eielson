package freemarker.core;

import freemarker.core.IteratorBlock;
import freemarker.template.TemplateException;
import java.io.IOException;

class Items extends TemplateElement {
    private final String loopVarName;

    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#items";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return true;
    }

    public Items(String str, TemplateElement templateElement) {
        this.loopVarName = str;
        setNestedBlock(super);
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        IteratorBlock.IterationContext findEnclosingIterationContext = IteratorBlock.findEnclosingIterationContext(environment, null);
        if (findEnclosingIterationContext != null) {
            findEnclosingIterationContext.loopForItemsElement(environment, getNestedBlock(), this.loopVarName);
        } else {
            throw new _MiscTemplateException(environment, new Object[]{getNodeTypeSymbol(), " without iteraton in context"});
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        stringBuffer.append(" as ");
        stringBuffer.append(this.loopVarName);
        if (z) {
            stringBuffer.append('>');
            if (getNestedBlock() != null) {
                stringBuffer.append(getNestedBlock().getCanonicalForm());
            }
            stringBuffer.append("</");
            stringBuffer.append(getNodeTypeSymbol());
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        return this.loopVarName;
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        if (i == 0) {
            return ParameterRole.TARGET_LOOP_VARIABLE;
        }
        throw new IndexOutOfBoundsException();
    }
}
