package freemarker.core;

import freemarker.template.TemplateDateModel;
import freemarker.template.TemplateModelException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class JavaTemplateDateFormat extends TemplateDateFormat {
    private final DateFormat javaDateFormat;

    public boolean isLocaleBound() {
        return true;
    }

    public JavaTemplateDateFormat(DateFormat dateFormat) {
        this.javaDateFormat = dateFormat;
    }

    public String format(TemplateDateModel templateDateModel) throws TemplateModelException {
        return this.javaDateFormat.format(templateDateModel.getAsDate());
    }

    public Date parse(String str) throws ParseException {
        return this.javaDateFormat.parse(str);
    }

    public String getDescription() {
        DateFormat dateFormat = this.javaDateFormat;
        if (dateFormat instanceof SimpleDateFormat) {
            return ((SimpleDateFormat) dateFormat).toPattern();
        }
        return dateFormat.toString();
    }
}
