package freemarker.core;

import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

abstract class BuiltInForString extends BuiltIn {
    /* access modifiers changed from: package-private */
    public abstract TemplateModel calculateResult(String str, Environment environment) throws TemplateException;

    BuiltInForString() {
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        return calculateResult(this.target.evalAndCoerceToString(environment), environment);
    }
}
