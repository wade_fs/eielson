package freemarker.core;

import freemarker.ext.beans._MethodUtil;
import freemarker.log.Logger;
import freemarker.template.Template;
import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.StringUtil;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

public class _ErrorDescriptionBuilder {
    private static final Logger LOG = Logger.getLogger("freemarker.runtime");
    private Expression blamed;
    private final String description;
    private final Object[] descriptionParts;
    private boolean showBlamer;
    private Template template;
    private Object tip;
    private Object[] tips;

    public _ErrorDescriptionBuilder(String str) {
        this.description = str;
        this.descriptionParts = null;
    }

    public _ErrorDescriptionBuilder(Object[] objArr) {
        this.descriptionParts = objArr;
        this.description = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.core._ErrorDescriptionBuilder.toString(freemarker.core.TemplateElement, boolean):java.lang.String
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      freemarker.core._ErrorDescriptionBuilder.toString(java.lang.Object, boolean):java.lang.String
      freemarker.core._ErrorDescriptionBuilder.toString(freemarker.core.TemplateElement, boolean):java.lang.String */
    public String toString() {
        return toString((TemplateElement) null, true);
    }

    public String toString(TemplateElement templateElement, boolean z) {
        Expression expression;
        if (this.blamed == null && this.tips == null && this.tip == null && this.descriptionParts == null) {
            return this.description;
        }
        StringBuffer stringBuffer = new StringBuffer(200);
        if (!(templateElement == null || (expression = this.blamed) == null || !this.showBlamer)) {
            try {
                Blaming findBlaming = findBlaming(templateElement, expression, 0);
                if (findBlaming != null) {
                    stringBuffer.append("For ");
                    String nodeTypeSymbol = findBlaming.blamer.getNodeTypeSymbol();
                    char c = '\"';
                    if (nodeTypeSymbol.indexOf(34) != -1) {
                        c = '`';
                    }
                    stringBuffer.append(c);
                    stringBuffer.append(nodeTypeSymbol);
                    stringBuffer.append(c);
                    stringBuffer.append(" ");
                    stringBuffer.append(findBlaming.roleOfblamed);
                    stringBuffer.append(": ");
                }
            } catch (Throwable th) {
                LOG.error("Error when searching blamer for better error message.", th);
            }
        }
        String str = this.description;
        if (str != null) {
            stringBuffer.append(str);
        } else {
            appendParts(stringBuffer, this.descriptionParts);
        }
        String str2 = null;
        int i = 1;
        if (this.blamed != null) {
            int length = stringBuffer.length() - 1;
            while (length >= 0 && Character.isWhitespace(stringBuffer.charAt(length))) {
                stringBuffer.deleteCharAt(length);
                length--;
            }
            char charAt = stringBuffer.length() > 0 ? stringBuffer.charAt(stringBuffer.length() - 1) : 0;
            if (charAt != 0) {
                stringBuffer.append(10);
            }
            if (charAt != ':') {
                stringBuffer.append("The blamed expression:\n");
            }
            String[] splitToLines = splitToLines(this.blamed.toString());
            int i2 = 0;
            while (i2 < splitToLines.length) {
                stringBuffer.append(i2 == 0 ? "==> " : "\n    ");
                stringBuffer.append(splitToLines[i2]);
                i2++;
            }
            stringBuffer.append("  [");
            stringBuffer.append(this.blamed.getStartLocation());
            stringBuffer.append(']');
            if (containsSingleInterpolatoinLiteral(this.blamed, 0)) {
                str2 = "It has been noticed that you are using ${...} as the sole content of a quoted string. That does nothing but forcably converts the value inside ${...} to string (as it inserts it into the enclosing string). If that's not what you meant, just remove the quotation marks, ${ and }; you don't need them. If you indeed wanted to convert to string, use myExpression?string instead.";
            }
        }
        if (z) {
            Object[] objArr = this.tips;
            int length2 = (objArr != null ? objArr.length : 0) + (this.tip != null ? 1 : 0) + (str2 != null ? 1 : 0);
            Object[] objArr2 = this.tips;
            if (objArr2 == null || length2 != objArr2.length) {
                objArr2 = new Object[length2];
                Object obj = this.tip;
                if (obj != null) {
                    objArr2[0] = obj;
                } else {
                    i = 0;
                }
                if (this.tips != null) {
                    int i3 = 0;
                    while (true) {
                        Object[] objArr3 = this.tips;
                        if (i3 >= objArr3.length) {
                            break;
                        }
                        objArr2[i] = objArr3[i3];
                        i3++;
                        i++;
                    }
                }
                if (str2 != null) {
                    objArr2[i] = str2;
                }
            }
            if (objArr2 != null && objArr2.length > 0) {
                stringBuffer.append("\n\n");
                for (int i4 = 0; i4 < objArr2.length; i4++) {
                    if (i4 != 0) {
                        stringBuffer.append(10);
                    }
                    stringBuffer.append(_CoreAPI.ERROR_MESSAGE_HR);
                    stringBuffer.append(10);
                    stringBuffer.append("Tip: ");
                    Object obj2 = objArr2[i4];
                    if (!(obj2 instanceof Object[])) {
                        stringBuffer.append(objArr2[i4]);
                    } else {
                        appendParts(stringBuffer, (Object[]) obj2);
                    }
                }
                stringBuffer.append(10);
                stringBuffer.append(_CoreAPI.ERROR_MESSAGE_HR);
            }
        }
        return stringBuffer.toString();
    }

    private boolean containsSingleInterpolatoinLiteral(Expression expression, int i) {
        if (expression == null || i > 20) {
            return false;
        }
        if ((expression instanceof StringLiteral) && ((StringLiteral) expression).isSingleInterpolationLiteral()) {
            return true;
        }
        int parameterCount = expression.getParameterCount();
        for (int i2 = 0; i2 < parameterCount; i2++) {
            Object parameterValue = expression.getParameterValue(i2);
            if ((parameterValue instanceof Expression) && containsSingleInterpolatoinLiteral((Expression) parameterValue, i + 1)) {
                return true;
            }
        }
        return false;
    }

    private Blaming findBlaming(TemplateObject templateObject, Expression expression, int i) {
        Blaming findBlaming;
        if (i > 50) {
            return null;
        }
        int parameterCount = templateObject.getParameterCount();
        int i2 = 0;
        while (i2 < parameterCount) {
            Object parameterValue = templateObject.getParameterValue(i2);
            if (parameterValue == expression) {
                Blaming blaming = new Blaming();
                blaming.blamer = templateObject;
                blaming.roleOfblamed = templateObject.getParameterRole(i2);
                return blaming;
            } else if ((parameterValue instanceof TemplateObject) && (findBlaming = findBlaming((TemplateObject) parameterValue, expression, i + 1)) != null) {
                return findBlaming;
            } else {
                i2++;
            }
        }
        return null;
    }

    private void appendParts(StringBuffer stringBuffer, Object[] objArr) {
        Template template2 = this.template;
        if (template2 == null) {
            Expression expression = this.blamed;
            template2 = expression != null ? expression.getTemplate() : null;
        }
        for (Object obj : objArr) {
            if (obj instanceof Object[]) {
                appendParts(stringBuffer, (Object[]) obj);
            } else {
                String tryToString = tryToString(obj);
                if (tryToString == null) {
                    tryToString = "null";
                }
                if (template2 == null) {
                    stringBuffer.append(tryToString);
                } else if (tryToString.length() <= 4 || tryToString.charAt(0) != '<' || ((tryToString.charAt(1) != '#' && tryToString.charAt(1) != '@' && (tryToString.charAt(1) != '/' || (tryToString.charAt(2) != '#' && tryToString.charAt(2) != '@'))) || tryToString.charAt(tryToString.length() - 1) != '>')) {
                    stringBuffer.append(tryToString);
                } else if (template2.getActualTagSyntax() == 2) {
                    stringBuffer.append('[');
                    stringBuffer.append(tryToString.substring(1, tryToString.length() - 1));
                    stringBuffer.append(']');
                } else {
                    stringBuffer.append(tryToString);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.core._ErrorDescriptionBuilder.toString(java.lang.Object, boolean):java.lang.String
     arg types: [java.lang.Object, int]
     candidates:
      freemarker.core._ErrorDescriptionBuilder.toString(freemarker.core.TemplateElement, boolean):java.lang.String
      freemarker.core._ErrorDescriptionBuilder.toString(java.lang.Object, boolean):java.lang.String */
    public static String toString(Object obj) {
        return toString(obj, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.core._ErrorDescriptionBuilder.toString(java.lang.Object, boolean):java.lang.String
     arg types: [java.lang.Object, int]
     candidates:
      freemarker.core._ErrorDescriptionBuilder.toString(freemarker.core.TemplateElement, boolean):java.lang.String
      freemarker.core._ErrorDescriptionBuilder.toString(java.lang.Object, boolean):java.lang.String */
    public static String tryToString(Object obj) {
        return toString(obj, true);
    }

    private static String toString(Object obj, boolean z) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof Class) {
            return ClassUtil.getShortClassName((Class) obj);
        }
        if ((obj instanceof Method) || (obj instanceof Constructor)) {
            return _MethodUtil.toString((Member) obj);
        }
        return z ? StringUtil.tryToString(obj) : obj.toString();
    }

    private String[] splitToLines(String str) {
        return StringUtil.split(StringUtil.replace(StringUtil.replace(str, "\r\n", "\n"), "\r", "\n"), 10);
    }

    public _ErrorDescriptionBuilder template(Template template2) {
        this.template = template2;
        return this;
    }

    public _ErrorDescriptionBuilder blame(Expression expression) {
        this.blamed = expression;
        return this;
    }

    public _ErrorDescriptionBuilder showBlamer(boolean z) {
        this.showBlamer = z;
        return this;
    }

    public _ErrorDescriptionBuilder tip(String str) {
        tip((Object) str);
        return this;
    }

    public _ErrorDescriptionBuilder tip(Object[] objArr) {
        tip((Object) objArr);
        return this;
    }

    private _ErrorDescriptionBuilder tip(Object obj) {
        if (this.tip == null) {
            this.tip = obj;
        } else {
            Object[] objArr = this.tips;
            if (objArr == null) {
                this.tips = new Object[]{obj};
            } else {
                int length = objArr.length;
                Object[] objArr2 = new Object[(length + 1)];
                for (int i = 0; i < length; i++) {
                    objArr2[i] = this.tips[i];
                }
                objArr2[length] = obj;
                this.tips = objArr2;
            }
        }
        return this;
    }

    public _ErrorDescriptionBuilder tips(Object[] objArr) {
        Object[] objArr2 = this.tips;
        if (objArr2 == null) {
            this.tips = objArr;
        } else {
            int length = objArr2.length;
            int length2 = objArr.length;
            Object[] objArr3 = new Object[(length + length2)];
            for (int i = 0; i < length; i++) {
                objArr3[i] = this.tips[i];
            }
            for (int i2 = 0; i2 < length2; i2++) {
                objArr3[length + i2] = objArr[i2];
            }
            this.tips = objArr3;
        }
        return this;
    }

    private static class Blaming {
        TemplateObject blamer;
        ParameterRole roleOfblamed;

        private Blaming() {
        }
    }
}
