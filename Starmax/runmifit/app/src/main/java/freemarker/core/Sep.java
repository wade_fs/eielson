package freemarker.core;

import freemarker.core.IteratorBlock;
import freemarker.template.TemplateException;
import java.io.IOException;

class Sep extends TemplateElement {
    /* access modifiers changed from: package-private */
    public String getNodeTypeSymbol() {
        return "#sep";
    }

    /* access modifiers changed from: package-private */
    public int getParameterCount() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isNestedBlockRepeater() {
        return false;
    }

    public Sep(TemplateElement templateElement) {
        setNestedBlock(super);
    }

    /* access modifiers changed from: package-private */
    public void accept(Environment environment) throws TemplateException, IOException {
        IteratorBlock.IterationContext findEnclosingIterationContext = IteratorBlock.findEnclosingIterationContext(environment, null);
        if (findEnclosingIterationContext == null) {
            throw new _MiscTemplateException(environment, new Object[]{getNodeTypeSymbol(), " without iteraton in context"});
        } else if (findEnclosingIterationContext.hasNext()) {
            environment.visitByHiddingParent(getNestedBlock());
        }
    }

    /* access modifiers changed from: protected */
    public String dump(boolean z) {
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append('<');
        }
        stringBuffer.append(getNodeTypeSymbol());
        if (z) {
            stringBuffer.append('>');
            if (getNestedBlock() != null) {
                stringBuffer.append(getNestedBlock().getCanonicalForm());
            }
            stringBuffer.append("</");
            stringBuffer.append(getNodeTypeSymbol());
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public Object getParameterValue(int i) {
        throw new IndexOutOfBoundsException();
    }

    /* access modifiers changed from: package-private */
    public ParameterRole getParameterRole(int i) {
        throw new IndexOutOfBoundsException();
    }
}
