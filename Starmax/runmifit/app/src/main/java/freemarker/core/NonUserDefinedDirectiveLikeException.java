package freemarker.core;

import freemarker.template.TemplateModel;

class NonUserDefinedDirectiveLikeException extends UnexpectedTypeException {
    private static final Class[] EXPECTED_TYPES;
    static /* synthetic */ Class class$freemarker$core$Macro;
    static /* synthetic */ Class class$freemarker$template$TemplateDirectiveModel;
    static /* synthetic */ Class class$freemarker$template$TemplateTransformModel;

    static {
        Class[] clsArr = new Class[3];
        Class cls = class$freemarker$template$TemplateDirectiveModel;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateDirectiveModel");
            class$freemarker$template$TemplateDirectiveModel = cls;
        }
        clsArr[0] = cls;
        Class cls2 = class$freemarker$template$TemplateTransformModel;
        if (cls2 == null) {
            cls2 = class$("freemarker.template.TemplateTransformModel");
            class$freemarker$template$TemplateTransformModel = cls2;
        }
        clsArr[1] = cls2;
        Class cls3 = class$freemarker$core$Macro;
        if (cls3 == null) {
            cls3 = class$("freemarker.core.Macro");
            class$freemarker$core$Macro = cls3;
        }
        clsArr[2] = cls3;
        EXPECTED_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonUserDefinedDirectiveLikeException(Environment environment) {
        super(environment, "Expecting user-defined directive, transform or macro value here");
    }

    public NonUserDefinedDirectiveLikeException(String str, Environment environment) {
        super(environment, str);
    }

    NonUserDefinedDirectiveLikeException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(environment, _errordescriptionbuilder);
    }

    NonUserDefinedDirectiveLikeException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "user-defined directive, transform or macro", EXPECTED_TYPES, environment);
    }

    NonUserDefinedDirectiveLikeException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "user-defined directive, transform or macro", EXPECTED_TYPES, str, environment);
    }

    NonUserDefinedDirectiveLikeException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "user-defined directive, transform or macro", EXPECTED_TYPES, strArr, environment);
    }
}
