package freemarker.core;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;

class ThreadInterruptionSupportTemplatePostProcessor extends TemplatePostProcessor {
    ThreadInterruptionSupportTemplatePostProcessor() {
    }

    public void postProcess(Template template) throws TemplatePostProcessorException {
        addInterruptionChecks(template.getRootTreeNode());
    }

    private void addInterruptionChecks(TemplateElement templateElement) throws TemplatePostProcessorException {
        MixedContent mixedContent;
        if (templateElement != null) {
            TemplateElement nestedBlock = templateElement.getNestedBlock();
            if (nestedBlock != null) {
                addInterruptionChecks(nestedBlock);
            }
            int regulatedChildCount = templateElement.getRegulatedChildCount();
            for (int i = 0; i < regulatedChildCount; i++) {
                addInterruptionChecks(templateElement.getRegulatedChild(i));
            }
            if (!templateElement.isNestedBlockRepeater()) {
                return;
            }
            if (regulatedChildCount == 0) {
                try {
                    ThreadInterruptionCheck threadInterruptionCheck = new ThreadInterruptionCheck(templateElement);
                    if (nestedBlock == null) {
                        templateElement.setNestedBlock(threadInterruptionCheck);
                        return;
                    }
                    if (nestedBlock instanceof MixedContent) {
                        mixedContent = (MixedContent) nestedBlock;
                    } else {
                        MixedContent mixedContent2 = new MixedContent();
                        mixedContent2.setLocation(templateElement.getTemplate(), 0, 0, 0, 0);
                        mixedContent2.addElement(nestedBlock);
                        templateElement.setNestedBlock(mixedContent2);
                        mixedContent = mixedContent2;
                    }
                    mixedContent.addElement(0, threadInterruptionCheck);
                } catch (ParseException e) {
                    throw new TemplatePostProcessorException("Unexpected error; see cause", e);
                }
            } else {
                throw new BugException();
            }
        }
    }

    static class ThreadInterruptionCheck extends TemplateElement {
        /* access modifiers changed from: package-private */
        public String getNodeTypeSymbol() {
            return "##threadInterruptionCheck";
        }

        /* access modifiers changed from: package-private */
        public int getParameterCount() {
            return 0;
        }

        /* access modifiers changed from: package-private */
        public boolean isNestedBlockRepeater() {
            return false;
        }

        private ThreadInterruptionCheck(TemplateElement templateElement) throws ParseException {
            setLocation(super.getTemplate(), super.beginColumn, super.beginLine, super.beginColumn, super.beginLine);
        }

        /* access modifiers changed from: package-private */
        public void accept(Environment environment) throws TemplateException, IOException {
            if (Thread.currentThread().isInterrupted()) {
                throw new TemplateProcessingThreadInterruptedException();
            }
        }

        /* access modifiers changed from: protected */
        public String dump(boolean z) {
            if (z) {
                return "";
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("<#--");
            stringBuffer.append(getNodeTypeSymbol());
            stringBuffer.append("--#>");
            return stringBuffer.toString();
        }

        /* access modifiers changed from: package-private */
        public Object getParameterValue(int i) {
            throw new IndexOutOfBoundsException();
        }

        /* access modifiers changed from: package-private */
        public ParameterRole getParameterRole(int i) {
            throw new IndexOutOfBoundsException();
        }
    }

    static class TemplateProcessingThreadInterruptedException extends RuntimeException {
        TemplateProcessingThreadInterruptedException() {
            super("Template processing thread \"interrupted\" flag was set.");
        }
    }
}
