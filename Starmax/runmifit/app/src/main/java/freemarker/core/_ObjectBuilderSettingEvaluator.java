package freemarker.core;

import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;
import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.StringUtil;
import freemarker.template.utility.WriteProtectable;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class _ObjectBuilderSettingEvaluator {
    private static final String BUILDER_CLASS_POSTFIX = "Builder";
    private static final String BUILD_METHOD_NAME = "build";
    private static final String INSTANCE_FIELD_NAME = "INSTANCE";
    private static Map SHORTHANDS;
    static /* synthetic */ Class class$freemarker$ext$beans$BeansWrapper;
    static /* synthetic */ Class class$freemarker$template$DefaultObjectWrapper;
    static /* synthetic */ Class class$freemarker$template$SimpleObjectWrapper;
    /* access modifiers changed from: private */
    public final _SettingEvaluationEnvironment env;
    /* access modifiers changed from: private */
    public final Class expectedClass;
    private int pos;
    private final String src;
    private boolean v2321Mode = false;

    private boolean isASCIIDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private _ObjectBuilderSettingEvaluator(String str, int i, Class cls, _SettingEvaluationEnvironment _settingevaluationenvironment) {
        this.src = str;
        this.pos = i;
        this.expectedClass = cls;
        this.env = _settingevaluationenvironment;
    }

    public static Object eval(String str, Class cls, _SettingEvaluationEnvironment _settingevaluationenvironment) throws _ObjectBuilderSettingEvaluationException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        return new _ObjectBuilderSettingEvaluator(str, 0, cls, _settingevaluationenvironment).eval();
    }

    public static int configureBean(String str, int i, Object obj, _SettingEvaluationEnvironment _settingevaluationenvironment) throws _ObjectBuilderSettingEvaluationException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        return new _ObjectBuilderSettingEvaluator(str, i, obj.getClass(), _settingevaluationenvironment).configureBean(obj);
    }

    private Object eval() throws _ObjectBuilderSettingEvaluationException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        return execute(parse());
    }

    private int configureBean(Object obj) throws _ObjectBuilderSettingEvaluationException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        PropertyAssignmentsExpression propertyAssignmentsExpression = new PropertyAssignmentsExpression(obj);
        fetchParameterListInto(propertyAssignmentsExpression);
        skipWS();
        propertyAssignmentsExpression.eval();
        return this.pos;
    }

    private BuilderExpression parse() throws _ObjectBuilderSettingEvaluationException {
        skipWS();
        BuilderExpression fetchBuilderCall = fetchBuilderCall(true, false);
        skipWS();
        if (this.pos == this.src.length()) {
            return fetchBuilderCall;
        }
        throw new _ObjectBuilderSettingEvaluationException("end-of-expression", this.src, this.pos);
    }

    private Object execute(BuilderExpression builderExpression) throws _ObjectBuilderSettingEvaluationException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        if (!this.v2321Mode) {
            return ClassUtil.forName(builderExpression.className).newInstance();
        }
        return builderExpression.eval();
    }

    private Object eval(Object obj) throws _ObjectBuilderSettingEvaluationException {
        return obj instanceof SettingExpression ? ((SettingExpression) obj).eval() : obj;
    }

    private BuilderExpression fetchBuilderCall(boolean z, boolean z2) throws _ObjectBuilderSettingEvaluationException {
        int i = this.pos;
        BuilderExpression builderExpression = new BuilderExpression();
        String fetchClassName = fetchClassName(z2);
        if (fetchClassName == null) {
            return null;
        }
        String unused = builderExpression.className = shorthandToFullQualified(fetchClassName);
        if (!fetchClassName.equals(builderExpression.className)) {
            this.v2321Mode = true;
        }
        skipWS();
        char fetchOptionalChar = fetchOptionalChar("(");
        if (fetchOptionalChar != 0 || z) {
            if (fetchOptionalChar != 0) {
                fetchParameterListInto(builderExpression);
            }
            return builderExpression;
        } else if (z2) {
            this.pos = i;
            return null;
        } else {
            throw new _ObjectBuilderSettingEvaluationException("(", this.src, this.pos);
        }
    }

    private void fetchParameterListInto(ExpressionWithParameters expressionWithParameters) throws _ObjectBuilderSettingEvaluationException {
        this.v2321Mode = true;
        skipWS();
        if (fetchOptionalChar(")") != ')') {
            do {
                skipWS();
                Object fetchValueOrName = fetchValueOrName(false);
                if (fetchValueOrName != null) {
                    skipWS();
                    if (fetchValueOrName instanceof ParameterName) {
                        expressionWithParameters.namedParamNames.add(((ParameterName) fetchValueOrName).name);
                        skipWS();
                        fetchRequiredChar("=");
                        skipWS();
                        int i = this.pos;
                        Object fetchValueOrName2 = fetchValueOrName(false);
                        if (!(fetchValueOrName2 instanceof ParameterName)) {
                            expressionWithParameters.namedParamValues.add(eval(fetchValueOrName2));
                        } else {
                            throw new _ObjectBuilderSettingEvaluationException("concrete value", this.src, i);
                        }
                    } else if (!expressionWithParameters.namedParamNames.isEmpty()) {
                        throw new _ObjectBuilderSettingEvaluationException("Positional parameters must precede named parameters");
                    } else if (expressionWithParameters.getAllowPositionalParameters()) {
                        expressionWithParameters.positionalParamValues.add(eval(fetchValueOrName));
                    } else {
                        throw new _ObjectBuilderSettingEvaluationException("Positional parameters not supported here");
                    }
                    skipWS();
                }
            } while (fetchRequiredChar(",)") == ',');
        }
    }

    private Object fetchValueOrName(boolean z) throws _ObjectBuilderSettingEvaluationException {
        if (this.pos < this.src.length()) {
            Object fetchNumberLike = fetchNumberLike(true);
            if (fetchNumberLike != null) {
                return fetchNumberLike;
            }
            Object fetchStringLiteral = fetchStringLiteral(true);
            if (fetchStringLiteral != null) {
                return fetchStringLiteral;
            }
            BuilderExpression fetchBuilderCall = fetchBuilderCall(false, true);
            if (fetchBuilderCall != null) {
                return fetchBuilderCall;
            }
            String fetchSimpleName = fetchSimpleName(true);
            if (fetchSimpleName != null) {
                if (fetchSimpleName.equals("true")) {
                    return Boolean.TRUE;
                }
                if (fetchSimpleName.equals("false")) {
                    return Boolean.FALSE;
                }
                if (fetchSimpleName.equals("null")) {
                    return NullExpression.INSTANCE;
                }
                return new ParameterName(fetchSimpleName);
            }
        }
        if (z) {
            return null;
        }
        throw new _ObjectBuilderSettingEvaluationException("value or name", this.src, this.pos);
    }

    private String fetchSimpleName(boolean z) throws _ObjectBuilderSettingEvaluationException {
        if (isIdentifierStart(this.pos < this.src.length() ? this.src.charAt(this.pos) : 0)) {
            int i = this.pos;
            this.pos = i + 1;
            while (this.pos != this.src.length() && isIdentifierMiddle(this.src.charAt(this.pos))) {
                this.pos++;
            }
            return this.src.substring(i, this.pos);
        } else if (z) {
            return null;
        } else {
            throw new _ObjectBuilderSettingEvaluationException("class name", this.src, this.pos);
        }
    }

    private String fetchClassName(boolean z) throws _ObjectBuilderSettingEvaluationException {
        int i = this.pos;
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            String fetchSimpleName = fetchSimpleName(true);
            if (fetchSimpleName != null) {
                stringBuffer.append(fetchSimpleName);
                skipWS();
                if (this.pos < this.src.length() && this.src.charAt(this.pos) == '.') {
                    stringBuffer.append('.');
                    this.pos++;
                    skipWS();
                }
            } else if (z) {
                this.pos = i;
                return null;
            } else {
                throw new _ObjectBuilderSettingEvaluationException(Config.FEED_LIST_NAME, this.src, this.pos);
            }
        }
        return stringBuffer.toString();
    }

    private Object fetchNumberLike(boolean z) throws _ObjectBuilderSettingEvaluationException {
        int i = this.pos;
        boolean z2 = false;
        boolean z3 = false;
        while (this.pos != this.src.length()) {
            char charAt = this.src.charAt(this.pos);
            if (charAt != '.') {
                if (!isASCIIDigit(charAt) && charAt != '-') {
                    break;
                }
            } else if (z2) {
                z3 = true;
            } else {
                z2 = true;
            }
            this.pos++;
        }
        int i2 = this.pos;
        if (i != i2) {
            String substring = this.src.substring(i, i2);
            if (z3) {
                try {
                    return new Version(substring);
                } catch (IllegalArgumentException e) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Malformed version number: ");
                    stringBuffer.append(substring);
                    throw new _ObjectBuilderSettingEvaluationException(stringBuffer.toString(), e);
                }
            } else {
                try {
                    if (substring.endsWith(FileUtil.HIDDEN_PREFIX)) {
                        throw new NumberFormatException("A number can't end with a dot");
                    } else if (!substring.startsWith(FileUtil.HIDDEN_PREFIX) && !substring.startsWith("-.") && !substring.startsWith("+.")) {
                        return new BigDecimal(substring);
                    } else {
                        throw new NumberFormatException("A number can't start with a dot");
                    }
                } catch (NumberFormatException e2) {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("Malformed number: ");
                    stringBuffer2.append(substring);
                    throw new _ObjectBuilderSettingEvaluationException(stringBuffer2.toString(), e2);
                }
            }
        } else if (z) {
            return null;
        } else {
            throw new _ObjectBuilderSettingEvaluationException("number-like", this.src, i2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0065, code lost:
        r1 = r8.pos;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0067, code lost:
        if (r0 != r1) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0069, code lost:
        if (r9 == false) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006b, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0076, code lost:
        throw new freemarker.core._ObjectBuilderSettingEvaluationException("string literal", r8.src, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0077, code lost:
        r9 = r8.src;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0079, code lost:
        if (r4 == false) goto L_0x007d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007b, code lost:
        r1 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007d, code lost:
        r1 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x007e, code lost:
        r9 = r9.substring(r0 + r1, r8.pos);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r8.pos++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x008a, code lost:
        if (r4 == false) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0092, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0093, code lost:
        r2 = new java.lang.StringBuffer();
        r2.append("Malformed string literal: ");
        r2.append(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a9, code lost:
        throw new freemarker.core._ObjectBuilderSettingEvaluationException(r2.toString(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        return freemarker.template.utility.StringUtil.FTLStringLiteralDec(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return r9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object fetchStringLiteral(boolean r9) throws freemarker.core._ObjectBuilderSettingEvaluationException {
        /*
            r8 = this;
            int r0 = r8.pos
            r1 = 0
            r2 = 0
            r3 = 0
            r4 = 0
        L_0x0006:
            int r5 = r8.pos
            java.lang.String r6 = r8.src
            int r6 = r6.length()
            r7 = 1
            if (r5 != r6) goto L_0x0022
            if (r2 != 0) goto L_0x0014
            goto L_0x0065
        L_0x0014:
            freemarker.core._ObjectBuilderSettingEvaluationException r9 = new freemarker.core._ObjectBuilderSettingEvaluationException
            java.lang.String r0 = java.lang.String.valueOf(r2)
            java.lang.String r1 = r8.src
            int r2 = r8.pos
            r9.<init>(r0, r1, r2)
            throw r9
        L_0x0022:
            java.lang.String r5 = r8.src
            int r6 = r8.pos
            char r5 = r5.charAt(r6)
            if (r2 != 0) goto L_0x0059
            r2 = 114(0x72, float:1.6E-43)
            if (r5 != r2) goto L_0x0045
            int r2 = r8.pos
            int r2 = r2 + r7
            java.lang.String r6 = r8.src
            int r6 = r6.length()
            if (r2 >= r6) goto L_0x0045
            java.lang.String r2 = r8.src
            int r4 = r8.pos
            int r4 = r4 + r7
            char r5 = r2.charAt(r4)
            r4 = 1
        L_0x0045:
            r2 = 34
            r6 = 39
            if (r5 != r6) goto L_0x004e
            r2 = 39
            goto L_0x0050
        L_0x004e:
            if (r5 != r2) goto L_0x0065
        L_0x0050:
            if (r4 == 0) goto L_0x00c9
            int r5 = r8.pos
            int r5 = r5 + r7
            r8.pos = r5
            goto L_0x00c9
        L_0x0059:
            if (r3 != 0) goto L_0x00c8
            r6 = 92
            if (r5 != r6) goto L_0x0063
            if (r4 != 0) goto L_0x0063
            r3 = 1
            goto L_0x00c9
        L_0x0063:
            if (r5 != r2) goto L_0x00aa
        L_0x0065:
            int r1 = r8.pos
            if (r0 != r1) goto L_0x0077
            if (r9 == 0) goto L_0x006d
            r9 = 0
            return r9
        L_0x006d:
            freemarker.core._ObjectBuilderSettingEvaluationException r9 = new freemarker.core._ObjectBuilderSettingEvaluationException
            java.lang.String r0 = r8.src
            java.lang.String r2 = "string literal"
            r9.<init>(r2, r0, r1)
            throw r9
        L_0x0077:
            java.lang.String r9 = r8.src
            if (r4 == 0) goto L_0x007d
            r1 = 2
            goto L_0x007e
        L_0x007d:
            r1 = 1
        L_0x007e:
            int r0 = r0 + r1
            int r1 = r8.pos
            java.lang.String r9 = r9.substring(r0, r1)
            int r0 = r8.pos     // Catch:{ ParseException -> 0x0092 }
            int r0 = r0 + r7
            r8.pos = r0     // Catch:{ ParseException -> 0x0092 }
            if (r4 == 0) goto L_0x008d
            goto L_0x0091
        L_0x008d:
            java.lang.String r9 = freemarker.template.utility.StringUtil.FTLStringLiteralDec(r9)     // Catch:{ ParseException -> 0x0092 }
        L_0x0091:
            return r9
        L_0x0092:
            r0 = move-exception
            freemarker.core._ObjectBuilderSettingEvaluationException r1 = new freemarker.core._ObjectBuilderSettingEvaluationException
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r3 = "Malformed string literal: "
            r2.append(r3)
            r2.append(r9)
            java.lang.String r9 = r2.toString()
            r1.<init>(r9, r0)
            throw r1
        L_0x00aa:
            r6 = 123(0x7b, float:1.72E-43)
            if (r5 != r6) goto L_0x00c9
            java.lang.String r5 = r8.src
            int r6 = r8.pos
            int r6 = r6 - r7
            char r5 = r5.charAt(r6)
            r6 = 36
            if (r5 == r6) goto L_0x00c0
            r6 = 35
            if (r5 == r6) goto L_0x00c0
            goto L_0x00c9
        L_0x00c0:
            freemarker.core._ObjectBuilderSettingEvaluationException r9 = new freemarker.core._ObjectBuilderSettingEvaluationException
            java.lang.String r0 = "${...} and #{...} aren't allowed here."
            r9.<init>(r0)
            throw r9
        L_0x00c8:
            r3 = 0
        L_0x00c9:
            int r5 = r8.pos
            int r5 = r5 + r7
            r8.pos = r5
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core._ObjectBuilderSettingEvaluator.fetchStringLiteral(boolean):java.lang.Object");
    }

    private void skipWS() {
        while (this.pos != this.src.length() && Character.isWhitespace(this.src.charAt(this.pos))) {
            this.pos++;
        }
    }

    private char fetchOptionalChar(String str) throws _ObjectBuilderSettingEvaluationException {
        return fetchChar(str, true);
    }

    private char fetchRequiredChar(String str) throws _ObjectBuilderSettingEvaluationException {
        return fetchChar(str, false);
    }

    private char fetchChar(String str, boolean z) throws _ObjectBuilderSettingEvaluationException {
        int i = 0;
        char charAt = this.pos < this.src.length() ? this.src.charAt(this.pos) : 0;
        if (str.indexOf(charAt) != -1) {
            this.pos++;
            return charAt;
        } else if (z) {
            return 0;
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            while (i < str.length()) {
                if (i != 0) {
                    stringBuffer.append(" or ");
                }
                int i2 = i + 1;
                stringBuffer.append(StringUtil.jQuote(str.substring(i, i2)));
                i = i2;
            }
            if (z) {
                stringBuffer.append(" or end-of-string");
            }
            throw new _ObjectBuilderSettingEvaluationException(stringBuffer.toString(), this.src, this.pos);
        }
    }

    private boolean isIdentifierStart(char c) {
        return Character.isLetter(c) || c == '_' || c == '$';
    }

    private boolean isIdentifierMiddle(char c) {
        return isIdentifierStart(c) || isASCIIDigit(c);
    }

    private static synchronized String shorthandToFullQualified(String str) {
        Class cls;
        Class cls2;
        Class cls3;
        synchronized (_ObjectBuilderSettingEvaluator.class) {
            if (SHORTHANDS == null) {
                SHORTHANDS = new HashMap();
                Map map = SHORTHANDS;
                if (class$freemarker$template$DefaultObjectWrapper == null) {
                    cls = class$("freemarker.template.DefaultObjectWrapper");
                    class$freemarker$template$DefaultObjectWrapper = cls;
                } else {
                    cls = class$freemarker$template$DefaultObjectWrapper;
                }
                map.put("DefaultObjectWrapper", cls.getName());
                Map map2 = SHORTHANDS;
                if (class$freemarker$ext$beans$BeansWrapper == null) {
                    cls2 = class$("freemarker.ext.beans.BeansWrapper");
                    class$freemarker$ext$beans$BeansWrapper = cls2;
                } else {
                    cls2 = class$freemarker$ext$beans$BeansWrapper;
                }
                map2.put("BeansWrapper", cls2.getName());
                Map map3 = SHORTHANDS;
                if (class$freemarker$template$SimpleObjectWrapper == null) {
                    cls3 = class$("freemarker.template.SimpleObjectWrapper");
                    class$freemarker$template$SimpleObjectWrapper = cls3;
                } else {
                    cls3 = class$freemarker$template$SimpleObjectWrapper;
                }
                map3.put("SimpleObjectWrapper", cls3.getName());
            }
            String str2 = (String) SHORTHANDS.get(str);
            if (str2 != null) {
                str = str2;
            }
        }
        return str;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* access modifiers changed from: private */
    public void setJavaBeanProperties(Object obj, List list, List list2) throws _ObjectBuilderSettingEvaluationException {
        if (!list.isEmpty()) {
            Class<?> cls = obj.getClass();
            try {
                PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(cls).getPropertyDescriptors();
                HashMap hashMap = new HashMap((propertyDescriptors.length * 4) / 3, 1.0f);
                int i = 0;
                for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                    Method writeMethod = propertyDescriptor.getWriteMethod();
                    if (writeMethod != null) {
                        hashMap.put(propertyDescriptor.getName(), writeMethod);
                    }
                }
                TemplateHashModel templateHashModel = null;
                while (i < list.size()) {
                    String str = (String) list.get(i);
                    if (hashMap.containsKey(str)) {
                        Method method = (Method) hashMap.put(str, null);
                        if (method != null) {
                            if (templateHashModel == null) {
                                try {
                                    TemplateModel wrap = this.env.getObjectWrapper().wrap(obj);
                                    if (wrap instanceof TemplateHashModel) {
                                        templateHashModel = (TemplateHashModel) wrap;
                                    } else {
                                        StringBuffer stringBuffer = new StringBuffer();
                                        stringBuffer.append("The ");
                                        stringBuffer.append(cls.getName());
                                        stringBuffer.append(" class is not a wrapped as TemplateHashModel.");
                                        throw new _ObjectBuilderSettingEvaluationException(stringBuffer.toString());
                                    }
                                } catch (Exception e) {
                                    StringBuffer stringBuffer2 = new StringBuffer();
                                    stringBuffer2.append("Failed to set ");
                                    stringBuffer2.append(StringUtil.jQuote(str));
                                    throw new _ObjectBuilderSettingEvaluationException(stringBuffer2.toString(), e);
                                }
                            }
                            TemplateModel templateModel = templateHashModel.get(method.getName());
                            if (templateModel == null) {
                                StringBuffer stringBuffer3 = new StringBuffer();
                                stringBuffer3.append("Can't find ");
                                stringBuffer3.append(method);
                                stringBuffer3.append(" as FreeMarker method.");
                                throw new _ObjectBuilderSettingEvaluationException(stringBuffer3.toString());
                            } else if (templateModel instanceof TemplateMethodModelEx) {
                                ArrayList arrayList = new ArrayList();
                                arrayList.add(this.env.getObjectWrapper().wrap(list2.get(i)));
                                ((TemplateMethodModelEx) templateModel).exec(arrayList);
                                i++;
                            } else {
                                StringBuffer stringBuffer4 = new StringBuffer();
                                stringBuffer4.append(StringUtil.jQuote(method.getName()));
                                stringBuffer4.append(" wasn't a TemplateMethodModelEx.");
                                throw new _ObjectBuilderSettingEvaluationException(stringBuffer4.toString());
                            }
                        } else {
                            StringBuffer stringBuffer5 = new StringBuffer();
                            stringBuffer5.append("JavaBeans property ");
                            stringBuffer5.append(StringUtil.jQuote(str));
                            stringBuffer5.append(" is set twice.");
                            throw new _ObjectBuilderSettingEvaluationException(stringBuffer5.toString());
                        }
                    } else {
                        StringBuffer stringBuffer6 = new StringBuffer();
                        stringBuffer6.append("The ");
                        stringBuffer6.append(cls.getName());
                        stringBuffer6.append(" class has no writeable JavaBeans property called ");
                        stringBuffer6.append(StringUtil.jQuote(str));
                        stringBuffer6.append(FileUtil.HIDDEN_PREFIX);
                        throw new _ObjectBuilderSettingEvaluationException(stringBuffer6.toString());
                    }
                }
            } catch (Exception e2) {
                StringBuffer stringBuffer7 = new StringBuffer();
                stringBuffer7.append("Failed to inspect ");
                stringBuffer7.append(cls.getName());
                stringBuffer7.append(" class");
                throw new _ObjectBuilderSettingEvaluationException(stringBuffer7.toString(), e2);
            }
        }
    }

    private static class ParameterName {
        /* access modifiers changed from: private */
        public final String name;

        public ParameterName(String str) {
            this.name = str;
        }
    }

    private static abstract class SettingExpression {
        /* access modifiers changed from: package-private */
        public abstract Object eval() throws _ObjectBuilderSettingEvaluationException;

        private SettingExpression() {
        }
    }

    private abstract class ExpressionWithParameters extends SettingExpression {
        protected List namedParamNames;
        protected List namedParamValues;
        protected List positionalParamValues;

        /* access modifiers changed from: protected */
        public abstract boolean getAllowPositionalParameters();

        private ExpressionWithParameters() {
            super();
            this.positionalParamValues = new ArrayList();
            this.namedParamNames = new ArrayList();
            this.namedParamValues = new ArrayList();
        }
    }

    private class BuilderExpression extends ExpressionWithParameters {
        /* access modifiers changed from: private */
        public String className;

        /* access modifiers changed from: protected */
        public boolean getAllowPositionalParameters() {
            return true;
        }

        private BuilderExpression() {
            super();
        }

        /* access modifiers changed from: package-private */
        public Object eval() throws _ObjectBuilderSettingEvaluationException {
            boolean z;
            try {
                Class forName = ClassUtil.forName(this.className);
                try {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(forName.getName());
                    stringBuffer.append(_ObjectBuilderSettingEvaluator.BUILDER_CLASS_POSTFIX);
                    forName = ClassUtil.forName(stringBuffer.toString());
                    z = true;
                } catch (ClassNotFoundException unused) {
                    z = false;
                }
                if (!z && hasNoParameters()) {
                    try {
                        Field field = forName.getField(_ObjectBuilderSettingEvaluator.INSTANCE_FIELD_NAME);
                        if ((field.getModifiers() & 9) == 9) {
                            return field.get(null);
                        }
                    } catch (NoSuchFieldException unused2) {
                    } catch (Exception e) {
                        StringBuffer stringBuffer2 = new StringBuffer();
                        stringBuffer2.append("Error when trying to access ");
                        stringBuffer2.append(StringUtil.jQuote(this.className));
                        stringBuffer2.append(FileUtil.HIDDEN_PREFIX);
                        stringBuffer2.append(_ObjectBuilderSettingEvaluator.INSTANCE_FIELD_NAME);
                        throw new _ObjectBuilderSettingEvaluationException(stringBuffer2.toString(), e);
                    }
                }
                Object callConstructor = callConstructor(forName);
                _ObjectBuilderSettingEvaluator.this.setJavaBeanProperties(callConstructor, this.namedParamNames, this.namedParamValues);
                if (z) {
                    callConstructor = callBuild(callConstructor);
                } else if (callConstructor instanceof WriteProtectable) {
                    ((WriteProtectable) callConstructor).writeProtect();
                }
                if (_ObjectBuilderSettingEvaluator.this.expectedClass.isInstance(callConstructor)) {
                    return callConstructor;
                }
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("The resulting object (of class ");
                stringBuffer3.append(callConstructor.getClass());
                stringBuffer3.append(") is not a(n) ");
                stringBuffer3.append(_ObjectBuilderSettingEvaluator.this.expectedClass.getName());
                stringBuffer3.append(FileUtil.HIDDEN_PREFIX);
                throw new _ObjectBuilderSettingEvaluationException(stringBuffer3.toString());
            } catch (Exception e2) {
                StringBuffer stringBuffer4 = new StringBuffer();
                stringBuffer4.append("Failed to get class ");
                stringBuffer4.append(StringUtil.jQuote(this.className));
                stringBuffer4.append(FileUtil.HIDDEN_PREFIX);
                throw new _ObjectBuilderSettingEvaluationException(stringBuffer4.toString(), e2);
            }
        }

        private Object callConstructor(Class cls) throws _ObjectBuilderSettingEvaluationException {
            if (hasNoParameters()) {
                try {
                    return cls.newInstance();
                } catch (Exception e) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Failed to call ");
                    stringBuffer.append(cls.getName());
                    stringBuffer.append(" 0-argument constructor");
                    throw new _ObjectBuilderSettingEvaluationException(stringBuffer.toString(), e);
                }
            } else {
                BeansWrapper objectWrapper = _ObjectBuilderSettingEvaluator.this.env.getObjectWrapper();
                ArrayList arrayList = new ArrayList(this.positionalParamValues.size());
                int i = 0;
                while (i < this.positionalParamValues.size()) {
                    try {
                        arrayList.add(objectWrapper.wrap(this.positionalParamValues.get(i)));
                        i++;
                    } catch (TemplateModelException e2) {
                        StringBuffer stringBuffer2 = new StringBuffer();
                        stringBuffer2.append("Failed to wrap arg #");
                        stringBuffer2.append(i + 1);
                        throw new _ObjectBuilderSettingEvaluationException(stringBuffer2.toString(), e2);
                    }
                }
                try {
                    return objectWrapper.newInstance(cls, arrayList);
                } catch (Exception e3) {
                    StringBuffer stringBuffer3 = new StringBuffer();
                    stringBuffer3.append("Failed to call ");
                    stringBuffer3.append(cls.getName());
                    stringBuffer3.append(" constructor");
                    throw new _ObjectBuilderSettingEvaluationException(stringBuffer3.toString(), e3);
                }
            }
        }

        private Object callBuild(Object obj) throws _ObjectBuilderSettingEvaluationException {
            Class<?> cls = obj.getClass();
            try {
                try {
                    return obj.getClass().getMethod(_ObjectBuilderSettingEvaluator.BUILD_METHOD_NAME, null).invoke(obj, null);
                } catch (Exception e) {
                    e = e;
                    if (e instanceof InvocationTargetException) {
                        e = ((InvocationTargetException) e).getTargetException();
                    }
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Failed to call build() method on ");
                    stringBuffer.append(cls.getName());
                    stringBuffer.append(" instance");
                    throw new _ObjectBuilderSettingEvaluationException(stringBuffer.toString(), e);
                }
            } catch (NoSuchMethodException e2) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("The ");
                stringBuffer2.append(cls.getName());
                stringBuffer2.append(" builder class must have a public ");
                stringBuffer2.append(_ObjectBuilderSettingEvaluator.BUILD_METHOD_NAME);
                stringBuffer2.append("() method");
                throw new _ObjectBuilderSettingEvaluationException(stringBuffer2.toString(), e2);
            } catch (Exception e3) {
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("Failed to get the build() method of the ");
                stringBuffer3.append(cls.getName());
                stringBuffer3.append(" builder class");
                throw new _ObjectBuilderSettingEvaluationException(stringBuffer3.toString(), e3);
            }
        }

        private boolean hasNoParameters() {
            return this.positionalParamValues.isEmpty() && this.namedParamValues.isEmpty();
        }
    }

    private class PropertyAssignmentsExpression extends ExpressionWithParameters {
        private final Object bean;

        /* access modifiers changed from: protected */
        public boolean getAllowPositionalParameters() {
            return false;
        }

        public PropertyAssignmentsExpression(Object obj) {
            super();
            this.bean = obj;
        }

        /* access modifiers changed from: package-private */
        public Object eval() throws _ObjectBuilderSettingEvaluationException {
            _ObjectBuilderSettingEvaluator.this.setJavaBeanProperties(this.bean, this.namedParamNames, this.namedParamValues);
            return this.bean;
        }
    }

    private static class NullExpression extends SettingExpression {
        static final NullExpression INSTANCE = new NullExpression();

        /* access modifiers changed from: package-private */
        public Object eval() throws _ObjectBuilderSettingEvaluationException {
            return null;
        }

        private NullExpression() {
            super();
        }
    }
}
