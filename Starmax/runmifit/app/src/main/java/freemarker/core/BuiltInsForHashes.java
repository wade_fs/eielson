package freemarker.core;

import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;

class BuiltInsForHashes {

    static class keysBI extends BuiltInForHashEx {
        keysBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(TemplateHashModelEx templateHashModelEx, Environment environment) throws TemplateModelException, InvalidReferenceException {
            TemplateCollectionModel keys = templateHashModelEx.keys();
            if (keys != null) {
                return keys instanceof TemplateSequenceModel ? keys : new CollectionAndSequence(keys);
            }
            throw newNullPropertyException("keys", templateHashModelEx, environment);
        }
    }

    static class valuesBI extends BuiltInForHashEx {
        valuesBI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel calculateResult(TemplateHashModelEx templateHashModelEx, Environment environment) throws TemplateModelException, InvalidReferenceException {
            TemplateCollectionModel values = templateHashModelEx.values();
            if (values != null) {
                return values instanceof TemplateSequenceModel ? values : new CollectionAndSequence(values);
            }
            throw newNullPropertyException("values", templateHashModelEx, environment);
        }
    }

    private BuiltInsForHashes() {
    }
}
