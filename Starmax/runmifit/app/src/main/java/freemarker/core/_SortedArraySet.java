package freemarker.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class _SortedArraySet extends _UnmodifiableSet {
    private final Object[] array;

    public _SortedArraySet(Object[] objArr) {
        this.array = objArr;
    }

    public int size() {
        return this.array.length;
    }

    public boolean contains(Object obj) {
        return Arrays.binarySearch(this.array, obj) >= 0;
    }

    public Iterator iterator() {
        return new _ArrayIterator(this.array);
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        throw new UnsupportedOperationException();
    }
}
