package freemarker.core;

import freemarker.template.TemplateModel;

public class NonSequenceOrCollectionException extends UnexpectedTypeException {
    private static final Class[] EXPECTED_TYPES;
    static /* synthetic */ Class class$freemarker$template$TemplateCollectionModel;
    static /* synthetic */ Class class$freemarker$template$TemplateSequenceModel;

    static {
        Class[] clsArr = new Class[2];
        Class cls = class$freemarker$template$TemplateSequenceModel;
        if (cls == null) {
            cls = class$("freemarker.template.TemplateSequenceModel");
            class$freemarker$template$TemplateSequenceModel = cls;
        }
        clsArr[0] = cls;
        Class cls2 = class$freemarker$template$TemplateCollectionModel;
        if (cls2 == null) {
            cls2 = class$("freemarker.template.TemplateCollectionModel");
            class$freemarker$template$TemplateCollectionModel = cls2;
        }
        clsArr[1] = cls2;
        EXPECTED_TYPES = clsArr;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public NonSequenceOrCollectionException(Environment environment) {
        super(environment, "Expecting sequence or collection value here");
    }

    public NonSequenceOrCollectionException(String str, Environment environment) {
        super(environment, str);
    }

    NonSequenceOrCollectionException(Environment environment, _ErrorDescriptionBuilder _errordescriptionbuilder) {
        super(environment, _errordescriptionbuilder);
    }

    NonSequenceOrCollectionException(Expression expression, TemplateModel templateModel, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "sequence or collection", EXPECTED_TYPES, environment);
    }

    NonSequenceOrCollectionException(Expression expression, TemplateModel templateModel, String str, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "sequence or collection", EXPECTED_TYPES, str, environment);
    }

    NonSequenceOrCollectionException(Expression expression, TemplateModel templateModel, String[] strArr, Environment environment) throws InvalidReferenceException {
        super(expression, templateModel, "sequence or collection", EXPECTED_TYPES, strArr, environment);
    }
}
