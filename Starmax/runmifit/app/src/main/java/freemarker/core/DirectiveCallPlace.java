package freemarker.core;

import freemarker.template.utility.ObjectFactory;

public interface DirectiveCallPlace {
    int getBeginColumn();

    int getBeginLine();

    int getEndColumn();

    int getEndLine();

    Object getOrCreateCustomData(Object obj, ObjectFactory objectFactory) throws CallPlaceCustomDataInitializationException;

    boolean isNestedOutputCacheable();
}
