package freemarker.core;

import freemarker.template.utility.DateUtil;
import java.util.TimeZone;

abstract class ISOLikeTemplateDateFormatFactory extends TemplateDateFormatFactory {
    private DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter;
    private DateUtil.DateToISO8601CalendarFactory dateToCalenderFieldsCalculator;

    public boolean isLocaleBound() {
        return false;
    }

    public ISOLikeTemplateDateFormatFactory(TimeZone timeZone) {
        super(timeZone);
    }

    public DateUtil.DateToISO8601CalendarFactory getISOBuiltInCalendar() {
        DateUtil.DateToISO8601CalendarFactory dateToISO8601CalendarFactory = this.dateToCalenderFieldsCalculator;
        if (dateToISO8601CalendarFactory != null) {
            return dateToISO8601CalendarFactory;
        }
        DateUtil.TrivialDateToISO8601CalendarFactory trivialDateToISO8601CalendarFactory = new DateUtil.TrivialDateToISO8601CalendarFactory();
        this.dateToCalenderFieldsCalculator = trivialDateToISO8601CalendarFactory;
        return trivialDateToISO8601CalendarFactory;
    }

    public DateUtil.CalendarFieldsToDateConverter getCalendarFieldsToDateCalculator() {
        DateUtil.CalendarFieldsToDateConverter calendarFieldsToDateConverter2 = this.calendarFieldsToDateConverter;
        if (calendarFieldsToDateConverter2 != null) {
            return calendarFieldsToDateConverter2;
        }
        DateUtil.TrivialCalendarFieldsToDateConverter trivialCalendarFieldsToDateConverter = new DateUtil.TrivialCalendarFieldsToDateConverter();
        this.calendarFieldsToDateConverter = trivialCalendarFieldsToDateConverter;
        return trivialCalendarFieldsToDateConverter;
    }
}
