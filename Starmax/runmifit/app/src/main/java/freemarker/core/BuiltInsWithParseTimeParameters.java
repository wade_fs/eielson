package freemarker.core;

import freemarker.core.Expression;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import java.util.ArrayList;
import java.util.List;

final class BuiltInsWithParseTimeParameters {

    static class then_BI extends BuiltInWithParseTimeParameters {
        private Expression whenFalseExp;
        private Expression whenTrueExp;

        /* access modifiers changed from: protected */
        public int getArgumentsCount() {
            return 2;
        }

        then_BI() {
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            return (this.target.evalToBoolean(environment) ? this.whenTrueExp : this.whenFalseExp).evalToNonMissing(environment);
        }

        /* access modifiers changed from: package-private */
        public void bindToParameters(List list, Token token, Token token2) throws ParseException {
            if (list.size() == 2) {
                this.whenTrueExp = (Expression) list.get(0);
                this.whenFalseExp = (Expression) list.get(1);
                return;
            }
            throw newArgumentCountException("requires exactly 2", token, token2);
        }

        /* access modifiers changed from: protected */
        public Expression getArgumentParameterValue(int i) {
            if (i == 0) {
                return this.whenTrueExp;
            }
            if (i == 1) {
                return this.whenFalseExp;
            }
            throw new IndexOutOfBoundsException();
        }

        /* access modifiers changed from: protected */
        public List getArgumentsAsList() {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(this.whenTrueExp);
            arrayList.add(this.whenFalseExp);
            return arrayList;
        }

        /* access modifiers changed from: protected */
        public void cloneArguments(Expression expression, String str, Expression expression2, Expression.ReplacemenetState replacemenetState) {
            then_BI then_bi = (then_BI) expression;
            then_bi.whenTrueExp = this.whenTrueExp.deepCloneWithIdentifierReplaced(str, expression2, replacemenetState);
            then_bi.whenFalseExp = this.whenFalseExp.deepCloneWithIdentifierReplaced(str, expression2, replacemenetState);
        }
    }

    private BuiltInsWithParseTimeParameters() {
    }

    static class switch_BI extends BuiltInWithParseTimeParameters {
        private List parameters;

        switch_BI() {
        }

        /* access modifiers changed from: package-private */
        public void bindToParameters(List list, Token token, Token token2) throws ParseException {
            if (list.size() >= 2) {
                this.parameters = list;
                return;
            }
            throw newArgumentCountException("must have at least 2", token, token2);
        }

        /* access modifiers changed from: protected */
        public List getArgumentsAsList() {
            return this.parameters;
        }

        /* access modifiers changed from: protected */
        public int getArgumentsCount() {
            return this.parameters.size();
        }

        /* access modifiers changed from: protected */
        public Expression getArgumentParameterValue(int i) {
            return (Expression) this.parameters.get(i);
        }

        /* access modifiers changed from: protected */
        public void cloneArguments(Expression expression, String str, Expression expression2, Expression.ReplacemenetState replacemenetState) {
            ArrayList arrayList = new ArrayList(this.parameters.size());
            for (int i = 0; i < this.parameters.size(); i++) {
                arrayList.add(((Expression) this.parameters.get(i)).deepCloneWithIdentifierReplaced(str, expression2, replacemenetState));
            }
            ((switch_BI) expression).parameters = arrayList;
        }

        /* access modifiers changed from: package-private */
        public TemplateModel _eval(Environment environment) throws TemplateException {
            Environment environment2 = environment;
            TemplateModel evalToNonMissing = this.target.evalToNonMissing(environment2);
            List list = this.parameters;
            int size = list.size();
            int i = 0;
            while (true) {
                int i2 = i + 1;
                if (i2 < size) {
                    Expression expression = (Expression) list.get(i);
                    int i3 = i2;
                    int i4 = i;
                    int i5 = size;
                    if (EvalUtil.compare(evalToNonMissing, this.target, 1, "==", expression.evalToNonMissing(environment2), expression, this, true, false, false, false, environment)) {
                        return ((Expression) list.get(i3)).evalToNonMissing(environment2);
                    }
                    i = i4 + 2;
                    size = i5;
                } else {
                    int i6 = size;
                    if (i6 % 2 != 0) {
                        return ((Expression) list.get(i6 - 1)).evalToNonMissing(environment2);
                    }
                    throw new _MiscTemplateException(this.target, new Object[]{"The value before ?", this.key, "(case1, value1, case2, value2, ...) didn't match any of the case parameters, and there was no default value parameter (an additional last parameter) eihter. "});
                }
            }
        }
    }
}
