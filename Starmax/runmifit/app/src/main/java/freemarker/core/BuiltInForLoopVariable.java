package freemarker.core;

import com.tamic.novate.util.FileUtil;
import freemarker.core.IteratorBlock;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

abstract class BuiltInForLoopVariable extends SpecialBuiltIn {
    private String loopVarName;

    /* access modifiers changed from: package-private */
    public abstract TemplateModel calculateResult(IteratorBlock.IterationContext iterationContext, Environment environment) throws TemplateException;

    BuiltInForLoopVariable() {
    }

    /* access modifiers changed from: package-private */
    public void bindToLoopVariable(String str) {
        this.loopVarName = str;
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        IteratorBlock.IterationContext findEnclosingIterationContext = IteratorBlock.findEnclosingIterationContext(environment, this.loopVarName);
        if (findEnclosingIterationContext != null) {
            return calculateResult(findEnclosingIterationContext, environment);
        }
        throw new _MiscTemplateException(this, environment, new Object[]{"There's no iteration in context that uses loop variable ", new _DelayedJQuote(this.loopVarName), FileUtil.HIDDEN_PREFIX});
    }
}
