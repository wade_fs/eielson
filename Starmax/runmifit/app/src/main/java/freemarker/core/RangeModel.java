package freemarker.core;

import freemarker.template.SimpleNumber;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;
import java.io.Serializable;

abstract class RangeModel implements TemplateSequenceModel, Serializable {
    private final int begin;

    /* access modifiers changed from: package-private */
    public abstract int getStep();

    /* access modifiers changed from: package-private */
    public abstract boolean isAffactedByStringSlicingBug();

    /* access modifiers changed from: package-private */
    public abstract boolean isRightAdaptive();

    /* access modifiers changed from: package-private */
    public abstract boolean isRightUnbounded();

    public RangeModel(int i) {
        this.begin = i;
    }

    /* access modifiers changed from: package-private */
    public final int getBegining() {
        return this.begin;
    }

    public final TemplateModel get(int i) throws TemplateModelException {
        if (i < 0 || i >= size()) {
            throw new _TemplateModelException(new Object[]{"Range item index ", new Integer(i), " is out of bounds."});
        }
        long step = ((long) this.begin) + (((long) getStep()) * ((long) i));
        return step <= 2147483647L ? new SimpleNumber((int) step) : new SimpleNumber(step);
    }
}
