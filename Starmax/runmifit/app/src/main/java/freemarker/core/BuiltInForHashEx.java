package freemarker.core;

import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

abstract class BuiltInForHashEx extends BuiltIn {
    /* access modifiers changed from: package-private */
    public abstract TemplateModel calculateResult(TemplateHashModelEx templateHashModelEx, Environment environment) throws TemplateModelException, InvalidReferenceException;

    BuiltInForHashEx() {
    }

    /* access modifiers changed from: package-private */
    public TemplateModel _eval(Environment environment) throws TemplateException {
        TemplateModel eval = this.target.eval(environment);
        if (eval instanceof TemplateHashModelEx) {
            return calculateResult((TemplateHashModelEx) eval, environment);
        }
        throw new NonExtendedHashException(this.target, eval, environment);
    }

    /* access modifiers changed from: protected */
    public InvalidReferenceException newNullPropertyException(String str, TemplateModel templateModel, Environment environment) {
        if (environment.getFastInvalidReferenceExceptions()) {
            return InvalidReferenceException.FAST_INSTANCE;
        }
        return new InvalidReferenceException(new _ErrorDescriptionBuilder(new Object[]{"The exteneded hash (of class ", templateModel.getClass().getName(), ") has returned null for its \"", str, "\" property. This is maybe a bug. The extended hash was returned by this expression:"}).blame(this.target), environment, this);
    }
}
