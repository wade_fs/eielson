package freemarker.core;

public class _ObjectBuilderSettingEvaluationException extends Exception {
    public _ObjectBuilderSettingEvaluationException(String str, Throwable th) {
        super(str, th);
    }

    public _ObjectBuilderSettingEvaluationException(String str) {
        super(str);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public _ObjectBuilderSettingEvaluationException(java.lang.String r4, java.lang.String r5, int r6) {
        /*
            r3 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "Expression syntax error: Expected a(n) "
            r0.append(r1)
            r0.append(r4)
            java.lang.String r4 = ", but "
            r0.append(r4)
            int r4 = r5.length()
            if (r6 >= r4) goto L_0x0052
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.lang.String r1 = "found character "
            r4.append(r1)
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = ""
            r1.append(r2)
            char r5 = r5.charAt(r6)
            r1.append(r5)
            java.lang.String r5 = r1.toString()
            java.lang.String r5 = freemarker.template.utility.StringUtil.jQuote(r5)
            r4.append(r5)
            java.lang.String r5 = " at position "
            r4.append(r5)
            int r6 = r6 + 1
            r4.append(r6)
            java.lang.String r5 = "."
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            goto L_0x0054
        L_0x0052:
            java.lang.String r4 = "the end of the parsed string was reached."
        L_0x0054:
            r0.append(r4)
            java.lang.String r4 = r0.toString()
            r3.<init>(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core._ObjectBuilderSettingEvaluationException.<init>(java.lang.String, java.lang.String, int):void");
    }
}
