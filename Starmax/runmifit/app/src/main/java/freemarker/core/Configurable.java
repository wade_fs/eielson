package freemarker.core;

import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.NullArgumentException;
import freemarker.template.utility.StringUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import no.nordicsemi.android.dfu.BuildConfig;

public class Configurable {
    private static final String ALLOWED_CLASSES = "allowed_classes";
    public static final String API_BUILTIN_ENABLED_KEY = "api_builtin_enabled";
    public static final String API_BUILTIN_ENABLED_KEY_CAMEL_CASE = "apiBuiltinEnabled";
    public static final String API_BUILTIN_ENABLED_KEY_SNAKE_CASE = "api_builtin_enabled";
    public static final String ARITHMETIC_ENGINE_KEY = "arithmetic_engine";
    public static final String ARITHMETIC_ENGINE_KEY_CAMEL_CASE = "arithmeticEngine";
    public static final String ARITHMETIC_ENGINE_KEY_SNAKE_CASE = "arithmetic_engine";
    public static final String AUTO_FLUSH_KEY = "auto_flush";
    public static final String AUTO_FLUSH_KEY_CAMEL_CASE = "autoFlush";
    public static final String AUTO_FLUSH_KEY_SNAKE_CASE = "auto_flush";
    public static final String BOOLEAN_FORMAT_KEY = "boolean_format";
    public static final String BOOLEAN_FORMAT_KEY_CAMEL_CASE = "booleanFormat";
    public static final String BOOLEAN_FORMAT_KEY_SNAKE_CASE = "boolean_format";
    public static final String CLASSIC_COMPATIBLE_KEY = "classic_compatible";
    public static final String CLASSIC_COMPATIBLE_KEY_CAMEL_CASE = "classicCompatible";
    public static final String CLASSIC_COMPATIBLE_KEY_SNAKE_CASE = "classic_compatible";
    static final String C_TRUE_FALSE = "true,false";
    public static final String DATETIME_FORMAT_KEY = "datetime_format";
    public static final String DATETIME_FORMAT_KEY_CAMEL_CASE = "datetimeFormat";
    public static final String DATETIME_FORMAT_KEY_SNAKE_CASE = "datetime_format";
    public static final String DATE_FORMAT_KEY = "date_format";
    public static final String DATE_FORMAT_KEY_CAMEL_CASE = "dateFormat";
    public static final String DATE_FORMAT_KEY_SNAKE_CASE = "date_format";
    private static final String DEFAULT = "default";
    private static final String DEFAULT_2_3_0 = "default_2_3_0";
    private static final String JVM_DEFAULT = "JVM default";
    public static final String LOCALE_KEY = "locale";
    public static final String LOCALE_KEY_CAMEL_CASE = "locale";
    public static final String LOCALE_KEY_SNAKE_CASE = "locale";
    public static final String LOG_TEMPLATE_EXCEPTIONS_KEY = "log_template_exceptions";
    public static final String LOG_TEMPLATE_EXCEPTIONS_KEY_CAMEL_CASE = "logTemplateExceptions";
    public static final String LOG_TEMPLATE_EXCEPTIONS_KEY_SNAKE_CASE = "log_template_exceptions";
    public static final String NEW_BUILTIN_CLASS_RESOLVER_KEY = "new_builtin_class_resolver";
    public static final String NEW_BUILTIN_CLASS_RESOLVER_KEY_CAMEL_CASE = "newBuiltinClassResolver";
    public static final String NEW_BUILTIN_CLASS_RESOLVER_KEY_SNAKE_CASE = "new_builtin_class_resolver";
    public static final String NUMBER_FORMAT_KEY = "number_format";
    public static final String NUMBER_FORMAT_KEY_CAMEL_CASE = "numberFormat";
    public static final String NUMBER_FORMAT_KEY_SNAKE_CASE = "number_format";
    public static final String OBJECT_WRAPPER_KEY = "object_wrapper";
    public static final String OBJECT_WRAPPER_KEY_CAMEL_CASE = "objectWrapper";
    public static final String OBJECT_WRAPPER_KEY_SNAKE_CASE = "object_wrapper";
    public static final String OUTPUT_ENCODING_KEY = "output_encoding";
    public static final String OUTPUT_ENCODING_KEY_CAMEL_CASE = "outputEncoding";
    public static final String OUTPUT_ENCODING_KEY_SNAKE_CASE = "output_encoding";
    private static final String[] SETTING_NAMES_CAMEL_CASE = {API_BUILTIN_ENABLED_KEY_CAMEL_CASE, ARITHMETIC_ENGINE_KEY_CAMEL_CASE, AUTO_FLUSH_KEY_CAMEL_CASE, BOOLEAN_FORMAT_KEY_CAMEL_CASE, CLASSIC_COMPATIBLE_KEY_CAMEL_CASE, DATE_FORMAT_KEY_CAMEL_CASE, DATETIME_FORMAT_KEY_CAMEL_CASE, "locale", LOG_TEMPLATE_EXCEPTIONS_KEY_CAMEL_CASE, NEW_BUILTIN_CLASS_RESOLVER_KEY_CAMEL_CASE, NUMBER_FORMAT_KEY_CAMEL_CASE, OBJECT_WRAPPER_KEY_CAMEL_CASE, OUTPUT_ENCODING_KEY_CAMEL_CASE, SHOW_ERROR_TIPS_KEY_CAMEL_CASE, SQL_DATE_AND_TIME_TIME_ZONE_KEY_CAMEL_CASE, STRICT_BEAN_MODELS_KEY_CAMEL_CASE, TEMPLATE_EXCEPTION_HANDLER_KEY_CAMEL_CASE, TIME_FORMAT_KEY_CAMEL_CASE, TIME_ZONE_KEY_CAMEL_CASE, URL_ESCAPING_CHARSET_KEY_CAMEL_CASE};
    private static final String[] SETTING_NAMES_SNAKE_CASE = {"api_builtin_enabled", "arithmetic_engine", "auto_flush", "boolean_format", "classic_compatible", "date_format", "datetime_format", "locale", "log_template_exceptions", "new_builtin_class_resolver", "number_format", "object_wrapper", "output_encoding", "show_error_tips", "sql_date_and_time_time_zone", "strict_bean_models", "template_exception_handler", "time_format", "time_zone", "url_escaping_charset"};
    public static final String SHOW_ERROR_TIPS_KEY = "show_error_tips";
    public static final String SHOW_ERROR_TIPS_KEY_CAMEL_CASE = "showErrorTips";
    public static final String SHOW_ERROR_TIPS_KEY_SNAKE_CASE = "show_error_tips";
    public static final String SQL_DATE_AND_TIME_TIME_ZONE_KEY = "sql_date_and_time_time_zone";
    public static final String SQL_DATE_AND_TIME_TIME_ZONE_KEY_CAMEL_CASE = "sqlDateAndTimeTimeZone";
    public static final String SQL_DATE_AND_TIME_TIME_ZONE_KEY_SNAKE_CASE = "sql_date_and_time_time_zone";
    public static final String STRICT_BEAN_MODELS = "strict_bean_models";
    public static final String STRICT_BEAN_MODELS_KEY = "strict_bean_models";
    public static final String STRICT_BEAN_MODELS_KEY_CAMEL_CASE = "strictBeanModels";
    public static final String STRICT_BEAN_MODELS_KEY_SNAKE_CASE = "strict_bean_models";
    public static final String TEMPLATE_EXCEPTION_HANDLER_KEY = "template_exception_handler";
    public static final String TEMPLATE_EXCEPTION_HANDLER_KEY_CAMEL_CASE = "templateExceptionHandler";
    public static final String TEMPLATE_EXCEPTION_HANDLER_KEY_SNAKE_CASE = "template_exception_handler";
    public static final String TIME_FORMAT_KEY = "time_format";
    public static final String TIME_FORMAT_KEY_CAMEL_CASE = "timeFormat";
    public static final String TIME_FORMAT_KEY_SNAKE_CASE = "time_format";
    public static final String TIME_ZONE_KEY = "time_zone";
    public static final String TIME_ZONE_KEY_CAMEL_CASE = "timeZone";
    public static final String TIME_ZONE_KEY_SNAKE_CASE = "time_zone";
    private static final String TRUSTED_TEMPLATES = "trusted_templates";
    public static final String URL_ESCAPING_CHARSET_KEY = "url_escaping_charset";
    public static final String URL_ESCAPING_CHARSET_KEY_CAMEL_CASE = "urlEscapingCharset";
    public static final String URL_ESCAPING_CHARSET_KEY_SNAKE_CASE = "url_escaping_charset";
    static /* synthetic */ Class class$freemarker$core$ArithmeticEngine;
    static /* synthetic */ Class class$freemarker$core$TemplateClassResolver;
    static /* synthetic */ Class class$freemarker$ext$beans$BeansWrapper;
    static /* synthetic */ Class class$freemarker$template$ObjectWrapper;
    static /* synthetic */ Class class$freemarker$template$TemplateExceptionHandler;
    private Boolean apiBuiltinEnabled;
    private ArithmeticEngine arithmeticEngine;
    private Boolean autoFlush;
    private String booleanFormat;
    private Integer classicCompatible;
    private HashMap customAttributes;
    private String dateFormat;
    private String dateTimeFormat;
    private String falseStringValue;
    private Locale locale;
    private Boolean logTemplateExceptions;
    private TemplateClassResolver newBuiltinClassResolver;
    private String numberFormat;
    private ObjectWrapper objectWrapper;
    private String outputEncoding;
    private boolean outputEncodingSet;
    private Configurable parent;
    private Properties properties;
    private Boolean showErrorTips;
    private TimeZone sqlDataAndTimeTimeZone;
    private boolean sqlDataAndTimeTimeZoneSet;
    private TemplateExceptionHandler templateExceptionHandler;
    private String timeFormat;
    private TimeZone timeZone;
    private String trueStringValue;
    private String urlEscapingCharset;
    private boolean urlEscapingCharsetSet;

    /* access modifiers changed from: protected */
    public String getCorrectedNameForUnknownSetting(String str) {
        return null;
    }

    public Configurable() {
        this(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
    }

    protected Configurable(Version version) {
        _TemplateAPI.checkVersionNotNullAndSupported(version);
        this.parent = null;
        this.properties = new Properties();
        this.locale = Locale.getDefault();
        this.properties.setProperty("locale", this.locale.toString());
        this.timeZone = TimeZone.getDefault();
        this.properties.setProperty("time_zone", this.timeZone.getID());
        this.sqlDataAndTimeTimeZone = null;
        this.properties.setProperty("sql_date_and_time_time_zone", String.valueOf(this.sqlDataAndTimeTimeZone));
        this.numberFormat = "number";
        this.properties.setProperty("number_format", this.numberFormat);
        this.timeFormat = "";
        this.properties.setProperty("time_format", this.timeFormat);
        this.dateFormat = "";
        this.properties.setProperty("date_format", this.dateFormat);
        this.dateTimeFormat = "";
        this.properties.setProperty("datetime_format", this.dateTimeFormat);
        this.classicCompatible = new Integer(0);
        this.properties.setProperty("classic_compatible", this.classicCompatible.toString());
        this.templateExceptionHandler = _TemplateAPI.getDefaultTemplateExceptionHandler(version);
        this.properties.setProperty("template_exception_handler", this.templateExceptionHandler.getClass().getName());
        this.arithmeticEngine = ArithmeticEngine.BIGDECIMAL_ENGINE;
        this.properties.setProperty("arithmetic_engine", this.arithmeticEngine.getClass().getName());
        this.objectWrapper = Configuration.getDefaultObjectWrapper(version);
        this.autoFlush = Boolean.TRUE;
        this.properties.setProperty("auto_flush", this.autoFlush.toString());
        this.newBuiltinClassResolver = TemplateClassResolver.UNRESTRICTED_RESOLVER;
        this.properties.setProperty("new_builtin_class_resolver", this.newBuiltinClassResolver.getClass().getName());
        this.showErrorTips = Boolean.TRUE;
        this.properties.setProperty("show_error_tips", this.showErrorTips.toString());
        this.apiBuiltinEnabled = Boolean.FALSE;
        this.properties.setProperty("api_builtin_enabled", this.apiBuiltinEnabled.toString());
        this.logTemplateExceptions = Boolean.valueOf(_TemplateAPI.getDefaultLogTemplateExceptions(version));
        this.properties.setProperty("log_template_exceptions", this.logTemplateExceptions.toString());
        setBooleanFormat(C_TRUE_FALSE);
        this.customAttributes = new HashMap();
    }

    public Configurable(Configurable configurable) {
        this.parent = configurable;
        this.locale = null;
        this.numberFormat = null;
        this.classicCompatible = null;
        this.templateExceptionHandler = null;
        this.properties = new Properties(configurable.properties);
        this.customAttributes = new HashMap();
    }

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        Configurable configurable = (Configurable) super.clone();
        configurable.properties = new Properties(this.properties);
        configurable.customAttributes = (HashMap) this.customAttributes.clone();
        return configurable;
    }

    public final Configurable getParent() {
        return this.parent;
    }

    /* access modifiers changed from: package-private */
    public final void setParent(Configurable configurable) {
        this.parent = configurable;
    }

    public void setClassicCompatible(boolean z) {
        this.classicCompatible = new Integer(z ? 1 : 0);
        this.properties.setProperty("classic_compatible", classicCompatibilityIntToString(this.classicCompatible));
    }

    public void setClassicCompatibleAsInt(int i) {
        if (i < 0 || i > 2) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Unsupported \"classicCompatibility\": ");
            stringBuffer.append(i);
            throw new IllegalArgumentException(stringBuffer.toString());
        }
        this.classicCompatible = new Integer(i);
    }

    private String classicCompatibilityIntToString(Integer num) {
        if (num == null) {
            return null;
        }
        if (num.intValue() == 0) {
            return "false";
        }
        if (num.intValue() == 1) {
            return "true";
        }
        return num.toString();
    }

    public boolean isClassicCompatible() {
        Integer num = this.classicCompatible;
        if (num != null) {
            return num.intValue() != 0;
        }
        return this.parent.isClassicCompatible();
    }

    public int getClassicCompatibleAsInt() {
        Integer num = this.classicCompatible;
        return num != null ? num.intValue() : this.parent.getClassicCompatibleAsInt();
    }

    public void setLocale(Locale locale2) {
        NullArgumentException.check("locale", locale2);
        this.locale = locale2;
        this.properties.setProperty("locale", locale2.toString());
    }

    public TimeZone getTimeZone() {
        TimeZone timeZone2 = this.timeZone;
        return timeZone2 != null ? timeZone2 : this.parent.getTimeZone();
    }

    public void setTimeZone(TimeZone timeZone2) {
        NullArgumentException.check(TIME_ZONE_KEY_CAMEL_CASE, timeZone2);
        this.timeZone = timeZone2;
        this.properties.setProperty("time_zone", timeZone2.getID());
    }

    public void setSQLDateAndTimeTimeZone(TimeZone timeZone2) {
        this.sqlDataAndTimeTimeZone = timeZone2;
        this.sqlDataAndTimeTimeZoneSet = true;
        this.properties.setProperty("sql_date_and_time_time_zone", timeZone2 != null ? timeZone2.getID() : "null");
    }

    public TimeZone getSQLDateAndTimeTimeZone() {
        if (this.sqlDataAndTimeTimeZoneSet) {
            return this.sqlDataAndTimeTimeZone;
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.getSQLDateAndTimeTimeZone();
        }
        return null;
    }

    public Locale getLocale() {
        Locale locale2 = this.locale;
        return locale2 != null ? locale2 : this.parent.getLocale();
    }

    public void setNumberFormat(String str) {
        NullArgumentException.check(NUMBER_FORMAT_KEY_CAMEL_CASE, str);
        this.numberFormat = str;
        this.properties.setProperty("number_format", str);
    }

    public String getNumberFormat() {
        String str = this.numberFormat;
        return str != null ? str : this.parent.getNumberFormat();
    }

    public void setBooleanFormat(String str) {
        NullArgumentException.check(BOOLEAN_FORMAT_KEY_CAMEL_CASE, str);
        int indexOf = str.indexOf(44);
        if (indexOf != -1) {
            this.booleanFormat = str;
            this.properties.setProperty("boolean_format", str);
            if (str.equals(C_TRUE_FALSE)) {
                this.trueStringValue = null;
                this.falseStringValue = null;
                return;
            }
            this.trueStringValue = str.substring(0, indexOf);
            this.falseStringValue = str.substring(indexOf + 1);
            return;
        }
        throw new IllegalArgumentException("Setting value must be string that contains two comma-separated values for true and false, respectively.");
    }

    public String getBooleanFormat() {
        String str = this.booleanFormat;
        return str != null ? str : this.parent.getBooleanFormat();
    }

    /* access modifiers changed from: package-private */
    public String formatBoolean(boolean z, boolean z2) throws TemplateException {
        if (z) {
            String trueStringValue2 = getTrueStringValue();
            if (trueStringValue2 != null) {
                return trueStringValue2;
            }
            if (z2) {
                return "true";
            }
            throw new _MiscTemplateException(getNullBooleanFormatErrorDescription());
        }
        String falseStringValue2 = getFalseStringValue();
        if (falseStringValue2 != null) {
            return falseStringValue2;
        }
        if (z2) {
            return "false";
        }
        throw new _MiscTemplateException(getNullBooleanFormatErrorDescription());
    }

    private _ErrorDescriptionBuilder getNullBooleanFormatErrorDescription() {
        Object[] objArr = new Object[5];
        objArr[0] = "Can't convert boolean to string automatically, because the \"";
        objArr[1] = "boolean_format";
        objArr[2] = "\" setting was ";
        objArr[3] = new _DelayedJQuote(getBooleanFormat());
        objArr[4] = getBooleanFormat().equals(C_TRUE_FALSE) ? ", which is the legacy default computer-language format, and hence isn't accepted." : FileUtil.HIDDEN_PREFIX;
        return new _ErrorDescriptionBuilder(objArr).tips(new Object[]{"If you just want \"true\"/\"false\" result as you are generting computer-language output, use \"?c\", like ${myBool?c}.", "You can write myBool?string('yes', 'no') and like to specify boolean formatting in place.", new Object[]{"If you need the same two values on most places, the programmers should set the \"", "boolean_format", "\" setting to something like \"yes,no\"."}});
    }

    /* access modifiers changed from: package-private */
    public String getTrueStringValue() {
        if (this.booleanFormat != null) {
            return this.trueStringValue;
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.getTrueStringValue();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String getFalseStringValue() {
        if (this.booleanFormat != null) {
            return this.falseStringValue;
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.getFalseStringValue();
        }
        return null;
    }

    public void setTimeFormat(String str) {
        NullArgumentException.check(TIME_FORMAT_KEY_CAMEL_CASE, str);
        this.timeFormat = str;
        this.properties.setProperty("time_format", str);
    }

    public String getTimeFormat() {
        String str = this.timeFormat;
        return str != null ? str : this.parent.getTimeFormat();
    }

    public void setDateFormat(String str) {
        NullArgumentException.check(DATE_FORMAT_KEY_CAMEL_CASE, str);
        this.dateFormat = str;
        this.properties.setProperty("date_format", str);
    }

    public String getDateFormat() {
        String str = this.dateFormat;
        return str != null ? str : this.parent.getDateFormat();
    }

    public void setDateTimeFormat(String str) {
        NullArgumentException.check("dateTimeFormat", str);
        this.dateTimeFormat = str;
        this.properties.setProperty("datetime_format", str);
    }

    public String getDateTimeFormat() {
        String str = this.dateTimeFormat;
        return str != null ? str : this.parent.getDateTimeFormat();
    }

    public void setTemplateExceptionHandler(TemplateExceptionHandler templateExceptionHandler2) {
        NullArgumentException.check(TEMPLATE_EXCEPTION_HANDLER_KEY_CAMEL_CASE, templateExceptionHandler2);
        this.templateExceptionHandler = templateExceptionHandler2;
        this.properties.setProperty("template_exception_handler", templateExceptionHandler2.getClass().getName());
    }

    public TemplateExceptionHandler getTemplateExceptionHandler() {
        TemplateExceptionHandler templateExceptionHandler2 = this.templateExceptionHandler;
        return templateExceptionHandler2 != null ? templateExceptionHandler2 : this.parent.getTemplateExceptionHandler();
    }

    public void setArithmeticEngine(ArithmeticEngine arithmeticEngine2) {
        NullArgumentException.check(ARITHMETIC_ENGINE_KEY_CAMEL_CASE, arithmeticEngine2);
        this.arithmeticEngine = arithmeticEngine2;
        this.properties.setProperty("arithmetic_engine", arithmeticEngine2.getClass().getName());
    }

    public ArithmeticEngine getArithmeticEngine() {
        ArithmeticEngine arithmeticEngine2 = this.arithmeticEngine;
        return arithmeticEngine2 != null ? arithmeticEngine2 : this.parent.getArithmeticEngine();
    }

    public void setObjectWrapper(ObjectWrapper objectWrapper2) {
        NullArgumentException.check(OBJECT_WRAPPER_KEY_CAMEL_CASE, objectWrapper2);
        this.objectWrapper = objectWrapper2;
        this.properties.setProperty("object_wrapper", objectWrapper2.getClass().getName());
    }

    public ObjectWrapper getObjectWrapper() {
        ObjectWrapper objectWrapper2 = this.objectWrapper;
        return objectWrapper2 != null ? objectWrapper2 : this.parent.getObjectWrapper();
    }

    public void setOutputEncoding(String str) {
        this.outputEncoding = str;
        if (str != null) {
            this.properties.setProperty("output_encoding", str);
        } else {
            this.properties.remove("output_encoding");
        }
        this.outputEncodingSet = true;
    }

    public String getOutputEncoding() {
        if (this.outputEncodingSet) {
            return this.outputEncoding;
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.getOutputEncoding();
        }
        return null;
    }

    public void setURLEscapingCharset(String str) {
        this.urlEscapingCharset = str;
        if (str != null) {
            this.properties.setProperty("url_escaping_charset", str);
        } else {
            this.properties.remove("url_escaping_charset");
        }
        this.urlEscapingCharsetSet = true;
    }

    public String getURLEscapingCharset() {
        if (this.urlEscapingCharsetSet) {
            return this.urlEscapingCharset;
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.getURLEscapingCharset();
        }
        return null;
    }

    public void setNewBuiltinClassResolver(TemplateClassResolver templateClassResolver) {
        NullArgumentException.check(NEW_BUILTIN_CLASS_RESOLVER_KEY_CAMEL_CASE, templateClassResolver);
        this.newBuiltinClassResolver = templateClassResolver;
        this.properties.setProperty("new_builtin_class_resolver", templateClassResolver.getClass().getName());
    }

    public TemplateClassResolver getNewBuiltinClassResolver() {
        TemplateClassResolver templateClassResolver = this.newBuiltinClassResolver;
        return templateClassResolver != null ? templateClassResolver : this.parent.getNewBuiltinClassResolver();
    }

    public void setAutoFlush(boolean z) {
        this.autoFlush = Boolean.valueOf(z);
        this.properties.setProperty("auto_flush", String.valueOf(z));
    }

    public boolean getAutoFlush() {
        Boolean bool = this.autoFlush;
        if (bool != null) {
            return bool.booleanValue();
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.getAutoFlush();
        }
        return true;
    }

    public void setShowErrorTips(boolean z) {
        this.showErrorTips = Boolean.valueOf(z);
        this.properties.setProperty("show_error_tips", String.valueOf(z));
    }

    public boolean getShowErrorTips() {
        Boolean bool = this.showErrorTips;
        if (bool != null) {
            return bool.booleanValue();
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.getShowErrorTips();
        }
        return true;
    }

    public void setAPIBuiltinEnabled(boolean z) {
        this.apiBuiltinEnabled = Boolean.valueOf(z);
        this.properties.setProperty("api_builtin_enabled", String.valueOf(z));
    }

    public boolean isAPIBuiltinEnabled() {
        Boolean bool = this.apiBuiltinEnabled;
        if (bool != null) {
            return bool.booleanValue();
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.isAPIBuiltinEnabled();
        }
        return false;
    }

    public void setLogTemplateExceptions(boolean z) {
        this.logTemplateExceptions = Boolean.valueOf(z);
        this.properties.setProperty("log_template_exceptions", String.valueOf(z));
    }

    public boolean getLogTemplateExceptions() {
        Boolean bool = this.logTemplateExceptions;
        if (bool != null) {
            return bool.booleanValue();
        }
        Configurable configurable = this.parent;
        if (configurable != null) {
            return configurable.getLogTemplateExceptions();
        }
        return true;
    }

    public void setSetting(String str, String str2) throws TemplateException {
        Class cls;
        Class cls2;
        Class cls3;
        Class cls4;
        try {
            boolean z = false;
            if ("locale".equals(str)) {
                setLocale(StringUtil.deduceLocale(str2));
            } else {
                if (!"number_format".equals(str)) {
                    if (!NUMBER_FORMAT_KEY_CAMEL_CASE.equals(str)) {
                        if (!"time_format".equals(str)) {
                            if (!TIME_FORMAT_KEY_CAMEL_CASE.equals(str)) {
                                if (!"date_format".equals(str)) {
                                    if (!DATE_FORMAT_KEY_CAMEL_CASE.equals(str)) {
                                        if (!"datetime_format".equals(str)) {
                                            if (!DATETIME_FORMAT_KEY_CAMEL_CASE.equals(str)) {
                                                if (!"time_zone".equals(str)) {
                                                    if (!TIME_ZONE_KEY_CAMEL_CASE.equals(str)) {
                                                        TimeZone timeZone2 = null;
                                                        if (!"sql_date_and_time_time_zone".equals(str)) {
                                                            if (!SQL_DATE_AND_TIME_TIME_ZONE_KEY_CAMEL_CASE.equals(str)) {
                                                                if (!"classic_compatible".equals(str)) {
                                                                    if (!CLASSIC_COMPATIBLE_KEY_CAMEL_CASE.equals(str)) {
                                                                        if (!"template_exception_handler".equals(str)) {
                                                                            if (!TEMPLATE_EXCEPTION_HANDLER_KEY_CAMEL_CASE.equals(str)) {
                                                                                if (!"arithmetic_engine".equals(str)) {
                                                                                    if (!ARITHMETIC_ENGINE_KEY_CAMEL_CASE.equals(str)) {
                                                                                        if (!"object_wrapper".equals(str)) {
                                                                                            if (!OBJECT_WRAPPER_KEY_CAMEL_CASE.equals(str)) {
                                                                                                if (!"boolean_format".equals(str)) {
                                                                                                    if (!BOOLEAN_FORMAT_KEY_CAMEL_CASE.equals(str)) {
                                                                                                        if (!"output_encoding".equals(str)) {
                                                                                                            if (!OUTPUT_ENCODING_KEY_CAMEL_CASE.equals(str)) {
                                                                                                                if (!"url_escaping_charset".equals(str)) {
                                                                                                                    if (!URL_ESCAPING_CHARSET_KEY_CAMEL_CASE.equals(str)) {
                                                                                                                        if (!"strict_bean_models".equals(str)) {
                                                                                                                            if (!STRICT_BEAN_MODELS_KEY_CAMEL_CASE.equals(str)) {
                                                                                                                                if (!"auto_flush".equals(str)) {
                                                                                                                                    if (!AUTO_FLUSH_KEY_CAMEL_CASE.equals(str)) {
                                                                                                                                        if (!"show_error_tips".equals(str)) {
                                                                                                                                            if (!SHOW_ERROR_TIPS_KEY_CAMEL_CASE.equals(str)) {
                                                                                                                                                if (!"api_builtin_enabled".equals(str)) {
                                                                                                                                                    if (!API_BUILTIN_ENABLED_KEY_CAMEL_CASE.equals(str)) {
                                                                                                                                                        if (!"new_builtin_class_resolver".equals(str)) {
                                                                                                                                                            if (!NEW_BUILTIN_CLASS_RESOLVER_KEY_CAMEL_CASE.equals(str)) {
                                                                                                                                                                if (!"log_template_exceptions".equals(str)) {
                                                                                                                                                                    if (!LOG_TEMPLATE_EXCEPTIONS_KEY_CAMEL_CASE.equals(str)) {
                                                                                                                                                                        z = true;
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                setLogTemplateExceptions(StringUtil.getYesNo(str2));
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                        if ("unrestricted".equals(str2)) {
                                                                                                                                                            setNewBuiltinClassResolver(TemplateClassResolver.UNRESTRICTED_RESOLVER);
                                                                                                                                                        } else if ("safer".equals(str2)) {
                                                                                                                                                            setNewBuiltinClassResolver(TemplateClassResolver.SAFER_RESOLVER);
                                                                                                                                                        } else {
                                                                                                                                                            if (!"allows_nothing".equals(str2)) {
                                                                                                                                                                if (!"allowsNothing".equals(str2)) {
                                                                                                                                                                    if (str2.indexOf(Config.TRACE_TODAY_VISIT_SPLIT) != -1) {
                                                                                                                                                                        ArrayList parseAsSegmentedList = parseAsSegmentedList(str2);
                                                                                                                                                                        HashSet hashSet = null;
                                                                                                                                                                        List list = null;
                                                                                                                                                                        for (int i = 0; i < parseAsSegmentedList.size(); i++) {
                                                                                                                                                                            KeyValuePair keyValuePair = (KeyValuePair) parseAsSegmentedList.get(i);
                                                                                                                                                                            String str3 = (String) keyValuePair.getKey();
                                                                                                                                                                            List list2 = (List) keyValuePair.getValue();
                                                                                                                                                                            if (str3.equals(ALLOWED_CLASSES)) {
                                                                                                                                                                                hashSet = new HashSet(list2);
                                                                                                                                                                            } else if (str3.equals(TRUSTED_TEMPLATES)) {
                                                                                                                                                                                list = list2;
                                                                                                                                                                            } else {
                                                                                                                                                                                StringBuffer stringBuffer = new StringBuffer();
                                                                                                                                                                                stringBuffer.append("Unrecognized list segment key: ");
                                                                                                                                                                                stringBuffer.append(StringUtil.jQuote(str3));
                                                                                                                                                                                stringBuffer.append(". Supported keys are: \"");
                                                                                                                                                                                stringBuffer.append(ALLOWED_CLASSES);
                                                                                                                                                                                stringBuffer.append("\", \"");
                                                                                                                                                                                stringBuffer.append(TRUSTED_TEMPLATES);
                                                                                                                                                                                stringBuffer.append("\"");
                                                                                                                                                                                throw new ParseException(stringBuffer.toString(), 0, 0);
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                        setNewBuiltinClassResolver(new OptInTemplateClassResolver(hashSet, list));
                                                                                                                                                                    } else if (str2.indexOf(46) != -1) {
                                                                                                                                                                        if (class$freemarker$core$TemplateClassResolver == null) {
                                                                                                                                                                            cls4 = class$("freemarker.core.TemplateClassResolver");
                                                                                                                                                                            class$freemarker$core$TemplateClassResolver = cls4;
                                                                                                                                                                        } else {
                                                                                                                                                                            cls4 = class$freemarker$core$TemplateClassResolver;
                                                                                                                                                                        }
                                                                                                                                                                        setNewBuiltinClassResolver((TemplateClassResolver) _ObjectBuilderSettingEvaluator.eval(str2, cls4, _SettingEvaluationEnvironment.getCurrent()));
                                                                                                                                                                    } else {
                                                                                                                                                                        throw invalidSettingValueException(str, str2);
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                            setNewBuiltinClassResolver(TemplateClassResolver.ALLOWS_NOTHING_RESOLVER);
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                                setAPIBuiltinEnabled(StringUtil.getYesNo(str2));
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        setShowErrorTips(StringUtil.getYesNo(str2));
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                setAutoFlush(StringUtil.getYesNo(str2));
                                                                                                                            }
                                                                                                                        }
                                                                                                                        setStrictBeanModels(StringUtil.getYesNo(str2));
                                                                                                                    }
                                                                                                                }
                                                                                                                setURLEscapingCharset(str2);
                                                                                                            }
                                                                                                        }
                                                                                                        setOutputEncoding(str2);
                                                                                                    }
                                                                                                }
                                                                                                setBooleanFormat(str2);
                                                                                            }
                                                                                        }
                                                                                        if ("default".equalsIgnoreCase(str2)) {
                                                                                            if (this instanceof Configuration) {
                                                                                                ((Configuration) this).unsetObjectWrapper();
                                                                                            } else {
                                                                                                setObjectWrapper(Configuration.getDefaultObjectWrapper(Configuration.VERSION_2_3_0));
                                                                                            }
                                                                                        } else if (DEFAULT_2_3_0.equalsIgnoreCase(str2)) {
                                                                                            setObjectWrapper(Configuration.getDefaultObjectWrapper(Configuration.VERSION_2_3_0));
                                                                                        } else if ("simple".equalsIgnoreCase(str2)) {
                                                                                            setObjectWrapper(ObjectWrapper.SIMPLE_WRAPPER);
                                                                                        } else if ("beans".equalsIgnoreCase(str2)) {
                                                                                            setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
                                                                                        } else if ("jython".equalsIgnoreCase(str2)) {
                                                                                            setObjectWrapper((ObjectWrapper) Class.forName("freemarker.ext.jython.JythonWrapper").getField("INSTANCE").get(null));
                                                                                        } else {
                                                                                            if (class$freemarker$template$ObjectWrapper == null) {
                                                                                                cls3 = class$("freemarker.template.ObjectWrapper");
                                                                                                class$freemarker$template$ObjectWrapper = cls3;
                                                                                            } else {
                                                                                                cls3 = class$freemarker$template$ObjectWrapper;
                                                                                            }
                                                                                            setObjectWrapper((ObjectWrapper) _ObjectBuilderSettingEvaluator.eval(str2, cls3, _SettingEvaluationEnvironment.getCurrent()));
                                                                                        }
                                                                                    }
                                                                                }
                                                                                if (str2.indexOf(46) != -1) {
                                                                                    if (class$freemarker$core$ArithmeticEngine == null) {
                                                                                        cls2 = class$("freemarker.core.ArithmeticEngine");
                                                                                        class$freemarker$core$ArithmeticEngine = cls2;
                                                                                    } else {
                                                                                        cls2 = class$freemarker$core$ArithmeticEngine;
                                                                                    }
                                                                                    setArithmeticEngine((ArithmeticEngine) _ObjectBuilderSettingEvaluator.eval(str2, cls2, _SettingEvaluationEnvironment.getCurrent()));
                                                                                } else if ("bigdecimal".equalsIgnoreCase(str2)) {
                                                                                    setArithmeticEngine(ArithmeticEngine.BIGDECIMAL_ENGINE);
                                                                                } else if ("conservative".equalsIgnoreCase(str2)) {
                                                                                    setArithmeticEngine(ArithmeticEngine.CONSERVATIVE_ENGINE);
                                                                                } else {
                                                                                    throw invalidSettingValueException(str, str2);
                                                                                }
                                                                            }
                                                                        }
                                                                        if (str2.indexOf(46) != -1) {
                                                                            if (class$freemarker$template$TemplateExceptionHandler == null) {
                                                                                cls = class$("freemarker.template.TemplateExceptionHandler");
                                                                                class$freemarker$template$TemplateExceptionHandler = cls;
                                                                            } else {
                                                                                cls = class$freemarker$template$TemplateExceptionHandler;
                                                                            }
                                                                            setTemplateExceptionHandler((TemplateExceptionHandler) _ObjectBuilderSettingEvaluator.eval(str2, cls, _SettingEvaluationEnvironment.getCurrent()));
                                                                        } else if (BuildConfig.BUILD_TYPE.equalsIgnoreCase(str2)) {
                                                                            setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER);
                                                                        } else {
                                                                            if (!"html_debug".equalsIgnoreCase(str2)) {
                                                                                if (!"htmlDebug".equals(str2)) {
                                                                                    if ("ignore".equalsIgnoreCase(str2)) {
                                                                                        setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
                                                                                    } else if ("rethrow".equalsIgnoreCase(str2)) {
                                                                                        setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
                                                                                    } else if (!"default".equalsIgnoreCase(str2) || !(this instanceof Configuration)) {
                                                                                        throw invalidSettingValueException(str, str2);
                                                                                    } else {
                                                                                        ((Configuration) this).unsetTemplateExceptionHandler();
                                                                                    }
                                                                                }
                                                                            }
                                                                            setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
                                                                        }
                                                                    }
                                                                }
                                                                char charAt = (str2 == null || str2.length() <= 0) ? 0 : str2.charAt(0);
                                                                if (!Character.isDigit(charAt) && charAt != '+') {
                                                                    if (charAt != '-') {
                                                                        setClassicCompatible(str2 != null ? StringUtil.getYesNo(str2) : false);
                                                                    }
                                                                }
                                                                setClassicCompatibleAsInt(Integer.parseInt(str2));
                                                            }
                                                        }
                                                        if (!str2.equals("null")) {
                                                            timeZone2 = parseTimeZoneSettingValue(str2);
                                                        }
                                                        setSQLDateAndTimeTimeZone(timeZone2);
                                                    }
                                                }
                                                setTimeZone(parseTimeZoneSettingValue(str2));
                                            }
                                        }
                                        setDateTimeFormat(str2);
                                    }
                                }
                                setDateFormat(str2);
                            }
                        }
                        setTimeFormat(str2);
                    }
                }
                setNumberFormat(str2);
            }
            if (z) {
                throw unknownSettingException(str);
            }
        } catch (Exception e) {
            throw settingValueAssignmentException(str, str2, e);
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* access modifiers changed from: package-private */
    public Set getSettingNames(boolean z) {
        return new _SortedArraySet(z ? SETTING_NAMES_CAMEL_CASE : SETTING_NAMES_SNAKE_CASE);
    }

    private TimeZone parseTimeZoneSettingValue(String str) {
        if (JVM_DEFAULT.equalsIgnoreCase(str)) {
            return TimeZone.getDefault();
        }
        return TimeZone.getTimeZone(str);
    }

    public void setStrictBeanModels(boolean z) {
        ObjectWrapper objectWrapper2 = this.objectWrapper;
        if (!(objectWrapper2 instanceof BeansWrapper)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("The value of the object_wrapper setting isn't a ");
            Class cls = class$freemarker$ext$beans$BeansWrapper;
            if (cls == null) {
                cls = class$("freemarker.ext.beans.BeansWrapper");
                class$freemarker$ext$beans$BeansWrapper = cls;
            }
            stringBuffer.append(cls.getName());
            stringBuffer.append(FileUtil.HIDDEN_PREFIX);
            throw new IllegalStateException(stringBuffer.toString());
        }
        ((BeansWrapper) objectWrapper2).setStrict(z);
    }

    public String getSetting(String str) {
        return this.properties.getProperty(str);
    }

    public Map getSettings() {
        return Collections.unmodifiableMap(this.properties);
    }

    /* access modifiers changed from: protected */
    public Environment getEnvironment() {
        if (this instanceof Environment) {
            return (Environment) this;
        }
        return Environment.getCurrentEnvironment();
    }

    /* access modifiers changed from: protected */
    public TemplateException unknownSettingException(String str) {
        return new UnknownSettingException(getEnvironment(), str, getCorrectedNameForUnknownSetting(str));
    }

    /* access modifiers changed from: protected */
    public TemplateException settingValueAssignmentException(String str, String str2, Throwable th) {
        return new SettingValueAssignmentException(getEnvironment(), str, str2, th);
    }

    /* access modifiers changed from: protected */
    public TemplateException invalidSettingValueException(String str, String str2) {
        return new _MiscTemplateException(getEnvironment(), new Object[]{"Invalid value for setting ", new _DelayedJQuote(str), ": ", new _DelayedJQuote(str2)});
    }

    public static class UnknownSettingException extends _MiscTemplateException {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private UnknownSettingException(freemarker.core.Environment r6, java.lang.String r7, java.lang.String r8) {
            /*
                r5 = this;
                r0 = 3
                java.lang.Object[] r0 = new java.lang.Object[r0]
                r1 = 0
                java.lang.String r2 = "Unknown FreeMarker configuration setting: "
                r0[r1] = r2
                freemarker.core._DelayedJQuote r2 = new freemarker.core._DelayedJQuote
                r2.<init>(r7)
                r7 = 1
                r0[r7] = r2
                r2 = 2
                if (r8 != 0) goto L_0x0016
                java.lang.String r7 = ""
                goto L_0x0024
            L_0x0016:
                java.lang.Object[] r3 = new java.lang.Object[r2]
                java.lang.String r4 = ". You may meant: "
                r3[r1] = r4
                freemarker.core._DelayedJQuote r1 = new freemarker.core._DelayedJQuote
                r1.<init>(r8)
                r3[r7] = r1
                r7 = r3
            L_0x0024:
                r0[r2] = r7
                r5.<init>(r6, r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: freemarker.core.Configurable.UnknownSettingException.<init>(freemarker.core.Environment, java.lang.String, java.lang.String):void");
        }
    }

    public static class SettingValueAssignmentException extends _MiscTemplateException {
        private SettingValueAssignmentException(Environment environment, String str, String str2, Throwable th) {
            super(th, environment, new Object[]{"Failed to set FreeMarker configuration setting ", new _DelayedJQuote(str), " to value ", new _DelayedJQuote(str2), "; see cause exception."});
        }
    }

    public void setSettings(Properties properties2) throws TemplateException {
        _SettingEvaluationEnvironment startScope = _SettingEvaluationEnvironment.startScope();
        try {
            for (String str : properties2.keySet()) {
                setSetting(str, properties2.getProperty(str).trim());
            }
        } finally {
            _SettingEvaluationEnvironment.endScope(startScope);
        }
    }

    public void setSettings(InputStream inputStream) throws TemplateException, IOException {
        Properties properties2 = new Properties();
        properties2.load(inputStream);
        setSettings(properties2);
    }

    /* access modifiers changed from: package-private */
    public void setCustomAttribute(Object obj, Object obj2) {
        synchronized (this.customAttributes) {
            this.customAttributes.put(obj, obj2);
        }
    }

    /* access modifiers changed from: package-private */
    public Object getCustomAttribute(Object obj, CustomAttribute customAttribute) {
        Object obj2;
        synchronized (this.customAttributes) {
            obj2 = this.customAttributes.get(obj);
            if (obj2 == null && !this.customAttributes.containsKey(obj)) {
                obj2 = customAttribute.create();
                this.customAttributes.put(obj, obj2);
            }
        }
        return obj2;
    }

    public void setCustomAttribute(String str, Object obj) {
        synchronized (this.customAttributes) {
            this.customAttributes.put(str, obj);
        }
    }

    public String[] getCustomAttributeNames() {
        String[] strArr;
        synchronized (this.customAttributes) {
            LinkedList linkedList = new LinkedList(this.customAttributes.keySet());
            Iterator it = linkedList.iterator();
            while (it.hasNext()) {
                if (!(it.next() instanceof String)) {
                    it.remove();
                }
            }
            strArr = (String[]) linkedList.toArray(new String[linkedList.size()]);
        }
        return strArr;
    }

    public void removeCustomAttribute(String str) {
        synchronized (this.customAttributes) {
            this.customAttributes.remove(str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        if (r1 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0019, code lost:
        r0 = r3.parent;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001b, code lost:
        if (r0 == null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0021, code lost:
        return r0.getCustomAttribute(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0022, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object getCustomAttribute(java.lang.String r4) {
        /*
            r3 = this;
            java.util.HashMap r0 = r3.customAttributes
            monitor-enter(r0)
            java.util.HashMap r1 = r3.customAttributes     // Catch:{ all -> 0x0023 }
            java.lang.Object r1 = r1.get(r4)     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0016
            java.util.HashMap r2 = r3.customAttributes     // Catch:{ all -> 0x0023 }
            boolean r2 = r2.containsKey(r4)     // Catch:{ all -> 0x0023 }
            if (r2 == 0) goto L_0x0016
            r4 = 0
            monitor-exit(r0)     // Catch:{ all -> 0x0023 }
            return r4
        L_0x0016:
            monitor-exit(r0)     // Catch:{ all -> 0x0023 }
            if (r1 != 0) goto L_0x0022
            freemarker.core.Configurable r0 = r3.parent
            if (r0 == 0) goto L_0x0022
            java.lang.Object r4 = r0.getCustomAttribute(r4)
            return r4
        L_0x0022:
            return r1
        L_0x0023:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0023 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.core.Configurable.getCustomAttribute(java.lang.String):java.lang.Object");
    }

    /* access modifiers changed from: protected */
    public void doAutoImportsAndIncludes(Environment environment) throws TemplateException, IOException {
        Configurable configurable = this.parent;
        if (configurable != null) {
            configurable.doAutoImportsAndIncludes(environment);
        }
    }

    /* access modifiers changed from: protected */
    public ArrayList parseAsList(String str) throws ParseException {
        return new SettingStringParser(str).parseAsList();
    }

    /* access modifiers changed from: protected */
    public ArrayList parseAsSegmentedList(String str) throws ParseException {
        return new SettingStringParser(str).parseAsSegmentedList();
    }

    /* access modifiers changed from: protected */
    public HashMap parseAsImportList(String str) throws ParseException {
        return new SettingStringParser(str).parseAsImportList();
    }

    private static class KeyValuePair {
        private final Object key;
        private final Object value;

        KeyValuePair(Object obj, Object obj2) {
            this.key = obj;
            this.value = obj2;
        }

        /* access modifiers changed from: package-private */
        public Object getKey() {
            return this.key;
        }

        /* access modifiers changed from: package-private */
        public Object getValue() {
            return this.value;
        }
    }

    private static class SettingStringParser {

        /* renamed from: ln */
        private int f7454ln;

        /* renamed from: p */
        private int f7455p;
        private String text;

        private SettingStringParser(String str) {
            this.text = str;
            this.f7455p = 0;
            this.f7454ln = str.length();
        }

        /* access modifiers changed from: package-private */
        public ArrayList parseAsSegmentedList() throws ParseException {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = null;
            while (skipWS() != ' ') {
                String fetchStringValue = fetchStringValue();
                char skipWS = skipWS();
                if (skipWS == ':') {
                    arrayList2 = new ArrayList();
                    arrayList.add(new KeyValuePair(fetchStringValue, arrayList2));
                } else if (arrayList2 != null) {
                    arrayList2.add(fetchStringValue);
                } else {
                    throw new ParseException("The very first list item must be followed by \":\" so it will be the key for the following sub-list.", 0, 0);
                }
                if (skipWS == ' ') {
                    break;
                } else if (skipWS == ',' || skipWS == ':') {
                    this.f7455p++;
                } else {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Expected \",\" or \":\" or the end of text but found \"");
                    stringBuffer.append(skipWS);
                    stringBuffer.append("\"");
                    throw new ParseException(stringBuffer.toString(), 0, 0);
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        public ArrayList parseAsList() throws ParseException {
            ArrayList arrayList = new ArrayList();
            while (skipWS() != ' ') {
                arrayList.add(fetchStringValue());
                char skipWS = skipWS();
                if (skipWS == ' ') {
                    break;
                } else if (skipWS == ',') {
                    this.f7455p++;
                } else {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Expected \",\" or the end of text but found \"");
                    stringBuffer.append(skipWS);
                    stringBuffer.append("\"");
                    throw new ParseException(stringBuffer.toString(), 0, 0);
                }
            }
            return arrayList;
        }

        /* access modifiers changed from: package-private */
        public HashMap parseAsImportList() throws ParseException {
            HashMap hashMap = new HashMap();
            while (skipWS() != ' ') {
                String fetchStringValue = fetchStringValue();
                if (skipWS() != ' ') {
                    String fetchKeyword = fetchKeyword();
                    if (!fetchKeyword.equalsIgnoreCase("as")) {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("Expected \"as\", but found ");
                        stringBuffer.append(StringUtil.jQuote(fetchKeyword));
                        throw new ParseException(stringBuffer.toString(), 0, 0);
                    } else if (skipWS() != ' ') {
                        hashMap.put(fetchStringValue(), fetchStringValue);
                        char skipWS = skipWS();
                        if (skipWS == ' ') {
                            break;
                        } else if (skipWS == ',') {
                            this.f7455p++;
                        } else {
                            StringBuffer stringBuffer2 = new StringBuffer();
                            stringBuffer2.append("Expected \",\" or the end of text but found \"");
                            stringBuffer2.append(skipWS);
                            stringBuffer2.append("\"");
                            throw new ParseException(stringBuffer2.toString(), 0, 0);
                        }
                    } else {
                        throw new ParseException("Unexpected end of text: expected gate hash name", 0, 0);
                    }
                } else {
                    throw new ParseException("Unexpected end of text: expected \"as\"", 0, 0);
                }
            }
            return hashMap;
        }

        /* access modifiers changed from: package-private */
        public String fetchStringValue() throws ParseException {
            String fetchWord = fetchWord();
            if (fetchWord.startsWith("'") || fetchWord.startsWith("\"")) {
                fetchWord = fetchWord.substring(1, fetchWord.length() - 1);
            }
            return StringUtil.FTLStringLiteralDec(fetchWord);
        }

        /* access modifiers changed from: package-private */
        public String fetchKeyword() throws ParseException {
            String fetchWord = fetchWord();
            if (!fetchWord.startsWith("'") && !fetchWord.startsWith("\"")) {
                return fetchWord;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Keyword expected, but a string value found: ");
            stringBuffer.append(fetchWord);
            throw new ParseException(stringBuffer.toString(), 0, 0);
        }

        /* access modifiers changed from: package-private */
        public char skipWS() {
            while (true) {
                int i = this.f7455p;
                if (i >= this.f7454ln) {
                    return ' ';
                }
                char charAt = this.text.charAt(i);
                if (!Character.isWhitespace(charAt)) {
                    return charAt;
                }
                this.f7455p++;
            }
        }

        private String fetchWord() throws ParseException {
            char charAt;
            int i = this.f7455p;
            if (i != this.f7454ln) {
                char charAt2 = this.text.charAt(i);
                int i2 = this.f7455p;
                if (charAt2 == '\'' || charAt2 == '\"') {
                    this.f7455p++;
                    boolean z = false;
                    while (true) {
                        int i3 = this.f7455p;
                        if (i3 >= this.f7454ln) {
                            break;
                        }
                        char charAt3 = this.text.charAt(i3);
                        if (z) {
                            z = false;
                        } else if (charAt3 == '\\') {
                            z = true;
                        } else if (charAt3 == charAt2) {
                            break;
                        }
                        this.f7455p++;
                    }
                    int i4 = this.f7455p;
                    if (i4 != this.f7454ln) {
                        this.f7455p = i4 + 1;
                        return this.text.substring(i2, this.f7455p);
                    }
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Missing ");
                    stringBuffer.append(charAt2);
                    throw new ParseException(stringBuffer.toString(), 0, 0);
                }
                do {
                    charAt = this.text.charAt(this.f7455p);
                    if (!Character.isLetterOrDigit(charAt) && charAt != '/' && charAt != '\\' && charAt != '_' && charAt != '.' && charAt != '-' && charAt != '!' && charAt != '*' && charAt != '?') {
                        break;
                    }
                    this.f7455p++;
                } while (this.f7455p < this.f7454ln);
                int i5 = this.f7455p;
                if (i2 != i5) {
                    return this.text.substring(i2, i5);
                }
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("Unexpected character: ");
                stringBuffer2.append(charAt);
                throw new ParseException(stringBuffer2.toString(), 0, 0);
            }
            throw new ParseException("Unexpeced end of text", 0, 0);
        }
    }
}
