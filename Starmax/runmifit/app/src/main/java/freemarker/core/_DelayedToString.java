package freemarker.core;

public class _DelayedToString extends _DelayedConversionToString {
    public _DelayedToString(Object obj) {
        super(obj);
    }

    public _DelayedToString(int i) {
        super(new Integer(i));
    }

    /* access modifiers changed from: protected */
    public String doConversion(Object obj) {
        return String.valueOf(obj);
    }
}
