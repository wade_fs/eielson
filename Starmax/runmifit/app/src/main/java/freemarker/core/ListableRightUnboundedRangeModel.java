package freemarker.core;

import freemarker.template.SimpleNumber;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import java.math.BigInteger;

final class ListableRightUnboundedRangeModel extends RightUnboundedRangeModel implements TemplateCollectionModel {
    public int size() throws TemplateModelException {
        return Integer.MAX_VALUE;
    }

    ListableRightUnboundedRangeModel(int i) {
        super(i);
    }

    public TemplateModelIterator iterator() throws TemplateModelException {
        return new TemplateModelIterator() {
            /* class freemarker.core.ListableRightUnboundedRangeModel.C32991 */
            boolean needInc;
            BigInteger nextBigInteger;
            int nextInt = ListableRightUnboundedRangeModel.this.getBegining();
            long nextLong;
            int nextType = 1;

            public boolean hasNext() throws TemplateModelException {
                return true;
            }

            public TemplateModel next() throws TemplateModelException {
                if (this.needInc) {
                    int i = this.nextType;
                    if (i == 1) {
                        int i2 = this.nextInt;
                        if (i2 < Integer.MAX_VALUE) {
                            this.nextInt = i2 + 1;
                        } else {
                            this.nextType = 2;
                            this.nextLong = ((long) i2) + 1;
                        }
                    } else if (i != 2) {
                        this.nextBigInteger = this.nextBigInteger.add(BigInteger.ONE);
                    } else {
                        long j = this.nextLong;
                        if (j < Long.MAX_VALUE) {
                            this.nextLong = j + 1;
                        } else {
                            this.nextType = 3;
                            this.nextBigInteger = BigInteger.valueOf(j);
                            this.nextBigInteger = this.nextBigInteger.add(BigInteger.ONE);
                        }
                    }
                }
                this.needInc = true;
                int i3 = this.nextType;
                if (i3 == 1) {
                    return new SimpleNumber(this.nextInt);
                }
                return i3 == 2 ? new SimpleNumber(this.nextLong) : new SimpleNumber(this.nextBigInteger);
            }
        };
    }
}
