package freemarker.ext.beans;

import freemarker.ext.beans._BeansAPI;
import freemarker.template.Version;
import java.lang.ref.ReferenceQueue;
import java.util.Map;
import java.util.WeakHashMap;

public class BeansWrapperBuilder extends BeansWrapperConfiguration {
    private static final WeakHashMap INSTANCE_CACHE = new WeakHashMap();
    private static final ReferenceQueue INSTANCE_CACHE_REF_QUEUE = new ReferenceQueue();

    private static class BeansWrapperFactory implements _BeansAPI._BeansWrapperSubclassFactory {
        /* access modifiers changed from: private */
        public static final BeansWrapperFactory INSTANCE = new BeansWrapperFactory();

        private BeansWrapperFactory() {
        }

        public BeansWrapper create(BeansWrapperConfiguration beansWrapperConfiguration) {
            return new BeansWrapper(beansWrapperConfiguration, true);
        }
    }

    public BeansWrapperBuilder(Version version) {
        super(version);
    }

    static void clearInstanceCache() {
        synchronized (INSTANCE_CACHE) {
            INSTANCE_CACHE.clear();
        }
    }

    static Map getInstanceCache() {
        return INSTANCE_CACHE;
    }

    public BeansWrapper build() {
        return _BeansAPI.getBeansWrapperSubclassSingleton(super, INSTANCE_CACHE, INSTANCE_CACHE_REF_QUEUE, BeansWrapperFactory.INSTANCE);
    }
}
