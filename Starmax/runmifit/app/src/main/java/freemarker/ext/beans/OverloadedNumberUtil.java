package freemarker.ext.beans;

import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceConstant;
import com.tencent.bugly.beta.tinker.TinkerReport;
import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.NumberUtil;
import java.math.BigDecimal;
import java.math.BigInteger;

class OverloadedNumberUtil {
    static final int BIG_MANTISSA_LOSS_PRICE = 40000;
    private static final double HIGHEST_BELOW_ONE = 0.999999d;
    private static final double LOWEST_ABOVE_ZERO = 1.0E-6d;
    private static final long MAX_DOUBLE_OR_LONG = 9007199254740992L;
    private static final int MAX_DOUBLE_OR_LONG_LOG_2 = 53;
    private static final int MAX_FLOAT_OR_INT = 16777216;
    private static final int MAX_FLOAT_OR_INT_LOG_2 = 24;
    private static final long MIN_DOUBLE_OR_LONG = -9007199254740992L;
    private static final int MIN_FLOAT_OR_INT = -16777216;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;

    /* renamed from: class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrDouble */
    static /* synthetic */ Class f7466xf08b2adc;

    /* renamed from: class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrFloat */
    static /* synthetic */ Class f7467xe6d4e5f1;

    /* renamed from: class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrInteger */
    static /* synthetic */ Class f7468x2797b393;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;

    /* renamed from: class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrShort */
    static /* synthetic */ Class f7469xe78a4811;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;

    /* renamed from: class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrIntegerOrFloat */
    static /* synthetic */ Class f7470xfd0e71d9;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;

    /* renamed from: class$freemarker$ext$beans$OverloadedNumberUtil$IntegerBigDecimal */
    static /* synthetic */ Class f7471x8fa423c5;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
    static /* synthetic */ Class class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
    static /* synthetic */ Class class$java$lang$Byte;
    static /* synthetic */ Class class$java$lang$Double;
    static /* synthetic */ Class class$java$lang$Float;
    static /* synthetic */ Class class$java$lang$Integer;
    static /* synthetic */ Class class$java$lang$Long;
    static /* synthetic */ Class class$java$lang$Short;
    static /* synthetic */ Class class$java$math$BigDecimal;
    static /* synthetic */ Class class$java$math$BigInteger;

    interface BigDecimalSource {
        BigDecimal bigDecimalValue();
    }

    interface BigIntegerSource {
        BigInteger bigIntegerValue();
    }

    interface ByteSource {
        Byte byteValue();
    }

    interface DoubleSource {
        Double doubleValue();
    }

    interface FloatSource {
        Float floatValue();
    }

    interface IntegerSource {
        Integer integerValue();
    }

    interface LongSource {
        Long longValue();
    }

    interface ShortSource {
        Short shortValue();
    }

    private OverloadedNumberUtil() {
    }

    static Number addFallbackType(Number number, int i) {
        int i2 = i;
        Class<?> cls = number.getClass();
        Class<?> cls2 = class$java$math$BigDecimal;
        if (cls2 == null) {
            cls2 = class$("java.math.BigDecimal");
            class$java$math$BigDecimal = cls2;
        }
        if (cls == cls2) {
            BigDecimal bigDecimal = (BigDecimal) number;
            return ((i2 & 316) == 0 || (i2 & 704) == 0 || !NumberUtil.isIntegerBigDecimal(bigDecimal)) ? bigDecimal : new IntegerBigDecimal(bigDecimal);
        }
        Class<?> cls3 = class$java$lang$Integer;
        if (cls3 == null) {
            cls3 = class$("java.lang.Integer");
            class$java$lang$Integer = cls3;
        }
        if (cls == cls3) {
            int intValue = number.intValue();
            if ((i2 & 4) == 0 || intValue > 127 || intValue < -128) {
                return ((i2 & 8) == 0 || intValue > 32767 || intValue < -32768) ? number : new IntegerOrShort((Integer) number, (short) intValue);
            }
            return new IntegerOrByte((Integer) number, (byte) intValue);
        }
        Class<?> cls4 = class$java$lang$Long;
        if (cls4 == null) {
            cls4 = class$("java.lang.Long");
            class$java$lang$Long = cls4;
        }
        if (cls == cls4) {
            long longValue = number.longValue();
            if ((i2 & 4) != 0 && longValue <= 127 && longValue >= -128) {
                return new LongOrByte((Long) number, (byte) ((int) longValue));
            }
            if ((i2 & 8) == 0 || longValue > 32767 || longValue < -32768) {
                return ((i2 & 16) == 0 || longValue > 2147483647L || longValue < -2147483648L) ? number : new LongOrInteger((Long) number, (int) longValue);
            }
            return new LongOrShort((Long) number, (short) ((int) longValue));
        }
        Class<?> cls5 = class$java$lang$Double;
        if (cls5 == null) {
            cls5 = class$("java.lang.Double");
            class$java$lang$Double = cls5;
        }
        boolean z = false;
        if (cls == cls5) {
            double doubleValue = number.doubleValue();
            if ((i2 & 316) != 0 && doubleValue <= 9.007199254740992E15d && doubleValue >= -9.007199254740992E15d) {
                long longValue2 = number.longValue();
                double d = (double) longValue2;
                Double.isNaN(d);
                double d2 = doubleValue - d;
                if (d2 == 0.0d) {
                    z = true;
                } else if (d2 > 0.0d) {
                    if (d2 >= 1.0E-6d) {
                        if (d2 > HIGHEST_BELOW_ONE) {
                            longValue2++;
                        }
                    }
                } else if (d2 <= -1.0E-6d) {
                    if (d2 < -0.999999d) {
                        longValue2--;
                    }
                }
                if ((i2 & 4) != 0 && longValue2 <= 127 && longValue2 >= -128) {
                    return new DoubleOrByte((Double) number, (byte) ((int) longValue2));
                }
                if ((i2 & 8) != 0 && longValue2 <= 32767 && longValue2 >= -32768) {
                    return new DoubleOrShort((Double) number, (short) ((int) longValue2));
                }
                if ((i2 & 16) != 0 && longValue2 <= 2147483647L && longValue2 >= -2147483648L) {
                    int i3 = (int) longValue2;
                    return ((i2 & 64) == 0 || i3 < -16777216 || i3 > 16777216) ? new DoubleOrInteger((Double) number, i3) : new DoubleOrIntegerOrFloat((Double) number, i3);
                } else if ((i2 & 32) != 0) {
                    if (z) {
                        return new DoubleOrLong((Double) number, longValue2);
                    }
                    if (longValue2 >= -2147483648L && longValue2 <= 2147483647L) {
                        return new DoubleOrLong((Double) number, longValue2);
                    }
                }
            }
            return ((i2 & 64) == 0 || doubleValue < -3.4028234663852886E38d || doubleValue > 3.4028234663852886E38d) ? number : new DoubleOrFloat((Double) number);
        }
        Class<?> cls6 = class$java$lang$Float;
        if (cls6 == null) {
            cls6 = class$("java.lang.Float");
            class$java$lang$Float = cls6;
        }
        if (cls == cls6) {
            float floatValue = number.floatValue();
            if ((i2 & 316) != 0 && floatValue <= 1.6777216E7f && floatValue >= -1.6777216E7f) {
                int intValue2 = number.intValue();
                double d3 = (double) (floatValue - ((float) intValue2));
                if (d3 == 0.0d) {
                    z = true;
                } else if (intValue2 >= -128 && intValue2 <= 127) {
                    if (d3 > 0.0d) {
                        if (d3 >= 1.0E-5d) {
                            if (d3 > 0.99999d) {
                                intValue2++;
                            }
                        }
                    } else if (d3 <= -1.0E-5d) {
                        if (d3 < -0.99999d) {
                            intValue2--;
                        }
                    }
                }
                if ((i2 & 4) != 0 && intValue2 <= 127 && intValue2 >= -128) {
                    return new FloatOrByte((Float) number, (byte) intValue2);
                }
                if ((i2 & 8) != 0 && intValue2 <= 32767 && intValue2 >= -32768) {
                    return new FloatOrShort((Float) number, (short) intValue2);
                }
                if ((i2 & 16) != 0) {
                    return new FloatOrInteger((Float) number, intValue2);
                }
                if ((i2 & 32) != 0) {
                    return z ? new FloatOrInteger((Float) number, intValue2) : new FloatOrByte((Float) number, (byte) intValue2);
                }
            }
            return number;
        }
        Class<?> cls7 = class$java$lang$Byte;
        if (cls7 == null) {
            cls7 = class$("java.lang.Byte");
            class$java$lang$Byte = cls7;
        }
        if (cls == cls7) {
            return number;
        }
        Class<?> cls8 = class$java$lang$Short;
        if (cls8 == null) {
            cls8 = class$("java.lang.Short");
            class$java$lang$Short = cls8;
        }
        if (cls == cls8) {
            short shortValue = number.shortValue();
            return ((i2 & 4) == 0 || shortValue > 127 || shortValue < -128) ? number : new ShortOrByte((Short) number, (byte) shortValue);
        }
        Class<?> cls9 = class$java$math$BigInteger;
        if (cls9 == null) {
            cls9 = class$("java.math.BigInteger");
            class$java$math$BigInteger = cls9;
        }
        if (cls == cls9 && (i2 & TinkerReport.KEY_LOADED_EXCEPTION_DEX) != 0) {
            BigInteger bigInteger = (BigInteger) number;
            int bitLength = bigInteger.bitLength();
            if ((i2 & 4) != 0 && bitLength <= 7) {
                return new BigIntegerOrByte(bigInteger);
            }
            if ((i2 & 8) != 0 && bitLength <= 15) {
                return new BigIntegerOrShort(bigInteger);
            }
            if ((i2 & 16) != 0 && bitLength <= 31) {
                return new BigIntegerOrInteger(bigInteger);
            }
            if ((i2 & 32) != 0 && bitLength <= 63) {
                return new BigIntegerOrLong(bigInteger);
            }
            if ((i2 & 64) != 0 && (bitLength <= 24 || (bitLength == 25 && bigInteger.getLowestSetBit() >= 24))) {
                return new BigIntegerOrFloat(bigInteger);
            }
            if ((i2 & 128) != 0 && (bitLength <= 53 || (bitLength == 54 && bigInteger.getLowestSetBit() >= 53))) {
                return new BigIntegerOrDouble(bigInteger);
            }
        }
        return number;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    static abstract class NumberWithFallbackType extends Number implements Comparable {
        /* access modifiers changed from: protected */
        public abstract Number getSourceNumber();

        NumberWithFallbackType() {
        }

        public int intValue() {
            return getSourceNumber().intValue();
        }

        public long longValue() {
            return getSourceNumber().longValue();
        }

        public float floatValue() {
            return getSourceNumber().floatValue();
        }

        public double doubleValue() {
            return getSourceNumber().doubleValue();
        }

        public byte byteValue() {
            return getSourceNumber().byteValue();
        }

        public short shortValue() {
            return getSourceNumber().shortValue();
        }

        public int hashCode() {
            return getSourceNumber().hashCode();
        }

        public boolean equals(Object obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return getSourceNumber().equals(((NumberWithFallbackType) obj).getSourceNumber());
        }

        public String toString() {
            return getSourceNumber().toString();
        }

        public int compareTo(Object obj) {
            Number sourceNumber = getSourceNumber();
            if (sourceNumber instanceof Comparable) {
                return ((Comparable) sourceNumber).compareTo(obj);
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(sourceNumber.getClass().getName());
            stringBuffer.append(" is not Comparable.");
            throw new ClassCastException(stringBuffer.toString());
        }
    }

    static final class IntegerBigDecimal extends NumberWithFallbackType {

        /* renamed from: n */
        private final BigDecimal f7484n;

        IntegerBigDecimal(BigDecimal bigDecimal) {
            this.f7484n = bigDecimal;
        }

        /* access modifiers changed from: protected */
        public Number getSourceNumber() {
            return this.f7484n;
        }

        public BigInteger bigIntegerValue() {
            return this.f7484n.toBigInteger();
        }
    }

    static abstract class LongOrSmallerInteger extends NumberWithFallbackType {

        /* renamed from: n */
        private final Long f7491n;

        protected LongOrSmallerInteger(Long l) {
            this.f7491n = l;
        }

        /* access modifiers changed from: protected */
        public Number getSourceNumber() {
            return this.f7491n;
        }

        public long longValue() {
            return this.f7491n.longValue();
        }
    }

    static class LongOrByte extends LongOrSmallerInteger {

        /* renamed from: w */
        private final byte f7488w;

        LongOrByte(Long l, byte b) {
            super(l);
            this.f7488w = b;
        }

        public byte byteValue() {
            return this.f7488w;
        }
    }

    static class LongOrShort extends LongOrSmallerInteger {

        /* renamed from: w */
        private final short f7490w;

        LongOrShort(Long l, short s) {
            super(l);
            this.f7490w = s;
        }

        public short shortValue() {
            return this.f7490w;
        }
    }

    static class LongOrInteger extends LongOrSmallerInteger {

        /* renamed from: w */
        private final int f7489w;

        LongOrInteger(Long l, int i) {
            super(l);
            this.f7489w = i;
        }

        public int intValue() {
            return this.f7489w;
        }
    }

    static abstract class IntegerOrSmallerInteger extends NumberWithFallbackType {

        /* renamed from: n */
        private final Integer f7487n;

        protected IntegerOrSmallerInteger(Integer num) {
            this.f7487n = num;
        }

        /* access modifiers changed from: protected */
        public Number getSourceNumber() {
            return this.f7487n;
        }

        public int intValue() {
            return this.f7487n.intValue();
        }
    }

    static class IntegerOrByte extends IntegerOrSmallerInteger {

        /* renamed from: w */
        private final byte f7485w;

        IntegerOrByte(Integer num, byte b) {
            super(num);
            this.f7485w = b;
        }

        public byte byteValue() {
            return this.f7485w;
        }
    }

    static class IntegerOrShort extends IntegerOrSmallerInteger {

        /* renamed from: w */
        private final short f7486w;

        IntegerOrShort(Integer num, short s) {
            super(num);
            this.f7486w = s;
        }

        public short shortValue() {
            return this.f7486w;
        }
    }

    static class ShortOrByte extends NumberWithFallbackType {

        /* renamed from: n */
        private final Short f7492n;

        /* renamed from: w */
        private final byte f7493w;

        protected ShortOrByte(Short sh, byte b) {
            this.f7492n = sh;
            this.f7493w = b;
        }

        /* access modifiers changed from: protected */
        public Number getSourceNumber() {
            return this.f7492n;
        }

        public short shortValue() {
            return this.f7492n.shortValue();
        }

        public byte byteValue() {
            return this.f7493w;
        }
    }

    static abstract class DoubleOrWholeNumber extends NumberWithFallbackType {

        /* renamed from: n */
        private final Double f7479n;

        protected DoubleOrWholeNumber(Double d) {
            this.f7479n = d;
        }

        /* access modifiers changed from: protected */
        public Number getSourceNumber() {
            return this.f7479n;
        }

        public double doubleValue() {
            return this.f7479n.doubleValue();
        }
    }

    static final class DoubleOrByte extends DoubleOrWholeNumber {

        /* renamed from: w */
        private final byte f7473w;

        DoubleOrByte(Double d, byte b) {
            super(d);
            this.f7473w = b;
        }

        public byte byteValue() {
            return this.f7473w;
        }

        public short shortValue() {
            return (short) this.f7473w;
        }

        public int intValue() {
            return this.f7473w;
        }

        public long longValue() {
            return (long) this.f7473w;
        }
    }

    static final class DoubleOrShort extends DoubleOrWholeNumber {

        /* renamed from: w */
        private final short f7478w;

        DoubleOrShort(Double d, short s) {
            super(d);
            this.f7478w = s;
        }

        public short shortValue() {
            return this.f7478w;
        }

        public int intValue() {
            return this.f7478w;
        }

        public long longValue() {
            return (long) this.f7478w;
        }
    }

    static final class DoubleOrIntegerOrFloat extends DoubleOrWholeNumber {

        /* renamed from: w */
        private final int f7476w;

        DoubleOrIntegerOrFloat(Double d, int i) {
            super(d);
            this.f7476w = i;
        }

        public int intValue() {
            return this.f7476w;
        }

        public long longValue() {
            return (long) this.f7476w;
        }
    }

    static final class DoubleOrInteger extends DoubleOrWholeNumber {

        /* renamed from: w */
        private final int f7475w;

        DoubleOrInteger(Double d, int i) {
            super(d);
            this.f7475w = i;
        }

        public int intValue() {
            return this.f7475w;
        }

        public long longValue() {
            return (long) this.f7475w;
        }
    }

    static final class DoubleOrLong extends DoubleOrWholeNumber {

        /* renamed from: w */
        private final long f7477w;

        DoubleOrLong(Double d, long j) {
            super(d);
            this.f7477w = j;
        }

        public long longValue() {
            return this.f7477w;
        }
    }

    static final class DoubleOrFloat extends NumberWithFallbackType {

        /* renamed from: n */
        private final Double f7474n;

        DoubleOrFloat(Double d) {
            this.f7474n = d;
        }

        public float floatValue() {
            return this.f7474n.floatValue();
        }

        public double doubleValue() {
            return this.f7474n.doubleValue();
        }

        /* access modifiers changed from: protected */
        public Number getSourceNumber() {
            return this.f7474n;
        }
    }

    static abstract class FloatOrWholeNumber extends NumberWithFallbackType {

        /* renamed from: n */
        private final Float f7483n;

        FloatOrWholeNumber(Float f) {
            this.f7483n = f;
        }

        /* access modifiers changed from: protected */
        public Number getSourceNumber() {
            return this.f7483n;
        }

        public float floatValue() {
            return this.f7483n.floatValue();
        }
    }

    static final class FloatOrByte extends FloatOrWholeNumber {

        /* renamed from: w */
        private final byte f7480w;

        FloatOrByte(Float f, byte b) {
            super(f);
            this.f7480w = b;
        }

        public byte byteValue() {
            return this.f7480w;
        }

        public short shortValue() {
            return (short) this.f7480w;
        }

        public int intValue() {
            return this.f7480w;
        }

        public long longValue() {
            return (long) this.f7480w;
        }
    }

    static final class FloatOrShort extends FloatOrWholeNumber {

        /* renamed from: w */
        private final short f7482w;

        FloatOrShort(Float f, short s) {
            super(f);
            this.f7482w = s;
        }

        public short shortValue() {
            return this.f7482w;
        }

        public int intValue() {
            return this.f7482w;
        }

        public long longValue() {
            return (long) this.f7482w;
        }
    }

    static final class FloatOrInteger extends FloatOrWholeNumber {

        /* renamed from: w */
        private final int f7481w;

        FloatOrInteger(Float f, int i) {
            super(f);
            this.f7481w = i;
        }

        public int intValue() {
            return this.f7481w;
        }

        public long longValue() {
            return (long) this.f7481w;
        }
    }

    static abstract class BigIntegerOrPrimitive extends NumberWithFallbackType {

        /* renamed from: n */
        protected final BigInteger f7472n;

        BigIntegerOrPrimitive(BigInteger bigInteger) {
            this.f7472n = bigInteger;
        }

        /* access modifiers changed from: protected */
        public Number getSourceNumber() {
            return this.f7472n;
        }
    }

    static final class BigIntegerOrByte extends BigIntegerOrPrimitive {
        BigIntegerOrByte(BigInteger bigInteger) {
            super(bigInteger);
        }
    }

    static final class BigIntegerOrShort extends BigIntegerOrPrimitive {
        BigIntegerOrShort(BigInteger bigInteger) {
            super(bigInteger);
        }
    }

    static final class BigIntegerOrInteger extends BigIntegerOrPrimitive {
        BigIntegerOrInteger(BigInteger bigInteger) {
            super(bigInteger);
        }
    }

    static final class BigIntegerOrLong extends BigIntegerOrPrimitive {
        BigIntegerOrLong(BigInteger bigInteger) {
            super(bigInteger);
        }
    }

    static abstract class BigIntegerOrFPPrimitive extends BigIntegerOrPrimitive {
        BigIntegerOrFPPrimitive(BigInteger bigInteger) {
            super(bigInteger);
        }

        public float floatValue() {
            return (float) this.f7472n.longValue();
        }

        public double doubleValue() {
            return (double) this.f7472n.longValue();
        }
    }

    static final class BigIntegerOrFloat extends BigIntegerOrFPPrimitive {
        BigIntegerOrFloat(BigInteger bigInteger) {
            super(bigInteger);
        }
    }

    static final class BigIntegerOrDouble extends BigIntegerOrFPPrimitive {
        BigIntegerOrDouble(BigInteger bigInteger) {
            super(bigInteger);
        }
    }

    static int getArgumentConversionPrice(Class cls, Class cls2) {
        if (cls2 == cls) {
            return 0;
        }
        Class cls3 = class$java$lang$Integer;
        if (cls3 == null) {
            cls3 = class$("java.lang.Integer");
            class$java$lang$Integer = cls3;
        }
        if (cls2 == cls3) {
            Class cls4 = f7471x8fa423c5;
            if (cls4 == null) {
                cls4 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerBigDecimal");
                f7471x8fa423c5 = cls4;
            }
            if (cls == cls4) {
                return 31003;
            }
            Class cls5 = class$java$math$BigDecimal;
            if (cls5 == null) {
                cls5 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls5;
            }
            if (cls == cls5) {
                return 41003;
            }
            Class cls6 = class$java$lang$Long;
            if (cls6 == null) {
                cls6 = class$("java.lang.Long");
                class$java$lang$Long = cls6;
            }
            if (cls == cls6) {
                return Integer.MAX_VALUE;
            }
            Class cls7 = class$java$lang$Double;
            if (cls7 == null) {
                cls7 = class$("java.lang.Double");
                class$java$lang$Double = cls7;
            }
            if (cls == cls7) {
                return Integer.MAX_VALUE;
            }
            Class cls8 = class$java$lang$Float;
            if (cls8 == null) {
                cls8 = class$("java.lang.Float");
                class$java$lang$Float = cls8;
            }
            if (cls == cls8) {
                return Integer.MAX_VALUE;
            }
            Class cls9 = class$java$lang$Byte;
            if (cls9 == null) {
                cls9 = class$("java.lang.Byte");
                class$java$lang$Byte = cls9;
            }
            if (cls == cls9) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ORDER_ID_NULL;
            }
            Class cls10 = class$java$math$BigInteger;
            if (cls10 == null) {
                cls10 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls10;
            }
            if (cls == cls10) {
                return Integer.MAX_VALUE;
            }
            Class cls11 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
            if (cls11 == null) {
                cls11 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger = cls11;
            }
            if (cls == cls11) {
                return 21003;
            }
            Class cls12 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
            if (cls12 == null) {
                cls12 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrFloat");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat = cls12;
            }
            if (cls == cls12) {
                return Integer.MAX_VALUE;
            }
            Class cls13 = f7470xfd0e71d9;
            if (cls13 == null) {
                cls13 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrIntegerOrFloat");
                f7470xfd0e71d9 = cls13;
            }
            if (cls == cls13) {
                return 22003;
            }
            Class cls14 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;
            if (cls14 == null) {
                cls14 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger = cls14;
            }
            if (cls == cls14) {
                return 22003;
            }
            Class cls15 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
            if (cls15 == null) {
                cls15 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong = cls15;
            }
            if (cls == cls15) {
                return Integer.MAX_VALUE;
            }
            Class cls16 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
            if (cls16 == null) {
                cls16 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte = cls16;
            }
            if (cls == cls16) {
                return 0;
            }
            Class cls17 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
            if (cls17 == null) {
                cls17 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte = cls17;
            }
            if (cls == cls17) {
                return 22003;
            }
            Class cls18 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
            if (cls18 == null) {
                cls18 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte = cls18;
            }
            if (cls == cls18) {
                return 21003;
            }
            Class cls19 = class$java$lang$Short;
            if (cls19 == null) {
                cls19 = class$("java.lang.Short");
                class$java$lang$Short = cls19;
            }
            if (cls == cls19) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ORDER_ID_NULL;
            }
            Class cls20 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
            if (cls20 == null) {
                cls20 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort = cls20;
            }
            if (cls == cls20) {
                return 21003;
            }
            Class cls21 = class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
            if (cls21 == null) {
                cls21 = class$("freemarker.ext.beans.OverloadedNumberUtil$ShortOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte = cls21;
            }
            if (cls == cls21) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ORDER_ID_NULL;
            }
            Class cls22 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
            if (cls22 == null) {
                cls22 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger = cls22;
            }
            if (cls == cls22) {
                return 21003;
            }
            Class cls23 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
            if (cls23 == null) {
                cls23 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte = cls23;
            }
            if (cls == cls23) {
                return 21003;
            }
            Class cls24 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;
            if (cls24 == null) {
                cls24 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort = cls24;
            }
            if (cls == cls24) {
                return 21003;
            }
            Class cls25 = f7468x2797b393;
            if (cls25 == null) {
                cls25 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrInteger");
                f7468x2797b393 = cls25;
            }
            if (cls == cls25) {
                return 16003;
            }
            Class cls26 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;
            if (cls26 == null) {
                cls26 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong = cls26;
            }
            if (cls == cls26) {
                return Integer.MAX_VALUE;
            }
            Class cls27 = f7466xf08b2adc;
            if (cls27 == null) {
                cls27 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrDouble");
                f7466xf08b2adc = cls27;
            }
            if (cls == cls27) {
                return Integer.MAX_VALUE;
            }
            Class cls28 = f7467xe6d4e5f1;
            if (cls28 == null) {
                cls28 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrFloat");
                f7467xe6d4e5f1 = cls28;
            }
            if (cls == cls28) {
                return Integer.MAX_VALUE;
            }
            Class cls29 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;
            if (cls29 == null) {
                cls29 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte = cls29;
            }
            if (cls == cls29) {
                return 16003;
            }
            Class cls30 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
            if (cls30 == null) {
                cls30 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort = cls30;
            }
            if (cls == cls30) {
                return 0;
            }
            Class cls31 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
            if (cls31 == null) {
                cls31 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort = cls31;
            }
            if (cls == cls31) {
                return 22003;
            }
            Class cls32 = f7469xe78a4811;
            if (cls32 == null) {
                cls32 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrShort");
                f7469xe78a4811 = cls32;
            }
            if (cls == cls32) {
                return 16003;
            }
            return Integer.MAX_VALUE;
        }
        Class cls33 = class$java$lang$Long;
        if (cls33 == null) {
            cls33 = class$("java.lang.Long");
            class$java$lang$Long = cls33;
        }
        if (cls2 == cls33) {
            Class cls34 = class$java$lang$Integer;
            if (cls34 == null) {
                cls34 = class$("java.lang.Integer");
                class$java$lang$Integer = cls34;
            }
            if (cls == cls34) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ROLE_TYPE_ERROR;
            }
            Class cls35 = f7471x8fa423c5;
            if (cls35 == null) {
                cls35 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerBigDecimal");
                f7471x8fa423c5 = cls35;
            }
            if (cls == cls35) {
                return 31004;
            }
            Class cls36 = class$java$math$BigDecimal;
            if (cls36 == null) {
                cls36 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls36;
            }
            if (cls == cls36) {
                return 41004;
            }
            Class cls37 = class$java$lang$Double;
            if (cls37 == null) {
                cls37 = class$("java.lang.Double");
                class$java$lang$Double = cls37;
            }
            if (cls == cls37) {
                return Integer.MAX_VALUE;
            }
            Class cls38 = class$java$lang$Float;
            if (cls38 == null) {
                cls38 = class$("java.lang.Float");
                class$java$lang$Float = cls38;
            }
            if (cls == cls38) {
                return Integer.MAX_VALUE;
            }
            Class cls39 = class$java$lang$Byte;
            if (cls39 == null) {
                cls39 = class$("java.lang.Byte");
                class$java$lang$Byte = cls39;
            }
            if (cls == cls39) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ROLE_TYPE_ERROR;
            }
            Class cls40 = class$java$math$BigInteger;
            if (cls40 == null) {
                cls40 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls40;
            }
            if (cls == cls40) {
                return Integer.MAX_VALUE;
            }
            Class cls41 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
            if (cls41 == null) {
                cls41 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger = cls41;
            }
            if (cls == cls41) {
                return 0;
            }
            Class cls42 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
            if (cls42 == null) {
                cls42 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrFloat");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat = cls42;
            }
            if (cls == cls42) {
                return Integer.MAX_VALUE;
            }
            Class cls43 = f7470xfd0e71d9;
            if (cls43 == null) {
                cls43 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrIntegerOrFloat");
                f7470xfd0e71d9 = cls43;
            }
            if (cls == cls43) {
                return 21004;
            }
            Class cls44 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;
            if (cls44 == null) {
                cls44 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger = cls44;
            }
            if (cls == cls44) {
                return 21004;
            }
            Class cls45 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
            if (cls45 == null) {
                cls45 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong = cls45;
            }
            if (cls == cls45) {
                return 21004;
            }
            Class cls46 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
            if (cls46 == null) {
                cls46 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte = cls46;
            }
            if (cls == cls46) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ROLE_TYPE_ERROR;
            }
            Class cls47 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
            if (cls47 == null) {
                cls47 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte = cls47;
            }
            if (cls == cls47) {
                return 21004;
            }
            Class cls48 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
            if (cls48 == null) {
                cls48 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte = cls48;
            }
            if (cls == cls48) {
                return 0;
            }
            Class cls49 = class$java$lang$Short;
            if (cls49 == null) {
                cls49 = class$("java.lang.Short");
                class$java$lang$Short = cls49;
            }
            if (cls == cls49) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ROLE_TYPE_ERROR;
            }
            Class cls50 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
            if (cls50 == null) {
                cls50 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort = cls50;
            }
            if (cls == cls50) {
                return 0;
            }
            Class cls51 = class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
            if (cls51 == null) {
                cls51 = class$("freemarker.ext.beans.OverloadedNumberUtil$ShortOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte = cls51;
            }
            if (cls == cls51) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ROLE_TYPE_ERROR;
            }
            Class cls52 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
            if (cls52 == null) {
                cls52 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger = cls52;
            }
            if (cls == cls52) {
                return 21004;
            }
            Class cls53 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
            if (cls53 == null) {
                cls53 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte = cls53;
            }
            if (cls == cls53) {
                return 21004;
            }
            Class cls54 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;
            if (cls54 == null) {
                cls54 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort = cls54;
            }
            if (cls == cls54) {
                return 21004;
            }
            Class cls55 = f7468x2797b393;
            if (cls55 == null) {
                cls55 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrInteger");
                f7468x2797b393 = cls55;
            }
            if (cls == cls55) {
                return 15004;
            }
            Class cls56 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;
            if (cls56 == null) {
                cls56 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong = cls56;
            }
            if (cls == cls56) {
                return 15004;
            }
            Class cls57 = f7466xf08b2adc;
            if (cls57 == null) {
                cls57 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrDouble");
                f7466xf08b2adc = cls57;
            }
            if (cls == cls57) {
                return Integer.MAX_VALUE;
            }
            Class cls58 = f7467xe6d4e5f1;
            if (cls58 == null) {
                cls58 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrFloat");
                f7467xe6d4e5f1 = cls58;
            }
            if (cls == cls58) {
                return Integer.MAX_VALUE;
            }
            Class cls59 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;
            if (cls59 == null) {
                cls59 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte = cls59;
            }
            if (cls == cls59) {
                return 15004;
            }
            Class cls60 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
            if (cls60 == null) {
                cls60 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort = cls60;
            }
            if (cls == cls60) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_ROLE_TYPE_ERROR;
            }
            Class cls61 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
            if (cls61 == null) {
                cls61 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort = cls61;
            }
            if (cls == cls61) {
                return 21004;
            }
            Class cls62 = f7469xe78a4811;
            if (cls62 == null) {
                cls62 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrShort");
                f7469xe78a4811 = cls62;
            }
            if (cls == cls62) {
                return 15004;
            }
            return Integer.MAX_VALUE;
        }
        Class cls63 = class$java$lang$Double;
        if (cls63 == null) {
            cls63 = class$("java.lang.Double");
            class$java$lang$Double = cls63;
        }
        if (cls2 == cls63) {
            Class cls64 = class$java$lang$Integer;
            if (cls64 == null) {
                cls64 = class$("java.lang.Integer");
                class$java$lang$Integer = cls64;
            }
            if (cls == cls64) {
                return 20007;
            }
            Class cls65 = f7471x8fa423c5;
            if (cls65 == null) {
                cls65 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerBigDecimal");
                f7471x8fa423c5 = cls65;
            }
            if (cls == cls65) {
                return 32007;
            }
            Class cls66 = class$java$math$BigDecimal;
            if (cls66 == null) {
                cls66 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls66;
            }
            if (cls == cls66) {
                return 32007;
            }
            Class cls67 = class$java$lang$Long;
            if (cls67 == null) {
                cls67 = class$("java.lang.Long");
                class$java$lang$Long = cls67;
            }
            if (cls == cls67) {
                return 30007;
            }
            Class cls68 = class$java$lang$Float;
            if (cls68 == null) {
                cls68 = class$("java.lang.Float");
                class$java$lang$Float = cls68;
            }
            if (cls == cls68) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_BAIDUMAP_NULL;
            }
            Class cls69 = class$java$lang$Byte;
            if (cls69 == null) {
                cls69 = class$("java.lang.Byte");
                class$java$lang$Byte = cls69;
            }
            if (cls == cls69) {
                return 20007;
            }
            Class cls70 = class$java$math$BigInteger;
            if (cls70 == null) {
                cls70 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls70;
            }
            if (cls == cls70) {
                return Integer.MAX_VALUE;
            }
            Class cls71 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
            if (cls71 == null) {
                cls71 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger = cls71;
            }
            if (cls == cls71) {
                return 21007;
            }
            Class cls72 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
            if (cls72 == null) {
                cls72 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrFloat");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat = cls72;
            }
            if (cls == cls72) {
                return 0;
            }
            Class cls73 = f7470xfd0e71d9;
            if (cls73 == null) {
                cls73 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrIntegerOrFloat");
                f7470xfd0e71d9 = cls73;
            }
            if (cls == cls73) {
                return 0;
            }
            Class cls74 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;
            if (cls74 == null) {
                cls74 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger = cls74;
            }
            if (cls == cls74) {
                return 0;
            }
            Class cls75 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
            if (cls75 == null) {
                cls75 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong = cls75;
            }
            if (cls == cls75) {
                return 0;
            }
            Class cls76 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
            if (cls76 == null) {
                cls76 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte = cls76;
            }
            if (cls == cls76) {
                return 20007;
            }
            Class cls77 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
            if (cls77 == null) {
                cls77 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte = cls77;
            }
            if (cls == cls77) {
                return 0;
            }
            Class cls78 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
            if (cls78 == null) {
                cls78 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte = cls78;
            }
            if (cls == cls78) {
                return 21007;
            }
            Class cls79 = class$java$lang$Short;
            if (cls79 == null) {
                cls79 = class$("java.lang.Short");
                class$java$lang$Short = cls79;
            }
            if (cls == cls79) {
                return 20007;
            }
            Class cls80 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
            if (cls80 == null) {
                cls80 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort = cls80;
            }
            if (cls == cls80) {
                return 21007;
            }
            Class cls81 = class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
            if (cls81 == null) {
                cls81 = class$("freemarker.ext.beans.OverloadedNumberUtil$ShortOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte = cls81;
            }
            if (cls == cls81) {
                return 20007;
            }
            Class cls82 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
            if (cls82 == null) {
                cls82 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger = cls82;
            }
            if (cls == cls82) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_BAIDUMAP_NULL;
            }
            Class cls83 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
            if (cls83 == null) {
                cls83 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte = cls83;
            }
            if (cls == cls83) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_BAIDUMAP_NULL;
            }
            Class cls84 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;
            if (cls84 == null) {
                cls84 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort = cls84;
            }
            if (cls == cls84) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_BAIDUMAP_NULL;
            }
            Class cls85 = f7468x2797b393;
            if (cls85 == null) {
                cls85 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrInteger");
                f7468x2797b393 = cls85;
            }
            if (cls == cls85) {
                return 20007;
            }
            Class cls86 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;
            if (cls86 == null) {
                cls86 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong = cls86;
            }
            if (cls == cls86) {
                return 30007;
            }
            Class cls87 = f7466xf08b2adc;
            if (cls87 == null) {
                cls87 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrDouble");
                f7466xf08b2adc = cls87;
            }
            if (cls == cls87) {
                return 20007;
            }
            Class cls88 = f7467xe6d4e5f1;
            if (cls88 == null) {
                cls88 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrFloat");
                f7467xe6d4e5f1 = cls88;
            }
            if (cls == cls88) {
                return 20007;
            }
            Class cls89 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;
            if (cls89 == null) {
                cls89 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte = cls89;
            }
            if (cls == cls89) {
                return 20007;
            }
            Class cls90 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
            if (cls90 == null) {
                cls90 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort = cls90;
            }
            if (cls == cls90) {
                return 20007;
            }
            Class cls91 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
            if (cls91 == null) {
                cls91 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort = cls91;
            }
            if (cls == cls91) {
                return 0;
            }
            Class cls92 = f7469xe78a4811;
            if (cls92 == null) {
                cls92 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrShort");
                f7469xe78a4811 = cls92;
            }
            if (cls == cls92) {
                return 20007;
            }
            return Integer.MAX_VALUE;
        }
        Class cls93 = class$java$lang$Float;
        if (cls93 == null) {
            cls93 = class$("java.lang.Float");
            class$java$lang$Float = cls93;
        }
        if (cls2 == cls93) {
            Class cls94 = class$java$lang$Integer;
            if (cls94 == null) {
                cls94 = class$("java.lang.Integer");
                class$java$lang$Integer = cls94;
            }
            if (cls == cls94) {
                return 30006;
            }
            Class cls95 = f7471x8fa423c5;
            if (cls95 == null) {
                cls95 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerBigDecimal");
                f7471x8fa423c5 = cls95;
            }
            if (cls == cls95) {
                return 33006;
            }
            Class cls96 = class$java$math$BigDecimal;
            if (cls96 == null) {
                cls96 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls96;
            }
            if (cls == cls96) {
                return 33006;
            }
            Class cls97 = class$java$lang$Long;
            if (cls97 == null) {
                cls97 = class$("java.lang.Long");
                class$java$lang$Long = cls97;
            }
            if (cls == cls97) {
                return 40006;
            }
            Class cls98 = class$java$lang$Double;
            if (cls98 == null) {
                cls98 = class$("java.lang.Double");
                class$java$lang$Double = cls98;
            }
            if (cls == cls98) {
                return Integer.MAX_VALUE;
            }
            Class cls99 = class$java$lang$Byte;
            if (cls99 == null) {
                cls99 = class$("java.lang.Byte");
                class$java$lang$Byte = cls99;
            }
            if (cls == cls99) {
                return 20006;
            }
            Class cls100 = class$java$math$BigInteger;
            if (cls100 == null) {
                cls100 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls100;
            }
            if (cls == cls100) {
                return Integer.MAX_VALUE;
            }
            Class cls101 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
            if (cls101 == null) {
                cls101 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger = cls101;
            }
            if (cls == cls101) {
                return 30006;
            }
            Class cls102 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
            if (cls102 == null) {
                cls102 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrFloat");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat = cls102;
            }
            if (cls == cls102) {
                return 30006;
            }
            Class cls103 = f7470xfd0e71d9;
            if (cls103 == null) {
                cls103 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrIntegerOrFloat");
                f7470xfd0e71d9 = cls103;
            }
            if (cls == cls103) {
                return 23006;
            }
            Class cls104 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;
            if (cls104 == null) {
                cls104 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger = cls104;
            }
            if (cls == cls104) {
                return 30006;
            }
            Class cls105 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
            if (cls105 == null) {
                cls105 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong = cls105;
            }
            if (cls == cls105) {
                return 40006;
            }
            Class cls106 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
            if (cls106 == null) {
                cls106 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte = cls106;
            }
            if (cls == cls106) {
                return 24006;
            }
            Class cls107 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
            if (cls107 == null) {
                cls107 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte = cls107;
            }
            if (cls == cls107) {
                return 23006;
            }
            Class cls108 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
            if (cls108 == null) {
                cls108 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte = cls108;
            }
            if (cls == cls108) {
                return 24006;
            }
            Class cls109 = class$java$lang$Short;
            if (cls109 == null) {
                cls109 = class$("java.lang.Short");
                class$java$lang$Short = cls109;
            }
            if (cls == cls109) {
                return 20006;
            }
            Class cls110 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
            if (cls110 == null) {
                cls110 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort = cls110;
            }
            if (cls == cls110) {
                return 24006;
            }
            Class cls111 = class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
            if (cls111 == null) {
                cls111 = class$("freemarker.ext.beans.OverloadedNumberUtil$ShortOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte = cls111;
            }
            if (cls == cls111) {
                return 20006;
            }
            Class cls112 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
            if (cls112 == null) {
                cls112 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger = cls112;
            }
            if (cls == cls112) {
                return 0;
            }
            Class cls113 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
            if (cls113 == null) {
                cls113 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte = cls113;
            }
            if (cls == cls113) {
                return 0;
            }
            Class cls114 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;
            if (cls114 == null) {
                cls114 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort = cls114;
            }
            if (cls == cls114) {
                return 0;
            }
            Class cls115 = f7468x2797b393;
            if (cls115 == null) {
                cls115 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrInteger");
                f7468x2797b393 = cls115;
            }
            if (cls == cls115) {
                return 30006;
            }
            Class cls116 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;
            if (cls116 == null) {
                cls116 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong = cls116;
            }
            if (cls == cls116) {
                return 40006;
            }
            Class cls117 = f7466xf08b2adc;
            if (cls117 == null) {
                cls117 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrDouble");
                f7466xf08b2adc = cls117;
            }
            if (cls == cls117) {
                return 40006;
            }
            Class cls118 = f7467xe6d4e5f1;
            if (cls118 == null) {
                cls118 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrFloat");
                f7467xe6d4e5f1 = cls118;
            }
            if (cls == cls118) {
                return 24006;
            }
            Class cls119 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;
            if (cls119 == null) {
                cls119 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte = cls119;
            }
            if (cls == cls119) {
                return 24006;
            }
            Class cls120 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
            if (cls120 == null) {
                cls120 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort = cls120;
            }
            if (cls == cls120) {
                return 24006;
            }
            Class cls121 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
            if (cls121 == null) {
                cls121 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort = cls121;
            }
            if (cls == cls121) {
                return 23006;
            }
            Class cls122 = f7469xe78a4811;
            if (cls122 == null) {
                cls122 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrShort");
                f7469xe78a4811 = cls122;
            }
            if (cls == cls122) {
                return 24006;
            }
            return Integer.MAX_VALUE;
        }
        Class cls123 = class$java$lang$Byte;
        if (cls123 == null) {
            cls123 = class$("java.lang.Byte");
            class$java$lang$Byte = cls123;
        }
        if (cls2 == cls123) {
            Class cls124 = class$java$lang$Integer;
            if (cls124 == null) {
                cls124 = class$("java.lang.Integer");
                class$java$lang$Integer = cls124;
            }
            if (cls == cls124) {
                return Integer.MAX_VALUE;
            }
            Class cls125 = f7471x8fa423c5;
            if (cls125 == null) {
                cls125 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerBigDecimal");
                f7471x8fa423c5 = cls125;
            }
            if (cls == cls125) {
                return 35001;
            }
            Class cls126 = class$java$math$BigDecimal;
            if (cls126 == null) {
                cls126 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls126;
            }
            if (cls == cls126) {
                return 45001;
            }
            Class cls127 = class$java$lang$Long;
            if (cls127 == null) {
                cls127 = class$("java.lang.Long");
                class$java$lang$Long = cls127;
            }
            if (cls == cls127) {
                return Integer.MAX_VALUE;
            }
            Class cls128 = class$java$lang$Double;
            if (cls128 == null) {
                cls128 = class$("java.lang.Double");
                class$java$lang$Double = cls128;
            }
            if (cls == cls128) {
                return Integer.MAX_VALUE;
            }
            Class cls129 = class$java$lang$Float;
            if (cls129 == null) {
                cls129 = class$("java.lang.Float");
                class$java$lang$Float = cls129;
            }
            if (cls == cls129) {
                return Integer.MAX_VALUE;
            }
            Class cls130 = class$java$math$BigInteger;
            if (cls130 == null) {
                cls130 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls130;
            }
            if (cls == cls130) {
                return Integer.MAX_VALUE;
            }
            Class cls131 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
            if (cls131 == null) {
                cls131 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger = cls131;
            }
            if (cls == cls131) {
                return Integer.MAX_VALUE;
            }
            Class cls132 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
            if (cls132 == null) {
                cls132 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrFloat");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat = cls132;
            }
            if (cls == cls132) {
                return Integer.MAX_VALUE;
            }
            Class cls133 = f7470xfd0e71d9;
            if (cls133 == null) {
                cls133 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrIntegerOrFloat");
                f7470xfd0e71d9 = cls133;
            }
            if (cls == cls133) {
                return Integer.MAX_VALUE;
            }
            Class cls134 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;
            if (cls134 == null) {
                cls134 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger = cls134;
            }
            if (cls == cls134) {
                return Integer.MAX_VALUE;
            }
            Class cls135 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
            if (cls135 == null) {
                cls135 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong = cls135;
            }
            if (cls == cls135) {
                return Integer.MAX_VALUE;
            }
            Class cls136 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
            if (cls136 == null) {
                cls136 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte = cls136;
            }
            if (cls == cls136) {
                return 22001;
            }
            Class cls137 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
            if (cls137 == null) {
                cls137 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte = cls137;
            }
            if (cls == cls137) {
                return 25001;
            }
            Class cls138 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
            if (cls138 == null) {
                cls138 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte = cls138;
            }
            if (cls == cls138) {
                return 23001;
            }
            Class cls139 = class$java$lang$Short;
            if (cls139 == null) {
                cls139 = class$("java.lang.Short");
                class$java$lang$Short = cls139;
            }
            if (cls == cls139) {
                return Integer.MAX_VALUE;
            }
            Class cls140 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
            if (cls140 == null) {
                cls140 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort = cls140;
            }
            if (cls == cls140) {
                return Integer.MAX_VALUE;
            }
            Class cls141 = class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
            if (cls141 == null) {
                cls141 = class$("freemarker.ext.beans.OverloadedNumberUtil$ShortOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte = cls141;
            }
            if (cls == cls141) {
                return 21001;
            }
            Class cls142 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
            if (cls142 == null) {
                cls142 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger = cls142;
            }
            if (cls == cls142) {
                return Integer.MAX_VALUE;
            }
            Class cls143 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
            if (cls143 == null) {
                cls143 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte = cls143;
            }
            if (cls == cls143) {
                return 23001;
            }
            Class cls144 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;
            if (cls144 == null) {
                cls144 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort = cls144;
            }
            if (cls == cls144) {
                return Integer.MAX_VALUE;
            }
            Class cls145 = f7468x2797b393;
            if (cls145 == null) {
                cls145 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrInteger");
                f7468x2797b393 = cls145;
            }
            if (cls == cls145) {
                return Integer.MAX_VALUE;
            }
            Class cls146 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;
            if (cls146 == null) {
                cls146 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong = cls146;
            }
            if (cls == cls146) {
                return Integer.MAX_VALUE;
            }
            Class cls147 = f7466xf08b2adc;
            if (cls147 == null) {
                cls147 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrDouble");
                f7466xf08b2adc = cls147;
            }
            if (cls == cls147) {
                return Integer.MAX_VALUE;
            }
            Class cls148 = f7467xe6d4e5f1;
            if (cls148 == null) {
                cls148 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrFloat");
                f7467xe6d4e5f1 = cls148;
            }
            if (cls == cls148) {
                return Integer.MAX_VALUE;
            }
            Class cls149 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;
            if (cls149 == null) {
                cls149 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte = cls149;
            }
            if (cls == cls149) {
                return 18001;
            }
            Class cls150 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
            if (cls150 == null) {
                cls150 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort = cls150;
            }
            if (cls == cls150) {
                return Integer.MAX_VALUE;
            }
            Class cls151 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
            if (cls151 == null) {
                cls151 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort = cls151;
            }
            if (cls == cls151) {
                return Integer.MAX_VALUE;
            }
            Class cls152 = f7469xe78a4811;
            if (cls152 == null) {
                cls152 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrShort");
                f7469xe78a4811 = cls152;
            }
            if (cls == cls152) {
            }
            return Integer.MAX_VALUE;
        }
        Class cls153 = class$java$lang$Short;
        if (cls153 == null) {
            cls153 = class$("java.lang.Short");
            class$java$lang$Short = cls153;
        }
        if (cls2 == cls153) {
            Class cls154 = class$java$lang$Integer;
            if (cls154 == null) {
                cls154 = class$("java.lang.Integer");
                class$java$lang$Integer = cls154;
            }
            if (cls == cls154) {
                return Integer.MAX_VALUE;
            }
            Class cls155 = f7471x8fa423c5;
            if (cls155 == 0) {
                cls155 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerBigDecimal");
                f7471x8fa423c5 = cls155;
            }
            if (cls == cls155) {
                return 34002;
            }
            Class cls156 = class$java$math$BigDecimal;
            if (cls156 == null) {
                cls156 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls156;
            }
            if (cls == cls156) {
                return 44002;
            }
            Class cls157 = class$java$lang$Long;
            if (cls157 == null) {
                cls157 = class$("java.lang.Long");
                class$java$lang$Long = cls157;
            }
            if (cls == cls157) {
                return Integer.MAX_VALUE;
            }
            Class cls158 = class$java$lang$Double;
            if (cls158 == null) {
                cls158 = class$("java.lang.Double");
                class$java$lang$Double = cls158;
            }
            if (cls == cls158) {
                return Integer.MAX_VALUE;
            }
            Class cls159 = class$java$lang$Float;
            if (cls159 == null) {
                cls159 = class$("java.lang.Float");
                class$java$lang$Float = cls159;
            }
            if (cls == cls159) {
                return Integer.MAX_VALUE;
            }
            Class cls160 = class$java$lang$Byte;
            if (cls160 == null) {
                cls160 = class$("java.lang.Byte");
                class$java$lang$Byte = cls160;
            }
            if (cls == cls160) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_URL_NULL;
            }
            Class cls161 = class$java$math$BigInteger;
            if (cls161 == null) {
                cls161 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls161;
            }
            if (cls == cls161) {
                return Integer.MAX_VALUE;
            }
            Class cls162 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
            if (cls162 == null) {
                cls162 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger = cls162;
            }
            if (cls == cls162) {
                return Integer.MAX_VALUE;
            }
            Class cls163 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
            if (cls163 == null) {
                cls163 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrFloat");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat = cls163;
            }
            if (cls == cls163) {
                return Integer.MAX_VALUE;
            }
            Class cls164 = f7470xfd0e71d9;
            if (cls164 == null) {
                cls164 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrIntegerOrFloat");
                f7470xfd0e71d9 = cls164;
            }
            if (cls == cls164) {
                return Integer.MAX_VALUE;
            }
            Class cls165 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;
            if (cls165 == null) {
                cls165 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger = cls165;
            }
            if (cls == cls165) {
                return Integer.MAX_VALUE;
            }
            Class cls166 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
            if (cls166 == null) {
                cls166 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong = cls166;
            }
            if (cls == cls166) {
                return Integer.MAX_VALUE;
            }
            Class cls167 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
            if (cls167 == null) {
                cls167 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte = cls167;
            }
            if (cls == cls167) {
                return 21002;
            }
            Class cls168 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
            if (cls168 == null) {
                cls168 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte = cls168;
            }
            if (cls == cls168) {
                return 24002;
            }
            Class cls169 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
            if (cls169 == null) {
                cls169 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte = cls169;
            }
            if (cls == cls169) {
                return 22002;
            }
            Class cls170 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
            if (cls170 == null) {
                cls170 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort = cls170;
            }
            if (cls == cls170) {
                return 22002;
            }
            Class cls171 = class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
            if (cls171 == null) {
                cls171 = class$("freemarker.ext.beans.OverloadedNumberUtil$ShortOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte = cls171;
            }
            if (cls == cls171) {
                return 0;
            }
            Class cls172 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
            if (cls172 == null) {
                cls172 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger = cls172;
            }
            if (cls == cls172) {
                return Integer.MAX_VALUE;
            }
            Class cls173 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
            if (cls173 == null) {
                cls173 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte = cls173;
            }
            if (cls == cls173) {
                return 22002;
            }
            Class cls174 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;
            if (cls174 == null) {
                cls174 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort = cls174;
            }
            if (cls == cls174) {
                return 22002;
            }
            Class cls175 = f7468x2797b393;
            if (cls175 == null) {
                cls175 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrInteger");
                f7468x2797b393 = cls175;
            }
            if (cls == cls175) {
                return Integer.MAX_VALUE;
            }
            Class cls176 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;
            if (cls176 == null) {
                cls176 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong = cls176;
            }
            if (cls == cls176) {
                return Integer.MAX_VALUE;
            }
            Class cls177 = f7466xf08b2adc;
            if (cls177 == null) {
                cls177 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrDouble");
                f7466xf08b2adc = cls177;
            }
            if (cls == cls177) {
                return Integer.MAX_VALUE;
            }
            Class cls178 = f7467xe6d4e5f1;
            if (cls178 == null) {
                cls178 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrFloat");
                f7467xe6d4e5f1 = cls178;
            }
            if (cls == cls178) {
                return Integer.MAX_VALUE;
            }
            Class cls179 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;
            if (cls179 == null) {
                cls179 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte = cls179;
            }
            if (cls == cls179) {
                return 17002;
            }
            Class cls180 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
            if (cls180 == null) {
                cls180 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort = cls180;
            }
            if (cls == cls180) {
                return 21002;
            }
            Class cls181 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
            if (cls181 == null) {
                cls181 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort = cls181;
            }
            if (cls == cls181) {
                return 24002;
            }
            Class cls182 = f7469xe78a4811;
            if (cls182 == null) {
                cls182 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrShort");
                f7469xe78a4811 = cls182;
            }
            if (cls == cls182) {
                return 17002;
            }
            return Integer.MAX_VALUE;
        }
        Class cls183 = class$java$math$BigDecimal;
        if (cls183 == null) {
            cls183 = class$("java.math.BigDecimal");
            class$java$math$BigDecimal = cls183;
        }
        if (cls2 == cls183) {
            Class cls184 = class$java$lang$Integer;
            if (cls184 == null) {
                cls184 = class$("java.lang.Integer");
                class$java$lang$Integer = cls184;
            }
            if (cls == cls184) {
                return 20008;
            }
            Class cls185 = f7471x8fa423c5;
            if (cls185 == null) {
                cls185 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerBigDecimal");
                f7471x8fa423c5 = cls185;
            }
            if (cls == cls185) {
                return 0;
            }
            Class cls186 = class$java$lang$Long;
            if (cls186 == null) {
                cls186 = class$("java.lang.Long");
                class$java$lang$Long = cls186;
            }
            if (cls == cls186) {
                return 20008;
            }
            Class cls187 = class$java$lang$Double;
            if (cls187 == null) {
                cls187 = class$("java.lang.Double");
                class$java$lang$Double = cls187;
            }
            if (cls == cls187) {
                return 20008;
            }
            Class cls188 = class$java$lang$Float;
            if (cls188 == null) {
                cls188 = class$("java.lang.Float");
                class$java$lang$Float = cls188;
            }
            if (cls == cls188) {
                return 20008;
            }
            Class cls189 = class$java$lang$Byte;
            if (cls189 == null) {
                cls189 = class$("java.lang.Byte");
                class$java$lang$Byte = cls189;
            }
            if (cls == cls189) {
                return 20008;
            }
            Class cls190 = class$java$math$BigInteger;
            if (cls190 == null) {
                cls190 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls190;
            }
            if (cls == cls190) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_NULL;
            }
            Class cls191 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
            if (cls191 == null) {
                cls191 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger = cls191;
            }
            if (cls == cls191) {
                return 20008;
            }
            Class cls192 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
            if (cls192 == null) {
                cls192 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrFloat");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat = cls192;
            }
            if (cls == cls192) {
                return 20008;
            }
            Class cls193 = f7470xfd0e71d9;
            if (cls193 == null) {
                cls193 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrIntegerOrFloat");
                f7470xfd0e71d9 = cls193;
            }
            if (cls == cls193) {
                return 20008;
            }
            Class cls194 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;
            if (cls194 == null) {
                cls194 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger = cls194;
            }
            if (cls == cls194) {
                return 20008;
            }
            Class cls195 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
            if (cls195 == null) {
                cls195 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong = cls195;
            }
            if (cls == cls195) {
                return 20008;
            }
            Class cls196 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
            if (cls196 == null) {
                cls196 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte = cls196;
            }
            if (cls == cls196) {
                return 20008;
            }
            Class cls197 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
            if (cls197 == null) {
                cls197 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte = cls197;
            }
            if (cls == cls197) {
                return 20008;
            }
            Class cls198 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
            if (cls198 == null) {
                cls198 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte = cls198;
            }
            if (cls == cls198) {
                return 20008;
            }
            Class cls199 = class$java$lang$Short;
            if (cls199 == null) {
                cls199 = class$("java.lang.Short");
                class$java$lang$Short = cls199;
            }
            if (cls == cls199) {
                return 20008;
            }
            Class cls200 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
            if (cls200 == null) {
                cls200 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort = cls200;
            }
            if (cls == cls200) {
                return 20008;
            }
            Class cls201 = class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
            if (cls201 == null) {
                cls201 = class$("freemarker.ext.beans.OverloadedNumberUtil$ShortOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte = cls201;
            }
            if (cls == cls201) {
                return 20008;
            }
            Class cls202 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
            if (cls202 == null) {
                cls202 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger = cls202;
            }
            if (cls == cls202) {
                return 20008;
            }
            Class cls203 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
            if (cls203 == null) {
                cls203 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte = cls203;
            }
            if (cls == cls203) {
                return 20008;
            }
            Class cls204 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;
            if (cls204 == null) {
                cls204 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort = cls204;
            }
            if (cls == cls204) {
                return 20008;
            }
            Class cls205 = f7468x2797b393;
            if (cls205 == null) {
                cls205 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrInteger");
                f7468x2797b393 = cls205;
            }
            if (cls == cls205) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_NULL;
            }
            Class cls206 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;
            if (cls206 == null) {
                cls206 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong = cls206;
            }
            if (cls == cls206) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_NULL;
            }
            Class cls207 = f7466xf08b2adc;
            if (cls207 == null) {
                cls207 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrDouble");
                f7466xf08b2adc = cls207;
            }
            if (cls == cls207) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_NULL;
            }
            Class cls208 = f7467xe6d4e5f1;
            if (cls208 == null) {
                cls208 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrFloat");
                f7467xe6d4e5f1 = cls208;
            }
            if (cls == cls208) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_NULL;
            }
            Class cls209 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;
            if (cls209 == null) {
                cls209 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte = cls209;
            }
            if (cls == cls209) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_NULL;
            }
            Class cls210 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
            if (cls210 == null) {
                cls210 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort = cls210;
            }
            if (cls == cls210) {
                return 20008;
            }
            Class cls211 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
            if (cls211 == null) {
                cls211 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort = cls211;
            }
            if (cls == cls211) {
                return 20008;
            }
            Class cls212 = f7469xe78a4811;
            if (cls212 == null) {
                cls212 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrShort");
                f7469xe78a4811 = cls212;
            }
            if (cls == cls212) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_NULL;
            }
            return Integer.MAX_VALUE;
        }
        Class cls213 = class$java$math$BigInteger;
        if (cls213 == null) {
            cls213 = class$("java.math.BigInteger");
            class$java$math$BigInteger = cls213;
        }
        if (cls2 == cls213) {
            Class cls214 = class$java$lang$Integer;
            if (cls214 == null) {
                cls214 = class$("java.lang.Integer");
                class$java$lang$Integer = cls214;
            }
            if (cls == cls214) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls215 = f7471x8fa423c5;
            if (cls215 == null) {
                cls215 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerBigDecimal");
                f7471x8fa423c5 = cls215;
            }
            if (cls == cls215) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls216 = class$java$math$BigDecimal;
            if (cls216 == null) {
                cls216 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls216;
            }
            if (cls == cls216) {
                return 40005;
            }
            Class cls217 = class$java$lang$Long;
            if (cls217 == null) {
                cls217 = class$("java.lang.Long");
                class$java$lang$Long = cls217;
            }
            if (cls == cls217) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls218 = class$java$lang$Double;
            if (cls218 == null) {
                cls218 = class$("java.lang.Double");
                class$java$lang$Double = cls218;
            }
            if (cls == cls218) {
                return Integer.MAX_VALUE;
            }
            Class cls219 = class$java$lang$Float;
            if (cls219 == null) {
                cls219 = class$("java.lang.Float");
                class$java$lang$Float = cls219;
            }
            if (cls == cls219) {
                return Integer.MAX_VALUE;
            }
            Class cls220 = class$java$lang$Byte;
            if (cls220 == null) {
                cls220 = class$("java.lang.Byte");
                class$java$lang$Byte = cls220;
            }
            if (cls == cls220) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls221 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger;
            if (cls221 == null) {
                cls221 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrInteger = cls221;
            }
            if (cls == cls221) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls222 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat;
            if (cls222 == null) {
                cls222 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrFloat");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrFloat = cls222;
            }
            if (cls == cls222) {
                return Integer.MAX_VALUE;
            }
            Class cls223 = f7470xfd0e71d9;
            if (cls223 == null) {
                cls223 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrIntegerOrFloat");
                f7470xfd0e71d9 = cls223;
            }
            if (cls == cls223) {
                return 21005;
            }
            Class cls224 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger;
            if (cls224 == null) {
                cls224 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrInteger = cls224;
            }
            if (cls == cls224) {
                return 21005;
            }
            Class cls225 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong;
            if (cls225 == null) {
                cls225 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrLong = cls225;
            }
            if (cls == cls225) {
                return 21005;
            }
            Class cls226 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte;
            if (cls226 == null) {
                cls226 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrByte = cls226;
            }
            if (cls == cls226) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls227 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte;
            if (cls227 == null) {
                cls227 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrByte = cls227;
            }
            if (cls == cls227) {
                return 21005;
            }
            Class cls228 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte;
            if (cls228 == null) {
                cls228 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrByte = cls228;
            }
            if (cls == cls228) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls229 = class$java$lang$Short;
            if (cls229 == null) {
                cls229 = class$("java.lang.Short");
                class$java$lang$Short = cls229;
            }
            if (cls == cls229) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls230 = class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort;
            if (cls230 == null) {
                cls230 = class$("freemarker.ext.beans.OverloadedNumberUtil$LongOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$LongOrShort = cls230;
            }
            if (cls == cls230) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls231 = class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte;
            if (cls231 == null) {
                cls231 = class$("freemarker.ext.beans.OverloadedNumberUtil$ShortOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$ShortOrByte = cls231;
            }
            if (cls == cls231) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls232 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger;
            if (cls232 == null) {
                cls232 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrInteger");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrInteger = cls232;
            }
            if (cls == cls232) {
                return 25005;
            }
            Class cls233 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte;
            if (cls233 == null) {
                cls233 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrByte = cls233;
            }
            if (cls == cls233) {
                return 25005;
            }
            Class cls234 = class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort;
            if (cls234 == null) {
                cls234 = class$("freemarker.ext.beans.OverloadedNumberUtil$FloatOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$FloatOrShort = cls234;
            }
            if (cls == cls234) {
                return 25005;
            }
            Class cls235 = f7468x2797b393;
            if (cls235 == null) {
                cls235 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrInteger");
                f7468x2797b393 = cls235;
            }
            if (cls == cls235) {
                return 0;
            }
            Class cls236 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong;
            if (cls236 == null) {
                cls236 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrLong");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrLong = cls236;
            }
            if (cls == cls236) {
                return 0;
            }
            Class cls237 = f7466xf08b2adc;
            if (cls237 == null) {
                cls237 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrDouble");
                f7466xf08b2adc = cls237;
            }
            if (cls == cls237) {
                return 0;
            }
            Class cls238 = f7467xe6d4e5f1;
            if (cls238 == null) {
                cls238 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrFloat");
                f7467xe6d4e5f1 = cls238;
            }
            if (cls == cls238) {
                return 0;
            }
            Class cls239 = class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte;
            if (cls239 == null) {
                cls239 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrByte");
                class$freemarker$ext$beans$OverloadedNumberUtil$BigIntegerOrByte = cls239;
            }
            if (cls == cls239) {
                return 0;
            }
            Class cls240 = class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort;
            if (cls240 == null) {
                cls240 = class$("freemarker.ext.beans.OverloadedNumberUtil$IntegerOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$IntegerOrShort = cls240;
            }
            if (cls == cls240) {
                return HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_USER_ID_NULL;
            }
            Class cls241 = class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort;
            if (cls241 == null) {
                cls241 = class$("freemarker.ext.beans.OverloadedNumberUtil$DoubleOrShort");
                class$freemarker$ext$beans$OverloadedNumberUtil$DoubleOrShort = cls241;
            }
            if (cls == cls241) {
                return 21005;
            }
            Class cls242 = f7469xe78a4811;
            if (cls242 == null) {
                cls242 = class$("freemarker.ext.beans.OverloadedNumberUtil$BigIntegerOrShort");
                f7469xe78a4811 = cls242;
            }
            if (cls == cls242) {
                return 0;
            }
        }
        return Integer.MAX_VALUE;
    }

    static int compareNumberTypeSpecificity(Class cls, Class cls2) {
        Class primitiveClassToBoxingClass = ClassUtil.primitiveClassToBoxingClass(cls);
        Class primitiveClassToBoxingClass2 = ClassUtil.primitiveClassToBoxingClass(cls2);
        if (primitiveClassToBoxingClass == primitiveClassToBoxingClass2) {
            return 0;
        }
        Class cls3 = class$java$lang$Integer;
        if (cls3 == null) {
            cls3 = class$("java.lang.Integer");
            class$java$lang$Integer = cls3;
        }
        if (primitiveClassToBoxingClass == cls3) {
            Class cls4 = class$java$lang$Long;
            if (cls4 == null) {
                cls4 = class$("java.lang.Long");
                class$java$lang$Long = cls4;
            }
            if (primitiveClassToBoxingClass2 == cls4) {
                return 1;
            }
            Class cls5 = class$java$lang$Double;
            if (cls5 == null) {
                cls5 = class$("java.lang.Double");
                class$java$lang$Double = cls5;
            }
            if (primitiveClassToBoxingClass2 == cls5) {
                return 4;
            }
            Class cls6 = class$java$lang$Float;
            if (cls6 == null) {
                cls6 = class$("java.lang.Float");
                class$java$lang$Float = cls6;
            }
            if (primitiveClassToBoxingClass2 == cls6) {
                return 3;
            }
            Class cls7 = class$java$lang$Byte;
            if (cls7 == null) {
                cls7 = class$("java.lang.Byte");
                class$java$lang$Byte = cls7;
            }
            if (primitiveClassToBoxingClass2 == cls7) {
                return -2;
            }
            Class cls8 = class$java$lang$Short;
            if (cls8 == null) {
                cls8 = class$("java.lang.Short");
                class$java$lang$Short = cls8;
            }
            if (primitiveClassToBoxingClass2 == cls8) {
                return -1;
            }
            Class cls9 = class$java$math$BigDecimal;
            if (cls9 == null) {
                cls9 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls9;
            }
            if (primitiveClassToBoxingClass2 == cls9) {
                return 5;
            }
            Class cls10 = class$java$math$BigInteger;
            if (cls10 == null) {
                cls10 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls10;
            }
            if (primitiveClassToBoxingClass2 == cls10) {
                return 2;
            }
            return 0;
        }
        Class cls11 = class$java$lang$Long;
        if (cls11 == null) {
            cls11 = class$("java.lang.Long");
            class$java$lang$Long = cls11;
        }
        if (primitiveClassToBoxingClass == cls11) {
            Class cls12 = class$java$lang$Integer;
            if (cls12 == null) {
                cls12 = class$("java.lang.Integer");
                class$java$lang$Integer = cls12;
            }
            if (primitiveClassToBoxingClass2 == cls12) {
                return -1;
            }
            Class cls13 = class$java$lang$Double;
            if (cls13 == null) {
                cls13 = class$("java.lang.Double");
                class$java$lang$Double = cls13;
            }
            if (primitiveClassToBoxingClass2 == cls13) {
                return 3;
            }
            Class cls14 = class$java$lang$Float;
            if (cls14 == null) {
                cls14 = class$("java.lang.Float");
                class$java$lang$Float = cls14;
            }
            if (primitiveClassToBoxingClass2 == cls14) {
                return 2;
            }
            Class cls15 = class$java$lang$Byte;
            if (cls15 == null) {
                cls15 = class$("java.lang.Byte");
                class$java$lang$Byte = cls15;
            }
            if (primitiveClassToBoxingClass2 == cls15) {
                return -3;
            }
            Class cls16 = class$java$lang$Short;
            if (cls16 == null) {
                cls16 = class$("java.lang.Short");
                class$java$lang$Short = cls16;
            }
            if (primitiveClassToBoxingClass2 == cls16) {
                return -2;
            }
            Class cls17 = class$java$math$BigDecimal;
            if (cls17 == null) {
                cls17 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls17;
            }
            if (primitiveClassToBoxingClass2 == cls17) {
                return 4;
            }
            Class cls18 = class$java$math$BigInteger;
            if (cls18 == null) {
                cls18 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls18;
            }
            if (primitiveClassToBoxingClass2 == cls18) {
                return 1;
            }
            return 0;
        }
        Class cls19 = class$java$lang$Double;
        if (cls19 == null) {
            cls19 = class$("java.lang.Double");
            class$java$lang$Double = cls19;
        }
        if (primitiveClassToBoxingClass == cls19) {
            Class cls20 = class$java$lang$Integer;
            if (cls20 == null) {
                cls20 = class$("java.lang.Integer");
                class$java$lang$Integer = cls20;
            }
            if (primitiveClassToBoxingClass2 == cls20) {
                return -4;
            }
            Class cls21 = class$java$lang$Long;
            if (cls21 == null) {
                cls21 = class$("java.lang.Long");
                class$java$lang$Long = cls21;
            }
            if (primitiveClassToBoxingClass2 == cls21) {
                return -3;
            }
            Class cls22 = class$java$lang$Float;
            if (cls22 == null) {
                cls22 = class$("java.lang.Float");
                class$java$lang$Float = cls22;
            }
            if (primitiveClassToBoxingClass2 == cls22) {
                return -1;
            }
            Class cls23 = class$java$lang$Byte;
            if (cls23 == null) {
                cls23 = class$("java.lang.Byte");
                class$java$lang$Byte = cls23;
            }
            if (primitiveClassToBoxingClass2 == cls23) {
                return -6;
            }
            Class cls24 = class$java$lang$Short;
            if (cls24 == null) {
                cls24 = class$("java.lang.Short");
                class$java$lang$Short = cls24;
            }
            if (primitiveClassToBoxingClass2 == cls24) {
                return -5;
            }
            Class cls25 = class$java$math$BigDecimal;
            if (cls25 == null) {
                cls25 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls25;
            }
            if (primitiveClassToBoxingClass2 == cls25) {
                return 1;
            }
            Class cls26 = class$java$math$BigInteger;
            if (cls26 == null) {
                cls26 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls26;
            }
            if (primitiveClassToBoxingClass2 == cls26) {
                return -2;
            }
            return 0;
        }
        Class cls27 = class$java$lang$Float;
        if (cls27 == null) {
            cls27 = class$("java.lang.Float");
            class$java$lang$Float = cls27;
        }
        if (primitiveClassToBoxingClass == cls27) {
            Class cls28 = class$java$lang$Integer;
            if (cls28 == null) {
                cls28 = class$("java.lang.Integer");
                class$java$lang$Integer = cls28;
            }
            if (primitiveClassToBoxingClass2 == cls28) {
                return -3;
            }
            Class cls29 = class$java$lang$Long;
            if (cls29 == null) {
                cls29 = class$("java.lang.Long");
                class$java$lang$Long = cls29;
            }
            if (primitiveClassToBoxingClass2 == cls29) {
                return -2;
            }
            Class cls30 = class$java$lang$Double;
            if (cls30 == null) {
                cls30 = class$("java.lang.Double");
                class$java$lang$Double = cls30;
            }
            if (primitiveClassToBoxingClass2 == cls30) {
                return 1;
            }
            Class cls31 = class$java$lang$Byte;
            if (cls31 == null) {
                cls31 = class$("java.lang.Byte");
                class$java$lang$Byte = cls31;
            }
            if (primitiveClassToBoxingClass2 == cls31) {
                return -5;
            }
            Class cls32 = class$java$lang$Short;
            if (cls32 == null) {
                cls32 = class$("java.lang.Short");
                class$java$lang$Short = cls32;
            }
            if (primitiveClassToBoxingClass2 == cls32) {
                return -4;
            }
            Class cls33 = class$java$math$BigDecimal;
            if (cls33 == null) {
                cls33 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls33;
            }
            if (primitiveClassToBoxingClass2 == cls33) {
                return 2;
            }
            Class cls34 = class$java$math$BigInteger;
            if (cls34 == null) {
                cls34 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls34;
            }
            if (primitiveClassToBoxingClass2 == cls34) {
                return -1;
            }
            return 0;
        }
        Class cls35 = class$java$lang$Byte;
        if (cls35 == null) {
            cls35 = class$("java.lang.Byte");
            class$java$lang$Byte = cls35;
        }
        if (primitiveClassToBoxingClass == cls35) {
            Class cls36 = class$java$lang$Integer;
            if (cls36 == null) {
                cls36 = class$("java.lang.Integer");
                class$java$lang$Integer = cls36;
            }
            if (primitiveClassToBoxingClass2 == cls36) {
                return 2;
            }
            Class cls37 = class$java$lang$Long;
            if (cls37 == null) {
                cls37 = class$("java.lang.Long");
                class$java$lang$Long = cls37;
            }
            if (primitiveClassToBoxingClass2 == cls37) {
                return 3;
            }
            Class cls38 = class$java$lang$Double;
            if (cls38 == null) {
                cls38 = class$("java.lang.Double");
                class$java$lang$Double = cls38;
            }
            if (primitiveClassToBoxingClass2 == cls38) {
                return 6;
            }
            Class cls39 = class$java$lang$Float;
            if (cls39 == null) {
                cls39 = class$("java.lang.Float");
                class$java$lang$Float = cls39;
            }
            if (primitiveClassToBoxingClass2 == cls39) {
                return 5;
            }
            Class cls40 = class$java$lang$Short;
            if (cls40 == null) {
                cls40 = class$("java.lang.Short");
                class$java$lang$Short = cls40;
            }
            if (primitiveClassToBoxingClass2 == cls40) {
                return 1;
            }
            Class cls41 = class$java$math$BigDecimal;
            if (cls41 == null) {
                cls41 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls41;
            }
            if (primitiveClassToBoxingClass2 == cls41) {
                return 7;
            }
            Class cls42 = class$java$math$BigInteger;
            if (cls42 == null) {
                cls42 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls42;
            }
            if (primitiveClassToBoxingClass2 == cls42) {
                return 4;
            }
            return 0;
        }
        Class cls43 = class$java$lang$Short;
        if (cls43 == null) {
            cls43 = class$("java.lang.Short");
            class$java$lang$Short = cls43;
        }
        if (primitiveClassToBoxingClass == cls43) {
            Class cls44 = class$java$lang$Integer;
            if (cls44 == null) {
                cls44 = class$("java.lang.Integer");
                class$java$lang$Integer = cls44;
            }
            if (primitiveClassToBoxingClass2 == cls44) {
                return 1;
            }
            Class cls45 = class$java$lang$Long;
            if (cls45 == null) {
                cls45 = class$("java.lang.Long");
                class$java$lang$Long = cls45;
            }
            if (primitiveClassToBoxingClass2 == cls45) {
                return 2;
            }
            Class cls46 = class$java$lang$Double;
            if (cls46 == null) {
                cls46 = class$("java.lang.Double");
                class$java$lang$Double = cls46;
            }
            if (primitiveClassToBoxingClass2 == cls46) {
                return 5;
            }
            Class cls47 = class$java$lang$Float;
            if (cls47 == null) {
                cls47 = class$("java.lang.Float");
                class$java$lang$Float = cls47;
            }
            if (primitiveClassToBoxingClass2 == cls47) {
                return 4;
            }
            Class cls48 = class$java$lang$Byte;
            if (cls48 == null) {
                cls48 = class$("java.lang.Byte");
                class$java$lang$Byte = cls48;
            }
            if (primitiveClassToBoxingClass2 == cls48) {
                return -1;
            }
            Class cls49 = class$java$math$BigDecimal;
            if (cls49 == null) {
                cls49 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls49;
            }
            if (primitiveClassToBoxingClass2 == cls49) {
                return 6;
            }
            Class cls50 = class$java$math$BigInteger;
            if (cls50 == null) {
                cls50 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls50;
            }
            if (primitiveClassToBoxingClass2 == cls50) {
                return 3;
            }
            return 0;
        }
        Class cls51 = class$java$math$BigDecimal;
        if (cls51 == null) {
            cls51 = class$("java.math.BigDecimal");
            class$java$math$BigDecimal = cls51;
        }
        if (primitiveClassToBoxingClass == cls51) {
            Class cls52 = class$java$lang$Integer;
            if (cls52 == null) {
                cls52 = class$("java.lang.Integer");
                class$java$lang$Integer = cls52;
            }
            if (primitiveClassToBoxingClass2 == cls52) {
                return -5;
            }
            Class cls53 = class$java$lang$Long;
            if (cls53 == null) {
                cls53 = class$("java.lang.Long");
                class$java$lang$Long = cls53;
            }
            if (primitiveClassToBoxingClass2 == cls53) {
                return -4;
            }
            Class cls54 = class$java$lang$Double;
            if (cls54 == null) {
                cls54 = class$("java.lang.Double");
                class$java$lang$Double = cls54;
            }
            if (primitiveClassToBoxingClass2 == cls54) {
                return -1;
            }
            Class cls55 = class$java$lang$Float;
            if (cls55 == null) {
                cls55 = class$("java.lang.Float");
                class$java$lang$Float = cls55;
            }
            if (primitiveClassToBoxingClass2 == cls55) {
                return -2;
            }
            Class cls56 = class$java$lang$Byte;
            if (cls56 == null) {
                cls56 = class$("java.lang.Byte");
                class$java$lang$Byte = cls56;
            }
            if (primitiveClassToBoxingClass2 == cls56) {
                return -7;
            }
            Class cls57 = class$java$lang$Short;
            if (cls57 == null) {
                cls57 = class$("java.lang.Short");
                class$java$lang$Short = cls57;
            }
            if (primitiveClassToBoxingClass2 == cls57) {
                return -6;
            }
            Class cls58 = class$java$math$BigInteger;
            if (cls58 == null) {
                cls58 = class$("java.math.BigInteger");
                class$java$math$BigInteger = cls58;
            }
            if (primitiveClassToBoxingClass2 == cls58) {
                return -3;
            }
            return 0;
        }
        Class cls59 = class$java$math$BigInteger;
        if (cls59 == null) {
            cls59 = class$("java.math.BigInteger");
            class$java$math$BigInteger = cls59;
        }
        if (primitiveClassToBoxingClass == cls59) {
            Class cls60 = class$java$lang$Integer;
            if (cls60 == null) {
                cls60 = class$("java.lang.Integer");
                class$java$lang$Integer = cls60;
            }
            if (primitiveClassToBoxingClass2 == cls60) {
                return -2;
            }
            Class cls61 = class$java$lang$Long;
            if (cls61 == null) {
                cls61 = class$("java.lang.Long");
                class$java$lang$Long = cls61;
            }
            if (primitiveClassToBoxingClass2 == cls61) {
                return -1;
            }
            Class cls62 = class$java$lang$Double;
            if (cls62 == null) {
                cls62 = class$("java.lang.Double");
                class$java$lang$Double = cls62;
            }
            if (primitiveClassToBoxingClass2 == cls62) {
                return 2;
            }
            Class cls63 = class$java$lang$Float;
            if (cls63 == null) {
                cls63 = class$("java.lang.Float");
                class$java$lang$Float = cls63;
            }
            if (primitiveClassToBoxingClass2 == cls63) {
                return 1;
            }
            Class cls64 = class$java$lang$Byte;
            if (cls64 == null) {
                cls64 = class$("java.lang.Byte");
                class$java$lang$Byte = cls64;
            }
            if (primitiveClassToBoxingClass2 == cls64) {
                return -4;
            }
            Class cls65 = class$java$lang$Short;
            if (cls65 == null) {
                cls65 = class$("java.lang.Short");
                class$java$lang$Short = cls65;
            }
            if (primitiveClassToBoxingClass2 == cls65) {
                return -3;
            }
            Class cls66 = class$java$math$BigDecimal;
            if (cls66 == null) {
                cls66 = class$("java.math.BigDecimal");
                class$java$math$BigDecimal = cls66;
            }
            if (primitiveClassToBoxingClass2 == cls66) {
                return 3;
            }
        }
        return 0;
    }
}
