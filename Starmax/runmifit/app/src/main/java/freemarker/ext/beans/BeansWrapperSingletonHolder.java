package freemarker.ext.beans;

class BeansWrapperSingletonHolder {
    static final BeansWrapper INSTANCE = new BeansWrapper();

    BeansWrapperSingletonHolder() {
    }
}
