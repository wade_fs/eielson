package freemarker.ext.jsp;

import freemarker.log.Logger;
import freemarker.template.utility.ClassUtil;
import java.security.AccessController;
import java.security.PrivilegedAction;
import javax.el.ELContext;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.el.ELException;
import javax.servlet.jsp.el.ExpressionEvaluator;
import javax.servlet.jsp.el.VariableResolver;

public class _FreeMarkerPageContext21 extends FreeMarkerPageContext {
    private static final Logger LOG = Logger.getLogger("freemarker.jsp");
    static /* synthetic */ Class class$javax$servlet$jsp$JspContext;
    private ELContext elContext;

    static {
        if (JspFactory.getDefaultFactory() == null) {
            JspFactory.setDefaultFactory(new FreeMarkerJspFactory21());
        }
        Logger logger = LOG;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Using JspFactory implementation class ");
        stringBuffer.append(JspFactory.getDefaultFactory().getClass().getName());
        logger.debug(stringBuffer.toString());
    }

    public ExpressionEvaluator getExpressionEvaluator() {
        try {
            return ((ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
                /* class freemarker.ext.jsp._FreeMarkerPageContext21.C33531 */

                public Object run() {
                    return Thread.currentThread().getContextClassLoader();
                }
            })).loadClass("org.apache.commons.el.ExpressionEvaluatorImpl").newInstance();
        } catch (Exception unused) {
            throw new UnsupportedOperationException("In order for the getExpressionEvaluator() method to work, you must have downloaded the apache commons-el jar and made it available in the classpath.");
        }
    }

    public VariableResolver getVariableResolver() {
        return new VariableResolver() {
            /* class freemarker.ext.jsp._FreeMarkerPageContext21.C33542 */

            public Object resolveVariable(String str) throws ELException {
                return this.findAttribute(str);
            }
        };
    }

    public ELContext getELContext() {
        if (this.elContext == null) {
            FreeMarkerJspApplicationContext jspApplicationContext = JspFactory.getDefaultFactory().getJspApplicationContext(getServletContext());
            if (jspApplicationContext instanceof FreeMarkerJspApplicationContext) {
                this.elContext = jspApplicationContext.createNewELContext(super);
                ELContext eLContext = this.elContext;
                Class cls = class$javax$servlet$jsp$JspContext;
                if (cls == null) {
                    cls = class$("javax.servlet.jsp.JspContext");
                    class$javax$servlet$jsp$JspContext = cls;
                }
                eLContext.putContext(cls, this);
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Can not create an ELContext using a foreign JspApplicationContext (of class ");
                stringBuffer.append(ClassUtil.getShortClassNameOfObject(jspApplicationContext));
                stringBuffer.append(").\n");
                stringBuffer.append("Hint: The cause of this is often that you are trying to use JSTL tags/functions in FTL. ");
                stringBuffer.append("In that case, know that that's not really suppored, and you are supposed to use FTL ");
                stringBuffer.append("constrcuts instead, like #list instead of JSTL's forEach, etc.");
                throw new UnsupportedOperationException(stringBuffer.toString());
            }
        }
        return this.elContext;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
