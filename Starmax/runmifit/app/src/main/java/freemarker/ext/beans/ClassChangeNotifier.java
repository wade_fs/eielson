package freemarker.ext.beans;

interface ClassChangeNotifier {
    void subscribe(ClassIntrospector classIntrospector);
}
