package freemarker.ext.jsp;

import freemarker.core.Environment;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.UndeclaredThrowableException;

class PageContextFactory {
    static /* synthetic */ Class class$javax$servlet$jsp$PageContext;
    private static final Class pageContextImpl = getPageContextImpl();

    PageContextFactory() {
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:9|10|(1:12)(1:13)|14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        if (freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext == null) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        r0 = class$("javax.servlet.jsp.PageContext");
        freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        r0 = freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        r0.getMethod("getExpressionEvaluator", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        return java.lang.Class.forName("freemarker.ext.jsp._FreeMarkerPageContext2");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0042, code lost:
        return java.lang.Class.forName("freemarker.ext.jsp._FreeMarkerPageContext1");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
        throw new java.lang.NoClassDefFoundError(r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0021 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Class getPageContextImpl() {
        /*
            java.lang.String r0 = "javax.servlet.jsp.PageContext"
            r1 = 0
            java.lang.Class r2 = freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext     // Catch:{ NoSuchMethodException -> 0x0021 }
            if (r2 != 0) goto L_0x000e
            java.lang.Class r2 = class$(r0)     // Catch:{ NoSuchMethodException -> 0x0021 }
            freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext = r2     // Catch:{ NoSuchMethodException -> 0x0021 }
            goto L_0x0010
        L_0x000e:
            java.lang.Class r2 = freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext     // Catch:{ NoSuchMethodException -> 0x0021 }
        L_0x0010:
            java.lang.String r3 = "getELContext"
            r4 = r1
            java.lang.Class[] r4 = (java.lang.Class[]) r4     // Catch:{ NoSuchMethodException -> 0x0021 }
            r2.getMethod(r3, r4)     // Catch:{ NoSuchMethodException -> 0x0021 }
            java.lang.String r2 = "freemarker.ext.jsp._FreeMarkerPageContext21"
            java.lang.Class r0 = java.lang.Class.forName(r2)     // Catch:{ NoSuchMethodException -> 0x0021 }
            return r0
        L_0x001f:
            r0 = move-exception
            goto L_0x0043
        L_0x0021:
            java.lang.Class r2 = freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext     // Catch:{ NoSuchMethodException -> 0x003c }
            if (r2 != 0) goto L_0x002c
            java.lang.Class r0 = class$(r0)     // Catch:{ NoSuchMethodException -> 0x003c }
            freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext = r0     // Catch:{ NoSuchMethodException -> 0x003c }
            goto L_0x002e
        L_0x002c:
            java.lang.Class r0 = freemarker.ext.jsp.PageContextFactory.class$javax$servlet$jsp$PageContext     // Catch:{ NoSuchMethodException -> 0x003c }
        L_0x002e:
            java.lang.String r2 = "getExpressionEvaluator"
            java.lang.Class[] r1 = (java.lang.Class[]) r1     // Catch:{ NoSuchMethodException -> 0x003c }
            r0.getMethod(r2, r1)     // Catch:{ NoSuchMethodException -> 0x003c }
            java.lang.String r0 = "freemarker.ext.jsp._FreeMarkerPageContext2"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ NoSuchMethodException -> 0x003c }
            return r0
        L_0x003c:
            java.lang.String r0 = "freemarker.ext.jsp._FreeMarkerPageContext1"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x001f }
            return r0
        L_0x0043:
            java.lang.NoClassDefFoundError r1 = new java.lang.NoClassDefFoundError
            java.lang.String r0 = r0.getMessage()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.jsp.PageContextFactory.getPageContextImpl():java.lang.Class");
    }

    static FreeMarkerPageContext getCurrentPageContext() throws TemplateModelException {
        Environment currentEnvironment = Environment.getCurrentEnvironment();
        TemplateModel globalVariable = currentEnvironment.getGlobalVariable("javax.servlet.jsp.jspPageContext");
        if (globalVariable instanceof FreeMarkerPageContext) {
            return (FreeMarkerPageContext) globalVariable;
        }
        try {
            FreeMarkerPageContext freeMarkerPageContext = (FreeMarkerPageContext) pageContextImpl.newInstance();
            currentEnvironment.setGlobalVariable("javax.servlet.jsp.jspPageContext", freeMarkerPageContext);
            return freeMarkerPageContext;
        } catch (IllegalAccessException e) {
            throw new IllegalAccessError(e.getMessage());
        } catch (InstantiationException e2) {
            throw new UndeclaredThrowableException(e2);
        }
    }
}
