package freemarker.ext.beans;

import java.lang.reflect.Array;
import java.util.AbstractList;

class PrimtiveArrayBackedReadOnlyList extends AbstractList {
    private final Object array;

    PrimtiveArrayBackedReadOnlyList(Object obj) {
        this.array = obj;
    }

    public Object get(int i) {
        return Array.get(this.array, i);
    }

    public int size() {
        return Array.getLength(this.array);
    }
}
