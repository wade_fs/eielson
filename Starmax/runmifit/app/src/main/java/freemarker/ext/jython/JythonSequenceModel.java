package freemarker.ext.jython;

import freemarker.ext.util.ModelFactory;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import freemarker.template.TemplateSequenceModel;
import org.python.core.PyException;
import org.python.core.PyObject;

public class JythonSequenceModel extends JythonModel implements TemplateSequenceModel, TemplateCollectionModel {
    static final ModelFactory FACTORY = new ModelFactory() {
        /* class freemarker.ext.jython.JythonSequenceModel.C33581 */

        public TemplateModel create(Object obj, ObjectWrapper objectWrapper) {
            return new JythonSequenceModel((PyObject) obj, (JythonWrapper) objectWrapper);
        }
    };

    public JythonSequenceModel(PyObject pyObject, JythonWrapper jythonWrapper) {
        super(pyObject, jythonWrapper);
    }

    public TemplateModel get(int i) throws TemplateModelException {
        try {
            return this.wrapper.wrap(this.object.__finditem__(i));
        } catch (PyException e) {
            throw new TemplateModelException((Exception) e);
        }
    }

    public int size() throws TemplateModelException {
        try {
            return this.object.__len__();
        } catch (PyException e) {
            throw new TemplateModelException((Exception) e);
        }
    }

    public TemplateModelIterator iterator() {
        return new TemplateModelIterator() {
            /* class freemarker.ext.jython.JythonSequenceModel.C33592 */

            /* renamed from: i */
            int f7496i = 0;

            public boolean hasNext() throws TemplateModelException {
                return this.f7496i < JythonSequenceModel.this.size();
            }

            public TemplateModel next() throws TemplateModelException {
                JythonSequenceModel jythonSequenceModel = JythonSequenceModel.this;
                int i = this.f7496i;
                this.f7496i = i + 1;
                return jythonSequenceModel.get(i);
            }
        };
    }
}
