package freemarker.ext.beans;

final class APIModel extends BeanModel {
    /* access modifiers changed from: protected */
    public boolean isMethodsShadowItems() {
        return true;
    }

    APIModel(Object obj, BeansWrapper beansWrapper) {
        super(obj, beansWrapper, false);
    }
}
