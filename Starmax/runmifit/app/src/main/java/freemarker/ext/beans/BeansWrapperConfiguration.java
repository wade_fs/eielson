package freemarker.ext.beans;

import freemarker.template.ObjectWrapper;
import freemarker.template.Version;
import freemarker.template._TemplateAPI;

public abstract class BeansWrapperConfiguration implements Cloneable {
    protected ClassIntrospectorBuilder classIntrospectorFactory;
    private int defaultDateType;
    private final Version incompatibleImprovements;
    private ObjectWrapper outerIdentity;
    private boolean simpleMapWrapper;
    private boolean strict;
    private boolean useModelCache;

    protected BeansWrapperConfiguration(Version version, boolean z) {
        this.simpleMapWrapper = false;
        this.defaultDateType = 0;
        this.outerIdentity = null;
        this.strict = false;
        this.useModelCache = false;
        _TemplateAPI.checkVersionNotNullAndSupported(version);
        version = !z ? BeansWrapper.normalizeIncompatibleImprovementsVersion(version) : version;
        this.incompatibleImprovements = version;
        this.classIntrospectorFactory = new ClassIntrospectorBuilder(version);
    }

    protected BeansWrapperConfiguration(Version version) {
        this(version, false);
    }

    public int hashCode() {
        int i = 1231;
        int hashCode = (((((this.incompatibleImprovements.hashCode() + 31) * 31) + (this.simpleMapWrapper ? 1231 : 1237)) * 31) + this.defaultDateType) * 31;
        ObjectWrapper objectWrapper = this.outerIdentity;
        int hashCode2 = (((hashCode + (objectWrapper != null ? objectWrapper.hashCode() : 0)) * 31) + (this.strict ? 1231 : 1237)) * 31;
        if (!this.useModelCache) {
            i = 1237;
        }
        return ((hashCode2 + i) * 31) + this.classIntrospectorFactory.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BeansWrapperConfiguration beansWrapperConfiguration = (BeansWrapperConfiguration) obj;
        return this.incompatibleImprovements.equals(beansWrapperConfiguration.incompatibleImprovements) && this.simpleMapWrapper == beansWrapperConfiguration.simpleMapWrapper && this.defaultDateType == beansWrapperConfiguration.defaultDateType && this.outerIdentity == beansWrapperConfiguration.outerIdentity && this.strict == beansWrapperConfiguration.strict && this.useModelCache == beansWrapperConfiguration.useModelCache && this.classIntrospectorFactory.equals(beansWrapperConfiguration.classIntrospectorFactory);
    }

    /* access modifiers changed from: protected */
    public Object clone(boolean z) {
        try {
            BeansWrapperConfiguration beansWrapperConfiguration = (BeansWrapperConfiguration) super.clone();
            if (z) {
                beansWrapperConfiguration.classIntrospectorFactory = (ClassIntrospectorBuilder) this.classIntrospectorFactory.clone();
            }
            return beansWrapperConfiguration;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Failed to clone BeansWrapperConfiguration", e);
        }
    }

    public boolean isSimpleMapWrapper() {
        return this.simpleMapWrapper;
    }

    public void setSimpleMapWrapper(boolean z) {
        this.simpleMapWrapper = z;
    }

    public int getDefaultDateType() {
        return this.defaultDateType;
    }

    public void setDefaultDateType(int i) {
        this.defaultDateType = i;
    }

    public ObjectWrapper getOuterIdentity() {
        return this.outerIdentity;
    }

    public void setOuterIdentity(ObjectWrapper objectWrapper) {
        this.outerIdentity = objectWrapper;
    }

    public boolean isStrict() {
        return this.strict;
    }

    public void setStrict(boolean z) {
        this.strict = z;
    }

    public boolean getUseModelCache() {
        return this.useModelCache;
    }

    public void setUseModelCache(boolean z) {
        this.useModelCache = z;
    }

    public Version getIncompatibleImprovements() {
        return this.incompatibleImprovements;
    }

    public int getExposureLevel() {
        return this.classIntrospectorFactory.getExposureLevel();
    }

    public void setExposureLevel(int i) {
        this.classIntrospectorFactory.setExposureLevel(i);
    }

    public boolean getExposeFields() {
        return this.classIntrospectorFactory.getExposeFields();
    }

    public void setExposeFields(boolean z) {
        this.classIntrospectorFactory.setExposeFields(z);
    }

    public MethodAppearanceFineTuner getMethodAppearanceFineTuner() {
        return this.classIntrospectorFactory.getMethodAppearanceFineTuner();
    }

    public void setMethodAppearanceFineTuner(MethodAppearanceFineTuner methodAppearanceFineTuner) {
        this.classIntrospectorFactory.setMethodAppearanceFineTuner(methodAppearanceFineTuner);
    }

    /* access modifiers changed from: package-private */
    public MethodSorter getMethodSorter() {
        return this.classIntrospectorFactory.getMethodSorter();
    }

    /* access modifiers changed from: package-private */
    public void setMethodSorter(MethodSorter methodSorter) {
        this.classIntrospectorFactory.setMethodSorter(methodSorter);
    }
}
