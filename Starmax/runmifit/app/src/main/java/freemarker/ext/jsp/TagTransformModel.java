package freemarker.ext.jsp;

import freemarker.log.Logger;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateTransformModel;
import freemarker.template.TransformControl;
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.Tag;

class TagTransformModel extends JspTagModelBase implements TemplateTransformModel {
    /* access modifiers changed from: private */
    public static final Logger LOG = Logger.getLogger("freemarker.jsp");
    static /* synthetic */ Class class$javax$servlet$jsp$tagext$BodyTag;
    static /* synthetic */ Class class$javax$servlet$jsp$tagext$IterationTag;
    static /* synthetic */ Class class$javax$servlet$jsp$tagext$Tag;
    static /* synthetic */ Class class$javax$servlet$jsp$tagext$TryCatchFinally;
    /* access modifiers changed from: private */
    public final boolean isBodyTag;
    /* access modifiers changed from: private */
    public final boolean isIterationTag;
    /* access modifiers changed from: private */
    public final boolean isTryCatchFinally;

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TagTransformModel(java.lang.String r1, java.lang.Class r2) throws java.beans.IntrospectionException {
        /*
            r0 = this;
            r0.<init>(r1, r2)
            java.lang.Class r1 = freemarker.ext.jsp.TagTransformModel.class$javax$servlet$jsp$tagext$IterationTag
            if (r1 != 0) goto L_0x000f
            java.lang.String r1 = "javax.servlet.jsp.tagext.IterationTag"
            java.lang.Class r1 = class$(r1)
            freemarker.ext.jsp.TagTransformModel.class$javax$servlet$jsp$tagext$IterationTag = r1
        L_0x000f:
            boolean r1 = r1.isAssignableFrom(r2)
            r0.isIterationTag = r1
            boolean r1 = r0.isIterationTag
            if (r1 == 0) goto L_0x002d
            java.lang.Class r1 = freemarker.ext.jsp.TagTransformModel.class$javax$servlet$jsp$tagext$BodyTag
            if (r1 != 0) goto L_0x0025
            java.lang.String r1 = "javax.servlet.jsp.tagext.BodyTag"
            java.lang.Class r1 = class$(r1)
            freemarker.ext.jsp.TagTransformModel.class$javax$servlet$jsp$tagext$BodyTag = r1
        L_0x0025:
            boolean r1 = r1.isAssignableFrom(r2)
            if (r1 == 0) goto L_0x002d
            r1 = 1
            goto L_0x002e
        L_0x002d:
            r1 = 0
        L_0x002e:
            r0.isBodyTag = r1
            java.lang.Class r1 = freemarker.ext.jsp.TagTransformModel.class$javax$servlet$jsp$tagext$TryCatchFinally
            if (r1 != 0) goto L_0x003c
            java.lang.String r1 = "javax.servlet.jsp.tagext.TryCatchFinally"
            java.lang.Class r1 = class$(r1)
            freemarker.ext.jsp.TagTransformModel.class$javax$servlet$jsp$tagext$TryCatchFinally = r1
        L_0x003c:
            boolean r1 = r1.isAssignableFrom(r2)
            r0.isTryCatchFinally = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.jsp.TagTransformModel.<init>(java.lang.String, java.lang.Class):void");
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public Writer getWriter(Writer writer, Map map) throws TemplateModelException {
        Class cls;
        boolean z;
        JspWriter jspWriter;
        try {
            Tag tag = (Tag) getTagInstance();
            FreeMarkerPageContext currentPageContext = PageContextFactory.getCurrentPageContext();
            if (class$javax$servlet$jsp$tagext$Tag == null) {
                cls = class$("javax.servlet.jsp.tagext.Tag");
                class$javax$servlet$jsp$tagext$Tag = cls;
            } else {
                cls = class$javax$servlet$jsp$tagext$Tag;
            }
            tag.setParent((Tag) currentPageContext.peekTopTag(cls));
            tag.setPageContext(currentPageContext);
            setupTag(tag, map, currentPageContext.getObjectWrapper());
            if (!(writer instanceof JspWriter)) {
                JspWriter jspWriterAdapter = new JspWriterAdapter(writer);
                currentPageContext.pushWriter(jspWriterAdapter);
                jspWriter = jspWriterAdapter;
                z = true;
            } else if (writer == currentPageContext.getOut()) {
                jspWriter = writer;
                z = false;
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("out != pageContext.getOut(). Out is ");
                stringBuffer.append(writer);
                stringBuffer.append(" pageContext.getOut() is ");
                stringBuffer.append(currentPageContext.getOut());
                throw new TemplateModelException(stringBuffer.toString());
            }
            TagWriter tagWriter = new TagWriter(jspWriter, tag, currentPageContext, z);
            currentPageContext.pushTopTag(tag);
            currentPageContext.pushWriter(tagWriter);
            return tagWriter;
        } catch (Exception e) {
            throw toTemplateModelExceptionOrRethrow(e);
        }
    }

    static class BodyContentImpl extends BodyContent {
        private CharArrayWriter buf;

        public void close() throws IOException {
        }

        public int getRemaining() {
            return Integer.MAX_VALUE;
        }

        BodyContentImpl(JspWriter jspWriter, boolean z) {
            super(jspWriter);
            if (z) {
                initBuffer();
            }
        }

        /* access modifiers changed from: package-private */
        public void initBuffer() {
            this.buf = new CharArrayWriter();
        }

        public void flush() throws IOException {
            if (this.buf == null) {
                getEnclosingWriter().flush();
            }
        }

        public void clear() throws IOException {
            if (this.buf != null) {
                this.buf = new CharArrayWriter();
                return;
            }
            throw new IOException("Can't clear");
        }

        public void clearBuffer() throws IOException {
            if (this.buf != null) {
                this.buf = new CharArrayWriter();
                return;
            }
            throw new IOException("Can't clear");
        }

        public void newLine() throws IOException {
            write(JspWriterAdapter.NEWLINE);
        }

        public void print(boolean z) throws IOException {
            write((z ? Boolean.TRUE : Boolean.FALSE).toString());
        }

        public void print(char c) throws IOException {
            write(c);
        }

        public void print(char[] cArr) throws IOException {
            write(cArr);
        }

        public void print(double d) throws IOException {
            write(Double.toString(d));
        }

        public void print(float f) throws IOException {
            write(Float.toString(f));
        }

        public void print(int i) throws IOException {
            write(Integer.toString(i));
        }

        public void print(long j) throws IOException {
            write(Long.toString(j));
        }

        public void print(Object obj) throws IOException {
            write(obj == null ? "null" : obj.toString());
        }

        public void print(String str) throws IOException {
            write(str);
        }

        public void println() throws IOException {
            newLine();
        }

        public void println(boolean z) throws IOException {
            print(z);
            newLine();
        }

        public void println(char c) throws IOException {
            print(c);
            newLine();
        }

        public void println(char[] cArr) throws IOException {
            print(cArr);
            newLine();
        }

        public void println(double d) throws IOException {
            print(d);
            newLine();
        }

        public void println(float f) throws IOException {
            print(f);
            newLine();
        }

        public void println(int i) throws IOException {
            print(i);
            newLine();
        }

        public void println(long j) throws IOException {
            print(j);
            newLine();
        }

        public void println(Object obj) throws IOException {
            print(obj);
            newLine();
        }

        public void println(String str) throws IOException {
            print(str);
            newLine();
        }

        public void write(int i) throws IOException {
            CharArrayWriter charArrayWriter = this.buf;
            if (charArrayWriter != null) {
                charArrayWriter.write(i);
            } else {
                getEnclosingWriter().write(i);
            }
        }

        public void write(char[] cArr, int i, int i2) throws IOException {
            CharArrayWriter charArrayWriter = this.buf;
            if (charArrayWriter != null) {
                charArrayWriter.write(cArr, i, i2);
            } else {
                getEnclosingWriter().write(cArr, i, i2);
            }
        }

        public String getString() {
            return this.buf.toString();
        }

        public Reader getReader() {
            return new CharArrayReader(this.buf.toCharArray());
        }

        public void writeOut(Writer writer) throws IOException {
            this.buf.writeTo(writer);
        }
    }

    class TagWriter extends BodyContentImpl implements TransformControl {
        private final boolean needDoublePop;
        private boolean needPop = true;
        private final FreeMarkerPageContext pageContext;
        private final Tag tag;

        TagWriter(Writer writer, Tag tag2, FreeMarkerPageContext freeMarkerPageContext, boolean z) {
            super((JspWriter) writer, false);
            this.needDoublePop = z;
            this.tag = tag2;
            this.pageContext = freeMarkerPageContext;
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("TagWriter for ");
            stringBuffer.append(this.tag.getClass().getName());
            stringBuffer.append(" wrapping a ");
            stringBuffer.append(getEnclosingWriter().toString());
            return stringBuffer.toString();
        }

        /* access modifiers changed from: package-private */
        public Tag getTag() {
            return this.tag;
        }

        /* access modifiers changed from: package-private */
        public FreeMarkerPageContext getPageContext() {
            return this.pageContext;
        }

        public int onStart() throws TemplateModelException {
            try {
                int doStartTag = this.tag.doStartTag();
                if (doStartTag != 0) {
                    if (doStartTag != 1) {
                        if (doStartTag != 2) {
                            if (doStartTag != 6) {
                                StringBuffer stringBuffer = new StringBuffer();
                                stringBuffer.append("Illegal return value ");
                                stringBuffer.append(doStartTag);
                                stringBuffer.append(" from ");
                                stringBuffer.append(this.tag.getClass().getName());
                                stringBuffer.append(".doStartTag()");
                                throw new RuntimeException(stringBuffer.toString());
                            }
                        } else if (TagTransformModel.this.isBodyTag) {
                            initBuffer();
                            BodyTag bodyTag = this.tag;
                            bodyTag.setBodyContent(this);
                            bodyTag.doInitBody();
                        } else {
                            StringBuffer stringBuffer2 = new StringBuffer();
                            stringBuffer2.append("Can't buffer body since ");
                            stringBuffer2.append(this.tag.getClass().getName());
                            stringBuffer2.append(" does not implement BodyTag.");
                            throw new TemplateModelException(stringBuffer2.toString());
                        }
                    }
                    return 1;
                }
                endEvaluation();
                return 0;
            } catch (Exception e) {
                throw TagTransformModel.this.toTemplateModelExceptionOrRethrow(e);
            }
        }

        public int afterBody() throws TemplateModelException {
            try {
                if (TagTransformModel.this.isIterationTag) {
                    int doAfterBody = this.tag.doAfterBody();
                    if (doAfterBody == 0) {
                        endEvaluation();
                        return 1;
                    } else if (doAfterBody == 2) {
                        return 0;
                    } else {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("Unexpected return value ");
                        stringBuffer.append(doAfterBody);
                        stringBuffer.append("from ");
                        stringBuffer.append(this.tag.getClass().getName());
                        stringBuffer.append(".doAfterBody()");
                        throw new TemplateModelException(stringBuffer.toString());
                    }
                } else {
                    endEvaluation();
                    return 1;
                }
            } catch (Exception e) {
                throw TagTransformModel.this.toTemplateModelExceptionOrRethrow(e);
            }
        }

        private void endEvaluation() throws JspException {
            if (this.needPop) {
                this.pageContext.popWriter();
                this.needPop = false;
            }
            if (this.tag.doEndTag() == 5) {
                Logger access$200 = TagTransformModel.LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Tag.SKIP_PAGE was ignored from a ");
                stringBuffer.append(this.tag.getClass().getName());
                stringBuffer.append(" tag.");
                access$200.warn(stringBuffer.toString());
            }
        }

        public void onError(Throwable th) throws Throwable {
            if (TagTransformModel.this.isTryCatchFinally) {
                this.tag.doCatch(th);
                return;
            }
            throw th;
        }

        public void close() {
            if (this.needPop) {
                this.pageContext.popWriter();
            }
            this.pageContext.popTopTag();
            try {
                if (TagTransformModel.this.isTryCatchFinally) {
                    this.tag.doFinally();
                }
                this.tag.release();
            } finally {
                if (this.needDoublePop) {
                    this.pageContext.popWriter();
                }
            }
        }
    }
}
