package freemarker.ext.beans;

import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

final class ReflectionCallableMemberDescriptor extends CallableMemberDescriptor {
    private final Member member;
    final Class[] paramTypes;

    ReflectionCallableMemberDescriptor(Method method, Class[] clsArr) {
        this.member = method;
        this.paramTypes = clsArr;
    }

    ReflectionCallableMemberDescriptor(Constructor constructor, Class[] clsArr) {
        this.member = constructor;
        this.paramTypes = clsArr;
    }

    /* access modifiers changed from: package-private */
    public TemplateModel invokeMethod(BeansWrapper beansWrapper, Object obj, Object[] objArr) throws TemplateModelException, InvocationTargetException, IllegalAccessException {
        return beansWrapper.invokeMethod(obj, (Method) this.member, objArr);
    }

    /* access modifiers changed from: package-private */
    public Object invokeConstructor(BeansWrapper beansWrapper, Object[] objArr) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return ((Constructor) this.member).newInstance(objArr);
    }

    /* access modifiers changed from: package-private */
    public String getDeclaration() {
        return _MethodUtil.toString(this.member);
    }

    /* access modifiers changed from: package-private */
    public boolean isConstructor() {
        return this.member instanceof Constructor;
    }

    /* access modifiers changed from: package-private */
    public boolean isStatic() {
        return (this.member.getModifiers() & 8) != 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isVarargs() {
        return _MethodUtil.isVarargs(this.member);
    }

    /* access modifiers changed from: package-private */
    public Class[] getParamTypes() {
        return this.paramTypes;
    }

    /* access modifiers changed from: package-private */
    public String getName() {
        return this.member.getName();
    }
}
