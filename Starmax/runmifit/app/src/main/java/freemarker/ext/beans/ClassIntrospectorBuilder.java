package freemarker.ext.beans;

import freemarker.template.Version;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

final class ClassIntrospectorBuilder implements Cloneable {
    private static final Map INSTANCE_CACHE = new HashMap();
    private static final ReferenceQueue INSTANCE_CACHE_REF_QUEUE = new ReferenceQueue();
    private final boolean bugfixed;
    private boolean exposeFields;
    private int exposureLevel = 1;
    private MethodAppearanceFineTuner methodAppearanceFineTuner;
    private MethodSorter methodSorter;

    ClassIntrospectorBuilder(ClassIntrospector classIntrospector) {
        this.bugfixed = classIntrospector.bugfixed;
        this.exposureLevel = classIntrospector.exposureLevel;
        this.exposeFields = classIntrospector.exposeFields;
        this.methodAppearanceFineTuner = classIntrospector.methodAppearanceFineTuner;
        this.methodSorter = classIntrospector.methodSorter;
    }

    ClassIntrospectorBuilder(Version version) {
        this.bugfixed = BeansWrapper.is2321Bugfixed(version);
    }

    /* access modifiers changed from: protected */
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Failed to clone ClassIntrospectorBuilder", e);
        }
    }

    public int hashCode() {
        int i = 1231;
        int i2 = ((this.bugfixed ? 1231 : 1237) + 31) * 31;
        if (!this.exposeFields) {
            i = 1237;
        }
        return ((((((i2 + i) * 31) + this.exposureLevel) * 31) + System.identityHashCode(this.methodAppearanceFineTuner)) * 31) + System.identityHashCode(this.methodSorter);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ClassIntrospectorBuilder classIntrospectorBuilder = (ClassIntrospectorBuilder) obj;
        return this.bugfixed == classIntrospectorBuilder.bugfixed && this.exposeFields == classIntrospectorBuilder.exposeFields && this.exposureLevel == classIntrospectorBuilder.exposureLevel && this.methodAppearanceFineTuner == classIntrospectorBuilder.methodAppearanceFineTuner && this.methodSorter == classIntrospectorBuilder.methodSorter;
    }

    public int getExposureLevel() {
        return this.exposureLevel;
    }

    public void setExposureLevel(int i) {
        if (i < 0 || i > 3) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Illegal exposure level: ");
            stringBuffer.append(i);
            throw new IllegalArgumentException(stringBuffer.toString());
        }
        this.exposureLevel = i;
    }

    public boolean getExposeFields() {
        return this.exposeFields;
    }

    public void setExposeFields(boolean z) {
        this.exposeFields = z;
    }

    public MethodAppearanceFineTuner getMethodAppearanceFineTuner() {
        return this.methodAppearanceFineTuner;
    }

    public void setMethodAppearanceFineTuner(MethodAppearanceFineTuner methodAppearanceFineTuner2) {
        this.methodAppearanceFineTuner = methodAppearanceFineTuner2;
    }

    public MethodSorter getMethodSorter() {
        return this.methodSorter;
    }

    public void setMethodSorter(MethodSorter methodSorter2) {
        this.methodSorter = methodSorter2;
    }

    private static void removeClearedReferencesFromInstanceCache() {
        while (true) {
            Reference poll = INSTANCE_CACHE_REF_QUEUE.poll();
            if (poll != null) {
                synchronized (INSTANCE_CACHE) {
                    Iterator it = INSTANCE_CACHE.values().iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (it.next() == poll) {
                                it.remove();
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
            } else {
                return;
            }
        }
    }

    static void clearInstanceCache() {
        synchronized (INSTANCE_CACHE) {
            INSTANCE_CACHE.clear();
        }
    }

    static Map getInstanceCache() {
        return INSTANCE_CACHE;
    }

    /* access modifiers changed from: package-private */
    public ClassIntrospector build() {
        MethodSorter methodSorter2;
        ClassIntrospector classIntrospector;
        MethodAppearanceFineTuner methodAppearanceFineTuner2 = this.methodAppearanceFineTuner;
        if ((methodAppearanceFineTuner2 != null && !(methodAppearanceFineTuner2 instanceof SingletonCustomizer)) || ((methodSorter2 = this.methodSorter) != null && !(methodSorter2 instanceof SingletonCustomizer))) {
            return new ClassIntrospector(this, new Object(), true, false);
        }
        synchronized (INSTANCE_CACHE) {
            Reference reference = (Reference) INSTANCE_CACHE.get(this);
            classIntrospector = reference != null ? (ClassIntrospector) reference.get() : null;
            if (classIntrospector == null) {
                ClassIntrospectorBuilder classIntrospectorBuilder = (ClassIntrospectorBuilder) clone();
                ClassIntrospector classIntrospector2 = new ClassIntrospector(classIntrospectorBuilder, new Object(), true, true);
                INSTANCE_CACHE.put(classIntrospectorBuilder, new WeakReference(classIntrospector2, INSTANCE_CACHE_REF_QUEUE));
                classIntrospector = classIntrospector2;
            }
        }
        removeClearedReferencesFromInstanceCache();
        return classIntrospector;
    }

    public boolean isBugfixed() {
        return this.bugfixed;
    }
}
