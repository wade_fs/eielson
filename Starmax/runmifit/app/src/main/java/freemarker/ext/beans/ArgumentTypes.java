package freemarker.ext.beans;

import freemarker.core.BugException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.ClassUtil;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

final class ArgumentTypes {
    private static final int CONVERSION_DIFFICULTY_FREEMARKER = 1;
    private static final int CONVERSION_DIFFICULTY_IMPOSSIBLE = 2;
    private static final int CONVERSION_DIFFICULTY_REFLECTION = 0;
    static /* synthetic */ Class class$freemarker$ext$beans$ArgumentTypes$Null;
    static /* synthetic */ Class class$freemarker$ext$beans$CharacterOrString;
    static /* synthetic */ Class class$java$lang$Boolean;
    static /* synthetic */ Class class$java$lang$Byte;
    static /* synthetic */ Class class$java$lang$Character;
    static /* synthetic */ Class class$java$lang$Double;
    static /* synthetic */ Class class$java$lang$Float;
    static /* synthetic */ Class class$java$lang$Integer;
    static /* synthetic */ Class class$java$lang$Long;
    static /* synthetic */ Class class$java$lang$Number;
    static /* synthetic */ Class class$java$lang$Object;
    static /* synthetic */ Class class$java$lang$Short;
    static /* synthetic */ Class class$java$lang$String;
    static /* synthetic */ Class class$java$math$BigDecimal;
    static /* synthetic */ Class class$java$util$Collection;
    static /* synthetic */ Class class$java$util$List;
    private final boolean bugfixed;
    private final Class[] types;

    ArgumentTypes(Object[] objArr, boolean z) {
        Class<?> cls;
        int length = objArr.length;
        Class[] clsArr = new Class[length];
        for (int i = 0; i < length; i++) {
            Object obj = objArr[i];
            if (obj != null) {
                cls = obj.getClass();
            } else if (z) {
                cls = class$freemarker$ext$beans$ArgumentTypes$Null;
                if (cls == null) {
                    cls = class$("freemarker.ext.beans.ArgumentTypes$Null");
                    class$freemarker$ext$beans$ArgumentTypes$Null = cls;
                }
            } else {
                cls = class$java$lang$Object;
                if (cls == null) {
                    cls = class$("java.lang.Object");
                    class$java$lang$Object = cls;
                }
            }
            clsArr[i] = cls;
        }
        this.types = clsArr;
        this.bugfixed = z;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public int hashCode() {
        int i = 0;
        int i2 = 0;
        while (true) {
            Class[] clsArr = this.types;
            if (i >= clsArr.length) {
                return i2;
            }
            i2 ^= clsArr[i].hashCode();
            i++;
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ArgumentTypes)) {
            return false;
        }
        ArgumentTypes argumentTypes = (ArgumentTypes) obj;
        if (argumentTypes.types.length != this.types.length) {
            return false;
        }
        int i = 0;
        while (true) {
            Class[] clsArr = this.types;
            if (i >= clsArr.length) {
                return true;
            }
            if (argumentTypes.types[i] != clsArr[i]) {
                return false;
            }
            i++;
        }
    }

    /* access modifiers changed from: package-private */
    public MaybeEmptyCallableMemberDescriptor getMostSpecific(List list, boolean z) {
        LinkedList applicables = getApplicables(list, z);
        if (applicables.isEmpty()) {
            return EmptyCallableMemberDescriptor.NO_SUCH_METHOD;
        }
        if (applicables.size() == 1) {
            return (CallableMemberDescriptor) applicables.getFirst();
        }
        LinkedList linkedList = new LinkedList();
        Iterator it = applicables.iterator();
        while (it.hasNext()) {
            CallableMemberDescriptor callableMemberDescriptor = (CallableMemberDescriptor) it.next();
            boolean z2 = false;
            Iterator it2 = linkedList.iterator();
            while (it2.hasNext()) {
                int compareParameterListPreferability = compareParameterListPreferability(callableMemberDescriptor.getParamTypes(), ((CallableMemberDescriptor) it2.next()).getParamTypes(), z);
                if (compareParameterListPreferability > 0) {
                    it2.remove();
                } else if (compareParameterListPreferability < 0) {
                    z2 = true;
                }
            }
            if (!z2) {
                linkedList.addLast(callableMemberDescriptor);
            }
        }
        if (linkedList.size() > 1) {
            return EmptyCallableMemberDescriptor.AMBIGUOUS_METHOD;
        }
        return (CallableMemberDescriptor) linkedList.getFirst();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0180, code lost:
        if (r5 > 40000) goto L_0x0182;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x012d, code lost:
        if (r2.isAssignableFrom(r1) != false) goto L_0x012f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x015e, code lost:
        if (r1 > 1) goto L_0x0173;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int compareParameterListPreferability(java.lang.Class[] r23, java.lang.Class[] r24, boolean r25) {
        /*
            r22 = this;
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r25
            java.lang.Class[] r4 = r0.types
            int r4 = r4.length
            int r5 = r1.length
            int r6 = r2.length
            boolean r7 = r0.bugfixed
            if (r7 == 0) goto L_0x020c
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
        L_0x001c:
            if (r7 >= r4) goto L_0x01b7
            r17 = r4
            java.lang.Class r4 = getParamType(r1, r5, r7, r3)
            java.lang.Class r1 = getParamType(r2, r6, r7, r3)
            r18 = 2
            if (r4 != r1) goto L_0x0033
            r20 = r5
            r19 = r6
        L_0x0030:
            r1 = 0
            goto L_0x01a1
        L_0x0033:
            java.lang.Class[] r2 = r0.types
            r2 = r2[r7]
            java.lang.Class r19 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Number
            if (r19 != 0) goto L_0x0043
            java.lang.String r19 = "java.lang.Number"
            java.lang.Class r19 = class$(r19)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Number = r19
        L_0x0043:
            r20 = r5
            r5 = r19
            boolean r5 = r5.isAssignableFrom(r2)
            r19 = r6
            if (r5 == 0) goto L_0x0068
            boolean r21 = freemarker.template.utility.ClassUtil.isNumerical(r4)
            if (r21 == 0) goto L_0x0068
            boolean r21 = r4.isPrimitive()
            if (r21 == 0) goto L_0x0062
            java.lang.Class r21 = freemarker.template.utility.ClassUtil.primitiveClassToBoxingClass(r4)
            r6 = r21
            goto L_0x0063
        L_0x0062:
            r6 = r4
        L_0x0063:
            int r6 = freemarker.ext.beans.OverloadedNumberUtil.getArgumentConversionPrice(r2, r6)
            goto L_0x006b
        L_0x0068:
            r6 = 2147483647(0x7fffffff, float:NaN)
        L_0x006b:
            if (r5 == 0) goto L_0x0087
            boolean r5 = freemarker.template.utility.ClassUtil.isNumerical(r1)
            if (r5 == 0) goto L_0x0087
            boolean r5 = r1.isPrimitive()
            if (r5 == 0) goto L_0x007e
            java.lang.Class r5 = freemarker.template.utility.ClassUtil.primitiveClassToBoxingClass(r1)
            goto L_0x007f
        L_0x007e:
            r5 = r1
        L_0x007f:
            int r5 = freemarker.ext.beans.OverloadedNumberUtil.getArgumentConversionPrice(r2, r5)
            r3 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x008d
        L_0x0087:
            r3 = 2147483647(0x7fffffff, float:NaN)
            r5 = 2147483647(0x7fffffff, float:NaN)
        L_0x008d:
            if (r6 != r3) goto L_0x016e
            if (r5 != r3) goto L_0x016a
            java.lang.Class r3 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
            java.lang.String r5 = "java.util.List"
            if (r3 != 0) goto L_0x009d
            java.lang.Class r3 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$java$util$List = r3
        L_0x009d:
            boolean r3 = r3.isAssignableFrom(r2)
            if (r3 == 0) goto L_0x0109
            boolean r3 = r4.isArray()
            if (r3 != 0) goto L_0x00af
            boolean r3 = r1.isArray()
            if (r3 == 0) goto L_0x0109
        L_0x00af:
            boolean r2 = r4.isArray()
            java.lang.String r3 = "java.util.Collection"
            if (r2 == 0) goto L_0x00f2
            boolean r2 = r1.isArray()
            if (r2 == 0) goto L_0x00db
            java.lang.Class r2 = r4.getComponentType()
            java.lang.Class r1 = r1.getComponentType()
            int r1 = r0.compareParameterListPreferability_cmpTypeSpecificty(r2, r1)
            if (r1 <= 0) goto L_0x00ce
            int r14 = r14 + 1
            goto L_0x00d7
        L_0x00ce:
            if (r1 >= 0) goto L_0x00d5
            int r13 = r13 + 1
            r18 = 1
            goto L_0x00d7
        L_0x00d5:
            r18 = 0
        L_0x00d7:
            r1 = r18
            goto L_0x01a1
        L_0x00db:
            java.lang.Class r2 = freemarker.ext.beans.ArgumentTypes.class$java$util$Collection
            if (r2 != 0) goto L_0x00e5
            java.lang.Class r2 = class$(r3)
            freemarker.ext.beans.ArgumentTypes.class$java$util$Collection = r2
        L_0x00e5:
            boolean r1 = r2.isAssignableFrom(r1)
            if (r1 == 0) goto L_0x00ee
            int r14 = r14 + 1
            goto L_0x0106
        L_0x00ee:
            int r8 = r8 + 1
            goto L_0x0175
        L_0x00f2:
            java.lang.Class r1 = freemarker.ext.beans.ArgumentTypes.class$java$util$Collection
            if (r1 != 0) goto L_0x00fc
            java.lang.Class r1 = class$(r3)
            freemarker.ext.beans.ArgumentTypes.class$java$util$Collection = r1
        L_0x00fc:
            boolean r1 = r1.isAssignableFrom(r4)
            if (r1 == 0) goto L_0x0104
            goto L_0x0182
        L_0x0104:
            int r10 = r10 + 1
        L_0x0106:
            r1 = 2
            goto L_0x01a1
        L_0x0109:
            boolean r2 = r2.isArray()
            if (r2 == 0) goto L_0x0157
            java.lang.Class r2 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
            if (r2 != 0) goto L_0x0119
            java.lang.Class r2 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$java$util$List = r2
        L_0x0119:
            boolean r2 = r2.isAssignableFrom(r4)
            if (r2 != 0) goto L_0x012f
            java.lang.Class r2 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
            if (r2 != 0) goto L_0x0129
            java.lang.Class r2 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$java$util$List = r2
        L_0x0129:
            boolean r2 = r2.isAssignableFrom(r1)
            if (r2 == 0) goto L_0x0157
        L_0x012f:
            java.lang.Class r2 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
            if (r2 != 0) goto L_0x0139
            java.lang.Class r2 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$java$util$List = r2
        L_0x0139:
            boolean r2 = r2.isAssignableFrom(r4)
            if (r2 == 0) goto L_0x0154
            java.lang.Class r2 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
            if (r2 != 0) goto L_0x0149
            java.lang.Class r2 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$java$util$List = r2
        L_0x0149:
            boolean r1 = r2.isAssignableFrom(r1)
            if (r1 == 0) goto L_0x0151
            goto L_0x0030
        L_0x0151:
            int r12 = r12 + 1
            goto L_0x0106
        L_0x0154:
            int r11 = r11 + 1
            goto L_0x0175
        L_0x0157:
            int r1 = r0.compareParameterListPreferability_cmpTypeSpecificty(r4, r1)
            if (r1 <= 0) goto L_0x0161
            r2 = 1
            if (r1 <= r2) goto L_0x00ee
            goto L_0x0173
        L_0x0161:
            if (r1 >= 0) goto L_0x0030
            r2 = -1
            if (r1 >= r2) goto L_0x0167
            goto L_0x016a
        L_0x0167:
            int r10 = r10 + 1
            goto L_0x016c
        L_0x016a:
            int r9 = r9 + 1
        L_0x016c:
            r1 = -1
            goto L_0x01a1
        L_0x016e:
            r2 = 2147483647(0x7fffffff, float:NaN)
            if (r5 != r2) goto L_0x0177
        L_0x0173:
            int r15 = r15 + 1
        L_0x0175:
            r1 = 1
            goto L_0x01a1
        L_0x0177:
            if (r6 == r5) goto L_0x018c
            r1 = 40000(0x9c40, float:5.6052E-41)
            if (r6 >= r5) goto L_0x0185
            if (r6 >= r1) goto L_0x0173
            if (r5 <= r1) goto L_0x0173
        L_0x0182:
            int r13 = r13 + 1
            goto L_0x0175
        L_0x0185:
            if (r5 >= r1) goto L_0x016a
            if (r6 <= r1) goto L_0x016a
            int r14 = r14 + 1
            goto L_0x016c
        L_0x018c:
            boolean r2 = r4.isPrimitive()
            boolean r1 = r1.isPrimitive()
            int r1 = r2 - r1
            r2 = 1
            if (r1 != r2) goto L_0x019c
            int r8 = r8 + 1
            goto L_0x01a1
        L_0x019c:
            r2 = -1
            if (r1 != r2) goto L_0x01a1
            int r10 = r10 + 1
        L_0x01a1:
            if (r16 != 0) goto L_0x01a7
            if (r1 == 0) goto L_0x01a7
            r16 = r1
        L_0x01a7:
            int r7 = r7 + 1
            r1 = r23
            r2 = r24
            r3 = r25
            r4 = r17
            r6 = r19
            r5 = r20
            goto L_0x001c
        L_0x01b7:
            r17 = r4
            r20 = r5
            r19 = r6
            if (r11 == r12) goto L_0x01c1
            int r11 = r11 - r12
            return r11
        L_0x01c1:
            if (r13 == r14) goto L_0x01c5
            int r13 = r13 - r14
            return r13
        L_0x01c5:
            if (r15 == r9) goto L_0x01c9
            int r15 = r15 - r9
            return r15
        L_0x01c9:
            if (r8 == r10) goto L_0x01cd
            int r8 = r8 - r10
            return r8
        L_0x01cd:
            if (r16 == 0) goto L_0x01d0
            return r16
        L_0x01d0:
            r1 = r25
            if (r1 == 0) goto L_0x020a
            r3 = r19
            r2 = r20
            if (r2 != r3) goto L_0x0207
            int r5 = r2 + -1
            r1 = r17
            if (r1 != r5) goto L_0x0205
            r4 = r23
            r5 = 1
            java.lang.Class r2 = getParamType(r4, r2, r1, r5)
            r6 = r24
            java.lang.Class r1 = getParamType(r6, r3, r1, r5)
            boolean r3 = freemarker.template.utility.ClassUtil.isNumerical(r2)
            if (r3 == 0) goto L_0x0200
            boolean r3 = freemarker.template.utility.ClassUtil.isNumerical(r1)
            if (r3 == 0) goto L_0x0200
            int r3 = freemarker.ext.beans.OverloadedNumberUtil.compareNumberTypeSpecificity(r2, r1)
            if (r3 == 0) goto L_0x0200
            return r3
        L_0x0200:
            int r1 = r0.compareParameterListPreferability_cmpTypeSpecificty(r2, r1)
            return r1
        L_0x0205:
            r5 = 0
            return r5
        L_0x0207:
            int r5 = r2 - r3
            return r5
        L_0x020a:
            r5 = 0
            return r5
        L_0x020c:
            r4 = r1
            r1 = r3
            r3 = r6
            r6 = r2
            r2 = r5
            r5 = 0
            r7 = 0
            r8 = 0
            r9 = 0
        L_0x0215:
            if (r7 >= r2) goto L_0x023e
            java.lang.Class r10 = getParamType(r4, r2, r7, r1)
            java.lang.Class r11 = getParamType(r6, r3, r7, r1)
            if (r10 == r11) goto L_0x023a
            if (r8 != 0) goto L_0x022c
            int r8 = freemarker.ext.beans._MethodUtil.isMoreOrSameSpecificParameterType(r10, r11, r5, r5)
            if (r8 == 0) goto L_0x022a
            goto L_0x022c
        L_0x022a:
            r8 = 0
            goto L_0x022d
        L_0x022c:
            r8 = 1
        L_0x022d:
            if (r9 != 0) goto L_0x0238
            int r9 = freemarker.ext.beans._MethodUtil.isMoreOrSameSpecificParameterType(r11, r10, r5, r5)
            if (r9 == 0) goto L_0x0236
            goto L_0x0238
        L_0x0236:
            r5 = 0
            goto L_0x0239
        L_0x0238:
            r5 = 1
        L_0x0239:
            r9 = r5
        L_0x023a:
            int r7 = r7 + 1
            r5 = 0
            goto L_0x0215
        L_0x023e:
            if (r8 == 0) goto L_0x0243
            r1 = 1
            r1 = r1 ^ r9
            return r1
        L_0x0243:
            if (r9 == 0) goto L_0x0247
            r1 = -1
            return r1
        L_0x0247:
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.ArgumentTypes.compareParameterListPreferability(java.lang.Class[], java.lang.Class[], boolean):int");
    }

    private int compareParameterListPreferability_cmpTypeSpecificty(Class cls, Class cls2) {
        Class cls3;
        Class primitiveClassToBoxingClass = cls.isPrimitive() ? ClassUtil.primitiveClassToBoxingClass(cls) : cls;
        if (cls2.isPrimitive()) {
            cls3 = ClassUtil.primitiveClassToBoxingClass(cls2);
        } else {
            cls3 = cls2;
        }
        if (primitiveClassToBoxingClass == cls3) {
            return primitiveClassToBoxingClass != cls ? cls3 != cls2 ? 0 : 1 : cls3 != cls2 ? -1 : 0;
        }
        if (cls3.isAssignableFrom(primitiveClassToBoxingClass)) {
            return 2;
        }
        if (primitiveClassToBoxingClass.isAssignableFrom(cls3)) {
            return -2;
        }
        Class cls4 = class$java$lang$Character;
        if (cls4 == null) {
            cls4 = class$("java.lang.Character");
            class$java$lang$Character = cls4;
        }
        if (primitiveClassToBoxingClass == cls4) {
            Class cls5 = class$java$lang$String;
            if (cls5 == null) {
                cls5 = class$("java.lang.String");
                class$java$lang$String = cls5;
            }
            if (cls3.isAssignableFrom(cls5)) {
                return 2;
            }
        }
        Class cls6 = class$java$lang$Character;
        if (cls6 == null) {
            cls6 = class$("java.lang.Character");
            class$java$lang$Character = cls6;
        }
        if (cls3 == cls6) {
            Class cls7 = class$java$lang$String;
            if (cls7 == null) {
                cls7 = class$("java.lang.String");
                class$java$lang$String = cls7;
            }
            if (primitiveClassToBoxingClass.isAssignableFrom(cls7)) {
                return -2;
            }
        }
        return 0;
    }

    private static Class getParamType(Class[] clsArr, int i, int i2, boolean z) {
        int i3;
        return (!z || i2 < (i3 = i + -1)) ? clsArr[i2] : clsArr[i3].getComponentType();
    }

    /* access modifiers changed from: package-private */
    public LinkedList getApplicables(List list, boolean z) {
        LinkedList linkedList = new LinkedList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ReflectionCallableMemberDescriptor reflectionCallableMemberDescriptor = (ReflectionCallableMemberDescriptor) it.next();
            int isApplicable = isApplicable(reflectionCallableMemberDescriptor, z);
            if (isApplicable != 2) {
                if (isApplicable == 0) {
                    linkedList.add(reflectionCallableMemberDescriptor);
                } else if (isApplicable == 1) {
                    linkedList.add(new SpecialConversionCallableMemberDescriptor(reflectionCallableMemberDescriptor));
                } else {
                    throw new BugException();
                }
            }
        }
        return linkedList;
    }

    private int isApplicable(ReflectionCallableMemberDescriptor reflectionCallableMemberDescriptor, boolean z) {
        Class[] paramTypes = reflectionCallableMemberDescriptor.getParamTypes();
        int length = this.types.length;
        int length2 = paramTypes.length - (z ? 1 : 0);
        if (z) {
            if (length < length2) {
                return 2;
            }
        } else if (length != length2) {
            return 2;
        }
        int i = 0;
        for (int i2 = 0; i2 < length2; i2++) {
            int isMethodInvocationConvertible = isMethodInvocationConvertible(paramTypes[i2], this.types[i2]);
            if (isMethodInvocationConvertible == 2) {
                return 2;
            }
            if (i < isMethodInvocationConvertible) {
                i = isMethodInvocationConvertible;
            }
        }
        if (z) {
            Class<?> componentType = paramTypes[length2].getComponentType();
            while (length2 < length) {
                int isMethodInvocationConvertible2 = isMethodInvocationConvertible(componentType, this.types[length2]);
                if (isMethodInvocationConvertible2 == 2) {
                    return 2;
                }
                if (i < isMethodInvocationConvertible2) {
                    i = isMethodInvocationConvertible2;
                }
                length2++;
            }
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:106:0x014b, code lost:
        if (r11 == r0) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0174, code lost:
        if (r11 == r0) goto L_0x0176;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x01a9, code lost:
        if (r11 == r0) goto L_0x01ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x01ea, code lost:
        if (r11 == r0) goto L_0x01ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x022b, code lost:
        if (r11 == r0) goto L_0x022d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int isMethodInvocationConvertible(java.lang.Class r10, java.lang.Class r11) {
        /*
            r9 = this;
            boolean r0 = r10.isAssignableFrom(r11)
            java.lang.String r1 = "freemarker.ext.beans.CharacterOrString"
            r2 = 0
            if (r0 == 0) goto L_0x0016
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$freemarker$ext$beans$CharacterOrString
            if (r0 != 0) goto L_0x0013
            java.lang.Class r0 = class$(r1)
            freemarker.ext.beans.ArgumentTypes.class$freemarker$ext$beans$CharacterOrString = r0
        L_0x0013:
            if (r11 == r0) goto L_0x0016
            return r2
        L_0x0016:
            boolean r0 = r9.bugfixed
            java.lang.String r3 = "java.lang.Character"
            r4 = 2
            if (r0 == 0) goto L_0x00da
            boolean r0 = r10.isPrimitive()
            java.lang.String r5 = "freemarker.ext.beans.ArgumentTypes$Null"
            if (r0 == 0) goto L_0x0039
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$freemarker$ext$beans$ArgumentTypes$Null
            if (r0 != 0) goto L_0x002f
            java.lang.Class r0 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$freemarker$ext$beans$ArgumentTypes$Null = r0
        L_0x002f:
            if (r11 != r0) goto L_0x0032
            return r4
        L_0x0032:
            java.lang.Class r0 = freemarker.template.utility.ClassUtil.primitiveClassToBoxingClass(r10)
            if (r11 != r0) goto L_0x0047
            return r2
        L_0x0039:
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$freemarker$ext$beans$ArgumentTypes$Null
            if (r0 != 0) goto L_0x0043
            java.lang.Class r0 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$freemarker$ext$beans$ArgumentTypes$Null = r0
        L_0x0043:
            if (r11 != r0) goto L_0x0046
            return r2
        L_0x0046:
            r0 = r10
        L_0x0047:
            java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Number
            java.lang.String r6 = "java.lang.Number"
            if (r5 != 0) goto L_0x0053
            java.lang.Class r5 = class$(r6)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Number = r5
        L_0x0053:
            boolean r5 = r5.isAssignableFrom(r11)
            if (r5 == 0) goto L_0x0074
            java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Number
            if (r5 != 0) goto L_0x0063
            java.lang.Class r5 = class$(r6)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Number = r5
        L_0x0063:
            boolean r5 = r5.isAssignableFrom(r0)
            if (r5 == 0) goto L_0x0074
            int r10 = freemarker.ext.beans.OverloadedNumberUtil.getArgumentConversionPrice(r11, r0)
            r11 = 2147483647(0x7fffffff, float:NaN)
            if (r10 != r11) goto L_0x0073
            r2 = 2
        L_0x0073:
            return r2
        L_0x0074:
            boolean r0 = r10.isArray()
            java.lang.String r2 = "java.util.List"
            r5 = 1
            if (r0 == 0) goto L_0x008f
            java.lang.Class r10 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
            if (r10 != 0) goto L_0x0087
            java.lang.Class r10 = class$(r2)
            freemarker.ext.beans.ArgumentTypes.class$java$util$List = r10
        L_0x0087:
            boolean r10 = r10.isAssignableFrom(r11)
            if (r10 == 0) goto L_0x008e
            r4 = 1
        L_0x008e:
            return r4
        L_0x008f:
            boolean r0 = r11.isArray()
            if (r0 == 0) goto L_0x00a6
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
            if (r0 != 0) goto L_0x009f
            java.lang.Class r0 = class$(r2)
            freemarker.ext.beans.ArgumentTypes.class$java$util$List = r0
        L_0x009f:
            boolean r0 = r10.isAssignableFrom(r0)
            if (r0 == 0) goto L_0x00a6
            return r5
        L_0x00a6:
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$freemarker$ext$beans$CharacterOrString
            if (r0 != 0) goto L_0x00b0
            java.lang.Class r0 = class$(r1)
            freemarker.ext.beans.ArgumentTypes.class$freemarker$ext$beans$CharacterOrString = r0
        L_0x00b0:
            if (r11 != r0) goto L_0x00d9
            java.lang.Class r11 = freemarker.ext.beans.ArgumentTypes.class$java$lang$String
            if (r11 != 0) goto L_0x00be
            java.lang.String r11 = "java.lang.String"
            java.lang.Class r11 = class$(r11)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$String = r11
        L_0x00be:
            boolean r11 = r10.isAssignableFrom(r11)
            if (r11 != 0) goto L_0x00d8
            java.lang.Class r11 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Character
            if (r11 != 0) goto L_0x00ce
            java.lang.Class r11 = class$(r3)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Character = r11
        L_0x00ce:
            boolean r11 = r10.isAssignableFrom(r11)
            if (r11 != 0) goto L_0x00d8
            java.lang.Class r11 = java.lang.Character.TYPE
            if (r10 != r11) goto L_0x00d9
        L_0x00d8:
            return r5
        L_0x00d9:
            return r4
        L_0x00da:
            boolean r0 = r10.isPrimitive()
            if (r0 == 0) goto L_0x0247
            java.lang.Class r0 = java.lang.Boolean.TYPE
            if (r10 != r0) goto L_0x00f5
            java.lang.Class r10 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Boolean
            if (r10 != 0) goto L_0x00f0
            java.lang.String r10 = "java.lang.Boolean"
            java.lang.Class r10 = class$(r10)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Boolean = r10
        L_0x00f0:
            if (r11 != r10) goto L_0x00f3
            goto L_0x00f4
        L_0x00f3:
            r2 = 2
        L_0x00f4:
            return r2
        L_0x00f5:
            java.lang.Class r0 = java.lang.Double.TYPE
            java.lang.String r1 = "java.lang.Float"
            java.lang.String r5 = "java.lang.Long"
            java.lang.String r6 = "java.lang.Integer"
            java.lang.String r7 = "java.lang.Short"
            java.lang.String r8 = "java.lang.Byte"
            if (r10 != r0) goto L_0x014e
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Double
            if (r0 != 0) goto L_0x010f
            java.lang.String r0 = "java.lang.Double"
            java.lang.Class r0 = class$(r0)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Double = r0
        L_0x010f:
            if (r11 == r0) goto L_0x014d
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Float
            if (r0 != 0) goto L_0x011b
            java.lang.Class r0 = class$(r1)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Float = r0
        L_0x011b:
            if (r11 == r0) goto L_0x014d
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Long
            if (r0 != 0) goto L_0x0127
            java.lang.Class r0 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Long = r0
        L_0x0127:
            if (r11 == r0) goto L_0x014d
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Integer
            if (r0 != 0) goto L_0x0133
            java.lang.Class r0 = class$(r6)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Integer = r0
        L_0x0133:
            if (r11 == r0) goto L_0x014d
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Short
            if (r0 != 0) goto L_0x013f
            java.lang.Class r0 = class$(r7)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Short = r0
        L_0x013f:
            if (r11 == r0) goto L_0x014d
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte
            if (r0 != 0) goto L_0x014b
            java.lang.Class r0 = class$(r8)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte = r0
        L_0x014b:
            if (r11 != r0) goto L_0x014e
        L_0x014d:
            return r2
        L_0x014e:
            java.lang.Class r0 = java.lang.Integer.TYPE
            if (r10 != r0) goto L_0x0177
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Integer
            if (r0 != 0) goto L_0x015c
            java.lang.Class r0 = class$(r6)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Integer = r0
        L_0x015c:
            if (r11 == r0) goto L_0x0176
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Short
            if (r0 != 0) goto L_0x0168
            java.lang.Class r0 = class$(r7)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Short = r0
        L_0x0168:
            if (r11 == r0) goto L_0x0176
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte
            if (r0 != 0) goto L_0x0174
            java.lang.Class r0 = class$(r8)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte = r0
        L_0x0174:
            if (r11 != r0) goto L_0x0177
        L_0x0176:
            return r2
        L_0x0177:
            java.lang.Class r0 = java.lang.Long.TYPE
            if (r10 != r0) goto L_0x01ac
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Long
            if (r0 != 0) goto L_0x0185
            java.lang.Class r0 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Long = r0
        L_0x0185:
            if (r11 == r0) goto L_0x01ab
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Integer
            if (r0 != 0) goto L_0x0191
            java.lang.Class r0 = class$(r6)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Integer = r0
        L_0x0191:
            if (r11 == r0) goto L_0x01ab
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Short
            if (r0 != 0) goto L_0x019d
            java.lang.Class r0 = class$(r7)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Short = r0
        L_0x019d:
            if (r11 == r0) goto L_0x01ab
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte
            if (r0 != 0) goto L_0x01a9
            java.lang.Class r0 = class$(r8)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte = r0
        L_0x01a9:
            if (r11 != r0) goto L_0x01ac
        L_0x01ab:
            return r2
        L_0x01ac:
            java.lang.Class r0 = java.lang.Float.TYPE
            if (r10 != r0) goto L_0x01ed
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Float
            if (r0 != 0) goto L_0x01ba
            java.lang.Class r0 = class$(r1)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Float = r0
        L_0x01ba:
            if (r11 == r0) goto L_0x01ec
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Long
            if (r0 != 0) goto L_0x01c6
            java.lang.Class r0 = class$(r5)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Long = r0
        L_0x01c6:
            if (r11 == r0) goto L_0x01ec
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Integer
            if (r0 != 0) goto L_0x01d2
            java.lang.Class r0 = class$(r6)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Integer = r0
        L_0x01d2:
            if (r11 == r0) goto L_0x01ec
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Short
            if (r0 != 0) goto L_0x01de
            java.lang.Class r0 = class$(r7)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Short = r0
        L_0x01de:
            if (r11 == r0) goto L_0x01ec
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte
            if (r0 != 0) goto L_0x01ea
            java.lang.Class r0 = class$(r8)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte = r0
        L_0x01ea:
            if (r11 != r0) goto L_0x01ed
        L_0x01ec:
            return r2
        L_0x01ed:
            java.lang.Class r0 = java.lang.Character.TYPE
            if (r10 != r0) goto L_0x0200
            java.lang.Class r10 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Character
            if (r10 != 0) goto L_0x01fb
            java.lang.Class r10 = class$(r3)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Character = r10
        L_0x01fb:
            if (r11 != r10) goto L_0x01fe
            goto L_0x01ff
        L_0x01fe:
            r2 = 2
        L_0x01ff:
            return r2
        L_0x0200:
            java.lang.Class r0 = java.lang.Byte.TYPE
            if (r10 != r0) goto L_0x0211
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte
            if (r0 != 0) goto L_0x020e
            java.lang.Class r0 = class$(r8)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte = r0
        L_0x020e:
            if (r11 != r0) goto L_0x0211
            return r2
        L_0x0211:
            java.lang.Class r0 = java.lang.Short.TYPE
            if (r10 != r0) goto L_0x022e
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Short
            if (r0 != 0) goto L_0x021f
            java.lang.Class r0 = class$(r7)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Short = r0
        L_0x021f:
            if (r11 == r0) goto L_0x022d
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte
            if (r0 != 0) goto L_0x022b
            java.lang.Class r0 = class$(r8)
            freemarker.ext.beans.ArgumentTypes.class$java$lang$Byte = r0
        L_0x022b:
            if (r11 != r0) goto L_0x022e
        L_0x022d:
            return r2
        L_0x022e:
            java.lang.Class r0 = freemarker.ext.beans.ArgumentTypes.class$java$math$BigDecimal
            if (r0 != 0) goto L_0x023a
            java.lang.String r0 = "java.math.BigDecimal"
            java.lang.Class r0 = class$(r0)
            freemarker.ext.beans.ArgumentTypes.class$java$math$BigDecimal = r0
        L_0x023a:
            boolean r11 = r0.isAssignableFrom(r11)
            if (r11 == 0) goto L_0x0247
            boolean r10 = freemarker.template.utility.ClassUtil.isNumerical(r10)
            if (r10 == 0) goto L_0x0247
            return r2
        L_0x0247:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.ArgumentTypes.isMethodInvocationConvertible(java.lang.Class, java.lang.Class):int");
    }

    private static class Null {
        private Null() {
        }
    }

    private static final class SpecialConversionCallableMemberDescriptor extends CallableMemberDescriptor {
        private final ReflectionCallableMemberDescriptor callableMemberDesc;

        SpecialConversionCallableMemberDescriptor(ReflectionCallableMemberDescriptor reflectionCallableMemberDescriptor) {
            this.callableMemberDesc = reflectionCallableMemberDescriptor;
        }

        /* access modifiers changed from: package-private */
        public TemplateModel invokeMethod(BeansWrapper beansWrapper, Object obj, Object[] objArr) throws TemplateModelException, InvocationTargetException, IllegalAccessException {
            convertArgsToReflectionCompatible(beansWrapper, objArr);
            return this.callableMemberDesc.invokeMethod(beansWrapper, obj, objArr);
        }

        /* access modifiers changed from: package-private */
        public Object invokeConstructor(BeansWrapper beansWrapper, Object[] objArr) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, TemplateModelException {
            convertArgsToReflectionCompatible(beansWrapper, objArr);
            return this.callableMemberDesc.invokeConstructor(beansWrapper, objArr);
        }

        /* access modifiers changed from: package-private */
        public String getDeclaration() {
            return this.callableMemberDesc.getDeclaration();
        }

        /* access modifiers changed from: package-private */
        public boolean isConstructor() {
            return this.callableMemberDesc.isConstructor();
        }

        /* access modifiers changed from: package-private */
        public boolean isStatic() {
            return this.callableMemberDesc.isStatic();
        }

        /* access modifiers changed from: package-private */
        public boolean isVarargs() {
            return this.callableMemberDesc.isVarargs();
        }

        /* access modifiers changed from: package-private */
        public Class[] getParamTypes() {
            return this.callableMemberDesc.getParamTypes();
        }

        /* access modifiers changed from: package-private */
        public String getName() {
            return this.callableMemberDesc.getName();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:38:0x008a, code lost:
            if (r3.isAssignableFrom(r5) != false) goto L_0x0096;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void convertArgsToReflectionCompatible(freemarker.ext.beans.BeansWrapper r8, java.lang.Object[] r9) throws freemarker.template.TemplateModelException {
            /*
                r7 = this;
                freemarker.ext.beans.ReflectionCallableMemberDescriptor r0 = r7.callableMemberDesc
                java.lang.Class[] r0 = r0.getParamTypes()
                int r1 = r0.length
                r2 = 0
            L_0x0008:
                if (r2 >= r1) goto L_0x00a7
                r3 = r0[r2]
                r4 = r9[r2]
                if (r4 != 0) goto L_0x0012
                goto L_0x00a3
            L_0x0012:
                boolean r5 = r3.isArray()
                if (r5 == 0) goto L_0x0026
                boolean r5 = r4 instanceof java.util.List
                if (r5 == 0) goto L_0x0026
                r5 = r4
                java.util.List r5 = (java.util.List) r5
                r6 = 0
                java.lang.Object r5 = r8.listToArray(r5, r3, r6)
                r9[r2] = r5
            L_0x0026:
                java.lang.Class r5 = r4.getClass()
                boolean r5 = r5.isArray()
                if (r5 == 0) goto L_0x004b
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
                if (r5 != 0) goto L_0x003d
                java.lang.String r5 = "java.util.List"
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$(r5)
                freemarker.ext.beans.ArgumentTypes.class$java$util$List = r5
                goto L_0x003f
            L_0x003d:
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$util$List
            L_0x003f:
                boolean r5 = r3.isAssignableFrom(r5)
                if (r5 == 0) goto L_0x004b
                java.util.List r5 = r8.arrayToList(r4)
                r9[r2] = r5
            L_0x004b:
                boolean r5 = r4 instanceof freemarker.ext.beans.CharacterOrString
                if (r5 == 0) goto L_0x00a3
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Character
                java.lang.String r6 = "java.lang.Character"
                if (r5 != 0) goto L_0x005c
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$(r6)
                freemarker.ext.beans.ArgumentTypes.class$java$lang$Character = r5
                goto L_0x005e
            L_0x005c:
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Character
            L_0x005e:
                if (r3 == r5) goto L_0x0096
                java.lang.Class r5 = java.lang.Character.TYPE
                if (r3 == r5) goto L_0x0096
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$lang$String
                if (r5 != 0) goto L_0x0071
                java.lang.String r5 = "java.lang.String"
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$(r5)
                freemarker.ext.beans.ArgumentTypes.class$java$lang$String = r5
                goto L_0x0073
            L_0x0071:
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$lang$String
            L_0x0073:
                boolean r5 = r3.isAssignableFrom(r5)
                if (r5 != 0) goto L_0x008d
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Character
                if (r5 != 0) goto L_0x0084
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$(r6)
                freemarker.ext.beans.ArgumentTypes.class$java$lang$Character = r5
                goto L_0x0086
            L_0x0084:
                java.lang.Class r5 = freemarker.ext.beans.ArgumentTypes.class$java$lang$Character
            L_0x0086:
                boolean r3 = r3.isAssignableFrom(r5)
                if (r3 == 0) goto L_0x008d
                goto L_0x0096
            L_0x008d:
                freemarker.ext.beans.CharacterOrString r4 = (freemarker.ext.beans.CharacterOrString) r4
                java.lang.String r3 = r4.getAsString()
                r9[r2] = r3
                goto L_0x00a3
            L_0x0096:
                java.lang.Character r3 = new java.lang.Character
                freemarker.ext.beans.CharacterOrString r4 = (freemarker.ext.beans.CharacterOrString) r4
                char r4 = r4.getAsChar()
                r3.<init>(r4)
                r9[r2] = r3
            L_0x00a3:
                int r2 = r2 + 1
                goto L_0x0008
            L_0x00a7:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.ArgumentTypes.SpecialConversionCallableMemberDescriptor.convertArgsToReflectionCompatible(freemarker.ext.beans.BeansWrapper, java.lang.Object[]):void");
        }
    }
}
