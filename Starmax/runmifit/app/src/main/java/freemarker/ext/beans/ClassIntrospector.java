package freemarker.ext.beans;

import freemarker.core.BugException;
import freemarker.core._ConcurrentMapFactory;
import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.util.ModelCache;
import freemarker.log.Logger;
import freemarker.template.utility.NullArgumentException;
import freemarker.template.utility.SecurityUtilities;
import java.beans.BeanInfo;
import java.beans.IndexedPropertyDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.MethodDescriptor;
import java.beans.PropertyDescriptor;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

class ClassIntrospector {
    private static final Object ARGTYPES_KEY = new Object();
    private static final ClassChangeNotifier CLASS_CHANGE_NOTIFIER;
    static final Object CONSTRUCTORS_KEY = new Object();
    static final boolean DEVELOPMENT_MODE = "true".equals(SecurityUtilities.getSystemProperty("freemarker.development", "false"));
    static final Object GENERIC_GET_KEY = new Object();
    private static final String JREBEL_INTEGRATION_ERROR_MSG = "Error initializing JRebel integration. JRebel integration disabled.";
    private static final String JREBEL_SDK_CLASS_NAME = "org.zeroturnaround.javarebel.ClassEventListener";
    private static final Logger LOG = Logger.getLogger("freemarker.beans");
    static /* synthetic */ Class class$java$lang$Object;
    static /* synthetic */ Class class$java$lang$String;
    final boolean bugfixed;
    private final Map cache;
    private final Set cacheClassNames;
    private final Set classIntrospectionsInProgress;
    private int clearingCounter;
    final boolean exposeFields;
    final int exposureLevel;
    private final boolean hasSharedInstanceRestrictons;
    private final boolean isCacheConcurrentMap;
    final MethodAppearanceFineTuner methodAppearanceFineTuner;
    final MethodSorter methodSorter;
    private final List modelFactories;
    private final ReferenceQueue modelFactoriesRefQueue;
    private final boolean shared;
    private final Object sharedLock;

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030  */
    static {
        /*
            java.lang.String r0 = "Error initializing JRebel integration. JRebel integration disabled."
            java.lang.String r1 = "freemarker.beans"
            freemarker.log.Logger r1 = freemarker.log.Logger.getLogger(r1)
            freemarker.ext.beans.ClassIntrospector.LOG = r1
            java.lang.String r1 = "freemarker.development"
            java.lang.String r2 = "false"
            java.lang.String r1 = freemarker.template.utility.SecurityUtilities.getSystemProperty(r1, r2)
            java.lang.String r2 = "true"
            boolean r1 = r2.equals(r1)
            freemarker.ext.beans.ClassIntrospector.DEVELOPMENT_MODE = r1
            java.lang.String r1 = "org.zeroturnaround.javarebel.ClassEventListener"
            java.lang.Class.forName(r1)     // Catch:{ all -> 0x0021 }
            r1 = 1
            goto L_0x002d
        L_0x0021:
            r1 = move-exception
            r2 = 0
            boolean r3 = r1 instanceof java.lang.ClassNotFoundException     // Catch:{ all -> 0x002c }
            if (r3 != 0) goto L_0x002c
            freemarker.log.Logger r3 = freemarker.ext.beans.ClassIntrospector.LOG     // Catch:{ all -> 0x002c }
            r3.error(r0, r1)     // Catch:{ all -> 0x002c }
        L_0x002c:
            r1 = 0
        L_0x002d:
            r2 = 0
            if (r1 == 0) goto L_0x0044
            java.lang.String r1 = "freemarker.ext.beans.JRebelClassChangeNotifier"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ all -> 0x003e }
            java.lang.Object r1 = r1.newInstance()     // Catch:{ all -> 0x003e }
            freemarker.ext.beans.ClassChangeNotifier r1 = (freemarker.ext.beans.ClassChangeNotifier) r1     // Catch:{ all -> 0x003e }
            r2 = r1
            goto L_0x0044
        L_0x003e:
            r1 = move-exception
            freemarker.log.Logger r3 = freemarker.ext.beans.ClassIntrospector.LOG     // Catch:{ all -> 0x0044 }
            r3.error(r0, r1)     // Catch:{ all -> 0x0044 }
        L_0x0044:
            freemarker.ext.beans.ClassIntrospector.CLASS_CHANGE_NOTIFIER = r2
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            freemarker.ext.beans.ClassIntrospector.ARGTYPES_KEY = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            freemarker.ext.beans.ClassIntrospector.CONSTRUCTORS_KEY = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            freemarker.ext.beans.ClassIntrospector.GENERIC_GET_KEY = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.ClassIntrospector.<clinit>():void");
    }

    ClassIntrospector(ClassIntrospectorBuilder classIntrospectorBuilder, Object obj) {
        this(classIntrospectorBuilder, obj, false, false);
    }

    ClassIntrospector(ClassIntrospectorBuilder classIntrospectorBuilder, Object obj, boolean z, boolean z2) {
        this.cache = _ConcurrentMapFactory.newMaybeConcurrentHashMap(0, 0.75f, 16);
        this.isCacheConcurrentMap = _ConcurrentMapFactory.isConcurrent(this.cache);
        this.cacheClassNames = new HashSet(0);
        this.classIntrospectionsInProgress = new HashSet(0);
        this.modelFactories = new LinkedList();
        this.modelFactoriesRefQueue = new ReferenceQueue();
        NullArgumentException.check("sharedLock", obj);
        this.exposureLevel = classIntrospectorBuilder.getExposureLevel();
        this.exposeFields = classIntrospectorBuilder.getExposeFields();
        this.methodAppearanceFineTuner = classIntrospectorBuilder.getMethodAppearanceFineTuner();
        this.methodSorter = classIntrospectorBuilder.getMethodSorter();
        this.bugfixed = classIntrospectorBuilder.isBugfixed();
        this.sharedLock = obj;
        this.hasSharedInstanceRestrictons = z;
        this.shared = z2;
        ClassChangeNotifier classChangeNotifier = CLASS_CHANGE_NOTIFIER;
        if (classChangeNotifier != null) {
            classChangeNotifier.subscribe(this);
        }
    }

    /* access modifiers changed from: package-private */
    public ClassIntrospectorBuilder getPropertyAssignments() {
        return new ClassIntrospectorBuilder(this);
    }

    /* access modifiers changed from: package-private */
    public Map get(Class cls) {
        Map map;
        if (this.isCacheConcurrentMap && (map = (Map) this.cache.get(cls)) != null) {
            return map;
        }
        synchronized (this.sharedLock) {
            Map map2 = (Map) this.cache.get(cls);
            if (map2 != null) {
                return map2;
            }
            String name = cls.getName();
            if (this.cacheClassNames.contains(name)) {
                onSameNameClassesDetected(name);
            }
            while (map2 == null && this.classIntrospectionsInProgress.contains(cls)) {
                try {
                    this.sharedLock.wait();
                    map2 = (Map) this.cache.get(cls);
                } catch (InterruptedException e) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Class inrospection data lookup aborded: ");
                    stringBuffer.append(e);
                    throw new RuntimeException(stringBuffer.toString());
                }
            }
            if (map2 != null) {
                return map2;
            }
            this.classIntrospectionsInProgress.add(cls);
            try {
                Map createClassIntrospectionData = createClassIntrospectionData(cls);
                synchronized (this.sharedLock) {
                    this.cache.put(cls, createClassIntrospectionData);
                    this.cacheClassNames.add(name);
                }
                synchronized (this.sharedLock) {
                    this.classIntrospectionsInProgress.remove(cls);
                    this.sharedLock.notifyAll();
                }
                return createClassIntrospectionData;
            } catch (Throwable th) {
                synchronized (this.sharedLock) {
                    this.classIntrospectionsInProgress.remove(cls);
                    this.sharedLock.notifyAll();
                    throw th;
                }
            }
        }
    }

    private Map createClassIntrospectionData(Class cls) {
        HashMap hashMap = new HashMap();
        if (this.exposeFields) {
            addFieldsToClassIntrospectionData(hashMap, cls);
        }
        Map discoverAccessibleMethods = discoverAccessibleMethods(cls);
        addGenericGetToClassIntrospectionData(hashMap, discoverAccessibleMethods);
        if (this.exposureLevel != 3) {
            try {
                addBeanInfoToClassIntrospectionData(hashMap, cls, discoverAccessibleMethods);
            } catch (IntrospectionException e) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Couldn't properly perform introspection for class ");
                stringBuffer.append(cls);
                logger.warn(stringBuffer.toString(), e);
                hashMap.clear();
            }
        }
        addConstructorsToClassIntrospectionData(hashMap, cls);
        if (hashMap.size() > 1) {
            return hashMap;
        }
        if (hashMap.size() == 0) {
            return Collections.EMPTY_MAP;
        }
        Map.Entry entry = (Map.Entry) hashMap.entrySet().iterator().next();
        return Collections.singletonMap(entry.getKey(), entry.getValue());
    }

    private void addFieldsToClassIntrospectionData(Map map, Class cls) throws SecurityException {
        Field[] fields = cls.getFields();
        for (Field field : fields) {
            if ((field.getModifiers() & 8) == 0) {
                map.put(field.getName(), field);
            }
        }
    }

    private void addBeanInfoToClassIntrospectionData(Map map, Class cls, Map map2) throws IntrospectionException {
        BeanInfo beanInfo = Introspector.getBeanInfo(cls);
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        if (propertyDescriptors != null) {
            for (int length = propertyDescriptors.length - 1; length >= 0; length--) {
                addPropertyDescriptorToClassIntrospectionData(map, propertyDescriptors[length], cls, map2);
            }
        }
        if (this.exposureLevel < 2) {
            BeansWrapper.MethodAppearanceDecision methodAppearanceDecision = new BeansWrapper.MethodAppearanceDecision();
            BeansWrapper.MethodAppearanceDecisionInput methodAppearanceDecisionInput = null;
            MethodDescriptor[] sortMethodDescriptors = sortMethodDescriptors(beanInfo.getMethodDescriptors());
            if (sortMethodDescriptors != null) {
                for (int length2 = sortMethodDescriptors.length - 1; length2 >= 0; length2--) {
                    Method matchingAccessibleMethod = getMatchingAccessibleMethod(sortMethodDescriptors[length2].getMethod(), map2);
                    if (matchingAccessibleMethod != null && isAllowedToExpose(matchingAccessibleMethod)) {
                        methodAppearanceDecision.setDefaults(matchingAccessibleMethod);
                        if (this.methodAppearanceFineTuner != null) {
                            if (methodAppearanceDecisionInput == null) {
                                methodAppearanceDecisionInput = new BeansWrapper.MethodAppearanceDecisionInput();
                            }
                            methodAppearanceDecisionInput.setContainingClass(cls);
                            methodAppearanceDecisionInput.setMethod(matchingAccessibleMethod);
                            this.methodAppearanceFineTuner.process(methodAppearanceDecisionInput, methodAppearanceDecision);
                        }
                        PropertyDescriptor exposeAsProperty = methodAppearanceDecision.getExposeAsProperty();
                        if (exposeAsProperty != null && !(map.get(exposeAsProperty.getName()) instanceof PropertyDescriptor)) {
                            addPropertyDescriptorToClassIntrospectionData(map, exposeAsProperty, cls, map2);
                        }
                        String exposeMethodAs = methodAppearanceDecision.getExposeMethodAs();
                        if (exposeMethodAs != null) {
                            Object obj = map.get(exposeMethodAs);
                            if (obj instanceof Method) {
                                OverloadedMethods overloadedMethods = new OverloadedMethods(this.bugfixed);
                                overloadedMethods.addMethod((Method) obj);
                                overloadedMethods.addMethod(matchingAccessibleMethod);
                                map.put(exposeMethodAs, overloadedMethods);
                                getArgTypes(map).remove(obj);
                            } else if (obj instanceof OverloadedMethods) {
                                ((OverloadedMethods) obj).addMethod(matchingAccessibleMethod);
                            } else if (methodAppearanceDecision.getMethodShadowsProperty() || !(obj instanceof PropertyDescriptor)) {
                                map.put(exposeMethodAs, matchingAccessibleMethod);
                                getArgTypes(map).put(matchingAccessibleMethod, matchingAccessibleMethod.getParameterTypes());
                            }
                        }
                    }
                }
            }
        }
    }

    private void addPropertyDescriptorToClassIntrospectionData(Map map, PropertyDescriptor propertyDescriptor, Class cls, Map map2) {
        if (propertyDescriptor instanceof IndexedPropertyDescriptor) {
            IndexedPropertyDescriptor indexedPropertyDescriptor = (IndexedPropertyDescriptor) propertyDescriptor;
            Method indexedReadMethod = indexedPropertyDescriptor.getIndexedReadMethod();
            Method matchingAccessibleMethod = getMatchingAccessibleMethod(indexedReadMethod, map2);
            if (matchingAccessibleMethod != null && isAllowedToExpose(matchingAccessibleMethod)) {
                if (indexedReadMethod != matchingAccessibleMethod) {
                    try {
                        indexedPropertyDescriptor = new IndexedPropertyDescriptor(indexedPropertyDescriptor.getName(), indexedPropertyDescriptor.getReadMethod(), (Method) null, matchingAccessibleMethod, (Method) null);
                    } catch (IntrospectionException e) {
                        Logger logger = LOG;
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("Failed creating a publicly-accessible property descriptor for ");
                        stringBuffer.append(cls.getName());
                        stringBuffer.append(" indexed property ");
                        stringBuffer.append(propertyDescriptor.getName());
                        stringBuffer.append(", read method ");
                        stringBuffer.append(matchingAccessibleMethod);
                        logger.warn(stringBuffer.toString(), e);
                        return;
                    }
                }
                map.put(indexedPropertyDescriptor.getName(), indexedPropertyDescriptor);
                getArgTypes(map).put(matchingAccessibleMethod, matchingAccessibleMethod.getParameterTypes());
                return;
            }
            return;
        }
        Method readMethod = propertyDescriptor.getReadMethod();
        Method matchingAccessibleMethod2 = getMatchingAccessibleMethod(readMethod, map2);
        if (matchingAccessibleMethod2 != null && isAllowedToExpose(matchingAccessibleMethod2)) {
            if (readMethod != matchingAccessibleMethod2) {
                try {
                    PropertyDescriptor propertyDescriptor2 = new PropertyDescriptor(propertyDescriptor.getName(), matchingAccessibleMethod2, (Method) null);
                    try {
                        propertyDescriptor2.setReadMethod(matchingAccessibleMethod2);
                        propertyDescriptor = propertyDescriptor2;
                    } catch (IntrospectionException e2) {
                        e = e2;
                        propertyDescriptor = propertyDescriptor2;
                        Logger logger2 = LOG;
                        StringBuffer stringBuffer2 = new StringBuffer();
                        stringBuffer2.append("Failed creating a publicly-accessible property descriptor for ");
                        stringBuffer2.append(cls.getName());
                        stringBuffer2.append(" property ");
                        stringBuffer2.append(propertyDescriptor.getName());
                        stringBuffer2.append(", read method ");
                        stringBuffer2.append(matchingAccessibleMethod2);
                        logger2.warn(stringBuffer2.toString(), e);
                        return;
                    }
                } catch (IntrospectionException e3) {
                    e = e3;
                }
            }
            map.put(propertyDescriptor.getName(), propertyDescriptor);
        }
    }

    private void addGenericGetToClassIntrospectionData(Map map, Map map2) {
        Method firstAccessibleMethod = getFirstAccessibleMethod(MethodSignature.GET_STRING_SIGNATURE, map2);
        if (firstAccessibleMethod == null) {
            firstAccessibleMethod = getFirstAccessibleMethod(MethodSignature.GET_OBJECT_SIGNATURE, map2);
        }
        if (firstAccessibleMethod != null) {
            map.put(GENERIC_GET_KEY, firstAccessibleMethod);
        }
    }

    private void addConstructorsToClassIntrospectionData(Map map, Class cls) {
        try {
            Constructor<?>[] constructors = cls.getConstructors();
            if (constructors.length == 1) {
                Constructor<?> constructor = constructors[0];
                map.put(CONSTRUCTORS_KEY, new SimpleMethod(constructor, constructor.getParameterTypes()));
            } else if (constructors.length > 1) {
                OverloadedMethods overloadedMethods = new OverloadedMethods(this.bugfixed);
                for (Constructor<?> constructor2 : constructors) {
                    overloadedMethods.addConstructor(constructor2);
                }
                map.put(CONSTRUCTORS_KEY, overloadedMethods);
            }
        } catch (SecurityException e) {
            Logger logger = LOG;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Can't discover constructors for class ");
            stringBuffer.append(cls.getName());
            logger.warn(stringBuffer.toString(), e);
        }
    }

    private static Map discoverAccessibleMethods(Class cls) {
        HashMap hashMap = new HashMap();
        discoverAccessibleMethods(cls, hashMap);
        return hashMap;
    }

    private static void discoverAccessibleMethods(Class cls, Map map) {
        if (Modifier.isPublic(cls.getModifiers())) {
            try {
                Method[] methods = cls.getMethods();
                for (Method method : methods) {
                    MethodSignature methodSignature = new MethodSignature(method);
                    Object obj = (List) map.get(methodSignature);
                    if (obj == null) {
                        obj = new LinkedList();
                        map.put(methodSignature, obj);
                    }
                    obj.add(method);
                }
                return;
            } catch (SecurityException e) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Could not discover accessible methods of class ");
                stringBuffer.append(cls.getName());
                stringBuffer.append(", attemping superclasses/interfaces.");
                logger.warn(stringBuffer.toString(), e);
            }
        }
        for (Class<?> cls2 : cls.getInterfaces()) {
            discoverAccessibleMethods(cls2, map);
        }
        Class superclass = cls.getSuperclass();
        if (superclass != null) {
            discoverAccessibleMethods(superclass, map);
        }
    }

    private static Method getMatchingAccessibleMethod(Method method, Map map) {
        List<Method> list;
        if (method == null || (list = (List) map.get(new MethodSignature(method))) == null) {
            return null;
        }
        for (Method method2 : list) {
            if (method2.getReturnType() == method.getReturnType()) {
                return method2;
            }
        }
        return null;
    }

    private static Method getFirstAccessibleMethod(MethodSignature methodSignature, Map map) {
        List list = (List) map.get(methodSignature);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (Method) list.iterator().next();
    }

    private MethodDescriptor[] sortMethodDescriptors(MethodDescriptor[] methodDescriptorArr) {
        MethodSorter methodSorter2 = this.methodSorter;
        return methodSorter2 != null ? methodSorter2.sortMethodDescriptors(methodDescriptorArr) : methodDescriptorArr;
    }

    /* access modifiers changed from: package-private */
    public boolean isAllowedToExpose(Method method) {
        return this.exposureLevel < 1 || !UnsafeMethods.isUnsafeMethod(method);
    }

    private static Map getArgTypes(Map map) {
        Map map2 = (Map) map.get(ARGTYPES_KEY);
        if (map2 != null) {
            return map2;
        }
        HashMap hashMap = new HashMap();
        map.put(ARGTYPES_KEY, hashMap);
        return hashMap;
    }

    private static final class MethodSignature {
        /* access modifiers changed from: private */
        public static final MethodSignature GET_OBJECT_SIGNATURE;
        /* access modifiers changed from: private */
        public static final MethodSignature GET_STRING_SIGNATURE;
        private final Class[] args;
        private final String name;

        static {
            Class cls;
            Class cls2;
            Class[] clsArr = new Class[1];
            if (ClassIntrospector.class$java$lang$String == null) {
                cls = ClassIntrospector.class$("java.lang.String");
                ClassIntrospector.class$java$lang$String = cls;
            } else {
                cls = ClassIntrospector.class$java$lang$String;
            }
            clsArr[0] = cls;
            GET_STRING_SIGNATURE = new MethodSignature("get", clsArr);
            Class[] clsArr2 = new Class[1];
            if (ClassIntrospector.class$java$lang$Object == null) {
                cls2 = ClassIntrospector.class$("java.lang.Object");
                ClassIntrospector.class$java$lang$Object = cls2;
            } else {
                cls2 = ClassIntrospector.class$java$lang$Object;
            }
            clsArr2[0] = cls2;
            GET_OBJECT_SIGNATURE = new MethodSignature("get", clsArr2);
        }

        private MethodSignature(String str, Class[] clsArr) {
            this.name = str;
            this.args = clsArr;
        }

        MethodSignature(Method method) {
            this(method.getName(), method.getParameterTypes());
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof MethodSignature)) {
                return false;
            }
            MethodSignature methodSignature = (MethodSignature) obj;
            if (!methodSignature.name.equals(this.name) || !Arrays.equals(this.args, methodSignature.args)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return this.name.hashCode() ^ this.args.length;
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void clearCache() {
        if (!getHasSharedInstanceRestrictons()) {
            forcedClearCache();
            return;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("It's not allowed to clear the whole cache in a read-only ");
        stringBuffer.append(getClass().getName());
        stringBuffer.append("instance. Use removeFromClassIntrospectionCache(String prefix) instead.");
        throw new IllegalStateException(stringBuffer.toString());
    }

    private void forcedClearCache() {
        synchronized (this.sharedLock) {
            this.cache.clear();
            this.cacheClassNames.clear();
            this.clearingCounter++;
            for (WeakReference weakReference : this.modelFactories) {
                Object obj = weakReference.get();
                if (obj != null) {
                    if (obj instanceof ClassBasedModelFactory) {
                        ((ClassBasedModelFactory) obj).clearCache();
                    } else if (obj instanceof ModelCache) {
                        ((ModelCache) obj).clearCache();
                    } else {
                        throw new BugException();
                    }
                }
            }
            removeClearedModelFactoryReferences();
        }
    }

    /* access modifiers changed from: package-private */
    public void remove(Class cls) {
        synchronized (this.sharedLock) {
            this.cache.remove(cls);
            this.cacheClassNames.remove(cls.getName());
            this.clearingCounter++;
            for (WeakReference weakReference : this.modelFactories) {
                Object obj = weakReference.get();
                if (obj != null) {
                    if (obj instanceof ClassBasedModelFactory) {
                        ((ClassBasedModelFactory) obj).removeFromCache(cls);
                    } else if (obj instanceof ModelCache) {
                        ((ModelCache) obj).clearCache();
                    } else {
                        throw new BugException();
                    }
                }
            }
            removeClearedModelFactoryReferences();
        }
    }

    /* access modifiers changed from: package-private */
    public int getClearingCounter() {
        int i;
        synchronized (this.sharedLock) {
            i = this.clearingCounter;
        }
        return i;
    }

    private void onSameNameClassesDetected(String str) {
        if (LOG.isInfoEnabled()) {
            Logger logger = LOG;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Detected multiple classes with the same name, \"");
            stringBuffer.append(str);
            stringBuffer.append("\". Assuming it was a class-reloading. Clearing class introspection ");
            stringBuffer.append("caches to release old data.");
            logger.info(stringBuffer.toString());
        }
        forcedClearCache();
    }

    /* access modifiers changed from: package-private */
    public void registerModelFactory(ClassBasedModelFactory classBasedModelFactory) {
        registerModelFactory((Object) classBasedModelFactory);
    }

    /* access modifiers changed from: package-private */
    public void registerModelFactory(ModelCache modelCache) {
        registerModelFactory((Object) modelCache);
    }

    private void registerModelFactory(Object obj) {
        synchronized (this.sharedLock) {
            this.modelFactories.add(new WeakReference(obj, this.modelFactoriesRefQueue));
            removeClearedModelFactoryReferences();
        }
    }

    /* access modifiers changed from: package-private */
    public void unregisterModelFactory(ClassBasedModelFactory classBasedModelFactory) {
        unregisterModelFactory((Object) classBasedModelFactory);
    }

    /* access modifiers changed from: package-private */
    public void unregisterModelFactory(ModelCache modelCache) {
        unregisterModelFactory((Object) modelCache);
    }

    /* access modifiers changed from: package-private */
    public void unregisterModelFactory(Object obj) {
        synchronized (this.sharedLock) {
            Iterator it = this.modelFactories.iterator();
            while (it.hasNext()) {
                if (((Reference) it.next()).get() == obj) {
                    it.remove();
                }
            }
        }
    }

    private void removeClearedModelFactoryReferences() {
        while (true) {
            Reference poll = this.modelFactoriesRefQueue.poll();
            if (poll != null) {
                synchronized (this.sharedLock) {
                    Iterator it = this.modelFactories.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (it.next() == poll) {
                                it.remove();
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
            } else {
                return;
            }
        }
    }

    static Class[] getArgTypes(Map map, AccessibleObject accessibleObject) {
        return (Class[]) ((Map) map.get(ARGTYPES_KEY)).get(accessibleObject);
    }

    /* access modifiers changed from: package-private */
    public int keyCount(Class cls) {
        Map map = get(cls);
        int size = map.size();
        if (map.containsKey(CONSTRUCTORS_KEY)) {
            size--;
        }
        if (map.containsKey(GENERIC_GET_KEY)) {
            size--;
        }
        return map.containsKey(ARGTYPES_KEY) ? size - 1 : size;
    }

    /* access modifiers changed from: package-private */
    public Set keySet(Class cls) {
        HashSet hashSet = new HashSet(get(cls).keySet());
        hashSet.remove(CONSTRUCTORS_KEY);
        hashSet.remove(GENERIC_GET_KEY);
        hashSet.remove(ARGTYPES_KEY);
        return hashSet;
    }

    /* access modifiers changed from: package-private */
    public int getExposureLevel() {
        return this.exposureLevel;
    }

    /* access modifiers changed from: package-private */
    public boolean getExposeFields() {
        return this.exposeFields;
    }

    /* access modifiers changed from: package-private */
    public MethodAppearanceFineTuner getMethodAppearanceFineTuner() {
        return this.methodAppearanceFineTuner;
    }

    /* access modifiers changed from: package-private */
    public MethodSorter getMethodSorter() {
        return this.methodSorter;
    }

    /* access modifiers changed from: package-private */
    public boolean getHasSharedInstanceRestrictons() {
        return this.hasSharedInstanceRestrictons;
    }

    /* access modifiers changed from: package-private */
    public boolean isShared() {
        return this.shared;
    }

    /* access modifiers changed from: package-private */
    public Object getSharedLock() {
        return this.sharedLock;
    }

    /* access modifiers changed from: package-private */
    public Object[] getRegisteredModelFactoriesSnapshot() {
        Object[] array;
        synchronized (this.sharedLock) {
            array = this.modelFactories.toArray();
        }
        return array;
    }
}
