package freemarker.ext.beans;

import freemarker.core._DelayedFTLTypeDescription;
import freemarker.core._DelayedShortClassName;
import freemarker.core._TemplateModelException;
import freemarker.ext.beans.OverloadedNumberUtil;
import freemarker.ext.util.IdentityHashMap;
import freemarker.ext.util.ModelCache;
import freemarker.ext.util.ModelFactory;
import freemarker.log.Logger;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.ObjectWrapperAndUnwrapper;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.Version;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.RichObjectWrapper;
import freemarker.template.utility.UndeclaredThrowableException;
import freemarker.template.utility.WriteProtectable;
import java.beans.PropertyDescriptor;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BeansWrapper implements RichObjectWrapper, WriteProtectable {
    static final Object CAN_NOT_UNWRAP = ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS;
    private static final ModelFactory ENUMERATION_FACTORY = new ModelFactory() {
        /* class freemarker.ext.beans.BeansWrapper.C33215 */

        public TemplateModel create(Object obj, ObjectWrapper objectWrapper) {
            return new EnumerationModel((Enumeration) obj, (BeansWrapper) objectWrapper);
        }
    };
    private static final Constructor ENUMS_MODEL_CTOR = enumsModelCtor();
    public static final int EXPOSE_ALL = 0;
    public static final int EXPOSE_NOTHING = 3;
    public static final int EXPOSE_PROPERTIES_ONLY = 2;
    public static final int EXPOSE_SAFE = 1;
    private static final Class ITERABLE_CLASS;
    private static final ModelFactory ITERATOR_FACTORY = new ModelFactory() {
        /* class freemarker.ext.beans.BeansWrapper.C33204 */

        public TemplateModel create(Object obj, ObjectWrapper objectWrapper) {
            return new IteratorModel((Iterator) obj, (BeansWrapper) objectWrapper);
        }
    };
    private static final Logger LOG = Logger.getLogger("freemarker.beans");
    static /* synthetic */ Class class$freemarker$ext$beans$BeansWrapper;
    static /* synthetic */ Class class$freemarker$ext$beans$BeansWrapper$MethodAppearanceDecision;
    static /* synthetic */ Class class$freemarker$ext$beans$HashAdapter;
    static /* synthetic */ Class class$freemarker$ext$beans$SequenceAdapter;
    static /* synthetic */ Class class$freemarker$ext$beans$SetAdapter;
    static /* synthetic */ Class class$freemarker$template$DefaultObjectWrapper;
    static /* synthetic */ Class class$freemarker$template$SimpleObjectWrapper;
    static /* synthetic */ Class class$java$lang$Boolean;
    static /* synthetic */ Class class$java$lang$Byte;
    static /* synthetic */ Class class$java$lang$Character;
    static /* synthetic */ Class class$java$lang$Class;
    static /* synthetic */ Class class$java$lang$Double;
    static /* synthetic */ Class class$java$lang$Float;
    static /* synthetic */ Class class$java$lang$Integer;
    static /* synthetic */ Class class$java$lang$Long;
    static /* synthetic */ Class class$java$lang$Number;
    static /* synthetic */ Class class$java$lang$Object;
    static /* synthetic */ Class class$java$lang$Short;
    static /* synthetic */ Class class$java$lang$String;
    static /* synthetic */ Class class$java$lang$reflect$Method;
    static /* synthetic */ Class class$java$math$BigDecimal;
    static /* synthetic */ Class class$java$math$BigInteger;
    static /* synthetic */ Class class$java$util$Collection;
    static /* synthetic */ Class class$java$util$Date;
    static /* synthetic */ Class class$java$util$Enumeration;
    static /* synthetic */ Class class$java$util$Iterator;
    static /* synthetic */ Class class$java$util$List;
    static /* synthetic */ Class class$java$util$Map;
    static /* synthetic */ Class class$java$util$ResourceBundle;
    static /* synthetic */ Class class$java$util$Set;
    private static volatile boolean ftmaDeprecationWarnLogged;
    private final ModelFactory BOOLEAN_FACTORY;
    private ClassIntrospector classIntrospector;
    private int defaultDateType;
    private final ClassBasedModelFactory enumModels;
    /* access modifiers changed from: private */
    public final BooleanModel falseModel;
    private final Version incompatibleImprovements;
    private boolean methodsShadowItems;
    private final ModelCache modelCache;
    private TemplateModel nullModel;
    private ObjectWrapper outerIdentity;
    private final Object sharedIntrospectionLock;
    private boolean simpleMapWrapper;
    private final StaticModels staticModels;
    private boolean strict;
    /* access modifiers changed from: private */
    public final BooleanModel trueModel;
    private volatile boolean writeProtected;

    /* access modifiers changed from: protected */
    public void finetuneMethodAppearance(Class cls, Method method, MethodAppearanceDecision methodAppearanceDecision) {
    }

    static {
        Class<?> cls;
        try {
            cls = Class.forName("java.lang.Iterable");
        } catch (ClassNotFoundException unused) {
            cls = null;
        }
        ITERABLE_CLASS = cls;
    }

    public BeansWrapper() {
        this(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
    }

    public BeansWrapper(Version version) {
        this(new BeansWrapperConfiguration(version) {
            /* class freemarker.ext.beans.BeansWrapper.C33171 */
        }, false);
    }

    protected BeansWrapper(BeansWrapperConfiguration beansWrapperConfiguration, boolean z) {
        this(beansWrapperConfiguration, z, true);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:37|38|67) */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r2 = r2.getSuperclass();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0092 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected BeansWrapper(freemarker.ext.beans.BeansWrapperConfiguration r9, boolean r10, boolean r11) {
        /*
            r8 = this;
            r8.<init>()
            r11 = 0
            r8.nullModel = r11
            r8.outerIdentity = r8
            r11 = 1
            r8.methodsShadowItems = r11
            freemarker.ext.beans.BeansWrapper$3 r0 = new freemarker.ext.beans.BeansWrapper$3
            r0.<init>()
            r8.BOOLEAN_FACTORY = r0
            freemarker.ext.beans.MethodAppearanceFineTuner r0 = r9.getMethodAppearanceFineTuner()
            if (r0 != 0) goto L_0x0106
            java.lang.Class r0 = r8.getClass()
            r1 = 0
            r2 = r0
            r0 = 0
        L_0x001f:
            java.lang.String r3 = "freemarker.ext.beans.BeansWrapper"
            if (r0 != 0) goto L_0x00bf
            java.lang.Class r4 = freemarker.ext.beans.BeansWrapper.class$freemarker$template$DefaultObjectWrapper     // Catch:{ all -> 0x0097 }
            if (r4 != 0) goto L_0x0030
            java.lang.String r4 = "freemarker.template.DefaultObjectWrapper"
            java.lang.Class r4 = class$(r4)     // Catch:{ all -> 0x0097 }
            freemarker.ext.beans.BeansWrapper.class$freemarker$template$DefaultObjectWrapper = r4     // Catch:{ all -> 0x0097 }
            goto L_0x0032
        L_0x0030:
            java.lang.Class r4 = freemarker.ext.beans.BeansWrapper.class$freemarker$template$DefaultObjectWrapper     // Catch:{ all -> 0x0097 }
        L_0x0032:
            if (r2 == r4) goto L_0x00bf
            java.lang.Class r4 = freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$BeansWrapper     // Catch:{ all -> 0x0097 }
            if (r4 != 0) goto L_0x003f
            java.lang.Class r4 = class$(r3)     // Catch:{ all -> 0x0097 }
            freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$BeansWrapper = r4     // Catch:{ all -> 0x0097 }
            goto L_0x0041
        L_0x003f:
            java.lang.Class r4 = freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$BeansWrapper     // Catch:{ all -> 0x0097 }
        L_0x0041:
            if (r2 == r4) goto L_0x00bf
            java.lang.Class r4 = freemarker.ext.beans.BeansWrapper.class$freemarker$template$SimpleObjectWrapper     // Catch:{ all -> 0x0097 }
            if (r4 != 0) goto L_0x0050
            java.lang.String r4 = "freemarker.template.SimpleObjectWrapper"
            java.lang.Class r4 = class$(r4)     // Catch:{ all -> 0x0097 }
            freemarker.ext.beans.BeansWrapper.class$freemarker$template$SimpleObjectWrapper = r4     // Catch:{ all -> 0x0097 }
            goto L_0x0052
        L_0x0050:
            java.lang.Class r4 = freemarker.ext.beans.BeansWrapper.class$freemarker$template$SimpleObjectWrapper     // Catch:{ all -> 0x0097 }
        L_0x0052:
            if (r2 == r4) goto L_0x00bf
            java.lang.String r4 = "finetuneMethodAppearance"
            r5 = 3
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ NoSuchMethodException -> 0x0092 }
            java.lang.Class r6 = freemarker.ext.beans.BeansWrapper.class$java$lang$Class     // Catch:{ NoSuchMethodException -> 0x0092 }
            if (r6 != 0) goto L_0x0066
            java.lang.String r6 = "java.lang.Class"
            java.lang.Class r6 = class$(r6)     // Catch:{ NoSuchMethodException -> 0x0092 }
            freemarker.ext.beans.BeansWrapper.class$java$lang$Class = r6     // Catch:{ NoSuchMethodException -> 0x0092 }
            goto L_0x0068
        L_0x0066:
            java.lang.Class r6 = freemarker.ext.beans.BeansWrapper.class$java$lang$Class     // Catch:{ NoSuchMethodException -> 0x0092 }
        L_0x0068:
            r5[r1] = r6     // Catch:{ NoSuchMethodException -> 0x0092 }
            java.lang.Class r6 = freemarker.ext.beans.BeansWrapper.class$java$lang$reflect$Method     // Catch:{ NoSuchMethodException -> 0x0092 }
            if (r6 != 0) goto L_0x0077
            java.lang.String r6 = "java.lang.reflect.Method"
            java.lang.Class r6 = class$(r6)     // Catch:{ NoSuchMethodException -> 0x0092 }
            freemarker.ext.beans.BeansWrapper.class$java$lang$reflect$Method = r6     // Catch:{ NoSuchMethodException -> 0x0092 }
            goto L_0x0079
        L_0x0077:
            java.lang.Class r6 = freemarker.ext.beans.BeansWrapper.class$java$lang$reflect$Method     // Catch:{ NoSuchMethodException -> 0x0092 }
        L_0x0079:
            r5[r11] = r6     // Catch:{ NoSuchMethodException -> 0x0092 }
            r6 = 2
            java.lang.Class r7 = freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$BeansWrapper$MethodAppearanceDecision     // Catch:{ NoSuchMethodException -> 0x0092 }
            if (r7 != 0) goto L_0x0089
            java.lang.String r7 = "freemarker.ext.beans.BeansWrapper$MethodAppearanceDecision"
            java.lang.Class r7 = class$(r7)     // Catch:{ NoSuchMethodException -> 0x0092 }
            freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$BeansWrapper$MethodAppearanceDecision = r7     // Catch:{ NoSuchMethodException -> 0x0092 }
            goto L_0x008b
        L_0x0089:
            java.lang.Class r7 = freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$BeansWrapper$MethodAppearanceDecision     // Catch:{ NoSuchMethodException -> 0x0092 }
        L_0x008b:
            r5[r6] = r7     // Catch:{ NoSuchMethodException -> 0x0092 }
            r2.getDeclaredMethod(r4, r5)     // Catch:{ NoSuchMethodException -> 0x0092 }
            r0 = 1
            goto L_0x001f
        L_0x0092:
            java.lang.Class r2 = r2.getSuperclass()     // Catch:{ all -> 0x0097 }
            goto L_0x001f
        L_0x0097:
            r0 = move-exception
            freemarker.log.Logger r4 = freemarker.ext.beans.BeansWrapper.LOG
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r6 = "Failed to check if finetuneMethodAppearance is overidden in "
            r5.append(r6)
            java.lang.String r2 = r2.getName()
            r5.append(r2)
            java.lang.String r2 = "; acting like if it was, but this way it won't utilize the shared class introspection "
            r5.append(r2)
            java.lang.String r2 = "cache."
            r5.append(r2)
            java.lang.String r2 = r5.toString()
            r4.info(r2, r0)
            r0 = 1
            r2 = 1
            goto L_0x00c0
        L_0x00bf:
            r2 = 0
        L_0x00c0:
            if (r0 == 0) goto L_0x0106
            if (r2 != 0) goto L_0x00f8
            boolean r0 = freemarker.ext.beans.BeansWrapper.ftmaDeprecationWarnLogged
            if (r0 != 0) goto L_0x00f8
            freemarker.log.Logger r0 = freemarker.ext.beans.BeansWrapper.LOG
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r4 = "Overriding "
            r2.append(r4)
            java.lang.Class r4 = freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$BeansWrapper
            if (r4 != 0) goto L_0x00de
            java.lang.Class r4 = class$(r3)
            freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$BeansWrapper = r4
        L_0x00de:
            java.lang.String r3 = r4.getName()
            r2.append(r3)
            java.lang.String r3 = ".finetuneMethodAppearance is deprecated "
            r2.append(r3)
            java.lang.String r3 = "and will be banned sometimes in the future. Use setMethodAppearanceFineTuner instead."
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.warn(r2)
            freemarker.ext.beans.BeansWrapper.ftmaDeprecationWarnLogged = r11
        L_0x00f8:
            java.lang.Object r9 = r9.clone(r1)
            freemarker.ext.beans.BeansWrapperConfiguration r9 = (freemarker.ext.beans.BeansWrapperConfiguration) r9
            freemarker.ext.beans.BeansWrapper$2 r11 = new freemarker.ext.beans.BeansWrapper$2
            r11.<init>()
            r9.setMethodAppearanceFineTuner(r11)
        L_0x0106:
            freemarker.template.Version r11 = r9.getIncompatibleImprovements()
            r8.incompatibleImprovements = r11
            boolean r11 = r9.isSimpleMapWrapper()
            r8.simpleMapWrapper = r11
            int r11 = r9.getDefaultDateType()
            r8.defaultDateType = r11
            freemarker.template.ObjectWrapper r11 = r9.getOuterIdentity()
            if (r11 == 0) goto L_0x0123
            freemarker.template.ObjectWrapper r11 = r9.getOuterIdentity()
            goto L_0x0124
        L_0x0123:
            r11 = r8
        L_0x0124:
            r8.outerIdentity = r11
            boolean r11 = r9.isStrict()
            r8.strict = r11
            if (r10 != 0) goto L_0x0141
            java.lang.Object r11 = new java.lang.Object
            r11.<init>()
            r8.sharedIntrospectionLock = r11
            freemarker.ext.beans.ClassIntrospector r11 = new freemarker.ext.beans.ClassIntrospector
            freemarker.ext.beans.ClassIntrospectorBuilder r0 = r9.classIntrospectorFactory
            java.lang.Object r1 = r8.sharedIntrospectionLock
            r11.<init>(r0, r1)
            r8.classIntrospector = r11
            goto L_0x0151
        L_0x0141:
            freemarker.ext.beans.ClassIntrospectorBuilder r11 = r9.classIntrospectorFactory
            freemarker.ext.beans.ClassIntrospector r11 = r11.build()
            r8.classIntrospector = r11
            freemarker.ext.beans.ClassIntrospector r11 = r8.classIntrospector
            java.lang.Object r11 = r11.getSharedLock()
            r8.sharedIntrospectionLock = r11
        L_0x0151:
            freemarker.ext.beans.BooleanModel r11 = new freemarker.ext.beans.BooleanModel
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r11.<init>(r0, r8)
            r8.falseModel = r11
            freemarker.ext.beans.BooleanModel r11 = new freemarker.ext.beans.BooleanModel
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            r11.<init>(r0, r8)
            r8.trueModel = r11
            freemarker.ext.beans.StaticModels r11 = new freemarker.ext.beans.StaticModels
            r11.<init>(r8)
            r8.staticModels = r11
            freemarker.ext.beans.ClassBasedModelFactory r11 = createEnumModels(r8)
            r8.enumModels = r11
            freemarker.ext.beans.BeansModelCache r11 = new freemarker.ext.beans.BeansModelCache
            r11.<init>(r8)
            r8.modelCache = r11
            boolean r9 = r9.getUseModelCache()
            r8.setUseCache(r9)
            r8.finalizeConstruction(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.BeansWrapper.<init>(freemarker.ext.beans.BeansWrapperConfiguration, boolean, boolean):void");
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* access modifiers changed from: protected */
    public void finalizeConstruction(boolean z) {
        if (z) {
            writeProtect();
        }
        registerModelFactories();
    }

    public void writeProtect() {
        this.writeProtected = true;
    }

    public boolean isWriteProtected() {
        return this.writeProtected;
    }

    /* access modifiers changed from: package-private */
    public Object getSharedIntrospectionLock() {
        return this.sharedIntrospectionLock;
    }

    /* access modifiers changed from: protected */
    public void checkModifiable() {
        if (this.writeProtected) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Can't modify the ");
            stringBuffer.append(getClass().getName());
            stringBuffer.append(" object, as it was write protected.");
            throw new IllegalStateException(stringBuffer.toString());
        }
    }

    public boolean isStrict() {
        return this.strict;
    }

    public void setStrict(boolean z) {
        checkModifiable();
        this.strict = z;
    }

    public void setOuterIdentity(ObjectWrapper objectWrapper) {
        checkModifiable();
        this.outerIdentity = objectWrapper;
    }

    public ObjectWrapper getOuterIdentity() {
        return this.outerIdentity;
    }

    public void setSimpleMapWrapper(boolean z) {
        checkModifiable();
        this.simpleMapWrapper = z;
    }

    public boolean isSimpleMapWrapper() {
        return this.simpleMapWrapper;
    }

    public void setExposureLevel(int i) {
        checkModifiable();
        if (this.classIntrospector.getExposureLevel() != i) {
            ClassIntrospectorBuilder propertyAssignments = this.classIntrospector.getPropertyAssignments();
            propertyAssignments.setExposureLevel(i);
            replaceClassIntrospector(propertyAssignments);
        }
    }

    public int getExposureLevel() {
        return this.classIntrospector.getExposureLevel();
    }

    public void setExposeFields(boolean z) {
        checkModifiable();
        if (this.classIntrospector.getExposeFields() != z) {
            ClassIntrospectorBuilder propertyAssignments = this.classIntrospector.getPropertyAssignments();
            propertyAssignments.setExposeFields(z);
            replaceClassIntrospector(propertyAssignments);
        }
    }

    public boolean isExposeFields() {
        return this.classIntrospector.getExposeFields();
    }

    public MethodAppearanceFineTuner getMethodAppearanceFineTuner() {
        return this.classIntrospector.getMethodAppearanceFineTuner();
    }

    public void setMethodAppearanceFineTuner(MethodAppearanceFineTuner methodAppearanceFineTuner) {
        checkModifiable();
        if (this.classIntrospector.getMethodAppearanceFineTuner() != methodAppearanceFineTuner) {
            ClassIntrospectorBuilder propertyAssignments = this.classIntrospector.getPropertyAssignments();
            propertyAssignments.setMethodAppearanceFineTuner(methodAppearanceFineTuner);
            replaceClassIntrospector(propertyAssignments);
        }
    }

    /* access modifiers changed from: package-private */
    public MethodSorter getMethodSorter() {
        return this.classIntrospector.getMethodSorter();
    }

    /* access modifiers changed from: package-private */
    public void setMethodSorter(MethodSorter methodSorter) {
        checkModifiable();
        if (this.classIntrospector.getMethodSorter() != methodSorter) {
            ClassIntrospectorBuilder propertyAssignments = this.classIntrospector.getPropertyAssignments();
            propertyAssignments.setMethodSorter(methodSorter);
            replaceClassIntrospector(propertyAssignments);
        }
    }

    public boolean isClassIntrospectionCacheRestricted() {
        return this.classIntrospector.getHasSharedInstanceRestrictons();
    }

    private void replaceClassIntrospector(ClassIntrospectorBuilder classIntrospectorBuilder) {
        checkModifiable();
        ClassIntrospector classIntrospector2 = new ClassIntrospector(classIntrospectorBuilder, this.sharedIntrospectionLock);
        synchronized (this.sharedIntrospectionLock) {
            ClassIntrospector classIntrospector3 = this.classIntrospector;
            if (classIntrospector3 != null) {
                if (this.staticModels != null) {
                    classIntrospector3.unregisterModelFactory((ClassBasedModelFactory) this.staticModels);
                    this.staticModels.clearCache();
                }
                if (this.enumModels != null) {
                    classIntrospector3.unregisterModelFactory(this.enumModels);
                    this.enumModels.clearCache();
                }
                if (this.modelCache != null) {
                    classIntrospector3.unregisterModelFactory(this.modelCache);
                    this.modelCache.clearCache();
                }
                if (this.trueModel != null) {
                    this.trueModel.clearMemberCache();
                }
                if (this.falseModel != null) {
                    this.falseModel.clearMemberCache();
                }
            }
            this.classIntrospector = classIntrospector2;
            registerModelFactories();
        }
    }

    private void registerModelFactories() {
        StaticModels staticModels2 = this.staticModels;
        if (staticModels2 != null) {
            this.classIntrospector.registerModelFactory((ClassBasedModelFactory) staticModels2);
        }
        ClassBasedModelFactory classBasedModelFactory = this.enumModels;
        if (classBasedModelFactory != null) {
            this.classIntrospector.registerModelFactory(classBasedModelFactory);
        }
        ModelCache modelCache2 = this.modelCache;
        if (modelCache2 != null) {
            this.classIntrospector.registerModelFactory(modelCache2);
        }
    }

    public void setMethodsShadowItems(boolean z) {
        synchronized (this) {
            checkModifiable();
            this.methodsShadowItems = z;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isMethodsShadowItems() {
        return this.methodsShadowItems;
    }

    public void setDefaultDateType(int i) {
        synchronized (this) {
            checkModifiable();
            this.defaultDateType = i;
        }
    }

    public int getDefaultDateType() {
        return this.defaultDateType;
    }

    public void setUseCache(boolean z) {
        checkModifiable();
        this.modelCache.setUseCache(z);
    }

    public boolean getUseCache() {
        return this.modelCache.getUseCache();
    }

    public void setNullModel(TemplateModel templateModel) {
        checkModifiable();
        this.nullModel = templateModel;
    }

    public Version getIncompatibleImprovements() {
        return this.incompatibleImprovements;
    }

    /* access modifiers changed from: package-private */
    public boolean is2321Bugfixed() {
        return is2321Bugfixed(getIncompatibleImprovements());
    }

    static boolean is2321Bugfixed(Version version) {
        return version.intValue() >= _TemplateAPI.VERSION_INT_2_3_21;
    }

    protected static Version normalizeIncompatibleImprovementsVersion(Version version) {
        _TemplateAPI.checkVersionNotNullAndSupported(version);
        if (version.intValue() >= _TemplateAPI.VERSION_INT_2_3_0) {
            return is2321Bugfixed(version) ? Configuration.VERSION_2_3_21 : Configuration.VERSION_2_3_0;
        }
        throw new IllegalArgumentException("Version must be at least 2.3.0.");
    }

    public static final BeansWrapper getDefaultInstance() {
        return BeansWrapperSingletonHolder.INSTANCE;
    }

    public TemplateModel wrap(Object obj) throws TemplateModelException {
        if (obj == null) {
            return this.nullModel;
        }
        return this.modelCache.getInstance(obj);
    }

    public TemplateMethodModelEx wrap(Object obj, Method method) {
        return new SimpleMethodModel(obj, method, method.getParameterTypes(), this);
    }

    public TemplateHashModel wrapAsAPI(Object obj) throws TemplateModelException {
        return new APIModel(obj, this);
    }

    /* access modifiers changed from: protected */
    public TemplateModel getInstance(Object obj, ModelFactory modelFactory) {
        return modelFactory.create(obj, this);
    }

    /* access modifiers changed from: protected */
    public ModelFactory getModelFactory(Class cls) {
        Class cls2 = class$java$util$Map;
        if (cls2 == null) {
            cls2 = class$("java.util.Map");
            class$java$util$Map = cls2;
        }
        if (cls2.isAssignableFrom(cls)) {
            return this.simpleMapWrapper ? SimpleMapModel.FACTORY : MapModel.FACTORY;
        }
        Class cls3 = class$java$util$Collection;
        if (cls3 == null) {
            cls3 = class$("java.util.Collection");
            class$java$util$Collection = cls3;
        }
        if (cls3.isAssignableFrom(cls)) {
            return CollectionModel.FACTORY;
        }
        Class cls4 = class$java$lang$Number;
        if (cls4 == null) {
            cls4 = class$("java.lang.Number");
            class$java$lang$Number = cls4;
        }
        if (cls4.isAssignableFrom(cls)) {
            return NumberModel.FACTORY;
        }
        Class cls5 = class$java$util$Date;
        if (cls5 == null) {
            cls5 = class$("java.util.Date");
            class$java$util$Date = cls5;
        }
        if (cls5.isAssignableFrom(cls)) {
            return DateModel.FACTORY;
        }
        Class cls6 = class$java$lang$Boolean;
        if (cls6 == null) {
            cls6 = class$("java.lang.Boolean");
            class$java$lang$Boolean = cls6;
        }
        if (cls6 == cls) {
            return this.BOOLEAN_FACTORY;
        }
        Class cls7 = class$java$util$ResourceBundle;
        if (cls7 == null) {
            cls7 = class$("java.util.ResourceBundle");
            class$java$util$ResourceBundle = cls7;
        }
        if (cls7.isAssignableFrom(cls)) {
            return ResourceBundleModel.FACTORY;
        }
        Class cls8 = class$java$util$Iterator;
        if (cls8 == null) {
            cls8 = class$("java.util.Iterator");
            class$java$util$Iterator = cls8;
        }
        if (cls8.isAssignableFrom(cls)) {
            return ITERATOR_FACTORY;
        }
        Class cls9 = class$java$util$Enumeration;
        if (cls9 == null) {
            cls9 = class$("java.util.Enumeration");
            class$java$util$Enumeration = cls9;
        }
        if (cls9.isAssignableFrom(cls)) {
            return ENUMERATION_FACTORY;
        }
        if (cls.isArray()) {
            return ArrayModel.FACTORY;
        }
        return StringModel.FACTORY;
    }

    public Object unwrap(TemplateModel templateModel) throws TemplateModelException {
        Class cls = class$java$lang$Object;
        if (cls == null) {
            cls = class$("java.lang.Object");
            class$java$lang$Object = cls;
        }
        return unwrap(templateModel, cls);
    }

    public Object unwrap(TemplateModel templateModel, Class cls) throws TemplateModelException {
        Object tryUnwrapTo = tryUnwrapTo(templateModel, cls);
        if (tryUnwrapTo != ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
            return tryUnwrapTo;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Can not unwrap model of type ");
        stringBuffer.append(templateModel.getClass().getName());
        stringBuffer.append(" to type ");
        stringBuffer.append(cls.getName());
        throw new TemplateModelException(stringBuffer.toString());
    }

    public Object tryUnwrapTo(TemplateModel templateModel, Class cls) throws TemplateModelException {
        return tryUnwrapTo(templateModel, cls, 0);
    }

    /* access modifiers changed from: package-private */
    public Object tryUnwrapTo(TemplateModel templateModel, Class cls, int i) throws TemplateModelException {
        Object tryUnwrapTo = tryUnwrapTo(templateModel, cls, i, null);
        return ((i & 1) == 0 || !(tryUnwrapTo instanceof Number)) ? tryUnwrapTo : OverloadedNumberUtil.addFallbackType((Number) tryUnwrapTo, i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:180:0x022c, code lost:
        if (r8.isAssignableFrom(r10) != false) goto L_0x022e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x025b, code lost:
        return r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x02aa, code lost:
        return new freemarker.ext.beans.HashAdapter((freemarker.template.TemplateHashModel) r7, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x02d1, code lost:
        return new freemarker.ext.beans.SequenceAdapter((freemarker.template.TemplateSequenceModel) r7, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x02f8, code lost:
        return new freemarker.ext.beans.SetAdapter((freemarker.template.TemplateCollectionModel) r7, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0054, code lost:
        if (r1 != null) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0091, code lost:
        if (r1 != null) goto L_0x0093;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object tryUnwrapTo(freemarker.template.TemplateModel r7, java.lang.Class r8, int r9, java.util.Map r10) throws freemarker.template.TemplateModelException {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x0319
            freemarker.template.TemplateModel r0 = r6.nullModel
            if (r7 != r0) goto L_0x0008
            goto L_0x0319
        L_0x0008:
            boolean r0 = r6.is2321Bugfixed()
            if (r0 == 0) goto L_0x0018
            boolean r1 = r8.isPrimitive()
            if (r1 == 0) goto L_0x0018
            java.lang.Class r8 = freemarker.template.utility.ClassUtil.primitiveClassToBoxingClass(r8)
        L_0x0018:
            boolean r1 = r7 instanceof freemarker.template.AdapterTemplateModel
            java.lang.String r2 = "java.lang.Object"
            if (r1 == 0) goto L_0x0057
            r1 = r7
            freemarker.template.AdapterTemplateModel r1 = (freemarker.template.AdapterTemplateModel) r1
            java.lang.Object r1 = r1.getAdaptedObject(r8)
            java.lang.Class r3 = freemarker.ext.beans.BeansWrapper.class$java$lang$Object
            if (r3 != 0) goto L_0x002f
            java.lang.Class r3 = class$(r2)
            freemarker.ext.beans.BeansWrapper.class$java$lang$Object = r3
        L_0x002f:
            if (r8 == r3) goto L_0x0056
            boolean r3 = r8.isInstance(r1)
            if (r3 == 0) goto L_0x0038
            goto L_0x0056
        L_0x0038:
            java.lang.Class r3 = freemarker.ext.beans.BeansWrapper.class$java$lang$Object
            if (r3 != 0) goto L_0x0042
            java.lang.Class r3 = class$(r2)
            freemarker.ext.beans.BeansWrapper.class$java$lang$Object = r3
        L_0x0042:
            if (r8 == r3) goto L_0x0057
            boolean r3 = r1 instanceof java.lang.Number
            if (r3 == 0) goto L_0x0057
            boolean r3 = freemarker.template.utility.ClassUtil.isNumerical(r8)
            if (r3 == 0) goto L_0x0057
            java.lang.Number r1 = (java.lang.Number) r1
            java.lang.Number r1 = forceUnwrappedNumberToType(r1, r8, r0)
            if (r1 == 0) goto L_0x0057
        L_0x0056:
            return r1
        L_0x0057:
            boolean r1 = r7 instanceof freemarker.ext.util.WrapperTemplateModel
            if (r1 == 0) goto L_0x0094
            r1 = r7
            freemarker.ext.util.WrapperTemplateModel r1 = (freemarker.ext.util.WrapperTemplateModel) r1
            java.lang.Object r1 = r1.getWrappedObject()
            java.lang.Class r3 = freemarker.ext.beans.BeansWrapper.class$java$lang$Object
            if (r3 != 0) goto L_0x006c
            java.lang.Class r3 = class$(r2)
            freemarker.ext.beans.BeansWrapper.class$java$lang$Object = r3
        L_0x006c:
            if (r8 == r3) goto L_0x0093
            boolean r3 = r8.isInstance(r1)
            if (r3 == 0) goto L_0x0075
            goto L_0x0093
        L_0x0075:
            java.lang.Class r3 = freemarker.ext.beans.BeansWrapper.class$java$lang$Object
            if (r3 != 0) goto L_0x007f
            java.lang.Class r3 = class$(r2)
            freemarker.ext.beans.BeansWrapper.class$java$lang$Object = r3
        L_0x007f:
            if (r8 == r3) goto L_0x0094
            boolean r3 = r1 instanceof java.lang.Number
            if (r3 == 0) goto L_0x0094
            boolean r3 = freemarker.template.utility.ClassUtil.isNumerical(r8)
            if (r3 == 0) goto L_0x0094
            java.lang.Number r1 = (java.lang.Number) r1
            java.lang.Number r1 = forceUnwrappedNumberToType(r1, r8, r0)
            if (r1 == 0) goto L_0x0094
        L_0x0093:
            return r1
        L_0x0094:
            java.lang.Class r1 = freemarker.ext.beans.BeansWrapper.class$java$lang$Object
            if (r1 != 0) goto L_0x009e
            java.lang.Class r1 = class$(r2)
            freemarker.ext.beans.BeansWrapper.class$java$lang$Object = r1
        L_0x009e:
            java.lang.String r2 = "java.lang.Boolean"
            java.lang.String r3 = "java.lang.String"
            r4 = 0
            r5 = 1
            if (r8 == r1) goto L_0x01dc
            java.lang.Class r1 = freemarker.ext.beans.BeansWrapper.class$java$lang$String
            if (r1 != 0) goto L_0x00b0
            java.lang.Class r1 = class$(r3)
            freemarker.ext.beans.BeansWrapper.class$java$lang$String = r1
        L_0x00b0:
            if (r1 != r8) goto L_0x00c0
            boolean r8 = r7 instanceof freemarker.template.TemplateScalarModel
            if (r8 == 0) goto L_0x00bd
            freemarker.template.TemplateScalarModel r7 = (freemarker.template.TemplateScalarModel) r7
            java.lang.String r7 = r7.getAsString()
            return r7
        L_0x00bd:
            java.lang.Object r7 = freemarker.template.ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS
            return r7
        L_0x00c0:
            boolean r1 = freemarker.template.utility.ClassUtil.isNumerical(r8)
            if (r1 == 0) goto L_0x00d8
            boolean r1 = r7 instanceof freemarker.template.TemplateNumberModel
            if (r1 == 0) goto L_0x00d8
            r1 = r7
            freemarker.template.TemplateNumberModel r1 = (freemarker.template.TemplateNumberModel) r1
            java.lang.Number r1 = r1.getAsNumber()
            java.lang.Number r0 = forceUnwrappedNumberToType(r1, r8, r0)
            if (r0 == 0) goto L_0x00d8
            return r0
        L_0x00d8:
            java.lang.Class r0 = java.lang.Boolean.TYPE
            if (r0 == r8) goto L_0x01ca
            java.lang.Class r0 = freemarker.ext.beans.BeansWrapper.class$java$lang$Boolean
            if (r0 != 0) goto L_0x00e6
            java.lang.Class r0 = class$(r2)
            freemarker.ext.beans.BeansWrapper.class$java$lang$Boolean = r0
        L_0x00e6:
            if (r0 != r8) goto L_0x00ea
            goto L_0x01ca
        L_0x00ea:
            java.lang.Class r0 = freemarker.ext.beans.BeansWrapper.class$java$util$Map
            if (r0 != 0) goto L_0x00f6
            java.lang.String r0 = "java.util.Map"
            java.lang.Class r0 = class$(r0)
            freemarker.ext.beans.BeansWrapper.class$java$util$Map = r0
        L_0x00f6:
            if (r0 != r8) goto L_0x0104
            boolean r0 = r7 instanceof freemarker.template.TemplateHashModel
            if (r0 == 0) goto L_0x0104
            freemarker.ext.beans.HashAdapter r8 = new freemarker.ext.beans.HashAdapter
            freemarker.template.TemplateHashModel r7 = (freemarker.template.TemplateHashModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x0104:
            java.lang.Class r0 = freemarker.ext.beans.BeansWrapper.class$java$util$List
            if (r0 != 0) goto L_0x0110
            java.lang.String r0 = "java.util.List"
            java.lang.Class r0 = class$(r0)
            freemarker.ext.beans.BeansWrapper.class$java$util$List = r0
        L_0x0110:
            if (r0 != r8) goto L_0x011e
            boolean r0 = r7 instanceof freemarker.template.TemplateSequenceModel
            if (r0 == 0) goto L_0x011e
            freemarker.ext.beans.SequenceAdapter r8 = new freemarker.ext.beans.SequenceAdapter
            freemarker.template.TemplateSequenceModel r7 = (freemarker.template.TemplateSequenceModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x011e:
            java.lang.Class r0 = freemarker.ext.beans.BeansWrapper.class$java$util$Set
            if (r0 != 0) goto L_0x012a
            java.lang.String r0 = "java.util.Set"
            java.lang.Class r0 = class$(r0)
            freemarker.ext.beans.BeansWrapper.class$java$util$Set = r0
        L_0x012a:
            if (r0 != r8) goto L_0x0138
            boolean r0 = r7 instanceof freemarker.template.TemplateCollectionModel
            if (r0 == 0) goto L_0x0138
            freemarker.ext.beans.SetAdapter r8 = new freemarker.ext.beans.SetAdapter
            freemarker.template.TemplateCollectionModel r7 = (freemarker.template.TemplateCollectionModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x0138:
            java.lang.Class r0 = freemarker.ext.beans.BeansWrapper.class$java$util$Collection
            if (r0 != 0) goto L_0x0144
            java.lang.String r0 = "java.util.Collection"
            java.lang.Class r0 = class$(r0)
            freemarker.ext.beans.BeansWrapper.class$java$util$Collection = r0
        L_0x0144:
            if (r0 == r8) goto L_0x014a
            java.lang.Class r0 = freemarker.ext.beans.BeansWrapper.ITERABLE_CLASS
            if (r0 != r8) goto L_0x0162
        L_0x014a:
            boolean r0 = r7 instanceof freemarker.template.TemplateCollectionModel
            if (r0 == 0) goto L_0x0156
            freemarker.ext.beans.CollectionAdapter r8 = new freemarker.ext.beans.CollectionAdapter
            freemarker.template.TemplateCollectionModel r7 = (freemarker.template.TemplateCollectionModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x0156:
            boolean r0 = r7 instanceof freemarker.template.TemplateSequenceModel
            if (r0 == 0) goto L_0x0162
            freemarker.ext.beans.SequenceAdapter r8 = new freemarker.ext.beans.SequenceAdapter
            freemarker.template.TemplateSequenceModel r7 = (freemarker.template.TemplateSequenceModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x0162:
            boolean r0 = r8.isArray()
            if (r0 == 0) goto L_0x0176
            boolean r9 = r7 instanceof freemarker.template.TemplateSequenceModel
            if (r9 == 0) goto L_0x0173
            freemarker.template.TemplateSequenceModel r7 = (freemarker.template.TemplateSequenceModel) r7
            java.lang.Object r7 = r6.unwrapSequenceToArray(r7, r8, r5, r10)
            return r7
        L_0x0173:
            java.lang.Object r7 = freemarker.template.ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS
            return r7
        L_0x0176:
            java.lang.Class r10 = java.lang.Character.TYPE
            if (r10 == r8) goto L_0x01ad
            java.lang.Class r10 = freemarker.ext.beans.BeansWrapper.class$java$lang$Character
            if (r10 != 0) goto L_0x0186
            java.lang.String r10 = "java.lang.Character"
            java.lang.Class r10 = class$(r10)
            freemarker.ext.beans.BeansWrapper.class$java$lang$Character = r10
        L_0x0186:
            if (r8 != r10) goto L_0x0189
            goto L_0x01ad
        L_0x0189:
            java.lang.Class r10 = freemarker.ext.beans.BeansWrapper.class$java$util$Date
            if (r10 != 0) goto L_0x0195
            java.lang.String r10 = "java.util.Date"
            java.lang.Class r10 = class$(r10)
            freemarker.ext.beans.BeansWrapper.class$java$util$Date = r10
        L_0x0195:
            boolean r10 = r10.isAssignableFrom(r8)
            if (r10 == 0) goto L_0x01dc
            boolean r10 = r7 instanceof freemarker.template.TemplateDateModel
            if (r10 == 0) goto L_0x01dc
            r10 = r7
            freemarker.template.TemplateDateModel r10 = (freemarker.template.TemplateDateModel) r10
            java.util.Date r10 = r10.getAsDate()
            boolean r0 = r8.isInstance(r10)
            if (r0 == 0) goto L_0x01dc
            return r10
        L_0x01ad:
            boolean r8 = r7 instanceof freemarker.template.TemplateScalarModel
            if (r8 == 0) goto L_0x01c7
            freemarker.template.TemplateScalarModel r7 = (freemarker.template.TemplateScalarModel) r7
            java.lang.String r7 = r7.getAsString()
            int r8 = r7.length()
            if (r8 != r5) goto L_0x01c7
            java.lang.Character r8 = new java.lang.Character
            char r7 = r7.charAt(r4)
            r8.<init>(r7)
            return r8
        L_0x01c7:
            java.lang.Object r7 = freemarker.template.ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS
            return r7
        L_0x01ca:
            boolean r8 = r7 instanceof freemarker.template.TemplateBooleanModel
            if (r8 == 0) goto L_0x01d9
            freemarker.template.TemplateBooleanModel r7 = (freemarker.template.TemplateBooleanModel) r7
            boolean r7 = r7.getAsBoolean()
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            return r7
        L_0x01d9:
            java.lang.Object r7 = freemarker.template.ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS
            return r7
        L_0x01dc:
            if (r9 == 0) goto L_0x01e2
            r10 = r9 & 2048(0x800, float:2.87E-42)
            if (r10 == 0) goto L_0x01f6
        L_0x01e2:
            boolean r10 = r7 instanceof freemarker.template.TemplateNumberModel
            if (r10 == 0) goto L_0x01f6
            r10 = r7
            freemarker.template.TemplateNumberModel r10 = (freemarker.template.TemplateNumberModel) r10
            java.lang.Number r10 = r10.getAsNumber()
            if (r9 != 0) goto L_0x01f5
            boolean r0 = r8.isInstance(r10)
            if (r0 == 0) goto L_0x01f6
        L_0x01f5:
            return r10
        L_0x01f6:
            if (r9 == 0) goto L_0x01fc
            r10 = r9 & 4096(0x1000, float:5.74E-42)
            if (r10 == 0) goto L_0x0210
        L_0x01fc:
            boolean r10 = r7 instanceof freemarker.template.TemplateDateModel
            if (r10 == 0) goto L_0x0210
            r10 = r7
            freemarker.template.TemplateDateModel r10 = (freemarker.template.TemplateDateModel) r10
            java.util.Date r10 = r10.getAsDate()
            if (r9 != 0) goto L_0x020f
            boolean r0 = r8.isInstance(r10)
            if (r0 == 0) goto L_0x0210
        L_0x020f:
            return r10
        L_0x0210:
            if (r9 == 0) goto L_0x0218
            r10 = 532480(0x82000, float:7.46163E-40)
            r10 = r10 & r9
            if (r10 == 0) goto L_0x025c
        L_0x0218:
            boolean r10 = r7 instanceof freemarker.template.TemplateScalarModel
            if (r10 == 0) goto L_0x025c
            if (r9 != 0) goto L_0x022e
            java.lang.Class r10 = freemarker.ext.beans.BeansWrapper.class$java$lang$String
            if (r10 != 0) goto L_0x0228
            java.lang.Class r10 = class$(r3)
            freemarker.ext.beans.BeansWrapper.class$java$lang$String = r10
        L_0x0228:
            boolean r10 = r8.isAssignableFrom(r10)
            if (r10 == 0) goto L_0x025c
        L_0x022e:
            r10 = r7
            freemarker.template.TemplateScalarModel r10 = (freemarker.template.TemplateScalarModel) r10
            java.lang.String r10 = r10.getAsString()
            if (r9 == 0) goto L_0x025b
            r0 = 524288(0x80000, float:7.34684E-40)
            r0 = r0 & r9
            if (r0 != 0) goto L_0x023d
            goto L_0x025b
        L_0x023d:
            int r0 = r10.length()
            if (r0 != r5) goto L_0x0257
            r7 = r9 & 8192(0x2000, float:1.14794E-41)
            if (r7 == 0) goto L_0x024d
            freemarker.ext.beans.CharacterOrString r7 = new freemarker.ext.beans.CharacterOrString
            r7.<init>(r10)
            return r7
        L_0x024d:
            java.lang.Character r7 = new java.lang.Character
            char r8 = r10.charAt(r4)
            r7.<init>(r8)
            return r7
        L_0x0257:
            r0 = r9 & 8192(0x2000, float:1.14794E-41)
            if (r0 == 0) goto L_0x025c
        L_0x025b:
            return r10
        L_0x025c:
            if (r9 == 0) goto L_0x0262
            r10 = r9 & 16384(0x4000, float:2.2959E-41)
            if (r10 == 0) goto L_0x0283
        L_0x0262:
            boolean r10 = r7 instanceof freemarker.template.TemplateBooleanModel
            if (r10 == 0) goto L_0x0283
            if (r9 != 0) goto L_0x0278
            java.lang.Class r10 = freemarker.ext.beans.BeansWrapper.class$java$lang$Boolean
            if (r10 != 0) goto L_0x0272
            java.lang.Class r10 = class$(r2)
            freemarker.ext.beans.BeansWrapper.class$java$lang$Boolean = r10
        L_0x0272:
            boolean r10 = r8.isAssignableFrom(r10)
            if (r10 == 0) goto L_0x0283
        L_0x0278:
            freemarker.template.TemplateBooleanModel r7 = (freemarker.template.TemplateBooleanModel) r7
            boolean r7 = r7.getAsBoolean()
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            return r7
        L_0x0283:
            if (r9 == 0) goto L_0x028b
            r10 = 32768(0x8000, float:4.5918E-41)
            r10 = r10 & r9
            if (r10 == 0) goto L_0x02ab
        L_0x028b:
            boolean r10 = r7 instanceof freemarker.template.TemplateHashModel
            if (r10 == 0) goto L_0x02ab
            if (r9 != 0) goto L_0x02a3
            java.lang.Class r10 = freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$HashAdapter
            if (r10 != 0) goto L_0x029d
            java.lang.String r10 = "freemarker.ext.beans.HashAdapter"
            java.lang.Class r10 = class$(r10)
            freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$HashAdapter = r10
        L_0x029d:
            boolean r10 = r8.isAssignableFrom(r10)
            if (r10 == 0) goto L_0x02ab
        L_0x02a3:
            freemarker.ext.beans.HashAdapter r8 = new freemarker.ext.beans.HashAdapter
            freemarker.template.TemplateHashModel r7 = (freemarker.template.TemplateHashModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x02ab:
            if (r9 == 0) goto L_0x02b2
            r10 = 65536(0x10000, float:9.18355E-41)
            r10 = r10 & r9
            if (r10 == 0) goto L_0x02d2
        L_0x02b2:
            boolean r10 = r7 instanceof freemarker.template.TemplateSequenceModel
            if (r10 == 0) goto L_0x02d2
            if (r9 != 0) goto L_0x02ca
            java.lang.Class r10 = freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$SequenceAdapter
            if (r10 != 0) goto L_0x02c4
            java.lang.String r10 = "freemarker.ext.beans.SequenceAdapter"
            java.lang.Class r10 = class$(r10)
            freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$SequenceAdapter = r10
        L_0x02c4:
            boolean r10 = r8.isAssignableFrom(r10)
            if (r10 == 0) goto L_0x02d2
        L_0x02ca:
            freemarker.ext.beans.SequenceAdapter r8 = new freemarker.ext.beans.SequenceAdapter
            freemarker.template.TemplateSequenceModel r7 = (freemarker.template.TemplateSequenceModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x02d2:
            if (r9 == 0) goto L_0x02d9
            r10 = 131072(0x20000, float:1.83671E-40)
            r10 = r10 & r9
            if (r10 == 0) goto L_0x02f9
        L_0x02d9:
            boolean r10 = r7 instanceof freemarker.template.TemplateCollectionModel
            if (r10 == 0) goto L_0x02f9
            if (r9 != 0) goto L_0x02f1
            java.lang.Class r10 = freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$SetAdapter
            if (r10 != 0) goto L_0x02eb
            java.lang.String r10 = "freemarker.ext.beans.SetAdapter"
            java.lang.Class r10 = class$(r10)
            freemarker.ext.beans.BeansWrapper.class$freemarker$ext$beans$SetAdapter = r10
        L_0x02eb:
            boolean r10 = r8.isAssignableFrom(r10)
            if (r10 == 0) goto L_0x02f9
        L_0x02f1:
            freemarker.ext.beans.SetAdapter r8 = new freemarker.ext.beans.SetAdapter
            freemarker.template.TemplateCollectionModel r7 = (freemarker.template.TemplateCollectionModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x02f9:
            r10 = 262144(0x40000, float:3.67342E-40)
            r10 = r10 & r9
            if (r10 == 0) goto L_0x030a
            boolean r10 = r7 instanceof freemarker.template.TemplateSequenceModel
            if (r10 == 0) goto L_0x030a
            freemarker.ext.beans.SequenceAdapter r8 = new freemarker.ext.beans.SequenceAdapter
            freemarker.template.TemplateSequenceModel r7 = (freemarker.template.TemplateSequenceModel) r7
            r8.<init>(r7, r6)
            return r8
        L_0x030a:
            if (r9 != 0) goto L_0x0316
            boolean r8 = r8.isInstance(r7)
            if (r8 == 0) goto L_0x0313
            return r7
        L_0x0313:
            java.lang.Object r7 = freemarker.template.ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS
            return r7
        L_0x0316:
            r9 = 0
            goto L_0x01dc
        L_0x0319:
            r7 = 0
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.BeansWrapper.tryUnwrapTo(freemarker.template.TemplateModel, java.lang.Class, int, java.util.Map):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public Object unwrapSequenceToArray(TemplateSequenceModel templateSequenceModel, Class cls, boolean z, Map map) throws TemplateModelException {
        if (map != null) {
            Object obj = map.get(templateSequenceModel);
            if (obj != null) {
                return obj;
            }
        } else {
            map = new IdentityHashMap();
        }
        Class<?> componentType = cls.getComponentType();
        Object newInstance = Array.newInstance(componentType, templateSequenceModel.size());
        map.put(templateSequenceModel, newInstance);
        try {
            int size = templateSequenceModel.size();
            int i = 0;
            while (i < size) {
                TemplateModel templateModel = templateSequenceModel.get(i);
                Object tryUnwrapTo = tryUnwrapTo(templateModel, componentType, 0, map);
                if (tryUnwrapTo != ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
                    Array.set(newInstance, i, tryUnwrapTo);
                    i++;
                } else if (z) {
                    return ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS;
                } else {
                    throw new _TemplateModelException(new Object[]{"Failed to convert ", new _DelayedFTLTypeDescription(templateSequenceModel), " object to ", new _DelayedShortClassName(newInstance.getClass()), ": Problematic sequence item at index ", new Integer(i), " with value type: ", new _DelayedFTLTypeDescription(templateModel)});
                }
            }
            map.remove(templateSequenceModel);
            return newInstance;
        } finally {
            map.remove(templateSequenceModel);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Exception):void
     arg types: [java.lang.String, java.lang.IllegalArgumentException]
     candidates:
      freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Throwable):void
      freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Exception):void */
    /* access modifiers changed from: package-private */
    public Object listToArray(List list, Class cls, Map map) throws TemplateModelException {
        int i;
        Object next;
        Class<?> cls2;
        Class<?> cls3;
        Class cls4;
        if (list instanceof SequenceAdapter) {
            return unwrapSequenceToArray(((SequenceAdapter) list).getTemplateSequenceModel(), cls, false, map);
        }
        if (map != null) {
            Object obj = map.get(list);
            if (obj != null) {
                return obj;
            }
        } else {
            map = new IdentityHashMap();
        }
        Class<?> componentType = cls.getComponentType();
        Object newInstance = Array.newInstance(componentType, list.size());
        map.put(list, newInstance);
        try {
            Iterator it = list.iterator();
            boolean z = false;
            boolean z2 = false;
            boolean z3 = false;
            i = 0;
            while (it.hasNext()) {
                next = it.next();
                if (next != null && !componentType.isInstance(next)) {
                    if (!z) {
                        z2 = ClassUtil.isNumerical(componentType);
                        if (class$java$util$List == null) {
                            cls4 = class$("java.util.List");
                            class$java$util$List = cls4;
                        } else {
                            cls4 = class$java$util$List;
                        }
                        z3 = cls4.isAssignableFrom(componentType);
                        z = true;
                    }
                    if (!z2 || !(next instanceof Number)) {
                        if (class$java$lang$String == null) {
                            cls2 = class$("java.lang.String");
                            class$java$lang$String = cls2;
                        } else {
                            cls2 = class$java$lang$String;
                        }
                        if (componentType != cls2 || !(next instanceof Character)) {
                            if (class$java$lang$Character == null) {
                                cls3 = class$("java.lang.Character");
                                class$java$lang$Character = cls3;
                            } else {
                                cls3 = class$java$lang$Character;
                            }
                            if ((componentType == cls3 || componentType == Character.TYPE) && (next instanceof String)) {
                                String str = (String) next;
                                if (str.length() == 1) {
                                    next = new Character(str.charAt(0));
                                }
                            } else if (componentType.isArray()) {
                                if (next instanceof List) {
                                    next = listToArray((List) next, componentType, map);
                                } else if (next instanceof TemplateSequenceModel) {
                                    next = unwrapSequenceToArray((TemplateSequenceModel) next, componentType, false, map);
                                }
                            } else if (z3 && next.getClass().isArray()) {
                                next = arrayToList(next);
                            }
                        } else {
                            next = String.valueOf(((Character) next).charValue());
                        }
                    } else {
                        next = forceUnwrappedNumberToType((Number) next, componentType, true);
                    }
                }
                Array.set(newInstance, i, next);
                i++;
            }
            map.remove(list);
            return newInstance;
        } catch (IllegalArgumentException e) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Failed to convert ");
            stringBuffer.append(ClassUtil.getShortClassNameOfObject(list));
            stringBuffer.append(" object to ");
            stringBuffer.append(ClassUtil.getShortClassNameOfObject(newInstance));
            stringBuffer.append(": Problematic List item at index ");
            stringBuffer.append(i);
            stringBuffer.append(" with value type: ");
            stringBuffer.append(ClassUtil.getShortClassNameOfObject(next));
            throw new TemplateModelException(stringBuffer.toString(), (Exception) e);
        } catch (Throwable th) {
            map.remove(list);
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public List arrayToList(Object obj) throws TemplateModelException {
        if (!(obj instanceof Object[])) {
            return Array.getLength(obj) == 0 ? Collections.EMPTY_LIST : new PrimtiveArrayBackedReadOnlyList(obj);
        }
        Object[] objArr = (Object[]) obj;
        return objArr.length == 0 ? Collections.EMPTY_LIST : new NonPrimitiveArrayBackedReadOnlyList(objArr);
    }

    static Number forceUnwrappedNumberToType(Number number, Class cls, boolean z) {
        if (cls == number.getClass()) {
            return number;
        }
        if (cls != Integer.TYPE) {
            Class cls2 = class$java$lang$Integer;
            if (cls2 == null) {
                cls2 = class$("java.lang.Integer");
                class$java$lang$Integer = cls2;
            }
            if (cls != cls2) {
                if (cls != Long.TYPE) {
                    Class cls3 = class$java$lang$Long;
                    if (cls3 == null) {
                        cls3 = class$("java.lang.Long");
                        class$java$lang$Long = cls3;
                    }
                    if (cls != cls3) {
                        if (cls != Double.TYPE) {
                            Class cls4 = class$java$lang$Double;
                            if (cls4 == null) {
                                cls4 = class$("java.lang.Double");
                                class$java$lang$Double = cls4;
                            }
                            if (cls != cls4) {
                                Class cls5 = class$java$math$BigDecimal;
                                if (cls5 == null) {
                                    cls5 = class$("java.math.BigDecimal");
                                    class$java$math$BigDecimal = cls5;
                                }
                                if (cls != cls5) {
                                    if (cls != Float.TYPE) {
                                        Class cls6 = class$java$lang$Float;
                                        if (cls6 == null) {
                                            cls6 = class$("java.lang.Float");
                                            class$java$lang$Float = cls6;
                                        }
                                        if (cls != cls6) {
                                            if (cls != Byte.TYPE) {
                                                Class cls7 = class$java$lang$Byte;
                                                if (cls7 == null) {
                                                    cls7 = class$("java.lang.Byte");
                                                    class$java$lang$Byte = cls7;
                                                }
                                                if (cls != cls7) {
                                                    if (cls != Short.TYPE) {
                                                        Class cls8 = class$java$lang$Short;
                                                        if (cls8 == null) {
                                                            cls8 = class$("java.lang.Short");
                                                            class$java$lang$Short = cls8;
                                                        }
                                                        if (cls != cls8) {
                                                            Class cls9 = class$java$math$BigInteger;
                                                            if (cls9 == null) {
                                                                cls9 = class$("java.math.BigInteger");
                                                                class$java$math$BigInteger = cls9;
                                                            }
                                                            if (cls != cls9) {
                                                                if (number instanceof OverloadedNumberUtil.NumberWithFallbackType) {
                                                                    number = ((OverloadedNumberUtil.NumberWithFallbackType) number).getSourceNumber();
                                                                }
                                                                if (cls.isInstance(number)) {
                                                                    return number;
                                                                }
                                                                return null;
                                                            } else if (number instanceof BigInteger) {
                                                                return number;
                                                            } else {
                                                                if (!z) {
                                                                    return new BigInteger(number.toString());
                                                                }
                                                                if (number instanceof OverloadedNumberUtil.IntegerBigDecimal) {
                                                                    return ((OverloadedNumberUtil.IntegerBigDecimal) number).bigIntegerValue();
                                                                }
                                                                if (number instanceof BigDecimal) {
                                                                    return ((BigDecimal) number).toBigInteger();
                                                                }
                                                                return BigInteger.valueOf(number.longValue());
                                                            }
                                                        }
                                                    }
                                                    return number instanceof Short ? (Short) number : new Short(number.shortValue());
                                                }
                                            }
                                            return number instanceof Byte ? (Byte) number : new Byte(number.byteValue());
                                        }
                                    }
                                    return number instanceof Float ? (Float) number : new Float(number.floatValue());
                                } else if (number instanceof BigDecimal) {
                                    return number;
                                } else {
                                    if (number instanceof BigInteger) {
                                        return new BigDecimal((BigInteger) number);
                                    }
                                    if (number instanceof Long) {
                                        return BigDecimal.valueOf(number.longValue());
                                    }
                                    return new BigDecimal(number.doubleValue());
                                }
                            }
                        }
                        return number instanceof Double ? (Double) number : new Double(number.doubleValue());
                    }
                }
                return number instanceof Long ? (Long) number : new Long(number.longValue());
            }
        }
        return number instanceof Integer ? (Integer) number : new Integer(number.intValue());
    }

    /* access modifiers changed from: package-private */
    public TemplateModel invokeMethod(Object obj, Method method, Object[] objArr) throws InvocationTargetException, IllegalAccessException, TemplateModelException {
        Object invoke = method.invoke(obj, objArr);
        if (method.getReturnType() == Void.TYPE) {
            return TemplateModel.NOTHING;
        }
        return getOuterIdentity().wrap(invoke);
    }

    public TemplateHashModel getStaticModels() {
        return this.staticModels;
    }

    public TemplateHashModel getEnumModels() {
        ClassBasedModelFactory classBasedModelFactory = this.enumModels;
        if (classBasedModelFactory != null) {
            return classBasedModelFactory;
        }
        throw new UnsupportedOperationException("Enums not supported before J2SE 5.");
    }

    /* access modifiers changed from: package-private */
    public ModelCache getModelCache() {
        return this.modelCache;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0098, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0099, code lost:
        throw r4;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0098 A[ExcHandler: TemplateModelException (r4v1 'e' freemarker.template.TemplateModelException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object newInstance(java.lang.Class r4, java.util.List r5) throws freemarker.template.TemplateModelException {
        /*
            r3 = this;
            freemarker.ext.beans.ClassIntrospector r0 = r3.classIntrospector     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.util.Map r0 = r0.get(r4)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.Object r1 = freemarker.ext.beans.ClassIntrospector.CONSTRUCTORS_KEY     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            if (r0 == 0) goto L_0x0057
            boolean r1 = r0 instanceof freemarker.ext.beans.SimpleMethod     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            r2 = 0
            if (r1 == 0) goto L_0x0031
            freemarker.ext.beans.SimpleMethod r0 = (freemarker.ext.beans.SimpleMethod) r0     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.reflect.Member r1 = r0.getMember()     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.reflect.Constructor r1 = (java.lang.reflect.Constructor) r1     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.Object[] r5 = r0.unwrapArguments(r5, r3)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.Object r4 = r1.newInstance(r5)     // Catch:{ Exception -> 0x0024, TemplateModelException -> 0x0098 }
            return r4
        L_0x0024:
            r5 = move-exception
            boolean r0 = r5 instanceof freemarker.template.TemplateModelException     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            if (r0 == 0) goto L_0x002c
            freemarker.template.TemplateModelException r5 = (freemarker.template.TemplateModelException) r5     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            throw r5     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
        L_0x002c:
            freemarker.template.TemplateModelException r5 = freemarker.ext.beans._MethodUtil.newInvocationTemplateModelException(r2, r1, r5)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            throw r5     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
        L_0x0031:
            boolean r1 = r0 instanceof freemarker.ext.beans.OverloadedMethods     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            if (r1 == 0) goto L_0x0051
            freemarker.ext.beans.OverloadedMethods r0 = (freemarker.ext.beans.OverloadedMethods) r0     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            freemarker.ext.beans.MemberAndArguments r5 = r0.getMemberAndArguments(r5, r3)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.Object r4 = r5.invokeConstructor(r3)     // Catch:{ Exception -> 0x0040, TemplateModelException -> 0x0098 }
            return r4
        L_0x0040:
            r0 = move-exception
            boolean r1 = r0 instanceof freemarker.template.TemplateModelException     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            if (r1 == 0) goto L_0x0048
            freemarker.template.TemplateModelException r0 = (freemarker.template.TemplateModelException) r0     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            throw r0     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
        L_0x0048:
            freemarker.ext.beans.CallableMemberDescriptor r5 = r5.getCallableMemberDescriptor()     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            freemarker.template.TemplateModelException r5 = freemarker.ext.beans._MethodUtil.newInvocationTemplateModelException(r2, r5, r0)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            throw r5     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
        L_0x0051:
            freemarker.core.BugException r5 = new freemarker.core.BugException     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            r5.<init>()     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            throw r5     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
        L_0x0057:
            freemarker.template.TemplateModelException r5 = new freemarker.template.TemplateModelException     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            r0.<init>()     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.String r1 = "Class "
            r0.append(r1)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.String r1 = r4.getName()     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            r0.append(r1)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.String r1 = " has no public constructors."
            r0.append(r1)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            java.lang.String r0 = r0.toString()     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            r5.<init>(r0)     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
            throw r5     // Catch:{ TemplateModelException -> 0x0098, Exception -> 0x0077 }
        L_0x0077:
            r5 = move-exception
            freemarker.template.TemplateModelException r0 = new freemarker.template.TemplateModelException
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r2 = "Error while creating new instance of class "
            r1.append(r2)
            java.lang.String r4 = r4.getName()
            r1.append(r4)
            java.lang.String r4 = "; see cause exception"
            r1.append(r4)
            java.lang.String r4 = r1.toString()
            r0.<init>(r4, r5)
            throw r0
        L_0x0098:
            r4 = move-exception
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.BeansWrapper.newInstance(java.lang.Class, java.util.List):java.lang.Object");
    }

    public void removeFromClassIntrospectionCache(Class cls) {
        this.classIntrospector.remove(cls);
    }

    public void clearClassIntrospecitonCache() {
        this.classIntrospector.clearCache();
    }

    /* access modifiers changed from: package-private */
    public ClassIntrospector getClassIntrospector() {
        return this.classIntrospector;
    }

    public static void coerceBigDecimals(AccessibleObject accessibleObject, Object[] objArr) {
        Class<?>[] clsArr = null;
        for (int i = 0; i < objArr.length; i++) {
            Object obj = objArr[i];
            if (obj instanceof BigDecimal) {
                if (clsArr == null) {
                    if (accessibleObject instanceof Method) {
                        clsArr = ((Method) accessibleObject).getParameterTypes();
                    } else if (accessibleObject instanceof Constructor) {
                        clsArr = ((Constructor) accessibleObject).getParameterTypes();
                    } else {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("Expected method or  constructor; callable is ");
                        stringBuffer.append(accessibleObject.getClass().getName());
                        throw new IllegalArgumentException(stringBuffer.toString());
                    }
                }
                objArr[i] = coerceBigDecimal((BigDecimal) obj, clsArr[i]);
            }
        }
    }

    public static void coerceBigDecimals(Class[] clsArr, Object[] objArr) {
        int length = clsArr.length;
        int length2 = objArr.length;
        int min = Math.min(length, length2);
        for (int i = 0; i < min; i++) {
            Object obj = objArr[i];
            if (obj instanceof BigDecimal) {
                objArr[i] = coerceBigDecimal((BigDecimal) obj, clsArr[i]);
            }
        }
        if (length2 > length) {
            Class cls = clsArr[length - 1];
            while (length < length2) {
                Object obj2 = objArr[length];
                if (obj2 instanceof BigDecimal) {
                    objArr[length] = coerceBigDecimal((BigDecimal) obj2, cls);
                }
                length++;
            }
        }
    }

    public static Object coerceBigDecimal(BigDecimal bigDecimal, Class cls) {
        if (cls != Integer.TYPE) {
            Class cls2 = class$java$lang$Integer;
            if (cls2 == null) {
                cls2 = class$("java.lang.Integer");
                class$java$lang$Integer = cls2;
            }
            if (cls != cls2) {
                if (cls != Double.TYPE) {
                    Class cls3 = class$java$lang$Double;
                    if (cls3 == null) {
                        cls3 = class$("java.lang.Double");
                        class$java$lang$Double = cls3;
                    }
                    if (cls != cls3) {
                        if (cls != Long.TYPE) {
                            Class cls4 = class$java$lang$Long;
                            if (cls4 == null) {
                                cls4 = class$("java.lang.Long");
                                class$java$lang$Long = cls4;
                            }
                            if (cls != cls4) {
                                if (cls != Float.TYPE) {
                                    Class cls5 = class$java$lang$Float;
                                    if (cls5 == null) {
                                        cls5 = class$("java.lang.Float");
                                        class$java$lang$Float = cls5;
                                    }
                                    if (cls != cls5) {
                                        if (cls != Short.TYPE) {
                                            Class cls6 = class$java$lang$Short;
                                            if (cls6 == null) {
                                                cls6 = class$("java.lang.Short");
                                                class$java$lang$Short = cls6;
                                            }
                                            if (cls != cls6) {
                                                if (cls != Byte.TYPE) {
                                                    Class cls7 = class$java$lang$Byte;
                                                    if (cls7 == null) {
                                                        cls7 = class$("java.lang.Byte");
                                                        class$java$lang$Byte = cls7;
                                                    }
                                                    if (cls != cls7) {
                                                        Class cls8 = class$java$math$BigInteger;
                                                        if (cls8 == null) {
                                                            cls8 = class$("java.math.BigInteger");
                                                            class$java$math$BigInteger = cls8;
                                                        }
                                                        return cls8.isAssignableFrom(cls) ? bigDecimal.toBigInteger() : bigDecimal;
                                                    }
                                                }
                                                return new Byte(bigDecimal.byteValue());
                                            }
                                        }
                                        return new Short(bigDecimal.shortValue());
                                    }
                                }
                                return new Float(bigDecimal.floatValue());
                            }
                        }
                        return new Long(bigDecimal.longValue());
                    }
                }
                return new Double(bigDecimal.doubleValue());
            }
        }
        return new Integer(bigDecimal.intValue());
    }

    public String toString() {
        String str;
        String propertiesString = toPropertiesString();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(ClassUtil.getShortClassNameOfObject(this));
        stringBuffer.append("@");
        stringBuffer.append(System.identityHashCode(this));
        stringBuffer.append("(");
        stringBuffer.append(this.incompatibleImprovements);
        stringBuffer.append(", ");
        if (propertiesString.length() != 0) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(propertiesString);
            stringBuffer2.append(", ...");
            str = stringBuffer2.toString();
        } else {
            str = "";
        }
        stringBuffer.append(str);
        stringBuffer.append(")");
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public String toPropertiesString() {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("simpleMapWrapper=");
        stringBuffer.append(this.simpleMapWrapper);
        stringBuffer.append(", ");
        stringBuffer.append("exposureLevel=");
        stringBuffer.append(this.classIntrospector.getExposureLevel());
        stringBuffer.append(", ");
        stringBuffer.append("exposeFields=");
        stringBuffer.append(this.classIntrospector.getExposeFields());
        stringBuffer.append(", ");
        stringBuffer.append("sharedClassIntrospCache=");
        if (this.classIntrospector.isShared()) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("@");
            stringBuffer2.append(System.identityHashCode(this.classIntrospector));
            str = stringBuffer2.toString();
        } else {
            str = "none";
        }
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    private static ClassBasedModelFactory createEnumModels(BeansWrapper beansWrapper) {
        Constructor constructor = ENUMS_MODEL_CTOR;
        if (constructor == null) {
            return null;
        }
        try {
            return (ClassBasedModelFactory) constructor.newInstance(beansWrapper);
        } catch (Exception e) {
            throw new UndeclaredThrowableException(e);
        }
    }

    private static Constructor enumsModelCtor() {
        Class cls;
        try {
            Class.forName("java.lang.Enum");
            Class<?> cls2 = Class.forName("freemarker.ext.beans._EnumModels");
            Class[] clsArr = new Class[1];
            if (class$freemarker$ext$beans$BeansWrapper == null) {
                cls = class$("freemarker.ext.beans.BeansWrapper");
                class$freemarker$ext$beans$BeansWrapper = cls;
            } else {
                cls = class$freemarker$ext$beans$BeansWrapper;
            }
            clsArr[0] = cls;
            return cls2.getDeclaredConstructor(clsArr);
        } catch (Exception unused) {
            return null;
        }
    }

    public static final class MethodAppearanceDecision {
        private PropertyDescriptor exposeAsProperty;
        private String exposeMethodAs;
        private boolean methodShadowsProperty;

        /* access modifiers changed from: package-private */
        public void setDefaults(Method method) {
            this.exposeAsProperty = null;
            this.exposeMethodAs = method.getName();
            this.methodShadowsProperty = true;
        }

        public PropertyDescriptor getExposeAsProperty() {
            return this.exposeAsProperty;
        }

        public void setExposeAsProperty(PropertyDescriptor propertyDescriptor) {
            this.exposeAsProperty = propertyDescriptor;
        }

        public String getExposeMethodAs() {
            return this.exposeMethodAs;
        }

        public void setExposeMethodAs(String str) {
            this.exposeMethodAs = str;
        }

        public boolean getMethodShadowsProperty() {
            return this.methodShadowsProperty;
        }

        public void setMethodShadowsProperty(boolean z) {
            this.methodShadowsProperty = z;
        }
    }

    public static final class MethodAppearanceDecisionInput {
        private Class containingClass;
        private Method method;

        /* access modifiers changed from: package-private */
        public void setMethod(Method method2) {
            this.method = method2;
        }

        /* access modifiers changed from: package-private */
        public void setContainingClass(Class cls) {
            this.containingClass = cls;
        }

        public Method getMethod() {
            return this.method;
        }

        public Class getContainingClass() {
            return this.containingClass;
        }
    }
}
