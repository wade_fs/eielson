package freemarker.ext.beans;

import java.beans.MethodDescriptor;

interface MethodSorter {
    MethodDescriptor[] sortMethodDescriptors(MethodDescriptor[] methodDescriptorArr);
}
