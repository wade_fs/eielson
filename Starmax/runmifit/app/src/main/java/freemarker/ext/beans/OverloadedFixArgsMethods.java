package freemarker.ext.beans;

import freemarker.template.ObjectWrapperAndUnwrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class OverloadedFixArgsMethods extends OverloadedMethodsSubset {
    /* access modifiers changed from: package-private */
    public void afterWideningUnwrappingHints(Class[] clsArr, int[] iArr) {
    }

    OverloadedFixArgsMethods(boolean z) {
        super(z);
    }

    /* access modifiers changed from: package-private */
    public Class[] preprocessParameterTypes(CallableMemberDescriptor callableMemberDescriptor) {
        return callableMemberDescriptor.getParamTypes();
    }

    /* access modifiers changed from: package-private */
    public MaybeEmptyMemberAndArguments getMemberAndArguments(List list, BeansWrapper beansWrapper) throws TemplateModelException {
        if (list == null) {
            list = Collections.EMPTY_LIST;
        }
        int size = list.size();
        Class[][] unwrappingHintsByParamCount = getUnwrappingHintsByParamCount();
        if (unwrappingHintsByParamCount.length <= size) {
            return EmptyMemberAndArguments.WRONG_NUMBER_OF_ARGUMENTS;
        }
        Class[] clsArr = unwrappingHintsByParamCount[size];
        if (clsArr == null) {
            return EmptyMemberAndArguments.WRONG_NUMBER_OF_ARGUMENTS;
        }
        Object[] objArr = new Object[size];
        int[] typeFlags = getTypeFlags(size);
        if (typeFlags == ALL_ZEROS_ARRAY) {
            typeFlags = null;
        }
        Iterator it = list.iterator();
        for (int i = 0; i < size; i++) {
            Object tryUnwrapTo = beansWrapper.tryUnwrapTo((TemplateModel) it.next(), clsArr[i], typeFlags != null ? typeFlags[i] : 0);
            if (tryUnwrapTo == ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
                return EmptyMemberAndArguments.noCompatibleOverload(i + 1);
            }
            objArr[i] = tryUnwrapTo;
        }
        MaybeEmptyCallableMemberDescriptor memberDescriptorForArgs = getMemberDescriptorForArgs(objArr, false);
        if (!(memberDescriptorForArgs instanceof CallableMemberDescriptor)) {
            return EmptyMemberAndArguments.from((EmptyCallableMemberDescriptor) memberDescriptorForArgs, objArr);
        }
        CallableMemberDescriptor callableMemberDescriptor = (CallableMemberDescriptor) memberDescriptorForArgs;
        if (!this.bugfixed) {
            BeansWrapper.coerceBigDecimals(callableMemberDescriptor.getParamTypes(), objArr);
        } else if (typeFlags != null) {
            forceNumberArgumentsToParameterTypes(objArr, callableMemberDescriptor.getParamTypes(), typeFlags);
        }
        return new MemberAndArguments(callableMemberDescriptor, objArr);
    }
}
