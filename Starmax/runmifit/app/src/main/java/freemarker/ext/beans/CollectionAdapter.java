package freemarker.ext.beans;

import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelAdapter;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import freemarker.template.utility.UndeclaredThrowableException;
import java.util.AbstractCollection;
import java.util.Iterator;

class CollectionAdapter extends AbstractCollection implements TemplateModelAdapter {
    /* access modifiers changed from: private */
    public final TemplateCollectionModel model;
    /* access modifiers changed from: private */
    public final BeansWrapper wrapper;

    CollectionAdapter(TemplateCollectionModel templateCollectionModel, BeansWrapper beansWrapper) {
        this.model = templateCollectionModel;
        this.wrapper = beansWrapper;
    }

    public TemplateModel getTemplateModel() {
        return this.model;
    }

    public int size() {
        throw new UnsupportedOperationException();
    }

    public Iterator iterator() {
        try {
            return new Iterator() {
                /* class freemarker.ext.beans.CollectionAdapter.C33221 */

                /* renamed from: i */
                final TemplateModelIterator f7465i = CollectionAdapter.this.model.iterator();

                public boolean hasNext() {
                    try {
                        return this.f7465i.hasNext();
                    } catch (TemplateModelException e) {
                        throw new UndeclaredThrowableException(e);
                    }
                }

                public Object next() {
                    try {
                        return CollectionAdapter.this.wrapper.unwrap(this.f7465i.next());
                    } catch (TemplateModelException e) {
                        throw new UndeclaredThrowableException(e);
                    }
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        } catch (TemplateModelException e) {
            throw new UndeclaredThrowableException(e);
        }
    }
}
