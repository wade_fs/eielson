package freemarker.ext.beans;

import java.lang.ref.WeakReference;
import org.zeroturnaround.javarebel.ClassEventListener;
import org.zeroturnaround.javarebel.ReloaderFactory;

class JRebelClassChangeNotifier implements ClassChangeNotifier {
    JRebelClassChangeNotifier() {
    }

    static void testAvailability() {
        ReloaderFactory.getInstance();
    }

    public void subscribe(ClassIntrospector classIntrospector) {
        ReloaderFactory.getInstance().addClassReloadListener(new ClassIntrospectorCacheInvalidator(classIntrospector));
    }

    private static class ClassIntrospectorCacheInvalidator implements ClassEventListener {
        private final WeakReference ref;

        ClassIntrospectorCacheInvalidator(ClassIntrospector classIntrospector) {
            this.ref = new WeakReference(classIntrospector);
        }

        public void onClassEvent(int i, Class cls) {
            ClassIntrospector classIntrospector = (ClassIntrospector) this.ref.get();
            if (classIntrospector == null) {
                ReloaderFactory.getInstance().removeClassReloadListener(this);
            } else if (i == 1) {
                classIntrospector.remove(cls);
            }
        }
    }
}
