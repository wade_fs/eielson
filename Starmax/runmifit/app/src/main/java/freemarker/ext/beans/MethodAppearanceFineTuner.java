package freemarker.ext.beans;

import freemarker.ext.beans.BeansWrapper;

public interface MethodAppearanceFineTuner {
    void process(BeansWrapper.MethodAppearanceDecisionInput methodAppearanceDecisionInput, BeansWrapper.MethodAppearanceDecision methodAppearanceDecision);
}
