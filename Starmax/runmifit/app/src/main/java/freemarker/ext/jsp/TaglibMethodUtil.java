package freemarker.ext.jsp;

import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.StringUtil;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class TaglibMethodUtil {
    private static final Pattern FUNCTION_PARAMETER_PATTERN = Pattern.compile("^([\\w\\.]+)(\\s*\\[\\s*\\])?$");
    private static final Pattern FUNCTION_SIGNATURE_PATTERN = Pattern.compile("^([\\w\\.]+(\\s*\\[\\s*\\])?)\\s+([\\w]+)\\s*\\((.*)\\)$");
    static /* synthetic */ Class array$B;
    static /* synthetic */ Class array$C;
    static /* synthetic */ Class array$D;
    static /* synthetic */ Class array$F;
    static /* synthetic */ Class array$I;
    static /* synthetic */ Class array$J;
    static /* synthetic */ Class array$S;
    static /* synthetic */ Class array$Z;

    private TaglibMethodUtil() {
    }

    public static Method getMethodByFunctionSignature(Class cls, String str) throws SecurityException, NoSuchMethodException, ClassNotFoundException {
        Class[] clsArr;
        Class cls2;
        Class cls3;
        Class cls4;
        Class cls5;
        Class cls6;
        Class cls7;
        Class cls8;
        Class cls9;
        Matcher matcher = FUNCTION_SIGNATURE_PATTERN.matcher(str);
        if (matcher.matches()) {
            String group = matcher.group(3);
            String trim = matcher.group(4).trim();
            if ("".equals(trim)) {
                clsArr = new Class[0];
            } else {
                String[] split = StringUtil.split(trim, ',');
                Class[] clsArr2 = new Class[split.length];
                int i = 0;
                while (i < split.length) {
                    String trim2 = split[i].trim();
                    Matcher matcher2 = FUNCTION_PARAMETER_PATTERN.matcher(trim2);
                    if (matcher2.matches()) {
                        boolean z = true;
                        String group2 = matcher2.group(1);
                        boolean z2 = group2.indexOf(46) == -1;
                        if (matcher2.group(2) == null) {
                            z = false;
                        }
                        if (z2) {
                            if ("byte".equals(group2)) {
                                if (z) {
                                    cls9 = array$B;
                                    if (cls9 == null) {
                                        cls9 = class$("[B");
                                        array$B = cls9;
                                    }
                                } else {
                                    cls9 = Byte.TYPE;
                                }
                                clsArr2[i] = cls9;
                            } else if ("short".equals(group2)) {
                                if (z) {
                                    cls8 = array$S;
                                    if (cls8 == null) {
                                        cls8 = class$("[S");
                                        array$S = cls8;
                                    }
                                } else {
                                    cls8 = Short.TYPE;
                                }
                                clsArr2[i] = cls8;
                            } else if ("int".equals(group2)) {
                                if (z) {
                                    cls7 = array$I;
                                    if (cls7 == null) {
                                        cls7 = class$("[I");
                                        array$I = cls7;
                                    }
                                } else {
                                    cls7 = Integer.TYPE;
                                }
                                clsArr2[i] = cls7;
                            } else if ("long".equals(group2)) {
                                if (z) {
                                    cls6 = array$J;
                                    if (cls6 == null) {
                                        cls6 = class$("[J");
                                        array$J = cls6;
                                    }
                                } else {
                                    cls6 = Long.TYPE;
                                }
                                clsArr2[i] = cls6;
                            } else if ("float".equals(group2)) {
                                if (z) {
                                    cls5 = array$F;
                                    if (cls5 == null) {
                                        cls5 = class$("[F");
                                        array$F = cls5;
                                    }
                                } else {
                                    cls5 = Float.TYPE;
                                }
                                clsArr2[i] = cls5;
                            } else if ("double".equals(group2)) {
                                if (z) {
                                    cls4 = array$D;
                                    if (cls4 == null) {
                                        cls4 = class$("[D");
                                        array$D = cls4;
                                    }
                                } else {
                                    cls4 = Double.TYPE;
                                }
                                clsArr2[i] = cls4;
                            } else if ("boolean".equals(group2)) {
                                if (z) {
                                    cls3 = array$Z;
                                    if (cls3 == null) {
                                        cls3 = class$("[Z");
                                        array$Z = cls3;
                                    }
                                } else {
                                    cls3 = Boolean.TYPE;
                                }
                                clsArr2[i] = cls3;
                            } else if ("char".equals(group2)) {
                                if (z) {
                                    cls2 = array$C;
                                    if (cls2 == null) {
                                        cls2 = class$("[C");
                                        array$C = cls2;
                                    }
                                } else {
                                    cls2 = Character.TYPE;
                                }
                                clsArr2[i] = cls2;
                            } else {
                                StringBuffer stringBuffer = new StringBuffer();
                                stringBuffer.append("Invalid primitive type: '");
                                stringBuffer.append(group2);
                                stringBuffer.append("'.");
                                throw new IllegalArgumentException(stringBuffer.toString());
                            }
                        } else if (z) {
                            StringBuffer stringBuffer2 = new StringBuffer();
                            stringBuffer2.append("[L");
                            stringBuffer2.append(group2);
                            stringBuffer2.append(";");
                            clsArr2[i] = ClassUtil.forName(stringBuffer2.toString());
                        } else {
                            clsArr2[i] = ClassUtil.forName(group2);
                        }
                        i++;
                    } else {
                        StringBuffer stringBuffer3 = new StringBuffer();
                        stringBuffer3.append("Invalid argument signature: '");
                        stringBuffer3.append(trim2);
                        stringBuffer3.append("'.");
                        throw new IllegalArgumentException(stringBuffer3.toString());
                    }
                }
                clsArr = clsArr2;
            }
            return cls.getMethod(group, clsArr);
        }
        throw new IllegalArgumentException("Invalid function signature.");
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
