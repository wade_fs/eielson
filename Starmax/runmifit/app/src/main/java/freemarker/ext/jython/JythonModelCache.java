package freemarker.ext.jython;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.DateModel;
import freemarker.ext.util.ModelCache;
import freemarker.template.TemplateModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.python.core.Py;
import org.python.core.PyDictionary;
import org.python.core.PyFloat;
import org.python.core.PyInteger;
import org.python.core.PyLong;
import org.python.core.PyNone;
import org.python.core.PyObject;
import org.python.core.PySequence;
import org.python.core.PyStringMap;

class JythonModelCache extends ModelCache {
    private final JythonWrapper wrapper;

    /* access modifiers changed from: protected */
    public boolean isCacheable(Object obj) {
        return true;
    }

    JythonModelCache(JythonWrapper jythonWrapper) {
        this.wrapper = jythonWrapper;
    }

    /* access modifiers changed from: protected */
    public TemplateModel create(Object obj) {
        boolean z;
        JythonVersionAdapter jythonVersionAdapter = JythonVersionAdapterHolder.INSTANCE;
        boolean z2 = false;
        if (jythonVersionAdapter.isPyInstance(obj)) {
            Object pyInstanceToJava = jythonVersionAdapter.pyInstanceToJava(obj);
            if (pyInstanceToJava instanceof TemplateModel) {
                return (TemplateModel) pyInstanceToJava;
            }
            z = pyInstanceToJava instanceof Map;
            if (pyInstanceToJava instanceof Date) {
                return new DateModel((Date) pyInstanceToJava, BeansWrapper.getDefaultInstance());
            }
            if (pyInstanceToJava instanceof Collection) {
                z2 = true;
                if (!(pyInstanceToJava instanceof List)) {
                    obj = new ArrayList((Collection) pyInstanceToJava);
                }
            }
        } else {
            z = false;
        }
        if (!(obj instanceof PyObject)) {
            obj = Py.java2py(obj);
        }
        if (z || (obj instanceof PyDictionary) || (obj instanceof PyStringMap)) {
            return JythonHashModel.FACTORY.create(obj, this.wrapper);
        }
        if (z2 || (obj instanceof PySequence)) {
            return JythonSequenceModel.FACTORY.create(obj, this.wrapper);
        }
        if ((obj instanceof PyInteger) || (obj instanceof PyLong) || (obj instanceof PyFloat)) {
            return JythonNumberModel.FACTORY.create(obj, this.wrapper);
        }
        if (obj instanceof PyNone) {
            return null;
        }
        return JythonModel.FACTORY.create(obj, this.wrapper);
    }
}
