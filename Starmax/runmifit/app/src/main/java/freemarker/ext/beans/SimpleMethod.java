package freemarker.ext.beans;

import freemarker.core._DelayedFTLTypeDescription;
import freemarker.core._DelayedOrdinal;
import freemarker.core._TemplateModelException;
import freemarker.template.ObjectWrapperAndUnwrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.ClassUtil;
import java.lang.reflect.Array;
import java.lang.reflect.Member;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class SimpleMethod {
    private final Class[] argTypes;
    private final Member member;

    protected SimpleMethod(Member member2, Class[] clsArr) {
        this.member = member2;
        this.argTypes = clsArr;
    }

    /* access modifiers changed from: package-private */
    public Object[] unwrapArguments(List list, BeansWrapper beansWrapper) throws TemplateModelException {
        List list2 = list == null ? Collections.EMPTY_LIST : list;
        boolean isVarargs = _MethodUtil.isVarargs(this.member);
        int length = this.argTypes.length;
        String str = " argument";
        if (isVarargs) {
            int i = length - 1;
            if (i > list2.size()) {
                Object[] objArr = new Object[7];
                objArr[0] = _MethodUtil.invocationErrorMessageStart(this.member);
                objArr[1] = " takes at least ";
                objArr[2] = new Integer(i);
                if (i != 1) {
                    str = " arguments";
                }
                objArr[3] = str;
                objArr[4] = ", but ";
                objArr[5] = new Integer(list2.size());
                objArr[6] = " was given.";
                throw new _TemplateModelException(objArr);
            }
        } else if (length != list2.size()) {
            Object[] objArr2 = new Object[7];
            objArr2[0] = _MethodUtil.invocationErrorMessageStart(this.member);
            objArr2[1] = " takes ";
            objArr2[2] = new Integer(length);
            if (length != 1) {
                str = " arguments";
            }
            objArr2[3] = str;
            objArr2[4] = ", but ";
            objArr2[5] = new Integer(list2.size());
            objArr2[6] = " was given.";
            throw new _TemplateModelException(objArr2);
        }
        return unwrapArguments(list2, this.argTypes, isVarargs, beansWrapper);
    }

    private Object[] unwrapArguments(List list, Class[] clsArr, boolean z, BeansWrapper beansWrapper) throws TemplateModelException {
        Object obj;
        Object tryUnwrapTo;
        if (list == null) {
            return null;
        }
        int length = clsArr.length;
        int size = list.size();
        Object[] objArr = new Object[length];
        Iterator it = list.iterator();
        int i = z ? length - 1 : length;
        int i2 = 0;
        int i3 = 0;
        while (i3 < i) {
            Class cls = clsArr[i3];
            TemplateModel templateModel = (TemplateModel) it.next();
            Object tryUnwrapTo2 = beansWrapper.tryUnwrapTo(templateModel, cls);
            if (tryUnwrapTo2 == ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
                throw createArgumentTypeMismarchException(i3, templateModel, cls);
            } else if (tryUnwrapTo2 != null || !cls.isPrimitive()) {
                objArr[i3] = tryUnwrapTo2;
                i3++;
            } else {
                throw createNullToPrimitiveArgumentException(i3, cls);
            }
        }
        if (z) {
            Class cls2 = clsArr[length - 1];
            Class<?> componentType = cls2.getComponentType();
            if (!it.hasNext()) {
                objArr[i3] = Array.newInstance(componentType, 0);
            } else {
                TemplateModel templateModel2 = (TemplateModel) it.next();
                int i4 = size - i3;
                if (i4 != 1 || (tryUnwrapTo = beansWrapper.tryUnwrapTo(templateModel2, cls2)) == ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
                    Object newInstance = Array.newInstance(componentType, i4);
                    while (i2 < i4) {
                        if (i2 == 0) {
                            obj = templateModel2;
                        } else {
                            obj = it.next();
                        }
                        TemplateModel templateModel3 = (TemplateModel) obj;
                        Object tryUnwrapTo3 = beansWrapper.tryUnwrapTo(templateModel3, componentType);
                        if (tryUnwrapTo3 == ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
                            throw createArgumentTypeMismarchException(i3 + i2, templateModel3, componentType);
                        } else if (tryUnwrapTo3 != null || !componentType.isPrimitive()) {
                            Array.set(newInstance, i2, tryUnwrapTo3);
                            i2++;
                        } else {
                            throw createNullToPrimitiveArgumentException(i3 + i2, componentType);
                        }
                    }
                    objArr[i3] = newInstance;
                } else {
                    objArr[i3] = tryUnwrapTo;
                }
            }
        }
        return objArr;
    }

    private TemplateModelException createArgumentTypeMismarchException(int i, TemplateModel templateModel, Class cls) {
        return new _TemplateModelException(new Object[]{_MethodUtil.invocationErrorMessageStart(this.member), " couldn't be called: Can't convert the ", new _DelayedOrdinal(new Integer(i + 1)), " argument's value to the target Java type, ", ClassUtil.getShortClassName(cls), ". The type of the actual value was: ", new _DelayedFTLTypeDescription(templateModel)});
    }

    private TemplateModelException createNullToPrimitiveArgumentException(int i, Class cls) {
        return new _TemplateModelException(new Object[]{_MethodUtil.invocationErrorMessageStart(this.member), " couldn't be called: The value of the ", new _DelayedOrdinal(new Integer(i + 1)), " argument was null, but the target Java parameter type (", ClassUtil.getShortClassName(cls), ") is primitive and so can't store null."});
    }

    /* access modifiers changed from: protected */
    public Member getMember() {
        return this.member;
    }
}
