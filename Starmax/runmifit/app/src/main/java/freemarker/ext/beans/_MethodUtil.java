package freemarker.ext.beans;

import freemarker.core.BugException;
import freemarker.core._DelayedConversionToString;
import freemarker.core._DelayedJQuote;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.UndeclaredThrowableException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public final class _MethodUtil {
    private static final Method CONSTRUCTOR_IS_VARARGS;
    private static final Method METHOD_IS_VARARGS;
    static /* synthetic */ Class class$java$lang$Byte;
    static /* synthetic */ Class class$java$lang$Double;
    static /* synthetic */ Class class$java$lang$Float;
    static /* synthetic */ Class class$java$lang$Integer;
    static /* synthetic */ Class class$java$lang$Long;
    static /* synthetic */ Class class$java$lang$Number;
    static /* synthetic */ Class class$java$lang$Short;
    static /* synthetic */ Class class$java$lang$reflect$Constructor;
    static /* synthetic */ Class class$java$lang$reflect$Method;

    static {
        Class cls = class$java$lang$reflect$Method;
        if (cls == null) {
            cls = class$("java.lang.reflect.Method");
            class$java$lang$reflect$Method = cls;
        }
        METHOD_IS_VARARGS = getIsVarArgsMethod(cls);
        Class cls2 = class$java$lang$reflect$Constructor;
        if (cls2 == null) {
            cls2 = class$("java.lang.reflect.Constructor");
            class$java$lang$reflect$Constructor = cls2;
        }
        CONSTRUCTOR_IS_VARARGS = getIsVarArgsMethod(cls2);
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private _MethodUtil() {
    }

    public static int isMoreOrSameSpecificParameterType(Class cls, Class cls2, boolean z, int i) {
        if (i >= 4) {
            return 0;
        }
        if (cls2.isAssignableFrom(cls)) {
            return cls2 == cls ? 1 : 4;
        }
        boolean isPrimitive = cls.isPrimitive();
        boolean isPrimitive2 = cls2.isPrimitive();
        if (isPrimitive) {
            if (isPrimitive2) {
                if (i < 3 && isWideningPrimitiveNumberConversion(cls, cls2)) {
                    return 3;
                }
                return 0;
            } else if (!z) {
                return 0;
            } else {
                Class primitiveClassToBoxingClass = ClassUtil.primitiveClassToBoxingClass(cls);
                if (primitiveClassToBoxingClass == cls2) {
                    return 2;
                }
                if (cls2.isAssignableFrom(primitiveClassToBoxingClass)) {
                    return 4;
                }
                if (i >= 3) {
                    return 0;
                }
                Class cls3 = class$java$lang$Number;
                if (cls3 == null) {
                    cls3 = class$("java.lang.Number");
                    class$java$lang$Number = cls3;
                }
                if (!cls3.isAssignableFrom(primitiveClassToBoxingClass)) {
                    return 0;
                }
                Class cls4 = class$java$lang$Number;
                if (cls4 == null) {
                    cls4 = class$("java.lang.Number");
                    class$java$lang$Number = cls4;
                }
                if (!cls4.isAssignableFrom(cls2) || !isWideningBoxedNumberConversion(primitiveClassToBoxingClass, cls2)) {
                    return 0;
                }
                return 3;
            }
        } else if (i >= 3 || !z || isPrimitive2) {
            return 0;
        } else {
            Class cls5 = class$java$lang$Number;
            if (cls5 == null) {
                cls5 = class$("java.lang.Number");
                class$java$lang$Number = cls5;
            }
            if (!cls5.isAssignableFrom(cls)) {
                return 0;
            }
            Class cls6 = class$java$lang$Number;
            if (cls6 == null) {
                cls6 = class$("java.lang.Number");
                class$java$lang$Number = cls6;
            }
            if (!cls6.isAssignableFrom(cls2) || !isWideningBoxedNumberConversion(cls, cls2)) {
                return 0;
            }
            return 3;
        }
    }

    private static boolean isWideningPrimitiveNumberConversion(Class cls, Class cls2) {
        if (cls2 == Short.TYPE && cls == Byte.TYPE) {
            return true;
        }
        if (cls2 == Integer.TYPE && (cls == Short.TYPE || cls == Byte.TYPE)) {
            return true;
        }
        if (cls2 == Long.TYPE && (cls == Integer.TYPE || cls == Short.TYPE || cls == Byte.TYPE)) {
            return true;
        }
        if (cls2 == Float.TYPE && (cls == Long.TYPE || cls == Integer.TYPE || cls == Short.TYPE || cls == Byte.TYPE)) {
            return true;
        }
        if (cls2 != Double.TYPE) {
            return false;
        }
        if (cls == Float.TYPE || cls == Long.TYPE || cls == Integer.TYPE || cls == Short.TYPE || cls == Byte.TYPE) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0042, code lost:
        if (r7 == r0) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0075, code lost:
        if (r7 == r0) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b4, code lost:
        if (r7 == r0) goto L_0x00b6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean isWideningBoxedNumberConversion(java.lang.Class r7, java.lang.Class r8) {
        /*
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Short
            java.lang.String r1 = "java.lang.Short"
            if (r0 != 0) goto L_0x000c
            java.lang.Class r0 = class$(r1)
            freemarker.ext.beans._MethodUtil.class$java$lang$Short = r0
        L_0x000c:
            r2 = 1
            java.lang.String r3 = "java.lang.Byte"
            if (r8 != r0) goto L_0x001e
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Byte
            if (r0 != 0) goto L_0x001b
            java.lang.Class r0 = class$(r3)
            freemarker.ext.beans._MethodUtil.class$java$lang$Byte = r0
        L_0x001b:
            if (r7 != r0) goto L_0x001e
            return r2
        L_0x001e:
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Integer
            java.lang.String r4 = "java.lang.Integer"
            if (r0 != 0) goto L_0x002a
            java.lang.Class r0 = class$(r4)
            freemarker.ext.beans._MethodUtil.class$java$lang$Integer = r0
        L_0x002a:
            if (r8 != r0) goto L_0x0045
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Short
            if (r0 != 0) goto L_0x0036
            java.lang.Class r0 = class$(r1)
            freemarker.ext.beans._MethodUtil.class$java$lang$Short = r0
        L_0x0036:
            if (r7 == r0) goto L_0x0044
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Byte
            if (r0 != 0) goto L_0x0042
            java.lang.Class r0 = class$(r3)
            freemarker.ext.beans._MethodUtil.class$java$lang$Byte = r0
        L_0x0042:
            if (r7 != r0) goto L_0x0045
        L_0x0044:
            return r2
        L_0x0045:
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Long
            java.lang.String r5 = "java.lang.Long"
            if (r0 != 0) goto L_0x0051
            java.lang.Class r0 = class$(r5)
            freemarker.ext.beans._MethodUtil.class$java$lang$Long = r0
        L_0x0051:
            if (r8 != r0) goto L_0x0078
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Integer
            if (r0 != 0) goto L_0x005d
            java.lang.Class r0 = class$(r4)
            freemarker.ext.beans._MethodUtil.class$java$lang$Integer = r0
        L_0x005d:
            if (r7 == r0) goto L_0x0077
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Short
            if (r0 != 0) goto L_0x0069
            java.lang.Class r0 = class$(r1)
            freemarker.ext.beans._MethodUtil.class$java$lang$Short = r0
        L_0x0069:
            if (r7 == r0) goto L_0x0077
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Byte
            if (r0 != 0) goto L_0x0075
            java.lang.Class r0 = class$(r3)
            freemarker.ext.beans._MethodUtil.class$java$lang$Byte = r0
        L_0x0075:
            if (r7 != r0) goto L_0x0078
        L_0x0077:
            return r2
        L_0x0078:
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Float
            java.lang.String r6 = "java.lang.Float"
            if (r0 != 0) goto L_0x0084
            java.lang.Class r0 = class$(r6)
            freemarker.ext.beans._MethodUtil.class$java$lang$Float = r0
        L_0x0084:
            if (r8 != r0) goto L_0x00b7
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Long
            if (r0 != 0) goto L_0x0090
            java.lang.Class r0 = class$(r5)
            freemarker.ext.beans._MethodUtil.class$java$lang$Long = r0
        L_0x0090:
            if (r7 == r0) goto L_0x00b6
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Integer
            if (r0 != 0) goto L_0x009c
            java.lang.Class r0 = class$(r4)
            freemarker.ext.beans._MethodUtil.class$java$lang$Integer = r0
        L_0x009c:
            if (r7 == r0) goto L_0x00b6
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Short
            if (r0 != 0) goto L_0x00a8
            java.lang.Class r0 = class$(r1)
            freemarker.ext.beans._MethodUtil.class$java$lang$Short = r0
        L_0x00a8:
            if (r7 == r0) goto L_0x00b6
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Byte
            if (r0 != 0) goto L_0x00b4
            java.lang.Class r0 = class$(r3)
            freemarker.ext.beans._MethodUtil.class$java$lang$Byte = r0
        L_0x00b4:
            if (r7 != r0) goto L_0x00b7
        L_0x00b6:
            return r2
        L_0x00b7:
            java.lang.Class r0 = freemarker.ext.beans._MethodUtil.class$java$lang$Double
            if (r0 != 0) goto L_0x00c3
            java.lang.String r0 = "java.lang.Double"
            java.lang.Class r0 = class$(r0)
            freemarker.ext.beans._MethodUtil.class$java$lang$Double = r0
        L_0x00c3:
            if (r8 != r0) goto L_0x0102
            java.lang.Class r8 = freemarker.ext.beans._MethodUtil.class$java$lang$Float
            if (r8 != 0) goto L_0x00cf
            java.lang.Class r8 = class$(r6)
            freemarker.ext.beans._MethodUtil.class$java$lang$Float = r8
        L_0x00cf:
            if (r7 == r8) goto L_0x0101
            java.lang.Class r8 = freemarker.ext.beans._MethodUtil.class$java$lang$Long
            if (r8 != 0) goto L_0x00db
            java.lang.Class r8 = class$(r5)
            freemarker.ext.beans._MethodUtil.class$java$lang$Long = r8
        L_0x00db:
            if (r7 == r8) goto L_0x0101
            java.lang.Class r8 = freemarker.ext.beans._MethodUtil.class$java$lang$Integer
            if (r8 != 0) goto L_0x00e7
            java.lang.Class r8 = class$(r4)
            freemarker.ext.beans._MethodUtil.class$java$lang$Integer = r8
        L_0x00e7:
            if (r7 == r8) goto L_0x0101
            java.lang.Class r8 = freemarker.ext.beans._MethodUtil.class$java$lang$Short
            if (r8 != 0) goto L_0x00f3
            java.lang.Class r8 = class$(r1)
            freemarker.ext.beans._MethodUtil.class$java$lang$Short = r8
        L_0x00f3:
            if (r7 == r8) goto L_0x0101
            java.lang.Class r8 = freemarker.ext.beans._MethodUtil.class$java$lang$Byte
            if (r8 != 0) goto L_0x00ff
            java.lang.Class r8 = class$(r3)
            freemarker.ext.beans._MethodUtil.class$java$lang$Byte = r8
        L_0x00ff:
            if (r7 != r8) goto L_0x0102
        L_0x0101:
            return r2
        L_0x0102:
            r7 = 0
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans._MethodUtil.isWideningBoxedNumberConversion(java.lang.Class, java.lang.Class):boolean");
    }

    public static Set getAssignables(Class cls, Class cls2) {
        HashSet hashSet = new HashSet();
        collectAssignables(cls, cls2, hashSet);
        return hashSet;
    }

    private static void collectAssignables(Class cls, Class cls2, Set set) {
        if (cls.isAssignableFrom(cls2)) {
            set.add(cls);
        }
        Class superclass = cls.getSuperclass();
        if (superclass != null) {
            collectAssignables(superclass, cls2, set);
        }
        for (Class<?> cls3 : cls.getInterfaces()) {
            collectAssignables(cls3, cls2, set);
        }
    }

    public static Class[] getParameterTypes(Member member) {
        if (member instanceof Method) {
            return ((Method) member).getParameterTypes();
        }
        if (member instanceof Constructor) {
            return ((Constructor) member).getParameterTypes();
        }
        throw new IllegalArgumentException("\"member\" must be Method or Constructor");
    }

    public static boolean isVarargs(Member member) {
        if (member instanceof Method) {
            return isVarargs(member, METHOD_IS_VARARGS);
        }
        if (member instanceof Constructor) {
            return isVarargs(member, CONSTRUCTOR_IS_VARARGS);
        }
        throw new BugException();
    }

    private static boolean isVarargs(Member member, Method method) {
        if (method == null) {
            return false;
        }
        try {
            return ((Boolean) method.invoke(member, null)).booleanValue();
        } catch (Exception e) {
            throw new UndeclaredThrowableException(e);
        }
    }

    private static Method getIsVarArgsMethod(Class cls) {
        try {
            return cls.getMethod("isVarArgs", null);
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    public static String toString(Member member) {
        if ((member instanceof Method) || (member instanceof Constructor)) {
            StringBuffer stringBuffer = new StringBuffer();
            if ((member.getModifiers() & 8) != 0) {
                stringBuffer.append("static ");
            }
            String shortClassName = ClassUtil.getShortClassName(member.getDeclaringClass());
            if (shortClassName != null) {
                stringBuffer.append(shortClassName);
                stringBuffer.append('.');
            }
            stringBuffer.append(member.getName());
            stringBuffer.append('(');
            Class[] parameterTypes = getParameterTypes(member);
            for (int i = 0; i < parameterTypes.length; i++) {
                if (i != 0) {
                    stringBuffer.append(", ");
                }
                String shortClassName2 = ClassUtil.getShortClassName(parameterTypes[i]);
                if (i != parameterTypes.length - 1 || !shortClassName2.endsWith("[]") || !isVarargs(member)) {
                    stringBuffer.append(shortClassName2);
                } else {
                    stringBuffer.append(shortClassName2.substring(0, shortClassName2.length() - 2));
                    stringBuffer.append("...");
                }
            }
            stringBuffer.append(')');
            return stringBuffer.toString();
        }
        throw new IllegalArgumentException("\"member\" must be a Method or Constructor");
    }

    public static Object[] invocationErrorMessageStart(Member member) {
        return invocationErrorMessageStart(member, member instanceof Constructor);
    }

    private static Object[] invocationErrorMessageStart(Object obj, boolean z) {
        Object[] objArr = new Object[3];
        objArr[0] = "Java ";
        objArr[1] = z ? "constructor " : "method ";
        objArr[2] = new _DelayedJQuote(obj);
        return objArr;
    }

    public static TemplateModelException newInvocationTemplateModelException(Object obj, Member member, Throwable th) {
        return newInvocationTemplateModelException(obj, member, (member.getModifiers() & 8) != 0, member instanceof Constructor, th);
    }

    public static TemplateModelException newInvocationTemplateModelException(Object obj, CallableMemberDescriptor callableMemberDescriptor, Throwable th) {
        return newInvocationTemplateModelException(obj, new _DelayedConversionToString(callableMemberDescriptor) {
            /* class freemarker.ext.beans._MethodUtil.C33361 */

            /* access modifiers changed from: protected */
            public String doConversion(Object obj) {
                return ((CallableMemberDescriptor) obj).getDeclaration();
            }
        }, callableMemberDescriptor.isStatic(), callableMemberDescriptor.isConstructor(), th);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v4, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v5, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static freemarker.template.TemplateModelException newInvocationTemplateModelException(java.lang.Object r6, java.lang.Object r7, boolean r8, boolean r9, java.lang.Throwable r10) {
        /*
        L_0x0000:
            boolean r0 = r10 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x000f
            r0 = r10
            java.lang.reflect.InvocationTargetException r0 = (java.lang.reflect.InvocationTargetException) r0
            java.lang.Throwable r0 = r0.getTargetException()
            if (r0 == 0) goto L_0x000f
            r10 = r0
            goto L_0x0000
        L_0x000f:
            freemarker.core._TemplateModelException r0 = new freemarker.core._TemplateModelException
            r1 = 4
            java.lang.Object[] r2 = new java.lang.Object[r1]
            java.lang.Object[] r7 = invocationErrorMessageStart(r7, r9)
            r3 = 0
            r2[r3] = r7
            r7 = 1
            java.lang.String r4 = " threw an exception"
            r2[r7] = r4
            r4 = 3
            r5 = 2
            if (r8 != 0) goto L_0x003f
            if (r9 == 0) goto L_0x0027
            goto L_0x003f
        L_0x0027:
            java.lang.Object[] r8 = new java.lang.Object[r1]
            java.lang.String r9 = " when invoked on "
            r8[r3] = r9
            java.lang.Class r9 = r6.getClass()
            r8[r7] = r9
            java.lang.String r7 = " object "
            r8[r5] = r7
            freemarker.core._DelayedJQuote r7 = new freemarker.core._DelayedJQuote
            r7.<init>(r6)
            r8[r4] = r7
            goto L_0x0041
        L_0x003f:
            java.lang.String r8 = ""
        L_0x0041:
            r2[r5] = r8
            java.lang.String r6 = "; see cause exception in the Java stack trace."
            r2[r4] = r6
            r0.<init>(r10, r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans._MethodUtil.newInvocationTemplateModelException(java.lang.Object, java.lang.Object, boolean, boolean, java.lang.Throwable):freemarker.template.TemplateModelException");
    }
}
