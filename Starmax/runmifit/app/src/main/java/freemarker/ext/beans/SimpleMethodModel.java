package freemarker.ext.beans;

import freemarker.core._UnexpectedTypeErrorExplainerTemplateModel;
import freemarker.template.SimpleNumber;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.utility.ClassUtil;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

public final class SimpleMethodModel extends SimpleMethod implements TemplateMethodModelEx, TemplateSequenceModel, _UnexpectedTypeErrorExplainerTemplateModel {
    static /* synthetic */ Class class$java$lang$Void;
    private final Object object;
    private final BeansWrapper wrapper;

    SimpleMethodModel(Object obj, Method method, Class[] clsArr, BeansWrapper beansWrapper) {
        super(method, clsArr);
        this.object = obj;
        this.wrapper = beansWrapper;
    }

    public Object exec(List list) throws TemplateModelException {
        try {
            return this.wrapper.invokeMethod(this.object, (Method) getMember(), unwrapArguments(list, this.wrapper));
        } catch (TemplateModelException e) {
            throw e;
        } catch (Exception e2) {
            throw _MethodUtil.newInvocationTemplateModelException(this.object, getMember(), e2);
        }
    }

    public TemplateModel get(int i) throws TemplateModelException {
        return (TemplateModel) exec(Collections.singletonList(new SimpleNumber(new Integer(i))));
    }

    public int size() throws TemplateModelException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Getting the number of items or enumerating the items is not supported on this ");
        stringBuffer.append(ClassUtil.getFTLTypeDescription(this));
        stringBuffer.append(" value.\n");
        stringBuffer.append("(");
        stringBuffer.append("Hint 1: Maybe you wanted to call this method first and then do something with its return value. ");
        stringBuffer.append("Hint 2: Getting items by intex possibly works, hence it's a \"+sequence\".");
        stringBuffer.append(")");
        throw new TemplateModelException(stringBuffer.toString());
    }

    public String toString() {
        return getMember().toString();
    }

    public Object[] explainTypeError(Class[] clsArr) {
        Method method;
        Class<?> returnType;
        Member member = getMember();
        if (!(!(member instanceof Method) || (returnType = (method = (Method) member).getReturnType()) == null || returnType == Void.TYPE)) {
            Class<?> cls = class$java$lang$Void;
            if (cls == null) {
                cls = class$("java.lang.Void");
                class$java$lang$Void = cls;
            }
            if (returnType != cls) {
                String name = method.getName();
                if (name.startsWith("get") && name.length() > 3 && Character.isUpperCase(name.charAt(3)) && method.getParameterTypes().length == 0) {
                    return new Object[]{"Maybe using obj.something instead of obj.getSomething will yield the desired value."};
                }
                if (name.startsWith("is") && name.length() > 2 && Character.isUpperCase(name.charAt(2)) && method.getParameterTypes().length == 0) {
                    return new Object[]{"Maybe using obj.something instead of obj.isSomething will yield the desired value."};
                }
                Object[] objArr = new Object[3];
                objArr[0] = "Maybe using obj.something(";
                objArr[1] = method.getParameterTypes().length != 0 ? "params" : "";
                objArr[2] = ") instead of obj.something will yield the desired value";
                return objArr;
            }
        }
        return null;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }
}
