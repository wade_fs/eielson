package freemarker.ext.servlet;

import com.baidu.mobstat.Config;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.util.FileUtil;
import freemarker.cache.TemplateLoader;
import freemarker.ext.jsp.TaglibFactory;
import freemarker.log.Logger;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNotFoundException;
import freemarker.template.utility.StringUtil;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import javax.servlet.GenericServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import no.nordicsemi.android.dfu.BuildConfig;

public class FreemarkerServlet extends HttpServlet {
    private static final String ATTR_APPLICATION_MODEL = ".freemarker.Application";
    private static final String ATTR_JETTY_CP_TAGLIB_JAR_PATTERNS = "org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern";
    private static final String ATTR_JSP_TAGLIBS_MODEL = ".freemarker.JspTaglibs";
    private static final String ATTR_REQUEST_MODEL = ".freemarker.Request";
    private static final String ATTR_REQUEST_PARAMETERS_MODEL = ".freemarker.RequestParameters";
    private static final String ATTR_SESSION_MODEL = ".freemarker.Session";
    private static final String DEFAULT_CONTENT_TYPE = "text/html";
    private static final String DEPR_INITPARAM_DEBUG = "debug";
    private static final String DEPR_INITPARAM_ENCODING = "DefaultEncoding";
    private static final String DEPR_INITPARAM_OBJECT_WRAPPER = "ObjectWrapper";
    private static final String DEPR_INITPARAM_TEMPLATE_DELAY = "TemplateDelay";
    private static final String DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER = "TemplateExceptionHandler";
    private static final String DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER_DEBUG = "debug";
    private static final String DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER_HTML_DEBUG = "htmlDebug";
    private static final String DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER_IGNORE = "ignore";
    private static final String DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER_RETHROW = "rethrow";
    private static final String DEPR_INITPARAM_WRAPPER_BEANS = "beans";
    private static final String DEPR_INITPARAM_WRAPPER_JYTHON = "jython";
    private static final String DEPR_INITPARAM_WRAPPER_SIMPLE = "simple";
    private static final String EXPIRATION_DATE;
    public static final String INIT_PARAM_BUFFER_SIZE = "BufferSize";
    public static final String INIT_PARAM_CLASSPATH_TLDS = "ClasspathTlds";
    public static final String INIT_PARAM_CONTENT_TYPE = "ContentType";
    private static final String INIT_PARAM_DEBUG = "Debug";
    public static final String INIT_PARAM_EXCEPTION_ON_MISSING_TEMPLATE = "ExceptionOnMissingTemplate";
    public static final String INIT_PARAM_META_INF_TLD_LOCATIONS = "MetaInfTldSources";
    public static final String INIT_PARAM_NO_CACHE = "NoCache";
    public static final String INIT_PARAM_TEMPLATE_PATH = "TemplatePath";
    public static final String KEY_APPLICATION = "Application";
    public static final String KEY_APPLICATION_PRIVATE = "__FreeMarkerServlet.Application__";
    public static final String KEY_INCLUDE = "include_page";
    public static final String KEY_JSP_TAGLIBS = "JspTaglibs";
    public static final String KEY_REQUEST = "Request";
    public static final String KEY_REQUEST_PARAMETERS = "RequestParameters";
    public static final String KEY_REQUEST_PRIVATE = "__FreeMarkerServlet.Request__";
    public static final String KEY_SESSION = "Session";
    private static final Logger LOG = Logger.getLogger("freemarker.servlet");
    private static final Logger LOG_RT = Logger.getLogger("freemarker.runtime");
    public static final String META_INF_TLD_LOCATION_CLASSPATH = "classpath";
    public static final String META_INF_TLD_LOCATION_CLEAR = "clear";
    public static final String META_INF_TLD_LOCATION_WEB_INF_PER_LIB_JARS = "webInfPerLibJars";
    public static final String SYSTEM_PROPERTY_CLASSPATH_TLDS = "org.freemarker.jsp.classpathTlds";
    public static final String SYSTEM_PROPERTY_META_INF_TLD_SOURCES = "org.freemarker.jsp.metaInfTldSources";
    static /* synthetic */ Class class$freemarker$ext$servlet$FreemarkerServlet = null;
    public static final long serialVersionUID = -2440216393145762479L;
    private Integer bufferSize;
    private List classpathTlds;
    private Configuration config;
    private String contentType;
    protected boolean debug;
    private boolean exceptionOnMissingTemplate;
    private Object lazyInitFieldsLock = new Object();
    private List metaInfTldSources;
    private boolean noCache;
    private boolean noCharsetInContentType;
    private boolean objectWrapperMismatchWarnLogged;
    private ServletContextHashModel servletContextModel;
    private TaglibFactory taglibFactory;
    private String templatePath;
    private ObjectWrapper wrapper;

    /* access modifiers changed from: protected */
    public void initializeServletContext(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
    }

    /* access modifiers changed from: protected */
    public void initializeSession(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
    }

    /* access modifiers changed from: protected */
    public void postTemplateProcess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Template template, TemplateModel templateModel) throws ServletException, IOException {
    }

    /* access modifiers changed from: protected */
    public boolean preTemplateProcess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Template template, TemplateModel templateModel) throws ServletException, IOException {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean preprocessRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        return false;
    }

    /* access modifiers changed from: protected */
    public void setConfigurationDefaults() {
    }

    static {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.roll(1, -1);
        EXPIRATION_DATE = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US).format(gregorianCalendar.getTime());
    }

    public void init() throws ServletException {
        try {
            initialize();
        } catch (Exception e) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Error while initializing ");
            stringBuffer.append(getClass().getName());
            stringBuffer.append(" servlet; see cause exception.");
            throw new ServletException(stringBuffer.toString(), e);
        }
    }

    private void initialize() throws InitParamValueException, MalformedWebXmlException, ConflictingInitParamsException {
        this.config = createConfiguration();
        String initParameter = getInitParameter("incompatible_improvements");
        if (initParameter != null) {
            try {
                this.config.setSetting("incompatible_improvements", initParameter);
            } catch (Exception e) {
                throw new InitParamValueException("incompatible_improvements", initParameter, e);
            }
        }
        if (!this.config.isTemplateExceptionHandlerExplicitlySet()) {
            this.config.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        }
        if (!this.config.isLogTemplateExceptionsExplicitlySet()) {
            this.config.setLogTemplateExceptions(false);
        }
        this.contentType = DEFAULT_CONTENT_TYPE;
        this.wrapper = createObjectWrapper();
        if (LOG.isDebugEnabled()) {
            Logger logger = LOG;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Using object wrapper: ");
            stringBuffer.append(this.wrapper);
            logger.debug(stringBuffer.toString());
        }
        this.config.setObjectWrapper(this.wrapper);
        this.templatePath = getInitParameter(INIT_PARAM_TEMPLATE_PATH);
        if (this.templatePath == null && !this.config.isTemplateLoaderExplicitlySet()) {
            this.templatePath = "class://";
        }
        String str = this.templatePath;
        if (str != null) {
            try {
                this.config.setTemplateLoader(createTemplateLoader(str));
            } catch (Exception e2) {
                throw new InitParamValueException(INIT_PARAM_TEMPLATE_PATH, this.templatePath, e2);
            }
        }
        this.metaInfTldSources = createDefaultMetaInfTldSources();
        this.classpathTlds = createDefaultClassPathTlds();
        Enumeration initParameterNames = getServletConfig().getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            String str2 = (String) initParameterNames.nextElement();
            String initParameter2 = getInitParameter(str2);
            if (str2 == null) {
                throw new MalformedWebXmlException("init-param without param-name. Maybe the web.xml is not well-formed?");
            } else if (initParameter2 != null) {
                try {
                    if (!str2.equals(DEPR_INITPARAM_OBJECT_WRAPPER) && !str2.equals("object_wrapper") && !str2.equals(INIT_PARAM_TEMPLATE_PATH)) {
                        if (!str2.equals("incompatible_improvements")) {
                            if (str2.equals(DEPR_INITPARAM_ENCODING)) {
                                if (getInitParameter("default_encoding") == null) {
                                    this.config.setDefaultEncoding(initParameter2);
                                } else {
                                    throw new ConflictingInitParamsException("default_encoding", DEPR_INITPARAM_ENCODING);
                                }
                            } else if (str2.equals(DEPR_INITPARAM_TEMPLATE_DELAY)) {
                                if (getInitParameter("template_update_delay") == null) {
                                    try {
                                        this.config.setTemplateUpdateDelay(Integer.parseInt(initParameter2));
                                    } catch (NumberFormatException unused) {
                                    }
                                } else {
                                    throw new ConflictingInitParamsException("template_update_delay", DEPR_INITPARAM_TEMPLATE_DELAY);
                                }
                            } else if (str2.equals(DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER)) {
                                if (getInitParameter("template_exception_handler") != null) {
                                    throw new ConflictingInitParamsException("template_exception_handler", DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER);
                                } else if (DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER_RETHROW.equals(initParameter2)) {
                                    this.config.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
                                } else if (BuildConfig.BUILD_TYPE.equals(initParameter2)) {
                                    this.config.setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER);
                                } else if (DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER_HTML_DEBUG.equals(initParameter2)) {
                                    this.config.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
                                } else if (DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER_IGNORE.equals(initParameter2)) {
                                    this.config.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
                                } else {
                                    throw new InitParamValueException(DEPR_INITPARAM_TEMPLATE_EXCEPTION_HANDLER, initParameter2, "Not one of the supported values.");
                                }
                            } else if (str2.equals(INIT_PARAM_NO_CACHE)) {
                                this.noCache = StringUtil.getYesNo(initParameter2);
                            } else if (str2.equals(INIT_PARAM_BUFFER_SIZE)) {
                                this.bufferSize = new Integer(parseSize(initParameter2));
                            } else if (str2.equals(BuildConfig.BUILD_TYPE)) {
                                if (getInitParameter(INIT_PARAM_DEBUG) == null) {
                                    this.debug = StringUtil.getYesNo(initParameter2);
                                } else {
                                    throw new ConflictingInitParamsException(INIT_PARAM_DEBUG, BuildConfig.BUILD_TYPE);
                                }
                            } else if (str2.equals(INIT_PARAM_DEBUG)) {
                                this.debug = StringUtil.getYesNo(initParameter2);
                            } else if (str2.equals(INIT_PARAM_CONTENT_TYPE)) {
                                this.contentType = initParameter2;
                            } else if (str2.equals(INIT_PARAM_EXCEPTION_ON_MISSING_TEMPLATE)) {
                                this.exceptionOnMissingTemplate = StringUtil.getYesNo(initParameter2);
                            } else if (str2.equals(INIT_PARAM_META_INF_TLD_LOCATIONS)) {
                                this.metaInfTldSources = parseAsMetaInfTldLocations(initParameter2);
                            } else if (str2.equals(INIT_PARAM_CLASSPATH_TLDS)) {
                                ArrayList arrayList = new ArrayList();
                                if (this.classpathTlds != null) {
                                    arrayList.addAll(this.classpathTlds);
                                }
                                arrayList.addAll(InitParamParser.parseCommaSeparatedList(initParameter2));
                                this.classpathTlds = arrayList;
                            } else {
                                this.config.setSetting(str2, initParameter2);
                            }
                        }
                    }
                } catch (ConflictingInitParamsException e3) {
                    throw e3;
                } catch (Exception e4) {
                    throw new InitParamValueException(str2, initParameter2, e4);
                }
            } else {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("init-param ");
                stringBuffer2.append(StringUtil.jQuote(str2));
                stringBuffer2.append(" without param-value. ");
                stringBuffer2.append("Maybe the web.xml is not well-formed?");
                throw new MalformedWebXmlException(stringBuffer2.toString());
            }
        }
        this.noCharsetInContentType = true;
        int indexOf = this.contentType.toLowerCase().indexOf("charset=");
        if (indexOf != -1) {
            char c = ' ';
            int i = indexOf - 1;
            while (i >= 0) {
                c = this.contentType.charAt(i);
                if (!Character.isWhitespace(c)) {
                    break;
                }
                i--;
            }
            if (i == -1 || c == ';') {
                this.noCharsetInContentType = false;
            }
        }
    }

    private List parseAsMetaInfTldLocations(String str) throws ParseException {
        Object obj;
        ArrayList arrayList = null;
        for (String str2 : InitParamParser.parseCommaSeparatedList(str)) {
            if (str2.equals(META_INF_TLD_LOCATION_WEB_INF_PER_LIB_JARS)) {
                obj = TaglibFactory.WebInfPerLibJarMetaInfTldSource.INSTANCE;
            } else if (str2.startsWith(META_INF_TLD_LOCATION_CLASSPATH)) {
                String trim = str2.substring(9).trim();
                if (trim.length() == 0) {
                    obj = new TaglibFactory.ClasspathMetaInfTldSource(Pattern.compile(".*", 32));
                } else if (trim.startsWith(Config.TRACE_TODAY_VISIT_SPLIT)) {
                    String trim2 = trim.substring(1).trim();
                    if (trim2.length() != 0) {
                        obj = new TaglibFactory.ClasspathMetaInfTldSource(Pattern.compile(trim2));
                    } else {
                        throw new ParseException("Empty regular expression after \"classpath:\"", -1);
                    }
                } else {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Invalid \"classpath\" value syntax: ");
                    stringBuffer.append(str);
                    throw new ParseException(stringBuffer.toString(), -1);
                }
            } else if (str2.startsWith(META_INF_TLD_LOCATION_CLEAR)) {
                obj = TaglibFactory.ClearMetaInfTldSource.INSTANCE;
            } else {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("Item has no recognized source type prefix: ");
                stringBuffer2.append(str2);
                throw new ParseException(stringBuffer2.toString(), -1);
            }
            if (arrayList == null) {
                arrayList = new ArrayList();
            }
            arrayList.add(obj);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public TemplateLoader createTemplateLoader(String str) throws IOException {
        return InitParamParser.createTemplateLoader(str, getConfiguration(), getClass(), getServletContext());
    }

    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        process(httpServletRequest, httpServletResponse);
    }

    public void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        process(httpServletRequest, httpServletResponse);
    }

    private void process(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        TemplateModel createModel;
        if (!preprocessRequest(httpServletRequest, httpServletResponse)) {
            if (this.bufferSize != null && !httpServletResponse.isCommitted()) {
                try {
                    httpServletResponse.setBufferSize(this.bufferSize.intValue());
                } catch (IllegalStateException e) {
                    LOG.debug("Can't set buffer size any more,", e);
                }
            }
            String requestUrlToTemplatePath = requestUrlToTemplatePath(httpServletRequest);
            if (LOG.isDebugEnabled()) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Requested template ");
                stringBuffer.append(StringUtil.jQuoteNoXSS(requestUrlToTemplatePath));
                stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                logger.debug(stringBuffer.toString());
            }
            try {
                Template template = this.config.getTemplate(requestUrlToTemplatePath, deduceLocale(requestUrlToTemplatePath, httpServletRequest, httpServletResponse));
                Object customAttribute = template.getCustomAttribute("content_type");
                if (customAttribute != null) {
                    httpServletResponse.setContentType(customAttribute.toString());
                } else if (this.noCharsetInContentType) {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append(this.contentType);
                    stringBuffer2.append("; charset=");
                    stringBuffer2.append(template.getEncoding());
                    httpServletResponse.setContentType(stringBuffer2.toString());
                } else {
                    httpServletResponse.setContentType(this.contentType);
                }
                setBrowserCachingPolicy(httpServletResponse);
                ServletContext servletContext = getServletContext();
                try {
                    logWarnOnObjectWrapperMismatch();
                    createModel = createModel(this.wrapper, servletContext, httpServletRequest, httpServletResponse);
                    if (preTemplateProcess(httpServletRequest, httpServletResponse, template, createModel)) {
                        template.process(createModel, httpServletResponse.getWriter());
                        postTemplateProcess(httpServletRequest, httpServletResponse, template, createModel);
                    }
                } catch (TemplateException e2) {
                    TemplateExceptionHandler templateExceptionHandler = this.config.getTemplateExceptionHandler();
                    if (templateExceptionHandler == TemplateExceptionHandler.HTML_DEBUG_HANDLER || templateExceptionHandler == TemplateExceptionHandler.DEBUG_HANDLER || templateExceptionHandler.getClass().getName().indexOf(INIT_PARAM_DEBUG) != -1) {
                        httpServletResponse.flushBuffer();
                    }
                    throw newServletExceptionWithFreeMarkerLogging("Error executing FreeMarker template", e2);
                } catch (Throwable th) {
                    postTemplateProcess(httpServletRequest, httpServletResponse, template, createModel);
                    throw th;
                }
            } catch (TemplateNotFoundException e3) {
                if (!this.exceptionOnMissingTemplate) {
                    if (LOG.isDebugEnabled()) {
                        Logger logger2 = LOG;
                        StringBuffer stringBuffer3 = new StringBuffer();
                        stringBuffer3.append("Responding HTTP 404 \"Not found\" for missing template ");
                        stringBuffer3.append(StringUtil.jQuoteNoXSS(requestUrlToTemplatePath));
                        stringBuffer3.append(FileUtil.HIDDEN_PREFIX);
                        logger2.debug(stringBuffer3.toString(), e3);
                    }
                    httpServletResponse.sendError(404, "Page template not found");
                    return;
                }
                StringBuffer stringBuffer4 = new StringBuffer();
                stringBuffer4.append("Template not found for name ");
                stringBuffer4.append(StringUtil.jQuoteNoXSS(requestUrlToTemplatePath));
                stringBuffer4.append(FileUtil.HIDDEN_PREFIX);
                throw newServletExceptionWithFreeMarkerLogging(stringBuffer4.toString(), e3);
            } catch (freemarker.core.ParseException e4) {
                StringBuffer stringBuffer5 = new StringBuffer();
                stringBuffer5.append("Parsing error with template ");
                stringBuffer5.append(StringUtil.jQuoteNoXSS(requestUrlToTemplatePath));
                stringBuffer5.append(FileUtil.HIDDEN_PREFIX);
                throw newServletExceptionWithFreeMarkerLogging(stringBuffer5.toString(), e4);
            } catch (Exception e5) {
                StringBuffer stringBuffer6 = new StringBuffer();
                stringBuffer6.append("Unexpected error when loading template ");
                stringBuffer6.append(StringUtil.jQuoteNoXSS(requestUrlToTemplatePath));
                stringBuffer6.append(FileUtil.HIDDEN_PREFIX);
                throw newServletExceptionWithFreeMarkerLogging(stringBuffer6.toString(), e5);
            }
        }
    }

    private ServletException newServletExceptionWithFreeMarkerLogging(String str, Throwable th) throws ServletException {
        if (th instanceof TemplateException) {
            LOG_RT.error(str, th);
        } else {
            LOG.error(str, th);
        }
        ServletException servletException = new ServletException(str, th);
        try {
            servletException.initCause(th);
        } catch (Exception unused) {
        }
        throw servletException;
    }

    private void logWarnOnObjectWrapperMismatch() {
        boolean z;
        if (this.wrapper != this.config.getObjectWrapper() && !this.objectWrapperMismatchWarnLogged && LOG.isWarnEnabled()) {
            synchronized (this) {
                z = !this.objectWrapperMismatchWarnLogged;
                if (z) {
                    this.objectWrapperMismatchWarnLogged = true;
                }
            }
            if (z) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(getClass().getName());
                stringBuffer.append(".wrapper != config.getObjectWrapper(); possibly the result of incorrect extension of ");
                Class cls = class$freemarker$ext$servlet$FreemarkerServlet;
                if (cls == null) {
                    cls = class$("freemarker.ext.servlet.FreemarkerServlet");
                    class$freemarker$ext$servlet$FreemarkerServlet = cls;
                }
                stringBuffer.append(cls.getName());
                stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                logger.warn(stringBuffer.toString());
            }
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* access modifiers changed from: protected */
    public Locale deduceLocale(String str, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException {
        return this.config.getLocale();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.ext.servlet.ServletContextHashModel.<init>(javax.servlet.GenericServlet, freemarker.template.ObjectWrapper):void
     arg types: [freemarker.ext.servlet.FreemarkerServlet, freemarker.template.ObjectWrapper]
     candidates:
      freemarker.ext.servlet.ServletContextHashModel.<init>(javax.servlet.ServletContext, freemarker.template.ObjectWrapper):void
      freemarker.ext.servlet.ServletContextHashModel.<init>(javax.servlet.GenericServlet, freemarker.template.ObjectWrapper):void */
    /* access modifiers changed from: protected */
    public TemplateModel createModel(ObjectWrapper objectWrapper, ServletContext servletContext, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws TemplateModelException {
        TaglibFactory taglibFactory2;
        ServletContextHashModel servletContextHashModel;
        HttpSessionHashModel httpSessionHashModel;
        try {
            AllHttpScopesHashModel allHttpScopesHashModel = new AllHttpScopesHashModel(objectWrapper, servletContext, httpServletRequest);
            synchronized (this.lazyInitFieldsLock) {
                if (this.servletContextModel == null) {
                    servletContextHashModel = new ServletContextHashModel((GenericServlet) this, objectWrapper);
                    taglibFactory2 = createTaglibFactory(objectWrapper, servletContext);
                    servletContext.setAttribute(ATTR_APPLICATION_MODEL, servletContextHashModel);
                    servletContext.setAttribute(ATTR_JSP_TAGLIBS_MODEL, taglibFactory2);
                    initializeServletContext(httpServletRequest, httpServletResponse);
                    this.taglibFactory = taglibFactory2;
                    this.servletContextModel = servletContextHashModel;
                } else {
                    servletContextHashModel = this.servletContextModel;
                    taglibFactory2 = this.taglibFactory;
                }
            }
            allHttpScopesHashModel.putUnlistedModel(KEY_APPLICATION, servletContextHashModel);
            allHttpScopesHashModel.putUnlistedModel(KEY_APPLICATION_PRIVATE, servletContextHashModel);
            allHttpScopesHashModel.putUnlistedModel(KEY_JSP_TAGLIBS, taglibFactory2);
            HttpSession session = httpServletRequest.getSession(false);
            if (session != null) {
                httpSessionHashModel = (HttpSessionHashModel) session.getAttribute(ATTR_SESSION_MODEL);
                if (httpSessionHashModel == null || httpSessionHashModel.isOrphaned(session)) {
                    httpSessionHashModel = new HttpSessionHashModel(session, objectWrapper);
                    initializeSessionAndInstallModel(httpServletRequest, httpServletResponse, httpSessionHashModel, session);
                }
            } else {
                httpSessionHashModel = new HttpSessionHashModel(this, httpServletRequest, httpServletResponse, objectWrapper);
            }
            allHttpScopesHashModel.putUnlistedModel(KEY_SESSION, httpSessionHashModel);
            HttpRequestHashModel httpRequestHashModel = (HttpRequestHashModel) httpServletRequest.getAttribute(ATTR_REQUEST_MODEL);
            if (httpRequestHashModel == null || httpRequestHashModel.getRequest() != httpServletRequest) {
                httpRequestHashModel = new HttpRequestHashModel(httpServletRequest, httpServletResponse, objectWrapper);
                httpServletRequest.setAttribute(ATTR_REQUEST_MODEL, httpRequestHashModel);
                httpServletRequest.setAttribute(ATTR_REQUEST_PARAMETERS_MODEL, createRequestParametersHashModel(httpServletRequest));
            }
            allHttpScopesHashModel.putUnlistedModel(KEY_REQUEST, httpRequestHashModel);
            allHttpScopesHashModel.putUnlistedModel(KEY_INCLUDE, new IncludePage(httpServletRequest, httpServletResponse));
            allHttpScopesHashModel.putUnlistedModel(KEY_REQUEST_PRIVATE, httpRequestHashModel);
            allHttpScopesHashModel.putUnlistedModel(KEY_REQUEST_PARAMETERS, (HttpRequestParametersHashModel) httpServletRequest.getAttribute(ATTR_REQUEST_PARAMETERS_MODEL));
            return allHttpScopesHashModel;
        } catch (ServletException e) {
            throw new TemplateModelException((Exception) e);
        } catch (IOException e2) {
            throw new TemplateModelException((Exception) e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Exception):void
     arg types: [java.lang.String, java.text.ParseException]
     candidates:
      freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Throwable):void
      freemarker.template.TemplateModelException.<init>(java.lang.String, java.lang.Exception):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007a A[SYNTHETIC, Splitter:B:30:0x007a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public freemarker.ext.jsp.TaglibFactory createTaglibFactory(freemarker.template.ObjectWrapper r5, javax.servlet.ServletContext r6) throws freemarker.template.TemplateModelException {
        /*
            r4 = this;
            freemarker.ext.jsp.TaglibFactory r0 = new freemarker.ext.jsp.TaglibFactory
            r0.<init>(r6)
            r0.setObjectWrapper(r5)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.List r1 = r4.metaInfTldSources
            if (r1 == 0) goto L_0x0014
            r5.addAll(r1)
        L_0x0014:
            r1 = 0
            java.lang.String r2 = "org.freemarker.jsp.metaInfTldSources"
            java.lang.String r2 = freemarker.template.utility.SecurityUtilities.getSystemProperty(r2, r1)
            if (r2 == 0) goto L_0x0030
            java.util.List r2 = r4.parseAsMetaInfTldLocations(r2)     // Catch:{ ParseException -> 0x0027 }
            if (r2 == 0) goto L_0x0030
            r5.addAll(r2)     // Catch:{ ParseException -> 0x0027 }
            goto L_0x0030
        L_0x0027:
            r5 = move-exception
            freemarker.template.TemplateModelException r6 = new freemarker.template.TemplateModelException
            java.lang.String r0 = "Failed to parse system property \"org.freemarker.jsp.metaInfTldSources\""
            r6.<init>(r0, r5)
            throw r6
        L_0x0030:
            java.lang.String r2 = "org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern"
            java.lang.Object r6 = r6.getAttribute(r2)     // Catch:{ Exception -> 0x003f }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x003f }
            if (r6 == 0) goto L_0x0047
            java.util.List r6 = freemarker.ext.servlet.InitParamParser.parseCommaSeparatedPatterns(r6)     // Catch:{ Exception -> 0x003f }
            goto L_0x0048
        L_0x003f:
            r6 = move-exception
            freemarker.log.Logger r2 = freemarker.ext.servlet.FreemarkerServlet.LOG
            java.lang.String r3 = "Failed to parse application context attribute \"org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern\" - it will be ignored"
            r2.error(r3, r6)
        L_0x0047:
            r6 = r1
        L_0x0048:
            if (r6 == 0) goto L_0x0063
            java.util.Iterator r6 = r6.iterator()
        L_0x004e:
            boolean r2 = r6.hasNext()
            if (r2 == 0) goto L_0x0063
            java.lang.Object r2 = r6.next()
            java.util.regex.Pattern r2 = (java.util.regex.Pattern) r2
            freemarker.ext.jsp.TaglibFactory$ClasspathMetaInfTldSource r3 = new freemarker.ext.jsp.TaglibFactory$ClasspathMetaInfTldSource
            r3.<init>(r2)
            r5.add(r3)
            goto L_0x004e
        L_0x0063:
            r0.setMetaInfTldSources(r5)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.List r6 = r4.classpathTlds
            if (r6 == 0) goto L_0x0072
            r5.addAll(r6)
        L_0x0072:
            java.lang.String r6 = "org.freemarker.jsp.classpathTlds"
            java.lang.String r6 = freemarker.template.utility.SecurityUtilities.getSystemProperty(r6, r1)
            if (r6 == 0) goto L_0x008d
            java.util.List r6 = freemarker.ext.servlet.InitParamParser.parseCommaSeparatedList(r6)     // Catch:{ ParseException -> 0x0084 }
            if (r6 == 0) goto L_0x008d
            r5.addAll(r6)     // Catch:{ ParseException -> 0x0084 }
            goto L_0x008d
        L_0x0084:
            r5 = move-exception
            freemarker.template.TemplateModelException r6 = new freemarker.template.TemplateModelException
            java.lang.String r0 = "Failed to parse system property \"org.freemarker.jsp.classpathTlds\""
            r6.<init>(r0, r5)
            throw r6
        L_0x008d:
            r0.setClasspathTlds(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.servlet.FreemarkerServlet.createTaglibFactory(freemarker.template.ObjectWrapper, javax.servlet.ServletContext):freemarker.ext.jsp.TaglibFactory");
    }

    /* access modifiers changed from: protected */
    public List createDefaultClassPathTlds() {
        return TaglibFactory.DEFAULT_CLASSPATH_TLDS;
    }

    /* access modifiers changed from: protected */
    public List createDefaultMetaInfTldSources() {
        return TaglibFactory.DEFAULT_META_INF_TLD_SOURCES;
    }

    /* access modifiers changed from: package-private */
    public void initializeSessionAndInstallModel(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, HttpSessionHashModel httpSessionHashModel, HttpSession httpSession) throws ServletException, IOException {
        httpSession.setAttribute(ATTR_SESSION_MODEL, httpSessionHashModel);
        initializeSession(httpServletRequest, httpServletResponse);
    }

    /* access modifiers changed from: protected */
    public String requestUrlToTemplatePath(HttpServletRequest httpServletRequest) throws ServletException {
        String str = (String) httpServletRequest.getAttribute("javax.servlet.include.servlet_path");
        if (str != null) {
            String str2 = (String) httpServletRequest.getAttribute("javax.servlet.include.path_info");
            return str2 == null ? str : str2;
        }
        String pathInfo = httpServletRequest.getPathInfo();
        if (pathInfo != null) {
            return pathInfo;
        }
        String servletPath = httpServletRequest.getServletPath();
        return servletPath != null ? servletPath : "";
    }

    /* access modifiers changed from: protected */
    public Configuration createConfiguration() {
        return new Configuration();
    }

    /* access modifiers changed from: protected */
    public ObjectWrapper createObjectWrapper() {
        String initParameter = getServletConfig().getInitParameter(DEPR_INITPARAM_OBJECT_WRAPPER);
        if (initParameter == null) {
            String initParameter2 = getInitParameter("object_wrapper");
            if (initParameter2 != null) {
                try {
                    this.config.setSetting("object_wrapper", initParameter2);
                    return this.config.getObjectWrapper();
                } catch (TemplateException e) {
                    throw new RuntimeException("Failed to set object_wrapper", e);
                }
            } else if (!this.config.isObjectWrapperExplicitlySet()) {
                return createDefaultObjectWrapper();
            } else {
                return this.config.getObjectWrapper();
            }
        } else if (getInitParameter("object_wrapper") != null) {
            throw new RuntimeException("Conflicting init-params: object_wrapper and ObjectWrapper");
        } else if (DEPR_INITPARAM_WRAPPER_BEANS.equals(initParameter)) {
            return ObjectWrapper.BEANS_WRAPPER;
        } else {
            if (DEPR_INITPARAM_WRAPPER_SIMPLE.equals(initParameter)) {
                return ObjectWrapper.SIMPLE_WRAPPER;
            }
            if (!DEPR_INITPARAM_WRAPPER_JYTHON.equals(initParameter)) {
                return createDefaultObjectWrapper();
            }
            try {
                return (ObjectWrapper) Class.forName("freemarker.ext.jython.JythonWrapper").newInstance();
            } catch (InstantiationException e2) {
                throw new InstantiationError(e2.getMessage());
            } catch (IllegalAccessException e3) {
                throw new IllegalAccessError(e3.getMessage());
            } catch (ClassNotFoundException e4) {
                throw new NoClassDefFoundError(e4.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public ObjectWrapper createDefaultObjectWrapper() {
        return Configuration.getDefaultObjectWrapper(this.config.getIncompatibleImprovements());
    }

    /* access modifiers changed from: protected */
    public ObjectWrapper getObjectWrapper() {
        return this.wrapper;
    }

    /* access modifiers changed from: protected */
    public final String getTemplatePath() {
        return this.templatePath;
    }

    /* access modifiers changed from: protected */
    public HttpRequestParametersHashModel createRequestParametersHashModel(HttpServletRequest httpServletRequest) {
        return new HttpRequestParametersHashModel(httpServletRequest);
    }

    /* access modifiers changed from: protected */
    public Configuration getConfiguration() {
        return this.config;
    }

    private void setBrowserCachingPolicy(HttpServletResponse httpServletResponse) {
        if (this.noCache) {
            httpServletResponse.setHeader(HttpHeaders.CACHE_CONTROL, "no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
            httpServletResponse.setHeader(HttpHeaders.PRAGMA, "no-cache");
            httpServletResponse.setHeader(HttpHeaders.EXPIRES, EXPIRATION_DATE);
        }
    }

    private int parseSize(String str) throws ParseException {
        int i = 1;
        int length = str.length() - 1;
        while (length >= 0) {
            char charAt = str.charAt(length);
            if (charAt >= '0' && charAt <= '9') {
                break;
            }
            length--;
        }
        int i2 = length + 1;
        int parseInt = Integer.parseInt(str.substring(0, i2).trim());
        String upperCase = str.substring(i2).trim().toUpperCase();
        if (upperCase.length() != 0 && !upperCase.equals("B")) {
            if (upperCase.equals("K") || upperCase.equals("KB") || upperCase.equals("KIB")) {
                i = 1024;
            } else if (upperCase.equals("M") || upperCase.equals("MB") || upperCase.equals("MIB")) {
                i = 1048576;
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Unknown unit: ");
                stringBuffer.append(upperCase);
                throw new ParseException(stringBuffer.toString(), i2);
            }
        }
        long j = ((long) parseInt) * ((long) i);
        if (j < 0) {
            throw new IllegalArgumentException("Buffer size can't be negative");
        } else if (j <= 2147483647L) {
            return (int) j;
        } else {
            throw new IllegalArgumentException("Buffer size can't bigger than 2147483647");
        }
    }

    private static class InitParamValueException extends Exception {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        InitParamValueException(java.lang.String r3, java.lang.String r4, java.lang.Throwable r5) {
            /*
                r2 = this;
                java.lang.StringBuffer r0 = new java.lang.StringBuffer
                r0.<init>()
                java.lang.String r1 = "Failed to set the "
                r0.append(r1)
                java.lang.String r3 = freemarker.template.utility.StringUtil.jQuote(r3)
                r0.append(r3)
                java.lang.String r3 = " servlet init-param to "
                r0.append(r3)
                java.lang.String r3 = freemarker.template.utility.StringUtil.jQuote(r4)
                r0.append(r3)
                java.lang.String r3 = "; see cause exception."
                r0.append(r3)
                java.lang.String r3 = r0.toString()
                r2.<init>(r3, r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.servlet.FreemarkerServlet.InitParamValueException.<init>(java.lang.String, java.lang.String, java.lang.Throwable):void");
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public InitParamValueException(java.lang.String r3, java.lang.String r4, java.lang.String r5) {
            /*
                r2 = this;
                java.lang.StringBuffer r0 = new java.lang.StringBuffer
                r0.<init>()
                java.lang.String r1 = "Failed to set the "
                r0.append(r1)
                java.lang.String r3 = freemarker.template.utility.StringUtil.jQuote(r3)
                r0.append(r3)
                java.lang.String r3 = " servlet init-param to "
                r0.append(r3)
                java.lang.String r3 = freemarker.template.utility.StringUtil.jQuote(r4)
                r0.append(r3)
                java.lang.String r3 = ": "
                r0.append(r3)
                r0.append(r5)
                java.lang.String r3 = r0.toString()
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.servlet.FreemarkerServlet.InitParamValueException.<init>(java.lang.String, java.lang.String, java.lang.String):void");
        }
    }

    private static class ConflictingInitParamsException extends Exception {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        ConflictingInitParamsException(java.lang.String r3, java.lang.String r4) {
            /*
                r2 = this;
                java.lang.StringBuffer r0 = new java.lang.StringBuffer
                r0.<init>()
                java.lang.String r1 = "Conflicting servlet init-params: "
                r0.append(r1)
                java.lang.String r1 = freemarker.template.utility.StringUtil.jQuote(r3)
                r0.append(r1)
                java.lang.String r1 = " and "
                r0.append(r1)
                java.lang.String r4 = freemarker.template.utility.StringUtil.jQuote(r4)
                r0.append(r4)
                java.lang.String r4 = ". Only use "
                r0.append(r4)
                java.lang.String r3 = freemarker.template.utility.StringUtil.jQuote(r3)
                r0.append(r3)
                java.lang.String r3 = "."
                r0.append(r3)
                java.lang.String r3 = r0.toString()
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.servlet.FreemarkerServlet.ConflictingInitParamsException.<init>(java.lang.String, java.lang.String):void");
        }
    }

    private static class MalformedWebXmlException extends Exception {
        MalformedWebXmlException(String str) {
            super(str);
        }
    }
}
