package freemarker.ext.servlet;

import com.tamic.novate.util.FileUtil;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.cache.WebappTemplateLoader;
import freemarker.core._ObjectBuilderSettingEvaluator;
import freemarker.core._SettingEvaluationEnvironment;
import freemarker.log.Logger;
import freemarker.template.Configuration;
import freemarker.template._TemplateAPI;
import freemarker.template.utility.StringUtil;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import org.apache.commons.math3.geometry.VectorFormat;

final class InitParamParser {
    private static final Logger LOG = Logger.getLogger("freemarker.servlet");
    static final String TEMPLATE_PATH_PREFIX_CLASS = "class://";
    static final String TEMPLATE_PATH_PREFIX_CLASSPATH = "classpath:";
    static final String TEMPLATE_PATH_PREFIX_FILE = "file://";
    static final String TEMPLATE_PATH_SETTINGS_BI_NAME = "settings";

    private InitParamParser() {
    }

    static TemplateLoader createTemplateLoader(String str, Configuration configuration, Class cls, ServletContext servletContext) throws IOException {
        String str2;
        TemplateLoader templateLoader;
        int findTemplatePathSettingAssignmentsStart = findTemplatePathSettingAssignmentsStart(str);
        if (findTemplatePathSettingAssignmentsStart == -1) {
            str2 = str;
        } else {
            str2 = str.substring(0, findTemplatePathSettingAssignmentsStart);
        }
        String trim = str2.trim();
        if (trim.startsWith(TEMPLATE_PATH_PREFIX_CLASS)) {
            templateLoader = new ClassTemplateLoader(cls, normalizeToAbsolutePackagePath(trim.substring(8)));
        } else if (trim.startsWith(TEMPLATE_PATH_PREFIX_CLASSPATH)) {
            String normalizeToAbsolutePackagePath = normalizeToAbsolutePackagePath(trim.substring(10));
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            if (contextClassLoader == null) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("No Thread Context Class Loader was found. Falling back to the class loader of ");
                stringBuffer.append(cls.getName());
                stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                logger.warn(stringBuffer.toString());
                contextClassLoader = cls.getClassLoader();
            }
            templateLoader = new ClassTemplateLoader(contextClassLoader, normalizeToAbsolutePackagePath);
        } else if (trim.startsWith(TEMPLATE_PATH_PREFIX_FILE)) {
            templateLoader = new FileTemplateLoader(new File(trim.substring(7)));
        } else if (!trim.startsWith("[") || configuration.getIncompatibleImprovements().intValue() < _TemplateAPI.VERSION_INT_2_3_22) {
            if (!trim.startsWith(VectorFormat.DEFAULT_PREFIX) || configuration.getIncompatibleImprovements().intValue() < _TemplateAPI.VERSION_INT_2_3_22) {
                templateLoader = new WebappTemplateLoader(servletContext, trim);
            } else {
                throw new TemplatePathParsingException("Template paths starting with \"{\" are reseved for future purposes");
            }
        } else if (trim.endsWith("]")) {
            List parseCommaSeparatedTemplatePaths = parseCommaSeparatedTemplatePaths(trim.substring(1, trim.length() - 1).trim());
            TemplateLoader[] templateLoaderArr = new TemplateLoader[parseCommaSeparatedTemplatePaths.size()];
            for (int i = 0; i < parseCommaSeparatedTemplatePaths.size(); i++) {
                templateLoaderArr[i] = createTemplateLoader((String) parseCommaSeparatedTemplatePaths.get(i), configuration, cls, servletContext);
            }
            templateLoader = new MultiTemplateLoader(templateLoaderArr);
        } else {
            throw new TemplatePathParsingException("Failed to parse template path; closing \"]\" is missing.");
        }
        if (findTemplatePathSettingAssignmentsStart != -1) {
            try {
                if (_ObjectBuilderSettingEvaluator.configureBean(str, str.indexOf(40, findTemplatePathSettingAssignmentsStart) + 1, templateLoader, _SettingEvaluationEnvironment.getCurrent()) != str.length()) {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("Template path should end after the setting list in: ");
                    stringBuffer2.append(str);
                    throw new TemplatePathParsingException(stringBuffer2.toString());
                }
            } catch (Exception e) {
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("Failed to set properties in: ");
                stringBuffer3.append(str);
                throw new TemplatePathParsingException(stringBuffer3.toString(), e);
            }
        }
        return templateLoader;
    }

    static String normalizeToAbsolutePackagePath(String str) {
        while (str.startsWith("/")) {
            str = str.substring(1);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("/");
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    static List parseCommaSeparatedList(String str) throws ParseException {
        ArrayList arrayList = new ArrayList();
        String[] split = StringUtil.split(str, ',');
        for (int i = 0; i < split.length; i++) {
            String trim = split[i].trim();
            if (trim.length() != 0) {
                arrayList.add(trim);
            } else if (i != split.length - 1) {
                throw new ParseException("Missing list item berfore a comma", -1);
            }
        }
        return arrayList;
    }

    static List parseCommaSeparatedPatterns(String str) throws ParseException {
        List parseCommaSeparatedList = parseCommaSeparatedList(str);
        ArrayList arrayList = new ArrayList(parseCommaSeparatedList.size());
        for (int i = 0; i < parseCommaSeparatedList.size(); i++) {
            arrayList.add(Pattern.compile((String) parseCommaSeparatedList.get(i)));
        }
        return arrayList;
    }

    static List parseCommaSeparatedTemplatePaths(String str) {
        ArrayList arrayList = new ArrayList();
        while (str.length() != 0) {
            int findTemplatePathSettingAssignmentsStart = findTemplatePathSettingAssignmentsStart(str);
            if (findTemplatePathSettingAssignmentsStart == -1) {
                findTemplatePathSettingAssignmentsStart = str.length();
            }
            int lastIndexOf = str.lastIndexOf(44, findTemplatePathSettingAssignmentsStart - 1);
            String trim = str.substring(lastIndexOf != -1 ? lastIndexOf + 1 : 0).trim();
            if (trim.length() != 0) {
                arrayList.add(0, trim);
            } else if (arrayList.size() > 0) {
                throw new TemplatePathParsingException("Missing list item before a comma");
            }
            str = lastIndexOf != -1 ? str.substring(0, lastIndexOf).trim() : "";
        }
        return arrayList;
    }

    static int findTemplatePathSettingAssignmentsStart(String str) {
        int length = str.length() - 1;
        while (length >= 0 && Character.isWhitespace(str.charAt(length))) {
            length--;
        }
        if (length >= 0 && str.charAt(length) == ')') {
            int i = length - 1;
            int i2 = 1;
            char c = 0;
            while (i2 > 0) {
                if (i < 0) {
                    return -1;
                }
                char charAt = str.charAt(i);
                if (c != 0) {
                    if (c != 1) {
                    }
                    c = 0;
                } else if (charAt != '\"') {
                    switch (charAt) {
                        case '\'':
                            c = 1;
                            continue;
                        case '(':
                            i2--;
                            continue;
                        case ')':
                            i2++;
                            continue;
                    }
                } else {
                    c = 2;
                }
                i--;
            }
            while (i >= 0 && Character.isWhitespace(str.charAt(i))) {
                i--;
            }
            int i3 = i + 1;
            while (i >= 0 && Character.isJavaIdentifierPart(str.charAt(i))) {
                i--;
            }
            int i4 = i + 1;
            if (i4 == i3) {
                return -1;
            }
            String substring = str.substring(i4, i3);
            while (i >= 0 && Character.isWhitespace(str.charAt(i))) {
                i--;
            }
            if (i >= 0 && str.charAt(i) == '?') {
                if (substring.equals(TEMPLATE_PATH_SETTINGS_BI_NAME)) {
                    return i;
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(StringUtil.jQuote(substring));
                stringBuffer.append(" is unexpected after the \"?\". ");
                stringBuffer.append("Expected \"");
                stringBuffer.append(TEMPLATE_PATH_SETTINGS_BI_NAME);
                stringBuffer.append("\".");
                throw new TemplatePathParsingException(stringBuffer.toString());
            }
        }
        return -1;
    }

    private static final class TemplatePathParsingException extends RuntimeException {
        public TemplatePathParsingException(String str, Throwable th) {
            super(str, th);
        }

        public TemplatePathParsingException(String str) {
            super(str);
        }
    }
}
