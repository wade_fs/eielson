package freemarker.ext.beans;

import freemarker.core._DelayedConversionToString;
import freemarker.core._ErrorDescriptionBuilder;
import freemarker.core._TemplateModelException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.ClassUtil;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

final class OverloadedMethods {
    private final boolean bugfixed;
    /* access modifiers changed from: private */
    public final OverloadedMethodsSubset fixArgMethods;
    /* access modifiers changed from: private */
    public OverloadedMethodsSubset varargMethods;

    OverloadedMethods(boolean z) {
        this.bugfixed = z;
        this.fixArgMethods = new OverloadedFixArgsMethods(z);
    }

    /* access modifiers changed from: package-private */
    public void addMethod(Method method) {
        addCallableMemberDescriptor(new ReflectionCallableMemberDescriptor(method, method.getParameterTypes()));
    }

    /* access modifiers changed from: package-private */
    public void addConstructor(Constructor constructor) {
        addCallableMemberDescriptor(new ReflectionCallableMemberDescriptor(constructor, constructor.getParameterTypes()));
    }

    private void addCallableMemberDescriptor(ReflectionCallableMemberDescriptor reflectionCallableMemberDescriptor) {
        this.fixArgMethods.addCallableMemberDescriptor(reflectionCallableMemberDescriptor);
        if (reflectionCallableMemberDescriptor.isVarargs()) {
            if (this.varargMethods == null) {
                this.varargMethods = new OverloadedVarArgsMethods(this.bugfixed);
            }
            this.varargMethods.addCallableMemberDescriptor(reflectionCallableMemberDescriptor);
        }
    }

    /* access modifiers changed from: package-private */
    public MemberAndArguments getMemberAndArguments(List list, BeansWrapper beansWrapper) throws TemplateModelException {
        MaybeEmptyMemberAndArguments maybeEmptyMemberAndArguments;
        MaybeEmptyMemberAndArguments memberAndArguments = this.fixArgMethods.getMemberAndArguments(list, beansWrapper);
        if (memberAndArguments instanceof MemberAndArguments) {
            return (MemberAndArguments) memberAndArguments;
        }
        OverloadedMethodsSubset overloadedMethodsSubset = this.varargMethods;
        if (overloadedMethodsSubset != null) {
            maybeEmptyMemberAndArguments = overloadedMethodsSubset.getMemberAndArguments(list, beansWrapper);
            if (maybeEmptyMemberAndArguments instanceof MemberAndArguments) {
                return (MemberAndArguments) maybeEmptyMemberAndArguments;
            }
        } else {
            maybeEmptyMemberAndArguments = null;
        }
        _ErrorDescriptionBuilder _errordescriptionbuilder = new _ErrorDescriptionBuilder(new Object[]{toCompositeErrorMessage((EmptyMemberAndArguments) memberAndArguments, (EmptyMemberAndArguments) maybeEmptyMemberAndArguments, list), "\nThe matching overload was searched among these members:\n", memberListToString()});
        if (!this.bugfixed) {
            _errordescriptionbuilder.tip("You seem to use BeansWrapper with incompatibleImprovements set below 2.3.21. If you think this error is unfounded, enabling 2.3.21 fixes may helps. See version history for more.");
        }
        throw new _TemplateModelException(_errordescriptionbuilder);
    }

    private Object[] toCompositeErrorMessage(EmptyMemberAndArguments emptyMemberAndArguments, EmptyMemberAndArguments emptyMemberAndArguments2, List list) {
        if (emptyMemberAndArguments2 == null) {
            return toErrorMessage(emptyMemberAndArguments, list);
        }
        if (emptyMemberAndArguments == null || emptyMemberAndArguments.isNumberOfArgumentsWrong()) {
            return toErrorMessage(emptyMemberAndArguments2, list);
        }
        return new Object[]{"When trying to call the non-varargs overloads:\n", toErrorMessage(emptyMemberAndArguments, list), "\nWhen trying to call the varargs overloads:\n", toErrorMessage(emptyMemberAndArguments2, null)};
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object[] toErrorMessage(freemarker.ext.beans.EmptyMemberAndArguments r9, java.util.List r10) {
        /*
            r8 = this;
            java.lang.Object[] r0 = r9.getUnwrappedArguments()
            r1 = 3
            java.lang.Object[] r2 = new java.lang.Object[r1]
            java.lang.Object r9 = r9.getErrorDescription()
            r3 = 0
            r2[r3] = r9
            java.lang.String r9 = "."
            java.lang.String r4 = ""
            r5 = 2
            r6 = 1
            if (r10 == 0) goto L_0x0025
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r7 = "\nThe FTL type of the argument values were: "
            r1[r3] = r7
            freemarker.core._DelayedConversionToString r10 = r8.getTMActualParameterTypes(r10)
            r1[r6] = r10
            r1[r5] = r9
            goto L_0x0026
        L_0x0025:
            r1 = r4
        L_0x0026:
            r2[r6] = r1
            if (r0 == 0) goto L_0x0045
            java.lang.Object[] r4 = new java.lang.Object[r5]
            java.lang.String r10 = "\nThe Java type of the argument values were: "
            r4[r3] = r10
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.Object r0 = r8.getUnwrappedActualParameterTypes(r0)
            r10.append(r0)
            r10.append(r9)
            java.lang.String r9 = r10.toString()
            r4[r6] = r9
        L_0x0045:
            r2[r5] = r4
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.OverloadedMethods.toErrorMessage(freemarker.ext.beans.EmptyMemberAndArguments, java.util.List):java.lang.Object[]");
    }

    private _DelayedConversionToString memberListToString() {
        return new _DelayedConversionToString(null) {
            /* class freemarker.ext.beans.OverloadedMethods.C33301 */

            /* access modifiers changed from: protected */
            public String doConversion(Object obj) {
                Iterator memberDescriptors = OverloadedMethods.this.fixArgMethods.getMemberDescriptors();
                Iterator memberDescriptors2 = OverloadedMethods.this.varargMethods != null ? OverloadedMethods.this.varargMethods.getMemberDescriptors() : null;
                if (!(memberDescriptors.hasNext() || (memberDescriptors2 != null && memberDescriptors2.hasNext()))) {
                    return "No members";
                }
                StringBuffer stringBuffer = new StringBuffer();
                HashSet hashSet = new HashSet();
                while (memberDescriptors.hasNext()) {
                    if (stringBuffer.length() != 0) {
                        stringBuffer.append(",\n");
                    }
                    stringBuffer.append("    ");
                    CallableMemberDescriptor callableMemberDescriptor = (CallableMemberDescriptor) memberDescriptors.next();
                    hashSet.add(callableMemberDescriptor);
                    stringBuffer.append(callableMemberDescriptor.getDeclaration());
                }
                if (memberDescriptors2 != null) {
                    while (memberDescriptors2.hasNext()) {
                        CallableMemberDescriptor callableMemberDescriptor2 = (CallableMemberDescriptor) memberDescriptors2.next();
                        if (!hashSet.contains(callableMemberDescriptor2)) {
                            if (stringBuffer.length() != 0) {
                                stringBuffer.append(",\n");
                            }
                            stringBuffer.append("    ");
                            stringBuffer.append(callableMemberDescriptor2.getDeclaration());
                        }
                    }
                }
                return stringBuffer.toString();
            }
        };
    }

    private _DelayedConversionToString getTMActualParameterTypes(List list) {
        String[] strArr = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            strArr[i] = ClassUtil.getFTLTypeDescription((TemplateModel) list.get(i));
        }
        return new DelayedCallSignatureToString(strArr) {
            /* class freemarker.ext.beans.OverloadedMethods.C33312 */

            /* access modifiers changed from: package-private */
            public String argumentToString(Object obj) {
                return (String) obj;
            }
        };
    }

    private Object getUnwrappedActualParameterTypes(Object[] objArr) {
        Class[] clsArr = new Class[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            Object obj = objArr[i];
            clsArr[i] = obj != null ? obj.getClass() : null;
        }
        return new DelayedCallSignatureToString(clsArr) {
            /* class freemarker.ext.beans.OverloadedMethods.C33323 */

            /* access modifiers changed from: package-private */
            public String argumentToString(Object obj) {
                if (obj != null) {
                    return ClassUtil.getShortClassName((Class) obj);
                }
                return ClassUtil.getShortClassNameOfObject(null);
            }
        };
    }

    private abstract class DelayedCallSignatureToString extends _DelayedConversionToString {
        /* access modifiers changed from: package-private */
        public abstract String argumentToString(Object obj);

        public DelayedCallSignatureToString(Object[] objArr) {
            super(objArr);
        }

        /* access modifiers changed from: protected */
        public String doConversion(Object obj) {
            Object[] objArr = (Object[]) obj;
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < objArr.length; i++) {
                if (i != 0) {
                    stringBuffer.append(", ");
                }
                stringBuffer.append(argumentToString(objArr[i]));
            }
            return stringBuffer.toString();
        }
    }
}
