package freemarker.ext.beans;

import java.util.AbstractList;

class NonPrimitiveArrayBackedReadOnlyList extends AbstractList {
    private final Object[] array;

    NonPrimitiveArrayBackedReadOnlyList(Object[] objArr) {
        this.array = objArr;
    }

    public Object get(int i) {
        return this.array[i];
    }

    public int size() {
        return this.array.length;
    }
}
