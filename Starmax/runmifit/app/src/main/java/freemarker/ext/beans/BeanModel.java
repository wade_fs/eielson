package freemarker.ext.beans;

import freemarker.core.CollectionAndSequence;
import freemarker.core._DelayedFTLTypeDescription;
import freemarker.core._DelayedJQuote;
import freemarker.core._TemplateModelException;
import freemarker.ext.util.ModelFactory;
import freemarker.ext.util.WrapperTemplateModel;
import freemarker.log.Logger;
import freemarker.template.AdapterTemplateModel;
import freemarker.template.ObjectWrapper;
import freemarker.template.SimpleScalar;
import freemarker.template.SimpleSequence;
import freemarker.template.TemplateCollectionModel;
import freemarker.template.TemplateHashModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateModelIterator;
import freemarker.template.TemplateModelWithAPISupport;
import freemarker.template.TemplateScalarModel;
import freemarker.template.utility.StringUtil;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BeanModel implements TemplateHashModelEx, AdapterTemplateModel, WrapperTemplateModel, TemplateModelWithAPISupport {
    static final ModelFactory FACTORY = new ModelFactory() {
        /* class freemarker.ext.beans.BeanModel.C33161 */

        public TemplateModel create(Object obj, ObjectWrapper objectWrapper) {
            return new BeanModel(obj, (BeansWrapper) objectWrapper);
        }
    };
    private static final Logger LOG = Logger.getLogger("freemarker.beans");
    static final TemplateModel UNKNOWN = new SimpleScalar("UNKNOWN");
    private HashMap memberMap;
    protected final Object object;
    protected final BeansWrapper wrapper;

    public BeanModel(Object obj, BeansWrapper beansWrapper) {
        this(obj, beansWrapper, true);
    }

    BeanModel(Object obj, BeansWrapper beansWrapper, boolean z) {
        this.object = obj;
        this.wrapper = beansWrapper;
        if (z && obj != null) {
            beansWrapper.getClassIntrospector().get(obj.getClass());
        }
    }

    public TemplateModel get(String str) throws TemplateModelException {
        TemplateModel templateModel;
        Class<?> cls = this.object.getClass();
        Map map = this.wrapper.getClassIntrospector().get(cls);
        try {
            if (this.wrapper.isMethodsShadowItems()) {
                Object obj = map.get(str);
                templateModel = obj != null ? invokeThroughDescriptor(obj, map) : invokeGenericGet(map, cls, str);
            } else {
                TemplateModel invokeGenericGet = invokeGenericGet(map, cls, str);
                TemplateModel wrap = this.wrapper.wrap(null);
                if (invokeGenericGet != wrap && invokeGenericGet != UNKNOWN) {
                    return invokeGenericGet;
                }
                Object obj2 = map.get(str);
                if (obj2 != null) {
                    TemplateModel invokeThroughDescriptor = invokeThroughDescriptor(obj2, map);
                    templateModel = (invokeThroughDescriptor == UNKNOWN && invokeGenericGet == wrap) ? wrap : invokeThroughDescriptor;
                } else {
                    templateModel = null;
                }
            }
            if (templateModel != UNKNOWN) {
                return templateModel;
            }
            if (!this.wrapper.isStrict()) {
                if (LOG.isDebugEnabled()) {
                    logNoSuchKey(str, map);
                }
                return this.wrapper.wrap(null);
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("No such bean property: ");
            stringBuffer.append(str);
            throw new InvalidPropertyException(stringBuffer.toString());
        } catch (TemplateModelException e) {
            throw e;
        } catch (Exception e2) {
            throw new _TemplateModelException(e2, new Object[]{"An error has occurred when reading existing sub-variable ", new _DelayedJQuote(str), "; see cause exception! The type of the containing value was: ", new _DelayedFTLTypeDescription(this)});
        }
    }

    private void logNoSuchKey(String str, Map map) {
        Logger logger = LOG;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Key ");
        stringBuffer.append(StringUtil.jQuoteNoXSS(str));
        stringBuffer.append(" was not found on instance of ");
        stringBuffer.append(this.object.getClass().getName());
        stringBuffer.append(". Introspection information for ");
        stringBuffer.append("the class is: ");
        stringBuffer.append(map);
        logger.debug(stringBuffer.toString());
    }

    /* access modifiers changed from: protected */
    public boolean hasPlainGetMethod() {
        return this.wrapper.getClassIntrospector().get(this.object.getClass()).get(ClassIntrospector.GENERIC_GET_KEY) != null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0081  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private freemarker.template.TemplateModel invokeThroughDescriptor(java.lang.Object r5, java.util.Map r6) throws java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException, freemarker.template.TemplateModelException {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.HashMap r0 = r4.memberMap     // Catch:{ all -> 0x0098 }
            r1 = 0
            if (r0 == 0) goto L_0x000f
            java.util.HashMap r0 = r4.memberMap     // Catch:{ all -> 0x0098 }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x0098 }
            freemarker.template.TemplateModel r0 = (freemarker.template.TemplateModel) r0     // Catch:{ all -> 0x0098 }
            goto L_0x0010
        L_0x000f:
            r0 = r1
        L_0x0010:
            monitor-exit(r4)     // Catch:{ all -> 0x0098 }
            if (r0 == 0) goto L_0x0014
            return r0
        L_0x0014:
            freemarker.template.TemplateModel r2 = freemarker.ext.beans.BeanModel.UNKNOWN
            boolean r3 = r5 instanceof java.beans.IndexedPropertyDescriptor
            if (r3 == 0) goto L_0x0031
            r0 = r5
            java.beans.IndexedPropertyDescriptor r0 = (java.beans.IndexedPropertyDescriptor) r0
            java.lang.reflect.Method r0 = r0.getIndexedReadMethod()
            freemarker.ext.beans.SimpleMethodModel r1 = new freemarker.ext.beans.SimpleMethodModel
            java.lang.Object r2 = r4.object
            java.lang.Class[] r6 = freemarker.ext.beans.ClassIntrospector.getArgTypes(r6, r0)
            freemarker.ext.beans.BeansWrapper r3 = r4.wrapper
            r1.<init>(r2, r0, r6, r3)
        L_0x002e:
            r0 = r1
        L_0x002f:
            r2 = r0
            goto L_0x007f
        L_0x0031:
            boolean r3 = r5 instanceof java.beans.PropertyDescriptor
            if (r3 == 0) goto L_0x0045
            r6 = r5
            java.beans.PropertyDescriptor r6 = (java.beans.PropertyDescriptor) r6
            freemarker.ext.beans.BeansWrapper r2 = r4.wrapper
            java.lang.Object r3 = r4.object
            java.lang.reflect.Method r6 = r6.getReadMethod()
            freemarker.template.TemplateModel r2 = r2.invokeMethod(r3, r6, r1)
            goto L_0x007f
        L_0x0045:
            boolean r1 = r5 instanceof java.lang.reflect.Field
            if (r1 == 0) goto L_0x0059
            freemarker.ext.beans.BeansWrapper r6 = r4.wrapper
            r1 = r5
            java.lang.reflect.Field r1 = (java.lang.reflect.Field) r1
            java.lang.Object r2 = r4.object
            java.lang.Object r1 = r1.get(r2)
            freemarker.template.TemplateModel r2 = r6.wrap(r1)
            goto L_0x007f
        L_0x0059:
            boolean r1 = r5 instanceof java.lang.reflect.Method
            if (r1 == 0) goto L_0x006e
            r0 = r5
            java.lang.reflect.Method r0 = (java.lang.reflect.Method) r0
            freemarker.ext.beans.SimpleMethodModel r1 = new freemarker.ext.beans.SimpleMethodModel
            java.lang.Object r2 = r4.object
            java.lang.Class[] r6 = freemarker.ext.beans.ClassIntrospector.getArgTypes(r6, r0)
            freemarker.ext.beans.BeansWrapper r3 = r4.wrapper
            r1.<init>(r2, r0, r6, r3)
            goto L_0x002e
        L_0x006e:
            boolean r6 = r5 instanceof freemarker.ext.beans.OverloadedMethods
            if (r6 == 0) goto L_0x007f
            freemarker.ext.beans.OverloadedMethodsModel r0 = new freemarker.ext.beans.OverloadedMethodsModel
            java.lang.Object r6 = r4.object
            r1 = r5
            freemarker.ext.beans.OverloadedMethods r1 = (freemarker.ext.beans.OverloadedMethods) r1
            freemarker.ext.beans.BeansWrapper r2 = r4.wrapper
            r0.<init>(r6, r1, r2)
            goto L_0x002f
        L_0x007f:
            if (r0 == 0) goto L_0x0097
            monitor-enter(r4)
            java.util.HashMap r6 = r4.memberMap     // Catch:{ all -> 0x0094 }
            if (r6 != 0) goto L_0x008d
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ all -> 0x0094 }
            r6.<init>()     // Catch:{ all -> 0x0094 }
            r4.memberMap = r6     // Catch:{ all -> 0x0094 }
        L_0x008d:
            java.util.HashMap r6 = r4.memberMap     // Catch:{ all -> 0x0094 }
            r6.put(r5, r0)     // Catch:{ all -> 0x0094 }
            monitor-exit(r4)     // Catch:{ all -> 0x0094 }
            goto L_0x0097
        L_0x0094:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0094 }
            throw r5
        L_0x0097:
            return r2
        L_0x0098:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0098 }
            goto L_0x009c
        L_0x009b:
            throw r5
        L_0x009c:
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.BeanModel.invokeThroughDescriptor(java.lang.Object, java.util.Map):freemarker.template.TemplateModel");
    }

    /* access modifiers changed from: package-private */
    public void clearMemberCache() {
        synchronized (this) {
            this.memberMap = null;
        }
    }

    /* access modifiers changed from: protected */
    public TemplateModel invokeGenericGet(Map map, Class cls, String str) throws IllegalAccessException, InvocationTargetException, TemplateModelException {
        Method method = (Method) map.get(ClassIntrospector.GENERIC_GET_KEY);
        if (method == null) {
            return UNKNOWN;
        }
        return this.wrapper.invokeMethod(this.object, method, new Object[]{str});
    }

    /* access modifiers changed from: protected */
    public TemplateModel wrap(Object obj) throws TemplateModelException {
        return this.wrapper.getOuterIdentity().wrap(obj);
    }

    /* access modifiers changed from: protected */
    public Object unwrap(TemplateModel templateModel) throws TemplateModelException {
        return this.wrapper.unwrap(templateModel);
    }

    public boolean isEmpty() {
        Object obj = this.object;
        if (obj instanceof String) {
            if (((String) obj).length() == 0) {
                return true;
            }
            return false;
        } else if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        } else {
            if (obj instanceof Map) {
                return ((Map) obj).isEmpty();
            }
            if (obj == null || Boolean.FALSE.equals(this.object)) {
                return true;
            }
            return false;
        }
    }

    public Object getAdaptedObject(Class cls) {
        return this.object;
    }

    public Object getWrappedObject() {
        return this.object;
    }

    public int size() {
        return this.wrapper.getClassIntrospector().keyCount(this.object.getClass());
    }

    public TemplateCollectionModel keys() {
        return new CollectionAndSequence(new SimpleSequence(keySet(), this.wrapper));
    }

    public TemplateCollectionModel values() throws TemplateModelException {
        ArrayList arrayList = new ArrayList(size());
        TemplateModelIterator it = keys().iterator();
        while (it.hasNext()) {
            arrayList.add(get(((TemplateScalarModel) it.next()).getAsString()));
        }
        return new CollectionAndSequence(new SimpleSequence(arrayList, this.wrapper));
    }

    /* access modifiers changed from: package-private */
    public String getAsClassicCompatibleString() {
        Object obj = this.object;
        return obj == null ? "null" : obj.toString();
    }

    public String toString() {
        return this.object.toString();
    }

    /* access modifiers changed from: protected */
    public Set keySet() {
        return this.wrapper.getClassIntrospector().keySet(this.object.getClass());
    }

    public TemplateModel getAPI() throws TemplateModelException {
        return this.wrapper.wrapAsAPI(this.object);
    }
}
