package freemarker.ext.beans;

import freemarker.template.TemplateModelException;
import freemarker.template.utility.CollectionUtils;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class _BeansAPI {

    public interface _BeansWrapperSubclassFactory {
        BeansWrapper create(BeansWrapperConfiguration beansWrapperConfiguration);
    }

    private _BeansAPI() {
    }

    public static String getAsClassicCompatibleString(BeanModel beanModel) {
        return beanModel.getAsClassicCompatibleString();
    }

    public static Object newInstance(Class cls, Object[] objArr, BeansWrapper beansWrapper) throws NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, TemplateModelException {
        return newInstance(getConstructorDescriptor(cls, objArr), objArr, beansWrapper);
    }

    private static CallableMemberDescriptor getConstructorDescriptor(Class cls, Object[] objArr) throws NoSuchMethodException {
        if (objArr == null) {
            objArr = CollectionUtils.EMPTY_OBJECT_ARRAY;
        }
        ArgumentTypes argumentTypes = new ArgumentTypes(objArr, true);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Constructor<?>[] constructors = cls.getConstructors();
        for (Constructor<?> constructor : constructors) {
            ReflectionCallableMemberDescriptor reflectionCallableMemberDescriptor = new ReflectionCallableMemberDescriptor(constructor, constructor.getParameterTypes());
            if (!_MethodUtil.isVarargs(constructor)) {
                arrayList.add(reflectionCallableMemberDescriptor);
            } else {
                arrayList2.add(reflectionCallableMemberDescriptor);
            }
        }
        MaybeEmptyCallableMemberDescriptor mostSpecific = argumentTypes.getMostSpecific(arrayList, false);
        if (mostSpecific == EmptyCallableMemberDescriptor.NO_SUCH_METHOD) {
            mostSpecific = argumentTypes.getMostSpecific(arrayList2, true);
        }
        if (!(mostSpecific instanceof EmptyCallableMemberDescriptor)) {
            return (CallableMemberDescriptor) mostSpecific;
        }
        if (mostSpecific == EmptyCallableMemberDescriptor.NO_SUCH_METHOD) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("There's no public ");
            stringBuffer.append(cls.getName());
            stringBuffer.append(" constructor with compatible parameter list.");
            throw new NoSuchMethodException(stringBuffer.toString());
        } else if (mostSpecific == EmptyCallableMemberDescriptor.AMBIGUOUS_METHOD) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("There are multiple public ");
            stringBuffer2.append(cls.getName());
            stringBuffer2.append(" constructors that match the compatible parameter list with the same preferability.");
            throw new NoSuchMethodException(stringBuffer2.toString());
        } else {
            throw new NoSuchMethodException();
        }
    }

    private static Object newInstance(CallableMemberDescriptor callableMemberDescriptor, Object[] objArr, BeansWrapper beansWrapper) throws InstantiationException, IllegalAccessException, InvocationTargetException, IllegalArgumentException, TemplateModelException {
        if (objArr == null) {
            objArr = CollectionUtils.EMPTY_OBJECT_ARRAY;
        }
        if (callableMemberDescriptor.isVarargs()) {
            Class[] paramTypes = callableMemberDescriptor.getParamTypes();
            int length = paramTypes.length - 1;
            Object[] objArr2 = new Object[(length + 1)];
            for (int i = 0; i < length; i++) {
                objArr2[i] = objArr[i];
            }
            Class<?> componentType = paramTypes[length].getComponentType();
            int length2 = objArr.length - length;
            Object newInstance = Array.newInstance(componentType, length2);
            for (int i2 = 0; i2 < length2; i2++) {
                Array.set(newInstance, i2, objArr[length + i2]);
            }
            objArr2[length] = newInstance;
            objArr = objArr2;
        }
        return callableMemberDescriptor.invokeConstructor(beansWrapper, objArr);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: freemarker.ext.beans.BeansWrapper} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static freemarker.ext.beans.BeansWrapper getBeansWrapperSubclassSingleton(freemarker.ext.beans.BeansWrapperConfiguration r3, java.util.Map r4, java.lang.ref.ReferenceQueue r5, freemarker.ext.beans._BeansAPI._BeansWrapperSubclassFactory r6) {
        /*
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            java.lang.ClassLoader r0 = r0.getContextClassLoader()
            monitor-enter(r4)
            java.lang.Object r1 = r4.get(r0)     // Catch:{ all -> 0x006b }
            java.util.Map r1 = (java.util.Map) r1     // Catch:{ all -> 0x006b }
            r2 = 0
            if (r1 != 0) goto L_0x001c
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ all -> 0x006b }
            r1.<init>()     // Catch:{ all -> 0x006b }
            r4.put(r0, r1)     // Catch:{ all -> 0x006b }
            r0 = r2
            goto L_0x0022
        L_0x001c:
            java.lang.Object r0 = r1.get(r3)     // Catch:{ all -> 0x006b }
            java.lang.ref.Reference r0 = (java.lang.ref.Reference) r0     // Catch:{ all -> 0x006b }
        L_0x0022:
            monitor-exit(r4)     // Catch:{ all -> 0x006b }
            if (r0 == 0) goto L_0x002c
            java.lang.Object r0 = r0.get()
            freemarker.ext.beans.BeansWrapper r0 = (freemarker.ext.beans.BeansWrapper) r0
            goto L_0x002d
        L_0x002c:
            r0 = r2
        L_0x002d:
            if (r0 == 0) goto L_0x0030
            return r0
        L_0x0030:
            r0 = 1
            java.lang.Object r3 = r3.clone(r0)
            freemarker.ext.beans.BeansWrapperConfiguration r3 = (freemarker.ext.beans.BeansWrapperConfiguration) r3
            freemarker.ext.beans.BeansWrapper r6 = r6.create(r3)
            boolean r0 = r6.isWriteProtected()
            if (r0 == 0) goto L_0x0065
            monitor-enter(r4)
            java.lang.Object r0 = r1.get(r3)     // Catch:{ all -> 0x0062 }
            java.lang.ref.Reference r0 = (java.lang.ref.Reference) r0     // Catch:{ all -> 0x0062 }
            if (r0 == 0) goto L_0x0051
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0062 }
            r2 = r0
            freemarker.ext.beans.BeansWrapper r2 = (freemarker.ext.beans.BeansWrapper) r2     // Catch:{ all -> 0x0062 }
        L_0x0051:
            if (r2 != 0) goto L_0x005c
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0062 }
            r0.<init>(r6, r5)     // Catch:{ all -> 0x0062 }
            r1.put(r3, r0)     // Catch:{ all -> 0x0062 }
            goto L_0x005d
        L_0x005c:
            r6 = r2
        L_0x005d:
            monitor-exit(r4)     // Catch:{ all -> 0x0062 }
            removeClearedReferencesFromCache(r4, r5)
            return r6
        L_0x0062:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0062 }
            throw r3
        L_0x0065:
            freemarker.core.BugException r3 = new freemarker.core.BugException
            r3.<init>()
            throw r3
        L_0x006b:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x006b }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans._BeansAPI.getBeansWrapperSubclassSingleton(freemarker.ext.beans.BeansWrapperConfiguration, java.util.Map, java.lang.ref.ReferenceQueue, freemarker.ext.beans._BeansAPI$_BeansWrapperSubclassFactory):freemarker.ext.beans.BeansWrapper");
    }

    private static void removeClearedReferencesFromCache(Map map, ReferenceQueue referenceQueue) {
        while (true) {
            Reference poll = referenceQueue.poll();
            if (poll != null) {
                synchronized (map) {
                    Iterator it = map.values().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Iterator it2 = ((Map) it.next()).values().iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                if (it2.next() == poll) {
                                    it2.remove();
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                return;
            }
        }
    }
}
