package freemarker.ext.beans;

import freemarker.core.BugException;
import freemarker.template.ObjectWrapperAndUnwrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.lang.reflect.Array;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class OverloadedVarArgsMethods extends OverloadedMethodsSubset {
    OverloadedVarArgsMethods(boolean z) {
        super(z);
    }

    /* access modifiers changed from: package-private */
    public Class[] preprocessParameterTypes(CallableMemberDescriptor callableMemberDescriptor) {
        Class[] clsArr = (Class[]) callableMemberDescriptor.getParamTypes().clone();
        int length = clsArr.length - 1;
        Class<?> componentType = clsArr[length].getComponentType();
        if (componentType != null) {
            clsArr[length] = componentType;
            return clsArr;
        }
        throw new BugException("Only varargs methods should be handled here");
    }

    /* access modifiers changed from: package-private */
    public void afterWideningUnwrappingHints(Class[] clsArr, int[] iArr) {
        Class[] clsArr2;
        int length = clsArr.length;
        Class[][] unwrappingHintsByParamCount = getUnwrappingHintsByParamCount();
        int i = length - 1;
        int i2 = i;
        while (true) {
            if (i2 < 0) {
                break;
            }
            Class[] clsArr3 = unwrappingHintsByParamCount[i2];
            if (clsArr3 != null) {
                widenHintsToCommonSupertypes(length, clsArr3, getTypeFlags(i2));
                break;
            }
            i2--;
        }
        int i3 = length + 1;
        if (i3 < unwrappingHintsByParamCount.length && (clsArr2 = unwrappingHintsByParamCount[i3]) != null) {
            widenHintsToCommonSupertypes(length, clsArr2, getTypeFlags(i3));
        }
        while (i3 < unwrappingHintsByParamCount.length) {
            widenHintsToCommonSupertypes(i3, clsArr, iArr);
            i3++;
        }
        if (length > 0) {
            widenHintsToCommonSupertypes(i, clsArr, iArr);
        }
    }

    private void widenHintsToCommonSupertypes(int i, Class[] clsArr, int[] iArr) {
        Class[] clsArr2 = getUnwrappingHintsByParamCount()[i];
        if (clsArr2 != null) {
            int length = clsArr2.length;
            int length2 = clsArr.length;
            int min = Math.min(length2, length);
            for (int i2 = 0; i2 < min; i2++) {
                clsArr2[i2] = getCommonSupertypeForUnwrappingHint(clsArr2[i2], clsArr[i2]);
            }
            if (length > length2) {
                Class cls = clsArr[length2 - 1];
                while (length2 < length) {
                    clsArr2[length2] = getCommonSupertypeForUnwrappingHint(clsArr2[length2], cls);
                    length2++;
                }
            }
            if (this.bugfixed) {
                mergeInTypesFlags(i, iArr);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public MaybeEmptyMemberAndArguments getMemberAndArguments(List list, BeansWrapper beansWrapper) throws TemplateModelException {
        BeansWrapper beansWrapper2 = beansWrapper;
        List list2 = list == null ? Collections.EMPTY_LIST : list;
        int size = list2.size();
        Class[][] unwrappingHintsByParamCount = getUnwrappingHintsByParamCount();
        Object[] objArr = new Object[size];
        int min = Math.min(size + 1, unwrappingHintsByParamCount.length - 1);
        int[] iArr = null;
        loop0:
        while (min >= 0) {
            Class[] clsArr = unwrappingHintsByParamCount[min];
            if (clsArr != null) {
                iArr = getTypeFlags(min);
                if (iArr == ALL_ZEROS_ARRAY) {
                    iArr = null;
                }
                Iterator it = list2.iterator();
                int i = 0;
                while (i < size) {
                    int i2 = i < min ? i : min - 1;
                    Object tryUnwrapTo = beansWrapper2.tryUnwrapTo((TemplateModel) it.next(), clsArr[i2], iArr != null ? iArr[i2] : 0);
                    if (tryUnwrapTo != ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
                        objArr[i] = tryUnwrapTo;
                        i++;
                    }
                }
                break loop0;
            } else if (min == 0) {
                return EmptyMemberAndArguments.WRONG_NUMBER_OF_ARGUMENTS;
            }
            min--;
        }
        MaybeEmptyCallableMemberDescriptor memberDescriptorForArgs = getMemberDescriptorForArgs(objArr, true);
        if (!(memberDescriptorForArgs instanceof CallableMemberDescriptor)) {
            return EmptyMemberAndArguments.from((EmptyCallableMemberDescriptor) memberDescriptorForArgs, objArr);
        }
        CallableMemberDescriptor callableMemberDescriptor = (CallableMemberDescriptor) memberDescriptorForArgs;
        Object replaceVarargsSectionWithArray = replaceVarargsSectionWithArray(objArr, list2, callableMemberDescriptor, beansWrapper2);
        if (!(replaceVarargsSectionWithArray instanceof Object[])) {
            return EmptyMemberAndArguments.noCompatibleOverload(((Integer) replaceVarargsSectionWithArray).intValue());
        }
        Object[] objArr2 = (Object[]) replaceVarargsSectionWithArray;
        if (!this.bugfixed) {
            BeansWrapper.coerceBigDecimals(callableMemberDescriptor.getParamTypes(), objArr2);
        } else if (iArr != null) {
            forceNumberArgumentsToParameterTypes(objArr2, callableMemberDescriptor.getParamTypes(), iArr);
        }
        return new MemberAndArguments(callableMemberDescriptor, objArr2);
    }

    private Object replaceVarargsSectionWithArray(Object[] objArr, List list, CallableMemberDescriptor callableMemberDescriptor, BeansWrapper beansWrapper) throws TemplateModelException {
        Class[] paramTypes = callableMemberDescriptor.getParamTypes();
        int length = paramTypes.length;
        int i = length - 1;
        Class<?> componentType = paramTypes[i].getComponentType();
        int length2 = objArr.length;
        if (objArr.length != length) {
            Object[] objArr2 = new Object[length];
            System.arraycopy(objArr, 0, objArr2, 0, i);
            Object newInstance = Array.newInstance(componentType, length2 - i);
            for (int i2 = i; i2 < length2; i2++) {
                Object tryUnwrapTo = beansWrapper.tryUnwrapTo((TemplateModel) list.get(i2), componentType);
                if (tryUnwrapTo == ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
                    return new Integer(i2 + 1);
                }
                Array.set(newInstance, i2 - i, tryUnwrapTo);
            }
            objArr2[i] = newInstance;
            return objArr2;
        }
        Object tryUnwrapTo2 = beansWrapper.tryUnwrapTo((TemplateModel) list.get(i), componentType);
        if (tryUnwrapTo2 == ObjectWrapperAndUnwrapper.CANT_UNWRAP_TO_TARGET_CLASS) {
            return new Integer(i + 1);
        }
        Object newInstance2 = Array.newInstance(componentType, 1);
        Array.set(newInstance2, 0, tryUnwrapTo2);
        objArr[i] = newInstance2;
        return objArr;
    }
}
