package freemarker.ext.beans;

import freemarker.core._ConcurrentMapFactory;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

abstract class ClassBasedModelFactory implements TemplateHashModel {
    private final Map cache = _ConcurrentMapFactory.newMaybeConcurrentHashMap();
    private final Set classIntrospectionsInProgress = new HashSet();
    private final boolean isCacheConcurrentMap = _ConcurrentMapFactory.isConcurrent(this.cache);
    private final BeansWrapper wrapper;

    /* access modifiers changed from: protected */
    public abstract TemplateModel createModel(Class cls) throws TemplateModelException;

    public boolean isEmpty() {
        return false;
    }

    protected ClassBasedModelFactory(BeansWrapper beansWrapper) {
        this.wrapper = beansWrapper;
    }

    public TemplateModel get(String str) throws TemplateModelException {
        try {
            return getInternal(str);
        } catch (Exception e) {
            if (e instanceof TemplateModelException) {
                throw ((TemplateModelException) e);
            }
            throw new TemplateModelException(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r3 = freemarker.template.utility.ClassUtil.forName(r6);
        r1.get(r3);
        r3 = createModel(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006f, code lost:
        if (r3 == null) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0071, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0078, code lost:
        if (r1 != r5.wrapper.getClassIntrospector()) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007e, code lost:
        if (r2 != r1.getClearingCounter()) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0080, code lost:
        r5.cache.put(r6, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0085, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x008a, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r5.classIntrospectionsInProgress.remove(r6);
        r0.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0093, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0094, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0098, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0099, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r5.classIntrospectionsInProgress.remove(r6);
        r0.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00a3, code lost:
        throw r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private freemarker.template.TemplateModel getInternal(java.lang.String r6) throws freemarker.template.TemplateModelException, java.lang.ClassNotFoundException {
        /*
            r5 = this;
            boolean r0 = r5.isCacheConcurrentMap
            if (r0 == 0) goto L_0x000f
            java.util.Map r0 = r5.cache
            java.lang.Object r0 = r0.get(r6)
            freemarker.template.TemplateModel r0 = (freemarker.template.TemplateModel) r0
            if (r0 == 0) goto L_0x000f
            return r0
        L_0x000f:
            freemarker.ext.beans.BeansWrapper r0 = r5.wrapper
            java.lang.Object r0 = r0.getSharedIntrospectionLock()
            monitor-enter(r0)
            java.util.Map r1 = r5.cache     // Catch:{ all -> 0x00a7 }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x00a7 }
            freemarker.template.TemplateModel r1 = (freemarker.template.TemplateModel) r1     // Catch:{ all -> 0x00a7 }
            if (r1 == 0) goto L_0x0022
            monitor-exit(r0)     // Catch:{ all -> 0x00a7 }
            return r1
        L_0x0022:
            if (r1 != 0) goto L_0x0050
            java.util.Set r2 = r5.classIntrospectionsInProgress     // Catch:{ all -> 0x00a7 }
            boolean r2 = r2.contains(r6)     // Catch:{ all -> 0x00a7 }
            if (r2 == 0) goto L_0x0050
            r0.wait()     // Catch:{ InterruptedException -> 0x0038 }
            java.util.Map r1 = r5.cache     // Catch:{ InterruptedException -> 0x0038 }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ InterruptedException -> 0x0038 }
            freemarker.template.TemplateModel r1 = (freemarker.template.TemplateModel) r1     // Catch:{ InterruptedException -> 0x0038 }
            goto L_0x0022
        L_0x0038:
            r6 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00a7 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ all -> 0x00a7 }
            r2.<init>()     // Catch:{ all -> 0x00a7 }
            java.lang.String r3 = "Class inrospection data lookup aborded: "
            r2.append(r3)     // Catch:{ all -> 0x00a7 }
            r2.append(r6)     // Catch:{ all -> 0x00a7 }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x00a7 }
            r1.<init>(r6)     // Catch:{ all -> 0x00a7 }
            throw r1     // Catch:{ all -> 0x00a7 }
        L_0x0050:
            if (r1 == 0) goto L_0x0054
            monitor-exit(r0)     // Catch:{ all -> 0x00a7 }
            return r1
        L_0x0054:
            java.util.Set r1 = r5.classIntrospectionsInProgress     // Catch:{ all -> 0x00a7 }
            r1.add(r6)     // Catch:{ all -> 0x00a7 }
            freemarker.ext.beans.BeansWrapper r1 = r5.wrapper     // Catch:{ all -> 0x00a7 }
            freemarker.ext.beans.ClassIntrospector r1 = r1.getClassIntrospector()     // Catch:{ all -> 0x00a7 }
            int r2 = r1.getClearingCounter()     // Catch:{ all -> 0x00a7 }
            monitor-exit(r0)     // Catch:{ all -> 0x00a7 }
            java.lang.Class r3 = freemarker.template.utility.ClassUtil.forName(r6)     // Catch:{ all -> 0x0098 }
            r1.get(r3)     // Catch:{ all -> 0x0098 }
            freemarker.template.TemplateModel r3 = r5.createModel(r3)     // Catch:{ all -> 0x0098 }
            if (r3 == 0) goto L_0x008a
            monitor-enter(r0)     // Catch:{ all -> 0x0098 }
            freemarker.ext.beans.BeansWrapper r4 = r5.wrapper     // Catch:{ all -> 0x0087 }
            freemarker.ext.beans.ClassIntrospector r4 = r4.getClassIntrospector()     // Catch:{ all -> 0x0087 }
            if (r1 != r4) goto L_0x0085
            int r1 = r1.getClearingCounter()     // Catch:{ all -> 0x0087 }
            if (r2 != r1) goto L_0x0085
            java.util.Map r1 = r5.cache     // Catch:{ all -> 0x0087 }
            r1.put(r6, r3)     // Catch:{ all -> 0x0087 }
        L_0x0085:
            monitor-exit(r0)     // Catch:{ all -> 0x0087 }
            goto L_0x008a
        L_0x0087:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0087 }
            throw r1     // Catch:{ all -> 0x0098 }
        L_0x008a:
            monitor-enter(r0)
            java.util.Set r1 = r5.classIntrospectionsInProgress     // Catch:{ all -> 0x0095 }
            r1.remove(r6)     // Catch:{ all -> 0x0095 }
            r0.notifyAll()     // Catch:{ all -> 0x0095 }
            monitor-exit(r0)     // Catch:{ all -> 0x0095 }
            return r3
        L_0x0095:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0095 }
            throw r6
        L_0x0098:
            r1 = move-exception
            monitor-enter(r0)
            java.util.Set r2 = r5.classIntrospectionsInProgress     // Catch:{ all -> 0x00a4 }
            r2.remove(r6)     // Catch:{ all -> 0x00a4 }
            r0.notifyAll()     // Catch:{ all -> 0x00a4 }
            monitor-exit(r0)     // Catch:{ all -> 0x00a4 }
            throw r1
        L_0x00a4:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00a4 }
            throw r6
        L_0x00a7:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00a7 }
            goto L_0x00ab
        L_0x00aa:
            throw r6
        L_0x00ab:
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.beans.ClassBasedModelFactory.getInternal(java.lang.String):freemarker.template.TemplateModel");
    }

    /* access modifiers changed from: package-private */
    public void clearCache() {
        synchronized (this.wrapper.getSharedIntrospectionLock()) {
            this.cache.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public void removeFromCache(Class cls) {
        synchronized (this.wrapper.getSharedIntrospectionLock()) {
            this.cache.remove(cls.getName());
        }
    }

    /* access modifiers changed from: protected */
    public BeansWrapper getWrapper() {
        return this.wrapper;
    }
}
