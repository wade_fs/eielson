package freemarker.ext.jsp;

import com.tamic.novate.util.FileUtil;
import freemarker.core.Environment;
import freemarker.core._DelayedJQuote;
import freemarker.core._DelayedShortClassName;
import freemarker.core._ErrorDescriptionBuilder;
import freemarker.core._TemplateModelException;
import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.jsp.SimpleTagDirectiveModel;
import freemarker.template.ObjectWrapper;
import freemarker.template.ObjectWrapperAndUnwrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.StringUtil;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

class JspTagModelBase {
    static /* synthetic */ Class class$java$lang$ClassCastException;
    static /* synthetic */ Class class$java$lang$IllegalArgumentException;
    static /* synthetic */ Class class$java$lang$IndexOutOfBoundsException;
    static /* synthetic */ Class class$java$lang$NullPointerException;
    static /* synthetic */ Class class$java$lang$Object;
    static /* synthetic */ Class class$java$lang$String;
    private final Method dynaSetter;
    private final Map propertySetters = new HashMap();
    private final Class tagClass;
    protected final String tagName;

    protected JspTagModelBase(String str, Class cls) throws IntrospectionException {
        Method method;
        Class cls2;
        Class cls3;
        Class cls4;
        this.tagName = str;
        this.tagClass = cls;
        PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(cls).getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            Method writeMethod = propertyDescriptor.getWriteMethod();
            if (writeMethod != null) {
                this.propertySetters.put(propertyDescriptor.getName(), writeMethod);
            }
        }
        try {
            Class[] clsArr = new Class[3];
            if (class$java$lang$String == null) {
                cls2 = class$("java.lang.String");
                class$java$lang$String = cls2;
            } else {
                cls2 = class$java$lang$String;
            }
            clsArr[0] = cls2;
            if (class$java$lang$String == null) {
                cls3 = class$("java.lang.String");
                class$java$lang$String = cls3;
            } else {
                cls3 = class$java$lang$String;
            }
            clsArr[1] = cls3;
            if (class$java$lang$Object == null) {
                cls4 = class$("java.lang.Object");
                class$java$lang$Object = cls4;
            } else {
                cls4 = class$java$lang$Object;
            }
            clsArr[2] = cls4;
            method = cls.getMethod("setDynamicAttribute", clsArr);
        } catch (NoSuchMethodException unused) {
            method = null;
        }
        this.dynaSetter = method;
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* access modifiers changed from: package-private */
    public Object getTagInstance() throws IllegalAccessException, InstantiationException {
        return this.tagClass.newInstance();
    }

    /* access modifiers changed from: package-private */
    public void setupTag(Object obj, Map map, ObjectWrapper objectWrapper) throws TemplateModelException, InvocationTargetException, IllegalAccessException {
        ObjectWrapperAndUnwrapper objectWrapperAndUnwrapper;
        if (map != null && !map.isEmpty()) {
            if (objectWrapper instanceof ObjectWrapperAndUnwrapper) {
                objectWrapperAndUnwrapper = (ObjectWrapperAndUnwrapper) objectWrapper;
            } else {
                objectWrapperAndUnwrapper = BeansWrapper.getDefaultInstance();
            }
            Object[] objArr = new Object[1];
            for (Map.Entry entry : map.entrySet()) {
                Object unwrap = objectWrapperAndUnwrapper.unwrap((TemplateModel) entry.getValue());
                objArr[0] = unwrap;
                Object key = entry.getKey();
                Method method = (Method) this.propertySetters.get(key);
                if (method == null) {
                    Method method2 = this.dynaSetter;
                    if (method2 != null) {
                        method2.invoke(obj, null, key, objArr[0]);
                    } else {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("Unknown property ");
                        stringBuffer.append(StringUtil.jQuote(key.toString()));
                        stringBuffer.append(" on instance of ");
                        stringBuffer.append(this.tagClass.getName());
                        throw new TemplateModelException(stringBuffer.toString());
                    }
                } else {
                    if (unwrap instanceof BigDecimal) {
                        objArr[0] = BeansWrapper.coerceBigDecimal((BigDecimal) unwrap, method.getParameterTypes()[0]);
                    }
                    try {
                        method.invoke(obj, objArr);
                    } catch (Exception e) {
                        Class<?> cls = method.getParameterTypes()[0];
                        Object[] objArr2 = new Object[6];
                        objArr2[0] = "Failed to set JSP tag parameter ";
                        objArr2[1] = new _DelayedJQuote(key);
                        objArr2[2] = " (declared type: ";
                        StringBuffer stringBuffer2 = new StringBuffer();
                        stringBuffer2.append(new _DelayedShortClassName(cls));
                        stringBuffer2.append(", actual value's type: ");
                        objArr2[3] = stringBuffer2.toString();
                        objArr2[4] = objArr[0] != null ? new _DelayedShortClassName(objArr[0].getClass()) : "Null";
                        objArr2[5] = "). See cause exception for the more specific cause...";
                        _ErrorDescriptionBuilder _errordescriptionbuilder = new _ErrorDescriptionBuilder(objArr2);
                        if (e instanceof IllegalArgumentException) {
                            Class cls2 = class$java$lang$String;
                            if (cls2 == null) {
                                cls2 = class$("java.lang.String");
                                class$java$lang$String = cls2;
                            }
                            if (!cls.isAssignableFrom(cls2) && objArr[0] != null && (objArr[0] instanceof String)) {
                                _errordescriptionbuilder.tip(new Object[]{"This problem is often caused by unnecessary parameter quotation. Paramters aren't quoted in FTL, similarly as they aren't quoted in most languages. For example, these parameter assignments are wrong: ", "<@my.tag p1=\"true\" p2=\"10\" p3=\"${someVariable}\" p4=\"${x+1}\" />", ". The correct form is: ", "<@my.tag p1=true p2=10 p3=someVariable p4=x+1 />", ". Only string literals are quoted (regardless of where they occur): ", "<@my.box style=\"info\" message=\"Hello ${name}!\" width=200 />", FileUtil.HIDDEN_PREFIX});
                            }
                        }
                        throw new _TemplateModelException(e, (Environment) null, _errordescriptionbuilder);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final TemplateModelException toTemplateModelExceptionOrRethrow(Exception exc) throws TemplateModelException {
        if (exc instanceof RuntimeException) {
            RuntimeException runtimeException = (RuntimeException) exc;
            if (!isCommonRuntimeException(runtimeException)) {
                throw runtimeException;
            }
        }
        if (exc instanceof TemplateModelException) {
            throw ((TemplateModelException) exc);
        } else if (exc instanceof SimpleTagDirectiveModel.TemplateExceptionWrapperJspException) {
            return (TemplateModelException) exc.getCause();
        } else {
            return new _TemplateModelException(exc, new Object[]{"Error while invoking the ", new _DelayedJQuote(this.tagName), " JSP custom tag; see cause exception"});
        }
    }

    private boolean isCommonRuntimeException(RuntimeException runtimeException) {
        Class<?> cls = runtimeException.getClass();
        Class<?> cls2 = class$java$lang$NullPointerException;
        if (cls2 == null) {
            cls2 = class$("java.lang.NullPointerException");
            class$java$lang$NullPointerException = cls2;
        }
        if (cls != cls2) {
            Class<?> cls3 = class$java$lang$IllegalArgumentException;
            if (cls3 == null) {
                cls3 = class$("java.lang.IllegalArgumentException");
                class$java$lang$IllegalArgumentException = cls3;
            }
            if (cls != cls3) {
                Class<?> cls4 = class$java$lang$ClassCastException;
                if (cls4 == null) {
                    cls4 = class$("java.lang.ClassCastException");
                    class$java$lang$ClassCastException = cls4;
                }
                if (cls != cls4) {
                    Class<?> cls5 = class$java$lang$IndexOutOfBoundsException;
                    if (cls5 == null) {
                        cls5 = class$("java.lang.IndexOutOfBoundsException");
                        class$java$lang$IndexOutOfBoundsException = cls5;
                    }
                    return cls == cls5;
                }
            }
        }
    }
}
