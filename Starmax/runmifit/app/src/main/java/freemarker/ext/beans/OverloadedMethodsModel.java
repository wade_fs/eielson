package freemarker.ext.beans;

import freemarker.template.SimpleNumber;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;
import java.util.Collections;
import java.util.List;

public class OverloadedMethodsModel implements TemplateMethodModelEx, TemplateSequenceModel {
    private final Object object;
    private final OverloadedMethods overloadedMethods;
    private final BeansWrapper wrapper;

    OverloadedMethodsModel(Object obj, OverloadedMethods overloadedMethods2, BeansWrapper beansWrapper) {
        this.object = obj;
        this.overloadedMethods = overloadedMethods2;
        this.wrapper = beansWrapper;
    }

    public Object exec(List list) throws TemplateModelException {
        MemberAndArguments memberAndArguments = this.overloadedMethods.getMemberAndArguments(list, this.wrapper);
        try {
            return memberAndArguments.invokeMethod(this.wrapper, this.object);
        } catch (Exception e) {
            if (e instanceof TemplateModelException) {
                throw ((TemplateModelException) e);
            }
            throw _MethodUtil.newInvocationTemplateModelException(this.object, memberAndArguments.getCallableMemberDescriptor(), e);
        }
    }

    public TemplateModel get(int i) throws TemplateModelException {
        return (TemplateModel) exec(Collections.singletonList(new SimpleNumber(new Integer(i))));
    }

    public int size() throws TemplateModelException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("?size is unsupported for ");
        stringBuffer.append(getClass().getName());
        throw new TemplateModelException(stringBuffer.toString());
    }
}
