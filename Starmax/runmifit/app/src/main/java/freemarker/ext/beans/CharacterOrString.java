package freemarker.ext.beans;

final class CharacterOrString {
    private final String stringValue;

    CharacterOrString(String str) {
        this.stringValue = str;
    }

    /* access modifiers changed from: package-private */
    public String getAsString() {
        return this.stringValue;
    }

    /* access modifiers changed from: package-private */
    public char getAsChar() {
        return this.stringValue.charAt(0);
    }
}
