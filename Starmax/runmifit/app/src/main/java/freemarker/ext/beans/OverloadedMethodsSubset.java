package freemarker.ext.beans;

import freemarker.core._ConcurrentMapFactory;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.NullArgumentException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

abstract class OverloadedMethodsSubset {
    static final int[] ALL_ZEROS_ARRAY = new int[0];
    private static final int[][] ZERO_PARAM_COUNT_TYPE_FLAGS_ARRAY = new int[1][];
    static /* synthetic */ Class class$java$io$Serializable;
    static /* synthetic */ Class class$java$lang$Byte;
    static /* synthetic */ Class class$java$lang$Character;
    static /* synthetic */ Class class$java$lang$Cloneable;
    static /* synthetic */ Class class$java$lang$Comparable;
    static /* synthetic */ Class class$java$lang$Double;
    static /* synthetic */ Class class$java$lang$Float;
    static /* synthetic */ Class class$java$lang$Integer;
    static /* synthetic */ Class class$java$lang$Long;
    static /* synthetic */ Class class$java$lang$Number;
    static /* synthetic */ Class class$java$lang$Object;
    static /* synthetic */ Class class$java$lang$Short;
    private final Map argTypesToMemberDescCache = _ConcurrentMapFactory.newMaybeConcurrentHashMap(6, 0.75f, 1);
    protected final boolean bugfixed;
    private final boolean isArgTypesToMemberDescCacheConcurrentMap = _ConcurrentMapFactory.isConcurrent(this.argTypesToMemberDescCache);
    private final List memberDescs = new LinkedList();
    private int[][] typeFlagsByParamCount;
    private Class[][] unwrappingHintsByParamCount;

    /* access modifiers changed from: package-private */
    public abstract void afterWideningUnwrappingHints(Class[] clsArr, int[] iArr);

    /* access modifiers changed from: package-private */
    public abstract MaybeEmptyMemberAndArguments getMemberAndArguments(List list, BeansWrapper beansWrapper) throws TemplateModelException;

    /* access modifiers changed from: package-private */
    public abstract Class[] preprocessParameterTypes(CallableMemberDescriptor callableMemberDescriptor);

    static {
        ZERO_PARAM_COUNT_TYPE_FLAGS_ARRAY[0] = ALL_ZEROS_ARRAY;
    }

    OverloadedMethodsSubset(boolean z) {
        this.bugfixed = z;
    }

    /* access modifiers changed from: package-private */
    public void addCallableMemberDescriptor(ReflectionCallableMemberDescriptor reflectionCallableMemberDescriptor) {
        this.memberDescs.add(reflectionCallableMemberDescriptor);
        Class[] preprocessParameterTypes = preprocessParameterTypes(reflectionCallableMemberDescriptor);
        int length = preprocessParameterTypes.length;
        Class[][] clsArr = this.unwrappingHintsByParamCount;
        if (clsArr == null) {
            this.unwrappingHintsByParamCount = new Class[(length + 1)][];
            this.unwrappingHintsByParamCount[length] = (Class[]) preprocessParameterTypes.clone();
        } else if (clsArr.length <= length) {
            Class[][] clsArr2 = new Class[(length + 1)][];
            System.arraycopy(clsArr, 0, clsArr2, 0, clsArr.length);
            this.unwrappingHintsByParamCount = clsArr2;
            this.unwrappingHintsByParamCount[length] = (Class[]) preprocessParameterTypes.clone();
        } else {
            Class[] clsArr3 = clsArr[length];
            if (clsArr3 == null) {
                clsArr[length] = (Class[]) preprocessParameterTypes.clone();
            } else {
                for (int i = 0; i < preprocessParameterTypes.length; i++) {
                    clsArr3[i] = getCommonSupertypeForUnwrappingHint(clsArr3[i], preprocessParameterTypes[i]);
                }
            }
        }
        int[] iArr = ALL_ZEROS_ARRAY;
        if (this.bugfixed) {
            for (int i2 = 0; i2 < length; i2++) {
                int classToTypeFlags = TypeFlags.classToTypeFlags(preprocessParameterTypes[i2]);
                if (classToTypeFlags != 0) {
                    if (iArr == ALL_ZEROS_ARRAY) {
                        iArr = new int[length];
                    }
                    iArr[i2] = classToTypeFlags;
                }
            }
            mergeInTypesFlags(length, iArr);
        }
        if (!this.bugfixed) {
            preprocessParameterTypes = this.unwrappingHintsByParamCount[length];
        }
        afterWideningUnwrappingHints(preprocessParameterTypes, iArr);
    }

    /* access modifiers changed from: package-private */
    public Class[][] getUnwrappingHintsByParamCount() {
        return this.unwrappingHintsByParamCount;
    }

    /* access modifiers changed from: package-private */
    public final MaybeEmptyCallableMemberDescriptor getMemberDescriptorForArgs(Object[] objArr, boolean z) {
        ArgumentTypes argumentTypes = new ArgumentTypes(objArr, this.bugfixed);
        MaybeEmptyCallableMemberDescriptor maybeEmptyCallableMemberDescriptor = this.isArgTypesToMemberDescCacheConcurrentMap ? (MaybeEmptyCallableMemberDescriptor) this.argTypesToMemberDescCache.get(argumentTypes) : null;
        if (maybeEmptyCallableMemberDescriptor == null) {
            synchronized (this.argTypesToMemberDescCache) {
                MaybeEmptyCallableMemberDescriptor maybeEmptyCallableMemberDescriptor2 = (MaybeEmptyCallableMemberDescriptor) this.argTypesToMemberDescCache.get(argumentTypes);
                if (maybeEmptyCallableMemberDescriptor2 == null) {
                    maybeEmptyCallableMemberDescriptor2 = argumentTypes.getMostSpecific(this.memberDescs, z);
                    this.argTypesToMemberDescCache.put(argumentTypes, maybeEmptyCallableMemberDescriptor2);
                }
            }
        }
        return maybeEmptyCallableMemberDescriptor;
    }

    /* access modifiers changed from: package-private */
    public Iterator getMemberDescriptors() {
        return this.memberDescs.iterator();
    }

    /* access modifiers changed from: protected */
    public Class getCommonSupertypeForUnwrappingHint(Class cls, Class cls2) {
        boolean z;
        boolean z2;
        if (cls == cls2) {
            return cls;
        }
        if (this.bugfixed) {
            if (cls.isPrimitive()) {
                cls = ClassUtil.primitiveClassToBoxingClass(cls);
                z = true;
            } else {
                z = false;
            }
            if (cls2.isPrimitive()) {
                cls2 = ClassUtil.primitiveClassToBoxingClass(cls2);
                z2 = true;
            } else {
                z2 = false;
            }
            if (cls == cls2) {
                return cls;
            }
            Class cls3 = class$java$lang$Number;
            if (cls3 == null) {
                cls3 = class$("java.lang.Number");
                class$java$lang$Number = cls3;
            }
            if (cls3.isAssignableFrom(cls)) {
                Class cls4 = class$java$lang$Number;
                if (cls4 == null) {
                    cls4 = class$("java.lang.Number");
                    class$java$lang$Number = cls4;
                }
                if (cls4.isAssignableFrom(cls2)) {
                    Class cls5 = class$java$lang$Number;
                    if (cls5 != null) {
                        return cls5;
                    }
                    Class class$ = class$("java.lang.Number");
                    class$java$lang$Number = class$;
                    return class$;
                }
            }
            if (z || z2) {
                Class cls6 = class$java$lang$Object;
                if (cls6 != null) {
                    return cls6;
                }
                Class class$2 = class$("java.lang.Object");
                class$java$lang$Object = class$2;
                return class$2;
            }
        } else if (cls2.isPrimitive()) {
            if (cls2 == Byte.TYPE) {
                cls2 = class$java$lang$Byte;
                if (cls2 == null) {
                    cls2 = class$("java.lang.Byte");
                    class$java$lang$Byte = cls2;
                }
            } else if (cls2 == Short.TYPE) {
                cls2 = class$java$lang$Short;
                if (cls2 == null) {
                    cls2 = class$("java.lang.Short");
                    class$java$lang$Short = cls2;
                }
            } else if (cls2 == Character.TYPE) {
                cls2 = class$java$lang$Character;
                if (cls2 == null) {
                    cls2 = class$("java.lang.Character");
                    class$java$lang$Character = cls2;
                }
            } else if (cls2 == Integer.TYPE) {
                cls2 = class$java$lang$Integer;
                if (cls2 == null) {
                    cls2 = class$("java.lang.Integer");
                    class$java$lang$Integer = cls2;
                }
            } else if (cls2 == Float.TYPE) {
                cls2 = class$java$lang$Float;
                if (cls2 == null) {
                    cls2 = class$("java.lang.Float");
                    class$java$lang$Float = cls2;
                }
            } else if (cls2 == Long.TYPE) {
                cls2 = class$java$lang$Long;
                if (cls2 == null) {
                    cls2 = class$("java.lang.Long");
                    class$java$lang$Long = cls2;
                }
            } else if (cls2 == Double.TYPE && (cls2 = class$java$lang$Double) == null) {
                cls2 = class$("java.lang.Double");
                class$java$lang$Double = cls2;
            }
        }
        Set<Class> assignables = _MethodUtil.getAssignables(cls, cls2);
        assignables.retainAll(_MethodUtil.getAssignables(cls2, cls));
        if (assignables.isEmpty()) {
            Class cls7 = class$java$lang$Object;
            if (cls7 != null) {
                return cls7;
            }
            Class class$3 = class$("java.lang.Object");
            class$java$lang$Object = class$3;
            return class$3;
        }
        ArrayList arrayList = new ArrayList();
        for (Class cls8 : assignables) {
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    arrayList.add(cls8);
                    break;
                }
                Class cls9 = (Class) it.next();
                if (_MethodUtil.isMoreOrSameSpecificParameterType(cls9, cls8, false, 0) != 0) {
                    break;
                } else if (_MethodUtil.isMoreOrSameSpecificParameterType(cls8, cls9, false, 0) != 0) {
                    it.remove();
                }
            }
        }
        if (arrayList.size() > 1) {
            if (this.bugfixed) {
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    Class cls10 = (Class) it2.next();
                    if (!cls10.isInterface()) {
                        Class cls11 = class$java$lang$Object;
                        if (cls11 == null) {
                            cls11 = class$("java.lang.Object");
                            class$java$lang$Object = cls11;
                        }
                        if (cls10 != cls11) {
                            return cls10;
                        }
                        it2.remove();
                    }
                }
                Class cls12 = class$java$lang$Cloneable;
                if (cls12 == null) {
                    cls12 = class$("java.lang.Cloneable");
                    class$java$lang$Cloneable = cls12;
                }
                arrayList.remove(cls12);
                if (arrayList.size() > 1) {
                    Class cls13 = class$java$io$Serializable;
                    if (cls13 == null) {
                        cls13 = class$("java.io.Serializable");
                        class$java$io$Serializable = cls13;
                    }
                    arrayList.remove(cls13);
                    if (arrayList.size() > 1) {
                        Class cls14 = class$java$lang$Comparable;
                        if (cls14 == null) {
                            cls14 = class$("java.lang.Comparable");
                            class$java$lang$Comparable = cls14;
                        }
                        arrayList.remove(cls14);
                        if (arrayList.size() > 1) {
                            Class cls15 = class$java$lang$Object;
                            if (cls15 != null) {
                                return cls15;
                            }
                            Class class$4 = class$("java.lang.Object");
                            class$java$lang$Object = class$4;
                            return class$4;
                        }
                    }
                }
            } else {
                Class cls16 = class$java$lang$Object;
                if (cls16 != null) {
                    return cls16;
                }
                Class class$5 = class$("java.lang.Object");
                class$java$lang$Object = class$5;
                return class$5;
            }
        }
        return (Class) arrayList.get(0);
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    /* access modifiers changed from: protected */
    public final int[] getTypeFlags(int i) {
        int[][] iArr = this.typeFlagsByParamCount;
        if (iArr == null || iArr.length <= i) {
            return null;
        }
        return iArr[i];
    }

    /* access modifiers changed from: protected */
    public final void mergeInTypesFlags(int i, int[] iArr) {
        int i2;
        NullArgumentException.check("srcTypesFlagsByParamIdx", iArr);
        int i3 = 0;
        if (i == 0) {
            int[][] iArr2 = this.typeFlagsByParamCount;
            if (iArr2 == null) {
                this.typeFlagsByParamCount = ZERO_PARAM_COUNT_TYPE_FLAGS_ARRAY;
            } else if (iArr2 != ZERO_PARAM_COUNT_TYPE_FLAGS_ARRAY) {
                iArr2[0] = ALL_ZEROS_ARRAY;
            }
        } else {
            int[][] iArr3 = this.typeFlagsByParamCount;
            if (iArr3 == null) {
                this.typeFlagsByParamCount = new int[(i + 1)][];
            } else if (iArr3.length <= i) {
                int[][] iArr4 = new int[(i + 1)][];
                System.arraycopy(iArr3, 0, iArr4, 0, iArr3.length);
                this.typeFlagsByParamCount = iArr4;
            }
            int[][] iArr5 = this.typeFlagsByParamCount;
            int[] iArr6 = iArr5[i];
            if (iArr6 == null) {
                int[] iArr7 = ALL_ZEROS_ARRAY;
                if (iArr != iArr7) {
                    int length = iArr.length;
                    int[] iArr8 = new int[i];
                    while (i3 < i) {
                        iArr8[i3] = iArr[i3 < length ? i3 : length - 1];
                        i3++;
                    }
                    iArr7 = iArr8;
                }
                this.typeFlagsByParamCount[i] = iArr7;
            } else if (iArr != iArr6) {
                if (iArr6 == ALL_ZEROS_ARRAY && i > 0) {
                    iArr6 = new int[i];
                    iArr5[i] = iArr6;
                }
                int i4 = 0;
                while (i4 < i) {
                    if (iArr != ALL_ZEROS_ARRAY) {
                        int length2 = iArr.length;
                        i2 = iArr[i4 < length2 ? i4 : length2 - 1];
                    } else {
                        i2 = 0;
                    }
                    int i5 = iArr6[i4];
                    if (i5 != i2) {
                        int i6 = i2 | i5;
                        if ((i6 & 2044) != 0) {
                            i6 |= 1;
                        }
                        iArr6[i4] = i6;
                    }
                    i4++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void forceNumberArgumentsToParameterTypes(Object[] objArr, Class[] clsArr, int[] iArr) {
        Number forceUnwrappedNumberToType;
        int length = clsArr.length;
        int length2 = objArr.length;
        int i = 0;
        while (i < length2) {
            int i2 = i < length ? i : length - 1;
            if ((iArr[i2] & 1) != 0) {
                Object obj = objArr[i];
                if ((obj instanceof Number) && (forceUnwrappedNumberToType = BeansWrapper.forceUnwrappedNumberToType((Number) obj, clsArr[i2], this.bugfixed)) != null) {
                    objArr[i] = forceUnwrappedNumberToType;
                }
            }
            i++;
        }
    }
}
