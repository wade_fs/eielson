package freemarker.ext.jsp;

import com.tamic.novate.download.MimeType;
import com.tamic.novate.util.FileUtil;
import freemarker.core.BugException;
import freemarker.core.Environment;
import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.servlet.FreemarkerServlet;
import freemarker.ext.servlet.HttpRequestHashModel;
import freemarker.log.Logger;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.utility.ClassUtil;
import freemarker.template.utility.NullArgumentException;
import freemarker.template.utility.SecurityUtilities;
import freemarker.template.utility.StringUtil;
import java.beans.IntrospectionException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class TaglibFactory implements TemplateHashModel {
    public static final List DEFAULT_CLASSPATH_TLDS = Collections.EMPTY_LIST;
    public static final List DEFAULT_META_INF_TLD_SOURCES = Collections.singletonList(WebInfPerLibJarMetaInfTldSource.INSTANCE);
    private static final String DEFAULT_TLD_RESOURCE_PATH = "/META-INF/taglib.tld";
    private static final String JAR_URL_ENTRY_PATH_START = "!/";
    /* access modifiers changed from: private */
    public static final Logger LOG = Logger.getLogger("freemarker.jsp");
    private static final String META_INF_ABS_PATH = "/META-INF/";
    private static final String META_INF_REL_PATH = "META-INF/";
    /* access modifiers changed from: private */
    public static final String PLATFORM_FILE_ENCODING = SecurityUtilities.getSystemProperty("file.encoding", "utf-8");
    private static final int URL_TYPE_ABSOLUTE = 1;
    private static final int URL_TYPE_FULL = 0;
    private static final int URL_TYPE_RELATIVE = 2;
    static /* synthetic */ Class class$freemarker$ext$beans$BeansWrapper;
    static /* synthetic */ Class class$freemarker$ext$jsp$EventForwarding;
    static /* synthetic */ Class class$freemarker$ext$jsp$TaglibFactory;
    static /* synthetic */ Class class$freemarker$ext$servlet$FreemarkerServlet;
    static /* synthetic */ Class class$java$net$URL;
    static /* synthetic */ Class class$javax$servlet$jsp$tagext$Tag;
    private static final Method toURIMethod;
    private List classpathTlds = DEFAULT_CLASSPATH_TLDS;
    private List failedTldLocations = new ArrayList();
    private final Object lock = new Object();
    private List metaInfTldSources = DEFAULT_META_INF_TLD_SOURCES;
    private int nextTldLocationLookupPhase = 0;
    private ObjectWrapper objectWrapper;
    /* access modifiers changed from: private */
    public final ServletContext servletContext;
    private final Map taglibs = new HashMap();
    boolean test_emulateJarEntryUrlOpenStreamFails = false;
    boolean test_emulateNoJarURLConnections = false;
    boolean test_emulateNoUrlToFileConversions = false;
    private final Map tldLocations = new HashMap();

    private interface InputStreamFactory {
        InputStream getInputStream();
    }

    private interface TldLocation {
        InputStream getInputStream() throws IOException;

        String getXmlSystemId() throws IOException;
    }

    public boolean isEmpty() {
        return false;
    }

    static {
        Method method;
        Class cls;
        try {
            if (class$java$net$URL == null) {
                cls = class$("java.net.URL");
                class$java$net$URL = cls;
            } else {
                cls = class$java$net$URL;
            }
            method = cls.getMethod("toURI", new Class[0]);
        } catch (Exception unused) {
            method = null;
        }
        toURIMethod = method;
    }

    public TaglibFactory(ServletContext servletContext2) {
        this.servletContext = servletContext2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:67:0x015e A[SYNTHETIC, Splitter:B:67:0x015e] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x017c A[Catch:{ Exception -> 0x007b }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x017f A[Catch:{ Exception -> 0x007b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public freemarker.template.TemplateModel get(java.lang.String r9) throws freemarker.template.TemplateModelException {
        /*
            r8 = this;
            java.lang.Object r0 = r8.lock
            monitor-enter(r0)
            java.util.Map r1 = r8.taglibs     // Catch:{ all -> 0x01a0 }
            java.lang.Object r1 = r1.get(r9)     // Catch:{ all -> 0x01a0 }
            freemarker.ext.jsp.TaglibFactory$Taglib r1 = (freemarker.ext.jsp.TaglibFactory.Taglib) r1     // Catch:{ all -> 0x01a0 }
            if (r1 == 0) goto L_0x000f
            monitor-exit(r0)     // Catch:{ all -> 0x01a0 }
            return r1
        L_0x000f:
            r1 = 0
            r2 = 1
            r3 = 0
            freemarker.log.Logger r4 = freemarker.ext.jsp.TaglibFactory.LOG     // Catch:{ Exception -> 0x0158 }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ Exception -> 0x0158 }
            if (r4 == 0) goto L_0x0039
            freemarker.log.Logger r4 = freemarker.ext.jsp.TaglibFactory.LOG     // Catch:{ Exception -> 0x0158 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0158 }
            r5.<init>()     // Catch:{ Exception -> 0x0158 }
            java.lang.String r6 = "Locating TLD for taglib URI "
            r5.append(r6)     // Catch:{ Exception -> 0x0158 }
            java.lang.String r6 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r9)     // Catch:{ Exception -> 0x0158 }
            r5.append(r6)     // Catch:{ Exception -> 0x0158 }
            java.lang.String r6 = "."
            r5.append(r6)     // Catch:{ Exception -> 0x0158 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0158 }
            r4.debug(r5)     // Catch:{ Exception -> 0x0158 }
        L_0x0039:
            freemarker.ext.jsp.TaglibFactory$TldLocation r4 = r8.getExplicitlyMappedTldLocation(r9)     // Catch:{ Exception -> 0x0158 }
            if (r4 == 0) goto L_0x0040
            goto L_0x0075
        L_0x0040:
            int r4 = getUriType(r9)     // Catch:{ MalformedURLException -> 0x013c }
            r5 = 2
            if (r4 != r5) goto L_0x004c
            java.lang.String r2 = resolveRelativeUri(r9)     // Catch:{ Exception -> 0x0158 }
            goto L_0x004f
        L_0x004c:
            if (r4 != r2) goto L_0x00a8
            r2 = r9
        L_0x004f:
            boolean r4 = r2.equals(r9)     // Catch:{ Exception -> 0x0158 }
            if (r4 != 0) goto L_0x0061
            java.util.Map r4 = r8.taglibs     // Catch:{ Exception -> 0x0158 }
            java.lang.Object r4 = r4.get(r2)     // Catch:{ Exception -> 0x0158 }
            freemarker.ext.jsp.TaglibFactory$Taglib r4 = (freemarker.ext.jsp.TaglibFactory.Taglib) r4     // Catch:{ Exception -> 0x0158 }
            if (r4 == 0) goto L_0x0061
            monitor-exit(r0)     // Catch:{ all -> 0x01a0 }
            return r4
        L_0x0061:
            boolean r4 = isJarPath(r2)     // Catch:{ Exception -> 0x0158 }
            if (r4 == 0) goto L_0x006f
            freemarker.ext.jsp.TaglibFactory$ServletContextJarEntryTldLocation r4 = new freemarker.ext.jsp.TaglibFactory$ServletContextJarEntryTldLocation     // Catch:{ Exception -> 0x0158 }
            java.lang.String r5 = "/META-INF/taglib.tld"
            r4.<init>(r2, r5)     // Catch:{ Exception -> 0x0158 }
            goto L_0x0074
        L_0x006f:
            freemarker.ext.jsp.TaglibFactory$ServletContextTldLocation r4 = new freemarker.ext.jsp.TaglibFactory$ServletContextTldLocation     // Catch:{ Exception -> 0x0158 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0158 }
        L_0x0074:
            r9 = r2
        L_0x0075:
            freemarker.template.TemplateHashModel r9 = r8.loadTaglib(r4, r9)     // Catch:{ Exception -> 0x007b }
            monitor-exit(r0)     // Catch:{ all -> 0x01a0 }
            return r9
        L_0x007b:
            r1 = move-exception
            freemarker.template.TemplateModelException r2 = new freemarker.template.TemplateModelException     // Catch:{ all -> 0x01a0 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ all -> 0x01a0 }
            r3.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r5 = "Error while loading tag library for URI "
            r3.append(r5)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r9)     // Catch:{ all -> 0x01a0 }
            r3.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = " from TLD location "
            r3.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r4)     // Catch:{ all -> 0x01a0 }
            r3.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = "; see cause exception."
            r3.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = r3.toString()     // Catch:{ all -> 0x01a0 }
            r2.<init>(r9, r1)     // Catch:{ all -> 0x01a0 }
            throw r2     // Catch:{ all -> 0x01a0 }
        L_0x00a8:
            if (r4 != 0) goto L_0x0136
            java.lang.String r1 = r8.getFailedTLDsList()     // Catch:{ Exception -> 0x0158 }
            freemarker.ext.jsp.TaglibFactory$TaglibGettingException r4 = new freemarker.ext.jsp.TaglibFactory$TaglibGettingException     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0134 }
            r5.<init>()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "No TLD was found for the "
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r9)     // Catch:{ Exception -> 0x0134 }
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = " JSP taglib URI. (TLD-s are searched according "
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "the JSP 2.2 specification. In development- and embedded-servlet-container "
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "setups you may also need the "
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "\""
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "MetaInfTldSources"
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "\" and "
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "\""
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "ClasspathTlds"
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "\" "
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.Class r6 = freemarker.ext.jsp.TaglibFactory.class$freemarker$ext$servlet$FreemarkerServlet     // Catch:{ Exception -> 0x0134 }
            if (r6 != 0) goto L_0x00fb
            java.lang.String r6 = "freemarker.ext.servlet.FreemarkerServlet"
            java.lang.Class r6 = class$(r6)     // Catch:{ Exception -> 0x0134 }
            freemarker.ext.jsp.TaglibFactory.class$freemarker$ext$servlet$FreemarkerServlet = r6     // Catch:{ Exception -> 0x0134 }
            goto L_0x00fd
        L_0x00fb:
            java.lang.Class r6 = freemarker.ext.jsp.TaglibFactory.class$freemarker$ext$servlet$FreemarkerServlet     // Catch:{ Exception -> 0x0134 }
        L_0x00fd:
            java.lang.String r6 = r6.getName()     // Catch:{ Exception -> 0x0134 }
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = " init-params or the similar system "
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = "properites."
            r5.append(r6)     // Catch:{ Exception -> 0x0134 }
            if (r1 != 0) goto L_0x0113
            java.lang.String r1 = ""
            goto L_0x0124
        L_0x0113:
            java.lang.StringBuffer r6 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0134 }
            r6.<init>()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r7 = " Also note these TLD-s were skipped earlier due to errors; see error in the log: "
            r6.append(r7)     // Catch:{ Exception -> 0x0134 }
            r6.append(r1)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = r6.toString()     // Catch:{ Exception -> 0x0134 }
        L_0x0124:
            r5.append(r1)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = ")"
            r5.append(r1)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = r5.toString()     // Catch:{ Exception -> 0x0134 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0134 }
            throw r4     // Catch:{ Exception -> 0x0134 }
        L_0x0134:
            r1 = move-exception
            goto L_0x015b
        L_0x0136:
            freemarker.core.BugException r2 = new freemarker.core.BugException     // Catch:{ Exception -> 0x0158 }
            r2.<init>()     // Catch:{ Exception -> 0x0158 }
            throw r2     // Catch:{ Exception -> 0x0158 }
        L_0x013c:
            r2 = move-exception
            freemarker.ext.jsp.TaglibFactory$TaglibGettingException r4 = new freemarker.ext.jsp.TaglibFactory$TaglibGettingException     // Catch:{ Exception -> 0x0158 }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0158 }
            r5.<init>()     // Catch:{ Exception -> 0x0158 }
            java.lang.String r6 = "Malformed taglib URI: "
            r5.append(r6)     // Catch:{ Exception -> 0x0158 }
            java.lang.String r6 = freemarker.template.utility.StringUtil.jQuote(r9)     // Catch:{ Exception -> 0x0158 }
            r5.append(r6)     // Catch:{ Exception -> 0x0158 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0158 }
            r4.<init>(r5, r2)     // Catch:{ Exception -> 0x0158 }
            throw r4     // Catch:{ Exception -> 0x0158 }
        L_0x0158:
            r2 = move-exception
            r1 = r2
            r2 = 0
        L_0x015b:
            if (r2 == 0) goto L_0x015e
            goto L_0x0162
        L_0x015e:
            java.lang.String r3 = r8.getFailedTLDsList()     // Catch:{ all -> 0x01a0 }
        L_0x0162:
            freemarker.template.TemplateModelException r2 = new freemarker.template.TemplateModelException     // Catch:{ all -> 0x01a0 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ all -> 0x01a0 }
            r4.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r5 = "Error while looking for TLD file for "
            r4.append(r5)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r9)     // Catch:{ all -> 0x01a0 }
            r4.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = "; see cause exception."
            r4.append(r9)     // Catch:{ all -> 0x01a0 }
            if (r3 != 0) goto L_0x017f
            java.lang.String r9 = ""
            goto L_0x0195
        L_0x017f:
            java.lang.StringBuffer r9 = new java.lang.StringBuffer     // Catch:{ all -> 0x01a0 }
            r9.<init>()     // Catch:{ all -> 0x01a0 }
            java.lang.String r5 = " (Note: These TLD-s were skipped earlier due to errors; see errors in the log: "
            r9.append(r5)     // Catch:{ all -> 0x01a0 }
            r9.append(r3)     // Catch:{ all -> 0x01a0 }
            java.lang.String r3 = ")"
            r9.append(r3)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x01a0 }
        L_0x0195:
            r4.append(r9)     // Catch:{ all -> 0x01a0 }
            java.lang.String r9 = r4.toString()     // Catch:{ all -> 0x01a0 }
            r2.<init>(r9, r1)     // Catch:{ all -> 0x01a0 }
            throw r2     // Catch:{ all -> 0x01a0 }
        L_0x01a0:
            r9 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x01a0 }
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.jsp.TaglibFactory.get(java.lang.String):freemarker.template.TemplateModel");
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private String getFailedTLDsList() {
        synchronized (this.failedTldLocations) {
            if (this.failedTldLocations.isEmpty()) {
                return null;
            }
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < this.failedTldLocations.size(); i++) {
                if (i != 0) {
                    stringBuffer.append(", ");
                }
                stringBuffer.append(StringUtil.jQuote(this.failedTldLocations.get(i)));
            }
            String stringBuffer2 = stringBuffer.toString();
            return stringBuffer2;
        }
    }

    public ObjectWrapper getObjectWrapper() {
        return this.objectWrapper;
    }

    public void setObjectWrapper(ObjectWrapper objectWrapper2) {
        checkNotStarted();
        this.objectWrapper = objectWrapper2;
    }

    public List getMetaInfTldSources() {
        return this.metaInfTldSources;
    }

    public void setMetaInfTldSources(List list) {
        checkNotStarted();
        NullArgumentException.check("metaInfTldSources", list);
        this.metaInfTldSources = list;
    }

    public List getClasspathTlds() {
        return this.classpathTlds;
    }

    public void setClasspathTlds(List list) {
        checkNotStarted();
        NullArgumentException.check("classpathTlds", list);
        this.classpathTlds = list;
    }

    private void checkNotStarted() {
        Class cls;
        synchronized (this.lock) {
            if (this.nextTldLocationLookupPhase != 0) {
                StringBuffer stringBuffer = new StringBuffer();
                if (class$freemarker$ext$jsp$TaglibFactory == null) {
                    cls = class$("freemarker.ext.jsp.TaglibFactory");
                    class$freemarker$ext$jsp$TaglibFactory = cls;
                } else {
                    cls = class$freemarker$ext$jsp$TaglibFactory;
                }
                stringBuffer.append(cls.getName());
                stringBuffer.append(" object was already in use.");
                throw new IllegalStateException(stringBuffer.toString());
            }
        }
    }

    private TldLocation getExplicitlyMappedTldLocation(String str) throws SAXException, IOException, TaglibGettingException {
        while (true) {
            TldLocation tldLocation = (TldLocation) this.tldLocations.get(str);
            if (tldLocation != null) {
                return tldLocation;
            }
            int i = this.nextTldLocationLookupPhase;
            if (i == 0) {
                addTldLocationsFromClasspathTlds();
            } else if (i == 1) {
                addTldLocationsFromWebXml();
            } else if (i == 2) {
                addTldLocationsFromWebInfTlds();
            } else if (i == 3) {
                addTldLocationsFromMetaInfTlds();
            } else if (i == 4) {
                return null;
            } else {
                throw new BugException();
            }
            this.nextTldLocationLookupPhase++;
        }
    }

    private void addTldLocationsFromWebXml() throws SAXException, IOException {
        LOG.debug("Looking for TLD locations in servletContext:/WEB-INF/web.xml");
        WebXmlParser webXmlParser = new WebXmlParser();
        InputStream resourceAsStream = this.servletContext.getResourceAsStream("/WEB-INF/web.xml");
        if (resourceAsStream == null) {
            LOG.debug("No web.xml was found in servlet context");
            return;
        }
        try {
            parseXml(resourceAsStream, this.servletContext.getResource("/WEB-INF/web.xml").toExternalForm(), webXmlParser);
        } finally {
            resourceAsStream.close();
        }
    }

    private void addTldLocationsFromWebInfTlds() throws IOException, SAXException {
        LOG.debug("Looking for TLD locations in servletContext:/WEB-INF/**/*.tld");
        addTldLocationsFromServletContextResourceTlds("/WEB-INF");
    }

    private void addTldLocationsFromServletContextResourceTlds(String str) throws IOException, SAXException {
        Set resourcePaths = this.servletContext.getResourcePaths(str);
        if (resourcePaths != null) {
            ArrayList<String> arrayList = new ArrayList<>(resourcePaths);
            Collections.sort(arrayList);
            for (String str2 : arrayList) {
                if (str2.endsWith(".tld")) {
                    addTldLocationFromTld(new ServletContextTldLocation(str2));
                }
            }
            for (String str3 : arrayList) {
                if (str3.endsWith("/")) {
                    addTldLocationsFromServletContextResourceTlds(str3);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addTldLocationsFromMetaInfTlds() throws java.io.IOException, org.xml.sax.SAXException {
        /*
            r12 = this;
            java.util.List r0 = r12.metaInfTldSources
            if (r0 == 0) goto L_0x0105
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x000c
            goto L_0x0105
        L_0x000c:
            r0 = 0
            java.util.List r1 = r12.metaInfTldSources
            int r1 = r1.size()
            int r1 = r1 + -1
        L_0x0015:
            r2 = 0
            if (r1 < 0) goto L_0x0027
            java.util.List r3 = r12.metaInfTldSources
            java.lang.Object r3 = r3.get(r1)
            boolean r3 = r3 instanceof freemarker.ext.jsp.TaglibFactory.ClearMetaInfTldSource
            if (r3 == 0) goto L_0x0024
            goto L_0x00fb
        L_0x0024:
            int r1 = r1 + -1
            goto L_0x0015
        L_0x0027:
            r1 = 0
        L_0x0028:
            java.util.List r3 = r12.metaInfTldSources
            int r3 = r3.size()
            if (r1 >= r3) goto L_0x0105
            java.util.List r3 = r12.metaInfTldSources
            java.lang.Object r3 = r3.get(r1)
            freemarker.ext.jsp.TaglibFactory$MetaInfTldSource r3 = (freemarker.ext.jsp.TaglibFactory.MetaInfTldSource) r3
            freemarker.ext.jsp.TaglibFactory$WebInfPerLibJarMetaInfTldSource r4 = freemarker.ext.jsp.TaglibFactory.WebInfPerLibJarMetaInfTldSource.INSTANCE
            if (r3 != r4) goto L_0x0041
            r12.addTldLocationsFromWebInfPerLibJarMetaInfTlds()
            goto L_0x00fb
        L_0x0041:
            boolean r4 = r3 instanceof freemarker.ext.jsp.TaglibFactory.ClasspathMetaInfTldSource
            if (r4 == 0) goto L_0x00ff
            freemarker.ext.jsp.TaglibFactory$ClasspathMetaInfTldSource r3 = (freemarker.ext.jsp.TaglibFactory.ClasspathMetaInfTldSource) r3
            freemarker.log.Logger r4 = freemarker.ext.jsp.TaglibFactory.LOG
            boolean r4 = r4.isDebugEnabled()
            java.lang.String r5 = "/META-INF/"
            if (r4 == 0) goto L_0x0078
            freemarker.log.Logger r4 = freemarker.ext.jsp.TaglibFactory.LOG
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            java.lang.String r7 = "Looking for TLD-s in classpathRoots["
            r6.append(r7)
            java.util.regex.Pattern r7 = r3.getRootContainerPattern()
            r6.append(r7)
            java.lang.String r7 = "]"
            r6.append(r7)
            r6.append(r5)
            java.lang.String r7 = "**/*.tld"
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r4.debug(r6)
        L_0x0078:
            if (r0 != 0) goto L_0x007e
            java.util.Set r0 = collectMetaInfUrlsFromClassLoaders()
        L_0x007e:
            java.util.Iterator r4 = r0.iterator()
        L_0x0082:
            boolean r6 = r4.hasNext()
            if (r6 == 0) goto L_0x00fb
            java.lang.Object r6 = r4.next()
            freemarker.ext.jsp.TaglibFactory$URLWithExternalForm r6 = (freemarker.ext.jsp.TaglibFactory.URLWithExternalForm) r6
            java.net.URL r7 = r6.getUrl()
            boolean r8 = isJarUrl(r7)
            java.lang.String r9 = r6.externalForm
            if (r8 == 0) goto L_0x00aa
            java.lang.String r10 = "!/"
            int r10 = r9.indexOf(r10)
            r11 = -1
            if (r10 == r11) goto L_0x00ba
            java.lang.String r9 = r9.substring(r2, r10)
            goto L_0x00ba
        L_0x00aa:
            boolean r10 = r9.endsWith(r5)
            if (r10 == 0) goto L_0x00ba
            int r10 = r9.length()
            int r10 = r10 + -9
            java.lang.String r9 = r9.substring(r2, r10)
        L_0x00ba:
            java.util.regex.Pattern r10 = r3.getRootContainerPattern()
            java.util.regex.Matcher r9 = r10.matcher(r9)
            boolean r9 = r9.matches()
            if (r9 == 0) goto L_0x0082
            java.io.File r9 = r12.urlToFileOrNull(r7)
            if (r9 == 0) goto L_0x00d2
            r12.addTldLocationsFromFileDirectory(r9)
            goto L_0x0082
        L_0x00d2:
            if (r8 == 0) goto L_0x00d8
            r12.addTldLocationsFromJarDirectoryEntryURL(r7)
            goto L_0x0082
        L_0x00d8:
            freemarker.log.Logger r7 = freemarker.ext.jsp.TaglibFactory.LOG
            boolean r7 = r7.isDebugEnabled()
            if (r7 == 0) goto L_0x0082
            freemarker.log.Logger r7 = freemarker.ext.jsp.TaglibFactory.LOG
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            r8.<init>()
            java.lang.String r9 = "Can't list entries under this URL; TLD-s won't be discovered here: "
            r8.append(r9)
            java.lang.String r6 = r6.getExternalForm()
            r8.append(r6)
            java.lang.String r6 = r8.toString()
            r7.warn(r6)
            goto L_0x0082
        L_0x00fb:
            int r1 = r1 + 1
            goto L_0x0028
        L_0x00ff:
            freemarker.core.BugException r0 = new freemarker.core.BugException
            r0.<init>()
            throw r0
        L_0x0105:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.jsp.TaglibFactory.addTldLocationsFromMetaInfTlds():void");
    }

    private void addTldLocationsFromWebInfPerLibJarMetaInfTlds() throws IOException, SAXException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Looking for TLD locations in servletContext:/WEB-INF/lib/*.{jar,zip}/META-INF/*.tld");
        }
        Set<String> resourcePaths = this.servletContext.getResourcePaths("/WEB-INF/lib");
        if (resourcePaths != null) {
            for (String str : resourcePaths) {
                if (isJarPath(str)) {
                    addTldLocationsFromServletContextJar(str);
                }
            }
        }
    }

    private void addTldLocationsFromClasspathTlds() throws SAXException, IOException, TaglibGettingException {
        InputStream inputStream;
        List list = this.classpathTlds;
        if (list != null && list.size() != 0) {
            LOG.debug("Looking for TLD locations in TLD-s specified in cfg.classpathTlds");
            for (String str : this.classpathTlds) {
                if (str.trim().length() != 0) {
                    if (!str.startsWith("/")) {
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("/");
                        stringBuffer.append(str);
                        str = stringBuffer.toString();
                    }
                    if (!str.endsWith("/")) {
                        ClasspathTldLocation classpathTldLocation = new ClasspathTldLocation(str);
                        try {
                            inputStream = classpathTldLocation.getInputStream();
                        } catch (IOException e) {
                            if (LOG.isWarnEnabled()) {
                                Logger logger = LOG;
                                StringBuffer stringBuffer2 = new StringBuffer();
                                stringBuffer2.append("Ignored classpath TLD location ");
                                stringBuffer2.append(StringUtil.jQuoteNoXSS(str));
                                stringBuffer2.append(" because of error");
                                logger.warn(stringBuffer2.toString(), e);
                            }
                            inputStream = null;
                        }
                        if (inputStream != null) {
                            try {
                                addTldLocationFromTld(inputStream, classpathTldLocation);
                            } finally {
                                inputStream.close();
                            }
                        }
                    } else {
                        StringBuffer stringBuffer3 = new StringBuffer();
                        stringBuffer3.append("classpathTlds can't specify a directory: ");
                        stringBuffer3.append(str);
                        throw new TaglibGettingException(stringBuffer3.toString());
                    }
                } else {
                    throw new TaglibGettingException("classpathTlds can't contain empty item");
                }
            }
        }
    }

    private void addTldLocationsFromServletContextJar(String str) throws IOException, MalformedURLException, SAXException {
        ZipInputStream zipInputStream;
        String normalizeJarEntryPath = normalizeJarEntryPath(META_INF_ABS_PATH, true);
        JarFile servletContextResourceToFileOrNull = servletContextResourceToFileOrNull(str);
        if (servletContextResourceToFileOrNull != null) {
            if (LOG.isDebugEnabled()) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Scanning for /META-INF/*.tld-s in JarFile: servletContext:");
                stringBuffer.append(str);
                logger.debug(stringBuffer.toString());
            }
            Enumeration<JarEntry> entries = servletContextResourceToFileOrNull.entries();
            while (entries.hasMoreElements()) {
                String normalizeJarEntryPath2 = normalizeJarEntryPath(entries.nextElement().getName(), false);
                if (normalizeJarEntryPath2.startsWith(normalizeJarEntryPath) && normalizeJarEntryPath2.endsWith(".tld")) {
                    addTldLocationFromTld(new ServletContextJarEntryTldLocation(str, normalizeJarEntryPath2));
                }
            }
            return;
        }
        if (LOG.isDebugEnabled()) {
            Logger logger2 = LOG;
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Scanning for /META-INF/*.tld-s in ZipInputStream (slow): servletContext:");
            stringBuffer2.append(str);
            logger2.debug(stringBuffer2.toString());
        }
        InputStream resourceAsStream = this.servletContext.getResourceAsStream(str);
        if (resourceAsStream != null) {
            try {
                zipInputStream = new ZipInputStream(resourceAsStream);
                while (true) {
                    ZipEntry nextEntry = zipInputStream.getNextEntry();
                    if (nextEntry == null) {
                        zipInputStream.close();
                        resourceAsStream.close();
                        return;
                    }
                    String normalizeJarEntryPath3 = normalizeJarEntryPath(nextEntry.getName(), false);
                    if (normalizeJarEntryPath3.startsWith(normalizeJarEntryPath) && normalizeJarEntryPath3.endsWith(".tld")) {
                        addTldLocationFromTld(zipInputStream, new ServletContextJarEntryTldLocation(str, normalizeJarEntryPath3));
                    }
                }
            } catch (Throwable th) {
                resourceAsStream.close();
                throw th;
            }
        } else {
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append("ServletContext resource not found: ");
            stringBuffer3.append(str);
            throw new IOException(stringBuffer3.toString());
        }
    }

    private void addTldLocationsFromJarDirectoryEntryURL(URL url) throws IOException, MalformedURLException, SAXException {
        String str;
        JarFile jarFile;
        String str2;
        ZipInputStream zipInputStream;
        URLConnection openConnection = url.openConnection();
        if (this.test_emulateNoJarURLConnections || !(openConnection instanceof JarURLConnection)) {
            String externalForm = url.toExternalForm();
            int indexOf = externalForm.indexOf(JAR_URL_ENTRY_PATH_START);
            if (indexOf != -1) {
                str = externalForm.substring(externalForm.indexOf(58) + 1, indexOf);
                str2 = normalizeJarEntryPath(externalForm.substring(indexOf + 2), true);
                File urlToFileOrNull = urlToFileOrNull(new URL(str));
                jarFile = urlToFileOrNull != null ? new JarFile(urlToFileOrNull) : null;
            } else {
                throw newFailedToExtractEntryPathException(url);
            }
        } else {
            JarURLConnection jarURLConnection = (JarURLConnection) openConnection;
            jarFile = jarURLConnection.getJarFile();
            str2 = normalizeJarEntryPath(jarURLConnection.getEntryName(), true);
            if (str2 != null) {
                str = null;
            } else {
                throw newFailedToExtractEntryPathException(url);
            }
        }
        if (jarFile != null) {
            if (LOG.isDebugEnabled()) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Scanning for /META-INF/**/*.tld-s in random access mode: ");
                stringBuffer.append(url);
                logger.debug(stringBuffer.toString());
            }
            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                String normalizeJarEntryPath = normalizeJarEntryPath(entries.nextElement().getName(), false);
                if (normalizeJarEntryPath.startsWith(str2) && normalizeJarEntryPath.endsWith(".tld")) {
                    addTldLocationFromTld(new JarEntryUrlTldLocation(createJarEntryUrl(url, normalizeJarEntryPath.substring(str2.length())), null));
                }
            }
            return;
        }
        if (LOG.isDebugEnabled()) {
            Logger logger2 = LOG;
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Scanning for /META-INF/**/*.tld-s in stream mode (slow): ");
            stringBuffer2.append(str);
            logger2.debug(stringBuffer2.toString());
        }
        InputStream openStream = new URL(str).openStream();
        try {
            zipInputStream = new ZipInputStream(openStream);
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    zipInputStream.close();
                    openStream.close();
                    return;
                }
                String normalizeJarEntryPath2 = normalizeJarEntryPath(nextEntry.getName(), false);
                if (normalizeJarEntryPath2.startsWith(str2) && normalizeJarEntryPath2.endsWith(".tld")) {
                    addTldLocationFromTld(zipInputStream, new JarEntryUrlTldLocation(createJarEntryUrl(url, normalizeJarEntryPath2.substring(str2.length())), null));
                }
            }
        } catch (Throwable th) {
            openStream.close();
            throw th;
        }
    }

    private void addTldLocationsFromFileDirectory(File file) throws IOException, SAXException {
        if (file.isDirectory()) {
            if (LOG.isDebugEnabled()) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Scanning for *.tld-s in File directory: ");
                stringBuffer.append(StringUtil.jQuoteNoXSS(file));
                logger.debug(stringBuffer.toString());
            }
            for (File file2 : file.listFiles(new FilenameFilter() {
                /* class freemarker.ext.jsp.TaglibFactory.C33501 */

                public boolean accept(File file, String str) {
                    return TaglibFactory.isTldFileNameIgnoreCase(str);
                }
            })) {
                addTldLocationFromTld(new FileTldLocation(file2));
            }
            return;
        }
        Logger logger2 = LOG;
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("Skipped scanning for *.tld for non-existent directory: ");
        stringBuffer2.append(StringUtil.jQuoteNoXSS(file));
        logger2.warn(stringBuffer2.toString());
    }

    private void addTldLocationFromTld(TldLocation tldLocation) throws IOException, SAXException {
        InputStream inputStream = tldLocation.getInputStream();
        try {
            addTldLocationFromTld(inputStream, tldLocation);
        } finally {
            inputStream.close();
        }
    }

    private void addTldLocationFromTld(InputStream inputStream, TldLocation tldLocation) throws SAXException, IOException {
        String str;
        try {
            str = getTaglibUriFromTld(inputStream, tldLocation.getXmlSystemId());
        } catch (SAXException e) {
            Logger logger = LOG;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Error while parsing TLD; skipping: ");
            stringBuffer.append(tldLocation);
            logger.error(stringBuffer.toString(), e);
            synchronized (this.failedTldLocations) {
                this.failedTldLocations.add(tldLocation.toString());
                str = null;
            }
        }
        if (str != null) {
            addTldLocation(tldLocation, str);
        }
    }

    /* access modifiers changed from: private */
    public void addTldLocation(TldLocation tldLocation, String str) {
        if (!this.tldLocations.containsKey(str)) {
            this.tldLocations.put(str, tldLocation);
            if (LOG.isDebugEnabled()) {
                Logger logger = LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Mapped taglib URI ");
                stringBuffer.append(StringUtil.jQuoteNoXSS(str));
                stringBuffer.append(" to TLD location ");
                stringBuffer.append(StringUtil.jQuoteNoXSS(tldLocation));
                logger.debug(stringBuffer.toString());
            }
        } else if (LOG.isDebugEnabled()) {
            Logger logger2 = LOG;
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Ignored duplicate mapping of taglib URI ");
            stringBuffer2.append(StringUtil.jQuoteNoXSS(str));
            stringBuffer2.append(" to TLD location ");
            stringBuffer2.append(StringUtil.jQuoteNoXSS(tldLocation));
            logger2.debug(stringBuffer2.toString());
        }
    }

    private static Set collectMetaInfUrlsFromClassLoaders() throws IOException {
        TreeSet treeSet = new TreeSet();
        ClassLoader tryGetThreadContextClassLoader = tryGetThreadContextClassLoader();
        if (tryGetThreadContextClassLoader != null) {
            collectMetaInfUrlsFromClassLoader(tryGetThreadContextClassLoader, treeSet);
        }
        Class cls = class$freemarker$ext$jsp$TaglibFactory;
        if (cls == null) {
            cls = class$("freemarker.ext.jsp.TaglibFactory");
            class$freemarker$ext$jsp$TaglibFactory = cls;
        }
        ClassLoader classLoader = cls.getClassLoader();
        if (!isDescendantOfOrSameAs(tryGetThreadContextClassLoader, classLoader)) {
            collectMetaInfUrlsFromClassLoader(classLoader, treeSet);
        }
        return treeSet;
    }

    private static void collectMetaInfUrlsFromClassLoader(ClassLoader classLoader, Set set) throws IOException {
        Enumeration<URL> resources = classLoader.getResources(META_INF_REL_PATH);
        if (resources != null) {
            while (resources.hasMoreElements()) {
                set.add(new URLWithExternalForm(resources.nextElement()));
            }
        }
    }

    private String getTaglibUriFromTld(InputStream inputStream, String str) throws SAXException, IOException {
        TldParserForTaglibUriExtraction tldParserForTaglibUriExtraction = new TldParserForTaglibUriExtraction();
        parseXml(inputStream, str, tldParserForTaglibUriExtraction);
        return tldParserForTaglibUriExtraction.getTaglibUri();
    }

    private TemplateHashModel loadTaglib(TldLocation tldLocation, String str) throws IOException, SAXException {
        if (LOG.isDebugEnabled()) {
            Logger logger = LOG;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Loading taglib for URI ");
            stringBuffer.append(StringUtil.jQuoteNoXSS(str));
            stringBuffer.append(" from TLD location ");
            stringBuffer.append(StringUtil.jQuoteNoXSS(tldLocation));
            logger.debug(stringBuffer.toString());
        }
        Taglib taglib = new Taglib(this.servletContext, tldLocation, this.objectWrapper);
        this.taglibs.put(str, taglib);
        this.tldLocations.remove(str);
        return taglib;
    }

    /* access modifiers changed from: private */
    public static void parseXml(InputStream inputStream, String str, DefaultHandler defaultHandler) throws SAXException, IOException {
        InputSource inputSource = new InputSource();
        inputSource.setSystemId(str);
        inputSource.setByteStream(toCloseIgnoring(inputStream));
        SAXParserFactory newInstance = SAXParserFactory.newInstance();
        newInstance.setNamespaceAware(false);
        newInstance.setValidating(false);
        try {
            XMLReader xMLReader = newInstance.newSAXParser().getXMLReader();
            xMLReader.setEntityResolver(new LocalDtdEntityResolver());
            xMLReader.setContentHandler(defaultHandler);
            xMLReader.setErrorHandler(defaultHandler);
            xMLReader.parse(inputSource);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException("XML parser setup failed", e);
        }
    }

    private static String resolveRelativeUri(String str) throws TaglibGettingException {
        try {
            TemplateModel variable = Environment.getCurrentEnvironment().getVariable(FreemarkerServlet.KEY_REQUEST_PRIVATE);
            if (variable instanceof HttpRequestHashModel) {
                HttpServletRequest request = ((HttpRequestHashModel) variable).getRequest();
                String pathInfo = request.getPathInfo();
                String servletPath = request.getServletPath();
                if (servletPath == null) {
                    servletPath = "";
                }
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(servletPath);
                if (pathInfo == null) {
                    pathInfo = "";
                }
                stringBuffer.append(pathInfo);
                String stringBuffer2 = stringBuffer.toString();
                int lastIndexOf = stringBuffer2.lastIndexOf(47);
                if (lastIndexOf != -1) {
                    StringBuffer stringBuffer3 = new StringBuffer();
                    stringBuffer3.append(stringBuffer2.substring(0, lastIndexOf + 1));
                    stringBuffer3.append(str);
                    return stringBuffer3.toString();
                }
                StringBuffer stringBuffer4 = new StringBuffer();
                stringBuffer4.append('/');
                stringBuffer4.append(str);
                return stringBuffer4.toString();
            }
            StringBuffer stringBuffer5 = new StringBuffer();
            stringBuffer5.append("Can't resolve relative URI ");
            stringBuffer5.append(str);
            stringBuffer5.append(" as request URL information is unavailable.");
            throw new TaglibGettingException(stringBuffer5.toString());
        } catch (TemplateModelException e) {
            throw new TaglibGettingException("Failed to get FreemarkerServlet request information", e);
        }
    }

    private static FilterInputStream toCloseIgnoring(InputStream inputStream) {
        return new FilterInputStream(inputStream) {
            /* class freemarker.ext.jsp.TaglibFactory.C33512 */

            public void close() {
            }
        };
    }

    /* access modifiers changed from: private */
    public static int getUriType(String str) throws MalformedURLException {
        int indexOf;
        if (str == null) {
            throw new IllegalArgumentException("null is not a valid URI");
        } else if (str.length() != 0) {
            char charAt = str.charAt(0);
            if (charAt == '/') {
                return 1;
            }
            if (charAt < 'a' || charAt > 'z' || (indexOf = str.indexOf(58)) == -1) {
                return 2;
            }
            for (int i = 1; i < indexOf; i++) {
                char charAt2 = str.charAt(i);
                if ((charAt2 < 'a' || charAt2 > 'z') && ((charAt2 < '0' || charAt2 > '9') && charAt2 != '+' && charAt2 != '-' && charAt2 != '.')) {
                    return 2;
                }
            }
            return 0;
        } else {
            throw new MalformedURLException("empty string is not a valid URI");
        }
    }

    /* access modifiers changed from: private */
    public static boolean isJarPath(String str) {
        return str.endsWith(".jar") || str.endsWith(".zip");
    }

    private static boolean isJarUrl(URL url) {
        String protocol = url.getProtocol();
        return "jar".equals(protocol) || MimeType.ZIP.equals(protocol) || "vfszip".equals(protocol) || "wsjar".equals(protocol);
    }

    private static URL createJarEntryUrl(URL url, String str) throws MalformedURLException {
        if (str.startsWith("/")) {
            str = str.substring(1);
        }
        try {
            return new URL(url, StringUtil.URLPathEnc(str, PLATFORM_FILE_ENCODING));
        } catch (UnsupportedEncodingException unused) {
            throw new BugException();
        }
    }

    /* access modifiers changed from: private */
    public static String normalizeJarEntryPath(String str, boolean z) {
        if (!str.startsWith("/")) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("/");
            stringBuffer.append(str);
            str = stringBuffer.toString();
        }
        if (!z || str.endsWith("/")) {
            return str;
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append(str);
        stringBuffer2.append("/");
        return stringBuffer2.toString();
    }

    private static MalformedURLException newFailedToExtractEntryPathException(URL url) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Failed to extract jar entry path from: ");
        stringBuffer.append(url);
        return new MalformedURLException(stringBuffer.toString());
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0032, code lost:
        throw new freemarker.core.BugException(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r4 = java.net.URLDecoder.decode(r4.getFile(), freemarker.ext.jsp.TaglibFactory.PLATFORM_FILE_ENCODING);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x001c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.File urlToFileOrNull(java.net.URL r4) {
        /*
            r3 = this;
            boolean r0 = r3.test_emulateNoUrlToFileConversions
            r1 = 0
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            java.lang.String r0 = r4.getProtocol()
            java.lang.String r2 = "file"
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x0013
            return r1
        L_0x0013:
            java.net.URI r0 = toUri(r4)     // Catch:{ URISyntaxException -> 0x001c }
            java.lang.String r4 = r0.getSchemeSpecificPart()     // Catch:{ URISyntaxException -> 0x001c }
            goto L_0x0026
        L_0x001c:
            java.lang.String r4 = r4.getFile()     // Catch:{ UnsupportedEncodingException -> 0x002c }
            java.lang.String r0 = freemarker.ext.jsp.TaglibFactory.PLATFORM_FILE_ENCODING     // Catch:{ UnsupportedEncodingException -> 0x002c }
            java.lang.String r4 = java.net.URLDecoder.decode(r4, r0)     // Catch:{ UnsupportedEncodingException -> 0x002c }
        L_0x0026:
            java.io.File r0 = new java.io.File
            r0.<init>(r4)
            return r0
        L_0x002c:
            r4 = move-exception
            freemarker.core.BugException r0 = new freemarker.core.BugException
            r0.<init>(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.jsp.TaglibFactory.urlToFileOrNull(java.net.URL):java.io.File");
    }

    private static URI toUri(URL url) throws URISyntaxException {
        Method method = toURIMethod;
        if (method == null) {
            return new URI(url.toString());
        }
        try {
            return (URI) method.invoke(url, new Object[0]);
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof URISyntaxException) {
                throw ((URISyntaxException) targetException);
            } else if (targetException instanceof RuntimeException) {
                throw ((RuntimeException) targetException);
            } else {
                throw new RuntimeException("toURI() call failed", e);
            }
        } catch (Exception e2) {
            throw new RuntimeException("toURI() call failed", e2);
        }
    }

    private JarFile servletContextResourceToFileOrNull(String str) throws MalformedURLException, IOException {
        URL resource = this.servletContext.getResource(str);
        if (resource == null) {
            Logger logger = LOG;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("ServletContext resource URL was null (missing resource?): ");
            stringBuffer.append(str);
            logger.error(stringBuffer.toString());
            return null;
        }
        File urlToFileOrNull = urlToFileOrNull(resource);
        if (urlToFileOrNull == null) {
            return null;
        }
        if (urlToFileOrNull.isFile()) {
            return new JarFile(urlToFileOrNull);
        }
        Logger logger2 = LOG;
        StringBuffer stringBuffer2 = new StringBuffer();
        stringBuffer2.append("Jar file doesn't exist - falling back to stream mode: ");
        stringBuffer2.append(urlToFileOrNull);
        logger2.error(stringBuffer2.toString());
        return null;
    }

    /* access modifiers changed from: private */
    public static URL tryCreateServletContextJarEntryUrl(ServletContext servletContext2, String str, String str2) {
        try {
            URL resource = servletContext2.getResource(str);
            if (resource != null) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("jar:");
                stringBuffer.append(toUri(resource));
                stringBuffer.append(JAR_URL_ENTRY_PATH_START);
                stringBuffer.append(URLEncoder.encode(str2.startsWith("/") ? str2.substring(1) : str2, PLATFORM_FILE_ENCODING));
                return new URL(stringBuffer.toString());
            }
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Servlet context resource not found: ");
            stringBuffer2.append(str);
            throw new IOException(stringBuffer2.toString());
        } catch (Exception e) {
            Logger logger = LOG;
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append("Couldn't get URL for serlvetContext resource ");
            stringBuffer3.append(StringUtil.jQuoteNoXSS(str));
            stringBuffer3.append(" / jar entry ");
            stringBuffer3.append(StringUtil.jQuoteNoXSS(str2));
            logger.error(stringBuffer3.toString(), e);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static boolean isTldFileNameIgnoreCase(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf < 0) {
            return false;
        }
        return str.substring(lastIndexOf + 1).toLowerCase().equalsIgnoreCase("tld");
    }

    /* access modifiers changed from: private */
    public static ClassLoader tryGetThreadContextClassLoader() {
        try {
            return Thread.currentThread().getContextClassLoader();
        } catch (SecurityException e) {
            LOG.warn("Can't access Thread Context ClassLoader", e);
            return null;
        }
    }

    private static boolean isDescendantOfOrSameAs(ClassLoader classLoader, ClassLoader classLoader2) {
        while (classLoader != null) {
            if (classLoader == classLoader2) {
                return true;
            }
            classLoader = classLoader.getParent();
        }
        return false;
    }

    public static abstract class MetaInfTldSource {
        private MetaInfTldSource() {
        }
    }

    public static final class WebInfPerLibJarMetaInfTldSource extends MetaInfTldSource {
        public static final WebInfPerLibJarMetaInfTldSource INSTANCE = new WebInfPerLibJarMetaInfTldSource();

        private WebInfPerLibJarMetaInfTldSource() {
            super();
        }
    }

    public static final class ClasspathMetaInfTldSource extends MetaInfTldSource {
        private final Pattern rootContainerPattern;

        public ClasspathMetaInfTldSource(Pattern pattern) {
            super();
            this.rootContainerPattern = pattern;
        }

        public Pattern getRootContainerPattern() {
            return this.rootContainerPattern;
        }
    }

    public static final class ClearMetaInfTldSource extends MetaInfTldSource {
        public static final ClearMetaInfTldSource INSTANCE = new ClearMetaInfTldSource();

        private ClearMetaInfTldSource() {
            super();
        }
    }

    private class ServletContextTldLocation implements TldLocation {
        private final String fileResourcePath;

        public ServletContextTldLocation(String str) {
            this.fileResourcePath = str;
        }

        public InputStream getInputStream() throws IOException {
            InputStream resourceAsStream = TaglibFactory.this.servletContext.getResourceAsStream(this.fileResourcePath);
            if (resourceAsStream != null) {
                return resourceAsStream;
            }
            throw newResourceNotFoundException();
        }

        public String getXmlSystemId() throws IOException {
            URL resource = TaglibFactory.this.servletContext.getResource(this.fileResourcePath);
            if (resource != null) {
                return resource.toExternalForm();
            }
            return null;
        }

        private IOException newResourceNotFoundException() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Resource not found: servletContext:");
            stringBuffer.append(this.fileResourcePath);
            return new IOException(stringBuffer.toString());
        }

        public final String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("servletContext:");
            stringBuffer.append(this.fileResourcePath);
            return stringBuffer.toString();
        }
    }

    private static class ClasspathTldLocation implements TldLocation {
        private final String resourcePath;

        public ClasspathTldLocation(String str) {
            if (str.startsWith("/")) {
                this.resourcePath = str;
                return;
            }
            throw new IllegalArgumentException("\"resourcePath\" must start with /");
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("classpath:");
            stringBuffer.append(this.resourcePath);
            return stringBuffer.toString();
        }

        public InputStream getInputStream() throws IOException {
            InputStream resourceAsStream;
            if (TaglibFactory.tryGetThreadContextClassLoader() != null && (resourceAsStream = getClass().getResourceAsStream(this.resourcePath)) != null) {
                return resourceAsStream;
            }
            InputStream resourceAsStream2 = getClass().getResourceAsStream(this.resourcePath);
            if (resourceAsStream2 != null) {
                return resourceAsStream2;
            }
            throw newResourceNotFoundException();
        }

        public String getXmlSystemId() throws IOException {
            URL resource;
            if (TaglibFactory.tryGetThreadContextClassLoader() != null && (resource = getClass().getResource(this.resourcePath)) != null) {
                return resource.toExternalForm();
            }
            URL resource2 = getClass().getResource(this.resourcePath);
            if (resource2 == null) {
                return null;
            }
            return resource2.toExternalForm();
        }

        private IOException newResourceNotFoundException() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Resource not found: classpath:");
            stringBuffer.append(this.resourcePath);
            return new IOException(stringBuffer.toString());
        }
    }

    private abstract class JarEntryTldLocation implements TldLocation {
        private final String entryPath;
        private final URL entryUrl;
        private final InputStreamFactory fallbackRawJarContentInputStreamFactory;

        public JarEntryTldLocation(URL url, InputStreamFactory inputStreamFactory, String str) {
            if (url == null) {
                NullArgumentException.check(inputStreamFactory);
                NullArgumentException.check(str);
            }
            this.entryUrl = url;
            this.fallbackRawJarContentInputStreamFactory = inputStreamFactory;
            this.entryPath = str != null ? TaglibFactory.normalizeJarEntryPath(str, false) : null;
        }

        /* JADX WARNING: Removed duplicated region for block: B:53:0x00df  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x00e4  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.io.InputStream getInputStream() throws java.io.IOException {
            /*
                r6 = this;
                java.net.URL r0 = r6.entryUrl
                if (r0 == 0) goto L_0x004c
                freemarker.ext.jsp.TaglibFactory r0 = freemarker.ext.jsp.TaglibFactory.this     // Catch:{ Exception -> 0x0019 }
                boolean r0 = r0.test_emulateJarEntryUrlOpenStreamFails     // Catch:{ Exception -> 0x0019 }
                if (r0 != 0) goto L_0x0011
                java.net.URL r0 = r6.entryUrl     // Catch:{ Exception -> 0x0019 }
                java.io.InputStream r0 = r0.openStream()     // Catch:{ Exception -> 0x0019 }
                return r0
            L_0x0011:
                java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x0019 }
                java.lang.String r1 = "Test only"
                r0.<init>(r1)     // Catch:{ Exception -> 0x0019 }
                throw r0     // Catch:{ Exception -> 0x0019 }
            L_0x0019:
                r0 = move-exception
                freemarker.ext.jsp.TaglibFactory$InputStreamFactory r1 = r6.fallbackRawJarContentInputStreamFactory
                if (r1 != 0) goto L_0x0032
                boolean r1 = r0 instanceof java.io.IOException
                if (r1 != 0) goto L_0x002f
                boolean r1 = r0 instanceof java.lang.RuntimeException
                if (r1 == 0) goto L_0x0029
                java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0
                throw r0
            L_0x0029:
                java.lang.RuntimeException r1 = new java.lang.RuntimeException
                r1.<init>(r0)
                throw r1
            L_0x002f:
                java.io.IOException r0 = (java.io.IOException) r0
                throw r0
            L_0x0032:
                freemarker.log.Logger r0 = freemarker.ext.jsp.TaglibFactory.LOG
                java.lang.StringBuffer r1 = new java.lang.StringBuffer
                r1.<init>()
                java.lang.String r2 = "Failed to open InputStream for URL (will try fallback stream): "
                r1.append(r2)
                java.net.URL r2 = r6.entryUrl
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.error(r1)
            L_0x004c:
                java.lang.String r0 = r6.entryPath
                r1 = 0
                if (r0 == 0) goto L_0x0052
                goto L_0x0075
            L_0x0052:
                java.net.URL r0 = r6.entryUrl
                if (r0 == 0) goto L_0x00ff
                java.lang.String r0 = r0.toExternalForm()
                java.lang.String r2 = "!/"
                int r2 = r0.indexOf(r2)
                r3 = -1
                if (r2 == r3) goto L_0x00e8
                int r2 = r2 + 2
                java.lang.String r0 = r0.substring(r2)
                java.lang.String r2 = freemarker.ext.jsp.TaglibFactory.PLATFORM_FILE_ENCODING
                java.lang.String r0 = java.net.URLDecoder.decode(r0, r2)
                java.lang.String r0 = freemarker.ext.jsp.TaglibFactory.normalizeJarEntryPath(r0, r1)
            L_0x0075:
                r2 = 0
                freemarker.ext.jsp.TaglibFactory$InputStreamFactory r3 = r6.fallbackRawJarContentInputStreamFactory     // Catch:{ all -> 0x00db }
                java.io.InputStream r3 = r3.getInputStream()     // Catch:{ all -> 0x00db }
                if (r3 == 0) goto L_0x00bd
                java.util.zip.ZipInputStream r4 = new java.util.zip.ZipInputStream     // Catch:{ all -> 0x00bb }
                r4.<init>(r3)     // Catch:{ all -> 0x00bb }
            L_0x0083:
                java.util.zip.ZipEntry r2 = r4.getNextEntry()     // Catch:{ all -> 0x00b8 }
                if (r2 == 0) goto L_0x0098
                java.lang.String r2 = r2.getName()     // Catch:{ all -> 0x00b8 }
                java.lang.String r2 = freemarker.ext.jsp.TaglibFactory.normalizeJarEntryPath(r2, r1)     // Catch:{ all -> 0x00b8 }
                boolean r2 = r0.equals(r2)     // Catch:{ all -> 0x00b8 }
                if (r2 == 0) goto L_0x0083
                return r4
            L_0x0098:
                java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x00b8 }
                java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ all -> 0x00b8 }
                r2.<init>()     // Catch:{ all -> 0x00b8 }
                java.lang.String r5 = "Could not find JAR entry "
                r2.append(r5)     // Catch:{ all -> 0x00b8 }
                java.lang.String r0 = freemarker.template.utility.StringUtil.jQuoteNoXSS(r0)     // Catch:{ all -> 0x00b8 }
                r2.append(r0)     // Catch:{ all -> 0x00b8 }
                java.lang.String r0 = "."
                r2.append(r0)     // Catch:{ all -> 0x00b8 }
                java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x00b8 }
                r1.<init>(r0)     // Catch:{ all -> 0x00b8 }
                throw r1     // Catch:{ all -> 0x00b8 }
            L_0x00b8:
                r0 = move-exception
                r2 = r4
                goto L_0x00dd
            L_0x00bb:
                r0 = move-exception
                goto L_0x00dd
            L_0x00bd:
                java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x00bb }
                java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ all -> 0x00bb }
                r1.<init>()     // Catch:{ all -> 0x00bb }
                java.lang.String r4 = "Jar's InputStreamFactory ("
                r1.append(r4)     // Catch:{ all -> 0x00bb }
                freemarker.ext.jsp.TaglibFactory$InputStreamFactory r4 = r6.fallbackRawJarContentInputStreamFactory     // Catch:{ all -> 0x00bb }
                r1.append(r4)     // Catch:{ all -> 0x00bb }
                java.lang.String r4 = ") says the resource doesn't exist."
                r1.append(r4)     // Catch:{ all -> 0x00bb }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00bb }
                r0.<init>(r1)     // Catch:{ all -> 0x00bb }
                throw r0     // Catch:{ all -> 0x00bb }
            L_0x00db:
                r0 = move-exception
                r3 = r2
            L_0x00dd:
                if (r2 == 0) goto L_0x00e2
                r2.close()
            L_0x00e2:
                if (r3 == 0) goto L_0x00e7
                r3.close()
            L_0x00e7:
                throw r0
            L_0x00e8:
                java.io.IOException r1 = new java.io.IOException
                java.lang.StringBuffer r2 = new java.lang.StringBuffer
                r2.<init>()
                java.lang.String r3 = "Couldn't extract jar entry path from: "
                r2.append(r3)
                r2.append(r0)
                java.lang.String r0 = r2.toString()
                r1.<init>(r0)
                throw r1
            L_0x00ff:
                java.io.IOException r0 = new java.io.IOException
                java.lang.String r1 = "Nothing to deduce jar entry path from."
                r0.<init>(r1)
                goto L_0x0108
            L_0x0107:
                throw r0
            L_0x0108:
                goto L_0x0107
            */
            throw new UnsupportedOperationException("Method not decompiled: freemarker.ext.jsp.TaglibFactory.JarEntryTldLocation.getInputStream():java.io.InputStream");
        }

        public String getXmlSystemId() {
            URL url = this.entryUrl;
            if (url != null) {
                return url.toExternalForm();
            }
            return null;
        }

        public String toString() {
            URL url = this.entryUrl;
            if (url != null) {
                return url.toExternalForm();
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("jar:{");
            stringBuffer.append(this.fallbackRawJarContentInputStreamFactory);
            stringBuffer.append("}!");
            stringBuffer.append(this.entryPath);
            return stringBuffer.toString();
        }
    }

    private class JarEntryUrlTldLocation extends JarEntryTldLocation {
        private JarEntryUrlTldLocation(URL url, InputStreamFactory inputStreamFactory) {
            super(url, inputStreamFactory, null);
        }
    }

    private class ServletContextJarEntryTldLocation extends JarEntryTldLocation {
        private ServletContextJarEntryTldLocation(String str, String str2) {
            super(TaglibFactory.tryCreateServletContextJarEntryUrl(TaglibFactory.this.servletContext, str, str2), new SerlvetContextInputStreamFactory(str), str2);
        }
    }

    private final class SerlvetContextInputStreamFactory implements InputStreamFactory {
        private final String servletContextJarFilePath;

        private SerlvetContextInputStreamFactory(String str) {
            this.servletContextJarFilePath = str;
        }

        public InputStream getInputStream() {
            return TaglibFactory.this.servletContext.getResourceAsStream(this.servletContextJarFilePath);
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("servletContext:");
            stringBuffer.append(this.servletContextJarFilePath);
            return stringBuffer.toString();
        }
    }

    private static class FileTldLocation implements TldLocation {
        private final File file;

        public FileTldLocation(File file2) {
            this.file = file2;
        }

        public InputStream getInputStream() throws IOException {
            return new FileInputStream(this.file);
        }

        public String getXmlSystemId() throws IOException {
            return this.file.toURI().toURL().toExternalForm();
        }

        public String toString() {
            return this.file.toString();
        }
    }

    private static final class Taglib implements TemplateHashModel {
        private final Map tagsAndFunctions;

        Taglib(ServletContext servletContext, TldLocation tldLocation, ObjectWrapper objectWrapper) throws IOException, SAXException {
            this.tagsAndFunctions = parseToTagsAndFunctions(servletContext, tldLocation, objectWrapper);
        }

        public TemplateModel get(String str) {
            return (TemplateModel) this.tagsAndFunctions.get(str);
        }

        public boolean isEmpty() {
            return this.tagsAndFunctions.isEmpty();
        }

        /* JADX INFO: finally extract failed */
        private static final Map parseToTagsAndFunctions(ServletContext servletContext, TldLocation tldLocation, ObjectWrapper objectWrapper) throws IOException, SAXException {
            Class cls;
            Class cls2;
            TldParserForTaglibBuilding tldParserForTaglibBuilding = new TldParserForTaglibBuilding(objectWrapper);
            InputStream inputStream = tldLocation.getInputStream();
            try {
                TaglibFactory.parseXml(inputStream, tldLocation.getXmlSystemId(), tldParserForTaglibBuilding);
                inputStream.close();
                EventForwarding instance = EventForwarding.getInstance(servletContext);
                if (instance != null) {
                    instance.addListeners(tldParserForTaglibBuilding.getListeners());
                } else if (tldParserForTaglibBuilding.getListeners().size() > 0) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Event listeners specified in the TLD could not be  registered since the web application doesn't have a listener of class ");
                    if (TaglibFactory.class$freemarker$ext$jsp$EventForwarding == null) {
                        cls = TaglibFactory.class$("freemarker.ext.jsp.EventForwarding");
                        TaglibFactory.class$freemarker$ext$jsp$EventForwarding = cls;
                    } else {
                        cls = TaglibFactory.class$freemarker$ext$jsp$EventForwarding;
                    }
                    stringBuffer.append(cls.getName());
                    stringBuffer.append(". To remedy this, add this element to web.xml:\n");
                    stringBuffer.append("| <listener>\n");
                    stringBuffer.append("|   <listener-class>");
                    if (TaglibFactory.class$freemarker$ext$jsp$EventForwarding == null) {
                        cls2 = TaglibFactory.class$("freemarker.ext.jsp.EventForwarding");
                        TaglibFactory.class$freemarker$ext$jsp$EventForwarding = cls2;
                    } else {
                        cls2 = TaglibFactory.class$freemarker$ext$jsp$EventForwarding;
                    }
                    stringBuffer.append(cls2.getName());
                    stringBuffer.append("</listener-class>\n");
                    stringBuffer.append("| </listener>");
                    throw new TldParsingSAXException(stringBuffer.toString(), null);
                }
                return tldParserForTaglibBuilding.getTagsAndFunctions();
            } catch (Throwable th) {
                inputStream.close();
                throw th;
            }
        }
    }

    private class WebXmlParser extends DefaultHandler {
        private static final String E_TAGLIB = "taglib";
        private static final String E_TAGLIB_LOCATION = "taglib-location";
        private static final String E_TAGLIB_URI = "taglib-uri";
        private StringBuffer cDataCollector;
        private Locator locator;
        private String taglibLocationCData;
        private String taglibUriCData;

        private WebXmlParser() {
        }

        public void setDocumentLocator(Locator locator2) {
            this.locator = locator2;
        }

        public void startElement(String str, String str2, String str3, Attributes attributes) {
            if (E_TAGLIB_URI.equals(str3) || E_TAGLIB_LOCATION.equals(str3)) {
                this.cDataCollector = new StringBuffer();
            }
        }

        public void characters(char[] cArr, int i, int i2) {
            StringBuffer stringBuffer = this.cDataCollector;
            if (stringBuffer != null) {
                stringBuffer.append(cArr, i, i2);
            }
        }

        public void endElement(String str, String str2, String str3) throws TldParsingSAXException {
            if (E_TAGLIB_URI.equals(str3)) {
                this.taglibUriCData = this.cDataCollector.toString().trim();
                this.cDataCollector = null;
            } else if (E_TAGLIB_LOCATION.equals(str3)) {
                this.taglibLocationCData = this.cDataCollector.toString().trim();
                if (this.taglibLocationCData.length() != 0) {
                    try {
                        if (TaglibFactory.getUriType(this.taglibLocationCData) == 2) {
                            StringBuffer stringBuffer = new StringBuffer();
                            stringBuffer.append("/WEB-INF/");
                            stringBuffer.append(this.taglibLocationCData);
                            this.taglibLocationCData = stringBuffer.toString();
                        }
                        this.cDataCollector = null;
                    } catch (MalformedURLException e) {
                        StringBuffer stringBuffer2 = new StringBuffer();
                        stringBuffer2.append("Failed to detect URI type for: ");
                        stringBuffer2.append(this.taglibLocationCData);
                        throw new TldParsingSAXException(stringBuffer2.toString(), this.locator, e);
                    }
                } else {
                    throw new TldParsingSAXException("Required \"taglib-uri\" element was missing or empty", this.locator);
                }
            } else if (E_TAGLIB.equals(str3)) {
                TaglibFactory.this.addTldLocation(TaglibFactory.isJarPath(this.taglibLocationCData) ? new ServletContextJarEntryTldLocation(this.taglibLocationCData, TaglibFactory.DEFAULT_TLD_RESOURCE_PATH) : new ServletContextTldLocation(this.taglibLocationCData), this.taglibUriCData);
            }
        }
    }

    private static class TldParserForTaglibUriExtraction extends DefaultHandler {
        private static final String E_URI = "uri";
        private StringBuffer cDataCollector;
        private String uri;

        TldParserForTaglibUriExtraction() {
        }

        /* access modifiers changed from: package-private */
        public String getTaglibUri() {
            return this.uri;
        }

        public void startElement(String str, String str2, String str3, Attributes attributes) {
            if (E_URI.equals(str3)) {
                this.cDataCollector = new StringBuffer();
            }
        }

        public void characters(char[] cArr, int i, int i2) {
            StringBuffer stringBuffer = this.cDataCollector;
            if (stringBuffer != null) {
                stringBuffer.append(cArr, i, i2);
            }
        }

        public void endElement(String str, String str2, String str3) {
            if (E_URI.equals(str3)) {
                this.uri = this.cDataCollector.toString().trim();
                this.cDataCollector = null;
            }
        }
    }

    static final class TldParserForTaglibBuilding extends DefaultHandler {
        private static final String E_FUNCTION = "function";
        private static final String E_FUNCTION_CLASS = "function-class";
        private static final String E_FUNCTION_SIGNATURE = "function-signature";
        private static final String E_LISTENER = "listener";
        private static final String E_LISTENER_CLASS = "listener-class";
        private static final String E_NAME = "name";
        private static final String E_TAG = "tag";
        private static final String E_TAG_CLASS = "tag-class";
        private static final String E_TAG_CLASS_LEGACY = "tagclass";
        private final BeansWrapper beansWrapper;
        private StringBuffer cDataCollector;
        private String functionClassCData;
        private String functionNameCData;
        private String functionSignatureCData;
        private String listenerClassCData;
        private final List listeners = new ArrayList();
        private Locator locator;
        private Stack stack = new Stack();
        private String tagClassCData;
        private String tagNameCData;
        private final Map tagsAndFunctions = new HashMap();

        TldParserForTaglibBuilding(ObjectWrapper objectWrapper) {
            String str;
            Class cls;
            if (objectWrapper instanceof BeansWrapper) {
                this.beansWrapper = (BeansWrapper) objectWrapper;
                return;
            }
            this.beansWrapper = null;
            if (TaglibFactory.LOG.isWarnEnabled()) {
                Logger access$1000 = TaglibFactory.LOG;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Custom EL functions won't be loaded because ");
                if (objectWrapper == null) {
                    str = "no ObjectWrapper was specified ";
                } else {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("the ObjectWrapper wasn't instance of ");
                    if (TaglibFactory.class$freemarker$ext$beans$BeansWrapper == null) {
                        cls = TaglibFactory.class$("freemarker.ext.beans.BeansWrapper");
                        TaglibFactory.class$freemarker$ext$beans$BeansWrapper = cls;
                    } else {
                        cls = TaglibFactory.class$freemarker$ext$beans$BeansWrapper;
                    }
                    stringBuffer2.append(cls.getName());
                    str = stringBuffer2.toString();
                }
                stringBuffer.append(str);
                stringBuffer.append(FileUtil.HIDDEN_PREFIX);
                access$1000.warn(stringBuffer.toString());
            }
        }

        /* access modifiers changed from: package-private */
        public Map getTagsAndFunctions() {
            return this.tagsAndFunctions;
        }

        /* access modifiers changed from: package-private */
        public List getListeners() {
            return this.listeners;
        }

        public void setDocumentLocator(Locator locator2) {
            this.locator = locator2;
        }

        public void startElement(String str, String str2, String str3, Attributes attributes) {
            this.stack.push(str3);
            if (this.stack.size() != 3) {
                return;
            }
            if ("name".equals(str3) || E_TAG_CLASS_LEGACY.equals(str3) || E_TAG_CLASS.equals(str3) || E_LISTENER_CLASS.equals(str3) || E_FUNCTION_CLASS.equals(str3) || E_FUNCTION_SIGNATURE.equals(str3)) {
                this.cDataCollector = new StringBuffer();
            }
        }

        public void characters(char[] cArr, int i, int i2) {
            StringBuffer stringBuffer = this.cDataCollector;
            if (stringBuffer != null) {
                stringBuffer.append(cArr, i, i2);
            }
        }

        public void endElement(String str, String str2, String str3) throws TldParsingSAXException {
            Class cls;
            Object obj;
            if (this.stack.peek().equals(str3)) {
                if (this.stack.size() == 3) {
                    if ("name".equals(str3)) {
                        if (E_TAG.equals(this.stack.get(1))) {
                            this.tagNameCData = pullCData();
                        } else if (E_FUNCTION.equals(this.stack.get(1))) {
                            this.functionNameCData = pullCData();
                        }
                    } else if (E_TAG_CLASS_LEGACY.equals(str3) || E_TAG_CLASS.equals(str3)) {
                        this.tagClassCData = pullCData();
                    } else if (E_LISTENER_CLASS.equals(str3)) {
                        this.listenerClassCData = pullCData();
                    } else if (E_FUNCTION_CLASS.equals(str3)) {
                        this.functionClassCData = pullCData();
                    } else if (E_FUNCTION_SIGNATURE.equals(str3)) {
                        this.functionSignatureCData = pullCData();
                    }
                } else if (this.stack.size() == 2) {
                    if (E_TAG.equals(str3)) {
                        checkChildElementNotNull(str3, "name", this.tagNameCData);
                        checkChildElementNotNull(str3, E_TAG_CLASS, this.tagClassCData);
                        Class resoveClassFromTLD = resoveClassFromTLD(this.tagClassCData, "custom tag", this.tagNameCData);
                        try {
                            if (TaglibFactory.class$javax$servlet$jsp$tagext$Tag == null) {
                                cls = TaglibFactory.class$("javax.servlet.jsp.tagext.Tag");
                                TaglibFactory.class$javax$servlet$jsp$tagext$Tag = cls;
                            } else {
                                cls = TaglibFactory.class$javax$servlet$jsp$tagext$Tag;
                            }
                            if (cls.isAssignableFrom(resoveClassFromTLD)) {
                                obj = new TagTransformModel(this.tagNameCData, resoveClassFromTLD);
                            } else {
                                obj = new SimpleTagDirectiveModel(this.tagNameCData, resoveClassFromTLD);
                            }
                            this.tagsAndFunctions.put(this.tagNameCData, obj);
                            this.tagNameCData = null;
                            this.tagClassCData = null;
                        } catch (IntrospectionException e) {
                            StringBuffer stringBuffer = new StringBuffer();
                            stringBuffer.append("JavaBean introspection failed on custom tag class ");
                            stringBuffer.append(this.tagClassCData);
                            throw new TldParsingSAXException(stringBuffer.toString(), this.locator, e);
                        }
                    } else if (E_FUNCTION.equals(str3) && this.beansWrapper != null) {
                        checkChildElementNotNull(str3, E_FUNCTION_CLASS, this.functionClassCData);
                        checkChildElementNotNull(str3, E_FUNCTION_SIGNATURE, this.functionSignatureCData);
                        checkChildElementNotNull(str3, "name", this.functionNameCData);
                        Class resoveClassFromTLD2 = resoveClassFromTLD(this.functionClassCData, "custom EL function", this.functionNameCData);
                        try {
                            Method methodByFunctionSignature = TaglibMethodUtil.getMethodByFunctionSignature(resoveClassFromTLD2, this.functionSignatureCData);
                            int modifiers = methodByFunctionSignature.getModifiers();
                            if (!Modifier.isPublic(modifiers) || !Modifier.isStatic(modifiers)) {
                                StringBuffer stringBuffer2 = new StringBuffer();
                                stringBuffer2.append("The custom EL function method must be public and static: ");
                                stringBuffer2.append(methodByFunctionSignature);
                                throw new TldParsingSAXException(stringBuffer2.toString(), this.locator);
                            }
                            try {
                                this.tagsAndFunctions.put(this.functionNameCData, this.beansWrapper.wrap(null, methodByFunctionSignature));
                                this.functionNameCData = null;
                                this.functionClassCData = null;
                                this.functionSignatureCData = null;
                            } catch (Exception unused) {
                                StringBuffer stringBuffer3 = new StringBuffer();
                                stringBuffer3.append("FreeMarker object wrapping failed on method : ");
                                stringBuffer3.append(methodByFunctionSignature);
                                throw new TldParsingSAXException(stringBuffer3.toString(), this.locator);
                            }
                        } catch (Exception e2) {
                            StringBuffer stringBuffer4 = new StringBuffer();
                            stringBuffer4.append("Error while trying to resolve signature ");
                            stringBuffer4.append(StringUtil.jQuote(this.functionSignatureCData));
                            stringBuffer4.append(" on class ");
                            stringBuffer4.append(StringUtil.jQuote(resoveClassFromTLD2.getName()));
                            stringBuffer4.append(" for custom EL function ");
                            stringBuffer4.append(StringUtil.jQuote(this.functionNameCData));
                            stringBuffer4.append(FileUtil.HIDDEN_PREFIX);
                            throw new TldParsingSAXException(stringBuffer4.toString(), this.locator, e2);
                        }
                    } else if ("listener".equals(str3)) {
                        checkChildElementNotNull(str3, E_LISTENER_CLASS, this.listenerClassCData);
                        try {
                            this.listeners.add(resoveClassFromTLD(this.listenerClassCData, "listener", null).newInstance());
                            this.listenerClassCData = null;
                        } catch (Exception e3) {
                            StringBuffer stringBuffer5 = new StringBuffer();
                            stringBuffer5.append("Failed to create new instantiate from listener class ");
                            stringBuffer5.append(this.listenerClassCData);
                            throw new TldParsingSAXException(stringBuffer5.toString(), this.locator, e3);
                        }
                    }
                }
                this.stack.pop();
                return;
            }
            StringBuffer stringBuffer6 = new StringBuffer();
            stringBuffer6.append("Unbalanced tag nesting at \"");
            stringBuffer6.append(str3);
            stringBuffer6.append("\" end-tag.");
            throw new TldParsingSAXException(stringBuffer6.toString(), this.locator);
        }

        private String pullCData() {
            String trim = this.cDataCollector.toString().trim();
            this.cDataCollector = null;
            return trim;
        }

        private void checkChildElementNotNull(String str, String str2, String str3) throws TldParsingSAXException {
            if (str3 == null) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Missing required \"");
                stringBuffer.append(str2);
                stringBuffer.append("\" element inside the \"");
                stringBuffer.append(str);
                stringBuffer.append("\" element.");
                throw new TldParsingSAXException(stringBuffer.toString(), this.locator);
            }
        }

        private Class resoveClassFromTLD(String str, String str2, String str3) throws TldParsingSAXException {
            try {
                return ClassUtil.forName(str);
            } catch (LinkageError e) {
                throw newTLDEntryClassLoadingException(e, str, str2, str3);
            } catch (ClassNotFoundException e2) {
                throw newTLDEntryClassLoadingException(e2, str, str2, str3);
            }
        }

        private TldParsingSAXException newTLDEntryClassLoadingException(Throwable th, String str, String str2, String str3) throws TldParsingSAXException {
            String str4;
            int i;
            int lastIndexOf = str.lastIndexOf(46);
            if (lastIndexOf != -1) {
                lastIndexOf = str.lastIndexOf(46, lastIndexOf - 1);
            }
            boolean z = true;
            if (lastIndexOf == -1 || str.length() <= (i = lastIndexOf + 1) || !Character.isUpperCase(str.charAt(i))) {
                z = false;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(th instanceof ClassNotFoundException ? "Not found class " : "Can't load class ");
            stringBuffer.append(StringUtil.jQuote(str));
            stringBuffer.append(" for ");
            stringBuffer.append(str2);
            String str5 = "";
            if (str3 != null) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append(" ");
                stringBuffer2.append(StringUtil.jQuote(str3));
                str4 = stringBuffer2.toString();
            } else {
                str4 = str5;
            }
            stringBuffer.append(str4);
            stringBuffer.append(FileUtil.HIDDEN_PREFIX);
            if (z) {
                str5 = " Hint: Before nested classes, use \"$\", not \".\".";
            }
            stringBuffer.append(str5);
            return new TldParsingSAXException(stringBuffer.toString(), this.locator, th);
        }
    }

    private static final class LocalDtdEntityResolver implements EntityResolver {
        private static final Map DTDS = new HashMap();

        private LocalDtdEntityResolver() {
        }

        static {
            DTDS.put("-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.2//EN", "web-jsptaglibrary_1_2.dtd");
            DTDS.put("http://java.sun.com/dtd/web-jsptaglibrary_1_2.dtd", "web-jsptaglibrary_1_2.dtd");
            DTDS.put("-//Sun Microsystems, Inc.//DTD JSP Tag Library 1.1//EN", "web-jsptaglibrary_1_1.dtd");
            DTDS.put("http://java.sun.com/j2ee/dtds/web-jsptaglibrary_1_1.dtd", "web-jsptaglibrary_1_1.dtd");
            DTDS.put("-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN", "web-app_2_3.dtd");
            DTDS.put("http://java.sun.com/dtd/web-app_2_3.dtd", "web-app_2_3.dtd");
            DTDS.put("-//Sun Microsystems, Inc.//DTD Web Application 2.2//EN", "web-app_2_2.dtd");
            DTDS.put("http://java.sun.com/j2ee/dtds/web-app_2_2.dtd", "web-app_2_2.dtd");
        }

        public InputSource resolveEntity(String str, String str2) {
            InputStream inputStream;
            String str3 = (String) DTDS.get(str);
            if (str3 == null) {
                str3 = (String) DTDS.get(str2);
            }
            if (str3 != null) {
                inputStream = getClass().getResourceAsStream(str3);
            } else {
                inputStream = new ByteArrayInputStream(new byte[0]);
            }
            InputSource inputSource = new InputSource(inputStream);
            inputSource.setPublicId(str);
            inputSource.setSystemId(str2);
            return inputSource;
        }
    }

    private static class TldParsingSAXException extends SAXParseException {
        private final Throwable cause;

        TldParsingSAXException(String str, Locator locator) {
            this(str, locator, null);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        TldParsingSAXException(String str, Locator locator, Throwable th) {
            super(str, locator, th instanceof Exception ? (Exception) th : new Exception("Unchecked exception; see cause", th));
            this.cause = th;
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer(getClass().getName());
            stringBuffer.append(": ");
            int length = stringBuffer.length();
            String systemId = getSystemId();
            String publicId = getPublicId();
            if (!(systemId == null && publicId == null)) {
                stringBuffer.append("In ");
                if (systemId != null) {
                    stringBuffer.append(systemId);
                }
                if (publicId != null) {
                    if (systemId != null) {
                        stringBuffer.append(" (public ID: ");
                    }
                    stringBuffer.append(publicId);
                    if (systemId != null) {
                        stringBuffer.append(')');
                    }
                }
            }
            int lineNumber = getLineNumber();
            if (lineNumber != -1) {
                stringBuffer.append(stringBuffer.length() != length ? ", at " : "At ");
                stringBuffer.append("line ");
                stringBuffer.append(lineNumber);
                int columnNumber = getColumnNumber();
                if (columnNumber != -1) {
                    stringBuffer.append(", column ");
                    stringBuffer.append(columnNumber);
                }
            }
            String localizedMessage = getLocalizedMessage();
            if (localizedMessage != null) {
                if (stringBuffer.length() != length) {
                    stringBuffer.append(":\n");
                }
                stringBuffer.append(localizedMessage);
            }
            return stringBuffer.toString();
        }

        public Throwable getCause() {
            Throwable cause2 = super.getCause();
            return cause2 == null ? this.cause : cause2;
        }
    }

    private static class URLWithExternalForm implements Comparable {
        /* access modifiers changed from: private */
        public final String externalForm;
        private final URL url;

        public URLWithExternalForm(URL url2) {
            this.url = url2;
            this.externalForm = url2.toExternalForm();
        }

        public URL getUrl() {
            return this.url;
        }

        public String getExternalForm() {
            return this.externalForm;
        }

        public int hashCode() {
            return this.externalForm.hashCode();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj != null && getClass() == obj.getClass()) {
                return !this.externalForm.equals(((URLWithExternalForm) obj).externalForm);
            }
            return false;
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("URLWithExternalForm(");
            stringBuffer.append(this.externalForm);
            stringBuffer.append(")");
            return stringBuffer.toString();
        }

        public int compareTo(Object obj) {
            return getExternalForm().compareTo(((URLWithExternalForm) obj).getExternalForm());
        }
    }

    private static class TaglibGettingException extends Exception {
        public TaglibGettingException(String str, Throwable th) {
            super(str, th);
        }

        public TaglibGettingException(String str) {
            super(str);
        }
    }
}
