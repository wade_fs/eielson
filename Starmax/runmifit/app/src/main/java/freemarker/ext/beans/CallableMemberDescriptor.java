package freemarker.ext.beans;

import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.lang.reflect.InvocationTargetException;

abstract class CallableMemberDescriptor extends MaybeEmptyCallableMemberDescriptor {
    /* access modifiers changed from: package-private */
    public abstract String getDeclaration();

    /* access modifiers changed from: package-private */
    public abstract String getName();

    /* access modifiers changed from: package-private */
    public abstract Class[] getParamTypes();

    /* access modifiers changed from: package-private */
    public abstract Object invokeConstructor(BeansWrapper beansWrapper, Object[] objArr) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, TemplateModelException;

    /* access modifiers changed from: package-private */
    public abstract TemplateModel invokeMethod(BeansWrapper beansWrapper, Object obj, Object[] objArr) throws TemplateModelException, InvocationTargetException, IllegalAccessException;

    /* access modifiers changed from: package-private */
    public abstract boolean isConstructor();

    /* access modifiers changed from: package-private */
    public abstract boolean isStatic();

    /* access modifiers changed from: package-private */
    public abstract boolean isVarargs();

    CallableMemberDescriptor() {
    }
}
