package freemarker.ext.beans;

import freemarker.core._DelayedOrdinal;

final class EmptyMemberAndArguments extends MaybeEmptyMemberAndArguments {
    static final EmptyMemberAndArguments WRONG_NUMBER_OF_ARGUMENTS = new EmptyMemberAndArguments("No compatible overloaded variation was found; wrong number of arguments.", true, null);
    private final Object errorDescription;
    private final boolean numberOfArgumentsWrong;
    private final Object[] unwrappedArguments;

    private EmptyMemberAndArguments(Object obj, boolean z, Object[] objArr) {
        this.errorDescription = obj;
        this.numberOfArgumentsWrong = z;
        this.unwrappedArguments = objArr;
    }

    static EmptyMemberAndArguments noCompatibleOverload(int i) {
        return new EmptyMemberAndArguments(new Object[]{"No compatible overloaded variation was found; can't convert (unwrap) the ", new _DelayedOrdinal(new Integer(i)), " argument to the desired Java type."}, false, null);
    }

    static EmptyMemberAndArguments noCompatibleOverload(Object[] objArr) {
        return new EmptyMemberAndArguments("No compatible overloaded variation was found; declared parameter types and argument value types mismatch.", false, objArr);
    }

    static EmptyMemberAndArguments ambiguous(Object[] objArr) {
        return new EmptyMemberAndArguments("Multiple compatible overloaded variations were found with the same priority.", false, objArr);
    }

    static MaybeEmptyMemberAndArguments from(EmptyCallableMemberDescriptor emptyCallableMemberDescriptor, Object[] objArr) {
        if (emptyCallableMemberDescriptor == EmptyCallableMemberDescriptor.NO_SUCH_METHOD) {
            return noCompatibleOverload(objArr);
        }
        if (emptyCallableMemberDescriptor == EmptyCallableMemberDescriptor.AMBIGUOUS_METHOD) {
            return ambiguous(objArr);
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Unrecognized constant: ");
        stringBuffer.append(emptyCallableMemberDescriptor);
        throw new IllegalArgumentException(stringBuffer.toString());
    }

    /* access modifiers changed from: package-private */
    public Object getErrorDescription() {
        return this.errorDescription;
    }

    /* access modifiers changed from: package-private */
    public Object[] getUnwrappedArguments() {
        return this.unwrappedArguments;
    }

    public boolean isNumberOfArgumentsWrong() {
        return this.numberOfArgumentsWrong;
    }
}
