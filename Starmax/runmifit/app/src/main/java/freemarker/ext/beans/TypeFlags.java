package freemarker.ext.beans;

class TypeFlags {
    static final int ACCEPTS_ANY_OBJECT = 522240;
    static final int ACCEPTS_ARRAY = 262144;
    static final int ACCEPTS_BOOLEAN = 16384;
    static final int ACCEPTS_DATE = 4096;
    static final int ACCEPTS_LIST = 65536;
    static final int ACCEPTS_MAP = 32768;
    static final int ACCEPTS_NUMBER = 2048;
    static final int ACCEPTS_SET = 131072;
    static final int ACCEPTS_STRING = 8192;
    static final int BIG_DECIMAL = 512;
    static final int BIG_INTEGER = 256;
    static final int BYTE = 4;
    static final int CHARACTER = 524288;
    static final int DOUBLE = 128;
    static final int FLOAT = 64;
    static final int INTEGER = 16;
    static final int LONG = 32;
    static final int MASK_ALL_KNOWN_NUMERICALS = 1020;
    static final int MASK_ALL_NUMERICALS = 2044;
    static final int MASK_KNOWN_INTEGERS = 316;
    static final int MASK_KNOWN_NONINTEGERS = 704;
    static final int SHORT = 8;
    static final int UNKNOWN_NUMERICAL_TYPE = 1024;
    static final int WIDENED_NUMERICAL_UNWRAPPING_HINT = 1;
    static /* synthetic */ Class class$java$lang$Boolean;
    static /* synthetic */ Class class$java$lang$Byte;
    static /* synthetic */ Class class$java$lang$Character;
    static /* synthetic */ Class class$java$lang$Double;
    static /* synthetic */ Class class$java$lang$Float;
    static /* synthetic */ Class class$java$lang$Integer;
    static /* synthetic */ Class class$java$lang$Long;
    static /* synthetic */ Class class$java$lang$Number;
    static /* synthetic */ Class class$java$lang$Object;
    static /* synthetic */ Class class$java$lang$Short;
    static /* synthetic */ Class class$java$lang$String;
    static /* synthetic */ Class class$java$math$BigDecimal;
    static /* synthetic */ Class class$java$math$BigInteger;
    static /* synthetic */ Class class$java$util$Date;
    static /* synthetic */ Class class$java$util$List;
    static /* synthetic */ Class class$java$util$Map;
    static /* synthetic */ Class class$java$util$Set;

    TypeFlags() {
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    static int classToTypeFlags(Class cls) {
        Class cls2 = class$java$lang$Object;
        if (cls2 == null) {
            cls2 = class$("java.lang.Object");
            class$java$lang$Object = cls2;
        }
        if (cls == cls2) {
            return ACCEPTS_ANY_OBJECT;
        }
        Class cls3 = class$java$lang$String;
        if (cls3 == null) {
            cls3 = class$("java.lang.String");
            class$java$lang$String = cls3;
        }
        int i = 8192;
        if (cls == cls3) {
            return 8192;
        }
        if (!cls.isPrimitive()) {
            Class cls4 = class$java$lang$Number;
            if (cls4 == null) {
                cls4 = class$("java.lang.Number");
                class$java$lang$Number = cls4;
            }
            if (cls4.isAssignableFrom(cls)) {
                Class cls5 = class$java$lang$Integer;
                if (cls5 == null) {
                    cls5 = class$("java.lang.Integer");
                    class$java$lang$Integer = cls5;
                }
                if (cls == cls5) {
                    return 2064;
                }
                Class cls6 = class$java$lang$Long;
                if (cls6 == null) {
                    cls6 = class$("java.lang.Long");
                    class$java$lang$Long = cls6;
                }
                if (cls == cls6) {
                    return 2080;
                }
                Class cls7 = class$java$lang$Double;
                if (cls7 == null) {
                    cls7 = class$("java.lang.Double");
                    class$java$lang$Double = cls7;
                }
                if (cls == cls7) {
                    return 2176;
                }
                Class cls8 = class$java$lang$Float;
                if (cls8 == null) {
                    cls8 = class$("java.lang.Float");
                    class$java$lang$Float = cls8;
                }
                if (cls == cls8) {
                    return 2112;
                }
                Class cls9 = class$java$lang$Byte;
                if (cls9 == null) {
                    cls9 = class$("java.lang.Byte");
                    class$java$lang$Byte = cls9;
                }
                if (cls == cls9) {
                    return 2052;
                }
                Class cls10 = class$java$lang$Short;
                if (cls10 == null) {
                    cls10 = class$("java.lang.Short");
                    class$java$lang$Short = cls10;
                }
                if (cls == cls10) {
                    return 2056;
                }
                Class cls11 = class$java$math$BigDecimal;
                if (cls11 == null) {
                    cls11 = class$("java.math.BigDecimal");
                    class$java$math$BigDecimal = cls11;
                }
                if (cls11.isAssignableFrom(cls)) {
                    return 2560;
                }
                Class cls12 = class$java$math$BigInteger;
                if (cls12 == null) {
                    cls12 = class$("java.math.BigInteger");
                    class$java$math$BigInteger = cls12;
                }
                return cls12.isAssignableFrom(cls) ? 2304 : 3072;
            } else if (cls.isArray()) {
                return 262144;
            } else {
                Class cls13 = class$java$lang$String;
                if (cls13 == null) {
                    cls13 = class$("java.lang.String");
                    class$java$lang$String = cls13;
                }
                if (!cls.isAssignableFrom(cls13)) {
                    i = 0;
                }
                Class cls14 = class$java$util$Date;
                if (cls14 == null) {
                    cls14 = class$("java.util.Date");
                    class$java$util$Date = cls14;
                }
                if (cls.isAssignableFrom(cls14)) {
                    i |= 4096;
                }
                Class cls15 = class$java$lang$Boolean;
                if (cls15 == null) {
                    cls15 = class$("java.lang.Boolean");
                    class$java$lang$Boolean = cls15;
                }
                if (cls.isAssignableFrom(cls15)) {
                    i |= 16384;
                }
                Class cls16 = class$java$util$Map;
                if (cls16 == null) {
                    cls16 = class$("java.util.Map");
                    class$java$util$Map = cls16;
                }
                if (cls.isAssignableFrom(cls16)) {
                    i |= 32768;
                }
                Class cls17 = class$java$util$List;
                if (cls17 == null) {
                    cls17 = class$("java.util.List");
                    class$java$util$List = cls17;
                }
                if (cls.isAssignableFrom(cls17)) {
                    i |= 65536;
                }
                Class cls18 = class$java$util$Set;
                if (cls18 == null) {
                    cls18 = class$("java.util.Set");
                    class$java$util$Set = cls18;
                }
                if (cls.isAssignableFrom(cls18)) {
                    i |= 131072;
                }
                Class cls19 = class$java$lang$Character;
                if (cls19 == null) {
                    cls19 = class$("java.lang.Character");
                    class$java$lang$Character = cls19;
                }
                return cls == cls19 ? i | 524288 : i;
            }
        } else if (cls == Integer.TYPE) {
            return 2064;
        } else {
            if (cls == Long.TYPE) {
                return 2080;
            }
            if (cls == Double.TYPE) {
                return 2176;
            }
            if (cls == Float.TYPE) {
                return 2112;
            }
            if (cls == Byte.TYPE) {
                return 2052;
            }
            if (cls == Short.TYPE) {
                return 2056;
            }
            if (cls == Character.TYPE) {
                return 524288;
            }
            if (cls == Boolean.TYPE) {
                return 16384;
            }
            return 0;
        }
    }
}
