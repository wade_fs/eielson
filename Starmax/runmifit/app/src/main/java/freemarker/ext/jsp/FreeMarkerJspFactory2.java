package freemarker.ext.jsp;

class FreeMarkerJspFactory2 extends FreeMarkerJspFactory {
    /* access modifiers changed from: protected */
    public String getSpecificationVersion() {
        return "2.0";
    }

    FreeMarkerJspFactory2() {
    }
}
