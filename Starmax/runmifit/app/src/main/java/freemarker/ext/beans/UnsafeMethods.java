package freemarker.ext.beans;

import freemarker.template.utility.ClassUtil;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

class UnsafeMethods {
    private static final Set UNSAFE_METHODS = createUnsafeMethodsSet();
    static /* synthetic */ Class class$freemarker$ext$beans$BeansWrapper;

    private UnsafeMethods() {
    }

    static boolean isUnsafeMethod(Method method) {
        return UNSAFE_METHODS.contains(method);
    }

    private static final Set createUnsafeMethodsSet() {
        Properties properties = new Properties();
        Class cls = class$freemarker$ext$beans$BeansWrapper;
        if (cls == null) {
            cls = class$("freemarker.ext.beans.BeansWrapper");
            class$freemarker$ext$beans$BeansWrapper = cls;
        }
        InputStream resourceAsStream = cls.getResourceAsStream("unsafeMethods.txt");
        if (resourceAsStream == null) {
            return Collections.EMPTY_SET;
        }
        String str = null;
        try {
            properties.load(resourceAsStream);
            resourceAsStream.close();
            HashSet hashSet = new HashSet((properties.size() * 4) / 3, 1.0f);
            Map createPrimitiveClassesMap = createPrimitiveClassesMap();
            for (String str2 : properties.keySet()) {
                try {
                    hashSet.add(parseMethodSpec(str2, createPrimitiveClassesMap));
                } catch (ClassNotFoundException e) {
                    if (ClassIntrospector.DEVELOPMENT_MODE) {
                        throw e;
                    }
                } catch (NoSuchMethodException e2) {
                    if (ClassIntrospector.DEVELOPMENT_MODE) {
                        throw e2;
                    }
                } catch (Exception e3) {
                    e = e3;
                    str = str2;
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Could not load unsafe method ");
                    stringBuffer.append(str);
                    stringBuffer.append(" ");
                    stringBuffer.append(e.getClass().getName());
                    stringBuffer.append(" ");
                    stringBuffer.append(e.getMessage());
                    throw new RuntimeException(stringBuffer.toString());
                }
                String str3 = str2;
            }
            return hashSet;
        } catch (Exception e4) {
            e = e4;
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Could not load unsafe method ");
            stringBuffer2.append(str);
            stringBuffer2.append(" ");
            stringBuffer2.append(e.getClass().getName());
            stringBuffer2.append(" ");
            stringBuffer2.append(e.getMessage());
            throw new RuntimeException(stringBuffer2.toString());
        } catch (Throwable th) {
            resourceAsStream.close();
            throw th;
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private static Method parseMethodSpec(String str, Map map) throws ClassNotFoundException, NoSuchMethodException {
        int indexOf = str.indexOf(40);
        int lastIndexOf = str.lastIndexOf(46, indexOf);
        Class forName = ClassUtil.forName(str.substring(0, lastIndexOf));
        String substring = str.substring(lastIndexOf + 1, indexOf);
        StringTokenizer stringTokenizer = new StringTokenizer(str.substring(indexOf + 1, str.length() - 1), ",");
        int countTokens = stringTokenizer.countTokens();
        Class[] clsArr = new Class[countTokens];
        for (int i = 0; i < countTokens; i++) {
            String nextToken = stringTokenizer.nextToken();
            clsArr[i] = (Class) map.get(nextToken);
            if (clsArr[i] == null) {
                clsArr[i] = ClassUtil.forName(nextToken);
            }
        }
        return forName.getMethod(substring, clsArr);
    }

    private static Map createPrimitiveClassesMap() {
        HashMap hashMap = new HashMap();
        hashMap.put("boolean", Boolean.TYPE);
        hashMap.put("byte", Byte.TYPE);
        hashMap.put("char", Character.TYPE);
        hashMap.put("short", Short.TYPE);
        hashMap.put("int", Integer.TYPE);
        hashMap.put("long", Long.TYPE);
        hashMap.put("float", Float.TYPE);
        hashMap.put("double", Double.TYPE);
        return hashMap;
    }
}
