package freemarker.ext.beans;

import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.lang.reflect.InvocationTargetException;

class MemberAndArguments extends MaybeEmptyMemberAndArguments {
    private final Object[] args;
    private final CallableMemberDescriptor callableMemberDesc;

    MemberAndArguments(CallableMemberDescriptor callableMemberDescriptor, Object[] objArr) {
        this.callableMemberDesc = callableMemberDescriptor;
        this.args = objArr;
    }

    /* access modifiers changed from: package-private */
    public Object[] getArgs() {
        return this.args;
    }

    /* access modifiers changed from: package-private */
    public TemplateModel invokeMethod(BeansWrapper beansWrapper, Object obj) throws TemplateModelException, InvocationTargetException, IllegalAccessException {
        return this.callableMemberDesc.invokeMethod(beansWrapper, obj, this.args);
    }

    /* access modifiers changed from: package-private */
    public Object invokeConstructor(BeansWrapper beansWrapper) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, TemplateModelException {
        return this.callableMemberDesc.invokeConstructor(beansWrapper, this.args);
    }

    /* access modifiers changed from: package-private */
    public CallableMemberDescriptor getCallableMemberDescriptor() {
        return this.callableMemberDesc;
    }
}
