package freemarker.log;

interface LoggerFactory {
    Logger getLogger(String str);
}
