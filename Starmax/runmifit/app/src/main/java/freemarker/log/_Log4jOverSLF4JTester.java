package freemarker.log;

import org.apache.log4j.MDC;

public class _Log4jOverSLF4JTester {
    private static final String MDC_KEY;
    static /* synthetic */ Class class$freemarker$log$_Log4jOverSLF4JTester;

    static {
        Class cls = class$freemarker$log$_Log4jOverSLF4JTester;
        if (cls == null) {
            cls = class$("freemarker.log._Log4jOverSLF4JTester");
            class$freemarker$log$_Log4jOverSLF4JTester = cls;
        }
        MDC_KEY = cls.getName();
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static final boolean test() {
        MDC.put(MDC_KEY, "");
        try {
            return org.slf4j.MDC.get(MDC_KEY) != null;
        } finally {
            MDC.remove(MDC_KEY);
        }
    }
}
