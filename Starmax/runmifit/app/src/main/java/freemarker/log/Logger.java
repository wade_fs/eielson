package freemarker.log;

import java.io.PrintStream;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.HashMap;
import java.util.Map;

public abstract class Logger {
    private static final String[] LIBRARIES_BY_PRIORITY = {null, LIBRARY_NAME_JUL, "org.apache.log.Logger", LIBRARY_NAME_AVALON, "org.apache.log4j.Logger", LIBRARY_NAME_LOG4J, "org.apache.commons.logging.Log", LIBRARY_NAME_COMMONS_LOGGING, "org.slf4j.Logger", LIBRARY_NAME_SLF4J};
    public static final int LIBRARY_AUTO = -1;
    public static final int LIBRARY_AVALON = 2;
    public static final int LIBRARY_COMMONS = 4;
    public static final int LIBRARY_JAVA = 1;
    public static final int LIBRARY_LOG4J = 3;
    public static final String LIBRARY_NAME_AUTO = "auto";
    public static final String LIBRARY_NAME_AVALON = "Avalon";
    public static final String LIBRARY_NAME_COMMONS_LOGGING = "CommonsLogging";
    public static final String LIBRARY_NAME_JUL = "JUL";
    public static final String LIBRARY_NAME_LOG4J = "Log4j";
    public static final String LIBRARY_NAME_NONE = "none";
    public static final String LIBRARY_NAME_SLF4J = "SLF4J";
    public static final int LIBRARY_NONE = 0;
    public static final int LIBRARY_SLF4J = 5;
    private static final String LOG4J_OVER_SLF4J_TESTER_CLASS = "freemarker.log._Log4jOverSLF4JTester";
    private static final int MAX_LIBRARY_ENUM = 5;
    private static final int MIN_LIBRARY_ENUM = -1;
    private static final String REAL_LOG4J_PRESENCE_CLASS = "org.apache.log4j.FileAppender";
    public static final String SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY = "org.freemarker.loggerLibrary";
    private static String categoryPrefix = "";
    static /* synthetic */ Class class$freemarker$log$Logger;
    static /* synthetic */ Class class$freemarker$log$LoggerFactory;
    private static boolean initializedFromSystemProperty;
    private static int libraryEnum;
    private static LoggerFactory loggerFactory;
    private static final Map loggersByCategory = new HashMap();

    private static boolean isAutoDetected(int i) {
        return (i == -1 || i == 0 || i == 5 || i == 4) ? false : true;
    }

    public abstract void debug(String str);

    public abstract void debug(String str, Throwable th);

    public abstract void error(String str);

    public abstract void error(String str, Throwable th);

    public abstract void info(String str);

    public abstract void info(String str, Throwable th);

    public abstract boolean isDebugEnabled();

    public abstract boolean isErrorEnabled();

    public abstract boolean isFatalEnabled();

    public abstract boolean isInfoEnabled();

    public abstract boolean isWarnEnabled();

    public abstract void warn(String str);

    public abstract void warn(String str, Throwable th);

    static {
        if (LIBRARIES_BY_PRIORITY.length / 2 == 5) {
            return;
        }
        throw new AssertionError();
    }

    private static String getAvailabilityCheckClassName(int i) {
        if (i == -1 || i == 0) {
            return null;
        }
        return LIBRARIES_BY_PRIORITY[(i - 1) * 2];
    }

    private static String getLibraryName(int i) {
        if (i == -1) {
            return LIBRARY_NAME_AUTO;
        }
        return i == 0 ? "none" : LIBRARIES_BY_PRIORITY[((i - 1) * 2) + 1];
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static void selectLoggerLibrary(int i) throws ClassNotFoundException {
        Class cls;
        if (i < -1 || i > 5) {
            throw new IllegalArgumentException("Library enum value out of range");
        }
        Class cls2 = class$freemarker$log$Logger;
        if (cls2 == null) {
            cls2 = class$("freemarker.log.Logger");
            class$freemarker$log$Logger = cls2;
        }
        synchronized (cls2) {
            boolean z = loggerFactory != null;
            if (!z || i != libraryEnum) {
                ensureLoggerFactorySet(true);
                if (initializedFromSystemProperty) {
                    if (loggerFactory != null) {
                        if (i != libraryEnum) {
                            StringBuffer stringBuffer = new StringBuffer();
                            stringBuffer.append("Ignored ");
                            if (class$freemarker$log$Logger == null) {
                                cls = class$("freemarker.log.Logger");
                                class$freemarker$log$Logger = cls;
                            } else {
                                cls = class$freemarker$log$Logger;
                            }
                            stringBuffer.append(cls.getName());
                            stringBuffer.append(".selectLoggerLibrary(\"");
                            stringBuffer.append(getLibraryName(i));
                            stringBuffer.append("\") call, because the \"");
                            stringBuffer.append(SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY);
                            stringBuffer.append("\" system property is set to \"");
                            stringBuffer.append(getLibraryName(libraryEnum));
                            stringBuffer.append("\".");
                            logWarnInLogger(stringBuffer.toString());
                        }
                    }
                }
                int i2 = libraryEnum;
                setLibrary(i);
                loggersByCategory.clear();
                if (z) {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("Logger library was already set earlier to \"");
                    stringBuffer2.append(getLibraryName(i2));
                    stringBuffer2.append("\"; ");
                    stringBuffer2.append("change to \"");
                    stringBuffer2.append(getLibraryName(i));
                    stringBuffer2.append("\" won't effect loggers created ");
                    stringBuffer2.append("earlier.");
                    logWarnInLogger(stringBuffer2.toString());
                }
            }
        }
    }

    public static void setCategoryPrefix(String str) {
        Class cls = class$freemarker$log$Logger;
        if (cls == null) {
            cls = class$("freemarker.log.Logger");
            class$freemarker$log$Logger = cls;
        }
        synchronized (cls) {
            if (str != null) {
                try {
                    categoryPrefix = str;
                } catch (Throwable th) {
                    throw th;
                }
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    public static Logger getLogger(String str) {
        Logger logger;
        if (categoryPrefix.length() != 0) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(categoryPrefix);
            stringBuffer.append(str);
            str = stringBuffer.toString();
        }
        synchronized (loggersByCategory) {
            logger = (Logger) loggersByCategory.get(str);
            if (logger == null) {
                ensureLoggerFactorySet(false);
                logger = loggerFactory.getLogger(str);
                loggersByCategory.put(str, logger);
            }
        }
        return logger;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00a9, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void ensureLoggerFactorySet(boolean r9) {
        /*
            freemarker.log.LoggerFactory r0 = freemarker.log.Logger.loggerFactory
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            java.lang.Class r0 = freemarker.log.Logger.class$freemarker$log$Logger
            if (r0 != 0) goto L_0x0011
            java.lang.String r0 = "freemarker.log.Logger"
            java.lang.Class r0 = class$(r0)
            freemarker.log.Logger.class$freemarker$log$Logger = r0
        L_0x0011:
            monitor-enter(r0)
            freemarker.log.LoggerFactory r1 = freemarker.log.Logger.loggerFactory     // Catch:{ all -> 0x00aa }
            if (r1 == 0) goto L_0x0018
            monitor-exit(r0)     // Catch:{ all -> 0x00aa }
            return
        L_0x0018:
            java.lang.String r1 = "org.freemarker.loggerLibrary"
            java.lang.String r1 = getSystemProperty(r1)     // Catch:{ all -> 0x00aa }
            r2 = 0
            r3 = -1
            r4 = 1
            if (r1 == 0) goto L_0x005f
            java.lang.String r1 = r1.trim()     // Catch:{ all -> 0x00aa }
            r5 = -1
            r6 = 0
        L_0x0029:
            java.lang.String r7 = getLibraryName(r5)     // Catch:{ all -> 0x00aa }
            boolean r7 = r1.equalsIgnoreCase(r7)     // Catch:{ all -> 0x00aa }
            if (r7 == 0) goto L_0x0035
            r6 = 1
            goto L_0x0037
        L_0x0035:
            int r5 = r5 + 1
        L_0x0037:
            r7 = 5
            if (r5 > r7) goto L_0x003c
            if (r6 == 0) goto L_0x0029
        L_0x003c:
            if (r6 != 0) goto L_0x005b
            java.lang.StringBuffer r7 = new java.lang.StringBuffer     // Catch:{ all -> 0x00aa }
            r7.<init>()     // Catch:{ all -> 0x00aa }
            java.lang.String r8 = "Ignored invalid \"org.freemarker.loggerLibrary\" system property value: \""
            r7.append(r8)     // Catch:{ all -> 0x00aa }
            r7.append(r1)     // Catch:{ all -> 0x00aa }
            java.lang.String r8 = "\""
            r7.append(r8)     // Catch:{ all -> 0x00aa }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00aa }
            logWarnInLogger(r7)     // Catch:{ all -> 0x00aa }
            if (r9 == 0) goto L_0x005b
            monitor-exit(r0)     // Catch:{ all -> 0x00aa }
            return
        L_0x005b:
            if (r6 == 0) goto L_0x0063
            r3 = r5
            goto L_0x0063
        L_0x005f:
            if (r9 == 0) goto L_0x0063
            monitor-exit(r0)     // Catch:{ all -> 0x00aa }
            return
        L_0x0063:
            setLibrary(r3)     // Catch:{ all -> 0x006b }
            if (r1 == 0) goto L_0x00a8
            freemarker.log.Logger.initializedFromSystemProperty = r4     // Catch:{ all -> 0x006b }
            goto L_0x00a8
        L_0x006b:
            r5 = move-exception
            if (r9 == 0) goto L_0x0072
            if (r1 != 0) goto L_0x0071
            goto L_0x0072
        L_0x0071:
            r4 = 0
        L_0x0072:
            java.lang.StringBuffer r9 = new java.lang.StringBuffer     // Catch:{ all -> 0x00aa }
            r9.<init>()     // Catch:{ all -> 0x00aa }
            java.lang.String r1 = "Couldn't set up logger for \""
            r9.append(r1)     // Catch:{ all -> 0x00aa }
            java.lang.String r1 = getLibraryName(r3)     // Catch:{ all -> 0x00aa }
            r9.append(r1)     // Catch:{ all -> 0x00aa }
            java.lang.String r1 = "\""
            r9.append(r1)     // Catch:{ all -> 0x00aa }
            if (r4 == 0) goto L_0x008d
            java.lang.String r1 = "; logging disabled"
            goto L_0x008f
        L_0x008d:
            java.lang.String r1 = "."
        L_0x008f:
            r9.append(r1)     // Catch:{ all -> 0x00aa }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x00aa }
            logErrorInLogger(r9, r5)     // Catch:{ all -> 0x00aa }
            if (r4 == 0) goto L_0x00a8
            setLibrary(r2)     // Catch:{ ClassNotFoundException -> 0x009f }
            goto L_0x00a8
        L_0x009f:
            r9 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x00aa }
            java.lang.String r2 = "Bug"
            r1.<init>(r2, r9)     // Catch:{ all -> 0x00aa }
            throw r1     // Catch:{ all -> 0x00aa }
        L_0x00a8:
            monitor-exit(r0)     // Catch:{ all -> 0x00aa }
            return
        L_0x00aa:
            r9 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00aa }
            goto L_0x00ae
        L_0x00ad:
            throw r9
        L_0x00ae:
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.log.Logger.ensureLoggerFactorySet(boolean):void");
    }

    private static LoggerFactory createLoggerFactory(int i) throws ClassNotFoundException {
        if (i != -1) {
            return createLoggerFactoryForNonAuto(i);
        }
        int i2 = 5;
        while (i2 >= -1) {
            if (isAutoDetected(i2)) {
                if (i2 == 3 && hasLog4LibraryThatDelegatesToWorkingSLF4J()) {
                    i2 = 5;
                }
                try {
                    return createLoggerFactoryForNonAuto(i2);
                } catch (ClassNotFoundException unused) {
                } catch (Throwable th) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("Unexpected error when initializing logging for \"");
                    stringBuffer.append(getLibraryName(i2));
                    stringBuffer.append("\".");
                    logErrorInLogger(stringBuffer.toString(), th);
                }
            }
            i2--;
        }
        logWarnInLogger("Auto detecton couldn't set up any logger libraries; FreeMarker logging suppressed.");
        return new _NullLoggerFactory();
    }

    private static LoggerFactory createLoggerFactoryForNonAuto(int i) throws ClassNotFoundException {
        String availabilityCheckClassName = getAvailabilityCheckClassName(i);
        if (availabilityCheckClassName != null) {
            Class.forName(availabilityCheckClassName);
            String libraryName = getLibraryName(i);
            try {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("freemarker.log._");
                stringBuffer.append(libraryName);
                stringBuffer.append("LoggerFactory");
                return (LoggerFactory) Class.forName(stringBuffer.toString()).newInstance();
            } catch (Exception e) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("Unexpected error when creating logger factory for \"");
                stringBuffer2.append(libraryName);
                stringBuffer2.append("\".");
                throw new RuntimeException(stringBuffer2.toString(), e);
            }
        } else if (i == 1) {
            return new _JULLoggerFactory();
        } else {
            if (i == 0) {
                return new _NullLoggerFactory();
            }
            throw new RuntimeException("Bug");
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean hasLog4LibraryThatDelegatesToWorkingSLF4J() {
        /*
            r0 = 3
            r1 = 0
            java.lang.String r0 = getAvailabilityCheckClassName(r0)     // Catch:{ all -> 0x0033 }
            java.lang.Class.forName(r0)     // Catch:{ all -> 0x0033 }
            r0 = 5
            java.lang.String r0 = getAvailabilityCheckClassName(r0)     // Catch:{ all -> 0x0033 }
            java.lang.Class.forName(r0)     // Catch:{ all -> 0x0033 }
            java.lang.String r0 = "org.apache.log4j.FileAppender"
            java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0017 }
            return r1
        L_0x0017:
            java.lang.String r0 = "freemarker.log._Log4jOverSLF4JTester"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{  }
            java.lang.String r2 = "test"
            java.lang.Class[] r3 = new java.lang.Class[r1]     // Catch:{  }
            java.lang.reflect.Method r0 = r0.getMethod(r2, r3)     // Catch:{  }
            r2 = 0
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{  }
            java.lang.Object r0 = r0.invoke(r2, r3)     // Catch:{  }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{  }
            boolean r0 = r0.booleanValue()     // Catch:{  }
            return r0
        L_0x0033:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: freemarker.log.Logger.hasLog4LibraryThatDelegatesToWorkingSLF4J():boolean");
    }

    private static synchronized void setLibrary(int i) throws ClassNotFoundException {
        synchronized (Logger.class) {
            loggerFactory = createLoggerFactory(i);
            libraryEnum = i;
        }
    }

    private static void logWarnInLogger(String str) {
        logInLogger(false, str, null);
    }

    private static void logErrorInLogger(String str, Throwable th) {
        logInLogger(true, str, th);
    }

    private static void logInLogger(boolean z, String str, Throwable th) {
        boolean z2;
        Class cls = class$freemarker$log$Logger;
        if (cls == null) {
            cls = class$("freemarker.log.Logger");
            class$freemarker$log$Logger = cls;
        }
        synchronized (cls) {
            z2 = loggerFactory != null && !(loggerFactory instanceof _NullLoggerFactory);
        }
        if (z2) {
            try {
                Logger logger = getLogger("freemarker.logger");
                if (z) {
                    logger.error(str);
                } else {
                    logger.warn(str);
                }
            } catch (Throwable unused) {
                z2 = false;
            }
        }
        if (!z2) {
            PrintStream printStream = System.err;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(z ? "ERROR" : "WARN");
            stringBuffer.append(" ");
            Class cls2 = class$freemarker$log$LoggerFactory;
            if (cls2 == null) {
                cls2 = class$("freemarker.log.LoggerFactory");
                class$freemarker$log$LoggerFactory = cls2;
            }
            stringBuffer.append(cls2.getName());
            stringBuffer.append(": ");
            stringBuffer.append(str);
            printStream.println(stringBuffer.toString());
            if (th != null) {
                PrintStream printStream2 = System.err;
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("\tException: ");
                stringBuffer2.append(tryToString(th));
                printStream2.println(stringBuffer2.toString());
                while (th.getCause() != null) {
                    th = th.getCause();
                    PrintStream printStream3 = System.err;
                    StringBuffer stringBuffer3 = new StringBuffer();
                    stringBuffer3.append("\tCaused by: ");
                    stringBuffer3.append(tryToString(th));
                    printStream3.println(stringBuffer3.toString());
                }
            }
        }
    }

    private static String getSystemProperty(final String str) {
        try {
            return (String) AccessController.doPrivileged(new PrivilegedAction() {
                /* class freemarker.log.Logger.C33691 */

                public Object run() {
                    return System.getProperty(str, null);
                }
            });
        } catch (AccessControlException unused) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Insufficient permissions to read system property \"");
            stringBuffer.append(str);
            stringBuffer.append("\".");
            logWarnInLogger(stringBuffer.toString());
            return null;
        } catch (Throwable th) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("Failed to read system property \"");
            stringBuffer2.append(str);
            stringBuffer2.append("\".");
            logErrorInLogger(stringBuffer2.toString(), th);
            return null;
        }
    }

    private static String tryToString(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj.toString();
        } catch (Throwable unused) {
            return obj.getClass().getName();
        }
    }
}
