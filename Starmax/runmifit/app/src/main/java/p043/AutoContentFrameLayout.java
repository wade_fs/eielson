package p043;

import android.content.Context;
import android.support.v7.internal.widget.ContentFrameLayout;
import android.util.AttributeSet;
import com.zhy.autolayout.AutoFrameLayout;
import com.zhy.autolayout.utils.AutoLayoutHelper;

/* renamed from: AutoContentFrameLayout */
public class AutoContentFrameLayout extends ContentFrameLayout {
    private final AutoLayoutHelper mHelper = new AutoLayoutHelper(this);

    /* JADX WARN: Type inference failed for: r0v0, types: [AutoContentFrameLayout, android.view.ViewGroup] */
    public AutoContentFrameLayout(Context context) {
        super(context);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [AutoContentFrameLayout, android.view.ViewGroup] */
    public AutoContentFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [AutoContentFrameLayout, android.view.ViewGroup] */
    public AutoContentFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public AutoFrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new AutoFrameLayout.LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (!isInEditMode()) {
            this.mHelper.adjustChildren();
        }
        AutoContentFrameLayout.super.onMeasure(i, i2);
    }
}
