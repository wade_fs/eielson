package com.wade.adjustvolume;

import android.content.Context;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    AudioManager audioManager;
    private Text2Speech text2Speech;
    private TextView tvVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        tvVolume = findViewById(R.id.tvVolume);

        Button upButton = (Button) findViewById(R.id.upButton);
        upButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, 0);
                showVolume();
            }
        });

        Button downButton = (Button) findViewById(R.id.downButton);
        downButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, 0);
                showVolume();
            }
        });

        text2Speech = new Text2Speech(this);

        Button sayButton = (Button) findViewById(R.id.sayButton);
        sayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text2Speech.say("你好");
            }
        });
    }

    @Override public void onResume() {
        super.onResume();
        showVolume();
    }

    private void showVolume() {
        tvVolume.setText(
                "Music:"+ audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)+
                ",Alarm:"+ audioManager.getStreamVolume(AudioManager.STREAM_ALARM)+
                ",Notif:"+ audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION)+
                ",Ring:"+ audioManager.getStreamVolume(AudioManager.STREAM_RING)+
                ",System:"+ audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM)+
                ",Call:"+audioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL)
        );
    }
}
