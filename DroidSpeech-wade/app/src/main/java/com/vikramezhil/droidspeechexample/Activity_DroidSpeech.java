package com.vikramezhil.droidspeechexample;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vikramezhil.droidspeech.DroidSpeech;
import com.vikramezhil.droidspeech.OnDSListener;
import com.vikramezhil.droidspeech.OnDSPermissionsListener;

import java.util.List;
import java.util.Random;

/**
 * Droid Speech Example Activity
 *
 * @author Vikram Ezhil
 */

public class Activity_DroidSpeech extends Activity implements OnDSListener, OnDSPermissionsListener
{
    public final String TAG = "MyLog";

    private DroidSpeech droidSpeech;
    private TextView finalSpeechResult;

    // MARK: Activity Methods

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Setting the layout;[.
        setContentView(R.layout.activity_droid_speech);

        // Initializing the droid speech and setting the listener
        droidSpeech = new DroidSpeech(this, getFragmentManager());
        droidSpeech.setOnDroidSpeechListener(this);
        droidSpeech.setShowRecognitionProgressView(false);
        droidSpeech.setContinuousSpeechRecognition(true);
        droidSpeech.setOfflineSpeechRecognition(true);
        droidSpeech.setTAG(TAG);

        finalSpeechResult = findViewById(R.id.finalSpeechResult);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stop();
    }

    @Override
    public void onDroidSpeechSupportedLanguages(String currentSpeechLanguage, List<String> supportedSpeechLanguages)
    {
        Log.i(TAG, "Current speech language = " + currentSpeechLanguage);
        Log.i(TAG, "Supported speech languages = " + supportedSpeechLanguages.toString());

        if(supportedSpeechLanguages.contains("cmn-")) {
            // Setting the droid speech preferred language as tamil if found
            droidSpeech.setPreferredLanguage("cmn-Hant-TW");

            // Setting the confirm and retry text in tamil
            droidSpeech.setOneStepVerifyConfirmText("確認");
            droidSpeech.setOneStepVerifyRetryText("重試");
        } else {
            droidSpeech.setOneStepVerifyConfirmText("Confirm");
            droidSpeech.setOneStepVerifyRetryText("Try Again");
        }
    }

    @Override
    public void onDroidSpeechRmsChanged(float rmsChangedValue)
    {
    }

    @Override
    public void onDroidSpeechLiveResult(String liveSpeechResult)
    {
        Log.i(TAG, "onDroidSpeechLiveResult = " + liveSpeechResult);
    }

    @Override
    public void onDroidSpeechFinalResult(String finalSpeechResult) {
        Log.i(TAG, "onDroidSpeechFinalResult = " + finalSpeechResult);
        this.finalSpeechResult.setText(finalSpeechResult);
    }

    @Override
    public void onDroidSpeechClosedByUser() {
        Log.i(TAG, "onDroidSpeechClosedByUser = " + finalSpeechResult);
    }

    @Override
    public void onDroidSpeechError(String errorMsg) {
        Log.i(TAG, "onDroidSpeechError = " + finalSpeechResult);
        Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
        stop();
    }

    // MARK: DroidSpeechPermissionsListener Method

    @Override
    public void onDroidSpeechAudioPermissionStatus(boolean audioPermissionGiven, String errorMsgIfAny)
    {
        Log.i(TAG, "onDroidSpeechAudioPermissionStatus = " + errorMsgIfAny);
        if(audioPermissionGiven) {
            start();
        } else {
            if(errorMsgIfAny != null) {
                // Permissions error
                Toast.makeText(this, errorMsgIfAny, Toast.LENGTH_LONG).show();
            }
            stop();
        }
    }

    //////////////////////////////////////
    public void start() {
        Log.i(TAG, "start");
        droidSpeech.startDroidSpeechRecognition();
    }

    public void stop() {
        Log.i(TAG, "stop");
        droidSpeech.closeDroidSpeechOperations();
    }
}
