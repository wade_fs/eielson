
package com.wade.tempchart;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.wade.tempchart.notimportant.DemoBase;

import java.util.Locale;

import tw.com.prolific.pl2303ghidsdk.ErrorCode;
import tw.com.prolific.pl2303ghidsdk.HidToUart;
import tw.com.prolific.pl2303ghidsdk.UartObject;
import tw.com.prolific.pl2303ghidsdk.UartSetting;

public class RealtimeLineChartActivity extends DemoBase implements
        OnChartValueSelectedListener {
    private static final String TAG = "MyLog";
    private static final String DEFINE_ACTION_USB_PERMISSION = "tw.com.prolific.uartsample.USB_PERMISSION";

    private static final int DEFAULT_DATA_LENGTH = 64;
    private UartObject m_uartSettingObject = new UartObject();
    private UartObject m_readUartSettingObject = new UartObject();

    private final static int m_nDeviceVid = 0x067b;
    private int m_nUsbDeviceCount = 0;
    private int m_nDeviceIndex = 0;

    //Usb Permission
    private PendingIntent m_pendingIntent;
    HidToUart m_hidToUart;

    private static final int MSG_SHOW_INFO = 0x00;
    private static final int START_MEASURE = 0x01;
    private static final int STOP_MEASURE  = 0x02;

    private final String INFO_MESSAGE = "infoBody";
    private TextView m_txtLog;

    UartSetting m_uartSetting = new UartSetting();
    Thread readThread = null;
    private LineChart chart;

    private boolean measureStopped = true;
    private Button btnStart;
    private boolean isHeating = false;
    private Button btnHeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_realtime_linechart);

        btnStart = findViewById(R.id.btnStart);
        btnHeat = findViewById(R.id.btnHeat);
        chart = findViewById(R.id.chart1);
        chart.setOnChartValueSelectedListener(this);

        // enable description text
        chart.getDescription().setEnabled(true);

        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true);

        // set an alternative background color
        chart.setBackgroundColor(Color.LTGRAY);

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        chart.setData(data);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();

        // modify the legend ...
        l.setForm(LegendForm.LINE);
        l.setTypeface(tfLight);
        l.setTextColor(Color.WHITE);

        XAxis xl = chart.getXAxis();
        xl.setTypeface(tfLight);
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTypeface(tfLight);
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setAxisMaximum(45.0f);
        leftAxis.setAxisMinimum(-20.0f);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);

        // 溫度
        m_txtLog = (TextView) findViewById(R.id.txtLog);

        // get service
        m_hidToUart = new HidToUart(this, DEFINE_ACTION_USB_PERMISSION);
        //Step1: Register the broadcast receiver in onCreate() method
        //IntentFilter intentFilter = new IntentFilter(DEFINE_ACTION_USB_PERMISSION);
        IntentFilter m_intentFilter = new IntentFilter(DEFINE_ACTION_USB_PERMISSION);
        m_intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        m_intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        if(broadCastUsbReceiver != null) {
            registerReceiver(broadCastUsbReceiver, m_intentFilter);
        }

        //Setup Uart Object Default Value
        m_uartSettingObject.FlowControl = UartSetting.FlowControl.DISABLE;
        m_uartSettingObject.BaudRate = 9600;
        m_uartSettingObject.StopBit = UartSetting.StopBit.ONE;
        m_uartSettingObject.DataBit = UartSetting.DataBit.EIGHT;
        m_uartSettingObject.Parity =  UartSetting.Parity.NONE;
    }

    private final BroadcastReceiver broadCastUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG,"BroadcastReceiver-onReceive");
            String strAction = intent.getAction();
            if(strAction.equals(DEFINE_ACTION_USB_PERMISSION)) {
                Log.d(TAG,"DEFINE_ACTION_USB_PERMISSION");
                synchronized (this) {
                    UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(usbDevice !=null) {
//                            String strDeviceName = usbDevice.getDeviceName();

                            getDeviceConfig(m_nDeviceIndex);
                        }
                        Log.d(TAG,"Request Permission Pass");
                    } else {
                        Log.d(TAG,"Request Permission Fail");
                    }
                }
            }
            if( UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(strAction) ) {
                Log.d(TAG,"ACTION_USB_DEVICE_ATTACHED");
                //refresh enumdevice
                synchronized (this) {
                    m_nUsbDeviceCount = m_hidToUart.enumDevices(m_nDeviceVid);

                    getDeviceConfig(m_nDeviceIndex);
                }
                Log.d(TAG,"DeivceCount=" + m_nUsbDeviceCount);
            }
            if( UsbManager.ACTION_USB_DEVICE_DETACHED.equals(strAction) ) {
                Log.d(TAG,"ACTION_USB_DEVICE_DETACHED");
                //Remove Device, Cleans and closes communication with the device
                synchronized (this) {
                    m_nUsbDeviceCount = m_hidToUart.enumDevices(m_nDeviceVid);
                }
                Log.d(TAG,"DeivceCount=" + m_nUsbDeviceCount);
            }
        }//onReceive
    };

    private Runnable readRunnable = new Runnable(){ //Read Thread

        @Override
        public void run() {
            if(null != m_hidToUart) {
                Log.d(TAG,"Enter readRunnable");

                byte[] readBuffer = new byte[DEFAULT_DATA_LENGTH];

                int res = m_hidToUart.setUartConfig(m_nDeviceIndex, m_uartSettingObject);
                if (res < 0) {
                    Log.d(TAG,"readRunnable setUartConfig Failed.");
                    return;
                }

                Log.d(TAG,"Starting Reading Data");

                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        break;
                    }
                    if (measureStopped) {
                        Log.d(TAG,"readRunnable Stopped.");
                        break;
                    }
                    res = m_hidToUart.uartRead(m_nDeviceIndex, readBuffer, readBuffer.length, 1000);
                    if (res == ErrorCode.ERROR_READ_DATA_FAIL) {
                        Log.d(TAG,"readRunnable read data failed.");
                    } else if (res < 0) {
                        Log.d(TAG,"readRunnable uartRead Failed.");
                    } else { //Read data success
                        double temp = ShowBuffer(readBuffer, 5);
                        if (25.0f < temp && temp < 44f)
                            addEntry(temp);
                        Log.d(TAG,"readRunnable read data success with "+readBuffer.length + " bytes "+temp);
                        readBuffer = new byte[DEFAULT_DATA_LENGTH];
                    }
                }
            } else {
                Log.d(TAG, "m_hidToUart is null");
            }
        }
    };

    private final static byte startHeat[] =
            { (byte)0xFF, 0x10, 0x20, 0x21, 0x51 };
    private final static byte stopHeat[] =
            { (byte)0xFF, 0x11, 0x21, 0x20, 0x52 };
    public void onHeat(View view) {
        if (!isHeating) { // heating
            btnHeat.setText(R.string.heating);
            isHeating = true;
            m_hidToUart.uartWrite(m_nDeviceIndex, startHeat, 5,1000);
        } else {
            isHeating = false;
            btnHeat.setText(R.string.stopHeating);
            m_hidToUart.uartWrite(m_nDeviceIndex, stopHeat, 5,1000);
        }
    }

    //btnChart event
    public void onHistory(View view) {
        showLog(getString(R.string.notYetImplement));
    }

    //btnStart event
    public void onStartMeasure(View view) {
        Message msg = new Message();
        Intent intent = new Intent();
        if (measureStopped) {
            measureStopped = false;
            btnStart.setText(getString(R.string.stopMeasure));
            chart.clearValues();
            msg.what = START_MEASURE;
        } else {
            measureStopped = true;
            btnStart.setText(getString(R.string.measureStopped));
            msg.what = STOP_MEASURE;
        }
        msg.obj = intent;
        mHandler.sendMessage(msg);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // retrieves the USB devices, which has VID/PID ID associated which is/are attached to the system
        m_nUsbDeviceCount = m_hidToUart.enumDevices(m_nDeviceVid);
        Log.d(TAG,"Enter onStart "+m_nUsbDeviceCount+" devices");
        if( 0 < m_nUsbDeviceCount) {
            if(!m_hidToUart.hasPermissionByDeviceIndex(0)) {
                m_hidToUart.askPermissionByDeviceIndex(0);
            }
        }
        Log.d(TAG,"Leave onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG,"onDestroy");
        try {
            if(readThread!= null && readThread.isAlive()) {
                readThread.join();
                readThread = null;
                Log.d(TAG,"onDestroy Stop ReadThread.");
            }
        } catch (InterruptedException e) {
            Log.d(TAG,"onDestroy Stop ReadThread exception.");
            e.printStackTrace();
            readThread = null;
        }

        if(0!= m_nUsbDeviceCount) {
            unregisterReceiver(this.broadCastUsbReceiver);
        }
        finish();
        super.onDestroy();
    }

    synchronized public void showLog(String strLog){
        m_txtLog.setText(strLog);
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes, int len) {
        char[] hexChars = new char[bytes.length * 2];
        int l = len;
        if (l > bytes.length) {
            l = bytes.length;
        }
        for (int j = 0; j < l; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    private static int getUnsignedByte(byte b) {
        return b & 0x0FF;
    }
    public static String byteToHex(byte b) {
        char[] hexChars = new char[2];
        int v = (int)(b & 0xFF);
        hexChars[0] = HEX_ARRAY[v >>> 4];
        hexChars[1] = HEX_ARRAY[v & 0x0F];
        return new String(hexChars);
    }
    private final static double NOT_VALID = -1000.0f;
    private double ShowBuffer(byte[] buffer, int len) {
        double temp_val = NOT_VALID;
        int bit_sign = buffer[1];
        int temp_byte0 = getUnsignedByte(buffer[2]);
        int temp_byte1 = getUnsignedByte(buffer[3]);
        byte checksum = (byte)(buffer[1] + buffer[2] + buffer[3]);
        if (checksum == buffer[4]) {
            int val = (temp_byte0 << 8) + temp_byte1;

            if (bit_sign == 1) { // 負數
                val =~ val;
                val &= 0xFFFF;
                val++;
                temp_val = -3.90625 * val / 1000.00;
            } else {
                temp_val = 3.90625 * val / 1000.00;
            }
        }
        if (temp_val != NOT_VALID) {
            Message msg = new Message();
            msg.what = MSG_SHOW_INFO;
            Intent intent = new Intent();
            intent.putExtra(INFO_MESSAGE,
                    String.format(Locale.ENGLISH, "%2.1f", temp_val + 0.5f));
            msg.obj = intent;
            mHandler.sendMessage(msg);
        }
        return temp_val;
    }

    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case MSG_SHOW_INFO:
                    String str1 = ((Intent)msg.obj).getExtras().getString(INFO_MESSAGE);
                    showLog(str1);
                    break;
                case STOP_MEASURE: {
                    try {
                        if(readThread!= null && readThread.isAlive()) {
                            readThread.join();
                            readThread = null;
                            Log.d(TAG,"onDestroy Stop ReadThread.");
                        }
                    } catch (InterruptedException e) {
                        Log.d(TAG,"onDestroy Stop ReadThread exception.");
                        e.printStackTrace();
                        readThread = null;
                    }
                }
                case START_MEASURE: {
                    try {
                        if(readThread==null) {
                            getDeviceConfig(m_nDeviceIndex);
                            readThread = new Thread(readRunnable);
                            readThread.start();
                        } else if(!readThread.isAlive()) {
                            Log.d(TAG, "readThread not alive");
                            readThread.join();
                            readThread = null;
                            readThread = new Thread(readRunnable);
                            readThread.start();
                        }
                    } catch (InterruptedException e) {
                        Log.d(TAG,"readThread error");
                    }
                }
            }
            super.handleMessage(msg);
        }
    };

    void getDeviceConfig(int index) {
        Log.d(TAG,"Enter getDeviceConfig("+index+")");
        if(null == m_hidToUart) {
            Log.d(TAG,"Leave getDeviceConfig() m_hidToUart is null");
            return;
        }
        int res = m_hidToUart.openDeviceByIndex(index);
        if(res<0) {
            Log.d(TAG,"getDeviceConfig Open device failed.");
            return;
        }

        //Read Uart Config and compare
        res = m_hidToUart.getUartConfig(m_nDeviceIndex, m_readUartSettingObject);
        if(res<0) {
            Log.d(TAG,"getDeviceConfig get default setting failed.");
            return;
        }

        Log.d(TAG,"Leave getDeviceConfig");
    }

    private void addEntry() {
        addEntry(Math.random()*30);
    }

    private void addEntry(double temp) {
        LineData data = chart.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            data.addEntry(new Entry(set.getEntryCount(), (float) (temp)), 0);
            data.notifyDataChanged();

            // let the chart know it's data has changed
            chart.notifyDataSetChanged();

            // limit the number of visible entries
            chart.setVisibleXRangeMaximum(50);

            // move to the latest entry
            chart.moveViewToX(data.getEntryCount());
        }
    }

    private LineDataSet createSet() {

        LineDataSet set = new LineDataSet(null, getString(R.string.app_name));
        set.setAxisDependency(AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(4f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        return set;
    }

    private Thread thread;

    private void feedMultiple() {

        if (thread != null)
            thread.interrupt();

        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                addEntry();
            }
        };

        thread = new Thread(new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {

                    // Don't generate garbage runnables inside the loop.
                    runOnUiThread(runnable);

                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.realtime, menu);
        return true;
    }
    //menu about event
    void onAbout() {
        String strVersion = "SDK版本: " + m_hidToUart.getSdkVersion();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.about);
        alertDialog.setMessage(strVersion);
        alertDialog.setPositiveButton(R.string.OK, null);
        alertDialog.show();
    }

    //menu finish event
    void onFinishTest() {
        Log.d(TAG, "onFinishTest()");
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.actionAdd: {
                addEntry();
                break;
            }
            case R.id.actionClear: {
                chart.clearValues();
                Toast.makeText(this, "Chart cleared!", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.actionFeedMultiple: {
                feedMultiple();
                break;
            }
            case R.id.actionSave: {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    saveToGallery();
                } else {
                    requestStoragePermission(chart);
                }
                break;
            }
            case R.id.action_about:
                onAbout();
                return true;
            case R.id.action_finish:
                onFinishTest();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "RealtimeLineChartActivity");
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (thread != null) {
            thread.interrupt();
        }
    }
}
