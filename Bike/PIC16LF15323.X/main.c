/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC16LF15323
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"
#include "motion_sensor_custom.h"

extern long int gx, gy, gz;
extern int x_value[MAX_COUNT], y_value[MAX_COUNT], z_value[MAX_COUNT];

uint16_t tmr0_flag_25ms=0;
uint16_t tmr0_flag_1ms=0;

uint16_t bat = 0;
uint8_t checkCharger() {
    uint8_t ret = 0;
    uint8_t blink=0;
    
    // Battery bat get value in adc isr() but not poll
    //   bat = ADC_GetConversion(BAT);
    if (bat <= 750) {
        if (blink == 0) {
            blink = 1;
            LED_R_SetLow();
            LED_P_SetLow();
        } else {
            blink = 0;
            LED_R_SetHigh();
            LED_P_SetHigh();
        }
        ret = 0;
    } else {  // 0x03FF = 1024 full, 0x03CD is high, 0x0320 is low
        if (bat >= 800) { // normal
            LED_G_SetLow();
            LED_R_SetHigh();
            ret = 1;
        } else { // low
            LED_G_SetHigh();
            LED_R_SetLow();
            ret = 2;
        }
    }
    return ret;
}

void pwm_light(uint16_t duty) {
    if (duty >= 1023) duty = 1023;
    PWM3_LoadDutyValue(duty);
}

extern long int gx, gy, gz;
long int gx2, gy2, gz2;
int last_sensor = 0, delay_count=0;
uint8_t last_light = 0;
void g_sensor() {
        long int dx, dy, dz;
        if (acc_sensor_get_acc()) {
        acc_sensor_read_status();
        acc_sensor_clear_interrupt_status(); // it can read G-sensor INT 
        if(tmr0_flag_25ms) {
            tmr0_flag_25ms = 0;
            dx = (gx2-gx); dx *= 3*dx;
            dy = (gy2-gy); dy *= 3*dy;
            dz = (gz2-gz); dz *= 3*dz;
            gx2 = gx; gy2 = gy; gz2 = gz;
            last_sensor = (last_sensor*3 + dx+dy+dz) / 4;

            // ???g-sensor ????
            if (tmr0_flag_1ms > 1023) tmr0_flag_1ms = 0;
            pwm_light(tmr0_flag_1ms+last_sensor);
        }
    }
}
/*
                         Main application
 */
void main(void)
{
    // initialize the device
    SYSTEM_Initialize();

    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
#define G_SENSOR 1
#if G_SENSOR
    g_sensor_initial();
    ADC_StartConversion(1);
#else
    bat = ADC_GetConversion(BAT); // first time to start ISR
#endif

    __delay_ms(10);
    
    pwm_light(0);
    LED_P_SetLow();
    LED_R_SetHigh();
    LED_G_SetHigh();
    checkCharger();
    tmr0_flag_1ms=0;
    
    uint8_t dir_turn=1;
    while (1)
    {
#if G_SENSOR
        g_sensor();
#else
        // Add your application code
        if (dir_turn)
        {
            if (tmr0_flag_1ms<1023)
            {
                pwm_light(tmr0_flag_1ms);
            } else {
                pwm_light(1023);
                tmr0_flag_1ms=0;
                dir_turn=0;
            }
            
        } else {
            if (tmr0_flag_1ms<1023) {
                pwm_light(1023-tmr0_flag_1ms);
            } else {
                pwm_light(0);
                switch (checkCharger()) {
                    case 0: // critical low
                        tmr0_flag_1ms=1024;
                        dir_turn = 0;
                        __delay_ms(10);
                        continue;
                    case 1: // normal
                    case 2: // low
                    case 3: // no change
                        tmr0_flag_1ms=0;
                        dir_turn=1;
                    break;
                }
            }
        }
        __delay_ms(10);
#endif
    }
}
/**
 End of File
*/
