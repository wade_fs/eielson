/*****************************************************************************
 *
 * Filename:
 * ---------
 *    Motion_sensor_custom.c
 *
 * Project:
 * --------
 *    Motion Sensor stk832x SPI Interface
 *
 * Description:
 * ------------
 *   This Module defines Serial Interface.
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 ****************************************************************************/

#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "motion_sensor_custom.h"   
#include "stk8321.h"   

/* ---------------------------------------------------------------------------- */
/*
* FUNCTION                                                            
*  motion_sensor_check_softwave_ready
*
* DESCRIPTION                                                           
*   check sensor softwave function completion
*
* PARAMETERS
*  received data
*  
* RETURNS
*  None
*/    
void motion_sensor_check_softwave_ready(void)
{    
  do {
    __delay_ms(50);
  } while ((motion_sensor_read_data(STK2x2_CHIP_ID_ADDR) != 0x23));  //check chip ID = 0x23
}

/*
* FUNCTION                                                            
*  acc_sensor_pwr_up
*
* DESCRIPTION                                                           
*     This function is to power up acceleration module
*
* CALLS  
*
* PARAMETERS
*  None
*  
* RETURNS
*  None
*/    
void acc_sensor_pwr_up(void)
{
  __delay_ms(30);                        //delay for power on reset  
  motion_sensor_write_data(0xB6, STK2x2_RST_ADDR);      // stk832x soft reset
  motion_sensor_check_softwave_ready();            // wait & check reset completion
  
  motion_sensor_write_data(0x03, STK2x2_RANGE_SELECT_ADDR);  // set to low current, 2g
  motion_sensor_write_data(0x0A, STK2x2_BW_SELECT_ADDR);    // set ODR 62.5Hz
}

/*
* FUNCTION                                                            
*  acc_sensor_pwr_down 
*
* DESCRIPTION                                                           
*     This function is to power down acceleration
*
* CALLS  
*
* PARAMETERS
*  None
*  
* RETURNS
*  None
*/ 
void acc_sensor_pwr_down(void)
{   
  motion_sensor_enable_bits(0x80, STK2x2_MODE_CTRL_ADDR);    // into suspend mode (standby)
}

/*
* FUNCTION                                                            
*  acc_sensor_init
*
* DESCRIPTION                                                           
*     This function is to initialize acceleration module
*
* CALLS  
*
* PARAMETERS
*  None
*  
* RETURNS
*  None
*/ 
void acc_sensor_init(void)
{  
  motion_senosr_interface_init();
}

/*
* FUNCTION                                                            
*  acc_sensor_interrupt_initial
*
* DESCRIPTION                                                           
*   Set & enable interrupt function
*
* CALLS  
*
* PARAMETERS
*  None
*  
* RETURNS
*  None
*/    
void acc_sensor_interrupt_initial(void)
{
  motion_sensor_enable_bits(0x80, STK2x2_MODE_CTRL_ADDR);    // into suspend mode (standby)
  motion_sensor_write_data(0x05, STK2x2_INTR_SET_ADDR);    // active high, push-pull
  motion_sensor_write_data(0x8F, STK2x2_INTR_CTRL_ADDR);    // latched mode & reset INT
    //motion_sensor_write_data(,);  //etilt_angle_ll
    //motion_sensor_write_data(,);  //etilt_angle_HL
  
    //ADD INT interrupt
    //motion_sensor_write_data(0x04, STK2x2_SIGMOT2_ADDR);    // enable any-motion (WUF)
  //motion_sensor_write_data( , );              // *** stk832x has NO tilt angle detection *** //

  motion_sensor_disable_bits(0x80, STK2x2_MODE_CTRL_ADDR);  // set to work mode 
}

/*
* FUNCTION                                                            
*  acc_sensor_WUF_initial
*
* DESCRIPTION                                                           
*   Set & enable the WUF function
*
* CALLS  
*
* PARAMETERS
*  None
*  
* RETURNS
*  None
*/    
void acc_sensor_wuf_initial(void)
{
  motion_sensor_write_data(0x00, STK2x2_SLOPE_DURN_ADDR);    // slope duration (WUF_TIMER)
  motion_sensor_write_data(0x80, STK2x2_SLOPE_THRES_ADDR);  // 0x80=0d128, 128*3.91=~500, Threshold = 500mg @ 2G range
  motion_sensor_write_data(0x07, STK2x2_INTR_ENABLE1_ADDR);   // enable slope function (WUF engine)
  motion_sensor_write_data(0x04, STK2x2_SIGMOT2_ADDR);    // enable any-motion (WUF)
}

/*
* FUNCTION                                                            
*  acc_sensor_clear_int_status
*
* DESCRIPTION                                                           
*   Release the latch mode of interrupt
*
* CALLS  
*
* PARAMETERS
*  None
*  
* RETURNS
*  None
*/
void acc_sensor_clear_interrupt_status(void)
{
  motion_sensor_write_data(0x8F, STK2x2_INTR_CTRL_ADDR);      //  release interrupt latch status
}

/*
* FUNCTION                                                            
*  acc_sensor_read_status
*
* DESCRIPTION                                                           
*   Read the interrupt status
*
* CALLS  
*
* PARAMETERS
*  *status1_value : status 1 value 
*  *status2_value : status 2 value 
*   
* RETURNS
*  None
*/

uint8_t status1_value=0;
uint8_t status2_value=0;
uint8_t status3_value=0;
void acc_sensor_read_status(void)
{
  status1_value = motion_sensor_read_data(STK2x2_STAT1_ADDR); // any-motion & sig motion status
  status2_value = motion_sensor_read_data(STK2x2_STAT2_ADDR); // data & FIFO status 
  status3_value = motion_sensor_read_data(STK2x2_STAT_FIFO_ADDR); // FIFO overrun & FIFO count

  NOP();
  NOP();
}

/*Customization functin*/ 
/*
* FUNCTION                                                            
*  acc_sensor_get_acc
*
* DESCRIPTION                                                           
*     This function is to get ADC value in every axis
*
* CALLS  
*
* PARAMETERS
*  x_adc: ADC value in X-axis
*  y_adc: ADC value in Y-axis
*  z_adc: ADC value in Z-axis
*
* RETURNS
*  None
*/ 

int x_value[MAX_COUNT]={0};
int y_value[MAX_COUNT]={0};
int z_value[MAX_COUNT]={0};
long int gx=0, gy=0, gz=0;
uint8_t g_index=0;

void dump_data(uint8_t* data, uint8_t len) {
  for (uint8_t i=0; i<len; ++i) {
    printf("%02X ", data[i]);
  }
  printf("\r\n");
}
int acc_sensor_get_acc()
{  
  static uint8_t temp_data[6];
  motion_sensor_read_multi_data(STK2x2_X_AXIS_LSB_ADDR, temp_data, 6);
  dump_data(temp_data, 6);
  x_value[g_index] = ((temp_data[1] << 8) | temp_data[0]);    /* x-axis */      
  y_value[g_index] = ((temp_data[3] << 8) | temp_data[2]);    /* y-axis */      
  z_value[g_index] = ((temp_data[5] << 8) | temp_data[4]);    /* z-axis */   
  ++g_index;
  if (g_index >= MAX_COUNT) {
    gx = gy = gz = 0;
    for (int i=0; i<MAX_COUNT; ++i) {
      gx += x_value[i];
      gy += y_value[i];
      gz += z_value[i];
    }
    printf("\tg-sensor[%d,%d,%d]\r\n", gx, gy, gz);
    gx /= 8196;
    gy /= 8196;
    gz /= 8196;
    g_index=0;
    return 1;
  } else return 0;
}
