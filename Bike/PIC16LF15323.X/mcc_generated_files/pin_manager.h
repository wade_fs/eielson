/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC16LF15323
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set RA2 procedures
#define RA2_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define RA2_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define RA2_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define RA2_GetValue()              PORTAbits.RA2
#define RA2_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define RA2_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define RA2_SetPullup()             do { WPUAbits.WPUA2 = 1; } while(0)
#define RA2_ResetPullup()           do { WPUAbits.WPUA2 = 0; } while(0)
#define RA2_SetAnalogMode()         do { ANSELAbits.ANSA2 = 1; } while(0)
#define RA2_SetDigitalMode()        do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set RA4 procedures
#define RA4_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define RA4_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define RA4_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define RA4_GetValue()              PORTAbits.RA4
#define RA4_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define RA4_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define RA4_SetPullup()             do { WPUAbits.WPUA4 = 1; } while(0)
#define RA4_ResetPullup()           do { WPUAbits.WPUA4 = 0; } while(0)
#define RA4_SetAnalogMode()         do { ANSELAbits.ANSA4 = 1; } while(0)
#define RA4_SetDigitalMode()        do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set BAT aliases
#define BAT_TRIS                 TRISAbits.TRISA5
#define BAT_LAT                  LATAbits.LATA5
#define BAT_PORT                 PORTAbits.RA5
#define BAT_WPU                  WPUAbits.WPUA5
#define BAT_OD                   ODCONAbits.ODCA5
#define BAT_ANS                  ANSELAbits.ANSA5
#define BAT_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define BAT_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define BAT_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define BAT_GetValue()           PORTAbits.RA5
#define BAT_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define BAT_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define BAT_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define BAT_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define BAT_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define BAT_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define BAT_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define BAT_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set SCK1 aliases
#define SCK1_TRIS                 TRISCbits.TRISC0
#define SCK1_LAT                  LATCbits.LATC0
#define SCK1_PORT                 PORTCbits.RC0
#define SCK1_WPU                  WPUCbits.WPUC0
#define SCK1_OD                   ODCONCbits.ODCC0
#define SCK1_ANS                  ANSELCbits.ANSC0
#define SCK1_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define SCK1_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define SCK1_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define SCK1_GetValue()           PORTCbits.RC0
#define SCK1_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define SCK1_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define SCK1_SetPullup()          do { WPUCbits.WPUC0 = 1; } while(0)
#define SCK1_ResetPullup()        do { WPUCbits.WPUC0 = 0; } while(0)
#define SCK1_SetPushPull()        do { ODCONCbits.ODCC0 = 0; } while(0)
#define SCK1_SetOpenDrain()       do { ODCONCbits.ODCC0 = 1; } while(0)
#define SCK1_SetAnalogMode()      do { ANSELCbits.ANSC0 = 1; } while(0)
#define SCK1_SetDigitalMode()     do { ANSELCbits.ANSC0 = 0; } while(0)

// get/set SDI1 aliases
#define SDI1_TRIS                 TRISCbits.TRISC1
#define SDI1_LAT                  LATCbits.LATC1
#define SDI1_PORT                 PORTCbits.RC1
#define SDI1_WPU                  WPUCbits.WPUC1
#define SDI1_OD                   ODCONCbits.ODCC1
#define SDI1_ANS                  ANSELCbits.ANSC1
#define SDI1_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define SDI1_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define SDI1_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define SDI1_GetValue()           PORTCbits.RC1
#define SDI1_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define SDI1_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define SDI1_SetPullup()          do { WPUCbits.WPUC1 = 1; } while(0)
#define SDI1_ResetPullup()        do { WPUCbits.WPUC1 = 0; } while(0)
#define SDI1_SetPushPull()        do { ODCONCbits.ODCC1 = 0; } while(0)
#define SDI1_SetOpenDrain()       do { ODCONCbits.ODCC1 = 1; } while(0)
#define SDI1_SetAnalogMode()      do { ANSELCbits.ANSC1 = 1; } while(0)
#define SDI1_SetDigitalMode()     do { ANSELCbits.ANSC1 = 0; } while(0)

// get/set SDO1 aliases
#define SDO1_TRIS                 TRISCbits.TRISC2
#define SDO1_LAT                  LATCbits.LATC2
#define SDO1_PORT                 PORTCbits.RC2
#define SDO1_WPU                  WPUCbits.WPUC2
#define SDO1_OD                   ODCONCbits.ODCC2
#define SDO1_ANS                  ANSELCbits.ANSC2
#define SDO1_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define SDO1_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define SDO1_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define SDO1_GetValue()           PORTCbits.RC2
#define SDO1_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define SDO1_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define SDO1_SetPullup()          do { WPUCbits.WPUC2 = 1; } while(0)
#define SDO1_ResetPullup()        do { WPUCbits.WPUC2 = 0; } while(0)
#define SDO1_SetPushPull()        do { ODCONCbits.ODCC2 = 0; } while(0)
#define SDO1_SetOpenDrain()       do { ODCONCbits.ODCC2 = 1; } while(0)
#define SDO1_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define SDO1_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set LED_R aliases
#define LED_R_TRIS                 TRISCbits.TRISC3
#define LED_R_LAT                  LATCbits.LATC3
#define LED_R_PORT                 PORTCbits.RC3
#define LED_R_WPU                  WPUCbits.WPUC3
#define LED_R_OD                   ODCONCbits.ODCC3
#define LED_R_ANS                  ANSELCbits.ANSC3
#define LED_R_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define LED_R_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define LED_R_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define LED_R_GetValue()           PORTCbits.RC3
#define LED_R_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define LED_R_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define LED_R_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define LED_R_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define LED_R_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define LED_R_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define LED_R_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define LED_R_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set LED_G aliases
#define LED_G_TRIS                 TRISCbits.TRISC4
#define LED_G_LAT                  LATCbits.LATC4
#define LED_G_PORT                 PORTCbits.RC4
#define LED_G_WPU                  WPUCbits.WPUC4
#define LED_G_OD                   ODCONCbits.ODCC4
#define LED_G_ANS                  ANSELCbits.ANSC4
#define LED_G_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define LED_G_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define LED_G_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define LED_G_GetValue()           PORTCbits.RC4
#define LED_G_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define LED_G_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define LED_G_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define LED_G_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define LED_G_SetPushPull()        do { ODCONCbits.ODCC4 = 0; } while(0)
#define LED_G_SetOpenDrain()       do { ODCONCbits.ODCC4 = 1; } while(0)
#define LED_G_SetAnalogMode()      do { ANSELCbits.ANSC4 = 1; } while(0)
#define LED_G_SetDigitalMode()     do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set LED_P aliases
#define LED_P_TRIS                 TRISCbits.TRISC5
#define LED_P_LAT                  LATCbits.LATC5
#define LED_P_PORT                 PORTCbits.RC5
#define LED_P_WPU                  WPUCbits.WPUC5
#define LED_P_OD                   ODCONCbits.ODCC5
#define LED_P_ANS                  ANSELCbits.ANSC5
#define LED_P_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define LED_P_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define LED_P_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define LED_P_GetValue()           PORTCbits.RC5
#define LED_P_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define LED_P_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define LED_P_SetPullup()          do { WPUCbits.WPUC5 = 1; } while(0)
#define LED_P_ResetPullup()        do { WPUCbits.WPUC5 = 0; } while(0)
#define LED_P_SetPushPull()        do { ODCONCbits.ODCC5 = 0; } while(0)
#define LED_P_SetOpenDrain()       do { ODCONCbits.ODCC5 = 1; } while(0)
#define LED_P_SetAnalogMode()      do { ANSELCbits.ANSC5 = 1; } while(0)
#define LED_P_SetDigitalMode()     do { ANSELCbits.ANSC5 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);


/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handler for the IOCAF2 pin functionality
 * @Example
    IOCAF2_ISR();
 */
void IOCAF2_ISR(void);

/**
  @Summary
    Interrupt Handler Setter for IOCAF2 pin interrupt-on-change functionality

  @Description
    Allows selecting an interrupt handler for IOCAF2 at application runtime
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    InterruptHandler function pointer.

  @Example
    PIN_MANAGER_Initialize();
    IOCAF2_SetInterruptHandler(MyInterruptHandler);

*/
void IOCAF2_SetInterruptHandler(void (* InterruptHandler)(void));

/**
  @Summary
    Dynamic Interrupt Handler for IOCAF2 pin

  @Description
    This is a dynamic interrupt handler to be used together with the IOCAF2_SetInterruptHandler() method.
    This handler is called every time the IOCAF2 ISR is executed and allows any function to be registered at runtime.
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCAF2_SetInterruptHandler(IOCAF2_InterruptHandler);

*/
extern void (*IOCAF2_InterruptHandler)(void);

/**
  @Summary
    Default Interrupt Handler for IOCAF2 pin

  @Description
    This is a predefined interrupt handler to be used together with the IOCAF2_SetInterruptHandler() method.
    This handler is called every time the IOCAF2 ISR is executed. 
    
  @Preconditions
    Pin Manager intializer called

  @Returns
    None.

  @Param
    None.

  @Example
    PIN_MANAGER_Initialize();
    IOCAF2_SetInterruptHandler(IOCAF2_DefaultInterruptHandler);

*/
void IOCAF2_DefaultInterruptHandler(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/