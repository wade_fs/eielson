/*****************************************************************************
 *
 * Filename:
 * ---------
 *    Motion_sensor_spi.h
 *
 * Project:
 * --------
 *    Motion Sensor SPI Interface
 *
 * Description:
 * ------------
 *   This Module defines Serial Interface.
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 ****************************************************************************/

#ifndef __STK8321_H__
#define __STK8321_H__
#include "mcc_generated_files/mcc.h"

#define MS_SPI_CLK_PIN   		LATCbits.LATC0
#define MS_SPI_MISO_PIN  		LATCbits.LATC1
#define MS_SPI_MOSI_PIN  		LATCbits.LATC2

#define SET_MS_CLK_HIGH			LATCbits.LATC0=1;
#define SET_MS_CLK_LOW			LATCbits.LATC0=0;
#define SET_MS_MOSI_HIGH		LATCbits.LATC2=1;
#define SET_MS_MOSI_LOW			LATCbits.LATC2=0;
#define GET_MS_DATA			MS_SPI_MISO_PIN

#define MS_Delay(x) __delay_ms((x))
void motion_senosr_interface_init(void);
void motion_sensor_send_byte(uint8_t send_byte);
uint8_t motion_sensor_get_byte(void);
void motion_sensor_write_data(uint8_t data, uint8_t addr);
uint8_t motion_sensor_read_data(uint8_t addr);
void motion_sensor_read_multi_data(uint8_t addr, uint8_t *data, uint8_t num);
void motion_sensor_disable_bits(uint8_t data, uint8_t addr);
void motion_sensor_enable_bits(uint8_t data, uint8_t addr);
void g_sensor_initial(void);
uint8_t g_sensor_int_value(void);
uint8_t g_sensor_vision(void);

#endif
