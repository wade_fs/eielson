#ifndef __STK8321_H__
#define __STK8321_H__

#include "mcc_generated_files/mcc.h"
#include "stk8321.h"
#include "stk8321_custom.h"

void motion_sensor_interface_init(void) {
  SCK1_SetLow();
  SDO1_SetHigh();
}

void stk8321_write_data(uint8_t data, uint8_t addr)
{
  SPI1_Exchange8bit(addr & 0x7F);
  SPI1_Exchange8bit(data);      
  NOP();
  SPI1_Exchange8bit(addr & 0x7F);
  SPI1_Exchange8bit(data);      
}

uint8_t stk8321_read_data(uint8_t addr)
{
  uint8_t value = 0;

  SPI1_Exchange8bit(addr | 0x80);
  value = SPI1_Exchange8bit(0);

  return value;
}

void stk8321_read_multi_data(uint8_t addr, uint8_t *data, uint8_t num)
{ 
  SPI1_Exchange8bit(addr | 0x80);
  
  for (uint8_t i=0; i<=num; i++) {
    data[i] = SPI1_Exchange8bit(0);
  }
}

void stk8321_disable_bits(uint8_t data, uint8_t addr)
{
  uint8_t temp_data;

  temp_data = stk8321_read_data(addr);
  temp_data &= (~data);
  stk8321_write_data(temp_data, addr);
}

void stk8321_enable_bits(uint8_t data, uint8_t addr)
{
  uint8_t temp_data;

  temp_data = stk8321_read_data(addr);
  temp_data |= data;
  stk8321_write_data(temp_data, addr);
}

uint8_t g_sensor_vision(void)
{
    uint8_t g_id=0;
    
  __delay_ms(2);
  SDI1_SetLow();
  SPI1_Exchange8bit(STK2x2_CHIP_ID_ADDR | 0x80 );
  g_id = SPI1_Exchange8bit(0);
  SDI1_SetHigh();
  NOP();
  NOP();
  return g_id;
}

void g_sensor_initial(void)
{
  acc_sensor_pwr_down();
  acc_sensor_init();
  acc_sensor_interrupt_initial();
  acc_sensor_wuf_initial();
  acc_sensor_clear_interrupt_status();
  g_sensor_vision();
}

uint8_t g_sensor_int_value(void) {
  unsigned char int_value_temp = 0x80; //0x80=0d128, 128*3.91=~500, Threshold = 500mg @ 2G range
                
  acc_sensor_clear_interrupt_status(); // it can read G-sensor INT 
  stk8321_write_data(int_value_temp, STK2x2_SLOPE_THRES_ADDR);    //slope threshold  
  return stk8321_read_data(STK2x2_SLOPE_THRES_ADDR);
}

#endif
