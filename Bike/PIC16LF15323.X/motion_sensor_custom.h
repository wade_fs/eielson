/*****************************************************************************
 *
 * Filename:
 * ---------
 *    Motion_sensor_custom.c
 *
 * Project:
 * --------
 *    Motion Sensor STK832x I2C Interface
 *
 * Description:
 * ------------
 *   This Module defines Serial Interface.
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 ****************************************************************************/
#ifndef _MOTION_SENSOR_CUSTOM_H
#define _MOTION_SENSOR_CUSTOM_H

#include "mcc_generated_files/mcc.h"
#include "stk8321.h"

/* Gsensor slave address and read write bit */
#define MOTION_SENSOR_ADDRESS    		0x1F 	// when "PIN-ADDR" connect to "IO_VDD"
// #define MOTION_SENSOR_ADDRESS    	0x0F 	// when "PIN-ADDR" connect to "GND"

/** Motion sensor controller register macro define **/
#define STK2x2_CHIP_ID_ADDR		                (0x00)

/** DATA ADDRESS DEFINITIONS */
#define STK2x2_X_AXIS_LSB_ADDR                  (0x02)
#define STK2x2_X_AXIS_MSB_ADDR                  (0x03)
#define STK2x2_Y_AXIS_LSB_ADDR                  (0x04)
#define STK2x2_Y_AXIS_MSB_ADDR                  (0x05)
#define STK2x2_Z_AXIS_LSB_ADDR                  (0x06)
#define STK2x2_Z_AXIS_MSB_ADDR                  (0x07)

/**STATUS ADDRESS DEFINITIONS */
#define STK2x2_STAT1_ADDR                       (0x09)
#define STK2x2_STAT2_ADDR                       (0x0A)
#define STK2x2_STAT_TAP_SLOPE_ADDR              (0x0B)
#define STK2x2_STAT_FIFO_ADDR                   (0x0C)

/**STATUS ADDRESS DEFINITIONS */
#define STK2x2_RANGE_SELECT_ADDR                (0x0F)
#define STK2x2_BW_SELECT_ADDR                   (0x10)
#define STK2x2_MODE_CTRL_ADDR                   (0x11)
#define STK2x2_DATA_CTRL_ADDR                   (0x13)
#define STK2x2_RST_ADDR                         (0x14)

/**INTERUPT ADDRESS DEFINITIONS */
#define STK2x2_INTR_ENABLE1_ADDR                (0x16)
#define STK2x2_INTR_ENABLE2_ADDR                (0x17)
#define STK2x2_INTMAP1_ADDR                     (0x19)
#define STK2x2_INTMAP2_ADDR                     (0x1A)
#define STK2x2_INTR_SET_ADDR                    (0x20)
#define STK2x2_INTR_CTRL_ADDR                   (0x21)

/** FEATURE ADDRESS DEFINITIONS */
#define STK2x2_SLOPE_DURN_ADDR                  (0x27)
#define STK2x2_SLOPE_THRES_ADDR                 (0x28)
#define STK2x2_SIGMOT1_ADDR                     (0x29)
#define STK2x2_SIGMOT2_ADDR                     (0x2A)
#define STK2x2_SIGMOT3_ADDR                     (0x2B)

#define STK2x2_SERIAL_CTRL_ADDR                 (0x34)
#define STK2x2_OFFSET_CTRL_ADDR                 (0x36)
#define STK2x2_OFFSET_X_AXIS_ADDR               (0x38)
#define STK2x2_OFFSET_Y_AXIS_ADDR               (0x39)
#define STK2x2_OFFSET_Z_AXIS_ADDR               (0x3A)

/** interrupt configuration **/
#define MOTION_SENSOR_INT_SUPPORT   FALSE
#define MOTION_SENSOR_INT           4
#define MOTION_SENSOR_INT_LEVEL     LEVEL_HIGH
#define MOTION_SENSOR_LOW_G_MASK    0x1
#define MOTION_SENSOR_HIGH_G_MASK   0x2

/** PARAMETER **/
#define MAX_COUNT 8
#define _g_sens  16384 // 1g = 9.81m/s^2 = 16384 LSB

/* function */
void motion_sensor_check_softwave_ready(void);
void acc_sensor_pwr_up(void);
void acc_sensor_pwr_down(void);
void acc_sensor_init(void);
void acc_sensor_interrupt_initial(void);
void acc_sensor_wuf_initial(void);
void acc_sensor_clear_interrupt_status(void);
void acc_sensor_read_status(void);
int acc_sensor_get_acc();

#endif
