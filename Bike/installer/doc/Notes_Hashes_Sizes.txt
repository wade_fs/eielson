MPLAB X IDE v5.15 features:


* AVR devices enabled for Production
* Improved debugWire experience for AVR debugging
* Added support for SAM Xplained boards and Atmel-Ice tools
* Added Config bit Generation for many AVRs using XC8
* SEGGER J-Link distributed with the IDE (Plugin not required) 
* Bug fixes


md5 checksums:
----------------
4d46a5dc0d4c8a3bc86f6da8616a6c21  linux/MPLABX-v5.15-linux-installer.tar
8e437994f7c783848ac288a833609e62  mac/MPLABX-v5.15-osx-installer.dmg
ac7859df7deeb4cb83ec512da07af769  windows/MPLABX-v5.15-windows-installer.exe

Size in bytes:
----------------
    847,298,560  Feb 20 14:50  linux/MPLABX-v5.15-linux-installer.tar
    743,419,241  Feb 20 14:50  mac/MPLABX-v5.15-osx-installer.dmg
    896,064,424  Feb 20 14:30  windows/MPLABX-v5.15-windows-installer.exe
