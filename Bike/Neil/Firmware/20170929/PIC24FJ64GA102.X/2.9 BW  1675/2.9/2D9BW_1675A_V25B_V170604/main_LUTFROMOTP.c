//备注:用Source Insight软件浏览程序效果最佳
 
//	  
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx




//xx Includes xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#include "msp430x22x4.h"
#include "waveinit.h"
#include "image.h"



//xx Private macro xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#define SDA_H     	(P1OUT |=BIT7)						// P1.7
#define SDA_L     	(P1OUT &=~BIT7)
#define SCLK_H    	(P1OUT |=BIT6)  					// P1.6
#define SCLK_L   	(P1OUT &=~BIT6) 
#define nCS_H     	(P1OUT |=BIT5)						// P1.5
#define nCS_L     	(P1OUT &=~BIT5)
#define nDC_H     	(P1OUT |=BIT4)						// P1.4
#define nDC_L     	(P1OUT &=~BIT4)
#define nRST_H     	(P1OUT |=BIT3)						// P1.3 
#define nRST_L     	(P1OUT &=~BIT3)

#define R_SDA         0x80 	//P1.7


#define DELAY_TIME	3									// 图片显示完停留时间(单位:秒)

#define MODE1  											// panel scan direction



#define PIC_BLACK		252
#define PIC_WHITE		255
#define PIC_A			1
#define PIC_B   	    2
#define PIC_HLINE		3
#define PIC_VLINE	    4
#define PIC_C			5
#define PIC_D   	    6
#define PIC_E		    7
#define PIC_F	        8
/*
#define PIC_SUNING		3
#define PIC_SHOUSI 		4
#define PIC_BAZHE 		5
#define PIC_CHESSBOARD		6
#define PIC_JIFEN		7
#define PIC_FU			8
#define PIC_GC4			9
#define PIC_SAVE		10
#define PIC_HK                  13
#define PIC_T                   14
#define PIC_P                   15
#define PIC_Z                   16
#define PIC_COOK                17
*/
//xx Private functions xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
unsigned char num1;
unsigned char tempvalue;
unsigned char ByteA,ByteB,ByteC,ByteD,ByteE,ByteF,ByteG,ByteH;

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   延时函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void DELAY_100nS(int delaytime)   						// 30us 
{
	int i,j;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<1;j++);
}

void DELAY_mS(int delaytime)    						// 1ms
{
	int i,j;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<200;j++);
}

void DELAY_S(int delaytime)     						// 1s
{
	int i,j,k;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<1000;j++)
			for(k=0;k<200;k++);
}



//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   电子纸驱动操作函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// 复位 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void RESET()
{
	nRST_L;
	DELAY_mS(1);								
 	nRST_H;
  	DELAY_mS(1);
}

// 读忙 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void READBUSY()
{
  	while(1)
  	{
   		_NOP();
   	 	if((P1IN & 0x04)==0)
    		break;
  	}      
}

// 写命令 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void SPI4W_WRITECOM(unsigned char INIT_COM)
{
	unsigned char TEMPCOM;
	unsigned char scnt;
   P1DIR |= R_SDA;
	TEMPCOM=INIT_COM;
	nCS_H;
	nCS_L;
	SCLK_L;
	nDC_L;
	for(scnt=0;scnt<8;scnt++)
	{
		if(TEMPCOM&0x80)
			SDA_H;
		else
			SDA_L;
		SCLK_H;  
		SCLK_L;  
		TEMPCOM=TEMPCOM<<1;
	}
	//nCS_H;	
}

// 写数据 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void SPI4W_WRITEDATA(unsigned char INIT_DATA)
{
	unsigned char TEMPCOM;
	unsigned char scnt;
	P1DIR |= R_SDA;

	TEMPCOM=INIT_DATA;
	//nCS_H;
	//nCS_L;
	SCLK_L;
	nDC_H;
	for(scnt=0;scnt<8;scnt++)
	{
		if(TEMPCOM&0x80)
		SDA_H;
		else
		SDA_L;
		SCLK_H;  
		SCLK_L;  
		TEMPCOM=TEMPCOM<<1;
	}
	//nCS_H;	
}


// 读数据xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 unsigned char SPI4W_READDATA1()
 {
	 P1DIR &=~ R_SDA;

	 unsigned char scnt,temp;
         temp=0;
         
	// nCS_H;
	// nCS_L;
	// SCLK_H;
	 nDC_H;
	 for(scnt=0;scnt<8;scnt++)
	 {
		 	  
		 SCLK_L;  
		 if(P1IN&R_SDA)
		 temp=(temp<<1)|0x01;
		 else
		 temp=temp<<1;		
		 SCLK_H;	  
		 SCLK_L;  
	 }
	// nCS_H;  
         tempvalue= temp;
	 return temp;
 }

// 写波形数据表 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void WRITE_LUT()
{
	unsigned char i;
	
	SPI4W_WRITECOM(0x32);			// write LUT register
	for(i=0;i<70;i++)			// write LUT register with 29bytes instead of 30bytes 2D13
        SPI4W_WRITEDATA(init_datac[i]);
	nCS_H;
}

//void WriteFlow()
//{
//	unsigned char i;
//	
//	SPI4W_WRITECOM(0x24);		// write RAM
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data0[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data1[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data2[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data3[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data4[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data5[i]);
//	for(i=0;i<32;i++)	
//        SPI4W_WRITEDATA(init_data6[i]);
//	for(i=0;i<18;i++)	
//        SPI4W_WRITEDATA(init_data7[i]);
//}

// 电子纸驱动初始化 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void INIT_SSD1673()
{       SPI4W_WRITECOM(0x74);
        SPI4W_WRITEDATA(0x54);
        SPI4W_WRITECOM(0x75);
        SPI4W_WRITEDATA(0x3b);
	SPI4W_WRITECOM(0x01);		// Set MUX as 296
	SPI4W_WRITEDATA(0xD3);
	SPI4W_WRITEDATA(0x00);
        SPI4W_WRITEDATA(0x00);
	//SPI4W_WRITECOM(0x3A);		// Set 100Hz
      //  SPI4W_WRITEDATA(0X0F);          //Set 50HZ
         //SPI4W_WRITEDATA(0X01);          //Set 80HZ
       //SPI4W_WRITEDATA(0x25);         // Set 100Hz
       // SPI4W_WRITEDATA(0x18);         // Set 120Hz
       // SPI4W_WRITEDATA(0x07);         // Set 150Hz
//	SPI4W_WRITECOM(0x3B);		// Set 100Hz
      //  SPI4W_WRITEDATA(0X0C);          //Set 50HZ
        //SPI4W_WRITEDATA(0X08);          //Set 80HZ
      // SPI4W_WRITEDATA(0x06);         // Set 100Hz
	//SPI4W_WRITEDATA(0x05);         // Set 120Hz         
	SPI4W_WRITECOM(0x11);		// data enter mode
	SPI4W_WRITEDATA(0x01);
	SPI4W_WRITECOM(0x44);		// set RAM x address start/end, in page 36
	SPI4W_WRITEDATA(0x00);		// RAM x address start at 00h;
	SPI4W_WRITEDATA(0x0c);		// RAM x address end at 0fh(15+1)*8->128 
	SPI4W_WRITECOM(0x45);		// set RAM y address start/end, in page 37
	SPI4W_WRITEDATA(0xD3);		// RAM y address start at 127h;
	SPI4W_WRITEDATA(0x00);		
        SPI4W_WRITEDATA(0x00);		// RAM y address end at 00h;
	SPI4W_WRITEDATA(0x00);		
	//SPI4W_WRITECOM(0x04);		// set VSH,VSL value
      //  SPI4W_WRITEDATA(0x46);		// 	    2D9  16v
	//SPI4W_WRITEDATA(0x41);		// 	    2D9  15v
        
       // SPI4W_WRITEDATA(0xbf);		//	    2D13   7.3v 
         //SPI4W_WRITEDATA(0xc6);		//	    2D13   7.8v 
        //SPI4W_WRITEDATA(0xc1);		//	    2D13   7.5v 
          //SPI4W_WRITEDATA(0XC0);                //          2D13   7.4v 
         //SPI4W_WRITEDATA(0XBE);                //          2D13   7.2v
          //SPI4W_WRITEDATA(0XBC);                //          2D13   7.0v
          // SPI4W_WRITEDATA(0XBA);                //          2D13   6.8v
      //    SPI4W_WRITEDATA(0x96);		//	    2D9   3.8v 
       //  SPI4W_WRITEDATA(0xbf);		//	    2D13   7.3v 
       // SPI4W_WRITEDATA(0xb8);		//	    2D13   6.6v 
       // SPI4W_WRITEDATA(0xb6);		//	    2D13   6.4v 
       // SPI4W_WRITEDATA(0xb4);		//	    2D13   6.2v 
        //SPI4W_WRITEDATA(0xb2);		//	    2D13   6v 
         // SPI4W_WRITEDATA(0xB0);		//	    2D13   5.8v  
       // SPI4W_WRITEDATA(0xAE);		//	    2D13   5.6v  
      // SPI4W_WRITEDATA(0xa8);		//	    2D9   5v 
      //  SPI4W_WRITEDATA(0xA4);		//	    2D9   4.6v 
       // SPI4W_WRITEDATA(0xA2);		//	    2D9   4.4v 
     // SPI4W_WRITEDATA(0xa0);		//	    2D9   4.2v 
     // SPI4W_WRITEDATA(0x9C);		//	    2D9   3.8v 
      //  SPI4W_WRITEDATA(0x96);		//	    2D9   3.8v 
       // SPI4W_WRITEDATA(0x32);		//	    2D9  -15v
       //SPI4W_WRITEDATA(0x36);		//	    2D9  -16v
  /*      
		SPI4W_WRITECOM(0x22);
		SPI4W_WRITEDATA(0xc0);
		SPI4W_WRITECOM(0x20);
		READBUSY();
        SPI4W_WRITECOM(0x29);
		SPI4W_WRITEDATA(0x49);
		//DELAY_S(10);
		READBUSY();
		SPI4W_WRITECOM(0x28);
		READBUSY();
		SPI4W_WRITECOM(0x2D);
		ByteA = SPI4W_READDATA1(); 
		ByteB = SPI4W_READDATA1();
		ByteC = SPI4W_READDATA1();
		ByteD = SPI4W_READDATA1();
		ByteE = SPI4W_READDATA1();
		ByteF = SPI4W_READDATA1();
		ByteG = SPI4W_READDATA1();
		ByteH = SPI4W_READDATA1();
		nCS_H;
		SPI4W_WRITECOM(0x22);
		SPI4W_WRITEDATA(0x03);
		SPI4W_WRITECOM(0x20);
		READBUSY();
		*/
       //	SPI4W_WRITECOM(0x2C);           // vcom
       // SPI4W_WRITEDATA(0x88);           //-3.4V
      // SPI4W_WRITEDATA(0x6f);           //-2.6V
      //  SPI4W_WRITEDATA(0x6c);           //-2.6V
   	//SPI4W_WRITEDATA(0x68);           //-2.6V
     // SPI4W_WRITEDATA(0x50);           //-2V
        
	SPI4W_WRITECOM(0x18);		// data enter mode
	SPI4W_WRITEDATA(0x80);
	SPI4W_WRITECOM(0x3C);		// board
	SPI4W_WRITEDATA(0x00);		//GS1-->GS1
	//WRITE_LUT();
	nCS_H;
}

// 入深度睡眠 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void enterdeepsleep()
{
  	SPI4W_WRITECOM(0x10);
  	SPI4W_WRITEDATA(0x01);
	nCS_H;
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   图片显示函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void dis_img(unsigned char num,unsigned char num2)
{
	unsigned int row, col;
	unsigned int pcnt;
/***************************************************bw image****************************************************/
      //  SPI4W_WRITECOM(0x22);
	//SPI4W_WRITEDATA(0xC0);		//Analog On
	//SPI4W_WRITECOM(0x20);
      //   SPI4W_WRITECOM(0x37);    	//Waveform ID register
	//    SPI4W_WRITEDATA(0x00);		//ByteA
	//    SPI4W_WRITEDATA(0x00);		//ByteB         DM[7:0]
	//    SPI4W_WRITEDATA(0x00);		//ByteC         DM[[15:8]
	//    SPI4W_WRITEDATA(0x00);		//ByteD         DM[[23:16]
	//    SPI4W_WRITEDATA(0x40);		//ByteE         PP1,PP2,00 000,DM[[24]      [PP2=1 for pingpong]
      
          SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;
        SPI4W_WRITEDATA(0x00);
        SPI4W_WRITECOM(0x4F);		// set RAM y address count to 296;	2D9
        SPI4W_WRITEDATA(0xd3);
        SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x24);
	
         pcnt = 0;											// 复位或保存提示字节序号
	for(col=0; col<212; col++)							// 总共172列		// send 128x252bits ram 2D13
	{
		for(row=0; row<13; row++)						// 总共72行，每个像素2bit,即 72/4 字节
		{
				switch (num)
				{
				case PIC_A:
					//SPI4W_WRITEDATA(gImage_YB02[pcnt]);
                                      SPI4W_WRITEDATA(gImage_test_bw[pcnt]);
                                      
					break;	
                                        
                                        
                                  case PIC_B:
					SPI4W_WRITEDATA(gImage_mb_bw[pcnt]);
                                        break;
                                  case PIC_C:
                                       // SPI4W_WRITEDATA(gImage_anerle_bw[pcnt]);   
                                       SPI4W_WRITEDATA(0xff);
                                       break;
                                  case PIC_D:
                                      //  SPI4W_WRITEDATA(gImage_test_bw[pcnt]);
                                        break;
                                  case PIC_VLINE:
                                        if(col>106)
					  SPI4W_WRITEDATA(0xff);
                                        else
                                          SPI4W_WRITEDATA(0x00);
					break;
                                        
                                       case PIC_HLINE:
                                          if(row>5)
					  SPI4W_WRITEDATA(0xff);
                                       //  else if(row==7)
					//  SPI4W_WRITEDATA(0x0f);
                                         else
                                          SPI4W_WRITEDATA(0x00);
                                       /* if((row%2)<1)
					  SPI4W_WRITEDATA(0xff);
                   
                                          else
                                          SPI4W_WRITEDATA(0x00);*/
					break;	
                                       /* if((row%2)<1)
					  SPI4W_WRITEDATA(0xff);
                   
                                          else
                                          SPI4W_WRITEDATA(0x00);*/
					break;	
					
				case PIC_WHITE:
					SPI4W_WRITEDATA(0xff);
					break;	

				case PIC_BLACK:
					SPI4W_WRITEDATA(0x00);
					break;	
			   	default:
					break;
				}
			pcnt++;
		}
	}
        
  /* if(num==PIC_BLACK||num==PIC_WHITE||num==PIC_HLINE||num==PIC_VLINE)
        {
      //  SPI4W_WRITECOM(0x21);
     //   SPI4W_WRITEDATA(0x04);  
        SPI4W_WRITECOM(0x22);
        SPI4W_WRITEDATA(0xc7);
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY(); 
        return;
        }
     */
              
/***************************************************bw image****************************************************/
   
         SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;
        SPI4W_WRITEDATA(0x00);
        SPI4W_WRITECOM(0x4F);		// set RAM y address count to 296;	2D9
        SPI4W_WRITEDATA(0xd3);
        SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x26);
	
	pcnt = 0;											// 复位或保存提示字节序号
	for(col=0; col<212; col++)							// 总共172列		// send 128x252bits ram 2D13
	{
		for(row=0; row<13; row++)						// 总共72行，每个像素2bit,即 72/4 字节
		{
				switch (num2)
				{
				case PIC_A:
					//SPI4W_WRITEDATA(gImage_YB02[pcnt]);
                                      SPI4W_WRITEDATA(gImage_test_bw[pcnt]);
                                      
					break;	
                                        
                                        
                                  case PIC_B:
					SPI4W_WRITEDATA(gImage_mb_bw[pcnt]);
                                        break;
                                  case PIC_C:
                                       // SPI4W_WRITEDATA(gImage_anerle_bw[pcnt]);   
                                       SPI4W_WRITEDATA(0xff);
                                       break;
                                  case PIC_D:
                                      //  SPI4W_WRITEDATA(gImage_ceshi_bw[pcnt]);
                                        break;
                                  case PIC_VLINE:
                                        if(col>106)
					  SPI4W_WRITEDATA(0xff);
                                        else
                                          SPI4W_WRITEDATA(0x00);
					break;
                                        
                                       case PIC_HLINE:
                                         if(row>5)
					  SPI4W_WRITEDATA(0xff);
                                       //  else if(row==7)
					//  SPI4W_WRITEDATA(0x0f);
                                         else
                                          SPI4W_WRITEDATA(0x00);
                                       /* if((row%2)<1)
					  SPI4W_WRITEDATA(0xff);
                   
                                          else
                                          SPI4W_WRITEDATA(0x00);*/
					break;	
                                       /* if((row%2)<1)
					  SPI4W_WRITEDATA(0xff);
                   
                                          else
                                          SPI4W_WRITEDATA(0x00);*/
				//	break;	
					
				case PIC_WHITE:
					SPI4W_WRITEDATA(0xff);
					break;	

				case PIC_BLACK:
					SPI4W_WRITEDATA(0x00);
					break;	
			   	default:
					break;
				}
			pcnt++;
		}
	}
   
        
        SPI4W_WRITECOM(0x22);
	SPI4W_WRITEDATA(0xF7);		//Load LUT from MCU(0x32), Display update
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
	nCS_H;

}







//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   主函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void main( void )
{
	//int i;
        

	WDTCTL = WDTPW + WDTHOLD;							// Stop watchdog timer to prevent time out reset
	BCSCTL1 = CALBC1_1MHZ; 								// set DCO frequency 1MHZ
	DCOCTL = CALDCO_1MHZ; 

	P1DIR |=0xF8;  										// set P1.3~7 output
	P3OUT = 0XFF;
        P3DIR = 0xff;										// p3.4,p3.5 设为输入

	num1=0;	
	RESET();											// 电子纸控制器复位
    	SPI4W_WRITECOM(0x12);			//SWRESET
	READBUSY();
	
	
	INIT_SSD1673();
	
  /*  	
		SPI4W_WRITECOM(0x2D);
		ByteA = SPI4W_READDATA1(); 
		ByteB = SPI4W_READDATA1();
		ByteC = SPI4W_READDATA1();
		ByteD = SPI4W_READDATA1();
		ByteE = SPI4W_READDATA1();
		ByteF = SPI4W_READDATA1();
		ByteG = SPI4W_READDATA1();
		ByteH = SPI4W_READDATA1();
		nCS_H;
        */
    	//dis_img(PIC_WHITE);



	//while(1)
	//{
            	/*dis_img(PIC_BLACK,PIC_WHITE);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
      	        dis_img(PIC_WHITE,PIC_BLACK);
		DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                dis_img(PIC_BLACK,PIC_WHITE);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
      	        dis_img(PIC_WHITE,PIC_BLACK);
		DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);
                DELAY_S(DELAY_TIME);*/
                dis_img(PIC_BLACK,PIC_WHITE);
                DELAY_S(2);
				
								dis_img(PIC_WHITE,PIC_BLACK);
				DELAY_S(2);
               dis_img(PIC_VLINE,PIC_WHITE);
		DELAY_S(3);
		dis_img(PIC_HLINE,PIC_VLINE);
		DELAY_S(3);
	//	dis_img(PIC_A,PIC_BLACK);
		
             //   DELAY_S(2);
                  //   enterdeepsleep();
                        
            //    dis_img(PIC_C,PIC_A);
	      
              
             //   DELAY_S(DELAY_TIME);
          
                        
	//}

}
