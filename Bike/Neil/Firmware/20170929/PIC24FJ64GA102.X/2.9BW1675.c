//备注:用Source Insight软件浏览程序效果最佳
 
//  
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx




//xx Includes xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//#include "msp430x22x4.h"
//#include "waveinit.h"
//#include "image.h"
#include "mcc_generated_files/mcc.h"
#include <stdio.h>                  // ??? itoa() ??
//#include "lcd.h"                    // LCD ??????
//#include "st7066.h"
#include "gimage.h"
#include "def.h"
//#include "image_bw.h"
#include "waveinit_new.h"
//#include "image.h"
extern unsigned int  BatteryLevel;
extern unsigned  int showBattery(void);
unsigned char rs232_s=0;

void EPD_main(void);

#define EPD_1675 1
//define  EPD_INIT 1
//#define EPD_1675_SPEED 1


//xx Private macro xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/*
#define SDA_H     (P1OUT |=BIT7)// P1.7
#define SDA_L     (P1OUT &=~BIT7)
#define SCLK_H    (P1OUT |=BIT6)  // P1.6
#define SCLK_L   (P1OUT &=~BIT6) 
#define nCS_H     (P1OUT |=BIT5)// P1.5
#define nCS_L     (P1OUT &=~BIT5)
#define nDC_H     (P1OUT |=BIT4)// P1.4
#define nDC_L     (P1OUT &=~BIT4)
#define nRST_H     (P1OUT |=BIT3)// P1.3 
#define nRST_L     (P1OUT &=~BIT3)
*/

#define SDA_L	  _LATB5 = 0   // SPI_DO  D1
#define SDA_H	  _LATB5 = 1

#define SCLK_L	  _LATB4 = 0 // SPI_CLK1 D0
#define SCLK_H	  _LATB4 = 1

#define nCS_L	  _LATB3 = 0  // SPI_SS1
#define nCS_H	  _LATB3 = 1

#define nDC_L 	  _LATB2 = 0  // SPI_DI1
#define nDC_H 	  _LATB2 = 1

#define nRST_L	  _LATA0 = 0
#define nRST_H	  _LATA0 = 1


#define IF_SDA_H        (P1IN &BIT7==0X80)
#define R_SDA         0x80 //P1.7
#define DELAY_TIME 2// 图片显示完停留时间(单位:秒)

#define MODE1  // panel scan direction



#define PIC_BLACK 252
#define PIC_WHITE 255
#define PIC_A       1
#define PIC_B       2
#define PIC_HLINE   3
#define PIC_VLINE    4
#define PIC_C  5
#define PIC_D       6
#define PIC_E    7
#define PIC_F        8
/*
#define PIC_SUNING3
#define PIC_SHOUSI 4
#define PIC_BAZHE 5
#define PIC_CHESSBOARD6
#define PIC_JIFEN7
#define PIC_FU8
#define PIC_GC49
#define PIC_SAVE10
#define PIC_HK                  13
#define PIC_T                   14
#define PIC_P                   15
#define PIC_Z                   16
#define PIC_COOK                17
*/
//xx Private functions xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
unsigned int tempcode;
unsigned char tempvalue;

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   延时函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void DELAY_100nS(int delaytime)   // 30us 
{
int i,j;

for(i=0;i<delaytime;i++)
for(j=0;j<1;j++);
}

void DELAY_mS(int delaytime)    // 1ms
{
int i,j;

for(i=0;i<delaytime;i++)
for(j=0;j<200;j++);
}

void DELAY_S(int delaytime)     // 1s
{
int i,j,k;

for(i=0;i<delaytime;i++)
for(j=0;j<1000;j++)
for(k=0;k<200;k++);
}



//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   电子纸驱动操作函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// 复位 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void RESET()
{
nRST_L;
DELAY_mS(1);
 nRST_H;
  DELAY_mS(1);
}

// 读忙 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void READBUSY()
{
    unsigned char busy;
  while(1)
  {
//   _NOP();
       busy=0;
   // if((P1IN & 0x04)==0)
  if(((  PORTAbits.RA1))==0)
    break;
  }      
}

// 写命令 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void SPI4W_WRITECOM(unsigned char INIT_COM)
{
  unsigned char TEMPCOM;
  unsigned char scnt;

  TEMPCOM=INIT_COM;
    SCLK_H;  
 nDC_L;
  nCS_H;
  nCS_L;
  for(scnt=0;scnt<8;scnt++)
  {
    if(TEMPCOM&0x80)
      SDA_H;
    else
      SDA_L;
    SCLK_L;  
    SCLK_H;  
    TEMPCOM=TEMPCOM<<1;
  }
  nCS_H;  
}

// 写数据 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void SPI4W_WRITEDATA(unsigned char INIT_DATA)
{
  unsigned char TEMPCOM;
  unsigned char scnt;

  TEMPCOM=INIT_DATA;
    SCLK_H;  
 nDC_H;
  nCS_H;
  nCS_L;
  for(scnt=0;scnt<8;scnt++)
  {
    if(TEMPCOM&0x80)
      SDA_H;
    else
      SDA_L;
    SCLK_L;  
    SCLK_H;  
    TEMPCOM=TEMPCOM<<1;
  }
  nCS_H;  
}

// 读数据xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 /*
 unsigned char SPI4W_READDATA1()
 {
 P1DIR &=~ R_SDA;

 unsigned char scnt,temp;
         temp=0;
         
 nDC_H;
 for(scnt=0;scnt<8;scnt++)
 {
   
 SCLK_L;  
 if(P1IN&R_SDA)
 temp=(temp<<1)|0x01;
 else
 temp=temp<<1;
 SCLK_H;  
 SCLK_L;  
 }
         tempvalue= temp;
 return temp;
 }
*/

// 电子纸驱动初始化 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void INIT_SSD1675()
{       SPI4W_WRITECOM(0x74);
        SPI4W_WRITEDATA(0x54);
        SPI4W_WRITECOM(0x7E);
        SPI4W_WRITEDATA(0x3B);
SPI4W_WRITECOM(0x01);// Set MUX as 296
SPI4W_WRITEDATA(0x27);
SPI4W_WRITEDATA(0x01);
        SPI4W_WRITEDATA(0x00);
        
        SPI4W_WRITECOM(0x11);// data enter mode
SPI4W_WRITEDATA(0x01);
SPI4W_WRITECOM(0x44);// set RAM x address start/end, in page 36
SPI4W_WRITEDATA(0x00);// RAM x address start at 00h;
SPI4W_WRITEDATA(0x0f);// RAM x address end at 0fh(15+1)*8->128 
SPI4W_WRITECOM(0x45);// set RAM y address start/end, in page 37
SPI4W_WRITEDATA(0x27);// RAM y address start at 127h;
SPI4W_WRITEDATA(0x01);
        SPI4W_WRITEDATA(0x00);// RAM y address end at 00h;
SPI4W_WRITEDATA(0x00);
        
        
SPI4W_WRITECOM(0x3C);// board
SPI4W_WRITEDATA(0x33);//GS1-->GS1
        
        
         unsigned char temp1,temp2;
        
        SPI4W_WRITECOM(0x18);        //  温度sensor 控制
        SPI4W_WRITEDATA(0x80);        //  内部温度sensor 
        SPI4W_WRITECOM(0x22);
   SPI4W_WRITEDATA(0XB1);//Load Temperature and waveform setting.
   SPI4W_WRITECOM(0x20); 
        nCS_H;  
   READBUSY();
   
 //Jason  SPI4W_WRITECOM(0x1B);
 //Jason   temp1=SPI4W_READDATA1(); 
 //Jason  temp2=SPI4W_READDATA1();
   
 //nCS_H;  
        
//WRITE_LUT();
}

// 入深度睡眠 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void enterdeepsleep()
{
  SPI4W_WRITECOM(0x10);
  SPI4W_WRITEDATA(0x01);
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   图片显示函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

void dis_img2(unsigned char num)
{
	unsigned int row, col;
	unsigned int pcnt;

	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 296;
	SPI4W_WRITEDATA(0x27);
    SPI4W_WRITEDATA(0x01);
        
	SPI4W_WRITECOM(0x24);
	
	pcnt = 0;											// 复位或保存提示字节序号
	for(col=0; col<T_COL; col++)							// 总共172列
	{
		for(row=0; row<T_ROW; row++)						// 总共72行，每个像素2bit,即 72/4 字节
		{
		   SPI4W_WRITEDATA(gImage[pcnt]);
			pcnt++;
		}
	}

	
            SPI4W_WRITECOM(0x21);//Set display control 1,Bypass RED RAM,as 0
            SPI4W_WRITEDATA(0x40);

	SPI4W_WRITECOM(0x22);
 	SPI4W_WRITEDATA(0xC7);		//Load LUT from MCU(0x32), Display update
//	SPI4W_WRITEDATA(0xF7);		//TS operation, then Load LUT from OTP, Display update
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
  //   enterdeepsleep();
	
}

void dis_img(unsigned char num)
{
unsigned int row, col;
unsigned int pcnt;
/***************************************************bw image****************************************************/
        SPI4W_WRITECOM(0x4E);// set RAM x address count to 0;
        SPI4W_WRITEDATA(0x00);
        SPI4W_WRITECOM(0x4F);// set RAM y address count to 296;2D9
        SPI4W_WRITEDATA(0x27);
        SPI4W_WRITEDATA(0x01);
SPI4W_WRITECOM(0x24);

pcnt = 0;// 复位或保存提示字节序号
for(col=0; col<296; col++)// 总共172列// send 128x252bits ram 2D13
{
for(row=0; row<16; row++)// 总共72行，每个像素2bit,即 72/4 字节
{
switch (num)
{
case PIC_A:
SPI4W_WRITEDATA( gImage[pcnt]);
                                  
                                      
break;
                                        
                                        
                               
                                  case PIC_VLINE:
                                        if(col<148)
  SPI4W_WRITEDATA(0xff);
                                        else
                                          SPI4W_WRITEDATA(0x00);
break;
                                        
                                       case PIC_HLINE:
                                          if(row<8)
  SPI4W_WRITEDATA(0xff);
                                         else if(row==8)
  SPI4W_WRITEDATA(0x0f);
                                         else
                                          SPI4W_WRITEDATA(0x00);
                                      
break;

case PIC_WHITE:
SPI4W_WRITEDATA(0xff);
break;

case PIC_BLACK:
SPI4W_WRITEDATA(0x00);
break;
   default:
break;
}
pcnt++;
}
}
  
  
            SPI4W_WRITECOM(0x21);//Set display control 1,Bypass RED RAM,as 0
            SPI4W_WRITEDATA(0x40);

            SPI4W_WRITECOM(0x22);
            SPI4W_WRITEDATA(0xC7);//Load LUT from MCU(0x32)
            SPI4W_WRITECOM(0x20);
            DELAY_mS(1);
            READBUSY();
   
        
        
        
        
}   
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   主函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
////////////////// Jason Added

 uint8_t SPI1_8bit(uint8_t data);
 unsigned char EPD_index;
extern void EPD_print(void);
extern void print_letter(int x, int y, char letter);
unsigned char s_CurX=0; 
unsigned char s_CurY=0; 
unsigned char t_CurX=0; 
unsigned char t_CurY=0; 
unsigned char e_CurX=0; 
unsigned char EPD_buf[64];
extern void   EPD_clear(void)
{
    memset(gImage,EPD_CLEAR,T_LCD_SIZE);
    t_CurY=0;
    t_CurX=0;
    e_CurX=0;
	EPD_index=0;
}
void EPD_Set_Cursor(unsigned char CurY, unsigned char CurX)
{
    if(CurY>MAX_EPD_ROW)
        return ;
    t_CurY=CurY;
    t_CurX=CurX;
    e_CurX=CurX;
    
}  


void EPD_putcLCD(char LCD_Char)
{
   EPD_buf[e_CurX]= LCD_Char;
  
    e_CurX++;
    if (e_CurX>MAX_EPD_COL)
    {
     
       EPD_print();
       EPD_Set_Cursor(++EPD_index,0);
       e_CurX=0;
    }
    
}
void EPD_flash(void)
{
  //   EPD_printf(e_CurX, EPD_buf);
  //  dis_img(PIC_WHITE);
     dis_img2(PIC_WHITE);
 //    e_CurX=0;
 //    enterdeepsleep();
   //   SPI1_8bit(0xFC);
}
void EPD_print(void)
{
	if(e_CurX)
    EPD_printf(e_CurX, EPD_buf);
  
}
  void EPD_printf(unsigned char cc, unsigned char *buf)
 {
     unsigned char i;
    
     for (i=0;i<cc;i++)
     {
   
       print_letter16(i,t_CurY, buf[i]);      
     }
 }
#ifdef EPDD
void buttonUp(void)
#else
void buttonUp_EPD(void)
#endif
{
}
#ifdef EPDD
void buttonDown(void)
#else
void buttonDown_EPD(void)
#endif
{
   
}

 
 

void showTitle_EPD(void)
{
 
   // unsigned char cc=(*NVMDataPointer & 0x00FF);
    
    memset(gImage,EPD_CLEAR,T_LCD_SIZE);
    print_letter16(2,3,'E');
    print_letter16(3,3,'P');
    print_letter16(4,3,'D');
    print_letter16(5,3,'-');
    print_letter16(6,3,'D'); 
    print_letter16(7,3,'e');
    print_letter16(8,3,'m');
    print_letter16(9,3,'o');
    print_letter16(10,3,' ');
    print_letter16(11,3,'V');
    print_letter16(12,3,'0');
    print_letter16(13,3,'9');
    print_letter16(14,3,'1');
    print_letter16(15,3,'3');
  
    
  //  dis_img2(PIC_WHITE);
     EPD_print();
     EPD_flash();
     
//enterdeepsleep();
}
void log_data_EPD(unsigned char cc)
{
 
   
   
}    
void store_data_EPD(unsigned char cc)
{
  
   
} 

  uint8_t SPI1_8bit(uint8_t data)
{
    // Clear the Write Collision flag, to allow writing
   // SSP1CON1bits.WCOL = 0;

    SPI1BUF = data;

    return (SPI1BUF);
}
  
  void EPD_init(void)
  {
      IO_RA4_SetLow(); //
      	RESET();											// 电子纸控制器复位
    	SPI4W_WRITECOM(0x12);			//SWRESET
	READBUSY();
#ifdef EPD_1675	
	INIT_SSD1675();
#else    
	INIT_SSD1608();	
   SPI4W_WRITECOM(0x21);								//
   SPI4W_WRITEDATA(0x83);
    dis_img(PIC_WHITE);
    SPI4W_WRITECOM(0x21);								//
    SPI4W_WRITEDATA(0x03);
#endif   
 //   SPI1_8bit(0xFC);
     IO_RA4_SetHigh(); //
 }    
   void EPD_Show(void)
  {
       IO_RA4_SetLow(); //
      	RESET();											// 电子纸控制器复位
    	SPI4W_WRITECOM(0x12);			//SWRESET
	READBUSY();
#ifdef EPD_1675	
	INIT_SSD1675();
#else    
	INIT_SSD1608();	
   SPI4W_WRITECOM(0x21);								//
   SPI4W_WRITEDATA(0x83);
    dis_img(PIC_WHITE);
    SPI4W_WRITECOM(0x21);								//
    SPI4W_WRITEDATA(0x03);
#endif   

     showTitle_EPD();
     UART1_Write( 0xAA);
  //    SPI1_8bit(0xFD);
      IO_RA4_SetHigh(); //
 } 
   
   void store_data (unsigned char cc)
  {
      switch(rs232_s)
      {
          case 0:
              if (cc==0xFC)
              {
                 rs232_s=1;
                
              }
              else
              {
                  if(!(cc & 0x80))
                   EPD_putcLCD(cc);
              }    
             break; 
          case 1:
          {
              if(cc=='@')
              {
                  EPD_print();
              }
              else if (cc=='|')
              {
                  EPD_flash();
              }
              else if (cc=='#')
              {
                  EPD_clear();
              }
               else if (cc=='I')
              {
                  EPD_init();
              }
              else if (cc=='S')
              {
                  EPD_Show();
              }
              else if (cc=='$')
              {
                  rs232_s=2;
                  break;
              }
              else
              {
                    EPD_putcLCD('~');
                    EPD_putcLCD(cc);
              }
               rs232_s=0;
          }
          break;
          case 2:
              s_CurY=cc;
              rs232_s=3;
              break;
          case 3:
              s_CurX=cc;
              EPD_Set_Cursor(s_CurY, s_CurX);
              rs232_s=0;
              break;
                  
      }
  }
unsigned char spi1_recv=0 ;
//#define EPD_INIT 1
  void EPD_main(void)
  {	
      unsigned int i,k;
#ifdef EPD_INIT
      	RESET();											// 电子纸控制器复位
    	SPI4W_WRITECOM(0x12);			//SWRESET
	READBUSY();
#ifdef EPD_1675	
	INIT_SSD1675();
#else    
	INIT_SSD1608();	
   SPI4W_WRITECOM(0x21);								//
   SPI4W_WRITEDATA(0x83);
    dis_img(PIC_WHITE);
    SPI4W_WRITECOM(0x21);								//
    SPI4W_WRITEDATA(0x03);
#endif   
UART1_Write( 0xAA);
     showTitle_EPD();
#endif   
   IEC0bits.SPI1IE = 1;
   SPI1_8bit(0xFD);
      while(1)
	{
       //   UART1_Write( 0xAA);
    //    if(spi1_recv==1)
      //  {
     //       spi1_recv=0;
      //      IO_RA4_Toggle(); //
     //   } 
      if ( UART1_ReceiveBufferIsEmpty ()==false)
		{
            unsigned char rc;
           IO_RA4_Toggle();
            rc=UART1_Read();
           // UART1_Write( rc);
          //  i=1;
      	    store_data (rc);
         }
      }
}		
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   主函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void mainx( void )
{
//WDTCTL = WDTPW + WDTHOLD;// Stop watchdog timer to prevent time out reset
//BCSCTL1 = CALBC1_1MHZ; // set DCO frequency 1MHZ
//DCOCTL = CALDCO_1MHZ; 
//P1DIR |=0x78;  // set P1.3~7 output
//P3OUT = 0XFF;
//P3DIR = 0xff;// p3.4,p3.5 设为输入


RESET();// 电子纸控制器复位
SPI4W_WRITECOM(0x12);//SWRESET
READBUSY();


INIT_SSD1675();                
dis_img(PIC_A); 
enterdeepsleep();
}
