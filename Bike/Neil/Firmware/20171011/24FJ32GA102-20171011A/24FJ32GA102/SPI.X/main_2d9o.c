//备注:用Source Insight软件浏览程序效果最佳

//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//	Prorgam:           EPD2.9_Demo
//	Author:         
//	Date:              2014.04.16.
//	Rev:               1.0
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//	Rev History     Date            Modification
//	  1.0			2014.04.16.     Create     
//		
//	  
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx




//xx Includes xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//#include "msp430x22x4.h"
#include "mcc_generated_files/mcc.h"
#include <stdio.h>                  // ??? itoa() ??
//#include "lcd.h"                    // LCD ??????
//#include "st7066.h"
#include "gimage.h"
#include "def.h"
//#include "image_bw.h"
#include "waveinit_new.h"
//#include "image.h"


//xx Private macro xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
/*
#define SDA_H     	(P1OUT |=BIT7)						// P1.7
#define SDA_L     	(P1OUT &=~BIT7)
#define SCLK_H    	(P1OUT |=BIT6)  					// P1.6
#define SCLK_L   	(P1OUT &=~BIT6) 
#define nCS_H     	(P1OUT |=BIT5)						// P1.5
#define nCS_L     	(P1OUT &=~BIT5)
#define nDC_H     	(P1OUT |=BIT4)						// P1.4
#define nDC_L     	(P1OUT &=~BIT4)
#define nRST_H     	(P1OUT |=BIT3)						// P1.3 
#define nRST_L     	(P1OUT &=~BIT3)
*/


#define SDA_L	  _LATB5 = 0   // SPI_DO  D1
#define SDA_H	  _LATB5 = 1

#define SCLK_L	  _LATB4 = 0 // SPI_CLK1 D0
#define SCLK_H	  _LATB4 = 1

#define nCS_L	  _LATB3 = 0  // SPI_SS1
#define nCS_H	  _LATB3 = 1

#define nDC_L 	  _LATB2 = 0  // SPI_DI1
#define nDC_H 	  _LATB2 = 1

#define nRST_L	  _LATA0 = 0
#define nRST_H	  _LATA0 = 1




#define DELAY_TIME	1									// 图片显示完停留时间(单位:秒)

#define MODE1  											// panel scan direction



#define PIC_BLACK		252
#define PIC_WHITE		255
#define PIC_ONE			1
#define PIC_NOKIA   	2
#define PIC_SUNING		3
#define PIC_SHOUSI 		4
#define PIC_BAZHE 		5
#define PIC_CHESSBOARD	6
#define PIC_JIFEN		7
#define PIC_FU			8
#define PIC_GC4			9
#define PIC_SAVE		10
#define PIC_HLINE		11
#define PIC_VLINE	        12
#define PIC_HK                  13
#define PIC_T                   14
#define PIC_P                   15
#define PIC_Z                   16
#define PIC_COOK                17
#define PIC_HVLINE                 18

//xx Private functions xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   延时函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void DELAY_100nS(int delaytime)   						// 30us 
{
	int i,j;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<10;j++);
}

void DELAY_mS(int delaytime)    						// 1ms
{
	int i,j;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<2000;j++);
}

void DELAY_S(int delaytime)     						// 1s
{
	int i,j,k;
	
	for(i=0;i<delaytime;i++)
		for(j=0;j<1000;j++)
			for(k=0;k<2000;k++);
}



//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   电子纸驱动操作函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

// 复位 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void RESET()
{
	nRST_L;
	DELAY_mS(1);								
 	nRST_H;
  	DELAY_mS(1);
}

// 读忙 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void READBUSY()
{
    unsigned int i;
   // return;
       for (i=0;i<MAX_WAIT_TIME;i++)
       {
      //   delay_ms(60000);
        }
       for (i=0;i<MAX_WAIT_TIME;i++)
        {
       //  delay_ms(60000);
       }
 //    for (i=0;i<MAX_WAIT_TIME;i++)
  //      {
  //       delay_ms(60000);
  //      }
     return;
  	while(1)
  	{
   	//	_NOP();
   	 	//if((P1IN & 0x04)==0)
        if((( LATBbits.LATB2&0x01))==0)
    		break;
  	}      
}

// 写命令 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void SPI4W_WRITECOM(unsigned char INIT_COM)
{
  	unsigned char TEMPCOM;
  	unsigned char scnt;

  	TEMPCOM=INIT_COM;
  	nCS_H;
  	nCS_L;
  	SCLK_L;
 	nDC_L;
  	for(scnt=0;scnt<8;scnt++)
  	{
    	if(TEMPCOM&0x80)
     	 	SDA_H;
    	else
      		SDA_L;
    	SCLK_H;  
        DELAY_100nS(1) ;
    	SCLK_L;  
    	TEMPCOM=TEMPCOM<<1;
  	}
  	nCS_H;  
}

// 写数据 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void SPI4W_WRITEDATA(unsigned char INIT_DATA)
{
  	unsigned char TEMPCOM;
  	unsigned char scnt;

  	TEMPCOM=INIT_DATA;
  	nCS_H;
  	nCS_L;
  	SCLK_L;
  	nDC_H;
  	for(scnt=0;scnt<8;scnt++)
  	{
    	if(TEMPCOM&0x80)
      	SDA_H;
   	 	else
      	SDA_L;
    	SCLK_H;  
        DELAY_100nS(1) ;
    	SCLK_L;  
    	TEMPCOM=TEMPCOM<<1;
  	}
  	nCS_H;  
}

// 写波形数据表 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void WRITE_LUT()
{
	unsigned char i;
	
	SPI4W_WRITECOM(0x32);		// write LUT register
	for(i=0;i<30;i++)			// write LUT register
        SPI4W_WRITEDATA(init_data6[i]);
     //    SPI4W_WRITEDATA(init_data[i]);
}

// 电子纸驱动初始化 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//void INIT_SPD2701()
void INIT_SSD1608()
{

	SPI4W_WRITECOM(0x11);		// data enter mode
	SPI4W_WRITEDATA(0x01);
	SPI4W_WRITECOM(0x44);		// set RAM x address start/end, in page 36
	SPI4W_WRITEDATA(0x00);		// RAM x address start at 00h;
	SPI4W_WRITEDATA(0x0f);		// RAM x address end at 0fh(15+1)*8->128 
	SPI4W_WRITECOM(0x45);		// set RAM y address start/end, in page 37
	SPI4W_WRITEDATA(0x27);		// RAM y address start at 127h;
	SPI4W_WRITEDATA(0x01);		
        SPI4W_WRITEDATA(0x00);		// RAM y address end at 00h;
	SPI4W_WRITEDATA(0x00);		

	SPI4W_WRITECOM(0x2C);		// vcom
	SPI4W_WRITEDATA(0x6e);		
//	SPI4W_WRITEDATA(0xA0);		//-1V
	SPI4W_WRITECOM(0x3C);		// board
	SPI4W_WRITEDATA(0x33);		//GS1-->GS1
//	SPI4W_WRITEDATA(0x30);		//GS0-->GS0
//	SPI4W_WRITEDATA(0x63);		//VBD-->VSL

	WRITE_LUT();
}

// 入深度睡眠 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void enterdeepsleep()
{
  	SPI4W_WRITECOM(0x10);
  	SPI4W_WRITEDATA(0x01);
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   图片显示函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void dis_img2(unsigned char num)
{
	unsigned int row, col;
	unsigned int pcnt;

	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 296;
	SPI4W_WRITEDATA(0x27);
    SPI4W_WRITEDATA(0x01);
        
	SPI4W_WRITECOM(0x24);
	
	pcnt = 0;											// 复位或保存提示字节序号
	for(col=0; col<T_COL; col++)							// 总共172列
	{
		for(row=0; row<T_ROW; row++)						// 总共72行，每个像素2bit,即 72/4 字节
		{
		   SPI4W_WRITEDATA(gImage[pcnt]);
			pcnt++;
		}
	}

	SPI4W_WRITECOM(0x22);
 	SPI4W_WRITEDATA(0xC7);		//Load LUT from MCU(0x32), Display update
//	SPI4W_WRITEDATA(0xF7);		//TS operation, then Load LUT from OTP, Display update
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
//	READBUSY();
  //   enterdeepsleep();
	
}


//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   ??????    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
void dis_img(unsigned char num)
{
	unsigned int row, col;
	unsigned int pcnt;

	SPI4W_WRITECOM(0x4E);		// set RAM x address count to 0;
	SPI4W_WRITEDATA(0x00);
	SPI4W_WRITECOM(0x4F);		// set RAM y address count to 296;
	SPI4W_WRITEDATA(0x27);
  SPI4W_WRITEDATA(0x01);
        
	SPI4W_WRITECOM(0x24);
	
	pcnt = 0;											// ???????????
	for(col=0; col<296; col++)							// ??172?
	{
		for(row=0; row<16; row++)						// ??72??????2bit,? 72/4 ??
		{
				switch (num)
				{
				case PIC_HK:
					SPI4W_WRITEDATA(gImage[pcnt]);
					break;	
                                
                                case PIC_COOK:
					SPI4W_WRITEDATA(gImage[pcnt]);
					break;	
                                        
              //                  case PIC_Z:
				//	SPI4W_WRITEDATA(gImage_En_c[pcnt]);
				//	break;	
 
                                case PIC_T:
					SPI4W_WRITEDATA(gImage[pcnt]);
					break;	
                                        
                                case PIC_P:
					SPI4W_WRITEDATA(gImage[pcnt]);
					break;	

					case PIC_HVLINE:
					SPI4W_WRITEDATA(gImage[pcnt]);
					break;
                                case PIC_VLINE:
                                        if(col%2)
					    SPI4W_WRITEDATA(0xff);
                                        else
                                            SPI4W_WRITEDATA(0x00);
                                        
					break;	
                                        
                                case PIC_HLINE:
					SPI4W_WRITEDATA(0xAA);
					break;	
                                        
				case PIC_WHITE:
					SPI4W_WRITEDATA(0xff);
					break;	

				case PIC_BLACK:
					SPI4W_WRITEDATA(0x00);
					break;	
			   				default:
					break;
				}

			pcnt++;
		}
	}

	SPI4W_WRITECOM(0x22);
	SPI4W_WRITEDATA(0xC7);		//Load LUT from MCU(0x32), Display update
//	SPI4W_WRITEDATA(0xF7);		//TS operation, then Load LUT from OTP, Display update
	SPI4W_WRITECOM(0x20);
	DELAY_mS(1);
	READBUSY();
	
}
////////////////// Jason Added
 unsigned char EPD_index;
void EPD_print(void);
void print_letter(int x, int y, char letter);
unsigned char t_CurX=0; 
unsigned char t_CurY=0; 
unsigned char e_CurX=0; 
unsigned char EPD_buf[64];
void   EPD_clear(void)
{
//    memset(gImage,EPD_CLEAR,T_LCD_SIZE);
    t_CurY=0;
    t_CurX=0;
    e_CurX=0;
	EPD_index=0;
}
void EPD_Set_Cursor(unsigned char CurY, unsigned char CurX)
{
    if(CurY>MAX_EPD_ROW)
        return ;
    t_CurY=CurY;
    t_CurX=CurX;
    e_CurX=CurX;
    
}  


void EPD_putcLCD(char LCD_Char)
{
    EPD_buf[e_CurX]= LCD_Char;
   

     e_CurX++;
 
}
void EPD_flash(void)
{
  
     dis_img2(PIC_WHITE);

}
void EPD_print(void)
{
	if(e_CurX)
    EPD_printf(e_CurX, EPD_buf);
  
}
 void EPD_printf(unsigned char cc, unsigned char *buf)
 {

  //   print_letter16(i,t_CurY, buf[i]);    

     dis_img2(PIC_WHITE);
         t_CurY++;
     if(t_CurY>7)
         t_CurY=0;
  
 
 }
#ifdef EPDD
void buttonUp(void)
#else
void buttonUp_EPD(void)
#endif
{
}
#ifdef EPDD
void buttonDown(void)
#else
void buttonDown_EPD(void)
#endif
{
 
}

 
 
#ifdef EPD
 extern void showTitle_LCD(void);
void showTitle(void)
#else
void showTitle_EPD(void)
#endif
{

}
void log_data_EPD(unsigned char cc)
{
 
  
   
}    
void store_data_EPD(unsigned char cc)
{
   
} 
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xx   主函数    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#ifdef EPD_029 
void EPD_main(void)
#else
void EPD_main029(void)
#endif
{
	unsigned int i,k;
  //      WDTCTL = WDTPW + WDTHOLD;							// Stop watchdog timer to prevent time out reset
//	BCSCTL1 = CALBC1_1MHZ; 								// set DCO frequency 1MHZ
	//DCOCTL = CALDCO_1MHZ; 

//	P1DIR |=0xF8;  										// set P1.3~7 output
//	P3DIR |= 0x10;										// p3.4,p3.5 设为输入

		
	RESET();											// 电子纸控制器复位
    	SPI4W_WRITECOM(0x12);			//SWRESET
	READBUSY();
	
	
	INIT_SSD1608();	
    SPI4W_WRITECOM(0x21);								//
    SPI4W_WRITEDATA(0x83);
    dis_img(PIC_WHITE);
    SPI4W_WRITECOM(0x21);								//
    SPI4W_WRITEDATA(0x03);

 //   if(!BUTTON_DOWN ||  !BUTTON_UP )
 //   {  
 //        NVMErasePage( ( void * ) NVM_Page_Base );
 //        NVMWriteWord( ( void * ) NVM_Page_Base + NVMOffset , 0x31 ); 
  //  }    
  //  NVMDataPointer = ( unsigned char * ) NVM_Page_Base + NVMOffset;
  //  memset(gImage,0xFF,T_LCD_SIZE);
   
   
#ifdef  USE_EPD
//        main_2 ();
#else 
        showTitle();    
#endif   
   
   ///////////////////////////////////////////////////////////////////////////////////
	while(1)
	{
		
           dis_img2(PIC_BLACK);
			DELAY_S(DELAY_TIME);
		    dis_img(PIC_HK);
			   for (k=0;k<MAX_WAIT_TIME;k++)
        {
   //      delay_ms(MAX_WAIT_TIME2);
        }
        for (k=0;k<MAX_WAIT_TIME;k++)
        {
     //    delay_ms(MAX_WAIT_TIME2);
        }
        for (k=0;k<MAX_WAIT_TIME;k++)
        {
  //       delay_ms(MAX_WAIT_TIME2);
        }    
    //  dis_img(PIC_HLINE);
		//	DELAY_S(DELAY_TIME);
          //  dis_img(PIC_VLINE);
	//		DELAY_S(DELAY_TIME);
		//	dis_img(PIC_HVLINE);
		//	DELAY_S(DELAY_TIME);
		//	dis_img(PIC_COOK);
		//	DELAY_S(DELAY_TIME);
	//		dis_img(PIC_WHITE);
		//	DELAY_S(DELAY_TIME);
                        
        for (k=0;k<MAX_WAIT_TIME;k++)
        {
 //        delay_ms(MAX_WAIT_TIME2);
        }
        for (k=0;k<MAX_WAIT_TIME;k++)
        {
    //     delay_ms(MAX_WAIT_TIME2);
        }
        for (k=0;k<MAX_WAIT_TIME;k++)
        {
   //      delay_ms(MAX_WAIT_TIME2);
        }    
 
		
	}

}
