/*****************************************************************************
 *
 * Filename:
 * ---------
 *    Motion_sensor_custom.c
 *
 * Project:
 * --------
 *    Motion Sensor KX023 SPI Interface
 *
 * Description:
 * ------------
 *   This Module defines Serial Interface.
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 ****************************************************************************/
#define MOTION_SENSOR_SUPPORT
#define uint16_t unsigned int
#define uint8_t unsigned char
#ifdef MOTION_SENSOR_SUPPORT

#include "motion_sensor_custom.h"   
#include "motion_sensor_spi.h"   
#include <xc.h>
#define _XTAL_FREQ  2000000
/* ---------------------------------------------------------------------------- */
/*
* FUNCTION                                                            
*	motion_sensor_check_softwave_ready
*
* DESCRIPTION                                                           
*   check sensor softwave function completion
*
* PARAMETERS
*  received data
*	
* RETURNS
*	None
*/    
void motion_sensor_check_softwave_ready(void)
{    
	do
	{
		__delay_ms(50);
	} while ((motion_sensor_read_data(MOTION_SENSOR_CTRL_REGB) & 0x80));
}


/*
* FUNCTION                                                            
*	acc_sensor_pwr_up
*
* DESCRIPTION                                                           
*   	This function is to power up acceleration module
*
* CALLS  
*
* PARAMETERS
*  None
*	
* RETURNS
*	None
*/    
//test// //char temp1=0;
//test// //char temp2=0;
//test// //char temp3=0;
//test// //char temp4=0;
void acc_sensor_pwr_up(void)
{
	__delay_ms(30);	//delay for power on reset	
	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGB);	//KX023 reset
	motion_sensor_check_softwave_ready();						//wait & check reset completion
	
	motion_sensor_write_data(0x00, MOTION_SENSOR_CTRL_REGA);	//set to low current, 2g
	motion_sensor_write_data(0x02, MOTION_SENSOR_ODCNTL_REGC);	//set ODR 50Hz
	motion_sensor_write_data(0x10,MOTION_SENSOR_INT_CTRL_REG1);	//disable all INT1 interrupt funs
	motion_sensor_write_data(0x10,MOTION_SENSOR_INT_CTRL_REG5);	//disable all INT2 interrupt funs
	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGA);	//set to work mode
}


/*
* FUNCTION                                                            
*	acc_sensor_pwr_down 
*
* DESCRIPTION                                                           
*   	This function is to power down acceleration
*
* CALLS  
*
* PARAMETERS
*	None
*	
* RETURNS
*	None
*/ 
void acc_sensor_pwr_down(void)
{   
	motion_sensor_disable_bits(0x80, MOTION_SENSOR_CTRL_REGA);	// into standby mode
}


/*
* FUNCTION                                                            
*	acc_sensor_init
*
* DESCRIPTION                                                           
*   	This function is to initialize acceleration module
*
* CALLS  
*
* PARAMETERS
*	None
*	
* RETURNS
*	None
*/ 
void acc_sensor_init(void)
{  
	motion_senosr_interface_init();
}


/*
* FUNCTION                                                            
*	acc_sensor_interrupt_initial
*
* DESCRIPTION                                                           
*   Set & enable interrupt function
*
* CALLS  
*
* PARAMETERS
*  None
*	
* RETURNS
*	None
*/    
void acc_sensor_interrupt_initial(void)
{
	motion_sensor_disable_bits(0x80, MOTION_SENSOR_CTRL_REGA);		// into standby mode
	motion_sensor_write_data(0x00, MOTION_SENSOR_INT_CTRL_REG1);
	motion_sensor_enable_bits(0x30, MOTION_SENSOR_INT_CTRL_REG1);	// interrupt-pin (5) enable, active high, latch, no WUF
    motion_sensor_write_data(0x0C,0x32);	//etilt_angle_ll
    motion_sensor_write_data(0x2A,0x33);	//etilt_angle_HL
    //ADD INT interrupt
    motion_sensor_enable_bits(0x01, MOTION_SENSOR_INT_CTRL_REG4);	// interrupt-pin (5) enable, active high, latch, no WUF
    //ADD INT interrupt
	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGA);		// set to work mode 
}


/*
* FUNCTION                                                            
*	acc_sensor_WUF_initial
*
* DESCRIPTION                                                           
*   Set & enable the WUF function
*
* CALLS  
*
* PARAMETERS
*  None
*	
* RETURNS
*	None
*/    
void acc_sensor_wuf_initial(void)
{
	motion_sensor_disable_bits(0x80, MOTION_SENSOR_CTRL_REGA);	// into standby mode
	motion_sensor_write_data(0x00, MOTION_SENSOR_WUFC);			// WUF_TIMER
	motion_sensor_write_data(0x08, MOTION_SENSOR_ATH);			//  WUF_THRESH
	motion_sensor_enable_bits(0x02, MOTION_SENSOR_CTRL_REGA); 	// enable WUF function  
	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGA);	// set to work mode 
}


/*
* FUNCTION                                                            
*	acc_sensor_clear_int_status
*
* DESCRIPTION                                                           
*   Release the latch mode of interrupt
*
* CALLS  
*
* PARAMETERS
*  None
*	
* RETURNS
*	None
*/
void acc_sensor_clear_interrupt_status(void)
{
	uint8_t i;
	i = motion_sensor_read_data(MOTION_SENSOR_INT_REL);
}


/*
* FUNCTION                                                            
*	acc_sensor_read_status
*
* DESCRIPTION                                                           
*   Read the interrupt status
*
* CALLS  
*
* PARAMETERS
*  *status1_value : status 1 value 
*  *status2_value : status 2 value 
*	 
* RETURNS
*	None
*/

 uint8_t status1_value=0;
 uint8_t status2_value=0;
  uint8_t status3_value=0;
  uint8_t status4_value=0;
    uint8_t status5_value=0;
      uint8_t status6_value=0;
         uint8_t status7_value=0;
          uint8_t status8_value=0;
void acc_sensor_read_status(void)
{
//    motion_sensor_disable_bits(0x80, MOTION_SENSOR_CTRL_REGA);		// into standby mode READ
	status1_value = motion_sensor_read_data(MOTION_SENSOR_INS1);
	status2_value = motion_sensor_read_data(MOTION_SENSOR_INT_STATUS);
    status3_value = motion_sensor_read_data(MOTION_SENSOR_INS2);
    status4_value = motion_sensor_read_data(MOTION_SENSOR_INS3);
    status5_value = motion_sensor_read_data(MOTION_SENSOR_TSPP);
    status7_value = motion_sensor_read_data(MOTION_SENSOR_INT_CTRL_REG4);

//    	motion_sensor_enable_bits(0x80, MOTION_SENSOR_CTRL_REGA);	// set to work mode 
    NOP();
     NOP();
}


/*Customization functin*/ 
/*
* FUNCTION                                                            
*	acc_sensor_get_acc
*
* DESCRIPTION                                                           
*   	This function is to get ADC value in every axis
*
* CALLS  
*
* PARAMETERS
*	x_adc: ADC value in X-axis
*	y_adc: ADC value in Y-axis
*	z_adc: ADC value in Z-axis
*
* RETURNS
*	None
*/ 
#define _g_sens  16384
 int x_value[10]=0;
 int y_value[10]=0;
 int z_value[10]=0;
 long int x_value_main=0;
 long int y_value_main=0;
 long int z_value_main=0;
 int data[3]=0;
 uint8_t g_index=0;
 uint8_t g_index_flag=0;
uint8_t index=0;
void acc_sensor_get_acc(uint8_t mode)
{	
	uint8_t temp_data[5];
    index=mode; 
	motion_sensor_read_multi_data(MOTION_SENSOR_XOUT_L, temp_data, 6);
	
	x_value[g_index] = ((temp_data[1] << 8) | temp_data[0]);		/* x-axis */      
	y_value[g_index]  = ((temp_data[3] << 8) | temp_data[2]);		/* y-axis */      
	z_value[g_index]  = ((temp_data[5] << 8) | temp_data[4]);		/* z-axis */   
    g_index++;
       switch(index)
			{
				case 0:
				{
                    if(g_index==2)
                    {
                        x_value[0]= (x_value[0] + x_value[1])/2;
                        y_value[0]= (y_value[0] + y_value[1])/2;
                        z_value[0] = (z_value[0] + z_value[1])/2;
                        g_index=0;
                    }
                    break;				
                }
                case 1:
				{
                      if(g_index==4)
                    {
                            x_value[0]= (x_value[0] + x_value[1])/2;
                            y_value[0]= (y_value[0] + y_value[1])/2;
                            z_value[0]= (z_value[0] + z_value[1])/2;
                            //
                            x_value[1]= (x_value[2] + x_value[3])/2;
                            y_value[1]= (y_value[2] + y_value[3])/2;
                            z_value[1]= (z_value[2] + z_value[3])/2;
                            ///
                            x_value[0]= (x_value[0] + x_value[1])/2;
                            y_value[0]= (y_value[0] + y_value[1])/2;
                            z_value[0] = (z_value[0] + z_value[1])/2;
                            g_index=0;
                            g_index_flag=1;
                        g_index=0;
                    }
                    break;				
                }
                case 2:
				{
                      if(g_index==8)
                    {
                            x_value[0]= (x_value[0] + x_value[1])/2;//raw
                            y_value[0]= (y_value[0] + y_value[1])/2;//raw
                            z_value[0]= (z_value[0] + z_value[1])/2;//raw
                            //
                            x_value[1]= (x_value[2] + x_value[3])/2;//raw
                            y_value[1]= (y_value[2] + y_value[3])/2;//raw
                            z_value[1]= (z_value[2] + z_value[3])/2;//raw
                            ///
                            x_value[2]= (x_value[4] + x_value[5])/2;//raw
                            y_value[2]= (y_value[4] + y_value[5])/2;//raw
                            z_value[2] =(z_value[4] + z_value[5])/2;//raw
                                        ///
                            x_value[3]= (x_value[6] + x_value[7])/2;//raw
                            y_value[3]= (y_value[6] + y_value[7])/2;//raw
                            z_value[3] =(z_value[6] + z_value[7])/2;//raw
                             ///
                            x_value[0]= (x_value[0] + x_value[1])/2;//new
                            y_value[0]= (y_value[0] + y_value[1])/2;//new
                            z_value[0]= (z_value[0] + z_value[1])/2;//new
                            ////
                            x_value[1]= (x_value[2] + x_value[3])/2;//new
                            y_value[1]= (y_value[2] + y_value[3])/2;//new
                            z_value[1]= (z_value[2] + z_value[3])/2;//new
                            ///
                             ///
                            x_value[0]= (x_value[0] + x_value[1])/2;//final
                            y_value[0]= (y_value[0] + y_value[1])/2;//final
                            z_value[0]= (z_value[0] + z_value[1])/2;//final
                            ////
                            g_index=0;
                            g_index_flag=1;
                        g_index=0;
                    }
                    break;
                    		break;
				
                }
       }
//     if(g_index==8 && mode==8)
//    {
//        x_value_main= (x_value[0] + x_value[1]+x_value[2] + x_value[3]+x_value[4] + x_value[5]+x_value[6] + x_value[7])/8;
//        y_value_main= (y_value[0] + y_value[1]+y_value[2] + y_value[3]+y_value[4] + y_value[5]+y_value[6] + y_value[7])/8;
//        z_value_main= (z_value[0] + z_value[1]+z_value[2] + z_value[3]+z_value[4] + z_value[5]+z_value[6] + z_value[7])/8;
//        g_index=0;
//        g_index_flag=1;
//    }
    if(g_index==4 && mode==4)
    {
        x_value[0]= (x_value[0] + x_value[1])/2;
        y_value[0]= (y_value[0] + y_value[1])/2;
        z_value[0]= (z_value[0] + z_value[1])/2;
        //
        x_value[1]= (x_value[2] + x_value[3])/2;
        y_value[1]= (y_value[2] + y_value[3])/2;
        z_value[1]= (z_value[2] + z_value[3])/2;
        ///
        x_value[0]= (x_value[0] + x_value[1])/2;
        y_value[0]= (y_value[0] + y_value[1])/2;
        z_value[0] = (z_value[0] + z_value[1])/2;
        g_index=0;
        g_index_flag=1;
    }
//    else if(g_index==2 && mode==2)
//    if(g_index==2)
//    {
//        x_value[0]= (x_value[0] + x_value[1])/2;
//        y_value[0]= (y_value[0] + y_value[1])/2;
//        z_value[0] = (z_value[0] + z_value[1])/2;
//        g_index=0;
//        if(mode == 8)
//        {
//            g_index_flag=1;
//        }
//        else if(mode == 4)
//        {
//           g_index_flag=1;
//        }
//        else if(mode == 2)
//        {
//            g_index_flag=1;
//        }
//
//    }

  NOP();
  NOP();
}

#endif
