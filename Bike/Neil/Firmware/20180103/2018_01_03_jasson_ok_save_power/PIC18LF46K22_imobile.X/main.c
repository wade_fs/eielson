/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs 

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs  - 1.45
        Device            :  PIC18LF46K22
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.40
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include "mcc_generated_files/mcc.h"

/*
                         Main application
 */
#define INTERRUPT_GlobalInterruptEnable() (INTCONbits.GIE = 1)
#define INTERRUPT_PeripheralInterruptEnable() (INTCONbits.PEIE = 1)
#define debug
//#define Bluetooth
//#define test
#define wake
void g_sensor_vision(void);
void g_sensor_initial(void);
void mcu18_spi_24_send(void);
void wake_up_initial(void);
void ready_to_sleep(void);
void com_bluetoooth_set (void);
void g_sensor_int_value(void);
//void handle_3g_sensor(void);
//uint8_t eepromCmd1[3]  = {0x7F,0x00};
//uint8_t eepromCmd2[3]  = {0x19,0x00};
//uint8_t eepromCmd3[3]  = {0x19,0x80};
//uint8_t eepromCmd4[3]  = {SPI_READ_CMD,0x0F};
//uint8_t eepromCmd4_1[3]  = {0x12,0x00};
////
extern uint8_t tmr1_flag_1s;
extern uint8_t tmr1_flag_100ms;
uint8_t tmr1_flag_10s=0;
uint8_t tmr1_flag_10s_index=0;
#include "motion_sensor_custom.h"   
#include "motion_sensor_spi.h"   
#include	"SPISubs.h"
///uart
uint8_t int_value_temp=0;
uint8_t sersonr_temp=0;
extern uint8_t temp;

//////g-sensor
int main_x_value=0;
int main_y_value=0;
int main_z_value=0;
extern int x_value[10];
extern int y_value[10];
extern int z_value[10];
extern long int x_value_main;
extern long int y_value_main;
extern long int z_value_main;
extern uint8_t g_index;
extern uint8_t g_index_flag;
uint8_t str[160];
uint8_t str1[160];
extern uint8_t status1_value;
extern uint8_t status2_value;
extern uint8_t status3_value;
extern uint8_t status4_value;
extern uint8_t status5_value;
extern uint8_t status6_value;
extern uint8_t status7_value;
extern uint8_t status8_value;
uint8_t g_vison=0x00;
extern uint8_t g_index_flag;
uint8_t xyz_falg=0;;
//////g-sensor
extern uint8_t uart2_flag;
void main(void)
{ 
    // Initialize the device
//      OSCCON=0x00;
//        Sleep();//91 uA
//       NOP();

    SYSTEM_Initialize();

    g_sensor_initial();//sleep ok
////   // mcu18_spi_24_send();//it need debug in pic24 , if it do not connect pic24, it will stop
    INTERRUPT_GlobalInterruptHighEnable();
    INTERRUPT_GlobalInterruptLowEnable();
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();

    wake_up_initial();   
//     M_INDICATOR_SetHigh();
    TMR1_StartTimer();
//    power_24_on_SetHigh() ;

    NOP();
     NOP();

     ////TEST ////
     //mcu18_spi_24_send();//it need debug in pic24
     //M_INDICATOR_SetHigh();
     //power_24_on_SetHigh() ;
     //motion_sensor_write_data(0xC8,MOTION_SENSOR_INT_CTRL_REG1);	//enable all INT1 interrupt funs
     ////TEST 
  //   putrs2USART("\r\nMCC: It's Easy!\r\nPIC18 Lab 2\r\n");  
     //motion_sensor_write_data(0x00,MOTION_SENSOR_ATH);	//enable all INT1 interrupt funs    
     ////TEST ////
    while (1)
    {

        if(tmr1_flag_100ms == 1)
        {
            tmr1_flag_100ms = 0; 
            acc_sensor_get_acc(0x02);
            acc_sensor_read_status();
            // it is ok
            acc_sensor_clear_interrupt_status(); // it can read G-sensor INT 
        //    sprintf(str1, "INT_STATUS=%x, \r\n",status2_value);
            putrs2USART(str1);  
             // it is ok
            if(g_index_flag == 1)
            {
                g_index_flag=0;
                main_x_value=x_value[0];
                main_y_value=y_value[0];
                main_z_value=z_value[0];     
                #ifdef test
                 sprintf(str, "x=%d,y=%d,z=%d\r\n", main_x_value,main_y_value,main_z_value);
                 putrs2USART(str);  
                 #endif
           
            }
            ////////////////test////////////////
//            if(uart2_flag==1)
//            {
////                #ifdef wake
////                uart2_flag=0;
////                putrs2USART(" UART wake ok \r\n");  
////                sprintf(str, "int_value_temp=%x  \r\n", int_value_temp);
////                putrs2USART(str);
////                int_value_temp=0;
////                #endif
//            }
               ////////////////test////////////////
         if(uart2_flag==1)
         {
            uart2_flag=0;
            putrs2USART(" UART wake ok \r\n");  
            sprintf(str, "temp=%x  \r\n", temp);
            putrs2USART(str);
         }
        }
        
        if(tmr1_flag_1s==1)
        {
//           mcu18_spi_24_send();
//           acc_sensor_read_status();
           TMR1_Reload();
           tmr1_flag_1s=0;
//         M_INDICATOR_Toggle();
//             ready_to_sleep();//20nA
         tmr1_flag_10s_index++;
//         ready_to_sleep();//20nA
         if(tmr1_flag_10s_index == 10)
         {
             tmr1_flag_10s_index=0;
             tmr1_flag_10s=1;
              ready_to_sleep();//20nA
         }
//         if(tmr1_flag_5s == 1)
//         {
//             tmr1_flag_5s=0;
    //        ready_to_sleep();
//         }
//          M_INDICATOR_Toggle();   
//         #ifdef Bluetooth
//         com_bluetoooth_set();
//         #endif
//         #ifdef debug
         sprintf(str, "x=%d,y=%d,z=%d\r\n", main_x_value,main_y_value,main_z_value);
         putrs2USART(str);   
//         g_sensor_int_value();
//         #endif
        }
    }
}
void putrs2USART(const char *data)
{
  do
  {  // Transmit a byte
    while(Busy2USART());
    EUSART2_Write(*data);               // ?? enusart1.c ??? (from MCC)
  } while( *data++ );
}
void ready_to_sleep(void)
{

    //////////// it can off all
    PIE1bits.ADIE = 0;
    PIE1bits.RC1IE = 0;
    PIE3bits.RC2IE = 0;

    LATE = 0x00;    
    LATD = 0x00;    
    LATA = 0x00;    
    LATB = 0x00;    
    LATC = 0x00;    
////    M_INDICATOR_SetHigh() ; 21uA-28uA;
//////    //it is not for board
//////    TRISE = 0x00;
//////    TRISA = 0x00;
//////    TRISB = 0x00;
//////    TRISC = 0x00;
//////    TRISD = 0x00;
//////
//////    ANSELC = 0x00;
//////    ANSELB = 0x00;
//////    ANSELD = 0x00;
//////    ANSELE = 0x00;
//////    ANSELA = 0x00;
//////     //it is not for board
//    //////////it need to open,  for board`
     M_INDICATOR_SetHigh();//it need to open, it 30uA for board
     LATCbits.LATC0 = 1;//it need to open,  it 4mA for board   
     ///
     LATDbits.LATD5 = 1;//it need to open,  it 4mA for board   
     LATDbits.LATD2 = 1;//it need to open,  it 4mA for board   
     LATCbits.LATC1 = 1;//it need to open,  it 4mA for board   
       
     OSCCON=0x00;  
       //////////it need to open,  for board
   // PIE1bits.ADIE = 0; ///ADD 2017_12_20
//    if(BAUDCON2bits.RCIDL ==1)
//    {
//        BAUDCON2bits.WUE =1; // if it need wake up again, it need to add this in isr
//        OSCCONbits.IDLEN=1;
        Sleep();
        __delay_ms(30);
        SYSTEM_Initialize();
        wake_up_initial();
//        BAUDCON2bits.WUE =0;
    
}


void g_sensor_vision(void)
{

       __delay_ms(2);
         M_G_SSI_SetLow();
         SPI1_Exchange8bit(MOTION_SENSOR_WHO_AM_I | 0x80 );
         g_vison=SPI1_Exchange8bit(NULL);
         M_G_SSI_SetHigh();
         NOP();
         NOP();     
}
void g_sensor_initial(void)
{
        acc_sensor_pwr_down();
        acc_sensor_init();
        acc_sensor_interrupt_initial();
        acc_sensor_wuf_initial();
        acc_sensor_clear_interrupt_status();
        motion_sensor_write_data(0x89, MOTION_SENSOR_WHO_AM_I);             
         g_sensor_vision();
}
uint8_t temp=0;
	unsigned char 	EEPROM_Data1 , EEPROM_Data2,EEPROM_Data3 ;
	unsigned char	SPI_Status ;
void mcu18_spi_24_send(void)
{
       EEPROM_Data1 =   SPI2_Exchange8bit(0x34);/////ex: 0x34 0x12 0x56 is ok 18 to 24
       EEPROM_Data2  =  SPI2_Exchange8bit(0x12);/////ex: 0x34 0x12 0x56 is ok 18 to 24
       EEPROM_Data3 =   SPI2_Exchange8bit(0x56);/////ex: 0x34 0x12 0x56 is ok 18 to 24
       #ifdef debug
       sprintf(str1, "EEPROM_Data1=%x EEPROM_Data2=%x EEPROM_Data3=%x \r\n",EEPROM_Data1,EEPROM_Data2,EEPROM_Data3);
       putrs2USART(str1);  
        #endif
       NOP();
       NOP();
}
void wake_up_initial(void)
{
    INTCONbits.INT0IE =  1;
    INTCONbits.INT0IF  = 0;
    INTCON3bits.INT1IE = 1;
    INTCON3bits.INT1IF = 0;
    INTCON3bits.INT2IE = 1;
    INTCON3bits.INT2IF = 0;


}
void com_bluetoooth_set (void)
{
           #ifdef Bluetooth
            putrs2USART("{abcde56789abcde");  
            __delay_ms(5);
             putrs2USART("[1234");  
             __delay_ms(5);
              putrs2USART("(30");
            #endif
}
void	SPI_Delay(void)
{
	int	LoopSPI ;
	for (LoopSPI = 0 ; LoopSPI < 100 ; LoopSPI ++ ) ;

}
void g_sensor_int_value(void)

{
            acc_sensor_clear_interrupt_status(); // it can read G-sensor INT 
            sprintf(str1, "INT_STATUS=%x, \r\n",status2_value);
            putrs2USART(str1);  
            motion_sensor_write_data(int_value_temp,MOTION_SENSOR_ATH);	//ath    
            ////if you want to change , it need to modify this .
//            motion_sensor_write_data(0x0C,0x32);	//etilt_angle_ll default
//            motion_sensor_write_data(0x2A,0x33);	//etilt_angle_HL default
//            motion_sensor_write_data(0x1C,0x32);	//etilt_angle_ll
//            motion_sensor_write_data(0x3A,0x33);	//etilt_angle_HL
            sersonr_temp=motion_sensor_read_data(MOTION_SENSOR_ATH);
            sprintf(str, "int_value_temp=%x,sersonr_temp=%x\r\n", int_value_temp,sersonr_temp);
            putrs2USART(str);
            ///
                     ///
         /////debug g_gensor_value
//         sprintf(str1, "INS1=%x,INT_STATUS=%x,INS2=%x,INS3=%x,TSPP= %x,INT_REL=%x \r\n",status1_value,status2_value,status3_value,status5_value,status7_value);
//         putrs2USART(str1);  
         // #endif
}
/**
 End of File
*/