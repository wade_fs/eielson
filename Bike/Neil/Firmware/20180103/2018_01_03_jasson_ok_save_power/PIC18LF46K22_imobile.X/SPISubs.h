

#define			SLAVE_ADDRESS 	(0xA0 >> 1)		// I2C module compares bit<0..6> of I2CADD with bit<1..7> of I2C Command Byte
#define			SPI_EE_READ		0x03 
#define			SPI_EE_WRITE	0x02
#define			SPI_EE_WRDIS	0x04
#define			SPI_EE_WREN		0x06
#define			SPI_RD_STATUS	0x05
#define			SPI_WR_STATUS	0x01
#define			SPI_CS			PORTBbits.RB2
#define			DIR_SPI_CS		TRISBbits.TRISB2
#define			MODE_SPI_CS		ADPCFGbits.PCFG2

#define			ANALOG_MODE 	0
#define			DIGITAL_MODE	1
#define			INPUT			1
#define			OUTPUT			0

int				SPI_EEPROM_CmdWrite(unsigned char ) ;
int				SPI_EEPROM_StsWrite(unsigned char ) ;
unsigned char	SPI_EEPROM_StsRead( void ) ;
unsigned char	SPI_ByteWrite(unsigned char) ;
unsigned char	SPI_EEPROM_ByteRead(unsigned int ) ;
int				SI_EEPROM_ByteWrite(unsigned int  , unsigned char  ) ;
void			Init_SPIM(void) ;
int				SPI_EE_Ready(void);

