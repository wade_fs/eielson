
/**
  SPI1 Generated Driver API Source File

  Company:
    Microchip Technology Inc.

  File Name:
    spi1.c

  @Summary
    This is the generated source file for the SPI1 driver using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description
    This source file provides APIs for driver for SPI1.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - pic24-dspic-pic32mm : v1.35
        Device            :  PIC24FJ32GA102
    The generated drivers are tested against the following:
        Compiler          :  XC16 1.31
        MPLAB 	          :  MPLAB X 3.60
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/

#include <xc.h>
#include "spi1.h"


/**
 Section: File specific functions
*/

inline __attribute__((__always_inline__)) SPI1_TRANSFER_MODE SPI1_TransferModeGet(void);
void SPI1_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData );
uint16_t SPI1_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData);

/**
 Section: Driver Interface Function Definitions
*/


void SPI1_Initialize (void)
{
    // MSTEN Slave; DISSDO disabled; PPRE 64:1; SPRE 8:1; MODE16 disabled; SMP Middle; DISSCK disabled; CKP Idle:High, Active:Low; CKE Idle to Active; SSEN disabled; 
    SPI1CON1 = 0x0040;
    // SPIFSD disabled; SPIBEN enabled; SPIFPOL disabled; SPIFE disabled; FRMEN disabled; 
    SPI1CON2 = 0x0001;
    // SISEL SPI_INT_SPIRBF; SPIROV disabled; SPIEN enabled; SPISIDL disabled; 
    SPI1STAT = 0x800C;
}

void SPI1_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
{

    while( SPI1STATbits.SPITBF == true )
    {

    }

    if (SPI1_TransferModeGet() == SPI1_DRIVER_TRANSFER_MODE_16BIT)
        SPI1BUF = *((uint16_t*)pTransmitData);
    else
        SPI1BUF = *((uint8_t*)pTransmitData);

    while ( SPI1STATbits.SRXMPT == true);

    if (SPI1_TransferModeGet() == SPI1_DRIVER_TRANSFER_MODE_16BIT)
        *((uint16_t*)pReceiveData) = SPI1BUF;
    else
        *((uint8_t*)pReceiveData) = SPI1BUF;

}

uint16_t SPI1_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
{

    uint16_t dataSentCount = 0;
    uint16_t count = 0;
    uint16_t dummyDataReceived = 0;
    uint16_t dummyDataTransmit = SPI1_DUMMY_DATA;

    uint8_t  *pSend, *pReceived;
    uint16_t addressIncrement;
    uint16_t receiveAddressIncrement, sendAddressIncrement;

    SPI1_TRANSFER_MODE spiModeStatus;

    spiModeStatus = SPI1_TransferModeGet();
    // set up the address increment variable
    if (spiModeStatus == SPI1_DRIVER_TRANSFER_MODE_16BIT)
    {
        addressIncrement = 2;
        byteCount >>= 1;
    }        
    else
    {
        addressIncrement = 1;
    }

    // set the pointers and increment delta 
    // for transmit and receive operations
    if (pTransmitData == NULL)
    {
        sendAddressIncrement = 0;
        pSend = (uint8_t*)&dummyDataTransmit;
    }
    else
    {
        sendAddressIncrement = addressIncrement;
        pSend = (uint8_t*)pTransmitData;
    }
        
    if (pReceiveData == NULL)
    {
       receiveAddressIncrement = 0;
       pReceived = (uint8_t*)&dummyDataReceived;
    }
    else
    {
       receiveAddressIncrement = addressIncrement;        
       pReceived = (uint8_t*)pReceiveData;
    }


    while( SPI1STATbits.SPITBF == true )
    {

    }

    while (dataSentCount < byteCount)
    {
        if ((count < SPI1_FIFO_FILL_LIMIT))
        {
            if (spiModeStatus == SPI1_DRIVER_TRANSFER_MODE_16BIT)
                SPI1BUF = *((uint16_t*)pSend);
            else
                SPI1BUF = *pSend;
            pSend += sendAddressIncrement;
            dataSentCount++;
            count++;
        }

        if (SPI1STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI1_DRIVER_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI1BUF;
            else
                *pReceived = SPI1BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }

    }
    while (count)
    {
        if (SPI1STATbits.SRXMPT == false)
        {
            if (spiModeStatus == SPI1_DRIVER_TRANSFER_MODE_16BIT)
                *((uint16_t*)pReceived) = SPI1BUF;
            else
                *pReceived = SPI1BUF;
            pReceived += receiveAddressIncrement;
            count--;
        }
    }

    return dataSentCount;
}



uint8_t SPI1_Exchange8bit( uint8_t data )
{
    uint8_t receiveData;
    
    SPI1_Exchange(&data, &receiveData);

    return (receiveData);
}

uint16_t SPI1_Exchange8bitBuffer(uint8_t *dataTransmitted, uint16_t byteCount, uint8_t *dataReceived)
{
    return (SPI1_ExchangeBuffer(dataTransmitted, byteCount, dataReceived));
}

/**

    The module's transfer mode affects the operation
    of the exchange functions. The table below shows
    the effect on data sent or received:
    |=======================================================================|
    | Transfer Mode  |     Exchange Function      |        Comments         |
    |=======================================================================|
    |                | SPIx_Exchange8bitBuffer()  |                         |
    |                |----------------------------|  OK                     |
    |                | SPIx_Exchange8bit()        |                         |
    |     8 bits     |----------------------------|-------------------------|
    |                | SPIx_Exchange16bitBuffer() | Do not use. Only the    |
    |                |----------------------------| lower byte of the 16-bit|
    |                | SPIx_Exchange16bit()       | data will be sent or    |
    |                |                            | received.               |
    |----------------|----------------------------|-------------------------|
    |                | SPIx_Exchange8bitBuffer()  | Do not use. Additional  |
    |                |----------------------------| data byte will be       |
    |                | SPIx_Exchange8bit()        | inserted for each       |
    |                |                            | 8-bit data.             |
    |     16 bits    |----------------------------|-------------------------|
    |                | SPIx_Exchange16bitBuffer() |                         |
    |                |----------------------------|  OK                     |
    |                | SPIx_Exchange16bit()       |                         |
    |----------------|----------------------------|-------------------------|
*/
inline __attribute__((__always_inline__)) SPI1_TRANSFER_MODE SPI1_TransferModeGet(void)
{
	if (SPI1CON1bits.MODE16 == 0)
        return SPI1_DRIVER_TRANSFER_MODE_8BIT;
    else
        return SPI1_DRIVER_TRANSFER_MODE_16BIT;
}

uint8_t SPI_ReadBuffer[64]={0x00};
uint8_t BufIndex=0;
uint8_t CommandState=0;
uint8_t Temp;
#define			SLAVE_ADDRESS 	(0xA0 >> 1)		// I2C module compares bit<0..6> of I2CADD with bit<1..7> of I2C Command Byte
#define			SPI_EE_READ		0x03 
#define			SPI_EE_WRITE	0x02
#define			SPI_EE_WRDIS	0x04
#define			SPI_EE_WREN		0x06
#define			SPI_RD_STATUS	0x05
#define			SPI_WR_STATUS	0x01
extern uint8_t spi1_flag;
///ex: 0x34 0x12 0x56 is ok
//	unsigned char	SPI_ReadBuffer ;
 	unsigned char	SPI_State = 0 ;	
	unsigned char	SPI_Buffer[64] ;
	unsigned int	SPI_BufferIndex ;	
void __attribute__((__interrupt__)) _SPI1Interrupt(void)
{
      IFS0bits.SPI1IF = 0;
     Temp = SPI1BUF ;
     SPI_ReadBuffer[BufIndex] = Temp;
//     _LATA4 ^= 1;
//      SPI1BUF = 0x34 ;
     SPI1_STATUS status;
					if(SPI_ReadBuffer[BufIndex] == 0x68)
					{
                        SPI1BUF = 0x34 ;
                          _LATA4 ^= 1;
						CommandState = BufIndex = 0;
                          spi1_flag=1;
					}
//					else
//					{
//						CommandState = BufIndex = 0;
//					}
//					break;
//				}
//				case 1:
//				{
//					if(SPI_ReadBuffer[BufIndex] == 0x24)
//					{
//                        SPI1BUF = 0x56 ;
//						BufIndex++; CommandState++;
//					}
//					else
//					{
//					  CommandState = BufIndex = 0;
//					}
//
//					break;
//				}
//                	case 2:
//				{
//					if(SPI_ReadBuffer[BufIndex] == 0xAC)
//					{
//                        SPI1BUF = 0x78 ;
//                        BufIndex=0; CommandState=0;
//                             spi1_flag=1;
//					}
//					else
//					{
//					  CommandState = BufIndex = 0;
//					}
//					break;
//				}
                
//            }
//     IFS0bits.SPI1IF = 0;
//     SPI_ReadBuffer = SPI1BUF ;
//
//     		//
//		//	SPI_State 所代表的狀態 :
//		//	0 : 	尚未接收到有效 COMMAND
//		//	0x10:	已經接收到  WRITE COMMAND , 等待 Address 的 High Byte
//		//	0x11:	已經接收到 Address 的 High Byte , 等待接收 Address 的 Low Byte
//		//	0x12:	已經接收到 Address 的 Low Byte , 等待接收要寫入的 Data
//		//			>>	當 SPI_State == 0x12 , 且又有一個 Byte 被接收時 , 此 Byte 為要寫入的 Data
//		//			>>	dsPIC 必需將 Address Low 為指標 , 將 SPI1BUF 的資料寫入 SPI_Buffer[] 內
//		//			>>	記得將 SPI_State 歸零 
//		//
//		//	0x20:	已經接收到  READ COMMAND , 等待 Address 的 High Byte
//		//	0x21:	已經接收到 Address 的 High Byte , 等待接收 Address 的 Low Byte
//		//			>>	當 SPI_State == 0x21 , 且又有一個 Byte 被接收時 , 此 Byte 為 Address Low
//		//			>>	dsPIC 必需將 Address Low 為指標 , 取出在 SPI_Buffer[] 內的資料並寫入 SPI1BUF
//		//				如此資料就會被往外送
//		//	0xff:	表示先前的 COMMAND 已經結束 , 直接將 0x00 寫入 SPI1BUF 即可 
//		//			>> 	記得要將 SPI_State 歸零
//		//
//        spi1_flag=1;
//		switch (SPI_State)
//		{
//
//			case	0 : 										//	@ COMMAND Stage ..................
//					
//					if ( SPI_ReadBuffer == SPI_RD_STATUS)	
//							{ 
//								SPI1BUF = 0x78 ;				//	????? COMMAND
//								SPI_State = 0xff ;				// 
////                                spi1_flag=1;
//							}
//					else if ( SPI_ReadBuffer == SPI_WR_STATUS )	
//							{
//								SPI1BUF = 0x00 ;				//	????? COMMAND
//								SPI_State = 0xff ;
//							}
//					else if ( SPI_ReadBuffer == SPI_EE_WREN )
//							{
//
//								SPI1BUF = 0x00 ;				//	????? COMMAND ( Single Byte )
//								SPI_State = 0 ;
//							}
//					else if ( SPI_ReadBuffer == SPI_EE_WRDIS )
//							{
//
//								SPI1BUF = 0x00 ;				//	????? COMMAND ( Single Byte )
//								SPI_State = 0 ;
//							}
//					else if ( SPI_ReadBuffer == SPI_EE_WRITE )
//							{
//
//								SPI1BUF = 0x00 ;
//								SPI_State = 0x10 ;
//							}
//					else if ( SPI_ReadBuffer == SPI_EE_READ )
//							{
//
//								SPI1BUF = 0x00 ;
//								SPI_State = 0x20 ;
//							}
//					break ;
//			case	0x10 :							//	Address High for Write Command 
//						SPI1BUF = 0x00 ;
//						SPI_State ++ ;
//						break ;
//			case	0x11 :
//						SPI_BufferIndex = SPI1BUF ;
//						SPI_State ++ ;
//						break ;
//			case	0x12 :
//						SPI_Buffer[SPI_BufferIndex] = SPI1BUF ;
//						SPI_BufferIndex ++ ;
//						SPI_State = 0x00 ;
//						break ;
//			case	0x20 :							//	Address High for Write Command 
//						SPI1BUF = 0x00 ;
//						SPI_State ++ ;
//						break ;
//			case	0x21 :
//						SPI_BufferIndex = SPI1BUF ;
//						SPI1BUF = SPI_Buffer[SPI_BufferIndex];
//						SPI_BufferIndex ++ ;
//						SPI_State = 0xff ;
//						break ;					
//			default :
//						SPI1BUF = 0x00 ;
//						SPI_State = 0 ;
//						break ;
//		}
}

SPI1_STATUS SPI1_StatusGet()
{
    return(SPI1STAT);
}
/**
 End of File
*/
