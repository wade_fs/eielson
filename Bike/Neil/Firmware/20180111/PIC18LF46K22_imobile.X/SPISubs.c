
//#include 	<p30F4011.h>
#include	<spi.h>
#include	"SPISubs.h"


#define		MODE_SPICS	ADPCFGbits.PCFG2

volatile int	WaitLoop ;


void	Init_SPI(void)
{
//	unsigned int 	config1 , config2 ;
//
//	//MODE_//SPI_CS = 1 ;								//  CS PIN for SPIis Digital Mode
//	//SPI_CS = 1 ;									//	Inactive CS
//	DIR_//SPI_CS = 0 ;
//	
//	SPI1CON = 0x207c ;								//	0010 0010 0111 1100
//	SPI1STAT = 0xa000 ;								//	1010 0000 0000 0000
//	
//	ConfigIntSPI1(SPI_INT_DIS & SPI_INT_PRI_4) ;

}

int		SPI_EEPROM_ByteWrite(unsigned int SPI_Addr , unsigned char SPI_Data ) 
{
		unsigned char	Status ;

		Status = SPI_EEPROM_StsRead( ) ;				//	Make sure not in WIP ( Write in Process )
			SPI_Delay() ;

				if ((Status & 0b00000001 ))	
					return -1 ;

		SPI_EEPROM_CmdWrite(SPI_EE_WREN);				// 	Enable Write Latch !!
			SPI_Delay() ;

		
		//SPI_CS = 0 ;
			
			SPI_ByteWrite ( SPI_EE_WRITE ) ;				// Send Write Command 
			SPI_ByteWrite ( (SPI_Addr >> 8) & 0x00ff );
			SPI_ByteWrite ( SPI_Addr & 0x00ff ) ;
			SPI_ByteWrite ( SPI_Data ) ;
			
		//SPI_CS = 1 ;

		return 1 ;										//	Indicate Success 
		
}

unsigned char	SPI_ByteWrite(unsigned char SPI_DATA )
{
		int	Dummy ;

		SSP1BUF = SPI_DATA ;
		while ( ! PIR1bits.SSP1IF ) ;
			PIR1bits.SSP1IF = 0 ;
		
		for ( WaitLoop = 0 ; WaitLoop < 50 ; WaitLoop ++ ) ;
		return (unsigned char) SSP1BUF ;

}

int	SPI_EEPROM_CmdWrite(unsigned char SPI_CMD ) 
{

		unsigned char	CmdW_Result ;
		//SPI_CS = 0 ;
		
			CmdW_Result = SPI_ByteWrite(SPI_CMD) ;

		//SPI_CS = 1 ;

		return CmdW_Result ;
		
}

int		SPI_EE_Ready(void)
{
		unsigned char Status ;

		Status = SPI_EEPROM_StsRead( ) ;			//	Make sure not in WIP ( Write in Process )
			SPI_Delay() ;

				if ((Status & 0b00000001 ))	
					return 0 ;
				else
					return 1 ;
}

int		SPI_EEPROM_StsWrite(unsigned char SPI_STS ) 
{
		unsigned char	StsW_Result , Status;

		Status = SPI_EEPROM_StsRead( ) ;			//	Make sure not in WIP ( Write in Process )
			SPI_Delay() ;

				if ((Status & 0b00000001 ))	
					return -1 ;

		SPI_EEPROM_CmdWrite(SPI_EE_WREN);			// 	Enable Write Latch !!
			SPI_Delay() ;

		//SPI_CS = 0 ;

			StsW_Result = SPI_ByteWrite(SPI_WR_STATUS ) ;
	
			StsW_Result = SPI_ByteWrite(SPI_STS) ;
		

		//SPI_CS = 1 ;

			return 1 ;
		
}

unsigned char	SPI_EEPROM_StsRead(void ) 
{
		int		Dummy ;

		//SPI_CS = 0 ;

		SSP1BUF = SPI_RD_STATUS ;
			while ( ! PIR1bits.SSP1IF ) ;
			PIR1bits.SSP1IF = 0 ;
			Dummy = SSP1BUF ;
		
		SSP1BUF = 0xff ;

			while ( ! PIR1bits.SSP1IF ) ;
			PIR1bits.SSP1IF = 0 ;

				//SPI_CS = 1 ;

					return (unsigned char) SSP1BUF ;
}

unsigned char		SPI_EEPROM_ByteRead(unsigned int SPI_Addr  )
{
		unsigned char Temp_Buffer ;

		unsigned		ReadDelay ;

		//SPI_CS = 0 ;
			
			SPI_ByteWrite ( SPI_EE_READ );				// Send Read Command 
			SPI_ByteWrite ( (SPI_Addr >> 8) & 0x00ff );
			SPI_ByteWrite ( SPI_Addr & 0x00ff ) ;
			Temp_Buffer = SPI_ByteWrite ( 0xff ) ;
			
		//SPI_CS = 1 ;

		return	Temp_Buffer ;		

}


