/*****************************************************************************
 *
 * Filename:
 * ---------
 *    Motion_sensor_iic.h
 *
 * Project:
 * --------
 *    Motion Sensor KX023 I2C Interface
 *
 * Description:
 * ------------
 *   This Module defines Serial Interface.
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 ****************************************************************************/

#ifndef MOTION_SENSOR_I2C_H
#define MOTION_SENSOR_I2C_H

/*
#define MS_I2C_CLK_PIN   7		//define GPIO7 for i2c CLK Singal
#define MS_I2C_DATA_PIN  6  	//define GPIO6 for i2c DATA Singal

#define SET_MS_CLK_OUTPUT		GPIO_InitIO(OUTPUT,MS_I2C_CLK_PIN);
#define SET_MS_DATA_OUTPUT		GPIO_InitIO(OUTPUT,MS_I2C_DATA_PIN);
#define SET_MS_DATA_INPUT		GPIO_InitIO(INPUT,MS_I2C_DATA_PIN);

#define SET_MS_CLK_HIGH			GPIO_WriteIO(1,MS_I2C_CLK_PIN);
#define SET_MS_CLK_LOW			GPIO_WriteIO(0,MS_I2C_CLK_PIN);
#define SET_MS_DATA_HIGH		GPIO_WriteIO(1,MS_I2C_DATA_PIN);
#define SET_MS_DATA_LOW			GPIO_WriteIO(0,MS_I2C_DATA_PIN);
#define GET_MS_DATA				GPIO_ReadIO(MS_I2C_DATA_PIN)
*/
#define MS_I2C_CLK_PIN    PORTBBbits.RC3		//define GPIO7 for i2c CLK Singal
#define MS_I2C_DATA_PIN   PORTBBbits.RC4	//define GPIO6 for i2c DATA Singal

#define SET_MS_CLK_OUTPUT	TRISCbits.TRISC3= 0; //set portB pin 8 (red LED) to output
#define SET_MS_DATA_OUTPUT	TRISCbits.TRISC4 = 0; //set portB pin 9 (red LED) to output
#define SET_MS_DATA_INPUT	TRISCbits.TRISC4 = 1; //set portB pin 9 (red LED) to output

#define SET_MS_CLK_HIGH		LATCbits.LATC3 =1;
#define SET_MS_CLK_LOW		 LATCbits.LATC3 =0;
#define SET_MS_DATA_HIGH	LATCbits.LATC4 =1;
#define SET_MS_DATA_LOW		LATCbits.LATC4 =0;
#define GET_MS_DATA		   PORTCbits.RC4// LATBbits.LATB9// (LATB & 0x0200) //PORTBbits.RB9// (PORTB & 0x0200)//PORTBbits.RB9  // (PORTB && 0x0200)

#define I2C_DELAY				20

void US_Delay(uint16_t time);
void MS_Delay(uint16_t time);

void I2C_Start(void);
void I2C_Restart(void);
void I2C_Stop(void);

void motion_senosr_interface_init(void);
void motion_sensor_send_byte(uint8_t send_byte);
uint8_t motion_sensor_get_byte(void);
void motion_sensor_write_data(uint8_t data, uint8_t reg_addr, uint8_t i2c_addr);
uint8_t motion_sensor_read_data(uint8_t reg_addr, uint8_t i2c_addr);
void motion_sensor_read_multi_data(uint8_t *data, uint8_t reg_addr, uint8_t i2c_addr, uint8_t num);
void motion_sensor_disable_bits(uint8_t data, uint8_t reg_addr, uint8_t i2c_addr);
uint8_t  motion_sensor_enable_bits(uint8_t data, uint8_t reg_addr, uint8_t i2c_addr);

#endif
