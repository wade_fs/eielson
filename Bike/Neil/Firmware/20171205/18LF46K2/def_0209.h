/* 
 * File:   def.h
 * Author: vaio
 *
 * Created on 2016年12月15日, 上午 9:15
 */

#ifndef DEF_H
#define	DEF_H

#ifdef	__cplusplus
extern "C" {
#endif

#define TAIL_ADDR0         55 //267    
#define TAIL_ADDR00        56 //268

#define HEADER_SIZE 20
#define HEADER_BUFFER (HEADER_SIZE+2)

#define MULTI_BYTE   0x7B 
#define MULTI_index 7    
#define DATASIZE_l  8
#define DATASIZE_h  9
#define MAX_MULTIFLAG 3   
#define MAX_ONEFLAG   240       
    
    
#define UART_BUFFER     257
#define UART_BUFFERSIZE (UART_BUFFER-2)
    
#define MAX_PC_BUFFER 600
#define MAX_PC_ARRAY  (MAX_PC_BUFFER+2)     
    
#define PHASE1   3
#define PHASE2   20
#define PHASE3   1
#define PHASE4   1
#define PHASE5   2
#define PHASE6   0
#define PHASE7   2
#define PHASE18  0    

 #define ZERO_SIZE 208 
 #define FOUR_SIZE 47

 #define RANG0 0
 #define SIZE0 208
  
  
 #define RANG1 208 // PC_BUF START
 #define SIZE1 32   

 #define CHECKSUM1 256
 #define CHECKSUMH 257
    
#define   BASE1    0x2A //0x52   
#define   EMPTY1   0x20  
    
#define   BASEH   0x05  //0x08   
    
#define    D0_LOC 240
#define    D0_BASE 0xD0
#define    D0_OFFSET 16    
#define    EMPTY_DATA  0x20    
    
 #define MID_BUF_SIZE 650   
    
 #define SKIP1 249
 #define SKIP1_SIZE 10 
    
#define SKIP2 509
#define SKIP2_SIZE 10 

#define LCD_SIZE 16    
    
#define MAX_WAIT_TIME 80
#define MAX_WAIT_TIME2 60000
#define SPEEDUP 5    

#define PC1_SIZE 250
#define PC2_SIZE 500
#define PC3_SIZE 750
#define PC4_SIZE 250

#define TAIL_CHECKSUM_BASE 0x2100    
#define TAIL_BASE          0x71   
#define TAIL_256BASE       0xFF   
    
 
#define DATA_NORMAL_SIZE   224    
#define DATA_MAX_SIZE      256  
#define DATA_MAX_SIZE2     512 
#define TAIL_ADDR1         59 //267    
#define TAIL_ADDR2         60 //268    
#define TAIL_ADDR3         61 //269    
#define TAIL_ADDR4         62 //270    

#define ADJ1_ADDR1         250//267    
#define ADJ1_ADDR2         251 //268    
#define ADJ1_ADDR3         252 //269    
#define ADJ1_ADDR4         253 //270    
#define ADJ1_ADDR5         254 //270 
#define ADJ1_ADDR6         255//270 
    
#define ADJ2_ADDR1         500 //267    
#define ADJ2_ADDR2         501 //268    
#define ADJ2_ADDR3         502 //269    
#define ADJ2_ADDR4         503 //270    
#define ADJ2_ADDR5         504 //270 
#define ADJ2_ADDR6         505 //270 

#define ADJ2_ADDR7         506 //267    
#define ADJ2_ADDR8         507//268    
#define ADJ2_ADDR9         508 //269    
#define ADJ2_ADDR10        509 //270    
#define ADJ2_ADDR11        510 //270 
#define ADJ2_ADDR12        511 //270      
    
#define CHECKSUM1_L        0      
#define CHECKSUM1_H        1      
#define CHECKSUM2_L        2      
#define CHECKSUM2_H        3      
#define CHECKSUM3_L        4      
#define CHECKSUM3_H        5   
#define CHECKSUM4_L        6      
#define CHECKSUM4_H        7
#define CHECKSUM5_L        8      
#define CHECKSUM5_H        9 
#define DATASIZE           10   
    
#define ENTER_CODE 0x10
#define CHECKSUM_BASE     0x50    
    
extern unsigned char UART_Buffer[UART_BUFFER];

extern unsigned int rx_bytes;





#ifdef	__cplusplus
}
#endif
    
#endif	/* DEF_H */

