/*****************************************************************************
 *
 * Filename:
 * ---------
 *    Motion_sensor_iic.c
 *
 * Project:
 * --------
 *    Motion Sensor I2C Interface
 *
 * Description:
 * ------------
 *   This Module defines Serial Interface.
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 ****************************************************************************/
#include "mcc_generated_files/mcc.h"
#include <stdio.h>                  // ??? itoa() ??
//#include "lcd.h"       
//#include "st7066.h" // LCD ??????
#include "motion_sensor_custom.h"
#include "def.h"
#ifdef MOTION_SENSOR_SUPPORT

#include    "motion_sensor_iic.h"  


/*************************************************************************
* FUNCTION
*	Delay function
*
* DESCRIPTION
*	Using for delay time.
*
* PARAMETERS
*  	time (for "US_Delay" function): system executed cycle (1 counts = 1 cycle = 1usec)
*  	time (for "MS_Delay" function): system executed cycle (1 counts = 1000 cycle = 1msec)
*
* RETURNS
*	None 
* 
* GLOBALS AFFECTED
*	None
*
*************************************************************************/
void US_Delay(uint16_t  time)
{
	uint16_t  i;
	for(i=0;i<time;i++);
}

void MS_Delay(uint16_t  time)
{
	uint16_t  i, j;
	for(i=0;i<time;i++)
	{
		for(j=0;j<1000;j++);
	}
}

/*************************************************************************
* FUNCTION
*	I2C command function
*
* DESCRIPTION
*	I2C command : start、restart、stop
*
* PARAMETERS
*  	None
*
* RETURNS
*	None 
* 
* GLOBALS AFFECTED
*	None
*
*************************************************************************/
void I2C_Start(void)
{
	SET_MS_DATA_OUTPUT;
	SET_MS_DATA_HIGH;
	SET_MS_CLK_HIGH;
	SET_MS_DATA_LOW;
	US_Delay(I2C_DELAY);
	SET_MS_CLK_LOW;
	US_Delay(I2C_DELAY);	
}

void I2C_Restart(void)
{
	SET_MS_DATA_OUTPUT;
	SET_MS_DATA_HIGH;
	SET_MS_CLK_HIGH;
	SET_MS_DATA_LOW;
	US_Delay(I2C_DELAY);
	SET_MS_CLK_LOW;
	US_Delay(I2C_DELAY);
}

void I2C_Stop(void)
{
	SET_MS_DATA_OUTPUT;
	SET_MS_DATA_LOW;
	US_Delay(I2C_DELAY);
	SET_MS_CLK_HIGH;
	US_Delay(I2C_DELAY);
	SET_MS_DATA_HIGH;
	US_Delay(I2C_DELAY);
}

/*************************************************************************
* FUNCTION
*	motion_senosr_interface_init
*
* DESCRIPTION
*	Initialization I2C interface.
*
* PARAMETERS
*  	None
*
* RETURNS
*	None 
* 
* GLOBALS AFFECTED
*	None 
*
*************************************************************************/
void motion_senosr_interface_init(void) 
{       
	SET_MS_CLK_HIGH;
	SET_MS_DATA_HIGH;
	SET_MS_CLK_OUTPUT;
	SET_MS_DATA_OUTPUT;		   
}

/*************************************************************************
* FUNCTION
*	motion_sensor_send_byte
*
* DESCRIPTION
*	send a byte data through software I2C interface
*
* PARAMETERS
*	send_byte : send a byte
*
* RETURNS
*	None 
*
* GLOBALS AFFECTED
*	None
*
*************************************************************************/
void motion_sensor_send_byte(uint8_t  send_byte)
{
	int8_t  i;
	
/* MSB  data bit 7~0 */ 
	for (i=7;i>=0;i--)
	{	
		if (send_byte & (1<<i))
		{
			SET_MS_DATA_HIGH;
		}
		else
		{
			SET_MS_DATA_LOW;
		}
		SET_MS_CLK_HIGH;
        US_Delay(I2C_DELAY);
		SET_MS_CLK_LOW;
	}
/* receive ACK from SLAVE */
	SET_MS_DATA_INPUT;
	SET_MS_CLK_HIGH;
	SET_MS_CLK_LOW;
	SET_MS_DATA_OUTPUT;
}	

/*************************************************************************
* FUNCTION
*	motion_sensor_get_byte
*
* DESCRIPTION
*	get a byte data from software I2C interface
*
* PARAMETERS
*	None
*
* RETURNS
*	value : receiver a byte data
* 
* GLOBALS AFFECTED
*	None
*
*************************************************************************/
uint8_t  motion_sensor_get_byte(void)
{     
	int8_t  i;  
	uint8_t  value=0;
	
/*MSB  data bit 7~0 */ 
	SET_MS_DATA_INPUT;
// Jason 20171002     CNPDCbits.CNPDC4 = 1; 
	for (i=7;i>=0;i--)
	{	
		SET_MS_CLK_HIGH;
		US_Delay(I2C_DELAY);
		if (GET_MS_DATA)
			value |= (1 << i);
		
		SET_MS_CLK_LOW;
	}
/* 9th NACK bit */
	SET_MS_DATA_OUTPUT;
	SET_MS_DATA_HIGH;
	SET_MS_CLK_HIGH;
	SET_MS_CLK_LOW; 
	SET_MS_DATA_LOW;

	return value;
}

uint8_t  motion_sensor_get_byte2(void)
{     
	int8_t  i;  
	uint8_t  value=0;
	
/*MSB  data bit 7~0 */ 
	SET_MS_DATA_INPUT;
// Jason 20171002      CNPDBbits.CNPDB9 = 1; 
	for (i=7;i>=0;i--)
	{	
		SET_MS_CLK_HIGH;
		US_Delay(I2C_DELAY);
		if (GET_MS_DATA)
			value |= (1 << i);
		
		SET_MS_CLK_LOW;
	}
/* 9th NACK bit */
	

	return value;
}


/*************************************************************************
* FUNCTION
*	motion_sensor_write_data
*
* DESCRIPTION
*	send a byte data to sensor's register via I2C interface.
*
* PARAMETERS
* 	data     : 	register value
*	reg_addr :  register address
*	i2c_addr :  I2C slave address
*
* RETURNS
*	None
*   
* GLOBALS AFFECTED
*	None
*
*************************************************************************/
void motion_sensor_write_data(uint8_t  data, uint8_t  reg_addr, uint8_t  i2c_addr)
{
	I2C_Start();
	motion_sensor_send_byte((i2c_addr << 1) & 0xFE);
	motion_sensor_send_byte(reg_addr);
	motion_sensor_send_byte(data);
	I2C_Stop();	
}

/*************************************************************************
* FUNCTION
*	motion_sensor_read_data
*
* DESCRIPTION
*	Read a byte data from sensor's register via I2C interface.
*
* PARAMETERS
*  	reg_addr : register address
*	i2c_addr : I2C slave address
*
* RETURNS
*  	value : receiver a byte data
* 
* GLOBALS AFFECTED
*  None
*
*************************************************************************/
uint8_t motion_sensor_read_data(uint8_t  reg_addr, uint8_t  i2c_addr)
{    
	uint8_t  value=0;              
	 
	I2C_Start();         
	motion_sensor_send_byte((i2c_addr << 1) & 0xFE);			
	motion_sensor_send_byte(reg_addr);	
	I2C_Restart();
	motion_sensor_send_byte((i2c_addr << 1) | 0x01);
	value = motion_sensor_get_byte(); 
	I2C_Stop();   
	  
	return value;
}

/*************************************************************************
* FUNCTION
*	motion_sensor_read_multi_data
*
* DESCRIPTION
*	Read a byte data from sensor's register via I2C interface.
*
* PARAMETERS
*  	  *data  : data store (point)
*   reg_addr : register address
*	i2c_addr : I2C slave address
*	    num  : receiver number
*
* RETURNS
*  	None
*   
* GLOBALS AFFECTED
*	None
*
*************************************************************************/
void motion_sensor_read_multi_data(uint8_t  *data, uint8_t  reg_addr, uint8_t  i2c_addr, uint8_t  num)
{
	uint8_t i, cn;
	cn = num;
	
	I2C_Start();         
	motion_sensor_send_byte((i2c_addr << 1) & 0xFE);			
	motion_sensor_send_byte(reg_addr);	
	I2C_Restart();
	motion_sensor_send_byte((i2c_addr << 1) | 0x01);
	
	for ( i=0; i<=cn; i++) 
	{
		data[i] = motion_sensor_get_byte2(); 
	}
	
	I2C_Stop();   
}

/*************************************************************************
* FUNCTION
*	motion_sensor_disable_bits
*
* DESCRIPTION
*	Storage old value & mixed with new value (and)
*
* PARAMETERS
* 	data     : 	register value
*   reg_addr : register address
*	i2c_addr : I2C slave address
*
* RETURNS
*  None
* 
* GLOBALS AFFECTED
*  None
*
*************************************************************************/
void motion_sensor_disable_bits(uint8_t  data, uint8_t  reg_addr, uint8_t  i2c_addr)
{    
	uint8_t  temp_data;
	
	temp_data = motion_sensor_read_data(reg_addr, i2c_addr);
	temp_data &= (~data);
	motion_sensor_write_data(temp_data, reg_addr, i2c_addr);
}

/*************************************************************************
* FUNCTION
*	motion_sensor_enable_bits
*
* DESCRIPTION
*	Storage old value & mixed with new value (or)
*
* PARAMETERS
* 	data     : 	register value
*   reg_addr : register address
*	i2c_addr : I2C slave address
*
* RETURNS
*  None
* 
* GLOBALS AFFECTED
*  None
*
*************************************************************************/
uint8_t  motion_sensor_enable_bits(uint8_t  data, uint8_t  reg_addr, uint8_t  i2c_addr)
{    
	uint8_t  temp_data;
	
	temp_data = motion_sensor_read_data(reg_addr, i2c_addr);
	temp_data |= data;
	motion_sensor_write_data(temp_data, reg_addr, i2c_addr);
}

#endif
